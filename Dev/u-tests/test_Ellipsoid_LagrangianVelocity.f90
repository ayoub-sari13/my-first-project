!===============================================================================
module test_Ellipsoid_LagrangianVelocity
      !===============================================================================
      use Bib_VOFLag_VelocityInterpolationFromEulerianMesh2Point
      use Bib_VOFLag_SubDomain_Limited_PointIn 
      use Bib_VOFLag_SubDomain_Limited
      use Bib_VOFLag_NearestNode
      use test_MPI_Parameters,                 only : t_MPI_Parameters
      use test_Parameters,                     only : t_Parameters
      use test_Particle_Initialisation, only : t_Particle_Initialisation
      use pfunit
      implicit none

      contains
      !> @author Ayoub Sari 06/2023
      ! 
      !> @brief 
      !        
      !        
      !
      !
      !> @todo
      !    
      !
      !-----------------------------------------------------------------------------
      @mpitest(npes=[1])
      subroutine t_Ellipsoid_LagrangianVelocity(this)
      !===============================================================================
      !modules
      !===============================================================================
      use, intrinsic :: iso_fortran_env, only : dp => real64
      !-------------------------------------------------------------------------------
      !Modules de definition des structures et variables
      !-------------------------------------------------------------------------------
      use Module_VOFLag_ParticlesDataStructure      
      use Module_VOFLag_SubDomain_Data
      use mod_Parameters,              only : sxu,sxv,syu,szu,syv,exu,exv,sxw,exw,   &
                                              eyu,eyv,ezu,szv,ezv,syw,eyw,szw,ezw,   &
                                              sxus,exus,syus,eyus,szus,ezus,sxvs,    &
                                              exvs,syvs,eyvs,szvs,ezvs,sxws,exws,    &
                                              syws,eyws,szws,ezws,gx,gy,gz,gxs,gys,  &
                                              gzs,sx,ex,sy,ey,sz,ez
      use mod_Parameters,              only : rank,nproc,deeptracking
      use mod_mpi,                     only : mpi_code,comm3d, coords
      !-------------------------------------------------------------------------------
      !Modules de subroutines de la librairie RESPECT 
      !-------------------------------------------------------------------------------
      use Bib_VOFLag_Ellipsoid_LagrangianVelocity
      use Bib_VOFLag_Sphere_LagrangianVelocity     
      use Bib_VOFLag_Vector_FromInertial2ParticleFrame
      use Bib_VOFLag_Vector_FromComoving2ParticleFrame
      use Bib_VOFLag_SubDomain_Limited_PointIn
      use Bib_VOFLag_NearestNode
      
      !===============================================================================
      !declarations des variables
      !===============================================================================
      !implicit none
      !-------------------------------------------------------------------------------
      !variables globales
      !-------------------------------------------------------------------------------
      class (MpiTestMethod), intent(inout)   :: this
      !-------------------------------------------------------------------------------
      !variables locales 
      !-------------------------------------------------------------------------------
      !-------------------------------------------------------------------------------
      type(struct_vof_lag)                          :: vl
      !-------------------------------------------------------------------------------
      real(8), PARAMETER  :: PI = acos(-1.d0)
      !-------------------------------------------------------------------------------
      real(8), dimension(:,:,:), allocatable        :: u,v,w
      real(8)                                       :: Xmin,Ymin,Zmin,Xmax,Ymax,Zmax
      real(8), allocatable, dimension(:)            :: grid_xu,grid_yv,grid_zw
      real(8), allocatable, dimension(:)            :: grid_x,grid_y,grid_z
      real(8), allocatable, dimension(:)            :: dx,dy,dz
      real(8), allocatable, dimension(:)            :: dxu,dyv,dzw
      integer                                       :: gsx, gsy,gsz,gex,gey,gez,gsxu,gexu,gsyu,&
                                                       geyu,gszu,gezu,gsxv,gexv,gsyv,geyv,gszv,&
                                                       gezv,gsxw,gexw,gsyw,geyw,gszw,gezw
      real(8), allocatable, dimension(:,:,:)        :: vts_point    
      real(8), allocatable, dimension(:,:)          :: point, point_co      
      real(8), dimension(3)                         :: point_b_frame,point_s_frame,&
                                                         coord_deb,coord_fin,omega
      integer, allocatable, dimension(:,:)          :: points_iterpolation
      integer, dimension(3,3)                       :: ijk
      integer                                       :: gridChoice,meshprop
      integer                                       :: nx,ny,nz      
      integer                                       :: nd,np,ndim,nat,i,j,k,a,itr
      logical                                       :: in_domain,leqtrue,regular_mesh
      !-------------------------------------------------------------------------------
      meshprop=0
      ndim=2
      nat=2
      regular_mesh = .true.      
      Xmin=0d0;Xmax=1d0;Ymin=0d0;Ymax=1d0;Zmin=0d0;Zmax=1d0
      call t_Particle_Initialisation(vl,1,[1],nat,ndim)
      
      allocate(vts_point(2*ndim,3,vl%kpt))
      allocate(point(2*ndim,3)); allocate(point_co(2*ndim,3))
      allocate(points_iterpolation(4*ndim-4,3))
    
      if ( meshprop == 0)     then 
         nx=160;ny=160;nz=160
         !nx=32;ny=32;nz=32
      elseif ( meshprop == 1) then 
         nx=16;ny=20;nz=24 ! il faut qu'il soit multiple de 4
      end if 

      gx =3
      gy =3
      gz =3
      gxs=1
      gys=1
      gzs=0

      call t_MPI_Parameters(nx,ny,nz,ndim,this%getMpiCommunicator())
      m_coords = coords     
      call t_Parameters(Xmin,Xmax,Ymin,Ymax,Zmin,Zmax,nx,ny,nz,ndim,meshprop,&
                         grid_xu,grid_yv, grid_zw,grid_x,grid_y, grid_z)
            
      allocate(dx(sx-gx:ex+gx))
      allocate(dy(sy-gy:ey+gy))
      allocate(dz(sz-gz:ez+gz))
      allocate(dxu(sxu-gx:exu+gx))
      allocate(dyv(syv-gy:eyv+gy))
      allocate(dzw(szw-gz:ezw+gz))

      dx=1
      do i=sx-gx,ex+gx
            dx(i)=grid_xu(i+1)-grid_xu(i)
      end do
      dy=1
      do j=sy-gy,ey+gy
            dy(j)=grid_yv(j+1)-grid_yv(j)
      end do
      if (ndim==2) then
            dz=1
      else
            dz=1
            do k=sz-gz,ez+gz
                  dz(k)=grid_zw(k+1)-grid_zw(k)
            end do
      end if
      dxu=1
      do i=sxu-gx+1,exu+gx-1
            dxu(i)=grid_x(i)-grid_x(i-1)
      end do
      do i=sxu-gx,exu+gx
            if (i==gsxu-gx) then 
                  dxu(i)=dxu(i+1)
            else if (i==gexu+gx) then
                  dxu(i)=dxu(i-1)
            end if
      end do
      dyv=1
      do i=syv-gy+1,eyv+gy-1
            dyv(i)=grid_y(i)-grid_y(i-1)
      end do
      do i=syv-gy,eyv+gy
            if (i==gsyv-gy) then 
                  dyv(i)=dyv(i+1)
            else if (i==geyv+gy) then
                  dyv(i)=dyv(i-1)
            end if
      end do
      if (ndim==2) then
            dzw=1
      else
            dzw=1
            do i=szw-gz+1,ezw+gz-1
                  dzw(i)=grid_z(i)-grid_z(i-1)
            end do
            do i=szw-gz,ezw+gz
                  if (i==gszw-gz) then 
                        dzw(i)=dzw(i+1)
                  else if (i==gezw+gz) then
                        dzw(i)=dzw(i-1)
                  end if
            end do
      end if

      allocate(u(sxu-gx:exu+gx,syu-gy:eyu+gy,szu-gz:ezu+gz))
      allocate(v(sxv-gx:exv+gx,syv-gy:eyv+gy,szv-gz:ezv+gz))
      allocate(w(sxw-gx:exw+gx,syw-gy:eyw+gy,szw-gz:ezw+gz))
      ! u=1 
      ! v=1
      ! w=1

      s_domaines_int = 0d0
      call SubDomain_Limited(s_domaines_int,grid_x,grid_y,grid_z)
      coord_deb(1)=Xmin
      coord_deb(2)=Ymin
      coord_fin(1)=Xmax
      coord_fin(2)=Ymax
      if (ndim.eq.3) then
	coord_deb(3)=Zmin
	coord_fin(3)=Zmax
      end if

      !-------------------------------------------------------------------------------
      !-------------------------------------------------------------------------------
      do itr=1,100
	 do np=1,vl%kpt        
	    omega =0d0
	    vts_point(:,:,np) = 0.0d0
	    call random_number(vl%objet(np)%v)
	    call random_number(vl%objet(np)%omeg)
	    omega(3)= vl%objet(np)%omeg(3)
	    if (ndim==3) omega = vl%objet(np)%omeg

	    if (vl%objet(np)%on) then
               point = 0d0
               do nd = 1,ndim
        	  do i = 0,1
                     point_b_frame = 0d0  ! point_b_frame
		     if (vl%nature ==1) then
                	point_b_frame(nd) = point_b_frame(nd) + 0.5d0*(2*i-1)*vl%objet(np)%sca(1)
			point_co(2*nd+i-1,:)= point_b_frame
			point(2*nd+i-1,:) = point_b_frame + vl%objet(np)%pos(:)
		     else if (vl%nature ==2) then
                	point_b_frame(nd) = point_b_frame(nd) + 0.5d0*(2*i-1)*vl%objet(np)%sca(nd)
                      call Vector_FromComoving2ParticleFrame(vl,np,ndim,.false.,point_b_frame,point_co(2*nd+i-1,:)) ! point_co=point - pos	              
                      call Vector_FromInertial2ParticleFrame(vl,np,1,ndim,.false.,point_b_frame,point(2*nd+i-1,:)) ! space-fixed frame
		     end if
		  end do
               end do

               do i = 1,2*ndim
        	  do nd = 1,ndim
                     if (m_period(nd)) then
                	if ( (  point(i,nd) .gt. coord_fin(nd) ) ) then  
                	    point(i,nd) =  point(i,nd) - (coord_fin(nd) - coord_deb(nd) )
                	else if (  point(i,nd) .lt. coord_deb(nd)) then
                	   point(i,nd) =   point(i,nd) + (coord_fin(nd) - coord_deb(nd) )
                	end if
                     end if
        	  end do

        	  call SubDomain_Limited_PointIn(point(i,:),in_domain)
        	  if (in_domain) then
		    vts_point(i,1,np)= vl%objet(np)%v(1) + vl%objet(np)%omeg(2)*point_co(i,3) - vl%objet(np)%omeg(3)*point_co(i,2) 
        	    vts_point(i,2,np)= vl%objet(np)%v(2) + vl%objet(np)%omeg(3)*point_co(i,1) - vl%objet(np)%omeg(1)*point_co(i,3) 
        	    vts_point(i,3,np)= vl%objet(np)%v(3) + vl%objet(np)%omeg(1)*point_co(i,2) - vl%objet(np)%omeg(2)*point_co(i,1) 

        	  end if
        	 ! print*, "vitesse d'interpol lagrangienne_test", vts_point(i,:,:), char(10)

               end do

               do i = 1,2*ndim
        	  do nd = 1,ndim
        	    call NearestNode(grid_x,grid_y,grid_z,grid_xu,grid_yv,grid_zw,dx,dy,dz,dxu, &
                	    dyv,dzw,point(i,:),nd,.true.,regular_mesh,ndim,ijk(nd,:))
        	  end do  
		  if (point(i,1).lt.grid_xu(ijk(1,1)))             ijk(1,1)=ijk(1,1)-1
		  if (point(i,2).lt.grid_y(ijk(1,2)))              ijk(1,2)=ijk(1,2)-1
		  if (ndim==3 .and. point(i,3).lt.grid_z(ijk(1,3))) ijk(1,3)=ijk(1,3)-1
		  if (point(i,1).lt.grid_x(ijk(2,1)))              ijk(2,1)=ijk(2,1)-1
		  if (point(i,2).lt.grid_yv(ijk(2,2)))             ijk(2,2)=ijk(2,2)-1
		  if (ndim==3 .and. point(i,3).lt.grid_z(ijk(2,3))) ijk(2,3)=ijk(2,3)-1
		  if (ndim==3) then
        	     if (point(i,1).lt.grid_xu(ijk(3,1))) ijk(3,1)=ijk(3,1)-1
        	     if (point(i,2).lt.grid_y (ijk(3,2))) ijk(3,2)=ijk(3,2)-1
        	     if (point(i,3).lt.grid_z (ijk(3,3))) ijk(3,3)=ijk(3,3)-1
		  end if
		  do nd= 1,ndim
        	    do j=1,4*ndim-4
        	       points_iterpolation(j,:) = ijk(nd,:)
        	    end do
        	    points_iterpolation(2,1) = ijk(nd,1)+1
        	    points_iterpolation(3,1) = ijk(nd,1)+1 
        	    points_iterpolation(3,2) = ijk(nd,2)+1
        	    points_iterpolation(4,2) = ijk(nd,2)+1
        	    if (ndim==3) then
        	       points_iterpolation(5:8,3) = ijk(nd,3)+1        
        	       points_iterpolation(6,1)   = points_iterpolation(2,1)
        	       points_iterpolation(7,1)   = points_iterpolation(3,1)
        	       points_iterpolation(7,2)   = points_iterpolation(3,2)
        	       points_iterpolation(8,2)   = points_iterpolation(4,2)
        	    end if
        	    do j=1,4*ndim-4
        	       if (nd==1) then
        		  u(points_iterpolation(j,1),points_iterpolation(j,2),points_iterpolation(j,3))=vts_point(i,1,np) 
        	       end if
        	       if (nd==2) then
        		  v(points_iterpolation(j,1),points_iterpolation(j,2),points_iterpolation(j,3))=vts_point(i,2,np)
        	       end if
        	       if (ndim==3) then
        		  if (nd==3) then
                             w(points_iterpolation(j,1),points_iterpolation(j,2),points_iterpolation(j,3))=vts_point(i,3,np)
        		  end if
        	       end if
        	    end do
        	  end do
               end do
	    end if
	    vl%objet(np)%v= 99d0
	    vl%objet(np)%omeg= 99d0
	 end do
      !-------------------------------------------------------------------------------
      !-------------------------------------------------------------------------------
      
	 select case(nat)
	 case(1)      
           call Sphere_LagrangianVelocity(u,v,w,vl,ndim,grid_x,grid_y,&
        	 grid_z,grid_xu,grid_yv,grid_zw,dx,dy,dz,dxu,dyv,dzw)

	 case(2)
           call Ellipsoid_LagrangianVelocity(u,v,w,vl,ndim,grid_x,grid_y,&
        	 grid_z,grid_xu,grid_yv,grid_zw,dx,dy,dz,dxu,dyv,dzw)

	 end select

	 ! print *, 'v_center=',vl%objet(1)%v(:)
         print*,'>>>>>',itr
	 print *, 'omega predicted',vl%objet(1)%omeg(:)
	 print *, 'omega exact    ',omega,char(10)

         @assertEqual(omega,vl%objet(1)%omeg(:), 1.0e-15_dp)
      end do
            
      end subroutine t_Ellipsoid_LagrangianVelocity
    !===============================================================================
        
end module test_Ellipsoid_LagrangianVelocity
!===============================================================================
