!===============================================================================
module Bib_VOFLag_ExtrapolLagrange
   !===============================================================================
   contains
   !-----------------------------------------------------------------------------  
   !> @brief:
   
   
   
   
   
   
   !-----------------------------------------------------------------------------
   subroutine Extrapol_Lagrange()
      !===============================================================================
      !modules
      !===============================================================================
      !-------------------------------------------------------------------------------
      !Modules de definition des structures et variables => src/mod/module_...f90
      !-------------------------------------------------------------------------------
      use mod_Parameters,                      only : deeptracking
      !-------------------------------------------------------------------------------

      !===============================================================================
      !declarations des variables
      !===============================================================================
      implicit none
      !-------------------------------------------------------------------------------
      !variables globales
      !-------------------------------------------------------------------------------
      type(struct_vof_lag), intent(inout)           :: vl
      integer,               intent(in)             :: np, ndim
      real(8)             , intent(in)              :: dt
      integer             , intent(in)              :: Euler
      !-------------------------------------------------------------------------------
      !variables locales 
      !-------------------------------------------------------------------------------
      integer                                      :: i
      !-------------------------------------------------------------------------------

      !-------------------------------------------------------------------------------
      !if (deeptracking) write(*,*) 'entree Integration_Quaternion'
      !-------------------------------------------------------------------------------
function Omegaex_extrapol(LT, omega_) result(ExtrapOmega)
    real, dimension(3), intent(in) :: LT
    real, external :: omega_
    real, dimension(3, 3) :: expol_omega
    real, dimension(3) :: ExtrapOmega
    real, dimension(3) :: temp
    integer :: j, k

    do j = 1, 3
        expol_omega(j, :) = PolyLagrange([LT(1), LT(2), LT(3)], [omega_(LT(1))(j), omega_(LT(2))(j), omega_(LT(3))(j)])
    end do
    
    do j = 1, 3
        temp = expol_omega(j, :)
        do k = 1, 3
            ExtrapOmega(k) = temp(k)
        end do
    end do
end function Omegaex_extrapol

   subroutine Poly_Lagrange(X, Y, P)
      real(8), dimension(:), intent(in)  :: X, Y
      real(8), dimension(:), intent(out) :: P
      !-------------------------------------------------------------------------------
      !variables locales 
      !-------------------------------------------------------------------------------
      real(8), dimension(size(X)) :: li
      integer                     :: i, j

      !-------------------------------------------------------------------------------
      !-------------------------------------------------------------------------------
      P = 0d0
      do i = 1, size(X)
          li = 1d0
          do j = 1, size(X)
              if (i /= j) then
                  li = multipol(li, [-X(j) / (X(i) - X(j)), 1d0 / (X(i) - X(j))])
              end if
          end do
          P = addpol(P, multiscalPol(Y(i), li))
      end do

      !-------------------------------------------------------------------------------
      !-------------------------------------------------------------------------------
   end subroutine Poly_Lagrange

    function Omegaex_extrapol(a,b,c,LT) result(ExtrapOmega)
        real(8), intent(in)               :: a,b,c
	real(4), dimension(3), intent(in) :: LT
	real(4), dimension(3, 3)          :: expol_omega, om
	real(8), dimension(3)             :: ExtrapOmega
	real(8)                           :: t
	integer :: j
        
!	do j=1,3
!	    om(j,:)= omega_ex(a,b,c,LT(j))	
!	end do
	om=1d0
	do j = 1, 3
	   call Poly_Lagrange([LT(1), LT(2), LT(3)], [om(1,j), om(2,j), om(3,j)], expol_omega(j, :))
	end do
        ExtrapOmega=1
!	do j = 1, 3
!	   t = LT(j)
!           ExtrapOmega(j) = Eval(expol_omega(j, :),t)
!	end do
    end function Omegaex_extrapol





   subroutine Poly_Lagrange()
      !===============================================================================
      !modules
      !===============================================================================
      !-------------------------------------------------------------------------------
      !Modules de definition des structures et variables => src/mod/module_...f90
      !-------------------------------------------------------------------------------
      use mod_Parameters,                      only : deeptracking
      !-------------------------------------------------------------------------------

      !===============================================================================
      !declarations des variables
      !===============================================================================
      implicit none
      !-------------------------------------------------------------------------------
      !variables globales
      !-------------------------------------------------------------------------------
      type(struct_vof_lag), intent(inout)           :: vl
      integer,               intent(in)             :: np, ndim
      real(8)             , intent(in)              :: dt
      integer             , intent(in)              :: Euler
      !-------------------------------------------------------------------------------
      !variables locales 
      !-------------------------------------------------------------------------------
      integer                                      :: i
      !-------------------------------------------------------------------------------

      !-------------------------------------------------------------------------------
      !if (deeptracking) write(*,*) 'entree Integration_Quaternion'
      !-------------------------------------------------------------------------------










      !-------------------------------------------------------------------------------
      !if (deeptracking) write(*,*) 'sortie Integration_Quaternion'
      !-------------------------------------------------------------------------------
   end subroutine Poly_Lagrange


   function prod_quater(q1,q2) result(q3)
     !-----------------------------------------------

     real(8),dimension(4), intent(in) :: q1,q2
     real(8),dimension(4)             :: q3

     q3(1)= q1(1)*q2(1) - q1(2)*q2(2) - q1(3)*q2(3) - q1(4)*q2(4)
     q3(2)= q1(1)*q2(2) + q2(1)*q1(2) + q1(3)*q2(4) - q1(4)*q2(3)
     q3(3)= q1(1)*q2(3) + q2(1)*q1(3) + q1(4)*q2(2) - q1(2)*q2(4)
     q3(4)= q1(1)*q2(4) + q2(1)*q1(4) + q1(2)*q2(3) - q1(3)*q2(2)
     return

   end function prod_quater


   function inv_quater(q)

     real(8), dimension(4), intent(inout) :: q
     real(8), dimension(4) :: inv_quater
     q(1)=q(1)
     q(2:4)= -q(2:4)
     inv_quater= q/(norm_quater(q)**2)

   end function inv_quater

   function norm_quater(q)

     real(8), dimension(4), intent(in) :: q
     real(8)                           :: norm_quater

     norm_quater=sqrt(q(1)**2 + q(2)**2 + q(3)**2 + q(4)**2 )

   end function norm_quater

   !===============================================================================
end module Bib_VOFLag_ExtrapolLagrange
!===============================================================================
