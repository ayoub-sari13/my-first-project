!===============================================================================
module test_Particle_Orientation_Update
      !===============================================================================
      use test_Particle_Initialisation, only : t_Particle_Initialisation
      use test_Parameters,              only : t_Parameters
      use pfunit
      implicit none
      integer :: ndim=3

      contains
      !> @author Andelhamid Hakkoum 11/2022
      ! 
      !> @brief 
      !        
      !        
      !
      !
      !> @todo
      !    
      !
      !-----------------------------------------------------------------------------
      !@suite(name='test_projectile_motion_mod_suite')
      @mpitest(npes=[1])
      subroutine t_Particle_Orientation_Update(this)
      !===============================================================================
      !modules
      !===============================================================================
      use, intrinsic :: iso_fortran_env, only : dp => real64
      !-------------------------------------------------------------------------------
      !Modules de definition des structures et variables
      !-------------------------------------------------------------------------------
      use Module_VOFLag_ParticlesDataStructure
      use Module_VOFLag_SubDomain_Data
      use mod_Parameters,                      only : deeptracking,dt
      !===============================================================================
      use Bib_VOFLag_Particle_Orientation_Update
      use Bib_VOFLag_SubDomain_Limited_PointIn 
      use Bib_VOFLag_SubDomain_Limited      
      use Bib_VOFLag_Ellipsoid_LagrangianVelocity
      !===============================================================================
      !declarations des variables
      !===============================================================================
      !
      !-------------------------------------------------------------------------------
      class (MpiTestMethod), intent(inout)   :: this
      !-------------------------------------------------------------------------------
      !variables locales 
      !-------------------------------------------------------------------------------
      type(struct_vof_lag)                 :: vl
      !-------------------------------------------------------------------------------
      real(8), allocatable, dimension(:)   :: grid_xu,grid_yv, grid_zw
      real(8), allocatable, dimension(:)   :: grid_x,grid_y, grid_z
      real(8)             , dimension(3)   :: Xn, pos_b4
      real(8)                              :: Xmin,Ymin,Zmin,Xmax,Ymax,Zmax
      real(8)                              :: a,b,c
      integer                              :: nx, ny, nz
      integer                              :: meshprop
      logical                              :: in_domain,regular_mesh
      real(8), dimension(4,4)              :: mat,mat_inverse
      real(8), dimension(4)                :: quaternion
      real(8)                              :: n, Teta, Teta_n
      integer                              :: i,j,np,nd,Euler,itr

      !-------------------------------------------------------------------------------
      dt = 1d-2
      ndim = 2 
     ! nat= 2

      deeptracking = .false.
      meshprop=0
      Xmin=0d0;Xmax=1d0;Ymin=0d0;Ymax=1d0;Zmin=0d0;Zmax=1d0

      if ( meshprop == 0)     then 
         regular_mesh = .true.
         nx=64;ny=64;nz=64
      elseif ( meshprop == 1) then
         !regular_mesh = .false.
         nx=16;ny=20;nz=24 ! il faut qu'il soit multiple de 4
      end if 

      call t_Parameters(Xmin,Xmax,Ymin,Ymax,Zmin,Zmax,nx,ny,nz,ndim,meshprop,&
                         grid_xu,grid_yv, grid_zw,grid_x,grid_y, grid_z)

      call t_Particle_Initialisation(vl,1,[1],2,ndim)
      !-------------------------------------------------------------------------------
      !-------------------------------------------------------------------------------

      do itr=1, 100      
        call SubDomain_Limited(s_domaines_int,grid_x,grid_y,grid_z)
        call SubDomain_Limited_PointIn(vl%objet(1)%pos(:),in_domain)
        print*, '[T/F]', in_domain
        if (in_domain) vl%objet(1)%in = .true.
	print*,'in [T/F]', vl%objet(1)%in

        if (ndim==2) then
           Euler = 1
           call random_number(vl%objet(1)%omeg(:))
           call random_number(a)
	   vl%objet(1)%omeg= a
	   call random_number (Teta_n)
           vl%objet(1)%orientation(1)= Teta_n
	   Teta = a*dt +vl%objet(1)%orientation(1)

           call Particle_Orientation_Update(vl,Euler,ndim)

           print*,'>>>>>',itr
	   print*,'orientation at n+1',vl%objet(1)%orientation(1)
           print*,'prediction at n+1',Teta
	   print*, abs(vl%objet(1)%orientation(1)-Teta),char(10)

           @assertEqual(Teta ,vl%objet(1)%orientation(1), 1.0e-15_dp)
          !-------------------------------------------------------------------------------
          !-------------------------------------------------------------------------------
!           Euler = 2
!
!           call random_number(a)
!           call random_number(b)
!	   vl%objet(1)%omegn(:)= b
!	   vl%objet(1)%omeg(:)= a*dt
!	   call random_number (Teta_n)
!           vl%objet(1)%orientation(1)= Teta_n
!	   Teta = 0.5*a*dt*dt +b*dt +vl%objet(1)%orientation(1)
!           Teta = vl%objet(1)%orientation(1)
!           vl%objet(1)%orientation(1)= Teta_n
!
!           call Particle_Orientation_Update(vl,Euler,ndim)
!           print*,'orientation at n',Teta_n
!           print*,'orientation at n+1',vl%objet(1)%orientation(1)
!           print*,'prediction at n+1',Teta
!
!           @assertEqual(Teta ,vl%objet(1)%orientation(1), 1.0e-2_dp)
          !-------------------------------------------------------------------------------
          !-------------------------------------------------------------------------------
        else if (ndim==3) then
	  
        end if

      end do 
      end subroutine t_Particle_Orientation_Update
    !===============================================================================
        
end module test_Particle_Orientation_Update
!===============================================================================
 
