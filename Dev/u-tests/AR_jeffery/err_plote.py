import matplotlib.pyplot as plt
import numpy as np
from math import *
import os,sys


def Affiche_plot(ndim,num):
    if ndim == 3:
        d = 9
    elif ndim == 2:
        d = 3
    else:
        print(f"Invalid ndim argument: {ndim}")
        return
    
    filename = '../output_numercial_integration2D.dat'  
    
    if not os.path.isfile(filename):
        print("Error: File does not exist.")
        return
    
    print(f"Reading data from {filename}")
    data = np.loadtxt(filename)
    
    nb_tir = data[:, 0]
    err = data[:, d]
    
    if num != 0:
       err= np.column_stack((data[:, d], data[:, d+1], data[:, d+4]))
       
    fig= plt.figure()    
    ax= fig.add_subplot(1,1,1)
    #ax.plot(nb_tir,, color='blue', label='Data')
    ax.plot(nb_tir, err)
    ax.set_title('')
    ax.set_xlabel('time')
    ax.set_ylabel(' Err rel [%] ')
    #plt.legend()
    plt.show()
    #print(err[:100,:])
    

      
def Affiche_2plot(ndim):
    if ndim == 3:
        d = 9
    elif ndim == 2:
        d = 1
    else:
        print(f"Invalid ndim argument: {ndim}")
        return
    
    
    filename1 = '../output_numercial_integration2D.dat'
    #filename2 = '../output_numercial_integration2D.dat'
     
    filenames = [filename1]
    
    for filename in filenames:
      if not os.path.isfile(filename):
          print("Error: File does not exist.")
          return
    
    print(f"Reading data from {filename1}")
    data1 = np.loadtxt(filename1)
    nb_tir = data1[:, 0]
    err1 = data1[:, d]
    err2 = data1[:, d+1]


    fig= plt.figure()
    ax1 = fig.add_subplot(1, 1, 1)
    ax1.plot(nb_tir, err2, label='sol jeffery ')
    ax1.plot(nb_tir, err1, label='sol integrat')
    ax1.set_title(' ')
    ax1.set_xlabel('time')
    ax1.set_ylabel('Relative Err [%]')


    plt.legend(loc='upper right')
    fig.tight_layout()
    plt.show()
 
# Program
ndim = 2


Affiche_plot(ndim, 0)
Affiche_2plot(ndim)
#Affiche_nplot(ndim,nature)
