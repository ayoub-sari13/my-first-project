!===============================================================================
module test_Jeffery
      !===============================================================================
      use test_Particle_Initialisation, only : t_Particle_Initialisation
      use test_Parameters,              only : t_Parameters
      use pfunit
      implicit none
      integer :: ndim=3

      contains
      !> @author Andelhamid Hakkoum 11/2022
      ! 
      !> @brief 
      !        
      !        
      !
      !
      !> @todo
      !    
      !
      !-----------------------------------------------------------------------------
      !@suite(name='test_projectile_motion_mod_suite')
      @mpitest(npes=[1])
      subroutine t_Jeffery(this)
      !===============================================================================
      !modules
      !===============================================================================
      use, intrinsic :: iso_fortran_env, only : dp => real64
      !-------------------------------------------------------------------------------
      !Modules de definition des structures et variables
      !-------------------------------------------------------------------------------
      use Module_VOFLag_ParticlesDataStructure
      use Module_VOFLag_SubDomain_Data
      use mod_Parameters,                      only : deeptracking,dt
      !===============================================================================
      use Bib_VOFLag_Particle_Orientation_Update
      use Bib_VOFLag_SubDomain_Limited_PointIn 
      use Bib_VOFLag_SubDomain_Limited      
      use Bib_VOFLag_Ellipsoid_LagrangianVelocity
      !===============================================================================
      !declarations des variables
      !===============================================================================
      !
      !-------------------------------------------------------------------------------
      class (MpiTestMethod), intent(inout)   :: this
      !-------------------------------------------------------------------------------
      !variables locales 
      !-------------------------------------------------------------------------------
      type(struct_vof_lag)                 :: vl
      !-------------------------------------------------------------------------------
      real(8), allocatable, dimension(:)   :: grid_xu,grid_yv, grid_zw
      real(8), allocatable, dimension(:)   :: grid_x,grid_y, grid_z
      real(8)             , dimension(3)   :: Xn, pos_b4
      real(8)                              :: Xmin,Ymin,Zmin,Xmax,Ymax,Zmax
      real(8)                              :: a,b,c
      integer                              :: nx, ny, nz
      integer                              :: meshprop
      logical                              :: in_domain,regular_mesh
      real(8), dimension(4,4)              :: mat,mat_inverse
      real(8), dimension(4)                :: quaternion
      real(8)                              :: n, Teta, Teta_n, gamma, t
      integer                              :: i,j,np,nd,Euler,AR,itr
      character(len=200) :: filename1,filename2
      !-------------------------------------------------------------------------------
      dt = 1d-2
      ndim = 3
      AR=1                  ! less than 5
      gamma= 10
      deeptracking = .false.
      meshprop=0
      Xmin=0d0;Xmax=1d0;Ymin=0d0;Ymax=1d0;Zmin=0d0;Zmax=1d0

      print*, ndim
      if ( meshprop == 0)     then 
         regular_mesh = .true.
         nx=64;ny=64;nz=64
      elseif ( meshprop == 1) then
         !regular_mesh = .false.
         nx=16;ny=20;nz=24 ! il faut qu'il soit multiple de 4
      end if 

      call t_Parameters(Xmin,Xmax,Ymin,Ymax,Zmin,Zmax,nx,ny,nz,ndim,meshprop,&
                         grid_xu,grid_yv, grid_zw,grid_x,grid_y, grid_z)

      call t_Particle_Initialisation(vl,1,[1],2,ndim)

      !-------------------------------------------------------------------------------
      !-------------------------------------------------------------------------------
      write(filename1,'(A,I0,A)') 'output_numercial_integration.dat'        
      open(unit=1000,file=filename1,status='replace')
      write(1000,'(a1,11a16)',advance='no')'#',' -----time----- ',&   
                                            ' ---q0--        ',&
                                            ' --q1--         ',&
                                            ' --q2--         ',&
                                            ' --q3--         ',&
                                            ' --q0_J---      ',&
                                            ' --q1_J---      ',&
                                            ' --q2_J---      ',&
					    ' --q3_J---      ',&
					    '-- max|q-q_J|-- '
      close(1000)

    !  write(filename2,'(A,I0,A)') 'output_jeffery.dat'        
    !  open(unit=1001,file=filename2,status='replace')
    !  write(1001,'(a1,11a16)',advance='no')'#',' ----time---- ',&   
    !                                        ' ---Err Press-- ',&
    !                                        ' --Err Visc 1-- ',&
    !                                        ' --Err Visc 2-- ',&
    !                                        ' --Err Visc 3-- ',&
    !                                        ' --Err Vts 1--- ',&
    !                                        ' --Err Vts 2--- ',&
    !                                        ' --Err Vts 3--- '
    !  close(1001)
      
      !-------------------------------------------------------------------------------
      !-------------------------------------------------------------------------------
      vl%objet(1)%orientation(1)= 0d0      
      if(ndim==3)  vl%objet(1)%orientation= (/1d0,0d0,0d0,0d0/)       
      quaternion= (/1d0,0d0,0d0,0d0/)
      do itr=1, 100     
        !call SubDomain_Limited(s_domaines_int,grid_x,grid_y,grid_z)
        !call SubDomain_Limited_PointIn(vl%objet(1)%pos(:),in_domain)
	vl%objet(1)%in =.true.
	!if (in_domain) vl%objet(1)%in = .true.
        !call random_number(n)
	!AR= 1+ floor(5*n)
	!if (AR.gt.5) AR=5
	vl%objet(1)%sca(1:2) = 0.1d0
        vl%objet(1)%sca(3) = AR*vl%objet(1)%sca(1)
        if (ndim==2) vl%objet(1)%sca(2) = AR*vl%objet(1)%sca(1)
	
	t= itr*(dt)	
	!-------------------------------------------------------------------------------
	if (ndim==2) then
           Euler = 1
	   Teta_n = 0d0	   
	   Teta = atan(AR*tan(gamma*AR*t/(AR**2+1)+Teta_n))
           !vl%objet(1)%orientation(1)= 0d0
	   !Teta_n= atan(tan(Teta_n)/AR)
	   vl%objet(1)%omeg= gamma*AR**2*((tan(gamma*AR*t/(AR**2+1)+Teta_n))**2+1)/((AR**2+1)*&
	   (AR**2*(tan(gamma*AR*t/(AR**2+1)+Teta_n))**2+1))

           call Particle_Orientation_Update(vl,Euler,ndim)

           print*,'>>>>>',itr,'>>>>>>', 'AR=',AR
	   !print*,'orientation at n', Teta_n
	   print*,'orientation at n+1',vl%objet(1)%orientation(1)
           print*,'prediction  at n+1',Teta
	   print*, abs(vl%objet(1)%orientation(1)-Teta),char(10)

           @assertEqual(Teta ,vl%objet(1)%orientation(1), 1.0e-3_dp)
          !-------------------------------------------------------------------------------
        else if (ndim==3) then
           Euler = 1
	   Teta_n = 0d0
	   Teta = atan(AR*tan(gamma*AR*t/(AR**2+1)+Teta_n))
           !vl%objet(1)%orientation=quaternion
	   !Teta_n= atan(tan(Teta_n)/AR)
	   vl%objet(1)%omeg(3)= gamma*AR**2*((tan(gamma*AR*t/(AR**2+1)+Teta_n))**2+1)/((AR**2+1)*&
	   (AR**2*(tan(gamma*AR*t/(AR**2+1)+Teta_n))**2+1))
           quaternion= (/cos(Teta/2d0),0d0,0d0,sin(Teta/2d0)/)
           
	   call Particle_Orientation_Update(vl,Euler,ndim)

           print*,'>>>>>',itr,'>>>>>>', 'AR=',AR,'>>>>>>','t=',t
	   print*,'orientation at n+1',vl%objet(1)%orientation
           print*,'prediction  at n+1',(/cos(Teta/2d0),0d0,0d0,sin(Teta/2d0)/)
	   print*, maxval(abs(vl%objet(1)%orientation-quaternion)),char(10)
          !-------------------------------------------------------------------------------
           open(unit=1000,file=filename1,status='old',position='append')
           write(1000,'(10E16.8)',advance='no') t,&
                                               vl%objet(1)%orientation(1),&
                                               vl%objet(1)%orientation(2),&
                                               vl%objet(1)%orientation(3),&
                                               vl%objet(1)%orientation(4),&
                                               quaternion(1),&
                                               quaternion(2),&
                                               quaternion(3),&
					       quaternion(4),&
					       maxval(abs(vl%objet(1)%orientation-quaternion))
					       
           close(1000)

      ! @assertEqual(quaternion ,vl%objet(1)%orientation, 1.0e-3_dp)
	  
        end if
      end do 
      end subroutine t_Jeffery
    !===============================================================================
        
end module test_Jeffery
!===============================================================================
 
