  
 set out
 set term x11
 set title "Jeffery orbit| deltaT=0.0726056 |NBITER=1e6"
 set xlabel " t  "
 set ylabel "Angle"


r=5.
gamma=10.
#-----------------------------------------
f(x) = atan(r*tan((r*gamma*x)/(r**2+1.))) 
#-----------------------------------------

#plot "File_angle_dmm.dat" using 1:2   title "DMM" with points pt 3
#replot "File_angle_dmm_exact.dat" using 1:2   title "DMM_exact" with points pt 5
#replot "FILE_ANGLE_AB4.DAT" using 1:2   title "AB3" with points pt 5
plot "FILE_ANGLE_PCDMM.DAT" using 1:2   title "PCDMM" with points pt 5
#replot "File_angle_ABM.dat" using 1:2   title "AB2M" with points pt 7

#-----------------------------------------
replot f(x) with line ls 3 linewidth 1
#-----------------------------------------

 pause -1 "Press a key to continue"
 #===Output of the file ==========
 set terminal postscript eps color "Times-Roman" 16
 set output "test-26-08-all.ps"
 replot
 set term pop
