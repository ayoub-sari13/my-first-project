module lib_integration_method

  implicit none 

  double precision, PARAMETER                  :: Pi = acos(-1.)


CONTAINS


  !------------------------------------------------
  ! DIRECT MULTIPLICATION METHOD USING QUATERNIONS
  !------------------------------------------------
  !------------------------------------------------
  !                BEGIN SUBROUTINE DDM
  !------------------------------------------------

  SUBROUTINE DirectMultiplicationMethod(Quaternion,omega,TEMPS,I)


    double precision,dimension(:,:),intent(inout)    :: Quaternion
    double precision,intent(in)                           :: TEMPS
    double precision,dimension(3),intent(in)              :: Omega
    integer,intent(in)                                    :: I
    double precision,dimension(4)                         :: Quaternion_tild
    double precision                                      :: norme 


    norme = sqrt(sum( (omega(:))**2 ) )
    Quaternion_tild=0.d0

    Quaternion_tild(1)=cos((norme*TEMPS)/2.d0)
    Quaternion_tild(2)=sin((norme*TEMPS)/2.d0)*(omega(1)/norme)
    Quaternion_tild(3)=sin((norme*TEMPS)/2.d0)*(omega(2)/norme)
    Quaternion_tild(4)=sin((norme*TEMPS)/2.d0)*(omega(3)/norme)
    !----------------------------------------------------------------
    Quaternion(:,I)=multipl_Quater(Quaternion_tild,Quaternion(:,I-1))
    !----------------------------------------------------------------
  END SUBROUTINE DirectMultiplicationMethod
  !------------------------------------------------








  !------------------------------------------------
  ! DIRECT MULTIPLICATION METHOD USING QUATERNIONS
  !------------------------------------------------
  !------------------------------------------------
  !                BEGIN SUBROUTINE DDM
  !------------------------------------------------

  SUBROUTINE DirectMultiplicationMethod_pcdmm(Quaternion,omega,DELTA_T,I)

    double precision,dimension(:,:),intent(inout)         :: Quaternion
    double precision,intent(in)                           :: DELTA_T
    double precision,dimension(:,:),intent(inout)         :: Omega
    integer,intent(in)                                    :: I
    double precision,dimension(4)                         :: Quaternion_tild,vecteur
    double precision                                      :: norme 

    vecteur=omega(:,I)
    norme = sqrt(sum(vecteur**2))
    Quaternion_tild=0.d0

    Quaternion_tild(1)=cos((norme*DELTA_T)/2.d0)
    Quaternion_tild(2)=sin((norme*DELTA_T)/2.d0)*(omega(2,I)/norme)
    Quaternion_tild(3)=sin((norme*DELTA_T)/2.d0)*(omega(3,I)/norme)
    Quaternion_tild(4)=sin((norme*DELTA_T)/2.d0)*(omega(4,I)/norme)
    !----------------------------------------------------------------
    Quaternion(:,I)=multipl_Quater(Quaternion_tild,Quaternion(:,I-1))
    !----------------------------------------------------------------
  END SUBROUTINE DirectMultiplicationMethod_pcdmm
  !------------------------------------------------



  !-----------------------------------------------
  function multipl_Quater (q1,q2) result(q3)
    !-----------------------------------------------

    implicit none
    double precision,dimension(4), intent(in) :: q1,q2
    double precision,dimension(4)             :: q3

    q3(1)=q1(1)*q2(1) - dot_product(q2(2:4),q1(2:4))
    q3(2:4) = q1(1)*q2(2:4)+q2(1)*q1(2:4)+ cross(q1(2:4),q2(2:4))

    return

  end function multipl_Quater
  !==============================================================


  !-----------------------------------------------
  function solution_exacte (TEMPS,shear_rate, aspect_ratio) result(z)
    !-----------------------------------------------
    implicit none
    double precision,intent(in)             :: aspect_ratio,shear_rate,TEMPS
    double precision                        :: z
    z=atan(aspect_ratio*tan( (aspect_ratio*shear_rate*TEMPS )/(aspect_ratio**2+1.)))

    return
  end function solution_exacte






  !=============================================================
  subroutine angular_velocity(r,gamma,TEMPS,delta_t,z) 
    !============================================================

    implicit none
    double precision, intent(in)               :: r,gamma,DElta_t
    double precision, dimension(3),intent(out) :: z
    double precision, intent(inout)               :: TEMPS

    TEMPS=TEMPS-delta_T*1.5

    z(1) = 0.
    z(2) = 0.
    z(3) = (1.+ tan((r*gamma*TEMPS )/(r**2+1.))**2 )*(gamma*r**2) / ((r**2 + 1.)*(1.+(r**2)*tan((r*gamma*TEMPS )/(r**2+1.))**2))

  end subroutine angular_velocity


  !=============================================================
  subroutine angular_velocity_pcdmm(r,gamma,TEMPS,z,I) 
    !============================================================

    implicit none
    INTEGER,intent(in)                                  ::I
    double precision, intent(in)                        :: r,gamma,TEMPS
    double precision, dimension(:,:),intent(inout) :: z

    z(1,I)=0.d0
    z(2,I) = 0.d0
    z(3,I) = 0.d0
    z(4,I) = (1.+ tan((r*gamma*TEMPS )/(r**2+1.))**2 )*(gamma*r**2) / ((r**2 + 1.)*(1.+(r**2)*tan((r*gamma*TEMPS )/(r**2+1.))**2))

  end subroutine angular_velocity_pcdmm





  FUNCTION cross(a, b)
    double precision, DIMENSION(3) :: cross
    double precision, DIMENSION(3), INTENT(IN) :: a, b

    cross(1) = a(2) * b(3) - a(3) * b(2)
    cross(2) = a(3) * b(1) - a(1) * b(3)
    cross(3) = a(1) * b(2) - a(2) * b(1)
  END FUNCTION cross



  FUNCTION norm_quater(a)

    double precision :: norm_quater
    double precision, DIMENSION(4), INTENT(IN) :: a

    norm_quater=sqrt(a(1)**2 + a(2)**2 + a(3)**2 + a(4)**2 )

  END FUNCTION norm_quater





  SUBROUTINE AB3(TEMPS,DELTA_T,QUATERNION,R,GAMMA,NB_PERIOD,NBITER_PERIOD)

    DOUBLE PRECISION,DIMENSION(4,4),INTENT(INOUT)    :: QUATERNION
    DOUBLE PRECISION,INTENT(IN)                      :: R,GAMMA
    DOUBLE PRECISION,DIMENSION(4),INTENT(INOUT)      :: TEMPS
    DOUBLE PRECISION,INTENT(IN)                      :: DELTA_T
    DOUBLE PRECISION,DIMENSION(4,3)                  :: B
    INTEGER                                          :: I,K,J
    INTEGER,INTENT(IN)                               :: NBITER_PERIOD,NB_PERIOD
    DOUBLE PRECISION                                 :: PHI,ANGLE

    !---------------------------------------------------
    OPEN(UNIT=100,FILE='FILE_ANGLE_AB4.DAT',STATUS='UNKNOWN')
    !---------------------------------------------------
    J=3

    DO J=4,NB_PERIOD*NBITER_PERIOD
       B=0.D0
       DO I=1,3 
          B(:,I)=F(TEMPS(4-I),QUATERNION(:,4-I),R,GAMMA)
       END DO

       TEMPS(4)=TEMPS(3)+DELTA_T
       QUATERNION(:,4)=QUATERNION(:,3)+DELTA_T*(23.D0*B(:,1)-16.D0*B(:,2)+5.D0*B(:,3))/12.D0        
       !RENORMALISATION
       QUATERNION(:,4)=QUATERNION(:,4)/NORM_QUATER(QUATERNION(:,4))

       !------------------------------------------------------------------

       IF (J .GE. (NB_PERIOD-2)*(NBITER_PERIOD)) THEN

          PHI = 2.D0*ACOS(QUATERNION(1,4))
          ANGLE=PHI
          IF ((ANGLE .GE. 0.D0).AND. (ANGLE .LE. PI/2.D0) ) THEN  
             IF ( MOD( CEILING(REAL(J)/REAL(NBITER_PERIOD)),2) .EQ. 0 ) THEN
                WRITE(100,*) TEMPS(4), -ANGLE
             ELSE
                WRITE(100,*) TEMPS(4), ANGLE
             END IF
          ELSE
             IF  ((ANGLE .GE. PI/2.D0).AND.(ANGLE .LE. 3*PI/2.D0) ) THEN  
                IF ( MOD( CEILING(REAL(J)/REAL(NBITER_PERIOD)),2) .EQ. 0 ) THEN
                   WRITE(100,*) TEMPS(4), -ANGLE + PI
                ELSE
                   WRITE(100,*) TEMPS(4), ANGLE - PI
                END IF
             ELSE
                IF  ((ANGLE .GE. 3*PI/2.D0).AND.(ANGLE .LE. 2*PI) ) THEN  
                   IF ( MOD( CEILING(REAL(J)/REAL(NBITER_PERIOD)),2) .EQ. 0 ) THEN
                      WRITE(100,*) TEMPS(4), -ANGLE + 2.D0*PI
                   ELSE
                      WRITE(100,*) TEMPS(4), ANGLE - 2.D0*PI
                   END IF
                END IF
             END IF
          END IF
       END IF

       !---------------------------------------------------------------------
       DO K=1, 3
          TEMPS(K)=TEMPS(K+1)
          QUATERNION(:,K)=QUATERNION(:,K+1)
       END DO

    END DO
10  FORMAT(F12.6,F12.6,F12.6,A5)
11  FORMAT(F12.6,F12.6,F12.6,F12.6)
    CLOSE(100)

  END SUBROUTINE AB3



  FUNCTION F(TEMPS,QUATERNION,R,GAMMA)

    DOUBLE PRECISION,DIMENSION(4),INTENT(IN)    :: QUATERNION
    DOUBLE PRECISION,INTENT(IN)                 :: R,GAMMA
    DOUBLE PRECISION,DIMENSION(4)               :: F
    DOUBLE PRECISION,INTENT(IN)                 :: TEMPS
    DOUBLE PRECISION,DIMENSION(4)               :: OMEGA

    OMEGA=0.D0
    !----------------------------------------
    OMEGA=ANGULAR_VELOCITY_AB(R,GAMMA,TEMPS) 
    !----------------------------------------
    F=0.5D0*MULTIPL_QUATER(OMEGA,QUATERNION)
    RETURN 
  END FUNCTION F



  !RUNGE-KUTTA METHOD TO CALCULATE FIRST TWO POINTS ONLY
  SUBROUTINE RK4(K,DELTA_T,TEMPS,Y,R,GAMMA)

    INTEGER,INTENT(IN)                             ::  K
    DOUBLE PRECISION,DIMENSION(4,4),INTENT(INOUT)  :: Y
    DOUBLE PRECISION,DIMENSION(4),INTENT(INOUT)    :: TEMPS
    DOUBLE PRECISION,INTENT(IN)                    :: DELTA_T,R,GAMMA
    DOUBLE PRECISION,DIMENSION(4)                  :: C1,C2,C3,C4


    C1=0.D0
    C2=0.D0
    C3=0.D0
    C4=0.D0


    C1=F(TEMPS(K),Y(:,K),R,GAMMA)
    C2=F(TEMPS(K)+DELTA_T/2.D0,Y(:,K)+DELTA_T/2.D0*C1,R,GAMMA)
    C3=F(TEMPS(K)+DELTA_T/2.D0,Y(:,K)+DELTA_T/2.D0*C2,R,GAMMA)
    C4=F(TEMPS(K)+DELTA_T,Y(:,K)+DELTA_T*C3,R,GAMMA)

    TEMPS(K+1)=TEMPS(K)+DELTA_T

    Y(:,K+1)=Y(:,K)+DELTA_T*(C1+2*C2+2*C3+C4)/6.D0

    !RENORMALISATION
    Y(:,K+1)=Y(:,K+1)/NORM_QUATER(Y(:,K+1))

  END SUBROUTINE RK4



  !=============================================================
  FUNCTION ANGULAR_VELOCITY_AB(R,GAMMA,TEMPS) RESULT(Z) 
    !============================================================

    IMPLICIT NONE
    DOUBLE PRECISION, INTENT(IN)               :: R,GAMMA,TEMPS
    DOUBLE PRECISION, DIMENSION(4)             :: Z

    Z(1) = 0.
    Z(2) = 0.
    Z(3) =0.
    Z(4) = (1.+ TAN((R*GAMMA*TEMPS )/(R**2+1.))**2 )*(GAMMA*R**2) / ((R**2 + 1.)*(1.+(R**2)*TAN((R*GAMMA*TEMPS )/(R**2+1.))**2))
    RETURN 
  END FUNCTION ANGULAR_VELOCITY_AB





  SUBROUTINE RUNGE_KUTTA(TEMPS,QUATERNION,R,GAMMA,DELTA_T)

    DOUBLE PRECISION,DIMENSION(4),INTENT(INOUT)   :: TEMPS
    DOUBLE PRECISION,DIMENSION(4)                 ::YY,C1,C2,C3,C4,YC,YP
    DOUBLE PRECISION,DIMENSION(4,4),INTENT(INOUT) :: QUATERNION
    DOUBLE PRECISION,INTENT(IN)                ::R,GAMMA,DELTA_T
    DOUBLE PRECISION  ::XX
    INTEGER                                     :: K


    DO K = 1, 2
       XX = TEMPS(K); YY=QUATERNION(:,K)

       C1=F(XX,YY,R,GAMMA)
       XX = TEMPS(K) + DELTA_T / 2.D0
       YY = QUATERNION(:,K) + DELTA_T / 2.D0 * C1

       C2=F(XX,YY,R,GAMMA)
       YY = QUATERNION(:,K) + DELTA_T / 2.D0 * C2

       C3=F(XX,YY,R,GAMMA)
       TEMPS(K + 1) = TEMPS(K) + DELTA_T
       XX = TEMPS(K + 1)
       YY = QUATERNION(:,K) + DELTA_T * C3

       C4=F(XX,YY,R,GAMMA)
       QUATERNION(:,K + 1) = QUATERNION(:,K) + DELTA_T * (C1 + 2 * C2 + 2 * C3 + C4) / 6.D0
       !-----------------------------------------------------
       !-----------RENORMAISATION ---------------------------
       QUATERNION(:,K + 1)=QUATERNION(:,K + 1)/NORM_QUATER(QUATERNION(:,K + 1))
       !---------------------------------------------------------
       XX = TEMPS(K + 1)
    END DO


  END SUBROUTINE RUNGE_KUTTA



  !**************************************************************************************************
  !**************************************************************************************************
  !**************************************************************************************************
  !**************************************************************************************************
  !*************************** PCDMM*****************************************************************
  !**************************************************************************************************
  !**************************************************************************************************
  !**************************************************************************************************
  !**************************************************************************************************

  FUNCTION F_PCDMM(TEMPS,QUATERNION,R,GAMMA)


    DOUBLE PRECISION,DIMENSION(4),INTENT(IN)    :: QUATERNION
    DOUBLE PRECISION,INTENT(IN)                 :: R,GAMMA
    DOUBLE PRECISION,DIMENSION(4)               :: F_PCDMM
    DOUBLE PRECISION,INTENT(IN)                 :: TEMPS
    DOUBLE PRECISION,DIMENSION(4)               :: OMEGA


    !    CALL  ANGULAR_VELOCITY_PCDMM(R,GAMMA,TEMPS,OMEGA,I) 
    !OMEGA=ANGULAR_VELOCITY_(R,GAMMA,TEMPS) 
    !----------------------------------------

    omega(1)=0.d0
    omega(2) = 0.d0
    omega(3) = 0.d0
    omega(4) = (1.+ tan((r*gamma*TEMPS )/(r**2+1.))**2 )*(gamma*r**2) / ((r**2 + 1.)*(1.+(r**2)*tan((r*gamma*TEMPS )/(r**2+1.))**2))


    F_PCDMM=0.5D0*MULTIPL_QUATER(OMEGA,QUATERNION)

    RETURN 

  END FUNCTION F_PCDMM



  !RUNGE-KUTTA METHOD TO CALCULATE FIRST TWO POINTS ONLY

  SUBROUTINE RK4_PCDMM(I,DELTA_T,TEMPS,Y,R,GAMMA)

    INTEGER,INTENT(IN)                             :: I
    DOUBLE PRECISION,DIMENSION(:,:),INTENT(INOUT)  :: Y
    DOUBLE PRECISION,INTENT(IN)                    :: TEMPS
    DOUBLE PRECISION,INTENT(IN)                    :: DELTA_T,R,GAMMA
    DOUBLE PRECISION,DIMENSION(4)                  :: C1,C2,C3,C4
    !    DOUBLE PRECISION,DIMENSION(:),intent(in) :: OMEGA

    C1=0.D0
    C2=0.D0
    C3=0.D0
    C4=0.D0


    !    C1=F(TEMPS,Y(:,K),R,GAMMA,I-1)
    C1= F_PCDMM(TEMPS-DELTA_T,Y(:,I-1),R,GAMMA)

    !    C2=F(TEMPS+DELTA_T/2.D0,Y(:,I-1)+DELTA_T/2.D0*C1,R,GAMMA)
    C2= F_PCDMM((TEMPS-DELTA_T)+DELTA_T/2.D0,Y(:,I-1)+DELTA_T/2.D0*C1,R,GAMMA)

    !    C3=F(TEMPS+DELTA_T/2.D0,Y(:,I-1)+DELTA_T/2.D0*C2,R,GAMMA)
    C3= F_PCDMM((TEMPS-DELTA_T)+DELTA_T/2.D0,Y(:,I-1)+DELTA_T/2.D0*C2,R,GAMMA)

    !    C4=F(TEMPS+DELTA_T,Y(:,I-1)+DELTA_T*C3,R,GAMMA)
    C4= F_PCDMM((TEMPS-DELTA_T)+DELTA_T,Y(:,I-1)+DELTA_T*C3,R,GAMMA)

    !TEMPS(K+1)=TEMPS(K)+DELTA_T

    Y(:,I)=Y(:,I-1)+DELTA_T*(C1+2*C2+2*C3+C4)/6.D0

    !RENORMALISATION
    Y(:,I)=Y(:,I)/NORM_QUATER(Y(:,I))

  END SUBROUTINE RK4_PCDMM
  !**************************************************************************************************
  !**************************************************************************************************
  !**************************************************************************************************
  !**************************************************************************************************








  !**************************************************************************************************
  !**************************************************************************************************
  !**************************************************************************************************
  !**************************************************************************************************
  !*************************** PCDMM*****************************************************************
  !**************************************************************************************************
  !**************************************************************************************************
  !**************************************************************************************************
  !**************************************************************************************************

  FUNCTION F2_PCDMM(TEMPS,QUATERNION,R,GAMMA)


    DOUBLE PRECISION,DIMENSION(4),INTENT(IN)    :: QUATERNION
    DOUBLE PRECISION,INTENT(IN)                 :: R,GAMMA
    DOUBLE PRECISION,DIMENSION(4)               :: F2_PCDMM
    DOUBLE PRECISION,INTENT(IN)                 :: TEMPS
    DOUBLE PRECISION,DIMENSION(4)               :: OMEGA


    !    CALL  ANGULAR_VELOCITY_PCDMM(R,GAMMA,TEMPS,OMEGA,I) 
    !OMEGA=ANGULAR_VELOCITY_(R,GAMMA,TEMPS) 
    !----------------------------------------

    omega(1)=0.d0
    omega(2) = 0.d0
    omega(3) = 0.d0
    omega(4) = (1.+ tan((r*gamma*TEMPS )/(r**2+1.))**2 )*(gamma*r**2) / ((r**2 + 1.)*(1.+(r**2)*tan((r*gamma*TEMPS )/(r**2+1.))**2))


    F2_PCDMM=0.5D0*MULTIPL_QUATER(OMEGA,QUATERNION)

    RETURN 

  END FUNCTION F2_PCDMM



  !RUNGE-KUTTA METHOD TO CALCULATE FIRST TWO POINTS ONLY

  SUBROUTINE RK2_PCDMM(I,DELTA_T,TEMPS,Y,R,GAMMA)

    INTEGER,INTENT(IN)                             :: I
    DOUBLE PRECISION,DIMENSION(:,:),INTENT(INOUT)  :: Y
    DOUBLE PRECISION,INTENT(IN)                    :: TEMPS
    DOUBLE PRECISION,INTENT(IN)                    :: DELTA_T,R,GAMMA
    DOUBLE PRECISION,DIMENSION(4)                  :: C1


    C1=0.D0

    C1= F2_PCDMM(TEMPS-DELTA_T,Y(:,I-1),R,GAMMA)
    Y(:,I)=Y(:,I-1)+DELTA_T*F2_PCDMM((TEMPS-DELTA_T)+DELTA_T/2.d0,Y(:,I-1)+DELTA_T/2.D0*C1,R,GAMMA)

    !RENORMALISATION

    Y(:,I)=Y(:,I)/NORM_QUATER(Y(:,I))

  END SUBROUTINE RK2_PCDMM
  !**************************************************************************************************
  !**************************************************************************************************
  !**************************************************************************************************
  !**************************************************************************************************





  !**************************************************************************************************
  !**************************************************************************************************
  !**************************************************************************************************
  !**************************************************************************************************
  !*************************** EULER*****************************************************************
  !**************************************************************************************************
  !**************************************************************************************************
  !**************************************************************************************************
  !**************************************************************************************************

  FUNCTION FE_PCDMM(TEMPS,QUATERNION,R,GAMMA)


    DOUBLE PRECISION,DIMENSION(4),INTENT(IN)    :: QUATERNION
    DOUBLE PRECISION,INTENT(IN)                 :: R,GAMMA
    DOUBLE PRECISION,DIMENSION(4)               :: FE_PCDMM
    DOUBLE PRECISION,INTENT(IN)                 :: TEMPS
    DOUBLE PRECISION,DIMENSION(4)               :: OMEGA


    !    CALL  ANGULAR_VELOCITY_PCDMM(R,GAMMA,TEMPS,OMEGA,I) 
    !OMEGA=ANGULAR_VELOCITY_(R,GAMMA,TEMPS) 
    !----------------------------------------

    omega(1)=0.d0
    omega(2) = 0.d0
    omega(3) = 0.d0
    omega(4) = (1.+ tan((r*gamma*TEMPS )/(r**2+1.))**2 )*(gamma*r**2) / ((r**2 + 1.)*(1.+(r**2)*tan((r*gamma*TEMPS )/(r**2+1.))**2))


    FE_PCDMM=0.5D0*MULTIPL_QUATER(OMEGA,QUATERNION)

    RETURN 

  END FUNCTION FE_PCDMM



  !RUNGE-KUTTA METHOD TO CALCULATE FIRST TWO POINTS ONLY

  SUBROUTINE EULER(I,DELTA_T,TEMPS,Y,R,GAMMA)

    INTEGER,INTENT(IN)                             :: I
    DOUBLE PRECISION,DIMENSION(:,:),INTENT(INOUT)  :: Y
    DOUBLE PRECISION,INTENT(IN)                    :: TEMPS
    DOUBLE PRECISION,INTENT(IN)                    :: DELTA_T,R,GAMMA

    Y(:,I)=Y(:,I-1)+DELTA_T*FE_PCDMM(TEMPS,Y(:,I-1),R,GAMMA)
    !RENORMALISATION
    Y(:,I)=Y(:,I)/NORM_QUATER(Y(:,I))

  END SUBROUTINE EULER
  !**************************************************************************************************
  !**************************************************************************************************
  !**************************************************************************************************
  !**************************************************************************************************



  !**************************************************************************************************
  !**************************************************************************************************
  !**************************************************************************************************
  !**************************************************************************************************
  !*************************** AB2*****************************************************************
  !**************************************************************************************************
  !**************************************************************************************************
  !**************************************************************************************************
  !**************************************************************************************************

  FUNCTION FAB2(TEMPS,QUATERNION,R,GAMMA)


    DOUBLE PRECISION,DIMENSION(4),INTENT(IN)    :: QUATERNION
    DOUBLE PRECISION,INTENT(IN)                 :: R,GAMMA
    DOUBLE PRECISION,DIMENSION(4)               :: FAB2
    DOUBLE PRECISION,INTENT(IN)                 :: TEMPS
    DOUBLE PRECISION,DIMENSION(4)               :: OMEGA


    !    CALL  ANGULAR_VELOCITY_PCDMM(R,GAMMA,TEMPS,OMEGA,I) 
    !OMEGA=ANGULAR_VELOCITY_(R,GAMMA,TEMPS) 
    !----------------------------------------

    omega(1)=0.d0
    omega(2) = 0.d0
    omega(3) = 0.d0
    omega(4) = (1.+ tan((r*gamma*TEMPS )/(r**2+1.))**2 )*(gamma*r**2) / ((r**2 + 1.)*(1.+(r**2)*tan((r*gamma*TEMPS )/(r**2+1.))**2))


    FAB2=0.5D0*MULTIPL_QUATER(OMEGA,QUATERNION)

    RETURN 

  END FUNCTION FAB2



  !RUNGE-KUTTA METHOD TO CALCULATE FIRST TWO POINTS ONLY

  SUBROUTINE AB2(I,DELTA_T,TEMPS,Y,R,GAMMA)

    INTEGER,INTENT(IN)                             :: I
    DOUBLE PRECISION,DIMENSION(:,:),INTENT(INOUT)  :: Y
    DOUBLE PRECISION,INTENT(IN)                    :: TEMPS
    DOUBLE PRECISION,INTENT(IN)                    :: DELTA_T,R,GAMMA

    Y(:,I)=Y(:,I-1)+DELTA_T*((3./2.)*FAB2(TEMPS-DELTA_T,Y(:,I-1),R,GAMMA)-(1./2.)*FAB2(TEMPS-2.*DELTA_T,Y(:,I-2),R,GAMMA))
    !RENORMALISATION
    Y(:,I)=Y(:,I)/NORM_QUATER(Y(:,I))

  END SUBROUTINE AB2
  !**************************************************************************************************
  !**************************************************************************************************
  !**************************************************************************************************
  !**************************************************************************************************

  





  !ccccccccccccccccccccccccccccccccccccccccccccccccc
  subroutine Compute_Error(Uap,Uex,err,NBITER)
    !cccccccccccccccccccccccccccccccccccccccccccccccccc

    IMPLICIT NONE

    INTEGER,INTENT(IN)                           ::NBITER
    DOUBLE PRECISION , DIMENSION(:), INTENT(IN)  :: UAP,UEX
    DOUBLE PRECISION, INTENT(OUT)                :: ERR     
    INTEGER                                      :: I
    DOUBLE PRECISION                             :: SUM,SOLNUM,SOLEXACT,VALNORM


    print*, '-------------   inside   --------------------'
    print*, Uap(nbiter),Uex(NBITER)
    SUM=0.D0
    VALNORM=0.D0
    solnum=0.d0
    solexact=0.d0

    DO i=1,NBITER
       
       solnum=(Uap(i))
       solexact=(Uex(i))

       sum=sum+(solexact-solnum)**2
       valnorm=valnorm+solexact**2

    END DO
    print*, '-------------   outside   --------------------'
    print*, Uap(nbiter),Uex(NBITER)

    err=sqrt(sum)/sqrt(valnorm)

    
  end subroutine Compute_Error


    !ccccccccccccccccccccccccccccccccccccccccccccccccc
  subroutine Compute_Error_test(Uap,Uex,err,NBITER)
    !cccccccccccccccccccccccccccccccccccccccccccccccccc

    IMPLICIT NONE

    INTEGER,INTENT(IN)                           ::NBITER
    DOUBLE PRECISION , DIMENSION(:), INTENT(IN)  :: UAP,UEX
    DOUBLE PRECISION, INTENT(OUT)                :: ERR     
    INTEGER                                      :: I
    DOUBLE PRECISION                             :: SUM,SOLNUM,SOLEXACT,VALNORM



    SUM=0.D0
    VALNORM=0.D0
    solnum=0.d0
    solexact=0.d0

    solnum=(Uap(NBITER))
    solexact=(Uex(NBITER))
    
    err=abs(solexact-solnum)/abs(solexact)       
    print*, '-------------   outside   --------------------'
    print*, Uap(nbiter),Uex(NBITER)    

    
    
  end subroutine Compute_Error_test






END MODULE LIB_INTEGRATION_METHOD
