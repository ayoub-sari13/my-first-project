!=======================================================
! Numerical schemes for quaternion time-integration
! Using Direct Multiplication Method
!------------------------------------------------------
! author: Chiheb Sakka
! 01/06/2015 
! IMFT
!=======================================================

PROGRAM direct_multiplication_method

  use lib_integration_method

  IMPLICIT none
  character(len=30)                            :: filename
  DOUBLE PRECISION,DIMENSION(:,:),ALLOCATABLE  :: Quaternion,OMEGA
  DOUBLE PRECISION                             :: FINAL_TIME, omega_max,r,gamma,delta_T,norme,TEMPS,phi,angle,error
  INTEGER                                      :: i,L,j,NBITER,NB_PERIOD,nbiter_period
  !  double precision,dimension(3)                :: omeg
  DOUBLE PRECISION,DIMENSION(:),ALLOCATABLE    ::  SOL_APPROX, SOL_EXACT


  OPEN(UNIT=300,FILE='dmm-error.txt',STATUS='UNKNOWN')
  !---------------------------------------------------------------
  !  Jeffrey Test init value
  !===============================================================
  CALL GETARG(1,FILENAME)
  OPEN(UNIT=10,FILE=FILENAME, ACTION="READ")
  READ(10,*)  R
  READ(10,*)  GAMMA
  READ(10,*)  NBITER_PERIOD
  READ(10,*)  NB_PERIOD
  CLOSE(10)
  !---------------------------------------------------------------
  NBITER = NBITER_PERIOD * NB_PERIOD
  !-----------------------------------------------------------------
  ! INITIALISER QUATERNION

  ALLOCATE(QUATERNION(4,NBITER))
  ALLOCATE(OMEGA(4,NBITER))
  ALLOCATE(SOL_APPROX(NBITER))
  ALLOCATE(SOL_EXACT(NBITER))

  SOL_APPROX=0.d0
  SOL_EXACT=0.d0



  QUATERNION = 0.D0
  QUATERNION(:,1)=(/1.D0 , 0.D0 , 0.D0 , 0.D0/)
  !----------------------------------------------------------------
  !-----------------------------------------------------------------
  !--- PERIOD------------------------------------------------------
  FINAL_TIME=(2.* PI /GAMMA)*(R  + 1. /R)
  !-----------------------------------------------------------------
  OMEGA=0.D0
  !---------------------------------------------
  DELTA_T=FINAL_TIME/REAL(NBITER_PERIOD)

  !---------------------------------------------------
  !------------------------------------------------------
  !------ Angular velocity
  !------------------------------------------------------
  DO I=1,NBITER
     TEMPS=DELTA_T*(real(I))
     CALL angular_velocity(r,gamma,TEMPS,delta_t,omega(:,I)) 
  END DO

  !------------------------------------------------------
  !------ EULER
  !------------------------------------------------------
  ! DO I=2,3
  !    TEMPS=DELTA_T*(REAL(I)-1.)
  !    CALL euler(I,DELTA_T,TEMPS,QUATERNION,R,GAMMA)
  ! END do


  !---------------------------------------------------
  !OPEN(UNIT=100,FILE='FILE_ANGLE_DMM.DAT',STATUS='UNKNOWN')
  !---------------------------------------------------

  L=1
  print*, 'nbiter -->', nbiter, 'NBPERIOD -->', NB_PERIOD
  DO I=2,NBITER

     TEMPS=DELTA_T*(REAL(I)-1.5)
     PHI=0.d0
     ANGLE=0.D0

     !------------------------------------------
     CALL DIRECTMULTIPLICATIONMETHOD(QUATERNION,OMEGA,delta_t,I)
     !print*,Quaternion(:,I)
     !------------------------------------------
     !IF (I .GE. (NB_PERIOD-1)*(NBITER_PERIOD)) THEN
     !---------------------------------------------------------------------
     !---- ECRITURE DANS UN FICHIER DE LA SOLUTION EXACTE -----------------
     !WRITE(200,*) TEMPS, SOLUTION_EXACTE (TEMPS,GAMMA, R)
     !---------------------------------------------------------------------
     PHI = 2.D0*ACOS(QUATERNION(1,I))
     ANGLE=PHI
      ! print*,'phi of solution=',PHI
     IF ((ANGLE .GE. 0.D0).AND. (ANGLE .LE. PI/2.D0) ) THEN  
        IF ( MOD( CEILING(REAL(I)/REAL(NBITER_PERIOD)),2) .EQ. 0 ) THEN
           !   WRITE(100,*) TEMPS, -ANGLE
           SOL_APPROX(L)=-ANGLE
           SOL_EXACT(L)=SOLUTION_EXACTE (TEMPS,GAMMA,R)
        ELSE
           !    WRITE(100,*) TEMPS, ANGLE
           SOL_APPROX(L)=ANGLE
           SOL_EXACT(L)=SOLUTION_EXACTE (TEMPS,GAMMA,R)

        END IF
     ELSE
        IF  ((ANGLE .GE. PI/2.D0).AND.(ANGLE .LE. 3*PI/2.D0) ) THEN  
           IF ( MOD( CEILING(REAL(I)/REAL(NBITER_PERIOD)),2) .EQ. 0 ) THEN
              !        WRITE(100,*) TEMPS, -ANGLE + PI
              SOL_APPROX(L)=-ANGLE+PI
              SOL_EXACT(L)=SOLUTION_EXACTE (TEMPS,GAMMA,R)

           ELSE
              !         WRITE(100,*) TEMPS, ANGLE - PI
              SOL_APPROX(L)=ANGLE-PI
              SOL_EXACT(L)=SOLUTION_EXACTE (TEMPS,GAMMA,R)
  
           END IF
        ELSE
           IF  ((ANGLE .GE. 3*PI/2.D0).AND.(ANGLE .LE. 2*PI) ) THEN  
              IF ( MOD( CEILING(REAL(I)/REAL(NBITER_PERIOD)),2) .EQ. 0 ) THEN
                 !             WRITE(100,*) TEMPS, -ANGLE + 2.D0*PI
                 SOL_APPROX(L)=-ANGLE+2.D0*PI
                 SOL_EXACT(L)=SOLUTION_EXACTE (TEMPS,GAMMA,R)
              ELSE
                 !              WRITE(100,*) TEMPS, ANGLE - 2.D0*PI
                 SOL_APPROX(L)=ANGLE-2.D0*PI
                 SOL_EXACT(L)=SOLUTION_EXACTE (TEMPS,GAMMA,R)
              END IF
           END IF
        END IF
     END IF
     print*, '======================= before ======================================='
     print*, 'approx =',sol_approx(L),'exact=', sol_exact(L),char(10)
     CALL COMPUTE_ERROR(SOL_APPROX,SOL_EXACT,ERROR,L-1)
     WRITE(300,*) I,error*100.
     print*,'the error iiiiiiiiiiiiiiis:',I,error

     L=L+1
     print*,'L=',L

     !END IF

  END DO

  !CLOSE(100)

  !  CLOSE(200)
  !PRINT*,L,NBITER


  ! do i=1,nbiter_period-3
  !    print*,SOL_APPROX(i),SOL_EXACT(i)
  ! end do



  ! print*,'dmm q(2)',Quaternion(:,2)
  ! print*,'dmm q(3)',Quaternion(:,3)

  ! CALL COMPUTE_ERROR(SOL_APPROX,SOL_EXACT,ERROR,NBITER_period-2)
  ! PRINT*,'**************************'
  ! PRINT*,'=========================='
  ! PRINT*,'PERIOD',FINAL_TIME
  ! PRINT*,'=========================='
  ! PRINT*,'DELTA_T',FINAL_TIME/NBITER_PERIOD
  ! PRINT*,'=========================='
  ! PRINT*,'=========================='
  ! PRINT*,'ERROR DMM',ERROR
  ! PRINT*,'=========================='
  

  DEALLOCATE(QUATERNION)
  DEALLOCATE(OMEGA)
  DEALLOCATE(SOL_APPROX)
  DEALLOCATE(SOL_EXACT)

  close(300)


END PROGRAM direct_multiplication_method
