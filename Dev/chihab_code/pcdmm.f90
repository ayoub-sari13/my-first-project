!==================================================================
! The predictor-corrector direct multiplication (PCDM) method,
! approximates the angular velocity with a basic Lie–Euler method,
! or called predictor-corrector method
!-------------------------------------------------------------------
! author: Chiheb Sakka
! 01/08/2015 
! IMFT
!===================================================================


PROGRAM PREDICTOR_CORRECTOR_DIRECT_MULTIPLICATION_METHOD

  USE LIB_INTEGRATION_METHOD

  IMPLICIT NONE
  CHARACTER(LEN=30)                            :: FILENAME
  DOUBLE PRECISION,DIMENSION(:,:),ALLOCATABLE  :: QUATERNION,OMEGA
  DOUBLE PRECISION                             :: FINAL_TIME,R,GAMMA,DELTA_T,NORME,TEMPS,PHI,ANGLE,ERROR,tiny
  INTEGER                                      :: I,J,L,NBITER,NB_PERIOD,NBITER_PERIOD,K
  DOUBLE PRECISION,DIMENSION(:),ALLOCATABLE    ::  SOL_APPROX, SOL_EXACT
  double precision ::PHI2
  
  OPEN(UNIT=300,FILE='test1.txt',STATUS='UNKNOWN')
  
    do K=1,4

  !---------------------------------------------------------------
  !  JEFFREY TEST INIT VALUE
  !===============================================================
  !---------------------------------------------------------------
  CALL getarg(1,filename)
  OPEN(UNIT=10,FILE=filename, ACTION="read")
  READ(10,*) R
  READ(10,*) GAMMA
  !READ(10,*) NBITER_PERIOD
  READ(10,*) NB_PERIOD
  CLOSE(10)
  !----------------------------------------------------------------
  
  !NB_PERIOD=1+k*10
  !NB_period=1
  
  NBITER_PERIOD = k*50
  
  ! Tiny=0.0001
  !Tiny=0.04
  
  !NBITER_period=1
 
  ! print*,'--------------'
  ! print*,'Tiny',Tiny
  ! print*,'--------------'
  
  
  ! print*,'--------------'
  ! print*,'period',NB_PERIOD
  ! print*,'--------------'
  !----------------------------------------------------------------
  ! INITIALISER QUATERNION
  
  !call ptest(NBITER_PERIOD)
  
  !print*,NBITER_PERIOD
  

  NBITER=NBITER_PERIOD*NB_PERIOD

    
  ALLOCATE(QUATERNION(4,NBITER))
  ALLOCATE(OMEGA(4,NBITER))
  ALLOCATE(SOL_APPROX(NBITER))
  ALLOCATE(SOL_EXACT(NBITER))

  SOL_APPROX=0.D0
  SOL_EXACT=0.D0

  QUATERNION = 0.D0
  QUATERNION(:,1)=(/1.D0 , 0.D0 , 0.D0 , 0.D0/)
  !----------------------------------------------------------------
  FINAL_TIME=(2.d0* PI /GAMMA)*(R + 1.D0/R)
  
  !--------------------------------
  !--- VITESSE A T0 ----------------
  OMEGA=0.D0
  TEMPS=0.D0
  
  !CALL ANGULAR_VELOCITY_PCDMM(R,GAMMA,TEMPS,OMEGA,1) 
  !===============
  !---------------------------------------------
  DELTA_T=REAL(FINAL_TIME)/REAL(NBITER_PERIOD)
  !print*,delta_t
  
  !DELTA_T=FINAL_TIME/REAL(NBITER_PERIOD+1)
  
  !print*,delta_t
  
  !---------------------------------------------------
    OPEN(UNIT=100,FILE='FILE_ANGLE_PCDMM.DAT',STATUS='UNKNOWN')
  !---------------------------------------------------
  
  !------------------------------------------------------
  !------ Angular velocity
  !------------------------------------------------------
  DO I=1,NBITER
     TEMPS=DELTA_T*(real(I)-1.)
     CALL angular_velocity_pcdmm(r,gamma,TEMPS,OMEGA,I) 
     !print*,I, TEMPS , OMEGA(:,I)
  END DO
  !------------------------------------------------------
  !------ EULER
  !------------------------------------------------------
  ! DO I=2,3
  !    TEMPS=DELTA_T*(REAL(I))
  !    CALL EULER(I,DELTA_T,TEMPS-delta_T,QUATERNION,R,GAMMA)
  ! END do
  !------------------------------------------------------
  !------ RUNGE-KUTTA
  !------------------------------------------------------
  ! DO I=2,3
  !    TEMPS=DELTA_T*(REAL(I)-1.)
  !    CALL RK2_PCDMM(I,DELTA_T,TEMPS,QUATERNION,R,GAMMA)
  ! END do
  
  !------------------------------------------------------
  !------ AB2
  !------------------------------------------------------
  ! QUATERNION(:,2)=(/1.D0 , 0.D0 , 0.D0 , 0.D0/)

  ! DO I=2,2
  !    TEMPS=DELTA_T*(REAL(I)-1.)
  !    CALL AB2(I,DELTA_T,TEMPS,QUATERNION,R,GAMMA)
  ! END do


  !------------------------------------------------------
  !------ PCDMM
  !------------------------------------------------------
  SOL_APPROX(1)=2.D0*ACOS(QUATERNION(1,1))
  SOL_EXACT(1)=SOLUTION_EXACTE (0.d0,GAMMA,R)
 
  L=2
  
  !QUATERNION(:,2)=(/1.D0 , 0.D0 , 0.D0 , 0.D0/)
  !QUATERNION(:,3)=(/1.D0 , 0.D0 , 0.D0 , 0.D0/)

  DO I=2,NBITER
     
     if (I .LE. 1100 ) then 
        Tiny= NB_PERIOD * 1.0000e-04
     
        else 
           Tiny= 0.00
        
     end if
     
     
     
   
     TEMPS=0.d0
     TEMPS=DELTA_T*(real(I)-1.)  !50
     
     PHI=0.d0
     ANGLE=0.d0
          
     CALL PCDMM(QUATERNION,OMEGA,DELTA_T,I)
     !*************************************************************
!     IF (I .GT. (NB_PERIOD-1)*(NBITER_PERIOD)) THEN
     !---------------------------------------------------------------------
        !---------------------------------------------------------------------
     PHI = 2.D0*ACOS(QUATERNION(1,I))
     PHI2= 0.d0
     ANGLE=PHI
     
        !print*,I,TEMPS/delta_t,MOD( CEILING(REAL(I)/REAL(NBITER_PERIOD)),2)
        !print*,I,ANGLE,FINAL_TIME/4.D0
        !print*,I,MOD( CEILING(REAL(I)/REAL(NBITER_PERIOD)),2)
        
     IF ((ANGLE .GE. (0.D0-tiny)).AND. (ANGLE .LE. ((PI/2.D0)-tiny)) ) THEN  
           IF ( MOD( CEILING(REAL(I)/REAL(NBITER_PERIOD)),2) .EQ. 0 ) THEN
              !print*,MOD( CEILING(REAL(I)/REAL(NBITER_PERIOD)),2)
              WRITE(100,*) TEMPS, -ANGLE
              SOL_APPROX(L)=-ANGLE
              SOL_EXACT(L)=SOLUTION_EXACTE (TEMPS,GAMMA,R)
           ELSE
              WRITE(100,*) TEMPS, ANGLE
              SOL_APPROX(L)=ANGLE
              SOL_EXACT(L)=SOLUTION_EXACTE (TEMPS,GAMMA,R)
              
           END IF
        ELSE
           IF  ((ANGLE .GE. ((PI/2.D0)-tiny)).AND.(ANGLE .LE. ((3*PI/2.D0)-tiny) ) ) THEN  
              IF ( MOD( CEILING(REAL(I)/REAL(NBITER_PERIOD)),2) .EQ. 0 ) THEN
                 WRITE(100,*) TEMPS, -ANGLE + PI
                 SOL_APPROX(L)=-ANGLE + PI
                 SOL_EXACT(L)=SOLUTION_EXACTE (TEMPS,GAMMA,R)
              ELSE
                 WRITE(100,*) TEMPS, ANGLE - PI
                 SOL_APPROX(L)=ANGLE - PI
                 SOL_EXACT(L)=SOLUTION_EXACTE (TEMPS,GAMMA,R)
              END IF
           ELSE
              IF  ((ANGLE .GE. ((3*PI/2.D0)-tiny)).AND.(ANGLE .LE. ((2*PI) - tiny)) ) THEN  
                 IF ( MOD( CEILING(REAL(I)/REAL(NBITER_PERIOD)),2) .EQ. 0 ) THEN
                    WRITE(100,*) TEMPS, -ANGLE + 2.D0*PI
                    SOL_APPROX(L)=-ANGLE+ 2.D0*PI
                    SOL_EXACT(L)=SOLUTION_EXACTE (TEMPS,GAMMA,R)
                 ELSE
                    WRITE(100,*) TEMPS, ANGLE - 2.D0*PI
                    SOL_APPROX(L)=ANGLE- 2.D0*PI
                    SOL_EXACT(L)=SOLUTION_EXACTE (TEMPS,GAMMA,R)
                 END IF
              END IF
           END IF
        END IF
        !---------
        L=L+1
        !---------
     !END IF
        
     CALL COMPUTE_ERROR(SOL_APPROX,SOL_EXACT,ERROR,I)
     WRITE(300,*) I,error*100.
     
     END DO
  
     CLOSE(100)
  ! CLOSE(200)
  
  
     DO I=1,nbiter
        print*,PI/2.,I,SOL_APPROX(i),SOL_EXACT(i)
     end do
  
  
  
  ! do I=1,NBITER
  !    print*, Quaternion(:,I)
  ! end do



  ! PRINT*,'**************************'
  ! PRINT*,'=========================='
  !PRINT*,'PERIOD',FINAL_TIME
  ! PRINT*,'=========================='
  ! PRINT*,'DELTA_T',FINAL_TIME/NBITER_PERIOD
  ! PRINT*,'=========================='
  ! PRINT*,'=========================='
  ! PRINT*,'ERROR PCDMM',NBITER_PERIOD,ERROR,2*K*100-1, real(2*K*100-1)/real(NBITER_period),&
  !      &ceiling(real(2*K*100-1)/real(NBITER_period)),MOD( CEILING(REAL(2*K*100-1)/REAL(NBITER_PERIOD)),2)
  
  ! PRINT*,'==================================================================='
  ! PRINT*,'ERROR PCDMM',error
  ! PRINT*,'==================================================================='
  
 ! WRITE(300,*) NBITER_PERIOD,NB_PERIOD,NBITER,error*100.
  
  
  ! if (1.5707963493587425 .LE. 1.5707963705062866 ) then 
  !    print*,'yes'
  ! else
  !    print*,'NO'
  ! end if
  
  ! PRINT*,'=========================='
     
  !   DO I=1,NBITER
  !    print*,OMEGA(:,I)
  ! END DO

  ! PRINT*,'PI',PI
  ! do i=1,NBITER
  !   PRINT*,'ERROR PCDMM',Quaternion(:,i)
  !   PRINT*,'ERROR PCDMM',Quaternion(:,0)
  ! end do
  
 
     
     DEALLOCATE(QUATERNION)
     DEALLOCATE(OMEGA)
     DEALLOCATE(SOL_APPROX)
     DEALLOCATE(SOL_EXACT)
  
  !end do
     
     close(300)

   CONTAINS



  !----------------------------------------------------------------------
  ! PREDICTOR CORRECTOR DIRECT MULTIPLICATION METHOD USING QUATERNIONS
  !----------------------------------------------------------------------
  !----------------------------------------------------------------------
  !                BEGIN SUBROUTINE PCDDM
  !------------------------------------------------------------------------

     SUBROUTINE PCDMM(QUATERNION,OMEGA,DELTA_T,I)

    INTEGER,INTENT(IN)                                    :: I
    DOUBLE PRECISION,INTENT(IN)                           :: DELTA_T
    !   DOUBLE PRECISION,DIMENSION(4),INTENT(INOUT)           :: OMEGA
    !   DOUBLE PRECISION,DIMENSION(4),INTENT(INOUT)           :: OMEGA_B_OLD
    DOUBLE PRECISION,DIMENSION(4)                         :: OMEGA_B,Q_INV,OMEGA_DOT_B
    DOUBLE PRECISION,DIMENSION(4)                         :: OMEGA_QUARTER_B,OMEGA_HALF_B
    DOUBLE PRECISION,DIMENSION(4)                         :: OMEGA_QUARTER,OMEGA_HALF
    DOUBLE PRECISION,DIMENSION(4)                         :: Q_HALF_P,OMEGA_B_HALF_DOT
    DOUBLE PRECISION,DIMENSION(4)                         :: VECTEUR,VECTEUR2
    DOUBLE PRECISION                                      :: NORME 
    DOUBLE PRECISION,DIMENSION(:,:),INTENT(INOUT)    :: QUATERNION
    DOUBLE PRECISION,DIMENSION(:,:),INTENT(IN)            :: OMEGA
    DOUBLE PRECISION,DIMENSION(4)                         :: QUATERNION_TILD
    DOUBLE PRECISION,DIMENSION(4)                         :: Q_HALF_P_INV
    DOUBLE PRECISION,DIMENSION(4)                         :: TAMP1

    !print*,Quaternion(:,I-1)
    !----------------------------------------------------------------------------------------
    !---OMEGA_B----
    NORME=0.D0
    VECTEUR=0.D0
    VECTEUR=QUATERNION(:,I-1)
    NORME = SQRT(SUM( vecteur**2 ))
    !----------------------------------------------------------------------------------------
    Q_INV=0.D0
    !----------------------------------------------------------------------------------------
    Q_INV(1)=QUATERNION(1,I-1)
    Q_INV(2)=-QUATERNION(2,I-1)
    Q_INV(3)=-QUATERNION(3,I-1)
    Q_INV(4)=-QUATERNION(4,I-1)
    !----------------------------------------------------------------------------------------
    Q_INV=Q_INV/NORME
    !----------------------------------------------------------------------------------------
    !°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
    !OMEGA=0.D0
    !OMEGA=ANGULAR_VELOCITY(R,GAMMA,TEMPS)
    !°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
    !print*,Q_inv
    TAMP1=0.D0
    !   print*,'omega' ,OMEGA(:,I-1)
    VECTEUR=0.D0
    VECTEUR=OMEGA(:,I-1)
    TAMP1=MULTIPL_QUATER(Q_INV,VECTEUR)
    OMEGA_B= MULTIPL_QUATER(TAMP1,QUATERNION(:,I-1))
    !print*,OMEGA_B
    !----------------------------------------------------------------------------------------
    !----------------------------------------------------------------------------------------
    !---OMEGA_DOT_B----

    OMEGA_DOT_B=0.d0
    
    !OMEGA_DOT_B= ( 3*OMEGA(:,I)-4*OMEGA(:,I-1)+OMEGA(:,I-2))/2.*DELTA_T
    !OMEGA_DOT_B= ( 3*OMEGA(:,I)-2*OMEGA(:,I-1)+OMEGA(:,I-2))/2.*DELTA_T
    
    !OMEGA_DOT_B= (OMEGA(:,I)-OMEGA(:,I-2))/2.d0*delta_t
    
    OMEGA_DOT_B= (OMEGA(:,I)-OMEGA(:,I-1))/DELTA_T
    
    !OMEGA_DOT_B =  OMEGA_DOT_B /norm_quater(OMEGA_DOT_B)
    !print*,OMEGA_DOT_B
    
    !----------------------------------------------------------------------------------------
    !---OMEGA_QUARTER_B----
    OMEGA_QUARTER_B=OMEGA_B+OMEGA_DOT_B*DELTA_T/4.d0
    !---OMEGA_HALF_B----
    OMEGA_HALF_B=OMEGA_B+OMEGA_DOT_B*DELTA_T/2.d0
    !----------------------------------------------------------------------------------------
    !----------------------------------------------------------------------------------------
    !---OMEGA_QUARTER----------------------
    !----------------------------------------------------------------------------------------
    TAMP1=0.D0
    TAMP1=MULTIPL_QUATER(QUATERNION(:,I-1),OMEGA_QUARTER_B)
    OMEGA_QUARTER= MULTIPL_QUATER(TAMP1,Q_INV)
    !----------------------------------------------------------------------------------------
    !----------------------------------------------------------------------------------------
    !---Q_HALF_P--------------------------
    !----------------------------------------------------------------------------------------
    NORME=0.D0
    NORME= SQRT(SUM((OMEGA_QUARTER(:))**2))
    !----------------------------------------------------------------------------------------
    QUATERNION_TILD=0.D0
    Q_HALF_P=0.d0
    !----------------------------------------------------------------------------------------
    QUATERNION_TILD(1)=COS((NORME*DELTA_T)/4.D0)
    QUATERNION_TILD(2)=SIN((NORME*DELTA_T)/4.D0)*(OMEGA_QUARTER(2)/NORME)
    QUATERNION_TILD(3)=SIN((NORME*DELTA_T)/4.D0)*(OMEGA_QUARTER(3)/NORME)
    QUATERNION_TILD(4)=SIN((NORME*DELTA_T)/4.D0)*(OMEGA_QUARTER(4)/NORME)
    !--------------------------------------------------------------------
    Q_HALF_P=MULTIPL_QUATER(QUATERNION_TILD,QUATERNION(:,I-1))
    !----------------------------------------------------------------------------------------
    !---OMEGA_HALF----------------------
    !----------------------------------------------------------------------------------------
    Q_HALF_P_INV=0.d0
    OMEGA_HALF=0.d0
    NORME=0.D0
    NORME = SQRT(SUM((Q_HALF_P(:))**2))
    !----------------------------------------------------------------------------------------
    Q_HALF_P_INV(1)=Q_HALF_P(1)
    Q_HALF_P_INV(2)=-Q_HALF_P(2)
    Q_HALF_P_INV(3)=-Q_HALF_P(3)
    Q_HALF_P_INV(4)=-Q_HALF_P(4)
    !----------------------------------------------------------------------------------------
    Q_HALF_P_INV=Q_HALF_P_INV/NORME
    !----------------------------------------------------------------------------------------
    TAMP1=0.D0
    TAMP1=MULTIPL_QUATER(Q_HALF_P,OMEGA_HALF_B)
    OMEGA_HALF= MULTIPL_QUATER(TAMP1,Q_HALF_P_INV)
    !----------------------------------------------------------------------------------------
    !----------------------------------------------------------------------------------------
    !---NEW TIME LEVEL-------
    !----------------------------------------------------------------------------------------
    NORME=0.D0
    NORME= SQRT( SUM( (OMEGA_HALF(:))**2 ) )
    !----------------------------------------------------------------------------------------
    QUATERNION_TILD=0.D0
    !----------------------------------------------------------------------------------------
    QUATERNION_TILD(1)=COS((NORME*DELTA_T)/2.D0)
    QUATERNION_TILD(2)=SIN((NORME*DELTA_T)/2.D0)*(OMEGA_HALF(2)/NORME)
    QUATERNION_TILD(3)=SIN((NORME*DELTA_T)/2.D0)*(OMEGA_HALF(3)/NORME)
    QUATERNION_TILD(4)=SIN((NORME*DELTA_T)/2.D0)*(OMEGA_HALF(4)/NORME)
    !----------------------------------------------------------------------------------------
    !----Q(N+1)= Q_TILD *Q(N)----------------------------------------------------------------
    QUATERNION(:,I)=MULTIPL_QUATER(QUATERNION_TILD,QUATERNION(:,I-1))
    !----------------------------------------------------------------------------------------


  END SUBROUTINE PCDMM

  
  subroutine ptest(NBITER)

    implicit none

    integer,intent(inout) :: NBITER

    if (mod(NBITER,2) .EQ. 0) then
       NBITER=NBITER+1 
    end if
  end subroutine ptest
  
  subroutine COMPUTE_ERROR(SOL_APPROX,SOL_EXACT,ERROR,I)
     implicit none
    real(8),dimension(nbiter),intent(in) :: SOL_APPROX, SOL_EXACT
    real(8), intent(out) :: error
    integer, intent(in)             :: I
     
    error = abs(SOL_APPROX(I)- SOL_EXACT(I))
  
   end subroutine COMPUTE_ERROR 
  

END PROGRAM PREDICTOR_CORRECTOR_DIRECT_MULTIPLICATION_METHOD
