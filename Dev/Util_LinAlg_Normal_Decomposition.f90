subroutine MatrixMultiplication(A, B, C, m, n, q)
    real(8), dimension(m, n), intent(in) :: A
    real(8), dimension(n, q), intent(in) :: B
    real(8), dimension(m, q), intent(out) :: C
    integer :: i, j, k
    
    do i = 1, m
        do j = 1, q
            C(i, j) = 0.0
            do k = 1, n
                C(i, j) = C(i, j) + A(i, k) * B(k, j)
            end do
        end do
    end do
end subroutine MatrixMultiplication

subroutine TransposeMatrix(A, B, m, n)
    real(8), dimension(m, n), intent(in) :: A
    real(8), dimension(n, m), intent(out) :: B
    integer :: i, j
    
    do i = 1, m
        do j = 1, n
            B(j, i) = A(i, j)
        end do
    end do
end subroutine TransposeMatrix

program TransposeMatrixExample
    integer, parameter :: m = 3, n = 4
    real(8) :: A(m, n), B(n, m)
    integer :: i, j
    
    ! Initialize matrix A with some values
    A = reshape([(i + j) * 0.1 | i = 1, j = 1, m, n], [m, n])
    
    ! Transpose matrix A
    call TransposeMatrix(A, B, m, n)
    
    ! Display the transposed matrix B
    do i = 1, n
        write(*, '(4F8.2)') (B(i, j), j = 1, m)
    end do
end program TransposeMatrixExample

program MatrixMultiplicationExample
    integer, parameter :: m = 3, n = 4, p = 4, q = 2
    real(8) :: A(m, n), B(n, q), C(m, q)
    integer :: i, j
    
    ! Initialize matrices A and B with appropriate values
    A = reshape([(i + j) * 0.1, (i + j) * 0.1 | i = 1, j = 1, m, n], [m, n])
    B = reshape([(i - j) * 0.1, (i - j) * 0.1 | i = 1, j = 1, n, q], [n, q])
    
    ! Perform matrix multiplication
    call MatrixMultiplication(A, B, C, m, n, q)
    
    ! Display the result matrix C
    do i = 1, m
        write(*, '(3F8.2)') (C(i, j), j = 1, q)
    end do
end program MatrixMultiplicationExample
