!###############################################################################
!
!                             modules de front tracking
!
!
!###############################################################################
!===============================================================================
module mod_math
  use mod_Constants
  use mod_mpi

contains
  !---------------------------------------------------------------------------------
  !*****************************************************************************************************
  subroutine smoothing(mesh,col,ismooth)
    use mod_allocate
    use mod_borders
    use mod_struct_grid
    !*****************************************************************************************************
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    integer                                               :: ismooth
    real(8), dimension(:,:,:), allocatable, intent(inout) :: col
    type(grid_t), intent(in)                              :: mesh
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                               :: n1,i,j,k
    real(8)                                               :: vol
    real(8), dimension(:,:,:), allocatable                :: co1
    !-------------------------------------------------------------------------------
    
    call allocate_array_3(ALLOCATE_ON_P_MESH,co1,mesh)
    
    !-------------------------------------------------------------------------------
    if (dim==2) then
       do n1 = 1,ismooth
          co1 = col
          col = 0
          do j = mesh%sy,mesh%ey
             do i = mesh%sx,mesh%ex
                !vol=((dx(i)+dxu2(i)+dxu2(i+1))*(dy(j)+dyv2(j)+dyv2(j+1)))
                !col(i,j,1) = ( co1(i  ,j  ,1)*dx(i)*dy(j)*2   &
                !     &        +co1(i-1,j  ,1)*dxu2(i  )*dy(j) &
                !     &        +co1(i+1,j  ,1)*dxu2(i+1)*dy(j) &
                !     &        +co1(i  ,j-1,1)*dx(i)*dyv2(j  ) &
                !     &        +co1(i  ,j+1,1)*dx(i)*dyv2(j+1) )/vol
                col(i,j,1) = co1(i,j,1)/2 + (co1(i-1,j,1)+co1(i+1,j,1)+co1(i,j-1,1)+co1(i,j+1,1))/8
             end do
          end do
          call sca_borders_and_periodicity(mesh,col)
          call comm_mpi_sca(col)
       end do
    else
       do n1 = 1,ismooth
          co1 = col
          col = 0
          do k = mesh%sz,mesh%ez
             do j = mesh%sy,mesh%ey
                do i = mesh%sx,mesh%ex
                   !vol=((dx(i)+dxu2(i)+dxu2(i+1))*(dy(j)+dyv2(j)+dyv2(j+1))*(dz(k)+dzw2(k)+dzw2(k+1)))
                   !col(i,j,k) = ( co1(i  ,j  ,k  )*dx(i)*dy(j)*dz(k)     &
                   !     &        +co1(i-1,j  ,k  )*dxu2(i  )*dy(j)*dz(k) &
                   !     &        +co1(i+1,j  ,k  )*dxu2(i+1)*dy(j)*dz(k) &
                   !     &        +co1(i  ,j-1,k  )*dx(i)*dyv2(j  )*dz(k) &
                   !     &        +co1(i  ,j+1,k  )*dx(i)*dyv2(j+1)*dz(k) &
                   !     &        +co1(i  ,j  ,k-1)*dx(i)*dy(j)*dzw2(k  ) &
                   !     &        +co1(i  ,j  ,k+1)*dx(i)*dy(j)*dzw2(k+1) )/vol
                   col(i,j,k) = co1(i  ,j  ,k  )/2 +                  &
                        &     ( co1(i-1,j  ,k  )+co1(i+1,j  ,k  ) +   &
                        &       co1(i  ,j-1,k  )+co1(i  ,j+1,k  ) +   &
                        &       co1(i  ,j  ,k-1)+co1(i  ,j  ,k+1) )/12
                end do
             end do
          end do
          call sca_borders_and_periodicity(mesh,col)
          call comm_mpi_sca(col)
       end do
    endif
    
  end subroutine smoothing
  !******************************************************************************** 
  !******************************************************************************** 
  function prod_vec(U,V)
    !------------------------------------------------------------------------------
    implicit none
    !------------------------------------------------------------------------------
    real(8)             :: prod_vec
    real(8), dimension(dim),intent(in) :: U,V
    !------------------------------------------------------------------------------
    prod_vec = U(1)*V(2)-U(2)*V(1)
  end function prod_vec
  !******************************************************************************** 
  function intersection(A1,A2,B1,B2)
    !------------------------------------------------------------------------------
    implicit none
    !------------------------------------------------------------------------------
    logical                        :: intersection
    real(8),dimension(:),intent(in):: A1,A2,B1,B2
    real(8),dimension(dim)         :: t1,t2,t3
    real(8)                        :: det
    !------------------------------------------------------------------------------
    !--------fonction d'intersection entre deux segments---------------------------
    t1=B2-B1
    t2=B1-A1
    t3=B1-A2
    det=( t2(1)*t1(2)-t2(2)*t1(1) ) * ( t3(1)*t1(2)-t3(2)*t1(1) ) 
    intersection = (det<=0.d0) !Alors la droite B1B2 coupe le segment A1A2

    t1=A2-A1
    t2=A1-B1
    t3=A1-B2
    det=( t2(1)*t1(2)-t2(2)*t1(1) ) * ( t3(1)*t1(2)-t3(2)*t1(1) )
    intersection = (intersection .and. (det<=0.d0) )!Alors la droite A1A2 coupe aussi le segement B1B2
    !------------------------------------------------------------------------------
  end function intersection
  !********************************************************************************
  !********************************************************************************
  function intersection_bis(A1,A2,B1,B2)
    !------------------------------------------------------------------------------
    implicit none
    !------------------------------------------------------------------------------
    logical                        :: intersection_bis
    real(8),dimension(:),intent(in):: A1,A2,B1,B2
    real(8),dimension(dim)         :: t1,t2,t3
    real(8)                        :: det
    !------------------------------------------------------------------------------
    !--------fonction d'intersection entre une droite et un segment----------------

    t1=B2-B1
    t2=B1-A1
    t3=B1-A2
    det=( t2(1)*t1(2)-t2(2)*t1(1) ) * ( t3(1)*t1(2)-t3(2)*t1(1) ) 
    intersection_bis = (det<=0.d0) !Alors la droite B1B2 coupe le segment A1A2

  end function intersection_bis
  !********************************************************************************
  !********************************************************************************
  subroutine find_intersection_point(A,B,C,D,I)
    !-----------------------------------------------------------------------------
    implicit none
    !-----------------------------------------------------------------------------
    real(8),dimension(dim),intent(in)::A,B,C,D
    real(8),dimension(dim),intent(out)::I
    !-----------------------------------------------------------------------------
    real(8)::a1,a2,b1,b2
    !-----------------------------------------------------------------------------
    a1=( B(2)-A(2) )/( B(1)-A(1))!+1.d-14 )
    a2=( D(2)-C(2) )/( D(1)-C(1))!+1.d-14 )
    b1=A(2)-a1*A(1)
    b2=C(2)-a2*C(1)

    if ( abs( A(1)-B(1) )<=1.d-15 .and. C(1)/=D(1) ) then
       I(1)= A(1)
       I(2)= I(1)*a2+b2
    else if ( abs( C(1)-D(1) )<=1.d-15 .and. A(1)/=B(1) ) then
       I(1)= C(1)
       I(2)= I(1)*a1+b1
    else if ( abs( A(1)-B(1) )<=1.d-15 .and. abs( C(1)-D(1) )<1.d-15 ) then
       write(10,*) 'probleme intersection post_traitement'
    else
       I(1)=(b2-b1)/(a1-a2)
       I(2)=I(1)*a1+b1
    end if

  end subroutine find_intersection_point
  !********************************************************************************
  !********************************************************************************
  subroutine find_intersection_line_curve(X1,X2,xc,yc,axe1,axe2,xyzI)
    !----------------------------------------------------------------------------
    implicit none
    !-----------------------------------------------------------------------------
    real(8),dimension(dim),intent(in)::X1,X2
    real(8),intent(in)::axe1,axe2,xc,yc
    real(8),dimension(dim),intent(out)::xyzI
    !-----------------------------------------------------------------------------
    real(8)::delta,a,b,c,a1,b1,a0
    real(8),dimension(dim)::sol1,sol2,u,v,w
    !-----------------------------------------------------------------------------
    !------equation de droite à partir de deux points-----------------------------
    if ( abs(X2(1)-X1(1))<=eps_zero) then
       print*,'denominateur_nul'
       a0=X2(1)
       !-----------------------------------------------------------------------------
       !------intersection avec droite ellipse=equation du second degré--------------
       a=1/(axe2**2)
       b=(-2*yc)/(axe2**2)
       c=(yc**2)/(axe2**2)+(a0**2+xc**2-2*xc*a0)/(axe1**2)-1
       delta=b**2-4*a*c
       if (delta>0) then
          sol1(2)=(-b-sqrt(delta))/(2*a)
          sol1(1)=a0

          sol2(2)=(-b+sqrt(delta))/(2*a)
          sol2(1)=a0

          u=X2-X1
          v=sol1-X1
          w=sol2-X1

          if ( dot_product(u,v)>=0 ) xyzI=sol1
          if ( dot_product(u,w)>=0 ) xyzI=sol2

          !write(45,*) 'cas1',sol1,sol2,xyzI

       else if (abs(delta)<=eps_zero) then
          xyzI(2)=-b/(2*a)
          xyzI(1)=a0
          !write(45,*) 'delta nul',xyzI
       else
          write(45,*) 'pas de solution'
          print*,'pas de solution_1'
       end if
    else
       a1=( X2(2)-X1(2) )/( X2(1)-X1(1))
       b1=X1(2)-a1*X1(1)
       !-----On touve a1 et b1 de y=a1*x+b1------------------------------------------
       !-----------------------------------------------------------------------------
       !------intersection avec droite ellipse=equation du second degré--------------
       a=1/(axe1**2)+(a1**2)/(axe2**2)
       b=(-2*xc)/(axe1**2)+(2*a1*b1)/(axe2**2)-(2*yc*a1)/(axe2**2)
       c=(xc**2)/(axe1**2)+((b1-yc)/axe2)**2-1
       delta=b**2-4*a*c
       if (delta>0) then

          sol1(1)=(-b-sqrt(delta))/(2*a)
          sol1(2)=a1*sol1(1)+b1

          sol2(1)=(-b+sqrt(delta))/(2*a)
          sol2(2)=a1*sol2(1)+b1

          u=X2-X1
          v=sol1-X1
          w=sol2-X1

          if ( dot_product(u,v)>=0 ) xyzI=sol1
          if ( dot_product(u,w)>=0 ) xyzI=sol2

          !write(45,*) 'cas2',sol1,sol2,xyzI

       else if (abs(delta)<=eps_zero) then
          xyzI(1)=-b/(2*a)
          xyzI(2)=a1*xyzI(1)+b1
          ! write(45,*) 'delta nul',xyzI
       else
          ! write(45,*) 'pas de solution'
          print*,'pas de solution_2'
       end if
    end if
  end subroutine find_intersection_line_curve
  !********************************************************************************
  subroutine find_intersection_point_droites(a,b,c,aa,bb,cc,xyzI)
    !----------------------------------------------------------------------------
    implicit none
    !-----------------------------------------------------------------------------
    real(8),intent(in)::a,b,c,aa,bb,cc
    real(8),dimension(dim),intent(out)::xyzI
    !-----------------------------------------------------------------------------
!!$    if (abs(a)<=eps_zero .and. abs(aa)>eps_zero )then
!!$       xyzI(1)=b
!!$       xyzI(2)=aa*xyzI(1)+bb
!!$    else if (abs(aa)<=eps_zero .and. abs(a)>eps_zero ) then
!!$       xyzI(1)=bb
!!$       xyzI(2)=a*xyzI(1)+b
!!$    else if (abs(aa)<=eps_zero .and. abs(a)<=eps_zero ) then
!!$       print*,'pas d intersection'
!!$    else
!!$       xyzI(1)=(bb-b)/(a-aa)
!!$       xyzI(2)=a*xyzI(1)+b
!!$    end if
!!$    if (abs(a-aa)<=eps_zero) print*, 'probleme'
!!$    !if (abs(aa*b-bb*a)<=eps_zero ) then
!!$    !   print*,'cas 1'
!!$    xyzI(2)=(aa*c-cc*a)/(bb*a-aa*b)
!!$    if ( abs(a)<=eps_zero ) then
!!$       xyzI(1)= (-bb*xyzI(2)-cc)/aa
!!$    else if ( abs(aa)<=eps_zero) then
!!$       xyzI(1)= (-b*xyzI(2)-c)/a
!!$    end if
!!$    ! end if
    !  if (abs(bb*a-aa*b)<=eps_zero ) then
    !    print*,'cas 2'
    !xyzI(1)=(bb*c-cc*b)/(aa*b-bb*a)
    !if ( abs(b)<=eps_zero ) then
    !   xyzI(2)=(-cc-aa*xyzI(1))/bb
    !else !( abs(bb)<=eps_zero)! then
    !   xyzI(2)=(-c-a*xyzI(1))/b
    !end if
    !  end if
    !xyzI(2)=(aa*c-cc*a)/(bb*a-aa*b)
!!$    if ( abs(b)<=eps_zero) then
!!$       xyzI(2)=(-cc-aa*xyzI(1))/bb
!!$    else if ( abs(bb)<=eps_zero) then
!!$       xyzI(2)=(-c-a*xyzI(1))/b
!!$    end if
    xyzI(1)=(bb*c-cc*b)/(aa*b-bb*a)
    if ( abs(b)<=eps_zero ) then
       xyzI(2)=(-cc-aa*xyzI(1))/bb
    else
       xyzI(2)=(-c-a*xyzI(1))/b
    end if
    !-----------------------------------------------------------------------------
  end subroutine find_intersection_point_droites
  !********************************************************************************
  !********************************************************************************
  subroutine condition_number_matrix(n,AA,cond,lambda1,lambda2)
    !---------------------------------------------------------
    implicit none
    !---------------------------------------------------------
    !Global variable
    !---------------------------------------------------------
    integer,intent(in)::n
    real(8),dimension(n,n),intent(in)::AA
    real(8),intent(out)::cond,lambda1,lambda2
    !---------------------------------------------------------
    !Local variables
    !---------------------------------------------------------
    real(8),dimension(n,n) :: AT,AAT
    real(8) :: a,b,c,delta!,lambda1,lambda2
    !---------------------------------------------------------
    AT=transpose(AA)
    AAT=matmul(AA,AT)

    !---------------singular value calculation----------------
    a=1
    b=-( AAT(2,2)+AAT(1,1) )
    c= ( AAT(1,1)*AAT(2,2) )- ( AAT(1,2)*AAT(2,1) )
    delta= b*b -4*a*c
    if ( delta >= 0) then
       lambda1 =(-b-sqrt(delta))/2*a
       lambda2 =(-b+sqrt(delta))/2*a
!!$    else if ( abs(delta) <= 1.d-15) then
!!$       print*, 'single edgein value'
!!$       lambda1=-b/2*a
    else
       print*, 'complex edgein value'
       lambda1 = sqrt(b*b - delta)/2*a
       lambda2 = sqrt(b*b - delta)/2*a
    end if
    lambda1 = min(lambda1,lambda2)
    lambda2 = max(lambda1,lambda2)

    lambda1= sqrt(lambda1)
    lambda2= sqrt(lambda2)

    cond=lambda2/lambda1
    !---------------------------------------------------------
  end subroutine condition_number_matrix
  !********************************************************************************
  !************************************************************************************
  real(8) function pes(r)
    !*******************************************************************************
    implicit none
    !-------------------------------------------------------------------------------
    !variables globales
    !-------------------------------------------------------------------------------
    real(8) :: r
    !real(8) :: pes2
    !-------------------------------------------------------------------------------
    if (abs(r).ge.2.d0) then
       pes=0.d0
       return
    endif
    if (abs(r).le.1.d0) then
       pes=pes2(r)
    else
       pes=0.5d0-pes2(2.d0-abs(r))
    endif

    return

  end function pes
  !******************************************************************************

  real(8) function pes2(r)
    !*******************************************************************************
    implicit none
    !-------------------------------------------------------------------------------
    !variables globales
    !-------------------------------------------------------------------------------
    real(8) :: r
    !-------------------------------------------------------------------------------
    r=abs(r)
    !pes2=(3.d0-2.d0*r+sqrt(1.d0+4.d0*r-4.d0*r*r))/8.d0
    pes2=(3.d0-2.d0*abs(r)+sqrt(1.d0+4.d0*abs(r)-4.d0*r*r))/8.d0
    return
  end function pes2
  !*******************************************************************************

  real(8) function pes3(r,h)
    !*******************************************************************************
    implicit none
    !-------------------------------------------------------------------------------
    !Global variable
    !-------------------------------------------------------------------------------
    real(8) :: r,h
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    real(8) :: dimr
    !-------------------------------------------------------------------------------
    !dimr=1.d0
    !if (abs(r).ge.(dimr*h)) then
    if (abs(r).ge.(3*h)) then
       pes3=0.d0
    else
       !pes3=1.d0/(dimr**2*h)*(1.d0+cos(pi*r/(dimr*h)))
       pes3=1.d0/(4*h)*(1.d0+cos(pi*r/(2*h)))
    endif
    !write(*,*) abs(r),3*h,pes3 
    return
  end function pes3
  !**************************************************************************************
  !********************************************************************************
  function entre_les_bissectrices(xyz1,xyz2,n1,n2,M)
    implicit none
    logical:: entre_les_bissectrices
    real(8),dimension(:),intent(in)::xyz1,xyz2,n1,n2,M
    real(8),dimension(dim)::n0
    real(8)::det

    n0=M-xyz1
    n0=n0/sqrt(dot_product(n0,n0))
    det=1
    det=det*(n1(1)*n0(2)-n1(2)*n0(1))
    entre_les_bissectrices = (det>=0)

    n0=M-xyz2
    n0=n0/sqrt(dot_product(n0,n0))
    det=(n2(1)*n0(2)-n2(2)*n0(1))

    entre_les_bissectrices = (entre_les_bissectrices.and.(det<=0))
  end function entre_les_bissectrices
  !********************************************************************************

  !********************************************************************************
  real(8) function distance_algebrique(A,B,M,xyzI)
    !*******************************************************************************
    implicit none
    !-------------------------------------------------------------------------------
    real(8),dimension(2),intent(in)::A,B,M
    real(8),dimension(2),intent(out)::xyzI
    !-------------------------------------------------------------------------------
    real(8),dimension(2)::u,v,t,n
    real(8)::det,aa,bb,aaa,bbb,cc,ccc
    !-------------------------------------------------------------------------------
    u=M-A ; u=u/sqrt(dot_product(u,u))
    v=B-A ; n=v/sqrt(dot_product(v,v))
    !vecteur perpendiculaire à n
    t= (/ -n(2),n(1) /)
    !projection de u sur t pour déterminer le signe
    det=u(1)*t(2)-u(2)*t(1)
    ! meme operation sur MB
    u=M-B ; u=u/sqrt(dot_product(u,u))
    !on projette t sur u pour avoir le signe et on fait le produit des signes
    det=det*(u(2)*t(1)-u(1)*t(2))
    !si le signe est positif
    if (det>0) then
       u=M-A
       v=B-A ; v=v/sqrt(dot_product(v,v))   
       !simple produit vectoriel
       distance_algebrique= v(1)*u(2)-u(1)*v(2)
       !-----------------------on cherche les coordonnées du point projeté----------------------!
!!$       aa=(B(2)-A(2))/(B(1)-A(1))
!!$       if (abs(B(1)-A(1))<=eps_zero) print*,'denominateur nul'
!!$       bb = A(2)-aa*A(1)
!!$       aaa = -1.d0/aa
!!$       if (abs(aa)<=eps_zero) print*,'denominateur nul'
!!$       bbb = M(2)-aaa*M(1)
       aa=v(2) ; bb=-v(1) ; cc=v(1)*A(2)-v(2)*A(1)
       aaa=bb ; bbb=-aa ; ccc=-aaa*M(1)-bbb*M(2)
       call find_intersection_point_droites(aa,bb,cc,aaa,bbb,ccc,xyzI) 
       !----------------------------------------------------------------------------------------!
    else
       !le produit est negatif
       u=A-M
       v=B-M
       !distance= min entre AM et BM fois le signe
       if (dot_product(u,u) <= dot_product(v,v) )then
          distance_algebrique=dot_product(u,u)
          xyzI = A
       else
          distance_algebrique=dot_product(v,v)
          xyzI = B
       end if
       !distance_algebrique=min(dot_product(u,u),dot_product(v,v))
       distance_algebrique=sqrt(distance_algebrique)*sign(1._8,u(1)*v(2)-v(1)*u(2))
    end if

  end function distance_algebrique
  !********************************************************************************
  
  function cross_product(vec1,vec2)
    implicit none
    real(8), dimension(3), intent(in) :: vec1,vec2
    real(8), dimension(3)             :: cross_product

    cross_product(1) =  vec1(2)*vec2(3)-vec1(3)*vec2(2)
    cross_product(2) = -vec1(1)*vec2(3)+vec1(3)*vec2(1)
    cross_product(3) =  vec1(1)*vec2(2)-vec1(2)*vec2(1)

  end function cross_product
  
end module mod_math
