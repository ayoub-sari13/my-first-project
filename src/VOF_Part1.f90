!========================================================================
!
!                   FUGU Version 1.0.0
!
!========================================================================
!
! Copyright  Stéphane Vincent
!
! stephane.vincent@u-pem.fr          straightparadigm@gmail.com
!
! This file is part of the FUGU Fortran library, a set of Computational 
! Fluid Dynamics subroutines devoted to the simulation of multi-scale
! multi-phase flows for resolved scale interfaces (liquid/gas/solid). 
! FUGU uses the initial developments of DyJeAT and SHALA codes from 
! Jean-Luc Estivalezes (jean-luc.estivalezes@onera.fr) and 
! Frédéric Couderc (couderc@math.univ-toulouse.fr).
!
!========================================================================
!**
!**   NAME       : Fugu.f90
!**
!**   AUTHOR     : Stéphane Vincent
!**
!**   FUNCTION   : 2D VOF subroutines of FUGU software
!**
!**   DATES      : Version 1.0.0  : from : january, 16, 2016
!**
!========================================================================

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
module mod_VOF_2D3D
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  use mod_VOF_3D
  use mod_VOF_3D_bis
  use mod_Parameters
  use mod_Constants
  use mod_LevelSet
  use mod_math
  use mod_operators
  use mod_mpi
  integer :: iter_VOF
  
contains
  !*******************************************************************************                       
  !                                 VOF 2D/3D PLIC
  !*******************************************************************************

  !*******************************************************************************
  subroutine vofplic_2d(cou,u,v)
    use mod_borders
    !*******************************************************************************
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable, intent(in)    :: u,v
    real(8), dimension(:,:,:), allocatable, intent(inout) :: cou
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    integer                                               :: i,j,k,nbcfl,nbb,ntt
    real(8)                                               :: dtcfl,dv,dtrest,tdeb,tfin
    real(8), dimension(:,:,:), allocatable                :: ut,vt,wt
    !-------------------------------------------------------------------------------
    allocate(ut(sxu-gx:exu+gx,syu-gy:eyu+gy,szu-gz:ezu+gz),&
         &   vt(sxv-gx:exv+gx,syv-gy:eyv+gy,szv-gz:ezv+gz))

    ! use mod_allocate
    ! call allocate_array_3(ALLOCATE_ON_U_MESH,ut,mesh)
    ! call allocate_array_3(ALLOCATE_ON_V_MESH,vt,mesh)
    
    call transformation_2d3d(u,ut,ddxu,1)
    call transformation_2d3d(v,vt,ddyv,2)

    !call transformation_2d3d(u,ut,mesh%ddxu,1)
    !call transformation_2d3d(v,vt,mesh%ddyv,2)

    dv=dt
    dtcfl = 0
    dtrest = 0
    nbcfl = 0
    call cflvof_2d3d(ut,vt,wt,cou,dtcfl,dtrest,nbcfl)
    if (dtrest /= 0) then
       nbb = nbcfl + 1
    else
       nbb = nbcfl
    end if

    iter_VOF=nbb
    
    !-------------------------------------------------------------------------------
    !time loop
    !-------------------------------------------------------------------------------
    ! write(6,*) 'nbb=',nbb
    do ntt = 1,nbb
       !-------------------------------------------------------------------------------
       if (nproc>1) call comm_mpi_sca(cou)
       call sca_periodicity(mesh,cou)
       !-------------------------------------------------------------------------------
       if (ntt <= nbcfl) then
          dv = dtcfl
       else
          dv = dtrest
       end if
       call vof2dlin(cou,dv,ut,vt)
    end do
    deallocate(ut,vt)
    !-------------------------------------------------------------------------------
    ! removal of residues
    !-------------------------------------------------------------------------------
    do j = sy,ey
       do i = sx,ex
          if (cou(i,j,1)<1.d-06) cou(i,j,1)=0
       end do
    end do
    !-------------------------------------------------------------------------------
    if (nproc>1) call comm_mpi_sca(cou)
    call sca_periodicity(mesh,cou)
    !-------------------------------------------------------------------------------
    return
  end subroutine vofplic_2d
  !*******************************************************************************

  !*******************************************************************************
  subroutine vofplic_3d(cou,u,v,w)
    use mod_borders
    !*******************************************************************************
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable, intent(in)    :: u,v,w
    real(8), dimension(:,:,:), allocatable, intent(inout) :: cou
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    integer                                               :: i,j,k,nbcfl,nbb,ntt
    real(8)                                               :: dtcfl,dv,dtrest,tdeb,tfin
    real(8), dimension(:,:,:), allocatable                :: ut,vt,wt
    !-------------------------------------------------------------------------------
    allocate(ut(sxu-gx:exu+gx,syu-gy:eyu+gy,szu-gz:ezu+gz),&
         &   vt(sxv-gx:exv+gx,syv-gy:eyv+gy,szv-gz:ezv+gz),&
         &   wt(sxw-gx:exw+gx,syw-gy:eyw+gy,szw-gz:ezw+gz))
    call transformation_2d3d(u,ut,ddxu,1)
    call transformation_2d3d(v,vt,ddyv,2)
    call transformation_2d3d(w,wt,ddzw,3)
    dv=dt
    dtcfl = 0
    dtrest = 0
    nbcfl = 0
    call cflvof_2d3d(ut,vt,wt,cou,dtcfl,dtrest,nbcfl)
    if (dtrest /= 0) then
       nbb = nbcfl + 1
    else
       nbb = nbcfl
    end if

    iter_VOF=nbb
    
    !-------------------------------------------------------------------------------
    ! Time loop
    !-------------------------------------------------------------------------------
    ! write(6,*) 'nbb=',nbb
    do ntt = 1,nbb
       !-------------------------------------------------------------------------------
       if (nproc>1) call comm_mpi_sca(cou)
       call sca_periodicity(mesh,cou)
       !-------------------------------------------------------------------------------
       if (ntt <= nbcfl) then
          dv = dtcfl
       else
          dv = dtrest
       end if
       call vof3dlin(cou,dv,ut,vt,wt)
    end do
    deallocate(ut,vt,wt)
    !-------------------------------------------------------------------------------
    ! removal of residues
    !-------------------------------------------------------------------------------
    do k = sz,ez
       do j = sy,ey
          do i = sx,ex
             if (cou(i,j,k)<1.d-06) cou(i,j,k) = 0
          end do
       end do
    end do
    !-------------------------------------------------------------------------------
    if (nproc>1) call comm_mpi_sca(cou)
    call sca_periodicity(mesh,cou)
    !-------------------------------------------------------------------------------
    return
  end subroutine vofplic_3d
  !*******************************************************************************

  !*******************************************************************************
  subroutine vof2dlin(cou,dv,ut,vt)
    !*******************************************************************************
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    real(8), intent(in)                                    :: dv
    real(8), dimension(:,:,:),   allocatable, intent(inout):: cou
    real(8), dimension(:,:,:),   allocatable, intent(in)   :: ut,vt
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    integer                                                :: i,j,k
    integer                                                :: icas = 0,ixr = 0
    real(8)                                                :: a1,eps0,eps1
    real(8)                                                :: f1 = 0.0d0
    real(8)                                                :: c1,c3,eps2,s12,s22,s32,x1, &
                                                              x2,xm1,xm3,xmp1,xmp3,xr
    real(8), dimension(:,:,:),   allocatable               :: co0
    !-------------------------------------------------------------------------------
    eps0 = 1.d-8
    eps1 = 1.d-80
    eps2 = 1.d-13
    k=1
    allocate(co0(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
    !-------------------------------------------------------------------------------
    !x direction 
    !-------------------------------------------------------------------------------
    co0 = cou
    do j = sy,ey
       do i = sx,ex
          if (co0(i-1,j,1)+co0(i,j,1)+co0(i+1,j,1) > 0.d0) then
             if (co0(i-1,j,1)+co0(i,j,1)+co0(i+1,j,1) < 3.d0) then
                if (co0(i-1,j,1)+co0(i,j,1)<2.d0 .or. ut(i+1,j,1)<=0.d0) then
                   if (co0(i+1,j,1)+co0(i,j,1)<2.d0 .or. ut(i,j,1)>=0.d0) then
                      if (co0(i-1,j,1)+co0(i,j,1)>0.d0 .or. ut(i+1,j,1)<=0.d0) then
                         if (co0(i+1,j,1)+co0(i,j,1)>0.d0 .or. ut(i,j,1)>=0.d0) then
                            !-------------------------------------------------------------------------------
                            !s12 calculation
                            !normal calculation
                            !-------------------------------------------------------------------------------
                            if ((ut(i,j,1)>0.d0) .and. (co0(i-1,j,1)>0.d0)) then
                               call normaloc(co0,xm1,xm3,i-1,j,k,eps0,eps1)
                               xmp1 = xm1
                               xmp3 = xm3
                               !-------------------------------------------------------------------------------
                               !We arrange the normals in the domain +
                               !-------------------------------------------------------------------------------
                               call rangexm(co0(i-1,j,1),xm1,xm3,c1,c3,f1,icas,ixr)
                               !-------------------------------------------------------------------------------
                               !xr calculation
                               !-------------------------------------------------------------------------------
                               call calculxr(xm1,xm3,xr,c1,c3,f1,icas)
                               c1 = 1.d0
                               c3 = 1.d0
                               if (ixr == 1) then
                                  xr = xr
                               elseif (ixr == 4) then
                                  xr = -xr
                               elseif (ixr == 2) then
                                  xr = xr + xmp1*c1
                               else
                                  xr = xr + xmp3*c3
                               end if
                               !-------------------------------------------------------------------------------
                               !xr shifting
                               !-------------------------------------------------------------------------------
                               c1 = 1.d0
                               c3 = 1.d0
                               xm1 = xmp1 / (1.d0+(ut(i,j,1)-ut(i-1,j,1))*dv/c1)
                               xm3 = xmp3
                               xr = xr + xm1*ut(i-1,j,1)*dv
                               !-------------------------------------------------------------------------------
                               !reference change
                               !-------------------------------------------------------------------------------
                               x1 = c1
                               x2 = c1 + ut(i,j,1)*dv
                               xr = xr - xm1*x1
                               c1 = x2 - x1
                               c3 = c3
                               !-------------------------------------------------------------------------------
                               !area calculation
                               !-------------------------------------------------------------------------------
                               if (c1 > 0.d0) then
                                  call calculf(xm1,xm3,xr,c1,c3,a1,icas,ixr)
                               else
                                  a1 = 0.d0
                               end if

                               if (ixr == 4) then
                                  s12 = (c1*c3-a1)
                               else
                                  s12 = a1
                               end if
                               if (s12 < eps2) then
                                  s12 = 0.d0
                               end if
                            else
                               s12 = 0.d0
                            end if
                            !-------------------------------------------------------------------------------
                            !s32 calculation
                            !normal calculation
                            !-------------------------------------------------------------------------------
                            if ((ut(i+1,j,1)<0.d0) .and. (co0(i+1,j,1)>0.d0)) then
                               call normaloc(co0,xm1,xm3,i+1,j,k,eps0,eps1)
                               xmp1 = xm1
                               xmp3 = xm3
                               !-------------------------------------------------------------------------------
                               !We arrange the normals in the domain +
                               !-------------------------------------------------------------------------------
                               call rangexm(co0(i+1,j,1),xm1,xm3,c1,c3,f1,icas,ixr)
                               !-------------------------------------------------------------------------------
                               !xr calculation
                               !-------------------------------------------------------------------------------
                               call calculxr(xm1,xm3,xr,c1,c3,f1,icas)

                               c1 = 1.d0
                               c3 = 1.d0
                               if (ixr == 1) then
                                  xr = xr
                               elseif (ixr == 4) then
                                  xr = -xr
                               elseif (ixr == 2) then
                                  xr = xr + xmp1*c1
                               else
                                  xr = xr + xmp3*c3
                               end if
                               !-------------------------------------------------------------------------------
                               !xr shifting
                               !-------------------------------------------------------------------------------
                               c1 = 1.d0
                               c3 = 1.d0
                               xm1 = xmp1 / (1.d0+(ut(i+2,j,1)-ut(i+1,j,1))*dv/c1)
                               xm3 = xmp3
                               xr = xr + xm1*ut(i+1,j,1)*dv
                               !-------------------------------------------------------------------------------
                               !reference change
                               !-------------------------------------------------------------------------------
                               x1 = ut(i+1,j,1)*dv
                               x2 = 0.d0
                               xr = xr - xm1*x1
                               c1 = x2 - x1
                               c3 = c3
                               !-------------------------------------------------------------------------------
                               !area calculation 
                               !-------------------------------------------------------------------------------
                               if (c1 > 0.d0) then
                                  call calculf(xm1,xm3,xr,c1,c3,a1,icas,ixr)
                               else
                                  a1 = 0.d0
                               end if

                               if (ixr == 4) then
                                  s32 = (c1*c3-a1)
                               else
                                  s32 = a1
                               end if
                               if (s32 < eps2) then
                                  s32 = 0.d0
                               end if
                            else
                               s32 = 0.d0
                            end if

                            if (co0(i,j,1) > 0.d0) then
                               !-------------------------------------------------------------------------------
                               !calcul s22
                               !calcul des normales
                               !-------------------------------------------------------------------------------
                               call normaloc(co0,xm1,xm3,i,j,k,eps0,eps1)
                               xmp1 = xm1
                               xmp3 = xm3
                               !-------------------------------------------------------------------------------
                               !We arrange the normals in the domain +
                               !-------------------------------------------------------------------------------
                               call rangexm(co0(i,j,1),xm1,xm3,c1,c3,f1,icas,ixr)

                               !-------------------------------------------------------------------------------
                               !xr calculation
                               !-------------------------------------------------------------------------------
                               call calculxr(xm1,xm3,xr,c1,c3,f1,icas)

                               c1 = 1.d0
                               c3 = 1.d0
                               if (ixr == 1) then
                                  xr = xr
                               elseif (ixr == 4) then
                                  xr = -xr
                               elseif (ixr == 2) then
                                  xr = xr + xmp1*c1
                               else
                                  xr = xr + xmp3*c3
                               end if
                               !-------------------------------------------------------------------------------
                               !xr shifting
                               !-------------------------------------------------------------------------------
                               c1 = 1.d0
                               c3 = 1.d0
                               xm1 = xmp1 / (1.d0+(ut(i+1,j,1)-ut(i,j,1))*dv/c1)
                               xm3 = xmp3
                               xr = xr + xm1*ut(i,j,1)*dv
                               !-------------------------------------------------------------------------------
                               !reference change
                               !-------------------------------------------------------------------------------
                               x1 = max(0.d0,ut(i,j,1)*dv)
                               x2 = min(c1,c1+ut(i+1,j,1)*dv)
                               xr = xr - xm1*x1
                               c1 = x2 - x1
                               c3 = c3
                               !-------------------------------------------------------------------------------
                               !area calculation
                               !-------------------------------------------------------------------------------
                               if (c1 > 0.d0) then
                                  call calculf(xm1,xm3,xr,c1,c3,a1,icas,ixr)
                               else
                                  a1 = 0.d0
                               end if

                               if (ixr == 4) then
                                  s22 = (c1*c3-a1)
                               else
                                  s22 = a1
                               end if
                               if (s22 < eps2) then
                                  s22 = 0.d0
                               end if
                            else
                               s22 = 0.d0
                            end if
                            !-------------------------------------------------------------------------------
                            !cou calculation 
                            !-------------------------------------------------------------------------------
                            cou(i,j,1) = s12 + s22 + s32
                            if (1.d0-cou(i,j,1) < eps2) then
                               cou(i,j,1) = 1.d0
                            end if
                            cycle
                         end if
                      end if
                   end if
                end if
             end if
          end if
          cou(i,j,1) = co0(i,j,1)

       end do
    end do
    !-------------------------------------------------------------------------------
    if (nproc>1) call comm_mpi_sca(cou)
    !-------------------------------------------------------------------------------
    !y direction 
    !-------------------------------------------------------------------------------
    co0 = cou
    do j = sy,ey
       do i = sx,ex
          if (co0(i,j-1,1)+co0(i,j,1)+co0(i,j+1,1) > 0.d0) then
             if (co0(i,j-1,1)+co0(i,j,1)+co0(i,j+1,1)< 3.d0) then
                if (co0(i,j-1,1)+co0(i,j,1)<2.d0 .or. vt(i,j+1,1)<=0.d0) then
                   if (co0(i,j+1,1)+co0(i,j,1)<2.d0 .or. vt(i,j,1)>=0.d0) then
                      if (co0(i,j-1,1)+co0(i,j,1)>0.d0 .or. vt(i,j+1,1)<=0.d0) then
                         if (co0(i,j+1,1)+co0(i,j,1)>0.d0 .or. vt(i,j,1)>=0.d0) then
                            !-------------------------------------------------------------------------------
                            !s12 calculation
                            !normals calculation
                            !-------------------------------------------------------------------------------
                            if ((vt(i,j,1)>0.d0)  .and. (co0(i,j-1,1)>0.d0)) then
                               call normaloc(co0,xm1,xm3,i,j-1,k,eps0,eps1)
                               xmp1 = xm3
                               xmp3 = -xm1
                               xm1 = xmp1
                               xm3 = xmp3
                               !-------------------------------------------------------------------------------
                               !We arrange the normals in the domain + 
                               !-------------------------------------------------------------------------------
                               call rangexm(co0(i,j-1,1),xm1,xm3,c1,c3,f1,icas,ixr)
                               !-------------------------------------------------------------------------------
                               !xr calculation
                               !-------------------------------------------------------------------------------
                               call calculxr(xm1,xm3,xr,c1,c3,f1,icas)
                               c1 = 1.d0
                               c3 = 1.d0
                               if (ixr == 1) then
                                  xr = xr
                               elseif (ixr == 4) then
                                  xr = -xr
                               elseif (ixr == 2) then
                                  xr = xr + xmp1*c1
                               else
                                  xr = xr + xmp3*c3
                               end if
                               !-------------------------------------------------------------------------------
                               !xr shifting 
                               !-------------------------------------------------------------------------------
                               c1 = 1.d0
                               c3 = 1.d0
                               xm1 = xmp1 / (1.d0+(vt(i,j,1)-vt(i,j-1,1))*dv/c1)
                               xm3 = xmp3
                               xr = xr + xm1*vt(i,j-1,1)*dv
                               !-------------------------------------------------------------------------------
                               !reference change
                               !-------------------------------------------------------------------------------
                               x1 = c1
                               x2 = c1 + vt(i,j,1)*dv
                               xr = xr - xm1*x1
                               c1 = x2 - x1
                               c3 = c3
                               !-------------------------------------------------------------------------------
                               !area calculation
                               !-------------------------------------------------------------------------------
                               if (c1 > 0.d0) then
                                  call calculf(xm1,xm3,xr,c1,c3,a1,icas,ixr)
                               else
                                  a1 = 0.d0
                               end if

                               if (ixr == 4) then
                                  s12 = (c1*c3-a1)
                               else
                                  s12 = a1
                               end if
                               if (s12 < eps2) then
                                  s12 = 0.d0
                               end if
                            else
                               s12 = 0.d0
                            end if
                            !-------------------------------------------------------------------------------
                            !calcul s32
                            !normal calculation
                            !-------------------------------------------------------------------------------
                            !if ((vts(lvn)<0.d0) .and. (co0(ln)>0.d0)) then
                            if ((vt(i,j+1,1)<0.d0) .and. (co0(i,j+1,1)>0.d0)) then
                               !call normaloc(co0,xm1,xm3,jcoeft,ktj,ln,kkt,ncoeft,eps0,eps1)
                               call normaloc(co0,xm1,xm3,i,j+1,k,eps0,eps1)
                               xmp1 = xm3
                               xmp3 = -xm1
                               xm1 = xmp1
                               xm3 = xmp3
                               !-------------------------------------------------------------------------------
                               !We arrange the normals in the domain + 
                               !-------------------------------------------------------------------------------
                               call rangexm(co0(i,j+1,1),xm1,xm3,c1,c3,f1,icas,ixr)
                               !-------------------------------------------------------------------------------
                               !xr calculation
                               !-------------------------------------------------------------------------------
                               call calculxr(xm1,xm3,xr,c1,c3,f1,icas)
                               c1 = 1.d0
                               c3 = 1.d0
                               if (ixr == 1) then
                                  xr = xr
                               elseif (ixr == 4) then
                                  xr = -xr
                               elseif (ixr == 2) then
                                  xr = xr + xmp1*c1
                               else
                                  xr = xr + xmp3*c3
                               end if
                               !-------------------------------------------------------------------------------
                               !xr shifting
                               !-------------------------------------------------------------------------------
                               c1 = 1.d0
                               c3 = 1.d0
                               xm1 = xmp1 / (1.d0+(vt(i,j+2,1)-vt(i,j+1,1))*dv/c1)
                               xm3 = xmp3
                               xr = xr + xm1*vt(i,j+1,1)*dv
                               !-------------------------------------------------------------------------------
                               !reference change
                               !-------------------------------------------------------------------------------
                               x1 = vt(i,j+1,1)*dv                                                 
                               x2 = 0.d0
                               xr = xr - xm1*x1
                               c1 = x2 - x1
                               c3 = c3
                               !-------------------------------------------------------------------------------
                               !area calculation
                               !-------------------------------------------------------------------------------
                               if (c1 > 0.d0) then
                                  call calculf(xm1,xm3,xr,c1,c3,a1,icas,ixr)
                               else
                                  a1 = 0.d0
                               end if
                               if (ixr == 4) then
                                  s32 = (c1*c3-a1)
                               else
                                  s32 = a1
                               end if
                               if (s32 < eps2) then
                                  s32 = 0.d0
                               end if
                            else
                               s32 = 0.d0
                            end if
                            if (co0(i,j,1) > 0.d0) then
                               !-------------------------------------------------------------------------------
                               !s22 calculation
                               !normals calculation
                               !-------------------------------------------------------------------------------
                               call normaloc(co0,xm1,xm3,i,j,k,eps0,eps1)
                               xmp1 = xm3
                               xmp3 = -xm1
                               xm1 = xmp1
                               xm3 = xmp3
                               !-------------------------------------------------------------------------------
                               !We arrange the normals in the domain + 
                               !-------------------------------------------------------------------------------
                               call rangexm(co0(i,j,1),xm1,xm3,c1,c3,f1,icas,ixr)
                               !-------------------------------------------------------------------------------
                               !xr calculation
                               !-------------------------------------------------------------------------------
                               call calculxr(xm1,xm3,xr,c1,c3,f1,icas)
                               c1 = 1.d0
                               c3 = 1.d0
                               if (ixr == 1) then
                                  xr = xr
                               elseif (ixr == 4) then
                                  xr = -xr
                               elseif (ixr == 2) then
                                  xr = xr + xmp1*c1
                               else
                                  xr = xr + xmp3*c3
                               end if
                               !-------------------------------------------------------------------------------
                               !xr shifting
                               !-------------------------------------------------------------------------------
                               c1 = 1.d0
                               c3 = 1.d0
                               xm1 = xmp1 / (1.d0+(vt(i,j+1,1)-vt(i,j,1))*dv/c1)
                               xm3 = xmp3
                               xr = xr + xm1*vt(i,j,1)*dv
                               !-------------------------------------------------------------------------------
                               !reference change
                               !-------------------------------------------------------------------------------
                               x1 = max(0.d0,vt(i,j,1)*dv)
                               x2 = min(c1,c1+vt(i,j+1,1)*dv)
                               xr = xr - xm1*x1
                               c1 = x2 - x1
                               c3 = c3
                               !-------------------------------------------------------------------------------
                               !area calculation
                               !-------------------------------------------------------------------------------
                               if (c1 > 0.d0) then
                                  call calculf(xm1,xm3,xr,c1,c3,a1,icas,ixr)
                               else
                                  a1 = 0.d0
                               end if

                               if (ixr == 4) then
                                  s22 = (c1*c3-a1)
                               else
                                  s22 = a1
                               end if
                               if (s22 < eps2) then
                                  s22 = 0.d0
                               end if
                            else
                               s22 = 0.d0
                            end if
                            !-------------------------------------------------------------------------------
                            !Volume fraction calculation
                            !-------------------------------------------------------------------------------
                            cou(i,j,1) = s12 + s22 + s32
                            if (1.d0-cou(i,j,1) < eps2) then
                               cou(i,j,1) = 1.d0
                            end if
                            cycle
                         end if
                      end if
                   end if
                end if
             end if
          end if
          cou(i,j,1) = co0(i,j,1)
       end do
    end do
    deallocate(co0)
    return

  end subroutine vof2dlin
  !*******************************************************************************

  !*******************************************************************************
  subroutine calculf(xm1,xm3,xr,c1,c3,a1,icas,ixr)
    !*******************************************************************************
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in) :: icas,ixr
    real(8), intent(in) :: c1,c3,xm1,xm3,xr
    real(8), intent(out):: a1
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    real(8)             :: a0,b0,cp1,cp3,xmp1,xmp3,xrp
    !-------------------------------------------------------------------------------
    if (ixr == 1) then
       a0 = xm1
       b0 = xm3
       xrp = xr
    elseif (ixr == 4) then
       a0 = -xm1
       b0 = -xm3
       xrp = -xr
    elseif (ixr == 2) then
       a0 = -xm1
       b0 = xm3
       xrp = xr - xm1*c1
    else
       a0 = xm1
       b0 = -xm3
       xrp = xr - xm3*c3
    end if
    if (a0*c1 >= b0*c3) then
       xmp1 = b0
       xmp3 = a0
       cp1 = c3
       cp3 = c1
    else
       xmp1 = a0
       xmp3 = b0
       cp1 = c1
       cp3 = c3
    end if
    if (icas == 2) then
       a1 = cp1 * (max(0.d0,xrp/xmp3)-max(0.d0,xrp/xmp3-cp3))
    else
       a1 = (max(0.d0,xrp)*xrp-(max(0.d0,xrp-xmp1*cp1)*(xrp-xmp1*cp1))- &
            (max(0.d0,xrp-xmp3*cp3)*(xrp-xmp3*cp3))+ &
            (max(0.d0,xrp-xmp1*cp1-xmp3*cp3)*(xrp-xmp1*cp1-xmp3*cp3))) &
            / (2.d0*xmp1*xmp3)
    end if

    return

  end subroutine calculf
  !*******************************************************************************

  !*******************************************************************************
  subroutine calculxr(xm1,xm3,xr,c1,c3,f1,icas)
    !*******************************************************************************
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in) :: icas
    real(8), intent(in) :: c1,c3,f1,xm1,xm3
    real(8), intent(out) :: xr
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    real(8) :: a1,aa1,aa2,cp1,cp3,xmp1,xmp3
    !-------------------------------------------------------------------------------
    a1 = f1 * c1 * c3
    aa1 = xm1 * c1 * c1 / (2.d0*xm3)
    xmp1 = xm1
    xmp3 = xm3
    cp1 = c1
    cp3 = c3
    aa2 = c1*c3 - aa1
    if (a1 < 0.d0) then
       a1 = 0.d0
    end if
    if (a1 > c1*c3) then
       a1 = c1 * c3
    end if
    if (icas /= 1) then
       xr = a1 * xmp3 / cp1
       return
    end if
    if ((a1>0.d0) .and. (a1<=aa1)) then
       xr = sqrt(2.d0*a1*xmp1*xmp3)
       return
    end if
    if ((a1>aa1) .and. (a1<=aa2)) then
       xr = (xmp1*cp1*cp1+2.d0*a1*xmp3) / (2.d0*cp1)
    elseif ((a1>aa2) .and. (a1<c1*c3)) then
       xr = xmp1*cp1 + xmp3*cp3 - &
            sqrt(2.d0*xmp1*cp1*xmp3*cp3-2.d0*a1*xmp1*xmp3)
    else
       xr = xmp1*cp1 + xmp3*cp3
    end if

    return

  end subroutine calculxr
  !*******************************************************************************

  !*******************************************************************************
  subroutine normaloc(co0,xm1,xm3,i,j,k,eps0,eps1)
    !*******************************************************************************
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)                                  :: i,j,k
    real(8), intent(in)                                  :: eps0,eps1
    real(8), intent(out)                                 :: xm1,xm3
    real(8), dimension(:,:,:),   allocatable, intent(in) :: co0
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    real(8), dimension(3)                                :: lee,lew,len,les,leen,lees, &
                                                            lewn,lews,le
    real(8)                                              :: pm1,pm11,pm12,pm13,pm14,pm3, &
                                                            pm31,pm32,pm33,pm34,inorme_pm
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    !normals calculation
    !-------------------------------------------------------------------------------
    le = (/ i,j,k /)
    lee=(/ i+1,j,k /)
    lew=(/ i-1,j,k /)
    len=(/ i,j+1,k /)
    les=(/ i,j-1,k /)
    leen=(/ i+1,j+1,k /)
    lees=(/ i+1,j-1,k /)
    lewn=(/ i-1,j+1,k /)
    lews=(/ i-1,j-1,k /)
    pm11 = (co0(i+1,j,k)+co0(i+1,j+1,k)-(co0(i,j,k)+co0(i,j+1,k))) * 0.5d0
    pm31 = (co0(i,j+1,k)+co0(i+1,j+1,k)-(co0(i,j,k)+co0(i+1,j,k))) * 0.5d0
    pm12 = (co0(i+1,j,k)+co0(i+1,j-1,k)-(co0(i,j,k)+co0(i,j-1,k))) * 0.5d0
    pm32 = (co0(i,j,k)+co0(i+1,j,k)-(co0(i,j-1,k)+co0(i+1,j-1,k))) * 0.5d0
    pm13 = (co0(i,j,k)+co0(i,j+1,k)-(co0(i-1,j,k)+co0(i-1,j+1,k))) * 0.5d0
    pm33 = (co0(i-1,j+1,k)+co0(i,j+1,k)-(co0(i-1,j,k)+co0(i,j,k))) * 0.5d0
    pm14 = (co0(i,j,k)+co0(i,j-1,k)-(co0(i-1,j,k)+co0(i-1,j-1,k))) * 0.5d0
    pm34 = (co0(i,j,k)+co0(i-1,j,k)-(co0(i-1,j-1,k)+co0(i,j-1,k))) * 0.5d0
    pm1 = (pm12-pm14)*0.5d0 + (pm13-pm14)*0.5d0 + &
         (pm11+pm14-pm12-pm13)*0.25d0 + pm14
    pm3 = (pm32-pm34)*0.5d0 + (pm33-pm34)*0.5d0 + &
         (pm31+pm34-pm32-pm33)*0.25d0 + pm34
    !-------------------------------------------------------------------------------
    !For small components
    !-------------------------------------------------------------------------------
    if (abs(pm1) < eps0) then
       pm1 = 0.0d0
    end if
    if (abs(pm3) < eps0) then
       pm3 = 0.0d0
    end if
    !-------------------------------------------------------------------------------
    !For small norms of the vector
    !-------------------------------------------------------------------------------
    if (sqrt(pm1*pm1+pm3*pm3) < eps1) then
       pm1 = 0.0d0
       pm3 = 1.0d0
    end if
    !-------------------------------------------------------------------------------
    !Normalization of the vector
    !-------------------------------------------------------------------------------
    inorme_pm=1.d0/sqrt(pm1*pm1+pm3*pm3)
    xm1 = pm1*inorme_pm
    xm3 = pm3*inorme_pm
    !-------------------------------------------------------------------------------
    !Reorientation of the vector
    !-------------------------------------------------------------------------------
    xm1 = -xm1
    xm3 = -xm3
    !-------------------------------------------------------------------------------
    !If the vector if almost a unatary vector
    !-------------------------------------------------------------------------------
    if (co0(i,j,k) >= 1.d0) then
       xm1 = 0.0d0
       xm3 = 1.0d0
       return
    end if
    if ((abs(abs(xm1)-1.d0)) < eps0) then
       if (xm1 < 0.d0) then
          xm1 = -1.0d0
       else
          xm1 = 1.0d0
       end if
       xm3 = 0.0d0
    end if
    if ((abs(abs(xm3)-1.d0)) < eps0) then
       xm1 = 0.0d0
       if (xm3 < 0.d0) then
          xm3 = -1.0d0
       else
          xm3 = 1.0d0
       end if
    end if

    return

  end subroutine normaloc
  !******************************************************************************

  !*******************************************************************************
  subroutine rangexm(co0,xm1,xm3,c1,c3,f1,icas,ixr)
    !*******************************************************************************
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    integer, intent(out)   :: icas,ixr
    real(8), intent(in)    :: co0
    real(8), intent(inout) :: xm1,xm3
    real(8), intent(out)   :: c1,c3,f1
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    real(8)                :: a0 = 0.0d0,b0 = 0.0d0
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    if ((xm1>0.0d0) .and. (xm3>0.0d0)) then
       f1 = co0
       a0 = xm1
       b0 = xm3
       icas = 1
       ixr = 1
    elseif ((xm1>0.0d0) .and. (xm3<0.0d0)) then
       f1 = co0
       a0 = xm1
       b0 = -xm3
       icas = 1
       ixr = 3
    elseif ((xm1<0.0d0) .and. (xm3>0.0d0)) then
       f1 = co0
       a0 = -xm1
       b0 = xm3
       icas = 1
       ixr = 2
    elseif ((xm1<0.0d0) .and. (xm3<0.0d0)) then
       f1 = 1.0d0 - co0
       a0 = -xm1
       b0 = -xm3
       icas = 1
       ixr = 4
    elseif ((xm1>0.0d0) .and. (xm3==0.0d0)) then
       f1 = co0
       a0 = 1.d0
       b0 = 0.0d0
       icas = 2
       ixr = 1
    elseif ((xm1<0.0d0) .and. (xm3==0.0d0)) then
       f1 = 1.0d0 - co0
       a0 = 1.d0
       b0 = 0.0d0
       icas = 2
       ixr = 4
    elseif ((xm1==0.0d0) .and. (xm3>0.0d0)) then
       f1 = co0
       a0 = 0.0d0
       b0 = 1.d0
       icas = 2
       ixr = 1
    elseif ((xm1==0.0d0) .and. (xm3<0.0d0)) then
       f1 = 1.0d0 - co0
       a0 = 0.0d0
       b0 = 1.d0
       icas = 2
       ixr = 4
    end if
    if (a0 >= b0) then
       xm1 = b0
       xm3 = a0
       c1 = 1.d0
       c3 = 1.d0
    else
       xm1 = a0
       xm3 = b0
       c1 = 1.d0
       c3 = 1.d0
    end if

    return
    
  end subroutine rangexm
  !*******************************************************************************

  !******************************************************************************
  subroutine cflvof_2d3d(ut,vt,wt,cou,dtcfl,dtrest,nbcfl)
    !*******************************************************************************
    use mod_Parameters, only: sx,ex,sy,ey,sz,ez, &
         & VOF_cfl,dt 
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    integer, intent(out)                               :: nbcfl
    real(8), intent(out)                               :: dtcfl,dtrest
    real(8), dimension(:,:,:), allocatable, intent(in) :: cou,ut,vt,wt
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    integer                                            :: i,j,k
    real(8)                                            :: umax,gumax
    real(8), parameter                                 :: epsc=1d-20
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    umax=0
    do k=sz,ez
       do j=sy,ey
          do i=sx,ex
             !-------------------------------------------------------------------------------
             if (   (abs(cou(i,j,k)-cou(i-1,j,k))>1d-14) .or. &
                  & (abs(cou(i,j,k)-cou(i+1,j,k))>1d-14) .or. &
                  & (abs(cou(i,j,k)-cou(i,j-1,k))>1d-14) .or. &
                  & (abs(cou(i,j,k)-cou(i,j+1,k))>1d-14) ) then
                umax = max(umax,abs(ut(i,j,k)),abs(ut(i+1,j,k)), &
                     &          abs(vt(i,j,k)),abs(vt(i,j+1,k)))
             end if
             !-------------------------------------------------------------------------------
             if (dim==3) then
                if (   (abs(cou(i,j,k)-cou(i,j,k-1))>1d-14) .or. &
                     & (abs(cou(i,j,k)-cou(i,j,k+1))>1d-14) ) then
                   umax = max(umax,abs(wt(i,j,k)),abs(wt(i,j,k+1)))
                end if
             end if
             !-------------------------------------------------------------------------------
          end do
       end do
    end do
    !-------------------------------------------------------------------------------
    if (nproc>1) then
       call mpi_allreduce(umax,gumax,1,mpi_double_precision,mpi_max,comm3d,code)
       umax=gumax
    end if
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Time step with cfl = 0.5
    !-------------------------------------------------------------------------------
    if (umax>epsc) then
       dtcfl = VOF_cfl / umax
    else
       dtcfl = dt
    end if
    !-------------------------------------------------------------------------------
    if (dtcfl >= dt) then
       dtcfl  = dt
       nbcfl  = 1
       dtrest = 0
    else
       nbcfl  = int(dt/dtcfl)
       dtcfl  = dt/(nbcfl+1)
       dtrest = dt-nbcfl*dtcfl
    end if
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------
    if (nbcfl>6) then
       if (rank==0) then
          write(*,*) "VOF CFL is too high, please consider reducing the time step"
          write(*,*) "VOF CFL is too high, please consider reducing the time step"
          write(*,*) "VOF CFL is too high, please consider reducing the time step"
          write(*,*) "VOF CFL is too high, please consider reducing the time step"
          write(*,*) "VOF CFL is too high, please consider reducing the time step"
       end if
       call finalization_mpi
       stop
    end if
    !-------------------------------------------------------------------------------

    return
  end subroutine cflvof_2d3d
  !******************************************************************************
 
  !*****************************************************************************************************
  subroutine VOF_time(cou,cou0,nt)
    !*****************************************************************************************************
    !-------------------------------------------------------------------------------
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)                                   :: nt
    real(8), dimension(:,:,:), allocatable, intent(inout) :: cou,cou0
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------

    cou0=cou 

    !*****************************************************************************************************
  end subroutine VOF_time
  !*****************************************************************************************************

  !*******************************************************************************
  subroutine VOF_print (nt,sca)
    !*******************************************************************************
    use mod_Parameters, only: dim, &
         & rank,VOF_method,        &
         & sx,ex,sy,ey,sz,ez
    use mod_CoeffVS
    !-------------------------------------------------------------------------------
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)                                :: nt
    real(8), dimension(:,:,:), allocatable, intent(in) :: sca
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                            :: i,j,k,l
    real(8)                                            :: maxsca,minsca,int
    real(8)                                            :: lmaxsca,lminsca,lint
    logical, save                                      :: once=.true.
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------
    
    !-------------------------------------------------------------------------------
    ! Maximum values
    !-------------------------------------------------------------------------------
    lmaxsca=maxval(sca(sx:ex,sy:ey,sz:ez))
    lminsca=minval(sca(sx:ex,sy:ey,sz:ez))
    !-------------------------------------------------------------------------------
    call mpi_allreduce(lmaxsca,maxsca,1,mpi_double_precision,mpi_max,comm3d,code)
    call mpi_allreduce(lminsca,minsca,1,mpi_double_precision,mpi_min,comm3d,code)
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Integral value
    !-------------------------------------------------------------------------------
    if (once) then
       call InitCoeffVS
       once=.false.
    end if
    !-------------------------------------------------------------------------------
    lint=0
    if (dim==2) then
       do j=sy,ey
          do i=sx,ex
             lint=lint+sca(i,j,1)*dx(i)*dy(j)*coeffV(i,j,1)
          end do
       end do
    elseif (dim==3) then
       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                lint=lint+sca(i,j,k)*dx(i)*dy(j)*dz(k)*coeffV(i,j,k)
             end do
          end do
       end do
    end if
    !-------------------------------------------------------------------------------
    call mpi_allreduce(lint,int,1,mpi_double_precision,mpi_sum,comm3d,code)
    !-------------------------------------------------------------------------------

    if (rank==0) then
       select case (VOF_method)
       case(1)
          write(*,'("VOF-plic iterations:",1x,i5)') iter_VOF
       case(2) 
          write(*,'("VOF-Conservative iterations:",1x,i5)') iter_VOF
       end select
       write(*,102) 'IntV(cou)=',int, &
            & '| Max(|cou|)=',maxsca,  &
            & '| Min(|cou|)=',minsca
    end if
    !-------------------------------------------------------------------------------
    
101 format(a,1pe15.8)
102 format(999(a,1pe12.5,1x))


    !*****************************************************************************************************
  end subroutine VOF_print
  !*****************************************************************************************************


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
end module mod_VOF_2D3D
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
