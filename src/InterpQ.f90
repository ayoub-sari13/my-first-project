
!========================================================================
!
!                   FUGU Version 1.0.0
!
!========================================================================
!
! Copyright  St�phane Vincent
!
! stephane.vincent@u-pem.fr          straightparadigm@gmail.com
!
! This file is part of the FUGU Fortran library, a set of Computational 
! Fluid Dynamics subroutines devoted to the simulation of multi-scale
! multi-phase flows for resolved scale interfaces (liquid/gas/solid). 
! FUGU uses the initial developments of DyJeAT and SHALA codes from 
! Jean-Luc Estivalezes (jean-luc.estivalezes@onera.fr) and 
! Fr�d�ric Couderc (couderc@math.univ-toulouse.fr).
!
!========================================================================
!**
!**   NAME        : InterpQ.f90
!**
!**   AUTHOR      : St�phane Vincent
!**                 Beno�t Trouette
!**
!**   FUNCTION    : Quadratic interpolation routines on velocity, pressure or
!!*                 rot grids.
!**
!**   DATES       : Version 1.0.0  : from : june, 16, 2015
!**
!**   SUBROUTINES : - subroutine interp2D_uvw
!**                 - subroutine interp2D_phi
!**                 - subroutine interp2D_rot
!**                 - subroutine interp3D_uvw
!**                 - subroutine interp3D_phi
!**                 - subroutine interp3D_rot
!========================================================================

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
MODULE mod_Interp
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  use mod_Parameters, only: regular_mesh, mesh

contains

  subroutine interp2D_uvw(u,v,xyz,vp)
    implicit none  
    !***************************************************************************************
    !Global variables
    !***************************************************************************************
    real(8), allocatable, dimension(:,:,:), intent(in) :: u,v
    real(8), dimension(:), intent(in)                  :: xyz
    real(8), dimension(:), intent(inout)               :: vp
    !***************************************************************************************
    !Local variables
    !***************************************************************************************
    integer                                            :: i,j,ii,jj
    real(8), dimension(2)                              :: qv
    !***************************************************************************************
    !-----------------------Bilinear interpolation----------------------------------------
    !----------------------------x velocity----------------------------------------------- 
    if (regular_mesh) then
       i=floor((xyz(1)-(mesh%xmin-mesh%dxu2(mesh%sx)))/mesh%dxu(mesh%sx))
       j=floor((xyz(2)-mesh%ymin)/mesh%dy(mesh%sy))
    else
       do ii=lbound(mesh%xu,1),ubound(mesh%xu,1)-1
          if (xyz(1)>=mesh%xu(ii).and.xyz(1)<mesh%xu(ii+1)) then
             i=ii
             exit
          end if
       end do
       do jj=lbound(mesh%y,1),ubound(mesh%y,1)-1
          if (xyz(2)>=mesh%y(jj).and.xyz(2)<mesh%y(jj+1)) then
             j=jj
             exit
          end if
       end do
    end if
    qv(1)=(xyz(1)-mesh%xu(i))/mesh%dx(i)
    qv(2)=(xyz(2)-mesh%y(j))/mesh%dyv(j+1)
    !----------------------------Bilinear interpolation------------------------------------
    vp(1)=   u(i,j,1)*(1-qv(1))*(1-qv(2)) &
         & + u(i+1,j,1)*qv(1)*(1-qv(2))   &
         & + u(i,j+1,1)*qv(2)*(1-qv(1))   &
         & + u(i+1,j+1,1)*qv(1)*qv(2)
    !----------------------------y velocity----------------------------------------------- 
    if (regular_mesh) then
       i=floor((xyz(1)-mesh%xmin)/mesh%dx(mesh%sx))
       j=floor((xyz(2)-(mesh%ymin-mesh%dyv2(mesh%sy)))/mesh%dyv(mesh%sy))
    else
       do ii=lbound(mesh%x,1),ubound(mesh%x,1)-1
          if (xyz(1)>=mesh%x(ii).and.xyz(1)<mesh%x(ii+1)) then
             i=ii
             exit
          end if
       end do
       do jj=lbound(mesh%yv,1),ubound(mesh%yv,1)-1
          if (xyz(2)>=mesh%yv(jj).and.xyz(2)<mesh%yv(jj+1)) then
             j=jj
             exit
          end if
       end do
    end if
    qv(1)=(xyz(1)-mesh%x(i))/mesh%dxu(i+1)
    qv(2)=(xyz(2)-mesh%yv(j))/mesh%dy(j)
    !----------------------------Bilinear interpolation------------------------------------
    vp(2)=   v(i,j,1)*(1-qv(1))*(1-qv(2)) &
         & + v(i+1,j,1)*qv(1)*(1-qv(2))   &
         & + v(i,j+1,1)*qv(2)*(1-qv(1))   &
         & + v(i+1,j+1,1)*qv(1)*qv(2)
  end subroutine interp2D_uvw

  subroutine interp2D_phi(var,xyz,varp)
    !***************************************************************************************
    implicit none  
    !***************************************************************************************
    !Global variables
    !***************************************************************************************
    real(8), allocatable, dimension(:,:,:), intent(in) :: var
    real(8), dimension(:), intent(in)                  :: xyz
    real(8), intent(inout)                             :: varp
    !***************************************************************************************
    !Local variables
    !***************************************************************************************
    integer                                            :: i,j,ii,jj
    real(8), dimension(2)                              :: qvar
    !***************************************************************************************
    !-----------------------Bilinear interpolation-----------------------------------------
    if (regular_mesh) then
       i=floor((xyz(1)-mesh%xmin)/mesh%dx(mesh%sx))
       j=floor((xyz(2)-mesh%ymin)/mesh%dy(mesh%sy))
    else
       do ii=lbound(mesh%x,1),ubound(mesh%x,1)-1
          if (xyz(1)>=mesh%x(ii).and.xyz(1)<mesh%x(ii+1)) then
             i=ii
             exit
          end if
       end do
       do jj=lbound(mesh%y,1),ubound(mesh%y,1)-1
          if (xyz(2)>=mesh%y(jj).and.xyz(2)<mesh%y(jj+1)) then
             j=jj
             exit
          end if
       end do
    end if
    qvar(1)=(xyz(1)-mesh%x(i))/mesh%dxu(i+1) 
    qvar(2)=(xyz(2)-mesh%y(j))/mesh%dyv(j+1)
    !----------------------------Bilinear interpolation------------------------------------
    varp=    var(i,j,1)*(1-qvar(1))*(1-qvar(2)) &
         & + var(i+1,j,1)*qvar(1)*(1-qvar(2))   &
         & + var(i,j+1,1)*qvar(2)*(1-qvar(1))   &
         & + var(i+1,j+1,1)*qvar(1)*qvar(2)
  end subroutine interp2D_phi

  subroutine interp2D_rot(rotz,xyz,rotp)
    !***************************************************************************************
    implicit none  
    !***************************************************************************************
    !Global variables
    !***************************************************************************************
    real(8), allocatable, dimension(:,:,:), intent(in) :: rotz
    real(8), dimension(:), intent(in)                  :: xyz
    real(8), dimension(3), intent(inout)               :: rotp
    !***************************************************************************************
    !Local variables
    !***************************************************************************************
    integer                                            :: i,j,ii,jj
    real(8), dimension(2)                              :: qv
    !***************************************************************************************
    !-----------------------Bilinear interpolation-----------------------------------------
    !----------------------------rot z ---------------------------------------------------- 
    if (regular_mesh) then
       i=floor((xyz(1)-(mesh%xmin-mesh%dxu2(mesh%sx)))/mesh%dxu(mesh%sx))
       j=floor((xyz(2)-(mesh%ymin-mesh%dyv2(mesh%sy)))/mesh%dyv(mesh%sy))
    else
       do ii=lbound(mesh%xu,1),ubound(mesh%xu,1)-1
          if (xyz(1)>=mesh%xu(ii).and.xyz(1)<mesh%xu(ii+1)) then
             i=ii
             exit
          end if
       end do
       do jj=lbound(mesh%yv,1),ubound(mesh%yv,1)-1
          if (xyz(2)>=mesh%yv(jj).and.xyz(2)<mesh%yv(jj+1)) then
             j=jj
             exit
          end if
       end do
    end if
    qv(1)=(xyz(1)-mesh%xu(i))/mesh%dx(i)
    qv(2)=(xyz(2)-mesh%yv(j))/mesh%dy(j)
    !----------------------------Bilinear interpolation------------------------------------
    rotp=0
    rotp(3)= rotz(i,j,1)*(1-qv(1))*(1-qv(2)) &
         & + rotz(i+1,j,1)*qv(1)*(1-qv(2))   &
         & + rotz(i,j+1,1)*qv(2)*(1-qv(1))   &
         & + rotz(i+1,j+1,1)*qv(1)*qv(2)
  end subroutine interp2D_rot


  subroutine interp3D_uvw(u,v,w,xyz,vp)
    !***************************************************************************************
    ! http://paulbourke.net/miscellaneous/interpolation/
    implicit none  
    !***************************************************************************************
    !Global variables
    !***************************************************************************************
    real(8), allocatable, dimension(:,:,:), intent(in) :: u,v,w
    real(8), dimension(:), intent(in)                  :: xyz
    real(8), dimension(:), intent(inout)               :: vp
    !***************************************************************************************
    !Local variables
    !***************************************************************************************
    integer                                            :: i,j,k,ii,jj,kk
    real(8), dimension(3)                              :: qv
    !***************************************************************************************
    !----------------------------x velocity----------------------------------------------- 
    if (regular_mesh) then
       i=floor((xyz(1)-(mesh%xmin-mesh%dxu2(mesh%sx)))/mesh%dxu(mesh%sx))
       j=floor((xyz(2)-mesh%ymin)/mesh%dy(mesh%sy))
       k=floor((xyz(3)-mesh%zmin)/mesh%dz(mesh%sz))
    else
       do ii=lbound(mesh%xu,1),ubound(mesh%xu,1)-1
          if (xyz(1)>=mesh%xu(ii).and.xyz(1)<mesh%xu(ii+1)) then
             i=ii
             exit
          end if
       end do
       do jj=lbound(mesh%y,1),ubound(mesh%y,1)-1
          if (xyz(2)>=mesh%y(jj).and.xyz(2)<mesh%y(jj+1)) then
             j=jj
             exit
          end if
       end do
       do kk=lbound(mesh%z,1),ubound(mesh%z,1)-1
          if (xyz(3)>=mesh%z(kk).and.xyz(3)<mesh%z(kk+1)) then
             k=kk
             exit
          end if
       end do
    end if
    qv(1)=(xyz(1)-mesh%xu(i))/mesh%dx(i) 
    qv(2)=(xyz(2)-mesh%y(j))/mesh%dyv(j+1)
    qv(3)=(xyz(3)-mesh%z(k))/mesh%dzw(k+1)
    !-----------------------Trilinear interpolation---------------------------------------
    vp(1)  = u(i,j,k)*(1-qv(1))*(1-qv(2))*(1-qv(3)) &
         & + u(i+1,j,k)*qv(1)*(1-qv(2))*(1-qv(3))   &
         & + u(i,j+1,k)*(1-qv(1))*qv(2)*(1-qv(3))   &
         & + u(i,j,k+1)*(1-qv(1))*(1-qv(2))*qv(3)   &
         & + u(i+1,j,k+1)*qv(1)*(1-qv(2))*qv(3)     &
         & + u(i,j+1,k+1)*(1-qv(1))*qv(2)*qv(3)     &
         & + u(i+1,j+1,k)*qv(1)*qv(2)*(1-qv(3))     &
         & + u(i+1,j+1,k+1)*qv(1)*qv(2)*qv(3)
    !----------------------------y velocity-----------------------------------------------
    if (regular_mesh) then 
       i=floor((xyz(1)-mesh%xmin)/mesh%dx(mesh%sx))
       j=floor((xyz(2)-(mesh%ymin-mesh%dyv2(mesh%sy)))/mesh%dyv(mesh%sy))
       k=floor((xyz(3)-mesh%zmin)/mesh%dz(mesh%sz))
    else
       do ii=lbound(mesh%x,1),ubound(mesh%x,1)-1
          if (xyz(1)>=mesh%x(ii).and.xyz(1)<mesh%x(ii+1)) then
             i=ii
             exit
          end if
       end do
       do jj=lbound(mesh%yv,1),ubound(mesh%yv,1)-1
          if (xyz(2)>=mesh%yv(jj).and.xyz(2)<mesh%yv(jj+1)) then
             j=jj
             exit
          end if
       end do
       do kk=lbound(mesh%z,1),ubound(mesh%z,1)-1
          if (xyz(3)>=mesh%z(kk).and.xyz(3)<mesh%z(kk+1)) then
             k=kk
             exit
          end if
       end do
    end if
    qv(1)=(xyz(1)-mesh%x(i))/mesh%dxu(i+1) 
    qv(2)=(xyz(2)-mesh%yv(j))/mesh%dy(j)
    qv(3)=(xyz(3)-mesh%z(k))/mesh%dzw(k+1)
    !-----------------------Trilinear interpolation---------------------------------------
    vp(2)  = v(i,j,k)*(1-qv(1))*(1-qv(2))*(1-qv(3)) &
         & + v(i+1,j,k)*qv(1)*(1-qv(2))*(1-qv(3))   &
         & + v(i,j+1,k)*(1-qv(1))*qv(2)*(1-qv(3))   &
         & + v(i,j,k+1)*(1-qv(1))*(1-qv(2))*qv(3)   &
         & + v(i+1,j,k+1)*qv(1)*(1-qv(2))*qv(3)     &
         & + v(i,j+1,k+1)*(1-qv(1))*qv(2)*qv(3)     &
         & + v(i+1,j+1,k)*qv(1)*qv(2)*(1-qv(3))     &
         & + v(i+1,j+1,k+1)*qv(1)*qv(2)*qv(3)
    !----------------------------z velocity----------------------------------------------- 
    if (regular_mesh) then
       i=floor((xyz(1)-mesh%xmin)/mesh%dx(mesh%sx))
       j=floor((xyz(2)-mesh%ymin)/mesh%dy(mesh%sy))
       k=floor((xyz(3)-(mesh%zmin-mesh%dzw2(mesh%sz)))/mesh%dzw(mesh%sz))
    else
       do ii=lbound(mesh%x,1),ubound(mesh%x,1)-1
          if (xyz(1)>=mesh%x(ii).and.xyz(1)<mesh%x(ii+1)) then
             i=ii
             exit
          end if
       end do
       do jj=lbound(mesh%y,1),ubound(mesh%y,1)-1
          if (xyz(2)>=mesh%y(jj).and.xyz(2)<mesh%y(jj+1)) then
             j=jj
             exit
          end if
       end do
       do kk=lbound(mesh%z,1),ubound(mesh%z,1)-1
          if (xyz(3)>=mesh%zw(kk).and.xyz(3)<mesh%zw(kk+1)) then
             k=kk
             exit
          end if
       end do
    end if
    qv(1)=(xyz(1)-mesh%x(i))/mesh%dxu(i+1)
    qv(2)=(xyz(2)-mesh%y(j))/mesh%dyv(j+1)
    qv(3)=(xyz(3)-mesh%zw(k))/mesh%dz(k)
    !-----------------------Trilinear interpolation---------------------------------------
    vp(3)  = w(i,j,k)*(1-qv(1))*(1-qv(2))*(1-qv(3)) &
         & + w(i+1,j,k)*qv(1)*(1-qv(2))*(1-qv(3))   &
         & + w(i,j+1,k)*(1-qv(1))*qv(2)*(1-qv(3))   &
         & + w(i,j,k+1)*(1-qv(1))*(1-qv(2))*qv(3)   &
         & + w(i+1,j,k+1)*qv(1)*(1-qv(2))*qv(3)     &
         & + w(i,j+1,k+1)*(1-qv(1))*qv(2)*qv(3)     &
         & + w(i+1,j+1,k)*qv(1)*qv(2)*(1-qv(3))     &
         & + w(i+1,j+1,k+1)*qv(1)*qv(2)*qv(3)
  end subroutine interp3D_uvw

  subroutine interp3D_phi(var,xyz,varp)
    !***************************************************************************************
    implicit none  
    !***************************************************************************************
    !Global variables
    !***************************************************************************************
    real(8), allocatable, dimension(:,:,:), intent(in) :: var
    real(8), dimension(:), intent(in)                  :: xyz
    real(8), intent(inout)                             :: varp
    !***************************************************************************************
    !Local variables
    !***************************************************************************************
    integer                                            :: i,j,k,ii,jj,kk
    real(8), dimension(3)                              :: qv
    !***************************************************************************************
    if (regular_mesh) then
       i=floor((xyz(1)-mesh%xmin)/mesh%dx(mesh%sx))
       j=floor((xyz(2)-mesh%ymin)/mesh%dy(mesh%sy))
       k=floor((xyz(3)-mesh%zmin)/mesh%dz(mesh%sz))
    else
       do ii=lbound(mesh%x,1),ubound(mesh%x,1)-1
          if (xyz(1)>=mesh%x(ii).and.xyz(1)<mesh%x(ii+1)) then
             i=ii
             exit
          end if
       end do
       do jj=lbound(mesh%y,1),ubound(mesh%y,1)-1
          if (xyz(2)>=mesh%y(jj).and.xyz(2)<mesh%y(jj+1)) then
             j=jj
             exit
          end if
       end do
       do kk=lbound(mesh%z,1),ubound(mesh%z,1)-1
          if (xyz(3)>=mesh%z(kk).and.xyz(3)<mesh%z(kk+1)) then
             k=kk
             exit
          end if
       end do
    end if
    qv(1)=(xyz(1)-mesh%x(i))/mesh%dxu(i+1) 
    qv(2)=(xyz(2)-mesh%y(j))/mesh%dyv(j+1)
    qv(3)=(xyz(3)-mesh%z(k))/mesh%dzw(k+1)
    !----------------------------Trilinear interpolation------------------------------------
    varp   = var(i,j,k)*(1-qv(1))*(1-qv(2))*(1-qv(3)) &
         & + var(i+1,j,k)*qv(1)*(1-qv(2))*(1-qv(3))   &
         & + var(i,j+1,k)*(1-qv(1))*qv(2)*(1-qv(3))   &
         & + var(i,j,k+1)*(1-qv(1))*(1-qv(2))*qv(3)   &
         & + var(i+1,j,k+1)*qv(1)*(1-qv(2))*qv(3)     &
         & + var(i,j+1,k+1)*(1-qv(1))*qv(2)*qv(3)     &
         & + var(i+1,j+1,k)*qv(1)*qv(2)*(1-qv(3))     &
         & + var(i+1,j+1,k+1)*qv(1)*qv(2)*qv(3)
  end subroutine interp3D_phi
  
  subroutine interp3D_rot(rotx,roty,rotz,xyz,rotp)
    !***************************************************************************************
    IMPLICIT NONE  
    !***************************************************************************************
    !Global variables
    !***************************************************************************************
    real(8), allocatable, dimension(:,:,:), intent(in) :: rotx,roty,rotz
    real(8), dimension(:), intent(in)                  :: xyz
    real(8), dimension(:), intent(inout)               :: rotp
    !***************************************************************************************
    !Local variables
    !***************************************************************************************
    integer                                            :: i,j,k,ii,jj,kk
    real(8), dimension(3)                              :: qv
    !***************************************************************************************
    !----------------------------x component----------------------------------------------- 
    if (regular_mesh) then
       i=floor((xyz(1)-mesh%xmin)/mesh%dx(i))
       j=floor((xyz(2)-(mesh%ymin-mesh%dyv2(j)))/mesh%dy(j))
       k=floor((xyz(3)-(mesh%zmin-mesh%dzw2(k)))/mesh%dz(k))
    else
       do ii=lbound(mesh%x,1),ubound(mesh%x,1)-1
          if (xyz(1)>=mesh%x(ii).and.xyz(1)<mesh%x(ii+1)) then
             i=ii
             exit
          end if
       end do
       do jj=lbound(mesh%yv,1),ubound(mesh%yv,1)-1
          if (xyz(2)>=mesh%yv(jj).and.xyz(2)<mesh%yv(jj+1)) then
             j=jj
             exit
          end if
       end do
       do kk=lbound(mesh%zw,1),ubound(mesh%zw,1)-1
          if (xyz(3)>=mesh%zw(kk).and.xyz(3)<mesh%zw(kk+1)) then
             k=kk
             exit
          end if
       end do
    end if
    qv(1)=(xyz(1)-mesh%x(i))/mesh%dxu(i+1)
    qv(2)=(xyz(2)-mesh%yv(j))/mesh%dy(j)
    qv(3)=(xyz(3)-mesh%zw(k))/mesh%dz(k)
    !-----------------------Trilinear interpolation---------------------------------------
    rotp(1) = rotx(i,j,k)*(1-qv(1))*(1-qv(2))*(1-qv(3)) &
         & + rotx(i+1,j,k)*qv(1)*(1-qv(2))*(1-qv(3))   &
         & + rotx(i,j+1,k)*(1-qv(1))*qv(2)*(1-qv(3))   &
         & + rotx(i,j,k+1)*(1-qv(1))*(1-qv(2))*qv(3)   &
         & + rotx(i+1,j,k+1)*qv(1)*(1-qv(2))*qv(3)     &
         & + rotx(i,j+1,k+1)*(1-qv(1))*qv(2)*qv(3)     &
         & + rotx(i+1,j+1,k)*qv(1)*qv(2)*(1-qv(3))     &
         & + rotx(i+1,j+1,k+1)*qv(1)*qv(2)*qv(3)
    !----------------------------y component----------------------------------------------- 
    if (regular_mesh) then
       i=floor((xyz(1)-(mesh%xmin-mesh%dx(i)/2))/mesh%dx(i))
       j=floor((xyz(2)-mesh%ymin)/mesh%dy(j))
       k=floor((xyz(3)-(mesh%zmin-mesh%dz(k)/2))/mesh%dz(k))
    else
       do ii=lbound(mesh%xu,1),ubound(mesh%xu,1)-1
          if (xyz(1)>=mesh%xu(ii).and.xyz(1)<mesh%xu(ii+1)) then
             i=ii
             exit
          end if
       end do
       do jj=lbound(mesh%y,1),ubound(mesh%y,1)-1
          if (xyz(2)>=mesh%y(jj).and.xyz(2)<mesh%y(jj+1)) then
             j=jj
             exit
          end if
       end do
       do kk=lbound(mesh%zw,1),ubound(mesh%zw,1)-1
          if (xyz(3)>=mesh%zw(kk).and.xyz(3)<mesh%zw(kk+1)) then
             k=kk
             exit
          end if
       end do
    end if
    qv(1)=(xyz(1)-mesh%xu(i))/mesh%dx(i)
    qv(2)=(xyz(2)-mesh%y(j))/mesh%dyv(j+1)
    qv(3)=(xyz(3)-mesh%zw(k))/mesh%dz(k)
    !-----------------------Trilinear interpolation---------------------------------------
    rotp(2)= roty(i,j,k)*(1-qv(1))*(1-qv(2))*(1-qv(3)) &
         & + roty(i+1,j,k)*qv(1)*(1-qv(2))*(1-qv(3))   &
         & + roty(i,j+1,k)*(1-qv(1))*qv(2)*(1-qv(3))   &
         & + roty(i,j,k+1)*(1-qv(1))*(1-qv(2))*qv(3)   &
         & + roty(i+1,j,k+1)*qv(1)*(1-qv(2))*qv(3)     &
         & + roty(i,j+1,k+1)*(1-qv(1))*qv(2)*qv(3)     &
         & + roty(i+1,j+1,k)*qv(1)*qv(2)*(1-qv(3))     &
         & + roty(i+1,j+1,k+1)*qv(1)*qv(2)*qv(3)
    !----------------------------z component----------------------------------------------- 
    if (regular_mesh) then
       i=floor((xyz(1)-(mesh%xmin-mesh%dxu2(mesh%sx)))/mesh%dx(mesh%sx))
       j=floor((xyz(2)-(mesh%ymin-mesh%dyv2(mesh%sy)))/mesh%dy(mesh%sy))
       k=floor((xyz(3)-mesh%zmin)/mesh%dz(mesh%sz))
    else
       do ii=lbound(mesh%xu,1),ubound(mesh%xu,1)-1
          if (xyz(1)>=mesh%xu(ii).and.xyz(1)<mesh%xu(ii+1)) then
             i=ii
             exit
          end if
       end do
       do jj=lbound(mesh%yv,1),ubound(mesh%yv,1)-1
          if (xyz(2)>=mesh%yv(jj).and.xyz(2)<mesh%yv(jj+1)) then
             j=jj
             exit
          end if
       end do
       do kk=lbound(mesh%z,1),ubound(mesh%z,1)-1
          if (xyz(3)>=mesh%z(kk).and.xyz(3)<mesh%z(kk+1)) then
             k=kk
             exit
          end if
       end do
    end if
    qv(1)=(xyz(1)-mesh%xu(i))/mesh%dx(i)
    qv(2)=(xyz(2)-mesh%yv(j))/mesh%dy(j)
    qv(3)=(xyz(3)-mesh%z(k))/mesh%dzw(k+1)
    !-----------------------Trilinear interpolation---------------------------------------
    rotp(3)= rotz(i,j,k)*(1-qv(1))*(1-qv(2))*(1-qv(3)) &
         & + rotz(i+1,j,k)*qv(1)*(1-qv(2))*(1-qv(3))   &
         & + rotz(i,j+1,k)*(1-qv(1))*qv(2)*(1-qv(3))   &
         & + rotz(i,j,k+1)*(1-qv(1))*(1-qv(2))*qv(3)   &
         & + rotz(i+1,j,k+1)*qv(1)*(1-qv(2))*qv(3)     &
         & + rotz(i,j+1,k+1)*(1-qv(1))*qv(2)*qv(3)     &
         & + rotz(i+1,j+1,k)*qv(1)*qv(2)*(1-qv(3))     &
         & + rotz(i+1,j+1,k+1)*qv(1)*qv(2)*qv(3)
  end subroutine interp3D_rot

end MODULE mod_Interp
