
!###############################################################################
!
!                             modules de front tracking
!                                        Paul
!
!
!###############################################################################
!===============================================================================
module mod_front_tracking
  use mod_Parameters
  use mod_Constants
  use mod_struct_front_tracking
  use mod_math
contains
#if 0
  !---------------------------------------------------------------------------------
  subroutine front_tracking(u,v,u0,v0,pres,phase_u,phase_v,phase_Pres,cou,ft,nt,bul,Erreur)
    !-------------------------------------------------------------------------------
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:),allocatable,intent(in)    :: u,v,u0,v0,pres
    logical, dimension(:,:,:),allocatable,intent(inout) :: phase_u,phase_v,phase_Pres
    real(8),dimension(:,:,:),allocatable,intent(inout)  :: cou
    integer, intent(in)                                 :: nt
    type(struct_front_tracking),intent(inout)           :: ft
    type(bulle),dimension(:),allocatable,intent(inout)  :: bul
    real(8)                                             :: volume_tot
    real(8),intent(out)                                 :: Erreur
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    integer            :: ntt,nbb,nbcfl,l,n,i,j,k1
    real(8)            :: dv,dtcfl,dtrest
    real(8)            :: volume,alpha,lost_volume,volume_ini,t_sub
    real(8)            :: t1,t2,t_cpu
    character(LEN=100) :: NomFichier
    logical            :: old_conservation
    integer            :: conservation_volume
    character(len=200) :: filename
    integer            :: unit
    !-------------------------------------------------------------------------------
    !call cfl_2d(u,v,dtcfl,dtrest,nbcfl) !?????
!!$    if (dtrest /= 0.d0) then
!!$       nbb = nbcfl + 1
!!$    else
!!$       nbb = nbcfl
!!$    end if
    dv=dt
    nbb=1
    !-------------------------------------------------------------------------------
    !-----------------------------------time loop-----------------------------------
    !-------------------------------------------------------------------------------
    do ntt = 1,nbb
       !----------------------------------------------------------------------------
!!$       if (ntt <= nbcfl) then
!!$          dv = dtcfl
!!$       else
!!$          dv = dtrest
!!$       end if
       old_conservation=.true.
       conservation_volume=0
       if (ft%nbbul.gt.0) then
          !call volume_2d(bul,ft,nt)
          if (dim==2) then
             call newelement(bul,ft,lost_volume,nt,phase_u,phase_v,phase_Pres,cou)  
             call ft_trans2d(u,v,u0,v0,pres,bul,ft%nbbul,phase_u,phase_v,phase_Pres,cou,nt,Erreur)
             !call reference_RK4(ft,bul,nt)
          else 
             print*,'3D not done'
          end if

          if (conservation_volume==0) then
             !---------------------on ne fait rien----------------------------------------------------------------------------------
             write(11,*) 'no volume conservation'
          else if (conservation_volume==1) then
             call conservation_volume_2d(ft%nbbul,bul,alpha,phase_u,phase_v,phase_Pres,cou,lost_volume,nt)
          else if (conservation_volume==2) then
             call conservation_volume_vitesseprojete_2d(ft%nbbul,bul,volume,alpha,phase_u,phase_v,phase_Pres,cou,lost_volume,&
                  nt,old_conservation)
          else if (conservation_volume==3) then
             call conservation_volume_vitesseprojete_2d(ft%nbbul,bul,volume,alpha,phase_u,phase_v,phase_Pres,cou,lost_volume,&
                  nt)
          else
             print*,'enter a right number of volume conservation'
          end if
          !call coalesce_and_break_up(ft,bul)
       end if
    end do
  end subroutine front_tracking
  !***************************************************************************************
  !***************************************************************************************
  subroutine newelement(bul,ft,lost_volume,nt,phase_u,phase_v,phase_Pres,cou)
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    type(struct_front_tracking),intent(inout)           :: ft
    type(bulle),dimension(ft%nbbul),intent(inout)       :: bul
    real(8),intent(out)                                 :: lost_volume
    integer,intent(in)                                  :: nt
    logical, dimension(:,:,:),allocatable,intent(inout) :: phase_u,phase_v,phase_Pres
    real(8), dimension(:,:,:),allocatable,intent(inout) :: cou
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    integer                                   :: k,m,n,l,nbnewele,nbsmallele,nbsmallsom,nbnewsom,k1,k2
    integer                                   :: nbele_new,nbsom_new,num_ele_mask,num_som_mask
    integer                                   :: old_size,new_size
    real(8)                                   :: ds,work1,work2
    real(8)                                   :: sin_alpha,sin_beta,cos_alpha,cos_beta,alpha,beta
    type(sommet), dimension(:), allocatable   :: sommet_temp
    type(element), dimension(:), allocatable  :: element_temp
    real(8), dimension(dim)                   :: xyz1,xyz2,xyz_new,xyz_old,xyz1_vois1,xyz2_vois1,xyz1_vois2,xyz2_vois2
    real(8), dimension(dim)                   :: n_ele,n_ele1,n_ele2,v1,v2,v1_new,v2_new,xyz1_old,xyz2_old,vp1,vp2,vp
    real(8), dimension(dim)                   :: xyz1_vois1_old,xyz2_vois2_old 
    real(8)                                   :: t1,t2,t_cpu
    !-------------------------------------------------------------------------------
    call cpu_time(time=t1)
    work1=ft%ftdmin*ft%ftdmin
    work2=ft%ftdmax*ft%ftdmax

    do l=1,ft%nbbul
       if ( bul(l)%mask ) then
          nbsmallele=0 
          nbsmallsom=0
          nbnewele=0
          nbnewsom=0
          lost_volume=0
          !----------------------------------------------
          do m=1,bul(l)%nbele

             if (bul(l)%ele(m)%mask) then
                xyz1=bul(l)%som( bul(l)%ele(m)%nusom(1) )%xyz
                xyz2=bul(l)%som( bul(l)%ele(m)%nusom(2) )%xyz

                xyz1_old=bul(l)%som( bul(l)%ele(m)%nusom(1) )%xyz_old
                xyz2_old=bul(l)%som( bul(l)%ele(m)%nusom(2) )%xyz_old

                vp1=bul(l)%som( bul(l)%ele(m)%nusom(1) )%vp
                vp2=bul(l)%som( bul(l)%ele(m)%nusom(2) )%vp

                ds= (xyz2(1)-xyz1(1))*(xyz2(1)-xyz1(1)) + (xyz2(2)-xyz1(2))*(xyz2(2)-xyz1(2))    

                if (ds>work2) then
                   nbnewele=nbnewele+1
                   nbnewsom=nbnewsom+1
                   bul(l)%ele(m)%enrichir=.true.
                else
                   bul(l)%ele(m)%enrichir=.false.
                end if
                !---------------On desenrichit-------------------------------
                if ( ds<work1 .and. bul(l)%nbele_vrai>3 .and. &
                     bul(l)%som(bul(l)%ele(m)%nusom(1))%desenrichissement .and. &
                     bul(l)%som(bul(l)%ele(m)%nusom(2))%desenrichissement .and. &
                     bul(l)%som(bul(l)%ele(m)%nusom(2))%notfix ) then
                   !-----------------Normal calculation----------------------------------
                   n_ele(1)=+(xyz2(2)-xyz1(2))
                   n_ele(2)=-(xyz2(1)-xyz1(1))
                   n_ele=n_ele/sqrt(dot_product(n_ele,n_ele))

                   k1=bul(l)%ele(m)%ele_vois(1)
                   xyz1_vois1=bul(l)%som(bul(l)%ele(k1)%nusom(1))%xyz
                   xyz2_vois1=bul(l)%som(bul(l)%ele(k1)%nusom(2))%xyz

                   xyz1_vois1_old=bul(l)%som(bul(l)%ele(k1)%nusom(1))%xyz_old

                   n_ele1(1)=+(xyz2_vois1(2)-xyz1_vois1(2))
                   n_ele1(2)=-(xyz2_vois1(1)-xyz1_vois1(1))
                   n_ele1=n_ele1/sqrt(dot_product(n_ele1,n_ele1))

                   k2=bul(l)%ele(m)%ele_vois(2)
                   xyz1_vois2=bul(l)%som(bul(l)%ele(k2)%nusom(1))%xyz
                   xyz2_vois2=bul(l)%som(bul(l)%ele(k2)%nusom(2))%xyz

                   xyz2_vois2_old=bul(l)%som(bul(l)%ele(k2)%nusom(2))%xyz_old

                   n_ele2(1)=+(xyz2_vois2(2)-xyz1_vois2(2))
                   n_ele2(2)=-(xyz2_vois2(1)-xyz1_vois2(1))
                   n_ele2=n_ele2/sqrt(dot_product(n_ele2,n_ele2))
                   !--------------------Angle calculation-------------------------------------
                   cos_alpha=dot_product(n_ele1,n_ele)
                   cos_beta =dot_product(n_ele,n_ele2)
                   !--------------------Angle comparison-----------------------------------
                   if (cos_alpha<sqrt(3._8)/2 .and. cos_beta<sqrt(3._8)/2) then
                      bul(l)%ele(m)%desenrichir=.false.
                   else
                      nbsmallele=nbsmallele+1
                      nbsmallsom=nbsmallsom+1

                      bul(l)%nbele_vrai=bul(l)%nbele_vrai-1
                      bul(l)%nbsom_vrai=bul(l)%nbsom_vrai-1

                      bul(l)%ele(m)%desenrichir=.true.
                      bul(l)%ele(m)%mask=.false. 

                      alpha=min(ft%theta_lim,acos(cos_alpha)) ; beta=min(ft%theta_lim,acos(cos_beta))

                      xyz_new=(xyz1*(beta-ft%theta_lim) +&
                           xyz2*(alpha-ft%theta_lim)&
                           ) / (alpha+beta-2*ft%theta_lim)

                      xyz_old=(xyz1_old*(beta-ft%theta_lim) +&
                           xyz2_old*(alpha-ft%theta_lim)&
                           ) / (alpha+beta-2*ft%theta_lim)

                      vp=(vp1*(beta-ft%theta_lim) +&
                           vp2*(alpha-ft%theta_lim)&
                           ) / (alpha+beta-2*ft%theta_lim)

                      bul(l)%som( bul(l)%ele(m)%nusom(2) )%mask=.false.
                      !-----------------------Lost volume calculation--------------------------
                      v1=xyz1_old-xyz1_vois1_old        ;     v2=xyz2_old-xyz2_vois2_old
                      v1_new=xyz_old-xyz1_vois1_old     ;     v2_new=xyz_old-xyz2_vois2_old 
                      lost_volume=lost_volume-&
                           (v1(1)*v1_new(2)-v1(2)*v1_new(1) + v2_new(1)*v2(2)-v2_new(2)*v2(1))
                      !------------------------------------------------------------------------
                      bul(l)%som( bul(l)%ele(m)%nusom(1) )%xyz=xyz_new
                      bul(l)%som( bul(l)%ele(m)%nusom(1) )%xyz_old=xyz_old
                      bul(l)%som( bul(l)%ele(m)%nusom(1) )%vp=vp

                      bul(l)%ele( k2 )%nusom(1)   =bul(l)%ele(m)%nusom(1)   

                      bul(l)%ele( k2 )%ele_vois(1)=k1
                      bul(l)%ele( k1 )%ele_vois(2)=k2 
                      !-----------------------local phase update-------------------------------
                      !------------------------------u phase-----------------------------------
                      !----------------------------first triangle------------------------------
                      call phase_update(xyz1_vois1,xyz_new,xyz1,phase_u,sx,sy,ex+1,ey,dx,dy,(xmin-dx/2),ymin,cou)
                      !---------------------------second triangle------------------------------
                      call phase_update(xyz2_vois2,xyz2,xyz_new,phase_u,sx,sy,ex+1,ey,dx,dy,(xmin-dx/2),ymin,cou)
                      !------------------------------v phase-----------------------------------
                      !----------------------------first triangle------------------------------
                      call phase_update(xyz1_vois1,xyz_new,xyz1,phase_v,sx,sy,ex,ey+1,dx,dy,xmin,(ymin-dy/2),cou)
                      !---------------------------second triangle------------------------------
                      call phase_update(xyz2_vois2,xyz2,xyz_new,phase_v,sx,sy,ex,ey+1,dx,dy,xmin,(ymin-dy/2),cou)
                      !------------------------------P phase-----------------------------------
                      !----------------------------first triangle------------------------------
                      call phase_update(xyz1_vois1,xyz_new,xyz1,phase_Pres,sx,sy,ex,ey,dx,dy,xmin,ymin,cou)
                      !---------------------------second triangle------------------------------
                      call phase_update(xyz2_vois2,xyz2,xyz_new,phase_Pres,sx,sy,ex,ey,dx,dy,xmin,ymin,cou)
                      !------------------------------------------------------------------------
                   end if
                else
                   bul(l)%ele(m)%desenrichir=.false.
                end if
                !------------------------------------------------------------------------------
                !-------------------------fin desenrichissement--------------------------------
             end if
          end do

          do m=bul(l)%nbele,1,-1 !!!  CALCUL DU NOMBRE D'ELEMENTS POTENTIEL
             if (bul(l)%ele(m)%mask) exit 
          end do
          bul(l)%nbele=m

          do m=bul(l)%nbsom,1,-1 !!!  CALCUL DU NOMBRE D'ELEMENTS POTENTIEL
             if (bul(l)%som(m)%mask) exit
          end do
          bul(l)%nbsom=m

          !print*,"*******************",nbsmallele,nbnewele
          !---------------------REDIMENSIONNEMENT DES TABLEAUX---------------------------------

          if (bul(l)%nbele_vrai+nbnewele>bul(l)%nbelemax) then

             old_size=bul(l)%nbele
             new_size=(bul(l)%nbele_vrai+nbnewele) * ft%multiplicateur_d_element
             allocate(element_temp(1:new_size))
             element_temp(1:old_size)=bul(l)%ele(1:old_size)
             element_temp(old_size+1:new_size)%mask=.false.

             call MOVE_ALLOC(FROM=element_temp,TO=bul(l)%ele)
             bul(l)%nbelemax=new_size

          end if

          if (bul(l)%nbsom_vrai+nbnewsom>bul(l)%nbsommax) then

             old_size=bul(l)%nbsom
             new_size=(bul(l)%nbsom_vrai+nbnewsom) * ft%multiplicateur_de_sommet
             allocate(sommet_temp(1:new_size))
             sommet_temp(1:old_size)=bul(l)%som(1:old_size)
             sommet_temp(old_size+1:new_size)%mask=.false.

             call MOVE_ALLOC(FROM=sommet_temp,TO=bul(l)%som)
             bul(l)%nbsommax=new_size

          end if

          if (nbnewele>0) then
             num_ele_mask=1
             num_som_mask=1

             do m=1,bul(l)%nbele
                if (bul(l)%ele(m)%mask.and.bul(l)%ele(m)%enrichir ) then
                   if ( bul(l)%som( bul(l)%ele(m)%nusom(1) )%notfix .and. &
                        bul(l)%som( bul(l)%ele(m)%nusom(2) )%notfix ) then
                      nbnewele=nbnewele-1
                      !---------------Enrichissement-----------------------------
                      bul(l)%nbele_vrai=bul(l)%nbele_vrai+1
                      bul(l)%nbsom_vrai=bul(l)%nbsom_vrai+1 !!!!ET LES SOMMETS

                      do while ( bul(l)%ele(num_ele_mask)%mask ) !Recherche du mask faux (element)
                         num_ele_mask=num_ele_mask+1
                      end do
                      bul(l)%nbele=max(bul(l)%nbele,num_ele_mask)

                      do while ( bul(l)%som(num_som_mask)%mask ) !Recherche du mask faux (sommet)
                         num_som_mask=num_som_mask+1
                      end do
                      bul(l)%nbsom=max(bul(l)%nbsom,num_som_mask)

                      xyz1=bul(l)%som( bul(l)%ele(m)%nusom(1) )%xyz
                      xyz2=bul(l)%som( bul(l)%ele(m)%nusom(2) )%xyz

                      xyz1_old=bul(l)%som( bul(l)%ele(m)%nusom(1) )%xyz_old
                      xyz2_old=bul(l)%som( bul(l)%ele(m)%nusom(2) )%xyz_old

                      vp1=bul(l)%som( bul(l)%ele(m)%nusom(1) )%vp
                      vp2=bul(l)%som( bul(l)%ele(m)%nusom(2) )%vp

                      bul(l)%som(num_som_mask)%mask=.true.
                      bul(l)%som(num_som_mask)%xyz=0.5d0*(xyz1+xyz2)

                      bul(l)%som(num_som_mask)%xyz_old=0.5d0*(xyz1_old+xyz2_old)

                      bul(l)%som(num_som_mask)%vp=0.5d0*(vp1+vp2)

                      bul(l)%som(num_som_mask)%compteur=ft%compteur_init
                      bul(l)%som(num_som_mask)%desenrichissement=.true.
                      bul(l)%som(num_som_mask)%notfix=.true.

                      bul(l)%ele(num_ele_mask)%mask=.true.
                      bul(l)%ele(num_ele_mask)%nusom(1)=num_som_mask
                      bul(l)%ele(num_ele_mask)%nusom(2)=bul(l)%ele(m)%nusom(2)
                      bul(l)%ele(m)%nusom(2)=num_som_mask

                      bul(l)%ele(num_ele_mask)%ele_vois(1)=m
                      bul(l)%ele(num_ele_mask)%ele_vois(2)=bul(l)%ele(m)%ele_vois(2)
                      bul(l)%ele(m)%ele_vois(2)=num_ele_mask
                      bul(l)%ele( bul(l)%ele(num_ele_mask)%ele_vois(2) )%ele_vois(1)=num_ele_mask
                      if (nbnewele==0) exit
                   end if
                end if
             end do
             
          end if

          if (bul(l)%nbele_vrai<bul(l)%nbele*ft%ratio_compact) call compact_ele(bul(l),ft)
          if (bul(l)%nbsom_vrai<bul(l)%nbsom*ft%ratio_compact) call compact_som(bul(l),ft)

       end if

    end do
    call cpu_time(time=t2)
    t_cpu=t2-t1
  end subroutine newelement
  !*******************************************************************************
  !*******************************************************************************
  subroutine compact_ele(bul,ft)
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    type(struct_front_tracking),intent(inout):: ft
    type(bulle),intent(inout)                :: bul
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    real(8),dimension(:),allocatable        ::ind_ele,ind_som
    type(element), dimension(:), allocatable:: ele_temp
    integer::n,nb_true
    !-------------------------------------------------------------------------------
    bul%nbelemax=bul%nbele_vrai*ft%multiplicateur_d_element
    allocate(ind_ele(bul%nbele))
    allocate(ele_temp(bul%nbelemax))
    ele_temp(bul%nbele_vrai+1:)%mask=.false.

    nb_true=0
    do n=1,bul%nbele
       if (bul%ele(n)%mask) then
          nb_true=nb_true+1
          ele_temp(nb_true)=bul%ele(n)
          ind_ele(n)=nb_true
       end if
    end do

    print*,"ele ",nb_true,bul%nbele_vrai

    nb_true=0
    do n=1,bul%nbele
       if (bul%ele(n)%mask) then
          nb_true=nb_true+1
          ele_temp(nb_true)%ele_vois(1)= ind_ele(  bul%ele(n)%ele_vois(1)  )
          ele_temp(nb_true)%ele_vois(2)= ind_ele(  bul%ele(n)%ele_vois(2)  )

       end if
    end do

    deallocate(ind_ele)
    call MOVE_ALLOC(FROM=ele_temp,TO=bul%ele)

    bul%nbele=bul%nbele_vrai

  end subroutine compact_ele
  !*******************************************************************************
  !*******************************************************************************
  subroutine compact_som(bul,ft)
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    type(struct_front_tracking),intent(inout):: ft
    type(bulle),intent(inout)                :: bul
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    real(8),dimension(:),allocatable        :: ind_ele,ind_som
    type(sommet), dimension(:), allocatable :: som_temp
    integer                                 :: n,nb_true
    !-------------------------------------------------------------------------------
    bul%nbsommax=bul%nbsom_vrai*ft%multiplicateur_de_sommet
    allocate(ind_som(bul%nbsom))
    allocate(som_temp(bul%nbsommax))
    som_temp(bul%nbsom_vrai+1:)%mask=.false.

    nb_true=0
    do n=1,bul%nbsom
       if (bul%som(n)%mask) then
          nb_true=nb_true+1
          som_temp(nb_true)=bul%som(n)
          ind_som(n)=nb_true
       end if
    end do

    print*,"som",nb_true,bul%nbsom_vrai


    do n=1,bul%nbele
       if (bul%ele(n)%mask) then
          bul%ele(n)%nusom(1) =   ind_som(bul%ele(n)%nusom(1))
          bul%ele(n)%nusom(2) =   ind_som(bul%ele(n)%nusom(2))
       end if
    end do

    deallocate(ind_som)
    call MOVE_ALLOC(FROM=som_temp,TO=bul%som)

    bul%nbsom=bul%nbsom_vrai

  end subroutine compact_som
  !*******************************************************************************
  !*******************************************************************************
  subroutine reorganization(bul,ft)
    !*******************************************************************************
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables 
    !-------------------------------------------------------------------------------
    type(struct_front_tracking),intent(inout):: ft
    type(bulle),dimension(:),intent(inout)   :: bul
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    integer::n,som1,somf,vois2,num,num_old,l
    type(element), dimension(:), allocatable:: ele_temp
    type(sommet), dimension(:), allocatable :: som_temp
    !-------------------------------------------------------------------------------
    do n=1,ft%nbbul
       if (bul(n)%mask) then
          bul(n)%nbsommax=bul(n)%nbsom_vrai*ft%multiplicateur_de_sommet
          bul(n)%nbelemax=bul(n)%nbele_vrai*ft%multiplicateur_d_element
          allocate( ele_temp(bul(n)%nbelemax),som_temp(bul(n)%nbsommax) )
          ele_temp(bul(n)%nbele_vrai+1:)%mask=.false.
          som_temp(bul(n)%nbsom_vrai+1:)%mask=.false.

          vois2=1
          do while( bul(n)%ele(vois2)%mask .eqv. .false. )
             vois2=vois2+1
          end do

          som1=bul(n)%ele( vois2 )%nusom(1)
          somf=bul(n)%ele( bul(n)%ele(vois2)%ele_vois(1) )%nusom(1)
          num_old=bul(n)%nbele_vrai
          num=0
          do while (som1/=somf)
             num=num+1
             ele_temp(num)%nusom(1)=num
             ele_temp(num)%nusom(2)=num+1
             som_temp( ele_temp(num)%nusom(1) )%xyz=bul(n)%som(som1)%xyz
             som_temp( ele_temp(num)%nusom(1) )%xyz_old=bul(n)%som(som1)%xyz_old
             som_temp( ele_temp(num)%nusom(1) )%mask=.true.
             ele_temp(num)%mask=.true.
             ele_temp(num)%ele_vois(1)=num_old
             ele_temp(num_old)%ele_vois(2)=num
             som_temp( ele_temp(num)%nusom(1) )%compteur=bul(n)%som(som1)%compteur
             som_temp( ele_temp(num)%nusom(1) )%desenrichissement=bul(n)%som(som1)%desenrichissement
             !-------------------------mise à jour des vosins et des sommets------------------------
             vois2=bul(n)%ele( vois2 )%ele_vois(2)
             som1=bul(n)%ele( vois2 )%nusom(1)
             num_old=num
          end do
          num=num+1
          ele_temp(num)%nusom(1)=num
          ele_temp(num)%nusom(2)=1
          som_temp( ele_temp(num)%nusom(1) )%xyz=bul(n)%som(somf)%xyz
          som_temp( ele_temp(num)%nusom(1) )%xyz_old=bul(n)%som(somf)%xyz_old
          som_temp( ele_temp(num)%nusom(1) )%mask=.true.
          ele_temp(num)%mask=.true.
          ele_temp(num)%ele_vois(1)=num_old
          ele_temp(num_old)%ele_vois(2)=num
          som_temp( ele_temp(num)%nusom(1) )%compteur=bul(n)%som(somf)%compteur
          som_temp( ele_temp(num)%nusom(1) )%desenrichissement=bul(n)%som(somf)%desenrichissement

          call MOVE_ALLOC(FROM=ele_temp,TO=bul(n)%ele)
          call MOVE_ALLOC(FROM=som_temp,TO=bul(n)%som)
       end if
    end do
    !-------------------------------------------------------------------------------
  end subroutine reorganization
  !********************************************************************************************
  !********************************************************************************************

  !********************************************************************************************
  !***************************************************************************************** 
  subroutine ft_trans2d (u,v,u0,v0,pres,bul,nbbul,phase_u,phase_v,phase_Pres,cou,nt,Erreur)
    !*******************************************************************************
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables 
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:),allocatable,intent(in)    :: u,v,u0,v0,pres
    type(bulle),dimension(:), allocatable,intent(inout) :: bul
    logical, dimension(:,:,:),allocatable,intent(inout) :: phase_u,phase_v,phase_Pres
    real(8), dimension(:,:,:),allocatable,intent(inout) :: cou
    integer, intent(in)                                 :: nbbul
    integer, intent(in)                                 :: nt
    real(8), intent(out)                                :: Erreur
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:),allocatable :: vp,vpn
    integer                             :: num,l,n,j,i
    !-------------------------------------------------------------------------------
    num=0
    if (FT_RK==1) then
       !---------------------------Interpolation method with RK1-----------------------
       call normal_calculation(bul,nbbul)
       !-----------------------------interpolation-------------------------------------
       call interpolation(u,v,pres,bul,nbbul,phase_u,phase_v,phase_Pres,vp,num,nt,Erreur)
       !-------------------------------transport---------------------------------------
       call runge_kutta(bul,nbbul,phase_u,phase_v,phase_Pres,cou,nt,vp)
       !-------------------------------------------------------------------------------
    elseif (FT_RK==2) then
       !---------------------------Interpolation method with RK2-----------------------
       call normal_calculation(bul,nbbul)
       !-----------------------------interpolation-------------------------------------
       call interpolation(u,v,pres,bul,nbbul,phase_u,phase_v,phase_Pres,vp,num,nt,Erreur)
       !-------------------------------transport---------------------------------------
       call runge_kutta(bul,nbbul,phase_u,phase_v,phase_Pres,cou,nt,vp)
       !-------------------------------------------------------------------------------

       num=num+1
       !-------------------------------------------------------------------------------
       call normal_calculation(bul,nbbul)
       !-----------------------------interpolation-------------------------------------
       call interpolation(u0,v0,pres,bul,nbbul,phase_u,phase_v,phase_Pres,vpn,num,nt,Erreur)
       !-------------------------------transport---------------------------------------
       call runge_kutta(bul,nbbul,phase_u,phase_v,phase_Pres,cou,nt,vp,vpn)
       !-------------------------------------------------------------------------------
    else
       write(*,*) "Runge-Kutta > 2 not done"
       stop
    end if
    !print*,'nt=',nt
  end subroutine ft_trans2d
  !*****************************************************************************************
  !***************************************************************************************** 
  subroutine interpolation(u,v,pres,bul,nbbul,phase_u,phase_v,phase_Pres,vp,num,nt,Erreur)
    !*******************************************************************************
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables 
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:),allocatable,intent(in)    :: u,v,pres
    type(bulle),dimension(:), allocatable,intent(inout) :: bul
    logical, dimension(:,:,:),allocatable,intent(in)    :: phase_u,phase_v,phase_Pres
    real(8), dimension(:,:),allocatable,intent(out)     :: vp
    integer, intent(in)                                 :: nbbul
    integer, intent(in)                                 :: nt,num
    real(8), intent(out)                                :: Erreur
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    integer                               :: n,m,k,coef1,coef2,k1,k2
    real(8)                               :: Erreur_Linfty,kappa,kappa1,kappa2,kappa3,kappa4
    real(8), dimension(dim)               :: xyz,xyz_vois1,xyz_vois2,gradu_exact,ve,vint,n_node
    real(8), dimension(dim,dim)           :: gradu1,gradu2
    real(8)                               :: P1,P2
    !-------------------------------------------------------------------------------
    do n=1,nbbul
       if (allocated(vp)) deallocate(vp)

       if ( bul(n)%mask ) then
          allocate( vp(dim,bul(n)%nbsom))

          Erreur_Linfty=0.d0
          !print*,'num',num
          do m=1,bul(n)%nbele
             if ( bul(n)%ele(m)%mask ) then
                if ( bul(n)%som( bul(n)%ele(m)%nusom(1) )%notfix ) then
                   write(55,*) bul(n)%som( bul(n)%ele(m)%nusom(1) )%notfix
                   write(55,*) '-----------------------------'
                   write(55,*) nt,m
                   !-------------------update position---------------------------------
                   xyz(:)=bul(n)%som( bul(n)%ele(m)%nusom(1) )%xyz(:)
                   !-------------------------------------------------------------------
                   kappa =bul(n)%som(bul(n)%ele(m)%nusom(1))%kappa
                   n_node=bul(n)%som(bul(n)%ele(m)%nusom(1))%n_node
                   !-------------------------------------------------------------------
                   k1=bul(n)%ele(m)%ele_vois(1)
                   kappa1=bul(n)%som(bul(n)%ele(k1)%nusom(1))%kappa
                   kappa2=bul(n)%som(bul(n)%ele(m)%nusom(2))%kappa
                   k1=bul(n)%ele(k1)%ele_vois(1)
                   kappa3=bul(n)%som(bul(n)%ele(k1)%nusom(1))%kappa
                   k2=bul(n)%ele(m)%ele_vois(2)
                   kappa4=bul(n)%som(bul(n)%ele(k2)%nusom(2))%kappa
                   kappa=(kappa+kappa1+kappa2+kappa3+kappa4)/5
                   !-------------------------smooth curvature--------------------------
                   !-------------------------------------------------------------------
                   !        if (num==0 ) then 
                   !---------------------update marker velocity init-------------------
                   bul(n)%som(bul(n)%ele(m)%nusom(1))%vp_old=bul(n)%som(bul(n)%ele(m)%nusom(1))%vp
                   !---------------------update marker gradient init-------------------
                   !-----------------------------side 1--------------------------------
                   bul(n)%som(bul(n)%ele(m)%nusom(1))%gradu1_old=bul(n)%som(bul(n)%ele(m)%nusom(1))%gradu1
                   bul(n)%som(bul(n)%ele(m)%nusom(1))%gradv1_old=bul(n)%som(bul(n)%ele(m)%nusom(1))%gradv1
                   !-----------------------------side 2--------------------------------
                   bul(n)%som(bul(n)%ele(m)%nusom(1))%gradu2_old=bul(n)%som(bul(n)%ele(m)%nusom(1))%gradu2
                   bul(n)%som(bul(n)%ele(m)%nusom(1))%gradv2_old=bul(n)%som(bul(n)%ele(m)%nusom(1))%gradv2
                   !-------------------------------------------------------------------
                   !        end if
                   call interpolation_method(u,v,pres,xyz,phase_u,phase_v,phase_Pres,vint,Erreur_Linfty,&
                        nt,gradu1,gradu2,P1,P2,kappa,n_node)
                   !        if (num==0) then
                   !-------------------------------------------------------------------
                   vp(1,m)=vint(1)
                   vp(2,m)=vint(2)
                   !---------------------update marker velocity------------------------
                   bul(n)%som(bul(n)%ele(m)%nusom(1))%vp(1)=vp(1,m)
                   bul(n)%som(bul(n)%ele(m)%nusom(1))%vp(2)=vp(2,m)
                   !---------------------update marker pressure------------------------
                   bul(n)%som(bul(n)%ele(m)%nusom(1))%P1=P1
                   bul(n)%som(bul(n)%ele(m)%nusom(1))%P2=P2
                   !---------------------update marker gradient------------------------
                   !-----------------------------side 1--------------------------------
                   bul(n)%som(bul(n)%ele(m)%nusom(1))%gradu1(1)=gradu1(1,1)
                   bul(n)%som(bul(n)%ele(m)%nusom(1))%gradu1(2)=gradu1(1,2)
                   bul(n)%som(bul(n)%ele(m)%nusom(1))%gradv1(1)=gradu1(2,1)
                   bul(n)%som(bul(n)%ele(m)%nusom(1))%gradv1(2)=gradu1(2,2)
                   !-----------------------------side 2--------------------------------
                   bul(n)%som(bul(n)%ele(m)%nusom(1))%gradu2(1)=gradu2(1,1)
                   bul(n)%som(bul(n)%ele(m)%nusom(1))%gradu2(2)=gradu2(1,2)
                   bul(n)%som(bul(n)%ele(m)%nusom(1))%gradv2(1)=gradu2(2,1)
                   bul(n)%som(bul(n)%ele(m)%nusom(1))%gradv2(2)=gradu2(2,2)
                   !-------------------------------------------------------------------
                   coef1=2; coef2=1
                   if ( nt==1 ) then
                      coef1=1
                      coef2=0
                   end if
                   !---------------------update marker velocity star-------------------
                   bul(n)%som(bul(n)%ele(m)%nusom(1))%vp_star=coef1* bul(n)%som(bul(n)%ele(m)%nusom(1))%vp-&
                        coef2*bul(n)%som(bul(n)%ele(m)%nusom(1))%vp_old
                   !---------------------update marker gradient star-------------------
                   !-----------------------------side 1--------------------------------
                   bul(n)%som(bul(n)%ele(m)%nusom(1))%gradu1_star=coef1* bul(n)%som(bul(n)%ele(m)%nusom(1))%gradu1-&
                        coef2*bul(n)%som(bul(n)%ele(m)%nusom(1))%gradu1_old
                   bul(n)%som(bul(n)%ele(m)%nusom(1))%gradv1_star=coef1* bul(n)%som(bul(n)%ele(m)%nusom(1))%gradv1-&
                        coef2*bul(n)%som(bul(n)%ele(m)%nusom(1))%gradv1_old
                   !-----------------------------side 2--------------------------------
                   bul(n)%som(bul(n)%ele(m)%nusom(1))%gradu2_star=coef1* bul(n)%som(bul(n)%ele(m)%nusom(1))%gradu2-&
                        coef2*bul(n)%som(bul(n)%ele(m)%nusom(1))%gradu2_old
                   bul(n)%som(bul(n)%ele(m)%nusom(1))%gradv2_star=coef1* bul(n)%som(bul(n)%ele(m)%nusom(1))%gradv2-&
                        coef2*bul(n)%som(bul(n)%ele(m)%nusom(1))%gradv2_old
                   !-------------------------------------------------------------------
                   !-------------------------------------------------------------------
                   !              end if
!!$                write(22,*) '-------------------'
!!$                write(22,*) bul(n)%som(bul(n)%ele(m)%nusom(1))%vp_old
!!$                write(22,*) bul(n)%som(bul(n)%ele(m)%nusom(1))%vp
!!$                write(22,*) bul(n)%som(bul(n)%ele(m)%nusom(1))%vp_star
!!$                write(22,*) '-------------------'

!!$                call exact_velocity(xyz,ve,0,gradu_exact)
!!$                write(85,*) '----------------------------------------'
!!$                write(85,*) nt,m
!!$                write(85,*) xyz
!!$                write(85,*) bul(n)%som(bul(n)%ele(m)%nusom(1))%vp(1),bul(n)%som(bul(n)%ele(m)%nusom(1))%vp(2)
!!$                write(85,*) ve(1),ve(2)

                else
                   write(55,*) bul(n)%som( bul(n)%ele(m)%nusom(1) )%notfix
                   !------------------------nodes are fixed-----------------------------
                   vp(1,m)=0
                   vp(2,m)=0
                   !---------------------update marker velocity------------------------
                   bul(n)%som(bul(n)%ele(m)%nusom(1))%vp(1)=vp(1,m)
                   bul(n)%som(bul(n)%ele(m)%nusom(1))%vp(2)=vp(2,m)
                   !---------------------update marker pressure------------------------
                   bul(n)%som(bul(n)%ele(m)%nusom(1))%P1=0
                   bul(n)%som(bul(n)%ele(m)%nusom(1))%P2=0
                   !---------------------update marker gradient------------------------
                   !-----------------------------side 1--------------------------------
                   bul(n)%som(bul(n)%ele(m)%nusom(1))%gradu1(1)=0
                   bul(n)%som(bul(n)%ele(m)%nusom(1))%gradu1(2)=0
                   bul(n)%som(bul(n)%ele(m)%nusom(1))%gradv1(1)=0
                   bul(n)%som(bul(n)%ele(m)%nusom(1))%gradv1(2)=0
                   !-----------------------------side 2--------------------------------
                   bul(n)%som(bul(n)%ele(m)%nusom(1))%gradu2(1)=0
                   bul(n)%som(bul(n)%ele(m)%nusom(1))%gradu2(2)=0
                   bul(n)%som(bul(n)%ele(m)%nusom(1))%gradv2(1)=0
                   bul(n)%som(bul(n)%ele(m)%nusom(1))%gradv2(2)=0
                   !-------------------------------------------------------------------
                   !--------------------------------------------------------------------
                end if
             end if
          end do
          Erreur=Erreur_Linfty
       end if
    end do
  end subroutine interpolation
  !*****************************************************************************************
  !*****************************************************************************************       
  subroutine runge_kutta(bul,nbbul,phase_u,phase_v,phase_Pres,cou,nt,vp,vpn)
    !*******************************************************************************
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables 
    !-------------------------------------------------------------------------------
    type(bulle), dimension(:), allocatable,intent(inout)    :: bul
    logical, dimension(:,:,:),allocatable,intent(inout)     :: phase_u,phase_v,phase_Pres
    real(8), dimension(:,:,:),allocatable,intent(inout)     :: cou
    real(8), dimension(:,:),allocatable,intent(in)          :: vp
    real(8), dimension(:,:),allocatable,intent(in),optional :: vpn
    integer, intent(in)                                     :: nbbul,nt
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    integer :: n,m,k
    real(8), dimension(dim) :: xyz,xyz_vois1,xyz_vois2,xyzn,xyznn,ve,gradu_exact
    real(8) :: erreur
    !-------------------------------------------------------------------------------
    if (present(vpn)) then
       do n=1,nbbul
          if ( bul(n)%mask ) then
             do m=1,bul(n)%nbele
                if ( bul(n)%ele(m)%mask  ) then
                   if ( bul(n)%som( bul(n)%ele(m)%nusom(1) )%notfix ) then
                      !-------------------update position---------------------------------
                      xyzn(:)=bul(n)%som( bul(n)%ele(m)%nusom(1) )%xyz(:)
                      xyz(:)=bul(n)%som(bul(n)%ele(m)%nusom(1))%xyz_old(:)
                      !-------------------update neighbor position------------------------
                      k=bul(n)%ele(m)%ele_vois(1)
                      xyz_vois1(:)=bul(n)%som( bul(n)%ele(k)%nusom(1) )%xyz
                      k=bul(n)%ele(m)%ele_vois(2)
                      xyz_vois2(:)=bul(n)%som( bul(n)%ele(k)%nusom(1) )%xyz
                      !-------------------------transport---------------------------------
                      xyznn(1)=0.5d0*(xyz(1)+(xyzn(1)+dt*vpn(1,m)))
                      xyznn(2)=0.5d0*(xyz(2)+(xyzn(2)+dt*vpn(2,m)))
                      if ( xyznn(1) < xmin .or. xyznn(1) > xmax .or. &
                           xyznn(2) < ymin .or. xyznn(2) > ymax ) then
                         bul(n)%som( bul(n)%ele(m)%nusom(1) )%notfix=.false.
                      end if
                      bul(n)%som(bul(n)%ele(m)%nusom(1))%xyz(:)=xyznn(:)
                      !--------------------------u phase----------------------------------
                      !-----------------------first triangle------------------------------
                      call phase_update(xyz_vois1,xyzn,xyznn,phase_u,sx,sy,ex+1,ey,dx,dy,(xmin-dx/2),ymin,cou)
                      !-----------------------second triangle-----------------------------
                      call phase_update(xyzn,xyz_vois2,xyznn,phase_u,sx,sy,ex+1,ey,dx,dy,(xmin-dx/2),ymin,cou)
                      !--------------------------v phase----------------------------------
                      !-----------------------first triangle------------------------------
                      call phase_update(xyz_vois1,xyzn,xyznn,phase_v,sx,sy,ex,ey+1,dx,dy,xmin,(ymin-dy/2),cou)
                      !-----------------------second triangle-----------------------------
                      call phase_update(xyzn,xyz_vois2,xyznn,phase_v,sx,sy,ex,ey+1,dx,dy,xmin,(ymin-dy/2),cou)
                      !--------------------------P phase----------------------------------
                      !-----------------------first triangle------------------------------
                      call phase_update(xyz_vois1,xyzn,xyznn,phase_Pres,sx,sy,ex,ey,dx,dy,xmin,ymin,cou)
                      !-----------------------second triangle-----------------------------
                      call phase_update(xyzn,xyz_vois2,xyznn,phase_Pres,sx,sy,ex,ey,dx,dy,xmin,ymin,cou)
                      !-------------------------------------------------------------------   

!!$                   write(22,*) '-------------------'
!!$                   call exact_velocity(xyznn,ve,nt,gradu_exact)
!!$                   erreur=(abs(ve(1)-bul(n)%som(bul(n)%ele(m)%nusom(1))%vp(1))&
!!$                        +abs(ve(2)-bul(n)%som(bul(n)%ele(m)%nusom(1))%vp(2)))
!!$                   write(22,*) 'normal',erreur
!!$                   erreur=(abs(ve(1)-bul(n)%som(bul(n)%ele(m)%nusom(1))%vp_star(1))&
!!$                        +abs(ve(2)-bul(n)%som(bul(n)%ele(m)%nusom(1))%vp_star(2)))
!!$                   write(22,*) 'extrapole',erreur
                   end if
                end if
             end do
          end if
       end do
    else    
       do n=1,nbbul
          if ( bul(n)%mask ) then
             do m=1,bul(n)%nbele
                if ( bul(n)%ele(m)%mask ) then
                   if ( bul(n)%som( bul(n)%ele(m)%nusom(1) )%notfix ) then
                      !------------count on node for rupture or coalescence---------------
                      bul(n)%som(bul(n)%ele(m)%nusom(1))%compteur=bul(n)%som(bul(n)%ele(m)%nusom(1))%compteur-1
                      !-------------------update position---------------------------------
                      xyz(:)=bul(n)%som( bul(n)%ele(m)%nusom(1) )%xyz(:)
                      bul(n)%som(bul(n)%ele(m)%nusom(1))%xyz_old(:)=xyz(:)
                      !-------------------update neighbor position------------------------
                      k=bul(n)%ele(m)%ele_vois(1)
                      xyz_vois1(:)=bul(n)%som( bul(n)%ele(k)%nusom(1) )%xyz
                      k=bul(n)%ele(m)%ele_vois(2)
                      xyz_vois2(:)=bul(n)%som( bul(n)%ele(k)%nusom(1) )%xyz
                      !-------------------------transport---------------------------------
                      xyzn(1)=xyz(1)+dt*vp(1,m)
                      xyzn(2)=xyz(2)+dt*vp(2,m)
                      if ( xyzn(1) < xmin .or. xyzn(1) > xmax .or. &
                           xyzn(2) < ymin .or. xyzn(2) > ymax ) then
                         bul(n)%som( bul(n)%ele(m)%nusom(1) )%notfix=.false.
                      end if
                      bul(n)%som(bul(n)%ele(m)%nusom(1))%xyz(:)=xyzn(:)
                      !--------------------------u phase------------------------------
                      !-----------------------first triangle--------------------------
                      call phase_update(xyz_vois1,xyz,xyzn,phase_u,sx,sy,ex+1,ey,dx,dy,(xmin-dx/2),ymin,cou)
                      !-----------------------second triangle-------------------------
                      call phase_update(xyz,xyz_vois2,xyzn,phase_u,sx,sy,ex+1,ey,dx,dy,(xmin-dx/2),ymin,cou)
                      !--------------------------v phase---------------------------
                      !-----------------------first triangle-----------------------
                      call phase_update(xyz_vois1,xyz,xyzn,phase_v,sx,sy,ex,ey+1,dx,dy,xmin,(ymin-dy/2),cou)
                      !-----------------------second triangle----------------------
                      call phase_update(xyz,xyz_vois2,xyzn,phase_v,sx,sy,ex,ey+1,dx,dy,xmin,(ymin-dy/2),cou)
                      !--------------------------P phase----------------------------------
                      !-----------------------first triangle-----------------------
                      call phase_update(xyz_vois1,xyz,xyzn,phase_Pres,sx,sy,ex,ey,dx,dy,xmin,ymin,cou)
                      !-----------------------second triangle----------------------
                      call phase_update(xyz,xyz_vois2,xyzn,phase_Pres,sx,sy,ex,ey,dx,dy,xmin,ymin,cou)
                      !------------------------------------------------------------  
!!$                write(22,*) '-------------------'
!!$                write(22,*) bul(n)%som(bul(n)%ele(m)%nusom(1))%vp_old
!!$                write(22,*) bul(n)%som(bul(n)%ele(m)%nusom(1))%vp
!!$                write(22,*) bul(n)%som(bul(n)%ele(m)%nusom(1))%vp_star


!!$                write(22,*) '-------------------'
!!$                call exact_velocity(xyzn,ve,nt,gradu_exact)
!!$                erreur=(abs(ve(1)-bul(n)%som(bul(n)%ele(m)%nusom(1))%vp(1))&
!!$                     +abs(ve(2)-bul(n)%som(bul(n)%ele(m)%nusom(1))%vp(2)))
!!$                write(22,*) 'normal',erreur
!!$                erreur=(abs(ve(1)-bul(n)%som(bul(n)%ele(m)%nusom(1))%vp_star(1))&
!!$                     +abs(ve(2)-bul(n)%som(bul(n)%ele(m)%nusom(1))%vp_star(2)))
!!$                write(22,*) 'extrapole',erreur



!!$                write(85,*) '----------------------------------------'
!!$                write(85,*) nt,m
!!$                write(85,*) xyz
!!$                write(85,*) bul(n)%som(bul(n)%ele(m)%nusom(1))%vp(1),bul(n)%som(bul(n)%ele(m)%nusom(1))%vp(2)
!!$                write(85,*) ve(1),ve(2)
                   end if
                end if
             end do
          end if
       end do
    end if
    !-------------------------------------------------------------------
  end subroutine runge_kutta
  !*****************************************************************************************
  !*****************************************************************************************       
  subroutine old_conservation_volume_2d(ft,bul,volume,alpha,lost_volume,nt)
    !*****************************************************************************************               
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables 
    !-------------------------------------------------------------------------------
    type(struct_front_tracking),intent(in)      :: ft
    type(bulle),dimension(:),intent(inout)      :: bul
    real(8), intent(out)                        :: volume,alpha
    real(8), intent(in)                         :: lost_volume
    integer, intent(in)                         :: nt
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    integer                                     :: n,j,m
    real(8)                                     :: volume1,volume2,a0,a1,delta,alpha1,alpha2
    real(8),dimension(dim)                      :: xyz1_old,xyz2_old,xyz1,xyz2,u,v1,v2,vdiag
    !-------------------------------------------------------------------------------
    do m=1,ft%nbbul
       if (bul(m)%mask) then
          volume=0.d0
          a0=0.d0; a1=0.d0
          do n=1,bul(m)%nbele
             if (bul(m)%ele(n)%mask) then


                j=bul(m)%ele(n)%nusom(1)  ;  xyz1_old=bul(m)%som(j)%xyz_old
                j=bul(m)%ele(n)%nusom(2)  ;  xyz2_old=bul(m)%som(j)%xyz_old
                u=xyz2_old-xyz1_old

                j=bul(m)%ele(n)%nusom(1)  ;  xyz1=bul(m)%som(j)%xyz
                j=bul(m)%ele(n)%nusom(2)  ;  xyz2=bul(m)%som(j)%xyz
                v1=xyz1-xyz1_old  ;  v2=xyz2-xyz2_old

                vdiag=xyz1-xyz2_old

                volume1=0.5d0*(u(1)*v1(2)-u(2)*v1(1))
                volume2=0.5d0*(v2(1)*vdiag(2)-v2(2)*vdiag(1))
                volume=volume+(volume1+volume2)
                !----------constant alpha calculation -------------------------------------------
                a0=a0 + (u(1)*v1(2)-u(2)*v1(1)) + ( -v2(1)*u(2) + v2(2)*u(1) )
                a1=a1 + (v1(2)*v2(1) - v1(1)*v2(2))
             end if
          end do
          !----------constant alpha calculation -------------------------------------------
          !alpha=-a0/a1 
          delta= a0*a0 - 4*a1*(-lost_volume)
          if (delta >=0) then
             alpha1=(-a0-sqrt(delta))/(2*a1)
             alpha2=(-a0+sqrt(delta))/(2*a1)
             alpha=max(alpha1,alpha2)
             !write(16,'(1pe9.2,1pe16.9,1pe16.9,1pe16.9,1pe16.9,1pe16.9,1pe16.9)') nt*dt,a0,a1,alpha1,alpha2,alpha,lost_volume
             !write(16,'(1pe9.2,1pe16.9,1pe16.9,1pe16.9)') nt*dt,a0,a1,alpha

             do n=1,bul(m)%nbele
                if (bul(m)%ele(n)%mask) then

                   j=bul(m)%ele(n)%nusom(1)  
                   bul(m)%som(j)%xyz = bul(m)%som(j)%xyz_old + alpha * (bul(m)%som(j)%xyz-bul(m)%som(j)%xyz_old)
                end if
             end do

          else
             print*,'delta est negatif'
             write(14,'(1pe9.2,1pe16.9,1pe16.9,1pe16.9,1pe16.9)') nt*dt,delta,a0,a1,lost_volume
             exit
          end if
          !if (alpha>1.01.or.alpha<.99) write(14,*) "homothétie : alpha=",alpha
       end if
    end do

  end subroutine old_conservation_volume_2d
  !*****************************************************************************************  
  !*****************************************************************************************
  subroutine conservation_volume_vitesseprojete_2d(nbbul,bul,volume,alpha,phase_u,phase_v,phase_Pres,& !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
       cou,lost_volume,nt,old_conservation)
    !-------------------------------------------------------------------------------
    implicit none
    !-------------------------------------------------------------------------------
    type(bulle),dimension(:),intent(inout)              :: bul
    real(8), intent(out)                                :: volume,alpha
    real(8), intent(in)                                 :: lost_volume
    integer,intent(in)                                  :: nt
    logical,intent(in),optional                         :: old_conservation
    integer, intent(in)                                 :: nbbul
    logical, dimension(:,:,:),allocatable,intent(inout) :: phase_u,phase_v,phase_Pres
    real(8), dimension(:,:,:),allocatable,intent(inout) :: cou
    !-------------------------------------------------------------------------------
    integer                                     :: n,j,m,k
    real(8)                                     :: volume1,volume2,a0,a1,a2,delta,alpha1,alpha2
    real(8),dimension(dim)                      :: xyz1_old,xyz2_old,xyz1,xyz2,u,v1,v2,vdiag
    real(8),dimension(dim)                      :: n0,n1,n2
    real(8),dimension(:,:),allocatable          :: p1,p2
    real(8),dimension(:),allocatable            :: dot_prod1,dot_prod2
    real(8)                                     :: t1,t2,t_cpu
    real(8), dimension(dim)                     :: xyz_old,xyz_new,xyz_vois1,xyz_vois2
    !-------------------------------------------------------------------------------
    call cpu_time(time=t1)
    do m=1,nbbul
       if (bul(m)%mask) then
          volume=0.d0
          a0=0.d0; a1=0.d0 
          allocate(p1(dim,bul(m)%nbele),p2(dim,bul(m)%nbele),dot_prod1(bul(m)%nbele),dot_prod2(bul(m)%nbele))
          do n=1,bul(m)%nbele
             if (bul(m)%ele(n)%mask) then
                !------construction de la bissectrice-------------------------------------------
                k=bul(m)%ele(n)%ele_vois(1)
                xyz1=bul(m)%som(bul(m)%ele(k)%nusom(1))%xyz
                xyz2=bul(m)%som(bul(m)%ele(k)%nusom(2))%xyz
                call construction_normale_elt(xyz1,xyz2,n1)

                k=bul(m)%ele(n)%ele_vois(2)
                xyz1=bul(m)%som(bul(m)%ele(k)%nusom(1))%xyz
                xyz2=bul(m)%som(bul(m)%ele(k)%nusom(2))%xyz
                call construction_normale_elt(xyz1,xyz2,n2)

                xyz1=bul(m)%som(bul(m)%ele(n)%nusom(1))%xyz
                xyz2=bul(m)%som(bul(m)%ele(n)%nusom(2))%xyz
                call construction_normale_elt(xyz1,xyz2,n0)

                n1=n0+n1
                n1=n1/sqrt(dot_product(n1,n1))
                n2=n0+n2
                n2=n2/sqrt(dot_product(n2,n2))
                !----------fin bissectrice----------------------------------------------------
                j=bul(m)%ele(n)%nusom(1)  ;  xyz1_old=bul(m)%som(j)%xyz_old
                j=bul(m)%ele(n)%nusom(2)  ;  xyz2_old=bul(m)%som(j)%xyz_old
                u=xyz2_old-xyz1_old

                j=bul(m)%ele(n)%nusom(1)  ;  xyz1=bul(m)%som(j)%xyz
                j=bul(m)%ele(n)%nusom(2)  ;  xyz2=bul(m)%som(j)%xyz
                v1=xyz1-xyz1_old          ;  v2=xyz2-xyz2_old
                vdiag=xyz1-xyz2_old
                !------calcul volume entre position avant et après transport--------------------
                volume1=0.5d0*(u(1)*v1(2)-u(2)*v1(1))
                volume2=0.5d0*(v2(1)*vdiag(2)-v2(2)*vdiag(1))
                volume=volume+(volume1+volume2)
                !-------------------------------------------------------------------------------
                if (present(old_conservation)) then
                   !if (old_conservation) then
                   dot_prod1(n)=dot_product(v1,n1)
                   dot_prod2(n)=dot_product(v2,n2)

                   p1(1,n)=v1(1)*sign(1._8,dot_prod1(n))
                   p1(2,n)=v1(2)*sign(1._8,dot_prod1(n))
                   p2(1,n)=v2(1)*sign(1._8,dot_prod2(n))
                   p2(2,n)=v2(2)*sign(1._8,dot_prod2(n))
                else
                   p1(1,n)=abs( dot_product(v1,n1) )*n1(1)
                   p1(2,n)=abs( dot_product(v1,n1) )*n1(2)
                   p2(1,n)=abs( dot_product(v2,n2) )*n2(1)
                   p2(2,n)=abs( dot_product(v2,n2) )*n2(2)
                end if
                u=xyz2-xyz1
                a0= a0 + ( p2(1,n)*p1(2,n)-p2(2,n)*p1(1,n) )
                a1= a1 + ( u(1)*p2(2,n)-u(2)*p2(1,n) + u(1)*p1(2,n)-u(2)*p1(1,n) )
                a2= volume
             end if
          end do

          !------------------------alpha calculation -------------------------------------------
          delta= a1**2 - 4*a0*(2*a2-lost_volume)
          if (delta >=0) then
             alpha1=(-a1-sqrt(delta))/(2*a0)
             alpha2=(-a1+sqrt(delta))/(2*a0)
             alpha=max(alpha1,alpha2)   !!!!!?????? à revoir
             !write(16,'(1pe9.2,1pe16.9,1pe16.9,1pe16.9,1pe16.9)') nt*dt,alpha1,alpha2,alpha,lost_volume
             do n=1,bul(m)%nbele
                if (bul(m)%ele(n)%mask) then
                   !  if ( bul(m)%som( bul(m)%ele(n)%nusom(1) )%notfix ) then
                   !----------------------------------------------------------------
                   k=bul(m)%ele(n)%ele_vois(1)
                   xyz_vois1(:)=bul(m)%som( bul(m)%ele(k)%nusom(1) )%xyz
                   k=bul(m)%ele(n)%ele_vois(2)
                   xyz_vois2(:)=bul(m)%som( bul(m)%ele(k)%nusom(1) )%xyz
                   !----------------------------------------------------------------
                   j=bul(m)%ele(n)%nusom(1)
                   xyz_old=bul(m)%som(j)%xyz
                   bul(m)%som(j)%xyz(1) = alpha*p1(1,n)+bul(m)%som(j)%xyz(1)
                   bul(m)%som(j)%xyz(2) = alpha*p1(2,n)+bul(m)%som(j)%xyz(2)
                   xyz_new=bul(m)%som(j)%xyz
                   !------------------------phase update---------------------------------
                   !--------------------------u phase------------------------------------
                   !-----------------------first triangle--------------------------------
                   call phase_update(xyz_vois1,xyz_old,xyz_new,phase_u,sx,sy,ex+1,ey,dx,dy,(xmin-dx/2),ymin,cou)
                   !-----------------------second triangle-------------------------------
                   call phase_update(xyz_old,xyz_vois2,xyz_new,phase_u,sx,sy,ex+1,ey,dx,dy,(xmin-dx/2),ymin,cou)
                   !--------------------------v phase------------------------------------
                   !-----------------------first triangle--------------------------------
                   call phase_update(xyz_vois1,xyz_old,xyz_new,phase_v,sx,sy,ex,ey+1,dx,dy,xmin,(ymin-dy/2),cou)
                   !-----------------------second triangle-------------------------------
                   call phase_update(xyz_old,xyz_vois2,xyz_new,phase_v,sx,sy,ex,ey+1,dx,dy,xmin,(ymin-dy/2),cou)
                   !--------------------------P phase------------------------------------
                   !-----------------------first triangle--------------------------------
                   call phase_update(xyz_vois1,xyz_old,xyz_new,phase_Pres,sx,sy,ex,ey,dx,dy,xmin,ymin,cou)
                   !-----------------------second triangle-------------------------------
                   call phase_update(xyz_old,xyz_vois2,xyz_new,phase_Pres,sx,sy,ex,ey,dx,dy,xmin,ymin,cou)
                   !----------------------------------------------------------------------        
                   !   end if
                end if
             end do
          else
             print*,'delta est negatif'
             write(14,'(1pe9.2,1pe16.9,1pe16.9,1pe16.9,1pe16.9)') nt*dt,delta,a0,a1,lost_volume
             exit
          end if
          deallocate(p1,p2,dot_prod1,dot_prod2)
          !if (alpha>1.01.or.alpha<.99) write(14,*) "homothétie : alpha=",alpha
       end if
    end do
    call cpu_time(time=t2)
    t_cpu=t2-t1
  end subroutine conservation_volume_vitesseprojete_2d
  !*****************************************************************************************
  !*****************************************************************************************
  subroutine construction_normale_elt(xyz1,xyz2,n)
    !-------------------------------------------------------------------------------
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    real(8),dimension(dim),intent(in) :: xyz1,xyz2
    real(8),dimension(dim),intent(out):: n
    !--------------------element normal calculation---------------------------------
    n(1)=+(xyz2(2)-xyz1(2))
    n(2)=-(xyz2(1)-xyz1(1))
    n=n/( sqrt(dot_product(n,n)) )
    !if ( abs(dot_product(n,n)) > 1.d-15 ) n=n/( sqrt(dot_product(n,n)) )  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !-------------------------------------------------------------------------------
  end subroutine construction_normale_elt
  !*****************************************************************************************  
  !*****************************************************************************************  
  subroutine conservation_volume_2d(nbbul,bul,alpha,phase_u,phase_v,phase_Pres,cou,lost_volume,&
       nt)
    !-------------------------------------------------------------------------------
    implicit none
    !-------------------------------------------------------------------------------
    type(bulle),dimension(:),intent(inout)              :: bul
    real(8), intent(out)                                :: alpha
    real(8), intent(in)                                 :: lost_volume
    integer, intent(in)                                 :: nt
    integer, intent(in)                                 :: nbbul
    logical, dimension(:,:,:),allocatable,intent(inout) :: phase_u,phase_v,phase_Pres
    real(8), dimension(:,:,:),allocatable,intent(inout) :: cou
    !-------------------------------------------------------------------------------
    integer                                     :: n,j,m,k
    real(8)                                     :: volume_new,alpha1,alpha2
    real(8),dimension(dim)                      :: xyz0,xyz1,xyz2,v1,v2
    real(8)                                     :: t1,t2,t_cpu
    real(8), dimension(dim)                     :: xyz_old,xyz_new,xyz_vois1,xyz_vois2
    !-------------------------------------------------------------------------------
    call cpu_time(time=t1)

    do m=1,nbbul
       if ( bul(m)%mask ) then
          xyz0=0
          do n=1,bul(m)%nbele
             if (bul(m)%ele(n)%mask) then
                j=bul(m)%ele(n)%nusom(1)  ;  xyz1=bul(m)%som(j)%xyz
                xyz1(1)=min( max( xyz1(1),xmin ),xmax )
                xyz1(2)=min( max( xyz1(2),ymin ),ymax )
                xyz0=xyz0+xyz1
             end if
          end do
          xyz0=xyz0/bul(m)%nbele

          volume_new=0.d0
          do n=1,bul(m)%nbele
             if (bul(m)%ele(n)%mask) then

                j=bul(m)%ele(n)%nusom(1)  ;  xyz1=bul(m)%som(j)%xyz
                j=bul(m)%ele(n)%nusom(2)  ;  xyz2=bul(m)%som(j)%xyz

                xyz1(1)=min( max( xyz1(1),xmin ),xmax )
                xyz1(2)=min( max( xyz1(2),ymin ),ymax )
                xyz2(1)=min( max( xyz2(1),xmin ),xmax )
                xyz2(2)=min( max( xyz2(2),ymin ),ymax )

                v1 =xyz1-xyz0
                v2 =xyz2-xyz0

                volume_new=volume_new+(v1(1)*v2(2)-v1(2)*v2(1))
             end if
          end do
          volume_new=volume_new/2

          if ((bul(m)%volume_2d/volume_new)>=0.d0) then
             alpha=sqrt(bul(m)%volume_2d/volume_new)

             if (alpha>1.01.or.alpha<.99) write(14,*) "homothétie : alpha=",alpha

             do n=1,bul(m)%nbele
                if (bul(m)%ele(n)%mask) then
                   if ( bul(m)%som( bul(m)%ele(n)%nusom(1) )%notfix ) then
                      !----------------------------------------------------------------
                      k=bul(m)%ele(n)%ele_vois(1)
                      xyz_vois1(:)=bul(m)%som( bul(m)%ele(k)%nusom(1) )%xyz
                      k=bul(m)%ele(n)%ele_vois(2)
                      xyz_vois2(:)=bul(m)%som( bul(m)%ele(k)%nusom(1) )%xyz
                      !----------------------------------------------------------------
                      j=bul(m)%ele(n)%nusom(1)  
                      xyz_old=bul(m)%som(j)%xyz
                      bul(m)%som(j)%xyz = xyz0  + alpha * (bul(m)%som(j)%xyz-xyz0)
                      xyz_new=bul(m)%som(j)%xyz
                      !------------------------phase update---------------------------------
                      !--------------------------u phase------------------------------------
                      !-----------------------first triangle--------------------------------
                      call phase_update(xyz_vois1,xyz_old,xyz_new,phase_u,sx,sy,ex+1,ey,dx,dy,(xmin-dx/2),ymin,cou)
                      !-----------------------second triangle-------------------------------
                      call phase_update(xyz_old,xyz_vois2,xyz_new,phase_u,sx,sy,ex+1,ey,dx,dy,(xmin-dx/2),ymin,cou)
                      !--------------------------v phase------------------------------------
                      !-----------------------first triangle--------------------------------
                      call phase_update(xyz_vois1,xyz_old,xyz_new,phase_v,sx,sy,ex,ey+1,dx,dy,xmin,(ymin-dy/2),cou)
                      !-----------------------second triangle-------------------------------
                      call phase_update(xyz_old,xyz_vois2,xyz_new,phase_v,sx,sy,ex,ey+1,dx,dy,xmin,(ymin-dy/2),cou)
                      !--------------------------P phase------------------------------------
                      !-----------------------first triangle--------------------------------
                      call phase_update(xyz_vois1,xyz_old,xyz_new,phase_Pres,sx,sy,ex,ey,dx,dy,xmin,ymin,cou)
                      !-----------------------second triangle-------------------------------
                      call phase_update(xyz_old,xyz_vois2,xyz_new,phase_Pres,sx,sy,ex,ey,dx,dy,xmin,ymin,cou)
                      !----------------------------------------------------------------------
                   end if
                end if
             end do
          else 
             print*,'volume negatif'
             write(18,'(1pe9.2,1pe16.9)') nt*dt,(bul(m)%volume_2d/volume_new)
             exit
          end if
       end if
    end do
    call cpu_time(time=t2)
    t_cpu=t2-t1
  end subroutine conservation_volume_2d
  !*****************************************************************************************
  !*****************************************************************************************
  
  !*****************************************************************************************
  !*****************************************************************************************
  subroutine interpolation_method(u,v,pres,xyz,phase_u,phase_v,phase_Pres,vp,Erreur_Linfty,&
       nt,gradu1,gradu2,P1,P2,kappa,n_node)
    !***************************************************************************************
    implicit none 
    !***************************************************************************************
    !Global variables
    !***************************************************************************************
    real(8), dimension(:,:,:),allocatable,intent(in)    :: u,v,pres
    logical, dimension(:,:,:),allocatable,intent(in)    :: phase_u,phase_v,phase_Pres
    real(8), dimension(dim), intent(in)                 :: xyz,n_node
    real(8), dimension(dim), intent(out)                :: vp
    real(8),intent(inout)                               :: Erreur_Linfty
    integer, intent(in)                                 :: nt
    real(8), dimension(dim,dim), intent(out)            :: gradu1,gradu2
    real(8), intent(out)                                :: P1,P2
    real(8), intent(in)                                 :: kappa
    !***************************************************************************************
    ! FT_interp = 1 bilinear
    ! FT_interp = 2 PERM
    ! FT_interp = 3 Peskin
    ! FT_interp = 4 jump relations
    !***************************************************************************************
    select case (FT_interp)
    case (1) 
       call bilinear_interpolation(u,v,xyz,vp,gradu1,gradu2)
    case (2) 
       call perm_interpolation(u,v,xyz,vp)
    case (3) 
       call peskin_interpolation(u,v,xyz,vp)
    case (4) 
       call interpolation_with_jump_relation_conditions(u,v,pres,xyz,phase_u,phase_v,phase_Pres,&
            sx,sy,ex,ey,dx,dy,xmin,ymin,vp,Erreur_Linfty,nt,gradu1,gradu2,P1,P2,kappa,n_node)
    case default
       write(*,*) 'Warning wrong interpolation scheme for front-tracking'
    end select
    !***************************************************************************************
  end subroutine interpolation_method
  !*****************************************************************************************
  !*****************************************************************************************

  !*****************************************************************************************
  !*****************************************************************************************
  subroutine bilinear_interpolation(u,v,xyz,vp,gradu1,gradu2)
    !***************************************************************************************
    IMPLICIT NONE  
    !***************************************************************************************
    !Global variables
    !***************************************************************************************
    real(8), dimension(:,:,:),allocatable,intent(in)  :: u,v
    real(8), dimension(dim), intent(in)               :: xyz
    real(8), dimension(dim) ,intent(out)              :: vp
    real(8), dimension(dim,dim) ,intent(out)          :: gradu1,gradu2

    !real(8), dimension(:,:),allocatable,intent(inout) :: gradu1,gradu2
    !***************************************************************************************
    !Local variables
    !***************************************************************************************
    integer                 :: i,j
    real(8), dimension(dim) :: qvx,qvy
    real(8)                 :: t1,t2,t_cpu
    !***************************************************************************************
    !-----------------------Bilinear interpolation----------------------------------------
    call cpu_time(time=t1)
    !----------------------------x velocity----------------------------------------------- 
    i=floor( (xyz(1) -(xmin-dx/2) )/dx )
    j=floor( (xyz(2) - ymin)       /dy )
    qvx(1)=(xyz(1)-(dx*i-dx/2) )/dx
    qvx(2)=(xyz(2)- dy*j)       /dy 
    !----------------------------Bilinear interpolation------------------------------------
    vp(1)=u(i,j,1)*(1-qvx(1))*(1-qvx(2))+u(i+1,j,1)*qvx(1)*(1-qvx(2))+ &
         u(i,j+1,1)*qvx(2)*(1-qvx(1))+u(i+1,j+1,1)*qvx(1)*qvx(2)
    !----------------------------y velocity----------------------------------------------- 
    i=floor( (xyz(1)- xmin)       /dx )
    j=floor( (xyz(2)-(ymin-dy/2) )/dy )
    qvy(1)=(xyz(1)- dx*i)       /dx 
    qvy(2)=(xyz(2)-(dy*j-dy/2) )/dy
    !----------------------------Bilinear interpolation------------------------------------
    vp(2)=v(i,j,1)*(1-qvy(1))*(1-qvy(2))+v(i+1,j,1)*qvy(1)*(1-qvy(2))+ &
         v(i,j+1,1)*qvy(2)*(1-qvy(1))+v(i+1,j+1,1)*qvy(1)*qvy(2)
    !-----------------------------Gradient calculation-------------------------------------
    gradu1=0
    gradu2=0
    !--------------------------------------------------------------------------------------
    call cpu_time(time=t2)
    t_cpu=t2-t1
  end subroutine bilinear_interpolation
  !*****************************************************************************************
  !*****************************************************************************************
  subroutine perm_interpolation(u,v,xyz,vp)
    !***************************************************************************************
    IMPLICIT NONE
    !***************************************************************************************
    !Global variables
    !***************************************************************************************
    real(8), dimension(dim), intent(in)               :: xyz
    real(8), dimension(:,:,:), allocatable,intent(in) :: u,v
    real(8), dimension(dim) ,intent(out)              :: vp
    !***************************************************************************************
    !Local variables
    !***************************************************************************************
    integer               :: i,j,num
    real(8), dimension(2) :: R
    real(8)               :: DUS,DUN,DVW,DVE,DUSS,DUNN,DVWW,DVEE
    real(8)               :: DUSCHAP,DUNCHAP,DVECHAP,DVWCHAP
    real(8)               :: DIV1,DIV2,DIV3,DIV4,DIV5,DIV6,DIV7,DIV8,DIV9,DIVEX1,DIVEX2,DIVEX3,DIVEX4
    real(8)               :: VITTEXU1,VITTEXU2,VITTEXU3,VITTEXU4,VITTEXU5,VITTEXU6,VITTEXU7,VITTEXU8
    real(8)               :: VITTEXV1,VITTEXV2,VITTEXV3,VITTEXV4,VITTEXV9,VITTEXV10,VITTEXV11,VITTEXV12
    real(8)               :: VITTUE,VITTUW,VITTVS,VITTVN
    real(8)               :: VITEXU1,VITEXU2,VITEXU3,VITEXU4,VITEXV1,VITEXV2,VITEXV3,VITEXV4
    real(8)               :: VITUS,VITUN,VITVE,VITVW
    real(8)               :: DELTA1,DELTA2,DELTA3,DELTA4,h
    real(8)               :: t1,t2,t_cpu
    !***************************************************************************************
    call cpu_time(time=t1)
    H=dx
    !---------------------------velocity interpolation-------------------------------------- 

    i=( xyz(1)-(xmin-dx/2) )/dx
    j=( xyz(2)-(ymin-dy/2) )/dy
    !-----------------------------local coordinates-----------------------------------------
    R(1)=(xyz(1)-i*dx)/dx+0.5d0
    R(2)=(xyz(2)-j*dy)/dy+0.5d0
    !---------------------------Divergence discretization ----------------------------------
    DIV1=(u(i+1,j,1)-u(i,j,1)+v(i,j+1,1)-v(i,j,1))/H
    DIV2=(u(i,j,1)-u(i-1,j,1)+v(i-1,j+1,1)-v(i-1,j,1))/H
    DIV3=(u(i+1,j-1,1)-u(i,j-1,1)+v(i,j,1)-v(i,j-1,1))/H
    DIV4=(u(i,j-1,1)-u(i-1,j-1,1)+v(i-1,j,1)-v(i-1,j-1,1))/H

    DIV5=(u(i+2,j,1)-u(i+1,j,1)+v(i+1,j+1,1)-v(i+1,j,1))/H
    DIV6=(u(i+2,j-1,1)-u(i+1,j-1,1)+v(i+1,j,1)-v(i+1,j-1,1))/H

    DIV7=(u(i+1,j+1,1)-u(i,j+1,1)+v(i,j+2,1)-v(i,j+1,1))/H
    DIV8=(u(i,j+1,1)-u(i-1,j+1,1)+v(i-1,j+2,1)-v(i-1,j+1,1))/H

    DIV9=(u(i+2,j+1,1)-u(i+1,j+1,1)+v(i+1,j+2,1)-v(i+1,j+1,1))/H
    !---------------------------Divergence interpolation at nodes---------------------------
    !***************************************


    !! DIVERGENCE INTERPOLEE AUX NOEUDS 

    DIVEX1=H*0.25D0*(DIV1+DIV2+DIV3+DIV4)
    DIVEX2=H*0.25D0*(DIV5+DIV1+DIV6+DIV3)
    DIVEX3=H*0.25D0*(DIV7+DIV8+DIV1+DIV2)
    DIVEX4=H*0.25D0*(DIV9+DIV7+DIV5+DIV1)

    !******************************************  
    !!VITESSE TEMPORAIRE AUX NOEUDS

    !!POUR LA COMPOSANTE U

    VITTEXU1=0.5D0*(U(I,J,1)+U(I,J-1,1))
    VITTEXU2=0.5D0*(U(I+1,J,1)+U(I+1,J-1,1))
    VITTEXU3=0.5D0*(U(I,J+1,1)+U(I,J,1))
    VITTEXU4=0.5D0*(U(I+1,J+1,1)+U(I+1,J,1))
    VITTEXU5=0.5D0*(U(I-1,J,1)+U(I-1,J-1,1))
    VITTEXU6=0.5D0*(U(I+2,J,1)+U(I+2,J-1,1))
    VITTEXU7=0.5D0*(U(I-1,J+1,1)+U(I-1,J,1))
    VITTEXU8=0.5D0*(U(I+2,J+1,1)+U(I+2,J,1))

    !!POUR LA COMPOSANTE V 

    VITTEXV1=0.5D0*(V(I,J,1)+V(I-1,J,1))
    VITTEXV2=0.5D0*(V(I+1,J,1)+V(I,J,1))
    VITTEXV3=0.5D0*(V(I,J+1,1)+V(I-1,J+1,1))
    VITTEXV4=0.5D0*(V(I+1,J+1,1)+V(I,J+1,1))
    VITTEXV9=0.5D0*(V(I,J-1,1)+V(I-1,J-1,1))
    VITTEXV10=0.5D0*(V(I+1,J-1,1)+V(I,J-1,1))
    VITTEXV11=0.5D0*(V(I,J+2,1)+V(I-1,J+2,1))
    VITTEXV12=0.5D0*(V(I+1,J+2,1)+V(I,J+2,1))

    !*****************************************

    !! VITESSE TEMPORAIRE SUR LES FACES

    !! POUR LA COMPOSANTE U

    VITTUE=0.5D0*(VITTEXU4+VITTEXU2)
    VITTUW=0.5D0*(VITTEXU3+VITTEXU1)

    !! POUR LA COMPOSANTE V 

    VITTVS=0.5D0*(VITTEXV2+VITTEXV1)
    VITTVN=0.5D0*(VITTEXV4+VITTEXV3)

    !***************************************

    !! VITESSE AUX NOEUDS

    !!POUR LA COMPOSANTE U

    VITEXU1=VITTEXU1+U(I,J,1)-VITTUW
    VITEXU2=VITTEXU2+U(I+1,J,1)-VITTUE
    VITEXU3=VITTEXU3+U(I,J,1)-VITTUW
    VITEXU4=VITTEXU4+U(I+1,J,1)-VITTUE

    !!POUR LA COMPOSANTE V

    VITEXV1=VITTEXV1+V(I,J,1)-VITTVS
    VITEXV2=VITTEXV2+V(I,J,1)-VITTVS
    VITEXV3=VITTEXV3+V(I,J+1,1)-VITTVN
    VITEXV4=VITTEXV4+V(I,J+1,1)-VITTVN


    !*******************************************  
    !!VITESSE SUR LES FACES

    !!POUR LA COMPOPSANTE U

    VITUS=0.5D0*(VITEXU2+VITEXU1)
    VITUN=0.5D0*(VITEXU4+VITEXU3)


    !!POUR LA COMPOSANTE V 


    VITVE=0.5D0*(VITEXV4+VITEXV2)
    VITVW=0.5D0*(VITEXV3+VITEXV1)


    !*************************************       

    !!ORDRE SLOPES DEGRÉ 1

    DUS=VITEXU2-VITEXU1
    DUN=VITEXU4-VITEXU3
    DVW=VITEXV3-VITEXV1
    DVE=VITEXV4-VITEXV2

    !*************************************  

    !!ORDRE SLOPES DEGRÉ 2


    !PREMIER TERME DU SLOPE: DELTA CHAPEAU
    DUSCHAP=0.5D0*(VITTEXU6-VITTEXU2-VITTEXU1+VITTEXU5)
    DUNCHAP=0.5D0*(VITTEXU8-VITTEXU4-VITTEXU3+VITTEXU7)
    DVWCHAP=0.5D0*(VITTEXV11-VITTEXV3-VITTEXV1+VITTEXV9)
    DVECHAP=0.5D0*(VITTEXV12-VITTEXV4-VITTEXV2+VITTEXV10)


    !DEUXIEME TERME DU SLOPE: DELTA 
    DELTA1=0.25D0*(-3.D0*DIVEX1+4.D0*DVW-3.D0*DUSCHAP-DVWCHAP-DIVEX3-DUNCHAP+3.D0*DIVEX2&
         -4.D0*DVE+DVECHAP+DIVEX4)
    DELTA2=0.25D0*(-DIVEX1+4.D0*DVW-DUSCHAP+DVWCHAP-3.D0*DIVEX3-3.D0*DUNCHAP+DIVEX2-4.D0*DVE& 
         -DVECHAP+3.D0*DIVEX4)
    DELTA3=0.25D0*(-3.D0*DIVEX1+4.D0*DUS-DUSCHAP-3.D0*DVWCHAP+3.D0*DIVEX3-4.D0*DUN+DUNCHAP&
         -DIVEX2-DVECHAP+DIVEX4)
    DELTA4=0.25D0*(-DIVEX1+4.D0*DUS+DUSCHAP-DVWCHAP+DIVEX3-4.D0*DUN-DUNCHAP-3.D0*DIVEX2&
         -3.D0*DVECHAP+3.D0*DIVEX4)

    !SLOPE DEGREE 2

    DUSS=DUSCHAP+DELTA1
    DUNN=DUNCHAP+DELTA2
    DVWW=DVWCHAP+DELTA3
    DVEE=DVECHAP+DELTA4

    !! INTERPOLATED VELOCITY
    VP(1)=(1.D0-R(2))*(VITUS+(R(1)-0.5D0)*DUS+0.5D0*((R(1)-0.5D0)**2.D0-0.25D0)*DUSS)&
         +R(2)*(VITUN+(R(1)-0.5D0)*DUN+0.5D0*((R(1)-0.5D0)**2.D0-0.25D0)*DUNN)


    VP(2)=(1.D0-R(1))*(VITVW+(R(2)-0.5D0)*DVW +0.5D0*((R(2)-0.5D0)**2.D0-0.25D0)*DVWW)&
         +R(1)*(VITVE+(R(2)-0.5D0)*DVE+0.5D0*((R(2)-0.5D0)**2.D0-0.25D0)*DVEE)
    call cpu_time(time=t2)
    t_cpu=t2-t1
    !---------------------------------------------------------------------------------
  end subroutine perm_interpolation
  !************************************************************************************
  !************************************************************************************
  subroutine peskin_interpolation(u,v,xyz,vp)
    !***************************************************************************************
    IMPLICIT NONE  
    !***************************************************************************************
    !Global variables
    !***************************************************************************************
    real(8), dimension(dim), intent(in)               :: xyz
    real(8),  dimension(:,:,:),allocatable,intent(in) :: u,v
    real(8), dimension(dim) ,intent(out)              :: vp
    !***************************************************************************************
    !Local variables
    !***************************************************************************************
    integer :: i,j,k,l,m,n,ii,jj,kk,ll
    real(8) :: q1x,q2x,q1y,q2y
    real(8) :: t1,t2,t_cpu
    !***************************************************************************************
    call cpu_time(time=t1)
    i=floor(xyz(1)-xmin          /dx)
    j=floor(xyz(2)-xmin          /dy)
    k=floor((xyz(1)-(xmin-dx/2) )/dx)
    l=floor((xyz(2)-(xmin-dy/2) )/dy)
    vp(1)=0.d0; vp(2)=0.d0
    do m=1,4
       do n=1,4

          ii=i-2+n
          jj=j-2+m
          kk=k-2+n
          ll=l-2+m

          q1x=(xyz(1)-dx*ii)*ddx
          q1y=(xyz(2)-dy*jj)*ddy
          q2x=(xyz(1)-dx*kk+dx2)*ddx
          q2y=(xyz(2)-dy*ll+dy2)*ddy

          !-------------------------------------------------------------------------------
          ! integration juric ordre 2
          !-------------------------------------------------------------------------------

          vp(1)=vp(1)+pes(q2x)*pes(q1y)*u(kk,jj,1)
          vp(2)=vp(2)+pes(q1x)*pes(q2y)*v(ii,ll,1)
          !-------------------------------------------------------------------------------
          ! integration peskin ordre 1
          !-------------------------------------------------------------------------------
          !-------------------------------------------------------------------------------
       end do
    end do
    call cpu_time(time=t2)
    t_cpu=t2-t1
  end subroutine peskin_interpolation
  !************************************************************************************
  !************************************************************************************
  subroutine normal_calculation(bul,nbbul)
    !*******************************************************************************
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables 
    !-------------------------------------------------------------------------------
    type(bulle),dimension(:), allocatable,intent(inout) :: bul
    integer, intent(in)                                 :: nbbul
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    integer                               :: n,m,k
    real(8), dimension(dim)               :: xyz,xyz_vois1,xyz_vois2,n_node
    real(8)                               :: curvature_node
    !-------------------------------------------------------------------------------
    do n=1,nbbul
       if ( bul(n)%mask ) then
          do m=1,bul(n)%nbele
             if ( bul(n)%ele(m)%mask ) then
                if ( bul(n)%som( bul(n)%ele(m)%nusom(1) )%notfix ) then
                   !---------------------------position---------------------------------
                   xyz(:)=bul(n)%som( bul(n)%ele(m)%nusom(1) )%xyz(:)
                   !---------------------------neighbor position------------------------
                   k=bul(n)%ele(m)%ele_vois(1)
                   xyz_vois1(:)=bul(n)%som( bul(n)%ele(k)%nusom(1) )%xyz
                   k=bul(n)%ele(m)%ele_vois(2)
                   xyz_vois2(:)=bul(n)%som( bul(n)%ele(k)%nusom(1) )%xyz
                   !-------------------------------------------------------------------
                   call normal_node_calculation(xyz,xyz_vois1,xyz_vois2,n_node,curvature_node)
                   !-------------------------------------------------------------------
                   bul(n)%som(bul(n)%ele(m)%nusom(1))%kappa =curvature_node
                   bul(n)%som(bul(n)%ele(m)%nusom(1))%n_node=n_node
                else
                   bul(n)%som(bul(n)%ele(m)%nusom(1))%kappa =0
                   bul(n)%som(bul(n)%ele(m)%nusom(1))%n_node=0 
                end if
             end if
          end do
       end if
    end do
    !-------------------------------------------------------------------------------
  end subroutine normal_calculation
  !************************************************************************************
  !************************************************************************************
  subroutine normal_node_calculation(xyz,xyz_vois1,xyz_vois2,n_node,curvature_node)
    !**********************************************************************************
    implicit none
    !-------------------------------------------------------------------------------
    !Global variable
    !-------------------------------------------------------------------------------
    real(8), dimension(dim), intent(in) :: xyz,xyz_vois1,xyz_vois2
    real(8), dimension(dim), intent(out):: n_node
    real(8), intent(out)                :: curvature_node
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    real(8):: a,aa,b,bb,c,cc
    real(8),dimension(dim):: xyz_M1,xyz_M2,n_ele1,n_ele,xyzI
    !-------------------------------------------------------------------------------
    xyz_M1=(xyz_vois1+xyz)/2.d0
    n_ele1(1)=+(xyz(2)-xyz_vois1(2))
    n_ele1(2)=-(xyz(1)-xyz_vois1(1))
    n_ele1=n_ele1/sqrt(dot_product(n_ele1,n_ele1))

    xyz_M2=(xyz+xyz_vois2)/2.d0
    n_ele(1)=+(xyz_vois2(2)-xyz(2))
    n_ele(2)=-(xyz_vois2(1)-xyz(1))
    n_ele=n_ele/sqrt(dot_product(n_ele,n_ele))
    !-------------------------coef determination------------------------------------
    b=-n_ele(1) ; a=n_ele(2); c=xyz_M2(2)*n_ele(1)-xyz_M2(1)*n_ele(2)
    bb=-n_ele1(1) ; aa=n_ele1(2); cc=xyz_M1(2)*n_ele1(1)-xyz_M1(1)*n_ele1(2)
    if (abs(aa*b-bb*a)<=1.d-15) then
       n_node=n_ele
       curvature_node=1.d-15
       !print*,'none intersection'
    else
       call find_intersection_point_droites(a,b,c,aa,bb,cc,xyzI)
       n_node=xyz-xyzI
       n_node=n_node/sqrt(dot_product(n_node,n_node))
       curvature_node = 1.d0/sqrt( (xyzI(1)-xyz(1))*(xyzI(1)-xyz(1))+(xyzI(2)-xyz(2))*(xyzI(2)-xyz(2)) )
    end if
    if (dot_product(n_node,n_ele)<0.d0) then
       n_node=-n_node
       curvature_node = -curvature_node
    end if
  end subroutine normal_node_calculation
  !**************************************************************************************
  !**************************************************************************************
  subroutine localization_and_count_triangle(xyz,count1,count2,phase,startx,starty,endx,endy,dx0,dy0,&
       xmin0,ymin0,i,j)
    !************************************************************************************
    implicit none
    !-------------------------------------------------------------------------------
    !Global variable
    !-------------------------------------------------------------------------------
    real(8), dimension(dim), intent(in)              :: xyz
    logical, dimension(:,:,:),allocatable,intent(in) :: phase
    real(8), intent(in)                              :: dx0,dy0,xmin0,ymin0
    integer, intent(in)                              :: startx,starty,endx,endy
    integer, intent(inout)                           :: count1,count2
    integer, intent(out)                             :: i,j
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    integer                                   :: ii,jj
    !------------------------------node localization--------------------------------
    i=floor((xyz(1)-xmin0)/dx0)
    j=floor((xyz(2)-ymin0)/dy0)
    !write(56,'(I0,2x,I0,(2x,F18.15))') i,j,xyz
    do jj=max(starty,j-1),min(j+2,endy)
       do ii=max(startx,i-1),min(i+2,endx)
          if (phase(ii,jj,1)) then
             count1=count1+1
          else
             count2=count2+1
             cycle
          end if
       end do
    end do
    !write(56,*) count1,count2
    !-------------------------------------------------------------------------------
  end subroutine localization_and_count_triangle
  !**************************************************************************************
  !**************************************************************************************
  subroutine phase_localization_triangle(xyz,phase,count1,count2,dx0,dy0,xmin0,ymin0,i,j,&
       dist1,indi1,indj1,xu1,dist2,indi2,indj2,xu2,startx,starty,endx,endy)
    !************************************************************************************
    implicit none
    !-------------------------------------------------------------------------------
    !Global variable
    !-------------------------------------------------------------------------------
    real(8), dimension(dim), intent(in)               :: xyz
    logical, dimension(:,:,:),allocatable, intent(in) :: phase
    real(8), intent(in)                               :: dx0,dy0,xmin0,ymin0
    integer, intent(in)                               :: startx,starty,endx,endy
    integer, intent(inout)                            :: count1,count2
    integer, intent(in)                               :: i,j
    integer, dimension(:),allocatable, intent(inout)  :: indi1,indj1,indi2,indj2
    real(8), dimension(:),allocatable, intent(inout)  :: dist1,dist2
    real(8), dimension(:,:),allocatable, intent(inout):: xu1,xu2
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    integer                                          :: ii,jj
    !-------------------------------------------------------------------------------
    do jj=max(starty,j-1),min(j+2,endy)
       do ii=max(startx,i-1),min(i+2,endx)
          if (phase(ii,jj,1)) then
             count1=count1+1
             xu1(count1,1)=dx0*ii+xmin0
             xu1(count1,2)=dy0*jj+ymin0
             dist1(count1)=(xyz(1)-xu1(count1,1))*(xyz(1)-xu1(count1,1))+(xyz(2)-xu1(count1,2))*(xyz(2)-xu1(count1,2))
             indi1(count1)=ii
             indj1(count1)=jj
          else 
             count2=count2+1
             xu2(count2,1)=dx0*ii+xmin0
             xu2(count2,2)=dy0*jj+ymin0
             dist2(count2)=(xyz(1)-xu2(count2,1))*(xyz(1)-xu2(count2,1))+(xyz(2)-xu2(count2,2))*(xyz(2)-xu2(count2,2))
             indi2(count2)=ii
             indj2(count2)=jj
          end if
       end do
    end do
    !-------------------------------------------------------------------------------
  end subroutine phase_localization_triangle
  !**************************************************************************************
  !**************************************************************************************
  subroutine node_classification(xyz,count1,dist1,xu1,minlocat,minl,tabvec) 
    !************************************************************************************
    implicit none
    !-------------------------------------------------------------------------------
    !Global variable
    !-------------------------------------------------------------------------------
    real(8), dimension(dim), intent(in)                 :: xyz
    integer, intent(in)                                 :: count1
    real(8), dimension(:),allocatable, intent(inout)    :: dist1
    real(8), dimension(:,:),allocatable, intent(inout)  :: xu1
    integer, dimension(:),allocatable, intent(inout)    :: minlocat
    real(8), dimension(:),allocatable, intent(inout)    :: minl
    real(8), dimension(:,:),allocatable, intent(inout)  :: tabvec
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    integer :: l,n,loc
    !-------------------------------------------------------------------------------
    minl(0)=0
    do n=1,count1
       !print*,dist1
       minl(n)    = minval(dist1,dist1>minl(n-1))
       minlocat(n)= minloc(dist1,1, dist1>0)
       dist1( minlocat(n) )=0
       !print*,'min'
       !print*,n,minlocat(n)
       tabvec(n,1)=xu1(minlocat(n),1)-xyz(1)
       tabvec(n,2)=xu1(minlocat(n),2)-xyz(2)
    end do
    !-------------------------------------------------------------------------------
  end subroutine node_classification
  !**************************************************************************************
  !**************************************************************************************
  subroutine triangle_calculation(u,xyz,n_node,phase,u1,u2,x1,x2,mask_u,Q,limit_Q,count1,dist1,indi1,indj1,xu1,&
       minlocat,tabvec,num1,num2)
    !************************************************************************************
    implicit none
    !-------------------------------------------------------------------------------
    !Global variable
    !-------------------------------------------------------------------------------
    real(8), dimension(dim), intent(in)              :: xyz
    real(8), dimension(:,:,:),allocatable,intent(in) :: u
    logical, dimension(:,:,:),allocatable,intent(in) :: phase
    real(8), dimension(dim), intent(in)              :: n_node
    real(8), dimension(dim), intent(inout)           :: x1,x2
    real(8), intent(inout)                           :: u1,u2
    logical, intent(inout)                           :: mask_u
    real(8), intent(out)                             :: Q
    integer, intent(in)                              :: count1
    real(8), intent(in)                              :: limit_Q
    integer, dimension(:),allocatable, intent(in)    :: indi1,indj1,minlocat
    real(8), dimension(:),allocatable, intent(in)    :: dist1
    real(8), dimension(:,:),allocatable, intent(in)  :: xu1,tabvec
    integer, intent(inout)                           :: num1,num2
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    integer                                   :: num,n
    real(8)                                   :: l1,l2,l3,s,Am2,limit_length
    real(8), dimension(count1)                :: det
    !------------------------------------------------------------------------------
    !------------------------------------triangle----------------------------------
    !------------------------------------------------------------------------------
!!$    write(56,*) 'count u1 u2',count1
    limit_length=0.25_8*min(dx,dy)
    num=max(num1,num2)
    if (mod(num,2)==0) then
       num1=min(num1+2,count1)
    else
       num2=min(num2+2,count1)
    end if
    num=max(num1,num2)
    !write(56,*) 'count',count1
    !write(56,*) num,num1,num2

    det(num-1)=tabvec(num-1,1)*n_node(2)-tabvec(num-1,2)*n_node(1)
    bc1:do n =num,count1
       x1=(/ xu1(minlocat(num1),1),xu1(minlocat(num1),2) /)
       x2=(/ xu1(minlocat(num2),1),xu1(minlocat(num2),2) /)
       det(n)=tabvec(n,1)*n_node(2)-tabvec(n,2)*n_node(1)
       !--------------Calculation of the quality factor triangle------------------
       l1=sqrt((xyz(1)-x1(1))*(xyz(1)-x1(1))+(xyz(2)-x1(2))*(xyz(2)-x1(2)))
       l2=sqrt((xyz(1)-x2(1))*(xyz(1)-x2(1))+(xyz(2)-x2(2))*(xyz(2)-x2(2)))
       l3=sqrt(( x1(1)-x2(1))*( x1(1)-x2(1))+( x1(2)-x2(2))*( x1(2)-x2(2)))
       s=0.5_8*(l1+l2+l3)
       Am2=s*(s-l1)*(s-l2)*(s-l3)
       Q=(8*Am2)/(s*l1*l2*l3)
       !--------------------------------------------------------------------------
       if ( det(num1)*det(num2)<0 ) then
          if (  Q>limit_Q .and. l1> limit_length ) then
!!$             write(56,*) 'in'
!!$             write(56,*) 'det', det(num1)*det(num2)
             u1=u(indi1(minlocat(num1)),indj1(minlocat(num1)),1)
             x1=(/ xu1(minlocat(num1),1),xu1(minlocat(num1),2) /)
             u2=u(indi1(minlocat(num2)),indj1(minlocat(num2)),1)
             x2=(/ xu1(minlocat(num2),1),xu1(minlocat(num2),2) /)
!!$             write(56,*) '----vitesse u1 u2-----'
!!$             write(56,*) u1,phase(indi1(minlocat(num1)),indj1(minlocat(num1)),1), mask_u
!!$             write(56,*) x1
!!$             write(56,*) u2,phase(indi1(minlocat(num2)),indj1(minlocat(num2)),1)
!!$             write(56,*) x2
!!$             write(56,*) Q
             exit bc1
          else
!!$             write(56,*) 'out'
             if (n<count1) then
                if ( mod(n,2)==0) then
                   num1=n+1
                else
                   num2=n+1
                end if
                cycle bc1
             else
!!$                write(56,*) 'problem not enough velocity node'
                mask_u=.true.
                exit bc1
             end if
             !do while (n<count1)
             !   if ( mod(n,2)==0) then
             !      num1=n+1
             !   else
             !      num2=n+1
             !   end if
             !   cycle bc1
             !end do
             !write(56,*) 'problem not enough velocity node'
             !mask_u=.true.
             !exit bc1
          end if
       else
!!$          write(56,*) 'out'
          if (n<count1) then
             if ( mod(n,2)==0) then
                num1=n+1
             else
                num2=n+1
             end if
             cycle bc1
          else
!!$             write(56,*) 'problem not enough velocity node'
             mask_u=.true.
             exit bc1
          end if
       end if
    end do bc1
    !---------------------------------------------------------------------------------
  end subroutine triangle_calculation
  !**************************************************************************************
  !**************************************************************************************
  subroutine pressure_interpolation(pres,xyz,n_node,count1,P,mask_p,indi1,indj1,xp1,minlocat,tabvec)
    !************************************************************************************
    implicit none
    !-------------------------------------------------------------------------------
    !Global variable
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:),allocatable,intent(in)   :: pres
    real(8), dimension(dim), intent(in)                :: xyz,n_node
    real(8), intent(inout)                             :: P
    logical, intent(inout)                             :: mask_p
    integer, dimension(:),allocatable, intent(in)      :: indi1,indj1,minlocat
    integer, intent(in)                                :: count1
    real(8), dimension(:,:),allocatable, intent(in)    :: xp1,tabvec
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    integer                  :: num1,num2,n
    real(8),dimension(count1):: det
    real(8)                  :: P1,P2,alpha,beta,a,aa,b,bb,c,cc
    real(8),dimension(dim)   :: x1,x2,t_pres,xI
    !-------------------------------------------------------------------------------
    num1=1
    num2=num1+1
    bc1:do n =num2,count1
       det(num1)=tabvec(num1,1)*n_node(2)-tabvec(num1,2)*n_node(1)
       det(num2)=tabvec(num2,1)*n_node(2)-tabvec(num2,2)*n_node(1)
       if ( det(num1)*det(num2)<0 ) then
          !------------------------------------------------------
          P1=pres(indi1(minlocat(num1)),indj1(minlocat(num1)),1)
          P2=pres(indi1(minlocat(num2)),indj1(minlocat(num2)),1)
          !------------------------------------------------------
          x1=(/ xp1(minlocat(num1),1),xp1(minlocat(num1),2) /)
          x2=(/ xp1(minlocat(num2),1),xp1(minlocat(num2),2) /)
          t_pres = x1-x2
          t_pres=t_pres/sqrt(dot_product(t_pres,t_pres))
          !-------------------------coef determination-----------
          b =-t_pres(1) ; a =t_pres(2) ; c = x1(2)*t_pres(1)- x1(1)*t_pres(2)
          bb=-n_node(1) ; aa=n_node(2) ; cc=xyz(2)*n_node(1)-xyz(1)*n_node(2) 
          if (abs(aa*b-bb*a)<=1.d-15) then
             if (n<count1) then
!!$                if ( mod(n,2)==0) then
!!$                   num1=n+1
!!$                else
!!$                   num2=n+1
!!$                end if
                cycle bc1
             else
                mask_p=.true.
                write(55,*)'probleme',mask_p
                write(55,*) count1,num1,num2
                exit bc1
             end if
          else
             call find_intersection_point_droites(a,b,c,aa,bb,cc,xI)
          end if
          !------------------------------------------------------
          alpha=sqrt((xI(1)-x1(1))*(xI(1)-x1(1))+(xI(2)-x1(2))*(xI(2)-x1(2)))
          beta =sqrt((xI(1)-x2(1))*(xI(1)-x2(1))+(xI(2)-x2(2))*(xI(2)-x2(2)))
          P=( alpha*P2+beta*P1 )/( alpha+beta )
          exit bc1
       else
          if (n<count1) then
             if ( mod(n,2)==0) then
                num1=n+1
             else
                num2=n+1
             end if
             cycle bc1
          else
             mask_p=.true.
             write(55,*)'probleme',mask_p
             write(55,*) count1,num1,num2
             exit bc1
          end if
       end if
    end do bc1
    !-------------------------------------------------------------------------------
  end subroutine pressure_interpolation
  !***********************************************************************************************
  !**************************************************************************************
  subroutine interpolation_with_jump_relation_conditions(u,v,pres,xyz,phase_u,phase_v,phase_Pres,&
       startx,starty,endx,endy,dx0,dy0,xmin0,ymin0,vp,Erreur_Linfty,nt,gradu1,gradu2,P1,P2,kappa,n_node)
    !*******************************************************************************
    use mod_thermophys
    !*******************************************************************************
    implicit none
    !-------------------------------------------------------------------------------
    !Global variable
    !-------------------------------------------------------------------------------
    real(8), dimension(dim), intent(in)                :: xyz,n_node
    real(8), dimension(:,:,:),allocatable,intent(in)   :: u,v,pres
    logical, dimension(:,:,:),allocatable,intent(in)   :: phase_u,phase_v,phase_Pres
    integer, intent(in)                                :: startx,starty,endx,endy
    real(8), intent(in)                                :: dx0,dy0,xmin0,ymin0,kappa
    real(8), dimension(dim),intent(out)                :: vp
    real(8),intent(inout)                              :: Erreur_Linfty
    integer, intent(in)                                :: nt
    real(8), dimension(dim,dim) ,intent(out)           :: gradu1,gradu2
    real(8), intent(out)                               :: P1,P2
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    integer                             :: m,iu,ju,iv,jv,countu1,countu2,countv1,countv2,nu,count,mincond
    integer                             :: countp1,countp2,ip,jp
    integer                             :: numu1,numu2,numv1,numv2,numu1bis,numu2bis,numv1bis,numv2bis
    logical                             :: mask_u,mask_v,first_pass,infinity,change,mask_p
    real(8), dimension(dim)             :: n_pr,t_pr,vp_temp,ve,BB
    real(8), dimension(dim,dim)         :: AA
    real(8), dimension(dim)             :: x1,x2,x3,x4,y1,y2,y3,y4,gradu_exact
    real(8)                             :: u1,u2,u3,u4,v1,v2,v3,v4
    real(8)                             :: mu1,mu2,sigma,Erreur,Erreurux,Erreuruy,l1,l2
    integer, dimension(:),allocatable   :: indiu1,indju1,indiu2,indju2,indiv1,indjv1,indiv2,indjv2
    integer, dimension(:),allocatable   :: indip1,indjp1,indip2,indjp2,minlocatp1,minlocatp2
    integer, dimension(:),allocatable   :: minlocatu1,minlocatu2,minlocatv1,minlocatv2
    real(8), dimension(:),allocatable   :: distu1,distu2,distv1,distv2,minlu1,minlu2,minlv1,minlv2
    real(8), dimension(:),allocatable   :: distp1,distp2,minlp1,minlp2
    real(8), dimension(:,:),allocatable :: xu1,xu2,xv1,xv2,tabvecu1,tabvecu2,tabvecv1,tabvecv2
    real(8), dimension(:,:),allocatable :: xp1,xp2,tabvecp1,tabvecp2
    real(8)                             :: limit_Q,Qu_up,Qu_down,Qv_up,Qv_down,limit_Quv,limit_cond,max_cond
    real(8)                             :: alpha_u1x,beta_u1x,gamma_u1x,alpha_u1y,beta_u1y,gamma_u1y
    real(8)                             :: alpha_v1x,beta_v1x,gamma_v1x,alpha_v1y,beta_v1y,gamma_v1y
    real(8)                             :: alpha_u2x,beta_u2x,gamma_u2x,alpha_u2y,beta_u2y,gamma_u2y
    real(8)                             :: alpha_v2x,beta_v2x,gamma_v2x,alpha_v2y,beta_v2y,gamma_v2y
    real(8)                             :: C_u1x,C_u1y,C_v1x,C_v1y,C_u2x,C_u2y,C_v2x,C_v2y
    real(8)                             :: A,B,C,D,E,F,G,H
    real(8)                             :: det,detu,detv,deltaP
    real(8), dimension(:),allocatable   :: cond
    real(8), dimension(:,:),allocatable :: vp_cond
    !---------------------------------------------------------------------------------------------
    !!$    write(56,*) 'n=',n
    P1=0; P2=0; mu1=1; mu2=1; sigma=0
    mu1=fluids(1)%mu
    mu2=fluids(2)%mu
    !sigma=fluids(2)%sigma
    !-----------------------------------PRESSURE CALCULATION--------------------------------------
    countp1=0
    countp2=0
    call localization_and_count_triangle(xyz,countp1,countp2,phase_Pres,startx,starty,endx,endy,dx0,dy0,&
         xmin0,ymin0,ip,jp)
    !print*, countp1,countp2
    if ( countp1<=1 .or. countp2<=1  ) then
       P1=0 ; P2=0
       !----------------------attention trouver un solution------------------------------
!!$       write(78,*) '----dedans----------'
!!$       write(78,*) nt,P1,P2
!!$       write(78,*) countp1,countp2
    else
       !***************************************pressure********************************************
       allocate(distp1(countp1),indip1(countp1),indjp1(countp1),xp1(countp1,dim))
       allocate(distp2(countp2),indip2(countp2),indjp2(countp2),xp2(countp2,dim))
       countp1=0
       countp2=0
       !----------------------------------phase attribution of pressure----------------------------
       call phase_localization_triangle(xyz,phase_Pres,countp1,countp2,dx0,dy0,xmin0,ymin0,ip,jp,&
            distp1,indip1,indjp1,xp1,distp2,indip2,indjp2,xp2,startx,starty,endx,endy)
       !print*, countp1,countp2
       !----------------------------------------node p classification--------------------------------
       allocate(minlocatp1(countp1),minlp1(0:countp1),tabvecp1(countp1,dim))
       allocate(minlocatp2(countp2),minlp2(0:countp2),tabvecp2(countp2,dim))
       call node_classification(xyz,countp1,distp1,xp1,minlocatp1,minlp1,tabvecp1) 
       call node_classification(xyz,countp2,distp2,xp2,minlocatp2,minlp2,tabvecp2) 
       !write(55,*) indip2
       !write(55,*) indjp2
       !-----------------------------------pressure interpolation------------------------------------
       !--------------------------------------side 1-------------------------------------------------
       call pressure_interpolation(pres,xyz,n_node,countp1,P1,mask_p,indip1,indjp1,xp1,minlocatp1,tabvecp1)
       !--------------------------------------side 2-------------------------------------------------
       call pressure_interpolation(pres,xyz,n_node,countp2,P2,mask_p,indip2,indjp2,xp2,minlocatp2,tabvecp2)
!!$       write(78,*) '---------------'
!!$       write(78,*) nt,P1,P2
!!$       write(78,*) countp1,countp2
       !   P1=0 ; P2=0  
       deltaP=0!P1-P2
       !---------------------------------------------------------------------------------------------
       deallocate(minlocatp1,minlp1,tabvecp1,minlocatp2,minlp2,tabvecp2)
       deallocate(distp1,indip1,indjp1,xp1,distp2,indip2,indjp2,xp2)
       !---------------------------------------------------------------------------------------------
    end if
    !*********************************************************************************************
    !-----------------------------------VELOCITY CALCULATION--------------------------------------
    countu1=0
    countu2=0
    !------------------------------------node u localization--------------------------------------
    call localization_and_count_triangle(xyz,countu1,countu2,phase_u,startx,starty,endx+1,endy,dx0,dy0,&
         (xmin0-dx0/2),ymin0,iu,ju)
    !---------------------------------------------------------------------------------------------
    countv1=0
    countv2=0
    !-----------------------------------node v localization----------------------------------------
    call localization_and_count_triangle(xyz,countv1,countv2,phase_v,startx,starty,endx,endy+1,dx0,dy0,&
         xmin0,(ymin0-dy0/2),iv,jv)
    !---------------------------------------------------------------------------------------------
    if ( countu1<=1 .or. countu2<=1 .or. countv1<=1 .or. countv2<=1 ) then
       write(55,*) 'zero velocity node'
       mask_u=.true.
       write(55,*)'bilinear interpolation',nt
       write(55,*)'n_node',n_node
       call bilinear_interpolation(u,v,xyz,vp,gradu1,gradu2)
    else 
       !***************************************u velocity********************************************
!!$       write(56,*) 'vitesse u localization'
       !---------------------------------------------------------------------------------------------
       allocate(distu1(countu1),indiu1(countu1),indju1(countu1),xu1(countu1,dim))
       allocate(distu2(countu2),indiu2(countu2),indju2(countu2),xu2(countu2,dim))
       countu1=0
       countu2=0
       !----------------------------------phase attribution of velocity------------------------------
       call phase_localization_triangle(xyz,phase_u,countu1,countu2,dx0,dy0,(xmin0-dx0/2),ymin0,iu,ju,&
            distu1,indiu1,indju1,xu1,distu2,indiu2,indju2,xu2,startx,starty,endx+1,endy)
       !---------------------------------------------------------------------------------------------
       !***************************************v velocity********************************************
!!$       write(56,*) 'vitesse v localization'
       !---------------------------------------------------------------------------------------------
       allocate(distv1(countv1),indiv1(countv1),indjv1(countv1),xv1(countv1,dim))
       allocate(distv2(countv2),indiv2(countv2),indjv2(countv2),xv2(countv2,dim))
       countv1=0
       countv2=0
       !----------------------------------phase attribution of velocity------------------------------
       call phase_localization_triangle(xyz,phase_v,countv1,countv2,dx0,dy0,xmin0,(ymin0-dy0/2),iv,jv,&
            distv1,indiv1,indjv1,xv1,distv2,indiv2,indjv2,xv2,startx,starty,endx,endy+1)
       !---------------------------------------------------------------------------------------------
       !***********************************************************************************************
       !----------------------------------------node u classification----------------------------------
       allocate(minlocatu1(countu1),minlu1(0:countu1),tabvecu1(countu1,dim))
       allocate(minlocatu2(countu2),minlu2(0:countu2),tabvecu2(countu2,dim))
       call node_classification(xyz,countu1,distu1,xu1,minlocatu1,minlu1,tabvecu1) 
       call node_classification(xyz,countu2,distu2,xu2,minlocatu2,minlu2,tabvecu2) 
       !----------------------------------------node v classification----------------------------------
       allocate(minlocatv1(countv1),minlv1(0:countv1),tabvecv1(countv1,dim))
       allocate(minlocatv2(countv2),minlv2(0:countv2),tabvecv2(countv2,dim))
       call node_classification(xyz,countv1,distv1,xv1,minlocatv1,minlv1,tabvecv1) 
       call node_classification(xyz,countv2,distv2,xv2,minlocatv2,minlv2,tabvecv2) 
       !-----------------------------------------------------------------------------------------------
       limit_Q=0.1_8  ; limit_Quv=0.6_8
       !limit_Q=1.d-5  ; limit_Quv=1.d-5

       limit_cond=5 ; max_cond=limit_cond!5
       numu1 =1 ; numu2=0 ; numv1=1 ; numv2=0
       numu1bis =1 ; numu2bis=0 ; numv1bis=1 ; numv2bis=0
       mask_u=.false. ; mask_v=.false. ; first_pass=.true. ; infinity=.false.
       nu=0

       count=max(countu1,countu2,countv1,countv2)
       allocate(cond(0:count),vp_cond(dim,0:count))
       cond=0
       vp_cond=0
       cond(nu)=10000
       !----------------------------triangles construction for interpolation----------------------------
       bc1: do while ( cond(nu) > limit_cond .or. infinity .eqv. .true. )
          nu=nu+1
          !---------------------------------------------------------------------------------------------
          if (first_pass) then
!!$             write(55,*) 'first pass'
             !***************************************u triangles*******************************************
!!$             write(56,*) 'vitesse u triangles'
             !-------------------------------------upper triangles-----------------------------------------
             call triangle_calculation(u,xyz,n_node,phase_u,u1,u2,x1,x2,mask_u,Qu_up,  limit_Q,countu1,distu1,&
                  indiu1,indju1,xu1,minlocatu1,tabvecu1,numu1,numu2)
             !--------------------------------------down triangles-----------------------------------------
             call triangle_calculation(u,xyz,n_node,phase_u,u3,u4,x3,x4,mask_u,Qu_down,limit_Q,countu2,distu2,&
                  indiu2,indju2,xu2,minlocatu2,tabvecu2,numu1bis,numu2bis)
             !***************************************v triangles*******************************************
!!$             write(56,*) 'vitesse v triangles'
             !-------------------------------------upper triangles-----------------------------------------
             call triangle_calculation(v,xyz,n_node,phase_v,v1,v2,y1,y2,mask_v,Qv_up,  limit_Q,countv1,distv1,&
                  indiv1,indjv1,xv1,minlocatv1,tabvecv1,numv1,numv2)
             !--------------------------------------down triangles-----------------------------------------
             call triangle_calculation(v,xyz,n_node,phase_v,v3,v4,y3,y4,mask_v,Qv_down,limit_Q,countv2,distv2,&
                  indiv2,indjv2,xv2,minlocatv2,tabvecv2,numv1bis,numv2bis)
             !**********************************************************************************************
             first_pass=.false. 
          else
!!$             write(55,*) 'pass',nu
             !------------------------------------------------------------------------------------------------
             change=.false.
             !------------------------------------------------------------------------------------------------
             if ( Qu_up < limit_Quv ) then
                call triangle_calculation(u,xyz,n_node,phase_u,u1,u2,x1,x2,mask_u,Qu_up,  limit_Q,countu1,distu1,&
                     indiu1,indju1,xu1,minlocatu1,tabvecu1,numu1,numu2)
                change=.true.
             end if
             !------------------------------------------------------------------------------------------------
             if ( Qu_down < limit_Quv ) then
                call triangle_calculation(u,xyz,n_node,phase_u,u3,u4,x3,x4,mask_u,Qu_down,limit_Q,countu2,distu2,&
                     indiu2,indju2,xu2,minlocatu2,tabvecu2,numu1bis,numu2bis) 
                change=.true.
             end if
             !------------------------------------------------------------------------------------------------
             if ( Qv_up < limit_Quv ) then
                call triangle_calculation(v,xyz,n_node,phase_v,v1,v2,y1,y2,mask_v,Qv_up,  limit_Q,countv1,distv1,&
                     indiv1,indjv1,xv1,minlocatv1,tabvecv1,numv1,numv2)
                change=.true.
             end if
             !------------------------------------------------------------------------------------------------
             if (Qv_down < limit_Quv ) then
                call triangle_calculation(v,xyz,n_node,phase_v,v3,v4,y3,y4,mask_v,Qv_down,limit_Q,countv2,distv2,&
                     indiv2,indjv2,xv2,minlocatv2,tabvecv2,numv1bis,numv2bis)
                change=.true.
             end if
             !------------------------------------------------------------------------------------------------
             if ( .not. change ) then
                mask_u=.true.
             end if
          end if
          write(55,*) 'q_u',Qu_up,Qu_down
          write(55,*) 'q_v',Qv_up,Qv_down
          write(55,*) change
          if (mask_u .or. mask_v) then
             !      write(55,*) 'mask vrai'
             if (nu==1) then
                write(55,*)'bilinear interpolation',nt
                call bilinear_interpolation(u,v,xyz,vp,gradu1,gradu2)
             else
                !--------------------------------Interpolated velocity----------------------------------------
                mincond=minloc(cond,1,cond>=1)
                mincond=mincond + lbound(cond,1)-1
                if ( cond(mincond) < max_cond ) then
                   vp(1)=vp_cond(1,mincond)
                   vp(2)=vp_cond(2,mincond)
                   ! gradu1(
!!$                   write(55,*) cond
!!$                   write(55,*) 'mincond',mincond
!!$                   write(55,*) vp_cond
                else
                   write(55,*)'bilinear interpolation',nt
                   call bilinear_interpolation(u,v,xyz,vp,gradu1,gradu2)
                end if
                !---------------------------------------------------------------------------------------------
             end if
             exit bc1
          else
!!$             write(55,*) '------coordonnees-------------'
!!$             write(55,*) u1,u2
!!$             write(55,*) u3,u4
!!$             write(55,*) v1,v2
!!$             write(55,*) v3,v4
!!$             write(55,*) x1, Qu_up
!!$             write(55,*) x2
!!$             write(55,*) x3, Qu_down
!!$             write(55,*) x4
!!$             write(55,*) y1, Qv_up
!!$             write(55,*) y2
!!$             write(55,*) y3, Qv_down
!!$             write(55,*) y4
!!$             write(55,*) '------------------------------'
             !---------------------------------------------------------------------------------------------
             !-----------------------u constants calculation-----------------------------------------------
             !-----------------------------------------side 1----------------------------------------------
             alpha_u1x = -(x2(2) -x1(2)) /( (x1(1) -xyz(1))*(x2(2) -x1(2)) -(x2(1)-x1(1)) *(x1(2) -xyz(2)) )
             beta_u1x  = -(xyz(2)-x2(2)) /( (x2(1) -x1(1)) *(xyz(2)-x2(2)) -(xyz(1)-x2(1))*(x2(2) -x1(2))  )
             gamma_u1x = -(x1(2) -xyz(2))/( (xyz(1)-x2(1)) *(x1(2) -xyz(2))-(x1(1)-xyz(1))*(xyz(2)-x2(2))  )
             alpha_u1y =  (x2(1) -x1(1)) /( (x1(1) -xyz(1))*(x2(2) -x1(2)) -(x2(1)-x1(1)) *(x1(2) -xyz(2)) )
             beta_u1y  =  (xyz(1)-x2(1)) /( (x2(1) -x1(1)) *(xyz(2)-x2(2)) -(xyz(1)-x2(1))*(x2(2) -x1(2))  )
             gamma_u1y =  (x1(1) -xyz(1))/( (xyz(1)-x2(1)) *(x1(2) -xyz(2))-(x1(1)-xyz(1))*(xyz(2)-x2(2))  )
             !-----------------------------------------side 2----------------------------------------------
             alpha_u2x = -(x4(2) -x3(2)) /( (x3(1) -xyz(1))*(x4(2) -x3(2)) -(x4(1)-x3(1)) *(x3(2) -xyz(2)) )
             beta_u2x  = -(xyz(2)-x4(2)) /( (x4(1) -x3(1)) *(xyz(2)-x4(2)) -(xyz(1)-x4(1))*(x4(2) -x3(2))  )
             gamma_u2x = -(x3(2) -xyz(2))/( (xyz(1)-x4(1)) *(x3(2) -xyz(2))-(x3(1)-xyz(1))*(xyz(2)-x4(2))  )
             alpha_u2y =  (x4(1) -x3(1)) /( (x3(1) -xyz(1))*(x4(2) -x3(2)) -(x4(1)-x3(1)) *(x3(2) -xyz(2)) )
             beta_u2y  =  (xyz(1)-x4(1)) /( (x4(1) -x3(1)) *(xyz(2)-x4(2)) -(xyz(1)-x4(1))*(x4(2) -x3(2))  )
             gamma_u2y =  (x3(1) -xyz(1))/( (xyz(1)-x4(1)) *(x3(2) -xyz(2))-(x3(1)-xyz(1))*(xyz(2)-x4(2))  )
             !---------------------------------------------------------------------------------------------
             !-----------------------v constants calculation-----------------------------------------------
             !-----------------------------------------side 1----------------------------------------------
             alpha_v1x = -(y2(2) -y1(2)) /( (y1(1) -xyz(1))*(y2(2) -y1(2)) -(y2(1)-y1(1)) *(y1(2) -xyz(2)) )
             beta_v1x  = -(xyz(2)-y2(2)) /( (y2(1) -y1(1)) *(xyz(2)-y2(2)) -(xyz(1)-y2(1))*(y2(2) -y1(2))  )
             gamma_v1x = -(y1(2) -xyz(2))/( (xyz(1)-y2(1)) *(y1(2) -xyz(2))-(y1(1)-xyz(1))*(xyz(2)-y2(2))  )
             alpha_v1y =  (y2(1) -y1(1)) /( (y1(1) -xyz(1))*(y2(2) -y1(2)) -(y2(1)-y1(1)) *(y1(2) -xyz(2)) )
             beta_v1y  =  (xyz(1)-y2(1)) /( (y2(1) -y1(1)) *(xyz(2)-y2(2)) -(xyz(1)-y2(1))*(y2(2) -y1(2))  )
             gamma_v1y =  (y1(1) -xyz(1))/( (xyz(1)-y2(1)) *(y1(2) -xyz(2))-(y1(1)-xyz(1))*(xyz(2)-y2(2))  )
             !-----------------------------------------side 2----------------------------------------------
             alpha_v2x = -(y4(2) -y3(2)) /( (y3(1) -xyz(1))*(y4(2) -y3(2)) -(y4(1)-y3(1)) *(y3(2) -xyz(2)) )
             beta_v2x  = -(xyz(2)-y4(2)) /( (y4(1) -y3(1)) *(xyz(2)-y4(2)) -(xyz(1)-y4(1))*(y4(2) -y3(2))  )
             gamma_v2x = -(y3(2) -xyz(2))/( (xyz(1)-y4(1)) *(y3(2) -xyz(2))-(y3(1)-xyz(1))*(xyz(2)-y4(2))  )
             alpha_v2y =  (y4(1) -y3(1)) /( (y3(1) -xyz(1))*(y4(2) -y3(2)) -(y4(1)-y3(1)) *(y3(2) -xyz(2)) )
             beta_v2y  =  (xyz(1)-y4(1)) /( (y4(1) -y3(1)) *(xyz(2)-y4(2)) -(xyz(1)-y4(1))*(y4(2) -y3(2))  )
             gamma_v2y =  (y3(1) -xyz(1))/( (xyz(1)-y4(1)) *(y3(2) -xyz(2))-(y3(1)-xyz(1))*(xyz(2)-y4(2))  )
             !---------------------------------------------------------------------------------------------
             !--------------------------------normal projection--------------------------------------------
             n_pr= n_node
             t_pr= (/ n_pr(2),-n_pr(1) /)
             write(55,*) n_pr
             !---------------------------------------------------------------------------------------------
             !--------------------------------constants definition-----------------------------------------
             A=2*n_pr(1)*n_pr(1) ; B=2*n_pr(1)*n_pr(2) ;               C=B ; D=2*n_pr(2)*n_pr(2)
             E=2*n_pr(1)*t_pr(1) ; F=t_pr(1)*n_pr(2)+t_pr(2)*n_pr(1) ; G=F ; H=2*n_pr(2)*t_pr(2)

             C_u1x=beta_u1x*u1+gamma_u1x*u2
             C_u1y=beta_u1y*u1+gamma_u1y*u2

             C_u2x=beta_u2x*u3+gamma_u2x*u4
             C_u2y=beta_u2y*u3+gamma_u2y*u4

             C_v1x=beta_v1x*v1+gamma_v1x*v2
             C_v1y=beta_v1y*v1+gamma_v1y*v2

             C_v2x=beta_v2x*v3+gamma_v2x*v4
             C_v2y=beta_v2y*v3+gamma_v2y*v4 

             AA(1,1)=A*mu1*alpha_u1x+B*mu1*alpha_u1y-A*mu2*alpha_u2x-B*mu2*alpha_u2y
             AA(1,2) =C*mu1*alpha_v1x+D*mu1*alpha_v1y-C*mu2*alpha_v2x-D*mu2*alpha_v2y
             BB(1)=deltaP  +   sigma*kappa+&
                  A*mu2*C_u2x+B*mu2*C_u2y+C*mu2*C_v2x+D*mu2*C_v2y-&
                  A*mu1*C_u1x-B*mu1*C_u1y-C*mu1*C_v1x-D*mu1*C_v1y

             AA(2,1)=E*mu1*alpha_u1x+F*mu1*alpha_u1y-E*mu2*alpha_u2x-F*mu2*alpha_u2y
             AA(2,2)=G*mu1*alpha_v1x+H*mu1*alpha_v1y-G*mu2*alpha_v2x-H*mu2*alpha_v2y
             BB(2)=E*mu2*C_u2x+F*mu2*C_u2y+G*mu2*C_v2x+H*mu2*C_v2y-&
                  E*mu1*C_u1x-F*mu1*C_u1y-G*mu1*C_v1x-H*mu1*C_v1y

             det=AA(2,2)*AA(1,1)-AA(2,1)*AA(1,2)
             detu=BB(1)*AA(2,2)-BB(2)*AA(1,2)
             detv=BB(2)*AA(1,1)-BB(1)*AA(2,1)

             if (abs(det)<=1.d-6) infinity=.true.
             write(55,*) '-----det----------'
             write(55,*) AA(1,1),AA(1,2)
             write(55,*) AA(2,1),AA(2,2)
             write(55,*) BB(1),BB(2)
             write(55,*) '---------------'

             write(55,*) 'detu',detu
             write(55,*) 'detv',detv
             write(55,*) 'det',det
             !   if ( abs( P1-P2+sigma*kappa ) >=1.d-10 ) then
             write(55,*) '-------P--------'
             write(55,*) P1-P2+sigma*kappa
             write(55,*) P1,P2,kappa
             write(55,*) '-------mu--------'
             write(55,*) mu1,mu2
             write(55,*) A*mu2*C_u2x+B*mu2*C_u2y+C*mu2*C_v2x+D*mu2*C_v2y-&
                  A*mu1*C_u1x-B*mu1*C_u1y-C*mu1*C_v1x-D*mu1*C_v1y,&
                  E*mu2*C_u2x+F*mu2*C_u2y+G*mu2*C_v2x+H*mu2*C_v2y-&
                  E*mu1*C_u1x-F*mu1*C_u1y-G*mu1*C_v1x-H*mu1*C_v1y
             write(55,*) '--------BB-------'
             write(55,*) BB

             !  end if
             !---------------------------------------------------------------------------------------------
             call condition_number_matrix(dim,AA,cond(nu),l1,l2)
!!$             write(55,*) 'cond',cond(nu)
             write(55,*) '--------valeur propre---'
             write(55,*) l1,l2
             !--------------------------------Interpolated velocity----------------------------------------
             vp_cond(1,nu)=detu/(det+1.D-40)
             vp_cond(2,nu)=detv/(det+1.D-40)
             vp(1)=vp_cond(1,nu)
             vp(2)=vp_cond(2,nu)
!!$             vp(1)=0!vp_cond(1,nu)
!!$             vp(2)=0!vp_cond(2,nu)
             !---------------------------------------gradients----------------------------------------------
             !-----------------------------------------side 1-----------------------------------------------
             gradu1(1,1)= alpha_u1x*vp(1)+C_u1x
             gradu1(1,2)= alpha_u1y*vp(1)+C_u1y
             gradu1(2,1)= alpha_v1x*vp(2)+C_v1x
             gradu1(2,2)= alpha_v1y*vp(2)+C_v1y
             !-----------------------------------------side 2-----------------------------------------------
             gradu2(1,1)= alpha_u2x*vp(1)+C_u2x
             gradu2(1,2)= alpha_u2y*vp(1)+C_u2y
             gradu2(2,1)= alpha_v2x*vp(2)+C_v2x
             gradu2(2,2)= alpha_v2y*vp(2)+C_v2y
             !---------------------------------------------------------------------------------------------
          end if
       end do bc1
       !-----------------------------------------------------------------------------------------------
       deallocate(minlocatu1,minlu1,tabvecu1,minlocatu2,minlu2,tabvecu2)
       deallocate(minlocatv1,minlv1,tabvecv1,minlocatv2,minlv2,tabvecv2)
       deallocate(distu1,indiu1,indju1,xu1,distu2,indiu2,indju2,xu2)
       deallocate(distv1,indiv1,indjv1,xv1,distv2,indiv2,indjv2,xv2)

       deallocate(cond,vp_cond)
       !-----------------------------------------------------------------------------------------------
    end if
    write(55,*) 'vitesse'
    write(55,*) vp(1),vp(2)
    write(55,*) '-----------------'
    write(55,*) 'scal prod'
    write(55,*) dot_product(vp,n_node)
    write(55,*) '-----------------'

!!$    write(55,*) gradu_exact
!!$    write(55,*) gradu1(1,1,n),gradu1(1,2,n)
    vp_temp(1)=vp(1)
    vp_temp(2)=vp(2)
    call exact_velocity(xyz,ve,nt,gradu_exact)
    Erreur=(abs(ve(1)-vp_temp(1))+abs(ve(2)-vp_temp(2)))
    Erreur_Linfty=max(Erreur_Linfty,Erreur)
    !---------------------------------------------------------------------------------------------------
!!$    if (mask_u==.false. .and. mask_v==.false.) then
!!$       Erreurux=abs(gradu_exact(1)-gradu1(1,1,n))
!!$       Erreur_Linftyux=max(Erreur_Linftyux,Erreurux)
!!$       Erreuruy=abs(gradu_exact(2)-gradu1(1,2,n))
!!$       Erreur_Linftyuy=max(Erreur_Linftyuy,Erreuruy)
!!$    else
!!$       Erreurux=0.d0
!!$       Erreuruy=0.d0
!!$    end if
    !if ( abs(Erreur_Linfty)>1.d-1 )
    ! write(70,*) n,Erreur_Linfty,Erreur_Linftyux,Erreur_Linftyuy
    !---------------------------------------------------------------------------------------------
  end subroutine interpolation_with_jump_relation_conditions
  !***********************************************************************************************
  !***********************************************************************************************
  subroutine bound_cond2d(xyz,xyzn)
    !*********************************************************************************************
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables 
    !-------------------------------------------------------------------------------
    real(8), dimension(2), intent(in)                         ::xyz
    real(8), dimension(2), intent(inout)                      ::xyzn
    !-------------------------------------------------------------------------------
    if (xyzn(1).le.xmin-dx) xyzn(1)=xmin-dx
    if (xyzn(1).ge.xmax+dx) xyzn(1)=xmax+dx
    if (xyzn(2).le.ymin-dy) xyzn(2)=ymin-dy
    if (xyzn(2).ge.ymax+dy) xyzn(2)=ymax+dy
    if ((xyz(1).lt.xmin).and.((xyzn(1)-xyz(1)).gt.0.d0)) xyzn(1)=xyz(1)
    if ((xyz(1).gt.xmax).and.((xyzn(1)-xyz(1)).lt.0.d0)) xyzn(1)=xyz(1)
    if ((xyz(2).lt.ymin).and.((xyzn(2)-xyz(2)).gt.0.d0)) xyzn(2)=xyz(2)
    if ((xyz(2).gt.ymax).and.((xyzn(2)-xyz(2)).lt.0.d0)) xyzn(2)=xyz(2)
  end subroutine bound_cond2d

  !*******************************************************************************
  subroutine cfl_2d(u,v,dtcfl,dtrest,nbcfl)
    !*******************************************************************************!
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    integer,intent(out)                                                     :: nbcfl
    !-------------------------------------------------------------------------------
    real(8),  dimension(sx-3:ex+3,sy-3:ey+3,1),intent(in)          :: u,v
    real(8),intent(out)                                            :: dtcfl,dtrest
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    real(8)  :: umax
    real(8),parameter :: epsc = 1e-20_8
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------

    umax=max( maxval(abs(u)), maxval(abs(v)) ) 
    !-------------------------------------------------------------------------------
    !Time step with cfl = 0.5
    !-------------------------------------------------------------------------------
    if (umax>epsc) then
       dtcfl = cfl / umax
    else
       dtcfl = dt
    end if
    !-------------------------------------------------------------------------------
    if (dtcfl >= dt) then
       dtcfl = dt
       nbcfl = 1
       dtrest = 0.d0
    else
       nbcfl = int(dt/dtcfl)
       dtcfl= dt/(nbcfl+1)
       dtrest=dt-nbcfl*dtcfl
    end if
    !-------------------------------------------------------------------------------
    return
  end subroutine cfl_2d
  !******************************************************************************

  !************************************************************************************
  subroutine init_front_tracking(ft,bul,volume_ini)
    !*******************************************************************************
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables 
    !-------------------------------------------------------------------------------
    type(bulle),dimension(:), allocatable,intent(out)             :: bul
    type(struct_front_tracking),intent(out)                       :: ft
    real(8),intent(out)                                           :: volume_ini
    !-------------------------------------------------------------------------------
    !Local variables 
    !-------------------------------------------------------------------------------
    integer                             :: n,m,nbpt,npt
    !-------------------------------------------------------------------------------
    ft%minxy=min(dx,dy)
    ft%ftdmax=1.d0*ft%minxy;  ft%ftd=0.5d0*ft%minxy
    ft%ftdmin=0.25d0*ft%minxy
    if (dim.eq.2) then
       select case(trim(cas))
       case("point")
          call point
       case("cercle")
          call cercle
       case("breil")
          call breil
       case("cercle2")
          call cercle2
       case("carre")
          call carre
       case("ligne")
          call ligne
       case("rectangle1")
          call rectangle1
       case("ellipse")
          call ellipse
       case("poiseuille")
          call poiseuille
       case("couette")
          call couette          
       case default
          write(*,*)"NO INITIALIZATION OF FRONT-TRACKING SURFACE"
       end select
    end if
    !-------------------------------------------------------------------------------
  contains
    subroutine point
      !------------------------------------------------------------------------------- 
      implicit none
      !-------------------------------------------------------------------------------
      real(8)   ::x1,y1,x2,y2
      real(8), dimension(dim)::xyz
      !-------------------------------------------------------------------------------
      ft%nbbul=1
      ft%nbbulmax=ft%nbbul*ft%multiplicateur_de_bulle
      allocate(bul(ft%nbbulmax))
      do m=1,ft%nbbul
         bul(m)%mask=.true.

         bul(m)%nbele_vrai=1
         bul(m)%nbelebis=1!ft%nbele
         bul(m)%nbsom_vrai=1
         bul(m)%nbsombis=1!ft%nbsom

         bul(m)%nbsommax=bul(m)%nbsom_vrai*ft%multiplicateur_de_sommet 
         bul(m)%nbelemax=bul(m)%nbele_vrai*ft%multiplicateur_d_element

         bul(m)%nbele_vrai=1
         bul(m)%nbele=bul(m)%nbele_vrai
         bul(m)%nbsom_vrai=bul(m)%nbele_vrai
         bul(m)%nbsom=bul(m)%nbsom_vrai
         allocate(bul(m)%som(bul(m)%nbsommax)) ; allocate(bul(m)%ele(bul(m)%nbelemax))
         do n=1,bul(m)%nbsom_vrai
            x1=1.5d0; y1=0.75d0; x2=1.5d0; y2=0.75d0
            !xyz(1)=0.1d0; xyz(2)=0.5d0
            xyz(1)=1.5d0; xyz(2)=1.d0
            bul(m)%som(n)%xyz(1)=xyz(1)
            bul(m)%som(n)%xyz(2)=xyz(2)
            bul(m)%ele(n)%nusom(1)=n
            bul(m)%ele(n)%mask=.true.
            bul(m)%som(n)%mask=.true.
            bul(m)%som(n)%notfix=.true.

         end do
      end do
    end subroutine point
    !-------------------------------------------------------------------------------
    subroutine cercle
      !-------------------------------------------------------------------------------
      implicit none
      !-------------------------------------------------------------------------------
      real(8)   ::dxy
      integer            ::n,npt_old
      !-------------------------------------------------------------------------------
      ft%nbbul=1
      ft%nbbulmax=ft%nbbul*ft%multiplicateur_de_bulle
      allocate(bul(ft%nbbulmax))
      !---------initialisation serpentin---------------
!!$      bul(1)%r0=0.15d0
!!$      bul(1)%xc=0.193d0; bul(1)%yc=0.75d0+1.d-12
!!$      bul(2)%r0=0.15d0
!!$      bul(2)%xc=0.5d0; bul(2)%yc=0.75d0+1.d-12
      bul(1)%r0=0.075d0
      bul(1)%xc=0.143d0; bul(1)%yc=0.141d0
      !bul(1)%xc=0.15d0-1.d-12; bul(1)%yc=0.15d0-1.d-12

!!$      bul(1)%r0=0.075d0
!!$      bul(1)%xc=0.13d0-1.d-11; bul(1)%yc=0.201d0-1.d-12

      !---------------------------------------------
      do m=1,ft%nbbul
         bul(m)%mask=.true.
         nbpt=floor(pi*bul(m)%r0/ft%ftd) 
         dxy=pi/dble(nbpt)
         bul(m)%nbele_vrai=2*nbpt
         bul(m)%nbele=bul(m)%nbele_vrai
         bul(m)%nbsom_vrai=bul(m)%nbele_vrai
         bul(m)%nbsom=bul(m)%nbsom_vrai
         bul(m)%nbsommax=bul(m)%nbsom_vrai*ft%multiplicateur_de_sommet 
         bul(m)%nbelemax=bul(m)%nbele_vrai*ft%multiplicateur_d_element
         allocate(bul(m)%som(bul(m)%nbsommax)) ; allocate(bul(m)%ele(bul(m)%nbelemax))

         npt_old=bul(m)%nbele_vrai
         npt=0
         do n=1,nbpt
            npt=npt+1
            bul(m)%ele(npt)%nusom(1)=npt
            bul(m)%ele(npt)%nusom(2)=npt+1
            bul(m)%ele(npt)%ele_vois(1)=npt_old
            bul(m)%ele(npt_old)%ele_vois(2)=npt
            bul(m)%som(npt)%xyz(1)=bul(m)%xc+bul(m)%r0*cos(dxy*dble(n-1))
            bul(m)%som(npt)%xyz(2)=bul(m)%yc+bul(m)%r0*sin(dxy*dble(n-1))
            !------------------------------------------------------------

            npt_old=npt 
         enddo
         do n=1,nbpt
            npt=npt+1
            bul(m)%ele(npt)%nusom(1)=npt 
            bul(m)%ele(npt)%nusom(2)=npt+1
            bul(m)%ele(npt)%ele_vois(1)=npt_old
            bul(m)%ele(npt_old)%ele_vois(2)=npt
            bul(m)%som(npt)%xyz(1)=bul(m)%xc-bul(m)%r0*cos(dxy*dble(n-1))
            bul(m)%som(npt)%xyz(2)=bul(m)%yc-bul(m)%r0*sin(dxy*dble(n-1))
            !----------------------------------------------

            npt_old=npt 
         enddo
         bul(m)%ele(npt)%nusom(2)=1
         bul(ft%nbbul+1:ft%nbbulmax)%mask=.false.
         bul(m)%ele(1:bul(m)%nbele)%mask=.true.
         bul(m)%som(1:bul(m)%nbsom)%mask=.true.
         bul(m)%som(1:bul(m)%nbsom)%notfix=.true.
         bul(m)%som(1:bul(m)%nbsom)%desenrichissement=.true.
         bul(m)%som(1:bul(m)%nbsom)%compteur=ft%compteur_init

         call volume_2d(bul,ft,0)
         volume_ini=bul(m)%volume_2d
      end do

    end subroutine cercle
    !-------------------------------------------------------------------------------
    subroutine breil
      !-------------------------------------------------------------------------------
      implicit none
      !-------------------------------------------------------------------------------
      real(8)   ::dxy
      integer            ::n,npt_old
      !-------------------------------------------------------------------------------
      ft%nbbul=1
      ft%nbbulmax=ft%nbbul*ft%multiplicateur_de_bulle
      allocate(bul(ft%nbbulmax))
      !-----------------------------initialisation breil------------------------------
      ft%r0=0.15d0
      ft%xc=1.d0; ft%yc=0.75d0
      bul(1)%r0=0.15d0
      !bul(1)%xc=1.d0+1.d-12; bul(1)%yc=0.75d0+1.d-12
      !bul(1)%xc=1.d0+1.d-14; bul(1)%yc=0.75d0+1.d-14
      bul(1)%xc=1.d0-1.d-11; bul(1)%yc=0.75d0-1.d-14
      !---------------------------------------------
      do m=1,ft%nbbul
         bul(m)%mask=.true.
         nbpt=floor(pi*bul(m)%r0/ft%ftd) 
         !nbpt=20
         dxy=pi/dble(nbpt)
         bul(m)%nbele_vrai=2*nbpt
         bul(m)%nbele=bul(m)%nbele_vrai
         bul(m)%nbsom_vrai=bul(m)%nbele_vrai
         bul(m)%nbsom=bul(m)%nbsom_vrai
         bul(m)%nbsommax=bul(m)%nbsom_vrai*ft%multiplicateur_de_sommet 
         bul(m)%nbelemax=bul(m)%nbele_vrai*ft%multiplicateur_d_element
         allocate(bul(m)%som(bul(m)%nbsommax)) ; allocate(bul(m)%ele(bul(m)%nbelemax))
         npt_old=bul(m)%nbele_vrai
         npt=0
         do n=1,nbpt
            npt=npt+1
            bul(m)%ele(npt)%nusom(1)=npt
            bul(m)%ele(npt)%nusom(2)=npt+1
            bul(m)%ele(npt)%ele_vois(1)=npt_old
            bul(m)%ele(npt_old)%ele_vois(2)=npt
            bul(m)%som(npt)%xyz(1)=bul(m)%xc+bul(m)%r0*cos(dxy*dble(n-1))
            bul(m)%som(npt)%xyz(2)=bul(m)%yc+bul(m)%r0*sin(dxy*dble(n-1))
            !------------------------------------------------------------

            npt_old=npt 
         enddo
         do n=1,nbpt
            npt=npt+1
            bul(m)%ele(npt)%nusom(1)=npt 
            bul(m)%ele(npt)%nusom(2)=npt+1
            bul(m)%ele(npt)%ele_vois(1)=npt_old
            bul(m)%ele(npt_old)%ele_vois(2)=npt
            bul(m)%som(npt)%xyz(1)=bul(m)%xc-bul(m)%r0*cos(dxy*dble(n-1))
            bul(m)%som(npt)%xyz(2)=bul(m)%yc-bul(m)%r0*sin(dxy*dble(n-1))
            !----------------------------------------------

            npt_old=npt 
         enddo
         bul(m)%ele(npt)%nusom(2)=1
         bul(ft%nbbul+1:ft%nbbulmax)%mask=.false.
         bul(m)%ele(1:bul(m)%nbele)%mask=.true.
         bul(m)%som(1:bul(m)%nbsom)%mask=.true.
         bul(m)%som(1:bul(m)%nbsom)%notfix=.true.
         bul(m)%som(1:bul(m)%nbsom)%desenrichissement=.true.
         bul(m)%som(1:bul(m)%nbsom)%compteur=ft%compteur_init

         call volume_2d(bul,ft,0)
         volume_ini=bul(m)%volume_2d
      end do

    end subroutine breil
    !-------------------------------------------------------------------------------
    subroutine ellipse
      !-------------------------------------------------------------------------------
      implicit none
      !-------------------------------------------------------------------------------
      real(8)   ::dxy,a,b,per
      integer            ::n,npt_old
      !-------------------------------------------------------------------------------
      ft%nbbul=1
      ft%nbbulmax=ft%nbbul*ft%multiplicateur_de_bulle
      allocate(bul(ft%nbbulmax))
      !---------initialisation ellipse---------------
      bul(1)%r0=0.075d0
      bul(1)%xc=0.15d0+1.d-12; bul(1)%yc=0.141d0+1.d-12
      !bul(1)%xc=0.15d0; bul(1)%yc=0.15d0

      a=(3._8/2)*bul(1)%r0
      b=(2._8/3)*bul(1)%r0
      per=pi*sqrt(2*(a*a+b*b))
      nbpt=floor(per/(2*ft%ftd)) 
      dxy=pi/dble(nbpt)
      !---------------------------------------------
      do m=1,ft%nbbul
         bul(m)%mask=.true.
         bul(m)%nbele_vrai=2*nbpt
         bul(m)%nbele=bul(m)%nbele_vrai
         bul(m)%nbsom_vrai=bul(m)%nbele_vrai
         bul(m)%nbsom=bul(m)%nbsom_vrai
         bul(m)%nbsommax=bul(m)%nbsom_vrai*ft%multiplicateur_de_sommet 
         bul(m)%nbelemax=bul(m)%nbele_vrai*ft%multiplicateur_d_element
         allocate(bul(m)%som(bul(m)%nbsommax)) ; allocate(bul(m)%ele(bul(m)%nbelemax))

         npt_old=bul(m)%nbele_vrai
         npt=0
         do n=1,nbpt
            npt=npt+1
            bul(m)%ele(npt)%nusom(1)=npt
            bul(m)%ele(npt)%nusom(2)=npt+1
            bul(m)%ele(npt)%ele_vois(1)=npt_old
            bul(m)%ele(npt_old)%ele_vois(2)=npt
            bul(m)%som(npt)%xyz(1)=bul(m)%xc+a*cos(dxy*dble(n-1))
            bul(m)%som(npt)%xyz(2)=bul(m)%yc+b*sin(dxy*dble(n-1))
            !------------------------------------------------------------
            npt_old=npt 
         enddo
         do n=1,nbpt
            npt=npt+1
            bul(m)%ele(npt)%nusom(1)=npt 
            bul(m)%ele(npt)%nusom(2)=npt+1
            bul(m)%ele(npt)%ele_vois(1)=npt_old
            bul(m)%ele(npt_old)%ele_vois(2)=npt
            bul(m)%som(npt)%xyz(1)=bul(m)%xc-a*cos(dxy*dble(n-1))
            bul(m)%som(npt)%xyz(2)=bul(m)%yc-b*sin(dxy*dble(n-1))
            !------------------------------------------------------------
            npt_old=npt 
         enddo
         bul(m)%ele(npt)%nusom(2)=1
         bul(ft%nbbul+1:ft%nbbulmax)%mask=.false.
         bul(m)%ele(1:bul(m)%nbele)%mask=.true.
         bul(m)%som(1:bul(m)%nbsom)%mask=.true.
         bul(m)%som(1:bul(m)%nbsom)%notfix=.true.
         bul(m)%som(1:bul(m)%nbsom)%desenrichissement=.true.
         bul(m)%som(1:bul(m)%nbsom)%compteur=ft%compteur_init

         call volume_2d(bul,ft,0)
         volume_ini=bul(m)%volume_2d
      end do

    end subroutine ellipse
    !-------------------------------------------------------------------------------
    subroutine poiseuille
      !------------------------------------------------------------------------------- 
      implicit none
      !-------------------------------------------------------------------------------
      integer            ::n,npt_old,l,nbpt1,nbpt2,nbpt3,nbpt4
      real(8)            ::r1,r2
      !------------------------------------------------------------------------------
      ft%nbbul=1
      ft%nbbulmax=ft%nbbul*ft%multiplicateur_de_bulle
      allocate(bul(ft%nbbulmax))
      bul(1)%r0=(xmin-xmax+0.5*dx)
      r1=xmax-xmin+2*dx
      r2=(ymax-ymin+2*dy)/2
      bul(1)%xc=(xmin-dx)+1.d-12
      bul(1)%yc=(ymin-dy)+1.d-12
      nbpt1=1 ; nbpt2=1 ; nbpt3=floor(r1/(50*ft%ftd)) ; nbpt4=1
      do m=1,ft%nbbul
         bul(m)%mask=.true.
         bul(m)%nbele_vrai=nbpt1+nbpt2+nbpt3+nbpt4
         bul(m)%nbele=bul(m)%nbele_vrai
         bul(m)%nbsom_vrai=bul(m)%nbele_vrai
         bul(m)%nbsom=bul(m)%nbsom_vrai
         bul(m)%nbsommax=bul(m)%nbsom_vrai*ft%multiplicateur_de_sommet 
         bul(m)%nbelemax=bul(m)%nbele_vrai*ft%multiplicateur_d_element
         allocate(bul(m)%som(bul(m)%nbsommax)) ; allocate(bul(m)%ele(bul(m)%nbelemax))
         npt_old=bul(m)%nbele_vrai
         npt=0
         do n=1,nbpt1
            npt=npt+1
            bul(m)%ele(npt)%nusom(1)=npt
            bul(m)%ele(npt)%nusom(2)=npt+1
            bul(m)%ele(npt)%ele_vois(1)=npt_old
            bul(m)%ele(npt_old)%ele_vois(2)=npt
            bul(m)%som(npt)%xyz(1)=bul(m)%xc
            bul(m)%som(npt)%xyz(2)=bul(m)%yc
            npt_old=npt
         enddo

         do n=1,nbpt2
            npt=npt+1
            bul(m)%ele(npt)%nusom(1)=npt
            bul(m)%ele(npt)%nusom(2)=npt+1
            bul(m)%ele(npt)%ele_vois(1)=npt_old
            bul(m)%ele(npt_old)%ele_vois(2)=npt
            bul(m)%som(npt)%xyz(1)=bul(m)%xc+r1
            bul(m)%som(npt)%xyz(2)=bul(m)%yc
            npt_old=npt 
         enddo

         do n=1,nbpt3
            npt=npt+1
            bul(m)%ele(npt)%nusom(1)=npt
            bul(m)%ele(npt)%nusom(2)=npt+1
            bul(m)%ele(npt)%ele_vois(1)=npt_old
            bul(m)%ele(npt_old)%ele_vois(2)=npt
            bul(m)%som(npt)%xyz(1)=bul(m)%xc+r1-dble(50*ft%ftd)*dble(n-1)
            bul(m)%som(npt)%xyz(2)=bul(m)%yc+r2
            npt_old=npt 
         enddo

         do n=1,nbpt4
            npt=npt+1
            bul(m)%ele(npt)%nusom(1)=npt
            bul(m)%ele(npt)%nusom(2)=npt+1
            bul(m)%ele(npt)%ele_vois(1)=npt_old
            bul(m)%ele(npt_old)%ele_vois(2)=npt
            bul(m)%som(npt)%xyz(1)=bul(m)%xc
            bul(m)%som(npt)%xyz(2)=bul(m)%yc+r2
            npt_old=npt 
         enddo
         bul(m)%ele(npt)%nusom(2)=1
         !---------------------------------------------------
         bul(ft%nbbul+1:ft%nbbulmax)%mask=.false.
         bul(m)%ele(1:bul(m)%nbele)%mask=.true.
         bul(m)%som(1:bul(m)%nbsom)%mask=.true.
         bul(m)%som(1:bul(m)%nbsom)%notfix=.true.
         bul(m)%som(1:bul(m)%nbsom)%desenrichissement=.true.
         bul(m)%som(1:bul(m)%nbsom)%compteur=ft%compteur_init
         !------------fixing boundary nodes------------------
         do l=1,npt
            if ( bul(m)%som(l)%xyz(1) <= xmin .or. bul(m)%som(l)%xyz(1) >=xmax .or. &
                 bul(m)%som(l)%xyz(2) <= ymin .or. bul(m)%som(l)%xyz(2) >=ymax ) then
               bul(m)%som(l)%notfix=.false.
            end if
         end do
         !---------------------------------------------------
         call volume_2d(bul,ft,0)
         volume_ini=bul(m)%volume_2d
      end do
      !-------------------------------------------------------------------------------
    end subroutine poiseuille
    !-------------------------------------------------------------------------------
    subroutine couette
      !------------------------------------------------------------------------------- 
      implicit none
      !-------------------------------------------------------------------------------
      integer            ::n,npt_old,l,nbpt1,nbpt2,nbpt3,nbpt4
      real(8)            ::r1,r2
      !------------------------------------------------------------------------------
      ft%nbbul=1
      ft%nbbulmax=ft%nbbul*ft%multiplicateur_de_bulle
      allocate(bul(ft%nbbulmax))
      r1= xmax-xmin+4*dx
      r2=(ymax-ymin+4*dy)/2
      bul(1)%xc=(xmin-2*dx)+1.d-12
      bul(1)%yc=(ymin-2*dy)+1.d-12
      nbpt1=1 ; nbpt2=1 ; nbpt4=1 ; nbpt3=floor(r1/(ft%ftd))
      do m=1,ft%nbbul
         bul(m)%mask=.true.
         bul(m)%nbele_vrai=nbpt1+nbpt2+nbpt3+nbpt4
         bul(m)%nbele=bul(m)%nbele_vrai
         bul(m)%nbsom_vrai=bul(m)%nbele_vrai
         bul(m)%nbsom=bul(m)%nbsom_vrai
         bul(m)%nbsommax=bul(m)%nbsom_vrai*ft%multiplicateur_de_sommet 
         bul(m)%nbelemax=bul(m)%nbele_vrai*ft%multiplicateur_d_element
         allocate(bul(m)%som(bul(m)%nbsommax)) ; allocate(bul(m)%ele(bul(m)%nbelemax))
         npt_old=bul(m)%nbele_vrai
         npt=0
         do n=1,nbpt1
            npt=npt+1
            bul(m)%ele(npt)%nusom(1)=npt
            bul(m)%ele(npt)%nusom(2)=npt+1
            bul(m)%ele(npt)%ele_vois(1)=npt_old
            bul(m)%ele(npt_old)%ele_vois(2)=npt
            bul(m)%som(npt)%xyz(1)=bul(m)%xc
            bul(m)%som(npt)%xyz(2)=bul(m)%yc
            npt_old=npt
         enddo

         do n=1,nbpt2
            npt=npt+1
            bul(m)%ele(npt)%nusom(1)=npt
            bul(m)%ele(npt)%nusom(2)=npt+1
            bul(m)%ele(npt)%ele_vois(1)=npt_old
            bul(m)%ele(npt_old)%ele_vois(2)=npt
            bul(m)%som(npt)%xyz(1)=bul(m)%xc+r1
            bul(m)%som(npt)%xyz(2)=bul(m)%yc
            npt_old=npt 
         enddo

         do n=1,nbpt3
            npt=npt+1
            bul(m)%ele(npt)%nusom(1)=npt
            bul(m)%ele(npt)%nusom(2)=npt+1
            bul(m)%ele(npt)%ele_vois(1)=npt_old
            bul(m)%ele(npt_old)%ele_vois(2)=npt
            bul(m)%som(npt)%xyz(1)=bul(m)%xc+r1-dble(ft%ftd)*dble(n-1)
            bul(m)%som(npt)%xyz(2)=bul(m)%yc+r2
            npt_old=npt 
         enddo

         do n=1,nbpt4
            npt=npt+1
            bul(m)%ele(npt)%nusom(1)=npt
            bul(m)%ele(npt)%nusom(2)=npt+1
            bul(m)%ele(npt)%ele_vois(1)=npt_old
            bul(m)%ele(npt_old)%ele_vois(2)=npt
            bul(m)%som(npt)%xyz(1)=bul(m)%xc
            bul(m)%som(npt)%xyz(2)=bul(m)%yc+r2
            npt_old=npt 
         enddo
         bul(m)%ele(npt)%nusom(2)=1
         !---------------------------------------------------
         bul(ft%nbbul+1:ft%nbbulmax)%mask=.false.
         bul(m)%ele(1:bul(m)%nbele)%mask=.true.
         bul(m)%som(1:bul(m)%nbsom)%mask=.true.
         bul(m)%som(1:bul(m)%nbsom)%notfix=.true.
         bul(m)%som(1:bul(m)%nbsom)%desenrichissement=.true.
         bul(m)%som(1:bul(m)%nbsom)%compteur=ft%compteur_init
         !------------fixing boundary nodes------------------
         do l=1,npt
            if ( bul(m)%som(l)%xyz(1) <= xmin .or. bul(m)%som(l)%xyz(1) >=xmax .or. &
                 bul(m)%som(l)%xyz(2) <= ymin .or. bul(m)%som(l)%xyz(2) >=ymax ) then
               bul(m)%som(l)%notfix=.false.
            end if
         end do
         !---------------------------------------------------
         call volume_2d(bul,ft,0)
         volume_ini=bul(m)%volume_2d
      end do
      !-------------------------------------------------------------------------------
    end subroutine couette
    !-------------------------------------------------------------------------------
    subroutine cercle2
      !-------------------------------------------------------------------------------
      implicit none
      !-------------------------------------------------------------------------------
      real(8)   ::dxy
      integer            ::n
      !-------------------------------------------------------------------------------
!!$      ft%r0=0.25d0
!!$      ft%xc=0.5d0; ft%yc=0.5d0
!!$      npt=0
!!$      nbpt=floor(pi*ft%r0/ft%ftd)
!!$      dxy=pi/dble(nbpt)
!!$      ft%nbele=2*nbpt
!!$      ft%r0=0.5d0
!!$      npt=0
!!$      nbpt=floor(pi*ft%r0/ft%ftd)             end if

!!$      dxy=pi/dble(nbpt)
!!$      ft%nbele=ft%nbele+2*nbpt
!!$      allocate(ftp(ft%nbele,dim,dim))
!!$      !
!!$      ft%r0=0.25d0
!!$      ft%xc=0.5d0; ft%yc=0.5d0
!!$      npt=0
!!$      nbpt=floor(pi*ft%r0/ft%ftd)
!!$      dxy=pi/dble(nbpt)
!!$      do n=1,nbpt
!!$         npt=npt+1
!!$         ftp(npt,1,1)=ft%xc+ft%r0*dcos(dxy*dble(n-1))
!!$         ftp(npt,1,2)=ft%yc+ft%r0*dsin(dxy*dble(n-1))
!!$         ftp(npt,2,1)=ft%xc+ft%r0*dcos(dxy*dble(n))
!!$         ftp(npt,2,2)=ft%yc+ft%r0*dsin(dxy*dble(n))
!!$      enddo
!!$      do n=1,nbpt
!!$         npt=npt+1
!!$         ftp(npt,1,1)=ft%xc-ft%r0*dcos(dxy*dble(n-1))
!!$         ftp(npt,1,2)=ft%yc-ft%r0*dsin(dxy*dble(n-1))
!!$         ftp(npt,2,1)=ft%xc-ft%r0*dcos(dxy*dble(n))
!!$         ftp(npt,2,2)=ft%yc-ft%r0*dsin(dxy*dble(n))
!!$      enddo
!!$      ft%r0=0.5d0
!!$      npt=0
!!$      nbpt=floor(pi*ft%r0/ft%ftd)
!!$      dxy=pi/dble(nbpt)
!!$      do n=1,nbpt
!!$         npt=npt+1
!!$         ftp(npt,1,1)=ft%xc+ft%r0*dcos(dxy*dble(n-1))
!!$         ftp(npt,1,2)=ft%yc+ft%r0*dsin(dxy*dble(n-1))
!!$         ftp(npt,2,1)=ft%xc+ft%r0*dcos(dxy*dble(n))
!!$         ftp(npt,2,2)=ft%yc+ft%r0*dsin(dxy*dble(n))
!!$      enddo
!!$      do n=1,nbpt
!!$         npt=npt+1
!!$         ftp(npt,1,1)=ft%xc-ft%r0*dcos(dxy*dble(n-1))
!!$         ftp(npt,1,2)=ft%yc-ft%r0*dsin(dxy*dble(n-1))
!!$         ftp(npt,2,1)=ft%xc-ft%r0*dcos(dxy*dble(n))
!!$         ftp(npt,2,2)=ft%yc-ft%r0*dsin(dxy*dble(n))
!!$      enddo
    end subroutine cercle2
    !-------------------------------------------------------------------------------
    subroutine ligne
      !-------------------------------------------------------------------------------
      implicit none
      !-------------------------------------------------------------------------------
      integer            ::n
      !-------------------------------------------------------------------------------
!!$      ft%r0=1.d0
!!$      ! ft%xc=0.3125d0; ft%yc=0.d0
!!$      ! ft%xc=0.28125d0; ft%yc=0.d0
!!$      ft%xc=0.296875d0; ft%yc=0.d0
!!$      ! ft%xc=0.3046875d0; ft%yc=0.d0
!!$      ! ft%xc=0.30859375; ft%yc=0.d0
!!$      npt=0
!!$      nbpt=floor(ft%r0/ft%ftd)+1
!!$      ft%nbele=nbpt
!!$      allocate(ftp(ft%nbele,dim,dim))
!!$      do n=1,nbpt
!!$         npt=npt+1
!!$         ftp(npt,1,1)=ft%xc
!!$         ftp(npt,1,2)=ft%yc+ft%ftd/ft%r0*(n-1)
!!$         ftp(npt,2,1)=ft%xc
!!$         ftp(npt,2,2)=ft%yc+ft%ftd/ft%r0*n
!!$      enddo
    end subroutine ligne
    !-------------------------------------------------------------------------------
    subroutine carre
      !-------------------------------------------------------------------------------
      implicit none
      !-------------------------------------------------------------------------------
      integer            ::n,npt_old
      !------------------------------------------------------------------------------
      ft%nbbul=1
      ft%nbbulmax=ft%nbbul*ft%multiplicateur_de_bulle
      allocate(bul(ft%nbbulmax))
      bul(1)%r0=0.25d0
      bul(1)%xc=0.5d0+1.d-12; bul(1)%yc=0.5d0+1.d-12
      do m=1,ft%nbbul
         bul(m)%mask=.true.
         nbpt=floor(bul(m)%r0/ft%ftd) 
         bul(m)%nbele_vrai=4*nbpt
         bul(m)%nbele=bul(m)%nbele_vrai
         bul(m)%nbsom_vrai=bul(m)%nbele_vrai
         bul(m)%nbsom=bul(m)%nbsom_vrai
         bul(m)%nbsommax=bul(m)%nbsom_vrai*ft%multiplicateur_de_sommet 
         bul(m)%nbelemax=bul(m)%nbele_vrai*ft%multiplicateur_d_element
         allocate(bul(m)%som(bul(m)%nbsommax)) ; allocate(bul(m)%ele(bul(m)%nbelemax))
         npt_old=bul(m)%nbele_vrai
         npt=0
         do n=1,nbpt
            npt=npt+1
            bul(m)%ele(npt)%nusom(1)=npt
            bul(m)%ele(npt)%nusom(2)=npt+1
            bul(m)%ele(npt)%ele_vois(1)=npt_old
            bul(m)%ele(npt_old)%ele_vois(2)=npt
            bul(m)%som(npt)%xyz(1)=bul(m)%xc!+dble(ft%ftd)*dble(n-1)
            bul(m)%som(npt)%xyz(2)=bul(m)%yc
            bul(m)%som(npt)%compteur=ft%compteur_init
            bul(m)%som(npt)%desenrichissement=.true.
            npt_old=npt 
         enddo
         do n=1,nbpt
            npt=npt+1
            bul(m)%ele(npt)%nusom(1)=npt
            bul(m)%ele(npt)%nusom(2)=npt+1
            bul(m)%ele(npt)%ele_vois(1)=npt_old
            bul(m)%ele(npt_old)%ele_vois(2)=npt
            bul(m)%som(npt)%xyz(1)=bul(m)%xc+bul(m)%r0
            bul(m)%som(npt)%xyz(2)=bul(m)%yc+dble(ft%ftd)*dble(n-1)
            bul(m)%som(npt)%compteur=ft%compteur_init
            bul(m)%som(npt)%desenrichissement=.true.
            npt_old=npt 
         enddo
         do n=1,nbpt
            npt=npt+1
            bul(m)%ele(npt)%nusom(1)=npt
            bul(m)%ele(npt)%nusom(2)=npt+1
            bul(m)%ele(npt)%ele_vois(1)=npt_old
            bul(m)%ele(npt_old)%ele_vois(2)=npt
            bul(m)%som(npt)%xyz(1)=bul(m)%xc+bul(m)%r0-dble(ft%ftd)*dble(n-1)
            bul(m)%som(npt)%xyz(2)=bul(m)%yc+bul(m)%r0
            bul(m)%som(npt)%compteur=ft%compteur_init
            bul(m)%som(npt)%desenrichissement=.true.
            npt_old=npt 
         enddo
         do n=1,nbpt
            npt=npt+1
            bul(m)%ele(npt)%nusom(1)=npt
            bul(m)%ele(npt)%nusom(2)=npt+1
            bul(m)%ele(npt)%ele_vois(1)=npt_old
            bul(m)%ele(npt_old)%ele_vois(2)=npt
            bul(m)%som(npt)%xyz(1)=bul(m)%xc
            bul(m)%som(npt)%xyz(2)=bul(m)%yc+bul(m)%r0-dble(ft%ftd)*dble(n-1)
            bul(m)%som(npt)%desenrichissement=.true.
            npt_old=npt 
         enddo
         bul(m)%ele(npt)%nusom(2)=1
         bul(ft%nbbul+1:ft%nbbulmax)%mask=.false.
         bul(m)%ele(1:bul(m)%nbele)%mask=.true.
         bul(m)%som(1:bul(m)%nbsom)%mask=.true.
         bul(m)%som(1:bul(m)%nbsom)%notfix=.true.

         call volume_2d(bul,ft,0)
         volume_ini=bul(m)%volume_2d
      end do
    end subroutine carre
    !-------------------------------------------------------------------------------
    subroutine rectangle1
      !-------------------------------------------------------------------------------
      implicit none
      !-------------------------------------------------------------------------------
      real(8)   ::r0hr1,r0wr1,nbpthr1,nbptwr1,xcr1,ycr1
      integer            ::n
      !--------------------------------------------------------------------------------
!!$      r0hr1=0.002d0
!!$      r0wr1=0.015d0
!!$      nbpthr1=floor(r0hr1/ft%ftd)
!!$      nbptwr1=floor(r0wr1/ft%ftd)
!!$      xcr1=0.0425d0
!!$      ycr1=0.0986d0
!!$      ft%nbele=2*nbpthr1+2*nbptwr1
!!$      allocate(ftp(ft%nbele,2,dim))
!!$
!!$      do n=1,nbptwr1
!!$         npt=npt+1
!!$         ftp(npt,1,1)=xcr1+dble(ft%ftd)*dble(n-1)
!!$         ftp(npt,1,2)=ycr1
!!$         ftp(npt,2,1)=xcr1+dble(ft%ftd)*dble(n)
!!$         ftp(npt,2,2)=ycr1
!!$      enddo
!!$      do n=1,nbpthr1
!!$         npt=npt+1
!!$         ftp(npt,1,1)=xcr1+r0wr1
!!$         ftp(npt,1,2)=ycr1+dble(ft%ftd)*dble(n-1)
!!$         ftp(npt,2,1)=xcr1+r0wr1
!!$         ftp(npt,2,2)=ycr1+dble(ft%ftd)*dble(n)
!!$      enddo
!!$      do n=1,nbptwr1
!!$         npt=npt+1
!!$         ftp(npt,1,1)=xcr1+r0wr1-dble(ft%ftd)*dble(n-1)
!!$         ftp(npt,1,2)=ycr1+r0hr1
!!$         ftp(npt,2,1)=xcr1+r0wr1-dble(ft%ftd)*dble(n)
!!$         ftp(npt,2,2)=ycr1+r0hr1
!!$      enddo
!!$      do n=1,nbpthr1
!!$         npt=npt+1
!!$         ftp(npt,1,1)=xcr1
!!$         ftp(npt,1,2)=ycr1+r0hr1-dble(ft%ftd)*dble(n-1)
!!$         ftp(npt,2,1)=xcr1
!!$         ftp(npt,2,2)=ycr1+r0hr1-dble(ft%ftd)*dble(n)
!!$      enddo
    end subroutine rectangle1
!!$  !-------------------------------------------------------------------------------
!!$  subroutine rectangle2
!!$  end subroutine rectangle2
!!$
!!$  !-------------------------------------------------------------------------------
!!$  subroutine injecteur
!!$  end subroutine injecteur
!!$
!!$  !-------------------------------------------------------------------------------

  end subroutine init_front_tracking
  !*******************************************************************************
  subroutine phase_init(nbbul,bul,phase,startx,starty,endx,endy,dx0,dy0,xmin0,ymin0,cou)
    !*******************************************************************************
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables 
    !-------------------------------------------------------------------------------
    type(bulle),dimension(:), allocatable,intent(in)         :: bul
    integer,intent(in)                                       :: startx,starty,endx,endy
    real(8),intent(in)                                       :: dx0,dy0,xmin0,ymin0
    logical,dimension(:,:,:), allocatable,intent(inout)      :: phase
    real(8),dimension(:,:,:), allocatable,intent(inout)      :: cou
    integer, intent(in)                                      :: nbbul
    !-------------------------------------------------------------------------------
    !Local variables 
    !-------------------------------------------------------------------------------
    integer :: i,j,k,m,n,nb_intersection_old,nb_intersection
    integer,dimension(startx:endx,starty:endy,1)             :: decompte
    type(raycasting),dimension(startx:endx,starty:endy,1)    :: maille
    real(8),dimension(dim)                                   :: xyz1,xyz2,A,B
    !-------------------------------------------------------------------------------
    do m=1,nbbul
       if (bul(m)%mask) then
          decompte=0
          do n=1,bul(m)%nbele
             if (bul(m)%ele(n)%mask) then
                xyz1=bul(m)%som(bul(m)%ele(n)%nusom(1))%xyz
                xyz2=bul(m)%som(bul(m)%ele(n)%nusom(2))%xyz
                call decompte_element(xyz1,xyz2,decompte,dx0,dy0,xmin0,ymin0,startx,endx,starty,endy)
             end if
          end do
          !-------------2nd passage affectation--------------------------------------
          do j=starty,endy
             do i=startx,endx
                if (allocated(maille(i,j,1)%element)) deallocate(maille(i,j,1)%element)
                if (decompte(i,j,1)/=0) then
                   allocate(maille(i,j,1)%element(decompte(i,j,1)))
                end if
             end do
          end do
          decompte=0
          do n=1,bul(m)%nbele
             if (bul(m)%ele(n)%mask) then
                xyz1=bul(m)%som(bul(m)%ele(n)%nusom(1))%xyz
                xyz2=bul(m)%som(bul(m)%ele(n)%nusom(2))%xyz
                call affectation_element(xyz1,xyz2,decompte,n,dx0,dy0,xmin0,ymin0,startx,endx,starty,endy,maille)
             end if
          end do
          !***********************************ray-casting part**************************************************
          do j=starty,endy
             nb_intersection_old=0
             i=startx
             nb_intersection=nb_intersection_old
             do k=1,decompte(i,j,1)
                xyz1=bul(m)%som(bul(m)%ele( maille(i,j,1)%element(k) )%nusom(1))%xyz
                xyz2=bul(m)%som(bul(m)%ele( maille(i,j,1)%element(k) )%nusom(2))%xyz
                A=[(i-1/2.d0)*dx0+xmin0,j    *dy0+ymin0]
                B=[ i        *dx0+xmin0,j    *dy0+ymin0]
                if ( intersection(xyz2,xyz1,A,B) )  then
                   nb_intersection=nb_intersection+1
                end if
                if (mod(nb_intersection,2)==0) then
                   phase(i,j,1)=.true.
                   cou(i,j,1)=0
                else
                   phase(i,j,1)=.false.
                   cou(i,j,1)=1
                end if
                nb_intersection_old=nb_intersection
             end do
             do i=startx+1,endx
                nb_intersection=nb_intersection_old
                do k=1,decompte(i,j,1)
                   xyz1=bul(m)%som(bul(m)%ele( maille(i,j,1)%element(k) )%nusom(1))%xyz
                   xyz2=bul(m)%som(bul(m)%ele( maille(i,j,1)%element(k) )%nusom(2))%xyz
                   A=[(i-1/2.d0)*dx0+xmin0,j    *dy0+ymin0]
                   B=[ i        *dx0+xmin0,j    *dy0+ymin0]

                   if ( intersection(xyz2,xyz1,A,B) )   then
                      nb_intersection=nb_intersection+1
                   end if
                end do

                do k=1,decompte(i-1,j,1)
                   xyz1=bul(m)%som(bul(m)%ele( maille(i-1,j,1)%element(k) )%nusom(1))%xyz
                   xyz2=bul(m)%som(bul(m)%ele( maille(i-1,j,1)%element(k) )%nusom(2))%xyz
                   A=[(i-1/2.d0)*dx0+xmin0,j    *dy0+ymin0]
                   B=[(i-1     )*dx0+xmin0,j    *dy0+ymin0]

                   if ( intersection(xyz2,xyz1,A,B) )  nb_intersection=nb_intersection+1
                end do

                if (mod(nb_intersection,2)==0) then
                   phase(i,j,1)=.true.
                   cou(i,j,1)=0
                else
                   phase(i,j,1)=.false.
                   cou(i,j,1)=1
                end if
                nb_intersection_old=nb_intersection
             end do
          end do
       end if
    end do
  end subroutine phase_init
  !*********************************************************************************
  !*********************************************************************************
  subroutine phase_update(xyz_vois1,xyz_old,xyz,phase&
       ,startx,starty,endx,endy,dx0,dy0,xmin0,ymin0,cou)
    !*******************************************************************************
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables 
    !-------------------------------------------------------------------------------
    real(8),intent(in),dimension(dim)                   :: xyz_vois1,xyz_old,xyz
    integer,intent(in)                                  :: startx,starty,endx,endy
    real(8),intent(in)                                  :: dx0,dy0,xmin0,ymin0
    logical,dimension(:,:,:), allocatable,intent(inout) :: phase
    real(8),dimension(:,:,:), allocatable,intent(inout) :: cou
    !-------------------------------------------------------------------------------
    !Local variables 
    !-------------------------------------------------------------------------------
    integer                       :: i,j,n,imin,imax,jmin,jmax,i0,i1,i2,j0,j1,j2
    real(8),dimension(dim)        :: xyz1,xyz2,xyz1_old,xyz2_old,x,AB,BC,CA,AX,BX,CX
    real(8),dimension(4)          :: det
    !-------------------------------------------------------------------------------
    !------------------vector definition--------------------------------------
    AB=xyz_old-xyz_vois1
    BC=xyz-xyz_old
    CA=xyz_vois1-xyz
    !------------------old and new position localization----------------------
    i0=min(max(floor((xyz(1)-xmin0)/dx0),sx-gx),ex+gx)
    j0=min(max(floor((xyz(2)-ymin0)/dy0),sy-gy),ey+gy)
    
    i1=min(max(floor((xyz_old(1)-xmin0)/dx0),sx-gx),ex+gx)
    j1=min(max(floor((xyz_old(2)-ymin0)/dy0),sy-gy),ey+gy)

    i2=min(max(floor((xyz_vois1(1)-xmin0)/dx0),sx-gx),ex+gx)
    j2=min(max(floor((xyz_vois1(2)-ymin0)/dy0),sy-gy),ey+gy)
    !--------------------------------------------------------------------------
    imin=min(i0,i1,i2)
    imax=max(i0,i1,i2)
    jmin=min(j0,j1,j2)
    jmax=max(j0,j1,j2)
    !-----------------------updating loop--------------------------------------
    do j=jmin,jmax!+1
       do i=imin,imax!+1
          x=(/ i*dx0+xmin0,j*dy0+ymin0 /)
          AX=x-xyz_vois1
          BX=x-xyz_old
          CX=x-xyz

          det(1)=AB(1)*AX(2)-AB(2)*AX(1)
          det(2)=BC(1)*BX(2)-BC(2)*BX(1)
          det(3)=CA(1)*CX(2)-CA(2)*CX(1)

          bc1:do n=2,3
             if  ( det(1)*det(n)<0 ) then
                !------------------no changes--------------------
                exit bc1
             else
                !------------------------------------------------
                if (n/=3) then 
                   cycle bc1
                else
                   !------------------phase change-----------------  
                   if (phase(i,j,1)) then
                      phase(i,j,1)=.false.
                      cou(i,j,1)=1
                   else
                      phase(i,j,1)=.true.
                      cou(i,j,1)=0
                   end if
                end if
             end if
          end do bc1
       end do
    end do
    !-------------------------------------------------------------------------------
  end subroutine phase_update
  !*********************************************************************************
  !*********************************************************************************
  
  !*********************************************************************************
  !*********************************************************************************
  subroutine phase_intersection(bul,nbbul,dx0,dy0,xmin0,ymin0,startx,endx,starty,endy,nx,ny,inters,nt)
    !*******************************************************************************
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    integer,intent(in)                                     :: startx,starty,endx,endy,nx,ny
    real(8),intent(in)                                     :: dx0,dy0,xmin0,ymin0
    integer,intent(in)                                     :: nbbul,nt
    type(bulle),dimension(:),allocatable,intent(in)        :: bul
    type(facet),intent(inout)                              :: inters
    !-------------------------------------------------------------------------------
    !Local variables
    !----------------------------------------------------------------------------
    integer :: som1,som2,vois2,l,ninit,som1_init,n,nn,nfacexx,nfacexy,nfaceyx,nfaceyy,nfacex,nfacey
    integer :: num_ele_mask,nbul,nele
    real(8),dimension(dim)              :: xyz1,xyz2,v1,v2
    integer(8),dimension(:),allocatable :: numx,numy,minlocat
    real(8),dimension(:),allocatable    :: minl 
    real(8),dimension(:,:),allocatable  :: xyz_temp
    integer,dimension(:),allocatable    :: ele_temp,bul_temp
    !----------------------------------------------------------------------------
    do l=1,nbbul
       if (bul(l)%mask) then
          if ( allocated(inters%face_x)  ) deallocate(inters%face_x)
          if ( allocated(inters%face_y)  ) deallocate(inters%face_y)
          
          nfacex=nx ; nfacey=ny
          allocate( inters%face_x(nfacex*nfacey),inters%face_y(nfacex*nfacey) )
          allocate( numx(nfacex*nfacey),numy(nfacex*nfacey) )
          
          !------------------------------------------------------------------
          ninit=1
          num_ele_mask=1
          !------------------------------------------------------------------
          do while ( .not.bul(l)%ele(num_ele_mask)%mask ) !find 1 element
             num_ele_mask=num_ele_mask+1
          end do
          !------------------------------------------------------------------
          vois2=num_ele_mask!1
          som1=bul( l )%ele( vois2 )%nusom(1)
          som2=bul( l )%ele( vois2 )%nusom(2)
          som1_init=som1
          numx=0 ; numy=0
          do while( som2/=som1_init )
             !print*,vois2,som1,som2
             xyz1=bul( l )%som( som1 )%xyz
             xyz2=bul( l )%som( som2 )%xyz
             !print*,'xyz',xyz1,xyz2
             !------------------------------------------------------------------
             call phase_intersection_localization(xyz1,xyz2,numx,numy,dx0,dy0,xmin0,ymin0,startx,&
                  endx,starty,endy,nx,ny,inters,ninit,vois2,l)
             !------------------------------------------------------------------
             !------------------update of neighbours----------------------------
             vois2=bul( l )%ele( vois2 )%ele_vois(2)
             som1=bul( l )%ele ( vois2 )%nusom(1)
             som2=bul( l )%ele ( vois2 )%nusom(2)
             !------------------------------------------------------------------
          end do
          !----------------------update for the last element--------------------
          !print*,vois2,som1,som2
          xyz1=bul( l )%som( som1 )%xyz
          xyz2=bul( l )%som( som2 )%xyz
          !print*,'xyz',xyz1,xyz2
          !---------------------------------------------------------------------
          call phase_intersection_localization(xyz1,xyz2,numx,numy,dx0,dy0,xmin0,ymin0,startx,&
               endx,starty,endy,nx,ny,inters,ninit,vois2,l)
          !---------------------------------------------------------------------
          !---------------------------------------------------------------------
          !--------------------ordering of intersections------------------------
          do n=1,nfacex*nfacey
             if ( numx(n) > ninit ) then
                allocate( xyz_temp(dim,numx(n)),minlocat(numx(n)),minl(0:numx(n)),ele_temp(numx(n)),bul_temp(numx(n)) )
                minl(0)=-1000000000
                do nn=1,numx(n)
                   minlocat(nn)= minloc( inters%face_x(n)%xyzI(1,:),1,inters%face_x(n)%xyzI(1,:)>minl(nn-1) )
                   minl(nn)    = minval( inters%face_x(n)%xyzI(1,:),1,inters%face_x(n)%xyzI(1,:)>minl(nn-1) )
                   xyz_temp(1,nn)=inters%face_x(n)%xyzI(1,minlocat(nn))
                   xyz_temp(2,nn)=inters%face_x(n)%xyzI(2,minlocat(nn))
                   ele_temp(nn)=inters%face_x(n)%num_ele(minlocat(nn))
                   bul_temp(nn)=inters%face_x(n)%num_bul(minlocat(nn))
                end do
                call MOVE_ALLOC( FROM=xyz_temp,TO=inters%face_x(n)%xyzI )
                call MOVE_ALLOC( FROM=ele_temp,TO=inters%face_x(n)%num_ele )
                call MOVE_ALLOC( FROM=bul_temp,TO=inters%face_x(n)%num_bul )

                deallocate(minlocat,minl)
             end if

             if ( numx(n) >= ninit ) then
                allocate( inters%face_x(n)%vI(dim,numx(n)) )
                allocate( inters%face_x(n)%gradu1(dim,numx(n)),inters%face_x(n)%gradv1(dim,numx(n)) )
                allocate( inters%face_x(n)%gradu2(dim,numx(n)),inters%face_x(n)%gradv2(dim,numx(n)) )
                allocate( inters%face_x(n)%pI1(numx(n)),inters%face_x(n)%pI2(numx(n)) )
                allocate( inters%face_x(n)%kappaI(numx(n)) )

                do nn=1,numx(n)
                   nele=inters%face_x(n)%num_ele(nn)
                   nbul=inters%face_x(n)%num_bul(nn)
                   call velocity_pressure_intersect_loc(bul,inters%face_x(n),nbul,nele,nn,nt)
                end do
                !write(51,*) inters%face_x(n)%xyzI
             end if
             !---------------------------------
!!$  !     write(50,*) n,numx(n)
!!$  !     write(51,*) inters%face_x(n)%xyzI
!!$  !     write(50,*) inters%face_x(n)%num_ele
!!$  !     write(50,*) inters%face_x(n)%num_bul
!!$  !     write(50,*) '---------------------'
          end do
          !----------------------------------------------------------------------------
          do n=1,nfacex*nfacey
             if ( numy(n) > ninit ) then
               ! print*,'in',numy(n)
                allocate( xyz_temp(dim,numy(n)),minlocat(numy(n)),minl(0:numy(n)),ele_temp(numy(n)),bul_temp(numy(n)) )
                minl(0)=-1000000000
                do nn=1,numy(n)
                   minlocat(nn)= minloc( inters%face_y(n)%xyzI(2,:),1,inters%face_y(n)%xyzI(2,:)>minl(nn-1) )
                   minl(nn)    = minval( inters%face_y(n)%xyzI(2,:),1,inters%face_y(n)%xyzI(2,:)>minl(nn-1) )
                   !print*,minlocat(nn),inters%face_y(n)%xyzI(2,:)
                   xyz_temp(1,nn)=inters%face_y(n)%xyzI(1,minlocat(nn))
                   xyz_temp(2,nn)=inters%face_y(n)%xyzI(2,minlocat(nn))
                   ele_temp(nn)=inters%face_y(n)%num_ele(minlocat(nn))
                   bul_temp(nn)=inters%face_y(n)%num_bul(minlocat(nn))
                end do
                call MOVE_ALLOC( FROM=xyz_temp,TO=inters%face_y(n)%xyzI )
                call MOVE_ALLOC( FROM=ele_temp,TO=inters%face_y(n)%num_ele )
                call MOVE_ALLOC( FROM=bul_temp,TO=inters%face_y(n)%num_bul )
                deallocate(minlocat,minl)
             end if

             if ( numy(n) >= ninit ) then
                allocate( inters%face_y(n)%vI(dim,numy(n)) )
                allocate( inters%face_y(n)%gradu1(dim,numy(n)),inters%face_y(n)%gradv1(dim,numy(n)) )
                allocate( inters%face_y(n)%gradu2(dim,numy(n)),inters%face_y(n)%gradv2(dim,numy(n)) )
                allocate( inters%face_y(n)%pI1(numy(n)),inters%face_y(n)%pI2(numy(n)) )
                allocate( inters%face_y(n)%kappaI(numy(n)) )

                do nn=1,numy(n)
                   nele=inters%face_y(n)%num_ele(nn)
                   nbul=inters%face_y(n)%num_bul(nn)
                   call velocity_pressure_intersect_loc(bul,inters%face_y(n),nbul,nele,nn,nt)
                end do
               ! write(52,*) inters%face_y(n)%xyzI
             end if
             !---------------------------------
!!$  !    write(51,*) n,numy(n)
!!$  !    write(51,*) inters%face_y(n)%xyzI
!!$  !    write(51,*) inters%face_y(n)%num_ele
!!$  !    write(51,*) inters%face_y(n)%num_bul
!!$  !    write(51,*) '---------------------'
          end do
          !----------------------------------------------------------------------------
          deallocate( numx,numy )
       end if
    end do
    !----------------------------------------------------------------------------------------------
    !*******************************************************************************
  end subroutine phase_intersection
  !*******************************************************************************
  !*******************************************************************************
  
  !*******************************************************************************
  !*******************************************************************************
  subroutine phase_intersection_localization(xyz1,xyz2,numx,numy,dx0,dy0,xmin0,ymin0,&
       startx,endx,starty,endy,nx,ny,inters,ninit,nele,nbul)
    !*******************************************************************************
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    integer,intent(in)                                     :: startx,starty,endx,endy,nx,ny,ninit,nele,nbul
    real(8),intent(in)                                     :: dx0,dy0,xmin0,ymin0
    type(facet),intent(inout)                              :: inters
    real(8),dimension(dim),intent(in)                      :: xyz1,xyz2
    integer(8),dimension(:),allocatable,intent(inout)      :: numx,numy
    !-------------------------------------------------------------------------------
    !Local variables
    !----------------------------------------------------------------------------
    real(8),dimension(dim) :: u,AB,BC,A,B,C,D
    integer :: i1,j1,i2,j2,ii,jj,ipas,jpas
    !----------------------------------------------------------------------------
!!$  !i1=min(max(floor((xyz1(1)-xmin0)/dx0),startx),endx)
!!$  !j1=min(max(floor((xyz1(2)-ymin0)/dy0),starty),endy)
!!$
!!$  !i2=min(max(floor((xyz2(1)-xmin0)/dx0),startx),endx)
!!$  !j2=min(max(floor((xyz2(2)-ymin0)/dy0),starty),endy) 

    i1=min(max(nint((xyz1(1)-xmin0)/dx0),sx-gx),ex+gx)
    j1=min(max(nint((xyz1(2)-ymin0)/dy0),sy-gy),ey+gy)
    i2=min(max(nint((xyz2(1)-xmin0)/dx0),sx-gx),ex+gx)
    j2=min(max(nint((xyz2(2)-ymin0)/dy0),sy-gy),ey+gy)

    ipas=sign(1,i2-i1)
    jpas=sign(1,j2-j1)
    ! print*,i1,j1,i2,j2
    !if (i1/=i2.and.j1/=j2) then
!!$  ! print*,'-----------------'
!!$  ! print*,nele
!!$  ! print*,xyz1
!!$  ! print*,xyz2
!!$  ! print*,i1,j1,i2,j2
!!$  ! print*,'-----------------'
    do jj=j1,j2,jpas
       do ii=i1,i2,ipas
          !------------------------------------------------------------------
          u=xyz2-xyz1
          !------------------------------------------------------------------
          A=[(ii-1._8/2)*dx0+xmin0,(jj-1._8/2)*dy0+ymin0]
          B=[(ii+1._8/2)*dx0+xmin0,(jj-1._8/2)*dy0+ymin0]
          C=[(ii+1._8/2)*dx0+xmin0,(jj+1._8/2)*dy0+ymin0]
          D=[(ii-1._8/2)*dx0+xmin0,(jj+1._8/2)*dy0+ymin0]
          !-----------------sense and direction of vector--------------------
          !---------------------pointing on the rightside--------------------
          AB=B-A
          !---------------------pointing on the upside-----------------------
          BC=C-B
          !------------------------------------------------------------------
          !------------------------------------------------------------------
          if (dot_product(u,AB) >=0) then
             if ( intersection(xyz2,xyz1,B,C) ) then
                call intersection_face(xyz1,xyz2,B,C,inters%face_y,numy,ii+1,jj,ny,ninit,nele,nbul)
             end if
          else
             if ( intersection(xyz2,xyz1,D,A) ) then
                call intersection_face(xyz1,xyz2,D,A,inters%face_y,numy,ii,jj,ny,ninit,nele,nbul)
             end if
          end if
          !------------------------------------------------------------------
          !------------------------------------------------------------------
          if (dot_product(u,BC) >=0) then
             if ( intersection(xyz2,xyz1,C,D) ) then
                call intersection_face(xyz1,xyz2,C,D,inters%face_x,numx,ii,jj+1,nx,ninit,nele,nbul)
             end if
          else
             if ( intersection(xyz2,xyz1,A,B) ) then
                call intersection_face(xyz1,xyz2,A,B,inters%face_x,numx,ii,jj,nx,ninit,nele,nbul)
             end if
          end if
          !------------------------------------------------------------------
       end do
    end do
    !------------------------------------------------------------------
  end subroutine phase_intersection_localization
  !*******************************************************************************
  !*******************************************************************************

  !*******************************************************************************
  !*******************************************************************************
  subroutine intersection_face(xyz1,xyz2,B,C,facey,numy,ii,jj,ny,ninit,nele,nbul)
    !*******************************************************************************
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    real(8),dimension(dim),intent(in)                 :: xyz1,xyz2,B,C
    type(intersect),dimension(:),intent(inout)        :: facey
    integer(8),dimension(:),allocatable,intent(inout) :: numy
    integer, intent(in)                               :: ii,jj,ny,ninit,nele,nbul
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    real(8),dimension(:,:),allocatable  :: xyz_temp
    integer,dimension(:),allocatable    :: ele_temp,bul_temp
    real(8),dimension(dim) :: xyz0
    integer :: old_size,new_size,ly
    !-------------------------------------------------------------------------------
    !print*,'BC-------'
    call find_intersection_point(xyz2,xyz1,B,C,xyz0)
    ly=(ii+gx)+(jj+gy)*ny+1
    !print*,ii,jj,ly
    old_size=numy(ly)
    numy(ly)=numy(ly)+1
    !print*,ii,jj,numy(ly)
    !print*,'ny',ly,numy(ly),xyz0
    if ( allocated(facey(ly)%xyzI) ) then 
       new_size=numy(ly)
       allocate( xyz_temp(dim,new_size),ele_temp(new_size),bul_temp(new_size) )
       xyz_temp(1,1:old_size)=facey(ly)%xyzI(1,1:old_size)
       xyz_temp(2,1:old_size)=facey(ly)%xyzI(2,1:old_size)
       ele_temp(1:old_size)=facey(ly)%num_ele(1:old_size)
       bul_temp(1:old_size)=facey(ly)%num_bul(1:old_size)

       call MOVE_ALLOC( FROM=xyz_temp,TO=facey(ly)%xyzI )
       call MOVE_ALLOC( FROM=ele_temp,TO=facey(ly)%num_ele )
       call MOVE_ALLOC( FROM=bul_temp,TO=facey(ly)%num_bul )

       facey(ly)%xyzI(1,numy(ly))=xyz0(1)
       facey(ly)%xyzI(2,numy(ly))=xyz0(2)
       facey(ly)%num_ele(numy(ly))=nele
       facey(ly)%num_bul(numy(ly))=nbul
    else
       allocate( facey(ly)%xyzI(dim,ninit) )
       allocate( facey(ly)%num_ele(ninit) )
       allocate( facey(ly)%num_bul(ninit) )
    end if
    facey(ly)%xyzI(1,numy(ly))=xyz0(1)
    facey(ly)%xyzI(2,numy(ly))=xyz0(2)
    facey(ly)%num_ele(numy(ly))=nele
    facey(ly)%num_bul(numy(ly))=nbul
    !-------------------------------------------------------------------------------
  end subroutine intersection_face
  !*******************************************************************************
  subroutine velocity_pressure_intersect_loc( bul,facex,nbul,nele,l,nt ) 
    !*******************************************************************************
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    type(bulle),dimension(:),allocatable,intent(in) :: bul
    type(intersect),intent(inout)                   :: facex
    integer,intent(in)                              :: nbul,nele,l,nt
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    real(8),dimension(dim) :: xyz1,xyz2,v1,v2,v0,xyz0,ve1,ve2,gradu_exact
    real(8),dimension(dim) :: grau1_1,grav1_1,grau2_1,grav2_1,grau1_2,grav1_2,grau2_2,grav2_2
    real(8)                :: alpha,beta,erreur,P1_1,P2_1,P1_2,P2_2,kappa_1,kappa_2
    !-------------------------------------------------------------------------------
    !write(86,*) nbul,nele
    xyz1= bul( nbul )%som( bul( nbul )%ele( nele )%nusom(1) )%xyz
    xyz2= bul( nbul )%som( bul( nbul )%ele( nele )%nusom(2) )%xyz
    xyz0=[ facex%xyzI(1,l),facex%xyzI(2,l) ]
    alpha=sqrt((xyz1(1)-xyz0(1))*(xyz1(1)-xyz0(1)) + (xyz1(2)-xyz0(2))*(xyz1(2)-xyz0(2)))
    beta= sqrt((xyz2(1)-xyz0(1))*(xyz2(1)-xyz0(1)) + (xyz2(2)-xyz0(2))*(xyz2(2)-xyz0(2)))
    !----------------velocity interpolation on intersection--------------
    v1= bul( nbul )%som( bul( nbul )%ele( nele )%nusom(1) )%vp!_star
    v2= bul( nbul )%som( bul( nbul )%ele( nele )%nusom(2) )%vp!_star
    v0=(beta*v1 + alpha* v2)/(alpha+beta)
    !--------------------------------------------------------------------
    facex%vI(1,l)=(beta*v1(1) + alpha* v2(1))/(alpha+beta)
    facex%vI(2,l)=(beta*v1(2) + alpha* v2(2))/(alpha+beta)
    !----------------gradient interpolation on intersection--------------
    grau1_1=bul( nbul )%som( bul( nbul )%ele( nele )%nusom(1) )%gradu1!_star
    grav1_1=bul( nbul )%som( bul( nbul )%ele( nele )%nusom(1) )%gradv1!_star
    grau2_1=bul( nbul )%som( bul( nbul )%ele( nele )%nusom(1) )%gradu2!_star
    grav2_1=bul( nbul )%som( bul( nbul )%ele( nele )%nusom(1) )%gradv2!_star
    grau1_2=bul( nbul )%som( bul( nbul )%ele( nele )%nusom(2) )%gradu1!_star
    grav1_2=bul( nbul )%som( bul( nbul )%ele( nele )%nusom(2) )%gradv1!_star
    grau2_2=bul( nbul )%som( bul( nbul )%ele( nele )%nusom(2) )%gradu2!_star
    grav2_2=bul( nbul )%som( bul( nbul )%ele( nele )%nusom(2) )%gradv2!_star
    !--------------------------------------------------------------------
    facex%gradu1(1,l)=(beta*grau1_1(1) + alpha*grau1_2(1) )/(alpha+beta)
    facex%gradu1(2,l)=(beta*grau1_1(2) + alpha*grau1_2(2) )/(alpha+beta)
    facex%gradv1(1,l)=(beta*grav1_1(1) + alpha*grav1_2(1) )/(alpha+beta)
    facex%gradv1(2,l)=(beta*grav1_1(2) + alpha*grav1_2(2) )/(alpha+beta)
    facex%gradu2(1,l)=(beta*grau2_1(1) + alpha*grau2_2(1) )/(alpha+beta)
    facex%gradu2(2,l)=(beta*grau2_1(2) + alpha*grau2_2(2) )/(alpha+beta)
    facex%gradv2(1,l)=(beta*grav2_1(1) + alpha*grav2_2(1) )/(alpha+beta)
    facex%gradv2(2,l)=(beta*grav2_1(2) + alpha*grav2_2(2) )/(alpha+beta)
    !----------------pressure interpolation on intersection--------------
    P1_1= bul( nbul )%som( bul( nbul )%ele( nele )%nusom(1) )%P1
    P1_2= bul( nbul )%som( bul( nbul )%ele( nele )%nusom(2) )%P1
    P2_1= bul( nbul )%som( bul( nbul )%ele( nele )%nusom(1) )%P2
    P2_2= bul( nbul )%som( bul( nbul )%ele( nele )%nusom(2) )%P2
    !--------------------------------------------------------------------
    facex%pI1(l)=( beta*P1_1 + alpha* P1_2 )/(alpha+beta)
    facex%pI2(l)=( beta*P2_1 + alpha* P2_2 )/(alpha+beta)
    !----------------kappa interpolation on intersection--------------
    kappa_1= bul( nbul )%som( bul( nbul )%ele( nele )%nusom(1) )%kappa
    kappa_2= bul( nbul )%som( bul( nbul )%ele( nele )%nusom(2) )%kappa
    !--------------------------------------------------------------------
    facex%kappaI(l)=( beta*kappa_1 + alpha* kappa_2 )/(alpha+beta)
    !--------------------------------------------------------------------
    write(75,*) facex%PI1(l),facex%PI2(l)

!!$       call exact_velocity(xyz0,ve1,nt,gradu_exact)
!!$       erreur=(abs(ve1(1)-v0(1))+abs(ve1(2)-v0(2)))
!!$       write(86,*) '-----------prim----------------'
!!$       write(86,*) erreur
!!$       write(86,*) v0
!!$       call exact_velocity(xyz1,ve1,nt,gradu_exact)
!!$       erreur=(abs(ve1(1)-v1(1))+abs(ve1(2)-v1(2)))
!!$       write(86,*) '-----------bis----------------'
!!$       write(86,*) erreur
!!$       write(86,*) ve1
!!$       write(86,*) v1
    !-------------------------------------------------------------------------------
  end subroutine velocity_pressure_intersect_loc
  !*******************************************************************************
  !*******************************************************************************
  subroutine exact_volume_cell(dx0,dy0,xmin0,ymin0,startx,endx,starty,endy,bul,inters,nx,ny,&
       volume_cell,mean_curv,mean_stf,ind,phase)
    !*******************************************************************************
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    type(bulle),dimension(:),allocatable,intent(in)     :: bul
    type(facet),intent(inout)                           :: inters
    integer,intent(in)                                  :: startx,starty,endx,endy,nx,ny,ind
    real(8),intent(in)                                  :: dx0,dy0,xmin0,ymin0
    real(8),dimension(:,:,:), allocatable,intent(inout) :: volume_cell,mean_curv,mean_stf
    logical,dimension(:,:,:), allocatable,intent(in)    :: phase
    !integer,dimension(sx-3:ex+3,sy-3:ey+3,1)           :: decompte
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    integer :: l,m,i0,j0,i,j
    real(8) :: xyz1,xyz2
    !-------------------------------------------------------------------------------
!!$    do l=1,nbbul
!!$       if (bul(l)%mask) then
!!$          do m=1,bul(l)%nbele
!!$             if (bul(l)%ele(m)%mask) then 
!!$                xyz1=bul(l)%som( bul(l)%ele(m)%nusom(1) )%xyz
!!$                xyz2=bul(l)%som( bul(l)%ele(m)%nusom(2) )%xyz
!!$                call decompte_element(xyz1,xyz2,decompte,dx0,dy0,xmin0,ymin0,startx,endx,starty,endy)
!!$             end if
!!$          end do
!!$       end if
!!$    end do

    volume_cell=0
    mean_curv=0
    mean_stf=0
    do j=starty,endy
       do i=startx,endx
          !if (decompte(i,j,1)>0) then
          !print*,'in'
          call phase_volume_calculation(dx0,dy0,xmin0,ymin0,startx,endx,starty,endy,bul,inters,nx,ny,i,j,&
               volume_cell(i,j,1),mean_curv(i,j,1),mean_stf(i,j,1),ind)
         ! print*,'out'
          !end if
          if ( ( .not. phase(i,j,1) ) .and. abs( volume_cell(i,j,1) )<1.d-15 ) volume_cell(i,j,1)=dx0*dy0
          !write(78,*) i,j,volume_cell(i,j,1)
       end do
    end do
    !-------------------------------------------------------------------------------
  end subroutine exact_volume_cell
  !*******************************************************************************
  subroutine phase_volume_calculation(dx0,dy0,xmin0,ymin0,startx,endx,starty,endy,bul,inters,nx,ny,i,j,&
       volume_cell,mean_curv,mean_stf,ind)
    !*******************************************************************************
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    type(bulle),dimension(:),allocatable,intent(in)        :: bul
    type(facet),intent(inout)                              :: inters
    integer,intent(in)                                     :: startx,starty,endx,endy,nx,ny,i,j,ind
    real(8),intent(in)                                     :: dx0,dy0,xmin0,ymin0
    real(8),intent(inout)                                  :: volume_cell,mean_curv,mean_stf
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    integer :: i1,j1,nbul,nele,left,right,up,down,nb_intersection,n,trigo
    integer :: nb_intersection1,nb_intersection2,nb_intersection3,nb_intersection4,num
    real(8) :: local_volume,curv1
    real(8),dimension(dim) :: som0,som1,u,v,AB,BC,CD,DA,A,B,C,D
    logical :: pass1
    !--------------------------------initial count----------------------------
!!$    print*,'!-------------------------------!'
!!$    print*,'new'
    pass1=.true.

    nb_intersection=0
    nb_intersection1=0
    nb_intersection2=0
    nb_intersection3=0
    nb_intersection4=0

    down=(i+gx)+(j+gy)*nx+1
    right=(i+gx+1)+(j+gy)*ny+1
    up=(i+gx)+(j+gy+1)*nx+1
    left=(i+gx)+(j+gy)*ny+1
!!$    down=i+j*nx+1
!!$    right=(i+1)+j*ny+1
!!$    up=i+(j+1)*nx+1
!!$    left=i+j*ny+1

    A=[(i-1._8/2)*dx0+xmin0,(j-1._8/2)*dy0+ymin0]
    B=[(i+1._8/2)*dx0+xmin0,(j-1._8/2)*dy0+ymin0]
    C=[(i+1._8/2)*dx0+xmin0,(j+1._8/2)*dy0+ymin0]
    D=[(i-1._8/2)*dx0+xmin0,(j+1._8/2)*dy0+ymin0]

!!$    nb_intersection4=size(inters%face_y(left)%num_ele)
!!$    if (.not. allocated(inters%face_y(left)%mask)) allocate(inters%face_y(left)%mask(nb_intersection4))
!!$    inters%face_y(left)%mask=.false.
!!$
!!$    nb_intersection2=size(inters%face_y(right)%num_ele)
!!$    if (.not. allocated(inters%face_y(right)%mask)) allocate(inters%face_y(right)%mask(nb_intersection2))
!!$    inters%face_y(right)%mask=.false.
!!$
!!$    nb_intersection3=size(inters%face_x(up)%num_ele)
!!$    if (.not. allocated(inters%face_x(up)%mask)) allocate(inters%face_x(up)%mask(nb_intersection3))
!!$    inters%face_x(up)%mask=.false.
!!$
!!$    nb_intersection1=size(inters%face_x(down)%num_ele)
!!$    if (.not. allocated(inters%face_x(down)%mask)) allocate(inters%face_x(down)%mask(nb_intersection1))
!!$    inters%face_x(down)%mask=.false.
!!$
!!$
    nb_intersection4=0
    if ( allocated( inters%face_y(left)%num_ele) ) then
       nb_intersection4=size(inters%face_y(left)%num_ele)
       if (.not. allocated(inters%face_y(left)%mask)) allocate(inters%face_y(left)%mask(nb_intersection4))
       inters%face_y(left)%mask=.false.
    end if

!!$    !    nb_intersection4=size(inters%face_y(left)%num_ele)
!!$    !       if (.not. allocated(inters%face_y(left)%mask)) allocate(inters%face_y(left)%mask(nb_intersection4))
!!$    !    inters%face_y(left)%mask=.false.

    nb_intersection2=0
    if ( allocated( inters%face_y(right)%num_ele) ) then
       nb_intersection2=size(inters%face_y(right)%num_ele)
       if (.not. allocated(inters%face_y(right)%mask)) allocate(inters%face_y(right)%mask(nb_intersection2))
       inters%face_y(right)%mask=.false.
    end if

!!$    !   nb_intersection2=size(inters%face_y(right)%num_ele)
!!$    !   if (.not. allocated(inters%face_y(right)%mask)) allocate(inters%face_y(right)%mask(nb_intersection2))
!!$    !   inters%face_y(right)%mask=.false.

    nb_intersection3=0
    if ( allocated( inters%face_x(up)%num_ele) ) then
       nb_intersection3=size(inters%face_x(up)%num_ele)
       if (.not. allocated(inters%face_x(up)%mask))  allocate(inters%face_x(up)%mask(nb_intersection3))
       inters%face_x(up)%mask=.false.
    end if

!!$    !   nb_intersection3=size(inters%face_x(up)%num_ele)
!!$    !   if (.not. allocated(inters%face_x(up)%mask)) allocate(inters%face_x(up)%mask(nb_intersection3))
!!$    !   inters%face_x(up)%mask=.false.

    nb_intersection1=0
    if ( allocated( inters%face_x(down)%num_ele) ) then
       nb_intersection1=size(inters%face_x(down)%num_ele)
       if (.not. allocated(inters%face_x(down)%mask)) allocate(inters%face_x(down)%mask(nb_intersection1))
       inters%face_x(down)%mask=.false.
    end if

!!$    !   nb_intersection1=size(inters%face_x(down)%num_ele)
!!$    !   if (.not. allocated(inters%face_x(down)%mask)) allocate(inters%face_x(down)%mask(nb_intersection1))
!!$    !   inters%face_x(down)%mask=.false.

    num=0
!!$ !   print*,'!-------------------------------!'
!!$ !   print*, nb_intersection1,nb_intersection2
!!$ !   print*, nb_intersection3,nb_intersection4
!!$ !   print*,'!-------------------------------!'

    nb_intersection=nb_intersection1+nb_intersection2+nb_intersection3+nb_intersection4
!!$ !   print*, i,j,nb_intersection
    !-------------------------------------------------------------------------
    !-------------------------------Initialization----------------------------
    ! do while ( nb_intersection > 0 )
    !--------------------------------------------------------------------------
    som0=[ (i-1._8/2)*dx0+xmin0,(j-1._8/2)*dy0+ymin0 ]
    !------------------------------face 1--------------------------------------          
    if ( allocated(inters%face_x(down)%xyzI) ) then
       !-----------------------------------------------------------------------
       AB=B-A
       bc1: do n =1,nb_intersection1
          local_volume=0            
          if (  .not.inters%face_x(down)%mask(n) ) then
!!$    !         print*,'in'
             som1 =[ inters%face_x(down)%xyzI(1,n),inters%face_x(down)%xyzI(2,n) ]
             curv1=  inters%face_x(down)%kappaI(n)
!!$     !        print*,'som'
!!$     !        print*,som0
!!$     !        print*,som1
!!$     !        print*,'som'
             nele=inters%face_x(down)%num_ele(n)
             nbul=inters%face_x(down)%num_bul(n)
             inters%face_x(down)%mask(n)=.true.
             nb_intersection=nb_intersection-1
             call phase_face_calculation(inters%face_x,inters%face_y,bul,som0,som1,dx0,dy0,xmin0,ymin0,startx,&
                  endx,starty,endy,local_volume,i,j,nele,nbul,left,right,up,down,nb_intersection,trigo,AB,curv1,&
                  mean_curv,mean_stf,ind,num)
             local_volume=local_volume*sign(1,trigo)
             volume_cell=volume_cell+local_volume
          else
!!$  !           print*,'maks faux'
             cycle bc1
          end if
       end do bc1
    end if
!!$  !  print*, 'down',nb_intersection1,nb_intersection
    !------------------------------face 2----------------------------------------------
    if ( allocated(inters%face_y(right)%xyzI) ) then
       !-------------------------------------------------------------------------------
       BC=C-B
       bc2:do n=1,nb_intersection2
          local_volume=0
          if (  .not.inters%face_y(right)%mask(n) ) then
!!$   !          print*,'in'
             som1=[ (i+1._8/2)*dx0+xmin0,(j-1._8/2)*dy0+ymin0 ]
             u=som1-som0
             som1=[ inters%face_y(right)%xyzI(1,n),inters%face_y(right)%xyzI(2,n) ]
             curv1= inters%face_y(right)%kappaI(n)
             nele=inters%face_y(right)%num_ele(n)
             nbul=inters%face_y(right)%num_bul(n)
             inters%face_y(right)%mask(n)=.true.
             nb_intersection=nb_intersection-1
             v=som1-som0
             local_volume=local_volume+prod_vec(u,v)
             call phase_face_calculation(inters%face_x,inters%face_y,bul,som0,som1,dx0,dy0,xmin0,ymin0,startx,&
                  endx,starty,endy,local_volume,i,j,nele,nbul,left,right,up,down,nb_intersection,trigo,BC,curv1,&
                  mean_curv,mean_stf,ind,num)
             local_volume=local_volume*sign(1,trigo)
             volume_cell=volume_cell+local_volume
          else
!!$    !         print*,'maks faux'
             cycle bc2
          end if
       end do bc2
    end if
!!$ !   print*, 'right',nb_intersection2,nb_intersection
    !------------------------------face 3----------------------------------------------
    if ( allocated(inters%face_x(up)%xyzI) ) then
       !-------------------------------------------------------------------------------
       CD=D-C
       bc3:do n=nb_intersection3,1,-1
          local_volume=0
          if (  .not.inters%face_x(up)%mask(n) ) then
!!$  !           print*,'in'
             som1=[ (i+1._8/2)*dx0+xmin0,(j-1._8/2)*dy0+ymin0 ]
             u=som1-som0
             som1=[ (i+1._8/2)*dx0+xmin0,(j+1._8/2)*dy0+ymin0 ]
             v=som1-som0
             local_volume=local_volume+prod_vec(u,v)
             som1=[ inters%face_x(up)%xyzI(1,n),inters%face_x(up)%xyzI(2,n) ]
             curv1= inters%face_x(up)%kappaI(n)
!!$   !          print*,'som'
!!$   !          print*,som0
!!$   !          print*,som1
!!$   !          print*,'som'
             nele=inters%face_x(up)%num_ele(n)
             nbul=inters%face_x(up)%num_bul(n)
             inters%face_x(up)%mask(n)=.true.
             nb_intersection=nb_intersection-1
             u=v
             v=som1-som0
             local_volume=local_volume+prod_vec(u,v)
             call phase_face_calculation(inters%face_x,inters%face_y,bul,som0,som1,dx0,dy0,xmin0,ymin0,startx,&
                  endx,starty,endy,local_volume,i,j,nele,nbul,left,right,up,down,nb_intersection,trigo,CD,curv1,&
                  mean_curv,mean_stf,ind,num)
             local_volume=local_volume*sign(1,trigo)
             volume_cell=volume_cell+local_volume
          else
!!$  !           print*,'maks faux'
             cycle bc3
          end if
       end do bc3
    end if
!!$  !  print*,'up',nb_intersection3, nb_intersection
    !------------------------------face 4------------------------------------------
    if ( allocated(inters%face_y(left)%xyzI) ) then
       !---------------------------------------------------------------------------
       DA=A-D
       bc4:do n=nb_intersection4,1,-1
          local_volume=0
          if (  .not.inters%face_y(left)%mask(n) ) then
!!$  !           print*,'in'
             som1=[ (i+1._8/2)*dx0+xmin0,(j-1._8/2)*dy0+ymin0 ]
             u=som1-som0
             som1=[ (i+1._8/2)*dx0+xmin0,(j+1._8/2)*dy0+ymin0 ]
             v=som1-som0
             local_volume=local_volume+prod_vec(u,v)
             som1=[ (i-1._8/2)*dx0+xmin0,(j+1._8/2)*dy0+ymin0 ]
             u=v
             v=som1-som0
             local_volume=local_volume+prod_vec(u,v)
             som1=[ inters%face_y(left)%xyzI(1,n),inters%face_y(left)%xyzI(2,n) ]
             curv1= inters%face_y(left)%kappaI(n)
!!$ !            print*,'som'
!!$ !            print*,som0
!!$ !            print*,som1
!!$ !            print*,'som'
             nele=inters%face_y(left)%num_ele(n)
             nbul=inters%face_y(left)%num_bul(n)
             inters%face_y(left)%mask(n)=.true.
             nb_intersection=nb_intersection-1
             u=v
             v=som1-som0
             local_volume=local_volume+prod_vec(u,v)
             call phase_face_calculation(inters%face_x,inters%face_y,bul,som0,som1,dx0,dy0,xmin0,ymin0,startx,&
                  endx,starty,endy,local_volume,i,j,nele,nbul,left,right,up,down,nb_intersection,trigo,DA,curv1,&
                  mean_curv,mean_stf,ind,num)
             local_volume=local_volume*sign(1,trigo)
             volume_cell=volume_cell+local_volume
          else
!!$  !           print*,'maks faux'
             cycle bc4
          end if
       end do bc4
    end if
    if (num>0) mean_curv = mean_curv/num
!!$ !   print*, 'left',nb_intersection4,nb_intersection
    !-------------------------------------------------------------------------------
    !  end do
    !----------------------------------last loop------------------------------------
    volume_cell=volume_cell/2
    do while( volume_cell/dx0/dy0 > 1 .or. volume_cell < 0) 
       if ( volume_cell < 0 ) then
          volume_cell=volume_cell+dx0*dy0
       else 
          volume_cell=volume_cell-dx0*dy0
       end if
    end do
    !-------------------------------------------------------------------------------
  end subroutine phase_volume_calculation
  !*********************************************************************************
  !*********************************************************************************
  subroutine phase_face_calculation(facex,facey,bul,som0,som1,dx0,dy0,xmin0,ymin0,&
       startx,endx,starty,endy,local_volume,i,j,nele,nbul,left,right,up,down,&
       nb_intersection,trigo,w,curv1,mean_curv,mean_stf,ind,num)
    !*******************************************************************************
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    type(bulle),dimension(:),allocatable,intent(in)        :: bul
    real(8),dimension(dim),intent(in)                      :: som0,w
    real(8),dimension(dim),intent(inout)                   :: som1
    type(intersect),dimension(:),intent(inout)             :: facex,facey
    integer,intent(inout)                                  :: nele,nb_intersection,num
    integer,intent(in)                                     :: left,right,up,down,nbul,i,j,ind
    integer,intent(in)                                     :: startx,starty,endx,endy
    real(8),intent(in)                                     :: dx0,dy0,xmin0,ymin0
    real(8),intent(inout)                                  :: local_volume,mean_curv,curv1,mean_stf
    integer,intent(out)                                    :: trigo
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    real(8),dimension(dim) :: u,v,xyz_ele,xyz1,xyz2,somi,n_dl
    real(8)                :: curv,dist
    integer                :: i1,j1,n,nu
    logical                :: rightface
    !-------------------------------------------------------------------------------
    u=som1-som0 ; curv=0
    num=num+1
    mean_curv=mean_curv+curv1
    somi=som1
    xyz1= bul( nbul )%som( bul( nbul )%ele( nele )%nusom(1) )%xyz
    xyz2= bul( nbul )%som( bul( nbul )%ele( nele )%nusom(2) )%xyz
    xyz_ele= xyz2-xyz1
    if (prod_vec(w,xyz_ele)>=0) then
       !---------------------------trigo--------------------------
       som1=xyz2      
       trigo=1  ; nu=1
    else
       !--------------------------antitrigo-----------------------
       som1=xyz1
       trigo=-1 ; nu=0
    end if
!!$    i1=min(max(nint((som1(1)-xmin0)/dx0),startx),endx+1)
!!$    j1=min(max(nint((som1(2)-ymin0)/dy0),starty),endy+1)
      i1=min(max(nint((som1(1)-xmin0)/dx0),sx-gx),ex+gx)
      j1=min(max(nint((som1(2)-ymin0)/dy0),sy-gy),ey+gy)
    !-----------------------------------------------------------------
    do while(i1==i .and. j1==j)
       v=som1-som0
       local_volume=local_volume+prod_vec(u,v)
       num=num+1
       mean_curv=mean_curv+curv1
       if (trigo==1) then
          !-----------------------som2 in the cell-----------------------
          !----------------------surface tension force-------------------
          curv=( curv1+bul( nbul )%som( bul( nbul )%ele( nele )%nusom(2) )%kappa )/2
          dist=sqrt( (som1(1)-somi(1))*(som1(1)-somi(1))+(som1(2)-somi(2))*(som1(2)-somi(2)) )
          call construction_normale_elt(somi,som1,n_dl)
          mean_stf=mean_stf+( curv*n_dl(ind)*dist )
          !-------------------------update-------------------------------
          curv1=bul( nbul )%som( bul( nbul )%ele( nele )%nusom(2) )%kappa 
          somi=som1
          nele=bul( nbul )%ele( nele )%ele_vois(2)
          som1=bul( nbul )%som( bul( nbul )%ele( nele )%nusom(2) )%xyz
       else
          !-----------------------som1 in the cell-----------------------
          !----------------------surface tension force-------------------
          curv=( curv1+bul( nbul )%som( bul( nbul )%ele( nele )%nusom(1) )%kappa )/2
          dist=sqrt( (som1(1)-somi(1))*(som1(1)-somi(1))+(som1(2)-somi(2))*(som1(2)-somi(2)) )
          call construction_normale_elt(som1,somi,n_dl)
          mean_stf=mean_stf+( curv*n_dl(ind)*dist )
          !-------------------------update-------------------------------
          curv1=bul( nbul )%som( bul( nbul )%ele( nele )%nusom(1) )%kappa 
          somi=som1
          nele=bul( nbul )%ele( nele )%ele_vois(1)
          som1=bul( nbul )%som( bul( nbul )%ele( nele )%nusom(1) )%xyz
       end if
       u=v
       !-----------mean_curvature calculation---------------------------
       !if (ind==1 .and. i>51) write(79,*) 'ele',n_dl(ind),trigo
       !if (ind==1) write(79,*) n_dl(1),mean_stf
       !----------------------------------------------------------------
!!$       i1=min(max(nint((som1(1)-xmin0)/dx0),startx),endx+1)
!!$       j1=min(max(nint((som1(2)-ymin0)/dy0),starty),endy+1)
         i1=min(max(nint((som1(1)-xmin0)/dx0),sx-gx),ex+gx)
         j1=min(max(nint((som1(2)-ymin0)/dy0),sy-gy),ey+gy)
       !----------------------------------------------------------------
    end do
    !----------------------------------------------------------
    if (i1==i) then
       if (j1>j) then
          !---------------------------------------------------
          !-------------------upper face----------------------
          call phase_search_face( som1,facex(up)%xyzI,nele,facex(up)%num_ele,&
               facex(up)%mask,size(facex(up)%num_ele),n,nb_intersection,rightface )
          !---------------------------curvature at intersection---------------------------
          curv=(curv1+facex(up)%kappaI(n))/2
          dist=sqrt( (som1(1)-somi(1))*(som1(1)-somi(1))+(som1(2)-somi(2))*(som1(2)-somi(2)) )
          call construction_normale_elt(nu*somi+mod(nu+1,2)*som1,nu*som1+mod(nu+1,2)*somi,n_dl)
          !-------------------------------------------------------------------------------
          curv1=facex(up)%kappaI(n)
          !-------------------------------------------------------------------------------
          v=som1-som0
          local_volume=local_volume+prod_vec(u,v)
          u=v
          som1=[ (i-1._8/2)*dx0+xmin0,(j+1._8/2)*dy0+ymin0 ]
          v=som1-som0
          local_volume=local_volume+prod_vec(u,v)
       else
          !--------------------------------------------------
          !-------------------down face----------------------
          call phase_search_face( som1,facex(down)%xyzI,nele,facex(down)%num_ele,&
               facex(down)%mask,size( facex(down)%num_ele),n,nb_intersection,rightface )
          !---------------------------curvature at intersection---------------------------
          curv=(curv1+facex(down)%kappaI(n))/2
          dist=sqrt( (som1(1)-somi(1))*(som1(1)-somi(1))+(som1(2)-somi(2))*(som1(2)-somi(2)) )
          call construction_normale_elt(nu*somi+mod(nu+1,2)*som1,nu*som1+mod(nu+1,2)*somi,n_dl)
          !-------------------------------------------------------------------------------
          v=som1-som0
          local_volume=local_volume+prod_vec(u,v)
          u=v
          som1=[ (i+1._8/2)*dx0+xmin0,(j-1._8/2)*dy0+ymin0 ]
          v=som1-som0
          local_volume=local_volume+prod_vec(u,v)
          u=v
          som1=[ (i+1._8/2)*dx0+xmin0,(j+1._8/2)*dy0+ymin0 ]
          v=som1-som0
          local_volume=local_volume+prod_vec(u,v)
          u=v
          som1=[ (i-1._8/2)*dx0+xmin0,(j+1._8/2)*dy0+ymin0 ]
          v=som1-som0
          local_volume=local_volume+prod_vec(u,v)
       end if
    else if (j1==j) then
       if (i1>i) then 
          !-----------------------------------------------
          !-----------------right face--------------------
          call phase_search_face( som1,facey(right)%xyzI,nele,facey(right)%num_ele,&
               facey(right)%mask,size( facey(right)%num_ele),n,nb_intersection,rightface )
          !---------------------------curvature at intersection---------------------------
          curv=(curv1+facey(right)%kappaI(n))/2
          dist=sqrt( (som1(1)-somi(1))*(som1(1)-somi(1))+(som1(2)-somi(2))*(som1(2)-somi(2)) )
          call construction_normale_elt(nu*somi+mod(nu+1,2)*som1,nu*som1+mod(nu+1,2)*somi,n_dl)
          !-------------------------------------------------------------------------------
          v=som1-som0
          local_volume=local_volume+prod_vec(u,v)
          u=v
          som1=[ (i+1._8/2)*dx0+xmin0,(j+1._8/2)*dy0+ymin0 ]
          v=som1-som0
          local_volume=local_volume+prod_vec(u,v)
          u=v
          som1=[ (i-1._8/2)*dx0+xmin0,(j+1._8/2)*dy0+ymin0 ]
          v=som1-som0
          local_volume=local_volume+prod_vec(u,v)
       else
          !-----------------------------------------------
          !-----------------left face---------------------
          call phase_search_face( som1,facey(left)%xyzI,nele,facey(left)%num_ele,&
               facey(left)%mask,size( facey(left)%num_ele),n,nb_intersection,rightface )
          !---------------------------curvature at intersection---------------------------
          !print*,rightface
          curv=(curv1+facey(left)%kappaI(n))/2
          dist=sqrt( (som1(1)-somi(1))*(som1(1)-somi(1))+(som1(2)-somi(2))*(som1(2)-somi(2)) )
          call construction_normale_elt(nu*somi+mod(nu+1,2)*som1,nu*som1+mod(nu+1,2)*somi,n_dl)
          if (trigo==1) then
             call construction_normale_elt(somi,som1,n_dl)
          else
             call construction_normale_elt(som1,somi,n_dl)
          end if
          !-------------------------------------------------------------------------------
          v=som1-som0
          local_volume=local_volume+prod_vec(u,v)
       end if
    else
       !-----------------------------------------diagonals---------------------------------
       if ( i1<=i-1 .and. j1<=j-1   )then
          !----------------down left corner----------------
          call phase_search_face( som1,facex(down)%xyzI,nele,facex(down)%num_ele,&
               facex(down)%mask,size( facex(down)%num_ele),n,nb_intersection,rightface )
          !if (n > size( facex(down)%num_ele) ) then
          if ( rightface ) then
             !---------------------------curvature at intersection---------------------------
             curv=(curv1+facex(down)%kappaI(n))/2
             dist=sqrt( (som1(1)-somi(1))*(som1(1)-somi(1))+(som1(2)-somi(2))*(som1(2)-somi(2)) )
             call construction_normale_elt(nu*somi+mod(nu+1,2)*som1,nu*som1+mod(nu+1,2)*somi,n_dl)
             !-------------------------------------------------------------------------------
             v=som1-som0
             local_volume=local_volume+prod_vec(u,v)
             u=v
             som1=[ (i+1._8/2)*dx0+xmin0,(j-1._8/2)*dy0+ymin0 ]
             v=som1-som0
             local_volume=local_volume+prod_vec(u,v)
             u=v
             som1=[ (i+1._8/2)*dx0+xmin0,(j+1._8/2)*dy0+ymin0 ]
             v=som1-som0
             local_volume=local_volume+prod_vec(u,v)
             u=v
             som1=[ (i-1._8/2)*dx0+xmin0,(j+1._8/2)*dy0+ymin0 ]
             v=som1-som0
             local_volume=local_volume+prod_vec(u,v)!*sign(1,trigo)
          else
             call phase_search_face( som1,facey(left)%xyzI,nele,facey(left)%num_ele,&
                  facey(left)%mask,size( facey(left)%num_ele),n,nb_intersection,rightface )
             !---------------------------curvature at intersection---------------------------
             curv=(curv1+facey(left)%kappaI(n))/2
             dist=sqrt( (som1(1)-somi(1))*(som1(1)-somi(1))+(som1(2)-somi(2))*(som1(2)-somi(2)) )
             call construction_normale_elt(nu*somi+mod(nu+1,2)*som1,nu*som1+mod(nu+1,2)*somi,n_dl)
             !-------------------------------------------------------------------------------
             v=som1-som0
             local_volume=local_volume+prod_vec(u,v)
          end if
       else if ( i1<=i-1 .and. j1>=j+1 ) then
          !----------------upper left corner----------------
          call phase_search_face( som1,facex(up)%xyzI,nele,facex(up)%num_ele,&
               facex(up)%mask,size( facex(up)%num_ele),n,nb_intersection,rightface )
          !if (n > size( facex(up)%num_ele) ) then
          if ( rightface ) then
             !---------------------------curvature at intersection---------------------------
             curv=(curv1+facex(up)%kappaI(n))/2
             dist=sqrt( (som1(1)-somi(1))*(som1(1)-somi(1))+(som1(2)-somi(2))*(som1(2)-somi(2)) )
             call construction_normale_elt(nu*somi+mod(nu+1,2)*som1,nu*som1+mod(nu+1,2)*somi,n_dl)
             !-------------------------------------------------------------------------------
             v=som1-som0
             local_volume=local_volume+prod_vec(u,v)
             u=v
             som1=[ (i-1._8/2)*dx0+xmin0,(j+1._8/2)*dy0+ymin0 ]
             v=som1-som0
             local_volume=local_volume+prod_vec(u,v)
          else
             call phase_search_face( som1,facey(left)%xyzI,nele,facey(left)%num_ele,&
                  facey(left)%mask,size( facey(left)%num_ele),n,nb_intersection,rightface )
             !---------------------------curvature at intersection---------------------------
             curv=(curv1+facey(left)%kappaI(n))/2
             dist=sqrt( (som1(1)-somi(1))*(som1(1)-somi(1))+(som1(2)-somi(2))*(som1(2)-somi(2)) )
             call construction_normale_elt(nu*somi+mod(nu+1,2)*som1,nu*som1+mod(nu+1,2)*somi,n_dl)
             !-------------------------------------------------------------------------------
             v=som1-som0
             local_volume=local_volume+prod_vec(u,v)
          end if
       else if ( i1>=i+1 .and. j1<=j-1 ) then
          !---------------down right corner-----------------
          call phase_search_face( som1,facex(down)%xyzI,nele,facex(down)%num_ele,&
               facex(down)%mask,size( facex(down)%num_ele),n,nb_intersection,rightface )
          ! if (n > size( facex(down)%num_ele) ) then
          if (rightface) then
             !---------------------------curvature at intersection---------------------------
             curv=(curv1+facex(down)%kappaI(n))/2
             dist=sqrt( (som1(1)-somi(1))*(som1(1)-somi(1))+(som1(2)-somi(2))*(som1(2)-somi(2)) )
             call construction_normale_elt(nu*somi+mod(nu+1,2)*som1,nu*som1+mod(nu+1,2)*somi,n_dl)
             !-------------------------------------------------------------------------------
             v=som1-som0
             local_volume=local_volume+prod_vec(u,v)
             u=v
             som1=[ (i+1._8/2)*dx0+xmin0,(j-1._8/2)*dy0+ymin0 ]
             v=som1-som0
             local_volume=local_volume+prod_vec(u,v)
             u=v
             som1=[ (i+1._8/2)*dx0+xmin0,(j+1._8/2)*dy0+ymin0 ]
             v=som1-som0
             local_volume=local_volume+prod_vec(u,v)
             u=v
             som1=[ (i-1._8/2)*dx0+xmin0,(j+1._8/2)*dy0+ymin0 ]
             v=som1-som0
             local_volume=local_volume+prod_vec(u,v)
          else
             call phase_search_face( som1,facey(right)%xyzI,nele,facey(right)%num_ele,&
                  facey(right)%mask,size( facey(right)%num_ele),n,nb_intersection,rightface )
             !---------------------------curvature at intersection---------------------------
             curv=(curv1+facey(right)%kappaI(n))/2
             dist=sqrt( (som1(1)-somi(1))*(som1(1)-somi(1))+(som1(2)-somi(2))*(som1(2)-somi(2)) )
             call construction_normale_elt(nu*somi+mod(nu+1,2)*som1,nu*som1+mod(nu+1,2)*somi,n_dl)
             !-------------------------------------------------------------------------------
             v=som1-som0
             local_volume=local_volume+prod_vec(u,v)
             u=v
             som1=[ (i+1._8/2)*dx0+xmin0,(j+1._8/2)*dy0+ymin0 ]
             v=som1-som0
             local_volume=local_volume+prod_vec(u,v)
             u=v
             som1=[ (i-1._8/2)*dx0+xmin0,(j+1._8/2)*dy0+ymin0 ]
             v=som1-som0
             local_volume=local_volume+prod_vec(u,v)
          end if
       else
          !--------------upper right corner-----------------
          call phase_search_face( som1,facex(up)%xyzI,nele,facex(up)%num_ele,&
               facex(up)%mask,size( facex(up)%num_ele),n,nb_intersection,rightface )
          !   if (n > size( facex(up)%num_ele) ) then
          if (rightface) then
             !---------------------------curvature at intersection---------------------------
             curv=(curv1+facex(up)%kappaI(n))/2
             dist=sqrt( (som1(1)-somi(1))*(som1(1)-somi(1))+(som1(2)-somi(2))*(som1(2)-somi(2)) )
             call construction_normale_elt(nu*somi+mod(nu+1,2)*som1,nu*som1+mod(nu+1,2)*somi,n_dl)
             !-------------------------------------------------------------------------------
             v=som1-som0
             local_volume=local_volume+prod_vec(u,v)
             u=v
             som1=[ (i-1._8/2)*dx0+xmin0,(j+1._8/2)*dy0+ymin0 ]
             v=som1-som0
             local_volume=local_volume+prod_vec(u,v)
          else
             call phase_search_face( som1,facey(right)%xyzI,nele,facey(right)%num_ele,&
                  facey(right)%mask,size( facey(right)%num_ele),n,nb_intersection,rightface )
             !---------------------------curvature at intersection---------------------------
             curv=(curv1+facey(right)%kappaI(n))/2
             dist=sqrt( (som1(1)-somi(1))*(som1(1)-somi(1))+(som1(2)-somi(2))*(som1(2)-somi(2)) )
             call construction_normale_elt(nu*somi+mod(nu+1,2)*som1,nu*som1+mod(nu+1,2)*somi,n_dl)
             !-------------------------------------------------------------------------------
             v=som1-som0
             local_volume=local_volume+prod_vec(u,v)
             u=v
             som1=[ (i+1._8/2)*dx0+xmin0,(j+1._8/2)*dy0+ymin0 ]
             v=som1-som0
             local_volume=local_volume+prod_vec(u,v)
             u=v
             som1=[ (i-1._8/2)*dx0+xmin0,(j+1._8/2)*dy0+ymin0 ]
             v=som1-som0
             local_volume=local_volume+prod_vec(u,v)        
          end if
       end if
    end if
    !--------------------------mean curvature caluclation---------------------------
    mean_curv=mean_curv+curv1
    num=num+1
    !--------------------------surface tension force--------------------------------
    mean_stf=mean_stf+( curv*n_dl(ind)*dist )
    !if (ind==1 .and. i>51) write(79,*) 'last',n_dl(ind),trigo
    !if (ind==2 .and. j>50) write(79,*) 'ele',curv1,n_dl(ind)
    !if (ind==1) write(79,*) n_dl(1),mean_stf

    !-------------------------------------------------------------------------------
  end subroutine phase_face_calculation
  !*********************************************************************************
  subroutine phase_search_face(som1,xyzI,nele,num_ele,mask,size,n,n_intersection,rightface)
    !*******************************************************************************
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    real(8),dimension(:),intent(inout)             :: som1
    real(8),dimension(:,:),allocatable,intent(in)  :: xyzI
    integer,dimension(:),allocatable,intent(in)    :: num_ele
    integer,intent(in)                             :: nele,size
    logical,dimension(:),allocatable,intent(inout) :: mask
    integer,intent(out)                            :: n
    integer,intent(inout)                          :: n_intersection
    logical,intent(out)                            :: rightface
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    !integer :: n
    !-------------------------------------------------------------------------------
    if ( allocated(num_ele) ) then
       do n=1,size
          if ( nele==num_ele(n) ) then
             som1(1)=xyzI(1,n)
             som1(2)=xyzI(2,n)
             mask(n)=.true.
             rightface=.true.
             n_intersection=n_intersection-1
             exit
          else
             if (n<size) then
                cycle
             else
                rightface=.false.
             end if
          end if
       end do
    else
       rightface=.false. 
    end if
    !-------------------------------------------------------------------------------
  end subroutine phase_search_face
  !*********************************************************************************
  subroutine erreur_ft(ft,nt,Erreur_test,bul)
    !*******************************************************************************
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    real(8), intent(out)                          :: Erreur_test
    integer, intent(in)                                    :: nt
    type(struct_front_tracking),intent(in)                 :: ft
    !type(sommet), dimension(:),intent(in)                  :: som!,sombis
    type(bulle),dimension(:)                               :: bul
    !-------------------------------------------------------------------------------
    !Local variables
    !----------------------------------------------------------------------------
    real(8),dimension(dim)::xyzref,xyz
    integer::n,i,j
    !----------------------------------------------------------------------------
    !xyzref(:)=sombis(1)%xyz(:)
    xyz(:)=bul(1)%som(1)%xyz(:)
    !Erreur=(abs(xyzref(1)-xyz(1))+abs(xyzref(2)-xyz(2)))
    Erreur_test=xyz(1)**2 + xyz(2)**2
  end subroutine erreur_ft
  !*******************************************************************************
  subroutine reference_RK4(ft,bul,nt)
    !*******************************************************************************
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables 
    !-------------------------------------------------------------------------------
    integer, intent(in)                                        :: nt
    type(struct_front_tracking), intent(in)                    :: ft
    type(bulle),dimension(ft%nbbul),intent(inout)              :: bul

    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    integer                                  :: m,n,l
    real(8)                         :: dtt
    real(8), dimension(dim)         :: xyz,xyzn,xyznn,xyznnn,vp,vpn,vpnn,vpnnn
    !-------------------------------------------------------------------------------
!!$    dtt=dt/10
!!$    nt=int(dt/dtt)
!!$    !---------------subdivision en plusieurs petits pas de temps-------------------------
!!$    do n =0,nt-1
!!$       do l=1,ft%nbbul
!!$          if (bul(l)%mask ) then
!!$             do m=1,bul(l)%nbsom
!!$                if (bul(l)%som(m)%mask) then
!!$
!!$                   xyz(:)=bul(l)%som(m)%xyz(:)
!!$                   bul(l)%som(m)%xyz_old(:)=bul(l)%som(m)%xyz(:)
!!$                   !----------------1-----------------------------------!
!!$                   vp(:)=vitesse(xyz(1),xyz(2),nt0*dt+(n-0)*dtt) 
!!$                   
!!$                   !----------------2-----------------------------------!
!!$                   xyzn(:)=xyz(:)+0.5d0*dtt*vp(:)
!!$                   vpn(:)=vitesse(xyzn(1),xyzn(2),nt0*dt+(n+1/2.d0)*dtt)
!!$
!!$                   !----------------3-----------------------------------!
!!$                   xyznn(:)=xyz(:)+0.5d0*dtt*vpn(:)
!!$                   vpnn(:)=vitesse(xyznn(1),xyznn(2),nt0*dt+(n+1/2.d0)*dtt) 
!!$               
!!$                   !----------------4-----------------------------------!
!!$                   xyznnn(:)=xyz(:)+dtt*vpnn(:)
!!$                   vpnnn(:)=vitesse(xyznnn(1),xyznnn(2),nt0*dt+(n+1)*dtt)  
!!$
!!$                   !---------------end----------------------------------!
!!$                   xyzn=xyz(:)+(dtt*(vp(:)+2*vpn(:)+2*vpnn(:)+vpnnn(:)))/6.d0
!!$                   !----------------------------------------------------!
!!$                   bul(l)%som(m)%xyz(:)=xyzn(:)
!!$                end if
!!$             end do
!!$          end if
!!$       end do
!!$    end do
!!$    do l=1,ft%nbbul
!!$       if (bul(l)%mask ) then
!!$          do m=1,bul(l)%nbsom
!!$             if (bul(l)%som(m)%mask) then
!!$                bul(l)%som(m)%compteur=bul(l)%som(m)%compteur-1
!!$
!!$                xyz(:)=bul(l)%som(m)%xyz(:)
!!$                bul(l)%som(m)%xyz_old(:)=bul(l)%som(m)%xyz(:)
!!$                !----------------1-----------------------------------!
!!$                vp(:)=vitesse(xyz(1),xyz(2),(nt-0)*dt) 
!!$
!!$                !----------------2-----------------------------------!
!!$                xyzn(:)=xyz(:)+0.5d0*dt*vp(:)
!!$                vpn(:)=vitesse(xyzn(1),xyzn(2),(nt+1/2.d0)*dt)
!!$
!!$                !----------------3-----------------------------------!
!!$                xyznn(:)=xyz(:)+0.5d0*dt*vpn(:)
!!$                vpnn(:)=vitesse(xyznn(1),xyznn(2),(nt+1/2.d0)*dt) 
!!$
!!$                !----------------4-----------------------------------!
!!$                xyznnn(:)=xyz(:)+dt*vpnn(:)
!!$                vpnnn(:)=vitesse(xyznnn(1),xyznnn(2),(nt+1)*dt)  
!!$
!!$                !---------------end----------------------------------!
!!$                xyzn=xyz(:)+(dt*(vp(:)+2*vpn(:)+2*vpnn(:)+vpnnn(:)))/6.d0
!!$                !----------------------------------------------------!
!!$                bul(l)%som(m)%xyz(:)=xyzn(:)
!!$             end if
!!$          end do
!!$       end if
!!$    end do
  end subroutine reference_RK4
  !*******************************************************************************
  subroutine exact_velocity(xyz,ve,nt,gradu_exact)
    !*******************************************************************************
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables 
    !-------------------------------------------------------------------------------
    real(8), dimension(dim), intent(in)  :: xyz
    integer, intent(in)                  :: nt
    real(8), dimension(dim), intent(out) :: ve
    real(8), dimension(dim), intent(out) :: gradu_exact
    !-------------------------------------------------------------------------------
    ve(:)=vitesse(xyz(1),xyz(2),nt*dt) 
    gradu_exact(1)=-pi*sin(2*pi*xyz(1))*sin(2*pi*xyz(2))
    gradu_exact(2)=-2*pi*(sin(pi*xyz(1)))**2*cos(2*pi*xyz(2))
    !-------------------------------------------------------------------------------
  end subroutine exact_velocity
  !*******************************************************************************
  !*******************************************************************************
  subroutine distance_2d(bul,ft,distance_signe)
    !*******************************************************************************
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables 
    !-------------------------------------------------------------------------------
    type(struct_front_tracking), intent(in)             :: ft
    type(bulle),dimension(:), allocatable,intent(in)    :: bul
    real(8),dimension(sx-3:ex+3,sy-3:ey+3,1),intent(out):: distance_signe
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    integer::i,j,i0,j0,n,k,r0,l
    integer,dimension(dim)::ijk0,ijk1
    real(8),dimension(dim)::xyz1,xyz2,M,n_ele1,n_ele2,n_ele,xyzI
    real(8)::dist
    logical,dimension(:,:),allocatable::logic
    real(8), parameter:: ds=(xmax-xmin)/16.d0
    integer, parameter:: n_multi=nx/16
    real(8), parameter:: limite_bande=3*sqrt(2._8)*dx
    integer, parameter:: bande=2*(floor(limite_bande/dx)+1)!*n_multi
    !-------------------------------------------------------------------------------
    distance_signe=sqrt((xmax-xmin)*(xmax-xmin) +  (ymax-ymin)*(ymax-ymin))
    print*,limite_bande,bande
    do l=1,ft%nbbul
       if (bul(l)%mask) then
          do n=1,bul(l)%nbele
             if (bul(l)%ele(n)%mask) then
                !----------construire bissectrice-------------------------------------------
                k=bul(l)%ele(n)%ele_vois(1)
                xyz1=bul(l)%som(bul(l)%ele(k)%nusom(1))%xyz
                xyz2=bul(l)%som(bul(l)%ele(k)%nusom(2))%xyz
                n_ele1(1)=+(xyz2(2)-xyz1(2))
                n_ele1(2)=-(xyz2(1)-xyz1(1))
                n_ele1=n_ele1/sqrt(dot_product(n_ele1,n_ele1))

                k=bul(l)%ele(n)%ele_vois(2)
                xyz1=bul(l)%som(bul(l)%ele(k)%nusom(1))%xyz
                xyz2=bul(l)%som(bul(l)%ele(k)%nusom(2))%xyz
                n_ele2(1)=+(xyz2(2)-xyz1(2))
                n_ele2(2)=-(xyz2(1)-xyz1(1))
                n_ele2=n_ele2/sqrt(dot_product(n_ele2,n_ele2))

                xyz1=bul(l)%som(bul(l)%ele(n)%nusom(1))%xyz
                xyz2=bul(l)%som(bul(l)%ele(n)%nusom(2))%xyz
                n_ele(1)=+(xyz2(2)-xyz1(2))
                n_ele(2)=-(xyz2(1)-xyz1(1))
                n_ele=n_ele/sqrt(dot_product(n_ele,n_ele))

                n_ele1=n_ele+n_ele1
                n_ele2=n_ele+n_ele2
                !----------FIN construire bissectrice-------------------------------------------

                ijk1(1)=int(((xyz1(1)+xyz2(1))/2-xmin)/dx)
                ijk1(2)=int(((xyz1(2)+xyz2(2))/2-ymin)/dy)
                !write(55,*) ijk1

                bc1:do i=-bande,bande !Permet de déterminer 1 premier points dont la distance sera calculée
                   do j=-bande,bande

                      M(1)=xmin+(ijk1(1)+i)*dx
                      M(2)=ymin+(ijk1(2)+j)*dy


                      dist=distance_algebrique(xyz1,xyz2,M,xyzI)
                      if (entre_les_bissectrices(xyz1,xyz2,n_ele1,n_ele2,M).and.abs(dist)<limite_bande) then
                         if (abs(dist)< abs(distance_signe(ijk1(1)+i,ijk1(2)+j,1))) distance_signe(ijk1(1)+i,ijk1(2)+j,1)=dist
                         exit bc1
                      end if

                   end do
                end do bc1

                if (i/=bande+1.or.j/=bande+1) then
                   ijk0(1)=ijk1(1)+i  !!!Premier point dans le domaine
                   ijk0(2)=ijk1(2)+j

                   i0=bande-i ; j0=bande-j !!!!LIMITE MAILLAGE
                   call distance_signee_sur_un_element(&
                        0,i0,1   ,0,j0,1,&
                        ijk0,distance_signe,&
                        xyz1,xyz2,n_ele1,n_ele2,limite_bande)

                   i0=bande-i ; j0=bande+j
                   call distance_signee_sur_un_element(&
                        0,i0,1   ,-1,-j0,-1,&
                        ijk0,distance_signe,&
                        xyz1,xyz2,n_ele1,n_ele2,limite_bande)

                   i0=bande+i ; j0=bande-j
                   call distance_signee_sur_un_element(&
                        -1,-i0,-1   ,0,j0,1,&
                        ijk0,distance_signe,&
                        xyz1,xyz2,n_ele1,n_ele2,limite_bande)

                   i0=bande+i ; j0=bande+j
                   call distance_signee_sur_un_element(&
                        -1,-i0,-1   ,-1,-j0,-1,&
                        ijk0,distance_signe,&
                        xyz1,xyz2,n_ele1,n_ele2,limite_bande)
                end if
             end if
          end do
       end if
    end do


  end subroutine distance_2d
  !********************************************************************************
  !********************************************************************************
  subroutine distance_signee_sur_un_element(&
       i0,i1,ipas,j0,j1,jpas,&
       ijk1,distance_signe,&
       xyz1,xyz2,n_ele1,n_ele2,limite_bande)
    implicit none
    integer,intent(in)::i0,i1,ipas,j0,j1,jpas
    integer,dimension(:),intent(in)::ijk1
    real(8),dimension(sx-3:ex+3,sy-3:ey+3,1),intent(inout):: distance_signe
    real(8),dimension(:),intent(in)::xyz1,xyz2,n_ele1,n_ele2
    real(8),intent(in)::limite_bande
    real(8), parameter:: ds=(xmax-xmin)/16.d0
    integer, parameter:: n_multi=nx/16
    !---------------------------------------------------------------------------
    integer::i,j
    integer,dimension(dim)::ijk0
    real(8),dimension(dim)::M,xyzI
    logical::in_bissectrice_old,in_bissectrice_new,in_distance_old,in_distance_new
    real(8)::dist

    ijk0=ijk1
    ! write(60,*) ijk0
    in_bissectrice_new=.true.
    in_distance_new=.true.
    do i=i0,i1,ipas
       do j=j0,j1,jpas
          ijk0(1)=ijk1(1)+i
          ijk0(2)=ijk1(2)+j
          !write(61,*) ijk0 
          in_bissectrice_old=in_bissectrice_new
          in_distance_old=in_distance_new

          M(1)=xmin+(ijk0(1))*dx
          M(2)=ymin+(ijk0(2))*dy


          if (entre_les_bissectrices(xyz1,xyz2,n_ele1,n_ele2,M)) then
             in_bissectrice_new=.true.
          else
             in_bissectrice_new=.false.
             if (in_bissectrice_old) exit
             cycle
          end if
          !
          dist=distance_algebrique(xyz1,xyz2,M,xyzI)
          !if ( abs(dist)<=3*sqrt(2._8)*dx ) then
          if ( abs(dist)<=limite_bande ) then
             in_distance_new=.true.
             if (abs(dist)< abs(distance_signe(ijk0(1),ijk0(2),1))) distance_signe(ijk0(1),ijk0(2),1)=dist
          else
             in_distance_new=.false.
             if (in_distance_old) exit
             cycle 
          end if
          ! 
       end do
       in_bissectrice_new=.false.
       in_distance_new=.false.
    end do
    !-----------------------------------------------------------------------------
  end subroutine distance_signee_sur_un_element
  !********************************************************************************

  !********************************************************************************
  subroutine couleur_2d(ft,bul,cou,nb_subdivision,nt)
    !-----------------------------------------------------------------------------
    implicit none
    !-----------------------------------------------------------------------------
    type(struct_front_tracking), intent(in)               :: ft
    type(bulle),dimension(:), intent(inout)               :: bul
    real(8),dimension(sx-3:ex+3,sy-3:ey+3,1),intent(inout):: cou
    integer,intent(out):: nb_subdivision
    integer,intent(in):: nt
    !-----------------------------------------------------------------------------
    real(8)::epsilon_cou
    real(8)::volume_tot,erreur_cou
    !-----------------------------------------------------------------------------
    epsilon_cou=1.d-4
    nb_subdivision=1
    call raycasting_couleur(ft,bul,cou,nb_subdivision)
    call volume_2d(bul,ft,nt)
    call erreur_cou_2d(bul,ft,cou,erreur_cou)
    do while (erreur_cou>epsilon_cou) 
       nb_subdivision=nb_subdivision*2
       call raycasting_couleur(ft,bul,cou,nb_subdivision)
       call erreur_cou_2d(bul,ft,cou,erreur_cou)
    end do
    print*,'nb_subdivision=',nb_subdivision
  end subroutine couleur_2d

  !********************************************************************************
  subroutine raycasting_couleur(ft,bul,cou,nb_subdivision)
    !-----------------------------------------------------------------------------
    implicit none
    !-----------------------------------------------------------------------------
    type(struct_front_tracking), intent(in)             :: ft
    type(bulle),dimension(:), intent(in)                :: bul
    real(8),dimension(sx-3:ex+3,sy-3:ey+3,1),intent(out):: cou
    integer,intent(in):: nb_subdivision
    !-----------------------------------------------------------------------------
    integer:: i,j,i0,j0,i1,j1,ipas,jpas,m,n,k,nb_intersection,nb_intersection_old,snb_intersection_old
    integer:: snb_intersection
    !integer,parameter:: nb_subdivision!=10
    integer::isubd,jsubd,ii,jj,l
    real(8):: dx1,dy1,xmin1,ymin1
    real(8),dimension(dim):: xyz1,xyz2,A,B,C,D
    integer,dimension(sx:ex,sy:ey,1):: decompte
    integer,dimension(:,:,:),allocatable::sdecompte
    type(raycasting),dimension(sx:ex,sy:ey,1)::maille


    do m=1,ft%nbbul
       if (bul(m)%mask) then
          !-------------1er passage decompte-----------------------------------------
          decompte=0
          do n=1,bul(m)%nbele
             if (bul(m)%ele(n)%mask) then
                xyz1=bul(m)%som(bul(m)%ele(n)%nusom(1))%xyz
                xyz2=bul(m)%som(bul(m)%ele(n)%nusom(2))%xyz

                call decompte_element(xyz1,xyz2,decompte,dx,dy,xmin,ymin,sx,ex,sy,ey)

             end if
          end do
          !-------------2nd passage affectation--------------------------------------
          do j=sy,ey
             do i=sx,ex
                if (allocated(maille(i,j,1)%element)) deallocate(maille(i,j,1)%element)
                if (decompte(i,j,1)/=0) then
                   allocate(maille(i,j,1)%element(decompte(i,j,1)))
                end if
             end do
          end do

          decompte=0
          do n=1,bul(m)%nbele
             if (bul(m)%ele(n)%mask) then
                xyz1=bul(m)%som(bul(m)%ele(n)%nusom(1))%xyz
                xyz2=bul(m)%som(bul(m)%ele(n)%nusom(2))%xyz
                call affectation_element(xyz1,xyz2,decompte,n,dx,dy,xmin,ymin,sx,ex,sy,ey,maille)
             end if
          end do


          !************************************************************************************************
          do j=sy,ey
             i=sx
             nb_intersection_old=0
             nb_intersection=0

             do k=1,decompte(i,j,1)
                xyz1=bul(m)%som(bul(m)%ele( maille(i,j,1)%element(k) )%nusom(1))%xyz
                xyz2=bul(m)%som(bul(m)%ele( maille(i,j,1)%element(k) )%nusom(2))%xyz
                A=[(i-1/2.d0)*dx+xmin,j    *dy+ymin]
                B=[ i        *dx+xmin,j    *dy+ymin]

                if ( intersection(xyz2,xyz1,A,B) )  then
                   nb_intersection=nb_intersection+1
                end if

             end do

             if (mod(nb_intersection,2)==0) then
                cou(i,j,1)=0
             else
                cou(i,j,1)=1
             end if

             nb_intersection_old=nb_intersection


             if (decompte(i,j,1)/=0) then
                allocate(sdecompte(1:nb_subdivision,1:nb_subdivision,1))
                sdecompte=0
                dx1=dx/nb_subdivision
                dy1=dy/nb_subdivision
                xmin1=(i-1._8/2)*dx+xmin
                ymin1=(j-1._8/2)*dy+ymin

                do k=1,decompte(i,j,1)
                   xyz1=bul(m)%som(bul(m)%ele( maille(i,j,1)%element(k) )%nusom(1))%xyz
                   xyz2=bul(m)%som(bul(m)%ele( maille(i,j,1)%element(k) )%nusom(2))%xyz

                   !--------------------nouveau decompte dans les petites mailles----------------------

                   call decompte_sous_element(xyz1,xyz2,sdecompte,dx1,dy1,xmin1,ymin1,1,nb_subdivision,1,nb_subdivision)

                end do


                allocate(maille(i,j,1)%smaille(1:nb_subdivision,1:nb_subdivision,1))
                do jsubd=1,nb_subdivision
                   do isubd=1,nb_subdivision
                      if (allocated(maille(i,j,1)%smaille(isubd,jsubd,1)%selement)) then
                         deallocate(maille(i,j,1)%smaille(isubd,jsubd,1)%selement)
                      end if
                      if (sdecompte(isubd,jsubd,1)/=0) then
                         allocate(maille(i,j,1)%smaille(isubd,jsubd,1)%selement(sdecompte(isubd,jsubd,1)))
                      end if
                   end do
                end do

                sdecompte=0
                do k=1,decompte(i,j,1)
                   xyz1=bul(m)%som(bul(m)%ele( maille(i,j,1)%element(k) )%nusom(1))%xyz
                   xyz2=bul(m)%som(bul(m)%ele( maille(i,j,1)%element(k) )%nusom(2))%xyz
                   !----------------affectation des elements dans les mailles subdivises-----------------

                   i0=min(max(floor((xyz1(1)-xmin1)/dx1)+1,1),nb_subdivision)
                   j0=min(max(floor((xyz1(2)-ymin1)/dy1)+1,1),nb_subdivision)

                   i1=min(max(floor((xyz2(1)-xmin1)/dx1)+1,1),nb_subdivision)
                   j1=min(max(floor((xyz2(2)-ymin1)/dy1)+1,1),nb_subdivision)


                   ipas=sign(1,i1-i0)
                   jpas=sign(1,j1-j0)

                   if (i0/=i1.and.j0/=j1) then
                      do ii=i0,i1,ipas
                         do jj=j0,j1,jpas
                            A=[(ii-1)*dx1+xmin1,(jj-1)*dy1+ymin1]
                            B=[(ii+0)*dx1+xmin1,(jj-1)*dy1+ymin1]
                            C=[(ii+0)*dx1+xmin1,(jj+0)*dy1+ymin1]
                            D=[(ii-1)*dx1+xmin1,(jj+0)*dy1+ymin1]

                            if ( intersection(xyz2,xyz1,A,B) .or. intersection(xyz2,xyz1,B,C) .or. &
                                 intersection(xyz2,xyz1,C,D) .or. intersection(xyz2,xyz1,D,A) ) then
                               sdecompte(ii,jj,1)=sdecompte(ii,jj,1)+1
                               maille(i,j,1)%smaille(ii,jj,1)%selement(sdecompte(ii,jj,1))=maille(i,j,1)%element(k)
                            end if
                         end do
                      end do
                   else
                      do ii=i0,i1,ipas
                         do jj=j0,j1,jpas
                            sdecompte(ii,jj,1)=sdecompte(ii,jj,1)+1
                            maille(i,j,1)%smaille(ii,jj,1)%selement(sdecompte(ii,jj,1))=maille(i,j,1)%element(k)
                         end do
                      end do
                   end if
                   ! call affectation_sous_element(xyz1,xyz2,decompte,n,dx,dy,xmin,ymin,0,nb_subdivision,&
                   !     0,nb_subdivision,maille(i,j,1))
                end do


                !---------------ray casting dans les petites mailles------------------------------------
                cou(i,j,1)=0
                do jsubd=1,nb_subdivision

                   isubd=1
                   maille(i,j,1)%smaille(isubd,jsubd,1)%nb_intersection=0
                   do l=1,sdecompte(isubd,jsubd,1)
                      xyz1=bul(m)%som(bul(m)%ele( maille(i,j,1)%smaille(isubd,jsubd,1)%selement(l) )%nusom(1))%xyz
                      xyz2=bul(m)%som(bul(m)%ele( maille(i,j,1)%smaille(isubd,jsubd,1)%selement(l) )%nusom(2))%xyz
                      A=[(isubd-1/2.d0)*dx1+xmin1,(jsubd-1/2.d0)*dy1+ymin1]
                      B=[(isubd-1     )*dx1+xmin1,(jsubd-1/2.d0)*dy1+ymin1]

                      if ( intersection(xyz2,xyz1,A,B) ) then
                         maille(i,j,1)%smaille(isubd,jsubd,1)%nb_intersection=&
                              maille(i,j,1)%smaille(isubd,jsubd,1)%nb_intersection+1
                      end if
                   end do
                   if (mod(maille(i,j,1)%smaille(isubd,jsubd,1)%nb_intersection,2)==1) then
                      cou(i,j,1)=cou(i,j,1)+dx1*dy1
                   end if


                   do isubd=2,nb_subdivision
                      maille(i,j,1)%smaille(isubd,jsubd,1)%nb_intersection=maille(i,j,1)%smaille(isubd-1,jsubd,1)%nb_intersection

                      do l=1,sdecompte(isubd,jsubd,1)
                         xyz1=bul(m)%som(bul(m)%ele( maille(i,j,1)%smaille(isubd,jsubd,1)%selement(l) )%nusom(1))%xyz
                         xyz2=bul(m)%som(bul(m)%ele( maille(i,j,1)%smaille(isubd,jsubd,1)%selement(l) )%nusom(2))%xyz
                         A=[(isubd-1/2.d0)*dx1+xmin1,(jsubd-1/2.d0)*dy1+ymin1]
                         B=[(isubd-1     )*dx1+xmin1,(jsubd-1/2.d0)*dy1+ymin1]
                         if ( intersection(xyz2,xyz1,A,B) ) then
                            maille(i,j,1)%smaille(isubd,jsubd,1)%nb_intersection=&
                                 maille(i,j,1)%smaille(isubd,jsubd,1)%nb_intersection+1
                         end if
                      end do

                      do l=1,sdecompte(isubd-1,jsubd,1)
                         xyz1=bul(m)%som(bul(m)%ele( maille(i,j,1)%smaille(isubd-1,jsubd,1)%selement(l) )%nusom(1))%xyz
                         xyz2=bul(m)%som(bul(m)%ele( maille(i,j,1)%smaille(isubd-1,jsubd,1)%selement(l) )%nusom(2))%xyz
                         A=[(isubd-3/2.d0)*dx1+xmin1,(jsubd-1/2.d0)*dy1+ymin1]
                         B=[(isubd-1     )*dx1+xmin1,(jsubd-1/2.d0)*dy1+ymin1]
                         if ( intersection(xyz2,xyz1,A,B) )  then
                            maille(i,j,1)%smaille(isubd,jsubd,1)%nb_intersection=&
                                 maille(i,j,1)%smaille(isubd,jsubd,1)%nb_intersection+1
                         end if
                      end do

                      if (mod(maille(i,j,1)%smaille(isubd,jsubd,1)%nb_intersection,2)==1) then
                         cou(i,j,1)=cou(i,j,1)+dx1*dy1
                      end if
                   end do

                end do
                cou(i,j,1)=cou(i,j,1)/(dx*dy)
                deallocate(sdecompte)
             end if

             !new2
             !************************************************************************************************

             do i=sx+1,ex

                do k=1,decompte(i,j,1)
                   xyz1=bul(m)%som(bul(m)%ele( maille(i,j,1)%element(k) )%nusom(1))%xyz
                   xyz2=bul(m)%som(bul(m)%ele( maille(i,j,1)%element(k) )%nusom(2))%xyz
                   A=[(i-1/2.d0)*dx+xmin,j    *dy+ymin]
                   B=[ i        *dx+xmin,j    *dy+ymin]

                   if ( intersection(xyz2,xyz1,A,B) )   then
                      nb_intersection=nb_intersection+1
                   end if
                end do

                do k=1,decompte(i-1,j,1)
                   xyz1=bul(m)%som(bul(m)%ele( maille(i-1,j,1)%element(k) )%nusom(1))%xyz
                   xyz2=bul(m)%som(bul(m)%ele( maille(i-1,j,1)%element(k) )%nusom(2))%xyz
                   A=[(i-1/2.d0)*dx+xmin,j    *dy+ymin]
                   B=[(i-1     )*dx+xmin,j    *dy+ymin]

                   if ( intersection(xyz2,xyz1,A,B) )  nb_intersection=nb_intersection+1
                end do

                if (mod(nb_intersection,2)==0) then
                   cou(i,j,1)=0
                else
                   cou(i,j,1)=1
                end if
                if (decompte(i,j,1)/=0) then
                   allocate(sdecompte(1:nb_subdivision,1:nb_subdivision,1))
                   sdecompte=0
                   dx1=dx/nb_subdivision
                   dy1=dy/nb_subdivision
                   xmin1=(i-1._8/2)*dx+xmin
                   ymin1=(j-1._8/2)*dy+ymin

                   do k=1,decompte(i,j,1)
                      xyz1=bul(m)%som(bul(m)%ele( maille(i,j,1)%element(k) )%nusom(1))%xyz
                      xyz2=bul(m)%som(bul(m)%ele( maille(i,j,1)%element(k) )%nusom(2))%xyz

                      !--------------------nouveau decompte dans les petites mailles----------------------
                      call decompte_sous_element(xyz1,xyz2,sdecompte,dx1,dy1,xmin1,ymin1,1,nb_subdivision,1,nb_subdivision)
                   end do

                   allocate(maille(i,j,1)%smaille(1:nb_subdivision,1:nb_subdivision,1))
                   do jsubd=1,nb_subdivision
                      do isubd=1,nb_subdivision
                         if (allocated(maille(i,j,1)%smaille(isubd,jsubd,1)%selement)) then
                            deallocate(maille(i,j,1)%smaille(isubd,jsubd,1)%selement)
                         end if
                         if (sdecompte(isubd,jsubd,1)/=0) then
                            allocate(maille(i,j,1)%smaille(isubd,jsubd,1)%selement(sdecompte(isubd,jsubd,1)))
                         end if
                      end do
                   end do


                   sdecompte=0
                   do k=1,decompte(i,j,1)
                      xyz1=bul(m)%som(bul(m)%ele( maille(i,j,1)%element(k) )%nusom(1))%xyz
                      xyz2=bul(m)%som(bul(m)%ele( maille(i,j,1)%element(k) )%nusom(2))%xyz
                      !----------------affectation des elements dans les mailles subdivises-----------------

                      i0=min(max(floor((xyz1(1)-xmin1)/dx1)+1,1),nb_subdivision)
                      j0=min(max(floor((xyz1(2)-ymin1)/dy1)+1,1),nb_subdivision)

                      i1=min(max(floor((xyz2(1)-xmin1)/dx1)+1,1),nb_subdivision)
                      j1=min(max(floor((xyz2(2)-ymin1)/dy1)+1,1),nb_subdivision)

                      ipas=sign(1,i1-i0)
                      jpas=sign(1,j1-j0)

                      if (i0/=i1.and.j0/=j1) then
                         do ii=i0,i1,ipas
                            do jj=j0,j1,jpas
                               A=[(ii-1)*dx1+xmin1,(jj-1)*dy1+ymin1]
                               B=[(ii+0)*dx1+xmin1,(jj-1)*dy1+ymin1]
                               C=[(ii+0)*dx1+xmin1,(jj+0)*dy1+ymin1]
                               D=[(ii-1)*dx1+xmin1,(jj+0)*dy1+ymin1]

                               if ( intersection(xyz2,xyz1,A,B) .or. intersection(xyz2,xyz1,B,C) .or. &
                                    intersection(xyz2,xyz1,C,D) .or. intersection(xyz2,xyz1,D,A) ) then
                                  sdecompte(ii,jj,1)=sdecompte(ii,jj,1)+1
                                  maille(i,j,1)%smaille(ii,jj,1)%selement(sdecompte(ii,jj,1))=maille(i,j,1)%element(k)
                               end if
                            end do
                         end do
                      else
                         do ii=i0,i1,ipas
                            do jj=j0,j1,jpas
                               sdecompte(ii,jj,1)=sdecompte(ii,jj,1)+1
                               maille(i,j,1)%smaille(ii,jj,1)%selement(sdecompte(ii,jj,1))=maille(i,j,1)%element(k)
                            end do
                         end do
                      end if
                      ! call affectation_sous_element(xyz1,xyz2,decompte,n,dx,dy,xmin,ymin,0,nb_subdivision,&
                      !     0,nb_subdivision,maille(i,j,1))
                   end do

                   !---------------ray casting dans les petites mailles------------------------------------
                   cou(i,j,1)=0
                   do jsubd=1,nb_subdivision
                      isubd=1

                      maille(i,j,1)%smaille(isubd,jsubd,1)%nb_intersection=0
                      do l=1,sdecompte(isubd,jsubd,1)
                         xyz1=bul(m)%som(bul(m)%ele( maille(i,j,1)%smaille(isubd,jsubd,1)%selement(l) )%nusom(1))%xyz
                         xyz2=bul(m)%som(bul(m)%ele( maille(i,j,1)%smaille(isubd,jsubd,1)%selement(l) )%nusom(2))%xyz
                         A=[(isubd-1/2.d0)*dx1+xmin1,(jsubd-1/2.d0)*dy1+ymin1]
                         B=[(isubd-1     )*dx1+xmin1,(jsubd-1/2.d0)*dy1+ymin1]

                         if ( intersection(xyz2,xyz1,A,B) ) then
                            maille(i,j,1)%smaille(isubd,jsubd,1)%nb_intersection=&
                                 maille(i,j,1)%smaille(isubd,jsubd,1)%nb_intersection+1
                         end if
                      end do

                      if (decompte(i-1,j,1)/=0) then
                         do l=1,size(maille(i-1,j,1)%smaille(nb_subdivision,jsubd,1)%selement)
                            xyz1=bul(m)%som(bul(m)%ele( maille(i-1,j,1)%smaille(nb_subdivision,jsubd,1)%selement(l) )%nusom(1))%xyz
                            xyz2=bul(m)%som(bul(m)%ele( maille(i-1,j,1)%smaille(nb_subdivision,jsubd,1)%selement(l) )%nusom(2))%xyz
                            A=[(isubd-3/2.d0)*dx1+xmin1,(jsubd-1/2.d0)*dy1+ymin1]
                            B=[(isubd-1     )*dx1+xmin1,(jsubd-1/2.d0)*dy1+ymin1]

                            if ( intersection(xyz2,xyz1,A,B) ) then
                               maille(i,j,1)%smaille(isubd,jsubd,1)%nb_intersection=&
                                    maille(i,j,1)%smaille(isubd,jsubd,1)%nb_intersection+1
                            end if
                         end do

                         maille(i,j,1)%smaille(isubd,jsubd,1)%nb_intersection=maille(i,j,1)%smaille(isubd,jsubd,1)%nb_intersection+&
                              maille(i-1,j,1)%smaille(nb_subdivision,jsubd,1)%nb_intersection
                      else
                         maille(i,j,1)%smaille(isubd,jsubd,1)%nb_intersection=maille(i,j,1)%smaille(isubd,jsubd,1)%nb_intersection+&
                              nb_intersection_old
                      end if


                      if (mod(maille(i,j,1)%smaille(isubd,jsubd,1)%nb_intersection,2)==1) then
                         cou(i,j,1)=cou(i,j,1)+dx1*dy1
                      end if


                      do isubd=2,nb_subdivision
                         maille(i,j,1)%smaille(isubd,jsubd,1)%nb_intersection=maille(i,j,1)%smaille(isubd-1,jsubd,1)%nb_intersection

                         do l=1,sdecompte(isubd,jsubd,1)
                            xyz1=bul(m)%som(bul(m)%ele( maille(i,j,1)%smaille(isubd,jsubd,1)%selement(l) )%nusom(1))%xyz
                            xyz2=bul(m)%som(bul(m)%ele( maille(i,j,1)%smaille(isubd,jsubd,1)%selement(l) )%nusom(2))%xyz
                            A=[(isubd-1/2.d0)*dx1+xmin1,(jsubd-1/2.d0)*dy1+ymin1]
                            B=[(isubd-1     )*dx1+xmin1,(jsubd-1/2.d0)*dy1+ymin1]
                            if ( intersection(xyz2,xyz1,A,B) ) then
                               maille(i,j,1)%smaille(isubd,jsubd,1)%nb_intersection=&
                                    maille(i,j,1)%smaille(isubd,jsubd,1)%nb_intersection+1
                            end if
                         end do

                         do l=1,sdecompte(isubd-1,jsubd,1)
                            xyz1=bul(m)%som(bul(m)%ele( maille(i,j,1)%smaille(isubd-1,jsubd,1)%selement(l) )%nusom(1))%xyz
                            xyz2=bul(m)%som(bul(m)%ele( maille(i,j,1)%smaille(isubd-1,jsubd,1)%selement(l) )%nusom(2))%xyz
                            A=[(isubd-3/2.d0)*dx1+xmin1,(jsubd-1/2.d0)*dy1+ymin1]
                            B=[(isubd-1     )*dx1+xmin1,(jsubd-1/2.d0)*dy1+ymin1]
                            if ( intersection(xyz2,xyz1,A,B) )  then
                               maille(i,j,1)%smaille(isubd,jsubd,1)%nb_intersection=&
                                    maille(i,j,1)%smaille(isubd,jsubd,1)%nb_intersection+1
                            end if
                         end do

                         if (mod(maille(i,j,1)%smaille(isubd,jsubd,1)%nb_intersection,2)==1) then
                            cou(i,j,1)=cou(i,j,1)+dx1*dy1
                         end if
                      end do

                   end do
                   cou(i,j,1)=cou(i,j,1)/(dx*dy)
                   deallocate(sdecompte)

                   if (decompte(i-1,j,1)>0) deallocate(maille(i-1,j,1)%smaille)
                end if
                nb_intersection_old=nb_intersection

             end do
             if (decompte(ex,j,1)>0) deallocate(maille(ex,j,1)%smaille)
          end do

          do j=sy,ey
             do i=sx,ex
                if (allocated(maille(i,j,1)%smaille)) deallocate(maille(i,j,1)%smaille)
             end do
          end do
       end if
    end do
  end subroutine raycasting_couleur
  !******************************************************************************************
  !******************************************************************************************
  subroutine decompte_element(xyz1,xyz2,decompte,dx0,dy0,xmin0,ymin0,startx,endx,starty,endy)
    !-----------------------------------------------------------------------------
    implicit none
    !-----------------------------------------------------------------------------
    integer,intent(in):: startx,starty,endx,endy
    integer,dimension(startx:endx,starty:endy,1),intent(inout):: decompte
    real(8),dimension(:),intent(in)::xyz1,xyz2
    real(8),intent(in):: dx0,dy0,xmin0,ymin0
    !-----------------------------------------------------------------------------
    real(8),dimension(dim):: A,B,C,D
    integer:: ii,jj,i0,j0,i1,j1,ipas,jpas

    i0=min(max(nint((xyz1(1)-xmin0)/dx0),startx),endx) 
    j0=min(max(nint((xyz1(2)-ymin0)/dy0),starty),endy)

    i1=min(max(nint((xyz2(1)-xmin0)/dx0),startx),endx)
    j1=min(max(nint((xyz2(2)-ymin0)/dy0),starty),endy)

    ipas=sign(1,i1-i0)
    jpas=sign(1,j1-j0)

    if (i0/=i1.and.j0/=j1) then
       do jj=j0,j1,jpas
          do ii=i0,i1,ipas
             A=[(ii-1._8/2)*dx0+xmin0,(jj-1._8/2)*dy0+ymin0]
             B=[(ii+1._8/2)*dx0+xmin0,(jj-1._8/2)*dy0+ymin0]
             C=[(ii+1._8/2)*dx0+xmin0,(jj+1._8/2)*dy0+ymin0]
             D=[(ii-1._8/2)*dx0+xmin0,(jj+1._8/2)*dy0+ymin0]

             if ( intersection(xyz2,xyz1,A,B) .or. intersection(xyz2,xyz1,B,C) .or. &
                  intersection(xyz2,xyz1,C,D) .or. intersection(xyz2,xyz1,D,A) ) then
                decompte(ii,jj,1)=decompte(ii,jj,1)+1
             end if
          end do
       end do
    else
       decompte(i0:i1:ipas,j0:j1:jpas,1)=decompte(i0:i1:ipas,j0:j1:jpas,1)+1
    end if

  end subroutine decompte_element
  !********************************************************************************
  !********************************************************************************
  subroutine affectation_element(xyz1,xyz2,decompte,n,dx0,dy0,xmin0,ymin0,startx,endx,starty,endy,maille,l,affectation)
    !-----------------------------------------------------------------------------
    implicit none
    !-----------------------------------------------------------------------------
    integer,intent(in):: startx,starty,endx,endy
    integer,dimension(startx:endx,starty:endy,1),intent(inout):: decompte
    type(raycasting),dimension(startx:endx,starty:endy,1),intent(inout)::maille
    real(8),dimension(:),intent(in)::xyz1,xyz2
    integer,intent(in)::n
    real(8),intent(in):: dx0,dy0,xmin0,ymin0
    integer,optional::l
    logical,optional::affectation
    !-----------------------------------------------------------------------------
    real(8),dimension(dim):: A,B,C,D
    integer:: ii,jj,i0,j0,i1,j1,ipas,jpas
    !-----------------------------------------------------------------------------
    i0=min(max(nint((xyz1(1)-xmin0)/dx0),startx),endx)
    j0=min(max(nint((xyz1(2)-ymin0)/dy0),starty),endy)

    i1=min(max(nint((xyz2(1)-xmin0)/dx0),startx),endx)
    j1=min(max(nint((xyz2(2)-ymin0)/dy0),starty),endy)

    ipas=sign(1,i1-i0)
    jpas=sign(1,j1-j0)

    if (i0/=i1.and.j0/=j1) then
       do ii=i0,i1,ipas
          do jj=j0,j1,jpas
             A=[(ii-1._8/2)*dx0+xmin0,(jj-1._8/2)*dy0+ymin0]
             B=[(ii+1._8/2)*dx0+xmin0,(jj-1._8/2)*dy0+ymin0]
             C=[(ii+1._8/2)*dx0+xmin0,(jj+1._8/2)*dy0+ymin0]
             D=[(ii-1._8/2)*dx0+xmin0,(jj+1._8/2)*dy0+ymin0]

             if ( intersection(xyz2,xyz1,A,B) .or. intersection(xyz2,xyz1,B,C) .or. &
                  intersection(xyz2,xyz1,C,D) .or. intersection(xyz2,xyz1,D,A) ) then

                if ( present(l) .and. present(affectation) ) then

                   if (affectation) then
                      maille(ii,jj,1)%element(decompte(ii,jj,1))=n
                      maille(ii,jj,1)%bulbis(decompte(ii,jj,1))=l
                   else
                      decompte(ii,jj,1)=decompte(ii,jj,1)+1
                      maille(ii,jj,1)%element(decompte(ii,jj,1))=n 
                      maille(ii,jj,1)%bulbis(decompte(ii,jj,1))=l
                   end if

                else
                   decompte(ii,jj,1)=decompte(ii,jj,1)+1
                   maille(ii,jj,1)%element(decompte(ii,jj,1))=n 
                end if

             end if
          end do
       end do
    else
       do ii=i0,i1,ipas
          do jj=j0,j1,jpas

             if ( present(l) .and. present(affectation) ) then
                if (affectation) then
                   maille(ii,jj,1)%element(decompte(ii,jj,1))=n
                   maille(ii,jj,1)%bulbis(decompte(ii,jj,1))=l
                else
                   decompte(ii,jj,1)=decompte(ii,jj,1)+1
                   maille(ii,jj,1)%element(decompte(ii,jj,1))=n
                   maille(ii,jj,1)%bulbis(decompte(ii,jj,1))=l
                end if

             else
                decompte(ii,jj,1)=decompte(ii,jj,1)+1
                maille(ii,jj,1)%element(decompte(ii,jj,1))=n 
             end if

          end do
       end do
    end if

  end subroutine affectation_element
  !********************************************************************************
  subroutine decompte_sous_element(xyz1,xyz2,decompte,dx0,dy0,xmin0,ymin0,startx,endx,starty,endy)
    !-----------------------------------------------------------------------------
    implicit none
    !-----------------------------------------------------------------------------
    integer,intent(in):: startx,starty,endx,endy
    integer,dimension(startx:endx,starty:endy,1),intent(inout):: decompte
    real(8),dimension(:),intent(in)::xyz1,xyz2
    real(8),intent(in):: dx0,dy0,xmin0,ymin0
    !-----------------------------------------------------------------------------
    real(8),dimension(dim):: A,B,C,D
    integer:: ii,jj,i0,j0,i1,j1,ipas,jpas

    i0=min(max(floor((xyz1(1)-xmin0)/dx0)+1,startx),endx)
    j0=min(max(floor((xyz1(2)-ymin0)/dy0)+1,starty),endy)

    i1=min(max(floor((xyz2(1)-xmin0)/dx0)+1,startx),endx)
    j1=min(max(floor((xyz2(2)-ymin0)/dy0)+1,starty),endy)

    ipas=sign(1,i1-i0)
    jpas=sign(1,j1-j0)


    if (i0/=i1.and.j0/=j1) then
       do jj=j0,j1,jpas
          do ii=i0,i1,ipas
             A=[(ii-1)*dx0+xmin0,(jj-1)*dy0+ymin0]
             B=[(ii+0)*dx0+xmin0,(jj-1)*dy0+ymin0]
             C=[(ii+0)*dx0+xmin0,(jj+0)*dy0+ymin0]
             D=[(ii-1)*dx0+xmin0,(jj+0)*dy0+ymin0]

             if ( intersection(xyz2,xyz1,A,B) .or. intersection(xyz2,xyz1,B,C) .or. &
                  intersection(xyz2,xyz1,C,D) .or. intersection(xyz2,xyz1,D,A) ) then
                decompte(ii,jj,1)=decompte(ii,jj,1)+1
             end if
          end do
       end do
    else
       decompte(i0:i1:ipas,j0:j1:jpas,1)=decompte(i0:i1:ipas,j0:j1:jpas,1)+1
    end if

  end subroutine decompte_sous_element
  !********************************************************************************
  !********************************************************************************
!!$  !********************************************************************************
!!$  subroutine affectation_sous_element(xyz1,xyz2,decompte,n,dx,dy,xmin,ymin,startx,endx,starty,endy,maille)
!!$    !-----------------------------------------------------------------------------
!!$    implicit none
!!$    !-----------------------------------------------------------------------------
!!$    integer,intent(in):: startx,starty,endx,endy
!!$    integer,dimension(startx:endx,starty:endy,1),intent(inout):: decompte
!!$    type(raycasting),dimension(sx:ex,sy:ey,1),intent(inout)::maille
!!$    real(8),dimension(:),intent(in)::xyz1,xyz2
!!$    integer,intent(in)::n
!!$    real(8),intent(in):: dx,dy,xmin,ymin
!!$    !-----------------------------------------------------------------------------
!!$    real(8),dimension(dim):: A,B,C,D
!!$    integer:: ii,jj,i0,j0,i1,j1,ipas,jpas
!!$    !-----------------------------------------------------------------------------
!!$    
!!$    i0=nint((xyz1(1)-xmin)/dx)
!!$    j0=nint((xyz1(2)-ymin)/dy)
!!$
!!$    i1=nint((xyz2(1)-xmin)/dx)
!!$    j1=nint((xyz2(2)-ymin)/dy)
!!$
!!$    ipas=sign(1,i1-i0)
!!$    jpas=sign(1,j1-j0)
!!$
!!$    if (i0/=i1.and.j0/=j1) then
!!$       do ii=i0,i1,ipas
!!$          do jj=j0,j1,jpas
!!$             A=[(ii-1._8/2)*dx+xmin,(jj-1._8/2)*dy+ymin]
!!$             B=[(ii+1._8/2)*dx+xmin,(jj-1._8/2)*dy+ymin]
!!$             C=[(ii+1._8/2)*dx+xmin,(jj+1._8/2)*dy+ymin]
!!$             D=[(ii-1._8/2)*dx+xmin,(jj+1._8/2)*dy+ymin]
!!$
!!$             if ( intersection(xyz2,xyz1,A,B) .or. intersection(xyz2,xyz1,B,C) .or. &
!!$                  intersection(xyz2,xyz1,C,D) .or. intersection(xyz2,xyz1,D,A) ) then
!!$                decompte(ii,jj,1)=decompte(ii,jj,1)+1
!!$                maille%smaille(ii,jj,1)%selement(decompte(ii,jj,1))=n
!!$             end if
!!$          end do
!!$       end do
!!$    else
!!$       do ii=i0,i1,ipas
!!$          do jj=j0,j1,jpas
!!$             decompte(ii,jj,1)=decompte(ii,jj,1)+1
!!$             maille%smaille(ii,jj,1)%selement(decompte(ii,jj,1))=n
!!$          end do
!!$       end do
!!$    end if
!!$
!!$  end subroutine affectation_sous_element
!!$  !********************************************************************************
  !********************************************************************************
  subroutine erreur_cou_2d(bul,ft,cou,erreur_cou)
    !-----------------------------------------------------------------------------
    implicit none
    !-----------------------------------------------------------------------------
    type(struct_front_tracking), intent(in)             :: ft
    type(bulle),dimension(:), intent(in)                :: bul
    real(8),dimension(sx-3:ex+3,sy-3:ey+3,1),intent(in) :: cou
    real(8),intent(out)                                 :: erreur_cou
    !-----------------------------------------------------------------------------
    integer::i,j,m
    !real(8)::erreur_L1,erreur_L2,erreur_Linfty
    real(8)::cou_tot
    !-------------------------------------------------------------------------------
    do m=1,ft%nbbul
       if (bul(m)%mask) then
          cou_tot=0
          do j=sy,ey
             do i=sx,ex
                cou_tot=cou_tot+cou(i,j,1)
             end do
          end do
          cou_tot=cou_tot*dx*dy
          !erreur_L1=abs(1-(cou_tot/(pi*ft%r0**2)))
          erreur_cou=abs(1-cou_tot/ bul(m)%volume_2d)

          !write(*,'("Normes : L1= ",1pe14.6)') erreur_cou
       end if
    end do
  end subroutine erreur_cou_2d
  !********************************************************************************
  !********************************************************************************
  subroutine erreur_volume_2d(bul,ft,volume_cell,erreur_volume)
    !-----------------------------------------------------------------------------
    implicit none
    !-----------------------------------------------------------------------------
    type(struct_front_tracking), intent(in)             :: ft
    type(bulle),dimension(:), intent(in)                :: bul
    !real(8),intent(in)                                  :: volume_ini
    real(8),dimension(sx-3:ex+3,sy-3:ey+3,1),intent(in) :: volume_cell
    real(8),intent(out)                                 :: erreur_volume
    !-------------------------------------------------------------------------------
    integer :: m,i,j
    real(8) :: volume_tot
    !-------------------------------------------------------------------------------
!!$               do m=1,ft%nbbul
!!$                  if ( bul(m)%mask ) then
!!$                     erreur_volume=abs(1-bul(m)%volume_2d/volume_ini)
!!$                  end if
!!$               end do
    volume_tot=0
    do j=sy,ey
       do i=sx,ex
          volume_tot=volume_tot+volume_cell(i,j,1)
       end do
    end do
    !write(81,*) bul(1)%volume_2d,volume_tot
    erreur_volume=(bul(1)%volume_2d-volume_tot)/bul(1)%volume_2d
  end subroutine erreur_volume_2d
  !********************************************************************************
  !********************************************************************************
  function vitesse(x,y,t)
    implicit none
    !type(struct_front_tracking), intent(inout)::ft
    real(8),dimension(2)::vitesse
    real(8),intent(in)::x,y,t
    !--------------------------------------------------------------
    real(8)::xa,ya
    integer::a
    !--------------------------------------------------------------
    !-----------Champs de vitesse sinusoidal-----------------------
!!$    vitesse(1)=1-2*cos(x-t)*sin(y-t)
!!$    vitesse(2)=1+2*sin(x-t)*cos(y-t)

    !------------Champs de vitesse tournant------------------------
!!$    vitesse(1)=-pi/2*(y-1/2._8)
!!$    vitesse(2)= pi/2*(x-1/2._8)

!!$    !---------------champs de vitesse fortement cisaille (Serpentin aller-retour)
!!$    vitesse(1)=-2.d0*sin(pi*x)**2*cos(pi*y)*sin(pi*y)*cos((pi*t)/Tp)
!!$    vitesse(2)=2.d0*sin(pi*y)**2*cos(pi*x)*sin(pi*x)*cos((pi*t)/Tp)

!!$    !---------------champs de vitesse fortement cisaille (Serpentin aller)
!!$    vitesse(1)=-2.d0*(sin(pi*x))**2*cos(pi*y)*sin(pi*y)
!!$    vitesse(2)=2.d0*(sin(pi*y))**2*cos(pi*x)*sin(pi*x)

    !---------------Champs de vitesse deformant (ecoulement sur une plaque plane)
    xa=1.d0; ya=0.d0
    a=1
    vitesse(1)=2*a*(x-xa)
    vitesse(2)=-2*a*(y-ya)

  end function vitesse
  !===============================================================================
  !===============================================================================
  subroutine solution_analytique_breil(ft,nt,ellipse)
    !--------------------------------------------------------------
    implicit none
    !--------------------------------------------------------------
    type(struct_front_tracking),intent(inout):: ft
    integer,intent(in):: nt
    real(8), dimension(:,:),allocatable,intent(out)::ellipse
    !--------------------------------------------------------------
    real(8), dimension(:,:),allocatable::x_temp
    real(8), dimension(:),allocatable::courbure_ref,courbure1,courbure2,courbure_moy_ref,courb_temp,longueur_ele
    integer::n,i,j,npt,npt_old,npt_new,npt_newnew,n_f,npt_bis,m
    real(8)::theta,e,axe1,axe2,a,b,c,aa,bb,cc,dxy,courb
    real(8),dimension(dim)::xyz1,xyz2,xyz_M,n_ele1,n_ele,xyz_Mbis,xyzI
    real(8),dimension(dim)::t_ele1,t_ele2,f_result
    real(8)::Erreur_L1,Erreur_L2,Erreur_Linfty,Ref_L1,Ref_L2,Ref_Linfty,dist
    real(8)::Erreur_L1_bis,Erreur_L2_bis,Erreur_Linfty_bis,Ref_L1_bis,Ref_L2_bis,Ref_Linfty_bis
    !--------------------------------------------------------------
    Erreur_L1=0.d0 ; Erreur_L2=0.d0 ; Erreur_Linfty=0.d0
    Ref_L1=0.d0 ; Ref_L2=0.d0 ; Ref_Linfty=0.d0
    Erreur_L1_bis=0.d0 ; Erreur_L2_bis=0.d0 ; Erreur_Linfty_bis=0.d0
    Ref_L1_bis=0.d0 ; Ref_L2_bis=0.d0 ; Ref_Linfty_bis=0.d0
    ft%xa=1.d0; ft%ya=0.d0
    ft%a=1
    axe1=ft%r0*exp(2*ft%a*nt*dt)
    axe2=ft%r0*exp(-2*ft%a*nt*dt)

    npt=3600!36000
    n_f=npt/8
    allocate(ellipse(0:npt,dim))
    theta=2*pi/npt
    npt_bis = npt!*1000
    dxy=theta/npt_bis
    allocate(courb_temp(0:npt_bis),x_temp(0:npt_bis,dim))
    do n=1,npt
       ellipse(n,1)=ft%xa+ft%r0*exp(2*ft%a*nt*dt)*cos(theta*(n-1))
       ellipse(n,2)=(ft%yc*exp(-2*ft%a*nt*dt))+ft%r0*exp(-2*ft%a*nt*dt)*sin(theta*(n-1))
    end do

    allocate(courbure1(0:npt),courbure2(0:npt),courbure_ref(0:npt),courbure_moy_ref(0:npt),longueur_ele(0:npt))
    npt_old=npt
    bc1: do n=1,npt
       e=sqrt(1-(axe2/axe1)**2)
       courbure_ref(n)=axe2/(axe1**2*(1-e**2*cos(theta*(n-1))**2)**(3._8/2))
       do m=1,npt_bis
          x_temp(m,1)=ft%xa+ft%r0*exp(2*ft%a*nt*dt)*cos(theta*(n-1)+dxy*m)
          x_temp(m,2)=(ft%yc*exp(-2*ft%a*nt*dt))+ft%r0*exp(-2*ft%a*nt*dt)*sin(theta*(n-1)+dxy*m)
       end do
       courb=0.d0
       do m=1,npt_bis-1
          courb_temp(m)=axe2/(axe1**2*(1-e**2*cos(theta*(n-1)+dxy*m)**2)**(3._8/2))
          courb = courb + courb_temp(m)
       end do
       dist = sqrt((x_temp(npt_bis,1)-ellipse(n,1))**2+(x_temp(npt_bis,2)-ellipse(n,2))**2)
       courbure_moy_ref(n)=(dist/npt_bis)*((courbure_ref(n)+courb_temp(npt_bis))/2.d0+courb)
       courbure_moy_ref(n)=courbure_moy_ref(n)/dist


       !------------------------------calcul courbure-------------------------------------------------!
!!$       if (n==(1+0*n_f) .or. n==(1+1*n_f) .or. n==(1+2*n_f)) then 

       xyz1(1)=ellipse(npt_old,1)
       xyz1(2)=ellipse(npt_old,2)
       xyz2(1)=ellipse(n,1)
       xyz2(2)=ellipse(n,2)
       xyz_M=(xyz1+xyz2)/2.d0
       n_ele1(1)=+(xyz2(2)-xyz1(2))
       n_ele1(2)=-(xyz2(1)-xyz1(1))
       n_ele1=n_ele1/sqrt(dot_product(n_ele1,n_ele1))
       t_ele1 = xyz1-xyz2
       t_ele1=t_ele1/sqrt(dot_product(t_ele1,t_ele1))

       xyz1=xyz2
       if (n==npt-1 ) then
          npt_newnew=1
          npt_new=n+1
       else if (n==npt ) then
          npt_new=1
          npt_newnew=2
       else
          npt_new=n+1
          npt_newnew=n+2
       end if
       xyz2(1)=ellipse(npt_new,1)
       xyz2(2)=ellipse(npt_new,2)
       xyz_Mbis=(xyz1+xyz2)/2.d0
       n_ele(1)=+(xyz2(2)-xyz1(2))
       n_ele(2)=-(xyz2(1)-xyz1(1))
       n_ele=n_ele/sqrt(dot_product(n_ele,n_ele))
       longueur_ele(n) =sqrt((xyz1(1)-xyz2(1))*(xyz1(1)-xyz2(1)) + (xyz1(2)-xyz2(2))*(xyz1(2)-xyz2(2)))
       xyz1=xyz2
       xyz2(1)=ellipse(npt_newnew,1)
       xyz2(2)=ellipse(npt_newnew,2)
       t_ele2 = xyz2-xyz1
       t_ele2=t_ele2/sqrt(dot_product(t_ele2,t_ele2))
       f_result = (t_ele1 + t_ele2)/2.d0
       !-------------equation de droite--------------!
       !-----------on determine les coefficients-----!
       !_____________________courbure methode 1_______________________________!
       b=-n_ele(1) ; a=n_ele(2); c=xyz_Mbis(2)*n_ele(1)-xyz_Mbis(1)*n_ele(2)
       bb=-n_ele1(1) ; aa=n_ele1(2); cc=xyz_M(2)*n_ele1(1)-xyz_M(1)*n_ele1(2)
       call find_intersection_point_droites(a,b,c,aa,bb,cc,xyzI)
       courbure1(n) = 1.d0/sqrt( (xyzI(1)-xyz1(1))*(xyzI(1)-xyz1(1)) + (xyzI(2)-xyz1(2))*(xyzI(2)-xyz1(2)) )
       !_____________________courbure methode 2_______________________________!
       courbure2(n) = sqrt(f_result(1)*f_result(1) + f_result(2)*f_result(2))/longueur_ele(n)

!!$       Erreur_L1= Erreur_L1 + abs(courbure1(n)-courbure_ref(n))
!!$       Ref_L1= Ref_L1+courbure_ref(n)
!!$       Erreur_L2= Erreur_L2 + (courbure1(n)-courbure_ref(n))**2
!!$       Ref_L2= Ref_L2+courbure_ref(n)**2
!!$       Erreur_Linfty=max(Erreur_Linfty,abs(courbure1(n)-courbure_ref(n)))
!!$       Ref_Linfty=max(Ref_Linfty,courbure_ref(n))
!!$
!!$       Erreur_L1_bis= Erreur_L1_bis + abs(courbure2(n)-courbure_moy_ref(n))
!!$       Ref_L1_bis= Ref_L1_bis+courbure_moy_ref(n)
!!$       Erreur_L2_bis= Erreur_L2_bis + (courbure2(n)-courbure_moy_ref(n))**2
!!$       Ref_L2_bis= Ref_L2_bis+courbure_moy_ref(n)**2
!!$       Erreur_Linfty_bis=max(Erreur_Linfty_bis,abs(courbure2(n)-courbure_moy_ref(n)))
!!$       Ref_Linfty_bis=max(Ref_Linfty_bis,courbure_moy_ref(n))

       write(39,'(1pe12.5,1pe12.5,1pe16.9)') ellipse(n,1),ellipse(n,2),courbure_ref(n)

       !write(40,'(1pe12.5,1pe16.9,1pe16.9,1pe16.9,1pe16.9)') ellipse(n,1),courbure_ref(n),courbure_moy_ref(n),courbure1(n),&
       !     courbure2(n)

       !write(41,'(1pe16.9,1pe16.9)') courbure_ref(n),longueur_ele(n)

!!$       write(39,*) ellipse(1+0*n_f,1),ellipse(1+0*n_f,2)
!!$       write(39,*) ellipse(1+1*n_f,1),ellipse(1+1*n_f,2)
!!$       write(39,*) ellipse(1+2*n_f,1),ellipse(1+2*n_f,2)
!!$          write(*,'(1pe16.9,1pe16.9,1pe16.9)') abs(courbure_ref(n)-courbure1(n))/courbure_ref(n),&
!!$               abs(courbure_moy_ref(n)-courbure2(n))/courbure_moy_ref(n),courbure_ref(n)*longueur_ele(n)
!!$          write(*,'(1pe16.9,1pe16.9,1pe16.9)') abs(courbure_ref(n)-courbure1(n))/courbure_ref(n),&
!!$               abs(courbure_moy_ref(n)-courbure2(n))/courbure_moy_ref(n),courbure_ref(n)*longueur_ele(n)
!!$          write(*,'(1pe16.9,1pe16.9,1pe16.9)') abs(courbure_ref(n)-courbure1(n))/courbure_ref(n),&
!!$               abs(courbure_moy_ref(n)-courbure2(n))/courbure_moy_ref(n),courbure_ref(n)*longueur_ele(n)
!!$    write(*,'(1pe16.9,1pe16.9,1pe16.9)') abs(courbure_ref(1+3*n_f)-courbure1(1+3*n_f))/courbure_ref(1+3*n_f),&
!!$         abs(courbure_moy_ref(3*n_f)-courbure2(3*n_f))/courbure_moy_ref(3*n_f),courbure_ref(3*n_f)*longueur_ele(3*n_f)
!!$    write(*,'(1pe16.9,1pe16.9,1pe16.9)') abs(courbure_ref(1+4*n_f)-courbure1(1+4*n_f))/courbure_ref(1+4*n_f),&
!!$         abs(courbure_moy_ref(4*n_f)-courbure2(4*n_f))/courbure_moy_ref(4*n_f),courbure_ref(4*n_f)*longueur_ele(4*n_f)
       npt_old=n
!!$       else 
!!$          npt_old=n
!!$          cycle bc1
!!$       end if
    end do bc1
!!$    Erreur_L1= Erreur_L1/Ref_L1
!!$    Erreur_L2= sqrt(Erreur_L2/Ref_L2)
!!$    Erreur_Linfty= Erreur_Linfty/Ref_Linfty
!!$
!!$    Erreur_L1_bis= Erreur_L1_bis/Ref_L1_bis
!!$    Erreur_L2_bis= sqrt(Erreur_L2_bis/Ref_L2_bis)
!!$    Erreur_Linfty_bis= Erreur_Linfty_bis/Ref_Linfty_bis

!!$    write(41,'(I0,1pe16.9,1pe16.9,1pe16.9)') npt,Erreur_L1,Erreur_L2,Erreur_Linfty
!!$    write(41,'(I0,1pe16.9,1pe16.9,1pe16.9)') npt,Erreur_L1_bis,Erreur_L2_bis,Erreur_Linfty_bis

  end subroutine solution_analytique_breil
  !===============================================================================
  subroutine courbure_discontinue_trontin(ft,f)
    !--------------------------------------------------------------
    implicit none
    !--------------------------------------------------------------
    type(struct_front_tracking),intent(inout):: ft
    real(8), dimension(:),allocatable,intent(out)::f
    !--------------------------------------------------------------
    real(8), dimension(:),allocatable::courbure_ref,courbure1,courbure2,x,x_temp,y_temp,courbure_moy_ref,courb_temp
    integer::n,i,j,npt_old,npt_new,npt_newnew,npt1,npt2,npt3,m,npt1_bis,npt2_bis,npt3_bis,l,n0,count
    real(8)::theta,e,axe1,axe2,a,b,c,aa,bb,cc,longueur_ele,courb,dx_bis,dist,dx1,dx2,dx3
    real(8),dimension(dim)::xyz1,xyz2,xyz_M,n_ele1,n_ele,xyz_Mbis,xyzI
    real(8),dimension(dim)::t_ele1,t_ele2,f_result
    real(8)::Erreur_L1,Erreur_L2,Erreur_Linfty,Ref_L1,Ref_L2,Ref_Linfty
    real(8)::Erreur_L1_bis,Erreur_L2_bis,Erreur_Linfty_bis,Ref_L1_bis,Ref_L2_bis,Ref_Linfty_bis
    !--------------------------------------------------------------
    Erreur_L1=0.d0 ; Erreur_L2=0.d0 ; Erreur_Linfty=0.d0
    Ref_L1=0.d0 ; Ref_L2=0.d0 ; Ref_Linfty=0.d0
    Erreur_L1_bis=0.d0 ; Erreur_L2_bis=0.d0 ; Erreur_Linfty_bis=0.d0
    Ref_L1_bis=0.d0 ; Ref_L2_bis=0.d0 ; Ref_Linfty_bis=0.d0
    l=1

    npt1=53*l
    dx1=abs(-1/3._8-xmin)/npt1
    npt2=npt1+(20*l)
    dx2=(2/3._8)/(npt2-npt1)
    npt3=npt2+(23*l)
    dx3=(xmax-1/3._8)/(npt3-npt2)
    npt1_bis=npt1*1000
    npt2_bis=(npt2-npt1)*1000
    npt3_bis=(npt3-npt2)*1000
    n0= npt1 + (npt2-npt1)/2._8
    allocate(f(0:npt3),courbure_ref(0:npt3),courbure1(0:npt3),courbure2(0:npt3),x(0:npt3),courbure_moy_ref(0:npt3))
    allocate(courb_temp(0:npt1_bis),x_temp(0:npt1_bis),y_temp(0:npt1_bis))
    n=0
    count=0
    x(n)=n*dx1+xmin
    f(n) = -x(n)*sin((3*pi/2._8)*x(n)**2)
    courbure_ref(n) = (-9*cos((3*pi*x(n)**2)/2)*pi*x(n)+9*x(n)**3*sin((3*pi*x(n)**2)/2)*pi**2)/&
         (1+(-sin((3*pi*x(n)**2)/2)-3*pi*x(n)**2*cos((3*pi*x(n)**2)/2))**2)**(3/2._8)
    courbure_ref(n) = abs(courbure_ref(n))

    do n=1,npt1
       x(n)=n*dx1+xmin
       f(n) = -x(n)*sin((3*pi/2._8)*x(n)**2)
       courbure_ref(n) = (-9*cos((3*pi*x(n)**2)/2)*pi*x(n)+9*x(n)**3*sin(3*pi*x(n)**2/2)*pi**2)/&
            (1+(-sin((3*pi*x(n)**2)/2)-3*pi*x(n)**2*cos((3*pi*x(n)**2)/2))**2)**(3/2._8)
       courbure_ref(n) = abs(courbure_ref(n))
       !-----------------------mean curvature calculation---------------------------------------
       dx_bis=abs(x(n)-x(n-1))/npt1_bis
       do m=0,npt1_bis
          x_temp(m) = m*dx_bis+x(n-1)
          y_temp(m) = -x_temp(m)*sin((3*pi/2._8)*x_temp(m)**2)
       end do
       courb=0.d0
       do m=1,npt1_bis-1
          courb_temp(m)=(-9*cos((3*pi*x_temp(m)**2)/2)*pi*x_temp(m)+9*x_temp(m)**3*sin((3*pi*x_temp(m)**2)/2)*pi**2)/&
               (1+(-sin((3*pi*x_temp(m)**2)/2)-3*pi*x_temp(m)**2*cos((3*pi*x_temp(m)**2)/2))**2)**(3/2._8)
          courb = courb + courb_temp(m)
       end do
       dist = sqrt((x_temp(npt1_bis)-x(n-1))**2+(y_temp(npt1_bis)-f(n-1))**2)
       courbure_moy_ref(n-1)=(dist/npt1_bis)*((courbure_ref(n)+courbure_ref(n-1))/2.d0+courb)
       courbure_moy_ref(n-1)=courbure_moy_ref(n-1)/dist
       courbure_moy_ref(n-1) = abs(courbure_moy_ref(n-1))
    end do

    do n=npt1+1,npt2
       x(n)=(n-npt1)*dx2-1/3._8
       f(n) = (sqrt(3._8)/9._8)*abs(sin(2*pi*x(n)))
       if (x(n)>-1._8/3 .and. x(n)<=0) then
          courbure_ref(n) = ((-4*sqrt(3._8)/9._8)*pi**2*sin(2*pi*x(n)))/&
               (1+((2*sqrt(3._8)/9._8)*pi*cos(2*pi*x(n)))**2)**(3/2._8)
          courbure_ref(n) = abs(courbure_ref(n))
          !-----------------------mean curvature calculation---------------------------------------
          dx_bis=abs(x(n)-x(n-1))/npt2_bis
          do m=0,npt2_bis
             x_temp(m) = m*dx_bis+x(n-1)
             y_temp(m) = (sqrt(3._8)/9._8)*abs(sin(2*pi*x_temp(m)))
          end do
          courb=0.d0
          do m=1,npt2_bis-1
             courb_temp(m)= ((-4*sqrt(3._8)/9._8)*pi**2*sin(2*pi*x_temp(m)))/&
                  (1+((2*sqrt(3._8)/9._8)*pi*cos(2*pi*x_temp(m)))**2)**(3/2._8)
             courb = courb + courb_temp(m)
          end do
          dist = sqrt((x_temp(npt2_bis)-x(n-1))**2+(y_temp(npt2_bis)-f(n-1))**2)
          courbure_moy_ref(n-1)=(dist/npt2_bis)*((courbure_ref(n)+courbure_ref(n-1))/2.d0+courb)
          courbure_moy_ref(n-1)=courbure_moy_ref(n-1)/dist
          courbure_moy_ref(n-1) = abs(courbure_moy_ref(n-1))
       else
          courbure_ref(n) = ((4*sqrt(3._8)/9._8)*pi**2*sin(2*pi*x(n)))/&
               (1+((-2*sqrt(3._8)/9._8)*pi*cos(2*pi*x(n)))**2)**(3/2._8) 
          courbure_ref(n) = abs(courbure_ref(n))
          !-----------------------mean curvature calculation---------------------------------------
          dx_bis=abs(x(n)-x(n-1))/npt2_bis
          do m=0,npt2_bis
             x_temp(m) = m*dx_bis+x(n-1)
             y_temp(m) = (sqrt(3._8)/9._8)*abs(sin(2*pi*x_temp(m)))
          end do
          courb=0.d0
          do m=1,npt2_bis-1
             courb_temp(m)=((4*sqrt(3._8)/9._8)*pi**2*sin(2*pi*x_temp(m)))/&
                  (1+((-2*sqrt(3._8)/9._8)*pi*cos(2*pi*x_temp(m)))**2)**(3/2._8) 
             courb = courb + courb_temp(m)
          end do
          dist = sqrt((x_temp(npt2_bis)-x(n-1))**2+(y_temp(npt2_bis)-f(n-1))**2)
          courbure_moy_ref(n-1)=(dist/npt2_bis)*((courbure_ref(n)+courbure_ref(n-1))/2.d0+courb)
          courbure_moy_ref(n-1)=courbure_moy_ref(n-1)/dist
          courbure_moy_ref(n-1) = abs(courbure_moy_ref(n-1))
       end if
    end do

    do n=npt2+1,npt3
       x(n)=(n-npt2)*dx3+1/3._8
       f(n) = x(n)/2._8-(1._8/6._8)*sin(3*pi*x(n))
       courbure_ref(n) = ((3/2._8)*(sin(3*pi*x(n)))*pi**2)/(1+(1._8/2-(1._8/2)*cos(3*pi*x(n))*pi)**2)**(3/2._8)
       courbure_ref(n) = abs(courbure_ref(n))
       !-----------------------mean curvature calculation---------------------------------------
       dx_bis=abs(x(n)-x(n-1))/npt3_bis
       do m=0,npt3_bis
          x_temp(m) = m*dx_bis+x(n-1)
          y_temp(m) = x_temp(m)/2._8-(1._8/6._8)*sin(3*pi*x_temp(m))
       end do
       courb=0.d0
       do m=1,npt3_bis-1
          courb_temp(m)=((3/2._8)*(sin(3*pi*x_temp(m)))*pi**2)/(1+(1._8/2-(1._8/2)*cos(3*pi*x_temp(m))*pi)**2)**(3/2._8)
          courb = courb + courb_temp(m)
       end do
       dist = sqrt((x_temp(npt3_bis)-x(n-1))**2+(y_temp(npt3_bis)-f(n-1))**2)
       courbure_moy_ref(n-1)=(dist/npt3_bis)*((courbure_ref(n)+courbure_ref(n-1))/2.d0+courb)
       courbure_moy_ref(n-1)=courbure_moy_ref(n-1)/dist
       courbure_moy_ref(n-1) = abs(courbure_moy_ref(n-1))
    end do

!!$    do n=0,npt3-1
!!$       write(50,*) x(n),courbure_ref(n),courbure_moy_ref(n)
!!$    end do
    npt_old=0!npt
    do n=npt_old+1,npt1-2
       count=count+1
       xyz1(1)=x(npt_old)
       xyz1(2)=f(npt_old)
       xyz2(1)=x(n)
       xyz2(2)=f(n)
       xyz_M=(xyz1+xyz2)/2.d0
       n_ele1(1)=+(xyz2(2)-xyz1(2))
       n_ele1(2)=-(xyz2(1)-xyz1(1))
       n_ele1=n_ele1/sqrt(dot_product(n_ele1,n_ele1))
       t_ele1 = xyz1-xyz2
       t_ele1=t_ele1/sqrt(dot_product(t_ele1,t_ele1))
       xyz1=xyz2
       npt_new=n+1
       npt_newnew=n+2
       xyz2(1)=x(npt_new)
       xyz2(2)=f(npt_new)
       xyz_Mbis=(xyz1+xyz2)/2.d0
       n_ele(1)=+(xyz2(2)-xyz1(2))
       n_ele(2)=-(xyz2(1)-xyz1(1))
       n_ele=n_ele/sqrt(dot_product(n_ele,n_ele))
       longueur_ele =sqrt((xyz1(1)-xyz2(1))*(xyz1(1)-xyz2(1)) + (xyz1(2)-xyz2(2))*(xyz1(2)-xyz2(2)))
       xyz1=xyz2
       xyz2(1)=x(npt_newnew)
       xyz2(2)=f(npt_newnew)
       t_ele2 = xyz2-xyz1
       t_ele2=t_ele2/sqrt(dot_product(t_ele2,t_ele2))
       f_result = (t_ele1 + t_ele2)/2.d0
       !______________________________________________________________________!
       !_____________________courbure methode 1_______________________________!
       b=-n_ele(1) ; a=n_ele(2); c=xyz_Mbis(2)*n_ele(1)-xyz_Mbis(1)*n_ele(2)
       bb=-n_ele1(1) ; aa=n_ele1(2); cc=xyz_M(2)*n_ele1(1)-xyz_M(1)*n_ele1(2)
       if (abs(aa*b-bb*a)<=1.d-15) then
          courbure1(n) = 1.d-15
       else
          call find_intersection_point_droites(a,b,c,aa,bb,cc,xyzI)
       end if
       courbure1(n) = 1.d0/sqrt( (xyzI(1)-xyz1(1))*(xyzI(1)-xyz1(1)) + (xyzI(2)-xyz1(2))*(xyzI(2)-xyz1(2)) )
       !______________________________________________________________________!
       !_____________________courbure methode 2_______________________________!
       courbure2(n) = sqrt(f_result(1)*f_result(1) + f_result(2)*f_result(2))/longueur_ele
       !______________________________________________________________________!
       Erreur_L1= Erreur_L1 + abs(courbure1(n)-courbure_ref(n))
       Ref_L1= Ref_L1+courbure_ref(n)
       Erreur_L2= Erreur_L2 + (courbure1(n)-courbure_ref(n))**2
       Ref_L2= Ref_L2+courbure_ref(n)**2
       Erreur_Linfty=max(Erreur_Linfty,abs(courbure1(n)-courbure_ref(n)))
       Ref_Linfty=max(Ref_Linfty,courbure_ref(n))
       !______________________________________________________________________!
       Erreur_L1_bis= Erreur_L1_bis + abs(courbure2(n)-courbure_moy_ref(n))
       Ref_L1_bis= Ref_L1_bis+courbure_moy_ref(n)
       Erreur_L2_bis= Erreur_L2_bis + (courbure2(n)-courbure_moy_ref(n))**2
       Ref_L2_bis= Ref_L2_bis+courbure_moy_ref(n)**2
       Erreur_Linfty_bis=max(Erreur_Linfty_bis,abs(courbure2(n)-courbure_moy_ref(n)))
       Ref_Linfty_bis=max(Ref_Linfty_bis,courbure_moy_ref(n))
       !write(51,'(1pe17.10,1pe17.10,1pe17.10,1pe17.10,1pe17.10)') x(n),abs(courbure_ref(n)),abs(courbure_moy_ref(n)),&
       !    abs(courbure1(n)),abs(courbure2(n))
       !write(52,*) x(n),abs(courbure_ref(n)-courbure1(n)),abs(courbure_moy_ref(n)-courbure2(n))
       npt_old=n
    end do
    npt_old=npt1!+1
    do n =npt_old+1,npt2-2
       count=count+1
       xyz1(1)=x(npt_old)
       xyz1(2)=f(npt_old)
       xyz2(1)=x(n)
       xyz2(2)=f(n)
       xyz_M=(xyz1+xyz2)/2.d0
       n_ele1(1)=+(xyz2(2)-xyz1(2))
       n_ele1(2)=-(xyz2(1)-xyz1(1))
       n_ele1=n_ele1/sqrt(dot_product(n_ele1,n_ele1))
       t_ele1 = xyz1-xyz2
       t_ele1=t_ele1/sqrt(dot_product(t_ele1,t_ele1))
       xyz1=xyz2
       npt_new=n+1
       npt_newnew=n+2
       xyz2(1)=x(npt_new)
       xyz2(2)=f(npt_new)
       xyz_Mbis=(xyz1+xyz2)/2.d0
       n_ele(1)=+(xyz2(2)-xyz1(2))
       n_ele(2)=-(xyz2(1)-xyz1(1))
       n_ele=n_ele/sqrt(dot_product(n_ele,n_ele))
       longueur_ele =sqrt((xyz1(1)-xyz2(1))*(xyz1(1)-xyz2(1)) + (xyz1(2)-xyz2(2))*(xyz1(2)-xyz2(2)))
       xyz1=xyz2
       xyz2(1)=x(npt_newnew)
       xyz2(2)=f(npt_newnew)
       t_ele2 = xyz2-xyz1
       t_ele2=t_ele2/sqrt(dot_product(t_ele2,t_ele2))
       f_result = (t_ele1 + t_ele2)/2.d0
       !______________________________________________________________________!
       !_____________________courbure methode 1_______________________________!
       b=-n_ele(1) ; a=n_ele(2); c=xyz_Mbis(2)*n_ele(1)-xyz_Mbis(1)*n_ele(2)
       bb=-n_ele1(1) ; aa=n_ele1(2); cc=xyz_M(2)*n_ele1(1)-xyz_M(1)*n_ele1(2)
       if (abs(aa*b-bb*a)<=1.d-15) then
          courbure1(n) = 1.d-15
       else
          call find_intersection_point_droites(a,b,c,aa,bb,cc,xyzI)
       end if
       courbure1(n) = 1.d0/sqrt( (xyzI(1)-xyz1(1))*(xyzI(1)-xyz1(1)) + (xyzI(2)-xyz1(2))*(xyzI(2)-xyz1(2)) )
       !______________________________________________________________________!
       !_____________________courbure methode 2_______________________________!
       courbure2(n) = sqrt(f_result(1)*f_result(1) + f_result(2)*f_result(2))/longueur_ele
       if ( n==n0 .or. n==n0-1 .or. n==n0+1  ) then
          !if ( n==n0  ) then

          !print*,'calcul de courbure en x=0'
          count=count-1
          !print*, n
       else
          Erreur_L1= Erreur_L1 + abs(courbure1(n)-courbure_ref(n))
          Ref_L1= Ref_L1+courbure_ref(n)
          Erreur_L2= Erreur_L2 + (courbure1(n)-courbure_ref(n))**2
          Ref_L2= Ref_L2+courbure_ref(n)**2
          Erreur_Linfty=max(Erreur_Linfty,abs(courbure1(n)-courbure_ref(n)))
          Ref_Linfty=max(Ref_Linfty,courbure_ref(n))

          Erreur_L1_bis= Erreur_L1_bis + abs(courbure2(n)-courbure_moy_ref(n))
          Ref_L1_bis= Ref_L1_bis+courbure_moy_ref(n)
          Erreur_L2_bis= Erreur_L2_bis + (courbure2(n)-courbure_moy_ref(n))**2
          Ref_L2_bis= Ref_L2_bis+courbure_moy_ref(n)**2
          Erreur_Linfty_bis=max(Erreur_Linfty_bis,abs(courbure2(n)-courbure_moy_ref(n)))
          Ref_Linfty_bis=max(Ref_Linfty_bis,courbure_moy_ref(n))

          ! write(51,'(1pe17.10,1pe17.10,1pe17.10,1pe17.10,1pe17.10)') x(n),abs(courbure_ref(n)),abs(courbure_moy_ref(n)),&
          !      abs(courbure1(n)),abs(courbure2(n))
          !write(52,*) x(n),abs(courbure_ref(n)-courbure1(n)),abs(courbure_moy_ref(n)-courbure2(n))
       end if
       npt_old=n
    end do
    npt_old=npt2!+1
    do n =npt_old+1,npt3-2
       count=count+1
       xyz1(1)=x(npt_old)
       xyz1(2)=f(npt_old)
       xyz2(1)=x(n)
       xyz2(2)=f(n)
       xyz_M=(xyz1+xyz2)/2.d0
       n_ele1(1)=+(xyz2(2)-xyz1(2))
       n_ele1(2)=-(xyz2(1)-xyz1(1))
       n_ele1=n_ele1/sqrt(dot_product(n_ele1,n_ele1))
       t_ele1 = xyz1-xyz2
       t_ele1=t_ele1/sqrt(dot_product(t_ele1,t_ele1))
       xyz1=xyz2
       npt_new=n+1
       npt_newnew=n+2
       xyz2(1)=x(npt_new)
       xyz2(2)=f(npt_new)
       xyz_Mbis=(xyz1+xyz2)/2.d0
       n_ele(1)=+(xyz2(2)-xyz1(2))
       n_ele(2)=-(xyz2(1)-xyz1(1))
       n_ele=n_ele/sqrt(dot_product(n_ele,n_ele))
       longueur_ele =sqrt((xyz1(1)-xyz2(1))*(xyz1(1)-xyz2(1)) + (xyz1(2)-xyz2(2))*(xyz1(2)-xyz2(2)))
       xyz1=xyz2
       xyz2(1)=x(npt_newnew)
       xyz2(2)=f(npt_newnew)
       t_ele2 = xyz2-xyz1
       t_ele2=t_ele2/sqrt(dot_product(t_ele2,t_ele2))
       f_result = (t_ele1 + t_ele2)/2.d0
       !______________________________________________________________________!
       !_____________________courbure methode 1_______________________________!
       b=-n_ele(1) ; a=n_ele(2); c=xyz_Mbis(2)*n_ele(1)-xyz_Mbis(1)*n_ele(2)
       bb=-n_ele1(1) ; aa=n_ele1(2); cc=xyz_M(2)*n_ele1(1)-xyz_M(1)*n_ele1(2)
       if (abs(aa*b-bb*a)<=1.d-15) then
          courbure1(n) = 1.d-15
       else
          call find_intersection_point_droites(a,b,c,aa,bb,cc,xyzI)
       end if
       courbure1(n) = 1.d0/sqrt( (xyzI(1)-xyz1(1))*(xyzI(1)-xyz1(1)) + (xyzI(2)-xyz1(2))*(xyzI(2)-xyz1(2)) )
       !______________________________________________________________________!
       !_____________________courbure methode 2_______________________________!
       courbure2(n) = sqrt(f_result(1)*f_result(1) + f_result(2)*f_result(2))/longueur_ele
       !______________________________________________________________________!
       Erreur_L1= Erreur_L1 + abs(courbure1(n)-courbure_ref(n))
       Ref_L1= Ref_L1+courbure_ref(n)
       Erreur_L2= Erreur_L2 + (courbure1(n)-courbure_ref(n))**2
       Ref_L2= Ref_L2+courbure_ref(n)**2
       Erreur_Linfty=max(Erreur_Linfty,abs(courbure1(n)-courbure_ref(n)))
       Ref_Linfty=max(Ref_Linfty,courbure_ref(n))

       Erreur_L1_bis= Erreur_L1_bis + abs(courbure2(n)-courbure_moy_ref(n))
       Ref_L1_bis= Ref_L1_bis+courbure_moy_ref(n)
       Erreur_L2_bis= Erreur_L2_bis + (courbure2(n)-courbure_moy_ref(n))**2
       Ref_L2_bis= Ref_L2_bis+courbure_moy_ref(n)**2
       Erreur_Linfty_bis=max(Erreur_Linfty_bis,abs(courbure2(n)-courbure_moy_ref(n)))
       Ref_Linfty_bis=max(Ref_Linfty_bis,courbure_moy_ref(n))
       !write(51,'(1pe17.10,1pe17.10,1pe17.10,1pe17.10,1pe17.10)') x(n),abs(courbure_ref(n)),abs(courbure_moy_ref(n)),&
       !     abs(courbure1(n)),abs(courbure2(n))
       !write(52,*) x(n),abs(courbure_ref(n)-courbure1(n)),abs(courbure_moy_ref(n)-courbure2(n))
       npt_old=n
    end do

    Ref_L1=count ; Ref_L2=count ; Ref_Linfty=count
    Ref_L1_bis=count ; Ref_L2_bis=count ; Ref_Linfty_bis=count

    Erreur_L1= Erreur_L1/Ref_L1
    Erreur_L2= sqrt(Erreur_L2/Ref_L2)
    Erreur_Linfty= Erreur_Linfty/Ref_Linfty

    Erreur_L1_bis= Erreur_L1_bis/Ref_L1_bis
    Erreur_L2_bis= sqrt(Erreur_L2_bis/Ref_L2_bis)
    Erreur_Linfty_bis= Erreur_Linfty_bis/Ref_Linfty_bis

    write(41,'(I0,1pe16.9,1pe16.9,1pe16.9)') npt3,Erreur_L1,Erreur_L2,Erreur_Linfty
    write(41,'(I0,1pe16.9,1pe16.9,1pe16.9)') npt3,Erreur_L1_bis,Erreur_L2_bis,Erreur_Linfty
  end subroutine courbure_discontinue_trontin
  !*******************************************************************************

  !*******************************************************************************
  subroutine volume_2d(bul,ft,nt)
    !*******************************************************************************
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables 
    !-------------------------------------------------------------------------------
    type(struct_front_tracking),intent(in):: ft
    type(bulle),dimension(:),intent(inout):: bul
    integer, intent(in)                   :: nt
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------    
    integer                               :: n,j,m
    real(8),dimension(dim)                :: xyz0,xyz1,xyz2,v1,v2
    real(8)                               :: t1,t2,t_cpu
    !-------------------------------------------------------------------------------    
    call cpu_time(time=t1)

    !volume_tot=0
    do m=1,ft%nbbul
       if ( bul(m)%mask ) then
          xyz0=0
          do n=1,bul(m)%nbele
             !print*,bul(m)%nbele
             if ( bul(m)%ele(n)%mask ) then
                !-----------------barycenter research------------------
                j=bul(m)%ele(n)%nusom(1)  ;  xyz1=bul(m)%som(j)%xyz

                xyz1(1)=min( max( xyz1(1),xmin ),xmax )
                xyz1(2)=min( max( xyz1(2),ymin ),ymax )

                xyz0=xyz0+xyz1
             end if
          end do
          xyz0=xyz0/bul(m)%nbele

          bul(m)%volume_2d=0
          do n=1,bul(m)%nbele
             if (bul(m)%ele(n)%mask) then
                !-----------------volume calculation--------------------

                j=bul(m)%ele(n)%nusom(1)  ;  xyz1=bul(m)%som(j)%xyz
                j=bul(m)%ele(n)%nusom(2)  ;  xyz2=bul(m)%som(j)%xyz

                xyz1(1)=min( max( xyz1(1),xmin ),xmax )
                xyz1(2)=min( max( xyz1(2),ymin ),ymax )
                xyz2(1)=min( max( xyz2(1),xmin ),xmax )
                xyz2(2)=min( max( xyz2(2),ymin ),ymax )

                v1 =xyz1-xyz0
                v2 =xyz2-xyz0

                bul(m)%volume_2d=bul(m)%volume_2d + &
                     v1(1)*v2(2)-v1(2)*v2(1)
             end if
          end do
          bul(m)%volume_2d=bul(m)%volume_2d/2
          !print*,'-------------------'
          !print*,bul(m)%volume_2d
          !---------------------------VOLUME START-----------------------------
          write(25,'("t=",1pe9.2," m=",I0," V(num)=",1pe16.9)')&
               nt*dt,m,bul(m)%volume_2d
          !---------------------------VOLUME END-------------------------------
          ! volume_tot=volume_tot+bul(m)%volume_2d
       end if
    end do
    call cpu_time(time=t2)
    t_cpu=t2-t1
  end subroutine volume_2d
  !*******************************************************************************
  !*******************************************************************************

!!$  !===============================================================================
!!$  !===============================================================================
!!$  subroutine volume_2d(bul,ft,nt)
!!$    !-------------------------------------------------------------------------------  
!!$    implicit none
!!$    !-------------------------------------------------------------------------------
!!$    type(struct_front_tracking),intent(in):: ft
!!$    type(bulle),dimension(:),intent(inout):: bul
!!$    !real(8),intent(out)                   :: volume_tot
!!$    integer, intent(in)                   :: nt
!!$    !-------------------------------------------------------------------------------    
!!$    integer                               :: n,j,m
!!$    real(8),dimension(dim)                :: xyz0,xyz1,xyz2,v1,v2
!!$    real(8)                               :: t1,t2,t_cpu
!!$    !-------------------------------------------------------------------------------    
!!$    call cpu_time(time=t1)
!!$
!!$    !volume_tot=0
!!$    do m=1,ft%nbbul
!!$       if ( bul(m)%mask ) then
!!$          xyz0=0
!!$          do n=1,bul(m)%nbele
!!$             if (bul(m)%ele(n)%mask) then
!!$
!!$                j=bul(m)%ele(n)%nusom(1)  ;  xyz1=bul(m)%som(j)%xyz
!!$                xyz0=xyz0+xyz1
!!$             end if
!!$          end do
!!$          xyz0=xyz0/bul(m)%nbele
!!$
!!$          bul(m)%volume_2d=0
!!$          do n=1,bul(m)%nbele
!!$             if (bul(m)%ele(n)%mask) then
!!$
!!$                j=bul(m)%ele(n)%nusom(1)  ;  xyz1=bul(m)%som(j)%xyz
!!$                j=bul(m)%ele(n)%nusom(2)  ;  xyz2=bul(m)%som(j)%xyz
!!$
!!$                v1 =xyz1-xyz0
!!$                v2 =xyz2-xyz0
!!$
!!$                bul(m)%volume_2d=bul(m)%volume_2d + &
!!$                     v1(1)*v2(2)-v1(2)*v2(1)
!!$             end if
!!$          end do
!!$          bul(m)%volume_2d=bul(m)%volume_2d/2
!!$          !print*,'-------------------'
!!$          !print*,bul(m)%volume_2d
!!!VOLUME START
!!$          write(25,'("t=",1pe9.2," m=",I0," V(num)=",1pe16.9)')&
!!$               nt*dt,m,bul(m)%volume_2d
!!!VOLUME FIN
!!$          ! volume_tot=volume_tot+bul(m)%volume_2d
!!$       end if
!!$    end do
!!$    call cpu_time(time=t2)
!!$    t_cpu=t2-t1
!!$  end subroutine volume_2d
!!$  !===============================================================================
  subroutine coalesce_and_break_up(ft,bul)
    !-------------------------------------------------------------------------------
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    type(struct_front_tracking),intent(inout):: ft
    !type(bulle),dimension(:),intent(inout)   :: bul
    type(bulle),dimension(:),allocatable,intent(inout)   :: bul
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    type(raycasting),dimension(sx:ex,sy:ey,1)::maille
    integer,dimension(:), allocatable::maille_ele_temp,maille_bul_temp
    logical,dimension(:), allocatable::mask_temp
    integer,dimension(sx:ex,sy:ey,1):: decompte
    integer::nb_subdivision,i,j,newbul,k1,k1bis,vois1,vois2,som2,som1_vois1,p,som1,vois1_old
    integer::l,m,k,ibis,jbis,old_size,new_size,num_ele_mask,num_som_mask,max_ele,i0,j0,n
    integer::num_ele_mask_bis,num_som_mask_bis,som1_bis,num,num_old,vois2_new,vois1_k,vois1_l,bul2,bul2_new
    real(8)::dx1,dy1,work,ds,volume_bulk1
    real(8),dimension(dim)::xyz1,xyz2,xyz0,v1,v2
    type(sommet),dimension(:),allocatable  :: sommet_temp
    type(element),dimension(:),allocatable :: element_temp
    type(bulle),dimension(:),allocatable ::bul_temp
    integer(8),dimension(:),allocatable  :: renum_ele,renum_som
    logical::compte
    !----------------------------------------------------------------------------------------
!!$      if (allocated(bul)) deallocate(bul)
!!$      allocate(bul(ft%nbbulmax))
    !----------------------------decompte et affectation--------------------------------------
    decompte=0
    do l=1,ft%nbbul
       if (bul(l)%mask) then
          do m=1,bul(l)%nbele
             if (bul(l)%ele(m)%mask) then 
                xyz1=bul(l)%som( bul(l)%ele(m)%nusom(1) )%xyz

                i0=min(max(nint((xyz1(1)-xmin)/dx),sx),ex)
                j0=min(max(nint((xyz1(2)-ymin)/dy),sy),ey)
                decompte(i0,j0,1)=decompte(i0,j0,1)+1

             end if
          end do
       end if
    end do
    !----------------------------------------------------------------------------------------------
    do j=sy,ey
       do i=sx,ex
          if (allocated(maille(i,j,1)%element)) deallocate(maille(i,j,1)%element)
          if (allocated(maille(i,j,1)%bulbis)) deallocate(maille(i,j,1)%bulbis)
          if (allocated(maille(i,j,1)%mask)) deallocate(maille(i,j,1)%mask)

          if (decompte(i,j,1)/=0) then
             allocate(maille(i,j,1)%element(decompte(i,j,1)))
             allocate(maille(i,j,1)%bulbis(decompte(i,j,1)))
             allocate(maille(i,j,1)%mask(decompte(i,j,1)))
             maille(i,j,1)%mask=.true.
          end if
       end do
    end do
    !-------------------------------------------------------------------------------------------------
    decompte=0
    do l=1,ft%nbbul
       if (bul(l)%mask) then
          do m=1,bul(l)%nbele
             if (bul(l)%ele(m)%mask) then 
                xyz1=bul(l)%som( bul(l)%ele(m)%nusom(1) )%xyz

                i0=min(max(nint((xyz1(1)-xmin)/dx),sx),ex)
                j0=min(max(nint((xyz1(2)-ymin)/dy),sy),ey)
                decompte(i0,j0,1)=decompte(i0,j0,1)+1
                maille(i0,j0,1)%element(decompte(i0,j0,1))=m
                maille(i0,j0,1)%bulbis(decompte(i0,j0,1))=l
             end if
          end do
       end if
    end do
    !************************************************************************************************
    work=ft%ds_lim*ft%ds_lim
    !-------------------------------------------1er passage------------------------------------------
    do j=sy,ey
       do i=sx,ex

          do k=1,decompte(i,j,1)
             k1=maille(i,j,1)%bulbis(k)
             som1=bul( k1 )%ele( maille(i,j,1)%element(k) )%nusom(1)
             som2=bul( k1 )%ele( maille(i,j,1)%element(k) )%nusom(2)
             vois1=bul( k1 )%ele( maille(i,j,1)%element(k) )%ele_vois(1)
             vois2=bul( k1 )%ele( maille(i,j,1)%element(k) )%ele_vois(2)
             som1_vois1=bul( k1 )%ele( vois1 )%nusom(1)

             xyz1=bul( k1 )%som( bul( k1 )%ele( maille(i,j,1)%element(k) )%nusom(1) )%xyz
             maille(i,j,1)%mask(k)=.false.

             do jbis=max(sy,j-1),min(j+1,ey)
                do ibis=max(sx,i-1),min(i+1,ex)

                   bc2: do l=1,decompte(ibis,jbis,1)

                      if (maille(ibis,jbis,1)%mask(l)) then
                         k1bis=maille(ibis,jbis,1)%bulbis(l)
                         som1_bis=bul( k1bis )%ele( maille(ibis,jbis,1)%element(l) )%nusom(1)

                         xyz2=bul( k1bis )%som( bul( k1bis )%ele( maille(ibis,jbis,1)%element(l) )%nusom(1) )%xyz
                         ds= (xyz2(1)-xyz1(1))*(xyz2(1)-xyz1(1)) + (xyz2(2)-xyz1(2))*(xyz2(2)-xyz1(2))

                         if ( ds<work ) then
                            if ( som1_bis/=som1 .and.  som1_bis/=som2 .and. som1_bis/=som1_vois1 ) then
                               !--------------------coalescence or break up----------------------------------------
                               if ( k1/=k1bis ) then

                                  if ( bul(k1)%som( som1 )%compteur<0 .and. bul(k1bis)%som( som1_bis )%compteur<0 ) then
                                     !***********************************************************************************
                                     print*,'coalescence entree'
                                     !-------------------------------coalescence--------------------------------------
                                     !---------------------redimensionnement des tableaux-----------------------------
                                     if (bul(k1)%nbele_vrai+bul(k1bis)%nbele_vrai +2 > bul(k1)%nbelemax) then
                                        old_size=bul(k1)%nbele
                                        new_size=(2+bul(k1)%nbele_vrai+bul(k1bis)%nbele_vrai) * ft%multiplicateur_d_element
                                        allocate(element_temp(1:new_size))
                                        element_temp(1:old_size)=bul(k1)%ele(1:old_size)
                                        element_temp(old_size+1:new_size)%mask=.false.
                                        call MOVE_ALLOC(FROM=element_temp,TO=bul(k1)%ele)
                                        bul(k1)%nbelemax=new_size
                                     end if
                                     if (bul(k1)%nbsom_vrai+bul(k1bis)%nbsom_vrai +2 > bul(k1)%nbsommax) then
                                        old_size=bul(k1)%nbsom
                                        new_size=(2+bul(k1)%nbsom_vrai+bul(k1bis)%nbsom_vrai) * ft%multiplicateur_de_sommet
                                        allocate(sommet_temp(1:new_size))
                                        sommet_temp(1:old_size)=bul(k1)%som(1:old_size)
                                        sommet_temp(old_size+1:new_size)%mask=.false.
                                        call MOVE_ALLOC(FROM=sommet_temp,TO=bul(k1)%som)
                                        bul(k1)%nbsommax=new_size
                                     end if
                                     !------------------------recherche d'espaces dans la bulle ref----------------------
                                     allocate(renum_ele(bul(k1bis)%nbele),renum_som(bul(k1bis)%nbsom))
                                     !if (k1<k1bis) then !!!!!!!!!!!!! recopie le moins possible
                                     !else
                                     !end if
                                     num_ele_mask=1
                                     num_som_mask=1
                                     do m=1,bul(k1bis)%nbele
                                        if (bul(k1bis)%ele(m)%mask) then

                                           do while ( bul(k1)%ele(num_ele_mask)%mask ) !Recherche du mask faux (element)
                                              num_ele_mask=num_ele_mask+1
                                           end do
                                           bul(k1)%nbele=max(bul(k1)%nbele,num_ele_mask)

                                           do while ( bul(k1)%som(num_som_mask)%mask ) !Recherche du mask faux (sommet)
                                              num_som_mask=num_som_mask+1
                                           end do
                                           bul(k1)%nbsom=max(bul(k1)%nbsom,num_som_mask)

                                           !-----------------------------------renumerotation----------------------------
                                           renum_som(bul(k1bis)%ele(m)%nusom(1))=num_som_mask
                                           bul(k1)%ele(num_ele_mask)%mask=.true.
                                           renum_ele(m)=num_ele_mask
                                           bul(k1)%som(num_som_mask)%mask=.true.

                                        end if
                                     end do
                                     !-------------------------On copie une bulle dans la bulle ref-----------------------
                                     do m=1,bul(k1bis)%nbele
                                        if (bul(k1bis)%ele(m)%mask) then

                                           bul(k1)%ele(renum_ele(m))%nusom(1)= renum_som( bul(k1bis)%ele(m)%nusom(1) )
                                           bul(k1)%ele(renum_ele(m))%nusom(2)= renum_som( bul(k1bis)%ele(m)%nusom(2) )

                                           bul(k1)%som( renum_som(bul(k1bis)%ele(m)%nusom(1)) )%xyz = &
                                                bul(k1bis)%som( bul(k1bis)%ele(m)%nusom(1) )%xyz

                                           bul(k1)%som( renum_som(bul(k1bis)%ele(m)%nusom(1)) )%xyz_old = &
                                                bul(k1bis)%som( bul(k1bis)%ele(m)%nusom(1) )%xyz_old

                                           bul(k1)%som( renum_som(bul(k1bis)%ele(m)%nusom(1)) )%compteur = &
                                                bul(k1bis)%som( bul(k1bis)%ele(m)%nusom(1) )%compteur

                                           bul(k1)%som( renum_som(bul(k1bis)%ele(m)%nusom(1)) )%desenrichissement = &
                                                bul(k1bis)%som( bul(k1bis)%ele(m)%nusom(1) )%desenrichissement

                                           bul(k1)%som( renum_som(bul(k1bis)%ele(m)%nusom(1)) )%mask=.true.

                                           bul(k1)%ele(renum_ele(m))%ele_vois(1) =  renum_ele( bul(k1bis)%ele(m)%ele_vois(1) )
                                           bul(k1)%ele(renum_ele(m))%ele_vois(2) =  renum_ele( bul(k1bis)%ele(m)%ele_vois(2) )
                                           bul(k1)%ele(renum_ele(m))%mask=.true.

                                           bul(k1)%nbsom_vrai=bul(k1)%nbsom_vrai+1
                                           bul(k1)%nbele_vrai=bul(k1)%nbele_vrai+1

                                           xyz1=bul(k1)%som( renum_som(bul(k1bis)%ele(m)%nusom(1)) ) %xyz

                                           i0=min(max(nint((xyz1(1)-xmin)/dx),sx),ex) 
                                           j0=min(max(nint((xyz1(2)-ymin)/dy),sy),ey)

                                           bc3: do p=1,decompte(i0,j0,1)        
                                              if (maille(i0,j0,1)%bulbis(p)==k1bis)  then
                                                 if ( maille(i0,j0,1)%element(p)==m ) then
                                                    maille(i0,j0,1)%element(p)=renum_ele(m)
                                                    maille(i0,j0,1)%bulbis(p)=k1        
                                                    exit bc3
                                                 else
                                                    cycle bc3
                                                 end if
                                              else
                                                 cycle bc3
                                              end if
                                           end do bc3
                                        end if
                                     end do
                                     deallocate(renum_ele,renum_som)

                                     !----------------------------On dédouble les sommets dans la bulle----------------------------------
                                     !-------------------------sommet 1------------------------------------------------------------------
                                     do while ( bul(k1)%ele(num_ele_mask)%mask ) !Recherche du mask faux (element)
                                        num_ele_mask=num_ele_mask+1
                                     end do
                                     bul(k1)%nbele=max(bul(k1)%nbele,num_ele_mask)
                                     bul(k1)%nbele_vrai=bul(k1)%nbele_vrai+1

                                     do while ( bul(k1)%som(num_som_mask)%mask ) !Recherche du mask faux (sommet)
                                        num_som_mask=num_som_mask+1
                                     end do
                                     bul(k1)%nbsom=max(bul(k1)%nbsom,num_som_mask)
                                     bul(k1)%nbsom_vrai=bul(k1)%nbsom_vrai+1

                                     bul(k1)%som(num_som_mask)%mask=.true.
                                     bul(k1)%som(num_som_mask)%xyz=&
                                          bul( k1 )%som( bul(k1)%ele( maille(i,j,1)%element(k) )%nusom(1) )%xyz
                                     bul(k1)%som(num_som_mask)%xyz_old=&
                                          bul( k1 )%som( bul(k1)%ele( maille(i,j,1)%element(k) )%nusom(1) )%xyz_old

                                     bul(k1)%som(num_som_mask)%compteur=ft%compteur_coalescence
                                     bul(k1)%som(num_som_mask)%desenrichissement=ft%compteur_desenrichissement

                                     bul(k1)%ele(num_ele_mask)%mask=.true.
                                     !-------------------------sommet 2------------------------------------------------------------------
                                     num_ele_mask_bis=num_ele_mask
                                     num_som_mask_bis=num_som_mask
                                     do while ( bul(k1)%ele(num_ele_mask_bis)%mask ) !Recherche du mask faux (element)
                                        num_ele_mask_bis=num_ele_mask_bis+1
                                     end do
                                     bul(k1)%nbele=max(bul(k1)%nbele,num_ele_mask_bis)
                                     bul(k1)%nbele_vrai=bul(k1)%nbele_vrai+1

                                     do while ( bul(k1)%som(num_som_mask_bis)%mask ) !Recherche du mask faux (sommet)
                                        num_som_mask_bis=num_som_mask_bis+1
                                     end do
                                     bul(k1)%nbsom=max(bul(k1)%nbsom,num_som_mask_bis)
                                     bul(k1)%nbsom_vrai=bul(k1)%nbsom_vrai+1

                                     bul(k1)%som(num_som_mask_bis)%mask=.true.
                                     bul(k1)%som(num_som_mask_bis)%xyz=&
                                          bul( k1 )%som( bul(k1)%ele( maille(ibis,jbis,1)%element(l) )%nusom(1) )%xyz
                                     bul(k1)%som(num_som_mask_bis)%xyz_old=&
                                          bul( k1 )%som( bul(k1)%ele( maille(ibis,jbis,1)%element(l) )%nusom(1) )%xyz_old

                                     bul(k1)%som(num_som_mask_bis)%compteur=ft%compteur_coalescence
                                     bul(k1)%som(num_som_mask_bis)%desenrichissement=ft%compteur_desenrichissement

                                     bul(k1)%ele(num_ele_mask_bis)%mask=.true.
                                     !------------------------------------puis connexion---------------------------------------------
                                     bul(k1)%som(bul(k1)%ele( maille(i,j,1)%element(k) )%nusom(1))%compteur=ft%compteur_coalescence
                                     bul(k1)%som(bul(k1)%ele( maille(ibis,jbis,1)%element(l) )%nusom(1))%compteur=&
                                          ft%compteur_coalescence
                                     bul(k1)%som(bul(k1)%ele( maille(i,j,1)%element(k) )%nusom(1))%desenrichissement=&
                                          ft%compteur_desenrichissement
                                     bul(k1)%som(bul(k1)%ele( maille(ibis,jbis,1)%element(l) )%nusom(1))%desenrichissement=&
                                          ft%compteur_desenrichissement
                                     !---------------------------------------sommets------------------------------------------------
                                     vois1_k=bul(k1)%ele( maille(i,j,1)%element(k) )%ele_vois(1)
                                     vois1_l=bul(k1)%ele( maille(ibis,jbis,1)%element(l) )%ele_vois(1)
                                     !---------------------------------------------------------------------------------------------------
                                     bul(k1)%ele(num_ele_mask)%nusom(1)=bul(k1)%ele( vois1_l )%nusom(2)
                                     bul(k1)%ele(num_ele_mask)%nusom(2)=num_som_mask
                                     bul(k1)%ele( maille(i,j,1)%element(k) )%nusom(1)=num_som_mask

                                     bul(k1)%ele(num_ele_mask_bis)%nusom(1)=bul(k1)%ele( vois1_k )%nusom(2)
                                     bul(k1)%ele(num_ele_mask_bis)%nusom(2)=num_som_mask_bis
                                     bul(k1)%ele( maille(ibis,jbis,1)%element(l) )%nusom(1)=num_som_mask_bis
                                     !--------------rajouter les points supplementaires dans le decompte------------------------------
                                     xyz1=bul(k1)%som( bul(k1)%ele(num_ele_mask)%nusom(1) )%xyz
                                     i0=min(max(nint((xyz1(1)-xmin)/dx),sx),ex)
                                     j0=min(max(nint((xyz1(2)-ymin)/dy),sy),ey)

                                     old_size=decompte(i0,j0,1)
                                     decompte(i0,j0,1)=decompte(i0,j0,1)+1
                                     new_size=decompte(i0,j0,1)
                                     allocate(maille_ele_temp(1:new_size),maille_bul_temp(1:new_size),mask_temp(1:new_size))
                                     maille_ele_temp(1:old_size)=maille(i0,j0,1)%element(1:old_size)
                                     maille_bul_temp(1:old_size)=maille(i0,j0,1)%bulbis(1:old_size)
                                     mask_temp(1:old_size)=maille(i0,j0,1)%mask(1:old_size)
                                     call MOVE_ALLOC(FROM=maille_ele_temp,TO=maille(i0,j0,1)%element)
                                     call MOVE_ALLOC(FROM=maille_bul_temp,TO=maille(i0,j0,1)%bulbis)
                                     call MOVE_ALLOC(FROM=mask_temp,TO=maille(i0,j0,1)%mask)

                                     maille(i0,j0,1)%element(decompte(i0,j0,1))=num_ele_mask
                                     maille(i0,j0,1)%bulbis(decompte(i0,j0,1))=k1
                                     maille(i0,j0,1)%mask(decompte(i0,j0,1))=.true.

                                     xyz1=bul(k1)%som(bul(k1)%ele(num_ele_mask_bis)%nusom(1) )%xyz
                                     i0=min(max(nint((xyz1(1)-xmin)/dx),sx),ex)
                                     j0=min(max(nint((xyz1(2)-ymin)/dy),sy),ey)

                                     old_size=decompte(i0,j0,1)
                                     decompte(i0,j0,1)=decompte(i0,j0,1)+1
                                     new_size=decompte(i0,j0,1)
                                     allocate(maille_ele_temp(1:new_size),maille_bul_temp(1:new_size),mask_temp(1:new_size))
                                     maille_ele_temp(1:old_size)=maille(i0,j0,1)%element(1:old_size)
                                     maille_bul_temp(1:old_size)=maille(i0,j0,1)%bulbis(1:old_size)
                                     mask_temp(1:old_size)=maille(i0,j0,1)%mask(1:old_size)

                                     call MOVE_ALLOC(FROM=maille_ele_temp,TO=maille(i0,j0,1)%element)
                                     call MOVE_ALLOC(FROM=maille_bul_temp,TO=maille(i0,j0,1)%bulbis)
                                     call MOVE_ALLOC(FROM=mask_temp,TO=maille(i0,j0,1)%mask)

                                     maille(i0,j0,1)%element(decompte(i0,j0,1))=num_ele_mask_bis
                                     maille(i0,j0,1)%bulbis(decompte(i0,j0,1))=k1
                                     maille(i0,j0,1)%mask(decompte(i0,j0,1))=.true.
                                     !--------------------------------------voisins----------------------------------------------------
                                     bul(k1)%ele(num_ele_mask)%ele_vois(1)=vois1_l
                                     bul(k1)%ele(num_ele_mask)%ele_vois(2)=maille(i,j,1)%element(k)

                                     bul(k1)%ele(num_ele_mask_bis)%ele_vois(1)=vois1_k
                                     bul(k1)%ele(num_ele_mask_bis)%ele_vois(2)=maille(ibis,jbis,1)%element(l)

                                     bul(k1)%ele( maille(i,j,1)%element(k) )%ele_vois(1)=num_ele_mask
                                     bul(k1)%ele( vois1_l )%ele_vois(2)=num_ele_mask

                                     bul(k1)%ele( maille(ibis,jbis,1)%element(l) )%ele_vois(1)=num_ele_mask_bis
                                     bul(k1)%ele( vois1_k )%ele_vois(2)=num_ele_mask_bis
                                     !---------------------------------------------------------------------------------------------------
                                     deallocate (bul(k1bis)%ele,bul(k1bis)%som)
                                     bul(k1bis)%mask=.false.
                                     k1=maille(i,j,1)%bulbis(k)
                                     k1bis=maille(ibis,jbis,1)%bulbis(l)
                                     som1=bul( k1 )%ele( maille(i,j,1)%element(k) )%nusom(1)
                                     som2=bul( k1 )%ele( maille(i,j,1)%element(k) )%nusom(2)
                                     som1_bis=bul( k1bis )%ele( maille(ibis,jbis,1)%element(l) )%nusom(1)

                                     do n=1,ft%nbbul
                                        if (bul(n)%mask) then
                                           do m=1,bul(n)%nbele
                                              if (bul(n)%ele(m)%mask) then
                                                 write(55,*) n,bul(n)%ele(m)%ele_vois(1),m,bul(n)%ele(m)%ele_vois(2)
                                              end if
                                           end do
                                        else
                                        end if
                                     end do
                                     print*,'coalescence sortie'
                                     !***********************************************************************************
                                  else 
                                     cycle bc2
                                  end if
                               else
                                  if ( bul( k1 )%som( som1 )%compteur<0 .and. bul( k1bis )%som( som1_bis )%compteur<0 ) then
                                     print*,'break up'
                                     !--------------------------------break-up----------------------------------------
                                     !---------------------test sur le volume de la bulle-----------------------------
                                     som1=bul( k1 )%ele( maille(i,j,1)%element(k) )%nusom(1)
                                     som2=bul( k1 )%ele( maille(i,j,1)%element(k) )%nusom(2)
                                     som1_bis=bul( k1bis )%ele( maille(ibis,jbis,1)%element(l) )%nusom(1)

                                     vois2=maille(i,j,1)%element(k)
                                     xyz0=bul( k1 )%som( som1 )%xyz
                                     num=0
                                     volume_bulk1=0
                                     do while (som1/=som1_bis)
                                        num=num+1
                                        xyz1=bul( k1 )%som( som1 )%xyz
                                        xyz2=bul( k1 )%som( som2 )%xyz
                                        v1 =xyz1-xyz0
                                        v2 =xyz2-xyz0
                                        volume_bulk1=volume_bulk1 + v1(1)*v2(2)-v1(2)*v2(1)
                                        !--------------mise à jour des voisins-----------------------------
                                        vois2=bul( k1 )%ele( vois2 )%ele_vois(2)
                                        som1=bul( k1 )%ele ( vois2 )%nusom(1)
                                        som2=bul( k1 )%ele ( vois2 )%nusom(2)
                                        !------------------------------------------------------------------
                                     end do
                                     volume_bulk1=volume_bulk1/2
                                     print*,bul(k1)%volume_2d,volume_bulk1,num
                                     !----------------------------------------------------------------------
                                     if ( volume_bulk1 * (bul(k1)%volume_2d-volume_bulk1) <0 ) then 
                                        print*,'cas1-----------------------------------------------------------------------'
                                        !---------------------coalescence sur une meme bulle--------------------------------
                                     else 
                                        print*,'cas2-----------------------------------------------------------------------'
                                        !----------------------------break up-----------------------------------------------
                                     end if
                                     !******************************************************************************************
                                     !------------------------rupture------------------------------------------------
                                     newbul=ft%nbbul+1
                                     !---------------------------redimensionnement du tableau-------------------------
                                     if (newbul>ft%nbbulmax) then
                                        old_size=ft%nbbul
                                        new_size=(newbul) * ft%multiplicateur_de_bulle
                                        allocate(bul_temp(1:new_size))
                                        bul_temp(1:old_size)=bul(1:old_size)
                                        bul_temp(old_size+1:new_size)%mask=.false.
                                        call MOVE_ALLOC(FROM=bul_temp,TO=bul)
                                        ft%nbbulmax=new_size
                                     end if
                                     !--------------------------------------------------------------------------------
                                     newbul=1
                                     do while (bul(newbul)%mask)
                                        newbul=newbul+1
                                     end do
                                     ft%nbbul=max(newbul,ft%nbbul)
                                     bul(newbul)%mask=.true.
                                     !------------------recopie d'une partie de la bulle------------------------------
                                     !--------------------Recopier la bulle la + petite-------------------------------
                                     if (num<(bul(k1)%nbele_vrai/2)) then
                                        bul(newbul)%nbele_vrai=num+1
                                        bul(newbul)%nbele=bul(newbul)%nbele_vrai

                                        bul(newbul)%nbsom_vrai=num+1
                                        bul(newbul)%nbsom=bul(newbul)%nbsom_vrai

                                        som1=bul( k1 )%ele( maille(i,j,1)%element(k) )%nusom(1)
                                        som1_bis=bul( k1bis )%ele( maille(ibis,jbis,1)%element(l) )%nusom(1)
                                        vois2=maille(i,j,1)%element(k)
                                        vois1_old=bul(k1)%ele( maille(i,j,1)%element(k) )%ele_vois(1)
                                        vois2_new=maille(ibis,jbis,1)%element(l)
                                     else
                                        bul(newbul)%nbele_vrai=bul(k1)%nbele_vrai-num+1
                                        bul(newbul)%nbele=bul(newbul)%nbele_vrai

                                        bul(newbul)%nbsom_vrai=bul(k1)%nbsom_vrai-num+1
                                        bul(newbul)%nbsom=bul(newbul)%nbsom_vrai

                                        som1=bul( k1bis )%ele( maille(ibis,jbis,1)%element(l) )%nusom(1)
                                        som1_bis=bul( k1 )%ele( maille(i,j,1)%element(k) )%nusom(1)
                                        vois2=maille(ibis,jbis,1)%element(l)
                                        vois1_old=bul(k1bis)%ele( maille(ibis,jbis,1)%element(l) )%ele_vois(1)
                                        vois2_new=maille(i,j,1)%element(k)
                                     end if

                                     bul(k1)%som( som1 )%compteur=ft%compteur_coalescence
                                     bul(k1)%som( som1_bis )%compteur=ft%compteur_coalescence
                                     bul(k1)%som( som1 )%desenrichissement=ft%compteur_desenrichissement
                                     bul(k1)%som( som1_bis )%desenrichissement=ft%compteur_desenrichissement

                                     bul(newbul)%nbsommax=bul(newbul)%nbsom_vrai*ft%multiplicateur_de_sommet
                                     bul(newbul)%nbelemax=bul(newbul)%nbele_vrai*ft%multiplicateur_d_element

                                     allocate(bul(newbul)%som(bul(newbul)%nbsommax))
                                     allocate(bul(newbul)%ele(bul(newbul)%nbelemax))

                                     num_old=bul(newbul)%nbele_vrai
                                     num=0
                                     !------------------------------------------------------------------------------
                                     do while (som1/=som1_bis)
                                        num=num+1
                                        !-------------copie dans la nouvelle bulle----------------------------------
                                        bul(newbul)%ele(num)%nusom(1)=num
                                        bul(newbul)%ele(num)%nusom(2)=num+1
                                        bul(newbul)%som( bul(newbul)%ele(num)%nusom(1) )%xyz=bul(k1)%som (som1)%xyz
                                        bul(newbul)%som( bul(newbul)%ele(num)%nusom(1) )%xyz_old=bul(k1)%som(som1)%xyz_old
                                        bul(newbul)%som( bul(newbul)%ele(num)%nusom(1) )%mask=.true.
                                        bul(newbul)%ele(num)%mask=.true.
                                        bul(newbul)%ele(num)%ele_vois(1)=num_old
                                        bul(newbul)%ele(num_old)%ele_vois(2)=num
                                        bul(newbul)%som(num)%compteur=bul(k1)%som(som1)%compteur
                                        bul(newbul)%som(num)%desenrichissement=bul(k1)%som(som1)%desenrichissement
                                        !---------------suppression des elements dans l'ancienne bulle--------------
                                        bul(k1)%ele( vois2 )%mask=.false.
                                        bul(k1)%som( bul(k1)%ele( vois2 )%nusom(1) )%mask=.false.
                                        bul(k1)%nbsom_vrai=bul(k1)%nbsom_vrai-1
                                        bul(k1)%nbele_vrai=bul(k1)%nbele_vrai-1
                                        !---------------------------------------------------------------------------
                                        !-----------------------mise à jour de l'affectation---------------------------
                                        xyz1=bul(newbul)%som( bul(newbul)%ele(num)%nusom(1) )%xyz
                                        i0=min(max(nint((xyz1(1)-xmin)/dx),sx),ex) 
                                        j0=min(max(nint((xyz1(2)-ymin)/dy),sy),ey)
                                        bc4: do p=1,decompte(i0,j0,1)
                                           if (maille(i0,j0,1)%element(p)==vois2) then
                                              if (maille(i0,j0,1)%bulbis(p)==k1) then
                                                 maille(i0,j0,1)%bulbis(p)=newbul
                                                 maille(i0,j0,1)%element(p)=num
                                                 exit bc4
                                              end if
                                           end if
                                        end do bc4
                                        !------------------------------------------------------------------------------
                                        !-----------------------mise à jour des voisins et sommets---------------------
                                        vois2=bul( k1 )%ele( vois2 )%ele_vois(2)
                                        som1=bul( k1 )%ele ( vois2 )%nusom(1)
                                        !------------------------------------------------------------------------------
                                        num_old=num
                                     end do
                                     !------------------------on rajoute les points-------------------------------------
                                     !-------------------------Dans la nouvelle bulle------------------------------------
                                     num=num+1
                                     bul(newbul)%ele(num)%nusom(1)=num
                                     bul(newbul)%ele(num)%nusom(2)=1
                                     bul(newbul)%som( bul(newbul)%ele(num)%nusom(1) )%xyz=bul(k1)%som (som1_bis)%xyz
                                     bul(newbul)%som( bul(newbul)%ele(num)%nusom(1) )%xyz_old=bul(k1)%som (som1_bis)%xyz_old
                                     bul(newbul)%som(  bul(newbul)%ele(num)%nusom(1) )%mask=.true.
                                     bul(newbul)%ele(num)%mask=.true.
                                     bul(newbul)%ele(num)%ele_vois(1)=num_old
                                     bul(newbul)%ele(num_old)%ele_vois(2)=num

                                     bul(newbul)%som(num)%compteur=ft%compteur_coalescence
                                     bul(newbul)%som(num)%desenrichissement=ft%compteur_desenrichissement

                                     !-----------------------------------------------------------------------------------
                                     !--------------------------fin recopie newbul---------------------------------------
                                     !-----------------------------------------------------------------------------------
                                     bul(newbul)%ele(bul(newbul)%nbele+1:bul(newbul)%nbelemax)%mask=.false.
                                     bul(newbul)%som(bul(newbul)%nbsom+1:bul(newbul)%nbsommax)%mask=.false.
                                     !-----------------------------------------------------------------------------------
                                     !------------------rajouter les points supplementaires dans le decompte-------------
                                     xyz1=bul(newbul)%som( bul(newbul)%ele(num)%nusom(1) )%xyz
                                     i0=min(max(nint((xyz1(1)-xmin)/dx),sx),ex)
                                     j0=min(max(nint((xyz1(2)-ymin)/dy),sy),ey)
                                     old_size=decompte(i0,j0,1)
                                     decompte(i0,j0,1)=decompte(i0,j0,1)+1
                                     new_size=decompte(i0,j0,1)
                                     allocate(maille_ele_temp(1:new_size),maille_bul_temp(1:new_size),mask_temp(1:new_size))
                                     maille_ele_temp(1:old_size)=maille(i0,j0,1)%element(1:old_size)
                                     maille_bul_temp(1:old_size)=maille(i0,j0,1)%bulbis(1:old_size)
                                     mask_temp(1:old_size)=maille(i0,j0,1)%mask(1:old_size)
                                     call MOVE_ALLOC(FROM=maille_ele_temp,TO=maille(i0,j0,1)%element)
                                     call MOVE_ALLOC(FROM=maille_bul_temp,TO=maille(i0,j0,1)%bulbis)
                                     call MOVE_ALLOC(FROM=mask_temp,TO=maille(i0,j0,1)%mask)
                                     maille(i0,j0,1)%element(decompte(i0,j0,1))=num
                                     maille(i0,j0,1)%bulbis(decompte(i0,j0,1))=newbul
                                     maille(i0,j0,1)%mask(decompte(i0,j0,1))=.true.
                                     !-------------------------------------------------------------------------------------
                                     !----------------------------Dans la bulle k1-----------------------------------------
                                     num_ele_mask=1
                                     num_som_mask=1
                                     do while ( bul(k1)%ele(num_ele_mask)%mask ) !Recherche du mask faux (element)
                                        num_ele_mask=num_ele_mask+1
                                     end do
                                     bul(k1)%nbele=max(bul(k1)%nbele,num_ele_mask)
                                     bul(k1)%nbele_vrai=bul(k1)%nbele_vrai+1

                                     do while ( bul(k1)%som(num_som_mask)%mask ) !Recherche du mask faux (sommet)
                                        num_som_mask=num_som_mask+1
                                     end do
                                     bul(k1)%nbsom=max(bul(k1)%nbsom,num_som_mask)
                                     bul(k1)%nbsom_vrai=bul(k1)%nbsom_vrai+1

                                     bul(k1)%som(num_som_mask)%mask=.true.
                                     bul(k1)%ele(num_ele_mask)%mask=.true.
                                     bul(k1)%som(num_som_mask)%xyz=bul(k1)%som( bul(k1)%ele(vois1_old)%nusom(2) )%xyz
                                     bul(k1)%som(num_som_mask)%xyz_old=bul(k1)%som( bul(k1)%ele(vois1_old)%nusom(2) )%xyz_old

                                     bul(k1)%som(num_som_mask)%compteur=ft%compteur_coalescence
                                     bul(k1)%som(num_som_mask)%desenrichissement=ft%compteur_desenrichissement

                                     !------------------------------------puis connexion--------------------------------------------
                                     !---------------------------------------sommets------------------------------------------------
                                     bul(k1)%ele(num_ele_mask)%nusom(1)=num_som_mask
                                     bul(k1)%ele(num_ele_mask)%nusom(2)=som1_bis
                                     bul(k1)%ele(vois1_old)%nusom(2)=num_som_mask
                                     !------------------rajouter les points supplementaires dans le decompte------------------------
                                     xyz1=bul(k1)%som(bul(k1)%ele(num_ele_mask)%nusom(1) )%xyz
                                     i0=min(max(nint((xyz1(1)-xmin)/dx),sx),ex)
                                     j0=min(max(nint((xyz1(2)-ymin)/dy),sy),ey)
                                     old_size=decompte(i0,j0,1)
                                     decompte(i0,j0,1)=decompte(i0,j0,1)+1
                                     new_size=decompte(i0,j0,1)
                                     allocate(maille_ele_temp(1:new_size),maille_bul_temp(1:new_size),mask_temp(1:new_size))
                                     maille_ele_temp(1:old_size)=maille(i0,j0,1)%element(1:old_size)
                                     maille_bul_temp(1:old_size)=maille(i0,j0,1)%bulbis(1:old_size)
                                     mask_temp(1:old_size)=maille(i0,j0,1)%mask(1:old_size)
                                     call MOVE_ALLOC(FROM=maille_ele_temp,TO=maille(i0,j0,1)%element)
                                     call MOVE_ALLOC(FROM=maille_bul_temp,TO=maille(i0,j0,1)%bulbis)
                                     call MOVE_ALLOC(FROM=mask_temp,TO=maille(i0,j0,1)%mask)
                                     maille(i0,j0,1)%element(decompte(i0,j0,1))=num_ele_mask
                                     maille(i0,j0,1)%bulbis(decompte(i0,j0,1))=k1
                                     maille(i0,j0,1)%mask(decompte(i0,j0,1))=.true.
                                     !--------------------------------------voisins---------------------------------------------------
                                     bul(k1)%ele(num_ele_mask)%ele_vois(1)=vois1_old
                                     bul(k1)%ele(num_ele_mask)%ele_vois(2)=vois2_new
                                     bul(k1)%ele(vois1_old)%ele_vois(2)=num_ele_mask
                                     bul(k1)%ele(vois2_new)%ele_vois(1)=num_ele_mask
                                     !------------------------------------------------------------------------------------------------
                                     k1=maille(i,j,1)%bulbis(k)
                                     k1bis=maille(ibis,jbis,1)%bulbis(l)
                                     som1=bul( k1 )%ele( maille(i,j,1)%element(k) )%nusom(1)
                                     som2=bul( k1 )%ele( maille(i,j,1)%element(k) )%nusom(2)
                                     som1_bis=bul( k1bis )%ele( maille(ibis,jbis,1)%element(l) )%nusom(1)
                                     do n=1,ft%nbbul
                                        if (bul(n)%mask) then
                                           do m=1,bul(n)%nbele
                                              if (bul(n)%ele(m)%mask) then
                                                 write(56,*) n,bul(n)%ele(m)%ele_vois(1),m,bul(n)%ele(m)%ele_vois(2)
                                              end if
                                           end do
                                        end if
                                     end do
                                     !***************************************************************************************************
                                     print*,'sortie breakup'
                                  else
                                     cycle bc2
                                  end if
                               end if
                            else
                               cycle bc2
                            end if
                         else
                            cycle bc2
                         end if
                      else 
                         cycle bc2
                      end if
                   end do bc2
                end do
             end do

          end do

       end do
    end do
    !----------------------------2nd passage----------------------------------------
    !-------------------------------------------------------------------------------
  end subroutine coalesce_and_break_up
  !===============================================================================

#endif 

end module mod_front_tracking
!===============================================================================

