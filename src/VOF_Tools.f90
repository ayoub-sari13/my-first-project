!========================================================================
!
!                   FUGU Version 1.0.0
!
!========================================================================
!**
!**   NAME       : VOF_Tools.f90
!**
!**   AUTHOR     : Stéphane Vincent
!**                B. Trouette
!**                M. Elouafa
!**
!**   FUNCTION   : Tools
!**
!**   DATES      : Version 1.0.0  : from : May, 22
!**
!========================================================================


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#include "precomp.h"
module mod_VOF_Tools 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

contains

  subroutine tag_cou(tag,cou)
    !*******************************************************************************
    use mod_Parameters, only: nproc,dt, &
         & sx,ex,sy,ey,sz,ez,gx,gy,gz
    use mod_mpi
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable, intent(inout) :: tag
    real(8), dimension(:,:,:), allocatable, intent(in)    :: cou
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                               :: i,j,k,i0,j0,k0,l
    integer                                               :: m,n,cpt,nstencil
    integer                                               :: imin,imax,itagmax
    integer, dimension(:), allocatable                    :: istencil,iretag,ishift
    integer, dimension(:), allocatable                    :: tab_itag,tab_itag0
    integer, dimension(:,:,:), allocatable                :: itag
    logical                                               :: wloop
    !-------------------------------------------------------------------------------


    !-------------------------------------------------------------------------------
    ! local stencil index, +/- i0, ...
    !-------------------------------------------------------------------------------
    i0=1
    j0=1
    if (dim==2) then
       k0=0
    else
       k0=1
    end if
    !-------------------------------------------------------------------------------


    nstencil=2*dim
    allocate(istencil(nstencil))

    !-------------------------------------------------------------------------------
    ! integer tag array
    !-------------------------------------------------------------------------------
    allocate(itag(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
    !-------------------------------------------------------------------------------

    itag=0
    cpt=1
    ! algo de base, 1 proc
    do l=1,dim ! j'arrive pas a expliquer pourquoi il faut cette boucle ...
       do k=sz,ez
          do j=sy,ey
             do i=sx,ex

                if (cou(i,j,k)>0.5d0) then

                   wloop=.true.
                   do while(wloop)
                      wloop=.false.
                      !--------------------------------------------
                      ! stencil 
                      !--------------------------------------------
                      istencil(1)=itag(i-i0,j,k)
                      istencil(2)=itag(i+i0,j,k)
                      istencil(3)=itag(i,j-j0,k)
                      istencil(4)=itag(i,j+j0,k)
                      if (dim==3) then
                         istencil(5)=itag(i,j,k-k0)
                         istencil(6)=itag(i,j,k+k0)
                      end if
                      !--------------------------------------------
                      if (sum(istencil)>0) then
                         !--------------------------------------------
                         ! adjacent cells with ID ?
                         !--------------------------------------------
                         imin=100000000
                         do n=1,nstencil
                            if (istencil(n)>0.and.istencil(n)<=imin) then
                               imin=istencil(n)
                            end if
                         end do
                         ! verif id vois
                         itag(i,j,k)=imin
                         do n=1,nstencil-1
                            do m=n+1,nstencil
                               !--------------------------------------------
                               ! more than one adjacent cell ID ?
                               !--------------------------------------------
                               if (istencil(n)>0.and.istencil(m)>0) then
                                  if (istencil(n)/=istencil(m)) then
                                     wloop=.true.
                                     istencil(m)=min(istencil(m),istencil(n))
                                     istencil(n)=istencil(m)
                                  end if
                               end if
                               itag(i-i0,j,k)=istencil(1)
                               itag(i+i0,j,k)=istencil(2)
                               itag(i,j-j0,k)=istencil(3)
                               itag(i,j+j0,k)=istencil(4)
                               if (dim==3) then
                                  itag(i,j,k-k0)=istencil(5)
                                  itag(i,j,k+k0)=istencil(6)
                               end if
                            end do
                         end do
                      else
                         itag(i,j,k)=cpt
                         cpt=cpt+1
                      end if
                   end do
                end if
             end do
          end do
       end do
    end do

    !-------------------------------------------------------------------------------
    ! décompte pour que les tag soient contigues
    ! max ID 
    !-------------------------------------------------------------------------------
    itagmax=maxval(itag)
    allocate(iretag(itagmax))
    ! number of cells
    iretag=0
    do k=sz,ez
       do j=sy,ey
          do i=sx,ex
             if (itag(i,j,k)>0) then
                iretag(itag(i,j,k))=iretag(itag(i,j,k))+1
             end if
          end do
       end do
    end do
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Shifts
    !-------------------------------------------------------------------------------
    allocate(ishift(itagmax))
    ishift=0
    do i=1,itagmax-1
       if (iretag(i)==0) then
          ishift(i+1:itagmax)=ishift(i+1:itagmax)-1
       end if
    end do
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Final tags
    !-------------------------------------------------------------------------------
    do k=sz,ez
       do j=sy,ey
          do i=sx,ex
             if (itag(i,j,k)>0) then
                itag(i,j,k)=itag(i,j,k)+ishift(itag(i,j,k))
             end if
          end do
       end do
    end do
    !-------------------------------------------------------------------------------


    if (nproc>1) then

       !-------------------------------------------------------------------------------
       ! chaque proc a sa numerotation locale continue
       ! on décalle tout numero par rapport au nombre d'objet sur les procs précédents
       !-------------------------------------------------------------------------------
       allocate(tab_itag(nproc)) !nombre de proc par proc
       call mpi_allgather(maxval(itag),1,mpi_integer,tab_itag,1,mpi_integer,comm3d,code)
       write(*,*) "tab_itag",tab_itag
       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                if (itag(i,j,k)>0) then
                   itag(i,j,k)=itag(i,j,k)+sum(tab_itag(1:1+rank-1))
                end if
             end do
          end do
       end do
       call mpi_allgather(maxval(itag),1,mpi_integer,tab_itag,1,mpi_integer,comm3d,code)
       write(*,*) "tab_itag 2",tab_itag
       !-------------------------------------------------------------------------------


!!$       ! nombre d'objet max, avant fusion
!!$       call mpi_allreduce(maxval(itag),imax,1,MPI_INTEGER,MPI_SUM,COMM3D,CODE)
!!$       write(*,*) imax


       do l=1,2**dim
          !-------------------------------------------------------------------------------
          ! on échange pour fusionner les cellules qui sont à l'interface de deux ou plusieurs procs 
          !-------------------------------------------------------------------------------
          tag=itag ! passage en reel
          call comm_mpi_sca(tag)
          itag=tag ! retour en entiers
          !-------------------------------------------------------------------------------

          !-------------------------------------------------------------------------------
          ! fuuuuuuuuuuuuuuuusion ....
          !-------------------------------------------------------------------------------
          do k=sz,ez
             do j=sy,ey
                do i=sx,ex

                   if (itag(i,j,k)>0) then
                      wloop=.true.
                      do while(wloop)
                         wloop=.false.
                         !--------------------------------------------
                         ! stencil 
                         !--------------------------------------------
                         istencil(1)=itag(i-i0,j,k)
                         istencil(2)=itag(i+i0,j,k)
                         istencil(3)=itag(i,j-j0,k)
                         istencil(4)=itag(i,j+j0,k)
                         if (dim==3) then
                            istencil(5)=itag(i,j,k-k0)
                            istencil(6)=itag(i,j,k+k0)
                         end if
                         !--------------------------------------------
                         if (sum(istencil)>0) then
                            !--------------------------------------------
                            ! adjacent cells with ID ?
                            !--------------------------------------------
                            imin=100000000
                            do n=1,nstencil
                               if (istencil(n)>0.and.istencil(n)<=imin) then
                                  imin=istencil(n)
                               end if
                            end do
                            itag(i,j,k)=imin

                            do n=1,nstencil-1
                               do m=n+1,nstencil
                                  !--------------------------------------------
                                  ! more than one adjacent cell ID ?
                                  !--------------------------------------------
                                  if (istencil(n)>0.and.istencil(m)>0) then
                                     if (istencil(n)/=istencil(m)) then
                                        wloop=.true.
                                        istencil(m)=min(istencil(m),istencil(n))
                                        istencil(n)=min(istencil(m),istencil(n))
                                     end if
                                  end if
                                  itag(i-i0,j,k)=istencil(1)
                                  itag(i+i0,j,k)=istencil(2)
                                  itag(i,j-j0,k)=istencil(3)
                                  itag(i,j+j0,k)=istencil(4)
                                  if (dim==3) then
                                     itag(i,j,k-k0)=istencil(5)
                                     itag(i,j,k+k0)=istencil(6)
                                  end if
                               end do
                            end do
                         end if
                      end do
                   end if
                end do
             end do
          end do
       end do
       ! ... yhaaaaaaaaaaaa
       !-------------------------------------------------------------------------------


!!$       write(*,*) "ici rank =",rank, maxval(itag(sx:ex,sy:ey,sz:ez))
!!$       
!!$       call mpi_allgather(maxval(itag(sx:ex,sy:ey,sz:ez)),1,mpi_integer,tab_itag,1,mpi_integer,comm3d,code)
!!$       write(*,*) "ici ici", tab_itag
!!$
!!$
!!$       allocate(tab_itag0(nproc))
!!$       
!!$       tab_itag0=tab_itag
!!$       do n=1,nproc-1
!!$          do m=n+1,nproc
!!$             if (tab_itag0(n)==tab_itag0(m)) then
!!$                tab_itag0(m)=0
!!$             end if
!!$          end do
!!$       end do
!!$       write(*,*) "ici la", tab_itag0
!!$
!!$
!!$       if (allocated(ishift)) deallocate(ishift)
!!$       allocate(ishift(nproc))
!!$       
!!$       ishift=0
!!$       do n=1,nproc
!!$          if (tab_itag(n)>1) then
!!$             ishift(n)=sum(tab_itag0(1:n-1))
!!$          end if
!!$       end do
!!$
!!$       write(*,*) "ishift", ishift
!!$
!!$       do k=sz,ez
!!$          do j=sy,ey
!!$             do i=sx,ex
!!$                if (itag(i,j,k)>0) then
!!$                   
!!$                   
!!$                   
!!$                   
!!$                   itag(i,j,k)=itag(i,j,k)-ishift(rank+1)
!!$
!!$                end if
!!$             end do
!!$          end do
!!$       end do




    end if


    call mpi_allreduce(maxval(itag),imax,1,MPI_INTEGER,MPI_MAX,COMM3D,CODE)
    if (rank==0) then
       write(*,*) "Number of VOF object:", imax
    end if
    tag=itag





  end subroutine tag_cou
  
  
  subroutine Diffusion_cou(scas,diff,scain,pensca)
    use mod_Parameters, only: dt,dim, &
         & gx,gy,gz,                  &
         & sx,ex,sy,ey,sz,ez,         &
         & sxu,exu,syu,eyu,szu,ezu,   &
         & sxv,exv,syv,eyv,szv,ezv,   &
         & sxw,exw,syw,eyw,szw,ezw,   &
         & dx,dxu,dy,dyv,dz,dzw,      &
         & grid_x,grid_y,grid_z
    use mod_Constants, only: d1p3,d2p3
    use mod_struct_solver
    use mod_diffusion
    use mod_mpi
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), intent(in)                                     :: diff
    real(8), dimension(:,:,:), allocatable, intent(inout)   :: scas,scain
    real(8), dimension(:,:,:,:), allocatable, intent(inout) :: pensca
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    type(solver_sca_t)                                      :: sca_str
    integer                                                 :: i,j,k
    real(8)                                                 :: eps_cou
    real(8)                                                 :: dx_min,dy_min,dz_min,delta
    real(8)                                                 :: tmp
    real(8), dimension(:,:,:), allocatable                  :: diffu,diffv,diffw
    !-------------------------------------------------------------------------------
    
    !-------------------------------------------------------------------------------
    ! init
    !-------------------------------------------------------------------------------
    allocate(diffu(sxu-gx:exu+gx,syu-gy:eyu+gy,szu-gz:ezu+gz))
    allocate(diffv(sxv-gx:exv+gx,syv-gy:eyv+gy,szv-gz:ezv+gz))
    diffu=0; diffv=0
    if (dim==3) then
       allocate(diffw(sxw-gx:exw+gx,syw-gy:eyw+gy,szw-gz:ezw+gz))
       diffw=0
    end if
    !-------------------------------------------------------------------------------

    !--------------------------------------------------------------------
    ! cou smoothing with diffusion equation
    !--------------------------------------------------------------------
    if (.not.allocated(sca_str%output%it_solver)) then 
       allocate(sca_str%output%it_solver(1),&
            & sca_str%output%res_solver(1),&
            & sca_str%output%div(1))
       sca_str%output%it_solver=0
       sca_str%output%res_solver=0
    end if
    !--------------------------------------------------------------------
    ! time step
    !--------------------------------------------------------------------
    sca_str%dt=1
    !--------------------------------------------------------------------
    ! neumann boundary conditions
    !--------------------------------------------------------------------
    sca_str%bound%left=1
    sca_str%bound%right=1
    sca_str%bound%bottom=1
    sca_str%bound%top=1
    sca_str%bound%backward=1
    sca_str%bound%forward=1
    !--------------------------------------------------------------------
    pensca=0;scain=0     ! Neuman BC
    !--------------------------------------------------------------------
    ! h
    !--------------------------------------------------------------------
    tmp=minval(dx(sx:ex))
    CALL MPI_ALLREDUCE(tmp,dx_min,1,MPI_DOUBLE_PRECISION,MPI_MIN,COMM3D,MPI_CODE)
    tmp=minval(dy(sy:ey))
    CALL MPI_ALLREDUCE(tmp,dy_min,1,MPI_DOUBLE_PRECISION,MPI_MIN,COMM3D,MPI_CODE)
    if (dim==2) then
       delta=sqrt(dx_min*dy_min)
    else 
       tmp=minval(dz(sz:ez))
       CALL MPI_ALLREDUCE(tmp,dz_min,1,MPI_DOUBLE_PRECISION,MPI_MIN,COMM3D,MPI_CODE)
       delta=(dx_min*dy_min*dz_min)**d1p3
    end if
    !--------------------------------------------------------------------
    ! effective diffusivity
    !--------------------------------------------------------------------
    diffu=diff*delta**2
    diffv=diff*delta**2
    if (dim==3) diffw=diff*delta**2
    !--------------------------------------------------------------------
    call Diffusion_Delta(scas,scas,scas,diffu,diffv,diffw,scain,pensca,sca_str)
    !--------------------------------------------------------------------
    ! clipping
    !--------------------------------------------------------------------
    eps_cou=1d-6
    where (scas > 1-eps_cou)
       scas = 1
    elsewhere (scas < 0+eps_cou)
       scas = 0
    end where
    !--------------------------------------------------------------------
  end subroutine Diffusion_cou

  !===============================================================================
  ! DESCRIPTION OF FUNCTION AREA3D:
  ! compute in the unit cube the area cut by the plane
  ! [nr]*[x] = nr1*x1 + nr2*x2 + nr3*x3 = alpha
  ! INPUT: normal coefficients in nr(3) and volume fraction cc
  ! NOTE : the cut area A is invariant with respects to reflections, ordering 
  !        of the coefficients and midpoint (i.e., A(cc) = A(1-cc))
  !-------------------------------------------------------------------------------
  subroutine AREA3D_vof(nr,cc,AREA3D)

    IMPLICIT NONE
    REAL(8), INTENT(IN):: nr(3),cc
    REAL(8), intent(inout) :: AREA3D
    REAL(8) :: cch,ccr,al,c00,c01,c02,c03,np1,np2,np3
    REAL(8) :: m1,m2,m3,m12,numer,denom,p,pst,q,arc,csarc
    REAL(8), PARAMETER :: athird=1.d0/3.d0,eps0=1.d-50
    INTRINSIC DABS,DMIN1,DMAX1,DSQRT,DACOS,DCOS


    np1 = DABS(nr(1))                                 ! need positive coefficients
    np2 = DABS(nr(2))
    np3 = DABS(nr(3))
    m1 = DMIN1(np1,np2)                              ! order positive coefficients
    m3 = DMAX1(np1,np2)
    if (np3 < m1) then
       m2 = m1
       m1 = np3
    else if (np3 >= m3) then
       m2 = m3
       m3 = np3
    else
       m2 = np3
    endif

    denom = DMAX1(2.d0*m1*m2*m3,eps0)                          
    cch = DMIN1(cc,1.d0-cc)                              ! limit to: 0 < cch < 1/2
    ccr = DMAX1(cch,eps0)                                     ! avoid cch = m1 = 0 
    c01 = m1*m1*m1/(3.d0*denom)                                   ! get cch ranges
    c02  = c01 + 0.5d0*(m2-m1)/m3
    m12 = m1 + m2
    if (m12 <= m3) then
       c03 = 0.5d0*m12/m3
    else
       numer = m3*m3*(3.d0*m12-m3) + m1*m1*(m1-3.d0*m3) + m2*m2*(m2-3.d0*m3)
       c03 = numer/(3.d0*denom)
    endif
    c00 = DSQRT(m1*m1 + m2*m2 + m3*m3)

    ! 1: C<=C1; 2: C1<=C<=C2; 3: C2<=C<=C3; 4: C3<=C<=1/2 (a: m12<=m3; b: m3<m12))
    if (ccr <= c01) then
       al = (3.d0*denom*cch)**athird                                        
       AREA3D = c00*al*al/denom                                         ! case (1)
    else if (ccr <= c02) then
       al = 0.5d0*(m1 + DSQRT(m1*m1 + 8.d0*m2*m3*(cch - c01)))          
       AREA3D = c00*(2.d0*al-m1)/(2.d0*m2*m3)                            ! case (2)
    else if (ccr <= c03) then
       p = 2.d0*m1*m2                                                  
       q = 1.5d0*m1*m2*(m12 - 2.d0*m3*cch)
       pst = DSQRT(p)
       arc = athird*DACOS(q/(p*pst))
       csarc = DCOS(arc)
       al = pst*(DSQRT(3.d0*(1.d0-csarc*csarc)) - csarc) + m12
       AREA3D = c00*(-al*al + m1*(2.d0*al-m1) + m2*(2.d0*al-m2))/denom  ! case (3)
    else if (m12 <= m3) then                                 
       al = m3*cch + 0.5d0*m12                                        
       AREA3D = c00/m3                                                 ! case (4a)
    else                                 
       p = m12*m3 + m1*m2 - 0.25d0                                    
       q = 1.5d0*m1*m2*m3*(0.5d0-cch)
       pst = DSQRT(p)
       arc = athird*DACOS(q/(p*pst))
       csarc = DCOS(arc)
       al = pst*(DSQRT(3.d0*(1.d0-csarc*csarc)) - csarc) + 0.5d0
       AREA3D = c00*(2.d0*al*(1.d0-al) - c00*c00)/denom                ! case (4b)
    endif
  
  END subroutine  AREA3D_VOF


  subroutine manifold_death(cou,knd)
    !*******************************************************************************
    use mod_Parameters, only: dim,sx,ex,sy,ey,sz,ez,gx,gy,gz, &
         & grid_x,grid_y,grid_z,dx,dy,dz,                     &
         & VOF_MD_NHole,VOF_MD_length
    use mod_mpi
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable, intent(inout) :: cou
    real(8), dimension(:,:,:), allocatable, intent(inout) :: knd
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                               :: i,j,k
    integer                                               :: i0,j0,k0
    integer                                               :: nh,nh_max
    integer                                               :: cpt,cptt
    integer                                               :: ilength
    real(8)                                               :: couc,cous
    real(8)                                               :: rnd 
    !-------------------------------------------------------------------------------

    if (VOF_MD_length>0) then
       ilength=VOF_MD_length/minval(dx)
       if (modulo(ilength,2)==0) ilength=ilength+1
    else
       ilength=abs(VOF_MD_length)
    end if
    
    i0=max(1,(ilength-1)/2)
    j0=max(1,(ilength-1)/2)
    k0=max(1,(ilength-1)/2)
    if (dim==2) k0=0

!!$    write(*,*) "ilength=",ilength
!!$    stop
    
    
    call comm_mpi_sca(cou)
    call identify(cou,knd,ilength)
    
    couc=0.3d0
    couc=2*couc-1
    
    nh_max=VOF_MD_NHole
    cpt=0
    
    do k=sz,ez
       do j=sy,ey
          do i=sx,ex
             if (knd(i,j,k)<0) cpt=cpt+1
          end do
       end do
    end do
    
    call MPI_ALLREDUCE(cpt,cptt,1,MPI_INTEGER,MPI_SUM,COMM3D,CODE)
    nh_max=nh_max*real(cpt)/cptt
    
    
    if (nh_max>0.and.cpt>0) then

       nh=0
       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                
                if (nh<=nh_max) then 
                   
                   if (knd(i,j,k)<0) then 
                      cous=2*cou(i,j,k)-1
                      
                      call random_number(rnd)
                      if (rnd> real(nh_max)/cpt ) cycle
                      
                      nh=nh+1
                      if (cous>couc) then
                         cou(i-i0:i+i0,j-j0:j+j0,k-k0:k+k0)=0
                      else
                         cou(i-i0:i+i0,j-j0:j+j0,k-k0:k+k0)=1
                      end if
                      
                      if (cous<-couc) then
                         cou(i-i0:i+i0,j-j0:j+j0,k-k0:k+k0)=1
                      else
                         cou(i-i0:i+i0,j-j0:j+j0,k-k0:k+k0)=0
                      end if
                      
                      
                   end if
                   
                end if
                
             end do
          end do
       end do
       
    end if
    

    return
  end subroutine manifold_death

  subroutine identify(cou,knd,l_struct)
    !*******************************************************************************
    use mod_Parameters, only: dim,sx,ex,sy,ey,sz,ez,gx,gy,gz, &
         & grid_x,grid_y,grid_z,dx,dy,dz
    use mod_mpi
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)                                   :: l_struct
    real(8), dimension(:,:,:), allocatable, intent(inout) :: cou
    real(8), dimension(:,:,:), allocatable, intent(inout) :: knd
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                               :: i,j,k,m,n
    integer                                               :: is,js,ks,ns,ks0
    real(8)                                               :: dv
    real(8), dimension(:), allocatable                    :: x,x0
    real(8), dimension(:,:), allocatable                  :: T
    real(8), dimension(:,:,:), allocatable                :: phi,stencil
    !-------------------------------------------------------------------------------
    integer                                               :: LWORK,INFO
    real(8)                                               :: EPS_EV=1D-12
    real(8), allocatable, dimension(:)                    :: EV
    real(8), allocatable, dimension(:,:)                  :: WORK
    !-------------------------------------------------------------------------------

    ns=(l_struct-1)/2+1
    
    !-------------------------------------------------------------------------------
    ! init
    !-------------------------------------------------------------------------------
    allocate(x(dim),x0(dim))
    x=0
    x0=0
    allocate(T(dim,dim))
    T=0
    allocate(phi(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
    phi=0
    if (dim==2) then 
       allocate(stencil(-ns:+ns,-ns:+ns,1))
    else
       allocate(stencil(-ns:+ns,-ns:+ns,-ns:+ns))
    end if
    !-------------------------------------------------------------------------------
    allocate(EV(DIM))
    LWORK=3*DIM
    allocate(WORK(3*LWORK,3*LWORK))
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! symetric and binary color function 
    !-------------------------------------------------------------------------------
    m=gz
    if (dim==2) m=0
    do k=sz-m,ez+m
       do j=sy-gy,ey+gy
          do i=sx-gx,ex+gx
             phi(i,j,k)=cou(i,j,k)
             if (cou(i,j,k)>0.5d0) then
                phi(i,j,k)=1
             else
                phi(i,j,k)=0
             end if
          end do
       end do
    end do
    phi=2*phi-1
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! stencil coeff 
    !-------------------------------------------------------------------------------
    stencil=1
    if (dim==2) then
       stencil(-ns+1:+ns-1,-ns+1:+ns-1,1)=0
    else 
       stencil(-ns+1:+ns-1,-ns+1:+ns-1,-ns+1:+ns-1)=0
    end if
    !-------------------------------------------------------------------------------
    
    !-------------------------------------------------------------------------------
    ! loop over cells
    !-------------------------------------------------------------------------------
    do k=sz,ez
       do j=sy-gy+ns,ey+gy-ns
          do i=sx-gx+ns,ex+gx-ns
             !-------------------------------------------------------------------------------
             ! cell position and volume
             !-------------------------------------------------------------------------------
             if (dim==2) then
                x0=(/grid_x(i),grid_y(j)/)
                dv=1!dx(i)*dy(j)
             else
                x0=(/grid_x(i),grid_y(j),grid_z(k)/)
                dv=1!dx(i)*dy(j)*dz(k)
             end if
             !-------------------------------------------------------------------------------
             ! init quadratic form
             !-------------------------------------------------------------------------------
             T=0
             !-------------------------------------------------------------------------------
             ! loop over dim
             !-------------------------------------------------------------------------------
             do n=1,dim
                do m=1,dim
                   !-------------------------------------------------------------------------------
                   ! loop over stencil
                   !-------------------------------------------------------------------------------
                   do ks=(-ns)**(dim-2),ns**(dim-2)
                      do js=-ns,ns
                         do is=-ns,ns
                            !-------------------------------------------------------------------------------
                            ! cell of the stencil relative position
                            !-------------------------------------------------------------------------------
                            if (dim==2) then
                               x=(/grid_x(i+is),grid_y(j+js)/)
                               ks0=0
                            else
                               x=(/grid_x(i+is),grid_y(j+js),grid_z(k+ks)/)
                               ks0=ks
                            end if
                            x=x-x0
                            !-------------------------------------------------------------------------------
                            ! quadratic form
                            !-------------------------------------------------------------------------------
                            T(m,n)=T(m,n)+x(m)*x(n)*phi(i+is,j+js,k+ks0)*dv*stencil(is,js,ks)
                            !-------------------------------------------------------------------------------
                         end do
                      end do
                   end do
                   !-------------------------------------------------------------------------------
                   ! end loop over stencil
                   !-------------------------------------------------------------------------------
                end do
             end do
             !-------------------------------------------------------------------------------
             ! end loop over dim
             !-------------------------------------------------------------------------------

             !-------------------------------------------------------------------------------
             ! orthonormalize compute quadratic form
             !-------------------------------------------------------------------------------
             !call GramSchmidt_orthogonalization(T,dim)
             !call orthonormalization(T,dim)
             !-------------------------------------------------------------------------------

             !-------------------------------------------------------------------------------
             ! compute eigenvalues
             !-------------------------------------------------------------------------------
#if LAPACK
             INFO=0
             CALL DSYEV ('N', 'U', DIM, T, DIM, EV, WORK, LWORK, INFO)
             do n=1,dim
                if (abs(EV(n))<EPS_EV) EV(n)=0
             end do
#else
             if (rank==0) then
                write(*,*) "LAPACK is not compiled in this version, STOP at CALL DSYEV"
                write(*,*) "LAPACK is not compiled in this version, STOP at CALL DSYEV"
                write(*,*) "LAPACK is not compiled in this version, STOP at CALL DSYEV"
                write(*,*) "LAPACK is not compiled in this version, STOP at CALL DSYEV"
                write(*,*) "LAPACK is not compiled in this version, STOP at CALL DSYEV"
             end if
             call finalization_mpi
             stop
#endif 
             !-------------------------------------------------------------------------------

             !-------------------------------------------------------------------------------
             ! eigenvalues independant of the symetric phase indicator ?
             !-------------------------------------------------------------------------------
             !EV=EV*phi(i,j,k)
             !-------------------------------------------------------------------------------
             
             !-------------------------------------------------------------------------------
             ! type
             !-------------------------------------------------------------------------------
             call type_of_x0(knd(i,j,k),EV,N)
             !-------------------------------------------------------------------------------

          end do
       end do
    end do
    !-------------------------------------------------------------------------------
    ! end loop over cells
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! deallocation
    !-------------------------------------------------------------------------------
    deallocate(x,x0,T,phi,stencil,EV,WORK)
    !-------------------------------------------------------------------------------
    return
  end subroutine identify

  
  subroutine GramSchmidt_orthogonalization(A,N)
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)                    :: N
    real(8), dimension(N,N), intent(inout) :: A
    !-------------------------------------------------------------------------------
    integer                                :: i,j
    real(8), dimension(N)                  :: v
    real(8), dimension(N,N)                :: R,Q
    !-------------------------------------------------------------------------------

    Q=0
    R=0
    do j=1,N
       v=A(:,j)
       do i=1,j-1
          R(i,j)=dot_product(Q(:,i),A(:,j))
          v=v-R(i,j)*Q(:,i)
       end do
       R(j,j)=sqrt(dot_product(v,v))
       Q(:,j)=v/R(j,j)
    end do

!!$    do i=1,N
!!$       write(*,*) (A(i,j),j=1,N)
!!$    end do
!!$    write(*,*)
!!$    do i=1,N
!!$       write(*,*) (Q(i,j),j=1,N)
!!$    end do
    
    A=Q

    return
  end subroutine GramSchmidt_orthogonalization

  subroutine orthonormalization(A,N)
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)                    :: N
    real(8), dimension(N,N), intent(inout) :: A
    !-------------------------------------------------------------------------------
    integer                                :: j
    real(8), dimension(N)                  :: v
    !-------------------------------------------------------------------------------

    do j=1,N
       v=A(:,j)
       A(:,j)=A(:,j)/sqrt(dot_product(v,v))
    end do

    return
  end subroutine orthonormalization

  subroutine signature(EV,N,pos,neg,zero)
    !*******************************************************************************
    use mod_Parameters, only: dim
    implicit none
    !*******************************************************************************
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)               :: N
    real(8), dimension(N), intent(in) :: EV
    integer, intent(inout)            :: pos,neg,zero
    !-------------------------------------------------------------------------------
    
    pos=0
    neg=0
    zero=0
    
    if (EV(1)==0) zero=zero+1
    if (EV(1)>0)  pos=pos+1
    if (EV(1)<0)  neg=neg+1
    
    if (EV(2)==0) zero=zero+1
    if (EV(2)>0)  pos=pos+1
    if (EV(2)<0)  neg=neg+1

    if (dim==3) then
       if (EV(3)==0) zero=zero+1
       if (EV(3)>0)  pos=pos+1
       if (EV(3)<0)  neg=neg+1
    end if

    if (zero+pos+neg.ne.dim) then
       stop "probleme"
    end if
    
    return
  end  subroutine signature

  subroutine type_of_x0(knd,EV,N)
    !*******************************************************************************
    use mod_Parameters, only: dim
    implicit none
    !*******************************************************************************
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)               :: N
    real(8), dimension(N), intent(in) :: EV
    real(8), intent(inout)            :: knd
    !-------------------------------------------------------------------------------
    integer                           :: pos,neg,zero
    !-------------------------------------------------------------------------------

    call signature(EV,N,pos,neg,zero)

    !-------------------------------------------------------------------------------
    ! type
    !-------------------------------------------------------------------------------
    !     0 --> interface
    !     1 --> bulk phase 1
    !    -1 --> ligament 2D/3D
    !    -2 --> sheet 3D
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! default
    !-------------------------------------------------------------------------------
    knd=1
    !-------------------------------------------------------------------------------
    
    !-------------------------------------------------------------------------------
    ! cases
    !-------------------------------------------------------------------------------
!!$    if (zero>0) then
!!$       knd=0
!!$    else
!!$       if (pos==dim.or.neg==dim) knd=1    ! bulk
!!$       if (pos==1.and.neg==1)    knd=-1   ! 2D ligament
!!$       if (pos==1.and.neg==2)    knd=-1   ! 3D ligament
!!$       if (pos==2.and.neg==1)    knd=-2   ! 3D sheet
!!$    end if

    if (dim==2) then
       select case(zero)
       case(0)
          if (pos==1) knd=-1
       case(1,2)
          knd=0
       end select
    else
       select case(zero)
       case(0)
          if (pos==1) knd=-1
          if (pos==2) knd=-2
       case(1,2,3)
          knd=0
       end select
    end if

    
    !-------------------------------------------------------------------------------
    return    
  end subroutine type_of_x0

  subroutine cou_bounds(cou,uin,vin,win)
    use mod_Parameters, only: dim,             &
         & sx,ex,sy,ey,sz,ez,                  &
         & gsx,gex,gsy,gey,gsz,gez,gx,gy,gz,   &
         & VOF_init_icase
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable, intent(inout) :: cou
    real(8), dimension(:,:,:), allocatable, intent(in   ) :: uin,vin,win
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                               :: i,j,k
    !-------------------------------------------------------------------------------
    
    select case(VOF_init_icase)
    case(18)
       !----------------------------------------------------------------
       !   Fluid cou=1 inlets
       !----------------------------------------------------------------
       if (dim==2) then
          k = 1
          do j=sy-gy,ey+gy
             do i=sx-gx,ex+gx
                
                if (i==gsx) then
                   if (uin(i  ,j  ,k)>0) cou(i:i-gx,j,k)=1
                else if (i==gex) then
                   if (uin(i+1,j  ,k)<0) cou(i:i+gx,j,k)=1
                end if

                if (j==gsy) then
                   if (vin(i  ,j  ,k)>0) cou(i,j:j+gy,k)=1
                else if (j==gey) then 
                   if (vin(i  ,j+1,k)<0) cou(i,j:j+gy,k)=1
                end if
                
             end do
          end do
       else if (dim==3) then 
          do k=sz-gz,ez+gz
             do j=sy-gy,ey+gy
                do i=sx-gx,ex+gx

                   if (i==gsx) then
                      if (uin(i  ,j  ,k  )>0) cou(i:i-gx,j,k)=1
                   else if (i==gex) then
                      if (uin(i+1,j  ,k  )<0) cou(i:i+gx,j,k)=1
                   end if

                   if (j==gsy) then
                      if (vin(i  ,j  ,k  )>0) cou(i,j:j+gy,k)=1
                   else if (j==gey) then 
                      if (vin(i  ,j+1,k  )<0) cou(i,j:j+gy,k)=1
                   end if

                   if (k==gsz) then
                      if (win(i  ,j  ,k  )>0) cou(i,j,k:k-gz)=1
                   else if (k==gez) then 
                      if (win(i  ,j  ,k+1)<0) cou(i,j,k:k+gz)=1
                   end if

                end do
             end do
          end do
       end if
    end select
    
  end subroutine cou_bounds
  
end module mod_VOF_Tools
