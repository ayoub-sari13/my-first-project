!===============================================================================
module Bib_VOFLag_RESPECT_Resolutions
  !===============================================================================
  contains
  !-----------------------------------------------------------------------------  
  !> @author amine chadil
  ! 
  !> @brief cette routine appelle les routines de preparation du Module RESPECT
  !
  !> @param[out]   vl     : la structure contenant toutes les informations
  !! relatives aux particules.
  !> @param u,v,w         : velocity field components
  !> @param      cou      : la fonction couleur pour les viscosite calculees dans les points
  !! de pression.
  !> @param[out] rho      : masse volumiques dans le maillage de pression
  !> @param[out] rovu     : masse volumiques dans le maillage de la premiere composante de la vitesse
  !> @param[out] rovv     : masse volumiques dans le maillage de la deuxieme composante de la vitesse
  !> @param[out] rovw     : masse volumiques dans le maillage de la troisieme composante de la vitesse
  !> @param[out] vie      : viscosite d'elongation stockee dans le maillage de pression 
  !> @param[out] vir      : viscosite de rotation stockee dans le maillage de viscosite 
  !> @param[out] vis      : viscosite de cisaillement stockee dans le maillage de viscosite 
  !-----------------------------------------------------------------------------
  subroutine RESPECT_Resolutions(vl,u,v,w,Ru0,Rv0,Rw0,pres,rho,rovu,rovv,rovw,vie,vir, &
                                 vis,cou,cou_vis,cou_vts,couin0,cou_visin,cou_vtsin,   &
                                 diff_pre,diff_u_mean,diff_v_mean,diff_w_mean,         &
                                 pas_de_temps,iteration_temps)
    !===============================================================================
    !modules
    !===============================================================================
    !-------------------------------------------------------------------------------
    !Modules de definition des structures et variables => src/mod/module_...f90
    !-------------------------------------------------------------------------------
    use Module_VOFLag_ParticlesDataStructure
    use Module_VOFLag_Flows
    use mod_Parameters,                       only : deeptracking,dim
    !-------------------------------------------------------------------------------
    !Modules de subroutines de la librairie RESPECT => src/Bib_VOFLag_...f90
    !-------------------------------------------------------------------------------
    use Bib_VOFLag_Valid_UniformStokesFlowPastAParticle
    use Bib_VOFLag_ParticlesDataStructure_Update
    use Bib_VOFLag_caracteristiques_thermophys
    use Bib_VOFLag_WallCreaction
    !-------------------------------------------------------------------------------
    !===============================================================================
    !declarations des variables
    !===============================================================================
    implicit none
    !-------------------------------------------------------------------------------
    !variables globales
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:,:), allocatable, intent(inout) :: vir,vis,cou_vis,cou_vts, &
                                                               cou_visin,cou_vtsin
    real(8), dimension(:,:,:),   allocatable, intent(inout) :: cou,couin0,rho,rovu,rovv,&
                                                               rovw,vie,diff_pre,       &
                                                               diff_u_mean,diff_v_mean, &
                                                               diff_w_mean,u,v,w,Ru0,   &
                                                               Rv0,Rw0,pres
    real(8)                                 , intent(in)    :: pas_de_temps
    integer                                 , intent(in)    :: iteration_temps
    !-------------------------------------------------------------------------------
    type(struct_vof_lag),                     intent(inout) :: vl
    !-------------------------------------------------------------------------------
    !variables locales 
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'entree RESPECT_Resolutions'
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! MaJ des donnees des particules
    !-------------------------------------------------------------------------------
    if (vl%deplacement .or. vl%vrot) then
      call ParticlesDataStructure_Update (cou,cou_vis,cou_vts,couin0,cou_visin,cou_vtsin,u,v,w,vl)
    end if

    !-------------------------------------------------------------------------------
    ! calcul des caracteristiques thermophysiques apres calcul de la fonction couleur
    !-------------------------------------------------------------------------------
    call caracteristiques_thermophys(rho,rovu,rovv,rovw,vie,vir,vis,cou,cou_vis,cou_vts,dim)

    !-------------------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------------------
    if (uniform_past_part) then 
        call Valid_UniformStokesFlowPastAParticle(u,v,w,Ru0,Rv0,Rw0,pres,diff_pre,diff_u_mean,  &
                                                  diff_v_mean,diff_w_mean,U_inf,vl%objet(1)%pos,&
                                                  vl%objet(1)%sca(1),iteration_temps)
    end if
    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'sortie RESPECT_Resolutions'
    !-------------------------------------------------------------------------------
  end subroutine RESPECT_Resolutions

  !===============================================================================
end module Bib_VOFLag_RESPECT_Resolutions
!===============================================================================