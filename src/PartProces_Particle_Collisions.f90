!===============================================================================
module Bib_VOFLag_Particle_Collisions
  !===============================================================================
  contains
  !-----------------------------------------------------------------------------  
  !> @author amine chadil
  ! 
  !> @brief 
  !
  !> @todo ecrire sourcevl pour les particules non spheriques
  !
  !> @param[out] vl              : la structure contenant toutes les informations
  !! relatives aux particules.
  !> @param[in]  pas_de_temps    : 
  !-----------------------------------------------------------------------------
  subroutine Particle_Collisions(vl,smvuin,smvvin,smvwin,smvu,smvv,smvw,pas_de_temps) 
    !===============================================================================
    !modules
    !===============================================================================
    !-------------------------------------------------------------------------------
    !Modules de definition des structures et variables => src/mod/module_...f90
    !-------------------------------------------------------------------------------
    use Module_VOFLag_ParticlesDataStructure
    use mod_Parameters,                      only : deeptracking,dim
    !-------------------------------------------------------------------------------
    !Modules de subroutines de la librairie VOF-Lag
    !-------------------------------------------------------------------------------
    use Bib_VOFLag_Sphere_Collisions
    !-------------------------------------------------------------------------------
    !===============================================================================
    !declarations des variables
    !===============================================================================
    implicit none
    !-------------------------------------------------------------------------------
    !variables globales
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable, intent(inout) :: smvu,smvv,smvw
    real(8), dimension(:,:,:), allocatable, intent(in)    :: smvuin,smvvin,smvwin
    real(8),                                intent(in)    :: pas_de_temps
    !-------------------------------------------------------------------------------
    type(struct_vof_lag),                     intent(inout) :: vl 
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------
    !variables locales 
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'entree Particle_Collisions'
    !-------------------------------------------------------------------------------

    select case(vl%nature)

    case(1)
      !-------------------------------------------------------------------------------
      ! Si les particules sont spherique
      !-------------------------------------------------------------------------------
      call Sphere_Collisions (vl,dim,smvuin,smvvin,smvwin,smvu,smvv,smvw,pas_de_temps) 

    case default
      write(*,*) "STOP : sourcevl n est pas ecrite pour les particules non spherique"
      stop

    end select

    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'sortie Particle_Collisions'
    !-------------------------------------------------------------------------------
  end subroutine Particle_Collisions

  !===============================================================================
end module Bib_VOFLag_Particle_Collisions
!===============================================================================
  


