!*******************************************************************************                       
!*******************************************************************************

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
module mod_Filters
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
contains

  subroutine Filter_cou(fvar,var)
    use mod_Parameters, only: dim,sx,ex,sy,ey,sz,ez
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable, intent(in)    :: var
    real(8), dimension(:,:,:), allocatable, intent(inout) :: fvar
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                               :: i,j,k
    real(8)                                               :: cop=4
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! initialisation
    !-------------------------------------------------------------------------------
    fvar=0
    !-------------------------------------------------------------------------------

    if (dim==2) then
       k=1
       do j=sy,ey
          do i=sx,ex
             fvar(i,j,k) = var(i+1,j+1,k  ) +    cop*var(i+1,j,k  ) +     var(i+1,j-1,k  ) + &
                  &    cop*var(i  ,j+1,k  ) + cop**2*var(i  ,j,k  ) + cop*var(i  ,j-1,k  ) + &
                  &        var(i-1,j+1,k  ) +    cop*var(i-1,j,k  ) +     var(i-1,j-1,k  )
             fvar(i,j,k) = fvar(i,j,k) / (2 + cop)**dim
          end do
       end do
    else 
       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                fvar(i,j,k) = var(i+1,j+1,k+1) +    cop*var(i+1,j,k+1) +        var(i+1,j-1,k+1) + &
                     &    cop*var(i  ,j+1,k+1) + cop**2*var(i  ,j,k+1) +    cop*var(i  ,j-1,k+1) + &
                     &        var(i-1,j+1,k+1) +    cop*var(i-1,j,k+1) +        var(i-1,j-1,k+1) + &
                     &    cop*var(i+1,j+1,k  ) + cop**2*var(i+1,j,k  ) +    cop*var(i+1,j-1,k  ) + &
                     & cop**2*var(i  ,j+1,k  ) + cop**3*var(i  ,j,k  ) + cop**2*var(i  ,j-1,k  ) + &
                     &    cop*var(i-1,j+1,k  ) + cop**2*var(i-1,j,k  ) +    cop*var(i-1,j-1,k  ) + &
                     &        var(i+1,j+1,k-1) +    cop*var(i+1,j,k-1) +        var(i+1,j-1,k-1) + &
                     &    cop*var(i  ,j+1,k-1) + cop**2*var(i  ,j,k-1) +    cop*var(i  ,j-1,k-1) + &
                     &        var(i-1,j+1,k-1) +    cop*var(i-1,j,k-1) +        var(i-1,j-1,k-1) 
                fvar(i,j,k) = fvar(i,j,k) / (2 + cop)**dim
             end do
          end do
       end do
    end if

    fvar= var-fvar

  end subroutine Filter_cou



  subroutine filtre_test(sumfvar,var)

    use mod_Parameters, only: dim,sx,ex,sy,ey,sz,ez,gx,gy,gz
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable, intent(in)    :: var
    real(8), dimension(:,:,:), allocatable, intent(inout) :: sumfvar
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable                :: svar
    real(8), dimension(:,:,:,:), allocatable              :: fvar
    integer                                               :: i,j,k,n,nMax
    integer                                               :: i0,j0,k0
    real(8)                                               :: cop=4
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! initialisation
    !-------------------------------------------------------------------------------

    i0=1
    j0=1
    if (dim==2) then
       nMax=4
       k0=0
    else if (dim==3) then
       nMax=13
    end if
    
    allocate(fvar(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz,nMax))
    allocate(svar(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))

    fvar=0
    !-------------------------------------------------------------------------------

    if (dim==2) then
       k=1
       do j=sy-j0,ey+j0
          do i=sx-i0,ex+i0
             fvar(i,j,k,1) = (var(i-1,j  ,k)+cop*var(i,j,k)+var(i+1,j  ,k))/(cop+2)
             fvar(i,j,k,2) = (var(i  ,j-1,k)+cop*var(i,j,k)+var(i  ,j+1,k))/(cop+2)
             fvar(i,j,k,3) = (var(i-1,j-1,k)+cop*var(i,j,k)+var(i+1,j+1,k))/(cop+2)
             fvar(i,j,k,4) = (var(i-1,j+1,k)+cop*var(i,j,k)+var(i+1,j-1,k))/(cop+2)
          end do
       end do
       do n=1,nMax
          fvar(:,:,:,n)=1-abs(var-fvar(:,:,:,n))
       end do
    else
       do k=sz-k0,ez+k0
          do j=sy-j0,ey+j0
             do i=sx-i0,ex+i0
                fvar(i,j,k,1 ) = (var(i-1,j  ,k  )+cop*var(i,j,k)+var(i+1,j  ,k  ))/(cop+2)
                fvar(i,j,k,2 ) = (var(i  ,j-1,k  )+cop*var(i,j,k)+var(i  ,j+1,k  ))/(cop+2)
                fvar(i,j,k,3 ) = (var(i  ,j  ,k-1)+cop*var(i,j,k)+var(i  ,j  ,k+1))/(cop+2) 

                fvar(i,j,k,4 ) = (var(i-1,j-1,k  )+cop*var(i,j,k)+var(i+1,j+1,k  ))/(cop+2)
                fvar(i,j,k,5 ) = (var(i-1,j+1,k  )+cop*var(i,j,k)+var(i+1,j-1,k  ))/(cop+2)
                fvar(i,j,k,6 ) = (var(i-1,j  ,k-1)+cop*var(i,j,k)+var(i+1,j  ,k+1))/(cop+2)
                fvar(i,j,k,7 ) = (var(i-1,j  ,k+1)+cop*var(i,j,k)+var(i+1,j  ,k-1))/(cop+2)
                fvar(i,j,k,8 ) = (var(i  ,j-1,k-1)+cop*var(i,j,k)+var(i  ,j+1,k+1))/(cop+2)
                fvar(i,j,k,9 ) = (var(i  ,j-1,k+1)+cop*var(i,j,k)+var(i  ,j+1,k-1))/(cop+2)

                fvar(i,j,k,10) = (var(i-1,j-1,k-1)+cop*var(i,j,k)+var(i+1,j+1,k+1))/(cop+2)
                fvar(i,j,k,11) = (var(i+1,j-1,k-1)+cop*var(i,j,k)+var(i-1,j+1,k+1))/(cop+2)
                fvar(i,j,k,12) = (var(i-1,j+1,k-1)+cop*var(i,j,k)+var(i+1,j-1,k+1))/(cop+2)
                fvar(i,j,k,13) = (var(i+1,j+1,k-1)+cop*var(i,j,k)+var(i-1,j-1,k+1))/(cop+2)
             end do
          end do
       end do
       do n=1,nMax
          fvar(:,:,:,n)=1-abs(var-fvar(:,:,:,n))
       end do
    end if

    sumfvar=10000
    do n=1,nMax
       do k=sz-k0,ez+k0
          do j=sy-j0,ey+j0
             do i=sx-i0,ex+i0
                if ( sumfvar(i,j,k)>=fvar(i,j,k,n)) then !>=0.999) then
                   !sumfvar(i,j,k)=sumfvar(i,j,k)+1
                   sumfvar(i,j,k)=fvar(i,j,k,n)
                end if
             end do
          end do
       end do
    end do

!!$    k=1
!!$    do j=sy-1,ey+1
!!$       do i=sx-1,ex+1
!!$          svar(i,j,k) = sumfvar(i+1,j+1,k  ) +    cop*sumfvar(i+1,j,k  ) +     sumfvar(i+1,j-1,k  ) + &
!!$               &    cop*sumfvar(i  ,j+1,k  ) + cop**2*sumfvar(i  ,j,k  ) + cop*sumfvar(i  ,j-1,k  ) + &
!!$               &        sumfvar(i-1,j+1,k  ) +    cop*sumfvar(i-1,j,k  ) +     sumfvar(i-1,j-1,k  )
!!$          svar(i,j,k) = svar(i,j,k) / (2 + cop)**dim
!!$       end do
!!$    end do
!!$    
!!$    sumfvar=svar

    deallocate(fvar)
    return
  end subroutine filtre_test


!!$  subroutine barFilterScalar (bsca,sca)
!!$    !---------------------------------------------------------------------------------
!!$    ! HAT Filtering subroutine
!!$    !---------------------------------------------------------------------------------
!!$    use mod_Parameters, only: dim,sx,ex,sy,ey,sz,ez,gx,gy,gz
!!$    implicit none
!!$    real(8), allocatable, dimension(:,:,:), intent(inout) :: bsca
!!$    real(8), allocatable, dimension(:,:,:), intent(out)   :: sca
!!$
!!$
!!$    real(8), allocatable, dimension(:) :: fsimpx,fsimpy,fsimpz
!!$
!!$    allocate(fsimpx(sx-gx:ex+gx))
!!$    allocate(fsimpy(sy-gy:ey+gy))
!!$    allocate(fsimpz(sz-gz:ez+gz))
!!$
!!$    
!!$    bsca = 0
!!$
!!$    do k=sz,ez
!!$       do j=sy,ey
!!$
!!$          do i=sx,ex
!!$             bsca(i,j,k) = (sca(i-1,j,k) + 4*sca(i,j,k) + sca(i+1,j,k)) / 6
!!$          end do
!!$
!!$          
!!$       enddo
!!$    enddo
!!$
!!$    do k=sz,ez
!!$       do i=sx,ex
!!$
!!$          do j=sy,ey
!!$             bsca(i,j,k) = (bsca(i,j-1,k) + 4*bsca(i,j,k) + bsca(i,j+1,k)) / 6
!!$          end do
!!$
!!$       enddo
!!$    enddo
!!$
!!$
!!$    do j=sy,ey
!!$       do i=sx,ex
!!$          fsimpz(:)   = bsca(i,j,:)
!!$          bsca(i,j,:) = simpson_1D(fsimpz, nz, b)
!!$       enddo
!!$    enddo
!!$
!!$    deallocate(fsimpx,fsimpy,fsimpz)
!!$    
!!$    return
!!$  end subroutine barFilterScalar
!!$  
!!$  subroutine  simpson_1D (f,is,ie)
!!$    
!!$    integer, intent(in) :: is,ie,b
!!$    real(8), intent(in), dimension(:), allocatable :: f
!!$    real(8), dimension(0:n) :: simpson_1D
!!$    integer :: i,p
!!$    simpson_1D = 0.
!!$
!!$    do i=is,ie 
!!$       simpson_1D(i) =  (f(i-b) + 4*f(i) + f(i+b)) / 6
!!$    end do
!!$
!!$  end subroutine simpson_1D
 


end module mod_Filters
