!===============================================================================
module Bib_VOFLag_Sphere_PointIn
  !===============================================================================
  contains
  !-----------------------------------------------------------------------------  
  !> @author amine chadil, jorge cesar brandle de motta
  ! 
  !> @brief cette routine renseigne sur l'etat du point p par rapport a la copie nbt de la 
  !! sphere np (la routine dit si p est a l'interieur ou a l'exterieur de la sphere) 
  !
  !> @param[in]  vl        : la structure contenant toutes les informations
  !! relatives aux particules
  !> @param[in]  np        : numero de la particule dans vl
  !> @param[in]  nbt       : numero de la copie de la particule np
  !> @param[in]  sca         : les dimensions de copie nbt de la particule np avec lesquelles
  !! le test sera fait
  !> @param[in]  p         : le point a tester
  !> @param[out] in_sphere : vrai si le point est a l'interieur de la sphere faux sinon
  !-----------------------------------------------------------------------------
  subroutine Sphere_PointIn(vl,np,nbt,sca,p,ndim,in_sphere)
    !===============================================================================
    !modules
    !===============================================================================
    !-------------------------------------------------------------------------------
    !Modules de definition des structures et variables => src/mod/module_...f90
    !-------------------------------------------------------------------------------
    use Module_VOFLag_ParticlesDataStructure
    use mod_Parameters,                      only : deeptracking
    !-------------------------------------------------------------------------------

    !===============================================================================
    !declarations des variables
    !===============================================================================
    implicit none
    !-------------------------------------------------------------------------------
    !variables globales
    !-------------------------------------------------------------------------------
    type(struct_vof_lag), intent(inout) :: vl
    logical,              intent(out)   :: in_sphere
    real(8), dimension(3),intent(in)    :: p,sca
    integer,              intent(in)    :: np,nbt  
    integer,              intent(in)    :: ndim
    !-------------------------------------------------------------------------------
    !variables locales 
    !-------------------------------------------------------------------------------
    double precision, dimension(3)               :: pos_sphere
    double precision                             :: distance
    integer                                      :: i
    !-------------------------------------------------------------------------------
    !if (deeptracking) write(*,*) 'entree Sphere_PointIn'
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    !Initialisation
    !-------------------------------------------------------------------------------
    in_sphere = .false.
    pos_sphere(1:ndim)=vl%objet(np)%symper(nbt,1:ndim)
    distance = 0.0d0

    !-------------------------------------------------------------------------------
    ! calcul de la distance entre p et l'origine de la sphere
    !-------------------------------------------------------------------------------
    do i=1,ndim
       distance = distance + (p(i)-pos_sphere(i))**2
    end do

    !-------------------------------------------------------------------------------
    ! comparaison de cette distance avec le rayon de la sphere
    !-------------------------------------------------------------------------------
    if (distance .le. sca(1)*sca(1)) in_sphere = .true.
    !-------------------------------------------------------------------------------
    !if (deeptracking) write(*,*) 'sortie Sphere_PointIn'
    !-------------------------------------------------------------------------------
  end subroutine Sphere_PointIn

  !===============================================================================
end module Bib_VOFLag_Sphere_PointIn
!===============================================================================
  


