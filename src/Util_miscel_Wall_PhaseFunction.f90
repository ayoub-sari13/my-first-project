!===============================================================================
module Bib_VOFLag_Wall_PhaseFunction
   !===============================================================================
   contains
   !-----------------------------------------------------------------------------  
   !> 
   ! 
   !> 
   !! 
   !! 
   !
   !> 
   !
   !> 
   !! 
   !! 
   !> 
   !-----------------------------------------------------------------------------
   subroutine Wall_PhaseFunction(couin0,cou_visin,cou_vtsin,grid_x,grid_y,grid_z,&
                                 grid_xu,grid_yv,grid_zw,ndim)
      !===============================================================================
      !modules
      !===============================================================================
      !-------------------------------------------------------------------------------
      !Modules de definition des structures et variables => src/mod/module_...f90
      !-------------------------------------------------------------------------------
      use Module_VOFLag_ParticlesDataStructure
      use mod_Parameters,                       only : rank,nproc,deeptracking
      use mod_mpi
      !-------------------------------------------------------------------------------
      !Modules de subroutines de la librairie RESPECT => src/Bib_VOFLag_...f90
      !-------------------------------------------------------------------------------
      !===============================================================================
      !declarations des variables
      !===============================================================================
      implicit none
      !-------------------------------------------------------------------------------
      !variables globales
      !-------------------------------------------------------------------------------
      real(8), dimension(:,:,:,:), allocatable, intent(inout) :: cou_visin,cou_vtsin
      real(8), dimension(:,:,:)  , allocatable, intent(inout) :: couin0
      real(8), dimension(:),       allocatable, intent(in)    :: grid_xu,grid_yv,grid_zw
      real(8), dimension(:),       allocatable, intent(in)    :: grid_x,grid_y,grid_z
      integer,                                  intent(in)    :: ndim
      !-------------------------------------------------------------------------------
      !variables locales 
      !-------------------------------------------------------------------------------
      real(8), dimension(:,:,:)  , allocatable              :: cou_vis_tmp
      real(8), dimension(3)                                 :: ne_av,nw_av,se_av,sw_av,ne_der,&
                                                               nw_der,se_der,sw_der,pt_fictif
      real(8)                                               :: xxb,xxf,yyb,yyf,zzb,zzf
      real(8)                                               :: xxd,xxg,yyd,zzh,sum1,sum0,sumvis1,sumvis10,&
                                                               sumvis3,sumvis30,sumvts1,sumvts10,sumvts2,sumvts20
      integer                                               :: np,nbt,nd,ltp,total_pt_fictifs,&
                                                               nb_pt_fictifs_in_particle,ii,ik,&
                                                               ij,I,J,K,ltp_particle,l,nb,nb0
      logical                                               :: ne_av_in,nw_av_in,se_av_in,sw_av_in,&
                                                               ne_der_in,nw_der_in,se_der_in,&
                                                               sw_der_in,pt_fictif_in
      !-------------------------------------------------------------------------------

      !-------------------------------------------------------------------------------
      if (deeptracking) write(*,*) 'entree Wall_PhaseFunction'
      !-------------------------------------------------------------------------------

      !-------------------------------------------------------------------------------
      !initialisation
      !-------------------------------------------------------------------------------
      total_pt_fictifs= 10
      couin0   =0d0
      cou_vtsin=0d0
      cou_visin=0d0
      ne_av =0d0;nw_av =0d0;se_av =0d0;sw_av =0d0
      ne_der=0d0;nw_der=0d0;se_der=0d0;sw_der=0d0
      pt_fictif=0.0d0
      !-------------------------------------------------------------------------------
      !ne traiter que les particules (ou copies) qui intersecte le domaine du proc courant
      !-------------------------------------------------------------------------------
      !-------------------------------------------------------------------------------
      !traitement des points de pression appartenant aux box
      !-------------------------------------------------------------------------------
      if (ndim==2) then
         do j=sy,ey
            do i=sx,ex
               !-------------------------------------------------------------------------------
               !construction de la cellule centree en lpt
               !-------------------------------------------------------------------------------
               xxb=grid_xu(i);xxf=grid_xu(i+1)
               yyb=grid_yv(j);yyf=grid_yv(j+1)
               ne_av(1)=xxf;ne_av(2)=yyf;nw_av(1)=xxb;nw_av(2)=yyf
               se_av(1)=xxf;se_av(2)=yyb;sw_av(1)=xxb;sw_av(2)=yyb
               !-------------------------------------------------------------------------------
               !tester chacun des coins de la cellule (in ou out de la particule)
               !-------------------------------------------------------------------------------
               ne_av_in=.false.;se_av_in=.false.
               nw_av_in=.false.;sw_av_in=.false.
               if (ne_av(1)<=WallPos(1)) ne_av_in=.true.
               if (se_av(1)<=WallPos(1)) se_av_in=.true.
               if (nw_av(1)<=WallPos(1)) nw_av_in=.true.
               if (sw_av(1)<=WallPos(1)) sw_av_in=.true.
               !-------------------------------------------------------------------------------
               !si tous les coins de la cellule sont a l'interieur de la particule
               !-------------------------------------------------------------------------------
               if (ne_av_in.and.nw_av_in.and.se_av_in.and.sw_av_in) then
                  couin0(i,j,1)=1.d0
               endif

               !-------------------------------------------------------------------------------
               !si la cellule n'est pas entierement a l'interieur de la particule
               !-------------------------------------------------------------------------------
               if (couin0(i,j,1).lt.1.d0) then
                  if  ( ne_av_in .or. nw_av_in .or. se_av_in .or. sw_av_in ) then
                     !-------------------------------------------------------------------------------
                     !     version ibm
                     !-------------------------------------------------------------------------------
                     nb_pt_fictifs_in_particle=0
                     do ij=1,total_pt_fictifs
                        do ii=1,total_pt_fictifs
                           pt_fictif(1)=xxb+dfloat(ii-1)*(xxf-xxb)/(dfloat(total_pt_fictifs)+1D-40)+&
                                                         0.5d0*(xxf-xxb)/(dfloat(total_pt_fictifs)+1D-40)
                           pt_fictif(2)=yyb+dfloat(ij-1)*(yyf-yyb)/(dfloat(total_pt_fictifs)+1D-40)+&
                                                         0.5d0*(yyf-yyb)/(dfloat(total_pt_fictifs)+1D-40)
                           pt_fictif_in=.false.
                           if (pt_fictif(1)<WallPos(1)) pt_fictif_in=.true.
                           if (pt_fictif_in) nb_pt_fictifs_in_particle=nb_pt_fictifs_in_particle+1
                        enddo
                     enddo
                     couin0(i,j,1)=couin0(i,j,1)+dfloat(nb_pt_fictifs_in_particle)/(dfloat(total_pt_fictifs)**ndim+1D-40)
                     if (couin0(i,j,1) .gt. 1.d0) couin0(i,j,1)=1.d0
                  endif
               endif
            end do
         end do
      else
         do k=sz,ez
            do j=sy,ey
               do i=sx,ex
                  if (grid_x(i)>0d0.or.grid_y(j)>=0d0.or.grid_z(k)>=0d0) then 
                     !-------------------------------------------------------------------------------
                     !construction de la cellule centree en lpt
                     !-------------------------------------------------------------------------------
                     xxb=grid_xu(i);xxf=grid_xu(i+1)
                     yyb=grid_yv(j);yyf=grid_yv(j+1)
                     zzb=grid_zw(k);zzf=grid_zw(k+1)
                     ne_av(1) =xxf;ne_av(2) =yyf;ne_av(3) =zzf;nw_av(1) =xxb;nw_av(2) =yyf;nw_av(3) =zzf 
                     se_av(1) =xxf;se_av(2) =yyb;se_av(3) =zzf;sw_av(1) =xxb;sw_av(2) =yyb;sw_av(3) =zzf 
                     ne_der(1)=xxf;ne_der(2)=yyf;ne_der(3)=zzb;nw_der(1)=xxb;nw_der(2)=yyf;nw_der(3)=zzb 
                     se_der(1)=xxf;se_der(2)=yyb;se_der(3)=zzb;sw_der(1)=xxb;sw_der(2)=yyb;sw_der(3)=zzb

                     !-------------------------------------------------------------------------------
                     !tester chacun des coins de la cellule (in ou out de la particule)
                     !-------------------------------------------------------------------------------
                     ne_av_in=.false.
                     nw_av_in=.false.
                     if (ne_av(1)<=WallPos(1)) ne_av_in =.true.
                     if (nw_av(1)<=WallPos(1)) nw_av_in =.true.
                     !-------------------------------------------------------------------------------
                     !si tous les coins de la cellule sont a l'interieur de la particule
                     !-------------------------------------------------------------------------------
                     if (ne_av_in.and.nw_av_in) then
                        couin0(i,j,k)=1.d0
                     endif
                     !-------------------------------------------------------------------------------
                     !si la cellule n'est pas entierement a l'interieur de la particule
                     !-------------------------------------------------------------------------------
                     if (couin0(i,j,k).lt.1.d0) then
                        if  (ne_av_in.or.nw_av_in) then
                           !-------------------------------------------------------------------------------
                           !     version ibm
                           !-------------------------------------------------------------------------------
                           nb_pt_fictifs_in_particle=0
                           do ik=1,total_pt_fictifs
                              do ij=1,total_pt_fictifs
                                 do ii=1,total_pt_fictifs
                                    pt_fictif(1)=xxb+dfloat(ii-1)*(xxf-xxb)/(dfloat(total_pt_fictifs)+1D-40)+&
                                                            0.5d0*(xxf-xxb)/(dfloat(total_pt_fictifs)+1D-40)
                                    pt_fictif(2)=yyb+dfloat(ij-1)*(yyf-yyb)/(dfloat(total_pt_fictifs)+1D-40)+&
                                                            0.5d0*(yyf-yyb)/(dfloat(total_pt_fictifs)+1D-40)
                                    pt_fictif(3)=zzb+dfloat(ik-1)*(zzf-zzb)/(dfloat(total_pt_fictifs)+1D-40)+&
                                                            0.5d0*(zzf-zzb)/(dfloat(total_pt_fictifs)+1D-40)
                                    pt_fictif_in=.false.
                                    if (pt_fictif(1)<WallPos(1)) pt_fictif_in=.true.
                                    if (pt_fictif_in) nb_pt_fictifs_in_particle=nb_pt_fictifs_in_particle+1
                                 enddo
                              enddo
                           enddo
                           couin0(i,j,k)=couin0(i,j,k)+dfloat(nb_pt_fictifs_in_particle)/(dfloat(total_pt_fictifs)**ndim+1D-40)
                           if (couin0(i,j,k).gt.1d0) couin0(i,j,k)=1d0
                        endif
                     endif
                  end if 
               end do
            enddo
         enddo
      end if
      if (ndim==2) then
         do j=sy,ey
            do i=sx,ex
               !-------------------------------------------------------------------------------
               !construction de la cellule centree en lpt
               !-------------------------------------------------------------------------------
               xxb=grid_x(i-1);xxf=grid_x(i)
               yyb=grid_y(j-1);yyf=grid_y(j)
               ne_av(1)=xxf;ne_av(2)=yyf;nw_av(1)=xxf;nw_av(2)=yyf
               se_av(1)=xxf;se_av(2)=yyb;sw_av(1)=xxf;sw_av(2)=yyb
               !-------------------------------------------------------------------------------
               !tester chacun des coins de la cellule (in ou out de la particule)
               !-------------------------------------------------------------------------------
               ne_av_in=.false.;nw_av_in=.false.
               if (ne_av(1)<=WallPos(1)) ne_av_in=.true.
               if (nw_av(1)<=WallPos(1)) nw_av_in=.true.
               !-------------------------------------------------------------------------------
               !si tous les coins de la cellule sont a l'interieur de la particule
               !-------------------------------------------------------------------------------
               if (ne_av_in.and.nw_av_in) then
                  cou_visin(i,j,1,1)=1.d0
               endif

               !-------------------------------------------------------------------------------
               !si la cellule n'est pas entierement a l'interieur de la particule
               !-------------------------------------------------------------------------------
               if (cou_visin(i,j,1,1).lt.1.d0) then
                  if  (ne_av_in.or.nw_av_in.or.se_av_in.or.sw_av_in) then
                     !-------------------------------------------------------------------------------
                     !     version ibm
                     !-------------------------------------------------------------------------------
                     nb_pt_fictifs_in_particle=0
                     do ij=1,total_pt_fictifs
                        do ii=1,total_pt_fictifs
                           pt_fictif(1)=xxb+dfloat(ii-1)*(xxf-xxb)/(dfloat(total_pt_fictifs)+1D-40)+&
                                                         0.5d0*(xxf-xxb)/(dfloat(total_pt_fictifs)+1D-40)
                           pt_fictif(2)=yyb+dfloat(ij-1)*(yyf-yyb)/(dfloat(total_pt_fictifs)+1D-40)+&
                                                         0.5d0*(yyf-yyb)/(dfloat(total_pt_fictifs)+1D-40)
                           pt_fictif_in=.false.
                           if (pt_fictif(1)<WallPos(1)) pt_fictif_in=.true.
                           if (pt_fictif_in) nb_pt_fictifs_in_particle=nb_pt_fictifs_in_particle+1
                        enddo
                     enddo
                     cou_visin(i,j,1,1)=cou_visin(i,j,1,1)+dfloat(nb_pt_fictifs_in_particle)/(dfloat(total_pt_fictifs)**ndim+1D-40)
                     if (cou_visin(i,j,1,1) .gt. 1.d0) cou_visin(i,j,1,1)=1.d0
                  endif
               endif
            end do
         end do
      else
         do k=sz,ez
            do j=sy,ey
               do i=sx,ex
                  !-------------------------------------------------------------------------------
                  !construction de la cellule centree en lpt
                  !-------------------------------------------------------------------------------
                  xxb=grid_x(i-1) ;xxf=grid_x(i)
                  yyb=grid_y(j-1) ;yyf=grid_y(j)
                  zzb=grid_zw(k)  ;zzf=grid_zw(k+1)
                  ne_av(1) =xxb;ne_av(2) =yyf;ne_av(3) =zzh;nw_av(1) =xxf;nw_av(2) =yyf;nw_av(3) =zzh 
                  se_av(1) =xxb;se_av(2) =yyd;se_av(3) =zzh;sw_av(1) =xxf;sw_av(2) =yyd;sw_av(3) =zzh 
                  ne_der(1)=xxb;ne_der(2)=yyf;ne_der(3)=zzb;nw_der(1)=xxf;nw_der(2)=yyf;nw_der(3)=zzb 
                  se_der(1)=xxb;se_der(2)=yyd;se_der(3)=zzb;sw_der(1)=xxf;sw_der(2)=yyd;sw_der(3)=zzb

                  !-------------------------------------------------------------------------------
                  !tester chacun des coins de la cellule (in ou out de la particule)
                  !-------------------------------------------------------------------------------
                  ne_av_in=.false.;nw_av_in=.false.
                  if (ne_av(1)<=WallPos(1)) ne_av_in =.true.
                  if (nw_av(1)<=WallPos(1)) nw_av_in =.true.
                  !-------------------------------------------------------------------------------
                  !si tous les coins de la cellule sont a l'interieur de la particule
                  !-------------------------------------------------------------------------------
                  if (ne_av_in.and.nw_av_in) then
                     cou_visin(i,j,k,3)=1.d0
                  endif

                  !-------------------------------------------------------------------------------
                  !si la cellule n'est pas entierement a l'interieur de la particule
                  !-------------------------------------------------------------------------------
                  if (cou_visin(i,j,k,3).lt.1.d0) then
                     if  (ne_av_in.or.nw_av_in) then
                        !-------------------------------------------------------------------------------
                        !     version ibm
                        !-------------------------------------------------------------------------------
                        nb_pt_fictifs_in_particle=0
                        do ik=1,total_pt_fictifs
                           do ij=1,total_pt_fictifs
                              do ii=1,total_pt_fictifs
                                 pt_fictif(1)=xxb+dfloat(ii-1)*(xxf-xxb)/(dfloat(total_pt_fictifs)+1D-40)+&
                                                         0.5d0*(xxf-xxb)/(dfloat(total_pt_fictifs)+1D-40)
                                 pt_fictif(2)=yyb+dfloat(ij-1)*(yyf-yyb)/(dfloat(total_pt_fictifs)+1D-40)+&
                                                         0.5d0*(yyf-yyb)/(dfloat(total_pt_fictifs)+1D-40)
                                 pt_fictif(3)=zzb+dfloat(ik-1)*(zzf-zzb)/(dfloat(total_pt_fictifs)+1D-40)+&
                                                         0.5d0*(zzf-zzb)/(dfloat(total_pt_fictifs)+1D-40)
                                 pt_fictif_in=.false.
                                 if (pt_fictif(1)<WallPos(1)) pt_fictif_in=.true.
                                 if (pt_fictif_in) nb_pt_fictifs_in_particle=nb_pt_fictifs_in_particle+1
                              enddo
                           enddo
                        enddo
                        cou_visin(i,j,k,3)=cou_visin(i,j,k,3)+dfloat(nb_pt_fictifs_in_particle)/(dfloat(total_pt_fictifs)**ndim+1D-40)
                        if (cou_visin(i,j,k,3) .gt. 1.d0) cou_visin(i,j,k,3)=1.d0
                     endif
                  endif
               end do
            enddo
         enddo
         cou_visin(:,:,:,2)=cou_visin(:,:,:,3)
         do k=sz,ez
            do j=sy,ey
               do i=sx,ex
                  !-------------------------------------------------------------------------------
                  !construction de la cellule centree en lpt
                  !-------------------------------------------------------------------------------
                  xxb=grid_xu(i) ;xxf=grid_xu(i+1)
                  yyb=grid_y(j-1);yyf=grid_y(j)
                  zzb=grid_z(k-1);zzf=grid_z(k)
                  ne_av(1) =xxb;ne_av(2) =yyf;ne_av(3) =zzh;nw_av(1) =xxf;nw_av(2) =yyf;nw_av(3) =zzh 
                  se_av(1) =xxb;se_av(2) =yyd;se_av(3) =zzh;sw_av(1) =xxf;sw_av(2) =yyd;sw_av(3) =zzh 
                  ne_der(1)=xxb;ne_der(2)=yyf;ne_der(3)=zzb;nw_der(1)=xxf;nw_der(2)=yyf;nw_der(3)=zzb 
                  se_der(1)=xxb;se_der(2)=yyd;se_der(3)=zzb;sw_der(1)=xxf;sw_der(2)=yyd;sw_der(3)=zzb

                  !-------------------------------------------------------------------------------
                  !tester chacun des coins de la cellule (in ou out de la particule)
                  !-------------------------------------------------------------------------------
                  ne_av_in=.false.;nw_av_in=.false.
                  if (ne_av(1)<=WallPos(1)) ne_av_in =.true.
                  if (nw_av(1)<=WallPos(1)) nw_av_in =.true.
                  !-------------------------------------------------------------------------------
                  !si tous les coins de la cellule sont a l'interieur de la particule
                  !-------------------------------------------------------------------------------
                  if (ne_av_in.and.nw_av_in) then
                     cou_visin(i,j,k,1)=1.d0
                  endif

                  !-------------------------------------------------------------------------------
                  !si la cellule n'est pas entierement a l'interieur de la particule
                  !-------------------------------------------------------------------------------
                  if (cou_visin(i,j,k,1).lt.1.d0) then
                     if  (ne_av_in.or.nw_av_in) then
                        !-------------------------------------------------------------------------------
                        !     version ibm
                        !-------------------------------------------------------------------------------
                        nb_pt_fictifs_in_particle=0
                        do ik=1,total_pt_fictifs
                           do ij=1,total_pt_fictifs
                              do ii=1,total_pt_fictifs
                                 pt_fictif(1)=xxb+dfloat(ii-1)*(xxf-xxb)/(dfloat(total_pt_fictifs)+1D-40)+&
                                                         0.5d0*(xxf-xxb)/(dfloat(total_pt_fictifs)+1D-40)
                                 pt_fictif(2)=yyb+dfloat(ij-1)*(yyf-yyb)/(dfloat(total_pt_fictifs)+1D-40)+&
                                                         0.5d0*(yyf-yyb)/(dfloat(total_pt_fictifs)+1D-40)
                                 pt_fictif(3)=zzb+dfloat(ik-1)*(zzf-zzb)/(dfloat(total_pt_fictifs)+1D-40)+&
                                                         0.5d0*(zzf-zzb)/(dfloat(total_pt_fictifs)+1D-40)
                                 pt_fictif_in=.false.
                                 if (pt_fictif(1)<WallPos(1)) pt_fictif_in=.true.
                                 if (pt_fictif_in) nb_pt_fictifs_in_particle=nb_pt_fictifs_in_particle+1
                              enddo
                           enddo
                        enddo
                        cou_visin(i,j,k,1)=cou_visin(i,j,k,1)+dfloat(nb_pt_fictifs_in_particle)/(dfloat(total_pt_fictifs)**ndim+1D-40)
                        if (cou_visin(i,j,k,1) .gt. 1.d0) cou_visin(i,j,k,1)=1.d0
                     endif
                  endif
               end do
            enddo
         enddo
      end if
      if (ndim==2) then
         do j=syu,eyu
            do i=sxu,exu
               !-------------------------------------------------------------------------------
               !construction de la cellule centree en lpt
               !-------------------------------------------------------------------------------
               xxb=grid_x(i-1);xxf=grid_x(i)
               yyb=grid_yv(j) ;yyf=grid_yv(j+1)
               ne_av(1)=xxb;ne_av(2)=zzh;nw_av(1)=xxf;nw_av(2)=zzh
               se_av(1)=xxb;se_av(2)=zzb;sw_av(1)=xxf;sw_av(2)=zzb
               !-------------------------------------------------------------------------------
               !tester chacun des coins de la cellule (in ou out de la particule)
               !-------------------------------------------------------------------------------
               ne_av_in=.false.;nw_av_in=.false.
               if (ne_av(1)<=WallPos(1)) ne_av_in=.true.
               if (nw_av(1)<=WallPos(1)) nw_av_in=.true.
               !-------------------------------------------------------------------------------
               !si tous les coins de la cellule sont a l'interieur de la particule
               !-------------------------------------------------------------------------------
               if (ne_av_in.and.nw_av_in) then
                  cou_vtsin(i,j,1,1)=1.d0
               endif

               !-------------------------------------------------------------------------------
               !si la cellule n'est pas entierement a l'interieur de la particule
               !-------------------------------------------------------------------------------
               if (cou_vtsin(i,j,1,1).lt.1.d0) then
                  if  (ne_av_in.or.nw_av_in) then
                     !-------------------------------------------------------------------------------
                     !     version ibm
                     !-------------------------------------------------------------------------------
                     nb_pt_fictifs_in_particle=0
                     do ij=1,total_pt_fictifs
                        do ii=1,total_pt_fictifs
                           pt_fictif(1)=xxb+dfloat(ii-1)*(xxf-xxb)/(dfloat(total_pt_fictifs)+1D-40)+&
                                                         0.5d0*(xxf-xxb)/(dfloat(total_pt_fictifs)+1D-40)
                           pt_fictif(2)=yyb+dfloat(ij-1)*(yyf-yyb)/(dfloat(total_pt_fictifs)+1D-40)+&
                                                         0.5d0*(yyf-yyb)/(dfloat(total_pt_fictifs)+1D-40)
                           pt_fictif_in=.false.
                           if (pt_fictif(1)<WallPos(1)) pt_fictif_in=.true.
                           if (pt_fictif_in) nb_pt_fictifs_in_particle=nb_pt_fictifs_in_particle+1
                        enddo
                     enddo
                     cou_vtsin(i,j,1,1)=cou_vtsin(i,j,1,1)+dfloat(nb_pt_fictifs_in_particle)/(dfloat(total_pt_fictifs)**ndim+1D-40)
                     if (cou_vtsin(i,j,1,1).gt.1.d0) cou_vtsin(i,j,1,1)=1.d0
                  endif
               endif
            end do
         end do
         do j=syv,eyv
            do i=sxv,exv
               !-------------------------------------------------------------------------------
               !construction de la cellule centree en lpt
               !-------------------------------------------------------------------------------
               xxb=grid_xu(i) ;xxf=grid_xu(i+1)
               yyb=grid_y(j-1);yyf=grid_y(j)
               ne_av(1)=xxd;ne_av(2)=yyf;nw_av(1)=xxf;nw_av(2)=yyf
               se_av(1)=xxd;se_av(2)=yyb;sw_av(1)=xxf;sw_av(2)=yyb
               !-------------------------------------------------------------------------------
               !tester chacun des coins de la cellule (in ou out de la particule)
               !-------------------------------------------------------------------------------
               ne_av_in=.false.;nw_av_in=.false.
               if (ne_av(1)<=WallPos(1).or.ne_av(2)<=WallPos(2)) ne_av_in=.true.
               if (nw_av(1)<=WallPos(1).or.nw_av(2)<=WallPos(2)) nw_av_in=.true.
               !-------------------------------------------------------------------------------
               !si tous les coins de la cellule sont a l'interieur de la particule
               !-------------------------------------------------------------------------------
               if (ne_av_in.and.nw_av_in) then
                  cou_vtsin(i,j,1,2)=1.d0
               endif

               !-------------------------------------------------------------------------------
               !si la cellule n'est pas entierement a l'interieur de la particule
               !-------------------------------------------------------------------------------
               if (cou_vtsin(i,j,1,2).lt.1.d0) then
                  if  (ne_av_in.or.nw_av_in) then
                     !-------------------------------------------------------------------------------
                     !     version ibm
                     !-------------------------------------------------------------------------------
                     nb_pt_fictifs_in_particle=0
                     do ij=1,total_pt_fictifs
                        do ii=1,total_pt_fictifs
                           pt_fictif(1)=xxb+dfloat(ii-1)*(xxf-xxb)/(dfloat(total_pt_fictifs)+1D-40)+&
                                                         0.5d0*(xxf-xxb)/(dfloat(total_pt_fictifs)+1D-40)
                           pt_fictif(2)=yyb+dfloat(ij-1)*(yyf-yyb)/(dfloat(total_pt_fictifs)+1D-40)+&
                                                         0.5d0*(yyf-yyb)/(dfloat(total_pt_fictifs)+1D-40)
                           pt_fictif_in=.false.
                           if (pt_fictif(1)<WallPos(1)) pt_fictif_in=.true.
                           if (pt_fictif_in) nb_pt_fictifs_in_particle=nb_pt_fictifs_in_particle+1
                        enddo
                     enddo
                     cou_vtsin(i,j,1,2)=cou_vtsin(i,j,1,2)+dfloat(nb_pt_fictifs_in_particle)/(dfloat(total_pt_fictifs)**ndim+1D-40)
                     if (cou_vtsin(i,j,1,2) .gt. 1.d0) cou_vtsin(i,j,1,2)=1.d0
                  endif
               endif
            end do
         end do
      else
         do k=szu,ezu
            do j=syu,eyu
               do i=sxu,exu
                  !-------------------------------------------------------------------------------
                  !construction de la cellule centree en lpt
                  !-------------------------------------------------------------------------------
                  xxb=grid_x(i-1);xxf=grid_x(i)
                  yyb=grid_yv(j) ;yyf=grid_yv(j+1)
                  zzb=grid_zw(k) ;zzf=grid_zw(k+1)
                  ne_av(1) =xxb;ne_av(2) =yyf;ne_av(3) =zzf;nw_av(1) =xxf;nw_av(2) =yyf;nw_av(3) =zzf 
                  se_av(1) =xxb;se_av(2) =yyb;se_av(3) =zzf;sw_av(1) =xxf;sw_av(2) =yyb;sw_av(3) =zzf 
                  ne_der(1)=xxb;ne_der(2)=yyf;ne_der(3)=zzb;nw_der(1)=xxf;nw_der(2)=yyf;nw_der(3)=zzb 
                  se_der(1)=xxb;se_der(2)=yyb;se_der(3)=zzb;sw_der(1)=xxf;sw_der(2)=yyb;sw_der(3)=zzb

                  !-------------------------------------------------------------------------------
                  !tester chacun des coins de la cellule (in ou out de la particule)
                  !-------------------------------------------------------------------------------
                  ne_av_in=.false.;nw_av_in=.false.
                  if (ne_av(1) <=WallPos(1)) ne_av_in =.true.
                  if (nw_av(1) <=WallPos(1)) nw_av_in =.true.

                  !-------------------------------------------------------------------------------
                  !si tous les coins de la cellule sont a l'interieur de la particule
                  !-------------------------------------------------------------------------------
                  if (ne_av_in.and.nw_av_in) then
                     cou_vtsin(i,j,k,1)=1.d0
                  endif

                  !-------------------------------------------------------------------------------
                  !si la cellule n'est pas entierement a l'interieur de la particule
                  !-------------------------------------------------------------------------------
                  if (cou_vtsin(i,j,k,1).lt.1.d0) then
                     if  (ne_av_in.or.nw_av_in) then
                        !-------------------------------------------------------------------------------
                        !     version ibm
                        !-------------------------------------------------------------------------------
                        nb_pt_fictifs_in_particle=0
                        do ik=1,total_pt_fictifs
                           do ij=1,total_pt_fictifs
                              do ii=1,total_pt_fictifs
                                 pt_fictif(1)=xxb+dfloat(ii-1)*(xxf-xxb)/(dfloat(total_pt_fictifs)+1D-40)+&
                                                         0.5d0*(xxf-xxb)/(dfloat(total_pt_fictifs)+1D-40)
                                 pt_fictif(2)=yyb+dfloat(ij-1)*(yyf-yyb)/(dfloat(total_pt_fictifs)+1D-40)+&
                                                         0.5d0*(yyf-yyb)/(dfloat(total_pt_fictifs)+1D-40)
                                 pt_fictif(3)=zzb+dfloat(ik-1)*(zzf-zzb)/(dfloat(total_pt_fictifs)+1D-40)+&
                                                         0.5d0*(zzf-zzb)/(dfloat(total_pt_fictifs)+1D-40)
                                 pt_fictif_in=.false.
                                 if (pt_fictif(1)<WallPos(1)) pt_fictif_in=.true.
                                 if (pt_fictif_in) nb_pt_fictifs_in_particle=nb_pt_fictifs_in_particle+1
                              enddo
                           enddo
                        enddo
                        cou_vtsin(i,j,k,1)=cou_vtsin(i,j,k,1)+dfloat(nb_pt_fictifs_in_particle)/(dfloat(total_pt_fictifs)**ndim+1D-40)
                        if (cou_vtsin(i,j,k,1) .gt. 1.d0) cou_vtsin(i,j,k,1)=1.d0
                     endif
                  endif
               end do
            enddo
         enddo
         do k=szv,ezv
            do j=syv,eyv
               do i=sxv,exv
                  !-------------------------------------------------------------------------------
                  !construction de la cellule centree en lpt
                  !-------------------------------------------------------------------------------
                  xxb=grid_xu(i) ;xxf=grid_x(i+1)
                  yyb=grid_y(j-1);yyf=grid_y(j)
                  zzb=grid_zw(k) ;zzf=grid_z(k+1)
                  ne_av(1) =xxb;ne_av(2) =yyf;ne_av(3) =zzf;nw_av(1) =xxf;nw_av(2) =yyf;nw_av(3) =zzf 
                  se_av(1) =xxb;se_av(2) =yyb;se_av(3) =zzf;sw_av(1) =xxf;sw_av(2) =yyb;sw_av(3) =zzf 
                  ne_der(1)=xxb;ne_der(2)=yyf;ne_der(3)=zzb;nw_der(1)=xxf;nw_der(2)=yyf;nw_der(3)=zzb 
                  se_der(1)=xxb;se_der(2)=yyb;se_der(3)=zzb;sw_der(1)=xxf;sw_der(2)=yyb;sw_der(3)=zzb

                  !-------------------------------------------------------------------------------
                  !tester chacun des coins de la cellule (in ou out de la particule)
                  !-------------------------------------------------------------------------------
                  ne_av_in=.false.;nw_av_in=.false.
                  if (ne_av(1) <=WallPos(1)) ne_av_in =.true.
                  if (nw_av(1) <=WallPos(1)) nw_av_in =.true.                  
                  !-------------------------------------------------------------------------------
                  !si tous les coins de la cellule sont a l'interieur de la particule
                  !-------------------------------------------------------------------------------
                  if (ne_av_in.and.nw_av_in) then
                     cou_vtsin(i,j,k,2)=1.d0
                  endif

                  !-------------------------------------------------------------------------------
                  !si la cellule n'est pas entierement a l'interieur de la particule
                  !-------------------------------------------------------------------------------
                  if (cou_vtsin(i,j,k,2).lt.1.d0) then
                     if  (ne_av_in.or.nw_av_in) then
                        !-------------------------------------------------------------------------------
                        !     version ibm
                        !-------------------------------------------------------------------------------
                        nb_pt_fictifs_in_particle=0
                        do ik=1,total_pt_fictifs
                           do ij=1,total_pt_fictifs
                              do ii=1,total_pt_fictifs
                                 pt_fictif(1)=xxb+dfloat(ii-1)*(xxf-xxb)/(dfloat(total_pt_fictifs)+1D-40)+&
                                                         0.5d0*(xxf-xxb)/(dfloat(total_pt_fictifs)+1D-40)
                                 pt_fictif(2)=yyb+dfloat(ij-1)*(yyf-yyb)/(dfloat(total_pt_fictifs)+1D-40)+&
                                                         0.5d0*(yyf-yyb)/(dfloat(total_pt_fictifs)+1D-40)
                                 pt_fictif(3)=zzb+dfloat(ik-1)*(zzf-zzb)/(dfloat(total_pt_fictifs)+1D-40)+&
                                                         0.5d0*(zzf-zzb)/(dfloat(total_pt_fictifs)+1D-40)
                                 pt_fictif_in=.false.
                                 if (pt_fictif(1)<WallPos(1)) pt_fictif_in=.true.
                                 if (pt_fictif_in) nb_pt_fictifs_in_particle=nb_pt_fictifs_in_particle+1
                              enddo
                           enddo
                        enddo
                        cou_vtsin(i,j,k,2)=cou_vtsin(i,j,k,2)+dfloat(nb_pt_fictifs_in_particle)/(dfloat(total_pt_fictifs)**ndim+1D-40)
                        if (cou_vtsin(i,j,k,2) .gt. 1.d0) cou_vtsin(i,j,k,2)=1.d0
                     endif
                  endif
               end do
            enddo
         enddo
         cou_vtsin(i,j,k,3)=cou_vtsin(i,j,k,2)
         ! do k=szw,ezw
         !    do j=syw,eyw
         !       do i=sxw,exw
         !          !-------------------------------------------------------------------------------
         !          !construction de la cellule centree en lpt
         !          !-------------------------------------------------------------------------------
         !          xxb=grid_xu(i) ;xxf=grid_xu(i+1)
         !          yyb=grid_yv(j) ;yyf=grid_yv(j+1)
         !          zzb=grid_z(k-1);zzf=grid_z(k)
         !          ne_av(1) =xxb;ne_av(2) =yyf;ne_av(3) =zzf;nw_av(1) =xxf;nw_av(2) =yyf;nw_av(3) =zzf 
         !          se_av(1) =xxb;se_av(2) =yyb;se_av(3) =zzf;sw_av(1) =xxf;sw_av(2) =yyb;sw_av(3) =zzf 
         !          ne_der(1)=xxb;ne_der(2)=yyf;ne_der(3)=zzb;nw_der(1)=xxf;nw_der(2)=yyf;nw_der(3)=zzb 
         !          se_der(1)=xxb;se_der(2)=yyb;se_der(3)=zzb;sw_der(1)=xxf;sw_der(2)=yyb;sw_der(3)=zzb

         !          !-------------------------------------------------------------------------------
         !          !tester chacun des coins de la cellule (in ou out de la particule)
         !          !-------------------------------------------------------------------------------
         !          ne_av_in=.false.;se_av_in=.false.;nw_av_in=.false.;sw_av_in=.false.
         !          if (ne_av(1) <=WallPos(1).or.ne_av(2) <=WallPos(2).or.ne_av(3) <=WallPos(3)) ne_av_in =.true.
         !          if (se_av(1) <=WallPos(1).or.se_av(2) <=WallPos(2).or.se_av(3) <=WallPos(3)) se_av_in =.true.
         !          if (nw_av(1) <=WallPos(1).or.nw_av(2) <=WallPos(2).or.nw_av(3) <=WallPos(3)) nw_av_in =.true.
         !          if (sw_av(1) <=WallPos(1).or.sw_av(2) <=WallPos(2).or.sw_av(3) <=WallPos(3)) sw_av_in =.true.
         !          if (ne_der(1)<=WallPos(1).or.ne_der(2)<=WallPos(2).or.ne_der(3)<=WallPos(3)) ne_der_in=.true.
         !          if (se_der(1)<=WallPos(1).or.se_der(2)<=WallPos(2).or.se_der(3)<=WallPos(3)) se_der_in=.true.
         !          if (nw_der(1)<=WallPos(1).or.nw_der(2)<=WallPos(2).or.nw_der(3)<=WallPos(3)) nw_der_in=.true.
         !          if (sw_der(1)<=WallPos(1).or.sw_der(2)<=WallPos(2).or.sw_der(3)<=WallPos(3)) sw_der_in=.true.

         !          !-------------------------------------------------------------------------------
         !          !si tous les coins de la cellule sont a l'interieur de la particule
         !          !-------------------------------------------------------------------------------
         !          if (ne_av_in.or.nw_av_in.or.se_av_in.or.sw_av_in.or.&
         !                ne_der_in.or.nw_der_in.or.se_der_in.or.sw_der_in) then
         !             cou_vtsin(i,j,k,3)=1.d0
         !          endif

         !          !-------------------------------------------------------------------------------
         !          !si la cellule n'est pas entierement a l'interieur de la particule
         !          !-------------------------------------------------------------------------------
         !          if (cou_vtsin(i,j,k,3).lt.1.d0) then
         !             if  (ne_av_in.or.nw_av_in.or.se_av_in.or.sw_av_in.or.&
         !                   ne_der_in.or.nw_der_in.or.se_der_in.or.sw_der_in) then
         !                !-------------------------------------------------------------------------------
         !                !     version ibm
         !                !-------------------------------------------------------------------------------
         !                nb_pt_fictifs_in_particle=0
         !                do ik=1,total_pt_fictifs
         !                   do ij=1,total_pt_fictifs
         !                      do ii=1,total_pt_fictifs
         !                         pt_fictif(1)=xxb+dfloat(ii-1)*(xxf-xxb)/(dfloat(total_pt_fictifs)+1D-40)+&
         !                                                 0.5d0*(xxf-xxb)/(dfloat(total_pt_fictifs)+1D-40)
         !                         pt_fictif(2)=yyb+dfloat(ij-1)*(yyf-yyb)/(dfloat(total_pt_fictifs)+1D-40)+&
         !                                                 0.5d0*(yyf-yyb)/(dfloat(total_pt_fictifs)+1D-40)
         !                         pt_fictif(3)=zzb+dfloat(ik-1)*(zzf-zzb)/(dfloat(total_pt_fictifs)+1D-40)+&
         !                                                 0.5d0*(zzf-zzb)/(dfloat(total_pt_fictifs)+1D-40)
         !                         if (pt_fictif(1)<=WallPos(1).or.pt_fictif(2)<=WallPos(2).or.pt_fictif(3)<=WallPos(3)) pt_fictif_in=.true.
         !                         if (pt_fictif_in) nb_pt_fictifs_in_particle=nb_pt_fictifs_in_particle+1
         !                      enddo
         !                   enddo
         !                enddo
         !                cou_vtsin(i,j,k,3)=cou_vtsin(i,j,k,3)+dfloat(nb_pt_fictifs_in_particle)/(dfloat(total_pt_fictifs)**ndim+1D-40)
         !                if (cou_vtsin(i,j,k,3) .gt. 1.d0) cou_vtsin(i,j,k,3)=1.d0
         !             endif
         !          endif
         !       end do
         !    enddo
         !enddo
      end if

      !-------------------------------------------------------------------------------
      ! diffusion de cou a tous les procs 
      !-------------------------------------------------------------------------------
      if (nproc>1) then
        call comm_mpi_sca(couin0)
        allocate(cou_vis_tmp(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
        do nd=1,3
           cou_vis_tmp(:,:,:)=cou_visin(:,:,:,nd)
           call comm_mpi_sca(cou_vis_tmp)
           cou_visin(:,:,:,nd)=cou_vis_tmp(:,:,:)
        end do 
        do nd=1,3
           cou_vis_tmp(:,:,:)=cou_vtsin(:,:,:,nd)
           call comm_mpi_sca(cou_vis_tmp)
           cou_vtsin(:,:,:,nd)=cou_vis_tmp(:,:,:)
        end do
        deallocate(cou_vis_tmp)
      end if
      ! sum1   =0d0 
      ! sumvis1=0d0 
      ! sumvis3=0d0 
      ! sumvts1=0d0 
      ! sumvts2=0d0
      ! nb=0
      ! if (ndim==3) then 
      !    do i=sx,ex 
      !       do j=sy,ey
      !          do k=sz,ez
      !             sum1=sum1+couin0(i,j,k)*(grid_xu(i+1)-grid_xu(i))*(grid_yv(j+1)-grid_yv(j))*&
      !                                  (grid_zw(k+1)-grid_zw(k))
      !             sumvis3=sumvis3+cou_visin(i,j,k,3)*(grid_x(i)-grid_x(i-1))*(grid_y(j)-grid_y(j-1))*&
      !                                  (grid_zw(k+1)-grid_zw(k))
      !             sumvis1=sumvis1+cou_visin(i,j,k,1)*(grid_xu(i+1)-grid_xu(i))*(grid_y(j)-grid_y(j-1))*&
      !                                  (grid_z(k)-grid_z(k-1))
      !             sumvts1=sumvts1+cou_vtsin(i,j,k,1)*(grid_x(i)-grid_x(i-1))*(grid_yv(j+1)-grid_yv(j))*&
      !                                  (grid_zw(k+1)-grid_zw(k))
      !             sumvts2=sumvts2+cou_vtsin(i,j,k,2)*(grid_xu(i+1)-grid_xu(i))*(grid_y(j)-grid_y(j-1))*&
      !                                  (grid_zw(k+1)-grid_zw(k))
      !          end do
      !       end do  
      !    end do 
      ! else 
      !    do i=sx,ex 
      !       do j=sy,ey
      !             sum1=sum1+couin0(i,j,1)*(grid_xu(i+1)-grid_xu(i))*(grid_yv(j+1)-grid_yv(j))
      !             if (couin0(i,j,1).ne.0d0 .and. couin0(i,j,1).ne.1d0 ) then
      !                print*, grid_x(i),grid_y(j),couin0(i,j,1), 'Hello'
      !             end if 
      !             if (couin0(i,j,1)==1d0) then
      !                nb=nb+1
      !                print*, grid_xu(i),grid_x(i),grid_xu(i+1),couin0(i,j,1), 'Cou = 1 '
      !             end if 
      !       end do  
      !    end do 
      ! end if 
      ! call MPI_BARRIER(comm3d,mpi_code)
      ! call MPI_REDUCE(sum1,sum0,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,comm3d,mpi_code) 
      ! call MPI_REDUCE(sumvis3,sumvis30,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,comm3d,mpi_code) 
      ! call MPI_REDUCE(sumvis1,sumvis10,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,comm3d,mpi_code) 
      ! call MPI_REDUCE(sumvts1,sumvts10,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,comm3d,mpi_code) 
      ! call MPI_REDUCE(sumvts2,sumvts20,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,comm3d,mpi_code) 
      ! call MPI_REDUCE(nb ,nb0 ,1,MPI_INTEGER,MPI_SUM,0,comm3d,mpi_code) 
      ! if (rank==0) then 
      !    print*, sum0, 'valeur numerique cou'
      !    print*, sumvis30, 'valeur numerique couvis3'
      !    print*, sumvis10, 'valeur numerique couvis1'
      !    print*, sumvts10, 'valeur numerique couvts1'
      !    print*, sumvts20, 'valeur numerique couvts2'
      !    if (ndim==2) print*, (WallPos(1)-grid_xu(gsxu))*(grid_yv(geyv)-grid_yv(gsyv)), 'valeur exact'
      !    if (ndim==3) print*, (WallPos(1)-grid_xu(gsxu))*(grid_yv(geyv)-grid_yv(gsyv))*(grid_zw(gezw)-grid_zw(gszw)), 'valeur exact'
      !    if (ndim==2) print*, 100*((WallPos(1)-grid_xu(gsxu))*(grid_yv(geyv)-grid_yv(gsyv))-sum0)/&
      !                         (WallPos(1)-grid_xu(gsxu))*(grid_yv(geyv)-grid_yv(gsyv)), 'Erreur relative'
      !    if (ndim==3) print*, 100*((WallPos(1)-grid_xu(gsxu))*(grid_yv(geyv)-grid_yv(gsyv))*&
      !                   (grid_zw(gezw)-grid_zw(gszw))-sum0)/(WallPos(1)*grid_y(gey)*grid_z(gez)), 'Erreur relative cou' 
      !    if (ndim==3) print*, 100*((WallPos(1)-grid_xu(gsxu))*(grid_yv(geyv)-grid_yv(gsyv))*&
      !                   (grid_zw(gezw)-grid_zw(gszw))-sumvis30)/(WallPos(1)*grid_y(gey)*grid_z(gez)), 'Erreur relative couvis3'
      !    if (ndim==3) print*, 100*((WallPos(1)-grid_xu(gsxu))*(grid_yv(geyv)-grid_yv(gsyv))*&
      !                   (grid_zw(gezw)-grid_zw(gszw))-sumvis10)/(WallPos(1)*grid_y(gey)*grid_z(gez)), 'Erreur relative couvis1' 
      !    if (ndim==3) print*, 100*((WallPos(1)-grid_xu(gsxu))*(grid_yv(geyv)-grid_yv(gsyv))*&
      !                   (grid_zw(gezw)-grid_zw(gszw))-sumvts10)/(WallPos(1)*grid_y(gey)*grid_z(gez)), 'Erreur relative couvts1' 
      !    if (ndim==3) print*, 100*((WallPos(1)-grid_xu(gsxu))*(grid_yv(geyv)-grid_yv(gsyv))*&
      !                   (grid_zw(gezw)-grid_zw(gszw))-sumvts20)/(WallPos(1)*grid_y(gey)*grid_z(gez)), 'Erreur relative couvts2' 
      ! end if 

      !-------------------------------------------------------------------------------
      if (deeptracking) write(*,*) 'sortie Wall_PhaseFunction'
      !-------------------------------------------------------------------------------
   end subroutine Wall_PhaseFunction

   !===============================================================================
end module Bib_VOFLag_Wall_PhaseFunction
!===============================================================================