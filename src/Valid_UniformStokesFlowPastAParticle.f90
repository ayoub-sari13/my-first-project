!===============================================================================
module Bib_VOFLag_Valid_UniformStokesFlowPastAParticle
  !===============================================================================
  contains
  !-----------------------------------------------------------------------------  
  !> @author  amine chadil
  ! 
  !> @brief calcul des champs erreur sur la pression et vitesse du fluide pour un ecoulement
  !! uniforme autour d'une sphere dans le regime de stokes
  !-----------------------------------------------------------------------------
  subroutine Valid_UniformStokesFlowPastAParticle(u,v,w,Ru0,Rv0,Rw0,pres,diff_pre,diff_u_mean,& 
                                                  diff_v_mean,diff_w_mean,U_inf,centre_part,&
                                                  rayon_part,iteration_temps)
    !===============================================================================
    !modules
    !===============================================================================
    !-------------------------------------------------------------------------------
    !Modules de definition des structures et variables => src/mod/module_...f90
    !-------------------------------------------------------------------------------
    use mod_Parameters, only : deeptracking,dim
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------
    !Modules de subroutines de la librairie VOF-Lag
    !-------------------------------------------------------------------------------
    use Bib_VOFLag_Valid_UniformStokesFlowPastAParticle_Sphere
    use Bib_VOFLag_Valid_UniformStokesFlowPastAParticle_Cylinder
    !-------------------------------------------------------------------------------
    
    !===============================================================================
    !declarations des variables
    !===============================================================================
    implicit none
    !-------------------------------------------------------------------------------
    !variables globales
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable, intent(inout) :: diff_pre,diff_u_mean,diff_v_mean,&
                                                             diff_w_mean
    real(8), dimension(:,:,:), allocatable, intent(in)    :: u,v,w,Ru0,Rv0,Rw0,pres
    real(8), dimension(3)                                 :: centre_part
    real(8)                                               :: rayon_part,U_inf
    integer                                               :: iteration_temps
    !-------------------------------------------------------------------------------
    
    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'entree Valid_UniformStokesFlowPastAParticle'
    !-------------------------------------------------------------------------------

    !validation_uniform_flow_past_part_analytique = .false.
    if (dim .eq. 2) then
       call Valid_UniformStokesFlowPastAParticle_Cylinder(u,v,Ru0,Rv0,pres,diff_pre,diff_u_mean,     &
                                                          diff_v_mean,centre_part,rayon_part,       &
                                                          iteration_temps)
    else
       call Valid_UniformStokesFlowPastAParticle_Sphere  (u,v,w,Ru0,Rv0,Rw0,pres,diff_pre,diff_u_mean,&
                                                          diff_v_mean,diff_w_mean,U_inf,centre_part,&
                                                          rayon_part,iteration_temps)
    end if

    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'sortie Valid_UniformStokesFlowPastAParticle'
    !-------------------------------------------------------------------------------
  end subroutine Valid_UniformStokesFlowPastAParticle

  !===============================================================================
end module Bib_VOFLag_Valid_UniformStokesFlowPastAParticle
!===============================================================================
  



