!===============================================================================
module Bib_VOFLag_ParticlesDataStructure_Impression_FileCreation
  !===============================================================================
  contains
  !---------------------------------------------------------------------------  
  !> @author jorge cesar brandle de motta, amine chadil
  ! 
  !> @brief cette routine ecrit la premiere ligne des fichiers contenant les donnees
  !!relatives a chaque particule 
  !
  !> @param[in] vl   : la structure contenant toutes les informations
  !! relatives aux particules 
  !> @param[in] na   : le nom du fichier a creer
  !---------------------------------------------------------------------------  
  subroutine ParticlesDataStructure_Impression_FileCreation(vl,na)
    !===============================================================================
    !modules
    !===============================================================================
    !-------------------------------------------------------------------------------
    !Modules de definition des structures et variables => src/mod/module_...f90
    !-------------------------------------------------------------------------------
    use Module_VOFLag_ParticlesDataStructure
    use mod_Parameters,                       only : deeptracking,dim
    !-------------------------------------------------------------------------------

    !===============================================================================
    !declarations des variables
    !===============================================================================
    implicit none
    !-------------------------------------------------------------------------------
    !variables globales
    !-------------------------------------------------------------------------------
    type(struct_vof_lag), intent(inout)  :: vl
    integer, intent(in)                  :: na
    !-------------------------------------------------------------------------------
    !variables locales 
    !-------------------------------------------------------------------------------
    integer                              :: np
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'entree ParticlesDataStructure_Impression_FileCreation'
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    !ouverture du fichier na
    !-------------------------------------------------------------------------------
    np=vl%affich(na)
    open(unit=1000+np,file=vl%filec(na),status='replace')

    !-------------------------------------------------------------------------------
    !ecrire la position de la particule a l'iteration nt
    !-------------------------------------------------------------------------------
    write(1000+np,'(a1,4a13)',advance='no') '#',' ----nt----- ',' ---temps--- ',&
         ' -----x----- ',' -----z----- '
    if (dim.eq.3) write(1000+np,'(a13)'    ,advance='no')     ' -----y----- '

    !-------------------------------------------------------------------------------
    !ecrire la vitesse de translation de la particule a l'iteration nt
    !-------------------------------------------------------------------------------
    if (vl%deplacement) then
       write(1000+np,'(2a13)'   ,advance='no')     ' ----vx----- ',' ----vz----- '
       if (dim.eq.3) write(1000+np,'(a13)'    ,advance='no')     ' ----vy----- '
    end if
    !-------------------------------------------------------------------------------
    !ecrire la vitesse angulaire de la particule a l'iteration nt
    !-------------------------------------------------------------------------------
    if (vl%vrot) then
       if (dim.eq.3) write(1000+np,'(2a13)'   ,advance='no')     ' ---omegx--- ',&
            ' ---omegz--- '
       write(1000+np,'(a13)'    ,advance='no')     ' ---omegy--- '
    end if

    !-------------------------------------------------------------------------------
    !ecrire les forces hydrodynamiques appliquees sur la particule a l'iteration nt
    !-------------------------------------------------------------------------------
    if (vl%postforce) then
       if (dim.eq.2) then
          write(1000+np,'(6a13)'   ,advance='no')     ' ----fx----- ',' ----fz----- ',&
               ' ----fpx---- ',' ----fpz---- ', ' ----fvx---- ',' ----fvz---- '          
       else
          write(1000+np,'(9a13)'  ,advance='no')      ' ----fx----- ',' ----fz----- ',&
               ' ----fy----- ',' ----fpx---- ',' ----fpz---- ',' ----fpy---- ',' ----fvx---- ',&
               ' ----fvz---- ',' ----fvy---- '
       end if
    end if

    !-------------------------------------------------------------------------------
    !ecrire le flux de chaleur de la particule a l'iteration nt
    !-------------------------------------------------------------------------------
    if (vl%postHeatFlux) then
      write(1000+np,'(2a13)'   ,advance='no')     ' ----HF----- ',' ----BT----- '
    end if

    !-------------------------------------------------------------------------------
    !ecrire les forces de collisions appliquees sur la particule a l'iteration nt
    !-------------------------------------------------------------------------------
    if (vl%postchoc_bpp)  write(1000+np,'(a3)',advance='no')  'bpp'
    if (vl%postchoc_fpp) then
       write(1000+np,'(2a13)',advance='no') ' ---fppx---- ',' ---fppz---- '
       if (dim.eq.3) write(1000+np,'(a13)' ,advance='no') ' ---fppy---- '
    end if
    if (vl%postchoc_bpm)  write(1000+np,'(a3)',advance='no')  'bpm'
    if (vl%postchoc_fpm) then
       write(1000+np,'(2a13)',advance='no') ' ---fpmx---- ',' ---fpmz---- '
       if (dim.eq.3) write(1000+np,'(a13)' ,advance='no') ' ---fpmy---- '
    end if
    close(1000+np)

    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'sortie ParticlesDataStructure_Impression_FileCreation'
    !-------------------------------------------------------------------------------
  end subroutine ParticlesDataStructure_Impression_FileCreation

  !===============================================================================
end module Bib_VOFLag_ParticlesDataStructure_Impression_FileCreation
!===============================================================================
  


