!===============================================================================
module Bib_VOFLag_HydrodynamicForce
  !===============================================================================
  contains
  !---------------------------------------------------------------------------  
  !> @author jorge cesar brandle de motta, amine chadil
  ! 
  !> @brief calcul des efforts hydrodynamiques appliquees sur les particules
  !
  !> @param[out] vl              : la structure contenant toutes les informations
  !! relatives aux particules.
  !> @param[in]  pres             : pression
  !> @param[in]  u,v,w             : vitesse
  !---------------------------------------------------------------------------  
  subroutine HydrodynamicForce(vl,pres,u,v,w,cou)
    !===============================================================================
    !modules
    !===============================================================================
    !-------------------------------------------------------------------------------
    !Modules de definition des structures et variables => src/mod/module_...f90
    !-------------------------------------------------------------------------------
    use Module_VOFLag_ParticlesDataStructure
    use mod_Parameters,                       only : rank,deeptracking,dim
    use mod_mpi
    !-------------------------------------------------------------------------------
    !Modules de subroutines de la librairie VOF-Lag
    !-------------------------------------------------------------------------------
    use Bib_VOFLag_HydrodynamicForce_LagrangeExtrapolation
    !-------------------------------------------------------------------------------
    !===============================================================================
    !declarations des variables
    !===============================================================================
    implicit none
    !-------------------------------------------------------------------------------
    !variables globales
    !-------------------------------------------------------------------------------
    type(struct_vof_lag),                   intent(inout) :: vl
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable, intent(in)    :: cou,pres
    real(8), dimension(:,:,:), allocatable, intent(in)    :: u,v,w
    !-------------------------------------------------------------------------------
    !variables locales 
    !-------------------------------------------------------------------------------
    integer                                       :: np,nd
    real                                          :: vals
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'entree HydrodynamicForce'
    !-------------------------------------------------------------------------------
    call HydrodynamicForce_LagrangeExtrapolation(vl,pres,u,v,w,cou)

    vl%ft_n=0.
    do np=1,vl%kpt
      if (rank.eq.vl%objet(np)%locpart) then
        vl%ft_n=vl%ft_n+vl%objet(np)%f
      endif
    enddo
    do nd = 1,dim   
      vals=vl%ft_n(nd)
      call mpi_allreduce(vals,vl%ft_n(nd),1,mpi_double_precision,mpi_sum,comm3d,code)
    enddo

    !-------------------------------------------------------------------------------
    if (deeptracking) write (*,*) 'sortie HydrodynamicForce'
    !-------------------------------------------------------------------------------
  end subroutine HydrodynamicForce

  !===============================================================================
end module Bib_VOFLag_HydrodynamicForce
!===============================================================================
  


