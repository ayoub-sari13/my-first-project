!========================================================================
!
!                   FUGU Version 1.0.0
!
!========================================================================
!
! Copyright  Stéphane Vincent
!
! stephane.vincent@u-pem.fr          straightparadigm@gmail.com
!
! This file is part of the FUGU Fortran library, a set of Computational 
! Fluid Dynamics subroutines devoted to the simulation of multi-scale
! multi-phase flows for resolved scale interfaces (liquid/gas/solid). 
! FUGU uses the initial developments of DyJeAT and SHALA codes from 
! Jean-Luc Estivalezes (jean-luc.estivalezes@onera.fr) and 
! Frédéric Couderc (couderc@math.univ-toulouse.fr).
!
!========================================================================
!**
!**   NAME       : operators.f90
!**
!**   AUTHOR     : Stéphane Vincent
!**
!**   FUNCTION   : module of discrete operators of FUGU software
!**
!**   DATES      : Version 1.0.0  : from : january, 16, 2017
!**
!========================================================================

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
module mod_operators
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
contains

  !-------------------------------------------------------------------------------
  ! Discrete gradient of a scalar with centered scheme
  !-------------------------------------------------------------------------------

  !*******************************************************************************
  subroutine gradient_scal_2D (sca,grdu,grdv)
    !*******************************************************************************
    use mod_Parameters, only:ex,sx,ey,sy,dx,dy,sxu,exu,syu,eyu,sxv,exv,syv,eyv,&
         & dxu,dyv
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8),dimension(:,:,:), allocatable, intent(in)     :: sca
    real(8), dimension(:,:,:), allocatable, intent(inout) :: grdu,grdv
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer :: i,j
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    grdu = 0; grdv=0
    !-------------------------------------------------------------------------------
    ! U component
    do j=syu,eyu
       do i=sxu,exu
          grdu(i,j,1)=(sca(i,j,1)-sca(i-1,j,1))/dxu(i)
       enddo
    enddo
    ! V component
    do j=syv,eyv
       do i=sxv,exv
          grdv(i,j,1)=(sca(i,j,1)-sca(i,j-1,1))/dyv(j)
       enddo
    enddo
    !-------------------------------------------------------------------------------

    return
  end subroutine gradient_scal_2D
  !*******************************************************************************

  !*******************************************************************************
  subroutine normal (cou,noru,norv,norw,noruu,norvu,norwu)
    !*******************************************************************************
    use mod_mpi
    use mod_Parameters, only:nproc,        &
         & ex,sx,ey,sy,sz,ez,dx,dy,dz,dim, &
         & dxu,dyv,dzw,dxu2,dyv2,dzw2,     &
         & sxu,exu,syu,eyu,szu,ezu,        &
         & sxv,exv,syv,eyv,szv,ezv,        &
         & sxw,exw,syw,eyw,szw,ezw
    !-------------------------------------------------------------------------------
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable, intent(in)    :: cou
    real(8), dimension(:,:,:), allocatable, intent(inout) :: noru,norv,norw
    real(8), dimension(:,:,:), allocatable, intent(inout) :: noruu,norvu,norwu
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer :: i,j,k
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    if (dim==2) then

       do j=syu,eyu
          do i=sxu,exu
             noru(i,j,1)=(cou(i,j,1)-cou(i-1,j  ,1))/dxu(i)
          end do
       end do

       do j=syv,eyv
          do i=sxv,exv
             norv(i,j,1)=(cou(i,j,1)-cou(i  ,j-1,1))/dyv(j)
          enddo
       enddo
       
       if (nproc>1) call comm_mpi_uvw(noru,norv,norw)
       
       do j=syu,eyu
          do i=sxu,exu
             noruu(i,j,1)=noru(i,j,1)/(1.D-40+sqrt(noru(i,j,1)**2+ &
                  & ((dxu2(i)*dyv2(j+1)*norv(i,j,1)+ &
                  & dxu2(i)*dyv2(j+1)*norv(i-1,j,1)+ &
                  & dxu2(i)*dyv2(j)*norv(i,j+1,1)+   &
                  & dxu2(i)*dyv2(j)*norv(i-1,j+1,1))/dxu(i)/dy(j))**2))
          end do
       end do

       do j=syv,eyv
          do i=sxv,exv
             norvu(i,j,1)=norv(i,j,1)/(1.D-40+sqrt(norv(i,j,1)**2+ &
                  & ((dxu2(i+1)*dyv2(j)*noru(i,j,1)+ &
                  & dxu2(i)*dyv2(j)*noru(i+1,j,1)+   &
                  & dxu2(i+1)*dyv2(j)*noru(i,j-1,1)+ &
                  & dxu2(i)*dyv2(j)*noru(i+1,j-1,1))/dx(i)/dyv(j))**2))
          enddo
       enddo
       
       if (nproc>1) call comm_mpi_uvw(noruu,norvu,norwu)
       
    else

       do k=szu,ezu
          do j=syu,eyu
             do i=sxu,exu
                noru(i,j,k)=(cou(i,j,k)-cou(i-1,j  ,k  ))/dxu(i)
             enddo
          enddo
       enddo
       
       do k=szv,ezv
          do j=syv,eyv
             do i=sxv,exv
                norv(i,j,k)=(cou(i,j,k)-cou(i  ,j-1,k  ))/dyv(j)
             enddo
          enddo
       enddo

       do k=szw,ezw
          do j=syw,eyw
             do i=sxw,exw
                norw(i,j,k)=(cou(i,j,k)-cou(i  ,j  ,k-1))/dzw(k)
             enddo
          enddo
       enddo
       
       if (nproc>1) call comm_mpi_uvw(noru,norv,norw)
       
       do k=szu,ezu
          do j=syu,eyu
             do i=sxu,exu
                noruu(i,j,k)=noru(i,j,k)/(1.D-40+sqrt(noru(i,j,k)**2+     &
                     & ((dxu2(i)*dyv2(j+1)*norv(i,j,k)+                   &
                     & dxu2(i)*dyv2(j+1)*norv(i-1,j,k)+                   &
                     & dxu2(i)*dyv2(j)*norv(i,j+1,k)+                     &
                     & dxu2(i)*dyv2(j)*norv(i-1,j+1,k))/dxu(i)/dy(j))**2+ &
                     & ((dxu2(i)*dzw2(k+1)*norw(i,j,k)+                   &                
                     & dxu2(i)*dzw2(k+1)*norw(i-1,j  ,k  )+               &
                     & dxu2(i)*dzw2(k)*norw(i,j  ,k+1)+                   &
                     & dxu2(i)*dzw2(k)*norw(i-1,j  ,k+1))/dxu(i)/dz(k))**2))
             end do
          end do
       end do

       do k=szv,ezv
          do j=syv,eyv
             do i=sxv,exv
                norvu(i,j,k)=norv(i,j,k)/(1.D-40+sqrt(norv(i,j,k)**2+     &
                     & ((dxu2(i+1)*dyv2(j)*noru(i,j,k)+                   &
                     & dxu2(i)*dyv2(j)*noru(i+1,j,k)+                     &
                     & dxu2(i+1)*dyv2(j)*noru(i,j-1,k)+                   &
                     & dxu2(i)*dyv2(j)*noru(i+1,j-1,k))/dx(i)/dyv(j))**2+ &
                     & ((dyv2(j)*dzw2(k+1)*norw(i,j,k)+                   & 
                     & dyv2(j)*dzw2(k+1)*norw(i  ,j-1,k  )+               &
                     & dyv2(j)*dzw2(k)*norw(i,j  ,k+1)+                   &
                     & dyv2(j)*dzw2(k)*norw(i  ,j-1,k+1))/dyv(j)/dz(k))**2))
             end do
          end do
       end do

       do k=szw,ezw
          do j=syw,eyw
             do i=sxw,exw
                norwu(i,j,k)=norw(i,j,k)/(1.D-40+sqrt(norw(i,j,k)**2+       &
                     & ((dxu2(i+1)*dzw2(k)*noru(i,j,k)+                     &
                     & dxu2(i)*dzw2(k)*noru(i+1,j  ,k  )+                   &
                     & dxu2(i+1)*dzw2(k)*noru(i,j  ,k-1)+                   &
                     & dxu2(i)*dzw2(k)*noru(i+1,j  ,k-1))/dx(i)/dzw(k))**2+ &
                     & ((dyv2(j+1)*dzw2(k)*norv(i,j,k)+                     &
                     & dyv2(j)*dzw2(k)*norv(i  ,j+1,k  )+                   &
                     & dyv2(j+1)*dzw2(k)*norv(i,j  ,k-1)+                   &
                     & dyv2(j)*dzw2(k)*norv(i  ,j+1,k-1))/dy(j)/dzw(k))**2))
             enddo
          enddo
       enddo
       
       if (nproc>1) call comm_mpi_uvw(noruu,norvu,norwu)
              
    endif
    !-------------------------------------------------------------------------------
    return

  end subroutine normal
  !*******************************************************************************

  !*******************************************************************************
  subroutine diverge_vec_2D (div,norm,maxi,u,v)
    !*******************************************************************************
    use mod_mpi
    use mod_Parameters, only:ex,sx,ey,sy,dx,dy
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), intent(out)                                 :: norm,maxi
    real(8), dimension(:,:,:), allocatable,intent(in)    :: u,v
    real(8), dimension(:,:,:), allocatable,intent(inout) :: div
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                              :: i,j
    real(8)                                              :: divp
    real(8)                                              :: lnorm,lmax 
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    div=0; norm=0; lnorm=0; lmax=0
    !-------------------------------------------------------------------------------
    do j=sy,ey
       do i=sx,ex
          divp=(u(i+1,j,1)-u(i,j,1))/dx(i) &
               & +(v(i,j+1,1)-v(i,j,1))/dy(j)
          div(i,j,1)=divp
          lnorm=lnorm+divp**2*dx(i)*dy(j)
          if (abs(divp)>lmax) lmax=divp
       enddo
    enddo

    call mpi_allreduce(lnorm,norm,1,mpi_double_precision,mpi_sum,comm3d,code)
    call mpi_allreduce(lmax,maxi,1,mpi_double_precision,mpi_max,comm3d,code)

    norm=sqrt(norm)
    !-------------------------------------------------------------------------------

    return
  end subroutine diverge_vec_2D
  !*******************************************************************************

  !*******************************************************************************
  subroutine diverge_vec_3D (div,norm,maxi,u,v,w)
    !*******************************************************************************
    use mod_mpi
    use mod_Parameters, only:ex,sx,ey,sy,sz,ez,dx,dy,dz
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), intent(out)                                 :: norm,maxi
    real(8), dimension(:,:,:), allocatable,intent(in)    :: u,v,w
    real(8), dimension(:,:,:), allocatable,intent(inout) :: div
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                              :: i,j,k
    real(8)                                              :: divp
    real(8)                                              :: lnorm,lmax
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    div=0; norm=0; lnorm=0; lmax=0
    !-------------------------------------------------------------------------------
    do k=sz,ez
       do j=sy,ey
          do i=sx,ex
             divp=(u(i+1,j,k)-u(i,j,k))/dx(i)    &
                  & +(v(i,j+1,k)-v(i,j,k))/dy(j) &
                  & +(w(i,j,k+1)-w(i,j,k))/dz(k)
             div(i,j,k)=divp
             lnorm=lnorm+divp**2*dx(i)*dy(j)*dz(k)
             if (abs(divp)>lmax) lmax=divp
          enddo
       enddo
    end do

    call mpi_allreduce(lnorm,norm,1,mpi_double_precision,mpi_sum,comm3d,code)
    call mpi_allreduce(lmax,maxi,1,mpi_double_precision,mpi_max,comm3d,code)

    norm=sqrt(norm)
    !-------------------------------------------------------------------------------
    return
  end subroutine diverge_vec_3D
  !*******************************************************************************

  subroutine diverge_vec(div,norm,maxi,u,v,w)
    !*******************************************************************************
    use mod_mpi
    use mod_Parameters, only: dim
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), intent(out)                                 :: norm,maxi
    real(8), dimension(:,:,:), allocatable,intent(in)    :: u,v,w
    real(8), dimension(:,:,:), allocatable,intent(inout) :: div
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------

    if (dim==2) then
       call diverge_vec_2D(div,norm,maxi,u,v)
    else if (dim==3) then
       call diverge_vec_3D(div,norm,maxi,u,v,w)
    end if
    
  end subroutine diverge_vec

end module mod_operators
