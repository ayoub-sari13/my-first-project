!========================================================================
!
!                   FUGU Version 1.0.0
!
!========================================================================
!
! Copyright  Stéphane Vincent
!
! stephane.vincent@u-pem.fr          straightparadigm@gmail.com
!
! This file is part of the FUGU Fortran library, a set of Computational 
! Fluid Dynamics subroutines devoted to the simulation of multi-scale
! multi-phase flows for resolved scale interfaces (liquid/gas/solid). 
! FUGU uses the initial developments of DyJeAT and SHALA codes from 
! Jean-Luc Estivalezes (jean-luc.estivalezes@onera.fr) and 
! Frédéric Couderc (couderc@math.univ-toulouse.fr).
!
!========================================================================
!**
!**   NAME       : Energy.f90
!**
!**   AUTHOR     : Stéphane Vincent
!**              : Benoit Trouette
!**
!**   FUNCTION   : Energy Equation modules of FUGU software
!**
!**   DATES      : Version 1.0.0  : from : feb, 20, 2018
!**
!========================================================================

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#include "precomp.h"
module mod_diffusion
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
contains

  subroutine Diffusion_Delta (tp,tp0,tp1,diffu,diffv,diffw,tpin,pentp,energy)
    !*****************************************************************************************************
    !*****************************************************************************************************
    !*****************************************************************************************************
    !*****************************************************************************************************
    use mod_mpi
    use mod_borders
    use mod_Parameters, only: dim,nb_T,gx,gy,gz, &
         & ex,sx,ey,sy,ez,sz,                    &
         & exs,sxs,eys,sys,ezs,szs,              &
         & gsx,gex,gsy,gey,gsz,gez,              &
         & mesh,                                 &
         & Euler,                                &
         & EN_solver_type,dx,dy,dz
    use mod_Constants, only: one
    use mod_Connectivity
    use mod_operators
    use mod_struct_solver
    use mod_solver_new
    !-------------------------------------------------------------------------------
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:),   allocatable :: cou,diffu,diffv,diffw
    real(8), dimension(:,:,:),   allocatable :: tp,tp0,tp1
    real(8), dimension(:,:,:),   allocatable :: tpin
    real(8), dimension(:,:,:,:), allocatable :: pentp
    type(solver_sca_t), intent(inout)        :: energy
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                  :: i,j,k,l,lts,impp,it_solv,la
    integer                                  :: ii,jj,kk
    integer                                  :: switch2D3D
    real(8)                                  :: pe_m,pe_p,pe_l,pe_r,pe_b,pe_f
    real(8)                                  :: res,div_norm
    real(8), dimension(:),       allocatable :: smc,sol
    real(8), dimension(:,:,:),   allocatable :: smc_tab
    character(len=100)                       :: fmt1
    real(8)                                  :: eps_slope=1d-15
    type(solver_t)                           :: solver
    type(system_t)                           :: system
    !-------------------------------------------------------------------------------


    switch2D3D=mesh%dim-2
    
    !-------------------------------------------------------------------------------
    ! Periodicity for explicit variables 
    !-------------------------------------------------------------------------------
    call sca_periodicity(mesh,tp)
    !-------------------------------------------------------------------------------
    
    !-------------------------------------------------------------------------------
    call Energy_Connectivity(system%mat,mesh)
    !-------------------------------------------------------------------------------
    ! alloc
    !-------------------------------------------------------------------------------
    allocate(sol(system%mat%npt))
    allocate(smc(system%mat%npt))
    !-------------------------------------------------------------------------------

    
    !-------------------------------------------------------------------------------
    ! Solved nodes
    !-------------------------------------------------------------------------------
    call Energy_kic(system%mat,mesh)
    !-------------------------------------------------------------------------------

    
    !*******************************************************************************
    ! Solving of A . U = F
    ! coef = A
    ! sol  = U
    ! smc  = F
    !*******************************************************************************
    
    
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------
    ! 2D/3D Energy equation Solving
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Second member of the problem
    !-------------------------------------------------------------------------------
    smc=0
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Time integration for temperature time derivative
    !-------------------------------------------------------------------------------
    do kk=szs,ezs
       do jj=sys,eys
          do ii=sxs,exs
             i=ii-sxs
             j=jj-sys
             k=kk-szs
             l=i+j*(exs-sxs+1)+k*switch2D3D*(exs-sxs+1)*(eys-sys+1)+1
             smc(l)=smc(l) + tp0(ii,jj,kk) * dx(ii) * dy(jj) * dz(kk) 
          enddo
       enddo
    end do
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Boundary conditions
    !-------------------------------------------------------------------------------
    do kk=szs,ezs 
       do jj=sys,eys
          do ii=sxs,exs
             i=ii-sxs
             j=jj-sys
             k=kk-szs
             l=i+j*(exs-sxs+1)+k*switch2D3D*(exs-sxs+1)*(eys-sys+1)+1
             smc(l)=smc(l)+pentp(ii,jj,kk,1)*tpin(ii,jj,kk) * dx(ii) * dy(jj) * dz(kk) 
          enddo
       enddo
    end do
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Making the Matrix
    !-------------------------------------------------------------------------------
    if (Euler==1) then
       do kk=szs,ezs 
          do jj=sys,eys
             do ii=sxs,exs
                i=ii-sxs
                j=jj-sys
                k=kk-szs
                l=i+j*(exs-sxs+1)+k*switch2D3D*(exs-sxs+1)*(eys-sys+1)+1
                sol(l)=tp0(ii,jj,kk)
             enddo
          enddo
       end do
    else
       do kk=szs,ezs 
          do jj=sys,eys
             do ii=sxs,exs
                i=ii-sxs
                j=jj-sys
                k=kk-szs
                l=i+j*(exs-sxs+1)+k*switch2D3D*(exs-sxs+1)*(eys-sys+1)+1
                sol(l)=2*tp0(ii,jj,kk)-tp1(ii,jj,kk)
             enddo
          enddo
       end do
    endif

    call Diffusion_Matrix(system%mat,tp,diffu,diffv,diffw,pentp,energy)
    !-------------------------------------------------------------------------------
    
    !-------------------------------------------------------------------------------
    ! Solving the linear system
    !-------------------------------------------------------------------------------
    solver%type    = 1
    solver%eqs     = 0
    solver%precond = 1
    solver%res_max = 1d-8
    solver%it_max  = 12

    system%mat%eqs = solver%eqs
!!$    system%mat%npt = mesh%nb%tot%p
!!$    system%mat%nps = energy%nb_matrix_element
!!$    system%mat%ndd = energy%nb_diagonal_precond
!!$    system%mat%kdv = energy%nb_coef_matrix
!!$    system%mat%nic = nic
!!$
!!$    allocate(system%mat%coef(system%mat%nps))   ; system%mat%coef = coef
!!$    allocate(system%mat%icof(system%mat%npt+1)) ; system%mat%icof = icof
!!$    allocate(system%mat%jcof(system%mat%nps))   ; system%mat%jcof = jcof
!!$    allocate(system%mat%kic(system%mat%npt))    ; system%mat%kic  = kic

    call solve_linear_system(system,smc,sol,solver,mesh)

    deallocate(system%mat%coef)
    deallocate(system%mat%icof)
    deallocate(system%mat%jcof)
    deallocate(system%mat%kic)
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! The new temperature solution is updated
    !-------------------------------------------------------------------------------
    do kk=szs,ezs 
       do jj=sys,eys
          do ii=sxs,exs
             i=ii-sxs
             j=jj-sys
             k=kk-szs
             l=i+j*(exs-sxs+1)+k*switch2D3D*(exs-sxs+1)*(eys-sys+1)+1
             tp(ii,jj,kk)=sol(l)
          enddo
       enddo
    end do
    !-------------------------------------------------------------------------------

    !===============================================================================
    ! End of solving
    !===============================================================================
    
    call Interp_Scal_Boundary(tp,energy%bound)

    !-------------------------------------------------------------------------------
    ! Saving solving parameters
    !-------------------------------------------------------------------------------
    energy%output%res_solver(1) = res
    energy%output%it_solver(1)  = it_solv

    !*******************************************************************************
  end subroutine Diffusion_Delta
  !*******************************************************************************


  subroutine Diffusion_Matrix(mat,tp,diffu,diffv,diffw,pentp,energy)
    !*****************************************************************************************************
    !*****************************************************************************************************
    !*****************************************************************************************************
    !*****************************************************************************************************
    !-------------------------------------------------------------------------------
    ! Finite volume discretization of the Energy equation
    !-------------------------------------------------------------------------------
    use mod_Parameters, only: dim 
    use mod_struct_solver
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable,  intent(inout) :: tp
    real(8),  dimension(:,:,:), allocatable,  intent(in)   :: diffu,diffv,diffw
    real(8), dimension(:,:,:,:), allocatable,  intent(in)  :: pentp
    type(solver_sca_t), intent(inout)                     :: energy
    type(matrix_t), intent(inout)                     :: mat 
    !-------------------------------------------------------------------------------

    if (dim==2) then
       call Diffusion_Matrix_2D(mat%coef,mat%jcof,mat%icof,tp,diffu,diffv,pentp,mat%kdv,mat%nps,energy)
    else
       call Diffusion_Matrix_3D(mat%coef,mat%jcof,mat%icof,tp,diffu,diffv,diffw,pentp,mat%kdv,mat%nps,energy)
    end if
    
  end subroutine Diffusion_Matrix
  !*****************************************************************************************************
  !*****************************************************************************************************
  !*****************************************************************************************************
  !*****************************************************************************************************
  subroutine Diffusion_Matrix_2D (coef,jcof,icof,tp,diffu,diffv, &
       & pentp,nb_coef_matrix,nb_matrix_element,energy)
    !*****************************************************************************************************
    !*****************************************************************************************************
    !*****************************************************************************************************
    !*****************************************************************************************************
    !-------------------------------------------------------------------------------
    ! Finite volume discretization of the Energy equation
    !-------------------------------------------------------------------------------
    use mod_Parameters, only: nproc, &
         & dim,dx,dy,dxu,dyv,        &
         & sx,ex,sy,ey,              &
         & sxs,exs,sys,eys
    use mod_Constants, only: zero,d1p2,d1p8,d3p8,d6p8,d3p2
    use mod_struct_solver
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)                                    :: nb_coef_matrix,nb_matrix_element
    integer, dimension(:), intent(in), allocatable         :: jcof,icof
    real(8), dimension(:), intent(inout), allocatable      :: coef
    real(8), dimension(:,:,:), allocatable,  intent(inout) :: tp
    real(8),  dimension(:,:,:), allocatable,  intent(in)   :: diffu,diffv
    real(8), dimension(:,:,:,:), allocatable,  intent(in)  :: pentp
    type(solver_sca_t), intent(inout)                     :: energy
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    real(8)                                                :: difb,diff,inerb,inerf,inerbb,inerff
    real(8)                                                :: rbm,rbp,rfm,rfp,alphab,alphaf
    real(8)                                                :: rocp
    real(8)                                                :: dx1,dx2
    integer                                                :: i,j,lvs
    integer                                                :: code
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Matrix initialization
    !-------------------------------------------------------------------------------
    do lvs = 1,nb_matrix_element
       coef(lvs) = 0
    end do
    !-------------------------------------------------------------------------------
    
    !-------------------------------------------------------------------------------
    ! Finite volume discretization
    !-------------------------------------------------------------------------------
    lvs=1
    do j=sys,eys
       do i=sxs,exs
          
          !if (nproc>1.and.(i==sxs.or.i==exs.or.j==sys.or.j==eys)) then
          if (nproc>1.and.(i<sx.or.i>ex.or.j<sy.or.j>ey)) then
             !-------------------------------------------------------------------------------
             ! Penalty term for MPI exchange cells
             !-------------------------------------------------------------------------------
             coef(lvs)=1!;coef(lvs+1:lvs+4)=0
          else           
             !-------------------------------------------------------------------------------
             ! Time integration scheme
             !-------------------------------------------------------------------------------
             coef(lvs) = dx(i) * dy(j)
             !-------------------------------------------------------------------------------
             ! Conductivity
             !-------------------------------------------------------------------------------
             difb= - diffu(i  ,j,1)/dxu(i  ) * dy(j) ! backward contribution
             diff=   diffu(i+1,j,1)/dxu(i+1) * dy(j) ! forward contribution
             coef(lvs  )= coef(lvs)   - (difb  - diff)
             coef(lvs+1)= coef(lvs+1) + difb
             coef(lvs+2)= coef(lvs+2) - diff
             difb= - diffv(i,j  ,1)/dyv(j  ) * dx(i) ! backward contribution
             diff=   diffv(i,j+1,1)/dyv(j+1) * dx(i) ! forward contribution
             coef(lvs  )= coef(lvs)   - (difb  - diff)
             coef(lvs+3)= coef(lvs+3) + difb
             coef(lvs+4)= coef(lvs+4) - diff
             !-------------------------------------------------------------------------------
             ! Penalty term for boundary conditions
             ! pentp (i,j,k,l)
             ! l=1 central T component
             ! l=2 left T component
             ! l=3 right T component
             ! l=4 bottom T component
             ! l=5 top T component
             !-------------------------------------------------------------------------------
             coef(lvs)   = coef(lvs)   + pentp(i,j,1,1) * dx(i) * dy(j)
             coef(lvs+1) = coef(lvs+1) + pentp(i,j,1,2) * dx(i) * dy(j)
             coef(lvs+2) = coef(lvs+2) + pentp(i,j,1,3) * dx(i) * dy(j)
             coef(lvs+3) = coef(lvs+3) + pentp(i,j,1,4) * dx(i) * dy(j)
             coef(lvs+4) = coef(lvs+4) + pentp(i,j,1,5) * dx(i) * dy(j)
             !-------------------------------------------------------------------------------
             ! End of discretization for T
             !-------------------------------------------------------------------------------
          end if
          
          !-------------------------------------------------------------------------------
          ! next line of the matrix
          !-------------------------------------------------------------------------------
          lvs=lvs+nb_coef_matrix 
       enddo
    enddo
    return
  end subroutine  Diffusion_Matrix_2D
  !*****************************************************************************************************
  !*****************************************************************************************************
  !*****************************************************************************************************
  !*****************************************************************************************************


  !*****************************************************************************************************
  !*****************************************************************************************************
  !*****************************************************************************************************
  !*****************************************************************************************************
  subroutine Diffusion_Matrix_3D (coef,jcof,icof,tp,diffu,diffv,diffw, &
       & pentp,nb_coef_matrix,nb_matrix_element,energy)
    !*****************************************************************************************************
    !*****************************************************************************************************
    !*****************************************************************************************************
    !*****************************************************************************************************
    !-------------------------------------------------------------------------------
    ! Finite volume discretization of the Energy equation
    !-------------------------------------------------------------------------------
    use mod_Parameters, only: nproc, &
         & dim,                      &
         & dx,dy,dz,dxu,dyv,dzw,     &
         & sx,ex,sy,ey,sz,ez,        &
         & sxs,exs,sys,eys,szs,ezs
    use mod_Constants, only: zero,d1p2
    use mod_struct_solver
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)                                    :: nb_coef_matrix,nb_matrix_element
    integer, dimension(:), intent(in), allocatable         :: jcof,icof
    real(8), dimension(:), intent(inout), allocatable      :: coef
    real(8), dimension(:,:,:), allocatable,  intent(inout) :: tp
    real(8), dimension(:,:,:), allocatable,  intent(in)    :: diffu,diffv,diffw
    real(8), dimension(:,:,:,:), allocatable,  intent(in)  :: pentp
    type(solver_sca_t), intent(inout)                     :: energy
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    real(8)                                                :: difb,diff,inerb,inerf,inerbb,inerff
    real(8)                                                :: alphab,alphaf
    real(8)                                                :: rocp
    integer                                                :: i,j,k,lvs
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Matrix initialization
    !-------------------------------------------------------------------------------
    do lvs = 1,nb_matrix_element
       coef(lvs) = 0
    end do
    !-------------------------------------------------------------------------------
    
    !-------------------------------------------------------------------------------
    ! Finite volume discretization
    !-------------------------------------------------------------------------------
    lvs=1
    do k=szs,ezs
       do j=sys,eys
          do i=sxs,exs

             !if (nproc>1.and.(i==sxs.or.i==exs.or.j==sys.or.j==eys.or.k==szs.or.k==ezs)) then
             if (nproc>1.and.(i<sx.or.i>ex.or.j<sy.or.j>ey.or.k<sz.or.k>ez)) then
                !-------------------------------------------------------------------------------
                ! Penalty term for MPI exchange cells
                !-------------------------------------------------------------------------------
                coef(lvs)=1!;coef(lvs+1:lvs+6)=0
             else
                !-------------------------------------------------------------------------------
                ! Time integration scheme
                !-------------------------------------------------------------------------------
                coef(lvs) = dx(i) * dy(j) * dz(k)
                !-------------------------------------------------------------------------------
                ! Conductivity
                !-------------------------------------------------------------------------------
                difb= - diffu(i  ,j,k)/dxu(i  ) * dy(j) * dz(k) ! backward contribution
                diff=   diffu(i+1,j,k)/dxu(i+1) * dy(j) * dz(k) ! forward contribution
                coef(lvs  )= coef(lvs)   - (difb  - diff)
                coef(lvs+1)= coef(lvs+1) + difb
                coef(lvs+2)= coef(lvs+2) - diff
                difb= - diffv(i,j  ,k)/dyv(j  ) * dx(i) * dz(k) ! backward contribution
                diff=   diffv(i,j+1,k)/dyv(j+1) * dx(i) * dz(k) ! forward contribution
                coef(lvs  )= coef(lvs)   - (difb  - diff)
                coef(lvs+3)= coef(lvs+3) + difb
                coef(lvs+4)= coef(lvs+4) - diff
                difb= - diffw(i,j,k  )/dzw(k  ) * dx(i) * dy(j) ! backward contribution
                diff=   diffw(i,j,k+1)/dzw(k+1) * dx(i) * dy(j) ! forward contribution
                coef(lvs  )= coef(lvs)   - (difb  - diff)
                coef(lvs+5)= coef(lvs+5) + difb
                coef(lvs+6)= coef(lvs+6) - diff
                !-------------------------------------------------------------------------------
                ! Penalty term for boundary conditions
                ! pentp (i,j,k,l)
                ! l=1 central T component
                ! l=2 left T component
                ! l=3 right T component
                ! l=4 bottom T component
                ! l=5 top T component
                ! l=6 backward T component
                ! l=7 forward T component
                !-------------------------------------------------------------------------------
                coef(lvs)   = coef(lvs)   + pentp(i,j,k,1) * dx(i) * dy(j) * dz(k)
                coef(lvs+1) = coef(lvs+1) + pentp(i,j,k,2) * dx(i) * dy(j) * dz(k)
                coef(lvs+2) = coef(lvs+2) + pentp(i,j,k,3) * dx(i) * dy(j) * dz(k)
                coef(lvs+3) = coef(lvs+3) + pentp(i,j,k,4) * dx(i) * dy(j) * dz(k)
                coef(lvs+4) = coef(lvs+4) + pentp(i,j,k,5) * dx(i) * dy(j) * dz(k)
                coef(lvs+5) = coef(lvs+5) + pentp(i,j,k,6) * dx(i) * dy(j) * dz(k)
                coef(lvs+6) = coef(lvs+6) + pentp(i,j,k,7) * dx(i) * dy(j) * dz(k)
                !-------------------------------------------------------------------------------
                ! End of discretization for T
                !-------------------------------------------------------------------------------
             end if
             
             !-------------------------------------------------------------------------------
             ! next line of the matrix
             !-------------------------------------------------------------------------------
             lvs=lvs+nb_coef_matrix 
          enddo
       enddo
    end do
    
    return
  end subroutine  Diffusion_Matrix_3D
  !*****************************************************************************************************
  !*****************************************************************************************************
  !*****************************************************************************************************
  !*****************************************************************************************************

end module mod_diffusion



