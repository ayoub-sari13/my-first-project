!===============================================================================
module Bib_VOFLag_ParticlesDataStructure_Object_allocate
  !===============================================================================
  contains
  !---------------------------------------------------------------------------  
  !> @author amine chadil
  ! 
  !> @brief aloue kpt structure (vl%objet), et initialise leurs champs
  !
  !> @param[out] vl  : la structure contenant toutes les informations
  !! relatives aux particules    
  !---------------------------------------------------------------------------  
  subroutine ParticlesDataStructure_Object_allocate(vl)
    !===============================================================================
    !modules
    !===============================================================================
    !-------------------------------------------------------------------------------
    !Modules de definition des structures et variables => src/mod/module_...f90
    !-------------------------------------------------------------------------------
    use Module_VOFLag_ParticlesDataStructure 
    use mod_Parameters,                       only : deeptracking
    !-------------------------------------------------------------------------------

    !===============================================================================
    !declarations des variables
    !===============================================================================
    implicit none
    !-------------------------------------------------------------------------------
    !variables globales
    !-------------------------------------------------------------------------------
    type(struct_vof_lag), intent(inout) :: vl
    !-------------------------------------------------------------------------------
    !variables locales 
    !-------------------------------------------------------------------------------
    integer               :: k
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'entree ParticlesDataStructure_Object_allocate'
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! allocation de kpt objet
    !-------------------------------------------------------------------------------
    if (allocated(vl%objet)) deallocate(vl%objet) 
    allocate(vl%objet(vl%kpt))

    !-------------------------------------------------------------------------------
    ! initialisation des caracteristiques des objets
    !-------------------------------------------------------------------------------
    do k=1,vl%kpt
      vl%objet(k)%ObjectFile    = ' '
      vl%objet(k)%kkl           = 0
      vl%objet(k)%rho           = 0.d0
      vl%objet(k)%masse         = 0.d0
      vl%objet(k)%sca           = 0.d0
      vl%objet(k)%pos           = 0.d0
      vl%objet(k)%euler_angle   = 0.d0
      vl%objet(k)%omeg          = 0.d0
      vl%objet(k)%v             = 0.d0
      vl%objet(k)%vn            = 0.d0
      vl%objet(k)%orientation   = 0.d0
      vl%objet(k)%locpart       = 0
      vl%objet(k)%aff           = .false.
      vl%objet(k)%in            = .false.
      vl%objet(k)%on            = .false.
      vl%objet(k)%ar            = .false.
      vl%objet(k)%nbtimes       = 0
      vl%objet(k)%chocparpar    = .false.
      vl%objet(k)%chocparmur    = .false.
      vl%objet(k)%fparmur       = 0.0d0
      vl%objet(k)%fparpar       = 0.0d0
      vl%objet(k)%fp            = 0.d0
      vl%objet(k)%fv            = 0.d0
      vl%objet(k)%f             = 0.d0
      vl%objet(k)%moment        = 0.0d0
      vl%objet(k)%HeatFlux      = 0.0d0
      vl%objet(k)%BulkTemp      = 0.0d0
    end do

    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'sortie ParticlesDataStructure_Object_allocate'
    !-------------------------------------------------------------------------------
  end subroutine ParticlesDataStructure_Object_allocate

  !===============================================================================
end module Bib_VOFLag_ParticlesDataStructure_Object_allocate
!===============================================================================