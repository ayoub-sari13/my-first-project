
  !-----------------------------------------------------------------
  ! general always used arrays
  !-----------------------------------------------------------------
  allocate(fluids(nb_phase))
  allocate(u(sxu-gx:exu+gx,syu-gy:eyu+gy,szu-gz:ezu+gz)) 
  allocate(v(sxv-gx:exv+gx,syv-gy:eyv+gy,szv-gz:ezv+gz))
  allocate(uref(sxu-gx:exu+gx,syu-gy:eyu+gy,szu-gz:ezu+gz)) 
  allocate(vref(sxv-gx:exv+gx,syv-gy:eyv+gy,szv-gz:ezv+gz))
  allocate(pres(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
  if (dim==3) allocate(w(sxw-gx:exw+gx,syw-gy:eyw+gy,szw-gz:ezw+gz)) 
  allocate(u0(sxu-gx:exu+gx,syu-gy:eyu+gy,szu-gz:ezu+gz)) 
  allocate(v0(sxv-gx:exv+gx,syv-gy:eyv+gy,szv-gz:ezv+gz)) 
  if (dim==3) allocate(w0(sxw-gx:exw+gx,syw-gy:eyw+gy,szw-gz:ezw+gz))
  allocate(pres0(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
  allocate(rho(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
  allocate(grdpu(sxu-gx:exu+gx,syu-gy:eyu+gy,szu-gz:ezu+gz)) 
  allocate(grdpv(sxv-gx:exv+gx,syv-gy:eyv+gy,szv-gz:ezv+gz))
  if (dim==3) allocate(grdpw(sxw-gx:exw+gx,syw-gy:eyw+gy,szw-gz:ezw+gz)) 
  !-----------------------------------------------------------------
  
  !-----------------------------------------------------------------
  ! boundary condition
  !-----------------------------------------------------------------
  allocate(uin(sxu-gx:exu+gx,syu-gy:eyu+gy,szu-gz:ezu+gz))
  allocate(vin(sxv-gx:exv+gx,syv-gy:eyv+gy,szv-gz:ezv+gz))
  if (dim==3) allocate(win(sxw-gx:exw+gx,syw-gy:eyw+gy,szw-gz:ezw+gz))
  !-----------------------------------------------------------------

  if (NS_activate) then
     allocate(u1(sxu-gx:exu+gx,syu-gy:eyu+gy,szu-gz:ezu+gz)) 
     allocate(v1(sxv-gx:exv+gx,syv-gy:eyv+gy,szv-gz:ezv+gz)) 
     if (dim==3) allocate(w1(sxw-gx:exw+gx,syw-gy:eyw+gy,szw-gz:ezw+gz)) 

     allocate(div(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
     
     if (VOF_activate.or.LS_activate.or.FT_Delta.or.NB_phase==1) then
        allocate(vie(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
        allocate(vis(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz,dim**(dim-1)))
        allocate(vir(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz,dim**(dim-1)))
        
        allocate(xit(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
        allocate(rovu(sxu-gx:exu+gx,syu-gy:eyu+gy,szu-gz:ezu+gz))
        allocate(rovv(sxv-gx:exv+gx,syv-gy:eyv+gy,szv-gz:ezv+gz))
        if (dim==3) allocate(rovw(sxw-gx:exw+gx,syw-gy:eyw+gy,szw-gz:ezw+gz))
        allocate(pencou(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz,2*dim+1))
        allocate(couin(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
        pencou=0
        couin=0
     end if
     
     if (LG_2Way) then
        allocate(force_PartFlow(1:dim,sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
     end if
     
     if (NS_linear_term) then
        allocate(slvu(sxu-gx:exu+gx,syu-gy:eyu+gy,szu-gz:ezu+gz))
        allocate(slvv(sxv-gx:exv+gx,syv-gy:eyv+gy,szv-gz:ezv+gz))
        if (dim==3) allocate(slvw(sxw-gx:exw+gx,syw-gy:eyw+gy,szw-gz:ezw+gz))
        allocate(obj(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
     end if
     
     if (NS_source_term) then
        allocate(smvu(sxu-gx:exu+gx,syu-gy:eyu+gy,szu-gz:ezu+gz))
        allocate(smvv(sxv-gx:exv+gx,syv-gy:eyv+gy,szv-gz:ezv+gz))
        if (dim==3) allocate(smvw(sxw-gx:exw+gx,syw-gy:eyw+gy,szw-gz:ezw+gz))
     end if

     if (EN_Boussinesq) then
        allocate(beta(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
        allocate(tpb(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
     end if
          
     allocate(penu(sxu-gx:exu+gx,syu-gy:eyu+gy,szu-gz:ezu+gz,2*dim+1))
     allocate(penv(sxv-gx:exv+gx,syv-gy:eyv+gy,szv-gz:ezv+gz,2*dim+1))
     if (dim==3) allocate(penw(sxw-gx:exw+gx,syw-gy:eyw+gy,szw-gz:ezw+gz,2*dim+1))
     
     select case(NS_method)
     case(1)
        allocate(pin(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
        allocate(penp(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz,2*dim+1))
     case(2)
        if (PJ_method/=0) then
           allocate(pin(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
           allocate(penp(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz,2*dim+1))
           allocate(dpres(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
        end if
     end select

     if (NS_pressure_pen) then
        allocate(bip(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
        allocate(pin(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
     end if

     if (LES_activate) then
        allocate(um(sxu-gx:exu+gx,syu-gy:eyu+gy,szu-gz:ezu+gz)) 
        allocate(vm(sxv-gx:exv+gx,syv-gy:eyv+gy,szv-gz:ezv+gz)) 
        if (dim==3) allocate(wm(sxw-gx:exw+gx,syw-gy:eyw+gy,szw-gz:ezw+gz)) 
     end if
        
     
  endif

  if (NS_MomentumConserving) then
     allocate(uadv2(sxu-gx:exu+gx,syu-gy:eyu+gy,szu-gz:ezu+gz)) 
     allocate(vadv2(sxv-gx:exv+gx,syv-gy:eyv+gy,szv-gz:ezv+gz))
     if (dim==3) allocate(wadv2(sxw-gx:exw+gx,syw-gy:eyw+gy,szw-gz:ezw+gz))
  end if
  
  allocate(rovu0(sxu-gx:exu+gx,syu-gy:eyu+gy,szu-gz:ezu+gz))
  allocate(rovv0(sxv-gx:exv+gx,syv-gy:eyv+gy,szv-gz:ezv+gz))
  if (dim==3) allocate(rovw0(sxw-gx:exw+gx,syw-gy:eyw+gy,szw-gz:ezw+gz))
  !-----------------------------------------------------------------
  ! Turbulent viscosity & conductivity
  !-----------------------------------------------------------------
  if (LES_activate) then
     allocate(mu_sgs(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
     if (SDT_activate) allocate(cond_sgs(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
  end if
  !-----------------------------------------------------------------

  
  !-----------------------------------------------------------------
  ! Energy or scalar arrays
  !-----------------------------------------------------------------
  if (EN_activate) then
     allocate(tp(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
     allocate(tp0(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
     allocate(condu(sxu-gx:exu+gx,syu-gy:eyu+gy,szu-gz:ezu+gz))
     allocate(condv(sxv-gx:exv+gx,syv-gy:eyv+gy,szv-gz:ezv+gz))
     if (dim==3) allocate(condw(sxw-gx:exw+gx,syw-gy:eyw+gy,szw-gz:ezw+gz))
     allocate(cp(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
     allocate(tpin(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
     allocate(pentp(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz,2*dim+1))
     if (abs(Euler)==2) allocate(tp1(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
     if (EN_linear_term) allocate(sltp(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
     if (EN_source_term) allocate(smtp(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
     if (EN_penalty_term) allocate(sptp(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz,2*dim+1))
     !-----------------------------------------------------------------
  end if
  !-----------------------------------------------------------------

  
  !-----------------------------------------------------------------
  ! Interface Tracking
  ! change indexes ? _u : ex+1 --> exu ?
  !-----------------------------------------------------------------
  if (VOF_activate.or.LS_activate.or.FT_activate) then
     allocate(cou(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
  end if
  if (VOF_activate) then
     allocate(uvof(sxu-gx:exu+gx,syu-gy:eyu+gy,szu-gz:ezu+gz)) 
     allocate(vvof(sxv-gx:exv+gx,syv-gy:eyv+gy,szv-gz:ezv+gz))
     if (dim==3) allocate(wvof(sxw-gx:exw+gx,syw-gy:eyw+gy,szw-gz:ezw+gz)) 
  end if
  if (LS_activate) allocate(dist(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
  if (FT_activate) then
     allocate(coub(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
     if (FT_2_phases.or.FT_Delta) then
        allocate(mean_curvu(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz),&
             & mean_curvv(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz),  &
             & mean_curvP(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
        allocate(mean_stfu(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz), &
             & mean_stfv(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz),   &
             & mean_stfP(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
     endif
  end if
  !-----------------------------------------------------------------
  
  
  !------------------------------------------------------------------------------------------
  ! Lagrangian structure allocation for particles 
  !------------------------------------------------------------------------------------------
  ! Lagrangian scheme for NS equation 
  !------------------------------------------------------------------------------------------
  select case (NS_inertial_scheme)
  case(-19:-10)
     nPartV=(ex-sx+3)*(ey-sy+3)*(ez-sz+3)**(dim-2)*nppdpc**dim
     allocate(VPart(nPartV))
  end select
  !------------------------------------------------------------------------------------------
  ! Lagrangian scheme for scalar equation 
  !------------------------------------------------------------------------------------------
  select case (EN_inertial_scheme)
  case(-19:-10)
     nPartS=(ex-sx+3)*(ey-sy+3)*(ez-sz+3)**(dim-2)*nppdpc**dim
!!$     nPartS=(ex-sx+1+2*gx)*(ey-sy+1+2*gy)*(ez-sz+1+2*gz)**(dim-2)*nppdpc**dim
     allocate(SPart(nPartS))
  case(-29:-20)
     nppdpc=1
     nppdpc_min=nppdpc
     nPartS=(ex-sx+1+2*gx)*(ey-sy+1+2*gy)*(ez-sz+1+2*gz)**(dim-2)*nppdpc**dim
     allocate(SPart(nPartS))
  end select
  !------------------------------------------------------------------------------------------
  ! Lagrangian scheme for VOF 
  !------------------------------------------------------------------------------------------
  select case (VOF_method)
  case(-39:-10)
     nPartC=(ex-sx+3)*(ey-sy+3)*(ez-sz+3)**(dim-2)*nppdpc**dim
     allocate(CPart(nPartC))
  end select
  !------------------------------------------------------------------------------------------
  ! Lagrangian Particles
  !------------------------------------------------------------------------------------------
  if (LG_activate) then
     allocate(LPart(nPart))
  end if
  !------------------------------------------------------------------------------------------
  
  !------------------------------------------------------------------------------------------
  ! work array for statistics
  !------------------------------------------------------------------------------------------
!!$  allocate(work1(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
!!$  allocate(work2(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
!!$  allocate(work3(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
!!$  allocate(work4(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
!!$  allocate(work5(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
!!$  allocate(work6(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
!!$  allocate(work7(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
!!$  allocate(work8(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
!!$  !------------------------------------------------------------------------------------------
!!$  allocate(work1u(sxu-gx:exu+gx,syu-gy:eyu+gy,szu-gz:ezu+gz))
!!$  allocate(work1v(sxv-gx:exv+gx,syv-gy:eyv+gy,szv-gz:ezv+gz))
!!$  if (dim==3) allocate(work1w(sxw-gx:exw+gx,syw-gy:eyw+gy,szw-gz:ezw+gz))
  !------------------------------------------------------------------------------------------
