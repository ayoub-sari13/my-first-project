!========================================================================
!
!                   FUGU Version 1.0.0
! 
!========================================================================
!
! Copyright  Stéphane Vincent
!
! stephane.vincent@u-pem.fr          straightparadigm@gmail.com
!
! This file is part of the FUGU Fortran library, a setc of Computational 
! Fluid Dynamics subroutines devoted to the simulation of multi-scale
! multi-phase flows for resolved scale interfaces (liquid/gas/solid). 
! FUGU uses the initial developments of DyJeAT and SHALA codes from 
! Jean-Luc Estivalezes (jean-luc.estivalezes@onera.fr) and  
! Frédéric Couderc (couderc@math.univ-toulouse.fr).
!
!========================================================================
!**
!**   NAME       : SolverCommMPI
!**
!**   AUTHOR     : Stéphane Vincent
!**              : Benoit Trouette
!**              : Mohamed Elouafa
!**
!**   FUNCTION   : Soluver communication of FUGU software
!**
!**   DATES      : Version 1.0.0  : from : january, 16, 2017
!**
!========================================================================

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#include "precomp.h"
module mod_solver_comm_mpi

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

contains


  subroutine solver_comm_mpi(sol,npt,icase)
    use mod_Parameters, only: dim
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)                    :: npt,icase 
    real(8), dimension(npt), intent(inout) :: sol
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------

    if (dim==2) then
       call solver_comm_mpi2D(sol,npt,icase)
    else
       call solver_comm_mpi3D(sol,npt,icase)
    endif
  end subroutine solver_comm_mpi
  
  subroutine solver_comm_mpi3D(sol,npt,icase)
    use mod_mpi
    use mod_Parameters, only: dim,             &
         & nb_Vx,nb_Vy,nb_Vz,                  &
         & ex,sx,ey,sy,ez,sz,gx,gy,gz,         &
         & gsx,gex,gsy,gey,gsz,gez,            &
         & sxu,exu,syu,eyu,szu,ezu,            &
         & sxv,exv,syv,eyv,szv,ezv,            &
         & sxw,exw,syw,eyw,szw,ezw,            &
         & sxs,exs,sys,eys,szs,ezs,            &
         & sxus,exus,syus,eyus,szus,ezus,      &
         & sxvs,exvs,syvs,eyvs,szvs,ezvs,      &
         & sxws,exws,syws,eyws,szws,ezws,      &
         & NS_method
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)                    :: npt,icase 
    real(8), dimension(npt), intent(inout) :: sol
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                :: i,j,k,l
    integer                                :: ii,jj,kk
    real(8), allocatable, dimension(:,:,:) :: sca,u,v,w
    !-------------------------------------------------------------------------------

    select case (icase)
       !------------------------------------------
       ! nvstkes
       !------------------------------------------
    case(0)
       !------------------------------------------
       ! alloc
       !------------------------------------------
       allocate(u(sxu-gx:exu+gx,syu-gy:eyu+gy,szu-gz:ezu+gz))
       allocate(v(sxv-gx:exv+gx,syv-gy:eyv+gy,szv-gz:ezv+gz))
       allocate(w(sxw-gx:exw+gx,syw-gy:eyw+gy,szw-gz:ezw+gz))
       u=0
       v=0
       w=0
       !------------------------------------------

       !------------------------------------------
       ! sol --> u,v,w
       !------------------------------------------
       ! U component
       do kk=szus,ezus
          do jj=syus,eyus
             do ii=sxus,exus
                i=ii-sxus
                j=jj-syus
                k=kk-szus
                l=i+j*(exus-sxus+1)&
                     & +(k*(exus-sxus+1)*(eyus-syus+1)+1)**(dim-2)
                u(ii,jj,kk)=sol(l)
             enddo
          enddo
       end do
       ! V component
       do kk=szvs,ezvs
          do jj=syvs,eyvs
             do ii=sxvs,exvs
                i=ii-sxvs
                j=jj-syvs
                k=kk-szvs
                l=i+j*(exvs-sxvs+1)&
                     & +(k*(exvs-sxvs+1)*(eyvs-syvs+1)+1)**(dim-2)+nb_Vx
                v(ii,jj,kk)=sol(l)
             enddo
          enddo
       end do
       ! W component
       do kk=szws,ezws
          do jj=syws,eyws
             do ii=sxws,exws
                i=ii-sxws
                j=jj-syws
                k=kk-szws
                l=i+j*(exws-sxws+1)&
                        & +k*(exws-sxws+1)*(eyws-syws+1)+1+nb_Vx+nb_Vy
                w(ii,jj,kk)=sol(l)
             enddo
          enddo
       end do
       !------------------------------------------

       !------------------------------------------
       ! mpi exchange
       !------------------------------------------
       call comm_mpi_uvw(u,v,w)
       !------------------------------------------

       !------------------------------------------
       ! u,v,w --> sol
       !------------------------------------------
       ! U component
       do kk=szus,ezus
          do jj=syus,eyus
             do ii=sxus,exus
                i=ii-sxus
                j=jj-syus
                k=kk-szus
                l=i+j*(exus-sxus+1)&
                     & +(k*(exus-sxus+1)*(eyus-syus+1)+1)**(dim-2)
                sol(l)=u(ii,jj,kk)
             enddo
          enddo
       end do
       ! V component
       do kk=szvs,ezvs
          do jj=syvs,eyvs
             do ii=sxvs,exvs
                i=ii-sxvs
                j=jj-syvs
                k=kk-szvs
                l=i+j*(exvs-sxvs+1)&
                     & +(k*(exvs-sxvs+1)*(eyvs-syvs+1)+1)**(dim-2)+nb_Vx
                sol(l)=v(ii,jj,kk)
             enddo
          enddo
       end do
       ! W component
       do kk=szws,ezws
          do jj=syws,eyws
             do ii=sxws,exws
                i=ii-sxws
                j=jj-syws
                k=kk-szws
                l=i+j*(exws-sxws+1)&
                     & +k*(exws-sxws+1)*(eyws-syws+1)+1+nb_Vx+nb_Vy
                sol(l)=w(ii,jj,kk)
             enddo
          enddo
       end do
       !------------------------------------------

       if (NS_method==1) then
          
          !------------------------------------------
          ! alloc
          !------------------------------------------
          allocate(sca(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
          sca=0
          !------------------------------------------

          !------------------------------------------
          ! sol --> sca
          !------------------------------------------
          do kk=szs,ezs
             do jj=sys,eys
                do ii=sxs,exs
                   i=ii-sxs
                   j=jj-sys
                   k=kk-szs
                   l=i+j*(exs-sxs+1)+(k*(exs-sxs+1)*(eys-sys+1)+1)**(dim-2)+nb_Vx+nb_Vy+nb_vz
                   sca(ii,jj,kk)=sol(l)
                enddo
             enddo
          end do

          !------------------------------------------
          ! mpi exchange
          !------------------------------------------
          call comm_mpi_sca(sca)
          !------------------------------------------

          !------------------------------------------
          ! sca --> sol
          !------------------------------------------
          do kk=szs,ezs
             do jj=sys,eys
                do ii=sxs,exs
                   i=ii-sxs
                   j=jj-sys
                   k=kk-szs
                   l=i+j*(exs-sxs+1)+(k*(exs-sxs+1)*(eys-sys+1)+1)**(dim-2)+nb_Vx+nb_Vy+nb_vz
                   sol(l)=sca(ii,jj,kk)
                enddo
             enddo
          end do
          deallocate(sca)
       endif
       
       !------------------------------------------
       ! free alloc
       !------------------------------------------
       deallocate(u,v,w)
       !------------------------------------------
       
       !------------------------------------------
       ! scalar
       !------------------------------------------
    case(1)
       !------------------------------------------
       ! alloc
       !------------------------------------------
       allocate(sca(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
       sca=0
       !------------------------------------------
       
       !------------------------------------------
       ! sol --> sca
       !------------------------------------------
       do kk=szs,ezs
          do jj=sys,eys
             do ii=sxs,exs
                i=ii-sxs
                j=jj-sys
                k=kk-szs
                l=i+j*(exs-sxs+1)+(k*(exs-sxs+1)*(eys-sys+1)+1)**(dim-2)
                sca(ii,jj,kk)=sol(l)
             enddo
          enddo
       end do
       !------------------------------------------
       
       !------------------------------------------
       ! mpi exchange
       !------------------------------------------
       call comm_mpi_sca(sca)
       !------------------------------------------
       
       !------------------------------------------
       ! sca --> sol
       !------------------------------------------
       do kk=szs,ezs
          do jj=sys,eys
             do ii=sxs,exs
                i=ii-sxs
                j=jj-sys
                k=kk-szs
                l=i+j*(exs-sxs+1)+(k*(exs-sxs+1)*(eys-sys+1)+1)**(dim-2)
                sol(l)=sca(ii,jj,kk)
             enddo
          enddo
       end do
       !------------------------------------------

       !------------------------------------------
       ! alloc
       !------------------------------------------
       deallocate(sca)
       !------------------------------------------
    case(2)
       !------------------------------------------
       ! alloc
       !------------------------------------------
       allocate(u(sxu-gx:exu+gx,syu-gy:eyu+gy,szu-gz:ezu+gz))
       allocate(v(sxv-gx:exv+gx,syv-gy:eyv+gy,szv-gz:ezv+gz))
       allocate(w(sxw-gx:exw+gx,syw-gy:eyw+gy,szw-gz:ezw+gz))
       u=0
       v=0
       w=0
       !------------------------------------------
       
       !------------------------------------------
       ! sol --> u,v,w
       !------------------------------------------
       ! U component
       do kk=szus,ezus
          do jj=syus,eyus
             do ii=sxus,exus
                i=ii-sxus
                j=jj-syus
                k=kk-szus
                l=i+j*(exus-sxus+1)&
                     & +(k*(exus-sxus+1)*(eyus-syus+1)+1)**(dim-2)
                u(ii,jj,kk)=sol(l)
             enddo
          enddo
       end do
       ! V component
       do kk=szvs,ezvs
          do jj=syvs,eyvs
             do ii=sxvs,exvs
                i=ii-sxvs
                j=jj-syvs
                k=kk-szvs
                l=i+j*(exvs-sxvs+1)&
                     & +(k*(exvs-sxvs+1)*(eyvs-syvs+1)+1)**(dim-2)+nb_Vx
                v(ii,jj,kk)=sol(l)
             enddo
          enddo
       end do
       ! W component
       do kk=szws,ezws
          do jj=syws,eyws
             do ii=sxws,exws
                i=ii-sxws
                j=jj-syws
                k=kk-szws
                l=i+j*(exws-sxws+1)&
                     & +k*(exws-sxws+1)*(eyws-syws+1)+1+nb_Vx+nb_Vy
                w(ii,jj,kk)=sol(l)
             enddo
          enddo
       end do
       !------------------------------------------

       !------------------------------------------
       ! mpi exchange
       !------------------------------------------
       call comm_mpi_uvw(u,v,w)
       !------------------------------------------

       !------------------------------------------
       ! u,v,w --> sol
       !------------------------------------------
       ! U component
       do kk=szus,ezus
          do jj=syus,eyus
             do ii=sxus,exus
                i=ii-sxus
                j=jj-syus
                k=kk-szus
                l=i+j*(exus-sxus+1)&
                     & +(k*(exus-sxus+1)*(eyus-syus+1)+1)**(dim-2)
                sol(l)=u(ii,jj,kk)
             enddo
          enddo
       end do
       ! V component
       do kk=szvs,ezvs
          do jj=syvs,eyvs
             do ii=sxvs,exvs
                i=ii-sxvs
                j=jj-syvs
                k=kk-szvs
                l=i+j*(exvs-sxvs+1)&
                     & +(k*(exvs-sxvs+1)*(eyvs-syvs+1)+1)**(dim-2)+nb_Vx
                sol(l)=v(ii,jj,kk)
             enddo
          enddo
       end do
       ! W component
       do kk=szws,ezws
          do jj=syws,eyws
             do ii=sxws,exws
                i=ii-sxws
                j=jj-syws
                k=kk-szws
                l=i+j*(exws-sxws+1)&
                     & +k*(exws-sxws+1)*(eyws-syws+1)+1+nb_Vx+nb_Vy
                sol(l)=w(ii,jj,kk)
             enddo
          enddo
       end do
       !------------------------------------------

       !------------------------------------------
       ! free alloc
       !------------------------------------------
       deallocate(u,v,w)
       
    case(3)

       !------------------------------------------
       ! alloc
       !------------------------------------------
       allocate(v(sxv-gx:exv+gx,syv-gy:eyv+gy,szv-gz:ezv+gz))
       v=0
       !------------------------------------------

       !------------------------------------------
       ! sol --> v
       !------------------------------------------

       ! V component
       do kk=szvs,ezvs
          do jj=syvs,eyvs
             do ii=sxvs,exvs
                i=ii-sxvs
                j=jj-syvs
                k=kk-szvs
                l=i+j*(exvs-sxvs+1)&
                     & +(k*(exvs-sxvs+1)*(eyvs-syvs+1)+1)**(dim-2)
                v(ii,jj,kk)=sol(l)
             enddo
          enddo
       end do
       !------------------------------------------

       !------------------------------------------
       ! mpi exchange
       !------------------------------------------
       call comm_mpi_v(v)
       !------------------------------------------
       
       !------------------------------------------
       ! v --> sol
       !------------------------------------------
       
       ! V component
       do kk=szvs,ezvs
          do jj=syvs,eyvs
             do ii=sxvs,exvs
                i=ii-sxvs
                j=jj-syvs
                k=kk-szvs
                l=i+j*(exvs-sxvs+1)&
                     & +(k*(exvs-sxvs+1)*(eyvs-syvs+1)+1)**(dim-2)
                sol(l)=v(ii,jj,kk)
             enddo
          enddo
       end do

       !------------------------------------------
       ! free alloc
       !------------------------------------------
       deallocate(v)

    case(4)
       
       !------------------------------------------
       ! alloc
       !------------------------------------------
       allocate(w(sxw-gx:exw+gx,syw-gy:eyw+gy,szw-gz:ezw+gz))
       w=0
       !------------------------------------------

       ! W component
       do kk=szws,ezws
          do jj=syws,eyws
             do ii=sxws,exws
                i=ii-sxws
                j=jj-syws
                k=kk-szws
                l=i+j*(exws-sxws+1)&
                     & +k*(exws-sxws+1)*(eyws-syws+1)+1
                w(ii,jj,kk)=sol(l)
             enddo
          enddo
       end do
       !------------------------------------------

       !------------------------------------------
       ! mpi exchange
       !------------------------------------------
       call comm_mpi_w(w)
       !------------------------------------------

       !------------------------------------------
       ! w --> sol
       !------------------------------------------

       ! W component
       do kk=szws,ezws
          do jj=syws,eyws
             do ii=sxws,exws
                i=ii-sxws
                j=jj-syws
                k=kk-szws
                l=i+j*(exws-sxws+1)&
                     & +k*(exws-sxws+1)*(eyws-syws+1)+1
                sol(l)=w(ii,jj,kk)
             enddo
          enddo
       end do
       !------------------------------------------

       !------------------------------------------
       ! free alloc
       !------------------------------------------
       deallocate(w)
       
    case default
       !write(*,*) "STOP, Error mpi_ieq_solve =", icase
       stop
    end select
      
    return
  end subroutine solver_comm_mpi3D
  
  subroutine solver_comm_mpi2D(sol,npt,icase)
    use mod_mpi
    use mod_Parameters, only: dim,nb_Vx,nb_Vy, &
         & ex,sx,ey,sy,ez,sz,gx,gy,gz,         &
         & gsx,gex,gsy,gey,gsz,gez,            &
         & sxu,exu,syu,eyu,szu,ezu,            &
         & sxv,exv,syv,eyv,szv,ezv,            &
         & sxw,exw,syw,eyw,szw,ezw,            &
         & sxs,exs,sys,eys,szs,ezs,            &
         & sxus,exus,syus,eyus,szus,ezus,      &
         & sxvs,exvs,syvs,eyvs,szvs,ezvs,      &
         & sxws,exws,syws,eyws,szws,ezws,      &
         & NS_method
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)                    :: npt,icase 
    real(8), dimension(npt), intent(inout) :: sol
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                :: i,j,k,l
    integer                                :: ii,jj,kk
    real(8), allocatable, dimension(:,:,:) :: sca,u,v,w
    !-------------------------------------------------------------------------------

    select case (icase)
       !------------------------------------------
       ! nvstkes
       !------------------------------------------
    case(0)
       !------------------------------------------
       ! alloc
       !------------------------------------------
       allocate(u(sxu-gx:exu+gx,syu-gy:eyu+gy,szu-gz:ezu+gz))
       allocate(v(sxv-gx:exv+gx,syv-gy:eyv+gy,szv-gz:ezv+gz))
       u=0
       v=0
       !------------------------------------------

       !------------------------------------------
       ! sol --> u,v,w
       !------------------------------------------
       ! U component
       do kk=szus,ezus
          do jj=syus,eyus
             do ii=sxus,exus
                i=ii-sxus
                j=jj-syus
                k=kk-szus
                l=i+j*(exus-sxus+1)&
                     & +(k*(exus-sxus+1)*(eyus-syus+1)+1)**(dim-2)
                u(ii,jj,kk)=sol(l)
             enddo
          enddo
       end do
       ! V component
       do kk=szvs,ezvs
          do jj=syvs,eyvs
             do ii=sxvs,exvs
                i=ii-sxvs
                j=jj-syvs
                k=kk-szvs
                l=i+j*(exvs-sxvs+1)&
                     & +(k*(exvs-sxvs+1)*(eyvs-syvs+1)+1)**(dim-2)+nb_Vx
                v(ii,jj,kk)=sol(l)
             enddo
          enddo
       end do
       !------------------------------------------
       
       !------------------------------------------
       ! mpi exchange
       !------------------------------------------
       call comm_mpi_uvw(u,v,w)
       !------------------------------------------

       !------------------------------------------
       ! u,v,w --> sol
       !------------------------------------------
       ! U component
       do kk=szus,ezus
          do jj=syus,eyus
             do ii=sxus,exus
                i=ii-sxus
                j=jj-syus
                k=kk-szus
                l=i+j*(exus-sxus+1)&
                     & +(k*(exus-sxus+1)*(eyus-syus+1)+1)**(dim-2)
                sol(l)=u(ii,jj,kk)
             enddo
          enddo
       end do
       ! V component
       do kk=szvs,ezvs
          do jj=syvs,eyvs
             do ii=sxvs,exvs
                i=ii-sxvs
                j=jj-syvs
                k=kk-szvs
                l=i+j*(exvs-sxvs+1)&
                     & +(k*(exvs-sxvs+1)*(eyvs-syvs+1)+1)**(dim-2)+nb_Vx
                sol(l)=v(ii,jj,kk)
             enddo
          enddo
       end do
       !------------------------------------------

       if (NS_method==1) then
          
          !------------------------------------------
          ! alloc
          !------------------------------------------
          allocate(sca(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
          sca=0
          !------------------------------------------

          !------------------------------------------
          ! sol --> sca
          !------------------------------------------
          do kk=szs,ezs
             do jj=sys,eys
                do ii=sxs,exs
                   i=ii-sxs
                   j=jj-sys
                   k=kk-szs
                   l=i+j*(exs-sxs+1)+(k*(exs-sxs+1)*(eys-sys+1)+1)**(dim-2)+nb_Vx+nb_Vy
                   sca(ii,jj,kk)=sol(l)
                enddo
             enddo
          end do
          
          !------------------------------------------
          ! mpi exchange
          !------------------------------------------
          call comm_mpi_sca(sca)
          !------------------------------------------

          !------------------------------------------
          ! sca --> sol
          !------------------------------------------
          do kk=szs,ezs
             do jj=sys,eys
                do ii=sxs,exs
                   i=ii-sxs
                   j=jj-sys
                   k=kk-szs
                   l=i+j*(exs-sxs+1)+(k*(exs-sxs+1)*(eys-sys+1)+1)**(dim-2)+nb_Vx+nb_Vy
                   sol(l)=sca(ii,jj,kk)
                enddo
             enddo
          end do
          
          deallocate(sca)
       endif
       
       !------------------------------------------
       ! free alloc
       !------------------------------------------
       deallocate(u,v)
       !------------------------------------------
       
       !------------------------------------------
       ! scalar
       !------------------------------------------
       
       !------------------------------------------
       ! scalar
       !------------------------------------------
    case(1)
       !------------------------------------------
       ! alloc
       !------------------------------------------
       allocate(sca(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
       sca=0
       !------------------------------------------

       !------------------------------------------
       ! sol --> sca
       !------------------------------------------
       do kk=szs,ezs
          do jj=sys,eys
             do ii=sxs,exs
                i=ii-sxs
                j=jj-sys
                k=kk-szs
                l=i+j*(exs-sxs+1)+(k*(exs-sxs+1)*(eys-sys+1)+1)**(dim-2)
                sca(ii,jj,kk)=sol(l)
             enddo
          enddo
       end do
       !------------------------------------------

       !------------------------------------------
       ! mpi exchange
       !------------------------------------------
       call comm_mpi_sca(sca)
       !------------------------------------------

       !------------------------------------------
       ! sca --> sol
       !------------------------------------------
       do kk=szs,ezs
          do jj=sys,eys
             do ii=sxs,exs
                i=ii-sxs
                j=jj-sys
                k=kk-szs
                l=i+j*(exs-sxs+1)+(k*(exs-sxs+1)*(eys-sys+1)+1)**(dim-2)
                sol(l)=sca(ii,jj,kk)
             enddo
          enddo
       end do
       !------------------------------------------

       !------------------------------------------
       ! alloc
       !------------------------------------------
       deallocate(sca)
       !------------------------------------------
       
    case(2)
       !------------------------------------------
       ! alloc
       !------------------------------------------
       allocate(u(sxu-gx:exu+gx,syu-gy:eyu+gy,szu-gz:ezu+gz))
       allocate(v(sxv-gx:exv+gx,syv-gy:eyv+gy,szv-gz:ezv+gz))
       u=0
       v=0
       !------------------------------------------

       !------------------------------------------
       ! sol --> u,v,w
       !------------------------------------------
       ! U component
       do kk=szus,ezus
          do jj=syus,eyus
             do ii=sxus,exus
                i=ii-sxus
                j=jj-syus
                k=kk-szus
                l=i+j*(exus-sxus+1)&
                     & +(k*(exus-sxus+1)*(eyus-syus+1)+1)**(dim-2)
                u(ii,jj,kk)=sol(l)
             enddo
          enddo
       end do
       ! V component
       do kk=szvs,ezvs
          do jj=syvs,eyvs
             do ii=sxvs,exvs
                i=ii-sxvs
                j=jj-syvs
                k=kk-szvs
                l=i+j*(exvs-sxvs+1)&
                     & +(k*(exvs-sxvs+1)*(eyvs-syvs+1)+1)**(dim-2)+nb_Vx
                v(ii,jj,kk)=sol(l)
             enddo
          enddo
       end do
       !------------------------------------------

       !------------------------------------------
       ! mpi exchange
       !------------------------------------------
       call comm_mpi_uvw(u,v,w)
       !------------------------------------------

       !------------------------------------------
       ! u,v,w --> sol
       !------------------------------------------
       ! U component
       do kk=szus,ezus
          do jj=syus,eyus
             do ii=sxus,exus
                i=ii-sxus
                j=jj-syus
                k=kk-szus
                l=i+j*(exus-sxus+1)&
                     & +(k*(exus-sxus+1)*(eyus-syus+1)+1)**(dim-2)
                sol(l)=u(ii,jj,kk)
             enddo
          enddo
       end do
       ! V component
       do kk=szvs,ezvs
          do jj=syvs,eyvs
             do ii=sxvs,exvs
                i=ii-sxvs
                j=jj-syvs
                k=kk-szvs
                l=i+j*(exvs-sxvs+1)&
                     & +(k*(exvs-sxvs+1)*(eyvs-syvs+1)+1)**(dim-2)+nb_Vx
                sol(l)=v(ii,jj,kk)
             enddo
          enddo
       end do
       !------------------------------------------

       !------------------------------------------
       ! free alloc
       !------------------------------------------
       deallocate(u,v)
       
    case(3)

       allocate(u(sxu-gx:exu+gx,syu-gy:eyu+gy,szu-gz:ezu+gz))
       allocate(v(sxv-gx:exv+gx,syv-gy:eyv+gy,szv-gz:ezv+gz))
       u=0
       v=0
       !------------------------------------------
       ! alloc
       !------------------------------------------

       !------------------------------------------
       ! sol --> v
       !------------------------------------------

       ! V component
       do kk=szvs,ezvs
          do jj=syvs,eyvs
             do ii=sxvs,exvs
                i=ii-sxvs
                j=jj-syvs
                k=kk-szvs
                l=i+j*(exvs-sxvs+1)&
                     & +(k*(exvs-sxvs+1)*(eyvs-syvs+1)+1)**(dim-2)
                v(ii,jj,kk)=sol(l)
             enddo
          enddo
       end do
       !------------------------------------------
       
       !------------------------------------------
       ! mpi exchange
       !------------------------------------------
       call comm_mpi_uvw(u,v,w)
       !------------------------------------------
       
       !------------------------------------------
       ! v --> sol
       !------------------------------------------
       
       ! V component
       do kk=szvs,ezvs
          do jj=syvs,eyvs
             do ii=sxvs,exvs
                i=ii-sxvs
                j=jj-syvs
                k=kk-szvs
                l=i+j*(exvs-sxvs+1)&
                     & +(k*(exvs-sxvs+1)*(eyvs-syvs+1)+1)**(dim-2)
                sol(l)=v(ii,jj,kk)
             enddo
          enddo
       end do
       
       !------------------------------------------
       ! free alloc
       !------------------------------------------
       deallocate(u,v)
       
    case default
       !write(*,*) "STOP, Error mpi_ieq_solve =", icase
       stop
    end select
    
    return
  end subroutine solver_comm_mpi2D


  
  
  
!!$  subroutine solver_comm_mpi(sol,npt,icase)
!!$    use mod_mpi
!!$    use mod_Parameters, only: dim,nb_Vx,nb_Vy, &
!!$         & ex,sx,ey,sy,ez,sz,gx,gy,gz,         &
!!$         & gsx,gex,gsy,gey,gsz,gez,            &
!!$         & sxu,exu,syu,eyu,szu,ezu,            &
!!$         & sxv,exv,syv,eyv,szv,ezv,            &
!!$         & sxw,exw,syw,eyw,szw,ezw,            &
!!$         & sxs,exs,sys,eys,szs,ezs,            &
!!$         & sxus,exus,syus,eyus,szus,ezus,      &
!!$         & sxvs,exvs,syvs,eyvs,szvs,ezvs,      &
!!$         & sxws,exws,syws,eyws,szws,ezws
!!$    implicit none
!!$    !-------------------------------------------------------------------------------
!!$    ! Global variables
!!$    !-------------------------------------------------------------------------------
!!$    integer, intent(in)                    :: npt,icase 
!!$    real(8), dimension(npt), intent(inout) :: sol
!!$    !-------------------------------------------------------------------------------
!!$    ! Local variables
!!$    !-------------------------------------------------------------------------------
!!$    integer                                :: i,j,k,l
!!$    integer                                :: ii,jj,kk
!!$    real(8), allocatable, dimension(:,:,:) :: sca,u,v,w
!!$    !-------------------------------------------------------------------------------
!!$
!!$    select case (icase)
!!$       !------------------------------------------
!!$       ! nvstkes
!!$       !------------------------------------------
!!$    case(0)
!!$       !------------------------------------------
!!$       ! alloc
!!$       !------------------------------------------
!!$       allocate(u(sxu-gx:exu+gx,syu-gy:eyu+gy,szu-gz:ezu+gz))
!!$       allocate(v(sxv-gx:exv+gx,syv-gy:eyv+gy,szv-gz:ezv+gz))
!!$       if (dim==3) allocate(w(sxw-gx:exw+gx,syw-gy:eyw+gy,szw-gz:ezw+gz))
!!$       u=0
!!$       v=0
!!$       if (dim==3) w=0
!!$       !------------------------------------------
!!$
!!$       !------------------------------------------
!!$       ! sol --> u,v,w
!!$       !------------------------------------------
!!$       ! U component
!!$       do kk=szus,ezus
!!$          do jj=syus,eyus
!!$             do ii=sxus,exus
!!$                i=ii-sxus
!!$                j=jj-syus
!!$                k=kk-szus
!!$                l=i+j*(exus-sxus+1)&
!!$                     & +(k*(exus-sxus+1)*(eyus-syus+1)+1)**(dim-2)
!!$                u(ii,jj,kk)=sol(l)
!!$             enddo
!!$          enddo
!!$       end do
!!$       ! V component
!!$       do kk=szvs,ezvs
!!$          do jj=syvs,eyvs
!!$             do ii=sxvs,exvs
!!$                i=ii-sxvs
!!$                j=jj-syvs
!!$                k=kk-szvs
!!$                l=i+j*(exvs-sxvs+1)&
!!$                     & +(k*(exvs-sxvs+1)*(eyvs-syvs+1)+1)**(dim-2)+nb_Vx
!!$                v(ii,jj,kk)=sol(l)
!!$             enddo
!!$          enddo
!!$       end do
!!$       ! W component
!!$       if (dim==3) then 
!!$          do kk=szws,ezws
!!$             do jj=syws,eyws
!!$                do ii=sxws,exws
!!$                   i=ii-sxws
!!$                   j=jj-syws
!!$                   k=kk-szws
!!$                   l=i+j*(exws-sxws+1)&
!!$                        & +k*(exws-sxws+1)*(eyws-syws+1)+1+nb_Vx+nb_Vy
!!$                   w(ii,jj,kk)=sol(l)
!!$                enddo
!!$             enddo
!!$          end do
!!$       end if
!!$       !------------------------------------------
!!$
!!$       !------------------------------------------
!!$       ! mpi exchange
!!$       !------------------------------------------
!!$       call comm_mpi_uvw(u,v,w)
!!$       !------------------------------------------
!!$
!!$       !------------------------------------------
!!$       ! u,v,w --> sol
!!$       !------------------------------------------
!!$       ! U component
!!$       do kk=szus,ezus
!!$          do jj=syus,eyus
!!$             do ii=sxus,exus
!!$                i=ii-sxus
!!$                j=jj-syus
!!$                k=kk-szus
!!$                l=i+j*(exus-sxus+1)&
!!$                     & +(k*(exus-sxus+1)*(eyus-syus+1)+1)**(dim-2)
!!$                sol(l)=u(ii,jj,kk)
!!$             enddo
!!$          enddo
!!$       end do
!!$       ! V component
!!$       do kk=szvs,ezvs
!!$          do jj=syvs,eyvs
!!$             do ii=sxvs,exvs
!!$                i=ii-sxvs
!!$                j=jj-syvs
!!$                k=kk-szvs
!!$                l=i+j*(exvs-sxvs+1)&
!!$                     & +(k*(exvs-sxvs+1)*(eyvs-syvs+1)+1)**(dim-2)+nb_Vx
!!$                sol(l)=v(ii,jj,kk)
!!$             enddo
!!$          enddo
!!$       end do
!!$       ! W component
!!$       if (dim==3) then 
!!$          do kk=szws,ezws
!!$             do jj=syws,eyws
!!$                do ii=sxws,exws
!!$                   i=ii-sxws
!!$                   j=jj-syws
!!$                   k=kk-szws
!!$                   l=i+j*(exws-sxws+1)&
!!$                        & +k*(exws-sxws+1)*(eyws-syws+1)+1+nb_Vx+nb_Vy
!!$                   sol(l)=w(ii,jj,kk)
!!$                enddo
!!$             enddo
!!$          end do
!!$       end if
!!$       !------------------------------------------
!!$
!!$       !------------------------------------------
!!$       ! free alloc
!!$       !------------------------------------------
!!$       deallocate(u,v)
!!$       if (dim==3) deallocate(w)
!!$       !------------------------------------------
!!$
!!$       !------------------------------------------
!!$       ! scalar
!!$       !------------------------------------------
!!$    case(1)
!!$       !------------------------------------------
!!$       ! alloc
!!$       !------------------------------------------
!!$       allocate(sca(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
!!$       sca=0
!!$       !------------------------------------------
!!$
!!$       !------------------------------------------
!!$       ! sol --> sca
!!$       !------------------------------------------
!!$       do kk=szs,ezs
!!$          do jj=sys,eys
!!$             do ii=sxs,exs
!!$                i=ii-sxs
!!$                j=jj-sys
!!$                k=kk-szs
!!$                l=i+j*(exs-sxs+1)+(k*(exs-sxs+1)*(eys-sys+1)+1)**(dim-2)
!!$                sca(ii,jj,kk)=sol(l)
!!$             enddo
!!$          enddo
!!$       end do
!!$       !------------------------------------------
!!$
!!$       !------------------------------------------
!!$       ! mpi exchange
!!$       !------------------------------------------
!!$       call comm_mpi_sca(sca)
!!$       !------------------------------------------
!!$
!!$       !------------------------------------------
!!$       ! sca --> sol
!!$       !------------------------------------------
!!$       do kk=szs,ezs
!!$          do jj=sys,eys
!!$             do ii=sxs,exs
!!$                i=ii-sxs
!!$                j=jj-sys
!!$                k=kk-szs
!!$                l=i+j*(exs-sxs+1)+(k*(exs-sxs+1)*(eys-sys+1)+1)**(dim-2)
!!$                sol(l)=sca(ii,jj,kk)
!!$             enddo
!!$          enddo
!!$       end do
!!$       !------------------------------------------
!!$
!!$       !------------------------------------------
!!$       ! alloc
!!$       !------------------------------------------
!!$       deallocate(sca)
!!$       !------------------------------------------
!!$
!!$    case default
!!$       write(*,*) "STOP, Error mpi_ieq_solve =", icase
!!$       stop
!!$    end select
!!$
!!$    return
!!$  end subroutine solver_comm_mpi



end module mod_solver_comm_mpi
