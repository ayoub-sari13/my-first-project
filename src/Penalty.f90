!========================================================================
!
!                   FUGU Version 1.0.0
!
!========================================================================
!
! Copyright  Stéphane Vincent
!
! stephane.vincent@u-pem.fr          straightparadigm@gmail.com
!
! This file is part of the FUGU Fortran library, a set of Computational 
! Fluid Dynamics subroutines devoted to the simulation of multi-scale
! multi-phase flows for resolved scale interfaces (liquid/gas/solid). 
! FUGU uses the initial developments of DyJeAT and SHALA codes from 
! Jean-Luc Estivalezes (jean-luc.estivalezes@onera.fr) and 
! Frédéric Couderc (couderc@math.univ-toulouse.fr).
!
!========================================================================
!**
!**   NAME       : Penalty.f90
!**
!**   AUTHOR     : Stéphane Vincent
!**
!**   FUNCTION   : subroutines for penalty methods of first order
!**
!**   DATES      : Version 1.0.0  : from : feb, 13, 2018
!**
!========================================================================

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
module mod_pen
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  !*******************************************************************************!
  !   Modules                                                                 
  !*******************************************************************************!
  use mod_Parameters, only: rank,nproc,nstep,restart_EQ, &
       & dim,xmin,xmax,ymin,ymax,zmin,zmax,              &
       & sxu,exu,syu,eyu,szu,ezu,                        &
       & sxv,exv,syv,eyv,szv,ezv,                        &
       & sxw,exw,syw,eyw,szw,ezw,                        &
       & dx,dy,dz,dx2,dy2,dz2,dxu,dyv,dzw,pi,dpi,        &
       & NS_ipen,NS_pen,EN_ipen,EN_pen,                  &
       & grid_x,grid_y,grid_z,grid_xu,grid_yv,grid_zw
  use mod_Constants, only: d1p2,d1p4,d1p5,d1p7,d3p4
  use mod_Init_Navier
  use mod_coeffvs
  use mod_geometry
  use mod_mpi
  use mod_InOut
  !*******************************************************************************!

  real(8), dimension(:,:,:), allocatable :: stdy_smvu,stdy_smvv,stdy_smvw
  real(8), dimension(:,:,:), allocatable :: stdy_slvu,stdy_slvv,stdy_slvw
  real(8), dimension(:,:,:), allocatable :: tmp_condu,tmp_condv,tmp_condw
  
  
  real(8) :: x_max_extraction,x_min_extraction
  real(8) :: y_max_extraction,y_min_extraction
  real(8) :: z_max_extraction,z_min_extraction
  real(8) :: x_max_injection,x_min_injection
  real(8) :: y_max_injection,y_min_injection
  real(8) :: z_max_injection,z_min_injection
  real(8) :: x0_sorbonne
  
  


contains

  !*******************************************************************************!
  !*******************************************************************************!
  !*******************************************************************************!
  ! Penalty method for Navier-Stokes equations
  !*******************************************************************************!
  !*******************************************************************************!
  !*******************************************************************************!
  subroutine penalty_NS(time,dt,cou,smvu,smvv,smvw,slvu,slvv,slvw,uin,vin,win,penu,penv,penw,obj)
    !*******************************************************************************!
    implicit none
    !---------------------------------------------------------------------
    ! Global variables                                            
    !---------------------------------------------------------------------
    real(8),                                  intent(in)              :: time,dt
    real(8), dimension(:,:,:),   allocatable, intent(inout)           :: cou
    real(8), dimension(:,:,:),   allocatable, intent(inout)           :: smvu,smvv,smvw
    real(8), dimension(:,:,:),   allocatable, intent(inout)           :: slvu,slvv,slvw
    real(8), dimension(:,:,:),   allocatable, intent(inout), optional :: uin,vin,win
    real(8), dimension(:,:,:,:), allocatable, intent(inout), optional :: penu,penv,penw
    real(8), dimension(:,:,:),   allocatable, intent(inout), optional :: obj 
    !---------------------------------------------------------------------
    ! Local variables                                             
    !---------------------------------------------------------------------
    integer                                                           :: i,j,k,n,ierr
    integer                                                           :: imin,imax,jmin,jmax,kmin,kmax
    integer                                                           :: is,ie,js,je,ks,ke
    real(8)                                                           :: x,y,z,xc,yc,zc,rc,rc2,rad2
    real(8)                                                           :: a,b,x1,x0,y1,y0,z1,z0,alpha
    real(8)                                                           :: f,uc,vc
    real(8)                                                           :: amp,uinf,vinf,winf,qinf,qtot
    real(8)                                                           :: surfu,surfv,surfw
    real(8)                                                           :: tmp
    real(8)                                                           :: pen_amp,ipen_amp
    real(8)                                                           :: x_max,x_min,y_max,y_min,z_max,z_min
    real(8)                                                           :: x_start,x_end
    real(8), save                                                     :: local_time
    logical, save                                                     :: once=.true.
    !---------------------------------------------------------------------
    real(8)                                                           :: l_sorbonne,l_obstacle
    real(8)                                                           :: q_sorbonne,q_gaz
    real(8)                                                           :: t_stop
    logical                                                           :: print_info=.true.
    !---------------------------------------------------------------------

    !---------------------------------------------------
    ! Initialization
    !---------------------------------------------------
    pen_amp=NS_pen
    ipen_amp=1/NS_pen
    !---------------------------------------------------

    !---------------------------------------------------
    ! default case
    !---------------------------------------------------
    if (allocated(smvu)) smvu=0
    if (allocated(smvu)) smvv=0
    slvu=0; slvv=0;
    if (dim==3) then
       if (allocated(smvw)) smvw=0
       slvw=0
    endif
    !---------------------------------------------------

    !---------------------------------------------------------------------
    ! parameters
    !---------------------------------------------------------------------
    select case(NS_ipen)
    case(1)
       rc=0.1d0
       rc2=rc*rc
       xc=d1p2*(xmax+xmin)
       yc=d1p2*(ymax+ymin)
    case(4)
       rc=5d-2
       rc2=rc*rc
       xc=2d-1
       yc=2d-1
    end select
    !---------------------------------------------------------------------


    select case(NS_ipen)
       !---------------------------------------------------------------------
       ! Nothing
       !---------------------------------------------------------------------
    case(0)
       !---------------------------------------------------------------------
       ! Fixed circular obstacle
       !---------------------------------------------------------------------
    case(1,4)

       !---------------------------------------------------------------------
       ! U component
       !---------------------------------------------------------------------
       do j=syu,eyu
          do i=sxu,exu
             x=grid_xu(i)
             y=grid_y(j)
             if ((x-xc)**2+(y-yc)**2 <= rc2) slvu(i,j,1)=pen_amp
          enddo
       enddo
       !---------------------------------------------------------------------

       !---------------------------------------------------------------------
       ! V component 
       !---------------------------------------------------------------------
       do j=syv,eyv
          do i=sxv,exv
             x=grid_x(i)
             y=grid_yv(j)
             if ((x-xc)**2+(y-yc)**2 <= rc2) slvv(i,j,1)=pen_amp
          enddo
       enddo
       !---------------------------------------------------------------------

       !---------------------------------------------------------------------
       ! Rotating circular obstacle in driven cavity flow
       !---------------------------------------------------------------------
    case(2)
       !---------------------------------------------------------------------
       ! parameters
       !---------------------------------------------------------------------
       amp=1000
       rc=0.1d0
       rc2=rc*rc
       xc=d1p2*(xmax+xmin)
       yc=d1p2*(ymax+ymin)
       !---------------------------------------------------------------------

       !---------------------------------------------------------------------
       ! U component
       !---------------------------------------------------------------------
       do j=syu,eyu
          do i=sxu,exu
             x=grid_xu(i)
             y=grid_y(j)
             rad2=(x-xc)**2+(y-yc)**2
             if (rad2 <= rc2) then
                slvu(i,j,1)=pen_amp
                if (x-xc>=0 .and. y-yc>=0 ) then 
                   uinf=amp*sqrt(rad2)*sin(-acos((x-xc)/sqrt(rad2+ipen_amp)))
                else if (x-xc<0 .and. y-yc>=0 ) then 
                   uinf=amp*sqrt(rad2)*sin(-acos((x-xc)/sqrt(rad2+ipen_amp)))
                else if (x-xc<0 .and. y-yc<0 ) then 
                   uinf=amp*sqrt(rad2)*sin(acos((x-xc)/sqrt(rad2+ipen_amp)))
                else
                   uinf=amp*sqrt(rad2)*sin(acos((x-xc)/sqrt(rad2+ipen_amp)))       
                endif
                smvu(i,j,1)=pen_amp*uinf
             endif
          enddo
       enddo
       !---------------------------------------------------------------------

       !---------------------------------------------------------------------
       ! V component
       !---------------------------------------------------------------------
       do j=syv,eyv
          do i=sxv,exv
             x=grid_x(i)
             y=grid_yv(j)
             rad2=(x-xc)**2+(y-yc)**2
             if (rad2 <= rc2) then
                slvv(i,j,1)=pen_amp
                vinf=amp*sqrt(rad2)*cos(acos((x-xc)/sqrt(rad2+ipen_amp)))
                smvv(i,j,1)=pen_amp*vinf
             endif
          enddo
       enddo
       !---------------------------------------------------------------------

       !---------------------------------------------------------------------
       ! Wannier flow 2D
       !---------------------------------------------------------------------
    case(3)
       !---------------------------------------------------------------------
       ! parameters
       !---------------------------------------------------------------------
       amp=1
       rc2=(d1p7*min(xmax-xmin,ymax-ymin))**2
       xc=0
       yc=0
       !---------------------------------------------------------------------

       !---------------------------------------------------------------------
       ! U component
       !---------------------------------------------------------------------
       do j=syu,eyu
          do i=sxu,exu
             x=grid_xu(i)
             y=grid_y(j)
             rad2=(x-xc)**2+(y-yc)**2
             if (rad2 <= rc2) then
                slvu(i,j,1)=pen_amp
                if (x-xc>=0 .and. y-yc>=0) then 
                   uinf=amp*sqrt(rad2)*sin(-acos((x-xc)/sqrt(rad2+ipen_amp)))
                else if (x-xc<0 .and. y-yc>=0) then 
                   uinf=amp*sqrt(rad2)*sin(-acos((x-xc)/sqrt(rad2+ipen_amp)))
                else if (x-xc<0 .and. y-yc<0) then 
                   uinf=amp*sqrt(rad2)*sin(acos((x-xc)/sqrt(rad2+ipen_amp)))
                else
                   uinf=amp*sqrt(rad2)*sin(acos((x-xc)/sqrt(rad2+ipen_amp)))
                endif
                smvu(i,j,1)=pen_amp*uinf
             endif
          enddo
       enddo
       !---------------------------------------------------------------------

       !---------------------------------------------------------------------
       ! V component
       !---------------------------------------------------------------------
       do j=syv,eyv
          do i=sxv,exv
             x=grid_x(i)
             y=grid_yv(j)
             rad2=(x-xc)**2+(y-yc)**2
             if (rad2 <= rc2) then
                slvv(i,j,1)=pen_amp
                vinf=amp*sqrt(rad2)*cos(acos((x-xc)/sqrt(rad2+ipen_amp)))
                smvv(i,j,1)=pen_amp*vinf
             endif
          enddo
       enddo
       !---------------------------------------------------------------------
    case(11)
       !---------------------------------------------------------------------
       ! U component
       !---------------------------------------------------------------------
       do k=szu,ezu
          do j=syu,eyu
             do i=sxu,exu
                x=grid_xu(i)
                rc2=(grid_y(j)-bc_ns%left%xyz(2))**2+(grid_z(k)-bc_ns%left%xyz(3))**2
                if (x<=0.and.rc2>bc_ns%left%r**2) then
                   slvu(i,j,k)=pen_amp
                   smvu(i,j,k)=0
                end if
             enddo
          enddo
       end do
       !---------------------------------------------------------------------
       ! V component
       !---------------------------------------------------------------------
       do k=szv,ezv
          do j=syv,eyv
             do i=sxv,exv
                x=grid_x(i)
                rc2=(grid_y(j)-bc_ns%left%xyz(2))**2+(grid_z(k)-bc_ns%left%xyz(3))**2
                if (x<=0.and.rc2>bc_ns%left%r**2) then
                   slvv(i,j,k)=pen_amp
                   smvv(i,j,k)=0
                end if
             enddo
          enddo
       end do
       !---------------------------------------------------------------------
       ! W component
       !---------------------------------------------------------------------
       do k=szw,ezw
          do j=syw,eyw
             do i=sxw,exw
                x=grid_x(i)
                rc2=(grid_y(j)-bc_ns%left%xyz(2))**2+(grid_z(k)-bc_ns%left%xyz(3))**2
                if (x<=0.and.rc2>bc_ns%left%r**2) then
                   slvw(i,j,k)=pen_amp
                   smvw(i,j,k)=0
                end if
             enddo
          enddo
       end do
       !---------------------------------------------------------------------
    case(12)
       !---------------------------------------------------------------------
       ! 2D / slope for wave running up
       !---------------------------------------------------------------------
       alpha=5
       y0=0
       x0=8
       x1=9
       a=1d0/15
       y1=a*(x1-x0)+y0
       !y1=tan(alpha*pi/180)*(x1-x0)+y0
       a=(y1-y0)/(x1-x0)
       b=y0-a*x0
       !---------------------------------------------------------------------
       ! U component
       !---------------------------------------------------------------------
       do k=szu,ezu
          do j=syu,eyu
             do i=sxu,exu
                x=grid_xu(i)
                y=grid_y(j)
                if (y<=a*x+b) then
                   slvu(i,j,k)=pen_amp
                end if
             enddo
          enddo
       end do
       !---------------------------------------------------------------------
       ! V component
       !---------------------------------------------------------------------
       do k=szv,ezv
          do j=syv,eyv
             do i=sxv,exv
                x=grid_x(i)
                y=grid_yv(j)
                if (y<=a*x+b) then
                   slvv(i,j,k)=pen_amp
                end if
             enddo
          enddo
       end do
       !---------------------------------------------------------------------
    case(13)
       !---------------------------------------------------------------------
       ! Benchmark VK Cylinder
       !---------------------------------------------------------------------
       rc=0.05d0
       rc2=rc*rc
       xc=xmin+0.15d0+rc
       yc=ymin+0.15d0+rc
       if (rank==0) then
          write(*,*) "------------------------------------------------------"
          write(*,*) "Case NS_ipen     =", NS_ipen
          write(*,*) "radius rc        =",rc
          write(*,*) "position (,y)    =",xc,yc
          write(*,*) "------------------------------------------------------"
       end if
       !---------------------------------------------------------------------
       ! U component
       !---------------------------------------------------------------------
       do j=syu,eyu
          do i=sxu,exu
             x=grid_xu(i)
             y=grid_y(j)
             if ((x-xc)**2+(y-yc)**2 <= rc2) slvu(i,j,1)=pen_amp
          enddo
       enddo
       !---------------------------------------------------------------------
       ! V component 
       !---------------------------------------------------------------------
       do j=syv,eyv
          do i=sxv,exv
             x=grid_x(i)
             y=grid_yv(j)
             if ((x-xc)**2+(y-yc)**2 <= rc2) slvv(i,j,1)=pen_amp
          enddo
       enddo
       !---------------------------------------------------------------------
    case(130)
       !---------------------------------------------------------------------
       ! Benchmark VK Cylinder 
       !---------------------------------------------------------------------
       rc=0.05d0
       rc2=rc*rc
       xc=xmin+3*0.15d0+rc
       yc=ymin+0.15d0+rc
       amp=0
       if (rank==0) then
          write(*,*) "------------------------------------------------------"
          write(*,*) "Case NS_ipen     =",NS_ipen
          write(*,*) "radius rc        =",rc
          write(*,*) "position (x,y)   =",xc,yc
          write(*,*) "Re_rotational    =",amp*rc2/(fluids(1)%mu/fluids(1)%rho)
          write(*,*) "Angular velocity =",amp
          write(*,*) "Velocity at rc   =",amp*rc
          write(*,*) "------------------------------------------------------"
       end if
       !---------------------------------------------------------------------
       ! U component
       !---------------------------------------------------------------------
       do j=syu,eyu
          do i=sxu,exu
             x=grid_xu(i)
             y=grid_y(j)
             rad2=(x-xc)**2+(y-yc)**2
             if (rad2 <= rc2) then
                slvu(i,j,1)=pen_amp
                if (x-xc>=0 .and. y-yc>=0 ) then 
                   uinf=amp*sqrt(rad2)*sin(-acos((x-xc)/sqrt(rad2+ipen_amp)))
                else if (x-xc<0 .and. y-yc>=0 ) then 
                   uinf=amp*sqrt(rad2)*sin(-acos((x-xc)/sqrt(rad2+ipen_amp)))
                else if (x-xc<0 .and. y-yc<0 ) then 
                   uinf=amp*sqrt(rad2)*sin(acos((x-xc)/sqrt(rad2+ipen_amp)))
                else
                   uinf=amp*sqrt(rad2)*sin(acos((x-xc)/sqrt(rad2+ipen_amp)))       
                endif
                smvu(i,j,1)=pen_amp*uinf
             endif
          enddo
       enddo
       !---------------------------------------------------------------------

       !---------------------------------------------------------------------
       ! V component
       !---------------------------------------------------------------------
       do j=syv,eyv
          do i=sxv,exv
             x=grid_x(i)
             y=grid_yv(j)
             rad2=(x-xc)**2+(y-yc)**2
             if (rad2 <= rc2) then
                slvv(i,j,1)=pen_amp
                vinf=amp*sqrt(rad2)*cos(acos((x-xc)/sqrt(rad2+ipen_amp)))
                smvv(i,j,1)=pen_amp*vinf
             endif
          enddo
       enddo
       !---------------------------------------------------------------------


    case(131)
       !---------------------------------------------------------------------
       ! Benchmark VK Cylinder 
       !---------------------------------------------------------------------
       rc=0.05d0
       rc2=rc*rc
       xc=xmin+3*0.15d0+rc
       yc=ymin+0.15d0+rc
       amp=10
       if (rank==0) then
          write(*,*) "------------------------------------------------------"
          write(*,*) "Case NS_ipen     =",NS_ipen
          write(*,*) "radius rc        =",rc
          write(*,*) "position (x,y)   =",xc,yc
          write(*,*) "Re_rotational    =",amp*rc2/(fluids(1)%mu/fluids(1)%rho)
          write(*,*) "Angular velocity =",amp
          write(*,*) "Velocity at rc   =",amp*rc
          write(*,*) "------------------------------------------------------"
       end if
       !---------------------------------------------------------------------
       ! U component
       !---------------------------------------------------------------------
       do j=syu,eyu
          do i=sxu,exu
             x=grid_xu(i)
             y=grid_y(j)
             rad2=(x-xc)**2+(y-yc)**2
             if (rad2 <= rc2) then
                slvu(i,j,1)=pen_amp
                if (x-xc>=0 .and. y-yc>=0 ) then 
                   uinf=amp*sqrt(rad2)*sin(-acos((x-xc)/sqrt(rad2+ipen_amp)))
                else if (x-xc<0 .and. y-yc>=0 ) then 
                   uinf=amp*sqrt(rad2)*sin(-acos((x-xc)/sqrt(rad2+ipen_amp)))
                else if (x-xc<0 .and. y-yc<0 ) then 
                   uinf=amp*sqrt(rad2)*sin(acos((x-xc)/sqrt(rad2+ipen_amp)))
                else
                   uinf=amp*sqrt(rad2)*sin(acos((x-xc)/sqrt(rad2+ipen_amp)))       
                endif
                smvu(i,j,1)=pen_amp*uinf
             endif
          enddo
       enddo
       !---------------------------------------------------------------------

       !---------------------------------------------------------------------
       ! V component
       !---------------------------------------------------------------------
       do j=syv,eyv
          do i=sxv,exv
             x=grid_x(i)
             y=grid_yv(j)
             rad2=(x-xc)**2+(y-yc)**2
             if (rad2 <= rc2) then
                slvv(i,j,1)=pen_amp
                vinf=amp*sqrt(rad2)*cos(acos((x-xc)/sqrt(rad2+ipen_amp)))
                smvv(i,j,1)=pen_amp*vinf
             endif
          enddo
       enddo
       !---------------------------------------------------------------------
    case(14)
       !---------------------------------------------------------------------
       ! Nusselt around an isotherm circular cylinder at Re=20
       ! Fu et al. 2002
       ! Zhang et al. 2008
       !---------------------------------------------------------------------
       ! parameters
       !---------------------------------------------------------------------
       amp=0            ! U  = 0
       f=0.2d0          ! 
       rc2=d1p2**2      ! 
       !---------------------------------------------------------------------
       ! position of the cylindre with time
       !---------------------------------------------------------------------
       xc=6.4d0
       yc=6.4d0+amp*sin(2*pi*f*time)
       uc=0
       vc=amp*2*pi*f*cos(2*pi*f*time)
       !---------------------------------------------------------------------

       !---------------------------------------------------------------------
       ! U component
       !---------------------------------------------------------------------
       do j=syu,eyu
          do i=sxu,exu
             x=grid_xu(i)
             y=grid_y(j)
             if ((x-xc)**2+(y-yc)**2 <= rc2) then
                slvu(i,j,1)=pen_amp
                smvu(i,j,1)=uc*pen_amp
             end if
          enddo
       enddo
       !---------------------------------------------------------------------
       ! V component
       !---------------------------------------------------------------------
       do j=syv,eyv
          do i=sxv,exv
             x=grid_x(i)
             y=grid_yv(j)
             if ((x-xc)**2+(y-yc)**2 <= rc2) then
                slvv(i,j,1)=pen_amp
                smvv(i,j,1)=vc*pen_amp
             end if
          enddo
       enddo
       !---------------------------------------------------------------------
    case(15,150)
       !---------------------------------------------------------------------
       ! U component
       !---------------------------------------------------------------------
       do k=szu,ezu
          do j=syu,eyu
             do i=sxu,exu
                if (i<=gsxu+3.or.i>gexu-3.or.j<=gsyu+3.or.j>geyu-3.or.k<=gszu+3.or.k>gezu-3) then 
                   slvu(i,j,k)=pen_amp
                end if
             enddo
          enddo
       end do
       !---------------------------------------------------------------------
       ! V component
       !---------------------------------------------------------------------
       do k=szv,ezv
          do j=syv,eyv
             do i=sxv,exv
                if (i<=gsxv+3.or.i>gexv-3.or.j<=gsyv+3.or.j>geyv-3.or.k<=gszv+3.or.k>gezv-3) then 
                   slvv(i,j,k)=pen_amp
                end if
             enddo
          enddo
       end do
       !---------------------------------------------------------------------
       ! W component
       !---------------------------------------------------------------------
       do k=szw,ezw
          do j=syw,eyw
             do i=sxw,exw
                if (i<=gsxw+3.or.i>gexw-3.or.j<=gsyw+3.or.j>geyw-3.or.k<=gszw+3.or.k>gezw-3) then 
                   slvw(i,j,k)=pen_amp
                end if
             enddo
          enddo
       end do
       !---------------------------------------------------------------------
    case(151)
       !---------------------------------------------------------------------
       ! Porous X- Y-directions
       !---------------------------------------------------------------------
       ! U component
       !---------------------------------------------------------------------
       do k=szu,ezu
          do j=syu,eyu
             do i=sxu,exu
                if (i<=gsxu+3.or.i>gexu-3.or.j<=gsyu+3.or.j>geyu-3) then 
                   slvu(i,j,k)=pen_amp
                end if
             enddo
          enddo
       end do
       !---------------------------------------------------------------------
       ! V component
       !---------------------------------------------------------------------
       do k=szv,ezv
          do j=syv,eyv
             do i=sxv,exv
                if (i<=gsxv+3.or.i>gexv-3.or.j<=gsyv+3.or.j>geyv-3) then 
                   slvv(i,j,k)=pen_amp
                end if
             enddo
          enddo
       end do
       !---------------------------------------------------------------------
       ! W component
       !---------------------------------------------------------------------
       do k=szw,ezw
          do j=syw,eyw
             do i=sxw,exw
                if (i<=gsxw+3.or.i>gexw-3.or.j<=gsyw+3.or.j>geyw-3) then 
                   slvw(i,j,k)=pen_amp
                end if
             enddo
          enddo
       end do
       !---------------------------------------------------------------------
    case(16)
       !---------------------------------------------------------------------
       ! Benchmark JV2
       !---------------------------------------------------------------------
       xc    = 6
       x_min = xc - 0.5d0
       x_max = xc + 0.5d0

       zc    = zmin + (zmax-zmin) / 2
       z_min = zc - 0.1d0
       z_max = zc + 0.1d0

       y_min = ymin
       y_max = ymax
       
       call Penalize_cubic_block( &
            & x_min,x_max,        &
            & y_min,y_max,        &
            & z_min,z_max,        &
            & slvu,slvv,slvw)
       
    case(500)
       include "Aerocab.f90"

       ! operateur fixe 
       uinf=0
       vinf=0
       winf=0

       x0=5.523d0
       y0=2.784d0
       z0=0.145d0

       x=x0

       x_min=x
       x_max=x+0.197d0
       y_max=(ymax-0.91d0-0.03d0-0.4d0)
       y_min=(y_max-0.397d0)
       z_min=(zmin+0.117d0+0.028d0)
       z_max=(z_min+1.65d0)

       do k=sz,ez
          do j=sy,ey
             do i=sx,ex

                if (   grid_x(i)>=x_min .and. grid_x(i)<=x_max .and. &
                     & grid_y(j)>=y_min .and. grid_y(j)<=y_max .and. &
                     & grid_z(k)>=z_min .and. grid_z(k)<=z_max ) then

                   slvu(i,j,k)=pen_amp
                   smvu(i,j,k)=uinf*pen_amp
                   slvu(i+1,j,k)=pen_amp
                   smvu(i+1,j,k)=uinf*pen_amp

                   slvv(i,j,k)=pen_amp
                   smvv(i,j,k)=vinf*pen_amp
                   slvv(i,j+1,k)=pen_amp
                   smvv(i,j+1,k)=vinf*pen_amp

                   slvw(i,j,k)=pen_amp
                   smvw(i,j,k)=winf*pen_amp
                   slvw(i,j,k+1)=pen_amp
                   smvw(i,j,k+1)=winf*pen_amp

                end if

             end do
          end do
       end do
       
    case(501,502:508,-508:-502)
       
       select case(abs(NS_ipen))
       case(501)
          l_sorbonne=1.2d0
          l_obstacle=0.4d0
          q_gaz=0.0024d0
          q_sorbonne=0.192d0
          t_stop=1d10
       case(502)
          l_sorbonne=1.2d0
          l_obstacle=0.4d0
          q_gaz=0.0024d0
          q_sorbonne=0.192d0
          t_stop=1d10
       case(503)
          l_sorbonne=1.2d0
          l_obstacle=1d0
          q_gaz=0.0024d0
          q_sorbonne=0.192d0
          t_stop=1d10
       case(504)
          l_sorbonne=1.2d0
          l_obstacle=0.4d0
          q_gaz=0.0024d0
          q_sorbonne=0.096d0
          t_stop=1d10
       case(505)
          l_sorbonne=1.2d0
          l_obstacle=1d0
          q_gaz=0.0024d0
          q_sorbonne=0.096d0
          t_stop=1d10
       case(506)
          l_sorbonne=1.8d0
          l_obstacle=0.4d0
          q_gaz=0.0036d0
          q_sorbonne=0.288d0
          t_stop=1d10
       case(507)
          l_sorbonne=1.2d0
          l_obstacle=0.4d0
          q_gaz=0.0024d0
          q_sorbonne=0.192d0
          t_stop=30
       case(508)
          l_sorbonne=1.2d0
          l_obstacle=0.4d0
          q_gaz=0.0024d0
          q_sorbonne=0.192d0
          t_stop=5
       end select

       
       include "Aerocab_v2.f90"
       

       ! operateur 
       uinf=0
       vinf=0
       winf=0
       
       x0=xmin+(xmax-xmin)/2+2.66d0/2
       y0=ymax-0.8d0-l_obstacle-0.2d0
       z0=0.1d0

       local_time=local_time+dt

       if (NS_ipen>0) then
          uinf = 0
          x    = x0
       else
          if (local_time<=2.66d0) then !aller 
             uinf = -1 
             x    = x0+uinf*local_time
          else if (local_time<=2.66+t_stop) then !arret 
             uinf = 0
             x    = x0-2.66d0
          else  if (local_time<=2*2.66+t_stop) then !retour
             uinf = 1
             x    = x0-2.66d0+(local_time-(2.66+t_stop))*uinf
          else
             x    = x0
             uinf = 0
          end if
       end if
       
       x_min=x
       x_max=x+0.2d0
       y_max=y0+0.2d0
       y_min=y0-0.2d0
       z_min=z0
       z_max=z_min+1.65d0
       
       select case(abs(NS_ipen))
       case(502:508)
          
          do k=sz,ez
             do j=sy,ey
                do i=sx,ex
                   if (   grid_x(i)>=x_min .and. grid_x(i)<=x_max .and. &
                        & grid_y(j)>=y_min .and. grid_y(j)<=y_max .and. &
                        & grid_z(k)>=z_min .and. grid_z(k)<=z_max ) then
                      
                      slvu(i,j,k)=pen_amp
                      smvu(i,j,k)=uinf*pen_amp
                      slvu(i+1,j,k)=pen_amp
                      smvu(i+1,j,k)=uinf*pen_amp
                      
                      slvv(i,j,k)=pen_amp
                      smvv(i,j,k)=vinf*pen_amp
                      slvv(i,j+1,k)=pen_amp
                      smvv(i,j+1,k)=vinf*pen_amp
                      
                      slvw(i,j,k)=pen_amp
                      smvw(i,j,k)=winf*pen_amp
                      slvw(i,j,k+1)=pen_amp
                      smvw(i,j,k+1)=winf*pen_amp
                      
                   end if
                   
                end do
             end do
          end do

       end select


       

       

       
    case(700)
       !-----------------------------------------------------------------------
       ! Gas injection in liquid alloy
       ! Simulating compressible gas bubbles with a smooth volume tracking1-Fluid method
       ! G.Pianet International Journal of Multiphase Flow
       !-----------------------------------------------------------------------
       
       uinf=2d-2
       vinf=0
       winf=0

       x_min=0
       x_max=20d-3
       y_min=28.5d-3!-2d-3
       y_max=31.5d-3!+2d-3
       z_min=28.5d-3!-2d-3
       z_max=31.5d-3!+2d-3

       
       !-----------------------------------------------------------------------
       ! penalty based on initial cou value
       !-----------------------------------------------------------------------
       obj=0
       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                if (   grid_x(i)>=x_min .and. grid_x(i)<=x_max .and. &
                     & grid_y(j)>=y_min .and. grid_y(j)<=y_max .and. &
                     & grid_z(k)>=z_min .and. grid_z(k)<=z_max ) then
                   cou(i,j,k)=1
                end if
             end do
          end do
       end do
       !-----------------------------------------------------------------------

       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                if (i==gsx) then
                   if (obj(i,j,k)>0) then
                      uin(i,j,k)=2*uinf
                      penu(i,j,k,:)=0
                      penu(i,j,k,1)=pen_amp/2
                      penu(i,j,k,3)=pen_amp/2
                   end if
                end if
                if ( obj(i-1,j,k)*obj(i,j,k)>0 ) then
                   slvu(i,j,k)=pen_amp
                   smvu(i,j,k)=uinf*pen_amp
                   slvu(i+1,j,k)=pen_amp
                   smvu(i+1,j,k)=uinf*pen_amp
                end if
                if ( obj(i,j-1,k)*obj(i,j,k)>0 ) then
                   slvv(i,j,k)=pen_amp
                   smvv(i,j,k)=vinf*pen_amp
                   slvv(i,j-1,k)=pen_amp
                   smvv(i,j-1,k)=vinf*pen_amp
                   slvv(i,j+1,k)=pen_amp
                   smvv(i,j+1,k)=vinf*pen_amp
                end if
                if ( obj(i,j,k-1)*obj(i,j,k)>0 ) then
                   slvw(i,j,k)=pen_amp
                   smvw(i,j,k)=winf*pen_amp
                   slvw(i,j,k-1)=pen_amp
                   smvw(i,j,k-1)=vinf*pen_amp
                   slvw(i,j,k+1)=pen_amp
                   smvw(i,j,k+1)=vinf*pen_amp
                end if
             end do
          end do
       end do
       
    case(701)
       !-----------------------------------------------------------------------
       ! Gas injection in liquid alloy
       ! Simulating compressible gas bubbles with a smooth volume tracking1-Fluid method
       ! G.Pianet International Journal of Multiphase Flow
       !-----------------------------------------------------------------------
       
       uinf=2d-2
       vinf=0
       winf=0

       x_min=0
       x_max=20d-3
       y_min=28.5d-3!-2d-3
       y_max=31.5d-3!+2d-3
       z_min=28.5d-3!-2d-3
       z_max=31.5d-3!+2d-3

       y0=(y_max+y_min)/2
       z0=(z_max+z_min)/2
       rc=(y_max-y_min)/2

       !-----------------------------------------------------------------------
       ! penalty based on initial cou value
       !-----------------------------------------------------------------------
       obj=0
       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                if ( grid_x(i)>=x_min .and. grid_x(i)<=x_max ) then
                   if ( (grid_y(j)-y0)**2 + (grid_z(k)-z0)**2 <= rc**2 ) then
                      obj(i,j,k)=1
                   end if
                end if
             end do
          end do
       end do
       !-----------------------------------------------------------------------

       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                if (i==gsx) then
                   if (obj(i,j,k)>0) then
                      uin(i,j,k)=2*uinf
                      penu(i,j,k,:)=0
                      penu(i,j,k,1)=pen_amp/2
                      penu(i,j,k,3)=pen_amp/2
                   end if
                end if
                if ( obj(i-1,j,k)*obj(i,j,k)>0 ) then
                   slvu(i,j,k)=pen_amp
                   smvu(i,j,k)=uinf*pen_amp
                   slvu(i+1,j,k)=pen_amp
                   smvu(i+1,j,k)=uinf*pen_amp
                end if
                if ( obj(i,j-1,k)*obj(i,j,k)>0 ) then
                   slvv(i,j,k)=pen_amp
                   smvv(i,j,k)=vinf*pen_amp
                   slvv(i,j-1,k)=pen_amp
                   smvv(i,j-1,k)=vinf*pen_amp
                   slvv(i,j+1,k)=pen_amp
                   smvv(i,j+1,k)=vinf*pen_amp
                end if
                if ( obj(i,j,k-1)*obj(i,j,k)>0 ) then
                   slvw(i,j,k)=pen_amp
                   smvw(i,j,k)=winf*pen_amp
                   slvw(i,j,k-1)=pen_amp
                   smvw(i,j,k-1)=vinf*pen_amp
                   slvw(i,j,k+1)=pen_amp
                   smvw(i,j,k+1)=vinf*pen_amp
                end if
             end do
          end do
       end do

    case(-500)

       include "Aerocab.f90"

       uinf=0
       vinf=0
       winf=0
       
       x0=5.523d0
       y0=2.784d0
       z0=0.145d0

       local_time=local_time+dt
       
       if (local_time<=0.125d0) then
          x    = -(1/(2*0.125d0))*local_time*local_time + x0
          uinf = -(1/0.125d0)*local_time
       else if (local_time<=2.65d0) then
          x    = -local_time + (0.125d0/2) + x0
          uinf = -1
       else if (local_time<=2.775d0) then 
          x    = (-(local_time*local_time) + (2*2.775d0*local_time) - (2.775d0*2.775d0))/(2*(2.65d0-2.775d0)) + 2.863d0
          uinf = (2.775d0 - local_time)/(2.65d0 - 2.775d0)
       else 
          x    = 2.863d0
          uinf = 0
       end if

       x_min=x
       x_max=x+0.197d0
       y_max=(ymax-0.91d0-0.03d0-0.4d0)
       y_min=(y_max-0.397d0)
       z_min=(zmin+0.117d0+0.028d0)
       z_max=(z_min+1.65d0)

       do k=sz,ez
          do j=sy,ey
             do i=sx,ex

                if (   grid_x(i)>=x_min .and. grid_x(i)<=x_max .and. &
                     & grid_y(j)>=y_min .and. grid_y(j)<=y_max .and. &
                     & grid_z(k)>=z_min .and. grid_z(k)<=z_max ) then

                   slvu(i,j,k)=pen_amp
                   smvu(i,j,k)=uinf*pen_amp
                   slvu(i+1,j,k)=pen_amp
                   smvu(i+1,j,k)=uinf*pen_amp

                   slvv(i,j,k)=pen_amp
                   smvv(i,j,k)=vinf*pen_amp
                   slvv(i,j+1,k)=pen_amp
                   smvv(i,j+1,k)=vinf*pen_amp

                   slvw(i,j,k)=pen_amp
                   smvw(i,j,k)=winf*pen_amp
                   slvw(i,j,k+1)=pen_amp
                   smvw(i,j,k+1)=winf*pen_amp

                end if

             end do
          end do
       end do
       
       
    case(1000)
       !---------------------------------------------------------------------
       ! U component
       !---------------------------------------------------------------------
       call read_geom_png(slvu,grid_xu,grid_y,grid_z)
       slvu=slvu*pen_amp
       !---------------------------------------------------------------------
       ! V component
       !---------------------------------------------------------------------
       call read_geom_png(slvv,grid_x,grid_yv,grid_z)
       slvv=slvv*pen_amp
       !---------------------------------------------------------------------
       ! W component
       !---------------------------------------------------------------------
       if (dim==3) then
          call read_geom_png(slvw,grid_x,grid_y,grid_zw)
          slvw=slvw*pen_amp
       end if
!!$       !---------------------------------------------------------------------
!!$       ! U component
!!$       !---------------------------------------------------------------------
!!$       do k=szu,ezu
!!$          z=grid_z(k)
!!$          if (z<zmin+d1p4*(zmax-zmin).or.z>zmin+d3p4*(zmax-zmin)) then
!!$          !if (z<zmin+d1p4*(zmax-zmin)) then
!!$             slvu(:,:,k)=0
!!$          end if
!!$       end do
!!$       !---------------------------------------------------------------------
!!$       ! V component
!!$       !---------------------------------------------------------------------
!!$       do k=szv,ezv
!!$          z=grid_z(k)
!!$          if (z<zmin+d1p4*(zmax-zmin).or.z>zmin+d3p4*(zmax-zmin)) then
!!$          !if (z<zmin+d1p4*(zmax-zmin)) then
!!$             slvv(:,:,k)=0
!!$          end if
!!$       end do
!!$       if (dim==3) then 
!!$          !---------------------------------------------------------------------
!!$          ! W component
!!$          !---------------------------------------------------------------------
!!$          do k=szw,ezw
!!$             z=grid_zw(k)
!!$             if (z<zmin+d1p4*(zmax-zmin).or.z>zmin+d3p4*(zmax-zmin)) then
!!$             !if (z<zmin+d1p4*(zmax-zmin)) then
!!$                slvw(:,:,k)=0
!!$             end if
!!$          end do
!!$       end if
       !---------------------------------------------------------------------
       !---------------------------------------------------------------------
       !---------------------------------------------------------------------
       ! unsteady cases
       !---------------------------------------------------------------------
       !---------------------------------------------------------------------
       !---------------------------------------------------------------------
    case(-1)
       !---------------------------------------------------------------------
       ! In Line oscillation of a circular cylinder
       ! Shen et al. 2009 Computer & Fluids
       ! Liao et al. 2009 Computer & Fluids
       ! Dutsch et al. 1998 JFM (Exp+Num)
       !---------------------------------------------------------------------
       ! parameters
       !---------------------------------------------------------------------
       amp=1            ! U  = 1
       f=d1p5           ! KC = U / f / D = 5
       rc2=d1p2**2      ! RE = U * D / nu = 100
       !---------------------------------------------------------------------
       ! position of the cylindre with time
       !---------------------------------------------------------------------
       xc=d1p2*(xmin+xmax)-amp*d1p2*dpi/f*sin(2*pi*f*time)
       yc=d1p2*(ymin+ymax)
       uc=-amp*cos(2*pi*f*time)
       vc=0
       !---------------------------------------------------------------------

       !---------------------------------------------------------------------
       ! U component
       !---------------------------------------------------------------------
       do j=syu,eyu
          do i=sxu,exu
             x=grid_xu(i)
             y=grid_y(j)
             if ((x-xc)**2+(y-yc)**2 <= rc2) then
                slvu(i,j,1)=pen_amp
                smvu(i,j,1)=uc*pen_amp
             end if
          enddo
       enddo
       !---------------------------------------------------------------------

       !---------------------------------------------------------------------
       ! V component
       !---------------------------------------------------------------------
       do j=syv,eyv
          do i=sxv,exv
             x=grid_x(i)
             y=grid_yv(j)
             if ((x-xc)**2+(y-yc)**2 <= rc2) then
                slvv(i,j,1)=pen_amp
                smvv(i,j,1)=vc*pen_amp
             end if
          enddo
       enddo
       !---------------------------------------------------------------------
    case(-2)
       !---------------------------------------------------------------------
       ! Vertical oscillation of a circular cylinder
       ! Fu et al. 2002
       ! Zhang et al. 2008
       !---------------------------------------------------------------------
       ! parameters
       !---------------------------------------------------------------------
       amp=0.4d0        ! U  = 0.4
       f=0.2d0          ! 
       rc2=d1p2**2      ! 
       !---------------------------------------------------------------------
       ! position of the cylindre with time
       !---------------------------------------------------------------------
       xc=6.4d0
       yc=6.4d0+amp*sin(2*pi*f*time)
       uc=0
       vc=amp*2*pi*f*cos(2*pi*f*time)
       !---------------------------------------------------------------------

       !---------------------------------------------------------------------
       ! U component
       !---------------------------------------------------------------------
       do j=syu,eyu
          do i=sxu,exu
             x=grid_xu(i)
             y=grid_y(j)
             if ((x-xc)**2+(y-yc)**2 <= rc2) then
                slvu(i,j,1)=pen_amp
                smvu(i,j,1)=uc*pen_amp
             end if
          enddo
       enddo
       !---------------------------------------------------------------------
       ! V component
       !---------------------------------------------------------------------
       do j=syv,eyv
          do i=sxv,exv
             x=grid_x(i)
             y=grid_yv(j)
             if ((x-xc)**2+(y-yc)**2 <= rc2) then
                slvv(i,j,1)=pen_amp
                smvv(i,j,1)=vc*pen_amp
             end if
          enddo
       enddo
       !---------------------------------------------------------------------
    case(999)
        open(unit=1001,file="particles.in",status="old",action="read",iostat=ierr)
        if (dim==2) then
         read(1001,*) x0,y0,rc
         rc=rc*0.5d0
         do j=syu,eyu
            do i=sxu,exu
               x=grid_xu(i)
               y=grid_y (j)
               if (((x-x0)*(x-x0)+(y-y0)*(y-y0))<=rc*rc) then 
                  slvu(i,j,1)=pen_amp
                  smvu(i,j,1)=0d0
               end if 
            end do 
         end do
         do j=syv,eyv
            do i=sxv,exv
               x=grid_x (i)
               y=grid_yv(j)
               if (((x-x0)*(x-x0)+(y-y0)*(y-y0))<=rc*rc) then 
                  slvv(i,j,1)=pen_amp
                  smvv(i,j,1)=0d0
               end if 
            end do 
         end do 
        else
         read(1001,*) x0,y0,z0,rc
         rc=rc*0.5d0
         do k=szu,ezu
            do j=syu,eyu
               do i=sxu,exu
                  x=grid_xu(i)
                  y=grid_y (j)
                  z=grid_z (k)
                  if (((x-x0)*(x-x0)+(y-y0)*(y-y0)+(z-z0)*(z-z0))<=rc*rc) then 
                     slvu(i,j,k)=pen_amp
                     smvu(i,j,k)=0d0
                  end if 
               end do 
            end do
         end do
         do k=szv,ezv
            do j=syv,eyv
               do i=sxv,exv
                  x=grid_x (i)
                  y=grid_yv(j)
                  z=grid_z (k)
                  if (((x-x0)*(x-x0)+(y-y0)*(y-y0)+(z-z0)*(z-z0))<=rc*rc) then 
                     slvv(i,j,k)=pen_amp
                     smvv(i,j,k)=0d0
                  end if 
               end do 
            end do
         end do
         do k=szw,ezw
            do j=syw,eyw
               do i=sxw,exw
                  x=grid_x (i)
                  y=grid_y (j)
                  z=grid_zw(k)
                  if (((x-x0)*(x-x0)+(y-y0)*(y-y0)+(z-z0)*(z-z0))<= rc*rc) then 
                     slvw(i,j,k)=pen_amp
                     smvw(i,j,k)=0d0
                  end if 
               end do 
            end do
         end do 
        end if 
        close(1001)
    case default 
       write(*,*) "Warning, Penaly case not known, NS_ipen=", NS_ipen
       write(*,*) "Warning, Penaly case not known, NS_ipen=", NS_ipen
       write(*,*) "Warning, Penaly case not known, NS_ipen=", NS_ipen
       write(*,*) "Warning, Penaly case not known, NS_ipen=", NS_ipen
       write(*,*) "Use NS_ipen=0"
       write(*,*) "Use NS_ipen=0"
       write(*,*) "Use NS_ipen=0"
       stop
    end select


    !---------------------------------------------------------------------
    if (nproc>1) then
       if (allocated(smvu)) call comm_mpi_uvw(smvu,smvv,smvw)
       if (allocated(slvu)) call comm_mpi_uvw(slvu,slvv,slvw)
    end if
    !---------------------------------------------------------------------

    !---------------------------------------------------------------------
    ! binary object
    !---------------------------------------------------------------------
    if (present(obj)) then
       if (allocated(obj)) then
          obj=0
          do k=sz,ez
             do j=sy,ey
                do i=sx,ex
                   if (slvu(i+1,j,k)*slvu(i,j,k)>0) obj(i,j,k)=1
                   if (slvv(i,j+1,k)*slvv(i,j,k)>0) obj(i,j,k)=1
                   if (dim==3) then
                      if (slvw(i,j,k+1)*slvw(i,j,k)>0) obj(i,j,k)=1
                   end if
                end do
             end do
          end do
       end if
    end if
    !---------------------------------------------------------------------

    call Purge_penalize_boundaries(slvu,slvv,slvw)
    
    !*******************************************************************************!
    !*******************************************************************************!
    !*******************************************************************************!
  end subroutine Penalty_NS
  !*******************************************************************************!
  !*******************************************************************************!
  !*******************************************************************************!


  !*******************************************************************************!
  !*******************************************************************************!
  !*******************************************************************************!
  ! Penalty method for scalar transport equation
  !*******************************************************************************!
  !*******************************************************************************!
  !*******************************************************************************!
  subroutine Penalty_Sca(time,dt,   &
       & tp,cou,                    &
       & smsca,slsca,spsca,         &
       & rovu,rovv,rovw,cp,         &
       & diffusca,diffvsca,diffwsca,&
       & slvu,slvv,slvw,            &
       & u,v,w)
    !*******************************************************************************!
    implicit none
    !---------------------------------------------------------------------
    ! Global variables                                            
    !---------------------------------------------------------------------
    real(8), intent(in)                                     :: time,dt
    real(8), dimension(:,:,:),   allocatable, intent(inout) :: smsca,slsca
    real(8), dimension(:,:,:,:), allocatable, intent(inout) :: spsca
    real(8), dimension(:,:,:),   allocatable, intent(inout) :: cou,tp
    real(8), dimension(:,:,:),   allocatable, intent(inout) :: diffusca,diffvsca,diffwsca
    real(8), dimension(:,:,:),   allocatable, intent(in)    :: rovu,rovv,rovw,cp
    real(8), dimension(:,:,:),   allocatable, intent(in)    :: slvu,slvv,slvw
    real(8), dimension(:,:,:),   allocatable, intent(in)    :: u,v,w
    !---------------------------------------------------------------------
    ! Local variables                                             
    !---------------------------------------------------------------------
    integer                                               :: i,j,k,n
    integer                                               :: lgx,lgy,lgz
    real(8)                                               :: vol
    real(8)                                               :: x,y,z,xc,yc,rc,rc2,rad2
    real(8)                                               :: scainf
    real(8)                                               :: pen_amp,ipen_amp
    real(8)                                               :: x_max,x_min,y_max,y_min,z_max,z_min
    logical                                               :: yes
    !---------------------------------------------------------------------

    !---------------------------------------------------
    ! Initialization
    !---------------------------------------------------
    pen_amp=EN_pen
    ipen_amp=1/EN_pen
    !---------------------------------------------------
    lgx=gx-1
    lgy=gy-1
    lgz=gz-1
    if (dim==2) lgz=0

    if (allocated(smsca)) smsca=0
    if (allocated(slsca)) slsca=0
    if (allocated(spsca)) spsca=0

    select case(EN_ipen)
    case(0)
       !---------------------------------------------------------------------
       ! Nothing
       !---------------------------------------------------------------------
    case(1)
       !---------------------------------------------------------------------
       ! Fixed adiabatic circular obstacle
       !---------------------------------------------------------------------
       ! parameters
       !---------------------------------------------------------------------
       rc=0.1d0*min((xmax-xmin),(ymax-ymin))
       rc2=rc*rc
       xc=d1p2*(xmax+xmin)
       yc=d1p2*(ymax+ymin)
       !---------------------------------------------------------------------
       do j=sy,ey
          y=grid_y(j)
          do i=sx,ex
             x=grid_x(i)
             if ((x-xc)**2+(y-yc)**2 <= rc2) then
                !---------------------------------------------------------------------
                ! X direction
                !---------------------------------------------------------------------
                diffusca(i,j,1)=diffusca(i,j,1)*ipen_amp/(ipen_amp+1)
                diffusca(i+1,j,1)=diffusca(i+1,j,1)*ipen_amp/(ipen_amp+1)
                !---------------------------------------------------------------------
                ! Y direction
                !---------------------------------------------------------------------
                diffvsca(i,j,1)=diffvsca(i,j,1)*ipen_amp/(ipen_amp+1)
                diffvsca(i,j+1,1)=diffvsca(i,j+1,1)*ipen_amp/(ipen_amp+1)
             end if
          enddo
       enddo
       !---------------------------------------------------------------------
    case(2)
       !---------------------------------------------------------------------
       ! Fixed circular obstacle with imposed sca value=scainf
       !---------------------------------------------------------------------
       ! parameters
       !---------------------------------------------------------------------
       rc=0.1d0*min((xmax-xmin),(ymax-ymin))
       rc2=rc*rc
       xc=d1p2*(xmax+xmin)
       yc=d1p2*(ymax+ymin)
       scainf=0
       !---------------------------------------------------------------------
       do j=sy,ey
          y=grid_y(j)
          do i=sx,ex
             x=grid_x(i)
             if ((x-xc)**2+(y-yc)**2 <= rc2) then
                slsca(i,j,1)=pen_amp
                smsca(i,j,1)=pen_amp*scainf
             end if
          enddo
       enddo
       !---------------------------------------------------------------------
    case(3)
       !---------------------------------------------------------------------
       ! Fixed adiabatic circular obstacle ...
       !---------------------------------------------------------------------
       ! parameters
       !---------------------------------------------------------------------
       rc=0.1d0*min((xmax-xmin),(ymax-ymin))
       rc2=rc*rc
       xc=d1p2*(xmax+xmin)
       yc=d1p2*(ymax+ymin)
       !---------------------------------------------------------------------
       do j=sy,ey
          y=grid_y(j)
          do i=sx,ex
             x=grid_x(i)
             if ((x-xc)**2+(y-yc)**2 <= rc2) then
                !---------------------------------------------------------------------
                ! X direction
                !---------------------------------------------------------------------
                diffusca(i,j,1)=diffusca(i,j,1)*ipen_amp/(ipen_amp+1)
                diffusca(i+1,j,1)=diffusca(i+1,j,1)*ipen_amp/(ipen_amp+1)
                !---------------------------------------------------------------------
                ! Y direction
                !---------------------------------------------------------------------
                diffvsca(i,j,1)=diffvsca(i,j,1)*ipen_amp/(ipen_amp+1)
                diffvsca(i,j+1,1)=diffvsca(i,j+1,1)*ipen_amp/(ipen_amp+1)
                !---------------------------------------------------------------------
                ! ... and fixed temperature value
                !---------------------------------------------------------------------
                scainf=0
                !---------------------------------------------------------------------
                slsca(i,j,1)=pen_amp
                smsca(i,j,1)=pen_amp*scainf
                !---------------------------------------------------------------------
             end if
          enddo
       enddo
       !---------------------------------------------------------------------
    case(-100,100)
       !---------------------------------------------------------------------
       ! Fixed temperature obstable base on velocity penalty
       !---------------------------------------------------------------------
       scainf=1
       !---------------------------------------------------------------------
       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                if (slvu(i,j,k)/=0.and.slvu(i+1,j,k)/=0) then
                   slsca(i,j,k)=pen_amp
                   smsca(i,j,k)=pen_amp*scainf
                end if
                if (slvu(i,j,k)/=0.and.slvu(i,j+1,k)/=0) then
                   slsca(i,j,k)=pen_amp
                   smsca(i,j,k)=pen_amp*scainf
                end if
             end do
          end do
       end do
       !---------------------------------------------------------------------
    case(1000)
       call read_geom_png(slsca,grid_x,grid_y,grid_z)
       do k=sz,ez
          z=grid_z(k)
          if (z<zmin+d1p4*(zmax-zmin).or.z>zmin+d3p4*(zmax-zmin)) then
             slsca(:,:,k)=0
          end if
       end do
       slsca=slsca*pen_amp
       smsca=pen_amp*scainf
       !---------------------------------------------------------------------
       !---------------------------------------------------------------------
       !---------------------------------------------------------------------
       ! unsteady cases
       !---------------------------------------------------------------------
       !---------------------------------------------------------------------
       !---------------------------------------------------------------------
    case(-1)
       !---------------------------------------------------------------------
       ! Imposed value to moving object
       ! Based on velocity penalization
       !---------------------------------------------------------------------
       scainf=1
       !---------------------------------------------------------------------
       do k=sz-(dim-2),ez+(dim-2)
          do j=sy-1,ey+1
             do i=sx-1,ex+1
                !---------------------------------------------------------------------
                ! X-faces
                !---------------------------------------------------------------------
                if (slvu(i,j,k)/=0.and.slvu(i+1,j,k)/=0) then
                   slsca(i,j,k)=pen_amp
                   smsca(i,j,k)=pen_amp*scainf
                end if
                !---------------------------------------------------------------------
                ! Y-faces
                !---------------------------------------------------------------------
                if (slvv(i,j,k)/=0.and.slvv(i,j+1,k)/=0) then
                   slsca(i,j,k)=pen_amp
                   smsca(i,j,k)=pen_amp*scainf
                end if
                !---------------------------------------------------------------------
                ! Z-faces
                !---------------------------------------------------------------------
                if (dim==3) then
                   if (slvw(i,j,k)/=0.and.slvw(i,j,k+1)/=0) then
                      slsca(i,j,k)=pen_amp
                      smsca(i,j,k)=pen_amp*scainf
                   end if
                end if
                !---------------------------------------------------------------------
             end do
          end do
       end do
       !---------------------------------------------------------------------
       !---------------------------------------------------------------------
       !---------------------------------------------------------------------
    case(-2)
       !---------------------------------------------------------------------
       ! zero flux
       ! Based on velocity penalization
       !---------------------------------------------------------------------
       do k=sz-(dim-2),ez+(dim-2)
          do j=sy-1,ey+1
             do i=sx-1,ex+1
                !---------------------------------------------------------------------
                vol=dx(i)*dy(j)*dz(k)
                !---------------------------------------------------------------------
                ! X-faces
                !---------------------------------------------------------------------
                ! backward 
                !---------------------------------------------------------------------
                if (slvu(i,j,k)/=0) then
                   spsca(i,j,k,1)=spsca(i,j,k,1) + pen_amp*             &
                        & (-rovu(i,j,k)*cp(i,j,k)*u(i,j,k)*d1p2         &
                        &  +diffusca(i,j,k)/dxu(i)) * dy(j)*dz(k) / vol ! vol factor in Energy_Martix
                   spsca(i,j,k,2)=spsca(i,j,k,2) + pen_amp*             &
                        & (-rovu(i,j,k)*cp(i,j,k)*u(i,j,k)*d1p2         &
                        &  -diffusca(i,j,k)/dxu(i)) * dy(j)*dz(k) / vol
                end if
                !---------------------------------------------------------------------
                ! forward
                !---------------------------------------------------------------------
                if (slvu(i+1,j,k)/=0) then
                   spsca(i,j,k,1)=spsca(i,j,k,1) + pen_amp*                 &
                        & (+rovu(i+1,j,k)*cp(i,j,k)*u(i+1,j,k)*d1p2         &
                        &  +diffusca(i+1,j,k)/dxu(i+1)) * dy(j)*dz(k) / vol
                   spsca(i,j,k,3)=spsca(i,j,k,3) + pen_amp*                 &
                        & (+rovu(i+1,j,k)*cp(i,j,k)*u(i+1,j,k)*d1p2         &
                        &  -diffusca(i+1,j,k)/dxu(i+1)) * dy(j)*dz(k) / vol
                end if
                !---------------------------------------------------------------------
                ! Y-faces
                !---------------------------------------------------------------------
                ! backward 
                !---------------------------------------------------------------------
                if (slvv(i,j,k)/=0) then
                   spsca(i,j,k,1)=spsca(i,j,k,1) + pen_amp*             &
                        & (-rovv(i,j,k)*cp(i,j,k)*v(i,j,k)*d1p2         &
                        &  +diffvsca(i,j,k)/dyv(j)) * dx(i)*dz(k) / vol
                   spsca(i,j,k,4)=spsca(i,j,k,4) + pen_amp*             &
                        & (-rovv(i,j,k)*cp(i,j,k)*v(i,j,k)*d1p2         &
                        &  -diffvsca(i,j,k)/dyv(j)) * dx(i)*dz(k) / vol
                end if
                !---------------------------------------------------------------------
                ! forward
                !---------------------------------------------------------------------
                if (slvv(i,j+1,k)/=0) then
                   spsca(i,j,k,1)=spsca(i,j,k,1) + pen_amp*                 &
                        & (+rovv(i,j+1,k)*cp(i,j,k)*v(i,j+1,k)*d1p2         &
                        &  +diffvsca(i,j+1,k)/dyv(j+1)) * dx(i)*dz(k) / vol
                   spsca(i,j,k,5)=spsca(i,j,k,5) + pen_amp*                 &
                        & (+rovv(i,j+1,k)*cp(i,j,k)*v(i,j+1,k)*d1p2         & 
                        &  -diffvsca(i,j+1,k)/dyv(j+1)) * dx(i)*dz(k) / vol
                end if
                !---------------------------------------------------------------------
                ! Z-faces
                !---------------------------------------------------------------------
                if (dim==3) then 
                   !---------------------------------------------------------------------
                   ! backward 
                   !---------------------------------------------------------------------
                   if (slvw(i,j,k)/=0) then
                      spsca(i,j,k,1)=spsca(i,j,k,1) + pen_amp*             &
                           & (-rovw(i,j,k)*cp(i,j,k)*w(i,j,k)*d1p2         &
                           &  +diffwsca(i,j,k)/dzw(k)) * dx(i)*dy(j) / vol
                      spsca(i,j,k,6)=spsca(i,j,k,6) + pen_amp*             &
                           & (-rovw(i,j,k)*cp(i,j,k)*w(i,j,k)*d1p2         &
                           &  -diffwsca(i,j,k)/dzw(k)) * dx(i)*dy(j) / vol
                   end if
                   !---------------------------------------------------------------------
                   ! forward
                   !---------------------------------------------------------------------
                   if (slvw(i,j,k+1)/=0) then
                      spsca(i,j,k,1)=spsca(i,j,k,1) + pen_amp*                 &
                           & (+rovw(i,j,k+1)*cp(i,j,k)*w(i,j,k+1)*d1p2         &
                           &  +diffwsca(i,j,k+1)/dzw(k+1)) * dx(i)*dy(j) / vol
                      spsca(i,j,k,7)=spsca(i,j,k,7) + pen_amp*                 &
                           & (+rovw(i,j,k+1)*cp(i,j,k)*w(i,j,k+1)*d1p2         &
                           &  -diffwsca(i,j,k+1)/dzw(k+1)) * dx(i)*dy(j) / vol
                   end if
                   !---------------------------------------------------------------------
                end if
                !---------------------------------------------------------------------
!!$                yes=.false.
!!$                if (slvu(i,j,k)/=0.and.slvu(i+1,j,k)/=0) yes=.true.
!!$                if (slvv(i,j,k)/=0.and.slvv(i,j+1,k)/=0) yes=.true.
!!$                if (dim==3) then
!!$                   if (slvw(i,j,k)/=0.and.slvw(i,j,k+1)/=0) yes=.true.
!!$                end if
!!$                if (yes) then
!!$                   spsca(i,j,k,1)=spsca(i,j,k,1)+pen_amp* dx(i)*dy(j)*dz(k) /dt
!!$                   smsca(i,j,k)  =pen_amp*tp(i,j,k)*dt
!!$                end if

             end do
          end do
       end do
       !---------------------------------------------------------------------
    case(-4)
       !---------------------------------------------------------------------
       ! zero diffusive flux
       ! Based on velocity penalization
       !---------------------------------------------------------------------
       do k=sz-(dim-2),ez+(dim-2)
          do j=sy-1,ey+1
             do i=sx-1,ex+1
                !---------------------------------------------------------------------
                vol=dx(i)*dy(j)*dz(k)
                !---------------------------------------------------------------------
                ! X-faces
                !---------------------------------------------------------------------
                ! backward 
                !---------------------------------------------------------------------
                if (slvu(i,j,k)/=0) then
                   spsca(i,j,k,1)=spsca(i,j,k,1) + pen_amp*             &
                        & (+diffusca(i,j,k)/dxu(i)) * dy(j)*dz(k) / vol ! vol factor in Energy_Martix
                   spsca(i,j,k,2)=spsca(i,j,k,2) + pen_amp*             &
                        & (-diffusca(i,j,k)/dxu(i)) * dy(j)*dz(k) / vol
                end if
                !---------------------------------------------------------------------
                ! forward
                !---------------------------------------------------------------------
                if (slvu(i+1,j,k)/=0) then
                   spsca(i,j,k,1)=spsca(i,j,k,1)+ pen_amp*                  &
                        & (+diffusca(i+1,j,k)/dxu(i+1)) * dy(j)*dz(k) / vol
                   spsca(i,j,k,3)=spsca(i,j,k,3) + pen_amp*                 &
                        & (-diffusca(i+1,j,k)/dxu(i+1)) * dy(j)*dz(k) / vol
                end if
                !---------------------------------------------------------------------
                ! Y-faces
                !---------------------------------------------------------------------
                ! backward 
                !---------------------------------------------------------------------
                if (slvv(i,j,k)/=0) then
                   spsca(i,j,k,1)=spsca(i,j,k,1) + pen_amp*             &
                        & (+diffvsca(i,j,k)/dyv(j)) * dx(i)*dz(k) / vol
                   spsca(i,j,k,4)=spsca(i,j,k,4) + pen_amp*             &
                        & (-diffvsca(i,j,k)/dyv(j)) * dx(i)*dz(k) / vol
                end if
                !---------------------------------------------------------------------
                ! forward
                !---------------------------------------------------------------------
                if (slvv(i,j+1,k)/=0) then
                   spsca(i,j,k,1)=spsca(i,j,k,1) + pen_amp*                 &
                        & (+diffvsca(i,j+1,k)/dyv(j+1)) * dx(i)*dz(k) / vol
                   spsca(i,j,k,5)=spsca(i,j,k,5) + pen_amp*                 &
                        & (-diffvsca(i,j+1,k)/dyv(j+1)) * dx(i)*dz(k) / vol
                end if
                !---------------------------------------------------------------------
                ! Z-faces
                !---------------------------------------------------------------------
                if (dim==3) then 
                   !---------------------------------------------------------------------
                   ! backward 
                   !---------------------------------------------------------------------
                   if (slvw(i,j,k)/=0) then
                      spsca(i,j,k,1)=spsca(i,j,k,1) + pen_amp*             &
                           & (+diffwsca(i,j,k)/dzw(k)) * dx(i)*dy(j) / vol
                      spsca(i,j,k,6)=spsca(i,j,k,6) + pen_amp*             &
                           & (-diffwsca(i,j,k)/dzw(k)) * dx(i)*dy(j) / vol
                   end if
                   !---------------------------------------------------------------------
                   ! forward
                   !---------------------------------------------------------------------
                   if (slvw(i,j,k+1)/=0) then
                      spsca(i,j,k,1)=spsca(i,j,k,1) + pen_amp*                 &
                           & (+diffwsca(i,j,k+1)/dzw(k+1)) * dx(i)*dy(j) / vol
                      spsca(i,j,k,7)=spsca(i,j,k,7) + pen_amp*                 &
                           & (-diffwsca(i,j,k+1)/dzw(k+1)) * dx(i)*dy(j) / vol
                   end if
                   !---------------------------------------------------------------------
                end if
                !---------------------------------------------------------------------
             end do
          end do
       end do
       !---------------------------------------------------------------------
    case(-3)
       !---------------------------------------------------------------------
       ! Adiabatic fixed object 
       ! Work on diffusivity coefficient
       ! Set 0 diffusive flux if normal velocity is penalized
       !---------------------------------------------------------------------
       do k=sz-(dim-2),ez+(dim-2)
          do j=sy-1,ey+1
             do i=sx-1,ex+1
                if (slvu(i,j,k)/=0) diffusca(i,j,k)=diffusca(i,j,k)*ipen_amp/(ipen_amp+1)
                if (slvv(i,j,k)/=0) diffvsca(i,j,k)=diffvsca(i,j,k)*ipen_amp/(ipen_amp+1)
                if (dim==3) then
                   if (slvw(i,j,k)/=0) diffwsca(i,j,k)=diffwsca(i,j,k)*ipen_amp/(ipen_amp+1)
                end if
             end do
          end do
       end do
       !---------------------------------------------------------------------
    case(-5)
       !---------------------------------------------------------------------
       ! Adiabatic moving object 
       ! Work on diffusivity coefficient
       ! Set 0 diffusive+convectif flux if normal velocity is penalized
       !---------------------------------------------------------------------
       do k=sz-(dim-2),ez+(dim-2)
          do j=sy-1,ey+1
             do i=sx-1,ex+1
                if (slvu(i,j,k)/=0) diffusca(i,j,k)=d1p2*u(i,j,k)*dxu(i+1)*(tp(i+1,j,k)+tp(i,j,k)) &
                     & /(tp(i+1,j,k)-tp(i,j,k))
                if (slvv(i,j,k)/=0) diffvsca(i,j,k)=d1p2*v(i,j,k)*dyv(j+1)*(tp(i,j+1,k)+tp(i,j,k)) &
                     & /(tp(i,j+1,k)-tp(i,j,k))
                if (dim==3) then
                   if (slvw(i,j,k)/=0) diffwsca(i,j,k)=d1p2*w(i,j,k)*dzw(k+1)*(tp(i,j,k+1)+tp(i,j,k)) &
                        & /(tp(i,j,k+1)-tp(i,j,k))
                end if
             end do
          end do
       end do
       !---------------------------------------------------------------------
    case(-6)
       !---------------------------------------------------------------------
       ! Two layers penalization
       ! * solid
       ! * and 1st fluid cells
       ! are penalized
       !---------------------------------------------------------------------
       do k=sz-(dim-2),ez+(dim-2)
          do j=sy-1,ey+1
             do i=sx-1,ex+1
                !---------------------------------------------------------------------
                vol=dx(i)*dy(j)*dz(k)
                !---------------------------------------------------------------------
                ! X-faces
                !---------------------------------------------------------------------
                if (slvu(i,j,k)/=0.and.slvu(i+1,j,k)/=0) then
                   !---------------------------------------------------------------------
                   ! object value
                   !---------------------------------------------------------------------
                   spsca(i,j,k,1)=pen_amp
                   smsca(i,j,k)=pen_amp*scainf
                   !---------------------------------------------------------------------
                else if (slvu(i,j,k)==0.and.slvu(i+1,j,k)/=0) then
                   !---------------------------------------------------------------------
                   ! forward penalization 
                   !---------------------------------------------------------------------
                   spsca(i,j,k,1)=spsca(i,j,k,1) + pen_amp*(                &
                        &  +rovu(i+1,j,k)*cp(i,j,k)*u(i+1,j,k)*d1p2         &
                        &  +diffusca(i+1,j,k)/dxu(i+1)) * dy(j)*dz(k) / vol
                   spsca(i,j,k,3)=spsca(i,j,k,3) + pen_amp*(                &
                        &  +rovu(i+1,j,k)*cp(i,j,k)*u(i+1,j,k)*d1p2         &
                        &  -diffusca(i+1,j,k)/dxu(i+1)) * dy(j)*dz(k) / vol
                   !---------------------------------------------------------------------
!!$                   spsca(i,j,k,1)=pen_amp
!!$                   spsca(i,j,k,3)=-pen_amp
                else if (slvu(i,j,k)/=0.and.slvu(i+1,j,k)==0) then
                   !---------------------------------------------------------------------
                   ! backward penalization 
                   !---------------------------------------------------------------------
                   spsca(i,j,k,1)=spsca(i,j,k,1) + pen_amp*(            &
                        &  -rovu(i,j,k)*cp(i,j,k)*u(i,j,k)*d1p2         &
                        &  +diffusca(i,j,k)/dxu(i)) * dy(j)*dz(k) / vol
                   spsca(i,j,k,2)=spsca(i,j,k,2) + pen_amp*(            &
                        &  -rovu(i,j,k)*cp(i,j,k)*u(i,j,k)*d1p2         &
                        &  -diffusca(i,j,k)/dxu(i)) * dy(j)*dz(k) / vol
                   !---------------------------------------------------------------------
!!$                   spsca(i,j,k,1)=pen_amp
!!$                   spsca(i,j,k,2)=-pen_amp
                end if
                !---------------------------------------------------------------------
                ! Y-faces
                !---------------------------------------------------------------------
                if (slvv(i,j,k)/=0.and.slvv(i,j+1,k)/=0) then
                   !---------------------------------------------------------------------
                   ! object value
                   !---------------------------------------------------------------------
                   spsca(i,j,k,1)=pen_amp
                   smsca(i,j,k)=pen_amp*scainf
                   !---------------------------------------------------------------------
                else if (slvv(i,j,k)==0.and.slvv(i,j+1,k)/=0) then
                   !---------------------------------------------------------------------
                   ! forward penalization 
                   !---------------------------------------------------------------------
                   spsca(i,j,k,1)=spsca(i,j,k,1) + pen_amp*(                &
                        &  +rovv(i,j+1,k)*cp(i,j,k)*v(i,j+1,k)*d1p2         &
                        &  +diffvsca(i,j+1,k)/dyv(j+1)) * dx(i)*dz(k) / vol
                   spsca(i,j,k,5)=spsca(i,j,k,5) + pen_amp*(                &
                        &  +rovv(i,j+1,k)*cp(i,j,k)*v(i,j+1,k)*d1p2         & 
                        &  -diffvsca(i,j+1,k)/dyv(j+1)) * dx(i)*dz(k) / vol
                   !---------------------------------------------------------------------
!!$                   spsca(i,j,k,1)=pen_amp
!!$                   spsca(i,j,k,5)=-pen_amp
                else if (slvv(i,j,k)/=0.and.slvv(i,j+1,k)==0) then
                   !---------------------------------------------------------------------
                   ! backward penalization 
                   !---------------------------------------------------------------------
                   spsca(i,j,k,1)=spsca(i,j,k,1) + pen_amp*(            &
                        &  -rovv(i,j,k)*cp(i,j,k)*v(i,j,k)*d1p2         &
                        &  +diffvsca(i,j,k)/dyv(j)) * dx(i)*dz(k) / vol
                   spsca(i,j,k,4)=spsca(i,j,k,4) + pen_amp*(            &
                        &  -rovv(i,j,k)*cp(i,j,k)*v(i,j,k)*d1p2         &
                        &  -diffvsca(i,j,k)/dyv(j)) * dx(i)*dz(k) / vol
                   !---------------------------------------------------------------------
!!$                   spsca(i,j,k,1)=pen_amp
!!$                   spsca(i,j,k,4)=-pen_amp
                end if
                !---------------------------------------------------------------------
                ! Z-faces
                !---------------------------------------------------------------------
                if (dim==3) then
                   if (slvw(i,j,k)/=0.and.slvw(i,j,k+1)/=0) then
                      !---------------------------------------------------------------------
                      ! object value
                      !---------------------------------------------------------------------
                      spsca(i,j,k,1)=pen_amp
                      smsca(i,j,k)=pen_amp*scainf
                      !---------------------------------------------------------------------
                   else if (slvw(i,j,k)==0.and.slvw(i,j,k+1)/=0) then
                      !---------------------------------------------------------------------
                      ! forward penalization 
                      !---------------------------------------------------------------------
                      spsca(i,j,k,1)=spsca(i,j,k,1) + pen_amp*(                &
                           &  +rovw(i,j,k+1)*cp(i,j,k)*w(i,j,k+1)*d1p2         &
                           &  +diffwsca(i,j,k+1)/dzw(k+1)) * dx(i)*dy(j) / vol
                      spsca(i,j,k,7)=spsca(i,j,k,7) + pen_amp*(                &
                           &  +rovw(i,j,k+1)*cp(i,j,k)*w(i,j,k+1)*d1p2         &
                           &  -diffwsca(i,j,k+1)/dzw(k+1)) * dx(i)*dy(j) / vol
                      !---------------------------------------------------------------------
                   else if (slvw(i,j,k)/=0.and.slvw(i,j,k+1)==0) then
                      !---------------------------------------------------------------------
                      ! backward penalization 
                      !---------------------------------------------------------------------
                      spsca(i,j,k,1)=spsca(i,j,k,1) + pen_amp*(            &
                           &  -rovw(i,j,k)*cp(i,j,k)*w(i,j,k)*d1p2         &
                           &  +diffwsca(i,j,k)/dzw(k)) * dx(i)*dy(j) / vol
                      spsca(i,j,k,6)=spsca(i,j,k,6) + pen_amp*(            &
                           &  -rovw(i,j,k)*cp(i,j,k)*w(i,j,k)*d1p2         &
                           &  -diffwsca(i,j,k)/dzw(k)) * dx(i)*dy(j) / vol
                      !---------------------------------------------------------------------
                   end if
                end if
                !---------------------------------------------------------------------
             end do
          end do
       end do
       !---------------------------------------------------------------------
    case(-500)
       !---------------------------------------------------------------------
       ! Work on diffusivity coefficient
       ! Set 0 diffusive flux if normal velocity is penalized
       !---------------------------------------------------------------------
       do k=sz-(dim-2),ez+(dim-2)
          do j=sy-1,ey+1
             do i=sx-1,ex+1
                if (slvu(i,j,k)/=0) diffusca(i,j,k)=diffusca(i,j,k)*ipen_amp/(ipen_amp+1)
                if (slvv(i,j,k)/=0) diffvsca(i,j,k)=diffvsca(i,j,k)*ipen_amp/(ipen_amp+1)
                if (dim==3) then
                   if (slvw(i,j,k)/=0) diffwsca(i,j,k)=diffwsca(i,j,k)*ipen_amp/(ipen_amp+1)
                end if
             end do
          end do
       end do

       
       
       x_min=x_min_injection
       x_max=x_max_injection
       y_min=y_min_injection
       y_max=y_min+0.168d0
       z_max=z_max_injection
       z_min=z_max-0.057d0

       scainf=1
       do k=sz-lgz,ez+lgz
          do j=sy-lgy,ey+lgy
             do i=sx-lgx,ex+lgx
                if (grid_x(i)>x_min.and.grid_x(i)<x_max) then
                   if (grid_y(j)>=y_min.and.grid_y(j)<=y_max) then
                      if (grid_z(k)>=z_min.and.grid_z(k)<=z_max) then
                         if (slvw(i,j,k+1)/=0) then 
                            slsca(i,j,k)=pen_amp
                            smsca(i,j,k)=pen_amp*scainf
                         end if
                      end if
                   end if
                end if
             end do
          end do
       end do
       !---------------------------------------------------------------------
    case(-501)
       !---------------------------------------------------------------------
       ! Work on diffusivity coefficient
       ! Set 0 diffusive flux if normal velocity is penalized
       !---------------------------------------------------------------------
       do k=sz-(dim-2),ez+(dim-2)
          do j=sy-1,ey+1
             do i=sx-1,ex+1
                if (slvu(i,j,k)/=0) diffusca(i,j,k)=diffusca(i,j,k)*ipen_amp/(ipen_amp+1)
                if (slvv(i,j,k)/=0) diffvsca(i,j,k)=diffvsca(i,j,k)*ipen_amp/(ipen_amp+1)
                if (dim==3) then
                   if (slvw(i,j,k)/=0) diffwsca(i,j,k)=diffwsca(i,j,k)*ipen_amp/(ipen_amp+1)
                end if
             end do
          end do
       end do

       x_min=x_min_injection
       x_max=x_max_injection
       y_min=y_min_injection
       y_max=y_max_injection
       z_max=z_max_injection
       z_min=z_max_injection-0.2d0

       scainf=1
       do k=sz-lgz,ez+lgz
          do j=sy-lgy,ey+lgy
             do i=sx-lgx,ex+lgx
                if (grid_x(i)>x_min.and.grid_x(i)<x_max) then
                   if (grid_y(j)>=y_min.and.grid_y(j)<=y_max) then
                      if (grid_z(k)>=z_min.and.grid_z(k)<=z_max) then
                         if (slvw(i,j,k+1)/=0) then 
                            slsca(i,j,k)=pen_amp
                            smsca(i,j,k)=pen_amp*scainf
                         end if
                      end if
                   end if
                end if
             end do
          end do
       end do
       !---------------------------------------------------------------------
       
    case default
       write(*,*) "Warning, Penaly case not known, EN_ipen=", EN_ipen
       write(*,*) "Warning, Penaly case not known, EN_ipen=", EN_ipen
       write(*,*) "Warning, Penaly case not known, EN_ipen=", EN_ipen
       write(*,*) "Use EN_ipen=0"
       write(*,*) "Use EN_ipen=0"
       write(*,*) "Use EN_ipen=0"
    end select

    !---------------------------------------------------------------------
    if (nproc>1) then
       if (allocated(smsca)) call comm_mpi_sca(smsca)
       if (allocated(slsca)) call comm_mpi_sca(slsca)
       if (allocated(diffusca)) call comm_mpi_uvw(diffusca,diffvsca,diffwsca)
    end if
    !---------------------------------------------------------------------

    !*******************************************************************************!
    !*******************************************************************************!
    !*******************************************************************************!
  end subroutine Penalty_Sca
  !*******************************************************************************!
  !*******************************************************************************!
  !*******************************************************************************!

  subroutine rescale_slv(slvu,slvv,slvw,slvu01,slvv01,slvw01)
    implicit none
    !---------------------------------------------------------------------
    ! Global variables                                            
    !---------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable, intent(in)  :: slvu,slvv,slvw
    real(8), dimension(:,:,:), allocatable, intent(out) :: slvu01,slvv01,slvw01
    !---------------------------------------------------------------------
    ! Local variables                                            
    !---------------------------------------------------------------------
    integer                                             :: i,j,k
    real(8)                                             :: eps=1d-40
    !---------------------------------------------------------------------

    if (.not.allocated(slvu01)) allocate(slvu01(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
    if (.not.allocated(slvv01)) allocate(slvv01(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
    if (.not.allocated(slvw01)) allocate(slvw01(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))

    do k=sz,ez
       do j=sy,ey
          do i=sx,ex
             slvu01(i,j,k)=(slvu(i,j,k)+slvu(i+1,j,k))
             slvv01(i,j,k)=(slvv(i,j,k)+slvv(i,j+1,k))
             if (dim==3) slvw01(i,j,k)=(slvw(i,j,k)+slvw(i,j,k+1))
          end do
       end do
    end do

    slvu01=slvu01/(slvu01+eps)
    slvv01=slvv01/(slvv01+eps)
    if (dim==3) slvw01=slvw01/(slvw01+eps)

  end subroutine rescale_slv

  subroutine rescale_pen(pen,pen01)
    implicit none
    !---------------------------------------------------------------------
    ! Global variables                                            
    !---------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable, intent(in)  :: pen
    real(8), dimension(:,:,:), allocatable, intent(out) :: pen01
    !---------------------------------------------------------------------
    ! Local variables                                            
    !---------------------------------------------------------------------
    real(8)                                             :: eps=1d-40
    !---------------------------------------------------------------------

    pen01=pen/(pen+eps)

  end subroutine rescale_pen


  subroutine compute_objet01(objet,slvu,slvv,slvw)
    implicit none
    !---------------------------------------------------------------------
    ! Global variables                                            
    !---------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable, intent(in)    :: slvu,slvv,slvw
    real(8), dimension(:,:,:), allocatable, intent(inout) :: objet
    !---------------------------------------------------------------------
    ! Local variables                                            
    !---------------------------------------------------------------------
    integer                                             :: i,j,k
    real(8)                                             :: eps=1d-40
    !---------------------------------------------------------------------

    objet=0
    do k=sz,ez
       do j=sy,ey
          do i=sx,ex

             if (slvu(i,j,k)/=0.and.slvu(i+1,j,k)/=0) objet(i,j,k)=1
             if (slvv(i,j,k)/=0.and.slvv(i,j+1,k)/=0) objet(i,j,k)=1
             if (dim==3) then
                if (slvw(i,j,k)/=0.and.slvw(i,j,k+1)/=0) objet(i,j,k)=1
             end if
          end do
       end do
    end do

  end subroutine compute_objet01

  
  subroutine Penalize_cubic_block(x_min,x_max,y_min,y_max,z_min,z_max,slvu,slvv,slvw)
    implicit none
    !---------------------------------------------------------------------
    ! Global variables                                            
    !---------------------------------------------------------------------
    real(8), intent(in)                                             :: x_min,x_max
    real(8), intent(in)                                             :: y_min,y_max
    real(8), intent(in)                                             :: z_min,z_max
    real(8), dimension(:,:,:), allocatable, intent(inout)           :: slvu,slvv,slvw
    !---------------------------------------------------------------------
    ! Local variables                                             
    !---------------------------------------------------------------------
    integer                                                         :: i,j,k
    integer                                                         :: lgx,lgy,lgz
    real(8)                                                         :: pen_amp
    !---------------------------------------------------------------------
    
    pen_amp=NS_pen

    
    lgx=gx-1
    lgy=gy-1
    lgz=gz-1
    if (dim==2) lgz=0
    
    do k=sz-lgz,ez+lgz
       do j=sy-lgy,ey+lgy
          do i=sx-lgx,ex+lgx
             if (grid_z(k)>=z_min.and.grid_z(k)<=z_max) then
                if (grid_y(j)>=y_min.and.grid_y(j)<=y_max) then
                   if (grid_x(i)>=x_min.and.grid_x(i)<=x_max) then
                      slvu(i  ,j,k)=pen_amp
                      slvu(i+1,j,k)=pen_amp
                      slvv(i,j  ,k)=pen_amp
                      slvv(i,j+1,k)=pen_amp
                      slvw(i,j,k  )=pen_amp
                      slvw(i,j,k+1)=pen_amp
                   end if
                end if
             end if
          end do
       end do
    end do
    
  end subroutine Penalize_cubic_block


  subroutine Purge_penalize_boundaries(slvu,slvv,slvw)
    implicit none
    !---------------------------------------------------------------------
    ! Global variables                                            
    !---------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable, intent(inout) :: slvu,slvv,slvw
    !---------------------------------------------------------------------
    ! Local variables                                            
    !---------------------------------------------------------------------
    integer :: i,j,k
    !---------------------------------------------------------------------

    !---------------------------------------------------------------------
    ! x-direction
    !---------------------------------------------------------------------
    do k=szu,ezu
       do j=syu,eyu
          do i=sxu,exu
             !-------------------------------------------------------
             if (i<=gsxu .or. i>=gexu) then
                slvu(i,j,k)=0
             end if
             !-------------------------------------------------------
             if (j<=gsyu .or. j>=geyu) then
                slvu(i,j,k)=0
             end if
             !-------------------------------------------------------
             if (dim==3) then
                if (k<=gszu .or. k>=gezu) then
                   slvu(i,j,k)=0
                end if
             end if
             !-------------------------------------------------------
          end do
       end do
    end do
    !---------------------------------------------------------------------
    ! y-direction
    !---------------------------------------------------------------------
    do k=szv,ezv
       do j=syv,eyv
          do i=sxv,exv
             !-------------------------------------------------------
             if (i<=gsxv .or. i>=gexv) then
                slvv(i,j,k)=0
             end if
             !-------------------------------------------------------
             if (j<=gsyv .or. j>=geyv) then
                slvv(i,j,k)=0
             end if
             !-------------------------------------------------------
             if (dim==3) then
                if (k<=gszv .or. k>=gezv) then
                   slvv(i,j,k)=0
                end if
             end if
             !-------------------------------------------------------
          end do
       end do
    end do
    !---------------------------------------------------------------------
    ! z-direction
    !---------------------------------------------------------------------
    if (dim==3) then
       do k=szw,ezw
          do j=syw,eyw
             do i=sxw,exw
                !-------------------------------------------------------
                if (i<=gsxw .or. i>=gexw) then
                   slvw(i,j,k)=0
                end if
                !-------------------------------------------------------
                if (j<=gsyw .or. j>=geyw) then
                   slvw(i,j,k)=0
                end if
                !-------------------------------------------------------
                if (k<=gszw .or. k>=gezw) then
                   slvw(i,j,k)=0
                end if
                !-------------------------------------------------------
             end do
          end do
       end do
    end if
    !-------------------------------------------------------

  end subroutine Purge_penalize_boundaries
  


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
end module mod_pen
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
