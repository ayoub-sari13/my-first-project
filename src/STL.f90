module mod_read_stl

  !********************************************************************************
  !>
  !  STL (STereoLithography) file library.
  !
  !### Author
  !  * Jacob Williams, Jan 12, 2020.
  !
  !### License
  !  * BSD-3
  !
  !### Reference
  !  * https://en.wikipedia.org/wiki/STL_(file_format)


  use,intrinsic :: iso_c_binding
  use,intrinsic :: iso_fortran_env, only: wp => real64, error_unit

  implicit none

  private

  ! constants:
  real(wp),parameter :: zero = 0.0_wp
  real(wp),parameter :: one  = 1.0_wp
  real(wp),parameter :: deg2rad = acos(-1.0_wp) / 180.0_wp !! degrees to radians
  real(wp),dimension(3),parameter :: x_unit = [one,zero,zero] !! x-axis unit vector
  real(wp),dimension(3),parameter :: y_unit = [zero,one,zero] !! y-axis unit vector
  real(wp),dimension(3),parameter :: z_unit = [zero,zero,one] !! z-axis unit vector

  type :: plate
     !! a 3D triangular plate.
     !! [note that the order of the vertices defines the
     !! surface normal via the right-hand rule]
     real(wp),dimension(3) :: v1 = zero  !! first vertex
     real(wp),dimension(3) :: v2 = zero  !! second vertex
     real(wp),dimension(3) :: v3 = zero  !! third vertex
  end type plate

  type,public :: stl_file
     !! the main class for STL file I/O.
     private
     integer :: n_plates = 0       !! number of plates
     integer :: chunk_size = 1000  !! expand `plates` array in chunks of this size
     type(plate),dimension(:),allocatable :: plates !! the array of plates
   contains
     private
     procedure,public :: write_ascii_stl_file
     procedure,public :: write_binary_stl_file
     procedure,public :: read_binary_stl_file
     procedure,public :: read => read_binary_stl_file
     procedure,public :: read_tab_file
     procedure,public :: destroy => destroy_stl_file
     procedure,public :: add_plate
     procedure,public :: add_sphere
     procedure,public :: add_cylinder
     procedure,public :: add_curve
     procedure,public :: add_cone
     procedure,public :: add_arrow
     procedure,public :: add_axes
     procedure,public :: shift_mesh
     procedure,public :: set_chunk_size
     procedure :: generate_circle
     procedure :: compute_vertex_scale
  end type stl_file

contains
  !********************************************************************************

  !********************************************************************************
  !>
  !  Destroy an `stl_file`.

  subroutine destroy_stl_file(me)

    implicit none

    class(stl_file),intent(inout) :: me

    if (allocated(me%plates)) deallocate(me%plates)
    me%n_plates = 0

  end subroutine destroy_stl_file
  !********************************************************************************

  !********************************************************************************
  !>
  !  Set the chunk size in the class.

  subroutine set_chunk_size(me,chunk_size)

    class(stl_file),intent(inout) :: me
    integer,intent(in)            :: chunk_size !! must be >0

    me%chunk_size = max(1,chunk_size)

  end subroutine set_chunk_size
  !********************************************************************************

  !********************************************************************************
  !>
  !  Add a plate to the class.

  subroutine add_plate(me,v1,v2,v3)

    class(stl_file),intent(inout)    :: me
    real(wp),dimension(3),intent(in) :: v1 !! first vertex
    real(wp),dimension(3),intent(in) :: v2 !! second vertex
    real(wp),dimension(3),intent(in) :: v3 !! third vertex

    integer :: n !! actual size of `plates` array in the class
    type(plate),dimension(:),allocatable :: tmp !! for resizing the `plates` array

    if (allocated(me%plates)) then

       n = size(me%plates)

       if (me%n_plates == n) then
          ! have to add another chunk
          allocate(tmp(n+me%chunk_size))
          tmp(1:n) = me%plates
          tmp(n+1)%v1 = v1
          tmp(n+1)%v2 = v2
          tmp(n+1)%v3 = v3
          call move_alloc(tmp,me%plates)
          me%n_plates = me%n_plates + 1
          return
       else
          ! add to next element in the class
          me%n_plates = me%n_plates + 1
       end if

    else
       allocate(me%plates(me%chunk_size))
       me%n_plates = 1
    end if

    me%plates(me%n_plates)%v1 = v1
    me%plates(me%n_plates)%v2 = v2
    me%plates(me%n_plates)%v3 = v3

  end subroutine add_plate
  !********************************************************************************

  !********************************************************************************
  !>
  !  Generate a binary STL file.
  !
  !### Notes
  !
  !  The file format is:
  !```
  !  UINT8[80] – Header
  !  UINT32 – Number of triangles
  !  foreach triangle
  !    REAL32[3] – Normal vector
  !    REAL32[3] – Vertex 1
  !    REAL32[3] – Vertex 2
  !    REAL32[3] – Vertex 3
  !    UINT16 – Attribute byte count
  !  end
  !```

  subroutine write_binary_stl_file(me,filename,istat,bounding_box)

    implicit none

    class(stl_file),intent(in)    :: me
    character(len=*),intent(in)   :: filename      !! STL file name
    integer,intent(out)           :: istat         !! `iostat` code (=0 if no errors)
    real(wp),intent(in),optional  :: bounding_box  !! scale vertices so that model fits in a
    !! box of this size (if <=0, no scaling is done)

    integer :: iunit                        !! file unit number
    integer :: i                            !! counter
    integer(c_int32_t) :: n_plates          !! number of plates [32 bits]
    real(c_float),dimension(3) :: n         !! normal vector [32 bits]
    real(c_float),dimension(3) :: v1,v2,v3  !! vertex vectors [32 bits]
    real(wp) :: scale                       !! scale factor

    integer(c_int16_t),parameter :: z = 0  !! Attribute byte count [16 bits]
    character(kind=c_char,len=80),parameter :: header = repeat(' ',80) !! [8 bits x 80]

    n_plates = int(me%n_plates,kind=c_int32_t)

    scale = me%compute_vertex_scale(bounding_box)

    ! open the binary file:
    open(newunit = iunit,&
         file    = filename,&
         action  = 'WRITE',&
         status  = 'REPLACE',&
         form    = 'UNFORMATTED', &
         access  = 'STREAM', &
         iostat  = istat)

    if (istat==0) then

       ! write the file:
       write(iunit,iostat=istat) header,n_plates
       if (istat==0) then
          do i = 1, me%n_plates
             n = real(normal(me%plates(i)%v1,me%plates(i)%v2,me%plates(i)%v3), c_float)
             v1 = real(me%plates(i)%v1*scale, c_float)
             v2 = real(me%plates(i)%v2*scale, c_float)
             v3 = real(me%plates(i)%v3*scale, c_float)
             write(iunit,iostat=istat) n,v1,v2,v3,z
             if (istat/=0) exit
          end do
       end if
       ! close the file:
       close(iunit)

    end if

  end subroutine write_binary_stl_file
  !********************************************************************************

  !********************************************************************************
  !>
  !  Read a binary STL file.

  subroutine read_binary_stl_file(me,filename,istat,nsommet,nface,xobj,yobj,zobj)


    implicit none

    class(stl_file),intent(out) :: me
    character(len=*),intent(in) :: filename !! STL file name
    integer,intent(out)         :: istat    !! `iostat` code (=0 if no errors)

    integer :: iunit                        !! file unit number
    integer :: i,j                            !! counter
    integer(c_int32_t) :: n_plates          !! number of plates [32 bits]
    real(c_float),dimension(3) :: n         !! normal vector [32 bits]
    real(c_float),dimension(3) :: v1,v2,v3  !! vertex vectors [32 bits]
    integer(c_int16_t) :: z                 !! Attribute byte count [16 bits]
    character(kind=c_char,len=80) :: header !! [8 bits x 80]

    integer,intent(out) :: nsommet,nface
    real(8),dimension(:),allocatable,intent(out) :: xobj,yobj,zobj

    call me%destroy()

    ! open the binary file:
    open(newunit = iunit,&
         file    = filename,&
         action  = 'READ',&
         status  = 'OLD',&
         form    = 'UNFORMATTED', &
         access  = 'STREAM', &
         iostat  = istat)

    if (istat==0) then

       ! read header:
       read(iunit,iostat=istat) header, n_plates

       nsommet=n_plates*3
       nface=n_plates

       if (.not.allocated(xobj)) allocate(xobj(1:nsommet))
       if (.not.allocated(yobj)) allocate(yobj(1:nsommet))
       if (.not.allocated(zobj)) allocate(zobj(1:nsommet))


       if (istat==0) then

          ! size arrays:
          me%n_plates = int(n_plates)
          allocate(me%plates(me%n_plates))

          ! read the data from the file:
          do i = 1, me%n_plates
             read(iunit,iostat=istat) n,v1,v2,v3,z
             if (istat/=0) exit
             ! only need to save the plates:
             ! Coordonnees x,y,z de chaque sommet de chaque face
             me%plates(i)%v1 = real(v1, wp)
             me%plates(i)%v2 = real(v2, wp)
             me%plates(i)%v3 = real(v3, wp)

          end do

          i=0
          do j = 1, me%n_plates
             i=i+1
             xobj(i)=me%plates(j)%v1(1)
             yobj(i)=me%plates(j)%v1(2)
             zobj(i)=me%plates(j)%v1(3)
             i=i+1
             xobj(i)=me%plates(j)%v2(1)
             yobj(i)=me%plates(j)%v2(2)
             zobj(i)=me%plates(j)%v2(3)
             i=i+1
             xobj(i)=me%plates(j)%v3(1)
             yobj(i)=me%plates(j)%v3(2)
             zobj(i)=me%plates(j)%v3(3)
          end do

       end if

       ! close the file:
       close(iunit, iostat=istat)

    end if

  end subroutine read_binary_stl_file
  !********************************************************************************

  !********************************************************************************
  !>
  !  Read a text vertex-facet file.
  !
  !### File Format
  !
  !  The file is a text file consisting of:
  !
  !  * Number of Vertices [int, %12d], Number of Triangular Plates [int, %12d]
  !  * Vertex table:
  !    * Vertex Number [int, %10d], x coordinate [real, %15.5f], y coordinate [real, %15.5f], z coordinate [real, %15.5f]
  !  * Plate table:
  !    * Plate Number [int, %10d], 1st Plate Vertex Number [int, %10d], 2nd Plate Vertex Number [int, %10d], 3rd Plate Vertex Number [int, %10d]
  !
  !### Example
  !
  !  * https://sbnarchive.psi.edu/pds4/non_mission/gaskell.phobos.shape-model/data/phobos_ver64q.tab
  !
  !```
  ! 25350        49152
  ! 1       -6.77444        6.26815        6.01149
  ! 2       -6.63342        6.34195        6.08444
  ! 3       -6.49302        6.41635        6.15759
  ! 4       -6.34883        6.48872        6.22619
  !  ...
  ! 1         1        67         2
  ! 2         1        66        67
  ! 3        66       132        67
  ! 4        66       131       132
  ! 5       131       197       132
  !  ...
  !```

  subroutine read_tab_file(me,filename,istat)

    implicit none

    class(stl_file),intent(out) :: me
    character(len=*),intent(in) :: filename !! Vertex-facet file name
    integer,intent(out)         :: istat    !! `iostat` code (=0 if no errors)

    integer :: iunit  !! file unit
    integer :: number_of_vertices !! number of vertices in the file
    integer :: number_of_plates   !! number of plates defined in the file (three vertices)
    integer :: i !! counter
    integer :: ii !! vertex or plate index
    integer :: i1 !! 1st vertex index of plate
    integer :: i2 !! 2nd vertex index of plate
    integer :: i3 !! 3rd vertex index of plate
    real(wp),dimension(:,:),allocatable :: v !! vertices from the file
    real(wp) :: x !! x coordinate of vertex
    real(wp) :: y !! y coordinate of vertex
    real(wp) :: z !! z coordinate of vertex

    !initialize:
    call me%destroy()

    !open the file:
    open(newunit = iunit,&
         file    = filename,&
         action  = 'READ',&
         status  = 'OLD',&
         iostat  = istat)

    if (istat==0) then

       read(iunit,*,iostat=istat) number_of_vertices, number_of_plates

       if (istat==0) then

          me%n_plates = number_of_plates
          allocate(me%plates(me%n_plates))
          allocate(v(3,number_of_vertices))

          ! first accumulate the vertex coordinates:
          do i = 1, number_of_vertices

             read(iunit,*,iostat=istat) ii, x, y, z

             !get the data:
             if (istat==0) then
                v(1,i) = x
                v(2,i) = y
                v(3,i) = z
             else
                write(error_unit,'(A)') 'Error reading vertex from file: '//trim(filename)
                call me%destroy()
                close(iunit)
                return
             end if

          end do

          ! now, read the plate vertex indices, and add them to the class
          do i = 1, number_of_plates

             read(iunit,*,iostat=istat) ii,i1,i2,i3

             if (istat==0) then
                me%plates(i)%v1 = v(:,i1)
                me%plates(i)%v2 = v(:,i2)
                me%plates(i)%v3 = v(:,i3)
             else
                write(error_unit,'(A)') 'Error reading plate from file: '//trim(filename)
                call me%destroy()
                close(iunit)
                return
             end if

          end do

          ! close the file:
          close(iunit)
          deallocate(v)

       else
          write(error_unit,'(A)') 'Error reading first line from file: '//trim(filename)
          close(iunit)
       end if

    end if

  end subroutine read_tab_file
  !********************************************************************************

  !********************************************************************************
  !>
  !  Generate an ascii STL file.

  subroutine write_ascii_stl_file(me,filename,modelname,istat,bounding_box)

    implicit none

    class(stl_file),intent(in)    :: me
    character(len=*),intent(in)   :: filename      !! STL file name
    character(len=*),intent(in)   :: modelname     !! the solid name (should not contain spaces)
    integer,intent(out)           :: istat         !! `iostat` code (=0 if no errors)
    real(wp),intent(in),optional  :: bounding_box  !! scale vertices so that model fits in a
    !! box of this size (if <=0, no scaling is done)

    integer  :: iunit  !! file unit number
    integer  :: i      !! counter
    real(wp) :: scale  !! scale factor

    character(len=*),parameter :: fmt = '(A,1X,E30.16,1X,E30.16,1X,E30.16)' !! format statement for vectors

    scale = me%compute_vertex_scale(bounding_box)

    ! open the text file:
    open(newunit=iunit, file=trim(filename), status='REPLACE', iostat=istat)

    if (istat==0) then

       ! write the file:
       write(iunit,'(A)') 'solid '//trim(modelname)
       do i = 1, me%n_plates
          write(iunit,fmt)    'facet normal', normal(me%plates(i)%v1,me%plates(i)%v2,me%plates(i)%v3)
          write(iunit,'(A)')  '    outer loop'
          write(iunit,fmt)    '        vertex', me%plates(i)%v1 * scale
          write(iunit,fmt)    '        vertex', me%plates(i)%v2 * scale
          write(iunit,fmt)    '        vertex', me%plates(i)%v3 * scale
          write(iunit,'(A)')  '    end loop'
          write(iunit,'(A)')  'end facet'
       end do
       write(iunit,'(A)') 'endsolid '//trim(modelname)

       ! close the file:
       close(iunit)

    end if

  end subroutine write_ascii_stl_file
  !********************************************************************************

  !********************************************************************************
  !>
  !  Compute the scale factor for the vertices (for writing to a file).

  pure function compute_vertex_scale(me,bounding_box) result(scale)

    implicit none

    class(stl_file),intent(in)    :: me
    real(wp),intent(in),optional  :: bounding_box  !! scale vertices so that model fits in a
    !! box of this size (if <=0, no scaling is done)
    real(wp)                      :: scale         !! scale factor

    real(wp) :: max_value  !! largest absolute value of any vertex coordinate
    integer  :: i          !! counter

    scale = one
    if (present(bounding_box)) then
       if (bounding_box>zero) then
          max_value = -huge(one)
          do i = 1, size(me%plates)
             max_value = max(max_value, maxval(abs(me%plates(i)%v1)),&
                  maxval(abs(me%plates(i)%v2)),&
                  maxval(abs(me%plates(i)%v3)) )
          end do
          scale = bounding_box / max_value
       end if
    end if

  end function compute_vertex_scale
  !********************************************************************************

  !********************************************************************************
  !>
  !  Shift the vertex coordinates so that there are no non-positive components.

  subroutine shift_mesh(me)

    implicit none

    class(stl_file),intent(inout) :: me

    integer :: i !! counter
    integer :: j !! counter
    real(wp),dimension(3) :: offset !! offset vector for vertext coordinates [x,y,z]
    real(wp),dimension(3) :: mins   !! min values of vertex coordinates [x,y,z]

    real(wp),parameter :: tiny = 1.0e-4_wp !! small value to avoid zero

    ! first find the min value of each coordinate:
    mins = huge(one)
    do i = 1, size(me%plates)
       do concurrent (j = 1:3)
          mins(j) = min(mins(j), &
               me%plates(i)%v1(j), &
               me%plates(i)%v2(j), &
               me%plates(i)%v3(j) )
       end do
    end do

    ! compute the offset vector:
    offset = zero
    do concurrent (j = 1:3)
       if (mins(j) <= zero) offset(j) = abs(mins(j)) + tiny
    end do

    if (any(offset/=zero)) then
       ! now add offset vector to each
       do i = 1, size(me%plates)
          me%plates(i)%v1 = me%plates(i)%v1 + offset
          me%plates(i)%v2 = me%plates(i)%v2 + offset
          me%plates(i)%v3 = me%plates(i)%v3 + offset
       end do
    end if

  end subroutine shift_mesh
  !********************************************************************************

  !********************************************************************************
  !>
  !  Add a sphere to an STL file.

  subroutine add_sphere(me,center,radius,num_lat_points,num_lon_points)

    implicit none

    class(stl_file),intent(inout)    :: me
    real(wp),dimension(3),intent(in) :: center         !! coordinates of sphere center [x,y,z]
    real(wp),intent(in)              :: radius         !! radius of the sphere
    integer,intent(in)               :: num_lat_points !! number of latitude points (not counting poles)
    integer,intent(in)               :: num_lon_points !! number of longitude points

    integer :: i  !! counter
    integer :: j  !! counter
    real(wp) :: delta_lat  !! step in latitude (deg)
    real(wp) :: delta_lon  !! step in longitude (deg)
    real(wp),dimension(:),allocatable :: lat !! array of latitude values (deg)
    real(wp),dimension(:),allocatable :: lon !! array of longitude value (deg)
    real(wp),dimension(3) :: v1,v2,v3,v4 !! vertices

    ! Example:
    !
    ! num_lat_points = 3
    ! num_lon_points = 5
    !
    !  90 -------------  North pole
    !     |  *  *  *  |
    !     |  *  *  *  |
    !     |  *  *  *  |
    ! -90 -------------  South pole
    !     0          360

    delta_lat = 180.0_wp / (1+num_lat_points)
    delta_lon = 360.0_wp / (1+num_lon_points)

    lat = -90.0_wp + [(delta_lat*(i-1), i = 1,num_lat_points+2)]
    lon =            [(delta_lon*(i-1), i = 1,num_lon_points+2)]

    ! generate all the plates on the sphere.
    ! start at bottom left and go right then up.
    ! each box is two triangular plates.
    do i = 1, num_lat_points+1
       do j = 1, num_lon_points+1

          !   3----2
          !   |  / |
          !   | /  |
          ! i 1----4
          !   j

          v1 = spherical_to_cartesian(radius,lon(j),  lat(i)  ) + center
          v2 = spherical_to_cartesian(radius,lon(j+1),lat(i+1)) + center
          v3 = spherical_to_cartesian(radius,lon(j),  lat(i+1)) + center
          v4 = spherical_to_cartesian(radius,lon(j+1),lat(i)  ) + center
          call me%add_plate(v1,v2,v3)
          call me%add_plate(v1,v4,v2)

       end do
    end do

  end subroutine add_sphere
  !********************************************************************************

  !********************************************************************************
  !>
  !  Add a cylinder to an STL file.
  !
  !  The cylinder is specified by the initial and final x,y,z coordinates. Optionally,
  !  an initial and final normal vector can be specified (if not specified,
  !  then a default one is constructed).

  subroutine add_cylinder(me,v1,v2,radius,num_points,initial_cap,final_cap,&
       initial_normal,final_normal,final_normal_used,initial_vector,final_initial_vector_used)

    implicit none

    class(stl_file),intent(inout)              :: me
    real(wp),dimension(3),intent(in)           :: v1                        !! coordinates of initial point
    real(wp),dimension(3),intent(in)           :: v2                        !! coordinates of final point
    real(wp),intent(in)                        :: radius                    !! radius of the cylinder
    integer,intent(in)                         :: num_points                !! number of point on the circle (>=3)
    logical,intent(in)                         :: initial_cap               !! add a cap plate to the initial point
    logical,intent(in)                         :: final_cap                 !! add a cap plate to the final point
    real(wp),dimension(3),intent(in),optional  :: initial_normal            !! outward normal vector for initial circle
    real(wp),dimension(3),intent(in),optional  :: final_normal              !! outward normal vector for final circle
    real(wp),dimension(3),intent(out),optional :: final_normal_used         !! outward normal vector for final circle
    !! actually used
    real(wp),dimension(3),intent(in),optional  :: initial_vector            !! vector to use to generate the initial
    !! circle (x_unit by default)
    real(wp),dimension(3),intent(out),optional :: final_initial_vector_used !! the initial vector used for the final
    !! cap to generate the points

    integer :: i  !! counter
    integer :: nc !! number of points on the circle
    real(wp),dimension(3) :: n0 !! normal vector for initial circle
    real(wp),dimension(3) :: nf !! normal vector for final circle
    real(wp),dimension(:,:),allocatable :: n0_cap_points !! points for the initial cap
    real(wp),dimension(:,:),allocatable :: nf_cap_points !! points for the final cap

    nc = max(3, num_points)

    ! compute the end unit vectors
    !
    !        1 _________2
    !        |          |
    !  n0 <--*----------*--> nf
    !        |          |
    !         ----------

    if (present(initial_normal)) then
       n0 = unit(initial_normal)
    else
       n0 = unit(v1-v2)
    end if
    if (present(final_normal)) then
       nf = unit(final_normal)
    else
       nf = unit(v2-v1)
    end if
    if (present(final_normal_used)) final_normal_used = nf ! return if necessary

    ! create the points on the initial cap (optionally add the plate)
    call me%generate_circle(v1,radius,n0,nc,initial_cap,n0_cap_points,initial_vector=initial_vector)

    ! create the points on the final cap (optionally add the plate)
    ! [use the same initial vector to sure that the plate will form a good cylinder]
    call me%generate_circle(v2,radius,nf,nc,final_cap,nf_cap_points,&
         initial_vector=unit(n0_cap_points(:,1)-v1),cw=.true.)
    if (present(final_initial_vector_used)) final_initial_vector_used = unit(nf_cap_points(:,1)-v2)

    ! now connect the points to form the cylinder:
    !   1----2  nf
    !   |  / |
    !   | /  |
    !   1----2  n0
    do i = 1, nc-1
       call me%add_plate(n0_cap_points(:,i),n0_cap_points(:,i+1),nf_cap_points(:,i+1))
       call me%add_plate(n0_cap_points(:,i),nf_cap_points(:,i+1),nf_cap_points(:,i))
    end do
    ! last one:
    !   n----1  nf
    !   |  / |
    !   | /  |
    !   n----1  n0
    call me%add_plate(n0_cap_points(:,nc),n0_cap_points(:,1),nf_cap_points(:,1))
    call me%add_plate(n0_cap_points(:,nc),nf_cap_points(:,1),nf_cap_points(:,nc))

  end subroutine add_cylinder
  !********************************************************************************

  !********************************************************************************
  !>
  !  Add a cone to an STL file.
  !
  !  The cylinder is specified by the initial and final x,y,z coordinates. Optionally,
  !  an initial and final normal vector can be specified (if not specified,
  !  then a default one is constructed).

  subroutine add_cone(me,v1,v2,radius,num_points,initial_cap,initial_normal)

    implicit none

    class(stl_file),intent(inout)             :: me
    real(wp),dimension(3),intent(in)          :: v1             !! coordinates of initial point (bottom of the cone)
    real(wp),dimension(3),intent(in)          :: v2             !! coordinates of final point (point of the cone)
    real(wp),intent(in)                       :: radius         !! radius of the cone (the bottom plate)
    integer,intent(in)                        :: num_points     !! number of point on the circle (>=3)
    logical,intent(in)                        :: initial_cap    !! add a cap plate to the initial point (bottom)
    real(wp),dimension(3),intent(in),optional :: initial_normal !! outward normal vector for initial plate (bottom)

    integer :: i  !! counter
    integer :: nc !! number of points on the circle
    real(wp),dimension(3) :: n0 !! normal vector for initial circle
    real(wp),dimension(:,:),allocatable :: n0_cap_points !! points for the initial cap

    nc = max(3, num_points)

    ! compute the end unit vector:
    if (present(initial_normal)) then
       n0 = unit(initial_normal)
    else
       n0 = unit(v1-v2)
    end if

    ! create the points on the initial cap (optionally add the plate)
    call me%generate_circle(v1,radius,n0,nc,initial_cap,n0_cap_points)

    ! draw the cone plates
    !      *     v2
    !     / \
    !    /   \
    !   1--*--2  v1
    do i = 1, nc-1
       call me%add_plate(n0_cap_points(:,i),n0_cap_points(:,i+1),v2)
    end do
    ! last one:
    call me%add_plate(n0_cap_points(:,nc),n0_cap_points(:,1),v2)

  end subroutine add_cone
  !********************************************************************************

  !********************************************************************************
  !>
  !  Add x,y,z axes to an STL file.

  subroutine add_axes(me,origin,vx,vy,vz,radius,num_points,&
       arrowhead_radius_factor,arrowhead_length_factor)

    implicit none

    class(stl_file),intent(inout)    :: me
    real(wp),dimension(3),intent(in) :: origin                  !! coordinates of the origin of the axes
    real(wp),dimension(3),intent(in) :: vx                      !! x axis vector
    real(wp),dimension(3),intent(in) :: vy                      !! y axis vector
    real(wp),dimension(3),intent(in) :: vz                      !! z axis vector
    real(wp),intent(in)              :: radius                  !! radius of the cylinder
    integer,intent(in)               :: num_points              !! number of point on the circle (>=3)
    real(wp),intent(in)              :: arrowhead_radius_factor !! arrowhead cone radius factor
    !! (multiple of cylinder radius)
    real(wp),intent(in)              :: arrowhead_length_factor !! arrowhead tip length factor
    !! (multiple of vector length)

    call me%add_arrow(origin,vx,radius,num_points,arrowhead_radius_factor,arrowhead_length_factor)
    call me%add_arrow(origin,vy,radius,num_points,arrowhead_radius_factor,arrowhead_length_factor)
    call me%add_arrow(origin,vz,radius,num_points,arrowhead_radius_factor,arrowhead_length_factor)

  end subroutine add_axes
  !********************************************************************************

  !********************************************************************************
  !>
  !  Add an arrow to an STL file.

  subroutine add_arrow(me,origin,v,radius,num_points,&
       arrowhead_radius_factor,arrowhead_length_factor)

    implicit none

    class(stl_file),intent(inout)    :: me
    real(wp),dimension(3),intent(in) :: origin                   !! coordinates of the origin of the axes
    real(wp),dimension(3),intent(in) :: v                        !! vector
    real(wp),intent(in)              :: radius                   !! radius of the cylinder
    integer,intent(in)               :: num_points               !! number of point on the circle (>=3)
    real(wp),intent(in)              :: arrowhead_radius_factor  !! arrowhead cone radius factor
    !! (multiple of cylinder radius)
    real(wp),intent(in)              :: arrowhead_length_factor  !! arrowhead tip length factor
    !! (multiple of vector length)

    call me%add_cylinder(origin,v,radius,num_points,&
         initial_cap=.true.,final_cap=.true.)
    call me%add_cone(v,v+arrowhead_length_factor*v,&
         arrowhead_radius_factor*radius,num_points,initial_cap=.true.)

  end subroutine add_arrow
  !********************************************************************************

  !********************************************************************************
  !>
  !  Generate the points in a circle, and optionally add it as a plate.

  subroutine generate_circle(me,c,radius,n,nc,add_circle,circle,initial_vector,cw)

    implicit none

    class(stl_file),intent(inout)                   :: me
    real(wp),dimension(3),intent(in)                :: c              !! center of the circle
    real(wp),intent(in)                             :: radius         !! radius of the cylinder
    real(wp),dimension(3),intent(in)                :: n              !! normal vector to the circle
    integer,intent(in)                              :: nc             !! number of points on the circle
    !! (must be at least 3)
    logical,intent(in)                              :: add_circle     !! to also add to the circle as a plate
    real(wp),dimension(:,:),allocatable,intent(out) :: circle         !! points on the circle
    real(wp),dimension(3),intent(in),optional       :: initial_vector !! vector to use to generate the initial
    !! circle (x_unit by default)
    logical,intent(in),optional                     :: cw             !! generate the points in the clockwise
    !! direction abound n (default is false)

    real(wp),dimension(3) :: v      !! initial vector for the circle
    integer               :: i      !! counter
    real(wp)              :: factor !! cw/ccw factor
    logical               :: compute_initial_vector !! if we need to compute an initial vector

    if (nc<3) error stop 'number of points on a circle must be at least 3'

    allocate(circle(3,nc))
    ! circle = -999

    factor = one
    if (present(cw)) then
       if (cw) factor = -one
    end if

    if (present(initial_vector)) then
       compute_initial_vector =  all(initial_vector==zero)
    else
       compute_initial_vector = .true.
    end if

    ! start with an initial vector on the circle (perpendicular to n0)
    ! [project x to circle (or y if x is parallel to n)]
    if (.not. compute_initial_vector) then
       v = unit(vector_projection_on_plane(initial_vector,n))
       if (.not. perpendicular(v, n) .or. all(v==zero)) then
          ! fall back to x or y axis
          v = unit(vector_projection_on_plane(x_unit,n))
          if (.not. perpendicular(v, n) .or. all(v==zero)) then
             v = unit(vector_projection_on_plane(y_unit,n))
          end if
       end if
    else
       v = unit(vector_projection_on_plane(x_unit,n))
       if (.not. perpendicular(v, n) .or. all(v==zero)) then
          v = unit(vector_projection_on_plane(y_unit,n))
       end if
    end if
    v = radius * unit(v)

    ! generate the points by rotating the initial vector around the circle:
    circle(:,1) = c + v
    do i = 2, nc
       circle(:,i) = c + axis_angle_rotation(v,n,(i-1)*factor*(360.0_wp/nc))
       if (add_circle) then
          ! draw the initial cap
          call me%add_plate(c,circle(:,i),circle(:,i-1))
       end if
    end do
    ! final plate that connects last to first
    if (add_circle) call me%add_plate(c,circle(:,1),circle(:,nc))

  end subroutine generate_circle
  !********************************************************************************

  !********************************************************************************
  !>
  !  Returns true if the two vectors are perpendicular.

  pure function perpendicular(v1, v2) result(is_parallel)

    implicit none

    real(wp),dimension(:),intent(in) :: v1
    real(wp),dimension(:),intent(in) :: v2
    logical :: is_parallel

    real(wp),parameter :: tol = 10.0_wp * epsilon(1.0_wp) !! tolerance

    is_parallel = abs(dot_product(unit(v1), unit(v2))) <= tol

  end function perpendicular
  !********************************************************************************

  !********************************************************************************
  !>
  !  Add a curve to an STL file.
  !
  !  A curve is a joined set of cylinders with no internal caps.

  subroutine add_curve(me,x,y,z,radius,num_points,&
       initial_cap,initial_normal,final_cap,final_normal,initial_vector)

    implicit none

    class(stl_file),intent(inout)             :: me
    real(wp),dimension(:),intent(in)          :: x              !! x coordinate array
    real(wp),dimension(:),intent(in)          :: y              !! y coordinate array
    real(wp),dimension(:),intent(in)          :: z              !! z coordinate array
    real(wp),intent(in)                       :: radius         !! radius of the cylinder
    integer,intent(in)                        :: num_points     !! number of point on the cylinder perimeter
    logical,intent(in),optional               :: initial_cap    !! add a cap plate to the initial point
    real(wp),dimension(3),intent(in),optional :: initial_normal !! outward normal vector for initial circle
    logical,intent(in),optional               :: final_cap      !! add a cap plate to the final point
    real(wp),dimension(3),intent(in),optional :: final_normal   !! outward normal vector for final circle
    real(wp),dimension(3),intent(in),optional :: initial_vector !! vector to use to generate the first circle (x_unit by default)

    integer               :: i      !! counter
    integer               :: n      !! number of points
    real(wp),dimension(3) :: nv     !! for intermediate normal vectors
    real(wp),dimension(3) :: nv_tmp !! for intermediate normal vectors
    real(wp),dimension(3) :: v      !! for intermediate initial vectors

    n = min(size(x), size(y), size(z))
    if (n<2) error stop 'error: a curve must have more than one point'

    ! first cylinder [no final cap unless only two points]
    call me%add_cylinder([x(1),y(1),z(1)],&
         [x(2),y(2),z(2)],&
         radius,num_points,&
         initial_cap=initial_cap,initial_normal=initial_normal,initial_vector=initial_vector,&
         final_cap=n==2,final_normal_used=nv,final_initial_vector_used=v)

    if (n>3) then
       ! intermediate cylinders (the initial normal is the final normal from the previous cylinder)
       do i = 2, n-2
          call me%add_cylinder([x(i),y(i),z(i)],&
               [x(i+1),y(i+1),z(i+1)],&
               radius,num_points,&
               initial_cap=.false.,initial_normal=-nv,&
               final_cap=.false.,final_normal_used=nv_tmp,&
               initial_vector=v)
          nv = unit(nv_tmp)
       end do
    end if

    ! last cylinder [no initial cap]
    if (n>=3) then
       call me%add_cylinder([x(n-1),y(n-1),z(n-1)],&
            [x(n),y(n),z(n)],&
            radius,num_points,&
            final_cap=final_cap,final_normal=final_normal,&
            initial_normal=-nv,initial_cap=.false.,&
            initial_vector=v)
    end if

  end subroutine add_curve
  !********************************************************************************

  !********************************************************************************
  !>
  !  Normal vector for the plate (computed using right hand rule).

  pure function normal(v1,v2,v3) result(n)

    implicit none

    real(wp),dimension(3),intent(in) :: v1  !! first vertex of the triangle [x,y,z]
    real(wp),dimension(3),intent(in) :: v2  !! second vertex of the triangle [x,y,z]
    real(wp),dimension(3),intent(in) :: v3  !! third vertex of the triangle [x,y,z]
    real(wp),dimension(3) :: n  !! surface normal vector

    n = unit( cross( v2-v1, v3-v1 ) )

  end function normal
  !********************************************************************************

  !********************************************************************************
  !>
  !  3x1 Unit vector.

  pure function unit(r) result(rhat)

    implicit none

    real(wp),dimension(3)            :: rhat
    real(wp),dimension(3),intent(in) :: r

    real(wp) :: rmag

    rmag = norm2(r)

    if (rmag/=zero) then
       rhat = r/rmag
    else
       rhat = zero
    end if

  end function unit
  !********************************************************************************

  !********************************************************************************
  !>
  !  Vector cross product.

  pure function cross(a,b) result(axb)

    implicit none

    real(wp),dimension(3) :: axb
    real(wp),dimension(3),intent(in) :: a
    real(wp),dimension(3),intent(in) :: b

    axb(1) = a(2)*b(3) - a(3)*b(2)
    axb(2) = a(3)*b(1) - a(1)*b(3)
    axb(3) = a(1)*b(2) - a(2)*b(1)

  end function cross
  !********************************************************************************

  !********************************************************************************
  !>
  !  Convert spherical (r,alpha,beta) to Cartesian (x,y,z).

  pure function spherical_to_cartesian(r,alpha,beta) result(rvec)

    implicit none

    real(wp),intent(in)   :: r        !! magnitude
    real(wp),intent(in)   :: alpha    !! right ascension [deg]
    real(wp),intent(in)   :: beta     !! declination [deg]
    real(wp),dimension(3) :: rvec     !! [x,y,z] vector

    rvec(1) = r * cos(alpha*deg2rad) * cos(beta*deg2rad)
    rvec(2) = r * sin(alpha*deg2rad) * cos(beta*deg2rad)
    rvec(3) = r * sin(beta*deg2rad)

  end function spherical_to_cartesian
  !********************************************************************************

  !********************************************************************************
  !> author: Jacob Williams
  !  date: 7/20/2014
  !
  !  Rotate a 3x1 vector in space, given an axis and angle of rotation.
  !
  !# Reference
  !   * [Wikipedia](http://en.wikipedia.org/wiki/Rodrigues%27_rotation_formula)

  pure function axis_angle_rotation(v,k,theta) result(vrot)

    implicit none

    real(wp),dimension(3),intent(in)  :: v      !! vector to rotate
    real(wp),dimension(3),intent(in)  :: k      !! rotation axis
    real(wp),intent(in)               :: theta  !! rotation angle [deg]
    real(wp),dimension(3)             :: vrot   !! result

    real(wp),dimension(3) :: khat
    real(wp) :: ct,st

    ct = cos(theta*deg2rad)
    st = sin(theta*deg2rad)
    khat = unit(k)   !rotation axis unit vector

    vrot = v*ct + cross(khat,v)*st + khat*dot_product(khat,v)*(one-ct)

  end function axis_angle_rotation
  !********************************************************************************

  !********************************************************************************
  !>
  !  The projection of one vector onto another vector.
  !
  !### Reference
  !   * [Wikipedia](http://en.wikipedia.org/wiki/Gram-Schmidt_process)
  !
  !### History
  !  * Jacob Williams : 7/21/2014
  !  * JW : fixed a typo : 6/18/2021

  pure function vector_projection(a,b) result(c)

    implicit none

    real(wp),dimension(:),intent(in)       :: a  !! the original vector
    real(wp),dimension(size(a)),intent(in) :: b  !! the vector to project on to
    real(wp),dimension(size(a))            :: c  !! the projection of a onto b

    real(wp) :: bmag2

    bmag2 = dot_product(b,b)

    if (bmag2==zero) then
       c = zero
    else
       c = b * dot_product(a,b) / bmag2
    end if

  end function vector_projection
  !********************************************************************************

  !********************************************************************************
  !>
  !  Project a vector onto a plane.
  !
  !### Reference
  !   * [Projection of a Vector onto a Plane](http://www.maplesoft.com/support/help/Maple/view.aspx?path=MathApps/ProjectionOfVectorOntoPlane)

  pure function vector_projection_on_plane(a,b) result(c)

    implicit none

    real(wp),dimension(3),intent(in)  :: a !! the original vector
    real(wp),dimension(3),intent(in)  :: b !! the plane to project on to (a normal vector)
    real(wp),dimension(3) :: c !! the projection of a onto the b plane

    c = a - vector_projection(a,b)

  end function vector_projection_on_plane
  !********************************************************************************

  !********************************************************************************
end module mod_read_stl
!********************************************************************************



module mod_stl
  use mod_Parameters, only: rank,  &
       & NS_pen,STL_infile,mesh,pi
  use mod_read_stl
  use mod_mpi
  use mod_pen
  use mod_search
  use mod_timers
  
  implicit none

  type index_vert_t
     integer, dimension(3) :: ijk = 0
     real(8), dimension(3) :: xyz = 0
  end type index_vert_t

  type index_face_t
     integer, dimension(3) :: idvert  = 0
     integer, dimension(3) :: ijk_min = 0
     integer, dimension(3) :: ijk_max = 0
     real(8), dimension(3) :: xyz_min = 0
     real(8), dimension(3) :: xyz_max = 0
  end type index_face_t

  type element_t
     integer                            :: nb_max = 250
     integer                            :: nb     = 0
     integer, dimension(:), allocatable :: element  
  end type element_t

  type geom_stl_t
     integer                                       :: nvert
     integer                                       :: nface
     type(index_vert_t), dimension(:), allocatable :: vert
     type(index_face_t), dimension(:), allocatable :: face
  end type geom_stl_t
  
contains

  subroutine Penalty_STL(id,slvu,slvv,slvw,obj)
    use mod_stl_num
    implicit none
    !---------------------------------------------------------------------
    ! Global variables                                            
    !---------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable, intent(inout) :: slvu,slvv,slvw
    real(8), dimension(:,:,:), allocatable, intent(inout) :: obj
    integer, intent(in)                                   :: id
    !---------------------------------------------------------------------
    ! Local variables                                             
    !---------------------------------------------------------------------
    integer                                               :: i,j,k,n
    integer                                               :: ii,jj,kk,nn
    integer                                               :: idir
    integer                                               :: nintersect
    integer                                               :: imethod
    integer                                               :: istat
    integer                                               :: nface,nsommet
    integer, dimension(3)                                 :: ijk
    logical                                               :: file_exists
    logical                                               :: intersect,intersectError
    real(8)                                               :: pen_amp
    real(8), dimension(3)                                 :: xyz
    real(8), dimension(3)                                 :: direction
    real(8), dimension(3)                                 :: origin
    real(8), dimension(3)                                 :: intersectPoint
    real(8), dimension(:), allocatable                    :: xobj,yobj,zobj
    real(8), dimension(:,:,:), allocatable                :: objr,objr_tmp
    type(stl_file)                                        :: model
    type(geom_stl_t)                                      :: geom
    type(element_t), dimension(:,:,:), allocatable        :: face_in_cell
    !---------------------------------------------------------------------

    pen_amp=1d20

    !---------------------------------------------------------------------
    ! Checks the existence of the stl file
    !---------------------------------------------------------------------
    inquire(file=trim(STL_infile%obj(id)%name), exist=file_exists)
    if (file_exists) then
       !---------------------------------------------------------------------
       ! Read the stl file
       !---------------------------------------------------------------------
       call model%read_binary_stl_file(trim(STL_infile%obj(id)%name),istat,nsommet,nface,xobj,yobj,zobj)
    else
       !---------------------------------------------------------------------
       ! Stop if stl file does not exist
       !---------------------------------------------------------------------
       if (rank==0) write(*,*) "WARNING, The specified STL file doesn't exist, STOP."
       stop
    end if
    !---------------------------------------------------------------------
    
    !---------------------------------------------------------------------
    ! Rotate STL with theta-dir angle (warning : rotate around geometrical center)
    !---------------------------------------------------------------------
    call Rotate_STL(xobj,yobj,zobj,STL_infile%obj(id)%angle)
    !---------------------------------------------------------------------
    ! Resize STL with L-dir length (homothety)
    !---------------------------------------------------------------------
    call Homothety_STL(xobj,yobj,zobj, &
         & STL_infile%obj(id)%length,  &
         & STL_infile%obj(id)%factor)
    !---------------------------------------------------------------------
    ! STL Positionning
    !---------------------------------------------------------------------
    call Positionning_STL(xobj,yobj,zobj, &
         & STL_infile%obj(id)%max,        &
         & STL_infile%obj(id)%min,        &
         & STL_infile%obj(id)%center)
    !---------------------------------------------------------------------

    !--------------------------------------------------
    ! stl struct
    !--------------------------------------------------
    
    geom%nvert = nsommet
    geom%nface = nface

    allocate(geom%vert(geom%nvert))
    allocate(geom%face(geom%nface))
    allocate(face_in_cell(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
    
    nn = 0
    do n = 1,geom%nface

       !--------------------------------------------------
       ! vertices 
       !--------------------------------------------------
       do i = 1,3 
          geom%vert(nn+i)%xyz = (/xobj(nn+i), yobj(nn+i), zobj(nn+i)/)
          !--------------------------------------------------
          call search_cell_ijk_global(mesh,geom%vert(nn+i)%xyz,geom%vert(nn+i)%ijk,PRESS_MESH)
          !--------------------------------------------------
       end do
       !--------------------------------------------------

       !--------------------------------------------------
       ! faces
       !--------------------------------------------------
       ! id triangle vertices
       !--------------------------------------------------
       geom%face(n)%idvert = (/nn+1, nn+2, nn+3/)
       !--------------------------------------------------

       !--------------------------------------------------
       ! ranges 
       !--------------------------------------------------
       j = 1
       do i = 1,3
          geom%face(n)%ijk_min(i) = min(geom%vert(nn+1)%ijk(i),geom%vert(nn+2)%ijk(i),geom%vert(nn+3)%ijk(i)) - j
          geom%face(n)%ijk_max(i) = max(geom%vert(nn+1)%ijk(i),geom%vert(nn+2)%ijk(i),geom%vert(nn+3)%ijk(i)) + j
          geom%face(n)%xyz_min(i) = min(geom%vert(nn+1)%xyz(i),geom%vert(nn+2)%xyz(i),geom%vert(nn+3)%xyz(i))
          geom%face(n)%xyz_max(i) = max(geom%vert(nn+1)%xyz(i),geom%vert(nn+2)%xyz(i),geom%vert(nn+3)%xyz(i))
       end do

       nn = nn + 3

       do k = geom%face(n)%ijk_min(3),geom%face(n)%ijk_max(3)
          do j = geom%face(n)%ijk_min(2),geom%face(n)%ijk_max(2)
             do i = geom%face(n)%ijk_min(1),geom%face(n)%ijk_max(1)

                if (i<mesh%sx-mesh%gx) cycle
                if (i>mesh%ex+mesh%gx) cycle
                if (j<mesh%sy-mesh%gy) cycle
                if (j>mesh%ey+mesh%gy) cycle
                if (k<mesh%sz-mesh%gz) cycle
                if (k>mesh%ez+mesh%gz) cycle

                if (.not.allocated(face_in_cell(i,j,k)%element)) then
                   face_in_cell(i,j,k)%nb_max = geom%nface
                   allocate(face_in_cell(i,j,k)%element(1:face_in_cell(i,j,k)%nb_max))
                   face_in_cell(i,j,k)%element = 0
                   face_in_cell(i,j,k)%nb      = 0
                end if

                face_in_cell(i,j,k)%nb = face_in_cell(i,j,k)%nb+1
                face_in_cell(i,j,k)%element(face_in_cell(i,j,k)%nb) = n

             end do
          end do
       end do

    end do

    imethod = 2
    
    !-------------------------------------------------------------
    ! ray tracing
    ! obj sign changes as soon as and interface is crossing
    !-------------------------------------------------------------
    obj = 1
    !-------------------------------------------------------------

    !-------------------------------------------------------------
    ! z-line
    !-------------------------------------------------------------
    i = mesh%sx-2
    j = mesh%sy-2
    do k = mesh%sz-2,mesh%ez+1 ! for //
       !-------------------------------------------------------------
       ! ray vector
       !-------------------------------------------------------------
       origin    = (/mesh%x(i), mesh%y(j), mesh%z(k)/)
       direction = (/0d0, 0d0, mesh%dzw(k+1)/)
       !-------------------------------------------------------------
       ! loop over possible triangles in cell
       !-------------------------------------------------------------
       nintersect = 0
       !-------------------------------------------------------------
       if (face_in_cell(i,j,k)%nb > 0) then
          do nn = 1,face_in_cell(i,j,k)%nb
             n = face_in_cell(i,j,k)%element(nn)
             call TriangleVectorIntersection(imethod,      &
                  & geom%vert(geom%face(n)%idvert(1))%xyz, &
                  & geom%vert(geom%face(n)%idvert(2))%xyz, &
                  & geom%vert(geom%face(n)%idvert(3))%xyz, &
                  & origin,                                &
                  & direction,                             &
                  & intersect,                             &
                  & intersectPoint,                        &
                  & intersectError)
             if (intersect) nintersect = nintersect + 1
          end do
       end if
       !-------------------------------------------------------------
       if (modulo(nintersect,2) == 1) then
          obj(i,j,k+1) = - obj(i,j,k)
       else
          obj(i,j,k+1) = + obj(i,j,k)
       end if
       !-------------------------------------------------------------
    end do
    !-------------------------------------------------------------
    ! // 
    !-------------------------------------------------------------
    if (mpi_dim(3)>1) then 
       allocate(objr(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
       allocate(objr_tmp(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))

       objr     = obj
       objr_tmp = objr
       
       call comm_mpi_sca(objr)
       
       i = mesh%sx
       j = mesh%sy
       k = mesh%sz-1
       
       do n = 1,mpi_dim(3)

          if (objr_tmp(i,j,k)*objr(i,j,k)<0) then
             objr(i,j,:) = - objr(i,j,:)
          end if
          objr_tmp = objr
          call comm_mpi_sca(objr)
       end do

       obj = objr
       obj(i,j,sz-gz:sz-1) = obj(i,j,sz-1)

       deallocate(objr)
       deallocate(objr_tmp)
    end if
    !-------------------------------------------------------------
    
    !-------------------------------------------------------------
    ! yz-plan
    !-------------------------------------------------------------
    i = mesh%sx-2
    do k = mesh%sz-2,mesh%ez+1
       do j = mesh%sy-2,mesh%ey+1
          !-------------------------------------------------------------
          ! ray vector
          !-------------------------------------------------------------
          origin    = (/mesh%x(i), mesh%y(j), mesh%z(k)/)
          direction = (/0d0, mesh%dyv(j+1), 0d0/)
          !-------------------------------------------------------------
          ! loop over possible triangles in cell
          !-------------------------------------------------------------
          nintersect = 0
          !-------------------------------------------------------------
          if (face_in_cell(i,j,k)%nb > 0) then
             do nn = 1,face_in_cell(i,j,k)%nb
                n = face_in_cell(i,j,k)%element(nn)
                call TriangleVectorIntersection(imethod,      &
                     & geom%vert(geom%face(n)%idvert(1))%xyz, &
                     & geom%vert(geom%face(n)%idvert(2))%xyz, &
                     & geom%vert(geom%face(n)%idvert(3))%xyz, &
                     & origin,                                &
                     & direction,                             &
                     & intersect,                             &
                     & intersectPoint,                        &
                     & intersectError)
                if (intersect) nintersect = nintersect + 1
             end do
          end if
          !-------------------------------------------------------------
          if (modulo(nintersect,2) == 1) then
             obj(i,j+1,k) = - obj(i,j,k)
          else
             obj(i,j+1,k) = + obj(i,j,k)
          end if
          !-------------------------------------------------------------
       end do
    end do
    !-------------------------------------------------------------
    ! // 
    !-------------------------------------------------------------
    if (mpi_dim(2)>1) then 
       allocate(objr(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
       allocate(objr_tmp(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))

       objr     = obj
       objr_tmp = objr

       call comm_mpi_sca(objr)
       
       i = mesh%sx
       j = mesh%sy-1
       do n = 1,mpi_dim(2)
          do k = mesh%sz,mesh%ez
             if (objr_tmp(i,j,k)*objr(i,j,k)<0) then
                objr(i,:,k) = - objr(i,:,k)
             end if
          end do
          objr_tmp = objr
          call comm_mpi_sca(objr)
       end do
       
       obj = objr
       do k = mesh%sz,mesh%ez
          obj(i,sy-gy:sy-1,k) = obj(i,sy-1,k)
       end do

       deallocate(objr)
       deallocate(objr_tmp)
    end if
    !-------------------------------------------------------------
    
    !-------------------------------------------------------------
    ! volume
    !-------------------------------------------------------------
    do k = mesh%sz-2,mesh%ez+1
       do j = mesh%sy-2,mesh%ey+1
          do i = mesh%sx-2,mesh%ex+1
             !-------------------------------------------------------------
             ! ray vector
             !-------------------------------------------------------------
             origin    = (/mesh%x(i), mesh%y(j), mesh%z(k)/)
             direction = (/mesh%dxu(i+1), 0d0, 0d0/)
             !-------------------------------------------------------------
             ! loop over possible triangles in cell
             !-------------------------------------------------------------
             nintersect = 0
             !-------------------------------------------------------------
             if (face_in_cell(i,j,k)%nb > 0) then
                do nn = 1,face_in_cell(i,j,k)%nb
                   n = face_in_cell(i,j,k)%element(nn)
                   call TriangleVectorIntersection(imethod,      &
                        & geom%vert(geom%face(n)%idvert(1))%xyz, &
                        & geom%vert(geom%face(n)%idvert(2))%xyz, &
                        & geom%vert(geom%face(n)%idvert(3))%xyz, &
                        & origin,                                &
                        & direction,                             &
                        & intersect,                             &
                        & intersectPoint,                        &
                        & intersectError)
                   if (intersect) nintersect = nintersect + 1
                end do
             end if
             !-------------------------------------------------------------
             if (modulo(nintersect,2) == 1) then
                obj(i+1,j,k) = - obj(i,j,k)
             else
                obj(i+1,j,k) = + obj(i,j,k)
             end if
             !-------------------------------------------------------------
          end do
       end do
    end do
    !-------------------------------------------------------------
    ! // 
    !-------------------------------------------------------------
    if (mpi_dim(1)>1) then 
       allocate(objr(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
       allocate(objr_tmp(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))

       objr     = obj
       objr_tmp = objr

       call comm_mpi_sca(objr)

       i = mesh%sx-1
       do n = 1,mpi_dim(1)
          do k = mesh%sz,mesh%ez
             do j = mesh%sy,mesh%ey
                if (objr_tmp(i,j,k)*objr(i,j,k)<0) then
                   objr(:,j,k) = - objr(:,j,k)
                end if
             end do
          end do
          objr_tmp = objr
          call comm_mpi_sca(objr)
       end do
       
       obj = objr
       do k = mesh%sz,mesh%ez
          do j = mesh%sy,mesh%ey
             obj(sx-gx:sx-1,j,k) = obj(sx-1,j,k)
          end do
       end do

       deallocate(objr)
       deallocate(objr_tmp)
    end if
    !-------------------------------------------------------------
    
    !---------------------------------------------------------------------
    ! projection on velocity meshes
    !---------------------------------------------------------------------
    obj = abs(obj-1)/2
    do k = mesh%sz,mesh%ez
       do j = mesh%sy,mesh%ey
          do i = mesh%sx,mesh%ex
             if (obj(i,j,k)*obj(i-1,j,k)/=0) slvu(i,j,k) = pen_amp
             if (obj(i,j,k)*obj(i,j-1,k)/=0) slvv(i,j,k) = pen_amp
             if (obj(i,j,k)*obj(i,j,k-1)/=0) slvw(i,j,k) = pen_amp
          end do
       end do
    end do
    !---------------------------------------------------------------------
    
  end subroutine Penalty_STL
  
  
  subroutine Rotate_STL(xobj,yobj,zobj,theta)
    implicit none
    !---------------------------------------------------------------------
    ! Global variables                                            
    !---------------------------------------------------------------------
    real(8), dimension(:),allocatable,intent(inout)                 :: xobj,yobj,zobj
    real(8),intent(in),dimension(3)                                 :: theta
    !---------------------------------------------------------------------
    ! Local variables                                             
    !---------------------------------------------------------------------
    real(8), dimension(:),allocatable                               :: xM,yM,zM
    real(8), dimension(3)                                           :: centerSTL
    real(8)                                                         :: xminObj,xmaxObj
    real(8)                                                         :: yminObj,ymaxObj
    real(8)                                                         :: zminObj,zmaxObj
    real(8)                                                         :: thetar
    !---------------------------------------------------------------------

    ! bounding box
    xminObj=minval(xobj) ; xmaxObj=maxval(xobj)
    yminObj=minval(yobj) ; ymaxObj=maxval(yobj)
    zminObj=minval(zobj) ; zmaxObj=maxval(zobj)

    ! Center of the object :
    centerSTL(1)=0.5d0*(xmaxObj+xminObj)
    centerSTL(2)=0.5d0*(ymaxObj+yminObj)
    centerSTL(3)=0.5d0*(zmaxObj+zminObj)

    if (.not.allocated(xM)) allocate(xM(1:size(xobj)))
    if (.not.allocated(yM)) allocate(yM(1:size(yobj)))
    if (.not.allocated(zM)) allocate(zM(1:size(zobj)))

    !---------------------------------------------------------------------
    ! Rotate points along X-dir with thetax angle
    !---------------------------------------------------------------------
    thetar=theta(1)*pi/180d0 ! radian
    yM=yobj-centerSTL(2)
    zM=zobj-centerSTL(3)
    yobj=yM*cos(thetar)+zM*sin(thetar)+centerSTL(2)
    zobj=-yM*sin(thetar)+zM*cos(thetar)+centerSTL(3)
    !---------------------------------------------------------------------
    ! Rotate points along Y-dir with thetay angle
    !---------------------------------------------------------------------
    thetar=theta(2)*pi/180d0
    xM=xobj-centerSTL(1)
    zM=zobj-centerSTL(3)
    xobj=xM*cos(thetar)+zM*sin(thetar)+centerSTL(1)
    zobj=-xM*sin(thetar)+zM*cos(thetar)+centerSTL(3)
    !---------------------------------------------------------------------
    ! Rotate points along Z-dir with thetaz angle
    !---------------------------------------------------------------------
    thetar=theta(3)*pi/180d0
    xM=xobj-centerSTL(1)
    yM=yobj-centerSTL(2)
    xobj=xM*cos(thetar)+yM*sin(thetar)+centerSTL(1)
    yobj=-xM*sin(thetar)+yM*cos(thetar)+centerSTL(2)
    !---------------------------------------------------------------------

  end subroutine Rotate_STL

  subroutine Homothety_STL(xobj,yobj,zobj,length,factor)
    implicit none
    !---------------------------------------------------------------------
    ! Global variables                                            
    !---------------------------------------------------------------------
    real(8), dimension(:), allocatable, intent(inout) :: xobj,yobj,zobj
    real(8), intent(in), dimension(3)                 :: length,factor
    !---------------------------------------------------------------------
    ! Local variables                                             
    !---------------------------------------------------------------------
    real(8)                                           :: xminObj,xmaxObj
    real(8)                                           :: yminObj,ymaxObj
    real(8)                                           :: zminObj,zmaxObj
    real(8)                                           :: homothety
    real(8)                                           :: Lx,Ly,Lz
    !---------------------------------------------------------------------


    xminObj = minval(xobj) ; xmaxObj = maxval(xobj)
    yminObj = minval(yobj) ; ymaxObj = maxval(yobj)
    zminObj = minval(zobj) ; zmaxObj = maxval(zobj)

    Lx = (xmaxObj-xminObj)
    Ly = (ymaxObj-yminObj)
    Lz = (zmaxObj-zminObj)
    
    homothety = 0

    if (sum(length)/=0) then ! test longueurs
       !------------------------------------------------------------------------
       ! if only 1 lenght is specified -> same homothety in all directions
       !------------------------------------------------------------------------
       if (length(1) >0 .and. length(2)==0 .and. length(3)==0) homothety = length(1)/Lx
       if (length(1)==0 .and. length(2) >0 .and. length(3)==0) homothety = length(2)/Ly
       if (length(1)==0 .and. length(2)==0 .and. length(3) >0) homothety = length(3)/Lz
       !------------------------------------------------------------------------
       xobj = xobj * homothety
       yobj = yobj * homothety
       zobj = zobj * homothety
       !------------------------------------------------------------------------
       ! if 2 or 3 dir --> per direction
       !------------------------------------------------------------------------
       if (homothety==0) then
          if (length(1)>0) then
             homothety = length(1)/Lx
             xobj      = xobj * homothety
          end if
          if (length(2)>0) then
             homothety = length(2)/Ly
             yobj      = yobj * homothety
          end if
          if (length(3)>0) then
             homothety = length(3)/Lz
             zobj      = zobj * homothety
          end if
       end if
       !------------------------------------------------------------------------
    end if

    xobj = xobj * factor(1)
    yobj = yobj * factor(2)
    zobj = zobj * factor(3)

  end subroutine Homothety_STL


  subroutine Positionning_STL(xobj,yobj,zobj,pos_max,pos_min,pos_center)
    implicit none
    !---------------------------------------------------------------------
    ! Global variables                                            
    !---------------------------------------------------------------------
    real(8), dimension(:), allocatable, intent(inout) :: xobj,yobj,zobj
    real(8), dimension(3), intent(in)                 :: pos_min,pos_max,pos_center
   !---------------------------------------------------------------------
    ! Local variables                                             
    !---------------------------------------------------------------------
    integer                                           :: i
    real(8)                                           :: xminObj,xmaxObj
    real(8)                                           :: yminObj,ymaxObj
    real(8)                                           :: zminObj,zmaxObj
    real(8), dimension(3)                             :: pos
    integer, dimension(3)                             :: ipos
    !---------------------------------------------------------------------

    xminObj=minval(xobj) ; xmaxObj=maxval(xobj)
    yminObj=minval(yobj) ; ymaxObj=maxval(yobj)
    zminObj=minval(zobj) ; zmaxObj=maxval(zobj)
    
    
    !---------------------------------------------------------------------
    ! Priority : Max > Min > center
    !---------------------------------------------------------------------
    do i = 1,3 ! loop over dimension 
       if (pos_center(i)<1d40) then
          pos(i)  = pos_center(i)
          ipos(i) = 1
       end if
       if (pos_min(i)<1d40) then
          pos(i)  = pos_min(i)
          ipos(i) = 2
       end if
       if (pos_max(i)<1d40) then
          pos(i)  = pos_max(i)
          ipos(i) = 3
       end if
    end do

    ! Positionnement du STL en donnant les valeurs du centre souhaitee
    select case(ipos(1))
    case(1)
       xobj=xobj-(0.5d0*(xminObj+xmaxObj)-pos(1))
    case(2)
       xobj=xobj-(xminObj-pos(1))
    case(3)
       xobj=xobj-(xmaxObj-pos(1))
    end select

    select case(ipos(2))
    case(1)
       yobj=yobj-(0.5d0*(yminObj+ymaxObj)-pos(2))
    case(2)
       yobj=yobj-(yminObj-pos(2))
    case(3)
       yobj=yobj-(ymaxObj-pos(2))
    end select

    select case(ipos(3))
    case(1)
       zobj=zobj-(0.5d0*(zminObj+zmaxObj)-pos(3))
    case(2)
       zobj=zobj-(zminObj-pos(3))
    case(3)
       zobj=zobj-(zmaxObj-pos(3))
    end select

  end subroutine Positionning_STL

  subroutine gather_vector_from_threads_for_STL(ulocal,uscalar)
    use mod_mpi
    implicit none
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable,intent(inout) :: ulocal, uscalar
    integer, dimension(:,:), allocatable :: tab_indices
    integer, dimension(:), allocatable :: tab2_indices
    real(8), dimension(:), allocatable :: tabu, tab1u
    integer :: nn, ii, jj, kk, i, j, k, n_i, n_j, n_k, l

    ! this subroutine gathers a distributed vector field
    ! uscalar,vscalar,wscalar on thread = rank = 0 in the fields
    ! ulocal,vlocal,wlocal.

    ! allocate needed arrays
    allocate(tab_indices(6,nproc), tab2_indices(nproc))
    tab_indices = 0; tab2_indices = 0

    ! get the starting and ending local indices sx,ex,sy,ey,sz,ez and store them
    CALL MPI_ALLGATHER(sx,1,MPI_INTEGER,tab2_indices,1,MPI_INTEGER,MPI_COMM_WORLD,MPI_CODE)
    tab_indices(1,:)=tab2_indices
    CALL MPI_ALLGATHER(ex,1,MPI_INTEGER,tab2_indices,1,MPI_INTEGER,MPI_COMM_WORLD,MPI_CODE)
    tab_indices(2,:)=tab2_indices
    CALL MPI_ALLGATHER(sy,1,MPI_INTEGER,tab2_indices,1,MPI_INTEGER,MPI_COMM_WORLD,MPI_CODE)
    tab_indices(3,:)=tab2_indices
    CALL MPI_ALLGATHER(ey,1,MPI_INTEGER,tab2_indices,1,MPI_INTEGER,MPI_COMM_WORLD,MPI_CODE)
    tab_indices(4,:)=tab2_indices
    CALL MPI_ALLGATHER(sz,1,MPI_INTEGER,tab2_indices,1,MPI_INTEGER,MPI_COMM_WORLD,MPI_CODE)
    tab_indices(5,:)=tab2_indices
    CALL MPI_ALLGATHER(ez,1,MPI_INTEGER,tab2_indices,1,MPI_INTEGER,MPI_COMM_WORLD,MPI_CODE)
    tab_indices(6,:)=tab2_indices

    if (rank == 0) then
       ulocal(sx:ex,sy:ey,sz:ez) = uscalar(sx:ex,sy:ey,sz:ez)
    endif

    !call comm_mpi_sca(ulocal)
    !call comm_mpi_sca(vlocal)
    !call comm_mpi_sca(wlocal)

    do nn=2, nproc
       n_i=tab_indices(2,nn)-tab_indices(1,nn)+1
       n_j=tab_indices(4,nn)-tab_indices(3,nn)+1
       n_k=tab_indices(6,nn)-tab_indices(5,nn)+1

       if (rank == nn-1) then
          ! allocate single dimension arrays
          allocate(tabu(1:n_i*n_j*n_k))
          ! print*,'rank = , sx , tab_indices(1,nn) = ,', rank, sx, tab_indices(1,nn)
          ! print*,'rank = , sy , tab_indices(3,nn) = ,', rank, sy, tab_indices(3,nn)
          ! print*,'rank = , sz , tab_indices(5,nn) = ,', rank, sz, tab_indices(5,nn)

          ! print*,'rank = , ex , tab_indices(2,nn) = ,', rank, ex, tab_indices(2,nn)
          ! print*,'rank = , ey , tab_indices(4,nn) = ,', rank, ey, tab_indices(4,nn)
          ! print*,'rank = , ez , tab_indices(6,nn) = ,',1 rank, ez, tab_indices(6,nn)

          ! wrap the local 3D array on a single dimension array 
          do kk=sz,ez
             do jj=sy,ey
                do ii=sx,ex

                   i=ii-sx
                   j=jj-sy
                   k=kk-sz

                   ! wrap here
                   if (dim==2) then
                      l = i+j*n_i+1
                   else
                      l = i + j*n_i + k*n_i*n_j + 1
                   end if

                   tabu(l) = uscalar(ii,jj,kk)

                end do
             end do
          end do

          ! send the single-dimension array on thread of rank = 0
          call MPI_SEND(tabu,size(tabu),MPI_DOUBLE_PRECISION,0,100,MPI_COMM_WORLD,code)
          !print*,'sending from rank data of size =', rank, size(tabu)

          deallocate(tabu)

       elseif (rank == 0) then

          ! allocate needed fields
          allocate(tab1u(1:n_i*n_j*n_k))

          !print*,'receiving on rank 0 of size = ', n_i*n_j*n_k

          ! receive the single-dimension array from thread of rank = nn - 1
          call MPI_RECV(tab1u,size(tab1u),MPI_DOUBLE_PRECISION,nn-1,100,MPI_COMM_WORLD,MPI_STATUS_IGNORE,code)

          ! unwrap here the single-dimensional array in the 3D array
          ! that is allocated with global indices
          do kk=tab_indices(5,nn),tab_indices(6,nn)
             do jj=tab_indices(3,nn),tab_indices(4,nn)
                do ii=tab_indices(1,nn),tab_indices(2,nn)

                   i=ii-tab_indices(1,nn)
                   j=jj-tab_indices(3,nn)
                   k=kk-tab_indices(5,nn)

                   if (dim==2) then
                      l = i + j*n_i + 1
                   else
                      l = i + j*n_i + k*n_i*n_j + 1
                   end if
                   ! print*,'rank 0, ii, jj, kk, l =', ii, jj, kk,l
                   ulocal(ii,jj,kk) = tab1u(l)
                end do
             end do
          end do

          deallocate(tab1u)

       end if
    enddo
    !print*,'end gather_vector_from_threads'
  end subroutine gather_vector_from_threads_for_STL

  subroutine spread_vector_to_threads_for_STL(ulocal,uscalar)
    use mod_mpi
    implicit none

    real(8), dimension(:,:,:), allocatable,intent(inout) :: ulocal, uscalar
    integer, dimension(:,:), allocatable :: tab_indices
    integer, dimension(:), allocatable :: tab2_indices
    real(8), dimension(:), allocatable :: tabu, tab1u
    integer :: nn, ii, jj, kk, i, j, k, n_i, n_j, n_k, l

    ! this subroutine spreads a globally kwown field
    ! ulocal,vlocal,wlocal on thread = rank = 0 to the other treads
    ! via mpi. The results are in the fields uscalar,vscalar,wscalar

    ! allocate needed arrays
    allocate(tab_indices(6,nproc), tab2_indices(nproc))
    tab_indices = 0; tab2_indices = 0

    ! get the starting and ending local indices sx,ex,sy,ey,sz,ez and store them
    CALL MPI_ALLGATHER(sx,1,MPI_INTEGER,tab2_indices,1,MPI_INTEGER,MPI_COMM_WORLD,MPI_CODE)
    tab_indices(1,:)=tab2_indices
    CALL MPI_ALLGATHER(ex,1,MPI_INTEGER,tab2_indices,1,MPI_INTEGER,MPI_COMM_WORLD,MPI_CODE)
    tab_indices(2,:)=tab2_indices
    CALL MPI_ALLGATHER(sy,1,MPI_INTEGER,tab2_indices,1,MPI_INTEGER,MPI_COMM_WORLD,MPI_CODE)
    tab_indices(3,:)=tab2_indices
    CALL MPI_ALLGATHER(ey,1,MPI_INTEGER,tab2_indices,1,MPI_INTEGER,MPI_COMM_WORLD,MPI_CODE)
    tab_indices(4,:)=tab2_indices
    CALL MPI_ALLGATHER(sz,1,MPI_INTEGER,tab2_indices,1,MPI_INTEGER,MPI_COMM_WORLD,MPI_CODE)
    tab_indices(5,:)=tab2_indices
    CALL MPI_ALLGATHER(ez,1,MPI_INTEGER,tab2_indices,1,MPI_INTEGER,MPI_COMM_WORLD,MPI_CODE)
    tab_indices(6,:)=tab2_indices

    do nn=2, nproc

       n_i=tab_indices(2,nn)-tab_indices(1,nn)+1
       n_j=tab_indices(4,nn)-tab_indices(3,nn)+1
       n_k=tab_indices(6,nn)-tab_indices(5,nn)+1

       if (rank == 0) then
          ! allocate single dimension arrays
          allocate(tabu(1:n_i*n_j*n_k))

          ! wrap the 3D array on a single dimension array 
          do kk=tab_indices(5,nn),tab_indices(6,nn)
             do jj=tab_indices(3,nn),tab_indices(4,nn)
                do ii=tab_indices(1,nn),tab_indices(2,nn)

                   i=ii-tab_indices(1,nn)
                   j=jj-tab_indices(3,nn)
                   k=kk-tab_indices(5,nn)

                   ! wrap here
                   if (dim==2) then
                      l = i+j*n_i+1
                   else
                      l = i + j*n_i + k*n_i*n_j + 1
                   end if

                   tabu(l) = ulocal(ii,jj,kk)

                end do
             end do
          end do

          ! send the single-dimension array to threads of rank > 0 
          call MPI_SEND(tabu,size(tabu),MPI_DOUBLE_PRECISION,nn-1,100,MPI_COMM_WORLD,code)
          deallocate(tabu)

          ! on thread 0 we have all the fields ulocal,vlocal,wlocal globally so we directly affect them
          uscalar(sx:ex,sy:ey,sz:ez) = ulocal(sx:ex,sy:ey,sz:ez)
       else  

          if (rank==nn-1) then

             allocate(tab1u((ex-sx+1)*(ey-sy+1)*(ez-sz+1)))

             ! receive the single-dimension array from thread of rank = 0 
             call MPI_RECV(tab1u,size(tab1u),MPI_DOUBLE_PRECISION,0,100,MPI_COMM_WORLD,MPI_STATUS_IGNORE,code)

             ! unwrap here the single-dimensional array in the 3D array
             do kk=sz,ez
                do jj=sy,ey
                   do ii=sx,ex
                      i=ii-sx
                      j=jj-sy
                      k=kk-sz

                      if (dim==2) then
                         l = i + j*n_i + 1
                      else
                         l = i + j*n_i + k*n_i*n_j + 1
                      end if

                      uscalar(ii,jj,kk) = tab1u(l)
                   end do
                end do
             end do
             deallocate(tab1u)
          end if
       end if
    end do

    ! sync the ghost cells for uscalar, vscalar, wscalar
    call comm_mpi_sca(uscalar)

  end subroutine spread_vector_to_threads_for_STL

  subroutine Moller_Trumbore_intersection_algorithm(xobj,yobj,zobj,nface,slv)
    implicit none
    !---------------------------------------------------------------------
    ! Global variables                                            
    !---------------------------------------------------------------------
    real(8),dimension(3)                               :: q,direction,e1,e2,s,itersec,origin,rstl
    real(8),dimension(:,:,:),allocatable,intent(inout) :: slv
    real(8),dimension(:),allocatable, intent(in)       :: xobj,yobj,zobj
    integer,intent(in)                                 :: nface
    !---------------------------------------------------------------------
    ! Local variables                                             
    !---------------------------------------------------------------------
    real(8) :: aa,ff,tt,uu,vv
    real(8) :: eps_MTR
    real(8) :: full_t1,full_t2,time_ellapsed
    integer :: i,j,k
    integer :: ii,jj,kk
    integer :: ll,o
    real(8) :: pen_amp
    real(8) :: xminObj,xmaxObj
    real(8) :: yminObj,ymaxObj
    real(8) :: zminObj,zmaxObj
    real(8) :: xx1,xx2,xx3
    real(8) :: yy1,yy2,yy3
    real(8) :: zz1,zz2,zz3


    !---------------------------------------------------------------------


    ! critere detection triangle paralleles aux rayons lancés
    eps_MTR=1d-40 ! georges : Ca engendre un cout ? Je me rappelle plus
    pen_amp=NS_pen

    ! Algorithme d'intersection de Möller–Trumbore
    if (rank==0) write(*,*) "Ray-tracing intersection algorithm working ..."
    full_t1=MPI_Wtime()

    ! Algorithme de desenrichissement de desir andre pour diminuer le cout ?
    ! Sinon, meshlab pour reduire le nombre de face du stl et reduire le cout

    xminObj=minval(xobj)
    xmaxObj=maxval(xobj)
    yminObj=minval(yobj)
    ymaxObj=maxval(yobj)
    zminObj=minval(zobj)
    zmaxObj=maxval(zobj)


    do ll=1,nface
       o=3*ll-2
       xx1=xobj(o)
       yy1=yobj(o)
       zz1=zobj(o)

       xx2=xobj(o+1)
       yy2=yobj(o+1)
       zz2=zobj(o+1)

       xx3=xobj(o+2)
       yy3=yobj(o+2)
       zz3=zobj(o+2)

       e1=(/xx2, yy2, zz2/)-(/xx1, yy1, zz1/)
       e2=(/xx3, yy3, zz3/)-(/xx1, yy1, zz1/)



       ! Xdir
       do k=sz,ez
          do j=sy,ey

             if (grid_z(k)>=zminObj.and.grid_z(k)<=zmaxObj) then
                if (grid_y(j)>=yminObj.and.grid_y(j)<=ymaxObj) then
                   origin=(/xmin, grid_y(j), grid_z(k) /)
                   direction=(/xmax, 0d0, 0d0 /)

                   q(1)=direction(2)*e2(3)-direction(3)*e2(2)
                   q(2)=direction(3)*e2(1)-direction(1)*e2(3)
                   q(3)=direction(1)*e2(2)-direction(2)*e2(1)


                   aa=e1(1)*q(1)+e1(2)*q(2)+e1(3)*q(3)                        


                   if (aa>-eps_MTR.and.aa<eps_MTR) then
                      !write(*,*) "le triangle est parallele au rayon lance"
                      cycle
                   end if

                   ff=1d0/aa

                   s=origin-(/xx1, yy1, zz1/)


                   uu=ff*(s(1)*q(1)+s(2)*q(2)+s(3)*q(3))

                   if (uu<0d0) then
                      !write(*,*) "intersection mais pas dans le triangle"
                      cycle
                   end if

                   rstl(1)=s(2)*e1(3)-s(3)*e1(2)
                   rstl(2)=s(3)*e1(1)-s(1)*e1(3)
                   rstl(3)=s(1)*e1(2)-s(2)*e1(1)


                   vv=ff*(direction(1)*rstl(1)+direction(2)*rstl(2)+direction(3)*rstl(3))

                   if (vv<0d0.or.uu+vv>1d0) then
                      !write(*,*) "intersection mais pas dans le triangle"
                      cycle
                   end if

                   tt=ff*(e2(1)*rstl(1)+e2(2)*rstl(2)+e2(3)*rstl(3))

                   itersec=origin+tt*direction

                   do ii=sx,ex
                      if (grid_x(ii)<=itersec(1).and.grid_x(ii+1)>=itersec(1)) then
                         !slvu(ii,j,k)=pen_amp
                         slv(ii,j,k)=pen_amp
                         !slvv(ii,j,k)=pen_amp
                         !slvw(ii,j,k)=pen_amp
                      end if
                   end do

                end if
             end if

          end do
       end do

       ! Ydir
       do k=sz,ez
          do i=sx,ex

             if (grid_z(k)>=zminObj.and.grid_z(k)<=zmaxObj) then
                if (grid_x(i)>=xminObj.and.grid_x(i)<=xmaxObj) then

                   origin=(/grid_x(i), ymin, grid_z(k) /)
                   direction=(/0d0, ymax, 0d0 /)

                   q(1)=direction(2)*e2(3)-direction(3)*e2(2)
                   q(2)=direction(3)*e2(1)-direction(1)*e2(3)
                   q(3)=direction(1)*e2(2)-direction(2)*e2(1)

                   aa=e1(1)*q(1)+e1(2)*q(2)+e1(3)*q(3)

                   if (aa>-eps_MTR.and.aa<eps_MTR) then
                      !write(*,*) "le triangle est parallele au rayon lance"
                      cycle
                   end if

                   ff=1d0/aa
                   s=origin-(/xx1, yy1, zz1/)

                   uu=ff*(s(1)*q(1)+s(2)*q(2)+s(3)*q(3))

                   if (uu<0) then
                      !write(*,*) "intersection mais pas dans le triangle"
                      cycle
                   end if

                   rstl(1)=s(2)*e1(3)-s(3)*e1(2)
                   rstl(2)=s(3)*e1(1)-s(1)*e1(3)
                   rstl(3)=s(1)*e1(2)-s(2)*e1(1)

                   vv=ff*(direction(1)*rstl(1)+direction(2)*rstl(2)+direction(3)*rstl(3))

                   if (vv<0.or.uu+vv>1) then
                      !write(*,*) "intersection mais pas dans le triangle"
                      cycle
                   end if

                   tt=ff*(e2(1)*rstl(1)+e2(2)*rstl(2)+e2(3)*rstl(3))

                   itersec=origin+tt*direction

                   do jj=sy,ey
                      if (grid_y(jj)<=itersec(2).and.grid_y(jj+1)>=itersec(2)) then
                         !slvu(i,jj,k)=pen_amp
                         slv(i,jj,k)=pen_amp
                         !slvv(i,jj,k)=pen_amp
                         !slvw(i,jj,k)=pen_amp
                         !global_slv(i,jj,k)=1
                         !slvu_y(jj)=1
                      end if
                   end do


                end if
             end if

          end do
       end do

       ! Zdir
       do j=sy,ey
          do i=sx,ex

             if (grid_y(j)>=yminObj.and.grid_y(j)<=ymaxObj) then
                if (grid_x(i)>=xminObj.and.grid_x(i)<=xmaxObj) then

                   origin=(/grid_x(i), grid_y(j), zmin /)
                   direction=(/0d0, 0d0, zmax /)

                   q(1)=direction(2)*e2(3)-direction(3)*e2(2)
                   q(2)=direction(3)*e2(1)-direction(1)*e2(3)
                   q(3)=direction(1)*e2(2)-direction(2)*e2(1)

                   aa=e1(1)*q(1)+e1(2)*q(2)+e1(3)*q(3)

                   if (aa>-eps_MTR.and.aa<eps_MTR) then
                      !write(*,*) "le triangle est parallele au rayon lance"
                      cycle
                   end if

                   ff=1d0/aa
                   s=origin-(/xx1, yy1, zz1/)

                   uu=ff*(s(1)*q(1)+s(2)*q(2)+s(3)*q(3))

                   if (uu<0) then
                      !write(*,*) "intersection mais pas dans le triangle"
                      cycle
                   end if

                   rstl(1)=s(2)*e1(3)-s(3)*e1(2)
                   rstl(2)=s(3)*e1(1)-s(1)*e1(3)
                   rstl(3)=s(1)*e1(2)-s(2)*e1(1)

                   vv=ff*(direction(1)*rstl(1)+direction(2)*rstl(2)+direction(3)*rstl(3))

                   if (vv<0.or.uu+vv>1) then
                      !write(*,*) "intersection mais pas dans le triangle"
                      cycle
                   end if

                   tt=ff*(e2(1)*rstl(1)+e2(2)*rstl(2)+e2(3)*rstl(3))

                   itersec=origin+tt*direction

                   do kk=sz,ez
                      if (grid_z(kk)<=itersec(3).and.grid_z(kk+1)>=itersec(3)) then
                         slv(i,j,kk)=pen_amp
                      end if
                   end do


                end if
             end if

          end do
       end do

    end do


  end subroutine Moller_Trumbore_intersection_algorithm


  subroutine Filling_STL(slv_fill,xminObj,xmaxObj,yminObj,ymaxObj,zminObj,zmaxObj)
    implicit none
    !---------------------------------------------------------------------
    ! Global variables                                            
    !---------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable, intent(inout) :: slv_fill
    real(8)                                               :: pen_amp
    real(8),intent(in)                                    :: xminObj,xmaxObj
    real(8),intent(in)                                    :: yminObj,ymaxObj
    real(8),intent(in)                                    :: zminObj,zmaxObj
    !---------------------------------------------------------------------
    ! Local variables                                             
    !---------------------------------------------------------------------
    integer :: cpt
    real(8) :: full_t1,full_t2,time_ellapsed
    integer :: i,j,k
    integer :: i0sl,j0sl,k0sl
    integer :: i1sl,j1sl,k1sl
    integer :: i2sl,j2sl,k2sl
    integer :: stlsx,stlex,stlsy,stley,stlsz,stlez
    real(8) :: x_min,x_max
    real(8) :: y_min,y_max
    real(8) :: z_min,z_max
    real(8) :: x,y,z
    !---------------------------------------------------------------------

    pen_amp=NS_pen

    cpt=0

    if (nproc>1) then
       stlsx=gsx
       stlex=gex
       stlsy=gsy
       stley=gey
       stlsz=gsz
       stlez=gez
    else
       stlsx=sx
       stlex=ex
       stlsy=sy
       stley=ey
       stlsz=sz
       stlez=ez
    end if

    if (rank==0) write(*,*) "Filling the geometry for fluid/structure interaction..."


    do k=stlsz,stlez
       z=grid_z(k)
       if (z>=zminObj.and.z<=zmaxObj) then
          do j=stlsy,stley
             y=grid_y(j)
             if (y>=yminObj.and.y<=ymaxObj) then
                do i=stlsx,stlex
                   x=grid_x(i)
                   if (x>=xminObj.and.x<=xmaxObj) then

                      cpt=0
                      if (slv_fill(i,j,k)==0) then
                         cpt=0
                         ! i et k fixe, j- variant (lancement d'un rayon en j-)
                         !1)
                         i0sl=i;k0sl=k

                         do j0sl=j,stlsy,-1 ! On parcourt de j vers sy
                            if (slv_fill(i0sl,j0sl,k0sl)==pen_amp) then
                               cpt=cpt+1
                               j1sl=j0sl
                               exit
                            end if
                         end do

                         ! i et k fixe, j+ variant
                         !2)
                         do j0sl=j,stley
                            if (slv_fill(i0sl,j0sl,k0sl)==pen_amp) then
                               cpt=cpt+1
                               j2sl=j0sl
                               exit
                            end if
                         end do
                         ! i et j fixe, k- variant
                         !3)
                         i0sl=i;j0sl=j
                         do k0sl=k,stlsz,-1
                            if (slv_fill(i0sl,j0sl,k0sl)==pen_amp) then
                               cpt=cpt+1
                               k1sl=k0sl
                               exit
                            end if
                         end do
                         ! i et j fixe, k+ variant
                         !4)
                         do k0sl=k,stlez
                            if (slv_fill(i0sl,j0sl,k0sl)==pen_amp) then
                               cpt=cpt+1
                               k2sl=k0sl
                               exit
                            end if
                         end do
                         ! j et k fixe, i- variant
                         !5)
                         j0sl=j;k0sl=k
                         do i0sl=i,stlsx,-1
                            if (slv_fill(i0sl,j0sl,k0sl)==pen_amp) then
                               cpt=cpt+1
                               i1sl=i0sl
                               exit
                            end if
                         end do
                         ! j et k fixe, i+ variant
                         !6)
                         j0sl=j;k0sl=k
                         do i0sl=i,stlex
                            if (slv_fill(i0sl,j0sl,k0sl)==pen_amp) then
                               cpt=cpt+1
                               i2sl=i0sl
                               exit
                            end if
                         end do

                         if (cpt==6) then
                            slv_fill(i,j,k)=pen_amp
                            cpt=0
                         end if
                      end if
                   end if
                end do
             end if
          end do
       end if
    end do




  end subroutine Filling_STL




end module mod_stl


