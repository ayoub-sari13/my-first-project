!===============================================================================
module Bib_VOFLag_TaylorInterpolation_PressureMesh_Hessian
  !===============================================================================
  contains
  !-----------------------------------------------------------------------------  
  !> @author amine chadil
  ! 
  !> @brief cette routine 
  !
  !> @param [in]  
  !> @param [in]  
  !> @param [out] 
  !> @param [in]  
  !-----------------------------------------------------------------------------
  subroutine TaylorInterpolation_PressureMesh_Hessian (pres,i,j,k,Order,norm,Hess,ndim,&
                                                       dx,dy,dz)
    !===============================================================================
    !modules
    !===============================================================================
    !-------------------------------------------------------------------------------
    !Modules de definition des structures et variables => src/mod/module_...f90
    !-------------------------------------------------------------------------------
    use Bib_VOFLag_DirectionOnEulerianMeshFollowingNormalVector
    !-------------------------------------------------------------------------------

    !===============================================================================
    !declarations des variables
    !===============================================================================
    implicit none
    !-------------------------------------------------------------------------------
    !variables globales
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable, intent(in)  :: pres
    real(8), dimension(:),     allocatable, intent(in)  :: dx,dy,dz
    integer                               , intent(in)  :: i,j,k,Order,ndim
    real(8), dimension(ndim)              , intent(in)  :: norm
    real(8), dimension(ndim,ndim)         , intent(out) :: Hess
    !-------------------------------------------------------------------------------
    !variables locales 
    !-------------------------------------------------------------------------------
    integer                             :: i_dir,j_dir,k_dir
    !-------------------------------------------------------------------------------
    Hess = 0.D0
    select case(Order)
    case(3)
        call DirectionOnEulerianMeshFollowingNormalVector (norm(1),i_dir)
        call DirectionOnEulerianMeshFollowingNormalVector (norm(2),j_dir)
        if (ndim==3) call DirectionOnEulerianMeshFollowingNormalVector (norm(3),k_dir)
        Hess(1,1) = (35d0*pres(i,j,k)&
                   -104d0*pres(i+i_dir,j,k)&
                   +114d0*pres(i+2*i_dir,j,k)&
                   - 56d0*pres(i+3*i_dir,j,k)&
                   + 11d0*pres(i+4*i_dir,j,k))&
                   /(12d0*dx(min(i,i+i_dir))**2+1D-40)
        Hess(2,2) = (35d0*pres(i,j,k)&
                   -104d0*pres(i,j+j_dir,k)&
                   +114d0*pres(i,j+2*j_dir,k)&
                   - 56d0*pres(i,j+3*j_dir,k)&
                   + 11d0*pres(i,j+4*j_dir,k))&
                   /(12d0*dy(min(j,j+j_dir))**2+1D-40)
        Hess(1,2) = ( 539d0*pres(i,j,k)&
                    - 781d0*pres(i+i_dir,j,k)&
                    + 297d0*pres(i+2*i_dir,j,k)&
                    -  55d0*pres(i+3*i_dir,j,k)&
                    - 781d0*pres(i,j+j_dir,k)&
                    +1035d0*pres(i+i_dir,j+j_dir,k)&
                    - 303d0*pres(i+2*i_dir,j+j_dir,k)&
                    +  49d0*pres(i+3*i_dir,j+j_dir,k)&
                    + 297d0*pres(i,j+2*j_dir,k)&
                    - 303d0*pres(i+i_dir,j+2*j_dir,k)&
                    +   3d0*pres(i+2*i_dir,j+2*j_dir,k)&
                    +   3d0*pres(i+3*i_dir,j+2*j_dir,k)&
                    -  55d0*pres(i,j+3*j_dir,k)&
                    +  49d0*pres(i+i_dir,j+3*j_dir,k)&
                    +   3d0*pres(i+2*i_dir,j+3*j_dir,k)&
                    +   3d0*pres(i+3*i_dir,j+3*j_dir,k))&
                    /(192d0*sign(1.D0,norm(1))*dx(min(i,i+i_dir))*sign(1.D0,norm(2))*dy(min(j,j+j_dir))+1D-40)

        Hess(2,1)=Hess(1,2)
        if (ndim==3) then 
          Hess(3,3) = (35d0*pres(i,j,k)&
                     -104d0*pres(i,j,k+k_dir)&
                     +114d0*pres(i,j,k+2*k_dir)&
                     - 56d0*pres(i,j,k+3*k_dir)&
                     + 11d0*pres(i,j,k+4*k_dir))&
                     /(12d0*dz(min(k,k+k_dir))**2+1D-40)

          Hess(1,3) = ( 539d0*pres(i,j,k)&
                      - 781d0*pres(i+i_dir,j,k)&
                      + 297d0*pres(i+2*i_dir,j,k)&
                      -  55d0*pres(i+3*i_dir,j,k)&
                      - 781d0*pres(i,j,k+k_dir)&
                      +1035d0*pres(i+i_dir,j,k+k_dir)&
                      - 303d0*pres(i+2*i_dir,j,k+k_dir)&
                      +  49d0*pres(i+3*i_dir,j,k+k_dir)&
                      + 297d0*pres(i,j,k+2*k_dir)&
                      - 303d0*pres(i+i_dir,j,k+2*k_dir)&
                      +   3d0*pres(i+2*i_dir,j,k+2*k_dir)&
                      +   3d0*pres(i+3*i_dir,j,k+2*k_dir)&
                      -  55d0*pres(i,j,k+3*k_dir)&
                      +  49d0*pres(i+i_dir,j,k+3*k_dir)&
                      +   3d0*pres(i+2*i_dir,j,k+3*k_dir)&
                      +   3d0*pres(i+3*i_dir,j,k+3*k_dir))&
                      /(192d0*sign(1.D0,norm(1))*dx(min(i,i+i_dir))*sign(1.D0,norm(3))*dz(min(k,k+k_dir))+1D-40)

          Hess(2,3) = ( 539d0*pres(i,j,k)&
                      - 781d0*pres(i,j+j_dir,k)&
                      + 297d0*pres(i,j+2*j_dir,k)&
                      -  55d0*pres(i,j+3*j_dir,k)&
                      - 781d0*pres(i,j,k+k_dir)&
                      +1035d0*pres(i,j+j_dir,k+k_dir)&
                      - 303d0*pres(i,j+2*j_dir,k+k_dir)&
                      +  49d0*pres(i,j+3*j_dir,k+k_dir)&
                      + 297d0*pres(i,j,k+2*k_dir)&
                      - 303d0*pres(i,j+j_dir,k+2*k_dir)&
                      +   3d0*pres(i,j+2*j_dir,k+2*k_dir)&
                      +   3d0*pres(i,j+3*j_dir,k+2*k_dir)&
                      -  55d0*pres(i,j,k+3*k_dir)&
                      +  49d0*pres(i,j+j_dir,k+3*k_dir)&
                      +   3d0*pres(i,j+2*j_dir,k+3*k_dir)&
                      +   3d0*pres(i,j+3*j_dir,k+3*k_dir))&
                      /(192d0*sign(1.D0,norm(2))*dy(min(j,j+j_dir))*sign(1.D0,norm(3))*dz(min(k,k+k_dir))+1D-40)

          Hess(3,2)=Hess(2,3)
          Hess(3,1)=Hess(1,3)
        end if 
    case(4)
        Hess(1,1) = ( 45d0*pres(i,j,k)&
                    -154d0*pres(i+i_dir,j,k)&
                    +214d0*pres(i+2*i_dir,j,k)&
                    -156d0*pres(i+3*i_dir,j,k)&
                    + 61d0*pres(i+4*i_dir,j,k)&
                    - 10d0*pres(i+5*i_dir,j,k))&
                    /(12d0*dx(min(i,i+i_dir))**2+1D-40)

        Hess(2,2) = ( 45d0*pres(i,j,k)&
                    -154d0*pres(i,j+j_dir,k)&
                    +214d0*pres(i,j+2*j_dir,k)&
                    -156d0*pres(i,j+3*j_dir,k)&
                    + 61d0*pres(i,j+4*j_dir,k)&
                    - 10d0*pres(i,j+5*j_dir,k))&
                    /(12d0*dy(min(j,j+j_dir))**2+1D-40)        

        Hess(1,2) = (117d0*pres(i,j,k)&
                    - 73d0*pres(i+i_dir,j,k)&
                    - 83d0*pres(i+2*i_dir,j,k)&
                    + 42d0*pres(i+3*i_dir,j,k)&
                    -  3d0*pres(i+4*i_dir,j,k)&
                    - 73d0*pres(i,j+j_dir,k)&
                    -243d0*pres(i+i_dir,j+j_dir,k)&
                    +477d0*pres(i+2*i_dir,j+j_dir,k)&
                    -173d0*pres(i+3*i_dir,j+j_dir,k)&
                    + 12d0*pres(i+4*i_dir,j+j_dir,k)&
                    - 83d0*pres(i,j+2*j_dir,k)&
                    +477d0*pres(i+1*i_dir,j+2*j_dir,k)&
                    -528d0*pres(i+2*i_dir,j+2*j_dir,k)&
                    +137d0*pres(i+3*i_dir,j+2*j_dir,k)&
                    -  3d0*pres(i+4*i_dir,j+2*j_dir,k)&
                    + 42d0*pres(i,j+3*j_dir,k)&
                    -173d0*pres(i+i_dir,j+3*j_dir,k)&
                    +137d0*pres(i+2*i_dir,j+3*j_dir,k)&
                    -  3d0*pres(i+3*i_dir,j+3*j_dir,k)&
                    -  3d0*pres(i+4*i_dir,j+3*j_dir,k)&
                    -  3d0*pres(i,j+4*j_dir,k)&
                    + 12d0*pres(i+i_dir,j+4*j_dir,k)&
                    -  3d0*pres(i+2*i_dir,j+4*j_dir,k)&
                    -  3d0*pres(i+3*i_dir,j+4*j_dir,k)&
                    -  3d0*pres(i+4*i_dir,j+4*j_dir,k))&
              /(60*sign(1.D0,norm(1))*dx(min(i,i+i_dir))*sign(1.D0,norm(2))*dy(min(j,j+j_dir))+1D-40)
        Hess(2,1)=Hess(1,2)
        if (ndim==3) then 
          Hess(3,3) = ( 45d0*pres(i,j,k)&
                    -154d0*pres(i,j,k+k_dir)&
                    +214d0*pres(i,j,k+2*k_dir)&
                    -156d0*pres(i,j,k+3*k_dir)&
                    + 61d0*pres(i,j,k+4*k_dir)&
                    - 10d0*pres(i,j,k+5*k_dir))&
                    /(12d0*dz(min(k,k+k_dir))**2+1D-40)

          Hess(1,3) = (117d0*pres(i,j,k)&
                      - 73d0*pres(i+i_dir,j,k)&
                      - 83d0*pres(i+2*i_dir,j,k)&
                      + 42d0*pres(i+3*i_dir,j,k)&
                      -  3d0*pres(i+4*i_dir,j,k)&
                      - 73d0*pres(i,j,k+k_dir)&
                      -243d0*pres(i+i_dir,j,k+k_dir)&
                      +477d0*pres(i+2*i_dir,j,k+k_dir)&
                      -173d0*pres(i+3*i_dir,j,k+k_dir)&
                      + 12d0*pres(i+4*i_dir,j,k+k_dir)&
                      - 83d0*pres(i,j,k+2*k_dir)&
                      +477d0*pres(i+1*i_dir,j,k+2*k_dir)&
                      -528d0*pres(i+2*i_dir,j,k+2*k_dir)&
                      +137d0*pres(i+3*i_dir,j,k+2*k_dir)&
                      -  3d0*pres(i+4*i_dir,j,k+2*k_dir)&
                      + 42d0*pres(i,j,k+3*k_dir)&
                      -173d0*pres(i+i_dir,j,k+3*k_dir)&
                      +137d0*pres(i+2*i_dir,j,k+3*k_dir)&
                      -  3d0*pres(i+3*i_dir,j,k+3*k_dir)&
                      -  3d0*pres(i+4*i_dir,j,k+3*k_dir)&
                      -  3d0*pres(i,j,k+4*k_dir)&
                      + 12d0*pres(i+i_dir,j,k+4*k_dir)&
                      -  3d0*pres(i+2*i_dir,j,k+4*k_dir)&
                      -  3d0*pres(i+3*i_dir,j,k+4*k_dir)&
                      -  3d0*pres(i+4*i_dir,j,k+4*k_dir))&
                /(60*sign(1.D0,norm(1))*dx(min(i,i+i_dir))*sign(1.D0,norm(3))*dz(min(k,k+k_dir))+1D-40)

          Hess(2,3) = (117d0*pres(i,j,k)&
                    - 73d0*pres(i,j+j_dir,k)&
                    - 83d0*pres(i,j+2*j_dir,k)&
                    + 42d0*pres(i,j+3*j_dir,k)&
                    -  3d0*pres(i,j+4*j_dir,k)&
                    - 73d0*pres(i,j,k+k_dir)&
                    -243d0*pres(i,j+j_dir,k+k_dir)&
                    +477d0*pres(i,j+2*j_dir,k+k_dir)&
                    -173d0*pres(i,j+3*j_dir,k+k_dir)&
                    + 12d0*pres(i,j+4*j_dir,k+k_dir)&
                    - 83d0*pres(i,j,k+2*k_dir)&
                    +477d0*pres(i,j+1*j_dir,k+2*k_dir)&
                    -528d0*pres(i,j+2*j_dir,k+2*k_dir)&
                    +137d0*pres(i,j+3*j_dir,k+2*k_dir)&
                    -  3d0*pres(i,j+4*j_dir,k+2*k_dir)&
                    + 42d0*pres(i,j,k+3*k_dir)&
                    -173d0*pres(i,j+j_dir,k+3*k_dir)&
                    +137d0*pres(i,j+2*j_dir,k+3*k_dir)&
                    -  3d0*pres(i,j+3*j_dir,k+3*k_dir)&
                    -  3d0*pres(i,j+4*j_dir,k+3*k_dir)&
                    -  3d0*pres(i,j,k+4*k_dir)&
                    + 12d0*pres(i,j+j_dir,k+4*k_dir)&
                    -  3d0*pres(i,j+2*j_dir,k+4*k_dir)&
                    -  3d0*pres(i,j+3*j_dir,k+4*k_dir)&
                    -  3d0*pres(i,j+4*j_dir,k+4*k_dir))&
              /(60*sign(1.D0,norm(2))*dy(min(j,j+j_dir))*sign(1.D0,norm(3))*dz(min(k,k+k_dir))+1D-40)
          Hess(3,1)=Hess(1,3)
          Hess(3,2)=Hess(2,3)
        end if 

    case default
      write(*,*) "STOP : ordre non implemente (Hessian) "
      stop
    end select

  end subroutine TaylorInterpolation_PressureMesh_Hessian

  !===============================================================================
end module Bib_VOFLag_TaylorInterpolation_PressureMesh_Hessian
!===============================================================================
  


