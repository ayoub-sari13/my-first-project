
!========================================================================
!
!                   FUGU Version 1.0.0
!
!========================================================================
!
! Copyright  Stéphane Vincent
!
! stephane.vincent@u-pem.fr          straightparadigm@gmail.com
!
! This file is part of the FUGU Fortran library, a set of Computational 
! Fluid Dynamics subroutines devoted to the simulation of multi-scale
! multi-phase flows for resolved scale interfaces (liquid/gas/solid). 
! FUGU uses the initial developments of DyJeAT and SHALA codes from 
! Jean-Luc Estivalezes (jean-luc.estivalezes@onera.fr) and 
! Frédéric Couderc (couderc@math.univ-toulouse.fr).
!
!========================================================================
!**
!**   NAME        : Interpolation.f90
!**
!**   AUTHOR      : Stéphane Vincent
!**                 Georges Halim Atallah
!**                 Benoit Trouette
!**
!**   FUNCTION    : Interpolation routines for velocity fields in 2D
!**                 and 3D
!**
!**   DATES       : Version 1.0.0  : from : May, 2018
!**
!**   SUBROUTINES : 
!========================================================================

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
module mod_interpolation
  use mod_Parameters, only: mesh
  use mod_Interp
  use mod_Perm
  use mod_Peskin
  
contains
  
  subroutine interpolation_uvw(time,u,v,w,xyz,vp,choice,dim)
    !***************************************************************************************
    use mod_Constants, only: d1p2
    implicit none 
    !***************************************************************************************
    !Global variables
    !***************************************************************************************
    integer, intent(in)                                :: choice,dim
    real(8), intent(in)                                :: time
    real(8), allocatable, dimension(:,:,:), intent(in) :: u,v,w
    real(8), dimension(:), intent(in)                  :: xyz
    real(8), dimension(:), intent(inout)               :: vp
    !***************************************************************************************
    !Local variables
    !***************************************************************************************
    integer                                            :: i,j,k,code
    !***************************************************************************************

    code=choice

    !---------------------------------------------------------------------------
    ! Interp Q1 irregular grid
    !---------------------------------------------------------------------------
    if (.not.regular_mesh) code=1
    !---------------------------------------------------------------------------
    
    !---------------------------------------------------------------------------
    ! Interp Q1 if boundary node
    !---------------------------------------------------------------------------
    if (code/=1) then
       i=(xyz(1)-(mesh%xmin-mesh%dx(mesh%sx)/2))/mesh%dx(mesh%sx)
       j=(xyz(2)-(mesh%ymin-mesh%dy(mesh%sy)/2))/mesh%dy(mesh%sy)
       if (i<=mesh%gsx.or.i>=mesh%gex.or.j<=mesh%gsy.or.j>=mesh%gey) code=1
       if (dim==3) then 
          k=(xyz(dim)-(mesh%zmin-mesh%dz(mesh%sz)/2))/mesh%dz(mesh%sz)
          if (k<=mesh%gsz.or.k>=mesh%gez) code=1
       end if
    end if
    !---------------------------------------------------------------------------
    
    select case(code)
    case(1)
       if (dim==2) then
          call Interp2D_uvw(u,v,xyz,vp)
       else if (dim==3) then
          call Interp3D_uvw(u,v,w,xyz,vp)
       end if
    case(2)
       if (dim==2) then
          call Perm2D(u,v,xyz,vp)
       else if (dim==3) then
          call Perm3D(u,v,w,xyz,vp)
       end if
    case(3)
       if (dim==2) then
          call peskin_interpolation(u,v,xyz,vp)
       else if (dim==3) then
          write(*,*) "Peskin not done in 3D, STOP"
          stop
       end if
    end select
    
  end subroutine interpolation_uvw
  !*****************************************************************************************
  subroutine interpolation_phi(time,var,xyz,varp,choice,dim)
    !***************************************************************************************
    implicit none 
    !***************************************************************************************
    !Global variables
    !***************************************************************************************
    integer, intent(in)                                :: choice,dim
    real(8), intent(in)                                :: time
    real(8), allocatable, dimension(:,:,:), intent(in) :: var
    real(8), dimension(:), intent(in)                  :: xyz 
    real(8), intent(inout)                             :: varp
    !***************************************************************************************
    !Local variables
    !***************************************************************************************
    !***************************************************************************************
    if (dim==2) then
       call Interp2D_phi(var,xyz,varp)
    else if (dim==3) then
       call Interp3D_phi(var,xyz,varp)
    end if
    
  end subroutine interpolation_phi
  !*****************************************************************************************
  subroutine interpolation_rot(time,rotx,roty,rotz,xyz,rotp,choice,dim)
    !***************************************************************************************
    implicit none 
    !***************************************************************************************
    !Global variables
    !***************************************************************************************
    integer, intent(in)                                :: choice,dim
    real(8), intent(in)                                :: time
    real(8), allocatable, dimension(:,:,:), intent(in) :: rotx,roty,rotz
    real(8), dimension(dim), intent(in)                :: xyz 
    real(8), dimension(:), intent(inout)               :: rotp
    !***************************************************************************************
    !Local variables
    !***************************************************************************************
    !***************************************************************************************
    if (dim==2) then
       call interp2D_rot(rotz,xyz,rotp)
    else if (dim==3) then
       call interp3D_rot(rotx,roty,rotz,xyz,rotp)
    end if

  end subroutine interpolation_rot
  !*****************************************************************************************

end module mod_interpolation
