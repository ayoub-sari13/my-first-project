!========================================================================
!
!                   FUGU Version 1.0.0
!
!========================================================================
!
! This file is part of the FUGU Fortran library, a set of Computational 
! Fluid Dynamics subroutines devoted to the simulation of multi-scale
! multi-phase flows for resolved scale interfaces (liquid/gas/solid). 
!
!========================================================================
!**
!**   NAME       : Borders.f90
!**
!**   AUTHOR     : Benoit Trouette
!**
!**   FUNCTION   : Borders and Periodicity for explicit variables
!**
!**   DATES      : March, 2020
!**
!========================================================================

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
module mod_borders
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  subroutine sca_borders_and_periodicity(mesh,sca)
    use mod_struct_grid
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable, intent(inout) :: sca
    type(grid_t), intent(in)                              :: mesh
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------

    call sca_periodicity(mesh,sca)
    call sca_borders(mesh,sca)

  end subroutine sca_borders_and_periodicity

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  subroutine uvw_borders_and_periodicity(mesh,u,v,w)
    use mod_struct_grid
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable, intent(inout) :: u,v,w
    type(grid_t), intent(in)                              :: mesh
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------

    call uvw_periodicity(mesh,u,v,w)
    call uvw_borders(mesh,u,v,w)

  end subroutine uvw_borders_and_periodicity

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  subroutine sca_periodicity(mesh,sca)
    use mod_Parameters, only: nproc
    use mod_struct_grid
    use mod_mpi
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable, intent(inout) :: sca
    type(grid_t), intent(in)                              :: mesh
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                               :: i,j
    !-------------------------------------------------------------------------------

    if (nproc==1) then 
       !-------------------------------------------------------------------------------
       ! X-direction
       !-------------------------------------------------------------------------------
       if (mesh%periodic(1)) then

          do i=mesh%sx,mesh%ex
             if (i==mesh%gsx) then
                do j=1,mesh%gx
                   sca(i-j,:,:)=sca(i-j+mesh%gex,:,:)
                end do
             else if (i==mesh%gex) then
                do j=1,mesh%gx
                   sca(i+j,:,:)=sca(i+j-mesh%gex,:,:)
                end do
             end if
          end do
       end if
       !-------------------------------------------------------------------------------
       ! Y-direction
       !-------------------------------------------------------------------------------
       if (mesh%periodic(2)) then
          do i=mesh%sy,mesh%ey
             if (i==mesh%gsy) then
                do j=1,mesh%gy 
                   sca(:,i-j,:)=sca(:,i-j+mesh%gey,:)
                end do
             else if (i==mesh%gey) then
                do j=1,mesh%gy
                   sca(:,i+j,:)=sca(:,i+j-mesh%gey,:)
                end do
             end if
          end do
       end if
       !-------------------------------------------------------------------------------
       ! Z-direction
       !-------------------------------------------------------------------------------
       if (mesh%dim==3.and.mesh%periodic(3)) then
          do i=mesh%sz,mesh%ez
             if (i==mesh%gsz) then 
                do j=1,mesh%gz
                   sca(:,:,i-j)=sca(:,:,i-j+mesh%gez)
                end do
             else if (i==mesh%gez) then 
                do j=1,mesh%gz
                   sca(:,:,i+j)=sca(:,:,i+j-mesh%gez)
                end do
             end if
          end do
       end if
       !-------------------------------------------------------------------------------
    else
       !-------------------------------------------------------------------------------
       ! Periodicity handled by MPI
       !-------------------------------------------------------------------------------
       call comm_mpi_sca(sca)
       !-------------------------------------------------------------------------------
    end if
    return 
  end subroutine sca_periodicity

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  subroutine uvw_periodicity(mesh,u,v,w)
    use mod_Parameters, only: nproc
    use mod_struct_grid
    use mod_mpi
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable, intent(inout) :: u,v,w
    type(grid_t), intent(in)                              :: mesh
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                               :: i,j
    !-------------------------------------------------------------------------------

    if (nproc==1) then 
       !-------------------------------------------------------------------------------
       ! X-direction
       !-------------------------------------------------------------------------------
       if (mesh%periodic(1)) then
          do i=mesh%sx,mesh%ex+1
             if (i==mesh%gsx) then
                do j=1,mesh%gx
                   u(i-j,:,:)=u(i-j+mesh%gex,:,:)
                   v(i-j,:,:)=v(i-j+mesh%gex,:,:)
                   if (mesh%dim==3) w(i-j,:,:)=w(i-j+mesh%gex,:,:)
                end do
             else if (i==mesh%gex) then
                do j=1,mesh%gx
                   u(i+1+j,:,:)=u(i+1+j-mesh%gex,:,:)
                   v(i+j,:,:)=v(i+j-mesh%gex,:,:)
                   if (mesh%dim==3) w(i+j,:,:)=w(i+j-mesh%gex,:,:)
                end do
             end if
          end do
       end if
       !-------------------------------------------------------------------------------
       ! Y-direction
       !-------------------------------------------------------------------------------
       if (mesh%periodic(2)) then
          do i=mesh%sy,mesh%ey+1
             if (i==mesh%gsy) then
                do j=1,mesh%gy
                   u(:,i-j,:)=u(:,i-j+mesh%gey,:)
                   v(:,i-j,:)=v(:,i-j+mesh%gey,:)
                   if (mesh%dim==3) w(:,i-j,:)=w(:,i-j+mesh%gey,:)
                end do
             else if (i==mesh%gey) then
                do j=1,mesh%gy
                   u(:,i+j,:)=u(:,i+j-mesh%gey,:)
                   v(:,i+1+j,:)=v(:,i+1+j-mesh%gey,:)
                   if (mesh%dim==3) w(:,i+j,:)=w(:,i+j-mesh%gey,:)
                end do
             end if
          end do
       end if
       !-------------------------------------------------------------------------------
       ! Z-direction
       !-------------------------------------------------------------------------------
       if (mesh%dim==3.and.mesh%periodic(3)) then
          do i=mesh%sz,mesh%ez+1
             if (i==mesh%gsz) then
                do j=1,mesh%gz
                   u(:,:,i-j)=u(:,:,i-j+mesh%gez)
                   v(:,:,i-j)=v(:,:,i-j+mesh%gez)
                   w(:,:,i-j)=w(:,:,i-j+mesh%gez)
                end do
             else if (i==mesh%gez) then
                do j=1,mesh%gz
                   u(:,:,i+j)=u(:,:,i+j-mesh%gez)
                   v(:,:,i+j)=v(:,:,i+j-mesh%gez)
                   w(:,:,i+1+j)=w(:,:,i+1+j-mesh%gez)
                end do
             end if
          end do
       end if
       !-------------------------------------------------------------------------------
    else 
       !-------------------------------------------------------------------------------
       ! Periodicity handled by MPI
       !-------------------------------------------------------------------------------
       call comm_mpi_uvw(u,v,w)
       !-------------------------------------------------------------------------------
    end if
    return 
  end subroutine uvw_periodicity

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  subroutine sca_borders(mesh,sca)
    use mod_struct_grid
    implicit none
    !-------------------------------------------------------------------------------
    ! global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable, intent(inout) :: sca
    type(grid_t), intent(in)                              :: mesh
    !-------------------------------------------------------------------------------
    ! local variables
    !-------------------------------------------------------------------------------
    integer                                               :: i,j
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! X-direction 
    !-------------------------------------------------------------------------------
    if (.not.mesh%periodic(1)) then 
       do i=mesh%sx,mesh%ex
          if (i==mesh%gsx) then
             do j=1,mesh%gx
                sca(i-j,:,:)=sca(i+j,:,:)
             end do
          else if (i==mesh%gex) then
             do j=1,mesh%gx
                sca(i+j,:,:)=sca(i-j,:,:)
             end do
          end if
       end do
    end if
    !-------------------------------------------------------------------------------
    ! Y-direction
    !-------------------------------------------------------------------------------
    if (.not.mesh%periodic(2)) then
       do i=mesh%sy,mesh%ey
          if (i==mesh%gsy) then
             do j=1,mesh%gy
                sca(:,i-j,:)=sca(:,i+j,:)
             end do
          else if (i==mesh%gey) then
             do j=1,mesh%gy
                sca(:,i+j,:)=sca(:,i-j,:)
             end do
          end if
       end do
    end if
    !-------------------------------------------------------------------------------
    ! Z-direction
    !-------------------------------------------------------------------------------
    if (mesh%dim==3.and..not.mesh%periodic(3)) then
       do i=mesh%sz,mesh%ez
          if (i==mesh%gsz) then
             do j=1,mesh%gz
                sca(:,:,i-j)=sca(:,:,i+j)
             end do
          else if (i==mesh%gez) then
             do j=1,mesh%gz
                sca(:,:,i+j)=sca(:,:,i-j)
             end do
          end if
       end do
    end if
    !-------------------------------------------------------------------------------
    return
  end subroutine sca_borders

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  subroutine uvw_borders(mesh,u,v,w)
    use mod_struct_grid
    implicit none
    !-------------------------------------------------------------------------------
    ! global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable, intent(inout) :: u,v,w
    type(grid_t), intent(in)                              :: mesh
    !-------------------------------------------------------------------------------
    ! local variables
    !-------------------------------------------------------------------------------
    integer                                               :: i,j
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! X-direction
    !-------------------------------------------------------------------------------
    if (.not.mesh%periodic(1)) then 
       do i=mesh%sx,mesh%ex+1
          if (i==mesh%gsx) then
             do j=1,mesh%gx
                u(i-j,:,:)=u(i,:,:)
                v(i-j,:,:)=v(i,:,:)
                if (mesh%dim==3) w(i-j,:,:)=w(i,:,:)
             end do
          else if (i==mesh%gex) then
             do j=1,mesh%gx
                u(i+1+j,:,:)=u(i+1,:,:)
                v(i+j,:,:)=v(i,:,:)
                if (mesh%dim==3) w(i+j,:,:)=w(i,:,:)
             end do
          end if
       end do
    end if
    !-------------------------------------------------------------------------------
    ! Y-direction
    !-------------------------------------------------------------------------------
    if (.not.mesh%periodic(2)) then 
       do i=mesh%sy,mesh%ey+1
          if (i==mesh%gsy) then
             do j=1,mesh%gy
                u(:,i-j,:)=u(:,i,:)
                v(:,i-j,:)=v(:,i,:)
                if (mesh%dim==3) w(:,i-j,:)=w(:,i,:)
             end do
          else if (i==mesh%gey) then
             do j=1,mesh%gy
                u(:,i+j,:)=u(:,i,:)
                v(:,i+1+j,:)=v(:,i+1,:)
                if (mesh%dim==3) w(:,i+j,:)=w(:,i,:)
             end do
          end if
       end do
    end if
    !-------------------------------------------------------------------------------
    ! Z-direction
    !-------------------------------------------------------------------------------
    if (mesh%dim==3.and..not.mesh%periodic(3)) then 
       do i=mesh%sz,mesh%ez+1
          if (i==mesh%gsz) then
             do j=1,mesh%gz
                u(:,:,i-j)=u(:,:,i)
                v(:,:,i-j)=v(:,:,i)
                w(:,:,i-j)=w(:,:,i)
             end do
          else if (i==mesh%gez) then
             do j=1,mesh%gz
                u(:,:,i+j)=u(:,:,i)
                v(:,:,i+j)=v(:,:,i)
                w(:,:,i+1+j)=w(:,:,i+1)
             end do
          end if
       end do
    end if
    !-------------------------------------------------------------------------------
    return
  end subroutine uvw_borders
  
  
  !! USLESS ???? --> return ???!!!???
  !! CALLED IN ENERGY AND DIFFUSION 
  subroutine Interp_Scal_Boundary(sca,bound)
    use mod_Parameters, only: dim,Periodic,nx,ny,nz, &
         & ex,sx,ey,sy,sz,ez,                        &
         & exs,sxs,eys,sys,szs,ezs,                  &
         & gsx,gex,gsy,gey,gsz,gez
    use mod_struct_boundary
    !-------------------------------------------------------------------------------
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable :: sca
    type(boundary_t), intent(in)           :: bound
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                :: i,j,k
    !-------------------------------------------------------------------------------

    return
    !-------------------------------------------------------------------------------
    if (dim==2) then
       do j=sys,eys
          do i=sxs,exs
             !-------------------------------------------------------------------------------
             ! bottom left corner
             if (i==gsx.and.j==gsy) then
                if (Periodic(1).and.Periodic(2)) then
                   sca(i-1,j,1)=sca(i-1+gex,j,1)
                   sca(i,j-1,1)=sca(i,j-1+gey,1)
                elseif (Periodic(2).and..not.Periodic(1)) then
                   sca(i-1,j,1)=sca(i+1,j,1)
                   sca(i,j-1,1)=sca(i,j-1+gey,1)
                elseif (.not.Periodic(2).and.Periodic(1)) then
                   sca(i-1,j,1)=sca(i-1+gex,j,1)
                   sca(i,j-1,1)=sca(i,j+1,1)
                else
                   sca(i,j,1)=(sca(i+1,j,1)+sca(i,j+1,1))/2
                   sca(i-1,j,1)=sca(i+1,j,1)
                   sca(i,j-1,1)=sca(i,j+1,1)
                endif
                ! bottom right corner
             elseif (i==gex.and.j==gsy) then
                if (Periodic(2).and.Periodic(1)) then
                   sca(i+1,j,1)=sca(i+1-gex,j,1)
                   sca(i,j-1,1)=sca(i,j-1+gey,1)
                elseif (Periodic(2).and..not.Periodic(1)) then
                   sca(i+1,j,1)=sca(i-1,j,1)
                   sca(i,j-1,1)=sca(i,j-1+gey,1)
                elseif (.not.Periodic(2).and.Periodic(1)) then
                   sca(i+1,j,1)=sca(i+1-gex,j,1)
                   sca(i,j-1,1)=sca(i,j+1,1)
                else
                   sca(i,j,1)=(sca(i-1,j,1)+sca(i,j+1,1))/2
                   sca(i+1,j,1)=sca(i-1,j,1)
                   sca(i,j-1,1)=sca(i,j+1,1)
                endif
                ! top left corner
             elseif (i==gsx.and.j==gey) then
                if (Periodic(2).and.Periodic(1)) then
                   sca(i-1,j,1)=sca(i-1+gex,j,1)
                   sca(i,j+1,1)=sca(i,j+1-gey,1)
                elseif (Periodic(2).and..not.Periodic(1)) then
                   sca(i-1,j,1)=sca(i+1,j,1)
                   sca(i,j+1,1)=sca(i,j+1-gey,1)
                elseif (.not.Periodic(2).and.Periodic(1)) then
                   sca(i-1,j,1)=sca(i-1+gex,j,1)
                   sca(i,j+1,1)=sca(i,j-1,1)
                else
                   sca(i,j,1)=(sca(i+1,j,1)+sca(i,j-1,1))/2
                   sca(i-1,j,1)=sca(i+1,j,1)
                   sca(i,j+1,1)=sca(i,j-1,1)
                endif
                ! top right corner
             elseif (i==gex.and.j==gey) then
                if (Periodic(2).and.Periodic(1)) then
                   sca(i+1,j,1)=sca(i+1-gex,j,1)
                   sca(i,j+1,1)=sca(i,j+1-gey,1)
                elseif (Periodic(2).and..not.Periodic(1)) then
                   sca(i+1,j,1)=sca(i-1,j,1)
                   sca(i,j+1,1)=sca(i,j+1-gey,1)
                elseif (.not.Periodic(2).and.Periodic(1)) then
                   sca(i+1,j,1)=sca(i+1-gex,j,1)
                   sca(i,j+1,1)=sca(i,j-1,1)
                else
                   sca(i,j,1)=(sca(i-1,j,1)+sca(i,j-1,1))/2
                   sca(i+1,j,1)=sca(i-1,j,1)
                   sca(i,j+1,1)=sca(i,j-1,1)
                endif
                ! bottom boundary
             elseif (j==gsy) then
                if (Periodic(2)) then
                   sca(i,j-1,1)=sca(i,j-1+gey,1)
                else
                   sca(i,j-1,1)=sca(i,j+1,1)
                endif
                ! top boundary
             elseif (j==gey) then
                if (Periodic(2)) then
                   sca(i,j+1,1)=sca(i,j+1-gey,1)
                else
                   sca(i,j+1,1)=sca(i,j-1,1)
                endif
                ! left boundary
             elseif (i==gsx) then
                if (Periodic(1)) then
                   sca(i-1,j,1)=sca(i-1+gex,j,1)
                else
                   sca(i-1,j,1)=sca(i+1,j,1)
                endif
                ! right boundary
             elseif (i==gex) then
                if (Periodic(1)) then
                   sca(i+1,j,1)=sca(i+1-gex,j,1)
                else
                   sca(i+1,j,1)=sca(i-1,j,1)
                endif
             endif
             !-------------------------------------------------------------------------------
          end do
       end do
    else if (dim==3) then
       !-------------------------------------------------------------------------------
       do k=szs,ezs
          do j=sys,eys
             do i=sxs,exs
                !BOTTOM/LEFT/BACKWARD CORNER(1/8)
                if (i==gsx.and.j==gsy.and.k==gsz) then 
                   if (Periodic(2).and.Periodic(1).and.Periodic(3)) then
                      sca(i-1,j,k)=sca(i-1+gex,j,k)
                      sca(i,j-1,k)=sca(i,j-1+gey,k)
                      sca(i,j,k-1)=sca(i,j,k-1+gez)
                   elseif (Periodic(2).and.Periodic(1).and..not.Periodic(3)) then
                      sca(i-1,j,k)=sca(i-1+gex,j,k)
                      sca(i,j-1,k)=sca(i,j-1+gey,k)
                      sca(i,j,k-1)=sca(i,j,k+1)
                   elseif (Periodic(2).and..not.Periodic(1).and.Periodic(3)) then
                      sca(i-1,j,k)=sca(i+1,j,k)
                      sca(i,j-1,k)=sca(i,j-1+gey,k)
                      sca(i,j,k-1)=sca(i,j,k-1+gez)
                   elseif (.not.Periodic(2).and.Periodic(1).and.Periodic(3)) then
                      sca(i-1,j,k)=sca(i-1+gex,j,k)
                      sca(i,j-1,k)=sca(i,j+1,k)
                      sca(i,j,k-1)=sca(i,j,k-1+gez)
                   elseif (.not.Periodic(2).and.Periodic(1).and..not.Periodic(3)) then
                      sca(i-1,j,k)=sca(i-1+gex,j,k)
                      sca(i,j-1,k)=sca(i,j+1,k)
                      sca(i,j,k-1)=sca(i,j,k+1)
                   elseif (Periodic(2).and..not.Periodic(1).and..not.Periodic(3)) then
                      sca(i-1,j,k)=sca(i+1,j,k)
                      sca(i,j-1,k)=sca(i,j-1+gey,k)
                      sca(i,j,k-1)=sca(i,j,k+1)
                   elseif (.not.Periodic(2).and..not.Periodic(1).and.Periodic(3)) then
                      sca(i-1,j,k)=sca(i+1,j,k)
                      sca(i,j-1,k)=sca(i,j+1,k)
                      sca(i,j,k-1)=sca(i,j,k-1+gez)
                   else
                      sca(i,j,k)=(sca(i+1,j,k)+sca(i,j+1,k)+sca(i,j,k+1))/3
                      sca(i-1,j,k)=sca(i+1,j,k)
                      sca(i,j-1,k)=sca(i,j+1,k)
                      sca(i,j,k-1)=sca(i,j,k+1)
                   endif
                   !BOTTOM/RIGHT/BACKWARD CORNER(2/8)
                elseif (i==gex.and.j==gsy.and.k==gsz) then
                   if (Periodic(2).and.Periodic(1).and.Periodic(3)) then
                      sca(i+1,j,k)=sca(i+1-gex,j,k)
                      sca(i,j-1,k)=sca(i,j-1+gey,k)
                      sca(i,j,k-1)=sca(i,j,k-1+gez)
                   elseif (Periodic(2).and.Periodic(1).and..not.Periodic(3)) then
                      sca(i+1,j,k)=sca(i+1-gex,j,k)
                      sca(i,j-1,k)=sca(i,j-1+gey,k)
                      sca(i,j,k-1)=sca(i,j,k+1)
                   elseif (Periodic(2).and..not.Periodic(1).and.Periodic(3)) then
                      sca(i+1,j,k)=sca(i-1,j,k)
                      sca(i,j-1,k)=sca(i,j-1+gey,k)
                      sca(i,j,k-1)=sca(i,j,k-1+gez)
                   elseif (.not.Periodic(2).and.Periodic(1).and.Periodic(3)) then
                      sca(i+1,j,k)=sca(i+1-gex,j,k)
                      sca(i,j-1,k)=sca(i,j+1,k)
                      sca(i,j,k-1)=sca(i,j,k-1+gez)
                   elseif (.not.Periodic(2).and.Periodic(1).and..not.Periodic(3)) then
                      sca(i+1,j,k)=sca(i+1-gex,j,k)
                      sca(i,j-1,k)=sca(i,j+1,k)
                      sca(i,j,k-1)=sca(i,j,k+1)
                   elseif (Periodic(2).and..not.Periodic(1).and..not.Periodic(3)) then
                      sca(i+1,j,k)=sca(i-1,j,k)
                      sca(i,j-1,k)=sca(i,j-1+gey,k)
                      sca(i,j,k-1)=sca(i,j,k+1)
                   elseif (.not.Periodic(2).and..not.Periodic(1).and.Periodic(3)) then
                      sca(i+1,j,k)=sca(i-1,j,k)
                      sca(i,j-1,k)=sca(i,j+1,k)
                      sca(i,j,k-1)=sca(i,j,k-1+gez)
                   else
                      sca(i,j,k)=(sca(i-1,j,k)+sca(i,j+1,k)+sca(i,j,k+1))/3
                      sca(i+1,j,k)=sca(i-1,j,k)
                      sca(i,j-1,k)=sca(i,j+1,k)
                      sca(i,j,k-1)=sca(i,j,k+1)
                   endif
                   !TOP/LEFT/BACKWARD CORNER(3/8)
                elseif (i==gsx.and.j==gey.and.k==gsz) then 
                   if (Periodic(2).and.Periodic(1).and.Periodic(3)) then
                      sca(i-1,j,k)=sca(i-1+gex,j,k)
                      sca(i,j+1,k)=sca(i,j+1-gey,k)
                      sca(i,j,k-1)=sca(i,j,k-1+gez)
                   elseif (Periodic(2).and.Periodic(1).and..not.Periodic(3)) then
                      sca(i-1,j,k)=sca(i-1+gex,j,k)
                      sca(i,j+1,k)=sca(i,j+1-gey,k)
                      sca(i,j,k-1)=sca(i,j,k+1)
                   elseif (Periodic(2).and..not.Periodic(1).and.Periodic(3)) then
                      sca(i-1,j,k)=sca(i+1,j,k)
                      sca(i,j+1,k)=sca(i,j+1-gey,k)
                      sca(i,j,k-1)=sca(i,j,k-1+gez)
                   elseif (.not.Periodic(2).and.Periodic(1).and.Periodic(3)) then
                      sca(i-1,j,k)=sca(i-1+gex,j,k)
                      sca(i,j+1,k)=sca(i,j-1,k)
                      sca(i,j,k-1)=sca(i,j,k-1+gez)
                   elseif (.not.Periodic(2).and.Periodic(1).and..not.Periodic(3)) then
                      sca(i-1,j,k)=sca(i-1+gex,j,k)
                      sca(i,j+1,k)=sca(i,j-1,k)
                      sca(i,j,k-1)=sca(i,j,k+1)
                   elseif (Periodic(2).and..not.Periodic(1).and..not.Periodic(3)) then
                      sca(i-1,j,k)=sca(i+1,j,k)
                      sca(i,j+1,k)=sca(i,j+1-gey,k)
                      sca(i,j,k-1)=sca(i,j,k+1)
                   elseif (.not.Periodic(2).and..not.Periodic(1).and.Periodic(3)) then
                      sca(i-1,j,k)=sca(i+1,j,k)
                      sca(i,j+1,k)=sca(i,j-1,k)
                      sca(i,j,k-1)=sca(i,j,k-1+gez)
                   else
                      sca(i,j,k)=(sca(i+1,j,k)+sca(i,j-1,k)+sca(i,j,k+1))/3
                      sca(i-1,j,k)=sca(i+1,j,k)
                      sca(i,j+1,k)=sca(i,j-1,k)
                      sca(i,j,k-1)=sca(i,j,k+1)
                   endif
                   !TOP/RIGHT/BACKWARD CORNER(4/8)
                elseif (i==gex.and.j==gey.and.k==gsz) then 
                   if (Periodic(2).and.Periodic(1).and.Periodic(3)) then
                      sca(i+1,j,k)=sca(i+1-gex,j,k)
                      sca(i,j+1,k)=sca(i,j+1-gey,k)
                      sca(i,j,k-1)=sca(i,j,k-1+gez)
                   elseif (Periodic(2).and.Periodic(1).and..not.Periodic(3)) then
                      sca(i+1,j,k)=sca(i+1-gex,j,k)
                      sca(i,j+1,k)=sca(i,j+1-gey,k)
                      sca(i,j,k-1)=sca(i,j,k+1)
                   elseif (Periodic(2).and..not.Periodic(1).and.Periodic(3)) then
                      sca(i+1,j,k)=sca(i-1,j,k)
                      sca(i,j+1,k)=sca(i,j+1-gey,k)
                      sca(i,j,k-1)=sca(i,j,k-1+gez)
                   elseif (.not.Periodic(2).and.Periodic(1).and.Periodic(3)) then
                      sca(i+1,j,k)=sca(i+1-gex,j,k)
                      sca(i,j+1,k)=sca(i,j-1,k)
                      sca(i,j,k-1)=sca(i,j,k-1+gez)
                   elseif (.not.Periodic(2).and.Periodic(1).and..not.Periodic(3)) then
                      sca(i+1,j,k)=sca(i+1-gex,j,k)
                      sca(i,j+1,k)=sca(i,j-1,k)
                      sca(i,j,k-1)=sca(i,j,k+1)
                   elseif (Periodic(2).and..not.Periodic(1).and..not.Periodic(3)) then
                      sca(i+1,j,k)=sca(i-1,j,k)
                      sca(i,j+1,k)=sca(i,j+1-gey,k)
                      sca(i,j,k-1)=sca(i,j,k+1)
                   elseif (.not.Periodic(2).and..not.Periodic(1).and.Periodic(3)) then
                      sca(i+1,j,k)=sca(i-1,j,k)
                      sca(i,j+1,k)=sca(i,j-1,k)
                      sca(i,j,k-1)=sca(i,j,k-1+gez)
                   else
                      sca(i,j,k)=(sca(i-1,j,k)+sca(i,j-1,k)+sca(i,j,k+1))/3
                      sca(i+1,j,k)=sca(i-1,j,k)
                      sca(i,j+1,k)=sca(i,j-1,k)
                      sca(i,j,k-1)=sca(i,j,k+1)
                   endif
                   !BOTTOM/LEFT/FORWARD CORNER(5/8)
                elseif (i==gsx.and.j==gsy.and.k==gez) then 
                   if (Periodic(2).and.Periodic(1).and.Periodic(3)) then
                      sca(i-1,j,k)=sca(i-1+gex,j,k)
                      sca(i,j-1,k)=sca(i,j-1+gey,k)
                      sca(i,j,k+1)=sca(i,j,k+1-gez)
                   elseif (Periodic(2).and.Periodic(1).and..not.Periodic(3)) then
                      sca(i-1,j,k)=sca(i-1+gex,j,k)
                      sca(i,j-1,k)=sca(i,j-1+gey,k)
                      sca(i,j,k+1)=sca(i,j,k-1)
                   elseif (Periodic(2).and..not.Periodic(1).and.Periodic(3)) then
                      sca(i-1,j,k)=sca(i+1,j,k)
                      sca(i,j-1,k)=sca(i,j-1+gey,k)
                      sca(i,j,k+1)=sca(i,j,k+1-gez)
                   elseif (.not.Periodic(2).and.Periodic(1).and.Periodic(3)) then
                      sca(i-1,j,k)=sca(i-1+gex,j,k)
                      sca(i,j-1,k)=sca(i,j+1,k)
                      sca(i,j,k+1)=sca(i,j,k+1-gez)
                   elseif (.not.Periodic(2).and.Periodic(1).and..not.Periodic(3)) then
                      sca(i-1,j,k)=sca(i-1+gex,j,k)
                      sca(i,j-1,k)=sca(i,j+1,k)
                      sca(i,j,k+1)=sca(i,j,k-1)
                   elseif (Periodic(2).and..not.Periodic(1).and..not.Periodic(3)) then
                      sca(i-1,j,k)=sca(i+1,j,k)
                      sca(i,j-1,k)=sca(i,j-1+gey,k)
                      sca(i,j,k+1)=sca(i,j,k-1)
                   elseif (.not.Periodic(2).and..not.Periodic(1).and.Periodic(3)) then
                      sca(i-1,j,k)=sca(i+1,j,k)
                      sca(i,j-1,k)=sca(i,j+1,k)
                      sca(i,j,k+1)=sca(i,j,k+1-gez)
                   else
                      sca(i,j,k)=(sca(i+1,j,k)+sca(i,j+1,k)+sca(i,j,k-1))/3
                      sca(i-1,j,k)=sca(i+1,j,k)
                      sca(i,j-1,k)=sca(i,j+1,k)
                      sca(i,j,k+1)=sca(i,j,k-1)
                   endif
                   !BOTTOM/RIGHT/FORWARD CORNER(6/8)
                elseif (i==gex.and.j==gsy.and.k==gez) then 
                   if (Periodic(2).and.Periodic(1).and.Periodic(3)) then
                      sca(i+1,j,k)=sca(i+1-gex,j,k)
                      sca(i,j-1,k)=sca(i,j-1+gey,k)
                      sca(i,j,k+1)=sca(i,j,k+1-gez)
                   elseif (Periodic(2).and.Periodic(1).and..not.Periodic(3)) then
                      sca(i+1,j,k)=sca(i+1-gex,j,k)
                      sca(i,j-1,k)=sca(i,j-1+gey,k)
                      sca(i,j,k+1)=sca(i,j,k-1)
                   elseif (Periodic(2).and..not.Periodic(1).and.Periodic(3)) then
                      sca(i+1,j,k)=sca(i-1,j,k)
                      sca(i,j-1,k)=sca(i,j-1+gey,k)
                      sca(i,j,k+1)=sca(i,j,k+1-gez)
                   elseif (.not.Periodic(2).and.Periodic(1).and.Periodic(3)) then
                      sca(i+1,j,k)=sca(i+1-gex,j,k)
                      sca(i,j-1,k)=sca(i,j+1,k)
                      sca(i,j,k+1)=sca(i,j,k+1-gez)
                   elseif (.not.Periodic(2).and.Periodic(1).and..not.Periodic(3)) then
                      sca(i+1,j,k)=sca(i+1-gex,j,k)
                      sca(i,j-1,k)=sca(i,j+1,k)
                      sca(i,j,k+1)=sca(i,j,k-1)
                   elseif (Periodic(2).and..not.Periodic(1).and..not.Periodic(3)) then
                      sca(i+1,j,k)=sca(i-1,j,k)
                      sca(i,j-1,k)=sca(i,j-1+gey,k)
                      sca(i,j,k+1)=sca(i,j,k-1)
                   elseif (.not.Periodic(2).and..not.Periodic(1).and.Periodic(3)) then
                      sca(i+1,j,k)=sca(i-1,j,k)
                      sca(i,j-1,k)=sca(i,j+1,k)
                      sca(i,j,k+1)=sca(i,j,k+1-gez)
                   else
                      sca(i,j,k)=(sca(i-1,j,k)+sca(i,j+1,k)+sca(i,j,k-1))/3
                      sca(i+1,j,k)=sca(i-1,j,k)
                      sca(i,j-1,k)=sca(i,j+1,k)
                      sca(i,j,k+1)=sca(i,j,k-1)
                   endif
                   !TOP/LEFT/FORWARD CORNER(7/8)
                elseif (i==gsx.and.j==gey.and.k==gez) then 
                   if (Periodic(2).and.Periodic(1).and.Periodic(3)) then
                      sca(i-1,j,k)=sca(i-1+gex,j,k)
                      sca(i,j+1,k)=sca(i,j+1-gey,k)
                      sca(i,j,k+1)=sca(i,j,k+1-gez)
                   elseif (Periodic(2).and.Periodic(1).and..not.Periodic(3)) then
                      sca(i-1,j,k)=sca(i-1+gex,j,k)
                      sca(i,j+1,k)=sca(i,j+1-gey,k)
                      sca(i,j,k+1)=sca(i,j,k-1)
                   elseif (Periodic(2).and..not.Periodic(1).and.Periodic(3)) then
                      sca(i-1,j,k)=sca(i+1,j,k)
                      sca(i,j+1,k)=sca(i,j+1-gey,k)
                      sca(i,j,k+1)=sca(i,j,k+1-gez)
                   elseif (.not.Periodic(2).and.Periodic(1).and.Periodic(3)) then
                      sca(i-1,j,k)=sca(i-1+gex,j,k)
                      sca(i,j+1,k)=sca(i,j-1,k)
                      sca(i,j,k+1)=sca(i,j,k+1-gez)
                   elseif (.not.Periodic(2).and.Periodic(1).and..not.Periodic(3)) then
                      sca(i-1,j,k)=sca(i-1+gex,j,k)
                      sca(i,j+1,k)=sca(i,j-1,k)
                      sca(i,j,k+1)=sca(i,j,k-1)
                   elseif (Periodic(2).and..not.Periodic(1).and..not.Periodic(3)) then
                      sca(i-1,j,k)=sca(i+1,j,k)
                      sca(i,j+1,k)=sca(i,j+1-gey,k)
                      sca(i,j,k+1)=sca(i,j,k-1)
                   elseif (.not.Periodic(2).and..not.Periodic(1).and.Periodic(3)) then
                      sca(i-1,j,k)=sca(i+1,j,k)
                      sca(i,j+1,k)=sca(i,j-1,k)
                      sca(i,j,k+1)=sca(i,j,k+1-gez)
                   else
                      sca(i,j,k)=(sca(i+1,j,k)+sca(i,j-1,k)+sca(i,j,k-1))/3
                      sca(i-1,j,k)=sca(i+1,j,k)
                      sca(i,j+1,k)=sca(i,j-1,k)
                      sca(i,j,k+1)=sca(i,j,k-1)
                   endif
                   !TOP/RIGHT/FORWARD CORNER(8/8)
                elseif (i==gex.and.j==gey.and.k==gez) then
                   if (Periodic(2).and.Periodic(1).and.Periodic(3)) then
                      sca(i+1,j,k)=sca(i+1-gex,j,k)
                      sca(i,j+1,k)=sca(i,j+1-gey,k)
                      sca(i,j,k+1)=sca(i,j,k+1-gez)
                   elseif (Periodic(2).and.Periodic(1).and..not.Periodic(3)) then
                      sca(i+1,j,k)=sca(i+1-gex,j,k)
                      sca(i,j+1,k)=sca(i,j+1-gey,k)
                      sca(i,j,k+1)=sca(i,j,k-1)
                   elseif (Periodic(2).and..not.Periodic(1).and.Periodic(3)) then
                      sca(i+1,j,k)=sca(i-1,j,k)
                      sca(i,j+1,k)=sca(i,j+1-gey,k)
                      sca(i,j,k+1)=sca(i,j,k+1-gez)
                   elseif (.not.Periodic(2).and.Periodic(1).and.Periodic(3)) then
                      sca(i+1,j,k)=sca(i+1-gex,j,k)
                      sca(i,j+1,k)=sca(i,j-1,k)
                      sca(i,j,k+1)=sca(i,j,k+1-gez)
                   elseif (.not.Periodic(2).and.Periodic(1).and..not.Periodic(3)) then
                      sca(i+1,j,k)=sca(i+1-gex,j,k)
                      sca(i,j+1,k)=sca(i,j-1,k)
                      sca(i,j,k+1)=sca(i,j,k-1)
                   elseif (Periodic(2).and..not.Periodic(1).and..not.Periodic(3)) then
                      sca(i+1,j,k)=sca(i-1,j,k)
                      sca(i,j+1,k)=sca(i,j+1-gey,k)
                      sca(i,j,k+1)=sca(i,j,k-1)
                   elseif (.not.Periodic(2).and..not.Periodic(1).and.Periodic(3)) then
                      sca(i+1,j,k)=sca(i-1,j,k)
                      sca(i,j+1,k)=sca(i,j-1,k)
                      sca(i,j,k+1)=sca(i,j,k+1-gez)
                   else
                      sca(i,j,k)=(sca(i-1,j,k)+sca(i,j-1,k)+sca(i,j,k-1))/3
                      sca(i+1,j,k)=sca(i-1,j,k)
                      sca(i,j+1,k)=sca(i,j-1,k)
                      sca(i,j,k+1)=sca(i,j,k-1)
                   endif
                   !BOTTOM/BACKWARD EDGE(1/12)
                elseif (j==gsy.and.k==gsz) then 
                   sca(i,j,k)=(sca(i,j+1,k)+sca(i,j,k+1))/2
                   if (Periodic(2).and.Periodic(3)) then
                      sca(i,j-1,k)=sca(i,j-1+gey,k)
                      sca(i,j,k-1)=sca(i,j,k-1+gez)
                   elseif (Periodic(2).and..not.Periodic(3)) then
                      sca(i,j-1,k)=sca(i,j-1+gey,k)
                      sca(i,j,k-1)=sca(i,j,k+1)
                   elseif (.not.Periodic(2).and.Periodic(3)) then
                      sca(i,j-1,k)=sca(i,j+1,k)
                      sca(i,j,k-1)=sca(i,j,k-1+gez)
                   else
                      sca(i,j-1,k)=sca(i,j+1,k)
                      sca(i,j,k-1)=sca(i,j,k+1)
                   endif
                   !TOP/BACKWARD EDGE(2/12)
                elseif (j==gey.and.k==gsz) then 
                   sca(i,j,k)=(sca(i,j-1,k)+sca(i,j,k+1))/2
                   if (Periodic(2).and.Periodic(3)) then
                      sca(i,j+1,k)=sca(i,j+1-gey,k)
                      sca(i,j,k-1)=sca(i,j,k-1+gez)
                   elseif (Periodic(2).and..not.Periodic(3)) then
                      sca(i,j+1,k)=sca(i,j+1-gey,k)
                      sca(i,j,k-1)=sca(i,j,k+1)
                   elseif (.not.Periodic(2).and.Periodic(3)) then
                      sca(i,j+1,k)=sca(i,j-1,k)
                      sca(i,j,k-1)=sca(i,j,k-1+gez)
                   else
                      sca(i,j+1,k)=sca(i,j-1,k)
                      sca(i,j,k-1)=sca(i,j,k+1)
                   endif
                   !BOTTOM/FORWARD EDGE(3/12)
                elseif (j==gsy.and.k==gez) then 
                   sca(i,j,k)=(sca(i,j+1,k)+sca(i,j,k-1))/2
                   if (Periodic(2).and.Periodic(3)) then
                      sca(i,j-1,k)=sca(i,j-1+gey,k)
                      sca(i,j,k+1)=sca(i,j,k+1-gez)
                   elseif (Periodic(2).and..not.Periodic(3)) then
                      sca(i,j-1,k)=sca(i,j-1+gey,k)
                      sca(i,j,k+1)=sca(i,j,k-1)
                   elseif (.not.Periodic(2).and.Periodic(3)) then
                      sca(i,j-1,k)=sca(i,j+1,k)
                      sca(i,j,k+1)=sca(i,j,k+1-gez)
                   else
                      sca(i,j-1,k)=sca(i,j+1,k)
                      sca(i,j,k+1)=sca(i,j,k-1)
                   endif
                   !TOP/FORWARD EDGE(4/12)
                elseif (j==gey.and.k==gez) then 
                   sca(i,j,k)=(sca(i,j-1,k)+sca(i,j,k-1))/2
                   if (Periodic(2).and.Periodic(3)) then
                      sca(i,j+1,k)=sca(i,j+1-gey,k)
                      sca(i,j,k+1)=sca(i,j,k+1-gez)
                   elseif (Periodic(2).and..not.Periodic(3)) then
                      sca(i,j+1,k)=sca(i,j+1-gey,k)
                      sca(i,j,k+1)=sca(i,j,k-1)
                   elseif (.not.Periodic(2).and.Periodic(3)) then
                      sca(i,j+1,k)=sca(i,j-1,k)
                      sca(i,j,k+1)=sca(i,j,k+1-gez)
                   else
                      sca(i,j+1,k)=sca(i,j-1,k)
                      sca(i,j,k+1)=sca(i,j,k-1)
                   endif
                   ! LEFT/BACKWARD EDGE(5/12)
                elseif (i==gsx.and.k==gsz) then 
                   sca(i,j,k)=(sca(i+1,j,k)+sca(i,j,k+1))/2
                   if (Periodic(1).and.Periodic(3)) then
                      sca(i-1,j,k)=sca(i-1+gex,j,k)
                      sca(i,j,k-1)=sca(i,j,k-1+gez)
                   elseif (Periodic(1).and..not.Periodic(3)) then
                      sca(i-1,j,k)=sca(i-1+gex,j,k)
                      sca(i,j,k-1)=sca(i,j,k+1)
                   elseif (.not.Periodic(1).and.Periodic(3)) then
                      sca(i-1,j,k)=sca(i+1,j,k)
                      sca(i,j,k-1)=sca(i,j,k-1+gez)
                   else
                      sca(i-1,j,k)=sca(i+1,j,k)
                      sca(i,j,k-1)=sca(i,j,k+1)
                   endif
                   !RIGHT/BACKWARD EDGE(6/12)
                elseif (i==gex.and.k==gsz) then 
                   sca(i,j,k)=(sca(i-1,j,k)+sca(i,j,k+1))/2
                   if (Periodic(1).and.Periodic(3)) then
                      sca(i+1,j,k)=sca(i+1-gex,j,k)
                      sca(i,j,k-1)=sca(i,j,k-1+gez)
                   elseif (Periodic(1).and..not.Periodic(3)) then
                      sca(i+1,j,k)=sca(i+1-gex,j,k)
                      sca(i,j,k-1)=sca(i,j,k+1)
                   elseif (.not.Periodic(1).and.Periodic(3)) then
                      sca(i+1,j,k)=sca(i-1,j,k)
                      sca(i,j,k-1)=sca(i,j,k-1+gez)
                   else
                      sca(i+1,j,k)=sca(i-1,j,k)
                      sca(i,j,k-1)=sca(i,j,k+1)
                   endif
                   ! LEFT/FORWARD EDGE(7/12)
                elseif (i==gsx.and.k==gez) then
                   sca(i,j,k)=(sca(i+1,j,k)+sca(i,j,k-1))/2
                   if (Periodic(1).and.Periodic(3)) then
                      sca(i-1,j,k)=sca(i-1+gex,j,k)
                      sca(i,j,k+1)=sca(i,j,k+1-gez)
                   elseif (Periodic(1).and..not.Periodic(3)) then
                      sca(i-1,j,k)=sca(i-1+gex,j,k)
                      sca(i,j,k+1)=sca(i,j,k-1)
                   elseif (.not.Periodic(1).and.Periodic(3)) then
                      sca(i-1,j,k)=sca(i+1,j,k)
                      sca(i,j,k+1)=sca(i,j,k+1-gez)
                   else
                      sca(i-1,j,k)=sca(i+1,j,k)
                      sca(i,j,k+1)=sca(i,j,k-1)
                   endif
                   !RIGHT/FORWARD EDGE(8/12)
                elseif (i==gex.and.k==gez) then 
                   sca(i,j,k)=(sca(i-1,j,k)+sca(i,j,k-1))/2
                   if (Periodic(1).and.Periodic(3)) then
                      sca(i+1,j,k)=sca(i+1-gex,j,k)
                      sca(i,j,k+1)=sca(i,j,k+1-gez)
                   elseif (Periodic(1).and..not.Periodic(3)) then
                      sca(i+1,j,k)=sca(i+1-gex,j,k)
                      sca(i,j,k+1)=sca(i,j,k-1)
                   elseif (.not.Periodic(1).and.Periodic(3)) then
                      sca(i+1,j,k)=sca(i-1,j,k)
                      sca(i,j,k+1)=sca(i,j,k+1-gez)
                   else
                      sca(i+1,j,k)=sca(i-1,j,k)
                      sca(i,j,k+1)=sca(i,j,k-1)
                   endif
                   ! LEFT/BOTTOM EDGE(9/12)
                elseif (i==gsx.and.j==gsy) then 
                   sca(i,j,k)=(sca(i+1,j,k)+sca(i,j+1,k))/2
                   if (Periodic(1).and.Periodic(2)) then
                      sca(i-1,j,k)=sca(i-1+gex,j,k)
                      sca(i,j-1,k)=sca(i,j-1+gey,k)
                   elseif (Periodic(1).and..not.Periodic(2)) then
                      sca(i-1,j,k)=sca(i-1+gex,j,k)
                      sca(i,j-1,k)=sca(i,j+1,k)
                   elseif (.not.Periodic(1).and.Periodic(2)) then
                      sca(i-1,j,k)=sca(i+1,j,k)
                      sca(i,j-1,k)=sca(i,j-1+gey,k)
                   else
                      sca(i-1,j,k)=sca(i+1,j,k)
                      sca(i,j-1,k)=sca(i,j+1,k)
                   endif
                   !RIGHT/BOTTOM EDGE(10/12)
                elseif (i==gex.and.j==gsy) then 
                   sca(i,j,k)=(sca(i-1,j,k)+sca(i,j+1,k))/2
                   if (Periodic(1).and.Periodic(2)) then
                      sca(i+1,j,k)=sca(i+1-gex,j,k)
                      sca(i,j-1,k)=sca(i,j-1+gey,k)
                   elseif (Periodic(1).and..not.Periodic(2)) then
                      sca(i+1,j,k)=sca(i+1-gex,j,k)
                      sca(i,j-1,k)=sca(i,j+1,k)
                   elseif (.not.Periodic(1).and.Periodic(2)) then
                      sca(i+1,j,k)=sca(i-1,j,k)
                      sca(i,j-1,k)=sca(i,j-1+gey,k)
                   else
                      sca(i+1,j,k)=sca(i-1,j,k)
                      sca(i,j-1,k)=sca(i,j+1,k)
                   endif
                   ! LEFT/TOP EDGE(11/12)
                elseif (i==gsx.and.j==gey) then 
                   sca(i,j,k)=(sca(i+1,j,k)+sca(i,j-1,k))/2
                   if (Periodic(1).and.Periodic(2)) then
                      sca(i-1,j,k)=sca(i-1+gex,j,k)
                      sca(i,j+1,k)=sca(i,j+1-gey,k)
                   elseif (Periodic(1).and..not.Periodic(2)) then
                      sca(i-1,j,k)=sca(i-1+gex,j,k)
                      sca(i,j+1,k)=sca(i,j-1,k)
                   elseif (.not.Periodic(1).and.Periodic(2)) then
                      sca(i-1,j,k)=sca(i+1,j,k)
                      sca(i,j+1,k)=sca(i,j+1-gey,k)
                   else
                      sca(i-1,j,k)=sca(i+1,j,k)
                      sca(i,j+1,k)=sca(i,j-1,k)
                   endif
                   !RIGHT/TOP EDGE(12/12)
                elseif (i==gex.and.j==gey) then 
                   sca(i,j,k)=(sca(i-1,j,k)+sca(i,j-1,k))/2
                   if (Periodic(1).and.Periodic(2)) then
                      sca(i+1,j,k)=sca(i+1-gex,j,k)
                      sca(i,j+1,k)=sca(i,j+1-gey,k)
                   elseif (Periodic(1).and..not.Periodic(2)) then
                      sca(i+1,j,k)=sca(i+1-gex,j,k)
                      sca(i,j+1,k)=sca(i,j-1,k)
                   elseif (.not.Periodic(1).and.Periodic(2)) then
                      sca(i+1,j,k)=sca(i-1,j,k)
                      sca(i,j+1,k)=sca(i,j+1-gey,k)
                   else
                      sca(i+1,j,k)=sca(i-1,j,k)
                      sca(i,j+1,k)=sca(i,j-1,k)
                   endif
                   !BACKWARD FACE(1/6)
                elseif (k==gsz) then 
                   if (Periodic(3)) then
                      sca(i,j,k-1)=sca(i,j,k-1+gez)
                   else
                      sca(i,j,k-1)=sca(i,j,k+1)
                   endif
                   !FORWARD FACE(2/6)
                elseif (k==gez) then 
                   if (Periodic(3)) then
                      sca(i,j,k+1)=sca(i,j,k+1-gez)
                   else
                      sca(i,j,k+1)=sca(i,j,k-1)
                   endif
                   ! LEFT FACE(3/6)
                elseif (i==gsx) then 
                   if (Periodic(1)) then
                      sca(i-1,j,k)=sca(i-1+gex,j,k)
                   else
                      sca(i-1,j,k)=sca(i+1,j,k)
                   endif
                   !RIGHT FACE(4/6)
                elseif (i==gex) then 
                   if (Periodic(1)) then
                      sca(i+1,j,k)=sca(i+1-gex,j,k)
                   else
                      sca(i+1,j,k)=sca(i-1,j,k)
                   endif
                   !BOTTOM FACE(5/6)
                elseif (j==gsy) then 
                   if (Periodic(2)) then
                      sca(i,j-1,k)=sca(i,j-1+gey,k)
                   else
                      sca(i,j-1,k)=sca(i,j+1,k)
                   endif
                   !TOP FACE(6/6)
                elseif (j==gey) then
                   if (Periodic(2)) then
                      sca(i,j+1,k)=sca(i,j+1-gey,k)
                   else
                      sca(i,j+1,k)=sca(i,j-1,k)
                   endif
                end if
             end do
          end do
       end do

    endif
    
  end subroutine Interp_Scal_Boundary

  !*****************************************************************************************************
  subroutine NS_scal_boundary(sca,ns)
    !*****************************************************************************************************
    use mod_Parameters, only: nproc,        &
         & Periodic,dim,                    &
         & ex,sx,ey,sy,sz,ez,               &
         & gsx,gex,gsy,gey,gsz,gez,         &
         & sxu,exu,syu,eyu,szu,ezu,         &
         & sxv,exv,syv,eyv,szv,ezv,         &
         & sxw,exw,syw,eyw,szw,ezw,         &
         & gsxu,gexu,gsyu,geyu,gszu,gezu,   &
         & gsxv,gexv,gsyv,geyv,gszv,gezv,   &
         & gsxw,gexw,gsyw,geyw,gszw,gezw
    use mod_struct_solver
    !-------------------------------------------------------------------------------
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8),dimension(:,:,:), allocatable :: sca
    type(solver_ns_t), intent(in)         :: ns
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                               :: i,j,k
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    if (nproc==1) then
       if (dim==2) then
          do j = sy,ey
             do i = sx,ex
                !-------------------------------------------------------------------------------
                ! bottom left corner
                if (i==gsx.and.j==gsy) then
                   sca(i,j,1) = (sca(i+1,j,1)+sca(i,j+1,1))/2
                   if (Periodic(1).and.Periodic(2)) then
                      sca(i-1,j,1) = sca(i-1+gex,j,1)
                      sca(i,j-1,1) = sca(i,j-1+gey,1)
                   elseif (Periodic(2).and..not.Periodic(1)) then
                      sca(i-1,j,1) = sca(i+1,j,1)
                      sca(i,j-1,1) = sca(i,j-1+gey,1)
                   elseif (.not.Periodic(2).and.Periodic(1)) then
                      sca(i-1,j,1) = sca(i-1+gex,j,1)
                      sca(i,j-1,1) = sca(i,j+1,1)
                   else
                      sca(i-1,j,1) = sca(i+1,j,1)
                      sca(i,j-1,1) = sca(i,j+1,1)
                   end if
                   ! bottom right corner
                elseif (i==gex.and.j==gsy) then
                   sca(i,j,1) = (sca(i-1,j,1)+sca(i,j+1,1))/2
                   if (Periodic(2).and.Periodic(1)) then
                      sca(i+1,j,1) = sca(i+1-gex,j,1)
                      sca(i,j-1,1) = sca(i,j-1+gey,1)
                   elseif (Periodic(2).and..not.Periodic(1)) then
                      sca(i+1,j,1) = sca(i-1,j,1)
                      sca(i,j-1,1) = sca(i,j-1+gey,1)
                   elseif (.not.Periodic(2).and.Periodic(1)) then
                      sca(i+1,j,1) = sca(i+1-gex,j,1)
                      sca(i,j-1,1) = sca(i,j+1,1)
                   else
                      sca(i+1,j,1) = sca(i-1,j,1)
                      sca(i,j-1,1) = sca(i,j+1,1)
                   end if
                   ! top left corner
                elseif (i==gsx.and.j==gey) then
                   sca(i,j,1) = (sca(i+1,j,1)+sca(i,j-1,1))/2
                   if (Periodic(2).and.Periodic(1)) then
                      sca(i-1,j,1) = sca(i-1+gex,j,1)
                      sca(i,j+1,1) = sca(i,j+1-gey,1)
                   elseif (Periodic(2).and..not.Periodic(1)) then
                      sca(i-1,j,1) = sca(i+1,j,1)
                      sca(i,j+1,1) = sca(i,j+1-gey,1)
                   elseif (.not.Periodic(2).and.Periodic(1)) then
                      sca(i-1,j,1) = sca(i-1+gex,j,1)
                      sca(i,j+1,1) = sca(i,j-1,1)
                   else
                      sca(i-1,j,1) = sca(i+1,j,1)
                      sca(i,j+1,1) = sca(i,j-1,1)
                   end if
                   ! top right corner
                elseif (i==gex.and.j==gey) then
                   sca(i,j,1) = (sca(i-1,j,1)+sca(i,j-1,1))/2
                   if (Periodic(2).and.Periodic(1)) then
                      sca(i+1,j,1) = sca(i+1-gex,j,1)
                      sca(i,j+1,1) = sca(i,j+1-gey,1)
                   elseif (Periodic(2).and..not.Periodic(1)) then
                      sca(i+1,j,1) = sca(i-1,j,1)
                      sca(i,j+1,1) = sca(i,j+1-gey,1)
                   elseif (.not.Periodic(2).and.Periodic(1)) then
                      sca(i+1,j,1) = sca(i+1-gex,j,1)
                      sca(i,j+1,1) = sca(i,j-1,1)
                   else
                      sca(i+1,j,1) = sca(i-1,j,1)
                      sca(i,j+1,1) = sca(i,j-1,1)
                   end if
                   ! bottom boundary
                elseif (j==gsy) then
                   if (Periodic(2)) then
                      sca(i,j-1,1) = sca(i,j-1+gey,1)
                   else
                      sca(i,j-1,1) = sca(i,j+1,1)
                   end if
                   ! top boundary
                elseif (j==gey) then
                   if (Periodic(2)) then
                      sca(i,j+1,1) = sca(i,j+1-gey,1)
                   else
                      sca(i,j+1,1) = sca(i,j-1,1)
                   end if
                   ! left boundary
                elseif (i==gsx) then
                   if (Periodic(1)) then
                      sca(i-1,j,1) = sca(i-1+gex,j,1)
                   else
                      sca(i-1,j,1) = sca(i+1,j,1)
                   end if
                   ! right boundary
                elseif (i==gex) then
                   if (Periodic(1)) then
                      sca(i+1,j,1) = sca(i+1-gex,j,1)
                   else
                      sca(i+1,j,1) = sca(i-1,j,1)
                   end if
                end if
                !-------------------------------------------------------------------------------
             end do
          end do
       else if (dim==3) then
          !-------------------------------------------------------------------------------
          do k = sz,ez
             do j = sy,ey
                do i = sx,ex
                   !BOTTOM/LEFT/BACKWARD CORNER(1/8)
                   if (i==gsx.and.j==gsy.and.k==gsz) then 
                      sca(i,j,k) = (sca(i+1,j,k)+sca(i,j+1,k)+sca(i,j,k+1))/3
                      if (Periodic(2).and.Periodic(1).and.Periodic(3)) then
                         sca(i-1,j,k) = sca(i-1+gex,j,k)
                         sca(i,j-1,k) = sca(i,j-1+gey,k)
                         sca(i,j,k-1) = sca(i,j,k-1+gez)
                      elseif (Periodic(2).and.Periodic(1).and..not.Periodic(3)) then
                         sca(i-1,j,k) = sca(i-1+gex,j,k)
                         sca(i,j-1,k) = sca(i,j-1+gey,k)
                         sca(i,j,k-1) = sca(i,j,k+1)
                      elseif (Periodic(2).and..not.Periodic(1).and.Periodic(3)) then
                         sca(i-1,j,k) = sca(i+1,j,k)
                         sca(i,j-1,k) = sca(i,j-1+gey,k)
                         sca(i,j,k-1) = sca(i,j,k-1+gez)
                      elseif (.not.Periodic(2).and.Periodic(1).and.Periodic(3)) then
                         sca(i-1,j,k) = sca(i-1+gex,j,k)
                         sca(i,j-1,k) = sca(i,j+1,k)
                         sca(i,j,k-1) = sca(i,j,k-1+gez)
                      elseif (.not.Periodic(2).and.Periodic(1).and..not.Periodic(3)) then
                         sca(i-1,j,k) = sca(i-1+gex,j,k)
                         sca(i,j-1,k) = sca(i,j+1,k)
                         sca(i,j,k-1) = sca(i,j,k+1)
                      elseif (Periodic(2).and..not.Periodic(1).and..not.Periodic(3)) then
                         sca(i-1,j,k) = sca(i+1,j,k)
                         sca(i,j-1,k) = sca(i,j-1+gey,k)
                         sca(i,j,k-1) = sca(i,j,k+1)
                      elseif (.not.Periodic(2).and..not.Periodic(1).and.Periodic(3)) then
                         sca(i-1,j,k) = sca(i+1,j,k)
                         sca(i,j-1,k) = sca(i,j+1,k)
                         sca(i,j,k-1) = sca(i,j,k-1+gez)
                      else
                         sca(i-1,j,k) = sca(i+1,j,k)
                         sca(i,j-1,k) = sca(i,j+1,k)
                         sca(i,j,k-1) = sca(i,j,k+1)
                      end if
                      !BOTTOM/RIGHT/BACKWARD CORNER(2/8)
                   elseif (i==gex.and.j==gsy.and.k==gsz) then
                      sca(i,j,k) = (sca(i-1,j,k)+sca(i,j+1,k)+sca(i,j,k+1))/3
                      if (Periodic(2).and.Periodic(1).and.Periodic(3)) then
                         sca(i+1,j,k) = sca(i+1-gex,j,k)
                         sca(i,j-1,k) = sca(i,j-1+gey,k)
                         sca(i,j,k-1) = sca(i,j,k-1+gez)
                      elseif (Periodic(2).and.Periodic(1).and..not.Periodic(3)) then
                         sca(i+1,j,k) = sca(i+1-gex,j,k)
                         sca(i,j-1,k) = sca(i,j-1+gey,k)
                         sca(i,j,k-1) = sca(i,j,k+1)
                      elseif (Periodic(2).and..not.Periodic(1).and.Periodic(3)) then
                         sca(i+1,j,k) = sca(i-1,j,k)
                         sca(i,j-1,k) = sca(i,j-1+gey,k)
                         sca(i,j,k-1) = sca(i,j,k-1+gez)
                      elseif (.not.Periodic(2).and.Periodic(1).and.Periodic(3)) then
                         sca(i+1,j,k) = sca(i+1-gex,j,k)
                         sca(i,j-1,k) = sca(i,j+1,k)
                         sca(i,j,k-1) = sca(i,j,k-1+gez)
                      elseif (.not.Periodic(2).and.Periodic(1).and..not.Periodic(3)) then
                         sca(i+1,j,k) = sca(i+1-gex,j,k)
                         sca(i,j-1,k) = sca(i,j+1,k)
                         sca(i,j,k-1) = sca(i,j,k+1)
                      elseif (Periodic(2).and..not.Periodic(1).and..not.Periodic(3)) then
                         sca(i+1,j,k) = sca(i-1,j,k)
                         sca(i,j-1,k) = sca(i,j-1+gey,k)
                         sca(i,j,k-1) = sca(i,j,k+1)
                      elseif (.not.Periodic(2).and..not.Periodic(1).and.Periodic(3)) then
                         sca(i+1,j,k) = sca(i-1,j,k)
                         sca(i,j-1,k) = sca(i,j+1,k)
                         sca(i,j,k-1) = sca(i,j,k-1+gez)
                      else
                         sca(i+1,j,k) = sca(i-1,j,k)
                         sca(i,j-1,k) = sca(i,j+1,k)
                         sca(i,j,k-1) = sca(i,j,k+1)
                      end if
                      !TOP/LEFT/BACKWARD CORNER(3/8)
                   elseif (i==gsx.and.j==gey.and.k==gsz) then 
                      sca(i,j,k) = (sca(i+1,j,k)+sca(i,j-1,k)+sca(i,j,k+1))/3
                      if (Periodic(2).and.Periodic(1).and.Periodic(3)) then
                         sca(i-1,j,k) = sca(i-1+gex,j,k)
                         sca(i,j+1,k) = sca(i,j+1-gey,k)
                         sca(i,j,k-1) = sca(i,j,k-1+gez)
                      elseif (Periodic(2).and.Periodic(1).and..not.Periodic(3)) then
                         sca(i-1,j,k) = sca(i-1+gex,j,k)
                         sca(i,j+1,k) = sca(i,j+1-gey,k)
                         sca(i,j,k-1) = sca(i,j,k+1)
                      elseif (Periodic(2).and..not.Periodic(1).and.Periodic(3)) then
                         sca(i-1,j,k) = sca(i+1,j,k)
                         sca(i,j+1,k) = sca(i,j+1-gey,k)
                         sca(i,j,k-1) = sca(i,j,k-1+gez)
                      elseif (.not.Periodic(2).and.Periodic(1).and.Periodic(3)) then
                         sca(i-1,j,k) = sca(i-1+gex,j,k)
                         sca(i,j+1,k) = sca(i,j-1,k)
                         sca(i,j,k-1) = sca(i,j,k-1+gez)
                      elseif (.not.Periodic(2).and.Periodic(1).and..not.Periodic(3)) then
                         sca(i-1,j,k) = sca(i-1+gex,j,k)
                         sca(i,j+1,k) = sca(i,j-1,k)
                         sca(i,j,k-1) = sca(i,j,k+1)
                      elseif (Periodic(2).and..not.Periodic(1).and..not.Periodic(3)) then
                         sca(i-1,j,k) = sca(i+1,j,k)
                         sca(i,j+1,k) = sca(i,j+1-gey,k)
                         sca(i,j,k-1) = sca(i,j,k+1)
                      elseif (.not.Periodic(2).and..not.Periodic(1).and.Periodic(3)) then
                         sca(i-1,j,k) = sca(i+1,j,k)
                         sca(i,j+1,k) = sca(i,j-1,k)
                         sca(i,j,k-1) = sca(i,j,k-1+gez)
                      else
                         sca(i-1,j,k) = sca(i+1,j,k)
                         sca(i,j+1,k) = sca(i,j-1,k)
                         sca(i,j,k-1) = sca(i,j,k+1)
                      end if
                      !TOP/RIGHT/BACKWARD CORNER(4/8)
                   elseif (i==gex.and.j==gey.and.k==gsz) then 
                      sca(i,j,k) = (sca(i-1,j,k)+sca(i,j-1,k)+sca(i,j,k+1))/3
                      if (Periodic(2).and.Periodic(1).and.Periodic(3)) then
                         sca(i+1,j,k) = sca(i+1-gex,j,k)
                         sca(i,j+1,k) = sca(i,j+1-gey,k)
                         sca(i,j,k-1) = sca(i,j,k-1+gez)
                      elseif (Periodic(2).and.Periodic(1).and..not.Periodic(3)) then
                         sca(i+1,j,k) = sca(i+1-gex,j,k)
                         sca(i,j+1,k) = sca(i,j+1-gey,k)
                         sca(i,j,k-1) = sca(i,j,k+1)
                      elseif (Periodic(2).and..not.Periodic(1).and.Periodic(3)) then
                         sca(i+1,j,k) = sca(i-1,j,k)
                         sca(i,j+1,k) = sca(i,j+1-gey,k)
                         sca(i,j,k-1) = sca(i,j,k-1+gez)
                      elseif (.not.Periodic(2).and.Periodic(1).and.Periodic(3)) then
                         sca(i+1,j,k) = sca(i+1-gex,j,k)
                         sca(i,j+1,k) = sca(i,j-1,k)
                         sca(i,j,k-1) = sca(i,j,k-1+gez)
                      elseif (.not.Periodic(2).and.Periodic(1).and..not.Periodic(3)) then
                         sca(i+1,j,k) = sca(i+1-gex,j,k)
                         sca(i,j+1,k) = sca(i,j-1,k)
                         sca(i,j,k-1) = sca(i,j,k+1)
                      elseif (Periodic(2).and..not.Periodic(1).and..not.Periodic(3)) then
                         sca(i+1,j,k) = sca(i-1,j,k)
                         sca(i,j+1,k) = sca(i,j+1-gey,k)
                         sca(i,j,k-1) = sca(i,j,k+1)
                      elseif (.not.Periodic(2).and..not.Periodic(1).and.Periodic(3)) then
                         sca(i+1,j,k) = sca(i-1,j,k)
                         sca(i,j+1,k) = sca(i,j-1,k)
                         sca(i,j,k-1) = sca(i,j,k-1+gez)
                      else
                         sca(i+1,j,k) = sca(i-1,j,k)
                         sca(i,j+1,k) = sca(i,j-1,k)
                         sca(i,j,k-1) = sca(i,j,k+1)
                      end if
                      !BOTTOM/LEFT/FORWARD CORNER(5/8)
                   elseif (i==gsx.and.j==gsy.and.k==gez) then 
                      sca(i,j,k) = (sca(i+1,j,k)+sca(i,j+1,k)+sca(i,j,k-1))/3
                      if (Periodic(2).and.Periodic(1).and.Periodic(3)) then
                         sca(i-1,j,k) = sca(i-1+gex,j,k)
                         sca(i,j-1,k) = sca(i,j-1+gey,k)
                         sca(i,j,k+1) = sca(i,j,k+1-gez)
                      elseif (Periodic(2).and.Periodic(1).and..not.Periodic(3)) then
                         sca(i-1,j,k) = sca(i-1+gex,j,k)
                         sca(i,j-1,k) = sca(i,j-1+gey,k)
                         sca(i,j,k+1) = sca(i,j,k-1)
                      elseif (Periodic(2).and..not.Periodic(1).and.Periodic(3)) then
                         sca(i-1,j,k) = sca(i+1,j,k)
                         sca(i,j-1,k) = sca(i,j-1+gey,k)
                         sca(i,j,k+1) = sca(i,j,k+1-gez)
                      elseif (.not.Periodic(2).and.Periodic(1).and.Periodic(3)) then
                         sca(i-1,j,k) = sca(i-1+gex,j,k)
                         sca(i,j-1,k) = sca(i,j+1,k)
                         sca(i,j,k+1) = sca(i,j,k+1-gez)
                      elseif (.not.Periodic(2).and.Periodic(1).and..not.Periodic(3)) then
                         sca(i-1,j,k) = sca(i-1+gex,j,k)
                         sca(i,j-1,k) = sca(i,j+1,k)
                         sca(i,j,k+1) = sca(i,j,k-1)
                      elseif (Periodic(2).and..not.Periodic(1).and..not.Periodic(3)) then
                         sca(i-1,j,k) = sca(i+1,j,k)
                         sca(i,j-1,k) = sca(i,j-1+gey,k)
                         sca(i,j,k+1) = sca(i,j,k-1)
                      elseif (.not.Periodic(2).and..not.Periodic(1).and.Periodic(3)) then
                         sca(i-1,j,k) = sca(i+1,j,k)
                         sca(i,j-1,k) = sca(i,j+1,k)
                         sca(i,j,k+1) = sca(i,j,k+1-gez)
                      else
                         sca(i-1,j,k) = sca(i+1,j,k)
                         sca(i,j-1,k) = sca(i,j+1,k)
                         sca(i,j,k+1) = sca(i,j,k-1)
                      end if
                      !BOTTOM/RIGHT/FORWARD CORNER(6/8)
                   elseif (i==gex.and.j==gsy.and.k==gez) then 
                      sca(i,j,k) = (sca(i-1,j,k)+sca(i,j+1,k)+sca(i,j,k-1))/3
                      if (Periodic(2).and.Periodic(1).and.Periodic(3)) then
                         sca(i+1,j,k) = sca(i+1-gex,j,k)
                         sca(i,j-1,k) = sca(i,j-1+gey,k)
                         sca(i,j,k+1) = sca(i,j,k+1-gez)
                      elseif (Periodic(2).and.Periodic(1).and..not.Periodic(3)) then
                         sca(i+1,j,k) = sca(i+1-gex,j,k)
                         sca(i,j-1,k) = sca(i,j-1+gey,k)
                         sca(i,j,k+1) = sca(i,j,k-1)
                      elseif (Periodic(2).and..not.Periodic(1).and.Periodic(3)) then
                         sca(i+1,j,k) = sca(i-1,j,k)
                         sca(i,j-1,k) = sca(i,j-1+gey,k)
                         sca(i,j,k+1) = sca(i,j,k+1-gez)
                      elseif (.not.Periodic(2).and.Periodic(1).and.Periodic(3)) then
                         sca(i+1,j,k) = sca(i+1-gex,j,k)
                         sca(i,j-1,k) = sca(i,j+1,k)
                         sca(i,j,k+1) = sca(i,j,k+1-gez)
                      elseif (.not.Periodic(2).and.Periodic(1).and..not.Periodic(3)) then
                         sca(i+1,j,k) = sca(i+1-gex,j,k)
                         sca(i,j-1,k) = sca(i,j+1,k)
                         sca(i,j,k+1) = sca(i,j,k-1)
                      elseif (Periodic(2).and..not.Periodic(1).and..not.Periodic(3)) then
                         sca(i+1,j,k) = sca(i-1,j,k)
                         sca(i,j-1,k) = sca(i,j-1+gey,k)
                         sca(i,j,k+1) = sca(i,j,k-1)
                      elseif (.not.Periodic(2).and..not.Periodic(1).and.Periodic(3)) then
                         sca(i+1,j,k) = sca(i-1,j,k)
                         sca(i,j-1,k) = sca(i,j+1,k)
                         sca(i,j,k+1) = sca(i,j,k+1-gez)
                      else
                         sca(i+1,j,k) = sca(i-1,j,k)
                         sca(i,j-1,k) = sca(i,j+1,k)
                         sca(i,j,k+1) = sca(i,j,k-1)
                      end if
                      !TOP/LEFT/FORWARD CORNER(7/8)
                   elseif (i==gsx.and.j==gey.and.k==gez) then 
                      sca(i,j,k) = (sca(i+1,j,k)+sca(i,j-1,k)+sca(i,j,k-1))/3
                      if (Periodic(2).and.Periodic(1).and.Periodic(3)) then
                         sca(i-1,j,k) = sca(i-1+gex,j,k)
                         sca(i,j+1,k) = sca(i,j+1-gey,k)
                         sca(i,j,k+1) = sca(i,j,k+1-gez)
                      elseif (Periodic(2).and.Periodic(1).and..not.Periodic(3)) then
                         sca(i-1,j,k) = sca(i-1+gex,j,k)
                         sca(i,j+1,k) = sca(i,j+1-gey,k)
                         sca(i,j,k+1) = sca(i,j,k-1)
                      elseif (Periodic(2).and..not.Periodic(1).and.Periodic(3)) then
                         sca(i-1,j,k) = sca(i+1,j,k)
                         sca(i,j+1,k) = sca(i,j+1-gey,k)
                         sca(i,j,k+1) = sca(i,j,k+1-gez)
                      elseif (.not.Periodic(2).and.Periodic(1).and.Periodic(3)) then
                         sca(i-1,j,k) = sca(i-1+gex,j,k)
                         sca(i,j+1,k) = sca(i,j-1,k)
                         sca(i,j,k+1) = sca(i,j,k+1-gez)
                      elseif (.not.Periodic(2).and.Periodic(1).and..not.Periodic(3)) then
                         sca(i-1,j,k) = sca(i-1+gex,j,k)
                         sca(i,j+1,k) = sca(i,j-1,k)
                         sca(i,j,k+1) = sca(i,j,k-1)
                      elseif (Periodic(2).and..not.Periodic(1).and..not.Periodic(3)) then
                         sca(i-1,j,k) = sca(i+1,j,k)
                         sca(i,j+1,k) = sca(i,j+1-gey,k)
                         sca(i,j,k+1) = sca(i,j,k-1)
                      elseif (.not.Periodic(2).and..not.Periodic(1).and.Periodic(3)) then
                         sca(i-1,j,k) = sca(i+1,j,k)
                         sca(i,j+1,k) = sca(i,j-1,k)
                         sca(i,j,k+1) = sca(i,j,k+1-gez)
                      else
                         sca(i-1,j,k) = sca(i+1,j,k)
                         sca(i,j+1,k) = sca(i,j-1,k)
                         sca(i,j,k+1) = sca(i,j,k-1)
                      end if
                      !TOP/RIGHT/FORWARD CORNER(8/8)
                   elseif (i==gex.and.j==gey.and.k==gez) then
                      sca(i,j,k) = (sca(i-1,j,k)+sca(i,j-1,k)+sca(i,j,k-1))/3
                      if (Periodic(2).and.Periodic(1).and.Periodic(3)) then
                         sca(i+1,j,k) = sca(i+1-gex,j,k)
                         sca(i,j+1,k) = sca(i,j+1-gey,k)
                         sca(i,j,k+1) = sca(i,j,k+1-gez)
                      elseif (Periodic(2).and.Periodic(1).and..not.Periodic(3)) then
                         sca(i+1,j,k) = sca(i+1-gex,j,k)
                         sca(i,j+1,k) = sca(i,j+1-gey,k)
                         sca(i,j,k+1) = sca(i,j,k-1)
                      elseif (Periodic(2).and..not.Periodic(1).and.Periodic(3)) then
                         sca(i+1,j,k) = sca(i-1,j,k)
                         sca(i,j+1,k) = sca(i,j+1-gey,k)
                         sca(i,j,k+1) = sca(i,j,k+1-gez)
                      elseif (.not.Periodic(2).and.Periodic(1).and.Periodic(3)) then
                         sca(i+1,j,k) = sca(i+1-gex,j,k)
                         sca(i,j+1,k) = sca(i,j-1,k)
                         sca(i,j,k+1) = sca(i,j,k+1-gez)
                      elseif (.not.Periodic(2).and.Periodic(1).and..not.Periodic(3)) then
                         sca(i+1,j,k) = sca(i+1-gex,j,k)
                         sca(i,j+1,k) = sca(i,j-1,k)
                         sca(i,j,k+1) = sca(i,j,k-1)
                      elseif (Periodic(2).and..not.Periodic(1).and..not.Periodic(3)) then
                         sca(i+1,j,k) = sca(i-1,j,k)
                         sca(i,j+1,k) = sca(i,j+1-gey,k)
                         sca(i,j,k+1) = sca(i,j,k-1)
                      elseif (.not.Periodic(2).and..not.Periodic(1).and.Periodic(3)) then
                         sca(i+1,j,k) = sca(i-1,j,k)
                         sca(i,j+1,k) = sca(i,j-1,k)
                         sca(i,j,k+1) = sca(i,j,k+1-gez)
                      else
                         sca(i+1,j,k) = sca(i-1,j,k)
                         sca(i,j+1,k) = sca(i,j-1,k)
                         sca(i,j,k+1) = sca(i,j,k-1)
                      end if
                      !BOTTOM/BACKWARD EDGE(1/12)
                   elseif (j==gsy.and.k==gsz) then 
                      sca(i,j,k) = (sca(i,j+1,k)+sca(i,j,k+1))/2
                      if (Periodic(2).and.Periodic(3)) then
                         sca(i,j-1,k) = sca(i,j-1+gey,k)
                         sca(i,j,k-1) = sca(i,j,k-1+gez)
                      elseif (Periodic(2).and..not.Periodic(3)) then
                         sca(i,j-1,k) = sca(i,j-1+gey,k)
                         sca(i,j,k-1) = sca(i,j,k+1)
                      elseif (.not.Periodic(2).and.Periodic(3)) then
                         sca(i,j-1,k) = sca(i,j+1,k)
                         sca(i,j,k-1) = sca(i,j,k-1+gez)
                      else
                         sca(i,j-1,k) = sca(i,j+1,k)
                         sca(i,j,k-1) = sca(i,j,k+1)
                      end if
                      !TOP/BACKWARD EDGE(2/12)
                   elseif (j==gey.and.k==gsz) then 
                      sca(i,j,k) = (sca(i,j-1,k)+sca(i,j,k+1))/2
                      if (Periodic(2).and.Periodic(3)) then
                         sca(i,j+1,k) = sca(i,j+1-gey,k)
                         sca(i,j,k-1) = sca(i,j,k-1+gez)
                      elseif (Periodic(2).and..not.Periodic(3)) then
                         sca(i,j+1,k) = sca(i,j+1-gey,k)
                         sca(i,j,k-1) = sca(i,j,k+1)
                      elseif (.not.Periodic(2).and.Periodic(3)) then
                         sca(i,j+1,k) = sca(i,j-1,k)
                         sca(i,j,k-1) = sca(i,j,k-1+gez)
                      else
                         sca(i,j+1,k) = sca(i,j-1,k)
                         sca(i,j,k-1) = sca(i,j,k+1)
                      end if
                      !BOTTOM/FORWARD EDGE(3/12)
                   elseif (j==gsy.and.k==gez) then 
                      sca(i,j,k) = (sca(i,j+1,k)+sca(i,j,k-1))/2
                      if (Periodic(2).and.Periodic(3)) then
                         sca(i,j-1,k) = sca(i,j-1+gey,k)
                         sca(i,j,k+1) = sca(i,j,k+1-gez)
                      elseif (Periodic(2).and..not.Periodic(3)) then
                         sca(i,j-1,k) = sca(i,j-1+gey,k)
                         sca(i,j,k+1) = sca(i,j,k-1)
                      elseif (.not.Periodic(2).and.Periodic(3)) then
                         sca(i,j-1,k) = sca(i,j+1,k)
                         sca(i,j,k+1) = sca(i,j,k+1-gez)
                      else
                         sca(i,j-1,k) = sca(i,j+1,k)
                         sca(i,j,k+1) = sca(i,j,k-1)
                      end if
                      !TOP/FORWARD EDGE(4/12)
                   elseif (j==gey.and.k==gez) then 
                      sca(i,j,k) = (sca(i,j-1,k)+sca(i,j,k-1))/2
                      if (Periodic(2).and.Periodic(3)) then
                         sca(i,j+1,k) = sca(i,j+1-gey,k)
                         sca(i,j,k+1) = sca(i,j,k+1-gez)
                      elseif (Periodic(2).and..not.Periodic(3)) then
                         sca(i,j+1,k) = sca(i,j+1-gey,k)
                         sca(i,j,k+1) = sca(i,j,k-1)
                      elseif (.not.Periodic(2).and.Periodic(3)) then
                         sca(i,j+1,k) = sca(i,j-1,k)
                         sca(i,j,k+1) = sca(i,j,k+1-gez)
                      else
                         sca(i,j+1,k) = sca(i,j-1,k)
                         sca(i,j,k+1) = sca(i,j,k-1)
                      end if
                      !LEFT/BACKWARD EDGE(5/12)
                   elseif (i==gsx.and.k==gsz) then 
                      sca(i,j,k) = (sca(i+1,j,k)+sca(i,j,k+1))/2
                      if (Periodic(1).and.Periodic(3)) then
                         sca(i-1,j,k) = sca(i-1+gex,j,k)
                         sca(i,j,k-1) = sca(i,j,k-1+gez)
                      elseif (Periodic(1).and..not.Periodic(3)) then
                         sca(i-1,j,k) = sca(i-1+gex,j,k)
                         sca(i,j,k-1) = sca(i,j,k+1)
                      elseif (.not.Periodic(1).and.Periodic(3)) then
                         sca(i-1,j,k) = sca(i+1,j,k)
                         sca(i,j,k-1) = sca(i,j,k-1+gez)
                      else
                         sca(i-1,j,k) = sca(i+1,j,k)
                         sca(i,j,k-1) = sca(i,j,k+1)
                      end if
                      !RIGHT/BACKWARD EDGE(6/12)
                   elseif (i==gex.and.k==gsz) then 
                      sca(i,j,k) = (sca(i-1,j,k)+sca(i,j,k+1))/2
                      if (Periodic(1).and.Periodic(3)) then
                         sca(i+1,j,k) = sca(i+1-gex,j,k)
                         sca(i,j,k-1) = sca(i,j,k-1+gez)
                      elseif (Periodic(1).and..not.Periodic(3)) then
                         sca(i+1,j,k) = sca(i+1-gex,j,k)
                         sca(i,j,k-1) = sca(i,j,k+1)
                      elseif (.not.Periodic(1).and.Periodic(3)) then
                         sca(i+1,j,k) = sca(i-1,j,k)
                         sca(i,j,k-1) = sca(i,j,k-1+gez)
                      else
                         sca(i+1,j,k) = sca(i-1,j,k)
                         sca(i,j,k-1) = sca(i,j,k+1)
                      end if
                      !LEFT/FORWARD EDGE(7/12)
                   elseif (i==gsx.and.k==gez) then
                      sca(i,j,k) = (sca(i+1,j,k)+sca(i,j,k-1))/2
                      if (Periodic(1).and.Periodic(3)) then
                         sca(i-1,j,k) = sca(i-1+gex,j,k)
                         sca(i,j,k+1) = sca(i,j,k+1-gez)
                      elseif (Periodic(1).and..not.Periodic(3)) then
                         sca(i-1,j,k) = sca(i-1+gex,j,k)
                         sca(i,j,k+1) = sca(i,j,k-1)
                      elseif (.not.Periodic(1).and.Periodic(3)) then
                         sca(i-1,j,k) = sca(i+1,j,k)
                         sca(i,j,k+1) = sca(i,j,k+1-gez)
                      else
                         sca(i-1,j,k) = sca(i+1,j,k)
                         sca(i,j,k+1) = sca(i,j,k-1)
                      end if
                      !RIGHT/FORWARD EDGE(8/12)
                   elseif (i==gex.and.k==gez) then 
                      sca(i,j,k) = (sca(i-1,j,k)+sca(i,j,k-1))/2
                      if (Periodic(1).and.Periodic(3)) then
                         sca(i+1,j,k) = sca(i+1-gex,j,k)
                         sca(i,j,k+1) = sca(i,j,k+1-gez)
                      elseif (Periodic(1).and..not.Periodic(3)) then
                         sca(i+1,j,k) = sca(i+1-gex,j,k)
                         sca(i,j,k+1) = sca(i,j,k-1)
                      elseif (.not.Periodic(1).and.Periodic(3)) then
                         sca(i+1,j,k) = sca(i-1,j,k)
                         sca(i,j,k+1) = sca(i,j,k+1-gez)
                      else
                         sca(i+1,j,k) = sca(i-1,j,k)
                         sca(i,j,k+1) = sca(i,j,k-1)
                      end if
                      !LEFT/BOTTOM EDGE(9/12)
                   elseif (i==gsx.and.j==gsy) then 
                      sca(i,j,k) = (sca(i+1,j,k)+sca(i,j+1,k))/2
                      if (Periodic(1).and.Periodic(2)) then
                         sca(i-1,j,k) = sca(i-1+gex,j,k)
                         sca(i,j-1,k) = sca(i,j-1+gey,k)
                      elseif (Periodic(1).and..not.Periodic(2)) then
                         sca(i-1,j,k) = sca(i-1+gex,j,k)
                         sca(i,j-1,k) = sca(i,j+1,k)
                      elseif (.not.Periodic(1).and.Periodic(2)) then
                         sca(i-1,j,k) = sca(i+1,j,k)
                         sca(i,j-1,k) = sca(i,j-1+gey,k)
                      else
                         sca(i-1,j,k) = sca(i+1,j,k)
                         sca(i,j-1,k) = sca(i,j+1,k)
                      end if
                      !RIGHT/BOTTOM EDGE(10/12)
                   elseif (i==gex.and.j==gsy) then 
                      sca(i,j,k) = (sca(i-1,j,k)+sca(i,j+1,k))/2
                      if (Periodic(1).and.Periodic(2)) then
                         sca(i+1,j,k) = sca(i+1-gex,j,k)
                         sca(i,j-1,k) = sca(i,j-1+gey,k)
                      elseif (Periodic(1).and..not.Periodic(2)) then
                         sca(i+1,j,k) = sca(i+1-gex,j,k)
                         sca(i,j-1,k) = sca(i,j+1,k)
                      elseif (.not.Periodic(1).and.Periodic(2)) then
                         sca(i+1,j,k) = sca(i-1,j,k)
                         sca(i,j-1,k) = sca(i,j-1+gey,k)
                      else
                         sca(i+1,j,k) = sca(i-1,j,k)
                         sca(i,j-1,k) = sca(i,j+1,k)
                      end if
                      !LEFT/TOP EDGE(11/12)
                   elseif (i==gsx.and.j==gey) then 
                      sca(i,j,k) = (sca(i+1,j,k)+sca(i,j-1,k))/2
                      if (Periodic(1).and.Periodic(2)) then
                         sca(i-1,j,k) = sca(i-1+gex,j,k)
                         sca(i,j+1,k) = sca(i,j+1-gey,k)
                      elseif (Periodic(1).and..not.Periodic(2)) then
                         sca(i-1,j,k) = sca(i-1+gex,j,k)
                         sca(i,j+1,k) = sca(i,j-1,k)
                      elseif (.not.Periodic(1).and.Periodic(2)) then
                         sca(i-1,j,k) = sca(i+1,j,k)
                         sca(i,j+1,k) = sca(i,j+1-gey,k)
                      else
                         sca(i-1,j,k) = sca(i+1,j,k)
                         sca(i,j+1,k) = sca(i,j-1,k)
                      end if
                      !RIGHT/TOP EDGE(12/12)
                   elseif (i==gex.and.j==gey) then 
                      sca(i,j,k) = (sca(i-1,j,k)+sca(i,j-1,k))/2
                      if (Periodic(1).and.Periodic(2)) then
                         sca(i+1,j,k) = sca(i+1-gex,j,k)
                         sca(i,j+1,k) = sca(i,j+1-gey,k)
                      elseif (Periodic(1).and..not.Periodic(2)) then
                         sca(i+1,j,k) = sca(i+1-gex,j,k)
                         sca(i,j+1,k) = sca(i,j-1,k)
                      elseif (.not.Periodic(1).and.Periodic(2)) then
                         sca(i+1,j,k) = sca(i-1,j,k)
                         sca(i,j+1,k) = sca(i,j+1-gey,k)
                      else
                         sca(i+1,j,k) = sca(i-1,j,k)
                         sca(i,j+1,k) = sca(i,j-1,k)
                      end if
                      !BACKWARD FACE(1/6)
                   elseif (k==gsz) then 
                      if (Periodic(3)) then
                         sca(i,j,k-1) = sca(i,j,k-1+gez)
                      else
                         sca(i,j,k-1) = sca(i,j,k+1)
                      end if
                      !FORWARD FACE(2/6)
                   elseif (k==gez) then 
                      if (Periodic(3)) then
                         sca(i,j,k+1) = sca(i,j,k+1-gez)
                      else
                         sca(i,j,k+1) = sca(i,j,k-1)
                      end if
                      !LEFT FACE(3/6)
                   elseif (i==gsx) then 
                      if (Periodic(1)) then
                         sca(i-1,j,k) = sca(i-1+gex,j,k)
                      else
                         sca(i-1,j,k) = sca(i+1,j,k)
                      end if
                      !RIGHT FACE(4/6)
                   elseif (i==gex) then 
                      if (Periodic(1)) then
                         sca(i+1,j,k) = sca(i+1-gex,j,k)
                      else
                         sca(i+1,j,k) = sca(i-1,j,k)
                      end if
                      !BOTTOM FACE(5/6)
                   elseif (j==gsy) then 
                      if (Periodic(2)) then
                         sca(i,j-1,k) = sca(i,j-1+gey,k)
                      else
                         sca(i,j-1,k) = sca(i,j+1,k)
                      end if
                      !TOP FACE(6/6)
                   elseif (j==gey) then
                      if (Periodic(2)) then
                         sca(i,j+1,k) = sca(i,j+1-gey,k)
                      else
                         sca(i,j+1,k) = sca(i,j-1,k)
                      end if
                   end if
                end do
             end do
          end do

       end if
    else
       if (dim==2) then
          do j = sy,ey
             do i = sx,ex
                !-------------------------------------------------------------------------------
                ! bottom left corner
                if (i==gsx.and.j==gsy) then
                   sca(i,j,1) = (sca(i+1,j,1)+sca(i,j+1,1))/2
                   if (Periodic(1).and.Periodic(2)) then
                   elseif (Periodic(2).and..not.Periodic(1)) then
                      sca(i-1,j,1) = sca(i+1,j,1)
                   elseif (.not.Periodic(2).and.Periodic(1)) then
                      sca(i,j-1,1) = sca(i,j+1,1)
                   else
                      sca(i-1,j,1) = sca(i+1,j,1)
                      sca(i,j-1,1) = sca(i,j+1,1)
                   end if
                   ! bottom right corner
                elseif (i==gex.and.j==gsy) then
                   sca(i,j,1) = (sca(i-1,j,1)+sca(i,j+1,1))/2
                   if (Periodic(2).and.Periodic(1)) then
                   elseif (Periodic(2).and..not.Periodic(1)) then
                      sca(i+1,j,1) = sca(i-1,j,1)
                   elseif (.not.Periodic(2).and.Periodic(1)) then
                      sca(i,j-1,1) = sca(i,j+1,1)
                   else
                      sca(i+1,j,1) = sca(i-1,j,1)
                      sca(i,j-1,1) = sca(i,j+1,1)
                   end if
                   ! top left corner
                elseif (i==gsx.and.j==gey) then
                   sca(i,j,1) = (sca(i+1,j,1)+sca(i,j-1,1))/2
                   if (Periodic(2).and.Periodic(1)) then
                   elseif (Periodic(2).and..not.Periodic(1)) then
                      sca(i-1,j,1) = sca(i+1,j,1)
                   elseif (.not.Periodic(2).and.Periodic(1)) then
                      sca(i,j+1,1) = sca(i,j-1,1)
                   else
                      sca(i-1,j,1) = sca(i+1,j,1)
                      sca(i,j+1,1) = sca(i,j-1,1)
                   end if
                   ! top right corner
                elseif (i==gex.and.j==gey) then
                   sca(i,j,1) = (sca(i-1,j,1)+sca(i,j-1,1))/2
                   if (Periodic(2).and.Periodic(1)) then
                   elseif (Periodic(2).and..not.Periodic(1)) then
                      sca(i+1,j,1) = sca(i-1,j,1)
                   elseif (.not.Periodic(2).and.Periodic(1)) then
                      sca(i,j+1,1) = sca(i,j-1,1)
                   else
                      sca(i+1,j,1) = sca(i-1,j,1)
                      sca(i,j+1,1) = sca(i,j-1,1)
                   end if
                   ! bottom boundary
                elseif (j==gsy) then
                   if (Periodic(2)) then
                   else
                      sca(i,j-1,1) = sca(i,j+1,1)
                   end if
                   ! top boundary
                elseif (j==gey) then
                   if (Periodic(2)) then
                   else
                      sca(i,j+1,1) = sca(i,j-1,1)
                   end if
                   ! left boundary
                elseif (i==gsx) then
                   if (Periodic(1)) then
                   else
                      sca(i-1,j,1) = sca(i+1,j,1)
                   end if
                   ! right boundary
                elseif (i==gex) then
                   if (Periodic(1)) then
                   else
                      sca(i+1,j,1) = sca(i-1,j,1)
                   end if
                end if
                !-------------------------------------------------------------------------------
             end do
          end do
       else if (dim==3) then
          !-------------------------------------------------------------------------------
          do k = sz,ez
             do j = sy,ey
                do i = sx,ex
                   !BOTTOM/LEFT/BACKWARD CORNER(1/8)
                   if (i==gsx.and.j==gsy.and.k==gsz) then 
                      sca(i,j,k) = (sca(i+1,j,k)+sca(i,j+1,k)+sca(i,j,k+1))/3
                      if (Periodic(2).and.Periodic(1).and.Periodic(3)) then
                      elseif (Periodic(2).and.Periodic(1).and..not.Periodic(3)) then
                         sca(i,j,k-1) = sca(i,j,k+1)
                      elseif (Periodic(2).and..not.Periodic(1).and.Periodic(3)) then
                         sca(i-1,j,k) = sca(i+1,j,k)
                      elseif (.not.Periodic(2).and.Periodic(1).and.Periodic(3)) then
                         sca(i,j-1,k) = sca(i,j+1,k)
                      elseif (.not.Periodic(2).and.Periodic(1).and..not.Periodic(3)) then
                         sca(i,j-1,k) = sca(i,j+1,k)
                         sca(i,j,k-1) = sca(i,j,k+1)
                      elseif (Periodic(2).and..not.Periodic(1).and..not.Periodic(3)) then
                         sca(i-1,j,k) = sca(i+1,j,k)
                         sca(i,j,k-1) = sca(i,j,k+1)
                      elseif (.not.Periodic(2).and..not.Periodic(1).and.Periodic(3)) then
                         sca(i-1,j,k) = sca(i+1,j,k)
                         sca(i,j-1,k) = sca(i,j+1,k)
                      else
                         sca(i-1,j,k) = sca(i+1,j,k)
                         sca(i,j-1,k) = sca(i,j+1,k)
                         sca(i,j,k-1) = sca(i,j,k+1)
                      end if
                      !BOTTOM/RIGHT/BACKWARD CORNER(2/8)
                   elseif (i==gex.and.j==gsy.and.k==gsz) then
                      sca(i,j,k) = (sca(i-1,j,k)+sca(i,j+1,k)+sca(i,j,k+1))/3
                      if (Periodic(2).and.Periodic(1).and.Periodic(3)) then
                      elseif (Periodic(2).and.Periodic(1).and..not.Periodic(3)) then
                         sca(i,j,k-1) = sca(i,j,k+1)
                      elseif (Periodic(2).and..not.Periodic(1).and.Periodic(3)) then
                         sca(i+1,j,k) = sca(i-1,j,k)
                      elseif (.not.Periodic(2).and.Periodic(1).and.Periodic(3)) then
                         sca(i,j-1,k) = sca(i,j+1,k)
                      elseif (.not.Periodic(2).and.Periodic(1).and..not.Periodic(3)) then
                         sca(i,j-1,k) = sca(i,j+1,k)
                         sca(i,j,k-1) = sca(i,j,k+1)
                      elseif (Periodic(2).and..not.Periodic(1).and..not.Periodic(3)) then
                         sca(i+1,j,k) = sca(i-1,j,k)
                         sca(i,j,k-1) = sca(i,j,k+1)
                      elseif (.not.Periodic(2).and..not.Periodic(1).and.Periodic(3)) then
                         sca(i+1,j,k) = sca(i-1,j,k)
                         sca(i,j-1,k) = sca(i,j+1,k)
                      else
                         sca(i+1,j,k) = sca(i-1,j,k)
                         sca(i,j-1,k) = sca(i,j+1,k)
                         sca(i,j,k-1) = sca(i,j,k+1)
                      end if
                      !TOP/LEFT/BACKWARD CORNER(3/8)
                   elseif (i==gsx.and.j==gey.and.k==gsz) then 
                      sca(i,j,k) = (sca(i+1,j,k)+sca(i,j-1,k)+sca(i,j,k+1))/3
                      if (Periodic(2).and.Periodic(1).and.Periodic(3)) then
                      elseif (Periodic(2).and.Periodic(1).and..not.Periodic(3)) then
                         sca(i,j,k-1) = sca(i,j,k+1)
                      elseif (Periodic(2).and..not.Periodic(1).and.Periodic(3)) then
                         sca(i-1,j,k) = sca(i+1,j,k)
                      elseif (.not.Periodic(2).and.Periodic(1).and.Periodic(3)) then
                         sca(i,j+1,k) = sca(i,j-1,k)
                      elseif (.not.Periodic(2).and.Periodic(1).and..not.Periodic(3)) then
                         sca(i,j+1,k) = sca(i,j-1,k)
                         sca(i,j,k-1) = sca(i,j,k+1)
                      elseif (Periodic(2).and..not.Periodic(1).and..not.Periodic(3)) then
                         sca(i-1,j,k) = sca(i+1,j,k)
                         sca(i,j,k-1) = sca(i,j,k+1)
                      elseif (.not.Periodic(2).and..not.Periodic(1).and.Periodic(3)) then
                         sca(i-1,j,k) = sca(i+1,j,k)
                         sca(i,j+1,k) = sca(i,j-1,k)
                      else
                         sca(i-1,j,k) = sca(i+1,j,k)
                         sca(i,j+1,k) = sca(i,j-1,k)
                         sca(i,j,k-1) = sca(i,j,k+1)
                      end if
                      !TOP/RIGHT/BACKWARD CORNER(4/8)
                   elseif (i==gex.and.j==gey.and.k==gsz) then 
                      sca(i,j,k) = (sca(i-1,j,k)+sca(i,j-1,k)+sca(i,j,k+1))/3
                      if (Periodic(2).and.Periodic(1).and.Periodic(3)) then
                      elseif (Periodic(2).and.Periodic(1).and..not.Periodic(3)) then
                         sca(i,j,k-1) = sca(i,j,k+1)
                      elseif (Periodic(2).and..not.Periodic(1).and.Periodic(3)) then
                         sca(i+1,j,k) = sca(i-1,j,k)
                      elseif (.not.Periodic(2).and.Periodic(1).and.Periodic(3)) then
                         sca(i,j+1,k) = sca(i,j-1,k)
                      elseif (.not.Periodic(2).and.Periodic(1).and..not.Periodic(3)) then
                         sca(i,j+1,k) = sca(i,j-1,k)
                         sca(i,j,k-1) = sca(i,j,k+1)
                      elseif (Periodic(2).and..not.Periodic(1).and..not.Periodic(3)) then
                         sca(i+1,j,k) = sca(i-1,j,k)
                         sca(i,j,k-1) = sca(i,j,k+1)
                      elseif (.not.Periodic(2).and..not.Periodic(1).and.Periodic(3)) then
                         sca(i+1,j,k) = sca(i-1,j,k)
                         sca(i,j+1,k) = sca(i,j-1,k)
                      else
                         sca(i+1,j,k) = sca(i-1,j,k)
                         sca(i,j+1,k) = sca(i,j-1,k)
                         sca(i,j,k-1) = sca(i,j,k+1)
                      end if
                      !BOTTOM/LEFT/FORWARD CORNER(5/8)
                   elseif (i==gsx.and.j==gsy.and.k==gez) then 
                      sca(i,j,k) = (sca(i+1,j,k)+sca(i,j+1,k)+sca(i,j,k-1))/3
                      if (Periodic(2).and.Periodic(1).and.Periodic(3)) then
                      elseif (Periodic(2).and.Periodic(1).and..not.Periodic(3)) then
                         sca(i,j,k+1) = sca(i,j,k-1)
                      elseif (Periodic(2).and..not.Periodic(1).and.Periodic(3)) then
                         sca(i-1,j,k) = sca(i+1,j,k)
                      elseif (.not.Periodic(2).and.Periodic(1).and.Periodic(3)) then
                         sca(i,j-1,k) = sca(i,j+1,k)
                      elseif (.not.Periodic(2).and.Periodic(1).and..not.Periodic(3)) then
                         sca(i,j-1,k) = sca(i,j+1,k)
                         sca(i,j,k+1) = sca(i,j,k-1)
                      elseif (Periodic(2).and..not.Periodic(1).and..not.Periodic(3)) then
                         sca(i-1,j,k) = sca(i+1,j,k)
                         sca(i,j,k+1) = sca(i,j,k-1)
                      elseif (.not.Periodic(2).and..not.Periodic(1).and.Periodic(3)) then
                         sca(i-1,j,k) = sca(i+1,j,k)
                         sca(i,j-1,k) = sca(i,j+1,k)
                      else
                         sca(i-1,j,k) = sca(i+1,j,k)
                         sca(i,j-1,k) = sca(i,j+1,k)
                         sca(i,j,k+1) = sca(i,j,k-1)
                      end if
                      !BOTTOM/RIGHT/FORWARD CORNER(6/8)
                   elseif (i==gex.and.j==gsy.and.k==gez) then 
                      sca(i,j,k) = (sca(i-1,j,k)+sca(i,j+1,k)+sca(i,j,k-1))/3
                      if (Periodic(2).and.Periodic(1).and.Periodic(3)) then
                      elseif (Periodic(2).and.Periodic(1).and..not.Periodic(3)) then
                         sca(i,j,k+1) = sca(i,j,k-1)
                      elseif (Periodic(2).and..not.Periodic(1).and.Periodic(3)) then
                         sca(i+1,j,k) = sca(i-1,j,k)
                      elseif (.not.Periodic(2).and.Periodic(1).and.Periodic(3)) then
                         sca(i,j-1,k) = sca(i,j+1,k)
                      elseif (.not.Periodic(2).and.Periodic(1).and..not.Periodic(3)) then
                         sca(i,j-1,k) = sca(i,j+1,k)
                         sca(i,j,k+1) = sca(i,j,k-1)
                      elseif (Periodic(2).and..not.Periodic(1).and..not.Periodic(3)) then
                         sca(i+1,j,k) = sca(i-1,j,k)
                         sca(i,j,k+1) = sca(i,j,k-1)
                      elseif (.not.Periodic(2).and..not.Periodic(1).and.Periodic(3)) then
                         sca(i+1,j,k) = sca(i-1,j,k)
                         sca(i,j-1,k) = sca(i,j+1,k)
                      else
                         sca(i+1,j,k) = sca(i-1,j,k)
                         sca(i,j-1,k) = sca(i,j+1,k)
                         sca(i,j,k+1) = sca(i,j,k-1)
                      end if
                      !TOP/LEFT/FORWARD CORNER(7/8)
                   elseif (i==gsx.and.j==gey.and.k==gez) then 
                      sca(i,j,k) = (sca(i+1,j,k)+sca(i,j-1,k)+sca(i,j,k-1))/3
                      if (Periodic(2).and.Periodic(1).and.Periodic(3)) then
                      elseif (Periodic(2).and.Periodic(1).and..not.Periodic(3)) then
                         sca(i,j,k+1) = sca(i,j,k-1)
                      elseif (Periodic(2).and..not.Periodic(1).and.Periodic(3)) then
                         sca(i-1,j,k) = sca(i+1,j,k)
                      elseif (.not.Periodic(2).and.Periodic(1).and.Periodic(3)) then
                         sca(i,j+1,k) = sca(i,j-1,k)
                      elseif (.not.Periodic(2).and.Periodic(1).and..not.Periodic(3)) then
                         sca(i,j+1,k) = sca(i,j-1,k)
                         sca(i,j,k+1) = sca(i,j,k-1)
                      elseif (Periodic(2).and..not.Periodic(1).and..not.Periodic(3)) then
                         sca(i-1,j,k) = sca(i+1,j,k)
                         sca(i,j,k+1) = sca(i,j,k-1)
                      elseif (.not.Periodic(2).and..not.Periodic(1).and.Periodic(3)) then
                         sca(i-1,j,k) = sca(i+1,j,k)
                         sca(i,j+1,k) = sca(i,j-1,k)
                      else
                         sca(i-1,j,k) = sca(i+1,j,k)
                         sca(i,j+1,k) = sca(i,j-1,k)
                         sca(i,j,k+1) = sca(i,j,k-1)
                      end if
                      !TOP/RIGHT/FORWARD CORNER(8/8)
                   elseif (i==gex.and.j==gey.and.k==gez) then
                      sca(i,j,k) = (sca(i-1,j,k)+sca(i,j-1,k)+sca(i,j,k-1))/3
                      if (Periodic(2).and.Periodic(1).and.Periodic(3)) then
                      elseif (Periodic(2).and.Periodic(1).and..not.Periodic(3)) then
                         sca(i,j,k+1) = sca(i,j,k-1)
                      elseif (Periodic(2).and..not.Periodic(1).and.Periodic(3)) then
                         sca(i+1,j,k) = sca(i-1,j,k)
                      elseif (.not.Periodic(2).and.Periodic(1).and.Periodic(3)) then
                         sca(i,j+1,k) = sca(i,j-1,k)
                      elseif (.not.Periodic(2).and.Periodic(1).and..not.Periodic(3)) then
                         sca(i,j+1,k) = sca(i,j-1,k)
                         sca(i,j,k+1) = sca(i,j,k-1)
                      elseif (Periodic(2).and..not.Periodic(1).and..not.Periodic(3)) then
                         sca(i+1,j,k) = sca(i-1,j,k)
                         sca(i,j,k+1) = sca(i,j,k-1)
                      elseif (.not.Periodic(2).and..not.Periodic(1).and.Periodic(3)) then
                         sca(i+1,j,k) = sca(i-1,j,k)
                         sca(i,j+1,k) = sca(i,j-1,k)
                      else
                         sca(i+1,j,k) = sca(i-1,j,k)
                         sca(i,j+1,k) = sca(i,j-1,k)
                         sca(i,j,k+1) = sca(i,j,k-1)
                      end if
                      !BOTTOM/BACKWARD EDGE(1/12)
                   elseif (j==gsy.and.k==gsz) then 
                      sca(i,j,k) = (sca(i,j+1,k)+sca(i,j,k+1))/2
                      if (Periodic(2).and.Periodic(3)) then
                      elseif (Periodic(2).and..not.Periodic(3)) then
                         sca(i,j,k-1) = sca(i,j,k+1)
                      elseif (.not.Periodic(2).and.Periodic(3)) then
                         sca(i,j-1,k) = sca(i,j+1,k)
                      else
                         sca(i,j-1,k) = sca(i,j+1,k)
                         sca(i,j,k-1) = sca(i,j,k+1)
                      end if
                      !TOP/BACKWARD EDGE(2/12)
                   elseif (j==gey.and.k==gsz) then 
                      sca(i,j,k) = (sca(i,j-1,k)+sca(i,j,k+1))/2
                      if (Periodic(2).and.Periodic(3)) then
                      elseif (Periodic(2).and..not.Periodic(3)) then
                         sca(i,j,k-1) = sca(i,j,k+1)
                      elseif (.not.Periodic(2).and.Periodic(3)) then
                         sca(i,j+1,k) = sca(i,j-1,k)
                      else
                         sca(i,j+1,k) = sca(i,j-1,k)
                         sca(i,j,k-1) = sca(i,j,k+1)
                      end if
                      !BOTTOM/FORWARD EDGE(3/12)
                   elseif (j==gsy.and.k==gez) then 
                      sca(i,j,k) = (sca(i,j+1,k)+sca(i,j,k-1))/2
                      if (Periodic(2).and.Periodic(3)) then
                      elseif (Periodic(2).and..not.Periodic(3)) then
                         sca(i,j,k+1) = sca(i,j,k-1)
                      elseif (.not.Periodic(2).and.Periodic(3)) then
                         sca(i,j-1,k) = sca(i,j+1,k)
                      else
                         sca(i,j-1,k) = sca(i,j+1,k)
                         sca(i,j,k+1) = sca(i,j,k-1)
                      end if
                      !TOP/FORWARD EDGE(4/12)
                   elseif (j==gey.and.k==gez) then 
                      sca(i,j,k) = (sca(i,j-1,k)+sca(i,j,k-1))/2
                      if (Periodic(2).and.Periodic(3)) then
                      elseif (Periodic(2).and..not.Periodic(3)) then
                         sca(i,j,k+1) = sca(i,j,k-1)
                      elseif (.not.Periodic(2).and.Periodic(3)) then
                         sca(i,j+1,k) = sca(i,j-1,k)
                      else
                         sca(i,j+1,k) = sca(i,j-1,k)
                         sca(i,j,k+1) = sca(i,j,k-1)
                      end if
                      !LEFT/BACKWARD EDGE(5/12)
                   elseif (i==gsx.and.k==gsz) then 
                      sca(i,j,k) = (sca(i+1,j,k)+sca(i,j,k+1))/2
                      if (Periodic(1).and.Periodic(3)) then
                      elseif (Periodic(1).and..not.Periodic(3)) then
                         sca(i,j,k-1) = sca(i,j,k+1)
                      elseif (.not.Periodic(1).and.Periodic(3)) then
                         sca(i-1,j,k) = sca(i+1,j,k)
                      else
                         sca(i-1,j,k) = sca(i+1,j,k)
                         sca(i,j,k-1) = sca(i,j,k+1)
                      end if
                      !RIGHT/BACKWARD EDGE(6/12)
                   elseif (i==gex.and.k==gsz) then 
                      sca(i,j,k) = (sca(i-1,j,k)+sca(i,j,k+1))/2
                      if (Periodic(1).and.Periodic(3)) then
                      elseif (Periodic(1).and..not.Periodic(3)) then
                         sca(i,j,k-1) = sca(i,j,k+1)
                      elseif (.not.Periodic(1).and.Periodic(3)) then
                         sca(i+1,j,k) = sca(i-1,j,k)
                      else
                         sca(i+1,j,k) = sca(i-1,j,k)
                         sca(i,j,k-1) = sca(i,j,k+1)
                      end if
                      !LEFT/FORWARD EDGE(7/12)
                   elseif (i==gsx.and.k==gez) then
                      sca(i,j,k) = (sca(i+1,j,k)+sca(i,j,k-1))/2
                      if (Periodic(1).and.Periodic(3)) then
                      elseif (Periodic(1).and..not.Periodic(3)) then
                         sca(i,j,k+1) = sca(i,j,k-1)
                      elseif (.not.Periodic(1).and.Periodic(3)) then
                         sca(i-1,j,k) = sca(i+1,j,k)
                      else
                         sca(i-1,j,k) = sca(i+1,j,k)
                         sca(i,j,k+1) = sca(i,j,k-1)
                      end if
                      !RIGHT/FORWARD EDGE(8/12)
                   elseif (i==gex.and.k==gez) then 
                      sca(i,j,k) = (sca(i-1,j,k)+sca(i,j,k-1))/2
                      if (Periodic(1).and.Periodic(3)) then
                      elseif (Periodic(1).and..not.Periodic(3)) then
                         sca(i,j,k+1) = sca(i,j,k-1)
                      elseif (.not.Periodic(1).and.Periodic(3)) then
                         sca(i+1,j,k) = sca(i-1,j,k)
                      else
                         sca(i+1,j,k) = sca(i-1,j,k)
                         sca(i,j,k+1) = sca(i,j,k-1)
                      end if
                      !LEFT/BOTTOM EDGE(9/12)
                   elseif (i==gsx.and.j==gsy) then 
                      sca(i,j,k) = (sca(i+1,j,k)+sca(i,j+1,k))/2
                      if (Periodic(1).and.Periodic(2)) then
                      elseif (Periodic(1).and..not.Periodic(2)) then
                         sca(i,j-1,k) = sca(i,j+1,k)
                      elseif (.not.Periodic(1).and.Periodic(2)) then
                         sca(i-1,j,k) = sca(i+1,j,k)
                      else
                         sca(i-1,j,k) = sca(i+1,j,k)
                         sca(i,j-1,k) = sca(i,j+1,k)
                      end if
                      !RIGHT/BOTTOM EDGE(10/12)
                   elseif (i==gex.and.j==gsy) then 
                      sca(i,j,k) = (sca(i-1,j,k)+sca(i,j+1,k))/2
                      if (Periodic(1).and.Periodic(2)) then
                      elseif (Periodic(1).and..not.Periodic(2)) then
                         sca(i,j-1,k) = sca(i,j+1,k)
                      elseif (.not.Periodic(1).and.Periodic(2)) then
                         sca(i+1,j,k) = sca(i-1,j,k)
                      else
                         sca(i+1,j,k) = sca(i-1,j,k)
                         sca(i,j-1,k) = sca(i,j+1,k)
                      end if
                      !LEFT/TOP EDGE(11/12)
                   elseif (i==gsx.and.j==gey) then 
                      sca(i,j,k) = (sca(i+1,j,k)+sca(i,j-1,k))/2
                      if (Periodic(1).and.Periodic(2)) then
                      elseif (Periodic(1).and..not.Periodic(2)) then
                         sca(i,j+1,k) = sca(i,j-1,k)
                      elseif (.not.Periodic(1).and.Periodic(2)) then
                         sca(i-1,j,k) = sca(i+1,j,k)
                      else
                         sca(i-1,j,k) = sca(i+1,j,k)
                         sca(i,j+1,k) = sca(i,j-1,k)
                      end if
                      !RIGHT/TOP EDGE(12/12)
                   elseif (i==gex.and.j==gey) then 
                      sca(i,j,k) = (sca(i-1,j,k)+sca(i,j-1,k))/2
                      if (Periodic(1).and.Periodic(2)) then
                      elseif (Periodic(1).and..not.Periodic(2)) then
                         sca(i,j+1,k) = sca(i,j-1,k)
                      elseif (.not.Periodic(1).and.Periodic(2)) then
                         sca(i+1,j,k) = sca(i-1,j,k)
                      else
                         sca(i+1,j,k) = sca(i-1,j,k)
                         sca(i,j+1,k) = sca(i,j-1,k)
                      end if
                      !BACKWARD FACE(1/6)
                   elseif (k==gsz) then 
                      if (Periodic(3)) then
                      else
                         sca(i,j,k-1) = sca(i,j,k+1)
                      end if
                      !FORWARD FACE(2/6)
                   elseif (k==gez) then 
                      if (Periodic(3)) then
                      else
                         sca(i,j,k+1) = sca(i,j,k-1)
                      end if
                      !LEFT FACE(3/6)
                   elseif (i==gsx) then 
                      if (Periodic(1)) then
                      else
                         sca(i-1,j,k) = sca(i+1,j,k)
                      end if
                      !RIGHT FACE(4/6)
                   elseif (i==gex) then 
                      if (Periodic(1)) then
                      else
                         sca(i+1,j,k) = sca(i-1,j,k)
                      end if
                      !BOTTOM FACE(5/6)
                   elseif (j==gsy) then 
                      if (Periodic(2)) then
                      else
                         sca(i,j-1,k) = sca(i,j+1,k)
                      end if
                      !TOP FACE(6/6)
                   elseif (j==gey) then
                      if (Periodic(2)) then
                      else
                         sca(i,j+1,k) = sca(i,j-1,k)
                      end if
                   end if
                end do
             end do
          end do

       end if

    end if
    !-------------------------------------------------------------------------------

    !*****************************************************************************************************
  end subroutine NS_scal_boundary
  !*****************************************************************************************************


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
end module mod_borders
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
