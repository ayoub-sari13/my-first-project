!========================================================================
!**
!**   NAME       : InOut.f90
!**
!**   AUTHOR     : Stéphane Vincent
!**                Benoît Trouette
!**
!**   FUNCTION   : subroutines for I/O management
!**
!**   DATES      : from : Jan, 2022
!**
!========================================================================

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#include "precomp.h"
module mod_InOut
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  !*******************************************************************************!
  !   Modules                                                                 
  !*******************************************************************************!
  use mod_Parameters
  use mod_Constants
  USE mod_struct_front_tracking
  use mod_struct_Particle_Tracking
  use mod_mpi
  use mod_tecplot
  !*******************************************************************************!

contains

  subroutine Write_fields(itime,time,u,v,w,                           &
       & sca01,sca02,sca03,sca04,sca05,sca06,sca07,sca08,sca09,sca10, &
       & sca11,sca12,sca13,sca14,sca15,sca16,sca17,sca18,sca19,sca20)
    !*******************************************************************************!
    implicit none
    !---------------------------------------------------------------------
    ! Global variables                                            
    !---------------------------------------------------------------------
    integer, intent(in)                                             :: itime
    real(8), intent(in)                                             :: time
    real(8), dimension(:,:,:), allocatable, intent(in)              :: u,v,w
    real(8), dimension(:,:,:), allocatable, intent(inout), optional ::  &
         & sca01,sca02,sca03,sca04,sca05,sca06,sca07,sca08,sca09,sca10, &
         & sca11,sca12,sca13,sca14,sca15,sca16,sca17,sca18,sca19,sca20
    !---------------------------------------------------------------------
    ! Local variables                                             
    !---------------------------------------------------------------------
    integer                                                         :: nbVar,cVar
    real(8), dimension(:,:,:,:), allocatable                        :: var
    integer, dimension(:), allocatable                              :: varType
    character(len=80), dimension(:), allocatable                    :: varName
    character(len=80)                                               :: caseName
    character(len=80)                                               :: iterName
    character(len=80)                                               :: varIterName
    !---------------------------------------------------------------------

    !---------------------------------------------------------------------
    ! VTK
    !---------------------------------------------------------------------
    if (ivtk>0) then
       if (itime==0.or.modulo(itime,ivtk)==0) then
          call Write_field_vtk(666,itime,time,u,v,w,                          &
               & sca01,sca02,sca03,sca04,sca05,sca06,sca07,sca08,sca09,sca10, &
               & sca11,sca12,sca13,sca14,sca15,sca16,sca17,sca18,sca19,sca20)
       end if
    end if
    !---------------------------------------------------------------------

    !---------------------------------------------------------------------
    ! Ensight Gold
    !---------------------------------------------------------------------
    if (itime>0) then  ! ne gere pas encore l'ajout d'une variable ecrite au cours du calcul
       if (file_egld%dt>0) then
          if (time>=file_egld%time) then
             file_egld%time=time+file_egld%dt
             file_egld%write=.true.
          end if
       else 
          if (iegld>0) then
             if (itime==1.or.modulo(itime,iegld)==0) then
                file_egld%write=.true.
             end if
          end if
       end if
    end if
    !---------------------------------------------------------------------
    if (file_egld%write) then
       call Write_field_ensight_gold(666,itime,time,u,v,w,                 &
            & sca01,sca02,sca03,sca04,sca05,sca06,sca07,sca08,sca09,sca10, &
            & sca11,sca12,sca13,sca14,sca15,sca16,sca17,sca18,sca19,sca20)
       file_egld%write=.false.
    end if
    !---------------------------------------------------------------------
    
    
    !---------------------------------------------------------------------
    ! PLT (Tecplot)
    !---------------------------------------------------------------------
    if (itec>0) then
       if (itime==0.or.modulo(itime,itec)==0) then
          call Write_field_tec(666,itime,time,u,v,w,                          &
               & sca01,sca02,sca03,sca04,sca05,sca06,sca07,sca08,sca09,sca10, &
               & sca11,sca12,sca13,sca14,sca15,sca16,sca17,sca18,sca19,sca20)
       end if
    end if
    !---------------------------------------------------------------------

    !---------------------------------------------------------------------
    ! HDF5
    !---------------------------------------------------------------------
    if (ih5>0) then
       if (itime==0.or.modulo(itime,ih5)==0) then
          call Write_field_phdf5(666,itime,time,u,v,w,                        &
               & sca01,sca02,sca03,sca04,sca05,sca06,sca07,sca08,sca09,sca10, &
               & sca11,sca12,sca13,sca14,sca15,sca16,sca17,sca18,sca19,sca20)
       end if
    end if
    !---------------------------------------------------------------------
    
  end subroutine Write_fields
  !*******************************************************************************!

  subroutine Write_particles(itime,time,LPart,nPart,SPart,nPartS,VPart,nPartV)
    !*******************************************************************************!
    implicit none
    !---------------------------------------------------------------------
    ! Global variables                                            
    !---------------------------------------------------------------------
    integer, intent(in)                                   :: itime,nPart,nPartS,nPartV
    real(8), intent(in)                                   :: time
    type(Particle), allocatable, dimension(:), intent(in) :: LPart,SPart,VPart
    !---------------------------------------------------------------------
    ! Local variables                                             
    !---------------------------------------------------------------------
    !---------------------------------------------------------------------

    !---------------------------------------------------------------------
    ! VTK
    !---------------------------------------------------------------------
    if (ivtk>0) then
       if (itime==0.or.modulo(itime,ivtk)==0) then
          if (allocated(LPart)) then 
             call Write_Particle_vtk(666,itime,time,LPart,nPart)
          elseif (allocated(SPart)) then
             call Write_Particle_vtk(666,itime,time,SPart,nPartS)
          elseif (allocated(VPart)) then 
             call Write_Particle_vtk(666,itime,time,VPart,nPartV)
          end if
       end if
    end if
    !---------------------------------------------------------------------

    !---------------------------------------------------------------------
    ! PLT (Tecplot)
    !---------------------------------------------------------------------
    if (itec>0) then
       if (itime==0.or.modulo(itime,itec)==0) then
       end if
    end if
    !---------------------------------------------------------------------
    
    !---------------------------------------------------------------------
    ! VMD
    !---------------------------------------------------------------------
    if (ivmd>0) then
       if (itime==0.or.modulo(itime,ivmd)==0) then
          if (allocated(LPart)) then 
             call Write_xyz(666,itime,time,LPart,nPart)
          elseif (allocated(SPart)) then
             call Write_xyz(666,itime,time,SPart,nPartS)
          elseif (allocated(VPart)) then 
             call Write_xyz(666,itime,time,VPart,nPartV)
          end if
       end if
    end if
    !---------------------------------------------------------------------
    
  end subroutine Write_particles
  !*******************************************************************************!


  
  subroutine Write_field_phdf5(unit,itime,time,u,v,w,                 &
       & sca01,sca02,sca03,sca04,sca05,sca06,sca07,sca08,sca09,sca10, &
       & sca11,sca12,sca13,sca14,sca15,sca16,sca17,sca18,sca19,sca20)
#if WRITE_HDF5
    use HDF5
#endif 
    implicit none 
    !---------------------------------------------------------------------
    ! Global variables                                            
    !---------------------------------------------------------------------
    integer, intent(in)                                             :: unit,itime
    real(8), intent(in)                                             :: time
    real(8), dimension(:,:,:), allocatable, intent(in)              :: u,v,w
    real(8), dimension(:,:,:), allocatable, intent(inout), optional ::  &
         & sca01,sca02,sca03,sca04,sca05,sca06,sca07,sca08,sca09,sca10, &
         & sca11,sca12,sca13,sca14,sca15,sca16,sca17,sca18,sca19,sca20
#if WRITE_HDF5
    !---------------------------------------------------------------------
    ! Local variables                                             
    !---------------------------------------------------------------------
    INTEGER                                :: i,j,k,ii,jj,kk,n
    INTEGER                                :: error           ! Error flags
    INTEGER                                :: rank_data       ! Dataset rank_data 
    integer, dimension(dim)                :: lchunk_dims,gchunk_dims
    real(8), ALLOCATABLE, DIMENSION(:,:,:) :: data
    !---------------------------------------------------------------------
    ! HDF5 variables
    !---------------------------------------------------------------------
    INTEGER(HID_T)                  :: file_id       ! File identifier 
    INTEGER(HID_T)                  :: dset_id       ! Dataset identifier 
    INTEGER(HID_T)                  :: filespace     ! Dataspace identifier in file 
    INTEGER(HID_T)                  :: memspace      ! Dataspace identifier in memory
    INTEGER(HID_T)                  :: plist_id      ! Property list identifier 
    INTEGER(HID_T)                  :: group1_id,group2_id,group3_id ! Property list identifier 
    INTEGER(HSIZE_T), DIMENSION(3)  :: dimsf                                                     
    INTEGER(HSIZE_T), DIMENSION(3)  :: dimsfi
    INTEGER(HSIZE_T), DIMENSION(3)  :: chunk_dims
    INTEGER(HSIZE_T), DIMENSION(3)  :: cnt  
    INTEGER(HSSIZE_T), DIMENSION(3) :: offset 
    INTEGER(HSIZE_T), DIMENSION(3)  :: stride
    INTEGER(HSIZE_T), DIMENSION(3)  :: blck
    !---------------------------------------------------------------------
    CHARACTER(LEN=99)               :: filename  ! File name
    CHARACTER(LEN=12)               :: dsetname  ! Dataset name
    CHARACTER(LEN=12)               :: groupname1,groupname2,groupname3
    CHARACTER(LEN=99)               :: name
    !---------------------------------------------------------------------
    ! MPI variables
    !---------------------------------------------------------------------
    INTEGER :: mpierror       ! MPI error flag
    INTEGER :: info
    !---------------------------------------------------------------------
    !---------------------------------------------------------------------
    !---------------------------------------------------------------------
    ! MPI init variables
    !---------------------------------------------------------------------
    info = MPI_INFO_NULL
    !---------------------------------------------------------------------

    !---------------------------------------------------------------------
    ! mpi 
    !---------------------------------------------------------------------
    if (nproc>1) then
       if (present(sca01)) then
          if (allocated(sca01)) call comm_mpi_sca(sca01)
       end if
       if (present(sca02)) then
          if (allocated(sca02)) call comm_mpi_sca(sca02)
       end if
       if(present(sca03)) then
          if (allocated(sca03)) call comm_mpi_sca(sca03)
       end if
       if (present(sca04)) then
          if (allocated(sca04)) call comm_mpi_sca(sca04)
       end if
       if (present(sca05)) then
          if (allocated(sca05)) call comm_mpi_sca(sca05)
       end if
       if (present(sca06)) then
          if (allocated(sca06)) call comm_mpi_sca(sca06)
       end if
       if (present(sca07)) then
          if (allocated(sca07)) call comm_mpi_sca(sca07)
       end if
       if (present(sca08)) then
          if (allocated(sca08)) call comm_mpi_sca(sca08)
       end if
       if (present(sca09)) then
          if (allocated(sca09)) call comm_mpi_sca(sca09)
       end if
       if (present(sca10)) then
          if (allocated(sca10)) call comm_mpi_sca(sca10)
       end if
       if (present(sca11)) then
          if (allocated(sca11)) call comm_mpi_sca(sca11)
       end if
       if (present(sca12)) then
          if (allocated(sca12)) call comm_mpi_sca(sca12)
       end if
       if (present(sca13)) then
          if (allocated(sca13)) call comm_mpi_sca(sca13)
       end if
       if (present(sca14)) then
          if (allocated(sca14)) call comm_mpi_sca(sca14)
       end if
       if (present(sca15)) then
          if (allocated(sca15)) call comm_mpi_sca(sca15)
       end if
       if (present(sca16)) then
          if (allocated(sca16)) call comm_mpi_sca(sca16)
       end if
       if (present(sca17)) then
          if (allocated(sca17)) call comm_mpi_sca(sca17)
       end if
       if (present(sca18)) then
          if (allocated(sca18)) call comm_mpi_sca(sca18)
       end if
       if (present(sca19)) then
          if (allocated(sca19)) call comm_mpi_sca(sca19)
       end if
       if (present(sca20)) then
          if (allocated(sca20)) call comm_mpi_sca(sca20)
       end if
    end if
    !---------------------------------------------------------------------

    !---------------------------------------------------------------------
    ! Build file name
    !---------------------------------------------------------------------
    write(name,'(i8.8)') itime
    filename=trim(adjustl(cas))//"_i"//trim(adjustl(name))//".h5"
    !---------------------------------------------------------------------

    !---------------------------------------------------------------------
    ! Get dimensions
    !---------------------------------------------------------------------
    rank_data=3
    dimsf(1:dim)=(/gex-gsx+1,gey-gsy+1,gez-gsz+1/)
    dimsfi(1:dim)=(/gex-gsx+1,gey-gsy+1,gez-gsz+1/)
    lchunk_dims(1:dim)=(/ex-sx+1,ey-sy+1,ez-sz+1/)
    call mpi_allreduce(lchunk_dims,gchunk_dims,rank_data,mpi_integer,mpi_max,comm3d,code)
    chunk_dims=gchunk_dims
    !---------------------------------------------------------------------

    !---------------------------------------------------------------------
    ! Initialize HDF5 library and Fortran interfaces.
    !---------------------------------------------------------------------
    CALL h5open_f(error) 
    !---------------------------------------------------------------------

    !---------------------------------------------------------------------
    ! Setup file access property list with parallel I/O access.
    !---------------------------------------------------------------------
    CALL h5pcreate_f(H5P_FILE_ACCESS_F, plist_id, error)
    CALL h5pset_fapl_mpio_f(plist_id, comm3d, info, error)
    !---------------------------------------------------------------------

    !---------------------------------------------------------------------
    ! Create the file collectively.
    !---------------------------------------------------------------------
    CALL h5fcreate_f(filename, H5F_ACC_TRUNC_F, file_id, error, access_prp = plist_id)
    CALL h5pclose_f(plist_id, error)
    !---------------------------------------------------------------------

    allocate(data(chunk_dims(1),chunk_dims(2),chunk_dims(3)))

    !---------------------------------------------------------------------
    ! Create groups
    !---------------------------------------------------------------------
    groupname1=""!"GRID/"
    groupname2=""!"VELOCITY/"
    groupname3=""!"SCALAR/"
!!$    CALL h5gcreate_f(file_id, groupname1, group1_id, error)
!!$    CALL h5gcreate_f(file_id, groupname2, group2_id, error)
!!$    CALL h5gcreate_f(file_id, groupname3, group3_id, error)
    !---------------------------------------------------------------------

    !---------------------------------------------------------------------
    ! Writing fields
    !---------------------------------------------------------------------
    dsetname=trim(adjustl(groupname1))//"X"
    do k=1,chunk_dims(3)
       do j=1,chunk_dims(2)
          do i=1,chunk_dims(1)
             ii=i+sx-1
             data(i,j,k)=grid_x(ii)
          end do
       end do
    end do
    call write_hdf5_real(filename,data,rank_data,dimsf,dimsfi,&
         & chunk_dims,dsetname,plist_id,file_id)
    !---------------------------------------------------------------------
    dsetname=trim(adjustl(groupname1))//"Y"
    do k=1,chunk_dims(3)
       do j=1,chunk_dims(2)
          do i=1,chunk_dims(1)
             jj=j+sy-1
             data(i,j,k)=grid_y(jj)
          end do
       end do
    end do
    call write_hdf5_real(filename,data,rank_data,dimsf,dimsfi,&
         & chunk_dims,dsetname,plist_id,file_id)
    !---------------------------------------------------------------------
    dsetname=trim(adjustl(groupname1))//"Z"
    do k=1,chunk_dims(3)
       do j=1,chunk_dims(2)
          do i=1,chunk_dims(1)
             kk=k+sz-1
             data(i,j,k)=grid_z(kk)
          end do
       end do
    end do
    call write_hdf5_real(filename,data,rank_data,dimsf,dimsfi,&
         & chunk_dims,dsetname,plist_id,file_id)
    !---------------------------------------------------------------------
    dsetname=trim(adjustl(groupname2))//"u"
    do k=1,chunk_dims(3)
       do j=1,chunk_dims(2)
          do i=1,chunk_dims(1)
             ii=i+sx-1
             jj=j+sy-1
             kk=k+sz-1
             data(i,j,k)=(dxu2(ii+1)*u(ii,jj,kk)+dxu2(ii)*u(ii+1,jj,kk))/dx(ii)
          end do
       end do
    end do
    call write_hdf5_real(filename,data,rank_data,dimsf,dimsfi,&
         & chunk_dims,dsetname,plist_id,file_id)
    !---------------------------------------------------------------------
    dsetname=trim(adjustl(groupname2))//"v"
    do k=1,chunk_dims(3)
       do j=1,chunk_dims(2)
          do i=1,chunk_dims(1)
             ii=i+sx-1
             jj=j+sy-1
             kk=k+sz-1
             data(i,j,k)=(dyv2(jj+1)*v(ii,jj,kk)+dyv2(jj)*v(ii,jj+1,kk))/dy(jj)
          end do
       end do
    end do
    call write_hdf5_real(filename,data,rank_data,dimsf,dimsfi,&
         & chunk_dims,dsetname,plist_id,file_id)
    !---------------------------------------------------------------------
    if (dim==3) then
       dsetname=trim(adjustl(groupname2))//"w"
       do k=1,chunk_dims(3)
          do j=1,chunk_dims(2)
             do i=1,chunk_dims(1)
                ii=i+sx-1
                jj=j+sy-1
                kk=k+sz-1
                data(i,j,k)=(dzw2(kk+1)*w(ii,jj,kk)+dzw2(kk)*w(ii,jj,kk+1))/dz(kk)
             end do
          end do
       end do
       call write_hdf5_real(filename,data,rank_data,dimsf,dimsfi,&
            & chunk_dims,dsetname,plist_id,file_id)
    end if
    !---------------------------------------------------------------------
    if (present(sca01)) then
       if (allocated(sca01)) then
          dsetname=trim(adjustl(groupname3))//"sca01"
          do k=1,chunk_dims(3)
             do j=1,chunk_dims(2)
                do i=1,chunk_dims(1)
                   ii=i+sx-1
                   jj=j+sy-1
                   kk=k+sz-1
                   data(i,j,k)=sca01(ii,jj,kk)
                end do
             end do
          end do
          call write_hdf5_real(filename,data,rank_data,dimsf,dimsfi,&
               & chunk_dims,dsetname,plist_id,file_id)
       end if
    end if
    !---------------------------------------------------------------------
    if (present(sca02)) then
       if (allocated(sca02)) then
          dsetname=trim(adjustl(groupname3))//"sca02"
          do k=1,chunk_dims(3)
             do j=1,chunk_dims(2)
                do i=1,chunk_dims(1)
                   ii=i+sx-1
                   jj=j+sy-1
                   kk=k+sz-1
                   data(i,j,k)=sca02(ii,jj,kk)
                end do
             end do
          end do
          call write_hdf5_real(filename,data,rank_data,dimsf,dimsfi,&
               & chunk_dims,dsetname,plist_id,file_id)
       end if
    end if
    !---------------------------------------------------------------------
    if (present(sca03)) then
       if (allocated(sca03)) then
          dsetname=trim(adjustl(groupname3))//"sca03"
          do k=1,chunk_dims(3)
             do j=1,chunk_dims(2)
                do i=1,chunk_dims(1)
                   ii=i+sx-1
                   jj=j+sy-1
                   kk=k+sz-1
                   data(i,j,k)=sca03(ii,jj,kk)
                end do
             end do
          end do
          call write_hdf5_real(filename,data,rank_data,dimsf,dimsfi,&
               & chunk_dims,dsetname,plist_id,file_id)
       end if
    end if
    !---------------------------------------------------------------------
    if (present(sca04)) then
       if (allocated(sca04)) then
          dsetname=trim(adjustl(groupname3))//"sca04"
          do k=1,chunk_dims(3)
             do j=1,chunk_dims(2)
                do i=1,chunk_dims(1)
                   ii=i+sx-1
                   jj=j+sy-1
                   kk=k+sz-1
                   data(i,j,k)=sca04(ii,jj,kk)
                end do
             end do
          end do
          call write_hdf5_real(filename,data,rank_data,dimsf,dimsfi,&
               & chunk_dims,dsetname,plist_id,file_id)
       end if
    end if
    !---------------------------------------------------------------------
    if (present(sca05)) then
       if (allocated(sca05)) then
          dsetname=trim(adjustl(groupname3))//"sca05"
          do k=1,chunk_dims(3)
             do j=1,chunk_dims(2)
                do i=1,chunk_dims(1)
                   ii=i+sx-1
                   jj=j+sy-1
                   kk=k+sz-1
                   data(i,j,k)=sca05(ii,jj,kk)
                end do
             end do
          end do
          call write_hdf5_real(filename,data,rank_data,dimsf,dimsfi,&
               & chunk_dims,dsetname,plist_id,file_id)
       end if
    end if
    !---------------------------------------------------------------------
    if (present(sca06)) then
       if (allocated(sca06)) then
          dsetname=trim(adjustl(groupname3))//"sca06"
          do k=1,chunk_dims(3)
             do j=1,chunk_dims(2)
                do i=1,chunk_dims(1)
                   ii=i+sx-1
                   jj=j+sy-1
                   kk=k+sz-1
                   data(i,j,k)=sca06(ii,jj,kk)
                end do
             end do
          end do
          call write_hdf5_real(filename,data,rank_data,dimsf,dimsfi,&
               & chunk_dims,dsetname,plist_id,file_id)
       end if
    end if
    !---------------------------------------------------------------------
    if (present(sca07)) then
       if (allocated(sca07)) then
          dsetname=trim(adjustl(groupname3))//"sca07"
          do k=1,chunk_dims(3)
             do j=1,chunk_dims(2)
                do i=1,chunk_dims(1)
                   ii=i+sx-1
                   jj=j+sy-1
                   kk=k+sz-1
                   data(i,j,k)=sca07(ii,jj,kk)
                end do
             end do
          end do
          call write_hdf5_real(filename,data,rank_data,dimsf,dimsfi,&
               & chunk_dims,dsetname,plist_id,file_id)
       end if
    end if
    !---------------------------------------------------------------------
    if (present(sca08)) then
       if (allocated(sca08)) then
          dsetname=trim(adjustl(groupname3))//"sca08"
          do k=1,chunk_dims(3)
             do j=1,chunk_dims(2)
                do i=1,chunk_dims(1)
                   ii=i+sx-1
                   jj=j+sy-1
                   kk=k+sz-1
                   data(i,j,k)=sca08(ii,jj,kk)
                end do
             end do
          end do
          call write_hdf5_real(filename,data,rank_data,dimsf,dimsfi,&
               & chunk_dims,dsetname,plist_id,file_id)
       end if
    end if
    !---------------------------------------------------------------------
    if (present(sca09)) then
       if (allocated(sca09)) then
          dsetname=trim(adjustl(groupname3))//"sca09"
          do k=1,chunk_dims(3)
             do j=1,chunk_dims(2)
                do i=1,chunk_dims(1)
                   ii=i+sx-1
                   jj=j+sy-1
                   kk=k+sz-1
                   data(i,j,k)=sca09(ii,jj,kk)
                end do
             end do
          end do
          call write_hdf5_real(filename,data,rank_data,dimsf,dimsfi,&
               & chunk_dims,dsetname,plist_id,file_id)
       end if
    end if
    !---------------------------------------------------------------------
    if (present(sca10)) then
       if (allocated(sca10)) then
          dsetname=trim(adjustl(groupname3))//"sca10"
          do k=1,chunk_dims(3)
             do j=1,chunk_dims(2)
                do i=1,chunk_dims(1)
                   ii=i+sx-1
                   jj=j+sy-1
                   kk=k+sz-1
                   data(i,j,k)=sca10(ii,jj,kk)
                end do
             end do
          end do
          call write_hdf5_real(filename,data,rank_data,dimsf,dimsfi,&
               & chunk_dims,dsetname,plist_id,file_id)
       end if
    end if
    !---------------------------------------------------------------------
    if (present(sca11)) then
       if (allocated(sca11)) then
          dsetname=trim(adjustl(groupname3))//"sca11"
          do k=1,chunk_dims(3)
             do j=1,chunk_dims(2)
                do i=1,chunk_dims(1)
                   ii=i+sx-1
                   jj=j+sy-1
                   kk=k+sz-1
                   data(i,j,k)=sca11(ii,jj,kk)
                end do
             end do
          end do
          call write_hdf5_real(filename,data,rank_data,dimsf,dimsfi,&
               & chunk_dims,dsetname,plist_id,file_id)
       end if
    end if
    !---------------------------------------------------------------------
    if (present(sca12)) then
       if (allocated(sca12)) then
          dsetname=trim(adjustl(groupname3))//"sca12"
          do k=1,chunk_dims(3)
             do j=1,chunk_dims(2)
                do i=1,chunk_dims(1)
                   ii=i+sx-1
                   jj=j+sy-1
                   kk=k+sz-1
                   data(i,j,k)=sca12(ii,jj,kk)
                end do
             end do
          end do
          call write_hdf5_real(filename,data,rank_data,dimsf,dimsfi,&
               & chunk_dims,dsetname,plist_id,file_id)
       end if
    end if
    !---------------------------------------------------------------------
    if (present(sca13)) then
       if (allocated(sca13)) then
          dsetname=trim(adjustl(groupname3))//"sca13"
          do k=1,chunk_dims(3)
             do j=1,chunk_dims(2)
                do i=1,chunk_dims(1)
                   ii=i+sx-1
                   jj=j+sy-1
                   kk=k+sz-1
                   data(i,j,k)=sca13(ii,jj,kk)
                end do
             end do
          end do
          call write_hdf5_real(filename,data,rank_data,dimsf,dimsfi,&
               & chunk_dims,dsetname,plist_id,file_id)
       end if
    end if
    !---------------------------------------------------------------------
    if (present(sca14)) then
       if (allocated(sca14)) then
          dsetname=trim(adjustl(groupname3))//"sca14"
          do k=1,chunk_dims(3)
             do j=1,chunk_dims(2)
                do i=1,chunk_dims(1)
                   ii=i+sx-1
                   jj=j+sy-1
                   kk=k+sz-1
                   data(i,j,k)=sca14(ii,jj,kk)
                end do
             end do
          end do
          call write_hdf5_real(filename,data,rank_data,dimsf,dimsfi,&
               & chunk_dims,dsetname,plist_id,file_id)
       end if
    end if
    !---------------------------------------------------------------------
    if (present(sca15)) then
       if (allocated(sca15)) then
          dsetname=trim(adjustl(groupname3))//"sca15"
          do k=1,chunk_dims(3)
             do j=1,chunk_dims(2)
                do i=1,chunk_dims(1)
                   ii=i+sx-1
                   jj=j+sy-1
                   kk=k+sz-1
                   data(i,j,k)=sca15(ii,jj,kk)
                end do
             end do
          end do
          call write_hdf5_real(filename,data,rank_data,dimsf,dimsfi,&
               & chunk_dims,dsetname,plist_id,file_id)
       end if
    end if
    !---------------------------------------------------------------------
    if (present(sca16)) then
       if (allocated(sca16)) then
          dsetname=trim(adjustl(groupname3))//"sca16"
          do k=1,chunk_dims(3)
             do j=1,chunk_dims(2)
                do i=1,chunk_dims(1)
                   ii=i+sx-1
                   jj=j+sy-1
                   kk=k+sz-1
                   data(i,j,k)=sca16(ii,jj,kk)
                end do
             end do
          end do
          call write_hdf5_real(filename,data,rank_data,dimsf,dimsfi,&
               & chunk_dims,dsetname,plist_id,file_id)
       end if
    end if
    !----------------------------------------------------------------
    if (present(sca17)) then 
       if (allocated(sca17)) then
          dsetname=trim(adjustl(groupname3))//"sca17"
          do k=1,chunk_dims(3)
             do j=1,chunk_dims(2)
                do i=1,chunk_dims(1)
                   ii=i+sx-1
                   jj=j+sy-1
                   kk=k+sz-1
                   data(i,j,k)=sca17(ii,jj,kk)
                end do
             end do
          end do
          call write_hdf5_real(filename,data,rank_data,dimsf,dimsfi,&
               & chunk_dims,dsetname,plist_id,file_id)
       end if
    end if
    !----------------------------------------------------------------
    if (present(sca18)) then
       if (allocated(sca18)) then
          dsetname=trim(adjustl(groupname3))//"sca18"
          do k=1,chunk_dims(3)
             do j=1,chunk_dims(2)
                do i=1,chunk_dims(1)
                   ii=i+sx-1
                   jj=j+sy-1
                   kk=k+sz-1
                   data(i,j,k)=sca18(ii,jj,kk)
                end do
             end do
          end do
          call write_hdf5_real(filename,data,rank_data,dimsf,dimsfi,&
               & chunk_dims,dsetname,plist_id,file_id)
       end if
    end if
    !----------------------------------------------------------------
    if (present(sca19)) then
       if (allocated(sca19)) then
          dsetname=trim(adjustl(groupname3))//"sca19"
          do k=1,chunk_dims(3)
             do j=1,chunk_dims(2)
                do i=1,chunk_dims(1)
                   ii=i+sx-1
                   jj=j+sy-1
                   kk=k+sz-1
                   data(i,j,k)=sca19(ii,jj,kk)
                end do
             end do
          end do
          call write_hdf5_real(filename,data,rank_data,dimsf,dimsfi,&
               & chunk_dims,dsetname,plist_id,file_id)
       end if
    end if
    !----------------------------------------------------------------
    if (present(sca20)) then
       if (allocated(sca20)) then
          dsetname=trim(adjustl(groupname3))//"sca20"
          do k=1,chunk_dims(3)
             do j=1,chunk_dims(2)
                do i=1,chunk_dims(1)
                   ii=i+sx-1
                   jj=j+sy-1
                   kk=k+sz-1
                   data(i,j,k)=sca20(ii,jj,kk)
                end do
             end do
          end do
          call write_hdf5_real(filename,data,rank_data,dimsf,dimsfi,&
               & chunk_dims,dsetname,plist_id,file_id)
       end if
    end if
    !----------------------------------------------------------------
    deallocate(data)

    !---------------------------------------------------------------------
    ! Close groups
    !---------------------------------------------------------------------
!!$    CALL h5gclose_f(group1_id, error)
!!$    CALL h5gclose_f(group2_id, error)
!!$    CALL h5gclose_f(group3_id, error)
    !---------------------------------------------------------------------


    !---------------------------------------------------------------------
    ! Close the file.
    !---------------------------------------------------------------------
    CALL h5fclose_f(file_id, error)
    !---------------------------------------------------------------------

    !---------------------------------------------------------------------
    ! Close FORTRAN interfaces and HDF5 library.
    !---------------------------------------------------------------------
    CALL h5close_f(error)
    !---------------------------------------------------------------------
#endif 
  end subroutine Write_field_phdf5

#if WRITE_HDF5
  subroutine write_hdf5_real(filename,data,rank_data,dimsf,dimsfi,chunk_dims,&
       & dsetname,plist_id,file_id)
    use HDF5
    implicit none 
    !---------------------------------------------------------------------
    ! Global variables                                            
    !---------------------------------------------------------------------
    INTEGER, intent(in)                                :: rank_data       ! Dataset rank_data 
    INTEGER(HID_T), intent(in)                         :: file_id       ! File identifier 
    INTEGER(HID_T), intent(inout)                      :: plist_id      ! Property list identifier 
    INTEGER(HSIZE_T), DIMENSION(3), intent(in)         :: dimsf,dimsfi,chunk_dims
    real(8), dimension(:,:,:), allocatable, intent(in) :: data
    CHARACTER(LEN=12), intent(in)                      :: dsetname ! Dataset name
    CHARACTER(LEN=99), intent(in)                      :: filename
    !---------------------------------------------------------------------
    ! Local variables                                             
    !---------------------------------------------------------------------
    INTEGER                              :: n
    INTEGER                              :: error           ! Error flags
    !---------------------------------------------------------------------
    ! HDF5 variables
    !---------------------------------------------------------------------
    INTEGER(HID_T)                  :: dset_id       ! Dataset identifier 
    INTEGER(HID_T)                  :: filespace     ! Dataspace identifier in file 
    INTEGER(HID_T)                  :: memspace      ! Dataspace identifier in memory
    INTEGER(HSIZE_T), DIMENSION(3)  :: cnt  
    INTEGER(HSSIZE_T), DIMENSION(3) :: offset 
    INTEGER(HSIZE_T), DIMENSION(3)  :: stride
    INTEGER(HSIZE_T), DIMENSION(3)  :: blck
    !---------------------------------------------------------------------
    ! MPI variables
    !---------------------------------------------------------------------
    INTEGER :: mpierror       ! MPI error flag
    INTEGER :: info
    !---------------------------------------------------------------------

    info = MPI_INFO_NULL

    !---------------------------------------------------------------------
    ! Create the data space for the dataset. 
    !---------------------------------------------------------------------
    CALL h5screate_simple_f(rank_data, dimsf, filespace, error)
    CALL h5screate_simple_f(rank_data, chunk_dims, memspace, error)
    !---------------------------------------------------------------------

    !---------------------------------------------------------------------
    ! Create chunked dataset.
    !---------------------------------------------------------------------
    CALL h5pcreate_f(H5P_DATASET_CREATE_F, plist_id, error)
    CALL h5pset_chunk_f(plist_id, rank_data, chunk_dims, error)
    CALL h5dcreate_f(file_id, dsetname, H5T_NATIVE_DOUBLE, filespace, &
         dset_id, error, plist_id)
    CALL h5sclose_f(filespace, error)
    !---------------------------------------------------------------------

    !---------------------------------------------------------------------
    ! Each process defines dataset in memory and writes it to the hyperslab
    ! in the file. 
    !---------------------------------------------------------------------
    stride=1
    cnt=1
    blck=chunk_dims
    offset=(/sx,sy,sz/)
    if (dim==2) offset(3)=0
    !---------------------------------------------------------------------
    ! Select hyperslab in the file.
    !---------------------------------------------------------------------
    CALL h5dget_space_f(dset_id, filespace, error)
    CALL h5sselect_hyperslab_f (filespace, H5S_SELECT_SET_F, offset, cnt, error, &
         stride, blck)
    !---------------------------------------------------------------------

    !---------------------------------------------------------------------
    ! Create property list for collective dataset write
    !---------------------------------------------------------------------
    CALL h5pcreate_f(H5P_DATASET_XFER_F, plist_id, error) 
    CALL h5pset_dxpl_mpio_f(plist_id, H5FD_MPIO_COLLECTIVE_F, error)
    !---------------------------------------------------------------------

    !---------------------------------------------------------------------
    ! Write the dataset collectively. 
    !---------------------------------------------------------------------
    CALL h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, data, dimsfi, error, &
         & file_space_id = filespace, mem_space_id = memspace, xfer_prp = plist_id)
    !---------------------------------------------------------------------
    ! Write the dataset independently. 
    !---------------------------------------------------------------------
    !CALL h5dwrite_f(dset_id, H5T_NATIVE_INTEGER, data, dimsfi,error, &
    !     & file_space_id = filespace, mem_space_id = memspace)
    !---------------------------------------------------------------------

    !---------------------------------------------------------------------
    ! Close dataspaces.
    !---------------------------------------------------------------------
    CALL h5sclose_f(filespace, error)
    CALL h5sclose_f(memspace, error)
    !---------------------------------------------------------------------

    !---------------------------------------------------------------------
    ! Close the dataset.
    !---------------------------------------------------------------------
    CALL h5dclose_f(dset_id, error)
    !---------------------------------------------------------------------

    !---------------------------------------------------------------------
    ! Close the property list.
    !---------------------------------------------------------------------
    CALL h5pclose_f(plist_id, error)
    !---------------------------------------------------------------------

  end subroutine write_hdf5_real
#endif 

  !*******************************************************************************!
  subroutine Write_field_tec(unit,itime,time,u,v,w,                   &
       & sca01,sca02,sca03,sca04,sca05,sca06,sca07,sca08,sca09,sca10, &
       & sca11,sca12,sca13,sca14,sca15,sca16,sca17,sca18,sca19,sca20)
    !*******************************************************************************!
    implicit none
    !---------------------------------------------------------------------
    ! Global variables                                            
    !---------------------------------------------------------------------
    integer, intent(in)                                             :: unit,itime
    real(8), intent(in)                                             :: time
    real(8), dimension(:,:,:), allocatable, intent(in)              :: u,v,w
    real(8), dimension(:,:,:), allocatable, intent(inout), optional ::  &
         & sca01,sca02,sca03,sca04,sca05,sca06,sca07,sca08,sca09,sca10, &
         & sca11,sca12,sca13,sca14,sca15,sca16,sca17,sca18,sca19,sca20
    !---------------------------------------------------------------------
    ! Local variables                                             
    !---------------------------------------------------------------------
    integer                                                      :: i,j,k
    integer                                                      :: lex,ley,lez
    character(500)                                               :: name
    character(500)                                               :: File
    !---------------------------------------------------------------------

    if (dim==2.or.dim==3) then 
       call Write_binary_field_tec(unit,itime,time,u,v,w,                  &
            & sca01,sca02,sca03,sca04,sca05,sca06,sca07,sca08,sca09,sca10, &
            & sca11,sca12,sca13,sca14,sca15,sca16,sca17,sca18,sca19,sca20)
       return
    end if

    !---------------------------------------------------------------------
    ! mpi 
    !---------------------------------------------------------------------
    if (nproc>1) then
       if (present(sca01)) then
          if (allocated(sca01)) call comm_mpi_sca(sca01)
       end if
       if (present(sca02)) then
          if (allocated(sca02)) call comm_mpi_sca(sca02)
       end if
       if(present(sca03)) then
          if (allocated(sca03)) call comm_mpi_sca(sca03)
       end if
       if (present(sca04)) then
          if (allocated(sca04)) call comm_mpi_sca(sca04)
       end if
       if (present(sca05)) then
          if (allocated(sca05)) call comm_mpi_sca(sca05)
       end if
       if (present(sca06)) then
          if (allocated(sca06)) call comm_mpi_sca(sca06)
       end if
       if (present(sca07)) then
          if (allocated(sca07)) call comm_mpi_sca(sca07)
       end if
       if (present(sca08)) then
          if (allocated(sca08)) call comm_mpi_sca(sca08)
       end if
       if (present(sca09)) then
          if (allocated(sca09)) call comm_mpi_sca(sca09)
       end if
       if (present(sca10)) then
          if (allocated(sca10)) call comm_mpi_sca(sca10)
       end if
       if (present(sca11)) then
          if (allocated(sca11)) call comm_mpi_sca(sca11)
       end if
       if (present(sca12)) then
          if (allocated(sca12)) call comm_mpi_sca(sca12)
       end if
       if (present(sca13)) then
          if (allocated(sca13)) call comm_mpi_sca(sca13)
       end if
       if (present(sca14)) then
          if (allocated(sca14)) call comm_mpi_sca(sca14)
       end if
       if (present(sca15)) then
          if (allocated(sca15)) call comm_mpi_sca(sca15)
       end if
       if (present(sca16)) then
          if (allocated(sca16)) call comm_mpi_sca(sca16)
       end if
       if (present(sca17)) then
          if (allocated(sca17)) call comm_mpi_sca(sca17)
       end if
       if (present(sca18)) then
          if (allocated(sca18)) call comm_mpi_sca(sca18)
       end if
       if (present(sca19)) then
          if (allocated(sca19)) call comm_mpi_sca(sca19)
       end if
       if (present(sca20)) then
          if (allocated(sca20)) call comm_mpi_sca(sca20)
       end if
    end if
    !---------------------------------------------------------------------

    !-------------------------------------------
    ! initialization of final indexes
    !-------------------------------------------
    lex=ex
    ley=ey
    lez=ez
    !-------------------------------------------


    if (nproc==1) then 
       write(name,'(i8.8)') itime
       File=trim(adjustl(cas))//"_i"//trim(adjustl(name))//".dat"
    else
       write(name,'(i8.8)') itime
       File=trim(adjustl(cas))//"_i"//trim(adjustl(name))
       write(name,'(i6.6)') rank
       File=trim(adjustl(File))//"_p"//trim(adjustl(name))//".dat"
       !-------------------------------------------
       if (lex/=gex) lex=lex+1
       if (ley/=gey) ley=ley+1
       if (lez/=gez) lez=lez+1
       !------------------------------------------
    end if

    open(unit,FILE=File,STATUS='UNKNOWN')
    REWIND unit
!!$
    if (dim==2) then
       name='VARIABLES = "X" "Y" "U" "V"'
    else
       name='VARIABLES = "X" "Y" "Z" "U" "V" "W"'
    end if
    if (present(sca01)) then
       if (allocated(sca01)) name=trim(adjustl(name))//'" sca01"'
    end if
    if (present(sca02)) then
       if (allocated(sca02)) name=trim(adjustl(name))//'" sca02'
    end if
    if(present(sca03)) then
       if (allocated(sca03)) name=trim(adjustl(name))//'" sca03"'
    end if
    if (present(sca04)) then
       if (allocated(sca04)) name=trim(adjustl(name))//'" sca04"'
    end if
    if (present(sca05)) then
       if (allocated(sca05)) name=trim(adjustl(name))//'" sca05"'
    end if
    if (present(sca06)) then
       if (allocated(sca06)) name=trim(adjustl(name))//'" sca06"'
    end if
    if (present(sca07)) then
       if (allocated(sca07)) name=trim(adjustl(name))//'" sca07"'
    end if
    if (present(sca08)) then
       if (allocated(sca08)) name=trim(adjustl(name))//'" sca08"'
    end if
    if (present(sca09)) then
       if (allocated(sca09)) name=trim(adjustl(name))//'" sca09"'
    end if
    if (present(sca10)) then
       if (allocated(sca10)) name=trim(adjustl(name))//'" sca10"'
    end if
    if (present(sca11)) then
       if (allocated(sca11)) name=trim(adjustl(name))//'" sca11"'
    end if
    if (present(sca12)) then
       if (allocated(sca12)) name=trim(adjustl(name))//'" sca12"'
    end if
    if (present(sca13)) then
       if (allocated(sca13)) name=trim(adjustl(name))//'" sca13"'
    end if
    if (present(sca14)) then
       if (allocated(sca14)) name=trim(adjustl(name))//'" sca14"'
    end if
    if (present(sca15)) then
       if (allocated(sca15)) name=trim(adjustl(name))//'" sca15"'
    end if
    if (present(sca16)) then
       if (allocated(sca16)) name=trim(adjustl(name))//'" sca16"'
    end if
    if (present(sca17)) then
       if (allocated(sca17)) name=trim(adjustl(name))//'" sca17"'
    end if
    if (present(sca18)) then
       if (allocated(sca18)) name=trim(adjustl(name))//'" sca18"'
    end if
    if (present(sca19)) then
       if (allocated(sca19)) name=trim(adjustl(name))//'" sca19"'
    end if
    if (present(sca20)) then
       if (allocated(sca20)) name=trim(adjustl(name))//'" sca20"'
    end if
    

    write(unit,'(A)')trim(adjustl(name))
    if (dim==2) then 
       write(unit,11) time,lex-sx+1,ley-sy+1,1
    elseif (dim==3) then
       write(unit,11) time,lex-sx+1,ley-sy+1,lez-sz+1
    end if

    write(unit,13) (((grid_x(i),i=sx,lex),j=sy,ley),k=sz,lez)
    write(unit,13) (((grid_y(j),i=sx,lex),j=sy,ley),k=sz,lez)
    if (dim==3) write(unit,13) (((grid_z(k),i=sx,lex),j=sy,ley),k=sz,lez)

    write(unit,13) ((((dxu2(i+1)*u(i,j,k)+dxu2(i)*u(i+1,j,k))/dx(i),i=sx,lex),j=sy,ley),k=sz,lez)
    write(unit,13) ((((dyv2(j+1)*v(i,j,k)+dyv2(j)*v(i,j+1,k))/dy(j),i=sx,lex),j=sy,ley),k=sz,lez)
    if (dim==3) write(unit,13) ((((dzw2(k+1)*w(i,j,k)+dzw2(k)*w(i,j,k+1))/dz(k),i=sx,lex),j=sy,ley),k=sz,lez)

    if (present(sca01)) then
       if(allocated(sca01)) write(unit,13) (((sca01(i,j,k),i=sx,lex),j=sy,ley),k=sz,lez)
    end if
    if (present(sca02)) then
       if(allocated(sca02)) write(unit,13) (((sca02(i,j,k),i=sx,lex),j=sy,ley),k=sz,lez)
    end if
    if (present(sca03)) then
       if(allocated(sca03)) write(unit,13) (((sca03(i,j,k),i=sx,lex),j=sy,ley),k=sz,lez)
    end if
    if (present(sca04)) then
       if(allocated(sca04)) write(unit,13) (((sca04(i,j,k),i=sx,lex),j=sy,ley),k=sz,lez)
    end if
    if (present(sca05)) then
       if(allocated(sca05)) write(unit,13) (((sca05(i,j,k),i=sx,lex),j=sy,ley),k=sz,lez)
    end if
    if (present(sca06)) then
       if(allocated(sca06)) write(unit,13) (((sca06(i,j,k),i=sx,lex),j=sy,ley),k=sz,lez)
    end if
    if (present(sca07)) then
       if(allocated(sca07)) write(unit,13) (((sca07(i,j,k),i=sx,lex),j=sy,ley),k=sz,lez)
    end if
    if (present(sca08)) then
       if(allocated(sca08)) write(unit,13) (((sca08(i,j,k),i=sx,lex),j=sy,ley),k=sz,lez)
    end if
    if (present(sca09)) then
       if(allocated(sca09)) write(unit,13) (((sca09(i,j,k),i=sx,lex),j=sy,ley),k=sz,lez)
    end if
    if (present(sca10)) then
       if(allocated(sca10)) write(unit,13) (((sca10(i,j,k),i=sx,lex),j=sy,ley),k=sz,lez)
    end if
    if (present(sca11)) then
       if(allocated(sca11)) write(unit,13) (((sca11(i,j,k),i=sx,lex),j=sy,ley),k=sz,lez)
    end if
    if (present(sca12)) then
       if(allocated(sca12)) write(unit,13) (((sca12(i,j,k),i=sx,lex),j=sy,ley),k=sz,lez)
    end if
    if (present(sca13)) then
       if(allocated(sca13)) write(unit,13) (((sca13(i,j,k),i=sx,lex),j=sy,ley),k=sz,lez)
    end if
    if (present(sca14)) then
       if(allocated(sca14)) write(unit,13) (((sca14(i,j,k),i=sx,lex),j=sy,ley),k=sz,lez)
    end if
    if (present(sca15)) then
       if(allocated(sca15)) write(unit,13) (((sca15(i,j,k),i=sx,lex),j=sy,ley),k=sz,lez)
    end if
    if (present(sca16)) then
       if(allocated(sca16)) write(unit,13) (((sca16(i,j,k),i=sx,lex),j=sy,ley),k=sz,lez)
    end if
    if (present(sca17)) then
       if(allocated(sca17)) write(unit,13) (((sca17(i,j,k),i=sx,lex),j=sy,ley),k=sz,lez)
    end if
    if (present(sca18)) then
       if(allocated(sca18)) write(unit,13) (((sca18(i,j,k),i=sx,lex),j=sy,ley),k=sz,lez)
    end if
    if (present(sca19)) then
       if(allocated(sca19)) write(unit,13) (((sca19(i,j,k),i=sx,lex),j=sy,ley),k=sz,lez)
    end if
    if (present(sca20)) then
       if(allocated(sca20)) write(unit,13) (((sca20(i,j,k),i=sx,lex),j=sy,ley),k=sz,lez)
    end if
    
    flush(unit)
    close(unit)

11  format('ZONE T="',1pe12.5,'", I=',i5,', J=',i5,', K=',i5,', DATAPACKING=BLOCK')
12  format('ZONE T="',1pe12.5,'", I=',i5,', J=',i5,', K=',i5,', DATAPACKING=BLOCK, &
         & VARSHARELIST=([1,2,3]=1), CONNECTIVITYSHAREZONE=1')
13  format(10(1pe13.5,1x))
    !
    return

  end subroutine Write_field_tec
  !*******************************************************************************!

  subroutine Write_binary_field_tec(unit,itime,time,u,v,w,            &
       & sca01,sca02,sca03,sca04,sca05,sca06,sca07,sca08,sca09,sca10, &
       & sca11,sca12,sca13,sca14,sca15,sca16,sca17,sca18,sca19,sca20)
    !*******************************************************************************!
    implicit none
    !---------------------------------------------------------------------
    ! Global variables                                            
    !---------------------------------------------------------------------
    integer, intent(in)                                             :: unit,itime
    real(8), intent(in)                                             :: time
    real(8), dimension(:,:,:), allocatable, intent(in)              :: u,v,w
    real(8), dimension(:,:,:), allocatable, intent(inout), optional ::  &
         & sca01,sca02,sca03,sca04,sca05,sca06,sca07,sca08,sca09,sca10, &
         & sca11,sca12,sca13,sca14,sca15,sca16,sca17,sca18,sca19,sca20
    !---------------------------------------------------------------------
    ! Local variables                                             
    !---------------------------------------------------------------------
    integer                                                      :: i,j,k
    integer                                                      :: lex,ley,lez
    character(500)                                               :: name
    character(500)                                               :: File
    !---------------------------------------------------------------------
    ! Local intrisic variables of the subroutine
    !---------------------------------------------------------------------
    ! define a tecplot object
    !---------------------------------------------------------------------
    type(tecplot_time_file)                  :: plt_file
    integer                                  :: num_of_variables
    integer                                  :: n,ii,jj,kk
    integer, allocatable                     :: locations(:)
    integer, allocatable                     :: type_list(:)
    integer, allocatable                     :: shared_list(:)
    real(4), dimension(:,:,:,:), allocatable :: your_datas
    character(len=100)                       :: filename
    !---------------------------------------------------------------------

    num_of_variables=0

    !---------------------------------------------------------------------
    ! mpi 
    !---------------------------------------------------------------------
    if (nproc>1) then
       if (present(sca01)) then
          if (allocated(sca01)) call comm_mpi_sca(sca01)
       end if
       if (present(sca02)) then
          if (allocated(sca02)) call comm_mpi_sca(sca02)
       end if
       if(present(sca03)) then
          if (allocated(sca03)) call comm_mpi_sca(sca03)
       end if
       if (present(sca04)) then
          if (allocated(sca04)) call comm_mpi_sca(sca04)
       end if
       if (present(sca05)) then
          if (allocated(sca05)) call comm_mpi_sca(sca05)
       end if
       if (present(sca06)) then
          if (allocated(sca06)) call comm_mpi_sca(sca06)
       end if
       if (present(sca07)) then
          if (allocated(sca07)) call comm_mpi_sca(sca07)
       end if
       if (present(sca08)) then
          if (allocated(sca08)) call comm_mpi_sca(sca08)
       end if
       if (present(sca09)) then
          if (allocated(sca09)) call comm_mpi_sca(sca09)
       end if
       if (present(sca10)) then
          if (allocated(sca10)) call comm_mpi_sca(sca10)
       end if
       if (present(sca11)) then
          if (allocated(sca11)) call comm_mpi_sca(sca11)
       end if
       if (present(sca12)) then
          if (allocated(sca12)) call comm_mpi_sca(sca12)
       end if
       if (present(sca13)) then
          if (allocated(sca13)) call comm_mpi_sca(sca13)
       end if
       if (present(sca14)) then
          if (allocated(sca14)) call comm_mpi_sca(sca14)
       end if
       if (present(sca15)) then
          if (allocated(sca15)) call comm_mpi_sca(sca15)
       end if
       if (present(sca16)) then
          if (allocated(sca16)) call comm_mpi_sca(sca16)
       end if
       if (present(sca17)) then
          if (allocated(sca17)) call comm_mpi_sca(sca17)
       end if
       if (present(sca18)) then
          if (allocated(sca18)) call comm_mpi_sca(sca18)
       end if
       if (present(sca19)) then
          if (allocated(sca19)) call comm_mpi_sca(sca19)
       end if
       if (present(sca20)) then
          if (allocated(sca20)) call comm_mpi_sca(sca20)
       end if
    end if
    !---------------------------------------------------------------------

    !-------------------------------------------
    ! initialization of final indexes
    !-------------------------------------------
    lex=ex
    ley=ey
    lez=ez
    !-------------------------------------------


    if (nproc==1) then
       write(name,'(i8.8)') itime
       File=trim(adjustl(cas))//"_i"//trim(adjustl(name))//".plt"
    else
       write(name,'(i8.8)') itime
       File=trim(adjustl(cas))//"_i"//trim(adjustl(name))
       write(name,'(i6.6)') rank
       File=trim(adjustl(File))//"_p"//trim(adjustl(name))//".plt"
       !-------------------------------------------
       if (lex/=gex) lex=lex+1
       if (ley/=gey) ley=ley+1
       if (lez/=gez) lez=lez+1
       if (dim==2) lez=sz
       !------------------------------------------
    end if

    !-------------------------------------------
    if (dim==2) then
       name='X,Y,U,V'
       num_of_variables=num_of_variables+4
    else
       name='X,Y,Z,U,V,W'
       num_of_variables=num_of_variables+6
    end if
    n=num_of_variables
    !-------------------------------------------

    if (present(sca01)) then
       if (allocated(sca01)) then
          name=trim(adjustl(name))//',sca01'
          num_of_variables=num_of_variables+1
       end if
    end if
    if (present(sca02)) then
       if (allocated(sca02)) then
          name=trim(adjustl(name))//',sca02'
          num_of_variables=num_of_variables+1
       end if
    end if
    if (present(sca03)) then
       if (allocated(sca03)) then
          name=trim(adjustl(name))//',sca03'
          num_of_variables=num_of_variables+1
       end if
    end if
    if (present(sca04)) then
       if (allocated(sca04)) then
          name=trim(adjustl(name))//',sca04'
          num_of_variables=num_of_variables+1
       end if
    end if
    if (present(sca05)) then
       if (allocated(sca05)) then
          name=trim(adjustl(name))//',sca05'
          num_of_variables=num_of_variables+1
       end if
    end if
    if (present(sca06)) then
       if (allocated(sca06)) then
          name=trim(adjustl(name))//',sca06'
          num_of_variables=num_of_variables+1
       end if
    end if
    if (present(sca07)) then
       if (allocated(sca07)) then
          name=trim(adjustl(name))//',sca07'
          num_of_variables=num_of_variables+1
       end if
    end if
    if (present(sca08)) then
       if (allocated(sca08)) then
          name=trim(adjustl(name))//',sca08'
          num_of_variables=num_of_variables+1
       end if
    end if
    if (present(sca09)) then
       if (allocated(sca09)) then
          name=trim(adjustl(name))//',sca09'
          num_of_variables=num_of_variables+1
       end if
    end if
    if (present(sca10)) then
       if (allocated(sca10)) then
          name=trim(adjustl(name))//',sca10'
          num_of_variables=num_of_variables+1
       end if
    end if
    if (present(sca11)) then
       if (allocated(sca11)) then
          name=trim(adjustl(name))//',sca11'
          num_of_variables=num_of_variables+1
       end if
    end if
    if (present(sca12)) then
       if (allocated(sca12)) then
          name=trim(adjustl(name))//',sca12'
          num_of_variables=num_of_variables+1
       end if
    end if
    if (present(sca13)) then
       if (allocated(sca13)) then
          name=trim(adjustl(name))//',sca13'
          num_of_variables=num_of_variables+1
       end if
    end if
    if (present(sca14)) then
       if (allocated(sca14)) then
          name=trim(adjustl(name))//',sca14'
          num_of_variables=num_of_variables+1
       end if
    end if
    if (present(sca15)) then
       if (allocated(sca15)) then
          name=trim(adjustl(name))//',sca15'
          num_of_variables=num_of_variables+1
       end if
    end if
    if (present(sca16)) then
       if (allocated(sca16)) then
          name=trim(adjustl(name))//',sca16'
          num_of_variables=num_of_variables+1
       end if
    end if
    if (present(sca17)) then
       if (allocated(sca17)) then
          name=trim(adjustl(name))//',sca17'
          num_of_variables=num_of_variables+1
       end if
    end if
    if (present(sca18)) then
       if (allocated(sca18)) then
          name=trim(adjustl(name))//',sca18'
          num_of_variables=num_of_variables+1
       end if
    end if
    if (present(sca19)) then
       if (allocated(sca19)) then
          name=trim(adjustl(name))//',sca19'
          num_of_variables=num_of_variables+1
       end if
    end if
    if (present(sca20)) then
       if (allocated(sca20)) then
          name=trim(adjustl(name))//',sca20'
          num_of_variables=num_of_variables+1
       end if
    end if

    if (.not.allocated(your_datas))  allocate(your_datas(sx+1:lex+1,sy+1:ley+1,sz+1:lez+1,num_of_variables))
    if (.not.allocated(locations))   allocate(locations(num_of_variables))
    if (.not.allocated(type_list))   allocate(type_list(num_of_variables))
    if (.not.allocated(shared_list)) allocate(shared_list(num_of_variables))

    locations = 0
    shared_list = -1
    type_list = 1


    if (dim==2) then
       do k=sz,lez
          do j=sy,ley
             do i=sx,lex
                kk=k+1
                jj=j+1
                ii=i+1
                your_datas(ii,jj,kk,1)=grid_x(i)
                your_datas(ii,jj,kk,3)=(dxu2(i+1)*u(i,j,k)+dxu2(i)*u(i+1,j,k))/dx(i)
             end do
          end do
       end do
       do k=sz,lez
          do j=sy,ley
             do i=sx,lex
                kk=k+1
                jj=j+1
                ii=i+1
                your_datas(ii,jj,kk,2)=grid_y(j)
                your_datas(ii,jj,kk,4)=(dyv2(j+1)*v(i,j,k)+dyv2(j)*v(i,j+1,k))/dy(j)
             end do
          end do
       end do
    else
       do k=sz,lez
          do j=sy,ley
             do i=sx,lex
                kk=k+1
                jj=j+1
                ii=i+1
                your_datas(ii,jj,kk,1)=grid_x(i)
                your_datas(ii,jj,kk,4)=(dxu2(i+1)*u(i,j,k)+dxu2(i)*u(i+1,j,k))/dx(i)
             end do
          end do
       end do
       do k=sz,lez
          do j=sy,ley
             do i=sx,lex
                kk=k+1
                jj=j+1
                ii=i+1
                your_datas(ii,jj,kk,2)=grid_y(j)
                your_datas(ii,jj,kk,5)=(dyv2(j+1)*v(i,j,k)+dyv2(j)*v(i,j+1,k))/dy(j)
             end do
          end do
       end do
       do k=sz,lez
          do j=sy,ley
             do i=sx,lex
                kk=k+1
                jj=j+1
                ii=i+1
                your_datas(ii,jj,kk,3)=grid_z(k)
                your_datas(ii,jj,kk,6)=(dzw2(k+1)*w(i,j,k)+dzw2(k)*w(i,j,k+1))/dz(k)
             end do
          end do
       end do
    end if

    if (present(sca01)) then
       if (allocated(sca01)) then
          n=n+1
          do k=sz,lez
             do j=sy,ley
                do i=sx,lex
                   kk=k+1
                   jj=j+1
                   ii=i+1
                   your_datas(ii,jj,kk,n) = sca01(i,j,k)
                end do
             end do
          end do
       end if
    end if
    if (present(sca02)) then
       if (allocated(sca02)) then
          n=n+1
          do k=sz,lez
             do j=sy,ley
                do i=sx,lex
                   kk=k+1
                   jj=j+1
                   ii=i+1
                   your_datas(ii,jj,kk,n) = sca02(i,j,k)
                end do
             end do
          end do
       end if
    end if
    if (present(sca03)) then
       if (allocated(sca03)) then
          n=n+1
          do k=sz,lez
             do j=sy,ley
                do i=sx,lex
                   kk=k+1
                   jj=j+1
                   ii=i+1
                   your_datas(ii,jj,kk,n) = sca03(i,j,k)
                end do
             end do
          end do
       end if
    end if
    if (present(sca04)) then
       if (allocated(sca04)) then
          n=n+1
          do k=sz,lez
             do j=sy,ley
                do i=sx,lex
                   kk=k+1
                   jj=j+1
                   ii=i+1
                   your_datas(ii,jj,kk,n) = sca04(i,j,k)
                end do
             end do
          end do
       end if
    end if
    if (present(sca05)) then
       if (allocated(sca05)) then
          n=n+1
          do k=sz,lez
             do j=sy,ley
                do i=sx,lex
                   kk=k+1
                   jj=j+1
                   ii=i+1
                   your_datas(ii,jj,kk,n) = sca05(i,j,k)
                end do
             end do
          end do
       end if
    end if
    if (present(sca06)) then
       if (allocated(sca06)) then
          n=n+1
          do k=sz,lez
             do j=sy,ley
                do i=sx,lex
                   kk=k+1
                   jj=j+1
                   ii=i+1
                   your_datas(ii,jj,kk,n) = sca06(i,j,k)
                end do
             end do
          end do
       end if
    end if
    if (present(sca07)) then
       if (allocated(sca07)) then
          n=n+1
          do k=sz,lez
             do j=sy,ley
                do i=sx,lex
                   kk=k+1
                   jj=j+1
                   ii=i+1
                   your_datas(ii,jj,kk,n) = sca07(i,j,k)
                end do
             end do
          end do
       end if
    end if
    if (present(sca08)) then
       if (allocated(sca08)) then
          n=n+1
          do k=sz,lez
             do j=sy,ley
                do i=sx,lex
                   kk=k+1
                   jj=j+1
                   ii=i+1
                   your_datas(ii,jj,kk,n) = sca08(i,j,k)
                end do
             end do
          end do
       end if
    end if
    if (present(sca09)) then
       if (allocated(sca09)) then
          n=n+1
          do k=sz,lez
             do j=sy,ley
                do i=sx,lex
                   kk=k+1
                   jj=j+1
                   ii=i+1
                   your_datas(ii,jj,kk,n) = sca09(i,j,k)
                end do
             end do
          end do
       end if
    end if
    if (present(sca10)) then
       if (allocated(sca10)) then
          n=n+1
          do k=sz,lez
             do j=sy,ley
                do i=sx,lex
                   kk=k+1
                   jj=j+1
                   ii=i+1
                   your_datas(ii,jj,kk,n) = sca10(i,j,k)
                end do
             end do
          end do
       end if
    end if
    if (present(sca11)) then
       if (allocated(sca11)) then
          n=n+1
          do k=sz,lez
             do j=sy,ley
                do i=sx,lex
                   kk=k+1
                   jj=j+1
                   ii=i+1
                   your_datas(ii,jj,kk,n) = sca11(i,j,k)
                end do
             end do
          end do
       end if
    end if
    if (present(sca12)) then
       if (allocated(sca12)) then
          n=n+1
          do k=sz,lez
             do j=sy,ley
                do i=sx,lex
                   kk=k+1
                   jj=j+1
                   ii=i+1
                   your_datas(ii,jj,kk,n) = sca12(i,j,k)
                end do
             end do
          end do
       end if
    end if
    if (present(sca13)) then
       if (allocated(sca13)) then
          n=n+1
          do k=sz,lez
             do j=sy,ley
                do i=sx,lex
                   kk=k+1
                   jj=j+1
                   ii=i+1
                   your_datas(ii,jj,kk,n) = sca13(i,j,k)
                end do
             end do
          end do
       end if
    end if
    if (present(sca14)) then
       if (allocated(sca14)) then
          n=n+1
          do k=sz,lez
             do j=sy,ley
                do i=sx,lex
                   kk=k+1
                   jj=j+1
                   ii=i+1
                   your_datas(ii,jj,kk,n) = sca14(i,j,k)
                end do
             end do
          end do
       end if
    end if
    if (present(sca15)) then
       if (allocated(sca15)) then
          n=n+1
          do k=sz,lez
             do j=sy,ley
                do i=sx,lex
                   kk=k+1
                   jj=j+1
                   ii=i+1
                   your_datas(ii,jj,kk,n) = sca15(i,j,k)
                end do
             end do
          end do
       end if
    end if
    if (present(sca16)) then
       if (allocated(sca16)) then
          n=n+1
          do k=sz,lez
             do j=sy,ley
                do i=sx,lex
                   kk=k+1
                   jj=j+1
                   ii=i+1
                   your_datas(ii,jj,kk,n) = sca16(i,j,k)
                end do
             end do
          end do
       end if
    end if
    if (present(sca17)) then
       if (allocated(sca17)) then
          n=n+1
          do k=sz,lez
             do j=sy,ley
                do i=sx,lex
                   kk=k+1
                   jj=j+1
                   ii=i+1
                   your_datas(ii,jj,kk,n) = sca17(i,j,k)
                end do
             end do
          end do
       end if
    end if
    if (present(sca18)) then
       if (allocated(sca18)) then
          n=n+1
          do k=sz,lez
             do j=sy,ley
                do i=sx,lex
                   kk=k+1
                   jj=j+1
                   ii=i+1
                   your_datas(ii,jj,kk,n) = sca18(i,j,k)
                end do
             end do
          end do
       end if
    end if
    if (present(sca19)) then
       if (allocated(sca19)) then
          n=n+1
          do k=sz,lez
             do j=sy,ley
                do i=sx,lex
                   kk=k+1
                   jj=j+1
                   ii=i+1
                   your_datas(ii,jj,kk,n) = sca19(i,j,k)
                end do
             end do
          end do
       end if
    end if
    if (present(sca20)) then
       if (allocated(sca20)) then
          n=n+1
          do k=sz,lez
             do j=sy,ley
                do i=sx,lex
                   kk=k+1
                   jj=j+1
                   ii=i+1
                   your_datas(ii,jj,kk,n) = sca20(i,j,k)
                end do
             end do
          end do
       end if
    end if

    ! 'x,y,z,u,v,w' is a string contains names of variables, must be divided by ','
    call plt_file%init(File,lex-sx+1,ley-sy+1,lez-sz+1,'Tecplot File Title',trim(adjustl(name)))

    ! for each zone, call the two subroutines
    ! physics_time can be any value, it will only be used when there are more than 1 zone in a file.

    call plt_file%write_zone_header('zone name', real(time,4), 0, locations) 
    ! your_datas(:,:,:,1:3) =  x,y,z ! coordinates(Variable assignment is omitted in this example)
    ! your_datas(:,:,:,4:6) =  u,v,w ! datas (Variable assignment is omitted in this example)
    ! ALL datas are stored in sequence like (((x(ix,iy,iz),ix=1,nx),iy=1,ny),iz=1,nz)
    call plt_file%write_zone_data(type_list, shared_list, your_datas)

    ! before exit, you must call complete subroutine
    call plt_file%complete

    return

  end subroutine Write_binary_field_tec
  !*******************************************************************************!

  subroutine Write_field_ensight_gold(unit,itime,time,u,v,w,          &
       & sca01,sca02,sca03,sca04,sca05,sca06,sca07,sca08,sca09,sca10, &
       & sca11,sca12,sca13,sca14,sca15,sca16,sca17,sca18,sca19,sca20)
    !*******************************************************************************!
    implicit none
    !---------------------------------------------------------------------
    ! Global variables                                            
    !---------------------------------------------------------------------
    integer, intent(in)                                             :: unit,itime
    real(8), intent(in)                                             :: time
    real(8), dimension(:,:,:), allocatable, intent(in)              :: u,v,w
    real(8), dimension(:,:,:), allocatable, intent(inout), optional ::  &
         & sca01,sca02,sca03,sca04,sca05,sca06,sca07,sca08,sca09,sca10, &
         & sca11,sca12,sca13,sca14,sca15,sca16,sca17,sca18,sca19,sca20
    !---------------------------------------------------------------------
    ! Local variables                                             
    !---------------------------------------------------------------------
    integer                                                         :: i,j,k
    integer                                                         :: nbVar,cVar
    real(8), dimension(:,:,:,:), allocatable                        :: var
    integer, dimension(:), allocatable                              :: varType
    character(len=80), dimension(:), allocatable                    :: varName
    character(len=80)                                               :: caseName
    character(len=80)                                               :: iterName
    character(len=80)                                               :: varIterName
    !---------------------------------------------------------------------
    
    !---------------------------------------------------------------------
    ! Test ensight gold format 
    !---------------------------------------------------------------------
    
    !---------------------------------------------------------------------
    ! nbVar count
    !---------------------------------------------------------------------
    nbVar=1
    if (present(sca01)) then
       if (allocated(sca01)) nbVar=nbVar+1
    end if
    if (present(sca02)) then
       if (allocated(sca02)) nbVar=nbVar+1
    end if
    if (present(sca03)) then
       if (allocated(sca03)) nbVar=nbVar+1
    end if
    if (present(sca04)) then
       if (allocated(sca04)) nbVar=nbVar+1
    end if
    if (present(sca05)) then
       if (allocated(sca05)) nbVar=nbVar+1
    end if
    if (present(sca06)) then
       if (allocated(sca06)) nbVar=nbVar+1
    end if
    if (present(sca07)) then
       if (allocated(sca07)) nbVar=nbVar+1
    end if
    if (present(sca08)) then
       if (allocated(sca08)) nbVar=nbVar+1
    end if
    if (present(sca09)) then
       if (allocated(sca09)) nbVar=nbVar+1
    end if
    if (present(sca10)) then
       if (allocated(sca10)) nbVar=nbVar+1
    end if
    if (present(sca11)) then
       if (allocated(sca11)) nbVar=nbVar+1
    end if
    if (present(sca12)) then
       if (allocated(sca12)) nbVar=nbVar+1
    end if
    if (present(sca13)) then
       if (allocated(sca13)) nbVar=nbVar+1
    end if
    if (present(sca14)) then
       if (allocated(sca14)) nbVar=nbVar+1
    end if
    if (present(sca15)) then
       if (allocated(sca15)) nbVar=nbVar+1
    end if
    if (present(sca16)) then
       if (allocated(sca16)) nbVar=nbVar+1
    end if
    if (present(sca17)) then
       if (allocated(sca17)) nbVar=nbVar+1
    end if
    if (present(sca18)) then
       if (allocated(sca18)) nbVar=nbVar+1
    end if
    if (present(sca19)) then
       if (allocated(sca19)) nbVar=nbVar+1
    end if
    if (present(sca20)) then
       if (allocated(sca20)) nbVar=nbVar+1
    end if
    !---------------------------------------------------------------------

    allocate(varName(nbVar))
    allocate(varType(nbVar))
    write(iterName,'(i10.10)') itime
    cVar=0

    !---------------------------------------------------------------------
    ! velocity field
    !---------------------------------------------------------------------
    cVar=cVar+1
    allocate(var(3,sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz)); var=0
    write(varName(cVar),'(a)') 'velocity'
    varType(cVar)=3

    do i=sx-2,ex+2 ! +-2 for mpi periodicity
       var(1,i,:,:)=(dxu2(i+1)*u(i,:,:)+dxu2(i)*u(i+1,:,:))/dx(i)
    end do

    do j=sy-2,ey+2
       var(2,:,j,:)=(dyv2(j+1)*v(:,j,:)+dyv2(j)*v(:,j+1,:))/dy(j)
    end do

    if (dim==3) then 
       do k=sz-2,ez+2
          var(3,:,:,k)=(dzw2(k+1)*w(:,:,k)+dzw2(k)*w(:,:,k+1))/dz(k)
       end do
    end if

    varIterName=trim(adjustl(varName(cVar)))//trim(adjustl(iterName))
    call WriteEnsightVar(varType(cVar),var,varIterName)
    deallocate(var) 
    !---------------------------------------------------------------------
    ! pression
    !---------------------------------------------------------------------
    if (present(sca01)) then
       if (allocated(sca01)) then
          cVar=cVar+1
          allocate(var(1,sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
          write(varName(cVar),'(a)') 'pressure'
          varType(cVar)=1
          var(1,:,:,:)=sca01
          varIterName=trim(adjustl(varName(cVar)))//trim(adjustl(iterName))
          call WriteEnsightVar(varType(cVar),var,varIterName)
          deallocate(var) 
       end if
    end if
    !---------------------------------------------------------------------
    ! divergence
    !---------------------------------------------------------------------
    if (present(sca02)) then
       if (allocated(sca02)) then
          cVar=cVar+1
          allocate(var(1,sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
          write(varName(cVar),'(a)') 'divergence'
          varType(cVar)=1
          var(1,:,:,:)=sca02
          varIterName=trim(adjustl(varName(cVar)))//trim(adjustl(iterName))
          call WriteEnsightVar(varType(cVar),var,varIterName)
          deallocate(var) 
       end if
    end if
    !---------------------------------------------------------------------
    ! temperature
    !---------------------------------------------------------------------
    if (present(sca03)) then
       if (allocated(sca03)) then
          cVar=cVar+1
          allocate(var(1,sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
          write(varName(cVar),'(a)') 'temperature'
          varType(cVar)=1
          var(1,:,:,:)=sca03
          varIterName=trim(adjustl(varName(cVar)))//trim(adjustl(iterName))
          call WriteEnsightVar(varType(cVar),var,varIterName)
          deallocate(var) 
       end if
    end if
    !---------------------------------------------------------------------
    ! concentration
    !---------------------------------------------------------------------
    if (present(sca04)) then
       if (allocated(sca04)) then
          cVar=cVar+1
          allocate(var(1,sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
          write(varName(cVar),'(a)') 'concentration'
          varType(cVar)=1
          var(1,:,:,:)=sca04
          varIterName=trim(adjustl(varName(cVar)))//trim(adjustl(iterName))
          call WriteEnsightVar(varType(cVar),var,varIterName)
          deallocate(var) 
       end if
    end if
    !---------------------------------------------------------------------
    ! phase
    !---------------------------------------------------------------------
    if (present(sca05)) then
       if (allocated(sca05)) then
          cVar=cVar+1
          allocate(var(1,sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
          write(varName(cVar),'(a)') 'phase'
          varType(cVar)=1
          var(1,:,:,:)=sca05
          varIterName=trim(adjustl(varName(cVar)))//trim(adjustl(iterName))
          call WriteEnsightVar(varType(cVar),var,varIterName)
          deallocate(var) 
       end if
    end if
    !---------------------------------------------------------------------
    ! sca06
    !---------------------------------------------------------------------
    if (present(sca06)) then
       if (allocated(sca06)) then
          cVar=cVar+1
          allocate(var(1,sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
          write(varName(cVar),'(a)') 'object'
          varType(cVar)=1
          var(1,:,:,:)=sca06
          varIterName=trim(adjustl(varName(cVar)))//trim(adjustl(iterName))
          call WriteEnsightVar(varType(cVar),var,varIterName)
          deallocate(var) 
       end if
    end if
    !---------------------------------------------------------------------
    ! sca07
    !---------------------------------------------------------------------
    if (present(sca07)) then
       if (allocated(sca07)) then
          cVar=cVar+1
          allocate(var(1,sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
          write(varName(cVar),'(a)') 'sca07'
          varType(cVar)=1
          var(1,:,:,:)=sca07
          varIterName=trim(adjustl(varName(cVar)))//trim(adjustl(iterName))
          call WriteEnsightVar(varType(cVar),var,varIterName)
          deallocate(var) 
       end if
    end if
    !---------------------------------------------------------------------
    ! sca08
    !---------------------------------------------------------------------
    if (present(sca08)) then
       if (allocated(sca08)) then
          cVar=cVar+1
          allocate(var(1,sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
          write(varName(cVar),'(a)') 'sca08'
          varType(cVar)=1
          var(1,:,:,:)=sca08
          varIterName=trim(adjustl(varName(cVar)))//trim(adjustl(iterName))
          call WriteEnsightVar(varType(cVar),var,varIterName)
          deallocate(var) 
       end if
    end if
    !---------------------------------------------------------------------
    ! sca09
    !---------------------------------------------------------------------
    if (present(sca09)) then
       if (allocated(sca09)) then
          cVar=cVar+1
          allocate(var(1,sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
          write(varName(cVar),'(a)') 'sca09'
          varType(cVar)=1
          var(1,:,:,:)=sca09
          varIterName=trim(adjustl(varName(cVar)))//trim(adjustl(iterName))
          call WriteEnsightVar(varType(cVar),var,varIterName)
          deallocate(var) 
       end if
    end if
    !---------------------------------------------------------------------
    ! sca10
    !---------------------------------------------------------------------
    if (present(sca10)) then
       if (allocated(sca10)) then
          cVar=cVar+1
          allocate(var(1,sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
          write(varName(cVar),'(a)') 'sca10'
          varType(cVar)=1
          var(1,:,:,:)=sca10
          varIterName=trim(adjustl(varName(cVar)))//trim(adjustl(iterName))
          call WriteEnsightVar(varType(cVar),var,varIterName)
          deallocate(var) 
       end if
    end if
    !---------------------------------------------------------------------
    ! sca11
    !---------------------------------------------------------------------
    if (present(sca11)) then
       if (allocated(sca11)) then
          cVar=cVar+1
          allocate(var(1,sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
          write(varName(cVar),'(a)') 'sca11'
          varType(cVar)=1
          var(1,:,:,:)=sca11
          varIterName=trim(adjustl(varName(cVar)))//trim(adjustl(iterName))
          call WriteEnsightVar(varType(cVar),var,varIterName)
          deallocate(var) 
       end if
    end if
    !---------------------------------------------------------------------
    ! sca12
    !---------------------------------------------------------------------
    if (present(sca12)) then
       if (allocated(sca12)) then
          cVar=cVar+1
          allocate(var(1,sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
          write(varName(cVar),'(a)') 'sca12'
          varType(cVar)=1
          var(1,:,:,:)=sca12
          varIterName=trim(adjustl(varName(cVar)))//trim(adjustl(iterName))
          call WriteEnsightVar(varType(cVar),var,varIterName)
          deallocate(var) 
       end if
    end if
    !---------------------------------------------------------------------
    ! sca13
    !---------------------------------------------------------------------
    if (present(sca13)) then
       if (allocated(sca13)) then
          cVar=cVar+1
          allocate(var(1,sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
          write(varName(cVar),'(a)') 'sca13'
          varType(cVar)=1
          var(1,:,:,:)=sca13
          varIterName=trim(adjustl(varName(cVar)))//trim(adjustl(iterName))
          call WriteEnsightVar(varType(cVar),var,varIterName)
          deallocate(var) 
       end if
    end if
    !---------------------------------------------------------------------
    ! sca14
    !---------------------------------------------------------------------
    if (present(sca14)) then
       if (allocated(sca14)) then
          cVar=cVar+1
          allocate(var(1,sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
          write(varName(cVar),'(a)') 'sca14'
          varType(cVar)=1
          var(1,:,:,:)=sca14
          varIterName=trim(adjustl(varName(cVar)))//trim(adjustl(iterName))
          call WriteEnsightVar(varType(cVar),var,varIterName)
          deallocate(var) 
       end if
    end if
    !---------------------------------------------------------------------
    ! sca15
    !---------------------------------------------------------------------
    if (present(sca15)) then
       if (allocated(sca15)) then
          cVar=cVar+1
          allocate(var(1,sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
          write(varName(cVar),'(a)') 'sca15'
          varType(cVar)=1
          var(1,:,:,:)=sca15
          varIterName=trim(adjustl(varName(cVar)))//trim(adjustl(iterName))
          call WriteEnsightVar(varType(cVar),var,varIterName)
          deallocate(var) 
       end if
    end if
    !---------------------------------------------------------------------

    !---------------------------------------------------------------------
    ! mesh and case
    !---------------------------------------------------------------------
    if (rank==0) then
       caseName="Ensight"//trim(adjustl(cas))
       CALL WriteEnsightCase(cVar,varName,caseName,VarType,itime,time)
       call WriteRectEnsightGeo(caseName)
    end if
    !---------------------------------------------------------------------
    
  end subroutine Write_field_ensight_gold
  
  !*******************************************************************************!
  subroutine Write_field_vtk(unit,itime,time,u,v,w,                   &
       & sca01,sca02,sca03,sca04,sca05,sca06,sca07,sca08,sca09,sca10, &
       & sca11,sca12,sca13,sca14,sca15,sca16,sca17,sca18,sca19,sca20)
    !*******************************************************************************!
    implicit none
    !---------------------------------------------------------------------
    ! Global variables                                            
    !---------------------------------------------------------------------
    integer, intent(in)                                             :: unit,itime
    real(8), intent(in)                                             :: time
    real(8), dimension(:,:,:), allocatable, intent(in)              :: u,v,w
    real(8), dimension(:,:,:), allocatable, intent(inout), optional ::  &
         & sca01,sca02,sca03,sca04,sca05,sca06,sca07,sca08,sca09,sca10, &
         & sca11,sca12,sca13,sca14,sca15,sca16,sca17,sca18,sca19,sca20
    !---------------------------------------------------------------------
    ! Local variables                                             
    !---------------------------------------------------------------------
    INTEGER                                   :: i,j,k
    INTEGER                                   :: lex,ley,lez
    INTEGER                                   :: dim_XYZ,dim_X,dim_Y,dim_Z
    CHARACTER(len=1), PARAMETER               :: eol=ACHAR(10)
    CHARACTER(len=500)                        :: field_name
    CHARACTER(len=500)                        :: header_main
    CHARACTER(len=200)                        :: header_struc
    CHARACTER(len=200)                        :: header_points
    CHARACTER(len=200)                        :: header_cells
    CHARACTER(len=200)                        :: header_cell_types
    CHARACTER(len=200)                        :: header_data
    CHARACTER(len=200)                        :: main_header_data
    CHARACTER(len=200)                        :: header_dim
    CHARACTER(len=200)                        :: header_table
    CHARACTER(len=12)                         :: c_points,x_points,y_points,z_points
    character(100)                            :: name
    character (LEN=100)                       :: File
    !---------------------------------------------------------------------

    !---------------------------------------------------------------------
    ! mpi 
    !---------------------------------------------------------------------
    if (nproc>1) then
       if (present(sca01)) then
          if (allocated(sca01)) call comm_mpi_sca(sca01)
       end if
       if (present(sca02)) then
          if (allocated(sca02)) call comm_mpi_sca(sca02)
       end if
       if(present(sca03)) then
          if (allocated(sca03)) call comm_mpi_sca(sca03)
       end if
       if (present(sca04)) then
          if (allocated(sca04)) call comm_mpi_sca(sca04)
       end if
       if (present(sca05)) then
          if (allocated(sca05)) call comm_mpi_sca(sca05)
       end if
       if (present(sca06)) then
          if (allocated(sca06)) call comm_mpi_sca(sca06)
       end if
       if (present(sca07)) then
          if (allocated(sca07)) call comm_mpi_sca(sca07)
       end if
       if (present(sca08)) then
          if (allocated(sca08)) call comm_mpi_sca(sca08)
       end if
       if (present(sca09)) then
          if (allocated(sca09)) call comm_mpi_sca(sca09)
       end if
       if (present(sca10)) then
          if (allocated(sca10)) call comm_mpi_sca(sca10)
       end if
       if (present(sca11)) then
          if (allocated(sca11)) call comm_mpi_sca(sca11)
       end if
       if (present(sca12)) then
          if (allocated(sca12)) call comm_mpi_sca(sca12)
       end if
       if (present(sca13)) then
          if (allocated(sca13)) call comm_mpi_sca(sca13)
       end if
       if (present(sca14)) then
          if (allocated(sca14)) call comm_mpi_sca(sca14)
       end if
       if (present(sca15)) then
          if (allocated(sca15)) call comm_mpi_sca(sca15)
       end if
       if (present(sca16)) then
          if (allocated(sca16)) call comm_mpi_sca(sca16)
       end if
       if (present(sca17)) then
          if (allocated(sca17)) call comm_mpi_sca(sca17)
       end if
       if (present(sca18)) then
          if (allocated(sca18)) call comm_mpi_sca(sca18)
       end if
       if (present(sca19)) then
          if (allocated(sca19)) call comm_mpi_sca(sca19)
       end if
       if (present(sca20)) then
          if (allocated(sca20)) call comm_mpi_sca(sca20)
       end if
    end if
    !---------------------------------------------------------------------

    !-------------------------------------------
    ! initialization of final indexes
    !-------------------------------------------
    lex=ex
    ley=ey
    lez=ez
    !-------------------------------------------

    if (nproc==1) then 
       write(name,'(i8.8)') itime
       File=trim(adjustl(cas))//"_i"//trim(adjustl(name))//".vtk"
    else
!!$       write(name,'(i8.8)') itime
!!$       File=trim(adjustl(cas))//"_i"//trim(adjustl(name))
!!$       write(name,'(i6.6)') rank
!!$       File=trim(adjustl(File))//"_p"//trim(adjustl(name))//".vtk"
       write(name,'(i6.6)') rank
       File=trim(adjustl(cas))//"_p"//trim(adjustl(name))
       write(name,'(i8.8)') itime
       File=trim(adjustl(File))//"_i"//trim(adjustl(name))//".vtk"
       !-------------------------------------------
       if (lex/=gex) lex=lex+1
       if (ley/=gey) ley=ley+1
       if (lez/=gez) lez=lez+1
       !------------------------------------------
    end if

    open(unit,FILE=File,         &
         & STATUS='UNKNOWN',     &
         & form='unformatted',   &
         & access='stream',      &
         & convert='big_endian')
    REWIND unit


    dim_X=lex-sx+1
    dim_Y=ley-sy+1
    if (dim==2) then
       dim_Z=1
    elseif (dim==3) then
       dim_Z=lez-sz+1
    end if
    dim_XYZ=dim_X*dim_Y*dim_Z 

    WRITE(c_points,'(i10)') dim_XYZ
    WRITE(x_points,'(i10)') dim_X
    WRITE(y_points,'(i10)') dim_Y
    WRITE(z_points,'(i10)') dim_Z

    header_main='# vtk DataFile Version 2.0'//eol//&
         "TEST"//eol//&
         "BINARY"//eol

    header_struc='DATASET STRUCTURED_GRID'//eol
    header_dim='DIMENSIONS '//TRIM(x_points)//TRIM(y_points)//TRIM(z_points)//eol
    header_points='POINTS '//TRIM(c_points)//' double'//eol
    header_table='LOOKUP_TABLE default'//eol

    WRITE(unit) TRIM(header_main)
    WRITE(unit) TRIM(header_struc)
    WRITE(unit) TRIM(header_dim)
    WRITE(unit) TRIM(header_points)
    ! ----------------MESH ---------------------
    DO k=sz,lez
       DO j=sy,ley
          DO i=sx,lex
             if (dim==2) then
                WRITE(unit) grid_x(i),grid_y(j),zero
             else 
                WRITE(unit) grid_x(i),grid_y(j),grid_z(k)
             end if
          ENDDO
       ENDDO
    ENDDO
    WRITE(unit) eol
    ! ---------------------- HEADER --------------------------
    main_header_data='POINT_DATA '//TRIM(c_points)//eol
    WRITE(unit) TRIM(main_header_data)
    ! --------------------  VECTOR FIELD  --------------------------
    field_name='V'
    header_data='VECTORS '//TRIM(field_name)//' double'//eol
    WRITE(unit) TRIM(header_data)
    DO k=sz,lez
       DO j=sy,ley
          DO i=sx,lex
             if (dim==2) then
                WRITE(unit) (dxu2(i+1)*u(i,j,k)+dxu2(i)*u(i+1,j,k))/dx(i),&
                     & (dyv2(j+1)*v(i,j,k)+dyv2(j)*v(i,j+1,k))/dy(j),     &
                     & zero
             else if (dim==3) then 
                WRITE(unit) (dxu2(i+1)*u(i,j,k)+dxu2(i)*u(i+1,j,k))/dx(i),&
                     & (dyv2(j+1)*v(i,j,k)+dyv2(j)*v(i,j+1,k))/dy(j),     &
                     & (dzw2(k+1)*w(i,j,k)+dzw2(k)*w(i,j,k+1))/dz(k)
             end if
          ENDDO
       ENDDO
    ENDDO
    WRITE(unit) eol
    ! --------------------  SCALAR FIELDS  --------------------------
    if (present(sca01)) then
       if (allocated(sca01)) then
          WRITE(z_points,'(i10)') 1
          field_name='sca01'
          header_data='SCALARS '//TRIM(field_name)//' double '//TRIM(adjustl(z_points))//eol
          WRITE(unit) TRIM(header_data)
          WRITE(unit) TRIM(header_table)
          DO k=sz,lez
             DO j=sy,ley
                DO i=sx,lex
                   WRITE(unit) sca01(i,j,k)
                ENDDO
             ENDDO
          ENDDO
          WRITE(unit) eol
       end if
    end if
    ! --------------------  SCALAR FIELDS  --------------------------
    if (present(sca02)) then
       if (allocated(sca02)) then
          WRITE(z_points,'(i10)') 1
          field_name='sca02'
          header_data='SCALARS '//TRIM(field_name)//' double '//TRIM(adjustl(z_points))//eol
          WRITE(unit) TRIM(header_data)
          WRITE(unit) TRIM(header_table)
          DO k=sz,lez
             DO j=sy,ley
                DO i=sx,lex
                   WRITE(unit) sca02(i,j,k)
                ENDDO
             ENDDO
          ENDDO
          WRITE(unit) eol
       end if
    end if
    ! --------------------  SCALAR FIELDS  --------------------------
    if (present(sca03)) then
       if (allocated(sca03)) then
          WRITE(z_points,'(i10)') 1
          field_name='sca03'
          header_data='SCALARS '//TRIM(field_name)//' double '//TRIM(adjustl(z_points))//eol
          WRITE(unit) TRIM(header_data)
          WRITE(unit) TRIM(header_table)
          DO k=sz,lez
             DO j=sy,ley
                DO i=sx,lex
                   WRITE(unit) sca03(i,j,k)
                ENDDO
             ENDDO
          ENDDO
          WRITE(unit) eol
       end if
    end if
    ! --------------------  SCALAR FIELDS  --------------------------
    if (present(sca04)) then
       if (allocated(sca04)) then
          WRITE(z_points,'(i10)') 1
          field_name='sca04'
          header_data='SCALARS '//TRIM(field_name)//' double '//TRIM(adjustl(z_points))//eol
          WRITE(unit) TRIM(header_data)
          WRITE(unit) TRIM(header_table)
          DO k=sz,lez
             DO j=sy,ley
                DO i=sx,lex
                   WRITE(unit) sca04(i,j,k)
                ENDDO
             ENDDO
          ENDDO
          WRITE(unit) eol
       end if
    end if
    ! --------------------  SCALAR FIELDS  --------------------------
    if (present(sca05)) then
       if (allocated(sca05)) then
          WRITE(z_points,'(i10)') 1
          field_name='sca05'
          header_data='SCALARS '//TRIM(field_name)//' double '//TRIM(adjustl(z_points))//eol
          WRITE(unit) TRIM(header_data)
          WRITE(unit) TRIM(header_table)
          DO k=sz,lez
             DO j=sy,ley
                DO i=sx,lex
                   WRITE(unit) sca05(i,j,k)
                ENDDO
             ENDDO
          ENDDO
          WRITE(unit) eol
       end if
    end if
    ! --------------------  SCALAR FIELDS  --------------------------
    if (present(sca06)) then
       if (allocated(sca06)) then
          WRITE(z_points,'(i10)') 1
          field_name='sca06'
          header_data='SCALARS '//TRIM(field_name)//' double '//TRIM(adjustl(z_points))//eol
          WRITE(unit) TRIM(header_data)
          WRITE(unit) TRIM(header_table)
          DO k=sz,lez
             DO j=sy,ley
                DO i=sx,lex
                   WRITE(unit) sca06(i,j,k)
                ENDDO
             ENDDO
          ENDDO
          WRITE(unit) eol
       end if
    end if
    ! --------------------  SCALAR FIELDS  --------------------------
    if (present(sca07)) then
       if (allocated(sca07)) then
          WRITE(z_points,'(i10)') 1
          field_name='sca07'
          header_data='SCALARS '//TRIM(field_name)//' double '//TRIM(adjustl(z_points))//eol
          WRITE(unit) TRIM(header_data)
          WRITE(unit) TRIM(header_table)
          DO k=sz,lez
             DO j=sy,ley
                DO i=sx,lex
                   WRITE(unit) sca07(i,j,k)
                ENDDO
             ENDDO
          ENDDO
          WRITE(unit) eol
       end if
    end if
    ! --------------------  SCALAR FIELDS  --------------------------
    if (present(sca08)) then
       if (allocated(sca08)) then
          WRITE(z_points,'(i10)') 1
          field_name='sca08'
          header_data='SCALARS '//TRIM(field_name)//' double '//TRIM(adjustl(z_points))//eol
          WRITE(unit) TRIM(header_data)
          WRITE(unit) TRIM(header_table)
          DO k=sz,lez
             DO j=sy,ley
                DO i=sx,lex
                   WRITE(unit) sca08(i,j,k)
                ENDDO
             ENDDO
          ENDDO
          WRITE(unit) eol
       end if
    end if
    ! --------------------  SCALAR FIELDS  --------------------------
    if (present(sca09)) then
       if (allocated(sca09)) then
          WRITE(z_points,'(i10)') 1
          field_name='sca09'
          header_data='SCALARS '//TRIM(field_name)//' double '//TRIM(adjustl(z_points))//eol
          WRITE(unit) TRIM(header_data)
          WRITE(unit) TRIM(header_table)
          DO k=sz,lez
             DO j=sy,ley
                DO i=sx,lex
                   WRITE(unit) sca09(i,j,k)
                ENDDO
             ENDDO
          ENDDO
          WRITE(unit) eol
       end if
    end if
    ! --------------------  SCALAR FIELDS  --------------------------
    if (present(sca10)) then
       if (allocated(sca10)) then
          WRITE(z_points,'(i10)') 1
          field_name='sca10'
          header_data='SCALARS '//TRIM(field_name)//' double '//TRIM(adjustl(z_points))//eol
          WRITE(unit) TRIM(header_data)
          WRITE(unit) TRIM(header_table)
          DO k=sz,lez
             DO j=sy,ley
                DO i=sx,lex
                   WRITE(unit) sca10(i,j,k)
                ENDDO
             ENDDO
          ENDDO
          WRITE(unit) eol
       end if
    end if
    ! --------------------  SCALAR FIELDS  --------------------------
    if (present(sca11)) then
       if (allocated(sca11)) then
          WRITE(z_points,'(i10)') 1
          field_name='sca11'
          header_data='SCALARS '//TRIM(field_name)//' double '//TRIM(adjustl(z_points))//eol
          WRITE(unit) TRIM(header_data)
          WRITE(unit) TRIM(header_table)
          DO k=sz,lez
             DO j=sy,ley
                DO i=sx,lex
                   WRITE(unit) sca11(i,j,k)
                ENDDO
             ENDDO
          ENDDO
          WRITE(unit) eol
       end if
    end if
    ! --------------------  SCALAR FIELDS  --------------------------
    if (present(sca12)) then
       if (allocated(sca12)) then
          WRITE(z_points,'(i10)') 1
          field_name='sca12'
          header_data='SCALARS '//TRIM(field_name)//' double '//TRIM(adjustl(z_points))//eol
          WRITE(unit) TRIM(header_data)
          WRITE(unit) TRIM(header_table)
          DO k=sz,lez
             DO j=sy,ley
                DO i=sx,lex
                   WRITE(unit) sca12(i,j,k)
                ENDDO
             ENDDO
          ENDDO
          WRITE(unit) eol
       end if
    end if
    ! --------------------  SCALAR FIELDS  --------------------------
    if (present(sca13)) then
       if (allocated(sca13)) then
          WRITE(z_points,'(i10)') 1
          field_name='sca13'
          header_data='SCALARS '//TRIM(field_name)//' double '//TRIM(adjustl(z_points))//eol
          WRITE(unit) TRIM(header_data)
          WRITE(unit) TRIM(header_table)
          DO k=sz,lez
             DO j=sy,ley
                DO i=sx,lex
                   WRITE(unit) sca13(i,j,k)
                ENDDO
             ENDDO
          ENDDO
          WRITE(unit) eol
       end if
    end if
    ! --------------------  SCALAR FIELDS  --------------------------
    if (present(sca14)) then
       if (allocated(sca14)) then
          WRITE(z_points,'(i10)') 1
          field_name='sca14'
          header_data='SCALARS '//TRIM(field_name)//' double '//TRIM(adjustl(z_points))//eol
          WRITE(unit) TRIM(header_data)
          WRITE(unit) TRIM(header_table)
          DO k=sz,lez
             DO j=sy,ley
                DO i=sx,lex
                   WRITE(unit) sca14(i,j,k)
                ENDDO
             ENDDO
          ENDDO
          WRITE(unit) eol
       end if
    end if
    ! --------------------  SCALAR FIELDS  --------------------------
    if (present(sca15)) then
       if (allocated(sca15)) then
          WRITE(z_points,'(i10)') 1
          field_name='sca15'
          header_data='SCALARS '//TRIM(field_name)//' double '//TRIM(adjustl(z_points))//eol
          WRITE(unit) TRIM(header_data)
          WRITE(unit) TRIM(header_table)
          DO k=sz,lez
             DO j=sy,ley
                DO i=sx,lex
                   WRITE(unit) sca15(i,j,k)
                ENDDO
             ENDDO
          ENDDO
          WRITE(unit) eol
       end if
    end if
    ! --------------------  SCALAR FIELDS  --------------------------
    if (present(sca16)) then
       if (allocated(sca16)) then
          WRITE(z_points,'(i10)') 1
          field_name='sca16'
          header_data='SCALARS '//TRIM(field_name)//' double '//TRIM(adjustl(z_points))//eol
          WRITE(unit) TRIM(header_data)
          WRITE(unit) TRIM(header_table)
          DO k=sz,lez
             DO j=sy,ley
                DO i=sx,lex
                   WRITE(unit) sca16(i,j,k)
                ENDDO
             ENDDO
          ENDDO
          WRITE(unit) eol
       end if
    end if
    ! --------------------  SCALAR FIELDS  --------------------------
    if (present(sca17)) then
       if (allocated(sca17)) then
          WRITE(z_points,'(i10)') 1
          field_name='sca17'
          header_data='SCALARS '//TRIM(field_name)//' double '//TRIM(adjustl(z_points))//eol
          WRITE(unit) TRIM(header_data)
          WRITE(unit) TRIM(header_table)
          DO k=sz,lez
             DO j=sy,ley
                DO i=sx,lex
                   WRITE(unit) sca17(i,j,k)
                ENDDO
             ENDDO
          ENDDO
          WRITE(unit) eol
       end if
    end if
    ! --------------------  SCALAR FIELDS  --------------------------
    if (present(sca18)) then
       if (allocated(sca18)) then
          WRITE(z_points,'(i10)') 1
          field_name='sca18'
          header_data='SCALARS '//TRIM(field_name)//' double '//TRIM(adjustl(z_points))//eol
          WRITE(unit) TRIM(header_data)
          WRITE(unit) TRIM(header_table)
          DO k=sz,lez
             DO j=sy,ley
                DO i=sx,lex
                   WRITE(unit) sca18(i,j,k)
                ENDDO
             ENDDO
          ENDDO
          WRITE(unit) eol
       end if
    end if
    ! --------------------  SCALAR FIELDS  --------------------------
    if (present(sca19)) then
       if (allocated(sca19)) then
          WRITE(z_points,'(i10)') 1
          field_name='sca19'
          header_data='SCALARS '//TRIM(field_name)//' double '//TRIM(adjustl(z_points))//eol
          WRITE(unit) TRIM(header_data)
          WRITE(unit) TRIM(header_table)
          DO k=sz,lez
             DO j=sy,ley
                DO i=sx,lex
                   WRITE(unit) sca19(i,j,k)
                ENDDO
             ENDDO
          ENDDO
          WRITE(unit) eol
       end if
    end if
    ! --------------------  SCALAR FIELDS  --------------------------
    if (present(sca20)) then
       if (allocated(sca20)) then
          WRITE(z_points,'(i10)') 1
          field_name='sca20'
          header_data='SCALARS '//TRIM(field_name)//' double '//TRIM(adjustl(z_points))//eol
          WRITE(unit) TRIM(header_data)
          WRITE(unit) TRIM(header_table)
          DO k=sz,lez
             DO j=sy,ley
                DO i=sx,lex
                   WRITE(unit) sca20(i,j,k)
                ENDDO
             ENDDO
          ENDDO
          WRITE(unit) eol
       end if
    end if

    close(unit)
    return
    
  end subroutine Write_field_vtk
  !*******************************************************************************!


    !*******************************************************************************!
  subroutine Write_Particle_vtk(unit,itime,time,LPart,nPart)
    !*******************************************************************************!
    implicit none
    !---------------------------------------------------------------------
    ! Global variables                                            
    !---------------------------------------------------------------------
    integer, intent(in)                                   :: unit,itime,nPart
    real(8), intent(in)                                   :: time
    type(Particle), allocatable, dimension(:), intent(in) :: LPart
    !---------------------------------------------------------------------
    ! Local variables                                             
    !---------------------------------------------------------------------
    INTEGER                                   :: i,n,cpt
    CHARACTER(len=1), PARAMETER               :: eol=ACHAR(10)
    CHARACTER(len=500)                        :: field_name
    CHARACTER(len=500)                        :: header_main
    CHARACTER(len=200)                        :: header_struc
    CHARACTER(len=200)                        :: header_points
    CHARACTER(len=200)                        :: header_cells
    CHARACTER(len=200)                        :: header_cell_types
    CHARACTER(len=200)                        :: header_data
    CHARACTER(len=200)                        :: main_header_data
    CHARACTER(len=200)                        :: header_dim
    CHARACTER(len=200)                        :: header_table
    CHARACTER(len=12)                         :: c_points,x_points,y_points,z_points
    character(100)                            :: name
    character (LEN=100)                       :: File
    !---------------------------------------------------------------------

    !---------------------------------------------------------------------
    ! mpi 
    !---------------------------------------------------------------------
    if (nproc>1) then
    end if
    !---------------------------------------------------------------------

    
    !---------------------------------------------------------------------
    ! File name
    !---------------------------------------------------------------------
    if (nproc==1) then 
       write(name,'(i8.8)') itime
       File=trim(adjustl(cas))//"_Particles"//"_i"//trim(adjustl(name))//".vtk"
    else
       write(name,'(i6.6)') rank
       File=trim(adjustl(cas))//"_Particles"//"_p"//trim(adjustl(name))
       write(name,'(i8.8)') itime
       File=trim(adjustl(File))//"_Particles"//"_i"//trim(adjustl(name))//".vtk"
    end if
    !---------------------------------------------------------------------
    open(unit,FILE=File,         &
         & STATUS='UNKNOWN',     &
         & form='unformatted',   &
         & access='stream',      &
         & convert='big_endian')
    REWIND unit
    !---------------------------------------------------------------------

    !---------------------------------------------------------------------
    ! number of points
    !---------------------------------------------------------------------
    cpt=0
    do n=1,nPart
       if (LPart(n)%active) cpt=cpt+1
    end do
    WRITE(c_points,'(i10)') cpt
    !---------------------------------------------------------------------
    
    header_main='# vtk DataFile Version 2.0'//eol//&
         "Unstructured Particles"//eol//&
         "BINARY"//eol
    
    header_struc='DATASET UNSTRUCTURED_GRID'//eol
    header_points='POINTS '//TRIM(c_points)//' double'//eol
    header_table='LOOKUP_TABLE default'//eol
    
    WRITE(unit) TRIM(header_main)
    WRITE(unit) TRIM(header_struc)
    WRITE(unit) TRIM(header_points)
    ! ---------------- POINTS ---------------------
    DO n=1,nPart
       if (LPart(n)%active) WRITE(unit) LPart(n)%r
    ENDDO
    WRITE(unit) eol
    ! ---------------------- CELLS --------------------------
    WRITE(z_points,'(i10)') 2*cpt
    header_cells='CELLS '//TRIM(c_points)//' '//TRIM(z_points)//eol
    WRITE(unit) TRIM(header_cells)
    i=0
    DO n=1,nPart
       if (LPart(n)%active) then
          WRITE(unit) 1, i
          i=i+1
       end if
    ENDDO
    WRITE(unit) eol
    ! ---------------------- CELLS TYPE --------------------------
    header_cell_types='CELL_TYPES '//TRIM(c_points)//eol
    WRITE(unit) TRIM(header_cell_types)
    DO n=1,nPart
       if (LPart(n)%active) WRITE(unit) 1
    ENDDO
    WRITE(unit) eol
    ! ----------------------  MAIN HEADER --------------------------
    main_header_data='POINT_DATA '//TRIM(c_points)//eol
    WRITE(unit) TRIM(main_header_data)
    ! --------------------  VECTOR FIELD  --------------------------
    field_name='V'
    header_data='VECTORS '//TRIM(field_name)//' double'//eol
    WRITE(unit) TRIM(header_data)
    DO n=1,nPart
       if (LPart(n)%active) WRITE(unit) LPart(n)%u
    ENDDO
    WRITE(unit) eol
    ! --------------------  SCALAR FIELDS  --------------------------
    WRITE(z_points,'(i10)') 1
    field_name='phi'
    header_data='SCALARS '//TRIM(field_name)//' double '//TRIM(adjustl(z_points))//eol
    WRITE(unit) TRIM(header_data)
    WRITE(unit) TRIM(header_table)
    DO n=1,nPart
       if (LPart(n)%active) WRITE(unit) LPart(n)%phi
    ENDDO
    WRITE(unit) eol
    ! --------------------  SCALAR FIELDS  --------------------------
    WRITE(z_points,'(i10)') 1
    field_name='time'
    header_data='SCALARS '//TRIM(field_name)//' double '//TRIM(adjustl(z_points))//eol
    WRITE(unit) TRIM(header_data)
    WRITE(unit) TRIM(header_table)
    DO n=1,nPart
       if (LPart(n)%active) WRITE(unit) LPart(n)%time
    ENDDO
    WRITE(unit) eol

    close(unit)
    
  end subroutine Write_Particle_vtk
  

  !*******************************************************************************!
  subroutine Write_traj_plt(unit,itime,time,LPart,nPart)
    !*******************************************************************************!
    implicit none
    !---------------------------------------------------------------------
    ! Global variables                                            
    !---------------------------------------------------------------------
    integer, intent(in)                                   :: unit,itime,nPart
    real(8), intent(in)                                   :: time
    type(Particle), allocatable, dimension(:), intent(in) :: LPart
    !---------------------------------------------------------------------
    ! Local variables                                             
    !---------------------------------------------------------------------
    integer :: i,j,k
    character(100)                                              :: name
    character (LEN=100)                                         :: File
    !---------------------------------------------------------------------
    write(name,'(i8.8)') itime
    File=trim(adjustl(cas))//"_Particles_i"//trim(adjustl(name))//".plt"
    open(unit,FILE=File,STATUS='UNKNOWN')
    REWIND unit

    write(unit,'(A)')'VARIABLES = "X" "Y" "Z" "UP" "VP" "WP"'
    write(unit,14) time, nPart
    do i=1,nPart
       write(unit,15) LPart(i)%r,LPart(i)%u
    end do

14  format('ZONE T="',1pe12.5,'", F=POINT, I=',i5)
15  format(6(1pe13.5,1x))

    close(unit)

    return

  end subroutine Write_traj_plt
  !*******************************************************************************!


  !*******************************************************************************!
  subroutine Write_xyz(unit,itime,time,LPart,npart)
    !*******************************************************************************!
    implicit none
    !---------------------------------------------------------------------
    ! Global variables                                            
    !---------------------------------------------------------------------
    integer, intent(in)                                   :: unit,itime
    integer, intent(in)                                   :: npart
    type(Particle), allocatable, dimension(:), intent(in) :: LPart
    real(8), intent(in)                                   :: time
    !---------------------------------------------------------------------
    ! Local variables                                             
    !---------------------------------------------------------------------
    integer                                               :: n
    real(8)                                               :: scale=10
    character(100)                                        :: name
    character (LEN=100)                                   :: File
    !---------------------------------------------------------------------


    File=trim(adjustl(cas))//"_Particles.xyz"
    open(unit,FILE=File,STATUS='UNKNOWN',position="append")

    write(unit,'(i8)') npart+8
    write(unit,'(1pe13.6)') time 

    do n=1,npart
       if (LPart(n)%active) then 
          write(unit,'("Ar",999(1x,1pe13.6))') scale*LPart(n)%r
       else
          write(unit,'("Mg",999(1x,1pe13.6))') scale*xmin,scale*ymin,scale*zmin
       end if
    end do

    write(unit,'("Mg",999(1x,1pe13.6))') scale*xmin,scale*ymin,scale*zmin
    write(unit,'("Mg",999(1x,1pe13.6))') scale*xmax,scale*ymin,scale*zmin
    write(unit,'("Mg",999(1x,1pe13.6))') scale*xmin,scale*ymax,scale*zmin
    write(unit,'("Mg",999(1x,1pe13.6))') scale*xmax,scale*ymax,scale*zmin
    write(unit,'("Mg",999(1x,1pe13.6))') scale*xmin,scale*ymin,scale*zmax
    write(unit,'("Mg",999(1x,1pe13.6))') scale*xmax,scale*ymin,scale*zmax
    write(unit,'("Mg",999(1x,1pe13.6))') scale*xmin,scale*ymax,scale*zmax
    write(unit,'("Mg",999(1x,1pe13.6))') scale*xmax,scale*ymax,scale*zmax

    close(unit)

    return

  end subroutine Write_xyz
  !*******************************************************************************!

  !*******************************************************************************!
  subroutine read_write_Lag(iter,LPart,nPart,code)
    !*******************************************************************************!
    implicit none
    !---------------------------------------------------------------------
    ! Global variables                                            
    !---------------------------------------------------------------------
    integer, intent(in)                                      :: nPart,code
    type(Particle), allocatable, dimension(:), intent(inout) :: LPart
    integer, intent(inout)                                   :: iter
    !---------------------------------------------------------------------
    ! Local variables                                             
    !---------------------------------------------------------------------
    integer                                         :: old_nPart,n
    integer, save                                   :: num=0
    character(len=100)                              :: name
    logical                                         :: file_exists
    !---------------------------------------------------------------------    
    if (code==1) then
       if (num<9) then
          num=num+1
       else
          num=0
       end if
       write(name,'(i2.2)') num
       name="restart_LG"//trim(adjustl(name))//".bin"
       write(*,'(a,1x,a)') "Generating the Lagrangian restart file:   ",trim(adjustl(name))
       open(101,&
            & file=trim(adjustl(name)),&
            & form="unformatted",&
            & action="write")
       write(101) iter
       write(101) nPart
       do n=1,nPart
          write(101) LPart(n)%r
          write(101) LPart(n)%v
          write(101) LPart(n)%u
          write(101) LPart(n)%a
          write(101) LPart(n)%radius
          write(101) LPart(n)%rho
          write(101) LPart(n)%mu
          write(101) LPart(n)%m
          write(101) LPart(n)%RK
          write(101) LPart(n)%interpol
          write(101) LPart(n)%active
       end do
       close(101)
    elseif (code==2) then
       name="restart_LG.bin"
       write(*,'(a,1x,a)') "Reading the Lagrangian restart file:      ",trim(adjustl(name))
       inquire(file=trim(adjustl(name)),exist=file_exists)
       if (file_exists) then 
          open(101,&
               & file=trim(adjustl(name)),&
               & form="unformatted",&
               & action="read")
          read(101) iter
          read(101) old_nPart
          if (old_nPart/=nPart) then
             write(*,*) "nPart of the restart file is different form the current one, STOP"
             STOP
          end if
          do n=1,nPart
             read(101) LPart(n)%r
             read(101) LPart(n)%v
             read(101) LPart(n)%u
             read(101) LPart(n)%a
             read(101) LPart(n)%radius
             read(101) LPart(n)%rho
             read(101) LPart(n)%mu
             read(101) LPart(n)%m
             read(101) LPart(n)%RK
             read(101) LPart(n)%interpol
             read(101) LPart(n)%active
          end do
          close(101)
       else
          write(*,'(a,1x,a,1x,a)') "The file", trim(adjustl(name)), "does not exists, STOP"
          STOP
       end if
    end if
    return

  end subroutine read_write_Lag
  !*******************************************************************************!


  SUBROUTINE WriteEnsightVar(ndv,var,VarName)
    ! ******************************************************************************
    ! WriteEnsightSca writes result data in Ensight's format
    !    from J.-L. Estivalezes
    !
    ! ndv.........: number of dimension of the variable (1=>scalar   3=>vector)  
    ! var.........: data to be written
    ! Varname.....: word used to build filenames
    !
    ! m1,m2,m3....: size of the variable in the x1,x2,x3 direction
    ! imin,imax...: range of writting data in the x1 direction
    ! jmin,jmax...: range of writting data in the x2 direction
    ! kmin,kmax...: range of writting data in the x3 direction
    !  ******************************************************************************
    IMPLICIT NONE
    
    !---------------------------------------------------------------------
    ! Global variables                                            
    !---------------------------------------------------------------------
    INTEGER, INTENT(IN)                                  :: ndv ! 1 = scalar , 2 = vector
    REAL(8), DIMENSION(:,:,:,:), ALLOCATABLE, INTENT(IN) :: var
    CHARACTER*80, INTENT(IN)                             :: Varname
    !---------------------------------------------------------------------
    ! Local variables                                            
    !---------------------------------------------------------------------
    INTEGER                                              :: lex,ley,lez
    REAL(4), ALLOCATABLE, DIMENSION(:,:,:)               :: var_sngl
    CHARACTER(LEN=80)                                    :: VarFileName
    CHARACTER(LEN=80)                                    :: part,blocck
    INTEGER                                              :: FileUnit
    INTEGER                                              :: i,j,k,ii,npart,m
    INTEGER                                              :: iskip
    INTEGER                                              :: descripteur
    INTEGER                                              :: tailleDoublePrecision,tailleCharacter,&
         & tailleInteger,tailleReel
    INTEGER                                              :: iaux,jaux,kaux
    INTEGER(KIND=MPI_OFFSET_KIND)                        :: positionInitiale
    INTEGER, DIMENSION(MPI_STATUS_SIZE)                  :: statut
    INTEGER                                              :: loc_size
    INTEGER, DIMENSION(3)                                :: ucount,ustart,dimsuids
    INTEGER                                              :: filetype
    !---------------------------------------------------------------------

    FileUnit = 666
    part = 'part'
    npart=1
    blocck='block rectilinear'

    !---------------------------------------------------------------------
    ! MPI init types
    !---------------------------------------------------------------------
    CALL MPI_TYPE_SIZE(MPI_DOUBLE_PRECISION,tailleDoublePrecision,MPI_CODE)
    CALL MPI_TYPE_SIZE(MPI_REAL,tailleReel,MPI_CODE)
    CALL MPI_TYPE_SIZE(MPI_CHARACTER,tailleCharacter,MPI_CODE)
    CALL MPI_TYPE_SIZE(MPI_INTEGER,tailleInteger,MPI_CODE)
    !---------------------------------------------------------------------

    !---------------------------------------------------------------------
    ! File creation
    !---------------------------------------------------------------------
    IF (ndv==1) then
       VarFileName=trim(Varname)//'.scl'
    else 
       VarFileName=trim(Varname)//'.vec'
    end IF
    
    !---------------------------------------------------------------------
    CALL MPI_FILE_OPEN(COMM3D,VarFileName,MPI_MODE_WRONLY+MPI_MODE_CREATE,MPI_INFO_NULL,descripteur,MPI_CODE)
    !---------------------------------------------------------------------

    !---------------------------------------------------------------------
    ! Header
    !---------------------------------------------------------------------
    positionInitiale = 0
    !---------------------------------------------------------------------
    CALL MPI_FILE_WRITE_AT_ALL(descripteur,positionInitiale,VarFileName,80,MPI_CHARACTER,statut,MPI_CODE)     
    positionInitiale = positionInitiale+80*tailleCharacter
    !---------------------------------------------------------------------
    CALL MPI_FILE_WRITE_AT_ALL(descripteur,positionInitiale,part,80,MPI_CHARACTER,statut,MPI_CODE)  
    positionInitiale = positionInitiale+80*tailleCharacter
    !---------------------------------------------------------------------
    CALL MPI_FILE_WRITE_AT_ALL(descripteur,positionInitiale,npart,1,MPI_INTEGER,statut,MPI_CODE)  
    positionInitiale = positionInitiale+tailleInteger
    !---------------------------------------------------------------------
    CALL MPI_FILE_WRITE_AT_ALL(descripteur,positionInitiale,blocck,80,MPI_CHARACTER,statut,MPI_CODE)
    positionInitiale = positionInitiale+80*tailleCharacter  
    !---------------------------------------------------------------------


    lex=ex
    ley=ey
    lez=ez

    if (Periodic(1)) then
       if (lex==gex-1) lex=lex+1
    end if
    if (Periodic(2)) then
       if (ley==gey-1) ley=ley+1
    end if
    if (dim==3.and.Periodic(3)) then
       if (lez==gez-1) lez=lez+1
    end if


    !---------------------------------------------------------------------
    ! Taille du bloc de données à écrire par le proc
    !---------------------------------------------------------------------
    iskip=1
    ucount(1)=(lex-sx+1)/iskip
    ucount(2)=(ley-sy+1)/iskip
    ucount(3)=(lez-sz+1)/iskip
    !---------------------------------------------------------------------
 
    !---------------------------------------------------------------------
    ! Dimension totale du domaine à écrire
    !---------------------------------------------------------------------
    dimsuids(1)=(gex-gsx+1)/iskip
    dimsuids(2)=(gey-gsy+1)/iskip
    dimsuids(3)=(gez-gsz+1)/iskip
    !---------------------------------------------------------------------
!!$    IF (dim == 2) THEN !Correction nécessaire pour le 2D quand iskip > 1
!!$       ucount  (3) = 1
!!$       dimsuids(3) = 1
!!$    END IF
    !---------------------------------------------------------------------

    !---------------------------------------------------------------------
    ! En cas de vecteur, on "empile" les différentes composantes sur la direction z
    ! afin qu'elles soient écrites les unes à la suite des autres dans le même fichier
    !---------------------------------------------------------------------
    dimsuids(3)=dimsuids(3)*ndv
    !---------------------------------------------------------------------

    ALLOCATE(var_sngl(1:ucount(1),1:ucount(2),1:ucount(3)))

    DO m = 1,ndv !Pour chacune des composantes du vecteur

       !---------------------------------------------------------------------
       !Passage en simple précision
       !---------------------------------------------------------------------
       kaux = 1
       DO k=sz,lez,iskip
          jaux = 1
          DO j=sy,ley,iskip   
             iaux = 1
             DO i=sx,lex,iskip
                var_sngl(iaux,jaux,kaux) = sngl(var(m,i,j,k))
                iaux = iaux + 1
             END DO
             jaux = jaux + 1
          END DO
          kaux = kaux+1
       END DO
       !---------------------------------------------------------------------
       
       !---------------------------------------------------------------------
       ! Position du sous-domaine à écrire dans le domaine complet
       ! Convention du langage C pour les indices   
       !---------------------------------------------------------------------
       !write(*,*) rank,coords
!!$       ustart(1)=0+coords(1)*ucount(1)
!!$       ustart(2)=0+coords(2)*ucount(2)
!!$       ustart(3)=0+coords(3)*ucount(3)+(m-1)*(dimsuids(3))/ndv ! Empilage des composantes du vecteur
       ustart(1)=sx
       ustart(2)=sy
       if (dim==2) then
          ustart(3)=(m-1)*(dimsuids(3))/ndv ! Empilage des composantes du vecteur
       else
          ustart(3)=sz+(m-1)*(dimsuids(3))/ndv ! Empilage des composantes du vecteur
       end if
       !---------------------------------------------------------------------

       !---------------------------------------------------------------------
       ! Création du sous tableau du proc local
       !---------------------------------------------------------------------
       !CALL mpi_type_create_subarray(3,dimsuids, ucount, ustart,mpi_order_fortran  ,mpi_real4,filetype, code)
       CALL mpi_type_create_subarray(3,dimsuids,ucount,ustart,mpi_order_fortran,mpi_real,filetype,MPI_CODE)
       CALL mpi_type_commit(filetype,MPI_CODE)
       loc_size=product(ucount)
       !---------------------------------------------------------------------

       !---------------------------------------------------------------------
       ! Ecriture
       !---------------------------------------------------------------------
       CALL mpi_file_set_view(descripteur, positionInitiale, mpi_real, filetype, "native", mpi_info_null, code)
!!$       CALL mpi_file_set_view(descripteur,positionInitiale,mpi_real,filetype,"native",mpi_info_null,MPI_CODE)
       CALL mpi_file_write_all(descripteur, var_sngl, loc_size, mpi_real,mpi_status_ignore, code)
!!$       CALL mpi_file_write_all(descripteur,var_sngl,1,filetype,mpi_status_ignore,MPI_CODE)
       !---------------------------------------------------------------------

    END DO


    DEALLOCATE(var_sngl)  
    CALL mpi_type_free(filetype, code)
    CALL MPI_FILE_CLOSE(descripteur,MPI_CODE)

  END SUBROUTINE WriteEnsightVar
  
  
  SUBROUTINE WriteRectEnsightGeo(FileName)
    use mod_Parameters, only: sx,ex,sy,ey,sz,ez, &
         & gsx,gex,gsy,gey,gsz,gez,grid_x,grid_y,grid_z 

    !
    !    ******************************************************************************
    !                      subrout  WriteRectEnsightGeo
    !    ******************************************************************************
    !
    !    writes mesh data in Ensight's ascii format for rectilinear geometry
    !
    !    n1,n2,n3....: number of nodes in the x1,x2,x3 direction
    !    x1,x2,x3....: coordinates
    !

    implicit none

    CHARACTER(LEN=80), INTENT(IN) :: FileName
    
    character(LEN=80)             :: binary_form
    character(LEN=80)             :: file_description1,file_description2
    character(LEN=80)             :: node_id,element_id
    character(LEN=80)             :: part,description_part,blocck

    integer                       :: FileUnit
    integer                       :: i,j,k,npart,iskip
    integer                       :: isize,jsize,ksize
    integer                       :: reclength

    FileUnit = 666

    binary_form       = 'C Binary'
    file_description1 = 'Ensight Model Geometry File Created by '
    file_description2 = 'WriteRectEnsightGeo Routine'
    node_id           = 'node id off'
    element_id        = 'element id off'
    part              = 'part'
    npart             = 1
    description_part  = 'Sortie Ensight'
    blocck            = 'block rectilinear'

    iskip=1
    isize=(gex-gsx+1)/iskip
    jsize=(gey-gsy+1)/iskip
    ksize=(gez-gsz+1)/iskip

    if (ksize == 0) ksize = 1 !AV

    reclength=80*8+4*(4+isize+jsize+ksize)

    open (unit=FileUnit,                &
         & file=trim(FileName)//'.geo', &
         & form='UNFORMATTED',          &
         & access="direct",             &
         & recl=reclength,              &
         & status='replace')

    write(unit=FileUnit,rec=1) binary_form,           &
         & file_description1,                         &
         & file_description2,                         &
         & node_id,                                   &
         & element_id,                                &
         & part,npart,                                &
         & description_part,                          &
         & blocck,                                    &
         & isize,jsize,ksize,                         &
         & (real(sngl(grid_x(i)),4),i=gsx,gex,iskip), &
         & (real(sngl(grid_y(j)),4),j=gsy,gey,iskip), &
         & (real(sngl(grid_z(k)),4),k=gsz,gez,iskip)

    close(FileUnit)

  END SUBROUTINE WriteRectEnsightGeo

  
  SUBROUTINE WriteEnsightCase(nbVar,varName,caseName,VarType,current_iteration,current_time)

    !
    !    ******************************************************************************
    !   EnsightCase helps to write a Ensight's case file
    !
    !    VarName.....: Name of the variable
    !    VarType.....: 1 => Scalar       3 => Vector
    
    
    IMPLICIT NONE

    INTEGER ::nbVar
    INTEGER, DIMENSION(nbVar),INTENT(IN)::VarType

    INTEGER, INTENT(IN) :: current_iteration
    REAL(8), INTENT(IN) :: current_time

    CHARACTER(LEN=80),DIMENSION(nbVar),INTENT(IN)::Varname
    CHARACTER(LEN=80),INTENT(IN)::caseName
    INTEGER::FileUnit,i,j

    CHARACTER (LEN=30) :: dummy
    INTEGER :: nb_fichiers
    INTEGER , DIMENSION(:) , ALLOCATABLE :: filenames
    REAL(8) , DIMENSION(:) , ALLOCATABLE :: timevalues
    REAL(8):: temps_final
    LOGICAL :: ok_ecriture
    LOGICAL, SAVE :: sortie_case_existant=.FALSE.

    FileUnit = 666
    nb_fichiers = 0 
    temps_final = 0
    ok_ecriture = .TRUE.

!!$    IF(reprise .NE. 0) THEN 
!!$       sortie_case_existant = .TRUE.
!!$    END IF

    IF (sortie_case_existant) THEN

       !Ouverture du fichier
       OPEN(FileUnit,FILE=trim(caseName)//'.case')

       !On passe toutes les lignes qui ne nous intéressent pas
       DO i = 1,5
          READ(FileUnit,*) dummy     !FORMAT ... => ... VARIABLE
       END DO

       DO i = 1, nbVar
          READ(FileUnit,*) dummy        !on passe les *.vec et *.scl
       END DO

       READ(FileUnit,*) dummy     !TIME
       READ(FileUnit,*) dummy     !time set
       READ(FileUnit,*) dummy,dummy,dummy, nb_fichiers !number of steps: X
       
       !On alloue et on lit dans la foulée les noms des variables    
       READ(FileUnit,*) dummy !Ligne "filename numbers:"
       ALLOCATE(filenames(1:nb_fichiers))
       DO i = 1,nb_fichiers
          READ(FileUnit,*) filenames(i)
       END DO

       !On lit les différents pas de temps à reprendre
       READ(FileUnit,*) dummy !Ligne "timevalues"
       ALLOCATE(timevalues(1:nb_fichiers))
       DO i = 1,nb_fichiers
          READ(FileUnit,*) timevalues(i)
       END DO

       !On note le temps final
       temps_final = timevalues(nb_fichiers)

       !Fermeture
       CLOSE(FileUnit)

    END IF   

!!$    !Evite les doublons dans les écritures
!!$    IF(reprise .NE. 0) THEN
!!$       IF( current_iteration == filenames(nb_fichiers)) THEN
!!$          ok_ecriture = .FALSE.
!!$       END IF
!!$    END IF
    
    IF(ok_ecriture) THEN

       OPEN (FileUnit,FILE=trim(caseName)//'.case',STATUS='replace')
       WRITE(FileUnit,10) trim(caseName)//'.geo'    !format en fin de routine

       DO i=1,nbVar
          IF(VarType(i)==1) then 
             WRITE(FileUnit,15) i,trim(Varname(i)),trim(Varname(i))//'**********.scl'
          else 
             WRITE(FileUnit,25) i,trim(Varname(i)),trim(Varname(i))//'**********.vec'
          end IF
       END DO

       WRITE(FileUnit,45) nb_fichiers+1    !Format en fin de routine

       !Ecriture numéro de fichiers
       WRITE(FileUnit,'(A)') 'filename numbers: '
       IF (sortie_case_existant) THEN
          DO j = 1, nb_fichiers
             WRITE(FileUnit,'(I10.10)') filenames(j)
          END DO
       END IF
       WRITE(FileUnit,'(I10.10)') current_iteration
       
       !Ecriture temps correspondants
       WRITE(FileUnit,'(A)') 'time values: '   
       IF ( sortie_case_existant) THEN
          DO j = 1, nb_fichiers
             WRITE(FileUnit,'(E13.5)') timevalues(j)    
          END DO
       ENDIF
       WRITE(FileUnit,'(E13.5)') current_time

       
       CLOSE(FileUnit)
    END IF

    IF( sortie_case_existant) THEN
       DEALLOCATE(timevalues,filenames)
    END IF

    sortie_case_existant = .TRUE.


10  format('FORMAT'            ,/ ,'type: ensight gold',//,'GEOMETRY'          ,/ ,'model:    ',A         ,//,'VARIABLE')   
15  format('scalar per node: V',i2.2,'-',A,'   ', A)
25  format('vector per node: V',i2.2,'-',A,'   ', A)
45  format(/,'TIME            '      ,/,'time set: 1     '      ,/,'number of steps: '      ,i10 )

  END SUBROUTINE WriteEnsightCase
 
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
end module mod_InOut
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
