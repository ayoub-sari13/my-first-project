!*******************************************************************************                       
!                                 VOF 3D Weymouth
! split 1D advection of the interface along the x,y,z (d=1,2,3) directions
!
! Following the advection method of Weymouth & Yue :
! Weymouth, G D, and Dick K P Yue,
! "Conservative Volume-of-Fluid Method for Free-Surface Simulations on Cartesian-Grids."
! Journal of Computational Physics 229, no. 8 (April 2010): 2853-2865.
! doi:10.1016/j.jcp.2009.12.018.
!*******************************************************************************

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
module mod_VOF_Cons
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

contains

  subroutine vofcons_3d(cou,u,v,w)
    !*******************************************************************************
    use mod_Parameters, only: sx,ex,sy,ey,sz,ez,gx,gy,gz, &
         & sxu,exu,syu,eyu,szu,ezu,                       &
         & sxv,exv,syv,eyv,szv,ezv,                       &
         & sxw,exw,syw,eyw,szw,ezw,                       &
         & ddxu,ddyv,ddzw,                                &
         & nproc,dt,mesh
    use mod_borders
    use mod_LevelSet
    use mod_mpi
    use mod_VOF_2D3D
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable, intent(in)    :: u,v,w
    real(8), dimension(:,:,:), allocatable, intent(inout) :: cou
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                               :: i,j,k,nbcfl,nbb,ntt
    integer, save                                         :: nt=0 
    real(8), dimension(:,:,:), allocatable                :: vof1,vof3,vof2
    real(8)                                               :: dtcfl,dv,dtrest,tdeb,tfin
    real(8), dimension(:,:,:), allocatable                :: ut,vt,wt
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    allocate(vof1(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
    allocate(vof2(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
    allocate(vof3(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
    allocate(ut(sxu-gx:exu+gx,syu-gy:eyu+gy,szu-gz:ezu+gz),&
         &   vt(sxv-gx:exv+gx,syv-gy:eyv+gy,szv-gz:ezv+gz),&
         &   wt(sxw-gx:exw+gx,syw-gy:eyw+gy,szw-gz:ezw+gz))
    !-------------------------------------------------------------------------------
    call transformation_2d3d(u,ut,ddxu,1)
    call transformation_2d3d(v,vt,ddyv,2)
    call transformation_2d3d(w,wt,ddzw,3)
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! CFL condition 
    !-------------------------------------------------------------------------------
    dv=dt
    dtcfl = 0
    dtrest = 0
    nbcfl = 0
    call cflvof_2d3d(ut,vt,wt,cou,dtcfl,dtrest,nbcfl)
    !-------------------------------------------------------------------------------
    if (dtrest /= 0) then
       nbb = nbcfl + 1
    else
       nbb = nbcfl
    end if
    !-------------------------------------------------------------------------------

    iter_VOF=nbb
    
    !-------------------------------------------------------------------------------
    ! iterations
    !-------------------------------------------------------------------------------
    nt=nt+1
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Vof2 Cbinary if cou > 0.5 vof2=1 else vof2=0
    !-------------------------------------------------------------------------------
    call C_mask(cou,vof2)
    !-------------------------------------------------------------------------------
    
    !-------------------------------------------------------------------------------
    ! inner time loop
    !-------------------------------------------------------------------------------
    !write(6,*) 'nbb=',nbb
    do ntt = 1,nbb
       !-------------------------------------------------------------------------------
       if (nproc>1) call comm_mpi_sca(cou)
       call sca_periodicity(mesh,cou)
       !-------------------------------------------------------------------------------
       if (ntt <= nbcfl) then
          dv = dtcfl
       else
          dv = dtrest
       end if
       !-------------------------------------------------------------------------------

       !-------------------------------------------------------------------------------
       ! VOF 3D Weymouth
       ! split 1D advection of the interface along the x,y,z (d=1,2,3) directions
       ! Weymouth-Yue = Eulerian Implicit + central cell stuff
       !-------------------------------------------------------------------------------
       ! pour la direction 1: vof1 le flux de la vof qui entre à la cellule i,j de   i-1,j
       ! pour la direction 1: vof3 le flux de la vof qui sort de la cellule i,j vers i+1,j
       !-------------------------------------------------------------------------------
       
       if (mod(nt,3)==0) then  ! do z then x then y
          call swp(wt,cou,3,vof1,vof2,vof3,dv)
          call sca_borders_and_periodicity(mesh,cou)
          call swp(ut,cou,1,vof1,vof2,vof3,dv)
          call sca_borders_and_periodicity(mesh,cou)
          call swp(vt,cou,2,vof1,vof2,vof3,dv)
          call sca_borders_and_periodicity(mesh,cou)
       elseif (mod(nt,3)==1) then ! do y z x
          call swp(vt,cou,2,vof1,vof2,vof3,dv)
          call sca_borders_and_periodicity(mesh,cou)
          call swp(wt,cou,3,vof1,vof2,vof3,dv)
          call sca_borders_and_periodicity(mesh,cou)
          call swp(ut,cou,1,vof1,vof2,vof3,dv)
          call sca_borders_and_periodicity(mesh,cou)
       else ! x,y,z 
          call swp(ut,cou,1,vof1,vof2,vof3,dv)
          call sca_borders_and_periodicity(mesh,cou)
          call swp(vt,cou,2,vof1,vof2,vof3,dv)
          call sca_borders_and_periodicity(mesh,cou)
          call swp(wt,cou,3,vof1,vof2,vof3,dv)
          call sca_borders_and_periodicity(mesh,cou)
       endif
       !-------------------------------------------------------------------------------

    end do
    !-------------------------------------------------------------------------------
    deallocate(ut,vt,wt)
    deallocate(vof1,vof2,vof3)
    !-------------------------------------------------------------------------------
    call comm_mpi_sca(cou)
    !call sca_periodicity(cou)
    !-------------------------------------------------------------------------------
    return
  end subroutine vofcons_3d

  SUBROUTINE swp(us,c,dir,vof1,cg,vof3,dv)
    use mod_Parameters, only: sx,ex,sy,ey,sz,ez, &
         & dx,dy,dz,NS_tension
    use mod_borders
    IMPLICIT NONE
    include 'mpif.h'
    INTEGER :: i,j,k
    INTEGER :: ii,jj,kk,i0,j0,k0
    INTEGER, INTENT(IN) :: dir
    real(8),intent(in) :: dv
    REAL(8), allocatable, DIMENSION(:,:,:), INTENT(IN) :: us
    REAL(8), allocatable, DIMENSION(:,:,:), INTENT(INOUT) :: c,vof1,cg,vof3
    REAL(8) :: dxyz
    REAL(8) :: eps_clip=1d-12
    REAL(8) :: a1,a2,alpha
    REAL(8) :: al3d, fl3d, x0(3), deltax(3)
    real(8) :: mxyz(3),stencil3x3(-1:1,-1:1,-1:1)
    INTRINSIC DMAX1,DMIN1

    if (NS_tension>0) eps_clip=1d-8

    ii=0
    jj=0
    kk=0
    if (dir == 1) then
       ii=1
    else if (dir == 2) then
       jj=1
    else if (dir == 3) then
       kk=1
    endif

    do k = sz-1,ez+1
       do j = sy-1,ey+1
          do i = sx-1,ex+1

             a2 = us(i+ii,j+jj,k+kk)*dv
             a1 = us(i,j,k)*dv
             !  default: fluxes=0. (good also for c=0.) 
             vof1(i,j,k) = 0.d0
             vof3(i,j,k) = 0.d0
             !  c = 1.
             if (c(i,j,k) == 1.0d0) then
                vof1(i,j,k) = DMAX1(-a1,0.d0)
                vof3(i,j,k) = DMAX1(a2,0.d0)
                ! 0. < c < 1.
             else if (c(i,j,k) > 0.d0) then
                ! local stencil and normal vector: |dmx|+|dmy|+|dmz| = 1.
                do i0=-1,1
                   do j0=-1,1
                      do k0=-1,1
                         stencil3x3(i0,j0,k0) = c(i+i0,j+j0,k+k0)
                      end do
                   end do
                end do
                call mycs(stencil3x3,mxyz)
                call FAL3D(mxyz,c(i,j,k),alpha)
                ! Eulerian advection
                x0=0d0
                deltax=1d0
                if(a1<0d0) then 
                   deltax(dir)=-a1
                   ! calculer le volume qui entre à la cellule i,j de i-1,j
                   call ffl3d(mxyz,alpha,x0,deltax,vof1(i,j,k))
                endif
                if(a2>0d0) then
                   x0(dir)=1d0-a2
                   deltax(dir)=a2
                   ! calculer le volume qui sort la cellule i,j vers i+1,j
                   call ffl3d(mxyz,alpha,x0,deltax,vof3(i,j,k))
                endif
             endif
          enddo
       enddo
    enddo

    ! assume that ghost layers take care of the boundary conditions, then
    ! fluxes vof1,vof3 must be computed for is-1, ie+1
    ! new clipped values of c (0. <= c <= 1)
    do k = sz,ez
       do j = sy,ey
          do i = sx,ex

             a2 = us(i+ii,j+jj,k+kk)*dv
             a1 = us(i,j,k)*dv
             c(i,j,k) = c(i,j,k) - (vof3(i,j,k) - vof1(i+ii,j+jj,k+kk)) + & 
                  (vof3(i-ii,j-jj,k-kk) - vof1(i,j,k)) + cg(i,j,k)*(a2-a1)

             if (c(i,j,k) < eps_clip) then
                c(i,j,k) = 0
             elseif (c(i,j,k) >  (1.d0 - eps_clip)) then
                c(i,j,k) = 1
             endif

          enddo
       enddo
    enddo
    ! apply proper boundary conditions to c
    !call setvofbc(c,f)
  end subroutine swp
  
  subroutine C_mask(cvof,cbinary)
    implicit none 
    real(8), allocatable, dimension(:,:,:), intent(inout) :: cbinary
    real(8), allocatable, dimension(:,:,:), intent(in)    :: cvof

    where (cvof > 0.5d0)
       cbinary = 1.d0
    elsewhere
       cbinary = 0.d0
    end where

  end subroutine C_mask
  
  subroutine FAL3D(nr,cc,AL3D)
    !===============================================================================
    ! DESCRIPTION OF FUNCTION AL3D:
    ! compute in the unit cube the plane constant alpha satisfying
    ! [nr]*[x] = nr1*x1 + nr2*x2 + nr3*x3 = alpha
    ! where the normal is pointing outward from the reference phase
    ! INPUT: normal coefficients in nr(3) and volume fraction cc 
    !-------------------------------------------------------------------------------
    IMPLICIT NONE
    REAL(8), INTENT(IN):: nr(3),cc
    REAL(8), INTENT(INout) :: AL3D
    REAL(8) :: cch,c01,c02,c03,np1,np2,np3
    REAL(8) :: m1,m2,m3,m12,numer,denom,p,pst,q,arc,csarc
    REAL(8), PARAMETER :: athird=1.d0/3.d0,eps0=1.d-50 
    INTRINSIC DABS,DMIN1,DMAX1,DSQRT,DACOS,DCOS

    np1 = DABS(nr(1))                                 ! need positive coefficients
    np2 = DABS(nr(2))
    np3 = DABS(nr(3))
    m1 = DMIN1(np1,np2)                              ! order positive coefficients
    m3 = DMAX1(np1,np2)
    if (np3 < m1) then
       m2 = m1
       m1 = np3
    else if (np3 >= m3) then
       m2 = m3
       m3 = np3
    else
       m2 = np3
    endif

    denom = DMAX1(6.d0*m1*m2*m3,eps0)                           
    cch = DMIN1(cc,1.d0-cc)                              ! limit to: 0 < cch < 1/2
    c01 = m1*m1*m1/denom                                          ! get cch ranges
    c02  = c01 + 0.5d0*(m2-m1)/m3
    m12 = m1 + m2
    if (m12 <= m3) then
       c03 = 0.5d0*m12/m3
    else
       numer = m3*m3*(3.d0*m12-m3) + m1*m1*(m1-3.d0*m3) + m2*m2*(m2-3.d0*m3)
       c03 = numer/denom
    endif

    ! 1: C<=C1; 2: C1<=C<=C2; 3: C2<=C<=C3; 4: C3<=C<=1/2 (a: m12<=m3; b: m3<m12)) 
    if (cch <= c01) then
       AL3D = (denom*cch)**athird                                       ! case (1)
    else if (cch <= c02) then 
       AL3D = 0.5d0*(m1 + DSQRT(m1*m1 + 8.d0*m2*m3*(cch-c01)))          ! case (2)
    else if (cch <= c03) then
       p = 2.d0*m1*m2
       q = 1.5d0*m1*m2*(m12 - 2.d0*m3*cch)
       pst = DSQRT(p)
       arc = athird*DACOS(q/(p*pst))
       csarc = DCOS(arc)
       AL3D = pst*(DSQRT(3.d0*(1.d0-csarc*csarc)) - csarc) + m12        ! case (3)
    else if (m12 <= m3) then
       AL3D = m3*cch + 0.5d0*m12                                       ! case (4a)
    else                                  
       p = m12*m3 + m1*m2 - 0.25d0                                     
       q = 1.5d0*m1*m2*m3*(0.5d0-cch)
       pst = DSQRT(p)
       arc = athird*DACOS(q/(p*pst))
       csarc = DCOS(arc)
       AL3D = pst*(DSQRT(3.d0*(1.d0-csarc*csarc)) - csarc) + 0.5d0     ! case (4b)
    endif

    if (cc > 0.5d0)  AL3D = 1.d0 - AL3D

    ! compute alpha for the given coefficients  
    AL3D = AL3D + DMIN1(0.d0,nr(1)) + DMIN1(0.d0,nr(2)) + DMIN1(0.d0,nr(3))

  END subroutine FAL3D

  subroutine FFL3D(nr,alpha,x0,dx,fl3d)
    !===============================================================================
    ! DESCRIPTION OF FUNCTION FL3D:
    !  compute in the right hexahedron starting at (x01,x02,x03) and of sides
    ! (dx1,dx2,dx3) the volume cut by the plane
    ! [nr]*[x] = nr1*x1 + nr2*x2 + nr3*x3 = alpha
    ! INPUT: normal coefficients in nr(3), plane constant alpha, starting
    ! point x0(3), sides dx(3)
    !-----------------------------------------------------------------------------
    IMPLICIT NONE
    REAL(8), INTENT(IN):: nr(3),x0(3),dx(3),alpha
    REAL(8), INTENT(INout):: FL3D
    REAL(8) :: al,almax,alh,np1,np2,np3,m1,m2,m3,m12,mm,denom,frac,top
    REAL(8), PARAMETER :: eps0=1.d-50 
    INTRINSIC DMAX1,DMIN1,DABS

    ! move origin to x0 
    al = alpha - nr(1)*x0(1) - nr(2)*x0(2) - nr(3)*x0(3)
    ! reflect the figure when negative coefficients
    al = al + DMAX1(0.d0,-nr(1)*dx(1)) + DMAX1(0.d0,-nr(2)*dx(2)) &
         + DMAX1(0.d0,-nr(3)*dx(3)) 
    np1 = DABS(nr(1))                                 ! need positive coefficients
    np2 = DABS(nr(2))
    np3 = DABS(nr(3))
    almax = np1*dx(1) + np2*dx(2) + np3*dx(3) + eps0                       
    al = DMAX1(0.d0,DMIN1(1.d0,al/almax))           !get new al within safe limits
    alh = DMIN1(al,1.d0-al)                              ! limit to: 0 < alh < 1/2


    ! normalized equation: m1*y1 + m2*y2 + m3*y3 = alh, with 0 <= m1 <= m2 <= m3
    ! the problem is then solved again in the unit cube
    np1 = np1/almax;
    np2 = np2/almax;
    np3 = np3/almax;
    m1 = DMIN1(np1*dx(1),np2*dx(2))                           ! order coefficients
    m3 = DMAX1(np1*dx(1),np2*dx(2))
    top = np3*dx(3)
    !write(*,*) "case top",top,np3,dx(3),almax
    if (top < m1) then
       m2 = m1
       m1 = top
    else if (top >= m3) then
       m2 = m3
       m3 = top
    else
       m2 = top
    endif

    m12 = m1 + m2
    mm = DMIN1(m12,m3)
    denom = DMAX1(6.d0*m1*m2*m3,eps0)

    ! 1: al<=m1; 2: m1<=al<=m2; 3: m2<=al<=mm; 4: mm<=al<=1/2 (a:m12<=m3; b:m3<m12)) 
    if (alh <= m1) then
       frac = alh*alh*alh/denom                                         ! case (1)
       !write(*,*) "case1",alh*alh*alh,denom  
    else if (alh <= m2) then
       frac = 0.5d0*alh*(alh-m1)/(m2*m3) +  m1*m1*m1/denom              ! case (2)
       !write(*,*) "case2",0.5d0*alh*(alh-m1)/(m2*m3) +  m1*m1*m1,denom 
    else if (alh <= mm) then
       top = alh*alh*(3.d0*m12-alh) + m1*m1*(m1-3.d0*alh)              
       frac = (top + m2*m2*(m2-3.d0*alh))/denom                         ! case (3)
       !write(*,*) "case3",(top + m2*m2*(m2-3.d0*alh)),denom     
    else if (m12 <= m3) then
       frac = (alh - 0.5d0*m12)/m3                                     ! case (4a)
       !write(*,*) "case4",(alh - 0.5d0*m12),m3   
    else
       top = alh*alh*(3.d0-2.d0*alh) + m1*m1*(m1-3.d0*alh)             
       frac = (top + m2*m2*(m2-3.d0*alh) + m3*m3*(m3-3.d0*alh))/denom  ! case (4b)
       !write(*,*) "case5",(top + m2*m2*(m2-3.d0*alh) + m3*m3*(m3-3.d0*alh)),denom
       !write(*,*) "case5",top,m2,m3,alh
    endif

    top = dx(1)*dx(2)*dx(3)
    if (al <= 0.5d0) then
       FL3D = frac*top
    else
       FL3D = (1.d0-frac)*top
    endif

  END subroutine FFL3D
  
  subroutine mycs(c,mxyz)
    !*-----------------------------------------------------*
    ! MYC - Mixed Youngs and Central Scheme              *
    !* returns normal normalized so that |mx|+|my|+|mz| = 1*
    !*-----------------------------------------------------*
    ! 
    ! Known problems: the index (1,1,1), i.e. the central cell
    ! in the block, never occurs: neither in the central scheme
    ! nor in Youngs' method. Therefore an isolated droplet will have
    ! a normal with all components to zero. I took care of the
    ! division-by-zero issue, but not of this one.
    !
    ! Ruben
    !
    ! Translated into f90 by Stephane Z.
    !*-----------------------------------------------------*
    implicit none
    real(8) c(0:2,0:2,0:2)
    real(8) mxyz(0:2)
    real(8) m1,m2,m(0:3,0:2),t0,t1,t2
    integer cn
    real(8), parameter  :: NOT_ZERO=1.e-30

    !write the plane as: sgn(mx) X =  my Y +  mz Z + alpha
    !m00 X = m01 Y + m02 Z + alpha

    m1 = c(0,1,0) + c(0,1,2) + c(0,0,1) + c(0,2,1) + &
         c(0,1,1)
    m2 = c(2,1,0) + c(2,1,2) + c(2,0,1) + c(2,2,1) + &
         c(2,1,1)

    if(m1>m2) then
       m(0,0) = 1.
    else
       m(0,0) = -1.
    end if

    m1 = c(0,0,1)+ c(2,0,1)+ c(1,0,1)
    m2 = c(0,2,1)+ c(2,2,1)+ c(1,2,1)
    m(0,1) = 0.5*(m1-m2)

    m1 = c(0,1,0)+ c(2,1,0)+ c(1,1,0)
    m2 = c(0,1,2)+ c(2,1,2)+ c(1,1,2)
    m(0,2) = 0.5*(m1-m2)

    ! write the plane as: sgn(my) Y =  mx X +  mz Z + alpha, 
    !                          m11 Y = m10 X + m12 Z + alpha.

    m1 = c(0,0,1) + c(0,2,1) + c(0,1,1)
    m2 = c(2,0,1) + c(2,2,1) + c(2,1,1)
    m(1,0) = 0.5*(m1-m2)

    m1 = c(1,0,0) + c(1,0,2) + c(2,0,1) + c(0,0,1) +&
         c(1,0,1)
    m2 = c(1,2,0) + c(1,2,2) + c(2,2,1) + c(0,2,1) +&
         c(1,2,1)


    if(m1>m2) then
       m(1,1) = 1.
    else
       m(1,1) = -1.
    end if

    m1 = c(1,0,0)+ c(1,1,0)+ c(1,2,0)
    m2 = c(1,0,2)+ c(1,1,2)+ c(1,2,2)
    m(1,2) = 0.5*(m1-m2)

    ! write the plane as: sgn(mz) Z =  mx X +  my Y + alpha 
    !                          m22 Z = m20 X + m21 Y + alpha

    m1 = c(0,1,0)+ c(0,1,2)+ c(0,1,1)
    m2 = c(2,1,0)+ c(2,1,2)+ c(2,1,1)
    m(2,0) = 0.5*(m1-m2)

    m1 = c(1,0,0)+ c(1,0,2)+ c(1,0,1)
    m2 = c(1,2,0)+ c(1,2,2)+ c(1,2,1)
    m(2,1) = 0.5*(m1-m2)

    m1 = c(0,1,0) + c(2,1,0) + c(1,0,0) + c(1,2,0) +&
         c(1,1,0)
    m2 = c(0,1,2) + c(2,1,2) + c(1,0,2) + c(1,2,2) +&
         c(1,1,2)

    if(m1>m2) then
       m(2,2) = 1.
    else
       m(2,2) = -1.
    end if

    ! normalize each set (mx,my,mz): |mx|+|my|+|mz| = 1

    t0 = DABS(m(0,0)) + DABS(m(0,1)) + DABS(m(0,2))
    m(0,0) = m(0,0)/t0
    m(0,1) = m(0,1)/t0
    m(0,2) = m(0,2)/t0

    t0 = DABS(m(1,0)) + DABS(m(1,1)) + DABS(m(1,2))
    m(1,0) = m(1,0)/t0
    m(1,1) = m(1,1)/t0
    m(1,2) = m(1,2)/t0

    t0 = DABS(m(2,0)) + DABS(m(2,1)) + DABS(m(2,2))
    m(2,0) = m(2,0)/t0
    m(2,1) = m(2,1)/t0
    m(2,2) = m(2,2)/t0

    ! choose among the three central schemes */ 
    t0 = DABS(m(0,0))
    t1 = DABS(m(1,1))
    t2 = DABS(m(2,2))

    cn = 0
    if (t1 > t0) then
       t0 = t1
       cn = 1
    endif

    if (t2 > t0) cn = 2

    ! Youngs-CIAM scheme */  

    call fd32(c,m(3,0:2))

    ! normalize the set (mx,my,mz): |mx|+|my|+|mz| = 1 

    t0 = DABS(m(3,0)) + DABS(m(3,1)) + DABS(m(3,2)) + NOT_ZERO
    m(3,0) = m(3,0)/t0
    m(3,1) = m(3,1)/t0
    m(3,2) = m(3,2)/t0

    ! choose between the previous choice and Youngs-CIAM 
    t0 = DABS (m(3,0))
    t1 = DABS (m(3,1))
    t2 = DABS (m(3,2))
    if (t1 > t0)  t0 = t1
    if (t2 > t0)  t0 = t2

    if (DABS(m(cn,cn)) > t0)  cn = 3

    ! components of the normal vector */
    mxyz(0) = m(cn,0)
    mxyz(1) = m(cn,1)
    mxyz(2) = m(cn,2)

    return 
  end subroutine mycs
  
  subroutine fd32(c,mm)
    !*----------------------------------------------------------------*
    !*  FD32 - Youngs Finite Difference Gradient Scheme               *
    !*  the gradient is computed with a multiplicative factor of -32: *
    !*  mm = - 32 * grad (c)                                          *
    ! *----------------------------------------------------------------*
    ! 
    ! Known problems: the index (1,1,1), i.e. the central cell
    ! in the block, never occurs:
    ! Therefore an isolated droplet will have
    ! a normal with all components to zero.
    !
    ! Ruben
    !
    !
    ! Translated into f90 by Stephane Z.
    !
    !*----------------------------------------------------------------*
    implicit none
    real(8), intent(in) :: c(0:2,0:2,0:2)
    real(8), intent(out) :: mm(0:2)
    real(8) :: m1,m2

    m1 = c(0,0,0) + c(0,2,0) + c(0,0,2) + c(0,2,2) +&
         2.d0*(c(0,0,1) + c(0,2,1) + c(0,1,0) + c(0,1,2)) +&
         4.d0*c(0,1,1)
    m2 = c(2,0,0) + c(2,2,0) + c(2,0,2) + c(2,2,2) +&
         2.d0*(c(2,0,1) + c(2,2,1) + c(2,1,0) + c(2,1,2)) +&
         4.d0*c(2,1,1)
    mm(0) = m1-m2

    m1 = c(0,0,0) + c(0,0,2) + c(2,0,0) + c(2,0,2) +&
         2.d0*(c(0,0,1) + c(2,0,1) + c(1,0,0) + c(1,0,2)) +&
         4.d0*c(1,0,1)
    m2 = c(0,2,0) + c(0,2,2) + c(2,2,0) + c(2,2,2) +&
         2.d0*(c(0,2,1) + c(2,2,1) + c(1,2,0) + c(1,2,2)) +&
         4.d0*c(1,2,1)
    mm(1) = m1-m2

    m1 = c(0,0,0) + c(0,2,0) + c(2,0,0) + c(2,2,0) +&
         2.d0*(c(0,1,0) + c(2,1,0) + c(1,0,0) + c(1,2,0)) +&
         4.d0*c(1,1,0)
    m2 = c(0,0,2) + c(0,2,2) + c(2,0,2) + c(2,2,2) +&
         2.d0*(c(0,1,2) + c(2,1,2) + c(1,0,2) + c(1,2,2)) +&
         4.d0*c(1,1,2)
    mm(2) = m1-m2

    return 
  end subroutine fd32
  !*******************************************************************************
  
end module mod_VOF_Cons
