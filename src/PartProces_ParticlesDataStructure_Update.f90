!===============================================================================
module Bib_VOFLag_ParticlesDataStructure_Update
   !===============================================================================
   contains
   !-----------------------------------------------------------------------------  
   !> @author amine chadil
   ! 
   !> @brief cette routine permet a chaque iteration d'actualiser la structure vl
   !
   !> @param      cou_vim  : la fonction couleur pour les viscosite calculees dans les points
   !! de pression.
   !> @param      cou_vic  : la fonction couleur pour les viscosite calculees dans les points
   !! de viscosite
   !> @param[in]  u,v,w    : vitesse au temps n+1.
   !> @param[out] vl       : la structure contenant toutes les informations
   !! relatives aux particules.
   !-----------------------------------------------------------------------------
   subroutine ParticlesDataStructure_Update (cou,cou_vis,cou_vts,couin0,cou_visin,cou_vtsin,u,v,w,vl)
      !===============================================================================
      !modules
      !===============================================================================
      !-------------------------------------------------------------------------------
      !Modules de definition des structures et variables => src/mod/module_...f90
      !-------------------------------------------------------------------------------
      use Module_VOFLag_ParticlesDataStructure
      use mod_Parameters,                      only : deeptracking,dim,Euler,grid_x,&
	                                       grid_y,grid_z,grid_xu,grid_yv,grid_zw,dx,&
					       dy,dz,dxu,dyv,dzw
      !-------------------------------------------------------------------------------
      !Modules de subroutines de la librairie imft => src/bib_imft_...f90
      !-------------------------------------------------------------------------------
      use Bib_VOFLag_Particle_LagrangianVelocity
      use Bib_VOFLag_Particle_Position_Update
      use Bib_VOFLag_Particle_Orientation_Update
      use Bib_VOFLag_ParticlesDataStructure_Object_Lag4AllCopies_Update
      use Bib_VOFLag_Particle_PhaseFunction
      use Bib_VOFLag_ParticlesDataStructure_SubDomainProcData
      !------------------------------------------------------------------------------
      !===============================================================================
      !declarations des variables
      !===============================================================================
      implicit none
      !-------------------------------------------------------------------------------
      !variables globales
      !-------------------------------------------------------------------------------
      real(8), dimension(:,:,:,:), allocatable, intent(inout) :: cou_vis,cou_vts,    &
                                                                 cou_visin,cou_vtsin
      real(8), dimension(:,:,:),   allocatable, intent(inout) :: cou,couin0
      real(8), dimension(:,:,:),   allocatable, intent(in)    :: u,v,w
      type(struct_vof_lag),                   intent(inout) :: vl
      !-------------------------------------------------------------------------------
      !variables locales 
      !-------------------------------------------------------------------------------
      integer                                               :: np
      !-------------------------------------------------------------------------------
      
      !-------------------------------------------------------------------------------
      if (deeptracking) write(*,*) 'entree ParticlesDataStructure_Update'
      !-------------------------------------------------------------------------------

      !-------------------------------------------------------------------------------
      !calcul des vitesses des Particlees
      !-------------------------------------------------------------------------------
      call Particle_LagrangianVelocity(u,v,w,vl,dim,grid_x,&
	      grid_y,grid_z,grid_xu,grid_yv,grid_zw,dx,dy,dz,dxu,dyv,dzw) 
      
      !-------------------------------------------------------------------------------
      !Actualiser les positions des ellipoides
      !-------------------------------------------------------------------------------
      if (vl%deplacement) then 
         call Particle_Position_Update(vl,Euler,dim)
      end if

      !-------------------------------------------------------------------------------
      !Actualiser les orientations des ellipoides
      !-------------------------------------------------------------------------------
      if (vl%vrot) then
         call Particle_Orientation_Update(vl)

         !-------------------------------------------------------------------------------
         !Actualiser l'orientation le lag_for_all_copies dans le cas de calcul des forces
         !-------------------------------------------------------------------------------
         if (vl%postforce) then
            do np=1,vl%kpt
               call ParticlesDataStructure_Object_Lag4AllCopies_Update(vl,np)
            end do
         end if

      end if

      !-------------------------------------------------------------------------------
      !Actualisation des situations des parts par rapport a chaque proc
      !-------------------------------------------------------------------------------
      call ParticlesDataStructure_SubDomainProcData(vl)

      !-------------------------------------------------------------------------------
      !Actualisation de la fonction couleur dans le domaine
      !-------------------------------------------------------------------------------
      call Particle_PhaseFunction(cou,cou_vis,cou_vts,couin0,cou_visin,cou_vtsin,vl)
      !-------------------------------------------------------------------------------
      if (deeptracking) write(*,*) 'sortie ParticlesDataStructure_Update'
      !-------------------------------------------------------------------------------
   end subroutine ParticlesDataStructure_Update

   !===============================================================================
end module Bib_VOFLag_ParticlesDataStructure_Update
!===============================================================================
