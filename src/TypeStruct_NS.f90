!========================================================================
!
!                   FUGU Version 1.0.0
!
!========================================================================
!
! Copyright  Stéphane Vincent
!
! stephane.vincent@u-pem.fr          straightparadigm@gmail.com
!
! This file is part of the FUGU Fortran library, a set of Computational 
! Fluid Dynamics subroutines devoted to the simulation of multi-scale
! multi-phase flows for resolved scale interfaces (liquid/gas/solid). 
! FUGU uses the initial developments of DyJeAT and SHALA codes from 
! Jean-Luc Estivalezes (jean-luc.estivalezes@onera.fr) and 
! Frédéric Couderc (couderc@math.univ-toulouse.fr).
!
!========================================================================
!**
!**   NAME       : type_struct_ns.f90
!**
!**   AUTHOR     : Stéphane Vincent
!**
!**   FUNCTION   : Navier-Stokes types of FUGU software
!**
!**   DATES      : Version 1.0.0  : from : january, 16, 2017
!**
!========================================================================

!===============================================================================
! Data structure and types for Navier-Stokes equations
!===============================================================================
module mod_struct_Navier_Stokes
  !===============================================================================
  use mod_struct_Solver
  use mod_boundary
  use mod_Parameters, only:dim

  type struct_NS
     integer                            :: nb_coef_matrix        ! number of non zero matrix coefficient by line = kdv
     integer                            :: nb_matrix_element     ! number of matrix elements in CSR format = nps
     integer                            :: nb_diagonal_precond   ! number of diagonal used for preconditionning = ndd
     integer                            :: nb_coef_matrix_pcd    ! ... for pressure convection diffusion operator
     integer                            :: nb_matrix_element_pcd ! ...
     real(8)                            :: dt                    ! time step
     type(struct_augmented_Lagrangian)  :: output                ! Output parameters of Navier-Stokes solving
     type(boundary)                     :: bound                 ! boundary conditions
  end type struct_NS
  
  !===============================================================================
end module mod_struct_Navier_Stokes
!===============================================================================

