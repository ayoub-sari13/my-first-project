
  if (once) then
     call InitCoeffVS
     once=.false.
  end if
  
  if (rank==0) then
     write(*,*) "CHAUSSETTE"
  end if

  ! debit total
  qtot=0

  !-------------------------------------------------------
  ! CHAUSSETTE
  !-------------------------------------------------------

  !-------------------------------------------------------
  ! dimensions
  !-------------------------------------------------------
  x_max=xmax
  x_min=x_max-0.384d0
  y_min=ymin
  y_max=y_min+0.384d0
  z_min=zmin
  z_max=z_min+2.85d0
  !-------------------------------------------------------
  
  !-------------------------------------------------------
  ! amplitude vitesse/debit
  !-------------------------------------------------------
  surfu=1
  surfv=1
  surfw=1
  qinf=0.249d0 ! total chaussette
  !-------------------------------------------------------

  !-------------------------------------------------------
  ! surface computation (1er passage) penalisation (2nd)
  !-------------------------------------------------------
  
  do n=1,2

     uinf=-qinf/2/surfu
     vinf=+qinf/2/surfv
     winf=0

     !write(*,*) uinf,vinf

     surfu=0
     surfv=0
     do k=szu,ezu
        do j=syu,eyu
           do i=sxu,exu
              if (grid_x(i)>=x_min.and.grid_x(i)<=x_max) then
                 if (grid_y(j)>=y_min.and.grid_y(j)<=y_max) then
                    if (grid_z(k)>=z_min.and.grid_z(k)<=z_max) then
                       !-------------------------------------------------------
                       ! U component
                       !-------------------------------------------------------
                       if (j>gsy.and.k>gsz) then
                          if (i==gex) then
                             surfu=surfu+CoeffS(i,j,k)*dy(j)*dz(k)
                             uin(i+1,j,k)=2*uinf
                             penu(i+1,j,k,:)=0
                             penu(i+1,j,k,1)=pen_amp/2
                             penu(i+1,j,k,2)=pen_amp/2
                          else
                             slvu(i,j,k)=pen_amp
                             smvu(i,j,k)=uinf*pen_amp
                             slvu(i-1,j,k)=pen_amp
                             smvu(i-1,j,k)=uinf*pen_amp
                          end if
                       end if
                    end if
                 end if
              end if
           end do
        end do
     end do
     do k=szv,ezv
        do j=syv,eyv
           do i=sxv,exv
              if (grid_x(i)>=x_min.and.grid_x(i)<=x_max) then
                 if (grid_y(j)>=y_min.and.grid_y(j)<=y_max) then
                    if (grid_z(k)>=z_min.and.grid_z(k)<=z_max) then
                       !-------------------------------------------------------
                       ! V component
                       !-------------------------------------------------------
                       if (i<gex.and.k>gsz) then
                          if (j==gsy) then
                             surfv=surfv+CoeffS(i,j,k)*dx(i)*dz(k)
                             vin(i,j,k)=2*vinf
                             penv(i,j,k,:)=0
                             penv(i,j,k,1)=pen_amp/2
                             penv(i,j,k,5)=pen_amp/2
                          else
                             slvv(i,j,k)=pen_amp
                             smvv(i,j,k)=vinf*pen_amp
                             slvv(i,j+1,k)=pen_amp
                             smvv(i,j+1,k)=vinf*pen_amp
                          end if
                       end if
                       !-------------------------------------------------------
                    end if
                 end if
              end if
           end do
        end do
     end do

     call MPI_ALLREDUCE(surfu,tmp,1,MPI_DOUBLE_PRECISION,MPI_SUM,COMM3D,CODE); surfu=tmp
     call MPI_ALLREDUCE(surfv,tmp,1,MPI_DOUBLE_PRECISION,MPI_SUM,COMM3D,CODE); surfv=tmp
     
  end do
  
  qtot=qtot+qinf
  if (rank==0) then
     write(*,*) "Q_CHAUSETTE : ", vinf*surfv, uinf*surfu, qinf/2
     write(*,*) "Q_TOT       : ", abs(vinf)*surfv+abs(uinf)*surfu, qinf
     write(*,*) "VITESSE X   : ", abs(uinf)
     write(*,*) "SURFACE X   : ", surfu,(y_max-y_min)*(z_max-z_min)
     write(*,*) "DEBIT X     : ", surfu*abs(uinf)
     write(*,*) "VITESSE Y   : ", abs(vinf)
     write(*,*) "SURFACE Y   : ", surfv,(x_max-x_min)*(z_max-z_min)
     write(*,*) "DEBIT Y     : ", surfv*abs(vinf)
  end if
  
  !---------------------------------------------------------------------
  ! Partie Cylindrique
  !---------------------------------------------------------------------
  x0=x_max-(x_max-x_min)/2
  y0=y_min+(y_max-y_min)/2
  alpha=(0.195d0/2)**2
  !write(*,*) "pos", x0,y0,sqrt(alpha)
  z_min=z_max
  z_max=zmax
  do k=sz,ez
     do j=sy,ey
        do i=sx,ex
           x=grid_x(i)
           y=grid_y(j)
           z=grid_z(k)
           rc2=((x-x0)**2+(y-y0)**2)
           if (rc2<=alpha.and.z>z_min) then
              slvu(i,j,k)=pen_amp
              slvu(i+1,j,k)=pen_amp
              slvv(i,j,k)=pen_amp
              slvv(i,j+1,k)=pen_amp
              slvw(i,j,k)=pen_amp
              slvw(i,j,k+1)=pen_amp
           endif
        enddo
     enddo
  end do

  !-------------------------------------------------------
  ! FIN CHAUSSETTE
  !-------------------------------------------------------


  !-------------------------------------------------------
  !  SUPPORT ET SORBONNE
  !-------------------------------------------------------

  !-------------------------------------------------------
  ! dimensions support 
  !-------------------------------------------------------
  x_min=xmin+3.52d0
  x_max=x_min+0.98d0
  y_max=ymax
  y_min=y_max-0.91d0-0.03d0
  z_min=zmin
  z_max=z_min+0.935d0
  !-------------------------------------------------------
  call Penalize_cubic_block( &
       & x_min,x_max,         &
       & y_min,y_max,         &
       & z_min,z_max,         &
       & slvu,slvv,slvw)
  !-------------------------------------------------------
  ! dimensions face gauche 
  !-------------------------------------------------------
  x_max=xmin+3.52d0+0.98d0/2-0.8d0/2
  x_min=x_max-max(0.015d0,minval(dx)+tiny(1d0))
  y_max=ymax
  y_min=y_max-0.91d0-0.03d0
  z_min=zmin
  z_max=z_min+0.935d0+0.065d0+0.4d0+2*0.015d0
  !-------------------------------------------------------
  x_min_extraction=x_max
  x_min_injection=x_max
  !-------------------------------------------------------
  call Penalize_cubic_block( &
       & x_min,x_max,         &
       & y_min,y_max,         &
       & z_min,z_max,         &
       & slvu,slvv,slvw)
  !-------------------------------------------------------
  ! dimensions face droite 
  !-------------------------------------------------------
  x_min=xmin+3.52d0+0.98d0/2+0.8d0/2
  x_max=x_min+max(0.015d0,minval(dx)+tiny(1d0))
  !-------------------------------------------------------
  x_max_extraction=x_min
  x_max_injection=x_min
  !-------------------------------------------------------
  call Penalize_cubic_block( &
       & x_min,x_max,         &
       & y_min,y_max,         &
       & z_min,z_max,         &
       & slvu,slvv,slvw)
  !-------------------------------------------------------
  ! plateau 
  !-------------------------------------------------------
  x_min=xmin+3.52d0+0.98d0/2-0.8d0/2
  x_max=x_min+0.8d0
  y_max=ymax
  y_min=y_max-0.91d0-0.03d0
  z_min=zmin+0.935d0
  z_max=z_min+0.065d0+0.015d0+0.057d0
  !-------------------------------------------------------
  z_min_extraction=z_max
  z_max_injection=z_max
  y_min_injection=y_min
  !-------------------------------------------------------
  call Penalize_cubic_block( &
       & x_min,x_max,         &
       & y_min,y_max,         &
       & z_min,z_max,         &
       & slvu,slvv,slvw)
  !-------------------------------------------------------
  ! dimensions face haute 
  !-------------------------------------------------------
  x_min=xmin+3.52d0+0.98d0/2-0.8d0/2-0.015d0
  x_max=x_min+0.8d0+2*0.015d0
  y_max=ymax
  y_min=y_max-0.91d0-0.03d0
  z_max=zmin+0.935d0+0.065d0+0.4d0+2*0.015d0
  z_min=z_max-max(0.015d0,minval(dz)+tiny(1d0))
  !-------------------------------------------------------
  z_max_extraction=z_min
  !-------------------------------------------------------
  call Penalize_cubic_block( &
       & x_min,x_max,         &
       & y_min,y_max,         &
       & z_min,z_max,         &
       & slvu,slvv,slvw)
  !-------------------------------------------------------

  !-------------------------------------------------------
  ! FIN SUPPORT ET SORBONNE
  !-------------------------------------------------------

  !-------------------------------------------------------
  ! EXTRACTION (FICTIVE)
  !-------------------------------------------------------
  x_min=xmin+3.52d0+0.98d0/2-0.23d0/2
  x_max=x_min+0.23d0
  y_min=ymax-(0.11d0+0.23d0)
  y_max=y_min+0.23d0
  z_min=0.935d0+0.065d0+0.015d0+0.4d0
  z_max=zmax
  !-------------------------------------------------------
  call Penalize_cubic_block( &
       & x_min,x_max,         &
       & y_min,y_max,         &
       & z_min,z_max,         &
       & slvu,slvv,slvw)
  !-------------------------------------------------------
  ! FIN EXTRACTION (FICTIVE)
  !-------------------------------------------------------

  if (rank==0) then
     write(*,*) "PLATEAU INJECTION"
  end if

  !-------------------------------------------------------
  ! PLATEAU INJECTION
  !-------------------------------------------------------
  
  !-------------------------------------------------------
  ! dimensions
  !-------------------------------------------------------
  x_min=x_min_injection
  x_max=x_max_injection
  y_min=y_min_injection
  y_max=y_min+0.168d0
  z_min=zmin
  z_max=z_max_injection
  !-------------------------------------------------------
  
  !-------------------------------------------------------
  ! amplitude vitesse/debit
  !-------------------------------------------------------
  surfu=1
  surfv=1
  surfw=1
  qinf=0.00186d0
  !-------------------------------------------------------

  !-------------------------------------------------------
  ! surface computation (1er passage) penalisation (2nd)
  !-------------------------------------------------------
  
  do n=1,2

     uinf=0
     vinf=0
     winf=+qinf/surfw

     !write(*,*) winf

     surfw=0
!!$     do k=sz-gz,ez+gz
!!$        do j=sy-gy,ey+gy
!!$           do i=sx-gx,ex+gx
     do k=szw,ezw
        do j=syw,eyw
           do i=sxw,exw
              if (grid_x(i)>x_min.and.grid_x(i)<x_max) then
                 if (grid_y(j)>=y_min.and.grid_y(j)<=y_max) then
                    if (grid_z(k)>=z_min.and.grid_z(k)<=z_max) then
                       !-------------------------------------------------------
                       ! W component
                       !-------------------------------------------------------
                       if (k==gsz) then
                          surfw=surfw+CoeffS(i,j,k)*dx(i)*dy(j)
                          win(i,j,k)=2*winf
                          penw(i,j,k,:)=0
                          penw(i,j,k,1)=pen_amp/2
                          penw(i,j,k,7)=pen_amp/2
                       else
                          slvw(i,j,k)=pen_amp
                          smvw(i,j,k)=winf*pen_amp
                          slvw(i,j,k+1)=pen_amp
                          smvw(i,j,k+1)=winf*pen_amp
                       end if
                       !-------------------------------------------------------
                    end if
                 end if
              end if
           end do
        end do
     end do

     call MPI_ALLREDUCE(surfw,tmp,1,MPI_DOUBLE_PRECISION,MPI_SUM,COMM3D,CODE); surfw=tmp
     
  end do
  
  qtot=qtot+qinf
  
  if (rank==0) then
     write(*,*) "S_INJ       : ", surfw
     write(*,*) "Q_INJ       : ", abs(winf)*surfw, qinf
     write(*,*) "Q_TOT       : ", qtot
     write(*,*) "W INJECTION : ", winf
  end if
  
  !-------------------------------------------------------
  ! FIN PLATEAU INJECTION
  !-------------------------------------------------------

  if (rank==0) then
     write(*,*) "EXTRATION CAISSON"
  end if

  !-------------------------------------------------------
  ! EXTRACTION CAISSON
  !-------------------------------------------------------
  
  !-------------------------------------------------------
  ! dimensions
  !-------------------------------------------------------
  y_max=ymax
  y_min=ymax-0.91d0-0.03d0+0.4d0
  x_min=x_min_extraction
  x_max=x_max_extraction
  z_min=z_min_extraction
  z_max=z_max_extraction

  
  !write(*,*) x_min,x_max
  !write(*,*) y_min,y_max
  !write(*,*) z_min,z_max

  !-------------------------------------------------------
  
  !-------------------------------------------------------
  ! amplitude vitesse/debit
  !-------------------------------------------------------
  surfu=1
  surfv=1
  surfw=1
  qinf=0.111d0
  qinf=0.0994d0
  !-------------------------------------------------------

  !-------------------------------------------------------
  ! surface computation (1er passage) penalisation (2nd)
  !-------------------------------------------------------
  
  do n=1,2

     uinf=0
     vinf=+qinf/surfv
     winf=0

     !write(*,*) vinf

     surfv=0
!!$     do k=sz-gz,ez+gz
!!$        do j=sy-gy,ey+gy
!!$           do i=sx-gx,ex+gx
     do k=szv,ezv
        do j=syv,eyv
           do i=sxv,exv
              if (grid_x(i)>x_min.and.grid_x(i)<x_max) then
                 if (grid_y(j)>=y_min.and.grid_y(j)<=y_max) then
                    if (grid_z(k)>z_min.and.grid_z(k)<z_max) then
                       !-------------------------------------------------------
                       ! V component
                       !-------------------------------------------------------
                       if (j==gey) then
                          surfv=surfv+CoeffS(i,j,k)*dx(i)*dz(k)
                          vin(i,j+1,k)=2*vinf
                          penv(i,j+1,k,:)=0
                          penv(i,j+1,k,1)=pen_amp/2
                          penv(i,j+1,k,4)=pen_amp/2
                       else
                          slvv(i,j,k)=pen_amp
                          smvv(i,j,k)=vinf*pen_amp
                          slvv(i,j-1,k)=pen_amp
                          smvv(i,j-1,k)=vinf*pen_amp
                       end if
                    end if
                 end if
              end if
           end do
        end do
     end do

     call MPI_ALLREDUCE(surfv,tmp,1,MPI_DOUBLE_PRECISION,MPI_SUM,COMM3D,CODE); surfv=tmp
     
  end do

  qtot=qtot-qinf
  
  if (rank==0) then
     write(*,*) "V_EXTRACTION : ", abs(vinf)
     write(*,*) "S_EXTRACTION : ", surfv
     write(*,*) "Q_EXTRACTION : ", abs(vinf)*surfv, qinf
     write(*,*) "Q_TOT        : ", qtot

     write(*,*) (x_max-x_min),(z_max-z_min), (x_max-x_min)*(z_max-z_min)
     write(*,*) vinf
     write(*,*) vinf*(x_max-x_min)*(z_max-z_min),qinf
  end if

  !-------------------------------------------------------
  ! FIN EXTRACTION CAISSON
  !-------------------------------------------------------

  if (rank==0) then
     write(*,*) "EXTRACTION SECONDAIRE"
  end if
  
  !-------------------------------------------------------
  ! EXTRACTION SECONDAIRE
  !-------------------------------------------------------
  
  !-------------------------------------------------------
  ! dimensions
  !-------------------------------------------------------
  x_min=0.5d0
  y_min=ymax-1d0
  z_min=0.1d0 ! rayon
  !-------------------------------------------------------
  
  !-------------------------------------------------------
  ! amplitude vitesse/debit
  !-------------------------------------------------------
  surfu=1
  surfv=1
  surfw=1
  !write(*,*) qtot
  qinf=qtot
  !-------------------------------------------------------

  !-------------------------------------------------------
  ! surface computation (1er passage) penalisation (2nd)
  !-------------------------------------------------------
  
  do n=1,2

     uinf=0
     vinf=0
     winf=qinf/surfw

     !write(*,*) winf

     surfw=0
     do k=szw,ezw
        do j=syw,eyw
           do i=sxw,exw 
              if (k==gez) then
                 if ((grid_x(i)-x_min)**2+(grid_y(j)-y_min)**2<=z_min**2) then 
                    !-------------------------------------------------------
                    ! W component
                    !-------------------------------------------------------
                    surfw=surfw+CoeffS(i,j,k)*dx(i)*dy(j)
                    win(i,j,k+1)=2*winf
                    penw(i,j,k+1,:)=0
                    penw(i,j,k+1,1)=pen_amp/2
                    penw(i,j,k+1,6)=pen_amp/2
                    !-------------------------------------------------------
                 end if
              end if
           end do
        end do
     end do
     
     call MPI_ALLREDUCE(surfw,tmp,1,MPI_DOUBLE_PRECISION,MPI_SUM,COMM3D,CODE); surfw=tmp

!!$     if (rank==0) then
!!$        write(*,*) winf*surfw, qinf
!!$     end if
     
  end do

  qtot=qtot-qinf
  
  if (rank==0) then
     write(*,*) "Q_EXT_SECONDAIRE   :", abs(winf)*surfw, qinf
     write(*,*) "Q_TOT              :", qtot
     write(*,*) "SURF_EXTRACTION    :", surfw,pi*z_min**2
     write(*,*) "VITESSE EXTRACTION :", winf
  end if

  !-------------------------------------------------------
  ! FIN EXTRACTION SECONDAIRE
  !-------------------------------------------------------
  
  !write(*,*) "SYSTEME DE DEPLACEMENT"

  !-------------------------------------------------------
  ! SYSTEME DE DEPLACEMENT
  !-------------------------------------------------------
  
  !-------------------------------------------------------
  ! 2 rails fixes
  !-------------------------------------------------------
  ! Rails gauche
  !-------------------------------------------------------
  x_min=xmin+3.11d0
  x_max=x_min+0.15d0
  y_min=ymin+1.48d0
  y_max=y_min+2.59d0
  z_min=zmin+3.32d0
  z_max=zmax
  !-------------------------------------------------------
  call Penalize_cubic_block( &
       & x_min,x_max,         &
       & y_min,y_max,         &
       & z_min,z_max,         &
       & slvu,slvv,slvw)
  !-------------------------------------------------------
  ! Rail droite
  !-------------------------------------------------------
  x_min=xmin+4.45d0
  x_max=x_min+0.15d0
  y_min=ymin+1.48d0
  y_max=y_min+2.59d0
  z_min=zmin+3.32d0
  z_max=z_min+0.18d0
  !-------------------------------------------------------
  call Penalize_cubic_block( &
       & x_min,x_max,         &
       & y_min,y_max,         &
       & z_min,z_max,         &
       & slvu,slvv,slvw)
  !-------------------------------------------------------
  ! rail selon x
  !-------------------------------------------------------
  x_max=xmax-1.65d0
  x_min=x_max-2.5d0
  y_max=ymax-2.23d0
  y_min=y_max-0.47d0
  z_max=zmax-0.18d0
  z_min=z_max-0.148d0
  call Penalize_cubic_block( &
       & x_min,x_max,         &
       & y_min,y_max,         &
       & z_min,z_max,         &
       & slvu,slvv,slvw)
  !-------------------------------------------------------
  ! rail z (en relatif du rail précédent)
  !-------------------------------------------------------
  z_max=z_min
  z_min=z_max-1.982d0
  y_min=y_min
  y_max=y_min+0.12d0
  x_min=x_min+1.325d0
  x_max=x_min+0.28d0
  !-------------------------------------------------------
  call Penalize_cubic_block( &
       & x_min,x_max,         &
       & y_min,y_max,         &
       & z_min,z_max,         &
       & slvu,slvv,slvw)
  !-------------------------------------------------------
  ! support 1 (en relatif du rail précédent)
  !-------------------------------------------------------
  z_min=zmin+1+0.06d0
  z_max=z_min+0.4d0
  y_max=y_min
  y_min=y_max-0.05d0
  x_min=x_min-(0.44d0-0.28d0)/2
  x_max=x_min+0.05d0
  !-------------------------------------------------------
  call Penalize_cubic_block( &
       & x_min,x_max,         &
       & y_min,y_max,         &
       & z_min,z_max,         &
       & slvu,slvv,slvw)
  !-------------------------------------------------------
  ! support 2 (en relatif du rail précédent)
  !-------------------------------------------------------
  x_min=x_min+0.44d0-0.05d0
  x_max=x_min+0.05d0
  !-------------------------------------------------------
  call Penalize_cubic_block( &
       & x_min,x_max,         &
       & y_min,y_max,         &
       & z_min,z_max,         &
       & slvu,slvv,slvw)
  !-------------------------------------------------------
  ! support 3 horizontal (en relatif du rail précédent)
  !-------------------------------------------------------
  x_max=x_max
  x_min=x_max-0.44d0
  z_min=z_max-0.05d0
  !-------------------------------------------------------
  call Penalize_cubic_block( &
       & x_min,x_max,         &
       & y_min,y_max,         &
       & z_min,z_max,         &
       & slvu,slvv,slvw)
  !-------------------------------------------------------
  ! support 4 barre camera (en relatif du rail précédent)
  !-------------------------------------------------------
  z_min=zmin+1
  z_max=z_min+0.06d0
  y_max=y_min
  y_min=y_max-0.06d0
  x_max=x_max+1.51d0
  x_min=x_max-3.45d0
  !-------------------------------------------------------
  call Penalize_cubic_block( &
       & x_min,x_max,         &
       & y_min,y_max,         &
       & z_min,z_max,         &
       & slvu,slvv,slvw)
  !-------------------------------------------------------
  ! camera 1 gauche (en relatif du rail précédent)
  !-------------------------------------------------------
  z_min=z_max
  z_max=z_min+0.15d0
  y_max=y_max+(0.25d0-0.06d0)/2
  y_min=y_max-0.25d0
  x_min=x_min+0.24d0
  x_max=x_min+0.2d0
  !-------------------------------------------------------
  call Penalize_cubic_block( &
       & x_min,x_max,         &
       & y_min,y_max,         &
       & z_min,z_max,         &
       & slvu,slvv,slvw)
  !-------------------------------------------------------
  ! camera 2 droite (en relatif du rail précédent)
  !-------------------------------------------------------
  x_min=x_min-0.24d0+3.45d0-0.25d0-0.2d0
  x_max=x_min+0.2d0
  call Penalize_cubic_block( &
       & x_min,x_max,         &
       & y_min,y_max,         &
       & z_min,z_max,         &
       & slvu,slvv,slvw)
  !-------------------------------------------------------

  !-------------------------------------------------------
  ! FIN SYSTEME DE DEPLACEMENT
  !-------------------------------------------------------
  
  !-------------------------------------------------------
  ! RAIL PLAQUE //
  !-------------------------------------------------------
  
  !-------------------------------------------------------
  x_min=2.73d0
  x_max=x_min+3.17d0-0.12d0
  y_max=ymax-1.48d0
  y_min=y_max-0.12d0
  z_min=zmin
  z_max=z_min+0.117d0
  !------------------------------------------------------
  ! Guide rail Penalization
  !------------------------------------------------------
  call Penalize_cubic_block( &
       & x_min,x_max,         &
       & y_min,y_max,         &
       & z_min,z_max,         &
       & slvu,slvv,slvw)
  !-----------------------------------------------------
  ! au bout
  !-----------------------------------------------------
  x_max=2.73d0+3.17d0-0.12d0
  x_min=x_max-0.12d0
  y_max=ymax-1.3d0
  y_min=y_max-0.7d0
  z_min=zmin
  z_max=z_min+0.15d0
  call Penalize_cubic_block( &
       & x_min,x_max,         &
       & y_min,y_max,         &
       & z_min,z_max,         &
       & slvu,slvv,slvw)
  !-------------------------------------------------------
  ! FIN RAIL PLAQUE //
  !-------------------------------------------------------

  
  !-------------------------------------------------------
  ! SUPPORT LASER
  !-------------------------------------------------------
  x_min=xmin+1.55d0
  x_max=x_min+0.87d0
  y_max=ymax-0.35d0
  y_min=y_max-1.105d0
  z_min=zmin
  z_max=z_min+1.53d0
  !-------------------------------------------------------
  call Penalize_cubic_block( &
       & x_min,x_max,         &
       & y_min,y_max,         &
       & z_min,z_max,         &
       & slvu,slvv,slvw)
  !-------------------------------------------------------
  ! FIN SUPPORT LASER
  !-------------------------------------------------------
  

  !-------------------------------------------------------
  ! PURGE BORDS 
  !-------------------------------------------------------
  do k=szu,ezu
     do j=syu,eyu
        do i=sxu,exu
           !-------------------------------------------------------
           if (i<=gsxu .or. i>=gexu) then
              slvu(i,j,k)=0
              smvu(i,j,k)=0
           end if
           !-------------------------------------------------------
           if (j<=gsyu .or. j>=geyu) then
              slvu(i,j,k)=0
              smvu(i,j,k)=0
           end if
           !-------------------------------------------------------
           if (k<=gszu .or. k>=gezu) then
              slvu(i,j,k)=0
              smvu(i,j,k)=0
           end if
           !-------------------------------------------------------
        end do
     end do
  end do
  do k=szv,ezv
     do j=syv,eyv
        do i=sxv,exv
           !-------------------------------------------------------
           if (i<=gsxv .or. i>=gexv) then
              slvv(i,j,k)=0
              smvv(i,j,k)=0
           end if
           !-------------------------------------------------------
           if (j<=gsyv .or. j>=geyv) then
              slvv(i,j,k)=0
              smvv(i,j,k)=0
           end if
           !-------------------------------------------------------
           if (k<=gszv .or. k>=gezv) then
              slvv(i,j,k)=0
              smvv(i,j,k)=0
           end if
           !-------------------------------------------------------
        end do
     end do
  end do
  do k=szw,ezw
     do j=syw,eyw
        do i=sxw,exw
           !-------------------------------------------------------
           if (i<=gsxw .or. i>=gexw) then
              slvw(i,j,k)=0
              smvw(i,j,k)=0
           end if
           !-------------------------------------------------------
           if (j<=gsyw .or. j>=geyw) then
              slvw(i,j,k)=0
              smvw(i,j,k)=0
           end if
           !-------------------------------------------------------
           if (k<=gszw .or. k>=gezw) then
              slvw(i,j,k)=0
              smvw(i,j,k)=0
           end if
           !-------------------------------------------------------
        end do
     end do
  end do
  !-------------------------------------------------------

  !-------------------------------------------------------
  ! FIN PURGE BORDS 
  !-------------------------------------------------------

!!$  smvu=0
!!$  slvu=0
!!$  smvv=0
!!$  slvv=0
!!$  smvw=0
!!$  slvw=0

  
