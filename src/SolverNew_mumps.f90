#include "precomp.h"
module mod_solver_new_mumps
  use mod_messages
  use mod_solver_new_def
  use mod_timers
  use mod_mpi
  
contains

#if MUMPS
  
  !*******************************************************************************  
  subroutine resol_MUMPS(mat,sol,rhs,solver,grid)
    !*******************************************************************************
    use mod_mpi
    use mod_solver_new_math
    use mod_struct_grid
    use mod_struct_solver
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables 
    !-------------------------------------------------------------------------------
    type(matrix_t), intent(in)                 :: mat
    type(solver_t), intent(inout)              :: solver
    type(grid_t), intent(in)                   :: grid
    real(8), dimension(mat%npt), intent(inout) :: sol,rhs
    !-------------------------------------------------------------------------------
    ! Local variables 
    !-------------------------------------------------------------------------------
    logical                                    :: lyse
    integer                                    :: i,j,ierr
    type(strct_mumps_t), save                  :: smumps
    real(8), dimension(mat%npt)                :: rsa
    !-------------------------------------------------------------------------------

    call compute_time(TIMER_START,"[sub] resol_MUMPS")
    
    if (solver%init) then
       nullify(smumps%par%irn)
       solver%init=.false.
    end if
    
    !-------------------------------------------------------------------------------
    lyse = .false.
    if (.not.associated(smumps%par%irn)) then
       lyse = .true.
    endif
    !-------------------------------------------------------------------------------
    smumps%par%comm = comm3d  ! MPI comm3d
    !-------------------------------------------------------------------------------
    if (lyse) then
       smumps%par%job = -1
       smumps%par%sym = 0
       smumps%par%par = 1
       call dmumps(smumps%par)
    endif

    if ( smumps%par%myid .eq. 0 ) then
       smumps%par%n  = mat%npt !! MPI NOMBRE INCONNUS GLOBAL
       smumps%par%nz = mat%nps
       if (lyse) then
          allocate( smumps%par%irn ( smumps%par%nz ) )
          allocate( smumps%par%jcn ( smumps%par%nz ) )
          allocate( smumps%par%a( smumps%par%nz ) )
          allocate( smumps%par%rhs ( smumps%par%n  ) )
       endif
       
       do i=1,mat%npt
          do j=mat%icof(i),mat%icof(i+1)-1
             smumps%par%irn(j)=i
             smumps%par%jcn(j)=mat%jcof(j)
             smumps%par%a(j)  =mat%coef(j)
          enddo
       enddo
       do i=1,mat%npt
          smumps%par%rhs(i)=rhs(i)
       enddo
    else
       print*,'error mumps',smumps%par%myid,ierr,mpi_comm_world
    endif

    smumps%par%icntl(7) = smumps%part
    smumps%par%icntl(3) = -1

    !-------------------------------------------------------------------------------
    ! Analyze
    !-------------------------------------------------------------------------------
    smumps%par%icntl(14) = smumps%mem
    if (lyse) then
       smumps%par%job = 1      
       call dmumps(smumps%par)
    endif
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Factorization
    !-------------------------------------------------------------------------------
    smumps%par%job = 2   
    call dmumps(smumps%par)
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Solving
    !-------------------------------------------------------------------------------
    smumps%par%job = 3      
    call dmumps(smumps%par)
    !-------------------------------------------------------------------------------
    sol=smumps%par%rhs
    !-------------------------------------------------------------------------------
    !End of mumps
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! residual
    !-------------------------------------------------------------------------------
    call matveke (mat,sol,rsa,solver,grid)
    rsa = rsa - rhs
    call pscalee(rsa,rsa,solver%res,mat%npt,mat%kic,mat%nic)
    solver%res = sqrt(solver%res+1.d-40)
    !--------------------------------------------------------------------------

    call compute_time(TIMER_END,"[sub] resol_MUMPS")
    
  end subroutine resol_MUMPS

#endif
  
end module mod_solver_new_mumps
