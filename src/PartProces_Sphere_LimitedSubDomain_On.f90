!===============================================================================
module Bib_VOFLag_Sphere_LimitedSubDomain_On
  !===============================================================================
  contains
  !-----------------------------------------------------------------------------  
  !> @author amine chadil, jorge cesar brandle de motta
  ! 
  !> @brief cette routine renseigne sur l'etat de la copie nbt de la sphere np par rapport 
  !! au domaine  du proc courant sans ses mailles de recouverement (la routine dit si 
  !! la sphere touche ou pas le domaine)  
  !
  !> @param[in,out]   vl     : la structure contenant toutes les informations
  !! relatives aux particules
  !> @param[in]   np     : numero de la particule dans vl
  !> @param[in]   nbt    : numero de la copie de la particule np
  !> @param [out] on_int : vrai si la sphere intersecte le domaine faux sinon.
  !-----------------------------------------------------------------------------
  subroutine Sphere_LimitedSubDomain_On(vl,np,nbt,on_int)
    !===============================================================================
    !modules
    !===============================================================================
    !-------------------------------------------------------------------------------
    !Modules de definition des structures et variables => src/mod/module_...f90
    !-------------------------------------------------------------------------------
    use Module_VOFLag_ParticlesDataStructure
    use Module_VOFLag_SubDomain_Data,         only : s_domaines_int
    use mod_Parameters,                       only : dim,deeptracking
    !-------------------------------------------------------------------------------

    !===============================================================================
    !declarations des variables
    !===============================================================================
    implicit none
    !-------------------------------------------------------------------------------
    !variables globales
    !-------------------------------------------------------------------------------
    type(struct_vof_lag), intent(inout)  :: vl
    integer, intent(in)                  :: np,nbt
    logical, intent(out)                 :: on_int
    !-------------------------------------------------------------------------------
    !variables locales 
    !-------------------------------------------------------------------------------
    double precision, dimension(3)       :: p,projete
    double precision                     :: distance
    integer                              :: nd
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'entree Sphere_LimitedSubDomain_On'
    !-------------------------------------------------------------------------------
    
    !-------------------------------------------------------------------------------
    ! initialisation de variables
    !-------------------------------------------------------------------------------
    p(1:dim) = vl%objet(np)%symper(nbt,1:dim)
    on_int = .false.
    distance = 0

    do nd = 1,dim
       !-------------------------------------------------------------------------------
       !Calcul du projete du centre de la particule sur la frontiere du sous-domaine du proc
       !-------------------------------------------------------------------------------
       if (p(nd) .lt. s_domaines_int(1,nd)) then
          projete(nd) = s_domaines_int(1,nd)
       else if (p(nd) .gt. s_domaines_int(2,nd)) then
          projete(nd) = s_domaines_int(2,nd)
       else 
          projete(nd) = p(nd)
       end if
      distance = distance + (p(nd)-projete(nd))**2
    end do
    
    !-------------------------------------------------------------------
    ! comparaison de cette distance avec le rayon de la sphere
    !-------------------------------------------------------------------
    if (distance .lt.  vl%objet(np)%sca(1)**2) on_int = .true.

    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'sortie Sphere_LimitedSubDomain_On'
    !-------------------------------------------------------------------------------
  end subroutine Sphere_LimitedSubDomain_On

  !===============================================================================
end module Bib_VOFLag_Sphere_LimitedSubDomain_On
!===============================================================================
  

