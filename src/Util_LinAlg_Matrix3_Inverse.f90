!===============================================================================
module Bib_VOFLag_Matrix3_Inverse
  !===============================================================================
  contains
  !-----------------------------------------------------------------------------  
  !> @author amine chadil
  ! 
  !> @brief calcul de la matrice inverse d'une matrice 3*3
  !
  !> @param [out] tab:
  !> @param [in]  n  :
  !-----------------------------------------------------------------------------
  subroutine Matrix3_Inverse(MAT,MAT_INV)
    !===============================================================================
    !modules
    !===============================================================================
    !-------------------------------------------------------------------------------
    !Modules de definition des structures et variables => src/mod/module_...f90
    !-------------------------------------------------------------------------------
    use mod_Parameters, only : deeptracking
    !-------------------------------------------------------------------------------
    !Modules de subroutines de la librairie RESPECT => src/Bib_VOFLag_...f90
    !-------------------------------------------------------------------------------
    use Bib_VOFLag_Matrix3_Determinant
    !-------------------------------------------------------------------------------

    !===============================================================================
    !declarations des variables
    !===============================================================================
    implicit none
    !-------------------------------------------------------------------------------
    !variables globales
    !-------------------------------------------------------------------------------
    real(8), dimension(3,3) , INTENT(IN)  :: MAT
    real(8), dimension(3,3) , INTENT(OUT) :: MAT_INV
    !-------------------------------------------------------------------------------
    !variables locales 
    !-------------------------------------------------------------------------------
    real(8)                              :: det_MAT
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'entree Matrix3_Inverse'
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! test de l'inversibilite de la matrice avec son determinant
    !-------------------------------------------------------------------------------
    call Matrix3_Determinant(MAT,det_MAT)
    det_NUL:if(ABS(det_MAT).lt.1.d-4) then
       write(6,*) "ATTENTION : det_MAT=",det_MAT
       stop "ATTENTION : MATRICE NON INVERSIBle dANS le CALCUL dU QUATERNION"
    end if det_NUL

    MAT_INV(1,1) =  ( MAT(2,2)*MAT(3,3) - MAT(3,2)*MAT(2,3) ) / (det_MAT+1D-40)
    MAT_INV(1,2) = -( MAT(1,2)*MAT(3,3) - MAT(3,2)*MAT(1,3) ) / (det_MAT+1D-40)
    MAT_INV(1,3) =  ( MAT(1,2)*MAT(2,3) - MAT(2,2)*MAT(1,3) ) / (det_MAT+1D-40)

    MAT_INV(2,1) = -( MAT(2,1)*MAT(3,3) - MAT(3,1)*MAT(2,3) ) / (det_MAT+1D-40)
    MAT_INV(2,2) =  ( MAT(1,1)*MAT(3,3) - MAT(3,1)*MAT(1,3) ) / (det_MAT+1D-40)
    MAT_INV(2,3) = -( MAT(1,1)*MAT(2,3) - MAT(2,1)*MAT(1,3) ) / (det_MAT+1D-40)

    MAT_INV(3,1) =  ( MAT(2,1)*MAT(3,2) - MAT(3,1)*MAT(2,2) ) / (det_MAT+1D-40)
    MAT_INV(3,2) = -( MAT(1,1)*MAT(3,2) - MAT(3,1)*MAT(1,2) ) / (det_MAT+1D-40)
    MAT_INV(3,3) =  ( MAT(1,1)*MAT(2,2) - MAT(2,1)*MAT(1,2) ) / (det_MAT+1D-40)

    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'sortie Matrix3_Inverse'
    !-------------------------------------------------------------------------------
  end subroutine Matrix3_Inverse

  !===============================================================================
end module Bib_VOFLag_Matrix3_Inverse
!===============================================================================