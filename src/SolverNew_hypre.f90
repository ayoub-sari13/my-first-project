#include "precomp.h"
module mod_solver_new_hypre
  use mod_messages
  use mod_solver_new_def
  use mod_timers
  
#define OPTIM_LOOPS 1
contains

#if HYPRE
  
  subroutine init_solver_HYPRE(solver,grid)
    use mod_struct_grid
    use mod_struct_solver
    implicit none
    include 'HYPREf.h'
    !--------------------------------------------------------------------------
    ! global variables
    !--------------------------------------------------------------------------
    type(grid_t), intent(in)      :: grid
    type(solver_t), intent(inout) :: solver
    !--------------------------------------------------------------------------
    ! local variables
    !--------------------------------------------------------------------------
    integer                       :: i
    !--------------------------------------------------------------------------

    call compute_time(TIMER_START,"[sub] init_solver_HYPRE")
    
    if (solver%init) then
       call init_stencil_HYPRE(solver,grid)
       call init_vector_HYPRE(solver)
       solver%init=.false.
    end if
    
    call compute_time(TIMER_END,"[sub] init_solver_HYPRE")
    
  end subroutine init_solver_HYPRE

  
  subroutine init_stencil_HYPRE(solver,grid)
    use mod_mpi 
    use mod_struct_solver
    use mod_struct_grid
    implicit none
    include 'HYPREf.h'
    !--------------------------------------------------------------------------
    ! global variables
    !--------------------------------------------------------------------------
    type(solver_t), intent(inout)             :: solver
    type(grid_t), intent(in)                  :: grid
    !--------------------------------------------------------------------------
    ! local variables
    !--------------------------------------------------------------------------
    integer                                   :: i,j,nvar 
    integer                                   :: imin,imax
    integer                                   :: lsx,lex
    integer                                   :: lsy,ley
    integer                                   :: lsz,lez
    !--------------------------------------------------------------------------
    ! local HYPRE types
    !--------------------------------------------------------------------------
    integer                                   :: ierr
    integer                                   :: size
    integer                                   :: part
    integer                                   :: nvars
    integer                                   :: var 
    integer, dimension(2*grid%dim+1)          :: array_var
    integer, parameter                        :: HYPRE_SSTRUCT_VARIABLE_CELL = 0
    integer, dimension(grid%dim)              :: periodicity
    integer, dimension(grid%dim)              :: ilower,iupper
    integer, dimension(grid%dim,2*grid%dim+1) :: offsets
    integer(8)                                :: stencil
    !--------------------------------------------------------------------------

    call compute_time(TIMER_START,"[sub] init_stencil_HYPRE")

    select case(solver%eqs)
    case(SOLVE_P,SOLVE_U,SOLVE_V,SOLVE_W)
       nvar=1
    case(SOLVE_UVP,SOLVE_UVW)
       nvar=3
    case(SOLVE_UVWP)
       nvar=4
    end select

    do i = 0,nvar-1
       
       nvars = 1
       part  = 0
       call HYPRE_SStructGridCreate(comm3d, grid%dim, solver%hypre(i)%npart, solver%hypre(i)%grid, ierr)
       
       solver%var=i
       select case (solver%var)
       case(SOLVE_P)
          lsx = grid%sx  ; lex = grid%ex
          lsy = grid%sy  ; ley = grid%ey
          lsz = grid%sz  ; lez = grid%ez
       case(SOLVE_U)
          lsx = grid%sxu ; lex = grid%exu
          lsy = grid%syu ; ley = grid%eyu
          lsz = grid%szu ; lez = grid%ezu
       case(SOLVE_V)
          lsx = grid%sxv ; lex = grid%exv
          lsy = grid%syv ; ley = grid%eyv
          lsz = grid%szv ; lez = grid%ezv
       case(SOLVE_W)
          lsx = grid%sxw ; lex = grid%exw
          lsy = grid%syw ; ley = grid%eyw
          lsz = grid%szw ; lez = grid%ezw
       end select

       !--------------------------------------------------------------------------
       ! periodicity
       !--------------------------------------------------------------------------
       periodicity = 0
       !--------------------------------------------------------------------------
       ! x-direction 
       !--------------------------------------------------------------------------
       call MPI_ALLREDUCE(lsx,imin,1,MPI_INTEGER,MPI_MIN,COMM3D,MPI_CODE)
       call MPI_ALLREDUCE(lex,imax,1,MPI_INTEGER,MPI_MAX,COMM3D,MPI_CODE)
       if (grid%periodic(1)) periodicity(1)=imax-imin+1
       !--------------------------------------------------------------------------
       ! y-direction 
       !--------------------------------------------------------------------------
       call MPI_ALLREDUCE(lsy,imin,1,MPI_INTEGER,MPI_MIN,COMM3D,MPI_CODE)
       call MPI_ALLREDUCE(ley,imax,1,MPI_INTEGER,MPI_MAX,COMM3D,MPI_CODE)
       if (grid%periodic(2)) periodicity(2)=imax-imin+1
       !--------------------------------------------------------------------------
       ! z-direction 
       !--------------------------------------------------------------------------
       if (grid%dim==3) then
          call MPI_ALLREDUCE(lsz,imin,1,MPI_INTEGER,MPI_MIN,COMM3D,MPI_CODE)
          call MPI_ALLREDUCE(lez,imax,1,MPI_INTEGER,MPI_MAX,COMM3D,MPI_CODE)
          if (grid%periodic(3)) periodicity(3)=imax-imin+1
       end if
       !--------------------------------------------------------------------------
       
       ilower(1) = lsx
       ilower(2) = lsy
       iupper(1) = lex
       iupper(2) = ley
       if (grid%dim==3) then
          ilower(3) = lsz
          iupper(3) = lez
       endif
       
       call HYPRE_SStructGridSetExtents(solver%hypre(i)%grid, part, ilower, iupper, ierr)
       call HYPRE_SStructGridSetVariables(solver%hypre(i)%grid, part, nvars, HYPRE_SSTRUCT_VARIABLE_CELL, ierr)
       
       call HYPRE_SStructGridSetPeriodic(solver%hypre(i)%grid, part, periodicity, ierr)
       call HYPRE_SStructGridAssemble(solver%hypre(i)%grid,ierr)
       
       size = 2*grid%dim+1
       call HYPRE_SStructStencilCreate(grid%dim, size, stencil, ierr)
       
       if (grid%dim==2) then
          offsets(1,1) =  0
          offsets(2,1) =  0
          offsets(1,2) = -1
          offsets(2,2) =  0
          offsets(1,3) =  1
          offsets(2,3) =  0
          offsets(1,4) =  0
          offsets(2,4) = -1
          offsets(1,5) =  0
          offsets(2,5) =  1
       else
          offsets(1,1) =  0
          offsets(2,1) =  0
          offsets(3,1) =  0
          offsets(1,2) = -1
          offsets(2,2) =  0
          offsets(3,2) =  0
          offsets(1,3) =  1
          offsets(2,3) =  0
          offsets(3,3) =  0
          offsets(1,4) =  0
          offsets(2,4) = -1
          offsets(3,4) =  0
          offsets(1,5) =  0
          offsets(2,5) =  1
          offsets(3,5) =  0
          offsets(1,6) =  0
          offsets(2,6) =  0
          offsets(3,6) = -1
          offsets(1,7) =  0
          offsets(2,7) =  0
          offsets(3,7) =  1
       endif

       var       = 0
       array_var = 0
       do j=1,size ! j=entry
          call HYPRE_SStructStencilSetEntry(stencil, j-1, offsets(1:grid%dim,j), array_var(j), ierr)  ! WTF ? useless ?
       enddo
       
       call HYPRE_SStructGraphCreate( comm3d, solver%hypre(i)%grid, solver%hypre(i)%graph, ierr)
       call HYPRE_SStructGraphSetObjectType(  solver%hypre(i)%graph, HYPRE_STRUCT, ierr)
       call HYPRE_SStructGraphSetStencil(     solver%hypre(i)%graph, part, var, stencil, ierr)
       call HYPRE_SStructGraphAssemble(       solver%hypre(i)%graph, ierr)
       call HYPRE_SStructStencilDestroy(      stencil, ierr)
       
    end do
    
    call compute_time(TIMER_END,"[sub] init_stencil_HYPRE")

  end subroutine init_stencil_HYPRE
  
  
  subroutine init_vector_HYPRE(solver)
    use mod_mpi
    use mod_struct_solver
    implicit none
    include 'HYPREf.h'
    !--------------------------------------------------------------------------
    ! global variables
    !--------------------------------------------------------------------------
    type(solver_t), intent(in) :: solver
    !--------------------------------------------------------------------------
    ! local variables
    !--------------------------------------------------------------------------
    integer                    :: i,nvar
    !--------------------------------------------------------------------------
    ! local HYPRE types
    !--------------------------------------------------------------------------
    integer                    :: ierr
    integer(8)                 :: x,y
    !--------------------------------------------------------------------------

    call compute_time(TIMER_START,"[sub] init_vector_HYPRE")

    select case(solver%eqs)
    case(SOLVE_P,SOLVE_U,SOLVE_V,SOLVE_W)
       nvar=1
    case(SOLVE_UVP,SOLVE_UVW)
       nvar=3
    case(SOLVE_UVWP)
       nvar=4
    end select

    do i=0,nvar-1

       call HYPRE_SStructVectorCreate(comm3d, solver%hypre(i)%grid, y, ierr)
       call HYPRE_SStructVectorCreate(comm3d, solver%hypre(i)%grid, x, ierr)
       
       call HYPRE_SStructVectorSetObjectTyp(x, HYPRE_STRUCT, ierr)
       call HYPRE_SStructVectorSetObjectTyp(y, HYPRE_STRUCT, ierr)
       
       call HYPRE_SStructVectorInitialize(y, ierr)
       call HYPRE_SStructVectorInitialize(x, ierr)
       
       call HYPRE_SStructVectorGetObject(y, solver%hypre(i)%structy, ierr)
       call HYPRE_SStructVectorGetObject(x, solver%hypre(i)%structx, ierr)

    end do

    call compute_time(TIMER_END,"[sub] init_vector_HYPRE")

  end subroutine init_vector_HYPRE


  subroutine make_setup_HYPRE(action,solver)
    use mod_struct_solver
    implicit none
    include 'HYPREf.h'
    !--------------------------------------------------------------------------
    ! global variables
    !--------------------------------------------------------------------------
    integer, intent(in)        :: action
    type(solver_t), intent(in) :: solver
    !--------------------------------------------------------------------------
    integer                    :: i,nvar
    integer                    :: ierr
    !--------------------------------------------------------------------------

    call compute_time(TIMER_START,"[sub] make_setup_HYPRE")
    
    select case(solver%eqs)
    case(SOLVE_P)
       nvar=1
    case(SOLVE_U,SOLVE_V,SOLVE_W)
       nvar=1
       call print_message("Solver is designer for FC or P cases only", MESSAGE_WARNING)
    case(SOLVE_UVP)
       nvar=3
    case(SOLVE_UVWP)
       nvar=4
    end select

    select case(action)
    case(CREATE) 
       
       select case(solver%precond)
       case(PRECOND_SMG)
          do i=0,nvar-1
             call HYPRE_StructSMGSetup(            &
                  & solver%hypre(i)%solver,        &
                  & solver%hypre(i)%matrixStorage, &
                  & solver%hypre(i)%structy,       &
                  & solver%hypre(i)%structx,       &
                  & ierr)
          end do
       case(PRECOND_PFMG)
          do i=0,nvar-1
             call HYPRE_StructPFMGSetup(           &
                  & solver%hypre(i)%solver,        &
                  & solver%hypre(i)%matrixStorage, &
                  & solver%hypre(i)%structy,       &
                  & solver%hypre(i)%structx,       &
                  & ierr)
          end do
       case DEFAULT
          call print_error_solver_choice(solver)
          stop
       end select

    case(DESTROY)
       
       select case(solver%precond)
       case(PRECOND_SMG)
          do i=0,nvar-1
             call HYPRE_StructSMGDestroy(   &
                  & solver%hypre(i)%solver, &
                  & ierr)
          end do
       case(PRECOND_PFMG)
          do i=0,nvar-1
             call HYPRE_StructPFMGDestroy(  &
                  & solver%hypre(i)%solver, &
                  & ierr)
          end do
       case DEFAULT
          call print_error_solver_choice(solver)
          stop
       end select
       
    end select

    call compute_time(TIMER_END,"[sub] make_setup_HYPRE")

  end subroutine make_setup_HYPRE


  subroutine make_matrix_puvw_HYPRE(action,system,solver,grid)
    use mod_mpi
    use mod_struct_grid
    use mod_struct_solver
    implicit none
    include 'HYPREf.h'
    !--------------------------------------------------------------------------
    ! global variables
    !--------------------------------------------------------------------------
    integer, intent(in)                   :: action
    type(system_t), intent(inout), target :: system
    type(solver_t), intent(inout)         :: solver
    type(grid_t), intent(in)              :: grid 
    !--------------------------------------------------------------------------
    ! local variables
    !--------------------------------------------------------------------------
    integer                               :: i,j,k,l,m,lvs
    integer                               :: nvar
    integer                               :: nb_no_ghost
    integer                               :: nb_jump
    integer                               :: nb_coeff
    integer                               :: lsx,lex,lsy,ley,lsz,lez
    integer                               :: lgsx,lgex,lgsy,lgey,lgsz,lgez
    integer                               :: lsxs,lexs,lsys,leys,lszs,lezs
    real(8), dimension(:), allocatable    :: values
    type(matrix_t), pointer               :: mat
    !--------------------------------------------------------------------------
    ! local HYPRE types
    !--------------------------------------------------------------------------
    integer                               :: ierr
    integer                               :: part
    integer                               :: nentries,nvalues
    integer, dimension(grid%dim)          :: ilower,iupper
    integer, dimension(2*grid%dim+1)      :: stencil
    integer                               :: var
    !--------------------------------------------------------------------------

    call compute_time(TIMER_START,"[sub] make_matrix_puvw_HYPRE")
   
    select case(solver%eqs)
    case(SOLVE_P,SOLVE_U,SOLVE_V,SOLVE_W)
       nvar=1
    case(SOLVE_UVP,SOLVE_UVW)
       nvar=3
    case(SOLVE_UVWP)
       nvar=4
    end select

    select case(action)
    case (CREATE)

       do m=0,nvar-1

          select case(m)
          case(SOLVE_P)
             nb_jump     = 0
             nb_coeff    = 5+2*(grid%dim-2)
             nb_no_ghost = grid%nb%no_ghost%p
             lgsx = grid%gsx ; lgex = grid%gex
             lgsy = grid%gsy ; lgey = grid%gey
             lgsz = grid%gsz ; lgez = grid%gez
             lsx  = grid%sx  ; lex  = grid%ex
             lsy  = grid%sy  ; ley  = grid%ey
             lsz  = grid%sz  ; lez  = grid%ez
             lsxs = grid%sxs ; lexs = grid%exs
             lsys = grid%sys ; leys = grid%eys
             lszs = grid%szs ; lezs = grid%ezs
          case(SOLVE_U)
             nb_jump     = 0
             nb_coeff    = 11+6*(grid%dim-2)
             nb_no_ghost = grid%nb%no_ghost%u
             lsx  = grid%sxu  ; lex  = grid%exu
             lsy  = grid%syu  ; ley  = grid%eyu
             lsz  = grid%szu  ; lez  = grid%ezu
             lsxs = grid%sxus ; lexs = grid%exus
             lsys = grid%syus ; leys = grid%eyus
             lszs = grid%szus ; lezs = grid%ezus
          case(SOLVE_V)
             nb_jump     = grid%nb%tot%u
             nb_coeff    = 11+6*(grid%dim-2)
             nb_no_ghost = grid%nb%no_ghost%v
             lsx  = grid%sxv  ; lex  = grid%exv
             lsy  = grid%syv  ; ley  = grid%eyv
             lsz  = grid%szv  ; lez  = grid%ezv
             lsxs = grid%sxvs ; lexs = grid%exvs
             lsys = grid%syvs ; leys = grid%eyvs
             lszs = grid%szvs ; lezs = grid%ezvs
          case(SOLVE_W)
             nb_jump     = grid%nb%tot%uv
             nb_coeff    = 11+6*(grid%dim-2)
             nb_no_ghost = grid%nb%no_ghost%w
             lsx  = grid%sxw  ; lex  = grid%exw
             lsy  = grid%syw  ; ley  = grid%eyw
             lsz  = grid%szw  ; lez  = grid%ezw
             lsxs = grid%sxws ; lexs = grid%exws
             lsys = grid%syws ; leys = grid%eyws
             lszs = grid%szws ; lezs = grid%ezws
          end select

          allocate(values((2*grid%dim+1)*nb_no_ghost))
          values = 0
          part   = 0
          var    = 0

          ilower(1) = lsx
          ilower(2) = lsy
          iupper(1) = lex
          iupper(2) = ley
          if (grid%dim==3) then 
             ilower(3) = lsz
             iupper(3) = lez
          endif
          
          call HYPRE_SStructMatrixCreate(comm3d, solver%hypre(m)%graph, solver%hypre(m)%matrix, ierr)
          call HYPRE_SStructMatrixSetObjectTyp(solver%hypre(m)%matrix, HYPRE_STRUCT, ierr)
          call HYPRE_SStructMatrixInitialize(solver%hypre(m)%matrix, ierr)

          do i = 1,2*grid%dim+1
             stencil(i) = i-1
          enddo

          nentries = 2*grid%dim+1
          nvalues  = nentries*nb_no_ghost

          select case(m)
          case(SOLVE_P)

             if (nvar==1) then
                ! projection
                mat => system%mat 
             else
                ! fully coupled
                mat => system%schur
             end if
             
             if (grid%dim==2) then
                lvs = 1+nb_coeff*nb_jump
                l   = 1
                do j=lsys,leys
                   do i=lsxs,lexs

                      if (nproc>1.and.(i==lsxs.or.i==lexs.or.j==lsys.or.j==leys)) then
                      else
                         values(l)   = mat%coef(lvs) 
                         values(l+1) = mat%coef(lvs+1)
                         values(l+2) = mat%coef(lvs+2)
                         values(l+3) = mat%coef(lvs+3)
                         values(l+4) = mat%coef(lvs+4)
                         if      (i==lgsx .and. j==lgsy) then
                            if      (     grid%periodic(1) .and.      grid%periodic(2)) then
                            else if (.not.grid%periodic(1) .and.      grid%periodic(2)) then
                               values(l+1) = 0
                               values(l+2) = 2*mat%coef(lvs+2)
                            else if (     grid%periodic(1) .and. .not.grid%periodic(2)) then
                               values(l+3) = 0
                               values(l+4) = 2*mat%coef(lvs+4)
                            else if (.not.grid%periodic(1) .and. .not.grid%periodic(2)) then
                               values(l+1) = 0
                               values(l+2) = 2*mat%coef(lvs+2)
                               values(l+3) = 0
                               values(l+4) = 2*mat%coef(lvs+4)
                            end if
                         else if (i==lgsx .and. j==lgey) then
                            if      (     grid%periodic(1) .and.      grid%periodic(2)) then
                            else if (.not.grid%periodic(1) .and.      grid%periodic(2)) then
                               values(l+1) = 0
                               values(l+2) = 2*mat%coef(lvs+2)
                            else if (     grid%periodic(1) .and. .not.grid%periodic(2)) then
                               values(l+3) = 2*mat%coef(lvs+3)
                               values(l+4) = 0
                            else if (.not.grid%periodic(1) .and. .not.grid%periodic(2)) then
                               values(l+1) = 0
                               values(l+2) = 2*mat%coef(lvs+2)
                               values(l+3) = 2*mat%coef(lvs+3)
                               values(l+4) = 0
                            end if
                         else if (i==lgex .and. j==lgsy) then
                            if      (     grid%periodic(1) .and.      grid%periodic(2)) then
                            else if (.not.grid%periodic(1) .and.      grid%periodic(2)) then
                               values(l+1) = 2*mat%coef(lvs+1)
                               values(l+2) = 0
                            else if (     grid%periodic(1) .and. .not.grid%periodic(2)) then
                               values(l+3) = 0
                               values(l+4) = 2*mat%coef(lvs+4)
                            else if (.not.grid%periodic(1) .and. .not.grid%periodic(2)) then
                               values(l+1) = 2*mat%coef(lvs+1)
                               values(l+2) = 0
                               values(l+3) = 0
                               values(l+4) = 2*mat%coef(lvs+4)
                            end if
                         else if (i==lgex .and. j==lgey) then
                            if      (     grid%periodic(1) .and.      grid%periodic(2)) then
                            else if (.not.grid%periodic(1) .and.      grid%periodic(2)) then
                               values(l+1) = 2*mat%coef(lvs+1)
                               values(l+2) = 0
                            else if (     grid%periodic(1) .and. .not.grid%periodic(2)) then
                               values(l+3) = 2*mat%coef(lvs+3)
                               values(l+4) = 0
                            else if (.not.grid%periodic(1) .and. .not.grid%periodic(2)) then
                               values(l+1) = 2*mat%coef(lvs+1)
                               values(l+2) = 0
                               values(l+3) = 2*mat%coef(lvs+3)
                               values(l+4) = 0
                            end if
                         else if (i==lgsx) then
                            if (grid%periodic(1)) then
                            else 
                               values(l+1) = 0
                               values(l+2) = 2*mat%coef(lvs+2)
                            end if
                         else if (i==lgex) then
                            if (grid%periodic(1)) then
                            else 
                               values(l+1) = 2*mat%coef(lvs+1)
                               values(l+2) = 0
                            end if
                         else if (j==lgsy) then
                            if (grid%periodic(2)) then
                            else 
                               values(l+3) = 0
                               values(l+4) = 2*mat%coef(lvs+4)
                            end if
                         else if (j==lgey) then
                            if (grid%periodic(2)) then
                            else 
                               values(l+3) = 2*mat%coef(lvs+3)
                               values(l+4) = 0
                            end if
                         end if
                         l=l+5
                      end if
                      lvs=lvs+nb_coeff
                   enddo
                enddo
             else
                lvs = 1+nb_coeff*nb_jump
                l   = 1
                do k=lszs,lezs
                   do j=lsys,leys
                      do i=lsxs,lexs

                         if (nproc>1.and.(i==lsxs.or.i==lexs.or.j==lsys.or.j==leys.or.k==lszs.or.k==lezs)) then
                         else 
                            values(l)   = mat%coef(lvs) 
                            values(l+1) = mat%coef(lvs+1)
                            values(l+2) = mat%coef(lvs+2)
                            values(l+3) = mat%coef(lvs+3)
                            values(l+4) = mat%coef(lvs+4)
                            values(l+5) = mat%coef(lvs+5)
                            values(l+6) = mat%coef(lvs+6)
                            if     (j==lgsy.and.i==lgsx.and.k==lgsz) then ! BOTTOM / LEFT / BACKWARD CORNER (1/8)
                               if      (     grid%periodic(1) .and.      grid%periodic(2) .and.      grid%periodic(3)) then
                               else if (.not.grid%periodic(1) .and.      grid%periodic(2) .and.      grid%periodic(3)) then
                               else if (     grid%periodic(1) .and. .not.grid%periodic(2) .and.      grid%periodic(3)) then
                               else if (.not.grid%periodic(1) .and. .not.grid%periodic(2) .and.      grid%periodic(3)) then
                               else if (     grid%periodic(1) .and.      grid%periodic(2) .and. .not.grid%periodic(3)) then
                               else if (.not.grid%periodic(1) .and.      grid%periodic(2) .and. .not.grid%periodic(3)) then
                               else if (     grid%periodic(1) .and. .not.grid%periodic(2) .and. .not.grid%periodic(3)) then
                               else if (.not.grid%periodic(1) .and. .not.grid%periodic(2) .and. .not.grid%periodic(3)) then
                                  values(l+1) = 0
                                  values(l+2) = 2*mat%coef(lvs+2)
                                  values(l+3) = 0
                                  values(l+4) = 2*mat%coef(lvs+4)
                                  values(l+5) = 0
                                  values(l+6) = 2*mat%coef(lvs+6)
                               end if
                            elseif (j==lgsy.and.i==lgex.and.k==lgsz) then ! BOTTOM / RIGHT / BACKWARD CORNER (2/8)
                               if      (     grid%periodic(1) .and.      grid%periodic(2) .and.      grid%periodic(3)) then
                               else if (.not.grid%periodic(1) .and.      grid%periodic(2) .and.      grid%periodic(3)) then
                               else if (     grid%periodic(1) .and. .not.grid%periodic(2) .and.      grid%periodic(3)) then
                               else if (.not.grid%periodic(1) .and. .not.grid%periodic(2) .and.      grid%periodic(3)) then
                               else if (     grid%periodic(1) .and.      grid%periodic(2) .and. .not.grid%periodic(3)) then
                               else if (.not.grid%periodic(1) .and.      grid%periodic(2) .and. .not.grid%periodic(3)) then
                               else if (     grid%periodic(1) .and. .not.grid%periodic(2) .and. .not.grid%periodic(3)) then
                               else if (.not.grid%periodic(1) .and. .not.grid%periodic(2) .and. .not.grid%periodic(3)) then
                                  values(l+2) = 0
                                  values(l+1) = 2*mat%coef(lvs+1)
                                  values(l+3) = 0
                                  values(l+4) = 2*mat%coef(lvs+4)
                                  values(l+5) = 0
                                  values(l+6) = 2*mat%coef(lvs+6)
                               end if
                            elseif (j==lgey.and.i==lgsx.and.k==lgsz) then ! TOP / LEFT / BACKWARD CORNER (3/8)
                               if      (     grid%periodic(1) .and.      grid%periodic(2) .and.      grid%periodic(3)) then
                               else if (.not.grid%periodic(1) .and.      grid%periodic(2) .and.      grid%periodic(3)) then
                               else if (     grid%periodic(1) .and. .not.grid%periodic(2) .and.      grid%periodic(3)) then
                               else if (.not.grid%periodic(1) .and. .not.grid%periodic(2) .and.      grid%periodic(3)) then
                               else if (     grid%periodic(1) .and.      grid%periodic(2) .and. .not.grid%periodic(3)) then
                               else if (.not.grid%periodic(1) .and.      grid%periodic(2) .and. .not.grid%periodic(3)) then
                               else if (     grid%periodic(1) .and. .not.grid%periodic(2) .and. .not.grid%periodic(3)) then
                               else if (.not.grid%periodic(1) .and. .not.grid%periodic(2) .and. .not.grid%periodic(3)) then
                                  values(l+1) = 0
                                  values(l+2) = 2*mat%coef(lvs+2)
                                  values(l+4) = 0
                                  values(l+3) = 2*mat%coef(lvs+3)
                                  values(l+5) = 0
                                  values(l+6) = 2*mat%coef(lvs+6)
                               end if
                            elseif (j==lgey.and.i==lgex.and.k==lgsz) then ! TOP / RIGHT / BACKWARD CORNER (4/8)
                               if      (     grid%periodic(1) .and.      grid%periodic(2) .and.      grid%periodic(3)) then
                               else if (.not.grid%periodic(1) .and.      grid%periodic(2) .and.      grid%periodic(3)) then
                               else if (     grid%periodic(1) .and. .not.grid%periodic(2) .and.      grid%periodic(3)) then
                               else if (.not.grid%periodic(1) .and. .not.grid%periodic(2) .and.      grid%periodic(3)) then
                               else if (     grid%periodic(1) .and.      grid%periodic(2) .and. .not.grid%periodic(3)) then
                               else if (.not.grid%periodic(1) .and.      grid%periodic(2) .and. .not.grid%periodic(3)) then
                               else if (     grid%periodic(1) .and. .not.grid%periodic(2) .and. .not.grid%periodic(3)) then
                               else if (.not.grid%periodic(1) .and. .not.grid%periodic(2) .and. .not.grid%periodic(3)) then
                                  values(l+2) = 0
                                  values(l+1) = 2*mat%coef(lvs+1)
                                  values(l+4) = 0
                                  values(l+3) = 2*mat%coef(lvs+3)
                                  values(l+5) = 0
                                  values(l+6) = 2*mat%coef(lvs+6)
                               end if
                            elseif (j==lgsy.and.i==lgsx.and.k==lgez) then ! BOTTOM / LEFT / FORWARD CORNER (5/8)
                               if      (     grid%periodic(1) .and.      grid%periodic(2) .and.      grid%periodic(3)) then
                               else if (.not.grid%periodic(1) .and.      grid%periodic(2) .and.      grid%periodic(3)) then
                               else if (     grid%periodic(1) .and. .not.grid%periodic(2) .and.      grid%periodic(3)) then
                               else if (.not.grid%periodic(1) .and. .not.grid%periodic(2) .and.      grid%periodic(3)) then
                               else if (     grid%periodic(1) .and.      grid%periodic(2) .and. .not.grid%periodic(3)) then
                               else if (.not.grid%periodic(1) .and.      grid%periodic(2) .and. .not.grid%periodic(3)) then
                               else if (     grid%periodic(1) .and. .not.grid%periodic(2) .and. .not.grid%periodic(3)) then
                               else if (.not.grid%periodic(1) .and. .not.grid%periodic(2) .and. .not.grid%periodic(3)) then
                                  values(l+1) = 0
                                  values(l+2) = 2*mat%coef(lvs+2)
                                  values(l+3) = 0
                                  values(l+4) = 2*mat%coef(lvs+4)
                                  values(l+6) = 0
                                  values(l+5) = 2*mat%coef(lvs+5)
                               end if
                            elseif (j==lgsy.and.i==lgex.and.k==lgez) then ! BOTTOM / RIGHT / FORWARD CORNER (6/8)
                               if      (     grid%periodic(1) .and.      grid%periodic(2) .and.      grid%periodic(3)) then
                               else if (.not.grid%periodic(1) .and.      grid%periodic(2) .and.      grid%periodic(3)) then
                               else if (     grid%periodic(1) .and. .not.grid%periodic(2) .and.      grid%periodic(3)) then
                               else if (.not.grid%periodic(1) .and. .not.grid%periodic(2) .and.      grid%periodic(3)) then
                               else if (     grid%periodic(1) .and.      grid%periodic(2) .and. .not.grid%periodic(3)) then
                               else if (.not.grid%periodic(1) .and.      grid%periodic(2) .and. .not.grid%periodic(3)) then
                               else if (     grid%periodic(1) .and. .not.grid%periodic(2) .and. .not.grid%periodic(3)) then
                               else if (.not.grid%periodic(1) .and. .not.grid%periodic(2) .and. .not.grid%periodic(3)) then
                                  values(l+2) = 0
                                  values(l+1) = 2*mat%coef(lvs+1)
                                  values(l+3) = 0
                                  values(l+4) = 2*mat%coef(lvs+4)
                                  values(l+6) = 0
                                  values(l+5) = 2*mat%coef(lvs+5)
                               end if
                            elseif (j==lgey.and.i==lgsx.and.k==lgez) then ! TOP / LEFT / FORWARD CORNER (7/8)
                               if      (     grid%periodic(1) .and.      grid%periodic(2) .and.      grid%periodic(3)) then
                               else if (.not.grid%periodic(1) .and.      grid%periodic(2) .and.      grid%periodic(3)) then
                               else if (     grid%periodic(1) .and. .not.grid%periodic(2) .and.      grid%periodic(3)) then
                               else if (.not.grid%periodic(1) .and. .not.grid%periodic(2) .and.      grid%periodic(3)) then
                               else if (     grid%periodic(1) .and.      grid%periodic(2) .and. .not.grid%periodic(3)) then
                               else if (.not.grid%periodic(1) .and.      grid%periodic(2) .and. .not.grid%periodic(3)) then
                               else if (     grid%periodic(1) .and. .not.grid%periodic(2) .and. .not.grid%periodic(3)) then
                               else if (.not.grid%periodic(1) .and. .not.grid%periodic(2) .and. .not.grid%periodic(3)) then
                                  values(l+1) = 0
                                  values(l+2) = 2*mat%coef(lvs+2)
                                  values(l+4) = 0
                                  values(l+3) = 2*mat%coef(lvs+3)
                                  values(l+6) = 0
                                  values(l+5) = 2*mat%coef(lvs+5)
                               end if
                            elseif (j==lgey.and.i==lgex.and.k==lgez) then ! TOP / RIGHT / FORWARD CORNER (8/8)
                               if      (     grid%periodic(1) .and.      grid%periodic(2) .and.      grid%periodic(3)) then
                               else if (.not.grid%periodic(1) .and.      grid%periodic(2) .and.      grid%periodic(3)) then
                               else if (     grid%periodic(1) .and. .not.grid%periodic(2) .and.      grid%periodic(3)) then
                               else if (.not.grid%periodic(1) .and. .not.grid%periodic(2) .and.      grid%periodic(3)) then
                               else if (     grid%periodic(1) .and.      grid%periodic(2) .and. .not.grid%periodic(3)) then
                               else if (.not.grid%periodic(1) .and.      grid%periodic(2) .and. .not.grid%periodic(3)) then
                               else if (     grid%periodic(1) .and. .not.grid%periodic(2) .and. .not.grid%periodic(3)) then
                               else if (.not.grid%periodic(1) .and. .not.grid%periodic(2) .and. .not.grid%periodic(3)) then
                                  values(l+2) = 0
                                  values(l+1) = 2*mat%coef(lvs+1)
                                  values(l+4) = 0
                                  values(l+3) = 2*mat%coef(lvs+3)
                                  values(l+6) = 0
                                  values(l+5) = 2*mat%coef(lvs+5)
                               end if
                            elseif (k==lgsz.and.i==lgsx) then ! BACKWARD / LEFT EDGE (1/12)
                               if      (     grid%periodic(1) .and.      grid%periodic(3)) then
                               else if (.not.grid%periodic(1) .and.      grid%periodic(3)) then
                               else if (     grid%periodic(1) .and. .not.grid%periodic(3)) then
                               else if (.not.grid%periodic(1) .and. .not.grid%periodic(3)) then
                                  values(l+1) = 0
                                  values(l+2) = 2*mat%coef(lvs+2)
                                  values(l+5) = 0
                                  values(l+6) = 2*mat%coef(lvs+6)
                               end if
                            elseif (k==lgsz.and.i==lgex) then ! BACKWARD / RIGHT EDGE (2/12)
                               if      (     grid%periodic(1) .and.      grid%periodic(3)) then
                               else if (.not.grid%periodic(1) .and.      grid%periodic(3)) then
                               else if (     grid%periodic(1) .and. .not.grid%periodic(3)) then
                               else if (.not.grid%periodic(1) .and. .not.grid%periodic(3)) then
                                  values(l+2) = 0
                                  values(l+1) = 2*mat%coef(lvs+1)
                                  values(l+5) = 0
                                  values(l+6) = 2*mat%coef(lvs+6)
                               end if
                            elseif (k==lgsz.and.j==lgsy) then ! BACKWARD / BOTTOM EDGE (3/12)
                               if      (     grid%periodic(2) .and.      grid%periodic(3)) then
                               else if (.not.grid%periodic(2) .and.      grid%periodic(3)) then
                               else if (     grid%periodic(2) .and. .not.grid%periodic(3)) then
                               else if (.not.grid%periodic(2) .and. .not.grid%periodic(3)) then
                                  values(l+3) = 0
                                  values(l+4) = 2*mat%coef(lvs+4)
                                  values(l+5) = 0
                                  values(l+6) = 2*mat%coef(lvs+6)
                               end if
                            elseif (k==lgsz.and.j==lgey) then ! BACKWARD / TOP EDGE (4/12)
                               if      (     grid%periodic(2) .and.      grid%periodic(3)) then
                               else if (.not.grid%periodic(2) .and.      grid%periodic(3)) then
                               else if (     grid%periodic(2) .and. .not.grid%periodic(3)) then
                               else if (.not.grid%periodic(2) .and. .not.grid%periodic(3)) then
                                  values(l+4) = 0
                                  values(l+3) = 2*mat%coef(lvs+3)
                                  values(l+5) = 0
                                  values(l+6) = 2*mat%coef(lvs+6)
                               end if
                            elseif (i==lgsx.and.j==lgsy) then ! LEFT / BOTTOM EDGE (5/12)
                               if      (     grid%periodic(1) .and.      grid%periodic(2)) then
                               else if (.not.grid%periodic(1) .and.      grid%periodic(2)) then
                               else if (     grid%periodic(1) .and. .not.grid%periodic(2)) then
                               else if (.not.grid%periodic(1) .and. .not.grid%periodic(2)) then
                                  values(l+1) = 0
                                  values(l+2) = 2*mat%coef(lvs+2)
                                  values(l+3) = 0
                                  values(l+4) = 2*mat%coef(lvs+4)
                               end if
                            elseif (i==lgex.and.j==lgsy) then ! RIGHT / BOTTOM EDGE (6/12)
                               if      (     grid%periodic(1) .and.      grid%periodic(2)) then
                               else if (.not.grid%periodic(1) .and.      grid%periodic(2)) then
                               else if (     grid%periodic(1) .and. .not.grid%periodic(2)) then
                               else if (.not.grid%periodic(1) .and. .not.grid%periodic(2)) then
                                  values(l+2) = 0
                                  values(l+1) = 2*mat%coef(lvs+1)
                                  values(l+3) = 0
                                  values(l+4) = 2*mat%coef(lvs+4)
                               end if
                            elseif (i==lgsx.and.j==lgey) then ! LEFT / TOP EDGE (7/12)
                               if      (     grid%periodic(1) .and.      grid%periodic(2)) then
                               else if (.not.grid%periodic(1) .and.      grid%periodic(2)) then
                               else if (     grid%periodic(1) .and. .not.grid%periodic(2)) then
                               else if (.not.grid%periodic(1) .and. .not.grid%periodic(2)) then
                                  values(l+1) = 0
                                  values(l+2) = 2*mat%coef(lvs+2)
                                  values(l+4) = 0
                                  values(l+3) = 2*mat%coef(lvs+3)
                               end if
                            elseif (i==lgex.and.j==lgey) then ! RIGHT / TOP EDGE (8/12)
                               if      (     grid%periodic(1) .and.      grid%periodic(2)) then
                               else if (.not.grid%periodic(1) .and.      grid%periodic(2)) then
                               else if (     grid%periodic(1) .and. .not.grid%periodic(2)) then
                               else if (.not.grid%periodic(1) .and. .not.grid%periodic(2)) then
                                  values(l+2) = 0
                                  values(l+1) = 2*mat%coef(lvs+1)
                                  values(l+4) = 0
                                  values(l+3) = 2*mat%coef(lvs+3)
                               end if
                            elseif (k==lgez.and.i==lgsx) then ! FORWARD / LEFT EDGE (9/12)
                               if      (     grid%periodic(1) .and.      grid%periodic(3)) then
                               else if (.not.grid%periodic(1) .and.      grid%periodic(3)) then
                               else if (     grid%periodic(1) .and. .not.grid%periodic(3)) then
                               else if (.not.grid%periodic(1) .and. .not.grid%periodic(3)) then
                                  values(l+1) = 0
                                  values(l+2) = 2*mat%coef(lvs+2)
                                  values(l+6) = 0
                                  values(l+5) = 2*mat%coef(lvs+5)
                               end if
                            elseif (k==lgez.and.i==lgex) then ! FORWARD / RIGHT EDGE (10/12)
                               if      (     grid%periodic(1) .and.      grid%periodic(3)) then
                               else if (.not.grid%periodic(1) .and.      grid%periodic(3)) then
                               else if (     grid%periodic(1) .and. .not.grid%periodic(3)) then
                               else if (.not.grid%periodic(1) .and. .not.grid%periodic(3)) then
                                  values(l+2) = 0
                                  values(l+1) = 2*mat%coef(lvs+1)
                                  values(l+6) = 0
                                  values(l+5) = 2*mat%coef(lvs+5)
                               end if
                            elseif (k==lgez.and.j==lgsy) then ! FORWARD / BOTTOM EDGE (11/12)
                               if      (     grid%periodic(2) .and.      grid%periodic(3)) then
                               else if (.not.grid%periodic(2) .and.      grid%periodic(3)) then
                               else if (     grid%periodic(2) .and. .not.grid%periodic(3)) then
                               else if (.not.grid%periodic(2) .and. .not.grid%periodic(3)) then
                                  values(l+3) = 0
                                  values(l+4) = 2*mat%coef(lvs+4)
                                  values(l+6) = 0
                                  values(l+5) = 2*mat%coef(lvs+5)
                               end if
                            elseif (k==lgez.and.j==lgey) then ! FORWARD / TOP EDGE (12/12)
                               if      (     grid%periodic(2) .and.      grid%periodic(3)) then
                               else if (.not.grid%periodic(2) .and.      grid%periodic(3)) then
                               else if (     grid%periodic(2) .and. .not.grid%periodic(3)) then
                               else if (.not.grid%periodic(2) .and. .not.grid%periodic(3)) then
                                  values(l+4) = 0
                                  values(l+3) = 2*mat%coef(lvs+3)
                                  values(l+6) = 0
                                  values(l+5) = 2*mat%coef(lvs+5)
                               end if
                            elseif (i==lgsx) then ! LEFT FACE (1/6)
                               if (grid%periodic(1)) then
                               else 
                                  values(l+1) = 0
                                  values(l+2) = 2*mat%coef(lvs+2)
                               end if
                            elseif (i==lgex) then ! RIGHT FACE (2/6)
                               if (grid%periodic(1)) then
                               else 
                                  values(l+2) = 0
                                  values(l+1) = 2*mat%coef(lvs+1)
                               end if
                            elseif (j==lgsy) then ! BOTTOM FACE (3/6)
                               if (grid%periodic(2)) then
                               else 
                                  values(l+3) = 0
                                  values(l+4) = 2*mat%coef(lvs+4)
                               end if
                            elseif (j==lgey) then ! TOP FACE (4/6)
                               if (grid%periodic(2)) then
                               else 
                                  values(l+4) = 0
                                  values(l+3) = 2*mat%coef(lvs+3)
                               end if
                            elseif (k==lgsz) then ! BACKWARD FACE (5/6)
                               if (grid%periodic(3)) then
                               else 
                                  values(l+5) = 0
                                  values(l+6) = 2*mat%coef(lvs+6)
                               end if
                            elseif (k==lgez) then ! FORWARD FACE (6/6)
                               if (grid%periodic(3)) then
                               else 
                                  values(l+6) = 0
                                  values(l+5) = 2*mat%coef(lvs+5)
                               end if
                            end if
                            l=l+7
                         end if
                         lvs=lvs+nb_coeff
                      enddo
                   enddo
                enddo
             endif

          case(SOLVE_U:SOLVE_W)

             mat => system%mat

             if (grid%dim==2) then
                lvs = 1+nb_coeff*nb_jump
                l   = 1
                do j=lsys,leys
                   do i=lsxs,lexs
                      if (nproc>1.and.(i==lsxs.or.i==lexs.or.j==lsys.or.j==leys)) then
                      else 
                         values(l)   = mat%coef(lvs) 
                         values(l+1) = mat%coef(lvs+1)
                         values(l+2) = mat%coef(lvs+2)
                         values(l+3) = mat%coef(lvs+3)
                         values(l+4) = mat%coef(lvs+4)
                         l           = l+5
                      end if
                      lvs=lvs+nb_coeff
                   enddo
                enddo
             else 
                lvs = 1+nb_coeff*nb_jump
                l   = 1
                do k=lszs,lezs
                   do j=lsys,leys
                      do i=lsxs,lexs
                         if (nproc>1.and.(i==lsxs.or.i==lexs.or.j==lsys.or.j==leys.or.k==lszs.or.k==lezs)) then
                         else 
                            values(l)   = mat%coef(lvs) 
                            values(l+1) = mat%coef(lvs+1)
                            values(l+2) = mat%coef(lvs+2)
                            values(l+3) = mat%coef(lvs+3)
                            values(l+4) = mat%coef(lvs+4)
                            values(l+5) = mat%coef(lvs+5)
                            values(l+6) = mat%coef(lvs+6)
                            l           = l+7
                         end if
                         lvs=lvs+nb_coeff
                      enddo
                   enddo
                enddo
             endif

          end select

          call HYPRE_SStructMatrixSetBoxValues( &
               & solver%hypre(m)%matrix,        &
               & part,                          &
               & ilower,                        &
               & iupper,                        &
               & var,                           &
               & nentries,                      &
               & stencil,                       &
               & values,                        &
               & ierr)
          
          call HYPRE_SStructMatrixAssemble(     &
               & solver%hypre(m)%matrix,        &
               & ierr)
          
          call HYPRE_SStructMatrixGetObject(    &
               & solver%hypre(m)%matrix,        &
               & solver%hypre(m)%matrixStorage, &
               & ierr)

          deallocate(values)
          
       end do

    case (DESTROY)

       do m=0,nvar-1
          call HYPRE_SStructMatrixDestroy(solver%hypre(m)%matrix,ierr)
       end do
          
    end select
    
    call compute_time(TIMER_END,"[sub] make_matrix_puvw_HYPRE")

  end subroutine make_matrix_puvw_HYPRE

  
  subroutine create_precond_multi_grid_HYPRE(solver)
    use mod_mpi
    use mod_struct_solver
    implicit none
    include 'HYPREf.h'
    !--------------------------------------------------------------------------
    ! global variables
    !--------------------------------------------------------------------------
    type(solver_t), intent(in) :: solver
    !--------------------------------------------------------------------------
    integer                    :: i,nvar
    !--------------------------------------------------------------------------
    ! local HYPRE types
    !--------------------------------------------------------------------------
    integer                    :: ierr
    !--------------------------------------------------------------------------

    call compute_time(TIMER_START,"[sub] create_precond_multi_grid_HYPRE")

    select case(solver%eqs)
    case(SOLVE_P,SOLVE_U,SOLVE_V,SOLVE_W)
       nvar=1
    case(SOLVE_UVP,SOLVE_UVW)
       nvar=3
    case(SOLVE_UVWP)
       nvar=4
    end select

!!$      !> Maximum number of multigrid grid levels --> maxLvL
!!$      integer(C_INT) :: max_levels
!!$      !!
!!$ 
!!$      !> Initial guess --> 0
!!$      integer        :: zero_guess      !! Do not set
!!$      !integer       :: zero_guess = 0  !! Use zero initial guess
!!$      !integer       :: zero_guess = 1  !! Use a non-zero initial guess
!!$      !!
!!$      !> Relaxation type --> typeRelax
!!$      !integer(C_INT) :: relax_type = 0 !! Jacobi
!!$      integer(C_INT)  :: relax_type = 1 !! Weighted Jacobi
!!$      !integer(C_INT) :: relax_type = 2 !! Red-Black Gauss-Seidel
!!$      !integer(C_INT) :: relax_type = 3 !! Red-Black Gauss-Seidel, non-symmetric
!!$      !!
!!$      !> Corase grid operator type
!!$      integer(C_INT)  :: rap_type = 0  !! Galerkin
!!$      !integer(C_INT) :: rap_type = 1  !! Non-Galerkin ParFlow operatos
!!$      !integer(C_INT) :: rap_type = 2  !! Galerkin, general operators
!!$
!!$      Current operators set by rap type are:
!!$      0 – Galerkin (default)
!!$      1 – non-Galerkin 5-pt or 7-pt stencils
!!$      Both operators are constructed algebraically. The non-Galerkin option maintains a 5-pt stencil in 2D and
!!$      a 7-pt stencil in 3D on all grid levels. The stencil coefficients are computed by averaging techniques.
!!$      
!!$      !> Number of relaxation sweeps before coarse-grid correction --> nb_pre_post_relax
!!$      integer(C_INT) :: num_pre_relax = 1
!!$      
!!$      !> Number of relaxation sweeps after coarse-grid correction --> nb_pre_post_relax
!!$      integer(C_INT) :: num_post_relax = 1
!!$      
!!$      !> Skip relaxation on certain grids for isotropic problems --> skipRelax
!!$      integer(C_INT) :: skip_relax = 0   1 anisentropic
!!$      !> @}
!!$
!!$
!!$      smg
!!$
!!$      !> Initial guess
!!$      integer        :: zero_guess      !! Do not set
!!$      !integer        :: zero_guess = 0  !! Use zero initial guess
!!$      !integer        :: zero_guess = 1  !! Use a non-zero initial guess
!!$      !!
!!$      !> Number of relaxation sweeps before coarse-grid correction
!!$      integer(C_INT) :: num_pre_relax = 1
!!$      !!
!!$      !> Number of relaxation sweeps after coarse-grid correction
!!$      integer(C_INT) :: num_post_relax = 1
!!$      !!
!!$      !> @}



    select case(solver%precond)
    case(PRECOND_SMG)
       do i=0,nvar-1
          call HYPRE_StructSMGCreate( comm3d,      solver%hypre(i)%solver,                             ierr)
          call HYPRE_StructSMGSetMemoryUse(        solver%hypre(i)%solver, 0,                          ierr)
          call HYPRE_StructSMGSetMaxIter(          solver%hypre(i)%solver, solver%hypre(i)%MaxIt,      ierr)
          call HYPRE_StructSMGSetTol(              solver%hypre(i)%solver, solver%hypre(i)%Tol,        ierr)
          if (solver%hypre(i)%zeroGuess==0) then
             call HYPRE_StructSMGSetZeroGuess(     solver%hypre(i)%solver,                             ierr)
          else
             call HYPRE_StructSMGSetNonZeroGuess(  solver%hypre(i)%solver,                             ierr)
          end if
          call HYPRE_StructSMGSetNumPreRelax(      solver%hypre(i)%solver, solver%hypre(i)%nPreRelax,  ierr)
          call HYPRE_StructSMGSetNumPostRelax(     solver%hypre(i)%solver, solver%hypre(i)%nPostRelax, ierr)
          call HYPRE_StructSMGSetPrintLevel(       solver%hypre(i)%solver, solver%hypre(i)%printLvL,   ierr)
          call HYPRE_StructSMGSetLogging(          solver%hypre(i)%solver, solver%hypre(i)%Log,        ierr)
       end do
    case(PRECOND_PFMG)
       do i=0,nvar-1
          call HYPRE_StructPFMGCreate( comm3d,     solver%hypre(i)%solver,                             ierr)
          call HYPRE_StructPFMGSetMaxIter(         solver%hypre(i)%solver, solver%hypre(i)%MaxIt,      ierr)
          call HYPRE_StructPFMGSetTol(             solver%hypre(i)%solver, solver%hypre(i)%Tol,        ierr)
          call HYPRE_StructPFMGsetmaxlevels(       solver%hypre(i)%solver, solver%hypre(i)%maxLvL,     ierr)
          if (solver%hypre(i)%zeroGuess==0) then
             call HYPRE_StructPFMGSetZeroGuess(    solver%hypre(i)%solver,                             ierr)
          else
             call HYPRE_StructPFMGSetNoNZeroGuess( solver%hypre(i)%solver,                             ierr)
          end if
          call HYPRE_StructPFMGSetNumPreRelax(     solver%hypre(i)%solver, solver%hypre(i)%nPreRelax,  ierr)
          call HYPRE_StructPFMGSetNumPostRelax(    solver%hypre(i)%solver, solver%hypre(i)%nPostRelax, ierr)
          call HYPRE_StructPFMGSetRAPType(         solver%hypre(i)%solver, solver%hypre(i)%rap,        ierr)
          call HYPRE_StructPFMGsetrelaxtype(       solver%hypre(i)%solver, solver%hypre(i)%typeRelax,  ierr) 
          call HYPRE_StructPFMGsetskiprelax(       solver%hypre(i)%solver, solver%hypre(i)%skipRelax,  ierr)
          call HYPRE_StructPFMGSetPrintLevel(      solver%hypre(i)%solver, solver%hypre(i)%printLvL,   ierr)
          call HYPRE_StructPFMGSetLogging(         solver%hypre(i)%solver, solver%hypre(i)%Log,        ierr)
       end do
    case DEFAULT
       call print_error_solver_choice(solver)
       stop
    end select

    call compute_time(TIMER_END,"[sub] create_precond_multi_grid_HYPRE")

  end subroutine create_precond_multi_grid_HYPRE


  subroutine precond_uvwp_with_SMG_PFMG_HYPRE(system,solver,grid,msa,qsa)
    use mod_mpi
    use mod_struct_grid
    use mod_struct_solver
    use mod_solver_new_math
    use mod_solver_new_num
    implicit none
    include 'HYPREf.h'
    !--------------------------------------------------------------------------
    ! global variables
    !--------------------------------------------------------------------------
    type(grid_t), intent(in)                          :: grid
    type(system_t), intent(in)                        :: system
    type(solver_t), intent(in)                        :: solver
    real(8), dimension(system%mat%npt), intent(inout) :: msa,qsa
    !--------------------------------------------------------------------------
    integer                                           :: i,j,k,l,m,ls,lvp,ii,jj,kk
    !--------------------------------------------------------------------------
    ! local HYPRE types
    !--------------------------------------------------------------------------
    integer                                           :: ierr
    integer                                           :: switch2D3D
    integer                                           :: part
    integer                                           :: var
    integer, dimension(grid%dim)                      :: ilower,iupper
    integer(8)                                        :: x,y
    integer(8)                                        :: ssx,ssy
    real(8), dimension(:), allocatable                :: valuesu,valuesv,valuesw,valuesp
    real(8), dimension(:), allocatable                :: ysa,zsa
    real(8), dimension(:), allocatable                :: w1,w2
    real(8), dimension(:), allocatable                :: bp
    real(8), dimension(:), allocatable                :: xp,px,p,p1
    real(8), dimension(:), allocatable                :: visco_p
    !--------------------------------------------------------------------------

    call compute_time(TIMER_START,"[sub] precond_uvwp_with_SMG_PFMG_HYPRE")

    !--------------------------------------------------------------------------
    allocate(ysa(grid%nb%tot%u))  ; ysa = 0
    allocate(w1(grid%nb%tot%v))   ; w1  = 0
    allocate(bp(grid%nb%tot%uvw)) ; bp  = 0
    !--------------------------------------------------------------------------
    ! Pressure variable
    !--------------------------------------------------------------------------
    allocate(visco_p(grid%nb%no_ghost%p)) ; visco_p = 0
    allocate(xp(grid%nb%no_ghost%p))      ; xp      = 0
    allocate(valuesp(grid%nb%no_ghost%p)) ; valuesp = 0
    allocate(p(grid%nb%tot%p))            ; p       = 0
    allocate(px(grid%nb%tot%p))           ; px      = 0
    allocate(p1(grid%nb%tot%p))           ; p1      = 0
    !--------------------------------------------------------------------------
    ! Velocity u,v,w variable
    !--------------------------------------------------------------------------
    allocate(valuesu(grid%nb%no_ghost%u))    ; valuesu = 0
    allocate(valuesv(grid%nb%no_ghost%v))    ; valuesv = 0
    if (grid%dim==3) then
       allocate(valuesw(grid%nb%no_ghost%w)) ; valuesw = 0
       allocate(w2(grid%nb%tot%w))           ; w2      = 0
       allocate(zsa(grid%nb%tot%uv))         ; zsa     = 0
    endif
    !--------------------------------------------------------------------------

    msa  = qsa
    part = 0
    var  = 0
    
    switch2D3D = grid%dim-2

    !--------------------------------------------------------------------------
    ! Approximate inverse Schur complement (Pressure Connvection Diffusion)
    !--------------------------------------------------------------------------
    
#if OPTIM_LOOPS
    l = 0
    do kk = grid%sz,grid%ez
       k = (kk-grid%szs)*switch2D3D
       do jj = grid%sy,grid%ey
          j = jj-grid%sys
          do ii = grid%sx,grid%ex
             i      = ii-grid%sxs
             ls     = i+j*(grid%exs-grid%sxs+1)+k*(grid%exs-grid%sxs+1)*(grid%eys-grid%sys+1)+1
             lvp    = ls+grid%nb%tot%uvw
             l      = l+1
             p(l)   = qsa(lvp)/(grid%dx(ii)*grid%dy(jj)*grid%dz(kk))
             p1(ls) = p(l)
          end do
       end do
    end do
#else 
    l = 0
    do kk = grid%sz,grid%ez
       do jj = grid%sy,grid%ey
          do ii = grid%sx,grid%ex
             i      = ii-grid%sxs
             j      = jj-grid%sys
             k      = kk-grid%szs
             ls     = i+j*(grid%exs-grid%sxs+1)+switch2D3D*k*(grid%exs-grid%sxs+1)*(grid%eys-grid%sys+1)+1
             lvp    = ls+grid%nb%tot%uvw
             l      = l+1
             p(l)   = qsa(lvp)/(grid%dx(ii)*grid%dy(jj)*grid%dz(kk))
             p1(ls) = p(l)
          end do
       end do
    end do
#endif

    call solver_comm_mpi(p1,solver,grid,SOLVE_P)
    call matveke_tri_sup(system%pcd,grid%nb%tot%p,p1,px,solver,grid)
    
    call HYPRE_SStructVectorCreate(comm3d, solver%hypre(0)%grid, y, ierr)
    call HYPRE_SStructVectorCreate(comm3d, solver%hypre(0)%grid, x, ierr)
    
    call HYPRE_SStructVectorSetObjectTyp(x, HYPRE_STRUCT, ierr)
    call HYPRE_SStructVectorSetObjectTyp(y, HYPRE_STRUCT, ierr)
    
    call HYPRE_SStructVectorInitialize(y, ierr)
    call HYPRE_SStructVectorInitialize(x, ierr)
    
    ilower(1) = grid%sx
    ilower(2) = grid%sy
    iupper(1) = grid%ex
    iupper(2) = grid%ey
    if (grid%dim==3) then 
       ilower(3) = grid%sz
       iupper(3) = grid%ez
    endif
    
    valuesp = 0
    call HYPRE_SStructVectorSetBoxValues(x,  part,  ilower,  iupper,  var,  valuesp,  ierr)

#if OPTIM_LOOPS
    valuesp = 0
    l       = 0
    do kk = grid%sz,grid%ez
       k = (kk-grid%szs)*switch2D3D
       do jj = grid%sy,grid%ey
          j = jj-grid%sys
          do ii = grid%sx,grid%ex
             i          = ii-grid%sxs
             ls         = i+j*(grid%exs-grid%sxs+1)+k*(grid%exs-grid%sxs+1)*(grid%eys-grid%sys+1)+1
             l          = l+1
             valuesp(l) = px(ls)
          end do
       end do
    end do
#else 
    valuesp = 0
    l       = 0
    do kk = grid%sz,grid%ez
       do jj = grid%sy,grid%ey
          do ii = grid%sx,grid%ex
             i          = ii-grid%sxs
             j          = jj-grid%sys
             k          = kk-grid%szs
             ls         = i+j*(grid%exs-grid%sxs+1)+switch2D3D*k*(grid%exs-grid%sxs+1)*(grid%eys-grid%sys+1)+1
             l          = l+1
             valuesp(l) = px(ls)
          end do
       end do
    end do
#endif

    
    call HYPRE_SStructVectorSetBoxValues(y, part, ilower, iupper, var, valuesp, ierr)
    
    call HYPRE_SStructVectorAssemble(x, ierr)
    call HYPRE_SStructVectorAssemble(y, ierr)
    
    call HYPRE_SStructVectorGetObject(y, ssy, ierr)
    call HYPRE_SStructVectorGetObject(x, ssx, ierr)

    select case(solver%precond)
    case(PRECOND_PFMG)
       call HYPRE_StructPFMGSolve(solver%hypre(0)%solver, solver%hypre(0)%matrixStorage, ssy, ssx, ierr)
       call HYPRE_StructPFMGGetNumIteration(solver%hypre(0)%solver, solver%hypre(0)%it, ierr)
       call HYPRE_StructPFMGGetFinalRelativ(solver%hypre(0)%solver, solver%hypre(0)%res, ierr)
    case(PRECOND_SMG)
       call HYPRE_StructSMGSolve(solver%hypre(0)%solver, solver%hypre(0)%matrixStorage, ssy, ssx, ierr)
       call HYPRE_StructSMGGetNumIterations(solver%hypre(0)%solver, solver%hypre(0)%it, ierr)
       call HYPRE_StructSMGGetFinalRelative(solver%hypre(0)%solver, solver%hypre(0)%res, ierr)
    case DEFAULT
       call print_error_solver_choice(solver)
       stop
    end select


    xp = 0
    call HYPRE_SStructVectorGather(x, ierr)
    call HYPRE_SStructVectorGetBoxValues(x, part, ilower, iupper, var, xp, ierr)

    call HYPRE_SStructVectorDestroy(y, ierr)
    call HYPRE_SStructVectorDestroy(x, ierr)
    
    do i=1,grid%nb%no_ghost%p
       if (abs(xp(i)) < THRESHOLD_SCHUR_PCD_TO_ZERO ) xp(i)=0  ! WFT others ?
    enddo

    l = 0
    do kk = grid%sz,grid%ez
       do jj = grid%sy,grid%ey
          do ii = grid%sx,grid%ex
             l          = l+1
             visco_p(l) = system%array3d_1(ii,jj,kk)
          end do
       end do
    end do
    
#if OPTIM_LOOPS
    p = 0
    l = 0
    do kk = grid%sz,grid%ez
       k = (kk-grid%szs)*switch2D3D
       do jj = grid%sy,grid%ey
          j = jj-grid%sys
          do ii = grid%sx,grid%ex
             i         = ii-grid%sxs
             ls        = i+j*(grid%exs-grid%sxs+1)+k*(grid%exs-grid%sxs+1)*(grid%eys-grid%sys+1)+1
             lvp       = ls+grid%nb%tot%uvw
             l         = l+1
             msa(lvp)  = xp(l)+p1(ls)*visco_p(l)
             p(ls)     = msa(lvp)
          end do
       end do
    end do
#else
    p = 0
    l = 0
    do kk = grid%sz,grid%ez
       do jj = grid%sy,grid%ey
          do ii = grid%sx,grid%ex
             i         = ii-grid%sxs
             j         = jj-grid%sys
             k         = kk-grid%szs
             ls        = i+j*(grid%exs-grid%sxs+1)+switch2D3D*k*(grid%exs-grid%sxs+1)*(grid%eys-grid%sys+1)+1
             lvp       = ls+grid%nb%tot%uvw
             l         = l+1
             msa(lvp)  = xp(l)+p1(ls)*visco_p(l)
             !msa(lvp)  = p1(ls)*visco_p(l) ! sans l'operateur PCD
             p(ls)     = msa(lvp)
          end do
       end do
    end do
#endif
    
    p = msa(grid%nb%tot%uvw+1:grid%nb%tot%uvwp)

    !--------------------------------------------------------------------------
    ! Calcul of bp = Bp * P
    !--------------------------------------------------------------------------

    bp = 0
    call solver_comm_mpi(p,solver,grid,SOLVE_P)
    call matveke_tri_sup(system%bp,grid%nb%tot%uvw,p,bp,solver,grid)
    
    if (grid%dim==3) then
       !--------------------------------------------------------------------------
       ! Approximate inverse of the BLock F_ww velocity
       !--------------------------------------------------------------------------
       
       call HYPRE_SStructVectorCreate(comm3d, solver%hypre(3)%grid, y, ierr)
       call HYPRE_SStructVectorCreate(comm3d, solver%hypre(3)%grid, x, ierr)
       
       call HYPRE_SStructVectorSetObjectTyp(x, HYPRE_STRUCT, ierr)
       call HYPRE_SStructVectorSetObjectTyp(y, HYPRE_STRUCT, ierr)
       
       call HYPRE_SStructVectorInitialize(y, ierr)
       call HYPRE_SStructVectorInitialize(x, ierr)
       
       ilower(1) = grid%sxw
       ilower(2) = grid%syw
       ilower(3) = grid%szw
       iupper(1) = grid%exw
       iupper(2) = grid%eyw
       iupper(3) = grid%ezw
       
       valuesw = 0
       call HYPRE_SStructVectorSetBoxValues(x, part, ilower, iupper, var, valuesw, ierr)

#if OPTIM_LOOPS 
       valuesw = 0
       l       = 0
       do kk = grid%szw,grid%ezw
          k = kk-grid%szws
          do jj = grid%syw,grid%eyw
             j = jj-grid%syws
             do ii = grid%sxw,grid%exw
                i          = ii-grid%sxws
                lvp        = i+j*(grid%exws-grid%sxws+1)+k*(grid%exws-grid%sxws+1)*(grid%eyws-grid%syws+1)+1+grid%nb%tot%uv
                l          = l+1
                valuesw(l) = qsa(lvp)-bp(lvp)
             end do
          end do
       end do
#else
       valuesw = 0
       l       = 0
       do kk = grid%szw,grid%ezw
          do jj = grid%syw,grid%eyw
             do ii = grid%sxw,grid%exw
                i          = ii-grid%sxws
                j          = jj-grid%syws
                k          = kk-grid%szws
                lvp        = i+j*(grid%exws-grid%sxws+1)+k*(grid%exws-grid%sxws+1)*(grid%eyws-grid%syws+1)+1+grid%nb%tot%uv
                l          = l+1
                valuesw(l) = qsa(lvp)-bp(lvp)
             end do
          end do
       end do
#endif
       
       call HYPRE_SStructVectorSetBoxValues(y, part, ilower, iupper, var, valuesw, ierr)
       
       call HYPRE_SStructVectorAssemble(x, ierr)
       call HYPRE_SStructVectorAssemble(y, ierr)

       call HYPRE_SStructVectorGetObject(y, ssy, ierr)
       call HYPRE_SStructVectorGetObject(x, ssx, ierr)

       select case(solver%precond)
       case(PRECOND_PFMG)
          call HYPRE_StructPFMGSolve(solver%hypre(3)%solver, solver%hypre(3)%matrixStorage, ssy, ssx, ierr)
          call HYPRE_StructPFMGGetNumIteration(solver%hypre(3)%solver, solver%hypre(3)%it, ierr)
          call HYPRE_StructPFMGGetFinalRelativ(solver%hypre(3)%solver, solver%hypre(3)%res, ierr)
       case(PRECOND_SMG)
          call HYPRE_StructSMGSolve(solver%hypre(3)%solver, solver%hypre(3)%matrixStorage, ssy, ssx, ierr)
          call HYPRE_StructSMGGetNumIterations(solver%hypre(3)%solver, solver%hypre(3)%it, ierr)
          call HYPRE_StructSMGGetFinalRelative(solver%hypre(3)%solver, solver%hypre(3)%res, ierr)
       case DEFAULT
          call print_error_solver_choice(solver)
          stop
       end select

       valuesw = 0
       call HYPRE_SStructVectorGather(x, ierr)
       call HYPRE_SStructVectorGetBoxValues(x, part, ilower, iupper, var, valuesw, ierr)

       l = 0
       do kk = grid%szw,grid%ezw
          do jj = grid%syw,grid%eyw
             do ii = grid%sxw,grid%exw
                i      = ii-grid%sxws
                j      = jj-grid%syws
                k      = kk-grid%szws
                ls     = i+j*(grid%exws-grid%sxws+1)+k*(grid%exws-grid%sxws+1)*(grid%eyws-grid%syws+1)+1
                lvp    = ls+grid%nb%tot%uv
                l      = l+1
                msa(lvp) = valuesw(l)
                w2(ls) = msa(lvp)
             end do
          end do
       end do

       call HYPRE_SStructVectorDestroy(y, ierr)
       call HYPRE_SStructVectorDestroy(x, ierr) 

       !--------------------------------------------------------------------------
       ! Calcul of zsa = Fuv_w * P
       !--------------------------------------------------------------------------
       zsa = 0
       call solver_comm_mpi(w2,solver,grid,SOLVE_W)
       call matveke_tri_sup(system%fuvw,grid%nb%tot%uv,w2,zsa,solver,grid)
    endif

    !--------------------------------------------------------------------------
    ! Approximate inverse of the BLock F_vv velocity
    !--------------------------------------------------------------------------

    call HYPRE_SStructVectorCreate(comm3d, solver%hypre(2)%grid, y, ierr)
    call HYPRE_SStructVectorCreate(comm3d, solver%hypre(2)%grid, x, ierr)
    
    call HYPRE_SStructVectorSetObjectTyp(x, HYPRE_STRUCT, ierr)
    call HYPRE_SStructVectorSetObjectTyp(y, HYPRE_STRUCT, ierr)

    call HYPRE_SStructVectorInitialize(y, ierr)
    call HYPRE_SStructVectorInitialize(x, ierr)

    ilower(1) = grid%sxv
    ilower(2) = grid%syv
    iupper(1) = grid%exv
    iupper(2) = grid%eyv
    if (grid%dim==3) then
       ilower(3) = grid%szv
       iupper(3) = grid%ezv
    end if
    
    valuesv = 0
    call HYPRE_SStructVectorSetBoxValues(x, part, ilower, iupper, var, valuesv, ierr)
    
#if OPTIM_LOOPS
    valuesv = 0
    l       = 0
    if (grid%dim==2) then 
       do kk = grid%szv,grid%ezv
          k = (kk-grid%szvs)*switch2D3D 
          do jj = grid%syv,grid%eyv
             j = jj-grid%syvs
             do ii = grid%sxv,grid%exv
                i          = ii-grid%sxvs
                lvp        = i+j*(grid%exvs-grid%sxvs+1)+k*(grid%exvs-grid%sxvs+1)*(grid%eyvs-grid%syvs+1)+1+grid%nb%tot%u
                l          = l+1
                valuesv(l) = qsa(lvp)-bp(lvp)
             end do
          end do
       end do
    else 
       do kk = grid%szv,grid%ezv
          k = (kk-grid%szvs)*switch2D3D 
          do jj = grid%syv,grid%eyv
             j = jj-grid%syvs
             do ii = grid%sxv,grid%exv
                i          = ii-grid%sxvs
                lvp        = i+j*(grid%exvs-grid%sxvs+1)+k*(grid%exvs-grid%sxvs+1)*(grid%eyvs-grid%syvs+1)+1+grid%nb%tot%u
                l          = l+1
                valuesv(l) = qsa(lvp)-bp(lvp)-zsa(lvp)
             end do
          end do
       end do
    end if
#else
    valuesv = 0
    l       = 0
    if (grid%dim==2) then 
       do kk = grid%szv,grid%ezv
          do jj = grid%syv,grid%eyv
             do ii = grid%sxv,grid%exv
                i          = ii-grid%sxvs
                j          = jj-grid%syvs
                k          = kk-grid%szvs 
                lvp        = i+j*(grid%exvs-grid%sxvs+1)+switch2D3D*k*(grid%exvs-grid%sxvs+1)*(grid%eyvs-grid%syvs+1)+1+grid%nb%tot%u
                l          = l+1
                valuesv(l) = qsa(lvp)-bp(lvp)
             end do
          end do
       end do
    else
       do kk = grid%szv,grid%ezv
          do jj = grid%syv,grid%eyv
             do ii = grid%sxv,grid%exv
                i          = ii-grid%sxvs
                j          = jj-grid%syvs
                k          = kk-grid%szvs 
                lvp        = i+j*(grid%exvs-grid%sxvs+1)+switch2D3D*k*(grid%exvs-grid%sxvs+1)*(grid%eyvs-grid%syvs+1)+1+grid%nb%tot%u
                l          = l+1
                valuesv(l) = qsa(lvp)-bp(lvp)-zsa(lvp)
             end do
          end do
       end do
    end if
#endif    
    
    call HYPRE_SStructVectorSetBoxValues(y, part, ilower, iupper, var, valuesv, ierr)

    call HYPRE_SStructVectorAssemble(x, ierr)
    call HYPRE_SStructVectorAssemble(y, ierr)

    call HYPRE_SStructVectorGetObject(y, ssy, ierr)
    call HYPRE_SStructVectorGetObject(x, ssx, ierr)

    select case(solver%precond)
    case(PRECOND_PFMG)
       call HYPRE_StructPFMGSolve(solver%hypre(2)%solver, solver%hypre(2)%matrixStorage, ssy, ssx, ierr)
       call HYPRE_StructPFMGGetNumIteration(solver%hypre(2)%solver, solver%hypre(2)%it, ierr)
       call HYPRE_StructPFMGGetFinalRelativ(solver%hypre(2)%solver, solver%hypre(2)%res, ierr)
    case(PRECOND_SMG)
       call HYPRE_StructSMGSolve(solver%hypre(2)%solver, solver%hypre(2)%matrixStorage, ssy, ssx, ierr)
       call HYPRE_StructSMGGetNumIterations(solver%hypre(2)%solver, solver%hypre(2)%it, ierr)
       call HYPRE_StructSMGGetFinalRelative(solver%hypre(2)%solver, solver%hypre(2)%res, ierr)
    case DEFAULT
       call print_error_solver_choice(solver)
       stop
    end select

    valuesv = 0
    call HYPRE_SStructVectorGather(x, ierr)
    call HYPRE_SStructVectorGetBoxValues(x, part, ilower, iupper, var, valuesv, ierr)

#if OPTIM_LOOPS
    l = 0
    do kk = grid%szv,grid%ezv
       k = (kk-grid%szvs)*switch2D3D
       do jj = grid%syv,grid%eyv
          j = jj-grid%syvs
          do ii = grid%sxv,grid%exv
             i        = ii-grid%sxvs
             ls       = i+j*(grid%exvs-grid%sxvs+1)+k*(grid%exvs-grid%sxvs+1)*(grid%eyvs-grid%syvs+1)+1
             lvp      = ls+grid%nb%tot%u
             l        = l+1
             msa(lvp) = valuesv(l)
             w1(ls)   = msa(lvp)
          end do
       end do
    end do
#else
    l = 0
    do kk = grid%szv,grid%ezv
       do jj = grid%syv,grid%eyv
          do ii = grid%sxv,grid%exv
             i        = ii-grid%sxvs
             j        = jj-grid%syvs
             k        = kk-grid%szvs
             ls       = i+j*(grid%exvs-grid%sxvs+1)+switch2D3D*k*(grid%exvs-grid%sxvs+1)*(grid%eyvs-grid%syvs+1)+1
             lvp      = ls+grid%nb%tot%u
             l        = l+1
             msa(lvp) = valuesv(l)
             w1(ls)   = msa(lvp)
          end do
       end do
    end do
#endif
    
    call HYPRE_SStructVectorDestroy(y, ierr)
    call HYPRE_SStructVectorDestroy(x, ierr) 
    
    ysa = 0
    call solver_comm_mpi(w1,solver,grid,SOLVE_V)
    call matveke_tri_sup(system%fuv,grid%nb%tot%u,w1,ysa,solver,grid)
    
    !--------------------------------------------------------------------------
    ! Approximate inverse of the BLock F_uu velocity
    !--------------------------------------------------------------------------
    
    call HYPRE_SStructVectorCreate(comm3d, solver%hypre(1)%grid, y, ierr)
    call HYPRE_SStructVectorCreate(comm3d, solver%hypre(1)%grid, x, ierr)
    
    call HYPRE_SStructVectorSetObjectTyp(x, HYPRE_STRUCT, ierr)
    call HYPRE_SStructVectorSetObjectTyp(y, HYPRE_STRUCT, ierr)
    
    call HYPRE_SStructVectorInitialize(y, ierr)
    call HYPRE_SStructVectorInitialize(x, ierr)
    
    ilower(1) = grid%sxu
    ilower(2) = grid%syu
    iupper(1) = grid%exu
    iupper(2) = grid%eyu
    if (grid%dim==3) then 
       ilower(3) = grid%szu
       iupper(3) = grid%ezu
    endif

    valuesu = 0 
    call HYPRE_SStructVectorSetBoxValues(x, part, ilower, iupper, var, valuesu, ierr)
    
#if OPTIM_LOOPS
    valuesu = 0
    l       = 0
    if (grid%dim==2) then
       do kk = grid%szu,grid%ezu
          k = (kk-grid%szus)*switch2D3D 
          do jj = grid%syu,grid%eyu
             j = jj-grid%syus
             do ii = grid%sxu,grid%exu
                i          = ii-grid%sxus
                lvp        = i+j*(grid%exus-grid%sxus+1)+k*(grid%exus-grid%sxus+1)*(grid%eyus-grid%syus+1)+1
                l          = l+1
                valuesu(l) = qsa(lvp)-bp(lvp)-ysa(lvp)
             end do
          end do
       end do
    else
       do kk = grid%szu,grid%ezu
          k = (kk-grid%szus)*switch2D3D 
          do jj = grid%syu,grid%eyu
             j = jj-grid%syus
             do ii = grid%sxu,grid%exu
                i          = ii-grid%sxus
                lvp        = i+j*(grid%exus-grid%sxus+1)+k*(grid%exus-grid%sxus+1)*(grid%eyus-grid%syus+1)+1
                l          = l+1
                valuesu(l) = qsa(lvp)-bp(lvp)-ysa(lvp)-zsa(lvp)
             end do
          end do
       end do
    end if
#else
    valuesu = 0
    l       = 0
    if (grid%dim==2) then
       do kk = grid%szu,grid%ezu
          do jj = grid%syu,grid%eyu
             do ii = grid%sxu,grid%exu
                i          = ii-grid%sxus
                j          = jj-grid%syus
                k          = kk-grid%szus 
                lvp        = i+j*(grid%exus-grid%sxus+1)+switch2D3D*k*(grid%exus-grid%sxus+1)*(grid%eyus-grid%syus+1)+1
                l          = l+1
                valuesu(l) = qsa(lvp)-bp(lvp)-ysa(lvp)
             end do
          end do
       end do
    else
       do kk = grid%szu,grid%ezu
          do jj = grid%syu,grid%eyu
             do ii = grid%sxu,grid%exu
                i          = ii-grid%sxus
                j          = jj-grid%syus
                k          = kk-grid%szus 
                lvp        = i+j*(grid%exus-grid%sxus+1)+switch2D3D*k*(grid%exus-grid%sxus+1)*(grid%eyus-grid%syus+1)+1
                l          = l+1
                valuesu(l) = qsa(lvp)-bp(lvp)-ysa(lvp)-zsa(lvp)
             end do
          end do
       end do
    end if
#endif
    
    call HYPRE_SStructVectorSetBoxValues(y, part, ilower, iupper, var, valuesu, ierr)
    
    call HYPRE_SStructVectorAssemble(x, ierr)
    call HYPRE_SStructVectorAssemble(y, ierr)
    
    call HYPRE_SStructVectorGetObject(y, ssy, ierr)
    call HYPRE_SStructVectorGetObject(x, ssx, ierr)
    
    select case(solver%precond)
    case(PRECOND_PFMG)
       call HYPRE_StructPFMGSolve(solver%hypre(1)%solver, solver%hypre(1)%matrixStorage, ssy, ssx, ierr)
       call HYPRE_StructPFMGGetNumIteration(solver%hypre(1)%solver, solver%hypre(1)%it, ierr)
       call HYPRE_StructPFMGGetFinalRelativ(solver%hypre(1)%solver, solver%hypre(1)%res, ierr)
    case(PRECOND_SMG)
       call HYPRE_StructSMGSolve(solver%hypre(1)%solver, solver%hypre(1)%matrixStorage, ssy, ssx, ierr)
       call HYPRE_StructSMGGetNumIterations(solver%hypre(1)%solver, solver%hypre(1)%it, ierr)
       call HYPRE_StructSMGGetFinalRelative(solver%hypre(1)%solver, solver%hypre(1)%res, ierr)
    case DEFAULT
       call print_error_solver_choice(solver)
       stop
    end select
    
    valuesu = 0
    call HYPRE_SStructVectorGather(x, ierr)
    call HYPRE_SStructVectorGetBoxValues(x, part, ilower, iupper, var, valuesu, ierr)

#if OPTIM_LOOPS
    l = 0
    do kk = grid%szu,grid%ezu
       k = (kk-grid%szus)*switch2D3D
       do jj = grid%syu,grid%eyu
          j = jj-grid%syus
          do ii = grid%sxu,grid%exu
             i        = ii-grid%sxus
             lvp      = i+j*(grid%exus-grid%sxus+1)+k*(grid%exus-grid%sxus+1)*(grid%eyus-grid%syus+1)+1
             l        = l+1
             msa(lvp) = valuesu(l)
          end do
       end do
    end do
#else
    l = 0
    do kk = grid%szu,grid%ezu
       do jj = grid%syu,grid%eyu
          do ii = grid%sxu,grid%exu
             i        = ii-grid%sxus
             j        = jj-grid%syus
             k        = kk-grid%szus
             lvp      = i+j*(grid%exus-grid%sxus+1)+switch2D3D*k*(grid%exus-grid%sxus+1)*(grid%eyus-grid%syus+1)+1
             l        = l+1
             msa(lvp) = valuesu(l)
          end do
       end do
    end do
#endif
    
    call HYPRE_SStructVectorDestroy(y, ierr)
    call HYPRE_SStructVectorDestroy(x, ierr)
    
101 format(a5,1x,5(1pe20.6,1x))
    
    call compute_time(TIMER_END,"[sub] precond_uvwp_with_SMG_PFMG_HYPRE")

  end subroutine precond_uvwp_with_SMG_PFMG_HYPRE


  subroutine BiCGStab_HYPRE(system,rhs,sol,solver,grid)
    use mod_mpi
    use mod_parameters, only: rank
    use mod_struct_solver
    use mod_struct_grid
    use mod_solver_new_math
    use mod_solver_new_num
    implicit none
    include 'HYPREf.h'
    !--------------------------------------------------------------------------
    ! global variables
    !--------------------------------------------------------------------------
    real(8), dimension(:), allocatable, intent(inout) :: rhs
    real(8), dimension(:), allocatable, intent(inout) :: sol
    type(system_t), intent(in)                        :: system
    type(solver_t), intent(inout)                     :: solver
    type(grid_t), intent(in)                          :: grid
    !--------------------------------------------------------------------------
    ! local variables
    !--------------------------------------------------------------------------
    integer                                           :: i,j,k,ii,jj,kk
    integer                                           :: l,lvp
    integer                                           :: id_precond
    integer                                           :: ierr
    integer                                           :: part
    integer                                           :: var
    integer(8)                                        :: x,y
    integer(8)                                        :: ssx,ssy
    integer, dimension(grid%dim)                      :: ilower,iupper
    real(8), dimension(:), allocatable                :: values
    !--------------------------------------------------------------------------

    call compute_time(TIMER_START,"[sub] BiCGStab_HYPRE")
    
    select case(solver%eqs)
    case(SOLVE_P)
    case DEFAULT
       call print_message("Projection solver only",MESSAGE_ERROR)
       stop
    end select

    allocate(values(grid%nb%no_ghost%p))
    values = 0
    
    part = 0
    var  = 0
    
    call HYPRE_StructBiCGStabCreate(comm3d, solver%hypre(0)%solver,                           ierr)
    call HYPRE_StructBiCGStabSetTol(        solver%hypre(0)%solver, solver%res_max,           ierr)
    call HYPRE_StructBiCGStabSetMaxIter(    solver%hypre(0)%solver, solver%it_max,            ierr)
    call HYPRE_StructBiCGStabSetprintlev(   solver%hypre(0)%solver, solver%hypre(0)%printLvL, ierr)
    call HYPRE_StructBiCGStabSetlogging(    solver%hypre(0)%solver, solver%hypre(0)%Log,      ierr)

    select case(solver%precond)
    case(PRECOND_JACOBI)
       !--------------------------------------------------------------------------
       call HYPRE_StructJacobiCreate( comm3d,   solver%hypre(0)%precon,                        ierr)
       call HYPRE_StructJacobiSetMaxIter(       solver%hypre(0)%precon, solver%hypre(0)%MaxIt, ierr)
       call HYPRE_StructJacobiSetTol(           solver%hypre(0)%precon, solver%hypre(0)%tol,   ierr)
       if (solver%hypre(0)%zeroGuess==0) then
          call HYPRE_StructJacobiSetZeroGuess(  solver%hypre(0)%precon,                        ierr)
       else
          call HYPRE_StructJacobiSetNonZeroGue( solver%hypre(0)%precon,                        ierr)
       end if
       !--------------------------------------------------------------------------
       id_precond = 7
       !--------------------------------------------------------------------------
    case(PRECOND_SMG)
       !--------------------------------------------------------------------------
       call HYPRE_StructSMGCreate( comm3d,     solver%hypre(0)%precon,                             ierr)
       call HYPRE_StructSMGSetMemoryUse(       solver%hypre(0)%precon, 0,                          ierr)
       call HYPRE_StructSMGSetMaxIter(         solver%hypre(0)%precon, solver%hypre(0)%MaxIt,      ierr)
       call HYPRE_StructSMGSetTol(             solver%hypre(0)%precon, solver%hypre(0)%tol,        ierr)
       if (solver%hypre(0)%zeroGuess==0) then
          call HYPRE_StructSMGSetZeroGuess(    solver%hypre(0)%precon,                             ierr)
       else
          call HYPRE_StructSMGSetNonZeroGuess( solver%hypre(0)%precon,                             ierr)
       end if
       call HYPRE_StructSMGSetNumPreRelax(     solver%hypre(0)%precon, solver%hypre(0)%nPreRelax,  ierr)
       call HYPRE_StructSMGSetNumPostRelax(    solver%hypre(0)%precon, solver%hypre(0)%nPostRelax, ierr)
       call HYPRE_StructSMGSetPrintLevel(      solver%hypre(0)%precon, solver%hypre(0)%printLvL,   ierr)
       call HYPRE_StructSMGSetLogging(         solver%hypre(0)%precon, solver%hypre(0)%Log,        ierr)
       !--------------------------------------------------------------------------
       id_precond = 0
       !--------------------------------------------------------------------------
    case(PRECOND_PFMG)
       !--------------------------------------------------------------------------
       call HYPRE_StructPFMGCreate( comm3d,     solver%hypre(0)%precon,                             ierr)
       call HYPRE_StructPFMGSetMaxIter(         solver%hypre(0)%precon, solver%hypre(0)%MaxIt,      ierr)
       call HYPRE_StructPFMGSetTol(             solver%hypre(0)%precon, solver%hypre(0)%Tol,        ierr)
       call HYPRE_StructPFMGSetmaxlevels(       solver%hypre(0)%precon, solver%hypre(0)%maxLvL,     ierr)
       if (solver%hypre(0)%zeroGuess==0) then
          call HYPRE_StructPFMGSetZeroGuess(    solver%hypre(0)%precon,                             ierr)
       else
          call HYPRE_StructPFMGSetNoNZeroGuess( solver%hypre(0)%precon,                             ierr)
       end if
       call HYPRE_StructPFMGSetNumPreRelax(     solver%hypre(0)%precon, solver%hypre(0)%nPreRelax,  ierr)
       call HYPRE_StructPFMGSetNumPostRelax(    solver%hypre(0)%precon, solver%hypre(0)%nPostRelax, ierr)
       call HYPRE_StructPFMGSetRAPType(         solver%hypre(0)%precon, solver%hypre(0)%rap,        ierr)
       call HYPRE_StructPFMGSetrelaxtype(       solver%hypre(0)%precon, solver%hypre(0)%typeRelax,  ierr) 
       call HYPRE_StructPFMGSetskiprelax(       solver%hypre(0)%precon, solver%hypre(0)%skipRelax,  ierr)
       call HYPRE_StructPFMGSetPrintLevel(      solver%hypre(0)%precon, solver%hypre(0)%printLvL,   ierr)
       call HYPRE_StructPFMGSetLogging(         solver%hypre(0)%precon, solver%hypre(0)%Log,        ierr)
       !--------------------------------------------------------------------------
       id_precond = 1
       !--------------------------------------------------------------------------
    end select

!!$    void
!!$    hypre_F90_IFACE(hypre_structbicgstabsetprecond, HYPRE_STRUCTBICGSTABSETPRECOND)
!!$    ( hypre_F90_Obj *solver,
!!$    hypre_F90_Int *precond_id,
!!$    hypre_F90_Obj *precond_solver,
!!$    hypre_F90_Int *ierr           )
!!$    {
!!$    
!!$    /*------------------------------------------------------------
!!$    * The precond_id flags mean :
!!$    * 0 - setup a smg preconditioner
!!$    * 1 - setup a pfmg preconditioner
!!$    * 7 - setup a jacobi preconditioner
!!$    * 8 - setup a ds preconditioner
!!$    * 9 - dont setup a preconditioner
!!$    *------------------------------------------------------------*/

    call HYPRE_StructBiCGStabSetPrecond( &
         & solver%hypre(0)%solver,       &
         & id_precond,                   &
         & solver%hypre(0)%precon,       &
         & ierr)

    call HYPRE_SStructMatrixGetObject(    &
         & solver%hypre(0)%matrix,        &
         & solver%hypre(0)%matrixStorage, &
         & ierr)

    call HYPRE_StructBiCGStabSetup(       &
         & solver%hypre(0)%solver,        &
         & solver%hypre(0)%matrixStorage, &
         & solver%hypre(0)%structy,       &
         & solver%hypre(0)%structx,       &
         & ierr)
    
    call HYPRE_SStructVectorCreate(comm3d, solver%hypre(0)%grid, y, ierr)
    call HYPRE_SStructVectorCreate(comm3d, solver%hypre(0)%grid, x, ierr)
    
    call HYPRE_SStructVectorSetObjectTyp(x, HYPRE_STRUCT, ierr)
    call HYPRE_SStructVectorSetObjectTyp(y, HYPRE_STRUCT, ierr)

    call HYPRE_SStructVectorInitialize(y, ierr)
    call HYPRE_SStructVectorInitialize(x, ierr)

    ilower(1) = grid%sx
    ilower(2) = grid%sy
    iupper(1) = grid%ex
    iupper(2) = grid%ey
    if (grid%dim==3) then 
       ilower(3) = grid%sz
       iupper(3) = grid%ez
    end if
    
    values = 0
    l      = 0
    do kk = grid%sz,grid%ez
       k = (kk-grid%szs)*(grid%dim-2)
       do jj = grid%sy,grid%ey
          j = jj-grid%sys
          do ii = grid%sx,grid%ex
             i         = ii-grid%sxs
             lvp       = i+j*(grid%exs-grid%sxs+1)+k*(grid%exs-grid%sxs+1)*(grid%eys-grid%sys+1)+1
             l         = l+1
             values(l) = system%mat%coef(lvp)
          end do
       end do
    end do
    
    call HYPRE_SStructVectorSetBoxValues(x, part, ilower, iupper, var, values, ierr)

    values = 0
    l      = 0
    do kk = grid%sz,grid%ez
       k = (kk-grid%szs)*(grid%dim-2)
       do jj = grid%sy,grid%ey
          j = jj-grid%sys
          do ii = grid%sx,grid%ex
             i         = ii-grid%sxs
             lvp       = i+j*(grid%exs-grid%sxs+1)+k*(grid%exs-grid%sxs+1)*(grid%eys-grid%sys+1)+1
             l         = l+1
             values(l) = rhs(lvp)
          end do
       end do
    end do
    
    call HYPRE_SStructVectorSetBoxValues(y, part, ilower, iupper, var, values, ierr)
    
    call HYPRE_SStructVectorAssemble(x, ierr)
    call HYPRE_SStructVectorAssemble(y, ierr)
    
    call HYPRE_SStructVectorGetObject(y, ssy, ierr)
    call HYPRE_SStructVectorGetObject(x, ssx, ierr)
    
    call HYPRE_StructBiCGStabSolve(       &
         & solver%hypre(0)%solver,        &
         & solver%hypre(0)%matrixStorage, &
         & ssy,                           &
         & ssx,                           & 
         & ierr)
    
    call HYPRE_StructBiCGStabGetFinalRel(solver%hypre(0)%solver, solver%res, ierr)
    call HYPRE_StructBiCGStabGetNumItera(solver%hypre(0)%solver, solver%it,  ierr)

    call HYPRE_SStructVectorGather(x, ierr)
    call HYPRE_SStructVectorGetBoxValues(x, part, ilower, iupper, var, values, ierr)

    sol = 0
    l   = 0
    do kk = grid%sz,grid%ez
       k = (kk-grid%szs)*(grid%dim-2)
       do jj = grid%sy,grid%ey
          j = jj-grid%sys
          do ii = grid%sx,grid%ex
             i         = ii-grid%sxs
             lvp       = i+j*(grid%exs-grid%sxs+1)+k*(grid%exs-grid%sxs+1)*(grid%eys-grid%sys+1)+1
             l         = l+1
             sol(lvp)  = values(l)
          end do
       end do
    end do
    
    call solver_comm_mpi(sol,solver,grid)
    
    call HYPRE_SStructVectorDestroy(y, ierr)
    call HYPRE_SStructVectorDestroy(x, ierr)
    call HYPRE_StructBiCGStabDestroy(solver%hypre(0)%solver, ierr)

    select case(solver%precond)
    case(PRECOND_JACOBI)
       call HYPRE_StructJacobiDestroy(solver%hypre(0)%precon, ierr)
    case(PRECOND_SMG)
       call HYPRE_StructSMGDestroy(solver%hypre(0)%precon, ierr)
    case(PRECOND_PFMG)
       call HYPRE_StructPFMGDestroy(solver%hypre(0)%precon, ierr)
    case DEFAULT
       call print_error_solver_choice(solver)
       stop
    end select

    call compute_time(TIMER_END,"[sub] BiCGStab_HYPRE")
    
  end subroutine BiCGStab_HYPRE


  subroutine PCG_HYPRE(system,rhs,sol,solver,grid)
    use mod_mpi
    use mod_parameters, only: rank
    use mod_struct_solver
    use mod_struct_grid
    use mod_solver_new_math
    use mod_solver_new_num
    implicit none
    include 'HYPREf.h'
    !--------------------------------------------------------------------------
    ! global variables
    !--------------------------------------------------------------------------
    real(8), dimension(:), allocatable, intent(inout) :: rhs
    real(8), dimension(:), allocatable, intent(inout) :: sol
    type(system_t), intent(in)                        :: system
    type(solver_t), intent(inout)                     :: solver
    type(grid_t), intent(in)                          :: grid
    !--------------------------------------------------------------------------
    ! local variables
    !--------------------------------------------------------------------------
    integer                                           :: i,j,k,ii,jj,kk
    integer                                           :: l,lvp
    integer                                           :: id_precond
    integer                                           :: ierr
    integer                                           :: part
    integer                                           :: var
    integer(8)                                        :: x,y
    integer(8)                                        :: ssx,ssy
    integer, dimension(grid%dim)                      :: ilower,iupper
    real(8), dimension(:), allocatable                :: values
    !--------------------------------------------------------------------------

    call compute_time(TIMER_START,"[sub] PCG_HYPRE")
    
    select case(solver%eqs)
    case(SOLVE_P)
    case DEFAULT
       call print_message("Projection solver only",MESSAGE_ERROR)
       stop
    end select

    allocate(values(grid%nb%no_ghost%p))
    values = 0
    
    part = 0
    var  = 0
    
    call HYPRE_StructPCGCreate(comm3d, solver%hypre(0)%solver,                           ierr)
    call HYPRE_StructPCGSetTol(        solver%hypre(0)%solver, solver%res_max,           ierr)
    call HYPRE_StructPCGSetMaxIter(    solver%hypre(0)%solver, solver%it_max,            ierr)
    call HYPRE_StructPCGSetprintlevel( solver%hypre(0)%solver, solver%hypre(0)%printLvL, ierr)
    call HYPRE_StructPCGSetlogging(    solver%hypre(0)%solver, solver%hypre(0)%Log,      ierr)

    select case(solver%precond)
    case(PRECOND_JACOBI)
       !--------------------------------------------------------------------------
       call HYPRE_StructJacobiCreate( comm3d,   solver%hypre(0)%precon,                        ierr)
       call HYPRE_StructJacobiSetMaxIter(       solver%hypre(0)%precon, solver%hypre(0)%MaxIt, ierr)
       call HYPRE_StructJacobiSetTol(           solver%hypre(0)%precon, solver%hypre(0)%tol,   ierr)
       if (solver%hypre(0)%zeroGuess==0) then
          call HYPRE_StructJacobiSetZeroGuess(  solver%hypre(0)%precon,                        ierr)
       else
          call HYPRE_StructJacobiSetNonZeroGue( solver%hypre(0)%precon,                        ierr)
       end if
       !--------------------------------------------------------------------------
       id_precond = 7
       !--------------------------------------------------------------------------
    case(PRECOND_SMG)
       !--------------------------------------------------------------------------
       call HYPRE_StructSMGCreate( comm3d,     solver%hypre(0)%precon,                             ierr)
       call HYPRE_StructSMGSetMemoryUse(       solver%hypre(0)%precon, 0,                          ierr)
       call HYPRE_StructSMGSetMaxIter(         solver%hypre(0)%precon, solver%hypre(0)%MaxIt,      ierr)
       call HYPRE_StructSMGSetTol(             solver%hypre(0)%precon, solver%hypre(0)%tol,        ierr)
       if (solver%hypre(0)%zeroGuess==0) then
          call HYPRE_StructSMGSetZeroGuess(    solver%hypre(0)%precon,                             ierr)
       else
          call HYPRE_StructSMGSetNonZeroGuess( solver%hypre(0)%precon,                             ierr)
       end if
       call HYPRE_StructSMGSetNumPreRelax(     solver%hypre(0)%precon, solver%hypre(0)%nPreRelax,  ierr)
       call HYPRE_StructSMGSetNumPostRelax(    solver%hypre(0)%precon, solver%hypre(0)%nPostRelax, ierr)
       call HYPRE_StructSMGSetPrintLevel(      solver%hypre(0)%precon, solver%hypre(0)%printLvL,   ierr)
       call HYPRE_StructSMGSetLogging(         solver%hypre(0)%precon, solver%hypre(0)%Log,        ierr)
       !--------------------------------------------------------------------------
       id_precond = 0
       !--------------------------------------------------------------------------
    case(PRECOND_PFMG)
       !--------------------------------------------------------------------------
       call HYPRE_StructPFMGCreate( comm3d,     solver%hypre(0)%precon,                             ierr)
       call HYPRE_StructPFMGSetMaxIter(         solver%hypre(0)%precon, solver%hypre(0)%MaxIt,      ierr)
       call HYPRE_StructPFMGSetTol(             solver%hypre(0)%precon, solver%hypre(0)%Tol,        ierr)
       call HYPRE_StructPFMGSetmaxlevels(       solver%hypre(0)%precon, solver%hypre(0)%maxLvL,     ierr)
       if (solver%hypre(0)%zeroGuess==0) then
          call HYPRE_StructPFMGSetZeroGuess(    solver%hypre(0)%precon,                             ierr)
       else
          call HYPRE_StructPFMGSetNoNZeroGuess( solver%hypre(0)%precon,                             ierr)
       end if
       call HYPRE_StructPFMGSetNumPreRelax(     solver%hypre(0)%precon, solver%hypre(0)%nPreRelax,  ierr)
       call HYPRE_StructPFMGSetNumPostRelax(    solver%hypre(0)%precon, solver%hypre(0)%nPostRelax, ierr)
       call HYPRE_StructPFMGSetRAPType(         solver%hypre(0)%precon, solver%hypre(0)%rap,        ierr)
       call HYPRE_StructPFMGSetrelaxtype(       solver%hypre(0)%precon, solver%hypre(0)%typeRelax,  ierr) 
       call HYPRE_StructPFMGSetskiprelax(       solver%hypre(0)%precon, solver%hypre(0)%skipRelax,  ierr)
       call HYPRE_StructPFMGSetPrintLevel(      solver%hypre(0)%precon, solver%hypre(0)%printLvL,   ierr)
       call HYPRE_StructPFMGSetLogging(         solver%hypre(0)%precon, solver%hypre(0)%Log,        ierr)
       !--------------------------------------------------------------------------
       id_precond = 1
       !--------------------------------------------------------------------------
    end select

    call HYPRE_StructPCGSetPrecond(      &
         & solver%hypre(0)%solver,       &
         & id_precond,                   &
         & solver%hypre(0)%precon,       &
         & ierr)

    call HYPRE_SStructMatrixGetObject(    &
         & solver%hypre(0)%matrix,        &
         & solver%hypre(0)%matrixStorage, &
         & ierr)

    call HYPRE_StructPCGSetup(            &
         & solver%hypre(0)%solver,        &
         & solver%hypre(0)%matrixStorage, &
         & solver%hypre(0)%structy,       &
         & solver%hypre(0)%structx,       &
         & ierr)
    
    call HYPRE_SStructVectorCreate(comm3d, solver%hypre(0)%grid, y, ierr)
    call HYPRE_SStructVectorCreate(comm3d, solver%hypre(0)%grid, x, ierr)
    
    call HYPRE_SStructVectorSetObjectTyp(x, HYPRE_STRUCT, ierr)
    call HYPRE_SStructVectorSetObjectTyp(y, HYPRE_STRUCT, ierr)

    call HYPRE_SStructVectorInitialize(y, ierr)
    call HYPRE_SStructVectorInitialize(x, ierr)

    ilower(1) = grid%sx
    ilower(2) = grid%sy
    iupper(1) = grid%ex
    iupper(2) = grid%ey
    if (grid%dim==3) then 
       ilower(3) = grid%sz
       iupper(3) = grid%ez
    end if
    
    values = 0
    l      = 0
    do kk = grid%sz,grid%ez
       k = (kk-grid%szs)*(grid%dim-2)
       do jj = grid%sy,grid%ey
          j = jj-grid%sys
          do ii = grid%sx,grid%ex
             i         = ii-grid%sxs
             lvp       = i+j*(grid%exs-grid%sxs+1)+k*(grid%exs-grid%sxs+1)*(grid%eys-grid%sys+1)+1
             l         = l+1
             values(l) = system%mat%coef(lvp)
          end do
       end do
    end do
    
    call HYPRE_SStructVectorSetBoxValues(x, part, ilower, iupper, var, values, ierr)

    values = 0
    l      = 0
    do kk = grid%sz,grid%ez
       k = (kk-grid%szs)*(grid%dim-2)
       do jj = grid%sy,grid%ey
          j = jj-grid%sys
          do ii = grid%sx,grid%ex
             i         = ii-grid%sxs
             lvp       = i+j*(grid%exs-grid%sxs+1)+k*(grid%exs-grid%sxs+1)*(grid%eys-grid%sys+1)+1
             l         = l+1
             values(l) = rhs(lvp)
          end do
       end do
    end do
    
    call HYPRE_SStructVectorSetBoxValues(y, part, ilower, iupper, var, values, ierr)
    
    call HYPRE_SStructVectorAssemble(x, ierr)
    call HYPRE_SStructVectorAssemble(y, ierr)
    
    call HYPRE_SStructVectorGetObject(y, ssy, ierr)
    call HYPRE_SStructVectorGetObject(x, ssx, ierr)
    
    call HYPRE_StructPCGSolve(            &
         & solver%hypre(0)%solver,        &
         & solver%hypre(0)%matrixStorage, &
         & ssy,                           &
         & ssx,                           & 
         & ierr)
    
    call HYPRE_StructPCGGetFinalRelative(solver%hypre(0)%solver, solver%res, ierr)
    call HYPRE_StructPCGGetNumIterations(solver%hypre(0)%solver, solver%it,  ierr)

    call HYPRE_SStructVectorGather(x, ierr)
    call HYPRE_SStructVectorGetBoxValues(x, part, ilower, iupper, var, values, ierr)

    sol = 0
    l   = 0
    do kk = grid%sz,grid%ez
       k = (kk-grid%szs)*(grid%dim-2)
       do jj = grid%sy,grid%ey
          j = jj-grid%sys
          do ii = grid%sx,grid%ex
             i         = ii-grid%sxs
             lvp       = i+j*(grid%exs-grid%sxs+1)+k*(grid%exs-grid%sxs+1)*(grid%eys-grid%sys+1)+1
             l         = l+1
             sol(lvp)  = values(l)
          end do
       end do
    end do
    
    call solver_comm_mpi(sol,solver,grid)
    
    call HYPRE_SStructVectorDestroy(y, ierr)
    call HYPRE_SStructVectorDestroy(x, ierr)
    call HYPRE_StructPCGDestroy(solver%hypre(0)%solver, ierr)

    select case(solver%precond)
    case(PRECOND_JACOBI)
       call HYPRE_StructJacobiDestroy(solver%hypre(0)%precon, ierr)
    case(PRECOND_SMG)
       call HYPRE_StructSMGDestroy(solver%hypre(0)%precon, ierr)
    case(PRECOND_PFMG)
       call HYPRE_StructPFMGDestroy(solver%hypre(0)%precon, ierr)
    case DEFAULT
       call print_error_solver_choice(solver)
       stop
    end select

    call compute_time(TIMER_END,"[sub] PCG_HYPRE")

  end subroutine PCG_HYPRE
  
  
  subroutine precond_p_with_SMG_PFMG_HYPRE(mat,solver,grid,sol,ysa)
    use mod_mpi
    use mod_parameters, only: rank
    use mod_struct_solver
    use mod_struct_grid
    use mod_solver_new_math
    use mod_solver_new_num
    implicit none
    include 'HYPREf.h'
    !--------------------------------------------------------------------------
    ! global variables
    !--------------------------------------------------------------------------
    type(matrix_t), intent(in)                 :: mat
    type(solver_t), intent(in)                 :: solver
    type(grid_t), intent(in)                   :: grid
    real(8), dimension(mat%npt), intent(in)    :: ysa
    real(8), dimension(mat%npt), intent(inout) :: sol
    !--------------------------------------------------------------------------
    ! local variables
    !--------------------------------------------------------------------------
    integer                                    :: i,j,k,ii,jj,kk
    integer                                    :: l,lvp
    integer                                    :: switch2D3D
    integer                                    :: ierr
    integer                                    :: part
    integer                                    :: var
    integer(8)                                 :: x,y
    integer(8)                                 :: ssx,ssy
    integer, dimension(grid%dim)               :: ilower,iupper
    real(8), dimension(:), allocatable         :: values
    !--------------------------------------------------------------------------

    call compute_time(TIMER_START,"[sub] precond_p_with_SMG_PFMG_HYPRE")

    !---------------------------------------------------------------------
    ! Pressure Varibale
    !---------------------------------------------------------------------
    allocate(values(grid%nb%no_ghost%p))  ; values = 0
    !---------------------------------------------------------------------
    
    sol  = ysa
    part = 0
    var  = 0
    
    switch2D3D = grid%dim-2
    
    call HYPRE_SStructVectorCreate(comm3d, solver%hypre(0)%grid, y, ierr)
    call HYPRE_SStructVectorCreate(comm3d, solver%hypre(0)%grid, x, ierr)
    
    call HYPRE_SStructVectorSetObjectTyp(x, HYPRE_STRUCT, ierr)
    call HYPRE_SStructVectorSetObjectTyp(y, HYPRE_STRUCT, ierr)
    
    call HYPRE_SStructVectorInitialize(y, ierr)
    call HYPRE_SStructVectorInitialize(x, ierr)

    ilower(1) = grid%sx
    ilower(2) = grid%sy
    iupper(1) = grid%ex
    iupper(2) = grid%ey
    if (grid%dim==3) then 
       ilower(3) = grid%sz
       iupper(3) = grid%ez
    end if

    call HYPRE_SStructVectorSetBoxValues(x, part, ilower, iupper, var, values, ierr)

    values = 0
    l      = 0
    do kk = grid%sz,grid%ez
       do jj = grid%sy,grid%ey
          do ii = grid%sx,grid%ex
             i         = ii-grid%sxs
             j         = jj-grid%sys
             k         = (kk-grid%szs)*switch2D3D
             lvp       = i+j*(grid%exs-grid%sxs+1)+k*(grid%exs-grid%sxs+1)*(grid%eys-grid%sys+1)+1
             l         = l+1
             values(l) = ysa(lvp)
          end do
       end do
    end do
    
    call HYPRE_SStructVectorSetBoxValues(y, part, ilower, iupper, var, values, ierr)
    
    call HYPRE_SStructVectorAssemble(x, ierr)
    call HYPRE_SStructVectorAssemble(y, ierr)
    
    call HYPRE_SStructVectorGetObject(y, ssy, ierr)
    call HYPRE_SStructVectorGetObject(x, ssx, ierr)
    
    select case(solver%precond)
    case(PRECOND_PFMG)
       call HYPRE_StructPFMGSolve(solver%hypre(0)%solver, solver%hypre(0)%matrixStorage, ssy, ssx, ierr)
       call HYPRE_StructPFMGGetNumIteration(solver%hypre(0)%solver, solver%hypre(0)%it, ierr)
       call HYPRE_StructPFMGGetFinalRelativ(solver%hypre(0)%solver, solver%hypre(0)%res, ierr)
    case(PRECOND_SMG)
       call HYPRE_StructSMGSolve(solver%hypre(0)%solver, solver%hypre(0)%matrixStorage, ssy, ssx, ierr)
       call HYPRE_StructSMGGetNumIterations(solver%hypre(0)%solver, solver%hypre(0)%it, ierr)
       call HYPRE_StructSMGGetFinalRelative(solver%hypre(0)%solver, solver%hypre(0)%res, ierr)
    case DEFAULT
       call print_error_solver_choice(solver)
       stop
    end select

    values = 0
    call HYPRE_SStructVectorGather(x, ierr)
    call HYPRE_SStructVectorGetBoxValues(x, part, ilower, iupper, var, values, ierr)
    
    l = 0
    do kk = grid%sz,grid%ez
       do jj = grid%sy,grid%ey
          do ii = grid%sx,grid%ex
             i        = ii-grid%sxs
             j        = jj-grid%sys
             k        = (kk-grid%szs)*switch2D3D
             lvp      = i+j*(grid%exs-grid%sxs+1)+k*(grid%exs-grid%sxs+1)*(grid%eys-grid%sys+1)+1
             l        = l+1
             sol(lvp) = values(l)
          end do
       end do
    end do
    
    
    call HYPRE_SStructVectorDestroy(y, ierr)
    call HYPRE_SStructVectorDestroy(x, ierr)

    call compute_time(TIMER_END,"[sub] precond_p_with_SMG_PFMG_HYPRE")

  end subroutine precond_p_with_SMG_PFMG_HYPRE

#endif
  
end module mod_solver_new_hypre
