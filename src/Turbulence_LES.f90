!========================================================================
!
!                   FUGU Version 1.0.0
!
!========================================================================
!
! Copyright  Stéphane Vincent
!
! stephane.vincent@u-pem.fr          straightparadigm@gmail.com
!
! This file is part of the FUGU Fortran library, a set of Computational 
! Fluid Dynamics subroutines devoted to the simulation of multi-scale
! multi-phase flows for resolved scale interfaces (liquid/gas/solid). 
! FUGU uses the initial developments of DyJeAT and SHALA codes from 
! Jean-Luc Estivalezes (jean-luc.estivalezes@onera.fr) and 
! Frédéric Couderc (couderc@math.univ-toulouse.fr).
!
!========================================================================
!**
!**   NAME       : Turbulence_LES.f90
!**
!**   AUTHOR     : Stéphane Vincent
!**                Georges Halim Atallah
!**                Benoit Trouette
!**
!**   FUNCTION   : LES turbulence modules of FUGU software
!**
!**   DATES      : Version 1.0.0  : from : May, 2018
!**
!========================================================================

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
module mod_turbulence_LES
  use mod_mpi
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
contains

  !*****************************************************************************************************
  !*****************************************************************************************************
  !*****************************************************************************************************
  !*****************************************************************************************************
  subroutine LES_models (u,v,w,vie,vis,vir,mu_sgs,rho)
    !***************************************************************************************************
    !***************************************************************************************************
    !***************************************************************************************************
    !***************************************************************************************************
    use mod_borders
    use mod_Parameters, only: Periodic,dx,dy,dz,gx,gy,gz, &
         & LES_scheme,LES_selection,                      &
         & LES_const_Smago,LES_const_TKE,LES_const_WALE,  &
         & sx,ex,sy,ey,sz,ez,                             &
         & sxs,exs,sys,eys,szs,ezs,                       &
         & gsx,gex,gsy,gey,gsz,gez,                       &
         & mesh
    use mod_Constants, only: d1p2,d1p3,d1p4,d3p2,d5p2,d5p4
    use Mod_deriv_xyz
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable, intent(in)      :: u,v,w,rho
    real(8), dimension(:,:,:), allocatable, intent(inout)   :: vie,mu_sgs
    real(8), dimension(:,:,:,:), allocatable, intent(inout) :: vis,vir
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                                 :: i,j,k
    real(8)                                                 :: vitemp
    real(8), dimension(:,:,:), allocatable                  :: vis_sm,SijSji,SijdSjid,Delta_Bar
    real(8)                                                 :: LES_const_MS,alpha
    real(8), dimension(:,:,:), allocatable                  :: ubt,vbt,wbt
    real(8), dimension(sx:ex,sy:ey,sz:ez)                   :: qsm,fles,Cs
    real(8)                                                 :: vol,dvol,gvol
    real(8)                                                 :: meanSijSji,gmeanSijSji
    !-------------------------------------------------------------------------------

    allocate(ubt(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
    allocate(vbt(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
    if (dim==3) then
       allocate(wbt(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
    end if

    allocate(vis_sm(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
    allocate(Delta_Bar(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))

    !-------------------------------------------------------------------------------
    ! LES scheme for turbulence
    !     1 Smagorinsky
    !     2 Turbulent Kinetic Energy (TKE)
    !     3 Mixed scale (alpha = 0.5)
    !     4 WALE
    !--------------------------------------------------------------------
    if (dim==3) then
       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                Delta_Bar(i,j,k)=(dx(i)*dy(j)*dz(k))**d1p3
             end do
          end do
       end do
    else
       do j=sy,ey
          do i=sx,ex
             Delta_Bar(i,j,:) = (dx(i)*dy(j))**d1p2
          end do
       end do
    end if

    !-------------------------------------------------------------------------------
    if (LES_selection) then
       !------------------------------------------------------------------------------- 
       call selecfunctionLES(fles,u,v,w)
       !-------------------------------------------------------------------------------
    else
       !-------------------------------------------------------------------------------
       fles = 1
       !-------------------------------------------------------------------------------
    endif
    !-------------------------------------------------------------------------------

    vis_sm=0
    select case(LES_scheme)
    case(0)
       vis_sm=0
    case(1,100)
       !---------------------------------------------------------------------
       ! Smagorinsky Model
       !---------------------------------------------------------------------
       allocate(SijSji(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
       call compute_SijSji(u,v,w,SijSji)
       !---------------------------------------------------------------------

       !-------------------------------------------------------------------------------
       ! periodicity 
       !-------------------------------------------------------------------------------
       call sca_periodicity(mesh,SijSji)
       !-------------------------------------------------------------------------------

       !------------------------------------------------
       ! Turbulent viscosity
       !------------------------------------------------
       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                vis_sm(i,j,k)=(LES_const_Smago*Delta_Bar(i,j,k))**2* &
                     & sqrt(2*SijSji(i,j,k))*rho(i,j,k)
                vis_sm(i,j,k)=vis_sm(i,j,k)*fles(i,j,k)
             enddo
          enddo
       enddo
       !------------------------------------------------
    case(101)
       !---------------------------------------------------------------------
       ! Dynamic Smagorinsky Model
       !---------------------------------------------------------------------
       allocate(SijSji(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
       call compute_SijSji(u,v,w,SijSji)
       !---------------------------------------------------------------------

       !-------------------------------------------------------------------------------
       ! periodicity 
       !-------------------------------------------------------------------------------
       call sca_periodicity(mesh,SijSji)
       !-------------------------------------------------------------------------------

       call Dynamic_SubgridScale_Model(u,v,w,Cs)

       !------------------------------------------------
       ! Turbulent viscosity
       !------------------------------------------------
       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                vis_sm(i,j,k)=(Cs(i,j,k)*Delta_Bar(i,j,k))**2*sqrt(2*SijSji(i,j,k))*rho(i,j,k)
             enddo
          enddo
       enddo
       !------------------------------------------------
    case(102)
       !---------------------------------------------------------------------
       ! Shear-Improved Smagorinsky Model
       !---------------------------------------------------------------------
       allocate(SijSji(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
       call compute_SijSji(u,v,w,SijSji)
       !---------------------------------------------------------------------

       !-------------------------------------------------------------------------------
       ! periodicity 
       !-------------------------------------------------------------------------------
       call sca_periodicity(mesh,SijSji)
       !-------------------------------------------------------------------------------

       !-------------------------------------------------------------------------------
       ! averaged SijSij
       !-------------------------------------------------------------------------------
       meanSijSji=0
       vol=0
       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                dvol=dx(i)*dy(i)*dz(k)
                meanSijSji=meanSijSji + sqrt(2d0*SijSji(i,j,k))*dvol
                vol=vol+dvol
             end do
          end do
       end do
       !-------------------------------------------------------------------------------
       call mpi_allreduce(meanSijSji,gmeanSijSji,1,mpi_double_precision,mpi_sum,comm3d,code)
       call mpi_allreduce(vol,gvol,1,mpi_double_precision,mpi_sum,comm3d,code)
       meanSijSji=gmeanSijSji/gvol
       !-------------------------------------------------------------------------------

       !------------------------------------------------
       ! Turbulent viscosity
       !------------------------------------------------
       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                vis_sm(i,j,k)=(LES_const_Smago*Delta_Bar(i,j,k))**2* &
                     & ( sqrt(2*SijSji(i,j,k)) - meanSijSji )*rho(i,j,k)
                vis_sm(i,j,k)=vis_sm(i,j,k)*fles(i,j,k)
             enddo
          enddo
       enddo
       !------------------------------------------------
    case(2)
       !---------------------------------------------------------------------
       ! TKE Model
       !---------------------------------------------------------------------

       !-------------------------------------------------------------------------------
       ! Filtered velocities
       !-------------------------------------------------------------------------------
       call filtering_var(ubt,u)
       call filtering_var(vbt,v)
       if (dim==3) call filtering_var(wbt,w)
       !-------------------------------------------------------------------------------

       if (dim==3) then
          do k=sz,ez
             do j=sy,ey
                do i=sx,ex
                   qsm(i,j,k) = d1p2*(ubt(i,j,k)*ubt(i,j,k) + &
                        &             vbt(i,j,k)*vbt(i,j,k) + &
                        &             wbt(i,j,k)*wbt(i,j,k))
                   vis_sm(i,j,k)=(LES_const_TKE*Delta_Bar(i,j,k))*sqrt(qsm(i,j,k))*rho(i,j,k)
                   vis_sm(i,j,k)=vis_sm(i,j,k)*fles(i,j,k)
                end do
             end do
          end do
       else
          do k=sz,ez
             do j=sy,ey
                do i=sx,ex
                   qsm(i,j,k) = d1p2*(ubt(i,j,k)*ubt(i,j,k) + &
                        &             vbt(i,j,k)*vbt(i,j,k))
                   vis_sm(i,j,k)=(LES_const_TKE*Delta_Bar(i,j,k))*sqrt(qsm(i,j,k))*rho(i,j,k)
                   vis_sm(i,j,k)=vis_sm(i,j,k)*fles(i,j,k)
                end do
             end do
          end do
       end if
    case(3)
       !---------------------------------------------------------------------
       ! Mixed Scale Model
       !---------------------------------------------------------------------
       alpha = d1p2
       !---------------------------------------------------------------------
       LES_const_MS = LES_const_Smago**(2*alpha)*LES_const_TKE**(1-alpha)
       !---------------------------------------------------------------------

       !---------------------------------------------------------------------
       ! SijSji (Smago contribution)
       !---------------------------------------------------------------------
       allocate(SijSji(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
       call compute_SijSji(u,v,w,SijSji)
       !---------------------------------------------------------------------

       !-------------------------------------------------------------------------------
       ! periodicity 
       !-------------------------------------------------------------------------------
       call sca_periodicity(mesh,SijSji)
       !-------------------------------------------------------------------------------

       !-------------------------------------------------------------------------------
       ! Filtered velocities
       !-------------------------------------------------------------------------------
       call filtering_var(ubt,u)
       call filtering_var(vbt,v)
       if (dim==3) call filtering_var(wbt,w)
       !-------------------------------------------------------------------------------

       if (dim==3) then
          do k=sz,ez
             do j=sy,ey
                do i=sx,ex
                   qsm(i,j,k) = d1p2*(ubt(i,j,k)*ubt(i,j,k) + &
                        &             vbt(i,j,k)*vbt(i,j,k) + &
                        &             wbt(i,j,k)*wbt(i,j,k))
                   vis_sm(i,j,k) = (LES_const_MS*Delta_Bar(i,j,k)**(1+alpha)* &
                        & (2*SijSji(i,j,k))**(d1p2*alpha)*(qsm(i,j,k))**(d1p2*(1-alpha)))*rho(i,j,k)
                   vis_sm(i,j,k)=vis_sm(i,j,k)*fles(i,j,k)
                end do
             end do
          end do
       else
          do k=sz,ez
             do j=sy,ey
                do i=sx,ex
                   qsm(i,j,k) = d1p2*(ubt(i,j,k)*ubt(i,j,k) + &
                        &             vbt(i,j,k)*vbt(i,j,k))
                   vis_sm(i,j,k) = (LES_const_MS*Delta_Bar(i,j,k)**(1+alpha)* &
                        & (2*SijSji(i,j,k))**(d1p2*alpha)*(qsm(i,j,k))**(d1p2*(1-alpha)))*rho(i,j,k)
                   vis_sm(i,j,k)=vis_sm(i,j,k)*fles(i,j,k)
                end do
             end do
          end do
       end if
    case(4)
       !---------------------------------------------------------------------
       ! Wall Adaptating Local Eddy-Viscosity (WALE) Model
       !                    (cf. P.Sagaut LES book)
       !---------------------------------------------------------------------

       !---------------------------------------------------------------------
       ! SijSji and Sij^dSij^d
       !---------------------------------------------------------------------
       allocate(SijSji(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
       allocate(SijdSjid(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
       call compute_SijSji(u,v,w,SijSji)
       call compute_SijdSjid(u,v,w,SijdSjid)
       !---------------------------------------------------------------------

       !---------------------------------------------------------------------
       ! periodicity
       !---------------------------------------------------------------------
       call sca_periodicity(mesh,SijSji)
       call sca_periodicity(mesh,SijdSjid)
       !---------------------------------------------------------------------

       !------------------------------------------------
       ! Turbulent viscosity
       !------------------------------------------------
       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                vis_sm(i,j,k)=rho(i,j,k)*(LES_const_WALE*Delta_Bar(i,j,k))**2 * &
                     & (SijdSjid(i,j,k)**d3p2)/(SijSji(i,j,k)**d5p2+SijdSjid(i,j,k)**d5p4+1e-40_8)
                vis_sm(i,j,k)=vis_sm(i,j,k)*fles(i,j,k)
             enddo
          enddo
       enddo
       !------------------------------------------------

    case DEFAULT
       !---------------------------------------------------------------------
       ! Wrong scheme
       !---------------------------------------------------------------------

       write(*,*) 'Wrong LES scheme, Stop'
       stop
    end select

    !------------------------------------------------
    ! borders and periodicity 
    !------------------------------------------------
    call sca_borders_and_periodicity(mesh,vis_sm)
    !------------------------------------------------

    !--------------------------------------------------------
    ! viscosities values, elongational, shear and rotationnal
    !--------------------------------------------------------
    if (dim==2) then
       do j=sy-1,ey+1
          do i=sx-1,ex+1
             vie(i,j,1)=vie(i,j,1)+2*vis_sm(i,j,1)
             mu_sgs(i,j,1)=vis_sm(i,j,1)
          enddo
       enddo
    else
       do k=sz-1,ez+1
          do j=sy-1,ey+1
             do i=sx-1,ex+1
                vie(i,j,k)=vie(i,j,k)+2*vis_sm(i,j,k)
                mu_sgs(i,j,k)=vis_sm(i,j,k)
             enddo
          enddo
       enddo
    end if

    if (dim==2) then
       do j=sy,ey+1
          do i=sx,ex+1
             vitemp=d1p4*(vis_sm(i,j,1)+vis_sm(i-1,j,1)+vis_sm(i,j-1,1)+vis_sm(i-1,j-1,1))
             vis(i,j,1,1)=vis(i,j,1,1)+2*vitemp
             vir(i,j,1,1)=vir(i,j,1,1)+  vitemp
          enddo
       enddo
    else
       do k=sz,ez+1
          do j=sy,ey+1
             do i=sx,ex+1
                vis(i,j,k,1)=vis(i,j,k,1)+2*d1p4*(vis_sm(i,j,k)+vis_sm(i,j,k-1)+ &
                     & vis_sm(i,j-1,k)+vis_sm(i,j-1,k-1))
                vis(i,j,k,2)=vis(i,j,k,2)+2*d1p4*(vis_sm(i,j,k)+vis_sm(i,j,k-1)+ &
                     & vis_sm(i-1,j,k)+vis_sm(i-1,j,k-1))
                vis(i,j,k,3)=vis(i,j,k,3)+2*d1p4*(vis_sm(i,j,k)+vis_sm(i-1,j,k)+ &
                     & vis_sm(i,j-1,k)+vis_sm(i-1,j-1,k))

                vir(i,j,k,1)=vir(i,j,k,1)+d1p4*(vis_sm(i,j,k)+vis_sm(i,j,k-1)+ &
                     &vis_sm(i,j-1,k)+vis_sm(i,j-1,k-1))
                vir(i,j,k,2)=vir(i,j,k,2)+d1p4*(vis_sm(i,j,k)+vis_sm(i,j,k-1)+ &
                     &vis_sm(i-1,j,k)+vis_sm(i-1,j,k-1))
                vir(i,j,k,3)=vir(i,j,k,3)+d1p4*(vis_sm(i,j,k)+vis_sm(i-1,j,k)+ &
                     & vis_sm(i,j-1,k)+vis_sm(i-1,j-1,k))                
             enddo
          enddo
       enddo
    endif
    !--------------------------------------------------------

  end subroutine LES_models

  !*****************************************************************************************************
  !*****************************************************************************************************
  !*****************************************************************************************************
  !*****************************************************************************************************

  subroutine SDT_models (condu,condv,condw,cond_sgs,rho,cp,vie,mu_sgs)
    use mod_Parameters, only: dim,Periodic,SDT_scheme,&
         & dx,dy,dz,sx,ex,sy,ey,sz,ez,gx,gy,gz,   &
         & sxu,exu,syu,eyu,szu,ezu,               & 
         & sxv,exv,syv,eyv,szv,ezv,               & 
         & sxw,exw,syw,eyw,szw,ezw,               & 
         & nx,ny,nz,gsx,gex,gsy,gey,gsz,gez
    use mod_Constants, only: d1p2,d1p4,d1p6
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable, intent(in)      :: rho,cp,vie,mu_sgs
    real(8), dimension(:,:,:), allocatable, intent(inout)   :: condu,condv,condw,cond_sgs
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                                 :: i,j,k
    real(8), allocatable, dimension(:,:,:)                  :: Schmidt_sgs,cond_sm
    real(8)                                                 :: Schmidt,cpl,mul,condl,rhol
    !-------------------------------------------------------------------------------

    allocate(Schmidt_sgs(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
    allocate(cond_sm(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))

    !-------------------------------------------------------------------------------
    ! SDT scheme for turbulence
    !     1 Default model
    !     2 Correlation from GOLDMAN
    !--------------------------------------------------------------------

    Schmidt_sgs=1

    select case(SDT_scheme)
    case(1)
       !------------------------------------------------
       ! Default Model, Turbulent Schmidt number = 1
       !------------------------------------------------
       Schmidt_sgs=1
       !------------------------------------------------
    case(2)
       !------------------------------------------------
       ! Correlation from GOLDMAN 1968 IJHMT
       ! Turbulent Schmidt number
       !------------------------------------------------
       if (dim==2) then
          do j=sy,ey
             do i=sx,ex

                rhol=rho(i,j,1)
                cpl=cp(i,j,1)
                mul=d1p2*vie(i,j,1)-mu_sgs(i,j,1)
                condl=d1p4*(condu(i,j,1)+condu(i+1,j,1)&
                     & +condv(i,j,1)+condv(i,j+1,1))
                Schmidt=mul*cpl/condl 

                Schmidt_sgs(i,j,1)=1                        &
                     & +mu_sgs(i,j,1)/mul/(Schmidt+2.93d0)  &
                     & -0.254d0*exp(-(mu_sgs(i,j,1)/mul*(0.015d0*Schmidt+0.00769_8)))
             enddo
          enddo
       else

          do k=sz,ez
             do j=sy,ey
                do i=sx,ex

                   rhol=rho(i,j,k)
                   cpl=cp(i,j,k)
                   mul=d1p2*vie(i,j,k)-mu_sgs(i,j,k)
                   condl=d1p6*(condu(i,j,k)+condu(i+1,j,k)&
                        & +condv(i,j,k)+condv(i,j+1,k)   &
                        & +condw(i,j,k)+condw(i,j,k+1) )
                   Schmidt=mul*cpl/condl 

                   Schmidt_sgs(i,j,k)=1                       &
                        & +mu_sgs(i,j,k)/mul/(Schmidt+2.93d0) &
                        & -0.254d0*exp(-(mu_sgs(i,j,k)/mul*(0.015d0*Schmidt+0.00769_8)))
                enddo
             enddo
          enddo
       end if
       !-------------------------------------------------     
    end select

    !--------------------------------------------------------
    ! borders & periodicity
    !--------------------------------------------------------

    call comm_mpi_sca(Schmidt_sgs)

    if (.not.Periodic(1)) then
       do i=sx,ex
          if (i==gsx) then 
             Schmidt_sgs(i-1,:,:)=Schmidt_sgs(i,:,:)
          else if (i==gex) then 
             Schmidt_sgs(i+1,:,:)=Schmidt_sgs(i,:,:)
          end if
       end do
    end if

    if (.not.Periodic(2)) then
       do j=sy,ey
          if (j==gsy) then 
             Schmidt_sgs(:,j-1,:)=Schmidt_sgs(:,j,:)
          else if (j==gey) then 
             Schmidt_sgs(:,j+1,:)=Schmidt_sgs(:,j,:)
          end if
       end do
    end if

    if (dim==3.and..not.Periodic(3)) then
       do k=sz,ez
          if (k==gsz) then 
             Schmidt_sgs(:,:,k-1)=Schmidt_sgs(:,:,k)
          else if (k==gez) then 
             Schmidt_sgs(:,:,k+1)=Schmidt_sgs(:,:,k)
          end if
       end do
    end if
    !--------------------------------------------------------

    !------------------------------------------------
    ! Sca node value
    !------------------------------------------------
    if (dim==2) then
       do j=sy-1,ey+1
          do i=sx-1,ex+1
             cond_sm(i,j,1)=mu_sgs(i,j,1)*cp(i,j,1)/Schmidt_sgs(i,j,1)
             cond_sgs(i,j,1)=cond_sm(i,j,1)
          end do
       end do
    else
       do k=sz-1,ez+1
          do j=sy-1,ey+1
             do i=sx-1,ex+1
                cond_sm(i,j,k)=mu_sgs(i,j,k)*cp(i,j,k)/Schmidt_sgs(i,j,k)
                cond_sgs(i,j,k)=cond_sm(i,j,k)
             end do
          end do
       end do
    end if
    !------------------------------------------------

    !------------------------------------------------
    ! U component 
    !------------------------------------------------
    do k=szu,ezu
       do j=syu,eyu
          do i=sxu,exu
             condu(i,j,k)=condu(i,j,k)+d1p2*(cond_sm(i,j,k)+cond_sm(i-1,j,k))
          enddo
       enddo
    enddo
    !------------------------------------------------

    !------------------------------------------------
    ! V component 
    !------------------------------------------------
    do k=szv,ezv
       do j=syv,eyv
          do i=sxv,exv
             condv(i,j,k)=condv(i,j,k)+d1p2*(cond_sm(i,j,k)+cond_sm(i,j-1,k))
          enddo
       enddo
    enddo
    !------------------------------------------------

    if (dim==3) then 
       !------------------------------------------------
       ! W component 
       !------------------------------------------------
       do k=szw,ezw
          do j=syw,eyw
             do i=sxw,exw
                condw(i,j,k)=condw(i,j,k)+d1p2*(cond_sm(i,j,k)+cond_sm(i,j,k-1))
             enddo
          enddo
       enddo
       !------------------------------------------------
    end if



  end subroutine SDT_models

  !***************************************************************************************************
  !***************************************************************************************************
  !***************************************************************************************************
  !***************************************************************************************************



  subroutine LES_Wall_Function (uin,vin,win,u,v,w,mu,rho)
    !***************************************************************************************************
    !***************************************************************************************************
    !***************************************************************************************************
    !***************************************************************************************************
    use mod_Parameters, only: dim,dx,dy,dz, &
         & gsxu,gexu,gsyu,geyu,gszu,gezu,   &
         & gsxv,gexv,gsyv,geyv,gszv,gezv,   &
         & gsxw,gexw,gsyw,geyw,gszw,gezw,   &
         & sxu,sxv,sxw,exu,exv,exw,         &
         & syu,syv,syw,eyu,eyv,eyw,         &
         & szu,szv,szw,ezu,ezv,ezw,         &
         & xmin,ymin,zmin,                  &
         & grid_x,grid_y,grid_z,            &
         & grid_xu,grid_yv,grid_zw
    use mod_Constants, only: one,d1p2,d1p4
    use mod_interpolation
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable, intent(inout)       :: uin,vin,win
    real(8), dimension(:,:,:), allocatable, intent(in)          :: u,v,w,mu,rho
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                                     :: i,j,k,l,imethod
    integer,save                                                :: case=1
    real(8)                                                     :: y_wf=11.225d0
    real(8)                                                     :: x,y,z,tmp
    real(8)                                                     :: u_p,u_1,u_tau,delta,nu
    real(8)                                                     :: rhol,mul,ustar,yplus
    real(8)                                                     :: f,df,f1,f2,x1,x2,eps
    real(8)                                                     :: kappa=0.415d0
    real(8)                                                     :: B=5.d0
    real(8), dimension(dim)                                     :: uvw
    logical                                                     :: activate_WF
    !-------------------------------------------------------------------------------

    if (dim==2) then

       !                                                         |y
       !                       TOP                               |     
       !                        |                                |_____x
       !             ___________|________
       !   LEFT ----|                    |-------- RIGHT
       !            |                    |
       !            |                    |
       !            |____________________|
       !                        |      
       !                        |      
       !                      BOTTOM    
       !

       select case(case)
       case(1) ! Wall law on all faces
          !-------------------------------------------------------------------------------
          ! U component 
          !-------------------------------------------------------------------------------
          do j=syu,eyu
             do i=sxu,exu

                activate_WF=.false.

                if (j==gsyu.and.i>gsxu.and.i<gexu) then                    
                   !-------------------------------------------------------------------------------
                   ! bottom wall
                   !-------------------------------------------------------------------------------
                   u_1=u(i,j+1,1)
                   activate_WF=.true.
                   delta=dy(j+1)
                else if (j==geyu.and.i>gsxu.and.i<gexu) then
                   !-------------------------------------------------------------------------------
                   ! top wall
                   !-------------------------------------------------------------------------------
                   u_1=u(i,j-1,1)
                   activate_WF=.true.
                   delta=dy(j)
                end if

                if (activate_WF) then
                   !-------------------------------------------------------------------------------
                   ! desactivate if inlet/outlet
                   !-------------------------------------------------------------------------------
                   x=grid_xu(i)
                   y=grid_y(j)
                   call interpolation_uvw(tmp,u,v,w,(/x,y/),uvw(1:dim),1,dim)
                   if ((j==gsyu.or.j==geyu).and.abs(uvw(2))>1d-10) cycle
                   !-------------------------------------------------------------------------------

                   !-------------------------------------------------------------------------------
                   ! molecular kinematic viscosity
                   !-------------------------------------------------------------------------------
                   mul=d1p4*(mu(i,j,1)+mu(i-1,j,1))
                   rhol=d1p2*(rho(i,j,1)+rho(i-1,j,1))
                   nu=mul/rhol
                   !-------------------------------------------------------------------------------

                   !-------------------------------------------------------------------------------
                   ! dimensionless u and y
                   !-------------------------------------------------------------------------------
                   u_p=uin(i,j,1)
                   ustar=sqrt(mul*abs(u_1-u_p)/delta/rhol)
                   yplus=ustar*delta/nu
                   !-------------------------------------------------------------------------------

                   !-------------------------------------------------------------------------------
                   ! procedure to find u_tau
                   !-------------------------------------------------------------------------------
                   u_tau=0
                   if (yplus<y_wf) then
                      u_tau=ustar*yplus
                   else 
                      call find_u_tau(u_tau,u_1,nu,delta)
                   end if
                   !-------------------------------------------------------------------------------

                   !-------------------------------------------------------------------------------
                   ! slip value
                   !-------------------------------------------------------------------------------
                   if (u_tau>0) uin(i,j,1)=sign(one,u_1)*(abs(u_1)-delta*u_tau**2/nu)
                   !-------------------------------------------------------------------------------
                end if

             end do
          end do

          !-------------------------------------------------------------------------------
          ! V component 
          !-------------------------------------------------------------------------------
          do j=syv,eyv
             do i=sxv,exv

                activate_WF=.false.

                if (i==gsxv.and.j>gsyv.and.j<geyv) then                    
                   !-------------------------------------------------------------------------------
                   ! left wall
                   !-------------------------------------------------------------------------------
                   u_1=v(i+1,j,1)
                   activate_WF=.true.
                   delta=dx(i+1)
                else if (i==gexv.and.j>gsyv.and.j<geyv) then
                   !-------------------------------------------------------------------------------
                   ! right wall
                   !-------------------------------------------------------------------------------
                   u_1=v(i-1,j,1)
                   activate_WF=.true.
                   delta=dx(i)
                end if

                if (activate_WF) then
                   !-------------------------------------------------------------------------------
                   ! desactivate if inlet/outlet
                   !-------------------------------------------------------------------------------
                   x=grid_x(i)
                   y=grid_yv(j)
                   call interpolation_uvw(tmp,u,v,w,(/x,y/),uvw(1:dim),1,dim)
                   if ((i==gsxv.or.i==gexv).and.abs(uvw(1))>1d-10) cycle
                   !-------------------------------------------------------------------------------

                   !-------------------------------------------------------------------------------
                   ! molecular kinematic viscosity
                   !-------------------------------------------------------------------------------
                   mul=d1p4*(mu(i,j,1)+mu(i,j-1,1))
                   rhol=d1p2*(rho(i,j,1)+rho(i,j-1,1))
                   nu=mul/rhol
                   !-------------------------------------------------------------------------------

                   !-------------------------------------------------------------------------------
                   ! dimensionless u and y
                   !-------------------------------------------------------------------------------
                   u_p=vin(i,j,1)
                   ustar=sqrt(mul*abs(u_1-u_p)/delta/rhol)
                   yplus=ustar*delta/nu
                   !-------------------------------------------------------------------------------

                   !-------------------------------------------------------------------------------
                   ! procedure to find u_tau
                   !-------------------------------------------------------------------------------
                   u_tau=0
                   if (yplus<y_wf) then
                      u_tau=ustar*yplus
                   else 
                      call find_u_tau(u_tau,u_1,nu,delta)
                   end if
                   !-------------------------------------------------------------------------------

                   !-------------------------------------------------------------------------------
                   ! slip value
                   !-------------------------------------------------------------------------------
                   if (u_tau>0) vin(i,j,1)=sign(one,u_1)*(abs(u_1)-delta*u_tau**2/nu)
                   !-------------------------------------------------------------------------------
                end if

             end do
          end do

       case DEFAULT
          write(*,*) 'Wrong Case'
       end select

    else

       !                                                             |y
       !                       TOP       BACKWARD                    |     
       !                        |          /                         |_____x
       !               _________|_________/_                        /
       !              /|        |        / /|                      /z
       !             / |                  / |
       !            /__|_________________/  |
       !   LEFT ----|- |                 | -|----- RIGHT
       !            |  |_________________|__|
       !            | /                  | /
       !            |/_/________|________|/ 
       !              /         |      
       !             /          |      
       !          FORWARD    BOTTOM    
       !

       select case(case)
       case(1) ! Wall law on all faces
          !-------------------------------------------------------------------------------
          ! U component 
          !-------------------------------------------------------------------------------
          do k=szu,ezu
             do j=syu,eyu
                do i=sxu,exu

                   activate_WF=.false.

                   if (j==gsyu.and.i>gsxu.and.i<gexu.and.k>gszu.and.k<gezu) then                    
                      !-------------------------------------------------------------------------------
                      ! bottom wall
                      !-------------------------------------------------------------------------------
                      u_1=u(i,j+1,k)
                      activate_WF=.true.
                      delta=dy(j+1)
                   else if (j==geyu.and.i>gsxu.and.i<gexu.and.k>gszu.and.k<gezu) then
                      !-------------------------------------------------------------------------------
                      ! top wall
                      !-------------------------------------------------------------------------------
                      u_1=u(i,j-1,k)
                      activate_WF=.true.
                      delta=dy(j)
                   else if (k==gszu.and.i>gsxu.and.i<gexu.and.j>gsyu.and.j<geyu) then
                      !-------------------------------------------------------------------------------
                      ! backward wall
                      !-------------------------------------------------------------------------------
                      u_1=u(i,j,k+1)
                      activate_WF=.true.
                      delta=dz(k+1)
                   else if (k==gezu.and.i>gsxu.and.i<gexu.and.j>gsyu.and.j<geyu) then
                      !-------------------------------------------------------------------------------
                      ! forward wall
                      !-------------------------------------------------------------------------------
                      u_1=u(i,j,k-1)
                      activate_WF=.true.
                      delta=dz(k)
                   end if

                   if (activate_WF) then
                      !-------------------------------------------------------------------------------
                      ! desactivate if inlet/outlet
                      !-------------------------------------------------------------------------------
                      x=grid_xu(i)
                      y=grid_y(j)
                      z=grid_z(k)
                      call interpolation_uvw(tmp,u,v,w,(/x,y,z/),uvw(1:dim),1,dim)
                      if ((j==gsyu.or.j==geyu).and.abs(uvw(2))>1d-10) cycle
                      if ((k==gszu.or.k==gezu).and.abs(uvw(3))>1d-10) cycle
                      !-------------------------------------------------------------------------------

                      !-------------------------------------------------------------------------------
                      ! molecular kinematic viscosity
                      !-------------------------------------------------------------------------------
                      mul=d1p4*(mu(i,j,k)+mu(i-1,j,k))
                      rhol=d1p2*(rho(i,j,k)+rho(i-1,j,k))
                      nu=mul/rhol
                      !-------------------------------------------------------------------------------

                      !-------------------------------------------------------------------------------
                      ! dimensionless u and y
                      !-------------------------------------------------------------------------------
                      u_p=uin(i,j,k)
                      ustar=sqrt(mul*abs(u_1-u_p)/delta/rhol)
                      yplus=ustar*delta/nu
                      !-------------------------------------------------------------------------------

                      !-------------------------------------------------------------------------------
                      ! procedure to find u_tau
                      !-------------------------------------------------------------------------------
                      u_tau=0
                      if (yplus<y_wf) then
                         u_tau=ustar*yplus
                      else 
                         call find_u_tau(u_tau,u_1,nu,delta)
                      end if
                      !-------------------------------------------------------------------------------

                      !-------------------------------------------------------------------------------
                      ! slip value
                      !-------------------------------------------------------------------------------
                      if (u_tau>0) uin(i,j,k)=sign(one,u_1)*(abs(u_1)-delta*u_tau**2/nu)
                      !-------------------------------------------------------------------------------
                   end if

                end do
             end do
          end do

          !-------------------------------------------------------------------------------
          ! edges for U component
          !-------------------------------------------------------------------------------
          do k=szu,ezu
             do j=syu,eyu
                do i=sxu,exu
                   if (j==gsyu.and.k==gszu.and.i>gsxu.and.i<gexu) then
                      !-------------------------------------------------------------------------------
                      ! bottom/backward
                      !-------------------------------------------------------------------------------
                      uin(i,j,k)=d1p2*(uin(i,j+1,k)+uin(i,j,k+1))
                   else if (j==gsyu.and.k==gezu.and.i>gsxu.and.i<gexu) then
                      !-------------------------------------------------------------------------------
                      ! bottom/forward
                      !-------------------------------------------------------------------------------
                      uin(i,j,k)=d1p2*(uin(i,j+1,k)+uin(i,j,k-1))
                   else if (j==geyu.and.k==gszu.and.i>gsxu.and.i<gexu) then
                      !-------------------------------------------------------------------------------
                      ! top/backward
                      !-------------------------------------------------------------------------------
                      uin(i,j,k)=d1p2*(uin(i,j-1,k)+uin(i,j,k+1))
                   else if (j==geyu.and.k==gezu.and.i>gsxu.and.i<gexu) then
                      !-------------------------------------------------------------------------------
                      ! top/forward
                      !-------------------------------------------------------------------------------
                      uin(i,j,k)=d1p2*(uin(i,j-1,k)+uin(i,j,k-1))
                   end if
                end do
             end do
          end do
          !-------------------------------------------------------------------------------

          !-------------------------------------------------------------------------------
          ! V component 
          !-------------------------------------------------------------------------------
          do k=szv,ezv
             do j=syv,eyv
                do i=sxv,exv

                   activate_WF=.false.

                   if (i==gsxv.and.j>gsyv.and.j<geyv.and.k>gszv.and.k<gezv) then                    
                      !-------------------------------------------------------------------------------
                      ! left wall
                      !-------------------------------------------------------------------------------
                      u_1=v(i+1,j,k)
                      activate_WF=.true.
                      delta=dx(i+1)
                   else if (i==gexv.and.j>gsyv.and.j<geyv.and.k>gszv.and.k<gezv) then
                      !-------------------------------------------------------------------------------
                      ! right wall
                      !-------------------------------------------------------------------------------
                      u_1=v(i-1,j,k)
                      activate_WF=.true.
                      delta=dx(i)
                   else if (k==gszv.and.i>gsxv.and.i<gexv.and.j>gsyv.and.j<geyv) then
                      !-------------------------------------------------------------------------------
                      ! backward wall
                      !-------------------------------------------------------------------------------
                      u_1=v(i,j,k+1)
                      activate_WF=.true.
                      delta=dz(k+1)
                   else if (k==gezv.and.i>gsxv.and.i<gexv.and.j>gsyv.and.j<geyv) then
                      !-------------------------------------------------------------------------------
                      ! forward wall
                      !-------------------------------------------------------------------------------
                      u_1=v(i,j,k-1)
                      activate_WF=.true.
                      delta=dz(k)
                   end if

                   if (activate_WF) then
                      !-------------------------------------------------------------------------------
                      ! desactivate if inlet/outlet
                      !-------------------------------------------------------------------------------
                      x=grid_x(i)
                      y=grid_yv(j)
                      z=grid_z(k)
                      call interpolation_uvw(tmp,u,v,w,(/x,y,z/),uvw(1:dim),1,dim)
                      if ((i==gsxv.or.i==gexv).and.abs(uvw(1))>1d-10) cycle
                      if ((k==gszv.or.k==gezv).and.abs(uvw(3))>1d-10) cycle
                      !-------------------------------------------------------------------------------

                      !-------------------------------------------------------------------------------
                      ! molecular kinematic viscosity
                      !-------------------------------------------------------------------------------
                      mul=d1p4*(mu(i,j,k)+mu(i,j-1,k))
                      rhol=d1p2*(rho(i,j,k)+rho(i,j-1,k))
                      nu=mul/rhol
                      !-------------------------------------------------------------------------------

                      !-------------------------------------------------------------------------------
                      ! dimensionless u and y
                      !-------------------------------------------------------------------------------
                      u_p=vin(i,j,k)
                      ustar=sqrt(mul*abs(u_1-u_p)/delta/rhol)
                      yplus=ustar*delta/nu
                      !-------------------------------------------------------------------------------

                      !-------------------------------------------------------------------------------
                      ! procedure to find u_tau
                      !-------------------------------------------------------------------------------
                      u_tau=0
                      if (yplus<y_wf) then
                         u_tau=ustar*yplus
                      else 
                         call find_u_tau(u_tau,u_1,nu,delta)
                      end if
                      !-------------------------------------------------------------------------------

                      !-------------------------------------------------------------------------------
                      ! slip value
                      !-------------------------------------------------------------------------------
                      if (u_tau>0) vin(i,j,k)=sign(one,u_1)*(abs(u_1)-delta*u_tau**2/nu)
                      !-------------------------------------------------------------------------------
                   end if

                end do
             end do
          end do

          !-------------------------------------------------------------------------------
          ! edges for V component
          !-------------------------------------------------------------------------------
          do k=szv,ezv
             do j=syv,eyv
                do i=sxv,exv
                   if (i==gsxv.and.k==gszv.and.j>gsyv.and.j<geyv) then
                      !-------------------------------------------------------------------------------
                      ! left/backward
                      !-------------------------------------------------------------------------------
                      vin(i,j,k)=d1p2*(vin(i+1,j,k)+vin(i,j,k+1))
                   else if (i==gsxv.and.k==gezv.and.j>gsyv.and.j<geyv) then
                      !-------------------------------------------------------------------------------
                      ! left/forward
                      !-------------------------------------------------------------------------------
                      vin(i,j,k)=d1p2*(vin(i+1,j,k)+vin(i,j,k-1))
                   else if (i==gexv.and.k==gszv.and.j>gsyv.and.j<geyv) then
                      !-------------------------------------------------------------------------------
                      ! right/backward
                      !-------------------------------------------------------------------------------
                      vin(i,j,k)=d1p2*(vin(i-1,j,k)+vin(i,j,k+1))
                   else if (i==gexv.and.k==gezv.and.j>gsyv.and.j<geyv) then
                      !-------------------------------------------------------------------------------
                      ! right/forward
                      !-------------------------------------------------------------------------------
                      vin(i,j,k)=d1p2*(vin(i-1,j,k)+vin(i,j,k-1))
                   end if
                end do
             end do
          end do
          !-------------------------------------------------------------------------------

          !-------------------------------------------------------------------------------
          ! W component 
          !-------------------------------------------------------------------------------
          do k=szw,ezw
             do j=syw,eyw
                do i=sxw,exw

                   activate_WF=.false.

                   if (i==gsxw.and.j>gsyw.and.j<geyw.and.k>gszw.and.k<gezw) then                    
                      !-------------------------------------------------------------------------------
                      ! left wall
                      !-------------------------------------------------------------------------------
                      u_1=w(i+1,j,k)
                      activate_WF=.true.
                      delta=dx(i+1)
                   else if (i==gexw.and.j>gsyw.and.j<geyw.and.k>gszw.and.k<gezw) then
                      !-------------------------------------------------------------------------------
                      ! right wall
                      !-------------------------------------------------------------------------------
                      u_1=w(i-1,j,k)
                      activate_WF=.true.
                      delta=dx(i)
                   else if (j==gsyw.and.i>gsxw.and.i<gexw.and.k>gszw.and.k<gezw) then
                      !-------------------------------------------------------------------------------
                      ! bottom wall
                      !-------------------------------------------------------------------------------
                      u_1=w(i,j+1,k)
                      activate_WF=.true.
                      delta=dy(j+1)
                   else if (j==geyw.and.i>gsxw.and.i<gexw.and.k>gszw.and.k<gezw) then
                      !-------------------------------------------------------------------------------
                      ! top wall
                      !-------------------------------------------------------------------------------
                      u_1=w(i,j-1,k)
                      activate_WF=.true.
                      delta=dy(j)
                   end if

                   if (activate_WF) then
                      !-------------------------------------------------------------------------------
                      ! desactivate if inlet/outlet
                      !-------------------------------------------------------------------------------
                      x=grid_x(i)
                      y=grid_y(j)
                      z=grid_zw(k)
                      call interpolation_uvw(tmp,u,v,w,(/x,y,z/),uvw(1:dim),1,dim)
                      if ((i==gsxw.or.i==gexw).and.abs(uvw(1))>1d-10) cycle
                      if ((j==gsyw.or.j==geyw).and.abs(uvw(2))>1d-10) cycle
                      !-------------------------------------------------------------------------------

                      !-------------------------------------------------------------------------------
                      ! molecular kinematic viscosity
                      !-------------------------------------------------------------------------------
                      mul=d1p4*(mu(i,j,k)+mu(i,j,k-1))
                      rhol=d1p2*(rho(i,j,k)+rho(i,j,k-1))
                      nu=mul/rhol
                      !-------------------------------------------------------------------------------

                      !-------------------------------------------------------------------------------
                      ! dimensionless u and y
                      !-------------------------------------------------------------------------------
                      u_p=win(i,j,k)
                      ustar=sqrt(mul*abs(u_1-u_p)/delta/rhol)
                      yplus=ustar*delta/nu
                      !-------------------------------------------------------------------------------

                      !-------------------------------------------------------------------------------
                      ! procedure to find u_tau
                      !-------------------------------------------------------------------------------
                      u_tau=0
                      if (yplus<y_wf) then
                         u_tau=ustar*yplus
                      else 
                         call find_u_tau(u_tau,u_1,nu,delta)
                      end if
                      !-------------------------------------------------------------------------------

                      !-------------------------------------------------------------------------------
                      ! slip value
                      !-------------------------------------------------------------------------------
                      if (u_tau>0) win(i,j,k)=sign(one,u_1)*(abs(u_1)-delta*u_tau**2/nu)
                      !-------------------------------------------------------------------------------
                   end if

                end do
             end do
          end do

          !-------------------------------------------------------------------------------
          ! edges for W component
          !-------------------------------------------------------------------------------
          do k=szw,ezw
             do j=syw,eyw
                do i=sxw,exw
                   if (i==gsxw.and.j==gsyw.and.k>gszw.and.k<gezw) then
                      !-------------------------------------------------------------------------------
                      ! left/bottom
                      !-------------------------------------------------------------------------------
                      win(i,j,k)=d1p2*(win(i+1,j,k)+win(i,j+1,k))
                   else if (i==gexw.and.j==gsyw.and.k>gszw.and.k<gezw) then
                      !-------------------------------------------------------------------------------
                      ! right/bottom
                      !-------------------------------------------------------------------------------
                      win(i,j,k)=d1p2*(win(i-1,j,k)+win(i,j+1,k))
                   else if (i==gsxw.and.j==geyw.and.k>gszw.and.k<gezw) then
                      !-------------------------------------------------------------------------------
                      ! left/top
                      !-------------------------------------------------------------------------------
                      win(i,j,k)=d1p2*(win(i+1,j,k)+win(i,j-1,k))
                   else if (i==gexw.and.j==geyw.and.k>gszw.and.k<gezw) then
                      !-------------------------------------------------------------------------------
                      ! right/top
                      !-------------------------------------------------------------------------------
                      win(i,j,k)=d1p2*(win(i-1,j,k)+win(i,j-1,k))
                   end if
                end do
             end do
          end do
          !-------------------------------------------------------------------------------

       case DEFAULT
          write(*,*) 'Wrong Case'
       end select
    end if


  end subroutine LES_Wall_Function



  !***************************************************************************************************
  !***************************************************************************************************
  !***************************************************************************************************
  !***************************************************************************************************



  subroutine wall_function(ifunc,f,df,x,u,y,nu)
    !-------------------------------------------------------------------------------
    ! Description:
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Modules
    !-------------------------------------------------------------------------------
    use mod_Constants, only: one,d1p6,d1p7
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)    :: ifunc
    real(8), intent(in)    :: x,u,y,nu
    real(8), intent(inout) :: f,df
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    real(8)                :: kappa=0.415d0, B=5, A=8.3d0, E=8
    !-------------------------------------------------------------------------------

    select case(ifunc)
    case(1) ! classical log law v1
       f=one/kappa*log(y*x/nu)+B-u/x
       df=one/kappa/x+u/x**2
    case(2) ! classical log law v2
       f=one/kappa*log(E*y*x/nu)-u/x
       df=one/kappa/x+u/x**2
    case(3) ! Werner et Wengle
       f=A*(y*x/nu)**d1p7-u/x
       df=d1p7*A*(y*x/nu)**d1p6*y/nu+u/x**2
    end select

  end subroutine wall_function


  subroutine find_u_tau(u_tau,u_1,nu,delta)
    use mod_Constants, only: d1p2
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), intent(out) :: u_tau
    real(8), intent(in)  :: u_1,nu,delta
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer              :: l,imethod
    real(8)              :: x1,x2,eps,f,f1,f2,df
    !-------------------------------------------------------------------------------

    u_tau=0
    if (abs(u_1)>0) then
       x1=abs(u_1)/1000
       x2=abs(u_1)*1000
       u_tau=d1p2*(x1+x2)
       imethod=3
       eps=1
       l=0
       do while (eps>1d-10)
          l=l+1
          select case(imethod)
          case(1)
             !---------------------------------------------------------
             ! newton 
             !---------------------------------------------------------
             call wall_function(1,f,df,u_tau,abs(u_1),delta,nu)
             u_tau=u_tau-f/df
             eps=abs(f)
          case(2)
             !---------------------------------------------------------
             ! secant 
             !---------------------------------------------------------
             call wall_function(1,f1,df,x1,abs(u_1),delta,nu)
             call wall_function(1,f2,df,x2,abs(u_1),delta,nu)
             u_tau=x2-(x2-x1)/(f2-f1)*f2
             call wall_function(1,f,df,u_tau,abs(u_1),delta,nu)
             if (f1*f>0) then
                x2=u_tau
             else
                x1=u_tau
             end if
             eps=abs(f)
             !if (eps<1d-2) imethod=1
          case(3) 
             !---------------------------------------------------------
             ! dicho
             !---------------------------------------------------------
             u_tau=d1p2*(x1+x2)
             call wall_function(1,f,df,u_tau,abs(u_1),delta,nu)
             if (f>0) then
                x2=u_tau
             else
                x1=u_tau
             end if
             eps=abs(x1-x2)
             if (abs(f)<1 .or. eps<1d-5) imethod=1
          end select
          !write(*,*) l,u_1,u_tau,x1,x2,eps,imethod

          if (l>100) then
             stop "no convergence"
          end if

       end do
       !write(*,*) u_tau,u_1,nu,f,df
       !stop
    end if

    return

  end subroutine find_u_tau

  !*************************************************************************************************
  !*************************************************************************************************
  !*************************************************************************************************
  !*************************************************************************************************

  subroutine  selecfunctionLES(fles,u,v,w)
    !-------------------------------------------------------------------------------
    ! Description:
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Modules
    !-------------------------------------------------------------------------------
    use mod_Parameters, only: sx,ex,sy,ey,sz,ez,sxs,exs,sys,eys,szs,ezs,dim,gx,gy,gz
    use mod_Constants, only: d1p2,pi
    use Mod_deriv_xyz
    use mod_math
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(sx:ex,sy:ey,sz:ez)                    :: fles
    real(8), dimension(:,:,:), allocatable, intent(in)       :: u,v,w
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                                  :: i,j,k
    real(8)                                                  :: num,val,ang,ang0,fsize,vorpvx,vorpvy,vorpvz
    real(8), allocatable, dimension(:,:,:)                   :: dudx,dudy,dudz,dvdx,dvdy,dvdz,dwdx,dwdy,dwdz,alpha
    real(8), allocatable, dimension(:,:,:)                   :: dudyijk,dudzijk,dvdxijk,dvdzijk,dwdxijk,dwdyijk
    real(8), allocatable, dimension(:,:,:)                   :: vorx,vory,vorz,vorxm,vorym,vorzm
    real(8), dimension(sx:ex,sy:ey,sz:ez)                    :: normvorm,normvor,normvorpv
    !-------------------------------------------------------------------------------
    fles=0
    if (dim==2) then
       !-------------------------------------------------------------------------------
       write(6,*) 'LES selection functions does not exist in 2D'
       !-------------------------------------------------------------------------------
    else
       !-------------------------------------------------------------------------------
       ! Spatial derivatives for rotational
       !-------------------------------------------------------------------------------
       allocate(vorx(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
       allocate(vory(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
       allocate(vorz(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
       allocate(vorxm(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
       allocate(vorym(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
       allocate(vorzm(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
       allocate( dudx(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz),&
            &    dudy(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz),&
            &    dudz(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz),&
            &    dvdx(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz),&
            &    dvdy(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz),&
            &    dvdz(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz),&
            &    dwdx(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz),&
            &    dwdy(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz),&
            &    dwdz(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz),&
            & dudyijk(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz),&
            & dudzijk(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz),&
            & dvdxijk(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz),&
            & dvdzijk(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz),&
            & dwdxijk(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz),&
            & dwdyijk(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
       call deriv_xyz(u,v,w,dudx,dudy,dudz,dvdx,dvdy,dvdz,dwdx,dwdy,dwdz,&
            & dudyijk,dudzijk,dvdxijk,dvdzijk,dwdxijk,dwdyijk)
       !-------------------------------------------------------------------------------
       ! Components of vorticity on pressure grid
       !-------------------------------------------------------------------------------
       vorx=d1p2*(dwdyijk-dvdzijk)    
       vory=d1p2*(dudzijk-dwdxijk)
       vorz=d1p2*(dvdxijk-dudyijk)
       !-------------------------------------------------------------------------------
       num  = 0
       val  = 0
       ang  = 0
       ang0 = 20*pi/180
       fsize = 4       
       !-------------------------------------------------------------------------------
       ! Estimate of explicitly filtered vorticity and fluctuating vorticity
       ! with their norms
       !-------------------------------------------------------------------------------
       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                !-------------------------------------------------------------------------------
                ! x-direction 
                !-------------------------------------------------------------------------------
                vorxm(i,j,k) =  vorx(i+1,j+1,k+1) +    fsize*vorx(i+1,j,k+1) +          vorx(i+1,j-1,k+1) + &
                     &    fsize*vorx(i  ,j+1,k+1) + fsize**2*vorx(i  ,j,k+1) +    fsize*vorx(i  ,j-1,k+1) + &
                     &          vorx(i-1,j+1,k+1) +    fsize*vorx(i-1,j,k+1) +          vorx(i-1,j-1,k+1) + &
                     &    fsize*vorx(i+1,j+1,k  ) + fsize**2*vorx(i+1,j,k  ) +    fsize*vorx(i+1,j-1,k  ) + &
                     & fsize**2*vorx(i  ,j+1,k  ) + fsize**3*vorx(i  ,j,k  ) + fsize**2*vorx(i  ,j-1,k  ) + &
                     &    fsize*vorx(i-1,j+1,k  ) + fsize**2*vorx(i-1,j,k  ) +    fsize*vorx(i-1,j-1,k  ) + &
                     &          vorx(i+1,j+1,k-1) +    fsize*vorx(i+1,j,k-1) +          vorx(i+1,j-1,k-1) + &
                     &    fsize*vorx(i  ,j+1,k-1) + fsize**2*vorx(i  ,j,k-1) +    fsize*vorx(i  ,j-1,k-1) + &
                     &          vorx(i-1,j+1,k-1) +    fsize*vorx(i-1,j,k-1) +          vorx(i-1,j-1,k-1) 
                vorxm(i,j,k) = vorxm(i,j,k) / (2 + fsize)**dim
                !-------------------------------------------------------------------------------
                ! y-direction
                !-------------------------------------------------------------------------------
                vorym(i,j,k) =  vory(i+1,j+1,k+1) +    fsize*vory(i+1,j,k+1) +          vory(i+1,j-1,k+1) + &
                     &    fsize*vory(i  ,j+1,k+1) + fsize**2*vory(i  ,j,k+1) +    fsize*vory(i  ,j-1,k+1) + &
                     &          vory(i-1,j+1,k+1) +    fsize*vory(i-1,j,k+1) +          vory(i-1,j-1,k+1) + &
                     &    fsize*vory(i+1,j+1,k  ) + fsize**2*vory(i+1,j,k  ) +    fsize*vory(i+1,j-1,k  ) + &
                     & fsize**2*vory(i  ,j+1,k  ) + fsize**3*vory(i  ,j,k  ) + fsize**2*vory(i  ,j-1,k  ) + &
                     &    fsize*vory(i-1,j+1,k  ) + fsize**2*vory(i-1,j,k  ) +    fsize*vory(i-1,j-1,k  ) + &
                     &          vory(i+1,j+1,k-1) +    fsize*vory(i+1,j,k-1) +          vory(i+1,j-1,k-1) + &
                     &    fsize*vory(i  ,j+1,k-1) + fsize**2*vory(i  ,j,k-1) +    fsize*vory(i  ,j-1,k-1) + &
                     &          vory(i-1,j+1,k-1) +    fsize*vory(i-1,j,k-1) +          vory(i-1,j-1,k-1)
                vorym(i,j,k) = vorym(i,j,k) / (2 + fsize)**dim
                !-------------------------------------------------------------------------------
                ! z-direction
                !-------------------------------------------------------------------------------
                vorzm(i,j,k) =  vorz(i+1,j+1,k+1) +    fsize*vorz(i+1,j,k+1) +          vorz(i+1,j-1,k+1) + &
                     &    fsize*vorz(i  ,j+1,k+1) + fsize**2*vorz(i  ,j,k+1) +    fsize*vorz(i  ,j-1,k+1) + &
                     &          vorz(i-1,j+1,k+1) +    fsize*vorz(i-1,j,k+1) +          vorz(i-1,j-1,k+1) + &
                     &    fsize*vorz(i+1,j+1,k  ) + fsize**2*vorz(i+1,j,k  ) +    fsize*vorz(i+1,j-1,k  ) + &
                     & fsize**2*vorz(i  ,j+1,k  ) + fsize**3*vorz(i  ,j,k  ) + fsize**2*vorz(i  ,j-1,k  ) + &
                     &    fsize*vorz(i-1,j+1,k  ) + fsize**2*vorz(i-1,j,k  ) +    fsize*vorz(i-1,j-1,k  ) + &
                     &          vorz(i+1,j+1,k-1) +    fsize*vorz(i+1,j,k-1) +          vorz(i+1,j-1,k-1) + &
                     &    fsize*vorz(i  ,j+1,k-1) + fsize**2*vorz(i  ,j,k-1) +    fsize*vorz(i  ,j-1,k-1) + &
                     &          vorz(i-1,j+1,k-1) +    fsize*vorz(i-1,j,k-1) +          vorz(i-1,j-1,k-1)
                vorzm(i,j,k) = vorzm(i,j,k) / (2 + fsize)**dim
                !-------------------------------------------------------------------------------
                ! norm
                !-------------------------------------------------------------------------------
                normvorm(i,j,k) = vorxm(i,j,k)**2+vorym(i,j,k)**2+vorzm(i,j,k)**2
                normvor (i,j,k) =  vorx(i,j,k)**2+ vory(i,j,k)**2+ vorz(i,j,k)**2
                !-------------------------------------------------------------------------------
                vorxm(i,j,k) = vorx(i,j,k) - vorxm(i,j,k)
                vorym(i,j,k) = vory(i,j,k) - vorym(i,j,k)
                vorzm(i,j,k) = vorz(i,j,k) - vorzm(i,j,k)
                !-------------------------------------------------------------------------------
             enddo
          enddo
       enddo

       normvorpv=0

       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                vorpvx = vory(i,j,k) * vorzm(i,j,k) - vorz(i,j,k) * vorym(i,j,k)
                vorpvy = vorz(i,j,k) * vorxm(i,j,k) - vorx(i,j,k) * vorzm(i,j,k)
                vorpvz = vorx(i,j,k) * vorym(i,j,k) - vory(i,j,k) * vorxm(i,j,k)
                normvorpv(i,j,k)=sqrt(vorpvx**2+vorpvY**2+vorpvz**2)
             enddo
          enddo
       enddo
       !-------------------------------------------------------------------------------
       ! Estimate of angular fluctuation of vorticity
       !-------------------------------------------------------------------------------
       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                val=normvorpv(i,j,k) / (normvor(i,j,k) * normvorm(i,j,k) + 1d-40)
                val=max(min(val,1d0),-1d0)
                ang=asin(val)
                !-------------------------------------------------------------------------------
                ! Estimate of selection function magnitude 
                !-------------------------------------------------------------------------------
                ! Reference Sagaut, 1998, Large Eddy Simulation for Imcompressible Flows
                if (ang>=ang0) then
                   fles(i,j,k) = 1
                else
!!$                   num = ((1.d0 - cos(ang)) / (1.d0 + cos(ang))) &
!!$                        &        / ((1.d0 - cos(ang0)) / (1.d0 + cos(ang0)))
!!$                   num=(tan(ang/2d0)**2)/(tan(ang0/2d0)**2)
!!$                   num = num * num
!!$                   fles(i,j,k) = num*1.65d0
                   fles(i,j,k)=((tan(ang/2d0)**2)/(tan(ang0/2d0)**2))**2
                endif
             enddo
          enddo
       enddo
    endif
    fles=fles*1.65d0
    !-------------------------------------------------------------------------------
    return
  end subroutine selecfunctionLES
  
  
  
  subroutine filtering_var(fvar,var,varo)
    use mod_Parameters, only: sx,ex,sy,ey,sz,ez
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable, intent(in)           :: var
    real(8), dimension(:,:,:), allocatable, intent(in), optional :: varo
    real(8), dimension(:,:,:), allocatable, intent(inout)        :: fvar
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                                      :: i,j,k
    real(8)                                                      :: cop=4
    !-------------------------------------------------------------------------------
    
    fvar=0
    if (.not.present(varo)) then 
       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                fvar(i,j,k) = var(i+1,j+1,k+1) +    cop*var(i+1,j,k+1) +        var(i+1,j-1,k+1) + & ! forward face (k+1)
                     &    cop*var(i  ,j+1,k+1) + cop**2*var(i  ,j,k+1) +    cop*var(i  ,j-1,k+1) + & !
                     &        var(i-1,j+1,k+1) +    cop*var(i-1,j,k+1) +        var(i-1,j-1,k+1) + & !
                     &    cop*var(i+1,j+1,k  ) + cop**2*var(i+1,j,k  ) +    cop*var(i+1,j-1,k  ) + & ! central face (k)
                     & cop**2*var(i  ,j+1,k  ) + cop**3*var(i  ,j,k  ) + cop**2*var(i  ,j-1,k  ) + & !
                     &    cop*var(i-1,j+1,k  ) + cop**2*var(i-1,j,k  ) +    cop*var(i-1,j-1,k  ) + & !
                     &        var(i+1,j+1,k-1) +    cop*var(i+1,j,k-1) +        var(i+1,j-1,k-1) + & ! backward face (k-1)
                     &    cop*var(i  ,j+1,k-1) + cop**2*var(i  ,j,k-1) +    cop*var(i  ,j-1,k-1) + & !
                     &        var(i-1,j+1,k-1) +    cop*var(i-1,j,k-1) +        var(i-1,j-1,k-1)     !
                fvar(i,j,k) = fvar(i,j,k) / (2 + cop)**dim
                fvar(i,j,k) = var(i,j,k) - fvar(i,j,k)
             end do
          end do
       end do
    else 
       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                fvar(i,j,k) = var(i+1,j+1,k+1)*varo(i+1,j+1,k+1) +    cop*var(i+1,j,k+1)*varo(i+1,j,k+1) +        var(i+1,j-1,k+1)*varo(i+1,j-1,k+1) + & ! forward face (k+1)
                     &    cop*var(i  ,j+1,k+1)*varo(i  ,j+1,k+1) + cop**2*var(i  ,j,k+1)*varo(i  ,j,k+1) +    cop*var(i  ,j-1,k+1)*varo(i  ,j-1,k+1) + & !
                     &        var(i-1,j+1,k+1)*varo(i-1,j+1,k+1) +    cop*var(i-1,j,k+1)*varo(i-1,j,k+1) +        var(i-1,j-1,k+1)*varo(i-1,j-1,k+1) + & !
                     &    cop*var(i+1,j+1,k  )*varo(i+1,j+1,k  ) + cop**2*var(i+1,j,k  )*varo(i+1,j,k  ) +    cop*var(i+1,j-1,k  )*varo(i+1,j-1,k  ) + & ! central face (k)
                     & cop**2*var(i  ,j+1,k  )*varo(i  ,j+1,k  ) + cop**3*var(i  ,j,k  )*varo(i  ,j,k  ) + cop**2*var(i  ,j-1,k  )*varo(i  ,j-1,k  ) + & !
                     &    cop*var(i-1,j+1,k  )*varo(i-1,j+1,k  ) + cop**2*var(i-1,j,k  )*varo(i-1,j,k  ) +    cop*var(i-1,j-1,k  )*varo(i-1,j-1,k  ) + & !
                     &        var(i+1,j+1,k-1)*varo(i+1,j+1,k-1) +    cop*var(i+1,j,k-1)*varo(i+1,j,k-1) +        var(i+1,j-1,k-1)*varo(i+1,j-1,k-1) + & ! backward face (k-1)
                     &    cop*var(i  ,j+1,k-1)*varo(i  ,j+1,k-1) + cop**2*var(i  ,j,k-1)*varo(i  ,j,k-1) +    cop*var(i  ,j-1,k-1)*varo(i  ,j-1,k-1) + & !
                     &        var(i-1,j+1,k-1)*varo(i-1,j+1,k-1) +    cop*var(i-1,j,k-1)*varo(i-1,j,k-1) +        var(i-1,j-1,k-1)*varo(i-1,j-1,k-1)     !
                fvar(i,j,k) = fvar(i,j,k) / (2 + cop)**dim
                fvar(i,j,k) = var(i,j,k) - fvar(i,j,k)
             end do
          end do
       end do
    end if
  end subroutine filtering_var


  subroutine Dynamic_SubgridScale_Model(u,v,w,Cs)
    !---------------------------------------------------------------------
    ! Dynamic Subgrid Scale Model
    !---------------------------------------------------------------------
    use mod_borders
    use mod_Parameters, only: Periodic,dx,dy,dz,gx,gy,gz,           &
         & LES_scheme,LES_const_Smago,LES_const_TKE,LES_const_WALE, &
         & sx,ex,sy,ey,sz,ez,                                       &
         & sxs,exs,sys,eys,szs,ezs,                                 &
         & gsx,gex,gsy,gey,gsz,gez,                                 &
         & mesh
    use mod_Constants, only: d1p2,d1p3,d1p4,d3p2,d5p2,d5p4
    use Mod_deriv_xyz
    use mod_InOut
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable, intent(in) :: u,v,w
    real(8), dimension(sx:ex,sy:ey,sz:ez), intent(out) :: Cs
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    real(8), dimension(sx:ex,sy:ey,sz:ez)              :: Cs0
    integer                                            :: i,j,k
    real(8)                                            :: Delta_Bar,vitemp
    real(8), dimension(:,:,:), allocatable             :: vis_sm,SijSji,SijdSjid
    real(8)                                            :: LES_const_MS,alpha
    real(8), dimension(:,:,:), allocatable             :: ubt,vbt,wbt
    real(8), dimension(:,:,:), allocatable             :: uubt,uvbt,uwbt,vwbt,vvbt,wwbt
    real(8), dimension(:,:,:), allocatable             :: DetSbt,Sijbt,DetSSijbt
    real(8)                                            :: cop=4,a1=0.5d0
    real(8), dimension(sx:ex,sy:ey,sz:ez)              :: Lij,Mij,denom,num
    real(8), dimension(3,3)                            :: tensL
    real(8), dimension(:,:,:), allocatable             :: Sij
    logical                                            :: once=.true.
    real(8), dimension(:,:,:), allocatable             :: tmp_field1,tmp_field2,tmp_field3,tmp_field4,tmp_field5
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Alloc 
    !-------------------------------------------------------------------------------
    allocate(ubt(sx:ex,sy:ey,sz:ez))
    allocate(vbt(sx:ex,sy:ey,sz:ez))
    allocate(wbt(sx:ex,sy:ey,sz:ez))
    allocate(uubt(sx:ex,sy:ey,sz:ez))
    allocate(vvbt(sx:ex,sy:ey,sz:ez))
    allocate(wwbt(sx:ex,sy:ey,sz:ez))
    allocate(uvbt(sx:ex,sy:ey,sz:ez))
    allocate(uwbt(sx:ex,sy:ey,sz:ez))
    allocate(vwbt(sx:ex,sy:ey,sz:ez))
    allocate(DetSbt(sx:ex,sy:ey,sz:ez))
    allocate(Sijbt(sx:ex,sy:ey,sz:ez))
    allocate(DetSSijbt(sx:ex,sy:ey,sz:ez))
    !-------------------------------------------------------------------------------
    
    !-------------------------------------------------------------------------------
    ! Init
    !-------------------------------------------------------------------------------
    DetSbt = 0
    Sijbt = 0
    DetSSijbt = 0
    Mij = 0
    Lij = 0
    !-------------------------------------------------------------------------------

    if (dim==3) then
       !-------------------------------------------------------------------------------
       ! Filtered velocities
       !-------------------------------------------------------------------------------
       call filtering_var(ubt,u)
       call filtering_var(vbt,v)
       call filtering_var(wbt,w)
       !-------------------------------------------------------------------------------
       ! Filtered velocities*velocities
       !-------------------------------------------------------------------------------
       call filtering_var(uubt,u,u)
       call filtering_var(vvbt,v,v)
       call filtering_var(wwbt,w,w)
       call filtering_var(uvbt,u,v)
       call filtering_var(uwbt,u,w)
       call filtering_var(vwbt,u,w)
       !-------------------------------------------------------------------------------

       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                !-------------------------------------------------------------------------------
                ! tensL
                !-------------------------------------------------------------------------------
                tensL(1,1)=uubt(i,j,k)-ubt(i,j,k)*ubt(i,j,k)
                tensL(1,2)=uvbt(i,j,k)-ubt(i,j,k)*vbt(i,j,k)
                tensL(1,3)=uwbt(i,j,k)-ubt(i,j,k)*wbt(i,j,k)
                tensL(2,1)=tensL(1,2)
                tensL(2,2)=vvbt(i,j,k)-vbt(i,j,k)*vbt(i,j,k)
                tensL(2,3)=vwbt(i,j,k)-vbt(i,j,k)*wbt(i,j,k)
                tensL(3,1)=tensL(1,3)
                tensL(3,2)=tensL(2,3)
                tensL(3,3)=wwbt(i,j,k)-wbt(i,j,k)*wbt(i,j,k)
                !-------------------------------------------------------------------------------
                ! Compute Germano Identity (Leonard Tensor)
                !-------------------------------------------------------------------------------
                do j1=1,dim
                   do i1=1,dim
                      Lij(i,j,k) = Lij(i,j,k) + tensL(i1,j1)
                      if (i1==j1) then
                         do k1=1,dim
                            Lij(i,j,k)=Lij(i,j,k)-d1p3*tensL(k1,k1)
                         end do
                      end if
                   end do
                end do
                !-------------------------------------------------------------------------------
             end do
          end do
       end do
       !-------------------------------------------------------------------------------
       ! Compute |S|
       !-------------------------------------------------------------------------------
       allocate(SijSji(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
       call compute_SijSji(u,v,w,SijSji)
       !-------------------------------------------------------------------------------
       ! periodicity 
       !-------------------------------------------------------------------------------
       call sca_periodicity(mesh,SijSji)
       SijSji=2*sqrt(SijSji)
       !-------------------------------------------------------------------------------
       
       !-------------------------------------------------------------------------------
       ! Compute Sij
       !-------------------------------------------------------------------------------
       allocate(Sij(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
       call compute_Sij(u,v,w,Sij)
       !-------------------------------------------------------------------------------
       ! periodicity 
       !-------------------------------------------------------------------------------
       call sca_periodicity(mesh,Sij)
       !-------------------------------------------------------------------------------

       !-------------------------------------------------------------------------------
       ! Filtered |S| ( with |S|=2*sqrt(SijSji) )
       ! Filtered Sij
       ! Filtered |S|*Sij
       !-------------------------------------------------------------------------------
       call filtering_var(DetSbt,SijSji)
       call filtering_var(Sijbt,Sij)
       call filtering_var(DetSSijbt,SijSji,Sij)
       !-------------------------------------------------------------------------------

       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                Mij(i,j,k)=2*max(dx(i),dy(j),dz(k))**2*(DetSSijbt(i,j,k) - 2**2*DetSbt(i,j,k)*Sijbt(i,j,k))
             end do
          end do
       end do

       if (.not.once) then
          num=  d1p2*(num+Lij*Mij)
          denom=d1p2*(denom+Mij*Mij)
          do k=sz,ez
             do j=sy,ey
                do i=sx,ex
                   Cs(i,j,k)=num(i,j,k)/denom(i,j,k)!Lij(i,j,k)/Mij(i,j,k)
                   if (abs(Cs(i,j,k)) > 0.2d0) then
                      Cs(i,j,k) = sign(1d0,Cs(i,j,k))*0.2d0
                   end if
                end do
             end do
          end do
       end if

       if (once) then
          Cs0=0
          Cs=0.18d0
          once=.false.
       else
          !Cs=a1*Cs+(1d0-a1)*Cs0
       end if
       Cs0=Cs
       num=Lij*Mij
       denom=Mij*Mij
       
    else
       if (mpi_chief) write(*,*) 'STOP, not implemented in 2D'
    end if
    
  end subroutine Dynamic_SubgridScale_Model

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
end module mod_turbulence_LES
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
