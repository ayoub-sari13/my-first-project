!===============================================================================
module Bib_VOFLag_DirectionOnEulerianMeshFollowingNormalVector
  !===============================================================================
  contains
  !-----------------------------------------------------------------------------  
  !> @author amine chadil
  ! 
  !> @brief cette routine 
  !
  !> @param [in]   
  !-----------------------------------------------------------------------------
  subroutine DirectionOnEulerianMeshFollowingNormalVector (norm,dir)
    !===============================================================================
    !modules
    !===============================================================================
    !-------------------------------------------------------------------------------
    !Modules de definition des structures et variables => src/mod/module_...f90
    !-------------------------------------------------------------------------------
    !===============================================================================
    !declarations des variables
    !===============================================================================
    implicit none
    !-------------------------------------------------------------------------------
    !variables globales
    !-------------------------------------------------------------------------------
    integer, intent(out) :: dir
    real(8), intent(in)  :: norm
    !-------------------------------------------------------------------------------
    !variables locales 
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------
    if(norm.ge.0.) then
      dir = -1
    else
      dir = 1
    endif
  end subroutine DirectionOnEulerianMeshFollowingNormalVector

  !===============================================================================
end module Bib_VOFLag_DirectionOnEulerianMeshFollowingNormalVector
!===============================================================================
  


