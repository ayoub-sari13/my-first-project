!===============================================================================
module Bib_VOFLag_Particle_Position_Update
  !===============================================================================
  contains
  !-----------------------------------------------------------------------------  
  !> @author amine chadil, jorge cesar brandle de motta
  ! 
  !> @brief cette routine permet a chaque iteration d'actualiser la structure vl dans le cas 
  !! de particule spherique
  !
  !> @param[out] vl              : la structure contenant toutes les informations
  !! relatives aux particules.
  !-----------------------------------------------------------------------------
  subroutine Particle_Position_Update(vl,Euler,ndim)
    !===============================================================================
    !modules
    !===============================================================================
    !-------------------------------------------------------------------------------
    !Modules de definition des structures et variables => src/mod/module_...f90
    !-------------------------------------------------------------------------------
    use Module_VOFLag_ParticlesDataStructure
    use Module_VOFLag_SubDomain_Data,        only : m_period
    use mod_Parameters,                      only : deeptracking,dt,xmin,xmax,ymin,&
                                                    ymax,zmin,zmax
    use mod_mpi
    !-------------------------------------------------------------------------------
    !===============================================================================
    !declarations des variables
    !===============================================================================
    implicit none
    !-------------------------------------------------------------------------------
    !variables globales
    !-------------------------------------------------------------------------------
    type(struct_vof_lag), intent(inout) :: vl
    integer             , intent(in)    :: ndim
    integer             , intent(in)    :: Euler
    !-------------------------------------------------------------------------------
    !variables locales 
    !-------------------------------------------------------------------------------
    real(8), dimension(3)               :: coord_deb,coord_fin
    integer                             :: nd,np
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'entree Particle_Position_Update'
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Initialisation
    !-------------------------------------------------------------------------------
    coord_deb(1)=xmin
    coord_deb(2)=ymin
    coord_fin(1)=xmax
    coord_fin(2)=ymax
    if (ndim.eq.3) then
      coord_deb(3)=zmin
      coord_fin(3)=zmax
    endif
    
    !-------------------------------------------------------------------------------
    ! Calculer la nouvelle position
    !-------------------------------------------------------------------------------
    do np=1,vl%kpt
      do nd=1,ndim
        if (vl%objet(np)%in) then 
          if (Euler == 1) then 
            vl%objet(np)%pos(nd) = vl%objet(np)%pos(nd) + dt*vl%objet(np)%v(nd)
          elseif ( Euler == 2) then 
            vl%objet(np)%pos(nd) = vl%objet(np)%pos(nd) + dt*( 3d0*vl%objet(np)%v(nd) - &
                                   vl%objet(np)%vn(nd))*0.5d0
          end if 
          if (m_period(nd)) then
            if ( (vl%objet(np)%pos(nd) .gt. coord_fin(nd)) ) then  
              vl%objet(np)%pos(nd) =  vl%objet(np)%pos(nd) - (coord_fin(nd) - &
                    coord_deb(nd) )
            else if ( vl%objet(np)%pos(nd) .lt. coord_deb(nd)) then
              vl%objet(np)%pos(nd) =  vl%objet(np)%pos(nd) + (coord_fin(nd) - &
                    coord_deb(nd) )
            end if
          end if
        end if
      end do
    end do

    !-------------------------------------------------------------------------------
    !Informer tous les procs de la nouvelle vitesse de la part
    !-------------------------------------------------------------------------------
    if (nproc > 1) then
      do np=1,vl%kpt
         do nd=1,ndim
            call mpi_bcast(vl%objet(np)%pos(nd),1,mpi_double_precision,vl%objet(np)%locpart,comm3d,code)  
         end do
      end do
    end if 
    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'sortie Particle_Position_Update'
    !-------------------------------------------------------------------------------
  end subroutine Particle_Position_Update

  !===============================================================================
end module Bib_VOFLag_Particle_Position_Update
!===============================================================================