!========================================================================
!
!                   FUGU Version 1.0.0
!
!========================================================================
!
! Copyright  Stéphane Vincent
!
! stephane.vincent@u-pem.fr          straightparadigm@gmail.com
!
! This file is part of the FUGU Fortran library, a set of Computational 
! Fluid Dynamics subroutines devoted to the simulation of multi-scale
! multi-phase flows for resolved scale interfaces (liquid/gas/solid). 
! FUGU uses the initial developments of DyJeAT and SHALA codes from 
! Jean-Luc Estivalezes (jean-luc.estivalezes@onera.fr) and 
! Frédéric Couderc (couderc@math.univ-toulouse.fr).
!
!========================================================================
!**
!**   NAME       : type_struct_eq.f90
!**
!**   AUTHOR     : Stéphane Vincent
!**
!**   FUNCTION   : types for conservation equations of FUGU software
!**
!**   DATES      : Version 1.0.0  : from : january, 16, 2017
!**
!========================================================================

#include "precomp.h"

!===============================================================================
! Data structure and types for conservation equations
!===============================================================================

!===============================================================================
module mod_thermophys
  !===============================================================================

  type phase
     !------------------------------------------------------
     ! equation of state :
     ! --> 0,1 : color,liquid
     ! --> 2   : ideal gas, rho = p / (r T)
     ! --> 3   : isotherm, rho = p / (r T_0)
     ! --> -1  : compressible, xit = 1 / p
     !------------------------------------------------------
     integer :: eos=0
     !------------------------------------------------------
     real(8) :: rho=1,mu=1,xit=1,sigma=1
     real(8) :: vie=1,vir=1,vis=1
     real(8) :: lambda=1,cp=1
     real(8) :: dif=1
     !------------------------------------------------------
     ! specific gas constant
     !------------------------------------------------------
     real(8) :: r=1
     !------------------------------------------------------
     ! beta and tpb for Boussinesq: rho = rho (1-beta(tp-tpb)) in rho g 
     !------------------------------------------------------
     real(8) :: beta=0,tpb=0 
     !------------------------------------------------------
     ! type (overwrite all values according database)
     !------------------------------------------------------
     character(20) :: type=""
     !------------------------------------------------------
  end type phase
  
  type(phase),dimension(:), allocatable :: fluids

  namelist /nml_prop_fluids/ fluids

contains

  subroutine read_nml_prop_fluids
    implicit none

    integer :: i

    open(unit=10,file="data.in",status="old",action="read")
    read(10,nml=nml_prop_fluids)
    close(10)

    do i=1,size(Fluids(:)%type)
       select case(fluids(i)%type)
       case("")
       case("water","water-liquid")
          fluids(i)%rho=998.2
          fluids(i)%mu=1.003d-3
          fluids(i)%lambda=6d-1
          fluids(i)%cp=4182
          fluids(i)%xit=4.6d-10
       case("air")
          fluids(i)%rho=1.225
          fluids(i)%mu=1.7894d-5
          fluids(i)%lambda=2.42d-2
          fluids(i)%cp=1006.43
          fluids(i)%xit=1d-5 ! 1/p_ref (ideal gas at SPT)
       case default
          write(*,*) "Unknown material"
          write(*,*) "Check material properties"
          write(*,*) "STOP"
          stop 
       end select
    end do
    
  end subroutine read_nml_prop_fluids
  
  !===============================================================================
end module mod_thermophys
!===============================================================================

!===============================================================================
module mod_boundary
  !===============================================================================
  
  !------------------------------------------------------
  ! left, right, top, bottom = 
  !    0: Dirichlet
  !    1: Neumann
  !    2: Symmetry
  !    3: Sliding
  !    4: Periodic
  !------------------------------------------------------
  type boundary
     integer :: left,right,top,bottom,backward,forward
  end type boundary

  !===============================================================================
end module mod_boundary
!===============================================================================

!===============================================================================
module mod_struct_Solver
  !===============================================================================

#if MUMPS
  include 'dmumps_struc.h'
#endif 
  
  type struct_solver_out
     integer :: iterations
     real(8) :: residual
  end type struct_solver_out

  type struct_solver_it
     integer :: type
     integer :: iterations
     real(8) :: threshold
  end type struct_solver_it

  type struct_augmented_Lagrangian
     integer                            :: it_lag     ! number of augmented Lagrangian iterations done
     integer, dimension(:), allocatable :: it_solver  ! number of solver iterations done
     real(8), dimension(:), allocatable :: res_solver ! solver residual
     real(8), dimension(:), allocatable :: div,divmax ! norm of divergence of the solution
  end type struct_augmented_Lagrangian

  !===============================================================================
end module mod_struct_Solver
!===============================================================================

