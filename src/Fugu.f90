!========================================================================
!
!                   FUGU Version 1.0.0
!
!========================================================================
!
! Copyright  Stéphane Vincent
!
! stephane.vincent@u-pem.fr          straightparadigm@gmail.com
!
! This file is part of the FUGU Fortran library, a set of Computational 
! Fluid Dynamics subroutines devoted to the simulatiiquid/gas/solid). 
!
!========================================================================
!**
!**   NAME       : Fugu.f90
!**
!**   AUTHOR     : Stéphane Vincent
!**
!**   FUNCTION   : Main program of FUGU software
!**
!**   DATES      : Version 1.1.0  : from : jan, 18, 2018
!**
!========================================================================

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#include "precomp.h"
program FUGU
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  !*******************************************************************************!
  !*******************************************************************************!
  !*******************************************************************************!
  !   Modules                                                                     !
  !*******************************************************************************!
  !*******************************************************************************!
  !*******************************************************************************!
  !*******************************************************************************!
  use mod_ADE
  use mod_Parameters 
  use mod_allocate
  use mod_borders
  use mod_Constants
  use mod_InOut
  use mod_Init_Lagrangian_Particle
  use mod_struct_Particle_Tracking
  use mod_Lagrangian_Particle
  use mod_ParticleConnectivity
  use mod_Navier
  use mod_struct_thermophysics
  use mod_Init_Navier
  use mod_Init_EQ
  use mod_Init_Heat
  use mod_Heat
  use mod_height_function
  use mod_references
  use mod_LevelSet
  use mod_VOF_2D3D
  use mod_VOF_Cons
  use mod_struct_solver
  use mod_mpi
  use mod_explicit_advection
  use mod_pen
  use mod_outputs
  use mod_turbulence_LES
  use mod_InterpLin
  use mod_Aslam
  use mod_VOF_Tools
  use mod_CoeffVS
  use mod_MomentumConserving
  use mod_operators
  use mod_thi
  use mod_timers
  use mod_messages
  use mod_cfl
  use mod_stl
  use mod_phase_field
  use mod_Lagrangian2Eulerian
#if RESPECT
  use Module_VOFLag_ParticlesDataStructure
  !-------------------------------------------------------------------------------
  !Modules de subroutines de la librairie RESPECT => src/Bib_VOFLag_...f90
  !-------------------------------------------------------------------------------
  use Bib_VOFLag_ParticlesDataStructure_SubDomainProcData
  use Bib_VOFLag_RESPECT_PostTreatementAndMore
  use Bib_VOFLag_caracteristiques_thermophys
  use Bib_VOFLag_ComputationTime_StartTimer
  use Bib_VOFLag_ComputationTime_StopTimer
  use Bib_VOFLag_Particle_PhaseFunction
  use Bib_VOFLag_RESPECT_Initialization
  use Bib_VOFLag_Particle_Collisions
  use Bib_VOFLag_RESPECT_Resolutions
  use Bib_VOFLag_WallCreaction
#endif
  !*******************************************************************************!
  !*******************************************************************************!
  !*******************************************************************************!

  !***************************************************************!
  implicit none
  !***************************************************************!


  !*******************************************************************************!
  !*******************************************************************************!
  !*******************************************************************************!
  ! Local variables
  !*******************************************************************************!
  !*******************************************************************************!
  !*******************************************************************************!

  !***************************************************************!
  ! Unknowns       
  !***************************************************************!
  real(8), dimension(:,:,:),   allocatable :: u,v,w,u0,v0,w0,u1,v1,w1
  real(8), dimension(:,:,:),   allocatable :: um,vm,wm
  real(8), dimension(:,:,:),   allocatable :: uvof,vvof,wvof
  real(8), dimension(:,:,:),   allocatable :: uref,vref,wref
  real(8), dimension(:,:,:),   allocatable :: grdpu,grdpv,grdpw
  real(8), dimension(:,:,:),   allocatable :: dist
  real(8), dimension(:,:,:),   allocatable :: cou,coub,volume_cell
  real(8), dimension(:,:,:),   allocatable :: pres,pres0,dpres,div
  real(8), dimension(:,:,:),   allocatable :: rho,vie,xit
  real(8), dimension(:,:,:),   allocatable :: rovu,rovv,rovw
  real(8), dimension(:,:,:),   allocatable :: rovu0,rovv0,rovw0
  real(8), dimension(:,:,:),   allocatable :: mu_sgs,cond_sgs
  real(8), dimension(:,:,:,:), allocatable :: vis,vir
  real(8), dimension(:,:,:),   allocatable :: mean_stfu,mean_stfv,mean_stfP
  real(8), dimension(:,:,:),   allocatable :: mean_curvu,mean_curvv,mean_curvP
  real(8), dimension(:,:,:),   allocatable :: slvu,slvv,slvw,smvu,smvv,smvw
  real(8), dimension(:,:,:),   allocatable :: uin,vin,win,bip,pin
  real(8), dimension(:,:,:),   allocatable :: stdy_uin,stdy_vin,stdy_win
  real(8), dimension(:,:,:,:), allocatable :: penu,penv,penw,penp
  real(8), dimension(:,:,:),   allocatable :: obj
  !***************************************************************!
  real(8), dimension(:,:,:),   allocatable :: tp,tp0,tp1
  real(8), dimension(:,:,:),   allocatable :: condu,condv,condw,cp
  real(8), dimension(:,:,:),   allocatable :: beta,tpb
  real(8), dimension(:,:,:),   allocatable :: sltp,smtp
  real(8), dimension(:,:,:),   allocatable :: tpin
  real(8), dimension(:,:,:,:), allocatable :: pentp
  real(8), dimension(:,:,:,:), allocatable :: sptp
  !***************************************************************!
  real(8), dimension(:,:,:),   allocatable :: difu,difv,difw
  real(8), dimension(:,:,:),   allocatable :: slcon,smcon
  real(8), dimension(:,:,:),   allocatable :: conin
  real(8), dimension(:,:,:,:), allocatable :: pencon
  real(8), dimension(:,:,:,:), allocatable :: spcon
  !***************************************************************! 
  real(8), dimension(:,:,:),   allocatable :: couin
  real(8), dimension(:,:,:,:), allocatable :: pencou
  !***************************************************************! 

  !***************************************************************! 
  ! variable for CFL
  !***************************************************************! 
  real(8), dimension(:,:,:),   allocatable :: u_cfl,v_cfl,w_cfl
  !***************************************************************! 

  !***************************************************************! 
  ! variable for momentum conserving
  !***************************************************************! 
  real(8), dimension(:,:,:),   allocatable :: uadv2,vadv2,wadv2
  !***************************************************************! 
  ! work 
  real(8), dimension(:,:,:),   allocatable :: work1,work2,work3,work4,work5
  real(8), dimension(:,:,:),   allocatable :: work6,work7,work8,work9
  real(8), dimension(:,:,:),   allocatable :: wtmp1,wtmp2,wtmp3
  real(8), dimension(:,:,:),   allocatable :: work1u,work1v,work1w
  real(8), dimension(:,:),     allocatable :: work2d1

  !***************************************************************! 
  ! variable for particle tracking
  !***************************************************************! 
  type(particle_t), allocatable, dimension(:) :: LPart   ! Classical lagrangian particle
  type(particle_t), allocatable, dimension(:) :: SPart   ! Lagrangian particle for scalar MEL scheme
  type(particle_t), allocatable, dimension(:) :: VPart   ! Lagrangian particle for velocity MEL scheme
  type(particle_t), allocatable, dimension(:) :: CPart   ! Lagrangian particle for lagragian VOF scheme

  !***************************************************************!
  ! Derived types of variables for equations                    
  !***************************************************************!
  type(solver_ns_t)                        :: ns
  type(solver_sca_t)                       :: energy,phi
  !***************************************************************!
  ! integer temporary 
  !***************************************************************!
  integer                                  :: nt,ntt,i,j,k,n
  integer                                  :: cpt_init,cpt_fin,cpt_max,freq,cpt,l,ierr
  integer                                  :: ii,jj,kk,ll
  integer                                  :: nb_subdivision,lchar,start_loop,num,n_reprise,nfx,nfy
  integer                                  :: cwrite=1
  integer                                  :: start_stats=0
  integer, dimension(3)                    :: tabi
  !***************************************************************!
  ! scalar temporary 
  !***************************************************************!
  real(8)                                  :: cpu=0,t1=0,t2=0
  real(8)                                  :: full_t1=0,full_t2=0
  real(8)                                  :: time=0
  real(8)                                  :: x,y,z,tmp
  real(8)                                  :: x1,y1,x2,y2,x3,y3
  real(8)                                  :: var,varmax
  real(8)                                  :: fact
  real(8)                                  :: var0,var1,var2,var3,var4
  real(8)                                  :: var5,var6,var7,var8,var9
  real(8), dimension(3)                    :: err,pos,vel
  !------------------------------------------------------------------
  character(len=80)                        :: VarName
  character(len=50)                        :: name,name0,name1
  character(len=20)                        :: pid_char
  !------------------------------------------------------------------
  real(8)                                  :: r,radius,xc0,yc0
  logical                                  :: init_once=.true.
  logical                                  :: yes
  !------------------------------------------------------------------
  integer                                  :: iter_to_restart
  character(8)                             :: cdate
  character(10)                            :: ctime
  character(5)                             :: czone
  integer, dimension(8)                    :: values
  integer, dimension(9)                    :: gmtvalues,gmtvalues_cpu
  !------------------------------------------------------------------
  integer                                  :: cpt_eta=0
  real(8), dimension(100)                  :: eta=0
  !------------------------------------------------------------------
  logical                                  :: exit_loop=.false.
  !------------------------------------------------------------------
  real(8)                                  :: eps
  real(8), dimension(3)                    :: vp1,vp2,vp3,vp4,position,position0
  !------------------------------------------------------------------
  real(8)                                  :: int_cpluss_injection,int_cmoins_injection,int_diff_injection
  real(8)                                  :: int_cpluss_extraction,int_cmoins_extraction,int_diff_extraction
  real(8)                                  :: int_cpluss_frontale,int_cmoins_frontale,int_diff_frontale
  integer, dimension(3)                    :: ijk
  real(8), dimension(3)                    :: xyz

  ! ajout can
  real(8), dimension(:,:,:),   allocatable :: curvature,norm_grad,g_of_phi
  type(phase_field_t)                      :: phase_field
  ! fin ajout can

#if RESPECT 
  !***************************************************************! 
  ! variables for RESPECT
  !***************************************************************! 
  real(8), dimension(:,:,:,:), allocatable :: cou_vis,cou_vts
  real(8), dimension(:,:,:,:), allocatable :: cou_visin,cou_vtsin
  real(8), dimension(:,:,:),   allocatable :: Rwork,Rwork1,Rwork2,Rwork3
  real(8), dimension(:,:,:),   allocatable :: Ru0,Rv0,Rw0
  real(8), dimension(:,:,:),   allocatable :: slvuin,slvvin,slvwin,smvuin,smvvin,smvwin
  real(8), dimension(:,:,:),   allocatable :: couin0
  type(struct_vof_lag) :: vl
#endif
  !*******************************************************************************!
  !*******************************************************************************!
  !*******************************************************************************!
  ! End of local variables
  !*******************************************************************************!
  !*******************************************************************************!
  !*******************************************************************************!


  !*******************************************************************************!  
  ! Initialization of parallel MPI
  !*******************************************************************************!  
  call initialization_MPI
  !*******************************************************************************!  

  
  !***************************************************************!
  !***************************************************************!
  !***************************************************************!
  ! Running file
  !***************************************************************!
  !***************************************************************!
  !***************************************************************!
  if (rank==0) then
     open(19,file="running")
     call date_and_time(cdate,ctime,czone,values)
     write(19,'(1x,a,6(a))') 'date: ', cdate(7:8),"/",cdate(5:6),"/",cdate(1:4) 
     write(19,'(1x,a,5(a))') 'time: ', ctime(1:2),":",ctime(3:4),":",ctime(5:6)
     write(19,'(1x,a,a5)') 'zone: ', czone
     write(19,'(8i5)')  values
     close(19)
  end if
  !***************************************************************!
  !***************************************************************!
  !***************************************************************!


  !*******************************************************************************!  
  ! Print Logo
  !*******************************************************************************!
  call print_message("       _________   ___   ___   _________   ___   ___    ________  ")
  call print_message("      /  _____ /  /  /  /  /  / _______/  /  /  /  /   /____   /  ")
  call print_message("     /  /___     /  /  /  /  / / _____   /  /  /  /   _____/  /   ")
  call print_message("    /  ____/    /  /  /  /  / / /__  /  /  /  /  /   /____   /    ")
  call print_message("   /  /        /  /__/  /  / /____/ /  /  /__/  /   _____/  /     ")
  call print_message("  /__/        /________/  /________/  /________/   /_______/      ")
  call print_message("                                                                  ")
  call print_message("                          verSion 3.0.0                           ")
  call print_message("                                                                  ")
  !*******************************************************************************!  
  
  
  !*******************************************************************************!  
  ! Timers init
  !*******************************************************************************!  
  full_t1=MPI_Wtime()
  call compute_time(TIMER_START,"WALL CLOCK TIME")
  call compute_time(TIMER_START,"Before time loop")
  !*******************************************************************************!  

  
  !*******************************************************************************!  
  ! Reading inputs
  !*******************************************************************************!  
  call read_parameters
  !*******************************************************************************!  
  ! to be continued after allocation for fluid properties
  !*******************************************************************************!

  
  !*******************************************************************************!  
  ! Initialization of MPI topology and derived types for exchanges
  !*******************************************************************************!  
  call topology_MPI
  !*******************************************************************************!  

  
  !*******************************************************************************!  
  ! Reading inputs
  !*******************************************************************************!  
  call init_parameters
  !*******************************************************************************!   

  
  !*******************************************************************************!
  !*******************************************************************************!
  !*******************************************************************************!
  ! Memory allocation for global variables
  !*******************************************************************************!
  !*******************************************************************************!
  !*******************************************************************************!

  include 'Alloc.f90'
  
  !*******************************************************************************!  
  ! Reading inputs (continuation)
  !*******************************************************************************!  
  call read_nml_prop_fluids
  open(unit=10,file="data.in",status="old",action="read")
  read(10,nml=nml_prop_particles) ; rewind(10)
  close(10)
  !*******************************************************************************!  
  ! Invicid case
  !*******************************************************************************!  
  do i=1,NB_phase
     if (fluids(i)%mu==0) NS_invicid=.true.
  end do
  !*******************************************************************************!  

  
  !*******************************************************************************!
  !*******************************************************************************!
  !*******************************************************************************!

  !*******************************************************************************!
  !*******************************************************************************!
  !*******************************************************************************!
  ! Initial and boundary conditions of the problem                            
  !*******************************************************************************!
  !*******************************************************************************!
  !*******************************************************************************!

  !***************************************************************!
  ! Initialisation of unknowns and thermodynamic variables
  !***************************************************************!
  ! Initialization of time variables                       
  !***************************************************************!
  call Init_Time_Variables(nt,start_loop)

  !***************************************************************!
  ! Initialization of boundary conditions / penalty terms       
  !***************************************************************!
  !----------------------------------------------------------------
  ! For Navier-Stokes/Stokes/Euler equations
  !----------------------------------------------------------------
  if (NS_activate) then
     call Init_BC_Navier_Stokes(uin,vin,win,bip,pin,penu,penv,penw,ns)
     select case(NS_method)
     case(1)
        !----------------------------------------------------------------
        ! neumann pressure schur operator
        !----------------------------------------------------------------
        pin=0 
        call Init_BC_phi(pin,penp,phi,ns)
        !----------------------------------------------------------------
     case(2)
        if (PJ_method/=0) call Init_BC_phi(pin,penp,phi,ns)
     end select
  end if
  !----------------------------------------------------------------

  !----------------------------------------------------------------
  ! For Energy equation
  !----------------------------------------------------------------
  if (EN_activate) then
     call Init_BC_Energy(tpin,pentp,energy)
  end if
  !----------------------------------------------------------------
  


  !----------------------------------------------------------------
  ! For Navier-Stokes/Stokes/Euler equations
  !----------------------------------------------------------------
  if (NS_activate) then 
     call Init_Navier_Stokes(u,v,w,u0,v0,w0,u1,v1,w1,pres,pres0,div,rho,&
          & vie,vis,vir,xit,rovu,rovv,rovw,slvu,slvv,slvw,smvu,smvv,smvw,cou,coub,tp,fluids,ns)
     if (NS_linear_term) then
        call Penalty_NS(zero,dt,&
             & cou,             &
             & smvu,smvv,smvw,  &
             & slvu,slvv,slvw,  &
             & uin,vin,win,     &
             & penu,penv,penw,  &
             & obj)
        if (STL_infile%nObj>0) then
           do n=1,STL_infile%nObj
              if (STL_infile%obj(1)%icase>0) then
                 call Penalty_STL(n,slvu,slvv,slvw,obj)
              end if
           end do
        end if
     end if
     if (VOF_activate.or.LS_activate.or.FT_Delta.or.NB_phase==1) then
        call thermophysics(rho,rovu,rovv,rovw,vie,vis,vir,xit,cou,pres,tp,fluids)
     endif
  end if
  !----------------------------------------------------------------

  !----------------------------------------------------------------
  ! For Energy equation
  !----------------------------------------------------------------
  if (EN_activate) then 
     call Init_Energy(tp,tp0,tp1,rho, &
          & condu,condv,condw,        &
          & cp,sltp,smtp,             &
          & cou,beta,tpb,fluids,energy)
     if (EN_linear_term.or.EN_penalty_term) then 
        call Penalty_Sca(zero,dt,  &
             & tp,cou,             &
             & smtp,sltp,sptp,     &
             & rovu,rovv,rovw,cp,  &
             & condu,condv,condw,  &
             & slvu,slvv,slvw,     &
             & u,v,w)
     end if
  end if
  !----------------------------------------------------------------

  !***************************************************************!
  ! Initialization of variables for all equations
  !***************************************************************!
  call EQ_init(mesh,        &
       & zero,              &
       & u,v,w,             &
       & u0,v0,w0,          &
       & u1,v1,w1,          &
       & uin,vin,win,       &
       & grdpu,grdpv,grdpw, &
       & pres,pres0,        &
       & tp,tp0,tp1,        &
       & cou,dist,          &
       & nt)
  !***************************************************************!

 

  !***************************************************************!
  ! Initialization of Lagrangian particle tracking
  !***************************************************************!
  if (LG_activate.or.LG_advection) then
     call Init_BC_Lag
     ! -----------------------------------------------------------
     ! Lagrangian physical particles
     ! -----------------------------------------------------------
     if (LG_activate) then 
        call Init_Particle_Lag(LPart,nPart)
        call derived_types_particle(LPart,nPart)
     end if
     ! -----------------------------------------------------------
     ! Particle for inertial schemes
     ! -----------------------------------------------------------
     if (LG_advection) then
        ! -----------------------------------------------------------
        ! Scalar advection
        ! -----------------------------------------------------------
        select case(EN_inertial_scheme)
        case(-19:-10,-29:-20)
           call Init_SParticle_Lag(SPart,nPartS,tp)
           call derived_types_particle(SPart,nPartS)
        end select
        ! -----------------------------------------------------------
        ! Momentum advection
        ! -----------------------------------------------------------
        select case(NS_inertial_scheme)
        case(-19:-10)
           call Init_VParticle_Lag(VPart,nPartV,u,v,w)
           call derived_types_particle(VPart,nPartV)
        end select
        ! -----------------------------------------------------------
        ! VOF advection
        ! -----------------------------------------------------------
        select case(VOF_method)
        case(-39:-30)
           call Init_CParticles(CPart,nPartC,cou)
           call derived_types_particle(CPart,nPartC)
        end select
        ! -----------------------------------------------------------
     end if
     ! -----------------------------------------------------------
  end if
  !***************************************************************!


  
  
  !*****************************************************************!
  !   Compute CFL condition and coef_time                           !
  !*****************************************************************!
  call allocate_array_3(ALLOCATE_ON_U_MESH,u_cfl,mesh)
  call allocate_array_3(ALLOCATE_ON_V_MESH,v_cfl,mesh)
  call allocate_array_3(ALLOCATE_ON_W_MESH,w_cfl,mesh)

  u_cfl = max(abs(uin),abs(u))
  v_cfl = max(abs(vin),abs(v))
  if (mesh%dim==3) then 
     w_cfl = max(abs(win),abs(w))
  end if

  cfl%dt_old = dt
  cfl%dt_new = dt

  call adapt_time_step(cfl,start_loop,u_cfl,v_cfl,w_cfl,coef_time_1,coef_time_2,coef_time_3)
  dt        = cfl%dt_new
  ns%dt     = dt ! used for projection method and compressible terms

  call CFL_print(cfl)

  call deallocate_array_3(u_cfl)
  call deallocate_array_3(v_cfl)
  call deallocate_array_3(w_cfl)
  !*****************************************************************!
#if RESPECT
   call RESPECT_Initialization(vl,u,v,w,Ru0,Rv0,Rw0,u1,v1,w1,pres,pres0,tp,&
                               tp0,tp1,penu,penv,penw,uin,vin,win,rho,rovu,&
                               rovv,rovw,vie,vir,vis,cou,cou_vis,cou_vts,  &
                               couin0,cou_visin,cou_vtsin,xit,slvu,slvv,   &
                               slvw,smvu,smvv,smvw,slvuin,slvvin,slvwin,   &
                               smvuin,smvvin,smvwin,Rwork,Rwork1,Rwork2,   &
                               Rwork3)
#endif


  !***************************************************************!
  !***************************************************************!
  ! Initialization of temporary variables
  !***************************************************************!

  !*******************************************************************************!
  !*******************************************************************************!
  !*******************************************************************************!
  ! End of initial conditions for the problem                
  !*******************************************************************************!
  !*******************************************************************************!
  !*******************************************************************************!

  !*******************************************************************************!
  !*******************************************************************************!
  !*******************************************************************************!
  ! Storing datas and files
  !*******************************************************************************!
  !*******************************************************************************!
  !*******************************************************************************!
  !***************************************************************!
  !   Generation of vizualization file                            !
  !***************************************************************!
  call Write_fields(start_loop,time,mesh,u,v,w,pres,div,tp,cou, &
          & obj,work1,work2,work3,Rwork,Rwork1,Rwork2,Rwork3)
  call Write_Particles(start_loop,time, &
       & LPart,nPart,                   &
       & SPart,nPartS,                  &
       & VPart,nPartV,                  &
       & CPart,nPartC )
  !***************************************************************!

  
  !*******************************************************************************!
  !                            Reading of restart file
  !*******************************************************************************!
  if (restart_EQ>=0) then
     !----------------------------------------------------------------
     ! For Navier-Stokes/Stokes/Euler
     !----------------------------------------------------------------
     if (NS_activate.or.EN_activate)  then
        !**** RESPECT
        call read_write_EQ_mpi(start_loop,time,start_stats, &
             & cfl%dt_new,cfl%dt_old,                       &
             & u,v,w,u0,v0,w0,pres,                         &
             & tp,tp0,cou,dist,                             &
             & work4,work5,work6,c_read,vl)
        call ParticlesDataStructure_SubDomainProcData(vl)
        if (Wall_Creaction) call WallCreaction(cou,cou_vis,cou_vts,couin0,cou_visin,&
                                           cou_vtsin,slvu,slvv,slvw,smvu,smvv,smvw, &
                                           slvuin,slvvin,slvwin,smvuin,smvvin,      &
                                           smvwin,WallPos)
        call Particle_PhaseFunction(cou,cou_vis,cou_vts,couin0,cou_visin,cou_vtsin,vl)
        call caracteristiques_thermophys(rho,rovu,rovv,rovw,vie,vir,vis,cou,cou_vis,cou_vts,dim)
        !**** RESPECT
        call EN_time(tp,tp0,tp1,nt)
        call EQ_time(u,v,w,u0,v0,w0,u1,v1,w1,pres,pres0,nt)
        
     end if
     
     !----------------------------------------------------------------
     ! For Lagrangian particle tracking
     !----------------------------------------------------------------
  end if
  !*******************************************************************************!

  
  !*******************************************************************************!
  !*******************************************************************************!
  !*******************************************************************************!
  ! End of storing datas and files
  !*******************************************************************************!
  !*******************************************************************************!
  !*******************************************************************************!
  
  


  !*******************************************************************************!
  !*******************************************************************************!
  !*******************************************************************************!
  ! Save and print namelist 
  !*******************************************************************************!
  !*******************************************************************************!
  !*******************************************************************************!
  if (rank==0) then
     open(unit=10,file="nml_data.out",action="write")
     write(10,nml=nml_mpi)
     write(10,nml=nml_space_integration)
     write(10,nml=nml_grid)
     write(10,nml=nml_time_integration)
     write(10,nml=nml_data_files)
     write(10,nml=nml_debug)
     write(10,nml=nml_gravity)
     write(10,nml=nml_nvstks_solver)
     write(10,nml=nml_les)
     write(10,nml=nml_energy_solver)
     write(10,nml=nml_interface_tracking)
     write(10,nml=nml_lagrangian_particle)
     write(10,nml=nml_bc_lag)
     write(10,nml=nml_bc_navier)
     write(10,nml=nml_bc_heat)
     write(10,nml=nml_prop_fluids)
     write(10,nml=nml_prop_particles)
     write(10,nml=nml_stl)
     close(10)
  end if
  !call system("cat data.out")
  !*******************************************************************************!
  !*******************************************************************************!
  !*******************************************************************************!
  ! End Save and print namelist 
  !*******************************************************************************!
  !*******************************************************************************!
  !*******************************************************************************!


#define TEST_IO_MPI 0
#if TEST_IO_MPI

  ii=0
  do i=1,100 
     call read_write_EQ_mpi(l,time,cStats, &
          & cfl%dt_new,cfl%dt_old,         &
          & u,v,w,u0,v0,w0,pres,           &
          & tp,tp0,cou,dist,               &
          & work4,work5,work6,c_write)


     if (ii<20) then
        ii=ii+1
     else
        ii=0
     end if

     restart_EQ=ii
     
     call read_write_EQ_mpi(l,time,cStats, &
          & cfl%dt_new,cfl%dt_old,         &
          & u,v,w,u0,v0,w0,pres,           &
          & tp,tp0,cou,dist,               &
          & work4,work5,work6,c_read)
  end do
  stop
#endif
  
  if (PF_activate) then
     call initial_phase_field_setup (u0, v0, tp0, curvature, norm_grad, g_of_phi, phase_field)
     tp = tp0
  endif

!!$  if (VOF_activate) then
!!$     call allocate_array_3(ALLOCATE_ON_P_MESH,work1,mesh)
!!$     call VOF_tension(mesh,cou,u,v,w,work1)
!!$     call Write_fields(1,time,mesh,u,v,w,cou,work1)
!!$     stop
!!$  end if

!!$  if (VOF_activate) then
!!$     call allocate_array_3(ALLOCATE_ON_P_MESH,work4,mesh)
!!$     call allocate_array_3(ALLOCATE_ON_P_MESH,work5,mesh)
!!$     call allocate_array_3(ALLOCATE_ON_P_MESH,work6,mesh)
!!$     call allocate_array_3(ALLOCATE_ON_P_MESH,work7,mesh)
!!$     call allocate_array_3(ALLOCATE_ON_P_MESH,work8,mesh)
!!$
!!$
!!$     call HF_compute_kappa(mesh,cou,work7)
!!$
!!$     do j=mesh%syu,mesh%eyu
!!$        do i=mesh%sxu,mesh%exu
!!$           u(i,j,1) = -(cou(i,j,1)-cou(i-1,j,1))/mesh%dxu(i)
!!$        end do
!!$     end do
!!$
!!$     do j=mesh%syv,mesh%eyv
!!$        do i=mesh%sxv,mesh%exv
!!$           v(i,j,1) = -(cou(i,j,1)-cou(i,j-1,1))/mesh%dyv(j)
!!$        end do
!!$     end do
!!$
!!$     call Diffusion_cou(cou,VOF_vsmooth,couin,pencou)
!!$
!!$     call tension(cou,rho,work4,work5,work6,mean_curvu,mean_curvv,work8)
!!$
!!$     
!!$     
!!$     call Write_fields(1,time,mesh,u,v,w,cou,work7,work8,work4,work5)
!!$     write(*,*) 'allo'
!!$     stop
!!$  end if

  
  !***********************************************************************************************!
  !***********************************************************************************************!
  !***********************************************************************************************!
  !***********************************************************************************************!
  !                                   Start of the time loop                                      !
  !***********************************************************************************************!
  !***********************************************************************************************!
  !***********************************************************************************************!
  !***********************************************************************************************!


  !*****************************************************************!
  ! init time counter
  !*****************************************************************!
  t1=MPI_Wtime()
  call compute_time(TIMER_END,"Before time loop")
  call compute_time(TIMER_START,"Time loop")
  !*****************************************************************!

  !*****************************************************************!
  !   Loop over time                                                !
  !*****************************************************************!
  ntt=0
  do nt = (start_loop+1),nstep+start_loop
    call ComputationTime_StartTimer(1)


     ntt=ntt+1
     !*****************************************************************!
     ! Update of Inputs parameters
     !*****************************************************************!
     if (modulo(nt,ireadin)==0) then
        call change_parameters
     end if
     !*****************************************************************!

     
     !*****************************************************************!
     ! Adapt time step according cfl value
     !*****************************************************************!
     if (cfl%active) then
        call adapt_time_step(cfl,nt,u,v,w,coef_time_1,coef_time_2,coef_time_3)
        dt               = cfl%dt_new
        ns%dt            = dt
        if (istep>0) then
           if (modulo(nt,istep)==0) call CFL_print(cfl)
        end if
     end if
     !*****************************************************************!

     
     !------------------------------------------------------------------
     ! Adams-Bashforth velocity extrapolation
     !------------------------------------------------------------------
     if (NS_activate) then
        if (Euler==2) then
           if (nt>1) then
              call Adams_Bashforth_velocity_extrapolation(u,u0,u1,cfl%dt_new,cfl%dt_old)
              call Adams_Bashforth_velocity_extrapolation(v,v0,v1,cfl%dt_new,cfl%dt_old)
              if (dim==3) call Adams_Bashforth_velocity_extrapolation(w,w0,w1,cfl%dt_new,cfl%dt_old)
           end if
        end if
     end if
     !------------------------------------------------------------------
     


     !*****************************************************************!
     ! time at the END of the time step !!!
     !*****************************************************************!
     time=time+dt
     !*****************************************************************!




     !***************************************************************!
     !***************************************************************!
     !***************************************************************!
     ! Fixed time evolution of velocity and pressure    
     !***************************************************************!
     !***************************************************************!
     !***************************************************************!
     if (NS_time_vel.or.NS_time_pre) then
        call EQ_init(mesh,        &
             & time-dt,           &
             & u,v,w,             &
             & u0,v0,w0,          &
             & u1,v1,w1,          &
             & uin,vin,win,       &
             & grdpu,grdpv,grdpw, &
             & pres,pres0,        &
             & tp,tp0,tp1,        &
             & cou,dist,          &
             & nt-1)
     end if
     !***************************************************************!
     !***************************************************************!
     !***************************************************************!
     ! End Time evolution of velocity and pressure    
     !***************************************************************!
     !***************************************************************!
     !***************************************************************!




     !***************************************************************!
     ! time dependent penalty term
     !***************************************************************!
     if (NS_linear_term.and.NS_ipen<0) then
        call Penalty_NS(time,dt, &
             & cou,              &
             & smvu,smvv,smvw,   &
             & slvu,slvv,slvw,   &
             & uin,vin,win,      &
             & penu,penv,penw,   &
             & obj)
     end if
     !***************************************************************!



     !***************************************************************!
     !***************************************************************!
     !***************************************************************!
     ! Momentum conserving scheme
     !***************************************************************!
     !***************************************************************!
     !***************************************************************!
     if (NS_MomentumConserving) then 
        call compute_time(TIMER_START,"Momentum conserving")
        
        if(ntt==1) then
           if (rank==0) then 
              write(*,*)"***************************************************************"
              write(*,*)" Momentum conserving "
              write(*,*)" Update density and compute convective term with consistent scheme"
              if (NS_MomentumConserving_scheme>0) then
                 write(*,*)" with rho update"
              else
                 write(*,*)" with rho and (rho*v) update"
              end if
              write(*,*)"***************************************************************"
           endif
        endif
        !-------------------------------------------------------------------------------
        ! Properties from VOF step n (general)
        !-------------------------------------------------------------------------------
        call thermophysics(rho,rovu0,rovv0,rovw0,&
             & vie,vis,vir,xit,cou,pres,tp,fluids)
        !-------------------------------------------------------------------------------
        call uvw_borders_and_periodicity(mesh,rovu0,rovv0,rovw0)
        call comm_mpi_uvw(rovu0,rovv0,rovw0)
        !-------------------------------------------------------------------------------

        call Momentumconserving(dt,u0,v0,w0,u1,v1,w1,smvu,smvv,smvw, &
!!$        call Momentumconserving(dt,u,v,w,u0,v0,w0,smvu,smvv,smvw, &
             & rovu,rovv,rovw,rovu0,rovv0,rovw0,uadv2,vadv2,wadv2)
        
        call compute_time(TIMER_END,"Momentum conserving")
     end if
     !***************************************************************!
     !***************************************************************!
     !***************************************************************!
     ! End Momentum conserving scheme
     !***************************************************************!
     !***************************************************************!
     !***************************************************************!






     !***************************************************************
     ! VOF
     !***************************************************************
     if (VOF_activate) then

        call print_step("VOF",COLOR_VOF,PRINT_START)

        if (VOF_solve) then 
           call compute_time(TIMER_START,"VOF")

           ! ----------------------------------------------
           ! Choose VOF Velocity 
           ! ----------------------------------------------
           if (NS_MomentumConserving) then
              uvof=uadv2
              vvof=vadv2
              if (dim==3) wvof=wadv2
           else
              uvof=u
              vvof=v
              if (dim==3) wvof=w
           end if
           ! ----------------------------------------------

           
           if (dim==2) then
              select case(VOF_method)
              case(0)
              case(1)
                 call vofplic_2d(cou,uvof,vvof)
                 !call vofplic_2d(mesh,cou,uvof,vvof)
                 !call vofplic_2d(mesh_dual,cou_dual,uvof_dual,vvof_dual)
              case(-39:-30)
                 call ScalarAdvection(nt,time,cou,cou,cou,&
                      & u,v,w,u0,v0,w0,CPart,nPartC,abs(VOF_method))
                 if (modulo(nt,istep)==0) then
                    if (LG_advection) call LG_print(nt,CPart,nPartC)
                 end if
              case default
                 call print_message("VOF_method doesn't exist",MESSAGE_ERROR)
                 stop
              end select
           else
              select case(VOF_method)
              case(0)
              case(1)
                 call vofplic_3d(cou,uvof,vvof,wvof)
              case(2)
                 call vofcons_3d(cou,uvof,vvof,wvof)
              case default
                 call print_message("VOF_method doesn't exist",MESSAGE_ERROR)
                 stop
              end select
           endif
           call sca_borders_and_periodicity(mesh,cou)
           call cou_bounds(cou,uin,vin,win)
           if (modulo(nt,istep)==0) then
              call VOF_print(nt,cou)
           end if
           !--------------------------------------------------------------------
           if (VOF_MD_activate) then
              if (modulo(nt,VOF_MD_istep)==0) then
                 if (.not.allocated(work3)) allocate(work3(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
                 call manifold_death(cou,work3)
              end if
           end if

           call compute_time(TIMER_END,"VOF")
        end if

        call print_step("VOF",COLOR_VOF,PRINT_STOP)

     end if
     !**************************************************************
     
     !***************************************************************!
     !***************************************************************!
     !***************************************************************!
     ! End Eulerian interface tracking and scalar advection
     !***************************************************************!
     !***************************************************************!
     !***************************************************************!







     !***************************************************************!
     !***************************************************************!
     !***************************************************************!
     ! Update of physical properties
     !***************************************************************!
     !***************************************************************!
     !***************************************************************!
     if (NS_activate) then
        call compute_time(TIMER_START,"Physical dyn. properties")
        if (NS_MomentumConserving) then
           !----------------------------------------------------------
           ! * rho is not updated here, rho values come from
           !   momentum conserving
           ! * uadv tabs just cancel rov update
           !----------------------------------------------------------
           if (VOF_activate.or.LS_activate.or.LES_activate) then
              call thermophysics(rho,uadv2,vadv2,wadv2,&
                   & vie,vis,vir,xit,cou,pres,tp,fluids)
           else if (FT_activate) then
              call thermophysics(rho,uadv2,vadv2,wadv2,&
                   & vie,vis,vir,xit,coub,pres,tp,fluids)
           endif
           !----------------------------------------------------------
           ! u^n / dt is directly in NS source term,
           !  coef_time_2=0 cancels the classical contribution
           !----------------------------------------------------------
           coef_time_2=0
           !----------------------------------------------------------
        else
           !----------------------------------------------------------
           ! new properties
           !----------------------------------------------------------
           if (VOF_activate.or.LS_activate.or.LES_activate) then
              call thermophysics(rho,rovu,rovv,rovw,&
                   & vie,vis,vir,xit,cou,pres,tp,fluids)
           else if (FT_activate) then
              call thermophysics(rho,rovu,rovv,rovw,&
                   & vie,vis,vir,xit,coub,pres,tp,fluids)
           endif
           !----------------------------------------------------------
        end if
        call compute_time(TIMER_END,"Physical dyn. properties")
     end if
     !***************************************************************!
     !***************************************************************!
     !***************************************************************!
     ! End Update of physical properties
     !***************************************************************!
     !***************************************************************!
     !***************************************************************!


     !***************************************************************!
     !***************************************************************!
     !***************************************************************!
     ! Turbulence modeling
     !***************************************************************!
     !***************************************************************!
     !***************************************************************!
     if (NS_activate.and.LES_activate) then
        call compute_time(TIMER_START,"LES")
        
        !------------------------------------------------------------------------
        ! Wall function
        !------------------------------------------------------------------------
        if (WF_activate) then
           call LES_Wall_Function(uin,vin,win,u,v,w,vie,rho)
        end if
        !------------------------------------------------------------------------

        !------------------------------------------------------------------------
        ! Sub mesh viscosity
        !------------------------------------------------------------------------
        call LES_models(u,v,w,vie,vis,vir,mu_sgs,rho)
        !------------------------------------------------------------------------

        call compute_time(TIMER_END,"LES")
     endif
     !***************************************************************!
     !***************************************************************!
     !***************************************************************!
     ! End Turbulence modeling
     !***************************************************************!
     !***************************************************************!
     !***************************************************************!


#if RESPECT
     if (vl%force_inter) call Particle_Collisions(vl,smvuin,smvvin,smvwin,smvu,smvv,smvw,dt) 
#endif

     !***************************************************************!
     !***************************************************************!
     !***************************************************************!
     ! Energy Equation
     !***************************************************************!
     !***************************************************************!
     !***************************************************************!
     if (EN_activate.and.EN_solve) then
        call compute_time(TIMER_START,"Energy eq.")

        !*****************************************************************!
        ! update physical properties
        !*****************************************************************!
        call Energy_Thermophysics(rho,condu,condv,condw,cp,cou,beta,tpb,fluids)
        !*****************************************************************!

        !------------------------------------------------------------------------
        ! Sub mesh conductivity
        !------------------------------------------------------------------------
        if (LES_activate.and.SDT_activate) then
           call SDT_models(condu,condv,condw,cond_sgs,rho,cp,vie,mu_sgs)
        end if
        !------------------------------------------------------------------------


        !*****************************************************************!
        ! time dependent penalty term
        !*****************************************************************!
        if (EN_linear_term.or.EN_penalty_term) then
           if (EN_ipen<0) then
              call Penalty_Sca(time,dt,  &
                   & tp,cou,             &
                   & smtp,sltp,sptp,     &
                   & rovu,rovv,rovw,cp,  &
                   & condu,condv,condw,  &
                   & slvu,slvv,slvw,     &
                   & u,v,w)
           end if
        end if
        !*****************************************************************!


        
        !***************************************************************!
        ! Explicit Advection
        !***************************************************************!
        if (EN_inertial_scheme<0) then
           !-------------------------------------------------------------------------------
           ! EN_inertial_scheme < 0
           !                     -1  Weno5n (non Conservative)
           !                     -2  Weno5c (Conservative)
           !                     -3  Max-Wendroff TVD
           !                     -19:-10 Lagragian scheme
           !                     -29:-20 PIC
           !-------------------------------------------------------------------------------
           call sca_borders_and_periodicity(mesh,tp)
           call sca_borders_and_periodicity(mesh,tp0)

           call ScalarAdvection(nt,time,tp,tp0,tp1,&
                & u,v,w,u0,v0,w0,SPart,nPartS,abs(EN_inertial_scheme))
           call EN_time(tp,tp0,tp1,nt)

           if (modulo(nt,istep)==0) then
              if (LG_advection) call LG_print(nt,SPart,nPartS)
           end if
           !-------------------------------------------------------------------------------
        end if
        !***************************************************************!
        
        !***************************************************************!
        ! Phase Field
        !***************************************************************!
        if (PF_activate) call PF_Delta(phase_field, smtp, tp, tp0, curvature, norm_grad, u, v)
        !***************************************************************!


        !***************************************************************!
        ! Energy Equation
        !***************************************************************!
        call Energy_Delta(u,v,w,tp,rho,cou,sltp,smtp,sptp,      &
             & tpin,tp0,tp1,condu,condv,condw,cp,               &
             & pentp,penu,penv,penw,energy,                     &
             & EN_source_term,EN_linear_term,EN_penalty_term,   &
             & EN_inertial_scheme,                              &
             & EN_solver_type,EN_solver_threshold,EN_solver_it, &
             & EN_solver_precond,impp_energy)
        !***************************************************************!

        !***************************************************************!
        ! outputs
        !***************************************************************!
        if (modulo(nt,istep)==0) then
           call EN_print(nt,energy)
           call EN_res(dt,tp,tp0)
        end if
        !***************************************************************!

        !-------------------------------------------------------------------------------
        ! Markers update
        !-------------------------------------------------------------------------------
        select case (EN_inertial_scheme)
        case(-19:-10,-29:-20)
           call ScalarAdvection(nt,time,tp,tp0,tp1,&
                & u,v,w,u0,v0,w0,SPart,nPartS,abs(EN_inertial_scheme))
        end select
        !-------------------------------------------------------------------------------
        
        call compute_time(TIMER_END,"Energy eq.")
     end if
     !***************************************************************!
     !***************************************************************!
     !***************************************************************!
     ! End Energy Equation
     !***************************************************************!
     !***************************************************************!
     !***************************************************************!

     
     
     !***************************************************************!
     !***************************************************************!
     !***************************************************************!
     ! ADE Equation
     !***************************************************************!
     !***************************************************************!
     !***************************************************************!
     
     call ADE_solve(nt,mesh,                       &
          & u,v,w,conc,conc0,conc1,                &
          & rho,ade_diffu,ade_diffv,ade_diffw,cou, &
          & conc_pen,conc_in,ade_src,ade_lnr,      &
          & coef_time_1,coef_time_2,coef_time_3,   &
          & ade_eq,ade_resol)

     !***************************************************************!
     !***************************************************************!
     !***************************************************************!
     ! End ADE Equation
     !***************************************************************!
     !***************************************************************!
     !***************************************************************!
     
     
     

     !***************************************************************!
     !***************************************************************!
     ! Time dependent BC
     !***************************************************************!
     !***************************************************************!
     if (NS_TimeBC) then
        call Time_BC_Navier_Stokes(time,uin,vin,win,stdy_uin,stdy_vin,stdy_win,ns)
     end if
     !***************************************************************!
     !***************************************************************!
     ! End Time dependent BC
     !***************************************************************!
     !***************************************************************!





     !***************************************************************!
     !***************************************************************!
     !***************************************************************!
     ! Navier-Stokes/Stokes/Euler
     !***************************************************************!
     !***************************************************************!
     !***************************************************************!

     if (NS_activate) then

        call print_step("NAVIER-STOKES",COLOR_NAVIER_STOKES,PRINT_START)
        
        call compute_time(TIMER_START,"Navier-Stokes eqs.")

        !-------------------------------------------------------------------------------
        ! N. Périnet, D. Juric and L. S. Tuckerman
        ! Numerical simulation of Faraday waves
        ! JFM 2009
        !-------------------------------------------------------------------------------
        if (NS_source_term) then 
           if (VOF_activate) then
              if (VOF_init_icase==8) then
                 tmp=30
                 if (dim==2) then 
                    smvv=rovv*(tmp*cos(12*2*pi*time)-9.81d0)
                 else
                    smvw=rovw*(tmp*cos(12*2*pi*time)-9.81d0)
                 end if
              end if
           end if
        end if
        !-------------------------------------------------------------------------------

        !***************************************************************!
        ! Explicit Advection
        !***************************************************************!
        if (NS_inertial_scheme<0) then
           !-------------------------------------------------------------------------------
           ! NS_inertial_scheme < 0
           !                     -10 Lagragian scheme
           !-------------------------------------------------------------------------------
           call uvw_borders_and_periodicity(mesh,u,v,w)
           call VelocityAdvection(nt,time,u,v,w,u0,v0,w0, &
                & VPart,nPartV,abs(NS_inertial_scheme))
           call uvw_borders_and_periodicity(mesh,u,v,w)
           if (modulo(nt,istep)==0) then
              if (LG_advection) call LG_print(nt,VPart,nPartV)
           end if
           u0=u
           v0=v
           !-------------------------------------------------------------------------------
        end if
        !***************************************************************!

        if (NS_solve) then 
           if ( FT_2_phases .and. nb_phase==2 ) then
              write(*,*) "Navier-Stokes 2 phase has to be implemented in this version"
           else
              if (FT_activate) then
                 call Navier_Stokes_Delta (pres,pres0,u,v,w, &
                      & div,rho,coub,tp,                     &
                      & slvu,slvv,slvw,smvu,smvv,smvw,       &
                      & uin,vin,win,bip,pin,                 &
                      & u0,u1,v0,v1,w0,w1,                   &
                      & grdpu,grdpv,grdpw,dpres,             &
                      & vie,vis,vir,xit,                     &
                      & rovu,rovv,rovw,                      &
                      & beta,tpb,                            &
                      & penu,penv,penw,penp,                 &
                      & ns,phi,                              &
                      & mean_curvu,mean_curvv)
              else
                 call Navier_Stokes_Delta (pres,pres0,u,v,w, &
                      & div,rho,cou,tp,                      &
                      & slvu,slvv,slvw,smvu,smvv,smvw,       &
                      & uin,vin,win,bip,pin,                 &
                      & u0,u1,v0,v1,w0,w1,                   &
                      & grdpu,grdpv,grdpw,dpres,             &
                      & vie,vis,vir,xit,                     &
                      & rovu,rovv,rovw,                      &
                      & beta,tpb,                            &
                      & penu,penv,penw,penp,                 &
                      & ns,phi,                              &
                      & mean_curvu,mean_curvv)
              end if
           end if

        end if

        !***************************************************************!
        ! Explicit Advection
        !***************************************************************!
        if (NS_inertial_scheme<0) then
           !-------------------------------------------------------------------------------
           ! EN_inertial_scheme < 0
           !                     -10 Lagragian scheme
           !                     -11 Lagragian scheme with cut off
           !-------------------------------------------------------------------------------
           call VelocityAdvection(nt,time,u,v,w,u0,v0,w0, &
                & VPart,nPartV,abs(NS_inertial_scheme))
           !-------------------------------------------------------------------------------
        end if
        !***************************************************************!

        if (NS_solve) then
           if (modulo(nt,istep)==0) then
              !call NS_print(nt,ns,phi)
              call NS_print2(nt,mesh,u,v,w,pres,cou)
              !call NS_res(dt,pres,u,v,w,pres0,u0,v0,w0)
           end if
        end if

        call compute_time(TIMER_END,"Navier-Stokes eqs.")
        call print_step("NAVIER-STOKES",COLOR_NAVIER_STOKES,PRINT_STOP)

     endif


     !***************************************************************!
     !***************************************************************!
     !***************************************************************!
     ! End Navier-Stokes/Stokes/Euler
     !***************************************************************!
     !***************************************************************!
     !***************************************************************!
#if RESPECT
      call ComputationTime_StartTimer(1)
      call RESPECT_Resolutions(vl,u,v,w,Ru0,Rv0,Rw0,pres,rho,rovu,rovv,&
                               rovw,vie,vir,vis,cou,cou_vis,cou_vts,   &
                               couin0,cou_visin,cou_vtsin,Rwork,Rwork1,&
                               Rwork2,Rwork3,dt,nt)
      call ComputationTime_StopTimer(1)
      Ru0=u
      Rv0=v
      if(dim==3) Rw0=w
#endif

     !***************************************************************!
     !***************************************************************!
     !***************************************************************!
     ! Outputs
     ! from unit 20
     !***************************************************************!
     !***************************************************************!
     !***************************************************************!
     call compute_time(TIMER_START,"Outputs")
     
     !---------------------------------------------------------------
     if (NS_activate) then
        if (iplot>0) then 
           if (modulo(nt,iplot)==0) then
              call outputs_general(20,time,u,v,w,rho)
#if FFTW3
              if (VOF_init_icase==9) then
                 !call compute_Ek_spectrum(u,v,w,nt)
                 !call compute_long_trans_autocor(u,v,nt)
              end if
#endif
           end if
        end if
     end if
     !---------------------------------------------------------------
     if (EN_activate) then
        if (iplot>0) then 
           if (modulo(nt,iplot)==0) then
              call outputs_general_energy(21,time,u,v,w,rho,cp,tp,sltp)
           end if
        end if
     end if
     !---------------------------------------------------------------
     if (iplot>0) then 
        if (modulo(nt,iplot)==0) then
           call outputs_cavity(60,time,u,v,w,pres,tp)
        end if
     end if
     !---------------------------------------------------------------




     !---------------------------------------------------------------
     ! Probes
     !---------------------------------------------------------------
     if (iprobe>0) then
        if (modulo(nt,iprobe)==0) then
           !---------------------------------------------------------------
           ! temperature probing
           !---------------------------------------------------------------
           if (EN_activate) call outputs_interp_probes(1,time,tp,u,v,w, &
                & nProbe_sca,xyzProbe_sca(1:nProbe_sca,:))
           !---------------------------------------------------------------
           ! velocity and pressure probing
           !---------------------------------------------------------------
           if (NS_activate) then
              call outputs_interp_probes(2,time,tp,u,v,w, &
                   & nProbe_uvw,xyzProbe_uvw(1:nProbe_uvw,:))
              call outputs_interp_probes(3,time,pres,u,v,w, &
                   & nProbe_p,xyzProbe_p(1:nProbe_p,:))
           end if
           !---------------------------------------------------------------
        end if
     end if
     !---------------------------------------------------------------
     ! end Probes
     !---------------------------------------------------------------



     !---------------------------------------------------------------
     ! Force computation on obstacles (adapt for Cd, Cl ...)
     !---------------------------------------------------------------
     if (NS_activate.and.NS_linear_term) then
        if (iplot>0) then 
           if (modulo(nt,iplot)==0) then
              call outputs_force_obstacle(80,time,u,v,w,slvu,slvv,slvw)
           end if
        end if
     end if
     !---------------------------------------------------------------
     ! End force computation on obstacles
     !---------------------------------------------------------------


     !---------------------------------------------------
     ! Output cylinder moving in fluid at rest
     !---------------------------------------------------
     if (NS_linear_term.and.NS_ipen==-1) then
        if (modulo(time,5d0)>=5d0/2.and.cwrite==1) then 
           call outputs_moving_cylinder(160,time,u,v,w,pres) ! 4 units
           cwrite=2
        elseif (modulo(time,5d0)>=7*5d0/12.and.cwrite==2) then
           call outputs_moving_cylinder(170,time,u,v,w,pres) ! 4 units
           cwrite=3
        elseif (modulo(time,5d0)>=11*5d0/12.and.cwrite==3) then
           call outputs_moving_cylinder(180,time,u,v,w,pres) ! 4 units
           cwrite=4
        elseif (modulo(time,5d0)-modulo(time-dt,5d0)<0.and.cwrite==4) then
           cwrite=1
        end if
     end if
     !---------------------------------------------------

     !---------------------------------------------------
     ! Output VOF 
     !---------------------------------------------------
     if (VOF_activate) then
        if (iplot>0) then 
           if (modulo(nt,iplot)==0) then
              call outputs_inv_phase(90,time,u,v,w,cou,rho)
              call outputs_drop_impact(91,time,u,v,w,cou,rho)
           end if
        end if
     end if
     !---------------------------------------------------


     !---------------------------------------------------
     ! Turbulence outputs
     !---------------------------------------------------
     if (LES_activate) then 
        call prepa_outputs_turbulence(cStats,u,v,w,tp,rho,mu_sgs, &
             & work1,work2,work3,work4,work5,work6,um,vm,wm)
     end if
     
     if (EN_activate) then
        call prepa_outputs_mean_age_air(time,dt,tp,tp0,work7)
     end if
     !---------------------------------------------------


#if INRS
     !---------------------------------------------------
     ! INRS AEROCAB  v2
     !---------------------------------------------------


     int_cpluss_injection  = 0
     int_cmoins_injection  = 0
     int_diff_injection    = 0
     int_cpluss_extraction = 0
     int_cmoins_extraction = 0
     int_diff_extraction   = 0
     
     do j=sy,ey
        do i=sx,ex

           x=grid_x(i)
           y=grid_y(j)

           z=1d0
           xyz=(/x,y,z/)
           if (   x>=grid_x(sx).and.x<grid_x(ex+1).and. &
                & y>=grid_y(sy).and.y<grid_y(ey+1).and. &
                & z>=grid_z(sz).and.z<grid_z(ez+1) ) then
              if (x>x_min_injection .and. x<x_max_injection) then
                 if (y>y_min_injection .and. y<y_max_injection) then
                    var1=0;vp1=0
                    call interpolation_phi(time,tp,(/x,y,z/),var1,1,dim)
                    call interpolation_uvw(time,u,v,w,(/x,y,z/),vp1,1,dim)
                    !if (abs(var1)<1d-12) var1=0
                    if (vp1(3)>0) then
                       int_cpluss_injection=int_cpluss_injection+var1*vp1(3)*dx(i)*dy(j)
                    else
                       int_cmoins_injection=int_cmoins_injection+var1*vp1(3)*dx(i)*dy(j)
                    end if

                    ijk=0
                    call search_cell_ijk(mesh,xyz,ijk,3)
                    int_diff_injection=int_diff_injection &
                         & - fluids(1)%lambda*(tp(ijk(1),ijk(2),ijk(3))-tp(ijk(1),ijk(2),ijk(3)-1))/(grid_z(ijk(3))-grid_z(ijk(3)-1))*dx(i)*dy(j)
                 end if
              end if
           end if
           
           z=zmax
           xyz=(/x,y,z/)
           if (   x>=grid_x(sx).and.x<grid_x(ex+1).and. &
                & y>=grid_y(sy).and.y<grid_y(ey+1).and. &
                & z>=grid_z(sz).and.z<grid_z(ez+1) ) then
              if ((x-x0_sorbonne)**2-(y-(ymax-0.2d0))**2<0.1d0**2) then
                 var1=0;vp1=0
                 call interpolation_phi(time,tp,(/x,y,z/),var1,1,dim)
                 call interpolation_uvw(time,u,v,w,(/x,y,z/),vp1,1,dim)
                 !if (abs(var1)<1d-12) var1=0
                 if (vp1(3)>0) then
                    int_cpluss_extraction=int_cpluss_extraction+var1*vp1(3)*dx(i)*dy(j)
                    !write(*,*) var1,vp1(3)
                 else
                    int_cmoins_extraction=int_cmoins_extraction+var1*vp1(3)*dx(i)*dy(j)
                 end if
                 
                 ijk=0
                 call search_cell_ijk(mesh,xyz,ijk,3)
                 int_diff_extraction=int_diff_extraction &
                      & - fluids(1)%lambda*(tp(ijk(1),ijk(2),ijk(3))-tp(ijk(1),ijk(2),ijk(3)-1))/(grid_z(ijk(3))-grid_z(ijk(3)-1))*dx(i)*dy(j)
              end if
           end if

        end do
     end do
     
     int_cpluss_frontale = 0
     int_cmoins_frontale = 0
     int_diff_frontale   = 0
     
     do k=sz,ez 
        do i=sx,ex
           
           x=grid_x(i)
           z=grid_z(k)
           
           y=ymax-0.8d0
           xyz=(/x,y,z/)
           if (   x>=grid_x(sx).and.x<grid_x(ex+1).and. &
                & y>=grid_y(sy).and.y<grid_y(ey+1).and. &
                & z>=grid_z(sz).and.z<grid_z(ez+1) ) then
              
              if (x>x_min_injection .and. x<x_max_injection) then
                 if (z>1 .and. z<1.4d0) then
                    var1=0;vp1=0
                    
                    ijk=0
                    call search_cell_ijk(mesh,xyz,ijk,2)

                    !call interpolation_phi(time,tp,(/x,y,z/),var1,1,dim)
                   ! call interpolation_uvw(time,u,v,w,(/x,y,z/),vp1,1,dim)
                    
                    !if (abs(var1)<1d-12) var1=0
                    var2 = v(ijk(1),ijk(2),ijk(3))
                    if (var2>0) then
                       var1 = tp(ijk(1),ijk(2)-1,ijk(3))
                       !var2 = v (ijk(1),ijk(2)-1,ijk(3))
                       int_cpluss_frontale = int_cpluss_frontale + var1*var2*dx(i)*dz(k)
                    else
                       var1 = tp(ijk(1),ijk(2),ijk(3))
                       !var2 = v (ijk(1),ijk(2)+1,ijk(3))
                       int_cmoins_frontale = int_cmoins_frontale + var1*var2*dx(i)*dz(k)
                    end if

                    int_diff_frontale=int_diff_frontale &
                         & - fluids(1)%lambda*(tp(ijk(1),ijk(2),ijk(3))-tp(ijk(1),ijk(2)-1,ijk(3)))/(grid_y(ijk(2))-grid_y(ijk(2)-1))*dx(i)*dz(k)
                 end if
              end if
           end if

        end do
     end do

     call mpi_allreduce(int_cpluss_injection,var1,1,mpi_double_precision,mpi_sum,comm3d,mpi_code)  ; int_cpluss_injection  = var1
     call mpi_allreduce(int_cmoins_injection,var1,1,mpi_double_precision,mpi_sum,comm3d,mpi_code)  ; int_cmoins_injection  = var1

     call mpi_allreduce(int_cpluss_extraction,var1,1,mpi_double_precision,mpi_sum,comm3d,mpi_code) ; int_cpluss_extraction = var1
     call mpi_allreduce(int_cmoins_extraction,var1,1,mpi_double_precision,mpi_sum,comm3d,mpi_code) ; int_cmoins_extraction = var1

     call mpi_allreduce(int_cpluss_frontale,var1,1,mpi_double_precision,mpi_sum,comm3d,mpi_code)   ; int_cpluss_frontale   = var1
     call mpi_allreduce(int_cmoins_frontale,var1,1,mpi_double_precision,mpi_sum,comm3d,mpi_code)   ; int_cmoins_frontale   = var1

     call mpi_allreduce(int_diff_injection,var1,1,mpi_double_precision,mpi_sum,comm3d,mpi_code)    ; int_diff_injection    = var1
     call mpi_allreduce(int_diff_extraction,var1,1,mpi_double_precision,mpi_sum,comm3d,mpi_code)   ; int_diff_extraction   = var1
     call mpi_allreduce(int_diff_frontale,var1,1,mpi_double_precision,mpi_sum,comm3d,mpi_code)     ; int_diff_frontale     = var1

     if (rank==0) then
        write(*,'(a40,999(1x,999(1x,1pe12.5)))') "[INRS] Injection,  uC+,uC-,q_diff:", int_cpluss_injection,  int_cmoins_injection,  int_diff_injection
        write(*,'(a40,999(1x,999(1x,1pe12.5)))') "[INRS] Extraction, uC+,uC-,q_diff:", int_cpluss_extraction, int_cmoins_extraction, int_diff_extraction
        write(*,'(a40,999(1x,999(1x,1pe12.5)))') "[INRS] Frontale,   uC+,uC-,q_diff:", int_cpluss_frontale,   int_cmoins_frontale,   int_diff_frontale
     end if
     !---------------------------------------------------
#endif 

     
     !---------------------------------------------------
     ! Output diffusion 2D
     !---------------------------------------------------
     if (EN_activate) then
        if (VOF_activate) then
           if (VOF_init_case%id_output==100) then 
              if (iplot>0) then 
                 if (modulo(nt,iplot)==0) then
                    
                    var1=0
                    var0=0
                    do i=sx,ex
                       !---------------------------------------------------
                       ! analytical steady solution
                       !---------------------------------------------------
                       var=(1d0-0d0)/( (VOF_init_case%thick-xmin)/fluids(2)%lambda/(ymax-ymin) &
                            & +(xmax-VOF_init_case%thick)/fluids(1)%lambda/(ymax-ymin) )
                       !---------------------------------------------------
                       ! thermal resistance method
                       !---------------------------------------------------
                       if (grid_x(i)<VOF_init_case%thick) then
                          tmp=1-var*(grid_x(i)-xmin)/fluids(2)%lambda/(ymax-ymin)
                       else
                          tmp=1-var*(VOF_init_case%thick-xmin)/fluids(2)%lambda/(ymax-ymin) &
                               & - var*(grid_x(i)-VOF_init_case%thick)/fluids(1)%lambda/(ymax-ymin)
                       end if
                       !---------------------------------------------------
                       ! norm
                       !---------------------------------------------------
                       var0=var0+(tp(i,ny/2,1)-tmp)**2*dx(i)*dy(j)*CoeffV(i,j,1)
                       var1=var1+(tmp)**2*dx(i)*dy(j)*CoeffV(i,j,1)
                       !---------------------------------------------------
                       write(401,*) grid_x(i),tp(i,ny/2,1),tmp, -0.5d0*(condu(i+1,ny/2,1)+condu(i,ny/2,1))*(tp(i+1,ny/2,1)-tp(i-1,ny/2,1))/(grid_x(i+1)-grid_x(i-1))*(ymax-ymin),var
                    end do
                    write(401,*)
                    write(401,*)

                    write(402,*) nx,sqrt(var0/var1)
                 end if
              end if
           end if
        end if
     end if
     !---------------------------------------------------

     
     call compute_time(TIMER_END,"Outputs")

     !***************************************************************!
     !***************************************************************!
     !***************************************************************!
     ! End Outputs                                                     
     !***************************************************************!
     !***************************************************************!
     !***************************************************************!

     
     !***************************************************************!
     !***************************************************************!
     !***************************************************************!
     !   Generation of visualization files                           
     !***************************************************************!
     !***************************************************************!
     !***************************************************************!

     call compute_time(TIMER_START,"Visualization files")

     !---------------------------------------------------------------
     ! int tp dS / int ds
     !---------------------------------------------------------------
     if (EN_activate) then
        call InitCoeffVS
        if (iplot>0) then 
           if (modulo(nt,iplot)==0) then
              !---------------------------------------------------------------
              ! if immersed objet
              !---------------------------------------------------------------
              if (allocated(slvu)) then
                 var1=0
                 var2=0
                 var3=0
                 var4=0
                 do k=sz,ez
                    do j=sy,ey
                       do i=sx,ex
                          !---------------------------------------------------------------
                          ! conditions
                          !---------------------------------------------------------------
                          yes=.true.
                          if (dim==3) then
                             if (slvw(i,j,k)/=0.and.slvw(i,j,k+1)/=0) yes=.false.
                          end if
                          if (slvu(i,j,k)/=0.and.slvu(i+1,j,k)/=0) yes=.false.
                          if (slvv(i,j,k)/=0.and.slvv(i,j+1,k)/=0) yes=.false.
                          !---------------------------------------------------------------
                          ! compute int in and out
                          !---------------------------------------------------------------
                          if (yes) then
                             var1=var1+tp(i,j,k)*dx(i)*dy(j)*dz(k)*coeffV(i,j,k)
                             var2=var2+dx(i)*dy(j)*dz(k)*coeffV(i,j,k)
                          else
                             var3=var3+tp(i,j,k)*dx(i)*dy(j)*dz(k)*coeffV(i,j,k)
                             var4=var4+dx(i)*dy(j)*dz(k)*coeffV(i,j,k)
                          end if
                          !---------------------------------------------------------------
                       end do
                    end do
                 end do
              end if
           end if
           !---------------------------------------------------------------
           ! full fluid domain
           !---------------------------------------------------------------
           var7=0
           var8=0
           do k=sz,ez 
              do j=sy,ey
                 do i=sx,ex
                    var7=var7+tp(i,j,k)*dx(i)*dy(j)*dz(k)*coeffV(i,j,k)
                    var8=var8+dx(i)*dy(j)*dz(k)*coeffV(i,j,k)
                 end do
              end do
           end do
           !---------------------------------------------------------------
           ! MPI
           !---------------------------------------------------------------
           call  mpi_allreduce(var1,var0,1,mpi_double_precision,mpi_sum,comm3d,code); var1=var0
           call  mpi_allreduce(var2,var0,1,mpi_double_precision,mpi_sum,comm3d,code); var2=var0
           call  mpi_allreduce(var3,var0,1,mpi_double_precision,mpi_sum,comm3d,code); var3=var0
           call  mpi_allreduce(var4,var0,1,mpi_double_precision,mpi_sum,comm3d,code); var4=var0
           call  mpi_allreduce(var7,var0,1,mpi_double_precision,mpi_sum,comm3d,code); var7=var0
           call  mpi_allreduce(var8,var0,1,mpi_double_precision,mpi_sum,comm3d,code); var8=var0
           call  mpi_allreduce(maxval(tp),var5,1,mpi_double_precision,mpi_max,comm3d,code)
           call  mpi_allreduce(minval(tp),var6,1,mpi_double_precision,mpi_max,comm3d,code)
           !---------------------------------------------------------------
           ! write files
           !---------------------------------------------------------------
           if (allocated(slvu)) then 
              write(71,'(999(1x,1pe12.5))') time,var1,var2,var3,var4,var5,var6
              flush(71)
           end if
           write(72,'(999(1x,1pe12.5))') time,var7,var8,var5,var6
           flush(72)
           !---------------------------------------------------------------
        end if
     end if
     !---------------------------------------------------------------


     if (NS_init_vel_icase==5) then
        !---------------------------------------------------------------
        ! Riemann pb solution u1<u2, u2>u3
        !     u1=0, x in [:,0]   var1
        !     u2=1, x in [0,1]   var2
        !     u3=0, x in [1,:]   var3 
        !                        in 2015 lecture pp. 50
        !---------------------------------------------------------------
        if (.not.allocated(work1)) allocate(work1(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
        if (.not.allocated(work3)) allocate(work3(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
        !---------------------------------------------------------------
        var1=0
        var2=1
        var3=0
        var0=2d0/(var2-var3)                             ! t*
        var4=d1p2*(var2+var3)*time+1                     ! \xi t<t*
        var5=sqrt(2*(var2-var3))*sqrt(time)+var3*time    ! \xi t>t*
        !---------------------------------------------------------------
        if (time<=var0) then 
           do i=sx-1,ex+1
              if (grid_x(i)<=var1*time) then
                 work3(i,:,:)=var1
              else if (grid_x(i)<=var2*time) then
                 work3(i,:,:)=grid_x(i)/time
              else if (grid_x(i)<=var4) then
                 work3(i,:,:)=var2
              else
                 work3(i,:,:)=var3
              end if
           end do
        else
           do i=sx-1,ex+1
              if (grid_x(i)<=var1*time) then
                 work3(i,:,:)=var1
              else if (grid_x(i)<=var5) then
                 work3(i,:,:)=grid_x(i)/time
              else 
                 work3(i,:,:)=var3
              end if
           end do
        end if

        do i=sx,ex
           work1(i,1,1)=sum((u(i,sy:ey,1)+u(i+1,sy:ey,1))/2)/(ey-sy+1)
           write(91,*) grid_x(i),  &
                & work1(i,1,1),    &
                & work3(i,1,1),    &
                & abs(work1(i,1,1)-work3(i,1,1))/maxval(abs(work3(sx:ex,1,1)))
        end do
        write(91,*) 
        write(91,*)

        write(92,*) time,  &
             & sqrt( sum( (work1(sx:ex,1,1)-work3(sx:ex,1,1))**2 ) / sum( (work3(sx:ex,1,1))**2 ) ), &
             & maxval(abs(work1(sx:ex,1,1)-work3(sx:ex,1,1)))/maxval(abs(work3(sx:ex,1,1)))
        deallocate(work1) 
     end if
     
     if (EN_activate) then
        if (EN_init_icase==14) then
           do i=sx-3,ex+3
              write(501,*)  grid_x(i),tp(i,ny/2,1)
           end do
           write(501,*)
           write(501,*)
        end if
     end if


     !---------------------------------------------------------------
     ! solution advection spot
     !---------------------------------------------------------------
     if (EN_activate) then
        if (NS_init_vel_icase==2) then 
           if (EN_init_icase==2.or.EN_init_icase==3) then
              call solution_reference_tache(time,work1,EN_init_icase)
           end if
        end if
     end if
     !---------------------------------------------------------------

     !---------------------------------------------------------------
     ! write fields
     !---------------------------------------------------------------
     if (nPart>0) then
!!$        call InitCoeffVS
        call compute_volume_fraction(mesh,work2,LPart,nPart,0)
     else if (nPartS>0) then
        call compute_volume_fraction(mesh,work2,SPart,nPartS,0)
     else if (nPartV>0) then
        call compute_volume_fraction(mesh,work2,VPart,nPartV,0)
     end if
     !---------------------------------------------------------------

     
     !call ComputeScaAverages2(0,mesh,CPart,nPartC,cou)

     call Write_fields(nt,time,mesh,u,v,w,pres,div,tp,cou, &
          & obj,conc,work1,work2,work3,work4,work5,work6,  &
          & Rwork,Rwork1,Rwork2,Rwork3)

     if (mesh_dual%active) then
        call allocate_array_3(ALLOCATE_ON_P_MESH,work7,mesh_dual)
        call ComputeScaAverages2(0,mesh_dual,CPart,nPartC,work7)
        call Write_fields(nt,time,mesh_dual,work1,work2,work3,work7)
     end if
     
     !---------------------------------------------------------------
     ! write particles 
     !---------------------------------------------------------------
!!$     PartAreMarkers = .true.
!!$     call Lagrangian_Particle_Tracking(u,v,w,u0,v0,w0, &
!!$          & LPart,nPart,nt,dim)
     call Write_Particles(nt,time, &
          & LPart,nPart,           &
          & SPart,nPartS,          &
          & VPart,nPartV,          &
          & CPart,nPartC )
     !---------------------------------------------------------------

     call compute_time(TIMER_END,"Visualization files")
     
     !***************************************************************!
     !***************************************************************!
     !***************************************************************!
     !   End Generation of vizualization files                           
     !***************************************************************!
     !***************************************************************!
     !***************************************************************!


     select case(EN_inertial_scheme)
     case(-19:-10,0,-29:-20)
        if ((EN_init_icase==2.or.EN_init_icase==3) &
             & .and.(NS_init_vel_icase==2)) then 
           if (.not.allocated(cou)) allocate(cou(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
           !----------------------------------------------------------
           ! error from diffusive solution
           !----------------------------------------------------------
           call solution_reference_tache(time,cou,EN_init_icase)
           work1=tp-cou
           !----------------------------------------------------------
           var=0
           var1=0
           var2=0
           var3=0
           var4=0
           var5=0
           do j=sy,ey
              do i=sx,ex
                 !----------------------------------------------------------
                 ! inf norme
                 !----------------------------------------------------------
                 if (abs(work1(i,j,1))>var) then
                    var=abs(work1(i,j,1))
                 end if
                 !----------------------------------------------------------
                 ! norme 1
                 !----------------------------------------------------------
                 var1=var1+abs(work1(i,j,1))*dx(i)*dy(j)
                 var2=var2+abs(cou(i,j,1))*dx(i)*dy(j)
                 !----------------------------------------------------------
                 ! norme 2
                 !----------------------------------------------------------
                 var3=var3+(work1(i,j,1)*work1(i,j,1))*dx(i)*dy(j)
                 var4=var4+(cou(i,j,1)*cou(i,j,1))*dx(i)*dy(j)
                 !----------------------------------------------------------
                 ! int tp
                 !----------------------------------------------------------
                 var5=var5+tp(i,j,1)*dx(i)*dy(j)
                 !----------------------------------------------------------
              end do
           end do
           !----------------------------------------------------------
           write(201,'(i4,1x,999(1pe12.5,1x))') nx,time, &
                & var/maxval(abs(cou)),                  &
                & var1/var2,                             &
                & sqrt(var3/var4),                       &
                & var5
           !----------------------------------------------------------
           deallocate(cou)
        end if
     end select
     


     
     !***************************************************************!
     !***************************************************************!
     !***************************************************************!
     ! Generation of restart file                                  
     !***************************************************************!
     !***************************************************************!
     !***************************************************************!
     if ( (irestart>0 .and. modulo(ntt,irestart)==0) .or. ntt==nstep) then
        
        call print_step("RESTART FILES",COLOR_RESTART,PRINT_START)
        l=nt
        !----------------------------------------------------------------
        ! For Navier-Stokes/Stokes/Euler
        !----------------------------------------------------------------
        if (NS_activate.or.EN_activate) then
          !**** RESPECT
          call read_write_EQ_mpi(l,time,cStats, &
                & cfl%dt_new,cfl%dt_old,         &
                & u,v,w,u0,v0,w0,pres,           &
                & tp,tp0,cou,dist,               &
                   & work4,work5,work6,c_write,vl)
          call print_step("RESTART FILES",COLOR_RESTART,PRINT_STOP)
          !**** RESPECT
        end if
        !----------------------------------------------------------------
        ! For Lagrangian particle tracking
        !----------------------------------------------------------------
        !----------------------------------------------------------------
        ! For front tracking
        !----------------------------------------------------------------
     end if
     !***************************************************************!
     !***************************************************************!
     !***************************************************************!
     ! End Generation of restart file                                  
     !***************************************************************!
     !***************************************************************!
     !***************************************************************!
     
     !------------------------------------------------------------------
     ! test is nan
     !------------------------------------------------------------------
     call compute_time(TIMER_START,"NaN testing")
     
     i=0
     if (is_nan_array(u)) i=1
     if (is_nan_array(v)) i=1
     if (dim==3) then
        if (is_nan_array(w)) i=1
     end if

     if (EN_activate) then 
        if (EN_solve) then
           if (is_nan_array(tp)) i=1
        end if
     end if
     
     CALL MPI_ALLREDUCE(i,j,1,MPI_INTEGER,MPI_SUM,COMM3D,MPI_CODE)
     if (j>0) exit_loop=.true.
     !------------------------------------------------------------------
     if (exit_loop) then
        if (NS_activate) then
           if (NS_solve) then
              call NS_print(nt,ns,phi)
              call NS_res(dt,pres,u,v,w,pres0,u0,v0,w0)
           end if
        end if
        if (EN_activate) then
           if (EN_solve) then
              call EN_print(nt,energy)
              call EN_res(dt,tp,tp0)
           end if
        end if

        u=u0
        v=v0
        pres=pres0
        if (dim==3) w=w0
        if (Euler==2) then
           u0=u1
           v0=v1
           if (dim==3) w0=w1
        end if

        if (EN_activate) then
           if (EN_solve) then
              tp=tp0
              if (Euler==2) tp0=tp1
           end if
        end if

        call print_message("NaN is found",MESSAGE_FATAL_ERROR)
        call print_message("Writing last iteration summary ...")
        call print_message("Exit computation with n-1 field values")

        if (NS_activate) then 
           if (NS_solve) then
              call NS_print(nt,ns,phi)
              call NS_res(dt,pres,u,v,w,pres0,u0,v0,w0)
           end if
        end if
        if (EN_activate) then 
           if (EN_solve) then
              call EN_print(nt,energy)
              call EN_res(dt,tp,tp0)
           end if
        end if
        exit
     end if
     
     call compute_time(TIMER_END,"NaN testing")
     !------------------------------------------------------------------




     !***********************************************************!
     !***********************************************************!
     !***********************************************************!
     !   Time update of variables                  
     !***********************************************************!
     !***********************************************************!
     !***********************************************************!
     call EN_time(tp,tp0,tp1,nt)
     call EQ_time(u,v,w,u0,v0,w0,u1,v1,w1,pres,pres0,nt)
     !***********************************************************!
     !***********************************************************!
     !***********************************************************!
     !   End Time update of variables                  
     !***********************************************************!
     !***********************************************************!
     !***********************************************************!



     
     !***********************************************************!
     !***********************************************************!
     !***********************************************************!
     !  Computation Time Cost                   
     !***********************************************************!
     !***********************************************************!
     !***********************************************************!

     t2=t1
     t1=MPI_Wtime()
     cpu=cpu+(t1-t2)

     if (istep>0) then 
        if (modulo(nt,istep)==0) then
           if (rank==0) then
              write(*,*) '==========================Time================================='
              write(name,'(i12)') nt
              write(*,'("End cummulative time step:",1x,a)') trim(adjustl(name))
              write(name0,'(i12)') ntt
              write(name,'(i12)') nstep
              write(name1,'(f5.1)') 1d2*ntt/nstep
              write(*,'("        Iter/Iter run:",1x,a,"/",a,1x,"(",a,"%)")') &
                   & trim(adjustl(name0)), &
                   & trim(adjustl(name)),  &
                   & trim(adjustl(name1))
              write(*,'("        Physical time:",1x,1pe12.5)') time
           end if
        end if
     end if
     
     if (t2-t1/=0) then
        cpt_eta=cpt_eta+1
        cpt_eta=min(cpt_eta,size(eta))
        
        eta=cshift(eta,-1)
        eta(1)=(t1-t2)

        write(name0,'(1pe9.2)') sum(eta)/cpt_eta
        write(name1,'(f10.1)') (nstep-ntt)*sum(eta)/cpt_eta
     else
        name0="-"
        name1="-"
     end if

     if (istep>0) then
        if (modulo(nt,istep)==0) then

           call gmtime(floor(cpu),gmtvalues_cpu)
           call gmtime(floor((nstep-ntt)*sum(eta)/cpt_eta),gmtvalues)

           if (rank==0) then 
              write(*,'( "Avg. cost (sec.) for full 1 temporal iteration (over the last ",i3,"): ",a)') &
                   & cpt_eta,trim(adjustl(name0))
              !write(*,'( "ET (sec)   : ",f10.1)') cpu
              write(*,'( "ET         : ",i2," day(s), ",i2," hour(s), ",i2," min(s), ",i2," sec(s)")')  &
                   &  gmtvalues_cpu(8), gmtvalues_cpu(3), gmtvalues_cpu(2), gmtvalues_cpu(1)
              write(*,'( "ETA        : ",i2," day(s), ",i2," hour(s), ",i2," min(s), ",i2," sec(s)")')  &
                   & gmtvalues(8), gmtvalues(3), gmtvalues(2), gmtvalues(1)
           end if

           if (irestart>0) then 
              iter_to_restart=irestart-modulo(ntt,irestart)
              call gmtime(floor(iter_to_restart*sum(eta)/cpt_eta),gmtvalues)
              
              if (rank==0) then 
                 write(*,'( "ETA restart: ",i2," day(s), ",i2," hour(s), ",i2," min(s), ",i2," sec(s)")')  &
                      & gmtvalues(8), gmtvalues(3), gmtvalues(2), gmtvalues(1)
              end if
           end if
           
        end if
     end if


#if RESPECT
   call RESPECT_PostTreatementAndMore(vl,pres,u,v,w,cou,nt,time)
#endif

     !--------------------------------------------------------------------------------------
     ! Exit conditions 
     !--------------------------------------------------------------------------------------
     full_t2=MPI_Wtime()
     if (time_limit>0) then
        if (full_t2-full_t1>time_limit-time_restart) time_loop_exit=.true.
     end if
     !--------------------------------------------------------------------------------------
     !--------------------------------------------------------------------------------------

     !***********************************************************!
     !***********************************************************!
     !***********************************************************!
     !   End Time                   
     !***********************************************************!
     !***********************************************************!
     !***********************************************************!

     call compute_time(TIMER_PRINT)

     !********************************************************************************************!
     !********************************************************************************************!
     !********************************************************************************************!
     !********************************************************************************************!
     !                                    End of the time loop                                    ! 
     !********************************************************************************************!
     if (time_loop_exit) exit 
     !********************************************************************************************!
     !********************************************************************************************!
     !********************************************************************************************!
     !********************************************************************************************!
  end do
  !*******************************************************************************************!
  !*******************************************************************************************!
  !*******************************************************************************************!

  call compute_time(TIMER_END,"Time loop")
  call compute_time(TIMER_PRINT)

  call print_message("End of the time loop")
  call print_message("Writing final outputs ...")
  
  !***********************************************************!
  !***********************************************************!
  !***********************************************************!
  ! Final Outputs
  !***********************************************************!
  !***********************************************************!
  !***********************************************************!
  
  if (.not.NS_activate) then 
     if (EN_activate.and.(EN_init_icase==8.or.EN_init_icase==9)) then 
        pres=tp
        call Interp_Aslam(tp,sltp,3,work1,work2)
        ! call Write_field
     end if
  end if

  
  !---------------------------------------------------------------
  ! Line extrapolation
  !---------------------------------------------------------------
  if (EN_activate.and.EN_init_icase==3.and.NS_init_vel_icase==2) then
     do i=sx,ex
        call interpolation_phi(time,tp,(/grid_x(i),0.75d0/),var1,1,dim)
        write(72,*) grid_x(i), var1
     end do
  end if
  !---------------------------------------------------------------



  
  !***********************************************************!
  !***********************************************************!
  !***********************************************************!
  ! Generation of vizualization file at the end of the time
  !***********************************************************!
  !***********************************************************!
  !***********************************************************!


  !***********************************************************!
  !***********************************************************!
  !***********************************************************!

  !***************************************************************!
  !***************************************************************!
  !***************************************************************!
  ! Generation of restart file                                  
  !***************************************************************!
  !***************************************************************!
  !***************************************************************!

  !----------------------------------------------------------------
  ! For Navier-Stokes/Stokes/Euler
  !----------------------------------------------------------------
  if (exit_loop.or.time_loop_exit) then
     if (NS_activate.or.EN_activate) then
        l=nt-1
        !**** RESPECT
        select case(Euler)
        case(1)
           call read_write_EQ_mpi(l,time,cStats, &
                & cfl%dt_new,cfl%dt_old,         &
                & u,v,w,u0,v0,w0,pres,           &
                & tp,tp0,cou,dist,               &
                & work4,work5,work6,c_write,vl)
        case(2)
           call read_write_EQ_mpi(l,time,cStats, &
                & cfl%dt_new,cfl%dt_old,         &
                & u0,v0,w0,u1,v1,w1,pres,        &
                & tp0,tp1,cou,dist,              &
                & work4,work5,work6,c_write,vl)
        end select
        !**** RESPECT
     end if
  end if
  !----------------------------------------------------------------
  ! For Lagrangian particle tracking
  !----------------------------------------------------------------
  
  !----------------------------------------------------------------
  ! For Lagrangian particle tracking
  !----------------------------------------------------------------
  ! For front tracking
  !----------------------------------------------------------------
  !***************************************************************!
  !***************************************************************!
  !***************************************************************!


  !***************************************************************!
  !***************************************************************!
  !***************************************************************!
  ! Done file !                                  
  !***************************************************************!
  !***************************************************************!
  !***************************************************************!
  if (rank==0) then
     call system("rm running")
     if (.not.exit_loop .and. .not.time_loop_exit) then
        open(19,file="done")
        close(19)
     end if
  end if
  !***************************************************************!
  !***************************************************************!
  !***************************************************************!

  call compute_time(TIMER_END,"WALL CLOCK TIME")
  call compute_time(TIMER_PRINT)
  call finalization_mpi

  


contains

  function is_nan_array(tab)
    implicit none
    !---------------------------------------------------------------------------
    ! global variables
    !---------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable, intent(in) :: tab
    logical                                            :: is_nan_array
    !---------------------------------------------------------------------------
    ! local variable
    !---------------------------------------------------------------------------
    real(8)                                            :: var
    !---------------------------------------------------------------------------

    is_nan_array=.false.
    var=sum(tab)
    if (var/=var) is_nan_array=.true.
  end function is_nan_array
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
end program FUGU
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!





!!!!!!!!!!! ASLAM !!!!!!!!!!!!!!!!!


#if 0 

  pres=tp
  call Interp_Aslam(tp,sltp,3,work1,work2)
  
  !---------------------------------------------------------------
  ! Force computation on obstacles (adapt for Cd, Cl ...)
  !---------------------------------------------------------------
  if (NS_activate.and.NS_linear_term) then
     if (iplot>0) then 
        if (modulo(nt,iplot)==0) then
           call outputs_force_obstacle(80,time,u,v,w,slvu,slvv,slvw)
        end if

        if (EN_activate) then

           work1=0
           work2=0
           work3=0
           work4=0
           do k=sz,ez
              do j=sy,ey
                 do i=sx,ex

                    if (sltp(i,j,k)==0) then 
                       ! grad T
                       work1(i,j,k)=(tp(i+1,j,k)-tp(i-1,j,k))/dx(i)/2
                       work2(i,j,k)=(tp(i,j+1,k)-tp(i,j-1,k))/dy(j)/2
                       ! grad C
                       work3(i,j,k)=(sltp(i+1,j,k)-sltp(i-1,j,k))/dx(i)/2
                       work4(i,j,k)=(sltp(i,j+1,k)-sltp(i,j-1,k))/dy(j)/2
                    end if
                 end do
              end do
           end do

           var5=0
           do k=sz,ez
              do j=sy,ey
                 do i=sx,ex

                    var1=work1(i,j,k)
                    var2=work2(i,j,k)

                    ! interior normal
                    var3=work3(i,j,k)/sqrt(work3(i,j,k)**2+work4(i,j,k)**2+1d-40)
                    var4=work4(i,j,k)/sqrt(work3(i,j,k)**2+work4(i,j,k)**2+1d-40)

                    work3(i,j,k)=work3(i,j,k)/sqrt(work3(i,j,k)**2+work4(i,j,k)**2+1d-40)
                    work4(i,j,k)=work4(i,j,k)/sqrt(work3(i,j,k)**2+work4(i,j,k)**2+1d-40)



                    work5(i,j,k)=(var1*var3+var2*var4)

                    var5=var5+work5(i,j,k)*dx(i)*dy(j)

                 end do
              end do
           end do

           !write(*,*) time,var5,var5*2*pi,maxval(work5)

        end if
     end if
  end if
  !---------------------------------------------------------------
  ! End force computation on obstacles
  !---------------------------------------------------------------


  if (EN_activate.and.EN_ipen==-100.and.NS_ipen==-2) then


     do k=1,101
        var3=(k-1)/100d0*2*pi     ! theta
        var4=6.4d0-d1p2*cos(var3) ! x
        var5=6.4d0+d1p2*sin(var3) ! y

        ! valeur la plus proche
        var1=100000
        do j=sy,ey
           do i=sx,ex
              if (((var4-grid_x(i))**2+(var5-grid_y(j))**2)<var1.and.work5(i,j,1)>1) then
                 var1=((var4-grid_x(i))**2+(var5-grid_y(j))**2)
                 var2=work5(i,j,1)
              end if
           end do
        end do
        
        write(41,*) var3, var2
        
     end do
     
     do j=sy,ey
        do i=sx,ex
           if (work5(i,j,1)>0.01) then
              var1=atan(-(grid_y(j)-6.4d0)/(grid_x(i)-6.4d0))
              var2=work5(i,j,1)

              if ((grid_x(i)-6.4d0)<0.and.(grid_y(j)-6.4d0)>0) then
                 var1=var1
              else if ((grid_x(i)-6.4d0)>0.and.(grid_y(j)-6.4d0)>0) then
                 var1=var1+pi
              else if ((grid_x(i)-6.4d0)>0.and.(grid_y(j)-6.4d0)<0) then
                 var1=var1+pi
              else
                 var1=var1+2*pi
              end if
                 
              
              write(42,*) var1, var2


           end if
        end do
     end do
     



     work9=sltp

     sltp=sltp/(sltp+1d-40)

     
     call smoothing(sltp,20)



     !----------------------------------------------------------------------
     ! version normale sur la maillage vitesse
     !----------------------------------------------------------------------
     work1=0
     work2=0
     work3=0
     work4=0
     do k=szu,ezu
        do j=syu,eyu
           do i=sxu,exu
              ! grad C
              work3(i,j,k)=(sltp(i,j,k)-sltp(i-1,j,k))/dxu(i)
           end do
        end do
     end do
     do k=szv,ezv
        do j=syv,eyv
           do i=sxv,exv
              ! grad C
              work4(i,j,k)=(sltp(i,j,k)-sltp(i,j-1,k))/dyv(j)
           end do
        end do
     end do
     
     do k=sz,ez
        do j=sy,ey
           do i=sx,ex
              work3(i,j,k)=work3(i,j,k)/sqrt(work3(i,j,k)**2+work4(i,j,k)**2+1d-40)
              work4(i,j,k)=work4(i,j,k)/sqrt(work3(i,j,k)**2+work4(i,j,k)**2+1d-40)
           end do
        end do
     end do
     
     
     
     work1=0
     work2=0
     do k=szu,ezu
        do j=syu,eyu
           do i=sxu,exu
              !---------------------------------------------------------
              ! grad T 2eme ordre centre
              !---------------------------------------------------------
              work1(i,j,k)=(tp(i,j,k)-tp(i-1,j,k))/dxu(i)
           end do
        end do
     end do
     do k=szv,ezv
        do j=syv,eyv
           do i=sxv,exv
              !---------------------------------------------------------
              ! grad T 2eme ordre centre
              !---------------------------------------------------------
              work2(i,j,k)=(tp(i,j,k)-tp(i,j-1,k))/dyv(j)
           end do
        end do
     end do
     
     do k=szu,ezu
        do j=syu,eyu
           do i=sxu,exu
              work5(i,j,k)=work1(i,j,k)*work3(i,j,k)
           end do
        end do
     end do
     do k=szv,ezv
        do j=syv,eyv
           do i=sxv,exv
              work6(i,j,k)=work2(i,j,k)*work4(i,j,k)
           end do
        end do
     end do
     do k=sz,ez
        do j=sy,ey
           do i=sx,ex
              work7(i,j,k)=d1p2*(work5(i+1,j,k)+work5(i,j,k)) &
                   & +d1p2*(work6(i,j+1,k)+work6(i,j,k))
           end do
        end do
     end do
     
     
     
     do j=sy,ey
        do i=sx,ex
           if (work9(i,j,1)==0.and.(sum(work9(i-1:i+1,j,1))/=0.or.sum(work9(i,j-1:j+1,1))/=0)) then
              var3=sqrt((grid_y(j)-6.4d0)**2+(grid_x(i)-6.4d0)**2)
              var1=atan(-(grid_y(j)-6.4d0)/(grid_x(i)-6.4d0)) ! theta
              var2=work7(i,j,1)
              
              if ((grid_x(i)-6.4d0)<0.and.(grid_y(j)-6.4d0)>0) then
                 var1=var1
              else if ((grid_x(i)-6.4d0)>0.and.(grid_y(j)-6.4d0)>0) then
                 var1=var1+pi
              else if ((grid_x(i)-6.4d0)>0.and.(grid_y(j)-6.4d0)<0) then
                 var1=var1+pi
              else
                 var1=var1+2*pi
              end if
                 
              write(53,*) var1,var2,var3,abs(var3-d1p2)/d1p2*100
       
           end if
        end do
     end do
     
     
     
     











































     







     
     
     work1=0
     work2=0
     work3=0
     work4=0
     do k=sz,ez
        do j=sy,ey
           do i=sx,ex
              
              !!       if (sltp(i,j,k)==0) then 
              ! grad T
              work1(i,j,k)=(tp(i+1,j,k)-tp(i-1,j,k))/dx(i)/2
              work2(i,j,k)=(tp(i,j+1,k)-tp(i,j-1,k))/dy(j)/2
              ! grad C
              work3(i,j,k)=(sltp(i+1,j,k)-sltp(i-1,j,k))/dx(i)/2
              work4(i,j,k)=(sltp(i,j+1,k)-sltp(i,j-1,k))/dy(j)/2
              !!       end if
           end do
        end do
     end do
     
     var5=0
     do k=sz,ez
        do j=sy,ey
           do i=sx,ex
              
              var1=work1(i,j,k)
              var2=work2(i,j,k)

              ! interior normal
              var3=work3(i,j,k)/sqrt(work3(i,j,k)**2+work4(i,j,k)**2+1d-40)
              var4=work4(i,j,k)/sqrt(work3(i,j,k)**2+work4(i,j,k)**2+1d-40)
              
              work3(i,j,k)=work3(i,j,k)/sqrt(work3(i,j,k)**2+work4(i,j,k)**2+1d-40)
              work4(i,j,k)=work4(i,j,k)/sqrt(work3(i,j,k)**2+work4(i,j,k)**2+1d-40)
              
           end do
        end do
     end do
     
     
     
     work1=0
     work2=0
     work3=0
     work4=0
     do k=sz,ez
        do j=sy,ey
           do i=sx,ex

              !---------------------------------------------------------
              ! grad T
              !---------------------------------------------------------
              ! 2eme ordre centre
              !---------------------------------------------------------
              work1(i,j,k)=(tp(i+1,j,k)-tp(i-1,j,k))/dx(i)/2
              work2(i,j,k)=(tp(i,j+1,k)-tp(i,j-1,k))/dy(j)/2
!!$              !---------------------------------------------------------
!!$              ! decentre 1 ordre
!!$              !---------------------------------------------------------
!!$              if  (work9(i-1,j,k)/=0.and.work9(i+1,j,k)==0) then
!!$                 work1(i,j,k)=(tp(i+1,j,k)-tp(i,j,k))/dxu(i+1)
!!$              else if (work9(i+1,j,k)/=0.and.work9(i-1,j,k)==0) then 
!!$                 work1(i,j,k)=(tp(i,j,k)-tp(i-1,j,k))/dxu(i)
!!$              else
!!$                 work1(i,j,k)=(tp(i+1,j,k)-tp(i-1,j,k))/dx(i)/2
!!$              end if
!!$              if  (work9(i,j-1,k)/=0.and.work9(i,j+1,k)==0) then
!!$                 work2(i,j,k)=(tp(i,j+1,k)-tp(i,j,k))/dyv(j+1)
!!$              else
!!$                 work2(i,j,k)=(tp(i,j,k)-tp(i,j-1,k))/dyv(j)
!!$              end if
!!$              !---------------------------------------------------------
!!$              ! decentre 2 ordre
!!$              !---------------------------------------------------------
!!$              if  (work9(i-1,j,k)/=0.and.work9(i+1,j,k)==0) then
!!$                 work1(i,j,k)=(-3*tp(i,j,k)+4*tp(i+1,j,k)-tp(i+2,j,k))/2/dx(i)
!!$              else if (work9(i+1,j,k)/=0.and.work9(i-1,j,k)==0) then 
!!$                 work1(i,j,k)=(3*tp(i,j,k)-4*tp(i-1,j,k)+tp(i-2,j,k))/2/dx(i)
!!$              else
!!$                 work1(i,j,k)=(tp(i+1,j,k)-tp(i-1,j,k))/dx(i)/2
!!$              end if
!!$              if  (work9(i,j-1,k)/=0.and.work9(i,j+1,k)==0) then
!!$                 work2(i,j,k)=(-3*tp(i,j,k)+4*tp(i,j+1,k)-tp(i,j+2,k))/2/dy(j)
!!$              else if (work9(i,j+1,k)/=0.and.work9(i,j-1,k)==0) then               
!!$                 work2(i,j,k)=(3*tp(i,j,k)-4*tp(i,j-1,k)+tp(i,j-2,k))/2/dy(j)
!!$              else
!!$                  work2(i,j,k)=(tp(i,j+1,k)-tp(i,j-1,k))/dy(j)/2
!!$              end if
              !---------------------------------------------------------

              !---------------------------------------------------------
              ! grad C
              !---------------------------------------------------------
              work3(i,j,k)=(sltp(i+1,j,k)-sltp(i-1,j,k))/dx(i)/2
              work4(i,j,k)=(sltp(i,j+1,k)-sltp(i,j-1,k))/dy(j)/2
              
              !---------------------------------------------------------

              !---------------------------------------------------------
              ! analytical normal
              !---------------------------------------------------------
              var1=atan(-(grid_y(j)-6.4d0)/(grid_x(i)-6.4d0)) ! theta
              if ((grid_x(i)-6.4d0)<0.and.(grid_y(j)-6.4d0)>0) then
                 var1=var1
              else if ((grid_x(i)-6.4d0)>0.and.(grid_y(j)-6.4d0)>0) then
                 var1=var1+pi
              else if ((grid_x(i)-6.4d0)>0.and.(grid_y(j)-6.4d0)<0) then
                 var1=var1+pi
              else
                 var1=var1+2*pi
              end if
              work3(i,j,k)=+d1p2*cos(var1)
              work4(i,j,k)=-d1p2*sin(var1)
              !---------------------------------------------------------

              var1=(sqrt(work3(i,j,k)**2+work4(i,j,k)**2)+1d-40)
              work3(i,j,k)=work3(i,j,k)/var1
              work4(i,j,k)=work4(i,j,k)/var1


           end do
        end do
     end do

     ! call Write_field

     
     var5=0
     do k=sz,ez
        do j=sy,ey
           do i=sx,ex

              var1=work1(i,j,k)
              var2=work2(i,j,k)

              !--------------------------------------------------------------
              ! interior normal
              !--------------------------------------------------------------
              var3=work3(i,j,k)/(sqrt(work3(i,j,k)**2+work4(i,j,k)**2)+1d-40)
              var4=work4(i,j,k)/(sqrt(work3(i,j,k)**2+work4(i,j,k)**2)+1d-40)
              work3(i,j,k)=var3
              work4(i,j,k)=var4
              !--------------------------------------------------------------

              write(*,*) sqrt(work3(i,j,k)**2+work4(i,j,k)**2),work3(i,j,k)**2+work4(i,j,k)**2

              
              work5(i,j,k)=(var1*var3+var2*var4)

              var5=var5+work5(i,j,k)*dx(i)*dy(j)

           end do
        end do
     end do



     do j=sy,ey
        do i=sx,ex
           !if (work6(i,j,1)==0.and.sum(work6(i-1:i+1,j-1:j+1,1))/=0) then
           if (work9(i,j,1)==0.and.(sum(work9(i-1:i+1,j,1))/=0.or.sum(work9(i,j-1:j+1,1))/=0)) then
              var3=sqrt((grid_y(j)-6.4d0)**2+(grid_x(i)-6.4d0)**2) ! r
              var1=atan(-(grid_y(j)-6.4d0)/(grid_x(i)-6.4d0))      ! theta
              var2=work5(i,j,1)

              if ((grid_x(i)-6.4d0)<0.and.(grid_y(j)-6.4d0)>0) then
                 var1=var1
              else if ((grid_x(i)-6.4d0)>0.and.(grid_y(j)-6.4d0)>0) then
                 var1=var1+pi
              else if ((grid_x(i)-6.4d0)>0.and.(grid_y(j)-6.4d0)<0) then
                 var1=var1+pi
              else
                 var1=var1+2*pi
              end if
                 
              write(43,*) var1,var2,var3,abs(var3-d1p2)/d1p2*100
       
           end if
        end do
     end do

     allocate(work2d1(sx:ex,sy:ey))
     
     do k=1,101
        var3=(k-1)/100d0*2*pi     ! theta
        var4=6.4d0-d1p2*cos(var3) ! x
        var5=6.4d0+d1p2*sin(var3) ! y

        !---------------------------------------------------------------
        ! valeur la plus proche
        !---------------------------------------------------------------
        var1=100000
        do j=sy,ey
           do i=sx,ex
              if (((var4-grid_x(i))**2+(var5-grid_y(j))**2)<var1.and.work6(i,j,1)==1) then
                 var1=((var4-grid_x(i))**2+(var5-grid_y(j))**2)
                 var2=work5(i,j,1)
              end if
           end do
        end do
        write(44,*) var3, var2
        !---------------------------------------------------------------
        ! interpolation 4 noeuds sca voisin
        !---------------------------------------------------------------
        call interpolation_phi(time,work5,(/var4,var5,zero/),var2,1,dim)
        write(45,*) var3, var2
        !---------------------------------------------------------------


        work2d1=10000
        do j=sy,ey
           do i=sx,ex
              if (work6(i,j,1)==0) then 
                 work2d1(i,j)=sqrt((var4-grid_x(i))**2+(var5-grid_y(j))**2)
              end if
           end do
        end do


        tabi(1:2)=minloc(work2d1(:,:))
        i=tabi(1)-1
        j=tabi(2)-1
        x1=grid_x(i)
        y1=grid_y(j)
        var1=work5(i,j,1)
        work2d1(i,j)=10000000

        tabi(1:2)=minloc(work2d1(:,:))
        i=tabi(1)-1
        j=tabi(2)-1
        x2=grid_x(i)
        y2=grid_y(j)
        var2=work5(i,j,1)
        work2d1(i,j)=10000000

        tabi(1:2)=minloc(work2d1(:,:))
        i=tabi(1)-1
        j=tabi(2)-1
        x3=grid_x(i)
        y3=grid_y(j)
        var3=work5(i,j,1)
        work2d1(i,j)=10000000
        
        call interp3pts((/var4,var5/),var,(/x1,y1/),var1,(/x2,y2/),var2,(/x3,y3/),var3)

        var3=(k-1)/100d0*2*pi     ! theta
        write(46,*) var3,var
        


        
     end do

     
  end if
  
#endif 
