!-----------------------------------------------------------------------------  
!> @author amine chadil
!
!> @brief ce module contient les donnees des particules
!
!> @date 4/04/2013   
!-----------------------------------------------------------------------------
!===============================================================================
!structure de donnees pour les particules lagrangiennes
module Module_VOFLag_ParticlesDataStructure
  !===============================================================================
  
  type struct_objet
    ! ObjectFile            : nom du fichier objet
    ! kkl                   : nombre d'elements de la surface
    ! lag_for_all_copies    : tableau des coordonnées des éléments de la surface (dans le repere fixe) avec son centre a l'origine du repere 
    ! lag                   : tableau des coordonnées des éléments de la surface de la particule traitee (dans le repere réel)
    ! normale               : tableau des normales a chaque element
    ! hea,heav              : Heaviside pression (1 ou 0), HEAV Heavyside maillage vitesse
    ! dist                  : fonction distance pression
    ! area                  : la mesure de la surface de chaque elements
    ! masse,rho             : la masse et la masse volumique de la particule
    ! sca                   : le scalling de la particule (Rayon pour une sphere, et demi-axe pour un ellipsoide)
    ! pos                   : coordonnees du barycentre a l'instant n+1
    ! omeg                  : la vitesse de rotation
    ! v,vn                  : les vitesses de translation
    ! euler_angle           : angle d'euler de rotation 123-sequence
    ! orientation           : tableau qui caracterise l'orientation de la particule
    !                              En 2D: orientation(1) est l'angle de rotation, dans le sens direct, du repere de la particule par rapport au repere fixe
    !                              En 3D: c'est le quaternion permettant la rotation de la particule tjrs par rapport au repere fixe
    ! locpart               : rang du proc dont se trouve la particule
    ! aff                   : afficher la particule (global)
    ! in                    : la particule est dans le domaine (local au proc)
    ! on                    : la particule touche le domaine (local au proc)
    ! ar                    : la particule est dans un domaine voisin (local au proc)
    ! nbtimes               : nombre de copies d'une particule due a la periodicite (de 1 a 27) 
    ! symper(1:nbtimes,3)   : positions de ces copies (symper(1,:) position actuelle)
    ! symperon(1:nbtimes)   : statut des copies de la particule par rapport au domaine du proc courant
    ! ltp_particle          : tableau du numero d'un noeud de pression appartenant a une copie de la particule
    ! chocparpar            : la particule choc contre une autre
    ! chocparmur            : la particule choc contre un mur
    ! fparpar               : valeur de la force de l'interaction par par
    ! fparmur               : valeur de la force de l'interaction par mur
    ! f,fp,fv,moment        : force totale, force de trainee, force de portance, moment,...
    ! kktBoxParticle        : tableau du nombre de point de pression dans le box contenant la copie de la particule 
    !                              taille = nbtimes
    ! ltpBoxParticle        : tableau des numeros des points de pression dans le box contenant la copie de la particule 
    !                              taille = max(kktBoxParticle)*nbtimes
    real(8), dimension(:,:,:), allocatable :: lag_for_all_copies,lag
    integer, dimension(:,:,:), allocatable :: BoxIndex
    real(8), dimension(:,:),   allocatable :: normal,symper
    real(8), dimension(:),     allocatable :: hea,dist,area
    logical, dimension(:),     allocatable :: symperon
    real(8), dimension(3)                  :: sca,pos,euler_angle,omeg,v,vn,fparpar,fparmur,f,fp,fv,moment
    real(8), dimension(4)                  :: orientation
    character(len=80)                      :: ObjectFile
    real(8)                                :: masse,rho,HeatFlux,BulkTemp
    integer                                :: locpart,kkl,nbtimes 
    logical                                :: aff,in,on,ar,chocparpar,chocparmur
  end type struct_objet

  type struct_vof_lag
    ! nature                : toutes les particules sont de la meme nature;1 si sphere 2 si ellipsoide, 3 si autre forme
    ! kpt                   : nombre de particules
    ! objet(1:vl%kpt)       : tous les objets
    ! nbaffich              : nombre de particules a afficher (local au proc)
    ! affich(1:vl%nbaffich) : liste des particules a afficher (local au proc)
    ! filec(1:vl%nbaffich)  : nom des fichiers pour ecrire ces particules (local au proc)
    ! deplacement           : activer le deplacement des particules
    ! vrot                  : activer la rotation des particules
    ! postforce             : activer le calcul de la force hydrodynamique
    ! postHeatFlux          : activer le calcul le flux de chaleur
    ! force_inter           : activer le traitement des collisions
    ! interpar(1:vl%kpt)    : y a t'il un choc de la particule 
    ! postchoc_-p-          : postraiter chocs : bp- binaire , fp- force, -pp par-par, -pm par-mur   
    ! choc_lubri            : activer la lubrification
    ! choc_sec              : activer le choc a vide
    ! choc_limit            : activer une limite de recouvrement
    ! sec_nc                : nombre d'iterations durant le choc a vide
    ! sec_ed                : coefficient de restitution a vide
    ! sec_distcri           : distance critique pour le choc a sec
    ! distcri               : distance critique = 0.1*r
    ! lubri_eps_..          : distances pour la lubrification (voir notice) pp : particule particule, pm : particule-mur
    ! lubri_lambda_..       : developpement limite pour la lubrification (voir notice)
    ! limit_distcri         : distance critique pour la limite
    character(len=1000), dimension(:), allocatable :: filec
    type(struct_objet),  dimension(:), allocatable :: objet
    integer,             dimension(:), allocatable :: affich
    real(8),             dimension(3)              :: ft_n=0.d0
    real(8)                                        :: sec_ed=0.9d0,sec_distcri=0.d0,lubri_eps_pp=0.025d0,     &
                                                      lubri_eps_pm=0.075d0,lubri_eps_1=1.d-3,lubri_eps_2=0.d0,&
                                                      limit_distcri=0.d0,lubri_lambda_pp,lubri_lambda_pm,&
                                                      lubri_lambda_pp_eps1,lubri_lambda_pm_eps1
    integer                                        :: nature=1,kpt=1,nbaffich=1,sec_nc=8
    logical                                        :: deplacement =.false.,vrot        =.false.,postforce   =.false.,&
                                                      force_inter =.false.,choc_lubri  =.false.,choc_sec    =.false.,&
                                                      postchoc_bpp=.false.,postchoc_bpm=.false.,postchoc_fpp=.false.,& 
                                                      postHeatFlux=.false.,choc_limit  =.false.,postchoc_fpm=.false.
  end type struct_vof_lag
  
  real(8), dimension(3)                                      :: WallPos=0d0
  integer                                                    :: nature=1,kpt=1
  logical                                                    :: deplacement =.false.,vrot          =.false.,&
                                                                force_inter =.false.,Wall_Creaction=.false.,&
                                                                choc_lubri  =.false.,choc_sec      =.false.,&
                                                                postchoc_fpm=.false.,postforce     =.false.
                                                                
  ! logical                                 :: RESPECT_ACTIVATE=.false.
  ! character(len=100)                      :: Particles_Data = ""
  namelist /nml_RESPECT/ nature,kpt,deplacement,vrot,force_inter,choc_lubri,&
                      choc_sec,postchoc_fpm,WallPos,Wall_Creaction,postforce!,postHeatFlux,&
                      ! choc_limit,&
                      ! postchoc_bpp,postchoc_bpm,postchoc_fpp,postchoc_fpm

  integer,                 save :: rank_nbtimes
  real(8), dimension(27,3), save :: rank_symper
  
  !===============================================================================
end module Module_VOFLag_ParticlesDataStructure
!===============================================================================