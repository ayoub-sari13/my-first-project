!========================================================================
!
!                   FUGU Version 1.0.0
!
!========================================================================
!
! Copyright  Stéphane Vincent
!
! stephane.vincent@u-pem.fr          straightparadigm@gmail.com
!
! This file is part of the FUGU Fortran library, a set of Computational 
! Fluid Dynamics subroutines devoted to the simulation of multi-scale
! multi-phase flows for resolved scale interfaces (liquid/gas/solid). 
! FUGU uses the initial developments of DyJeAT and SHALA codes from 
! Jean-Luc Estivalezes (jean-luc.estivalezes@onera.fr) and 
! Frédéric Couderc (couderc@math.univ-toulouse.fr).
!
!========================================================================
!**
!**   NAME       : Fugu.f90
!**
!**   AUTHORS    : Stéphane Vincent
!**
!**   FUNCTION   : References
!**
!**   DATES      : Version 1.1.0  : from : jan, 18, 2018
!**
!========================================================================
MODULE mod_references
  
contains
  
  subroutine Wannier2D(u,v,radius,rot,dist,uinf)
    ! call Wannier2D(u,v,2d0/7,0d0,abs(ymin),1d0)
    !-------------------------------------------------------------------------------
    ! Description:
    !-------------------------------------------------------------------------------
    ! Compute solution of Wannier flow given in Echeverlepo 2013 PhD Thesis
    !-------------------------------------------------------------------------------
    ! Modules
    !-------------------------------------------------------------------------------
    use mod_Parameters, only: sx,ex,sy,ey,dx,dy,dx2,dy2,xmin,ymin, &
         & grid_x,grid_y,grid_z,grid_xu,grid_yv,grid_zw
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), intent(in)                                   :: radius,rot,dist,uinf
    real(8), allocatable, dimension(:,:,:), intent(inout) :: u,v
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                               :: i,j
    real(8)                                               :: x,y
    real(8)                                               :: a0,a1,a2,a3,s,G,K1,K2,Y1,Y2
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------
    ! fixed quantities
    !-------------------------------------------------------------
    s=sqrt(dist**2-radius**2)
    G=(dist+s)/(dist-s)
    a0=uinf/log(G)
    a1=-dist*a0
    a2=2*(dist+s)*a0 ! factor 2 ? see High Order Method for Computational Physics 
    a3=2*(dist-s)*a0 !                Thimothy & Deconinck pp 287
    !-------------------------------------------------------------

    !-------------------------------------------------------------
    ! X-component
    !-------------------------------------------------------------
    u=0
    do j=sy,ey
       do i=sx,ex+1
          x=grid_xu(i)
          y=grid_y(j)
          Y1=y+dist
          Y2=2*Y1
          K1=x*x+(s+Y1)**2
          K2=x*x+(s-Y1)**2
          if (x**2+y**2>=radius**2) then 
             u(i,j,1)=uinf                              &
                  & -2*(a1+a0*Y1)*((s+Y1)/K1+(s-Y1)/K2) &
                  & -a0*log(K1/K2)                      &
                  & -a2/K1*(s+Y2-(s+Y1)**2*Y2/K1)       &
                  & -a3/K2*(s-Y2+(s-Y1)**2*Y2/K2)
!!$                  & -a2/K1*(s+Y2-Y2*(s+Y1)**2/K2) ! Karniadakis formulation
          end if
       end do
    end do
    !-------------------------------------------------------------

    !-------------------------------------------------------------
    ! Y-component
    !-------------------------------------------------------------
    v=0
    do j=sy,ey+1
       do i=sx,ex
          x=grid_x(i)
          y=grid_yv(j)
          Y1=y+dist
          Y2=2*Y1
          K1=x*x+(s+Y1)**2
          K2=x*x+(s-Y1)**2
          if (x**2+y**2>=radius**2) then 
             v(i,j,1)=2*x/K1/K2*(a1+a0*Y1)*(K2-K1) &
                  & -x*a2*(s+Y1)*Y2/K1/K1          &
                  & -x*a3*(s-Y1)*Y2/K2/K2
          end if
       end do
    end do
    !-------------------------------------------------------------
    
    return
    !-------------------------------------------------------------------------------
  end subroutine Wannier2D


end MODULE mod_references
