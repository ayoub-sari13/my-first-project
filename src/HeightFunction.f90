#include "precomp.h"
module mod_height_function

  INTEGER, PARAMETER :: EPS_COU_CLIP   = 1d-6    ! clipping color 
  INTEGER, PARAMETER :: EPS_KAPPA_FLAG = 1d-12   ! eps kappa

  INTEGER, PARAMETER :: FLAG_UNDECIDED = -1
  INTEGER, PARAMETER :: FLAG_FOUND     = 0
  INTEGER, PARAMETER :: FLAG_PICK_X    = 1
  INTEGER, PARAMETER :: FLAG_PICK_Y    = 2
  INTEGER, PARAMETER :: FLAG_PICK_Z    = 3
  
contains

  subroutine HF_compute_kappa(mesh,cou,kappa)
    use mod_constants, only: d1p2,d3p2
    use mod_mpi
    use mod_struct_grid
    use mod_timers
    implicit none
    !--------------------------------------------------------------------------
    ! global variables
    !--------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable, intent(in)    :: cou
    real(8), dimension(:,:,:), allocatable, intent(inout) :: kappa
    type(grid_t), intent(in)                              :: mesh
    !--------------------------------------------------------------------------
    ! local variables
    !--------------------------------------------------------------------------
    integer                                               :: i,j,k
    integer                                               :: ii,jj,kk
    integer                                               :: ip,im
    integer                                               :: snormal,dir
    integer, dimension(:,:,:), allocatable                :: kappa_flag
    real(8)                                               :: kap
    real(8)                                               :: den
    real(8)                                               :: normal
    real(8)                                               :: hy,hyy,hz,hzz,hyz
    real(8)                                               :: hy0,hy1,hy2
    real(8), dimension(3)                                 :: dcou
    real(8), dimension(-1:1)                              :: y,dy,z,dz
    real(8), dimension(-3:3)                              :: x,dx,alpha
    real(8), dimension(-1:1,-1:1)                         :: h
    real(8), dimension(-3:3,-1:1,-1:1)                    :: coul,couls
    real(8), dimension(:,:,:), allocatable                :: kappa_mix
    !--------------------------------------------------------------------------

    !--------------------------------------------------------------------------
    ! Cummings et al. Estimating curvature from volume fractions
    ! Malik et al. Adaptative VOF with surface curvature-based refinement
    ! Hernandez et al. A new volume of fluid method in three dimension -- Part 1
    ! Popinet. An accurate adaptative solver for surface-tension-driven interfactial flows.
    !--------------------------------------------------------------------------
    
    call compute_time(TIMER_START,"[sub] HF_compute_kappa")
    
#if 1

    allocate(kappa_flag(mesh%sx-mesh%gx:mesh%ex+mesh%gx, &
         &              mesh%sy-mesh%gy:mesh%ey+mesh%gy, &
         &              mesh%sz-mesh%gz:mesh%ez+mesh%gz))
    allocate(kappa_mix (mesh%sx-mesh%gx:mesh%ex+mesh%gx, &
         &              mesh%sy-mesh%gy:mesh%ey+mesh%gy, &
         &              mesh%sz-mesh%gz:mesh%ez+mesh%gz))

    kappa_flag = FLAG_UNDECIDED
    kappa_mix  = 0
    
    kappa = 0
    
    if (mesh%dim==2) then 
       
       do jj = mesh%sy,mesh%ey
          do ii = mesh%sx,mesh%ex
             
             ! at interface
             dcou = 0
             coul = 0

             dcou(1) = sum(cou(ii+1,jj-1:jj+1,1)) - sum(cou(ii-1,jj-1:jj+1,1))
             dcou(2) = sum(cou(ii-1:ii+1,jj+1,1)) - sum(cou(ii-1:ii+1,jj-1,1))

             if (sum(abs(dcou(1:mesh%dim)))<1d-13*mesh%dim) cycle
             
             ! main direction i, and local stencil
             dir  = 0
             dcou = abs(dcou)
             if ( dcou(1) >= dcou(2) ) then
                dir = 1
                ! i,j
                do j = -1,1
                   do i = -3,3
                      coul(i,j,0) = cou(ii+i,jj+j,1)
                   end do
                end do
                x  = mesh%x(ii-3:ii+3)
                y  = mesh%y(jj-1:jj+1)
                dx = mesh%dx(ii-3:ii+3)
!!$                dy = mesh%dy(jj-1:jj+1)
             else
                dir = 2
                ! j,i
                do j = -1,1
                   do i = -3,3
                      coul(i,j,0) = cou(ii+j,jj+i,1)
                   end do
                end do
                x  = mesh%y(jj-3:jj+3)
                y  = mesh%x(ii-1:ii+1)
                dx = mesh%dy(jj-3:jj+3)
!!$                dy = mesh%dx(ii-1:ii+1)
             end if
             
             
!!$             coul = 0
!!$             coul (-3:0,:,:) = 1
!!$             coul(-1,-1,:)   = 0.9d0
!!$             coul(0,-1,:)    = 0.4d0
!!$             coul(0,0,:)     = 0.8d0
!!$             coul(0,1,:)     = 1
!!$             coul(1,-1,:)    = 0
!!$             coul(1,0,:)     = 0.1
!!$             coul(1,1,:)     = 0.3d0
             
!!$             ! test shift
!!$             coul(:,:,-3:2) = coul(:,:,-2:3)
!!$             coul(:,:,3)    = 0
!!$             
!!$             ! test inverse phases    
!!$             coul = 1-coul
             
!!$             ! double interface, kappa = -0.14310835269246713
!!$             coul(-3,0,:)    = 0.7d0
!!$             coul(-3,1,:)    = 0.2d0
!!$             coul(-2,1,:)    = 0.95d0
             
             
!!$             dx = 1
!!$             dy = 1
!!$             x = (/ 1,2,3,4,5,6,7 /)
!!$             y = (/ 1,2,3 /)
             
             ! coul^star = coul
             couls = coul

             ! height does not lie in the cell
             if (dot_product(couls(:,0,0),dx(:)) < sum(dx(-3:-1))) then
                kappa_flag(ii,jj,1) = dir 
                cycle
             end if
             if (dot_product(couls(:,0,0),dx(:)) > sum(dx(-3:0)))  then
                kappa_flag(ii,jj,1) = dir 
                cycle
             end if

             ! normal
             normal = (coul(1,0,0)-coul(-1,0,0)) !/(x(1)-x(-1))
             
             ! sign normal
             snormal = 0
             if (normal > 0) then
                snormal = +1
             elseif (normal < 0) then
                snormal = -1
             else
                cycle
                write(*,*) "n=0"
                write(*,*) "coul"
                do i = -3,3
                   write(*,*) (coul(i,j,0),j=-1,1)
                end do
                write(*,*)
             end if
             
             ! alpha_k
             alpha = 0
             do j = -1,1
                do i = -3,3
                   alpha(i) = alpha(i) + coul(i,j,0)
                end do
             end do
             
             ! compute kp;km (t_up,t_low)
             ip = 0
             i  = 1
             do while (i<=3) 
                if ( snormal*(alpha(i)-alpha(i-1))>0 .and. (alpha(i)/=0) .and. (alpha(i)/=3) ) then
                   ip = i
                   i  = i+1
                else
                   exit
                end if
             end do

             im = 0
             i  = -1
             do while (i>=-3) 
                if ( snormal*(alpha(i+1)-alpha(i))>0 .and. (alpha(i)/=0) .and. (alpha(i)/=3) ) then
                   im = i
                   i  = i-1
                else
                   exit
                end if
             end do

             ! force monotonic variation
             do j = -1,1
                do i = im,-1
                   if ( snormal*(coul(i,j,0)-coul(i+1,j,0))>0 ) couls(i,j,0) = d1p2 * (1 - snormal)
                end do
                do i = 1,ip
                   if ( snormal*(coul(i,j,0)-coul(i-1,j,0))<0 ) couls(i,j,0) = d1p2 * (1 + snormal)
                end do
             end do

             ! compute H on kappa-stencil (Eqs. (14) or (16))
             h = 0
             do j = -1,1
                do i = im,ip
                   h(j,0) = h(j,0) + couls(i,j,0) * dx(i)
                end do
             end do
             
             ! useless ?
             if (coul(0,0,0)>0 .and.coul(0,0,0)<1) then 
!!$                write(*,*) "NOEUD", ii,jj, dir
!!$                
!!$                write(*,*) "coul"
!!$                do i = 3,-3,-1
!!$                   write(*,*) (coul(i,j,0),j=-1,1)
!!$                end do
!!$                write(*,*)
!!$                write(*,*) "couls, couls(0,0)=",couls(0,0,0)
!!$                do i = 3,-3,-1
!!$                   write(*,*) (couls(i,j,0),j=-1,1)
!!$                end do
!!$                write(*,*)
!!$                
!!$                write(*,*) "couls sur im ip",im,ip
!!$                do i = ip,im,-1
!!$                   write(*,*) (couls(i,j,0),j=-1,1)
!!$                end do
!!$                
!!$                write(*,*)
!!$                write(*,*) "H"
!!$                write(*,*) (h(j,0),j=-1,1)
!!$
!!$                write(*,*) "face haut=", (-im+1)*dx(1)
!!$                write(*,*) "face bas =", (-im)*dx(1)

!!$                if (h(0,0)<(-im  )*dx(1)) cycle
!!$                if (h(0,0)>(-im+1)*dx(1)) cycle
             else
                cycle
             end if

             !stop

             ! compute kappa (Eq. (15))
             ! irregular mesh 
             den = ((y(1)-y(0))**2*(y(0)-y(-1))+(y(1)-y(0))*(y(0)-y(-1))**2)
             hy  = ( (y(0)-y(-1))**2*(h(1,0)-h(0,0)) - (y(1)-y(0))**2*(h(-1,0)-h(0,0)) ) / den
             hyy = 2*( h(1,0)*(y(0)-y(-1)) - h(0,0)*(y(1)-y(-1)) + h(-1,0)*(y(1)-y(0)) ) / den
             
             kappa(ii,jj,1)      = hyy / ( 1 + hy**2 )**d3p2
             kappa_flag(ii,jj,1) = FLAG_FOUND
!!$             write(*,*) "kappa", kappa(ii,jj,1)
!!$
!!$
!!$             write(*,*)
!!$             write(*,*)
             

          end do
       end do
       
    else 
       
       do kk = mesh%sz,mesh%ez 
          do jj = mesh%sy,mesh%ey
             do ii = mesh%sx,mesh%ex
                
                ! at interface
                dcou = 0
                coul = 0

                dcou(1) = sum(cou(ii+1,jj-1:jj+1,kk-1:kk+1)) - sum(cou(ii-1,jj-1:jj+1,kk-1:kk+1))
                dcou(2) = sum(cou(ii-1:ii+1,jj+1,kk-1:kk+1)) - sum(cou(ii-1:ii+1,jj-1,kk-1:kk+1))
                dcou(3) = sum(cou(ii-1:ii+1,jj-1:jj+1,kk+1)) - sum(cou(ii-1:ii+1,jj-1:jj+1,kk-1))

                if (sum(abs(dcou(1:mesh%dim)))<1d-13*mesh%dim) cycle
                
                ! main direction i, and local stencil
                dir  = 0
                dcou = abs(dcou)
                if ( dcou(1) >= dcou(2) .and. dcou(1) >= dcou(3) ) then
                   dir = 1
                   ! i,j,k
                   do k = -1,1 
                      do j = -1,1
                         do i = -3,3
                            coul(i,j,k) = cou(ii+i,jj+j,kk+k)
                         end do
                      end do
                   end do
                   x  = mesh%x(ii-3:ii+3)
                   y  = mesh%y(jj-1:jj+1)
                   z  = mesh%z(kk-1:kk+1)
                   dx = mesh%dx(ii-3:ii+3)
                else if ( dcou(2) >= dcou(1) .and. dcou(2) >= dcou(3) ) then
                   dir = 2
                   ! j,k,i
                   do k = -1,1 
                      do j = -1,1
                         do i = -3,3
                            coul(i,j,k) = cou(ii+k,jj+i,kk+j)
                         end do
                      end do
                   end do
                   x  = mesh%y(jj-3:jj+3)
                   y  = mesh%z(kk-1:kk+1)
                   z  = mesh%x(ii-1:ii+1)
                   dx = mesh%dy(jj-3:jj+3)
                else
                   dir = 3
                   ! k,i,j
                   do k = -1,1 
                      do j = -1,1
                         do i = -3,3
                            coul(i,j,k) = cou(ii+j,jj+k,kk+i)
                         end do
                      end do
                   end do
                   x  = mesh%z(kk-3:kk+3)
                   y  = mesh%x(ii-1:ii+1)
                   z  = mesh%y(jj-1:jj+1)
                   dx = mesh%dz(kk-3:kk+3)
                end if
                
                ! coul^star = coul
                couls = coul

                ! height does not lie in the cell
                if (dot_product(couls(:,0,0),dx(:)) < sum(dx(-3:-1))) then
                   kappa_flag(ii,jj,kk) = dir 
                   cycle
                end if
                if (dot_product(couls(:,0,0),dx(:)) > sum(dx(-3:0)))  then
                   kappa_flag(ii,jj,kk) = dir 
                   cycle
                end if
                
                ! normal
                normal = (coul(1,0,0)-coul(-1,0,0)) !/(x(1)-x(-1))
                
                ! sign normal
                snormal = 0
                if (normal > 0) then
                   snormal = +1
                elseif (normal < 0) then
                   snormal = -1
                else
                   cycle
                end if
                
                ! alpha_k
                alpha = 0
                do k = -1,1
                   do j = -1,1
                      do i = -3,3
                         alpha(i) = alpha(i) + coul(i,j,k)
                      end do
                   end do
                end do
                
                ! compute kp;km (t_up,t_low)
                ip = 0
                i  = 1
                do while (i<=3) 
                   if ( snormal*(alpha(i)-alpha(i-1))>0 .and. (alpha(i)/=0) .and. (alpha(i)/=9) ) then
                      ip = i
                      i  = i+1
                   else
                      exit
                   end if
                end do
                
                im = 0
                i  = -1
                do while (i>=-3) 
                   if ( snormal*(alpha(i+1)-alpha(i))>0 .and. (alpha(i)/=0) .and. (alpha(i)/=9) ) then
                      im = i
                      i  = i-1
                   else
                      exit
                   end if
                end do
                
                ! force monotonic variation
                do k = -1,1
                   do j = -1,1
                      do i = im,-1
                         if ( snormal*(coul(i,j,k)-coul(i+1,j,k))>0 ) couls(i,j,k) = d1p2 * (1 - snormal)
                      end do
                      do i = 1,ip
                         if ( snormal*(coul(i,j,k)-coul(i-1,j,k))<0 ) couls(i,j,k) = d1p2 * (1 + snormal)
                      end do
                   end do
                end do
                
                ! compute H on kappa-stencil (Eqs. (14) or (16))
                h = 0
                do k = -1,1
                   do j = -1,1
                      do i = im,ip
                         h(j,k) = h(j,k) + couls(i,j,k) * dx(i)
                      end do
                   end do
                end do
                
                ! useless ?
                if (coul(0,0,0)>0 .and.coul(0,0,0)<1) then 
                else
                   cycle
                end if
                
                ! compute kappa (Eq. (15))
                ! irregular mesh 
                den = ((y(1)-y(0))**2*(y(0)-y(-1))+(y(1)-y(0))*(y(0)-y(-1))**2)
                hy  = ( (y(0)-y(-1))**2*(h(1,0)-h(0,0)) - (y(1)-y(0))**2*(h(-1,0)-h(0,0)) ) / den
                hyy = 2*( h(1,0)*(y(0)-y(-1)) - h(0,0)*(y(1)-y(-1)) + h(-1,0)*(y(1)-y(0)) ) / den

                hy0 = ( (y(0)-y(-1))**2*(h(1,-1)-h(0,-1)) - (y(1)-y(0))**2*(h(-1,-1)-h(0,-1)) ) / den
                hy1 = hy
                hy2 = ( (y(0)-y(-1))**2*(h(1,1)-h(0,1)) - (y(1)-y(0))**2*(h(-1,1)-h(0,1)) ) / den

                den = ((z(1)-z(0))**2*(z(0)-z(-1))+(z(1)-z(0))*(z(0)-z(-1))**2)
                hz  = ( (z(0)-z(-1))**2*(h(0,1)-h(0,0)) - (z(1)-z(0))**2*(h(0,-1)-h(0,0)) ) / den
                hzz = 2*( h(0,1)*(z(0)-z(-1)) - h(0,0)*(z(1)-z(-1)) + h(0,-1)*(z(1)-z(0)) ) / den

                hyz = ( (z(0)-z(-1))**2*(hy2-hy1) - (z(1)-z(0))**2*(hy0-hy1) ) / den

                kappa(ii,jj,kk)      = ( hyy + hzz + hyy*hz**2 + hzz*hy**2 - 2*hyz*hy*hz ) / ( 1 + hy**2 + hz**2 )**d3p2
                kappa_flag(ii,jj,kk) = FLAG_FOUND
             
             end do
          end do
       end do

    end if


    kappa_mix = kappa_flag ! conversion int -> real
    
    call comm_mpi_sca(kappa_mix)
    call comm_mpi_sca(kappa)

    kappa_flag = nint(kappa_mix) ! conversion real --> nearest int
    kappa_mix  = 0

    ! fill cases
    do kk = mesh%sz,mesh%ez 
       do jj = mesh%sy,mesh%ey
          do ii = mesh%sx,mesh%ex

             if (kappa_flag(ii,jj,kk)==FLAG_PICK_X) then

                if (kappa_flag(ii-1,jj,kk)==FLAG_FOUND) kappa_mix(ii,jj,kk) = kappa(ii-1,jj,kk)
                if (kappa_flag(ii+1,jj,kk)==FLAG_FOUND) kappa_mix(ii,jj,kk) = kappa(ii+1,jj,kk)

             elseif (kappa_flag(ii,jj,kk)==FLAG_PICK_Y) then

                if (kappa_flag(ii,jj-1,kk)==FLAG_FOUND) kappa_mix(ii,jj,kk) = kappa(ii,jj-1,kk)
                if (kappa_flag(ii,jj+1,kk)==FLAG_FOUND) kappa_mix(ii,jj,kk) = kappa(ii,jj+1,kk)

             elseif (kappa_flag(ii,jj,kk)==FLAG_PICK_Z) then

                if (kappa_flag(ii,jj,kk-1)==FLAG_FOUND) kappa_mix(ii,jj,kk) = kappa(ii,jj,kk-1)
                if (kappa_flag(ii,jj,kk+1)==FLAG_FOUND) kappa_mix(ii,jj,kk) = kappa(ii,jj,kk+1)

             end if

          end do
       end do
    end do

    kappa = kappa + kappa_mix

#else 
    
    ! init cou article Hernandez et al.

    dx = 1
    dy = 1
    dz = 1

    coul = 0

    ! simple interface, kappa = -0.14310835269246713
    coul (:,:,-3:0) = 1
    coul(-1,:,-1)   = 0.9d0
    coul(-1,:,0)    = 0.4d0
    coul(0,:,0)     = 0.8d0
    coul(1,:,0)     = 1
    coul(-1,:,1)    = 0
    coul(0,:,1)     = 0.1
    coul(1,:,1)     = 0.3d0
    
!!$    ! test shift
!!$    coul(:,:,-3:2) = coul(:,:,-2:3)
!!$    coul(:,:,3)    = 0

!!$    ! test inverse phases    
!!$    coul = 1-coul
    
!!$    ! double interface, kappa = -0.14310835269246713
!!$    coul(0,:,-3)    = 0.7d0
!!$    coul(1,:,-3)    = 0.2d0
!!$    coul(1,:,-2)    = 0.95d0

    ! coul^star = coul
    couls = coul

    ! normal 
    nx = coul(1,0,0) - coul(-1,0,0)
    ny = coul(0,1,0) - coul(0,-1,0)
    nz = coul(0,0,1) - coul(0,0,-1)

    ! sign normal
    snz = 0
    if (nz > 0) then
       snz = +1
    elseif (nz < 0) then
       snz = -1
    end if
    
    write(*,*) "snz", snz, nz

    ! reduce stencil

    ! alpha_k

    alpha = 0
    do k = -3,3
       do j = -1,1
          do i = -1,1
             alpha(k) = alpha(k) + coul(i,j,k)
          end do
       end do
    end do

    write(*,*) "alpha", alpha/3

    ! compute kp;km (t_up,t_low)

    kp = 0
    k  = 1
    do while (k<=3) 
       if ( snz*(alpha(k)-alpha(k-1))>0 .and. (alpha(k)/=0) .and. (alpha(k)/=3**2) ) then
          kp = k
          k  = k+1
       else
          exit
       end if
    end do

    km = 0
    k  = -1
    do while (k>=-3) 
       if ( snz*(alpha(k+1)-alpha(k))>0 .and. (alpha(k)/=0) .and. (alpha(k)/=3**2) ) then
          km = k
          k  = k-1
       else
          exit
       end if
    end do
    
    write(*,*) "kp,km",kp,km
    
    ! force monotonic variation
    do k = km,-1
       do j = -1,1
          do i = -1,1
             if ( snz*(coul(i,j,k)-coul(i,j,k+1))>0 ) couls(i,j,k) = d1p2 * (1 - snz)
          end do
       end do
    end do
    do k = 1,kp
       do j = -1,1
          do i = -1,1
             if ( snz*(coul(i,j,k)-coul(i,j,k-1))<0 ) couls(i,j,k) = d1p2 * (1 + snz)
          end do
       end do
    end do
    
    ! compute H on kappa-stencil (Eq. (14))
    h = 0
    do k = km,kp
       do j = -1,1
          do i = -1,1
             h(i,j) = h(i,j) + couls(i,j,k) * dz(1)
          end do
       end do
    end do

!!$    ! compute kappa (Eq. (15))
!!$    hyy = ( h(1,0) - 2*h(0,0) + h(-1,0) ) / dy(1)**2
!!$    hy  = ( h(1,0) - h(-1,0) ) / 2 / dy(1)
!!$    hzz = ( h(0,1) - 2*h(0,0) + h(0,-1) ) / dz(1)**2
!!$    hz  = ( h(0,1) - h(0,-1) ) / 2 / dz(1)
!!$    hyz = ( ( h(1,1) - h(1,-1) ) - ( h(-1,1) - h(-1,-1) ) ) / 4 / dy(1) / dz(1)

    ! irregular mesh
    y = (/ 1, 2, 3/)
    z = (/ 1, 2, 3/)

    den = ((y(1)-y(0))**2*(y(0)-y(-1))+(y(1)-y(0))*(y(0)-y(-1))**2)
    hyy = 2*( h(1,0)*(y(0)-y(-1)) - h(0,0)*(y(1)-y(-1)) + h(-1,0)*(y(1)-y(0)) ) / den
    hy  = ( (y(0)-y(-1))**2*(h(1,0)-h(0,0)) - (y(1)-y(0))**2*(h(-1,0)-h(0,0)) ) / den

    hy0 = ( (y(0)-y(-1))**2*(h(1,-1)-h(0,-1)) - (y(1)-y(0))**2*(h(-1,-1)-h(0,-1)) ) / den
    hy1 = hy
    hy2 = ( (y(0)-y(-1))**2*(h(1,1)-h(0,1)) - (y(1)-y(0))**2*(h(-1,1)-h(0,1)) ) / den

    den = ((z(1)-z(0))**2*(z(0)-z(-1))+(z(1)-z(0))*(z(0)-z(-1))**2)
    hzz = 2*( h(0,1)*(z(0)-z(-1)) - h(0,0)*(z(1)-z(-1)) + h(0,-1)*(z(1)-z(0)) ) / den
    hz  = ( (z(0)-z(-1))**2*(h(0,1)-h(0,0)) - (z(1)-z(0))**2*(h(0,-1)-h(0,0)) ) / den

    hyz = ( (z(0)-z(-1))**2*(hy2-hy1) - (z(1)-z(0))**2*(hy0-hy1) ) / den


    kap = ( hyy + hzz + hyy*hz**2 + hzz*hy**2 - 2*hyz*hy*hz ) / ( 1 + hy**2 + hz**2 )**d3p2


    write(*,*) "kappa", kap

#endif
    
    call compute_time(TIMER_END,"[sub] HF_compute_kappa")

  end subroutine HF_compute_kappa
  
end module mod_height_function
