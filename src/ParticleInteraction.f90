!========================================================================
!
!                   FUGU Version 1.0.0
!
!========================================================================
!
! Copyright  Stéphane Vincent
!
! stephane.vincent@u-pem.fr          straightparadigm@gmail.com
!
! This file is part of the FUGU Fortfran library, a set of Computational 
! Fluid Dynamics subroutines devoted to the simulation of multi-scale
! multi-phase flows for resolved scale interfaces (liquid/gas/solid). 
! FUGU uses the initial developments of DyJeAT and SHALA codes from 
! Jean-Luc Estivalezes (jean-luc.estivalezes@onera.fr) and 
! Frédéric Couderc (couderc@math.univ-toulouse.fr).
!
!========================================================================
!**
!**   NAME       : Collision.f90
!**
!**   AUTHOR     : Stéphane Vincent
!**
!**   FUNCTION   : modules for Lagrangian particle tracking of FUGU software
!**
!**   DATES      : Version 1.0.0  : from : october, 16, 2016
!**
!========================================================================

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
!                  modules for Lagrangian particle tracking
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!===============================================================================
module mod_ParticleInteraction
  use mod_Parameters
  use mod_struct_Particle_Tracking
  use mod_ParticleConnectivity

contains

  subroutine spring_dashpot_collision(LPart,nPart,dt)
    !*******************************************************************************!
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)                                      :: nPart
    real(8), intent(in)                                      :: dt
    type(particle_t), allocatable, dimension(:), intent(inout) :: LPart
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    integer                                                  :: Ngbr,i,j1,j2
    integer                                                  :: Nc=8
    integer, allocatable, dimension(:,:), save               :: cpt
    real(8)                                                  :: sig,dr2,dv2,delta,un
    real(8)                                                  :: me,im1,im2,kn,betan,ed,lned
    real(8)                                                  :: time
    real(8), dimension(3)                                    :: dr,dv,force
    logical, save                                            :: once=.true.
    !-------------------------------------------------------------------------------


    if (once) then
       allocate(cpt(nPart,nPart))
       cpt=0
       once=.false.
    end if

    do j1=1,nPart-1
       do j2=j1+1,nPart
          
          sig=LPart(j1)%radius(1)+LPart(j2)%radius(1)

          dr=LPart(j2)%r-LPart(j1)%r
          do i=1,dim
             if (periodic(i)) call vec_periodicity(dr,i,xyzBounds)
          end do
          dv=LPart(j2)%u-LPart(j1)%u

          dr2=dot_product(dr,dr)
          dv2=dot_product(dv,dv)

          delta=sqrt(dr2)-sig
          un=sqrt(dv2)

          if (delta<0) then
             cpt(j1,j2)=Nc
          end if

          if (cpt(j1,j2)>0) then

             cpt(j1,j2)=cpt(j1,j2)-1
             
             im1=1/LPart(j1)%m
             im2=1/LPart(j2)%m
             me=1/(im1+im2)
             
             ed=1
             lned=log(ed)
             
             time=Nc*dt
             
             kn=me*(pi2+lned**2)/time**2
             betan=2*me*lned/time
             
             !force=-kn*dr+betan*dv
             force=-kn*delta*dr/sqrt(dr2)
             
             LPart(j1)%a=LPart(j1)%a+force/LPart(j1)%m
             LPart(j2)%a=LPart(j2)%a-force/LPart(j2)%m
             
          end if
          
       end do
    end do
    
  end subroutine spring_dashpot_collision
  !******************************************************************************

  subroutine hard_sphere_collision(NgbrList,nNgbr,dt,LPart,nPart,code)
    !*******************************************************************************!
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)                                      :: nPart,code,nNgbr
    integer, dimension(:), intent(in)                        :: NgbrList
    real(8), intent(inout)                                   :: dt
    type(particle_t), allocatable, dimension(:), intent(inout) :: LPart
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    integer                                                  :: Ngbr,i,j1,j2
    integer, save                                            :: i1,i2
    real(8), parameter                                       :: epsc = 1e-20_8
    real(8)                                                  :: d,sig,sig2,dr2,dv2,dvdr
    real(8), dimension(3)                                    :: dr,dv,vecJ
    real(8), dimension(3)                                    :: vfj1,vfj2
    logical, save                                            :: collision=.false.
    !-------------------------------------------------------------------------------
    
    if (code==1) then
       
       collision=.false.
       
       do Ngbr=1,nNgbr
          j1=NgbrList(2*(Ngbr-1)+1)
          j2=NgbrList(2*(Ngbr-1)+2)
          
          sig=LPart(j1)%radius(1)+LPart(j2)%radius(1)
          sig2=sig*sig
          
          dr=LPart(j2)%r-LPart(j1)%r
          do i=1,dim
             if (periodic(i)) call vec_periodicity(dr,i,xyzBounds)
          end do
          
          dv=LPart(j2)%u-LPart(j1)%u
          
          dr2=dot_product(dr,dr)
          dv2=dot_product(dv,dv)
          dvdr=dot_product(dv,dr)
          
          d=dvdr**2-dv2*(dr2-sig2)
          
          if (dvdr<0 .and. d>=0) then
             if (-(dvdr+sqrt(d))/dv2<dt) then
                dt=-(dvdr+sqrt(d))/dv2
                i1=j1
                i2=j2
                collision=.true.
             end if
          end if
          
       end do
       
    else 
       
       if (collision) then

          j1=i1
          j2=i2
          
          vfj1=LPart(j1)%u-LPart(j1)%v
          vfj2=LPart(j2)%u-LPart(j2)%v
          
          sig=LPart(j1)%radius(1)+LPart(j2)%radius(1)
          sig2=sig*sig
          
          dr=LPart(j2)%r-LPart(j1)%r
          do i=1,dim
             if (periodic(i)) call vec_periodicity(dr,i,xyzBounds)
          end do
          
          dv=LPart(j2)%u-LPart(j1)%u
          
          dr2=dot_product(dr,dr)
          dv2=dot_product(dv,dv)
          dvdr=dot_product(dv,dr)
          
          d=dvdr**2-dv2*(dr2-sig2)
          
          vecJ=2*LPart(j1)%m*LPart(j2)%m*dvdr/sig/(LPart(j1)%m+LPart(j2)%m)
          
          LPart(j1)%u=LPart(j1)%u+vecJ*dr/sig/LPart(j1)%m 
          LPart(j2)%u=LPart(j2)%u-vecJ*dr/sig/LPart(j2)%m
          
          LPart(j1)%v=LPart(j1)%u-vfj1
          LPart(j2)%v=LPart(j2)%u-vfj2
          
       end if
    end if

  end subroutine hard_sphere_collision

  subroutine Lennard_Jones_collision(NgbrList,nNgbr,LPart,nPart)

    !-------------------------------------------------------------
    ! Global variables 
    !-------------------------------------------------------------
    integer, intent(in)                                      :: nPart,nNgbr
    integer, dimension(:), intent(in)                        :: NgbrList
    !-------------------------------------------------------------
    type(particle_t), allocatable, dimension(:), intent(inout) :: LPart
    !-------------------------------------------------------------
    ! Local variables 
    !-------------------------------------------------------------
    integer                                                  :: Ngbr,i,j1,j2
    real(8), dimension(3)                                    :: dr 
    real(8)                                                  :: sig,sig2,eps,isig2
    real(8)                                                  :: rCut2,r2,ir2,ir23
    real(8)                                                  :: cst,fVal
    
    !-------------------------------------------------------------
    ! initialization
    !-------------------------------------------------------------
    sig=10*LPart(1)%radius(1)
    sig2=sig*sig
    isig2=1/sig2
    eps=sig2
    cst=48*eps*isig2
    !-------------------------------------------------------------

    !-------------------------------------------------------------
    ! soft sphere potential
    !-------------------------------------------------------------
    rCut2=(2**(d1p6)*sig)**2
    !-------------------------------------------------------------

    !---------------------------------------------
    ! loop over all neighbors of the neighbor list
    !---------------------------------------------
    do Ngbr=1,nNgbr
       j1=NgbrList(2*(Ngbr-1)+1)
       j2=NgbrList(2*(Ngbr-1)+2)
       !-----------------------------------------------------
       ! distance & periodicity
       !-----------------------------------------------------
       dr=LPart(j1)%r-LPart(j2)%r
       do i=1,dim
          if (periodic(i)) call vec_periodicity(dr,i,xyzBounds)
       end do
       !-----------------------------------------------------
       
       !-----------------------------------------------------
       ! force if distance < rCut
       !-----------------------------------------------------
       r2=sum(dr(1:dim)**2)
       if (r2<=rCut2) then
          ir2=sig2/r2
          ir23=ir2**3
          fVal=cst*ir23*(ir23-demi)*ir2
          LPart(j1)%a=LPart(j1)%a+fVal*dr
          LPart(j2)%a=LPart(j2)%a-fVal*dr
       end if
       !-----------------------------------------------------
    end do
    
  end subroutine Lennard_Jones_collision

  !******************************************************************************


  
  
  
  !===============================================================================
end module mod_ParticleInteraction
!===============================================================================
