!===============================================================================
module Bib_VOFLag_RespectAlloc
   !===============================================================================
   contains
   !-----------------------------------------------------------------------------  
   !> @author amine chadil
   ! 
   !> @brief cette routine alloue les variables respect
   !
   !> @param[in]   cou   :  
   !-----------------------------------------------------------------------------
   subroutine RespectAlloc(Ru0,Rv0,Rw0,cou,cou_vis,cou_vts,couin0,cou_visin,cou_vtsin,&
                           rovu,rovv,rovw,vie,vir,vis,xit,slvu,slvv,slvw,smvu,smvv,   &
                           smvw,slvuin,slvvin,slvwin,smvuin,smvvin,smvwin,diff_pre,   &
                           diff_u_mean,diff_v_mean,diff_w_mean)
      !===============================================================================
      !modules
      !===============================================================================
      !-------------------------------------------------------------------------------
      !Modules de definition des structures et variables => src/mod/module_...f90
      !-------------------------------------------------------------------------------
      use Module_VOFLag_ParticlesDataStructure
      use Module_VOFLag_Flows
      use mod_Parameters,                      only : deeptracking,dim,sx,ex,sy,ey,sz,&
                                                      ez,gx,gy,gz,sxu,exu,sxv,exv,sxw,&
                                                      exw
      use mod_mpi
      !-------------------------------------------------------------------------------

      !===============================================================================
      !declarations des variables
      !===============================================================================
      implicit none
      !-------------------------------------------------------------------------------
      !variables globales
      !-------------------------------------------------------------------------------
      real(8), dimension(:,:,:,:), allocatable, intent(inout) :: vir,vis,cou_vis,cou_vts,&
                                                                 cou_visin,cou_vtsin
      real(8), dimension(:,:,:)  , allocatable, intent(inout) :: smvu,smvv,smvw,&
                                                                 slvu,slvv,slvw
      real(8), dimension(:,:,:),   allocatable, intent(inout) :: slvuin,slvvin,slvwin,&
                                                                 smvuin,smvvin,smvwin
      real(8), dimension(:,:,:),   allocatable, intent(inout) :: cou,couin0,rovu,rovv,   &
                                                                 diff_pre,rovw,vie,xit,  &
                                                                 diff_w_mean,diff_u_mean,&
                                                                 diff_v_mean,Ru0,Rv0,Rw0
      !-------------------------------------------------------------------------------
      !variables locales 
      !-------------------------------------------------------------------------------
      !-------------------------------------------------------------------------------

      !-------------------------------------------------------------------------------
      if (deeptracking) write(*,*) 'entree RespectAlloc'
      !-------------------------------------------------------------------------------

      !-------------------------------------------------------------------------------
      !initialisation
      !-------------------------------------------------------------------------------
      if (.not.allocated(cou)) allocate(cou(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))

      if (.not.allocated(cou_vis)) allocate(cou_vis(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz,1:3))

      if (.not.allocated(cou_vts)) allocate(cou_vts(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz,1:3))
      
      if (.not.allocated(vie)) allocate(vie(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
      
      if (.not.allocated(vis)) allocate(vis(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz,dim**(dim-1)))
      
      if (.not.allocated(vir)) allocate(vir(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz,dim**(dim-1)))
       
      if (.not.allocated(xit)) allocate(xit(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
      
      if (.not.allocated(rovu)) allocate(rovu(sxu-gx:exu+gx,syu-gy:eyu+gy,szu-gz:ezu+gz))

      if (.not.allocated(Ru0)) allocate(Ru0(sxu-gx:exu+gx,syu-gy:eyu+gy,szu-gz:ezu+gz))

      if (.not.allocated(Rv0)) allocate(Rv0(sxv-gx:exv+gx,syv-gy:eyv+gy,szv-gz:ezv+gz))

      if (.not.allocated(Rw0)) allocate(Rw0(sxw-gx:exw+gx,syw-gy:eyw+gy,szw-gz:ezw+gz))
      
      ! if (allocated(rovu0)) deallocate(rovu0) 
      ! allocate(rovu0(sxu-gx:exu+gx,syu-gy:eyu+gy,szu-gz:ezu+gz))

      if (.not.allocated(rovv)) allocate(rovv(sxv-gx:exv+gx,syv-gy:eyv+gy,szv-gz:ezv+gz))
      
      ! if (allocated(rovv0)) deallocate(rovv0) 
      ! allocate(rovv0(sxv-gx:exv+gx,syv-gy:eyv+gy,szv-gz:ezv+gz))
      if (dim==3) then 
         if (.not.allocated(rovw)) allocate(rovw(sxw-gx:exw+gx,syw-gy:eyw+gy,szw-gz:ezw+gz))
         
         ! if (allocated(rovw0)) deallocate(rovw0) 
         ! allocate(rovw0(sxw-gx:exw+gx,syw-gy:eyw+gy,szw-gz:ezw+gz))
      end if
      if (uniform_past_part) then 
         if (allocated(diff_pre))    deallocate(diff_pre) 
         if (allocated(diff_u_mean)) deallocate(diff_u_mean) 
         if (allocated(diff_v_mean)) deallocate(diff_v_mean) 
         allocate(diff_pre   (sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
         allocate(diff_u_mean(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
         allocate(diff_v_mean(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
         if (dim==3) then
            if (allocated(diff_w_mean))    deallocate(diff_w_mean)
            allocate(diff_w_mean(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
         end if 
      end if

      if (Wall_Creaction) then
         if (allocated(smvu)) deallocate(smvu) 
         if (allocated(smvv)) deallocate(smvv) 
         if (allocated(smvw)) deallocate(smvw) 
         if (allocated(slvu)) deallocate(slvu) 
         if (allocated(slvv)) deallocate(slvv) 
         if (allocated(slvw)) deallocate(slvw)
         allocate(smvu(sxu-gx:exu+gx,syu-gy:eyu+gy,szu-gz:ezu+gz))
         allocate(smvv(sxv-gx:exv+gx,syv-gy:eyv+gy,szv-gz:ezv+gz))
         if (dim==3) allocate(smvw(sxw-gx:exw+gx,syw-gy:eyw+gy,szw-gz:ezw+gz))
         allocate(slvu(sxu-gx:exu+gx,syu-gy:eyu+gy,szu-gz:ezu+gz))
         allocate(slvv(sxv-gx:exv+gx,syv-gy:eyv+gy,szv-gz:ezv+gz))
         if (dim==3) allocate(slvw(sxw-gx:exw+gx,syw-gy:eyw+gy,szw-gz:ezw+gz))
         if (allocated(smvuin)) deallocate(smvuin) 
         if (allocated(smvvin)) deallocate(smvvin) 
         if (allocated(smvwin)) deallocate(smvwin) 
         if (allocated(slvuin)) deallocate(slvuin) 
         if (allocated(slvvin)) deallocate(slvvin) 
         if (allocated(slvwin)) deallocate(slvwin)
         allocate(smvuin(sxu-gx:exu+gx,syu-gy:eyu+gy,szu-gz:ezu+gz))
         allocate(smvvin(sxv-gx:exv+gx,syv-gy:eyv+gy,szv-gz:ezv+gz))
         if (dim==3) allocate(smvwin(sxw-gx:exw+gx,syw-gy:eyw+gy,szw-gz:ezw+gz))
         allocate(slvuin(sxu-gx:exu+gx,syu-gy:eyu+gy,szu-gz:ezu+gz))
         allocate(slvvin(sxv-gx:exv+gx,syv-gy:eyv+gy,szv-gz:ezv+gz))
         if (dim==3) allocate(slvwin(sxw-gx:exw+gx,syw-gy:eyw+gy,szw-gz:ezw+gz))

         if (allocated(couin0)) deallocate(couin0) 
         allocate(couin0(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))

         if (allocated(cou_visin)) deallocate(cou_visin) 
         allocate(cou_visin(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz,1:3))

         if (allocated(cou_vtsin)) deallocate(cou_visin) 
         allocate(cou_vtsin(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz,1:3))
     end if
      !-------------------------------------------------------------------------------
      if (deeptracking) write(*,*) 'sortie RespectAlloc'
      !-------------------------------------------------------------------------------
   end subroutine RespectAlloc

   !===============================================================================
end module Bib_VOFLag_RespectAlloc
!===============================================================================