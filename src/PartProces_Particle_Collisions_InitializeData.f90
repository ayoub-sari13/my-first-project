!===============================================================================
module Bib_VOFLag_Particle_Collisions_InitializeData
  !===============================================================================
  contains
  !-----------------------------------------------------------------------------  
  !> @author amine chadil
  ! 
  !> @brief cette routine remplie la structure vl%par avec les caracteristiques des
  !! particules lues dans le fichier "data.in". 
  !
  !> @param[out] vl       : la structure contenant toutes les informations
  !! relatives aux particules.
  !-----------------------------------------------------------------------------
  subroutine Particle_Collisions_InitializeData(vl)  
    !===============================================================================
    !modules
    !===============================================================================
    !-------------------------------------------------------------------------------
    !Modules de definition des structures et variables => src/mod/module_...f90
    !-------------------------------------------------------------------------------
    use Module_VOFLag_ParticlesDataStructure
    use mod_Parameters,                       only : deeptracking
    !-------------------------------------------------------------------------------
    !Modules de subroutines de la librairie RESPECT => src/Bib_VOFLag_...f90
    !-------------------------------------------------------------------------------
    use Bib_VOFLag_Sphere_Collisions_InitializeData
    !-------------------------------------------------------------------------------
    !===============================================================================
    !declarations des variables
    !===============================================================================
    implicit none
    !-------------------------------------------------------------------------------
    !variables globales
    !-------------------------------------------------------------------------------
    type(struct_vof_lag), intent(inout)  :: vl
    !-------------------------------------------------------------------------------
    !variables locales 
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'entree Particle_Collisions_InitializeData'
    !-------------------------------------------------------------------------------

    select case(vl%nature)
    
    case(1)
      !-------------------------------------------------------------------------------
      ! Si les particules sont spherique
      !-------------------------------------------------------------------------------
      call Sphere_Collisions_InitializeData(vl)

    case default
      write(*,*) "STOP : sourcevl n est pas ecrite pour les particules non spherique"
      stop

    end select

    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'sortie Particle_Collisions_InitializeData'
    !-------------------------------------------------------------------------------
  end subroutine Particle_Collisions_InitializeData

  !===============================================================================
end module Bib_VOFLag_Particle_Collisions_InitializeData
!===============================================================================
  


