!===============================================================================
module Bib_VOFLag_Valid_UniformStokesFlowPastAParticle_Cylinder
  !===============================================================================
  contains
  !-----------------------------------------------------------------------------  
  !> @author  amine chadil
  ! 
  !> @brief calcul des champs erreur sur la pression et vitesse du fluide pour un ecoulement
  !! uniforme autour d'un cylindre dans le regime de stokes
  !
  !> @param vts             : velocity field
  !> @param pre             : pressure
  !> @param rayon_cylinder  : the cylinder's radii
  !> @param centre_cylinder : the cylinder's center
  !-----------------------------------------------------------------------------
  subroutine Valid_UniformStokesFlowPastAParticle_Cylinder(u,v,Ru0,Rv0,pres,diff_pre,diff_u_mean,&
                                                           diff_v_mean,centre_cylinder_bis,  &
                                                           rayon_cylinder_bis,iteration_temps)
    !===============================================================================
    !modules
    !===============================================================================
    !-------------------------------------------------------------------------------
    !Modules de definition des structures et variables => src/mod/module_...f90
    !-------------------------------------------------------------------------------
    use mod_struct_thermophysics, only : fluids
    use Module_VOFLag_Flows
    use mod_Parameters, only : deeptracking,sx,ex,sy,ey,gy,sxu,exu,syu,eyu,sxv,exv, &
                               syv,eyv,grid_xu,grid_yv,grid_x,grid_y,dx,dxu,dy,dyv, &
                               rank
    use mod_mpi
    !-------------------------------------------------------------------------------
    !Modules de subroutines de la librairie VOF-Lag
    !-------------------------------------------------------------------------------
    use Bib_VOFLag_k0
    use Bib_VOFLag_k1
    !-------------------------------------------------------------------------------
    
    !===============================================================================
    !declarations des variables
    !===============================================================================
    implicit none
    !-------------------------------------------------------------------------------
    !variables globales
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable, intent(inout) :: diff_pre,diff_u_mean,diff_v_mean
    real(8), dimension(:,:,:), allocatable, intent(in)    :: u,v,Ru0,Rv0,pres
    real(8), dimension(3)                 , intent(in)    :: centre_cylinder_bis
    real(8)                               , intent(in)    :: rayon_cylinder_bis
    integer                               , intent(in)    :: iteration_temps
    !-------------------------------------------------------------------------------
    !variables locales 
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable :: pre_exact,u_exact_pre,v_exact_pre
    real(8), dimension(:,:,:), allocatable :: u_exact,v_exact,diff_u,diff_v,u_pre,v_pre
    real(8), dimension(2)                  :: r,x,z,cosphi,sinphi,vr,vphi
    real(8), dimension(3)                  :: centre_cylinder
    real(8)                                :: sigma,rayon_cylinder
    real(8)                                :: N_pre_exact,N_pre,N_v_exact_pre,N_v_pre,& 
                                              N_u_exact_pre,N_u_pre,k_0,k_1,tmpu     ,&
                                              N_u_exact_pre_loc,N_u_pre_loc,tmpv     ,&
                                              N_v_exact_pre_loc,N_v_pre_loc,tmpuext  ,&
                                              N_pre_exact_loc  ,N_pre_loc,tmpvext    ,&
                                              tmpuloc,tmpuextloc,tmpvextloc,tmpvloc   
    integer                                :: i,j,k,err
    !-------------------------------------------------------------------------------
    
    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'entree Valid_UniformStokesFlowPastAParticle_Cylinder'
    !-------------------------------------------------------------------------------
    
    !-------------------------------------------------------------------------------
    !initialisation
    !-------------------------------------------------------------------------------
    sigma=0.1D0 
    k_0=k0(sigma)
    k_1=k1(sigma)
    open(unit=1001,file="particles.in",status="old",action="read",iostat=err)
    read(1001,*)  centre_cylinder(1),centre_cylinder(2),&
                     rayon_cylinder
    close(1001)
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    !allocation des variables locales
    !-------------------------------------------------------------------------------
    allocate(pre_exact  (sx-gx:ex+gx,sy-gy:ey+gy,1))
    allocate(u_exact(sxu-gx:exu+gx,syu-gy:eyu+gy,1))
    allocate(v_exact(sxv-gx:exv+gx,syv-gy:eyv+gy,1))
    allocate(diff_u (sxu-gx:exu+gx,syu-gy:eyu+gy,1))
    allocate(diff_v (sxv-gx:exv+gx,syv-gy:eyv+gy,1))
    allocate(u_exact_pre(sx-gx:ex+gx,sy-gy:ey+gy,1))
    allocate(v_exact_pre(sx-gx:ex+gx,sy-gy:ey+gy,1))
    allocate(u_pre(sx-gx:ex+gx,sy-gy:ey+gy,1))
    allocate(v_pre(sx-gx:ex+gx,sy-gy:ey+gy,1))
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------
    ! calcul des pression ,des vitesses exactes et des erreurs sur les pression  au points de pression
    !-------------------------------------------------------------------------------
    pre_exact=0d0;diff_pre =0d0
    do j=sy,ey
     do i=sx,ex
      select case (direction)
          case(1)
            x(1) = grid_x(i)-centre_cylinder(1)
            z(1) = grid_y(j)-centre_cylinder(2)
          case(2)
            x(1) = grid_y(j)-centre_cylinder(2)
            z(1) = grid_x(i)-centre_cylinder(1)
        end select
      r(1) = sqrt(x(1)*x(1)+z(1)*z(1))
      if (r(1).gt.rayon_cylinder) then
         pre_exact(i,j,1) = -fluids(1)%mu*sigma**2*((1d0+(2d0*k_1)/(sigma*k_0))/r(1)+r(1))*x(1)/r(1)
      end if
      if (r(1).ge.rayon_cylinder) then
         if (abs(pre_exact(i,j,1)) .gt. 1D-14) then
            diff_pre(i,j,1) = abs(pre_exact(i,j,1)-pres(i,j,1))/abs(pre_exact(i,j,1))*100
         else
            diff_pre(i,j,1) =abs(pres(i,j,1))*100
         end if
      end if
     end do
    end do 
    N_pre_exact=0d0
    N_pre      =0d0
    do j=sy,ey
     do i=sx,ex
      N_pre_exact = N_pre_exact + pre_exact(i,j,1)**2*dx(i)*dy(j)
      N_pre       = N_pre       + (pre_exact(i,j,1)-pres(i,j,1))**2*dx(i)*dy(j)
     end do 
    end do 
    N_pre_exact_loc = N_pre_exact
    N_pre_loc       = N_pre
    call mpi_allreduce(N_pre_exact_loc,N_pre_exact,1,mpi_double_precision,mpi_sum,comm3d,code)
    call mpi_allreduce(N_pre_loc      ,N_pre      ,1,mpi_double_precision,mpi_sum,comm3d,code)

    !-------------------------------------------------------------------------------
    ! calcul des vitesses exactes et des erreurs sur les vitesses au points de vitesse
    !-------------------------------------------------------------------------------
    u_exact=0d0;diff_u=0d0
    v_exact=0d0;diff_v=0d0
    do j=syu,eyu
      do i=sxu,exu
         select case (direction)
           case(1)
              x(1)=grid_xu(i)-centre_cylinder(1)
              z(1)=grid_y (j)-centre_cylinder(2)
           case(2)
              x(1)=grid_y (j)-centre_cylinder(2)
              z(1)=grid_xu(i)-centre_cylinder(1)
         end select
         r(1)=sqrt(x(1)**2+z(1)**2)
         cosphi(1)=x(1)/r(1)
         sinphi(1)=z(1)/r(1)
         vr(1)    =(r(1)-(1d0+(2d0*k_1)/(sigma*k_0))/r(1)+ &
                   (2d0*k1(sigma*r(1)))/(sigma*k_0))*cosphi(1)/r(1)
         vphi(1)  =-(1d0+(1d0+(2d0*k_1)/(sigma*k_0))/r(1)**2 &
                   -(2d0/(k_0))*(k0(sigma*r(1))+(k1(sigma*r(1)))/(sigma*r(1))))*sinphi(1)
         if (r(1).gt.rayon_cylinder) then 
            select case (direction)
                case(1)
                    u_exact(i,j,1)=cosphi(1)*vr(1)-sinphi(1)*vphi(1)
                case(2)
                    u_exact(i,j,1)=sinphi(1)*vr(1)+cosphi(1)*vphi(1)
            end select  
        end if 
         if (abs(u_exact(i,j,1)) .gt. 1D-15) then
            diff_u(i,j,1) = abs(u_exact(i,j,1)-u(i,j,1))/abs(u_exact(i,j,1))*100d0
         else
            diff_u(i,j,1) = abs(u(i,j,1))
         end if   
      end do 
    end do 

    do j=syv,eyv
      do i=sxv,exv
         select case (direction)
           case(1)
              x(2)=grid_x (i)-centre_cylinder(1)
              z(2)=grid_yv(j)-centre_cylinder(2)
           case(2)
              x(2)=grid_yv(j)-centre_cylinder(2)
              z(2)=grid_x (i)-centre_cylinder(1)
         end select
         r(2)=sqrt(x(2)**2+z(2)**2)
         cosphi(2)=x(2)/r(2)
         sinphi(2)=z(2)/r(2)
         vr(2)    =(r(2)-(1d0+(2d0*k_1)/(sigma*k_0))/r(2)+ &
                   (2d0*k1(sigma*r(2)))/(sigma*k_0))*cosphi(2)/r(2)
         vphi(2)  =-(1d0+(1d0+(2d0*k_1)/(sigma*k_0))/r(2)**2 &
                   -(2d0/(k_0))*(k0(sigma*r(2))+(k1(sigma*r(2)))/(sigma*r(2))))*sinphi(2)
        if (r(2).gt.rayon_cylinder) then 
            select case (direction)
           case(1)
              v_exact(i,j,1)=sinphi(2)*vr(2)+cosphi(2)*vphi(2)
           case(2)
              v_exact(i,j,1)=cosphi(2)*vr(2)-sinphi(2)*vphi(2)
        end select
        end if 
         if (abs(v_exact(i,j,1)) .gt. 1D-15) then
            diff_v(i,j,1) = abs(v_exact(i,j,1)-v(i,j,1))/abs(v_exact(i,j,1))*100d0
         else
            diff_v(i,j,1) = abs(v(i,j,1))
         end if
      end do 
    end do 
    !-------------------------------------------------------------------------------
    ! calcul des erreurs sur les vitesses au points de pression
    !-------------------------------------------------------------------------------
    u_pre=0d0;u_exact_pre=0d0;diff_u_mean=0d0
    v_pre=0d0;v_exact_pre=0d0;diff_v_mean=0d0
    do j=sy,ey
     do i=sx,ex
         u_exact_pre(i,j,1) = (u_exact(i,j,1)+u_exact(i+1,j,1))*0.5d0
         u_pre      (i,j,1) = (u      (i,j,1)+u      (i+1,j,1))*0.5d0
         diff_u_mean(i,j,1) = (diff_u (i,j,1)+diff_u (i+1,j,1))*0.5d0

         v_exact_pre(i,j,1) = (v_exact(i,j,1)+v_exact(i,j+1,1))*0.5d0
         v_pre      (i,j,1) = (v      (i,j,1)+v      (i,j+1,1))*0.5d0
         diff_v_mean(i,j,1) = (diff_v (i,j,1)+diff_v (i,j+1,1))*0.5d0
      end do
    end do

    N_u_exact_pre=0d0;N_u_pre=0d0
    N_v_exact_pre=0d0;N_v_pre=0d0
    do j=sy,ey
      do i=sx,ex
       N_u_exact_pre =N_u_exact_pre+ u_exact_pre(i,j,1)**2*dxu(i)*dy(j)
       N_u_pre       =N_u_pre      +(u_exact_pre(i,j,1)-u_pre(i,j,1))**2*dxu(i)*dy(j)

       N_v_exact_pre=N_v_exact_pre+ v_exact_pre(i,j,1)**2*dx(i)*dyv(j)
       N_v_pre      =N_v_pre      +(v_exact_pre(i,j,1)-v_pre(i,j,1))**2*dx(i)*dyv(j)
      end do 
    end do 
    
    N_u_exact_pre_loc=N_u_exact_pre;N_u_pre_loc=N_u_pre
    N_v_exact_pre_loc=N_v_exact_pre;N_v_pre_loc=N_v_pre
    if ( nproc>1) then 
      call mpi_allreduce(N_u_exact_pre_loc,N_u_exact_pre,1,mpi_double_precision,mpi_sum,comm3d,code)
      call mpi_allreduce(N_u_pre_loc      ,N_u_pre      ,1,mpi_double_precision,mpi_sum,comm3d,code)
      call mpi_allreduce(N_v_exact_pre_loc,N_v_exact_pre,1,mpi_double_precision,mpi_sum,comm3d,code)
      call mpi_allreduce(N_v_pre_loc      ,N_v_pre      ,1,mpi_double_precision,mpi_sum,comm3d,code)
    end if 

    tmpu=0d0;tmpuext=0d0;tmpuloc=0d0;tmpuextloc=0d0
    tmpv=0d0;tmpvext=0d0;tmpvloc=0d0;tmpvextloc=0d0
    do j=syu,eyu
      do i=sxu,exu
         tmpu   =tmpu   +((u(i,j,1) - Ru0(i,j,1))**2)*(dxu(i)*dy(j))
         tmpuext=tmpuext+(u_exact(i,j,1)**2)*dxu(i)*dy(j)
      end do 
    end do 
    do j=syv,eyv
      do i=sxv,exv
         tmpv   =tmpv   +((v(i,j,1) - Rv0(i,j,1))**2)*(dx(i)*dyv(j))
         tmpvext=tmpvext+(v_exact(i,j,1)**2)*dx(i)*dyv(j)
      end do 
    end do 

    tmpuloc  =tmpu;tmpuextloc=tmpuext
    tmpvloc  =tmpv;tmpvextloc=tmpvext
    if ( nproc>1) then
      call mpi_allreduce(tmpuloc   ,tmpu   ,1,mpi_double_precision,mpi_sum,comm3d,code)
      call mpi_allreduce(tmpuextloc,tmpuext,1,mpi_double_precision,mpi_sum,comm3d,code)
      call mpi_allreduce(tmpvloc   ,tmpv   ,1,mpi_double_precision,mpi_sum,comm3d,code)
      call mpi_allreduce(tmpvextloc,tmpvext,1,mpi_double_precision,mpi_sum,comm3d,code)
    end if 

    if ( iteration_temps > 10 .and. sqrt(tmpv/tmpvext) < 1e-14) stop
    if ( iteration_temps > 10 .and. sqrt(tmpu/tmpuext) < 1e-14) stop
    !-------------------------------------------------------------------------------
    ! deallocation 
    !-------------------------------------------------------------------------------
    deallocate(pre_exact,u_exact_pre,v_exact_pre,u_exact,diff_u,v_exact,diff_v,u_pre,v_pre)
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------
    ! Ecriture des erreurs 
    !-------------------------------------------------------------------------------
    if (rank .eq. 0) then 
      open(unit=1000,file='valid_uniform_flow.dat',status='old',position='append')
      write(1000,'(8E16.8)',advance='no') dfloat(iteration_temps),sqrt(N_pre/N_pre_exact)*100 ,&
                                          sqrt(N_u_pre/N_u_exact_pre)*100,&
                                          sqrt(N_v_pre/N_v_exact_pre)*100,&
                                          sqrt(tmpu/tmpuext)             ,&
                                          sqrt(tmpv/tmpvext) 
      close(1000)
    end if
    !if (validation_uniform_flow_past_cylinder_analytique) then
    !  pre=pre_exact
    !  vts=vts_exact
    !endif
    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'entree Valid_UniformStokesFlowPastAParticle_Cylinder'
    !-------------------------------------------------------------------------------
  end subroutine Valid_UniformStokesFlowPastAParticle_Cylinder

  !===============================================================================
end module Bib_VOFLag_Valid_UniformStokesFlowPastAParticle_Cylinder
!===============================================================================
  


