!========================================================================
!**
!**   NAME       : Outputs.f90
!**
!**   AUTHOR     : Benoit Trouette
!**
!**   FUNCTION   : subroutines for files Outputs
!**
!**   DATES      : Version 1.0.0  : from : May, 2018
!**
!========================================================================

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
module mod_coeffvs
  use mod_mpi
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  implicit none
  
  !-------------------------------------------------------------------------------
  ! cell volume 
  !-------------------------------------------------------------------------------
  real(8), allocatable, dimension(:,:,:) :: coeffV
  real(8), allocatable, dimension(:,:,:) :: coeffS
  !-------------------------------------------------------------------------------

contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


  subroutine InitCoeffVS
    !-------------------------------------------------------------------------------
    ! Modules
    !-------------------------------------------------------------------------------
    use mod_Parameters, only: dim,periodic,nproc, &
         & sx,ex,sy,ey,sz,ez,gx,gy,gz,            &
         & nx,ny,nz,gsx,gex,gsy,gey,gsz,gez
    use mod_Constants, only: d1p2,d1p4,d1p8
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer :: i,j,k
    !-------------------------------------------------------------------------------

    if (.not.allocated(CoeffV)) then

       allocate(CoeffV(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
       allocate(CoeffS(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
       CoeffV=1
       CoeffS=1

       if (nproc==1) then

          do k=sz-gz,ez+gz
             do j=sy-gy,ey+gy
                do i=sx-gx,ex+gx
                   if (dim==2) then
                      ! faces x4
                      if (i<=gsx) CoeffV(i,j,k)=d1p2
                      if (i>=gex) CoeffV(i,j,k)=d1p2
                      if (j<=gsy) CoeffV(i,j,k)=d1p2
                      if (j>=gey) CoeffV(i,j,k)=d1p2
                      ! corners x4
                      if (i<=gsx.and.j<=gsy) CoeffV(i,j,k)=d1p4
                      if (i>=gex.and.j<=gsy) CoeffV(i,j,k)=d1p4
                      if (i<=gsx.and.j>=gey) CoeffV(i,j,k)=d1p4
                      if (i>=gex.and.j>=gey) CoeffV(i,j,k)=d1p4
                   else 
                      ! faces x6
                      if (i<=gsx) CoeffV(i,j,k)=d1p2
                      if (i>=gex) CoeffV(i,j,k)=d1p2
                      if (j<=gsy) CoeffV(i,j,k)=d1p2
                      if (j>=gey) CoeffV(i,j,k)=d1p2
                      if (k<=gsz) CoeffV(i,j,k)=d1p2
                      if (k>=gez) CoeffV(i,j,k)=d1p2
                      ! edges x12
                      if (i<=gsx.and.j<=gsy) CoeffV(i,j,k)=d1p4
                      if (i>=gex.and.j<=gsy) CoeffV(i,j,k)=d1p4
                      if (i<=gsx.and.j>=gey) CoeffV(i,j,k)=d1p4
                      if (i>=gex.and.j>=gey) CoeffV(i,j,k)=d1p4
                      if (i<=gsx.and.k<=gsz) CoeffV(i,j,k)=d1p4
                      if (i>=gex.and.k<=gsz) CoeffV(i,j,k)=d1p4
                      if (i<=gsx.and.k>=gez) CoeffV(i,j,k)=d1p4
                      if (i>=gex.and.k>=gez) CoeffV(i,j,k)=d1p4
                      if (j<=gsy.and.k<=gsz) CoeffV(i,j,k)=d1p4
                      if (j>=gey.and.k<=gsz) CoeffV(i,j,k)=d1p4
                      if (j<=gsy.and.k>=gez) CoeffV(i,j,k)=d1p4
                      if (j>=gey.and.k>=gez) CoeffV(i,j,k)=d1p4
                      ! corners x8
                      if (i<=gsx.and.j<=gsy.and.k<=gsz) CoeffV(i,j,k)=d1p8
                      if (i>=gex.and.j<=gsy.and.k<=gsz) CoeffV(i,j,k)=d1p8
                      if (i<=gsx.and.j>=gey.and.k<=gsz) CoeffV(i,j,k)=d1p8
                      if (i>=gex.and.j>=gey.and.k<=gsz) CoeffV(i,j,k)=d1p8
                      if (i<=gsx.and.j<=gsy.and.k>=gez) CoeffV(i,j,k)=d1p8
                      if (i>=gex.and.j<=gsy.and.k>=gez) CoeffV(i,j,k)=d1p8
                      if (i<=gsx.and.j>=gey.and.k>=gez) CoeffV(i,j,k)=d1p8
                      if (i>=gex.and.j>=gey.and.k>=gez) CoeffV(i,j,k)=d1p8
                   end if
                end do
             end do
          end do

       else

          do k=sz-gz,ez+gz
             do j=sy-gy,ey+gy
                do i=sx-gx,ex+gx
                   if (dim==2) then
                      ! faces x4
                      if (i<=gsx .and. .not.periodic(1)) CoeffV(i,j,k)=d1p2
                      if (i>=gex .and. .not.periodic(1)) CoeffV(i,j,k)=d1p2
                      if (j<=gsy .and. .not.periodic(2)) CoeffV(i,j,k)=d1p2
                      if (j>=gey .and. .not.periodic(2)) CoeffV(i,j,k)=d1p2
                      ! corners x4
                      if (i<=gsx.and.j<=gsy) then
                         CoeffV(i,j,k)=d1p4
                         if (periodic(1)) CoeffV(i,j,k)=2*CoeffV(i,j,k)
                         if (periodic(2)) CoeffV(i,j,k)=2*CoeffV(i,j,k)
                      end if
                      if (i>=gex.and.j<=gsy) then
                         CoeffV(i,j,k)=d1p4
                         if (periodic(1)) CoeffV(i,j,k)=2*CoeffV(i,j,k)
                         if (periodic(2)) CoeffV(i,j,k)=2*CoeffV(i,j,k)
                      end if
                      if (i<=gsx.and.j>=gey) then
                         CoeffV(i,j,k)=d1p4
                         if (periodic(1)) CoeffV(i,j,k)=2*CoeffV(i,j,k)
                         if (periodic(2)) CoeffV(i,j,k)=2*CoeffV(i,j,k)
                      end if
                      if (i>=gex.and.j>=gey) then
                         CoeffV(i,j,k)=d1p4
                         if (periodic(1)) CoeffV(i,j,k)=2*CoeffV(i,j,k)
                         if (periodic(2)) CoeffV(i,j,k)=2*CoeffV(i,j,k)
                      end if
                   else 
                      ! faces x6
                      if (i<=gsx .and. .not.periodic(1)) CoeffV(i,j,k)=d1p2
                      if (i>=gex .and. .not.periodic(1)) CoeffV(i,j,k)=d1p2
                      if (j<=gsy .and. .not.periodic(2)) CoeffV(i,j,k)=d1p2
                      if (j>=gey .and. .not.periodic(2)) CoeffV(i,j,k)=d1p2
                      if (k<=gsz .and. .not.periodic(3)) CoeffV(i,j,k)=d1p2
                      if (k>=gez .and. .not.periodic(3)) CoeffV(i,j,k)=d1p2
                      ! edges x12
                      if (i<=gsx.and.j<=gsy) then
                         CoeffV(i,j,k)=d1p4
                         if (periodic(1)) CoeffV(i,j,k)=2*CoeffV(i,j,k)
                         if (periodic(2)) CoeffV(i,j,k)=2*CoeffV(i,j,k)
                      end if
                      if (i>=gex.and.j<=gsy) then
                         CoeffV(i,j,k)=d1p4
                         if (periodic(1)) CoeffV(i,j,k)=2*CoeffV(i,j,k)
                         if (periodic(2)) CoeffV(i,j,k)=2*CoeffV(i,j,k)
                      end if
                      if (i<=gsx.and.j>=gey) then
                         CoeffV(i,j,k)=d1p4
                         if (periodic(1)) CoeffV(i,j,k)=2*CoeffV(i,j,k)
                         if (periodic(2)) CoeffV(i,j,k)=2*CoeffV(i,j,k)
                      end if
                      if (i>=gex.and.j>=gey) then
                         CoeffV(i,j,k)=d1p4
                         if (periodic(1)) CoeffV(i,j,k)=2*CoeffV(i,j,k)
                         if (periodic(2)) CoeffV(i,j,k)=2*CoeffV(i,j,k)
                      end if
                      if (i<=gsx.and.k<=gsz) then
                         CoeffV(i,j,k)=d1p4
                         if (periodic(1)) CoeffV(i,j,k)=2*CoeffV(i,j,k)
                         if (periodic(3)) CoeffV(i,j,k)=2*CoeffV(i,j,k)
                      end if
                      if (i>=gex.and.k<=gsz) then
                         CoeffV(i,j,k)=d1p4
                         if (periodic(1)) CoeffV(i,j,k)=2*CoeffV(i,j,k)
                         if (periodic(3)) CoeffV(i,j,k)=2*CoeffV(i,j,k)
                      end if
                      if (i<=gsx.and.k>=gez) then
                         CoeffV(i,j,k)=d1p4
                         if (periodic(1)) CoeffV(i,j,k)=2*CoeffV(i,j,k)
                         if (periodic(3)) CoeffV(i,j,k)=2*CoeffV(i,j,k)
                      end if
                      if (i>=gex.and.k>=gez) then
                         CoeffV(i,j,k)=d1p4
                         if (periodic(1)) CoeffV(i,j,k)=2*CoeffV(i,j,k)
                         if (periodic(3)) CoeffV(i,j,k)=2*CoeffV(i,j,k)
                      end if
                      if (j<=gsy.and.k<=gsz) then
                         CoeffV(i,j,k)=d1p4
                         if (periodic(2)) CoeffV(i,j,k)=2*CoeffV(i,j,k)
                         if (periodic(3)) CoeffV(i,j,k)=2*CoeffV(i,j,k)
                      end if
                      if (j>=gey.and.k<=gsz) then
                         CoeffV(i,j,k)=d1p4
                         if (periodic(2)) CoeffV(i,j,k)=2*CoeffV(i,j,k)
                         if (periodic(3)) CoeffV(i,j,k)=2*CoeffV(i,j,k)
                      end if
                      if (j<=gsy.and.k>=gez) then
                         CoeffV(i,j,k)=d1p4
                         if (periodic(2)) CoeffV(i,j,k)=2*CoeffV(i,j,k)
                         if (periodic(3)) CoeffV(i,j,k)=2*CoeffV(i,j,k)
                      end if
                      if (j>=gey.and.k>=gez) then
                         CoeffV(i,j,k)=d1p4
                         if (periodic(2)) CoeffV(i,j,k)=2*CoeffV(i,j,k)
                         if (periodic(3)) CoeffV(i,j,k)=2*CoeffV(i,j,k)
                      end if
                      ! corners x8
                      if (i<=gsx.and.j<=gsy.and.k<=gsz) then
                         CoeffV(i,j,k)=d1p8
                         if (periodic(1)) CoeffV(i,j,k)=2*CoeffV(i,j,k)
                         if (periodic(2)) CoeffV(i,j,k)=2*CoeffV(i,j,k)
                         if (periodic(3)) CoeffV(i,j,k)=2*CoeffV(i,j,k)
                      end if
                      if (i>=gex.and.j<=gsy.and.k<=gsz) then
                         CoeffV(i,j,k)=d1p8
                         if (periodic(1)) CoeffV(i,j,k)=2*CoeffV(i,j,k)
                         if (periodic(2)) CoeffV(i,j,k)=2*CoeffV(i,j,k)
                         if (periodic(3)) CoeffV(i,j,k)=2*CoeffV(i,j,k)
                      end if
                      if (i<=gsx.and.j>=gey.and.k<=gsz) then
                         CoeffV(i,j,k)=d1p8
                         if (periodic(1)) CoeffV(i,j,k)=2*CoeffV(i,j,k)
                         if (periodic(2)) CoeffV(i,j,k)=2*CoeffV(i,j,k)
                         if (periodic(3)) CoeffV(i,j,k)=2*CoeffV(i,j,k)
                      end if
                      if (i>=gex.and.j>=gey.and.k<=gsz) then
                         CoeffV(i,j,k)=d1p8
                         if (periodic(1)) CoeffV(i,j,k)=2*CoeffV(i,j,k)
                         if (periodic(2)) CoeffV(i,j,k)=2*CoeffV(i,j,k)
                         if (periodic(3)) CoeffV(i,j,k)=2*CoeffV(i,j,k)
                      end if
                      if (i<=gsx.and.j<=gsy.and.k>=gez) then
                         CoeffV(i,j,k)=d1p8
                         if (periodic(1)) CoeffV(i,j,k)=2*CoeffV(i,j,k)
                         if (periodic(2)) CoeffV(i,j,k)=2*CoeffV(i,j,k)
                         if (periodic(3)) CoeffV(i,j,k)=2*CoeffV(i,j,k)
                      end if
                      if (i>=gex.and.j<=gsy.and.k>=gez) then
                         CoeffV(i,j,k)=d1p8
                         if (periodic(1)) CoeffV(i,j,k)=2*CoeffV(i,j,k)
                         if (periodic(2)) CoeffV(i,j,k)=2*CoeffV(i,j,k)
                         if (periodic(3)) CoeffV(i,j,k)=2*CoeffV(i,j,k)
                      end if
                      if (i<=gsx.and.j>=gey.and.k>=gez) then
                         CoeffV(i,j,k)=d1p8
                         if (periodic(1)) CoeffV(i,j,k)=2*CoeffV(i,j,k)
                         if (periodic(2)) CoeffV(i,j,k)=2*CoeffV(i,j,k)
                         if (periodic(3)) CoeffV(i,j,k)=2*CoeffV(i,j,k)
                      end if
                      if (i>=gex.and.j>=gey.and.k>=gez) then
                         CoeffV(i,j,k)=d1p8
                         if (periodic(1)) CoeffV(i,j,k)=2*CoeffV(i,j,k)
                         if (periodic(2)) CoeffV(i,j,k)=2*CoeffV(i,j,k)
                         if (periodic(3)) CoeffV(i,j,k)=2*CoeffV(i,j,k)
                      end if
                   end if
                end do
             end do
          end do

       end if
    end if

    coeffS=2*CoeffV

  end subroutine InitCoeffVS
  
end module mod_coeffvs
