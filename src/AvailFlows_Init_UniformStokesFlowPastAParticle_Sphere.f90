!===============================================================================
module Bib_VOFLag_Init_UniformStokesFlowPastAParticle_Sphere
  !===============================================================================
  contains
  !-----------------------------------------------------------------------------  
  !> @author  amine chadil
  ! 
  !> @brief initialisation of the uniform flow past a sphere 
  !
  !> @param u,v,w          : velocity field components
  !> @param pres           : pressure
  !> @param U_inf          : the velocity in infinity (in x direction)
  !> @param rayon_sphere   : the sphere's radii
  !> @param centre_sphere  : the sphere's center
  !-----------------------------------------------------------------------------
  subroutine Init_UniformStokesFlowPastAParticle_Sphere(u,v,w,pres,U_inf,centre_sphere,rayon_sphere)
    !===============================================================================
    !modules
    !===============================================================================
    !-------------------------------------------------------------------------------
    !Modules de definition des structures et variables => src/mod/module_...f90
    !-------------------------------------------------------------------------------
    use mod_struct_thermophysics, only : fluids
    use mod_Parameters,           only : deeptracking,sx,gx,ex,gx,sy,gy,ey,gy,sz,gz,ez,gz,sxu,&
                               exu,syu,eyu,szu,ezu,sxv,exv,syv,eyv,szv,ezv,sxw,exw, &
                               syw,eyw,szw,ezw,grid_xu,grid_yv,grid_zw,grid_x,grid_y,grid_z
    !-------------------------------------------------------------------------------
    
    !===============================================================================
    !declarations des variables
    !===============================================================================
    implicit none
    !-------------------------------------------------------------------------------
    !variables globales
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable, intent(inout) :: u,v,w,pres
    real(8), dimension(3)                                 :: centre_sphere
    real(8)                                               :: rayon_sphere,U_inf
    !-------------------------------------------------------------------------------
    !variables locales 
    !-------------------------------------------------------------------------------
    integer                                               :: i,j,k
    real(8)                                               :: x,z,y,r
    !-------------------------------------------------------------------------------
    
    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'entree Init_UniformStokesFlowPastAParticle_Sphere'
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! calcul de pression
    !-------------------------------------------------------------------------------
    pres = 0.d0
    do k=sz-gz,ez+gz
      do j=sy-gy,ey+gy
        do i=sx-gx,ex+gx
          x=grid_x(i)-centre_sphere(1)
          z=grid_y(j)-centre_sphere(2)
          y=grid_z(k)-centre_sphere(3)
          r=sqrt(x**2+z**2+y**2)
          if (r.gt.rayon_sphere) then
            pres(i,j,k)=-3d0*fluids(1)%mu*rayon_sphere*0.5d0*(x*U_inf/(r**3+1D-40))
          end if
        end do
      end do
    end do

    !-------------------------------------------------------------------------------
    ! calcul de vitesse
    !-------------------------------------------------------------------------------
    u=0d0;v=0d0;w=0d0;
    do k=szu-gz,ezu+gz
      do j=syu-gy,eyu+gy
        do i=sxu-gx,exu+gx
          !vitesse suivant x
          x=grid_xu(i)-centre_sphere(1)
          z=grid_y (j)-centre_sphere(2)
          y=grid_z (k)-centre_sphere(3)
          r=sqrt(x**2+z**2+y**2)
          if (r.gt.rayon_sphere) then
            u(i,j,k)=U_inf-3d0*rayon_sphere/4d0*(U_inf/(r+1D-40)+x**2*U_inf/(r**3+1D-40))&
                     -3d0*rayon_sphere**3/4d0*(U_inf/(3d0*r**3+1D-40)-x**2*U_inf/(r**5+1D-40))
          end if    
        end do
      end do
    end do

    do k=szv-gz,ezv+gz
      do j=syv-gy,eyv+gy
        do i=sxv-gx,exv+gx
          !vitesse suivant y
          x=grid_x (i)-centre_sphere(1)
          z=grid_yv(j)-centre_sphere(2)
          y=grid_z (k)-centre_sphere(3)
          r=sqrt(x**2+z**2+y**2)
          if (r.gt.rayon_sphere) then
            v(i,j,k)=-3d0*rayon_sphere/4d0*(x*z*U_inf/(r**3+1D-40)) & 
                     +3d0*rayon_sphere**3d0/4d0*(x*z*U_inf/(r**5+1D-40))
          end if
        end do
      end do
    end do

    do k=szw-gz,ezw+gz
      do j=syw-gy,eyw+gy
        do i=sxw-gx,exw+gx
          !vitesse suivant z
          x=grid_x (i)-centre_sphere(1)
          z=grid_y (j)-centre_sphere(2)
          y=grid_zw(k)-centre_sphere(3)
          r=sqrt(x**2+z**2+y**2)
          if (r.gt.rayon_sphere) then
            w(i,j,k)=-3d0*rayon_sphere/4d0*(x*y*U_inf/(r**3+1D-40))&
                     +3d0*rayon_sphere**3/4d0*(x*y*U_inf/(r**5+1D-40))
          end if     
        end do
      end do
    end do

    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'sortie Init_UniformStokesFlowPastAParticle_Sphere'
    !-------------------------------------------------------------------------------
  end subroutine Init_UniformStokesFlowPastAParticle_Sphere

  !===============================================================================
end module Bib_VOFLag_Init_UniformStokesFlowPastAParticle_Sphere
!===============================================================================