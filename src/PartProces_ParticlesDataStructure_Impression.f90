!===============================================================================
module Bib_VOFLag_ParticlesDataStructure_Impression
   !===============================================================================
   contains
   !---------------------------------------------------------------------------  
   !> @author jorge cesar brandle de motta, amine chadil
   ! 
   !> @brief initialise les donnees d'impression de la structure vl et creer les 
   !! fichiers contenant ces donnees 
   !
   !> @todo Il faut ajouter l'ecriture binaire
   !
   !> @todo Il faut augmenter la precision dans l'ecriture (ajouter toutes les decimaux), notament pour le temps.
   !
   !> @param[in] vl   : la structure contenant toutes les informations
   !! relatives aux particules       
   !---------------------------------------------------------------------------  
   subroutine ParticlesDataStructure_Impression(vl,nt,time)
      !===============================================================================
      !modules
      !===============================================================================
      !-------------------------------------------------------------------------------
      !Modules de definition des structures et variables => src/mod/module_...f90
      !-------------------------------------------------------------------------------
      use Module_VOFLag_ParticlesDataStructure
      use mod_Parameters,                       only : deeptracking,dim
      !-------------------------------------------------------------------------------
      !Modules de subroutines de la librairie RESPECT => src/Bib_VOFLag_...f90
      !-------------------------------------------------------------------------------
      use Bib_VOFLag_ParticlesDataStructure_Impression_FileCreation
      !-------------------------------------------------------------------------------

      !===============================================================================
      !declarations des variables
      !===============================================================================
      implicit none
      !-------------------------------------------------------------------------------
      !variables globales
      !-------------------------------------------------------------------------------
      type(struct_vof_lag), intent(inout)  :: vl
      real(8),              intent(in)     :: time
      integer,              intent(in)     :: nt
      !-------------------------------------------------------------------------------
      !variables locales 
      !-------------------------------------------------------------------------------
      integer                              :: np,na
      logical                              :: there
      !-------------------------------------------------------------------------------

      !-------------------------------------------------------------------------------
      if (deeptracking) write(*,*) 'entree ParticlesDataStructure_Impression'
      !-------------------------------------------------------------------------------

      !-------------------------------------------------------------------------------
      !
      !-------------------------------------------------------------------------------
      do na=1,vl%nbaffich
         np=vl%affich(na)
         !-------------------------------------------------------------------------------
         !
         !-------------------------------------------------------------------------------
         inquire(file=vl%filec(na),exist=there)
         if (.not.there) call ParticlesDataStructure_Impression_FileCreation(vl,na)

         open(unit=1000+np,file=vl%filec(na),status='old',position='append')
         !-------------------------------------------------------------------------------
         !
         !-------------------------------------------------------------------------------
         write(1000+np,'(4E13.5)',advance='no')  dfloat(nt),time,vl%objet(np)%pos(1:2)
         if (dim==3) write(1000+np,'(E13.5)' ,advance='no') vl%objet(np)%pos(3)

         !-------------------------------------------------------------------------------
         !
         !-------------------------------------------------------------------------------
         if (vl%deplacement) then
            write(1000+np,'(2E13.5)',advance='no') vl%objet(np)%v(1:2)
            if (dim==3) write(1000+np,'(E13.5)' ,advance='no') vl%objet(np)%v(3)
         end if

         !-------------------------------------------------------------------------------
         !
         !-------------------------------------------------------------------------------   
         if (vl%vrot) then
            if (dim==3) write(1000+np,'(2E13.5)',advance='no') vl%objet(np)%omeg(1:2)
            write(1000+np,'(E13.5)' ,advance='no') vl%objet(np)%omeg(3)
         end if

         !-------------------------------------------------------------------------------
         !
         !------------------------------------------------------------------------------- 
         if (vl%postforce) then
            if (dim==2) then
               write(1000+np,'(6E13.5)',advance='no') vl%objet(np)%f(1:2),vl%objet(np)%fp(1:2),vl%objet(np)%fv(1:2)
            else
               write(1000+np,'(9E13.5)',advance='no') vl%objet(np)%f(1:3),vl%objet(np)%fp(1:3),vl%objet(np)%fv(1:3)
            end if
         end if

         !-------------------------------------------------------------------------------
         !
         !------------------------------------------------------------------------------- 
         if (vl%postHeatFlux) then
         write(1000+np,'(2E13.5)',advance='no') vl%objet(np)%HeatFlux,vl%objet(np)%BulkTemp
         end if

         !-------------------------------------------------------------------------------
         !
         !-------------------------------------------------------------------------------
         if (vl%postchoc_bpp)  write(1000+np,'(L3)',advance='no')  vl%objet(np)%chocparpar
         if (vl%postchoc_fpp) then
            write(1000+np,'(2E13.5)',advance='no') vl%objet(np)%fparpar(1:2)
            if (dim==3) write(1000+np,'(E13.5)' ,advance='no') vl%objet(np)%fparpar(3)
         end if
         if (vl%postchoc_BPM)  write(1000+np,'(L3)',advance='no')  vl%objet(np)%chocparmur
         if (vl%postchoc_fPM) then
            write(1000+np,'(2E13.5)',advance='no') vl%objet(np)%fparmur(1:2)
            if (dim==3) write(1000+np,'(E13.5)' ,advance='no') vl%objet(np)%fparmur(3)
         end if

         close(1000+np)
      end do

      !-------------------------------------------------------------------------------
      if (deeptracking) write(*,*) 'sortie ParticlesDataStructure_Impression'
      !-------------------------------------------------------------------------------
   end subroutine ParticlesDataStructure_Impression

   !===============================================================================
end module Bib_VOFLag_ParticlesDataStructure_Impression
!===============================================================================