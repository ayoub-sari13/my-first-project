!===============================================================================
module Bib_VOFLag_ParticlesDataStructure_Initialize
  !===============================================================================
  contains
  !---------------------------------------------------------------------------  
  !> @author amine chadil
  ! 
  !> @brief initialisation de la structure vl, avec les caracteristiques des particules solides.
  !
  !> @param[out]   vl     : la structure contenant toutes les informations
  !! relatives aux particules.
  !> @param      cou_vim  : la fonction couleur pour les viscosite calculees dans les points
  !! de pression.
  !> @param      cou_vic  : la fonction couleur pour les viscosite calculees dans les points
  !! de viscosite
  !---------------------------------------------------------------------------  
  subroutine ParticlesDataStructure_Initialize(cou,vl)
    !===============================================================================
    !modules
    !===============================================================================
    !-------------------------------------------------------------------------------
    !Modules de definition des structures et variables => src/mod/module_...f90
    !-------------------------------------------------------------------------------
    use Module_VOFLag_ParticlesDataStructure
    use mod_Parameters,                       only : deeptracking
    !-------------------------------------------------------------------------------
    !Modules de subroutines de la librairie RESPECT => src/Bib_VOFLag_...f90
    !-------------------------------------------------------------------------------
    use Bib_VOFLag_ParticlesDataStructure_Objet_Initialize
    use Bib_VOFLag_Particle_Collisions_InitializeData
    !-------------------------------------------------------------------------------

    !===============================================================================
    !declarations des variables
    !===============================================================================
    implicit none
    !-------------------------------------------------------------------------------
    !variables globales
    !-------------------------------------------------------------------------------
    type(struct_vof_lag),                   intent(inout) :: vl
    real(8), dimension(:,:,:), allocatable, intent(inout) :: cou
    !-------------------------------------------------------------------------------
    !variables locales 
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'entree ParticlesDataStructure_Initialize'
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    !initialisation des structures vl%objet.
    !-------------------------------------------------------------------------------
    call ParticlesDataStructure_Objet_Initialize(vl)

    !-------------------------------------------------------------------------------
    !calcul des distances critiques pour les collisions 
    !-------------------------------------------------------------------------------
    if (vl%force_inter) call Particle_Collisions_InitializeData(vl)

    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'sortie ParticlesDataStructure_Initialize'
    !-------------------------------------------------------------------------------
  end subroutine ParticlesDataStructure_Initialize

  !===============================================================================
end module Bib_VOFLag_ParticlesDataStructure_Initialize
!===============================================================================
  


