!===============================================================================
module Bib_VOFLag_TaylorInterpolation_PressureMesh_Gradiant
  !===============================================================================
  contains
  !-----------------------------------------------------------------------------  
  !> @author amine chadil
  ! 
  !> @brief cette routine
  !
  !> @param [in]  
  !> @param [in]  
  !> @param [out] 
  !> @param [in]  
  !-----------------------------------------------------------------------------
  subroutine TaylorInterpolation_PressureMesh_Gradiant (pres,i,j,k,Order,norm,grad,&
                                                        ndim,dx,dy,dz)
    !===============================================================================
    !modules
    !===============================================================================
    !-------------------------------------------------------------------------------
    !Modules de definition des structures et variables => src/mod/module_...f90
    !-------------------------------------------------------------------------------
    !Modules de subroutines de la librairie VOF-Lag
    !-------------------------------------------------------------------------------
    use Bib_VOFLag_DirectionOnEulerianMeshFollowingNormalVector
    !-------------------------------------------------------------------------------
    !===============================================================================
    !declarations des variables
    !===============================================================================
    implicit none
    !-------------------------------------------------------------------------------
    !variables globales
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable, intent(in)  :: pres
    real(8), dimension(:),     allocatable, intent(in)  :: dx,dy,dz
    integer                               , intent(in)  :: i,j,k,Order,ndim
    real(8), dimension(ndim)              , intent(in)  :: norm
    real(8), dimension(ndim)              , intent(out) :: grad
    !-------------------------------------------------------------------------------
    !variables locales 
    !-------------------------------------------------------------------------------
    integer                             :: i_dir,j_dir,k_dir
    !-------------------------------------------------------------------------------
    call DirectionOnEulerianMeshFollowingNormalVector (norm(1),i_dir)
    call DirectionOnEulerianMeshFollowingNormalVector (norm(2),j_dir)
    call DirectionOnEulerianMeshFollowingNormalVector (norm(3),k_dir)

    select case(Order)
    case(1)
      grad(1) = (pres(i,j,k)&
                -pres(i+i_dir,j,k))&
                /(sign(1.D0,norm(1))*dx(min(i,i+i_dir))+1D-40)
      grad(2) = (pres(i,j,k)&
                -pres(i,j+j_dir,k))&
                /(sign(1.D0,norm(2))*dy(min(j,j+j_dir))+1D-40)
      if (ndim==3) then 
        grad(3) = (pres(i,j,k)&
                  -pres(i,j,k+k_dir))&
                  /(sign(1.D0,norm(3))*dz(min(k,k+k_dir))+1D-40)
      end if 
    case(2)
      grad(1) = (3d0*pres(i,j,k)&
                -4d0*pres(i+i_dir,j,k)&
                    +pres(i+2*i_dir,j,k))&
                /(2d0*sign(1.D0,norm(1))*dx(min(i,i+i_dir))+1D-40)
      grad(2) = (3d0*pres(i,j,k)&
                -4d0*pres(i,j+j_dir,k)&
                    +pres(i,j+2*j_dir,k))&
                /(2d0*sign(1.D0,norm(2))*dy(min(j,j+j_dir))+1D-40)
      if (ndim==3) then 
        grad(3) = (3d0*pres(i,j,k)&
                  -4d0*pres(i,j,k+k_dir)&
                      +pres(i,j,k+2*k_dir))&
                  /(2d0*sign(1.D0,norm(3))*dz(min(k,k+k_dir))+1D-40)
      end if 

    case(3)
      grad(1) = (11d0*pres(i,j,k)&
                -18d0*pres(i+i_dir,j,k)&
                + 9d0*pres(i+2*i_dir,j,k)&
                - 2d0*pres(i+3*i_dir,j,k))&
                /(6d0*sign(1.D0,norm(1))*dx(min(i,i+i_dir))+1D-40)
      grad(2) = (11d0*pres(i,j,k)&
                -18d0*pres(i,j+j_dir,k)&
                + 9d0*pres(i,j+2*j_dir,k)&
                - 2d0*pres(i,j+3*j_dir,k))&
                /(6d0*sign(1.D0,norm(2))*dy(min(j,j+j_dir))+1D-40)
      if (ndim==3) then 
        grad(3) = (11d0*pres(i,j,k)&
                  -18d0*pres(i,j,k+k_dir)&
                  + 9d0*pres(i,j,k+2*k_dir)&
                  - 2d0*pres(i,j,k+3*k_dir))&
                  /(6d0*sign(1.D0,norm(3))*dz(min(k,k+k_dir))+1D-40)
      end if 

    case(4)
      grad(1) = (25d0*pres(i,j,k)&
                -48d0*pres(i+i_dir,j,k)&
                +36d0*pres(i+2*i_dir,j,k)&
                -16d0*pres(i+3*i_dir,j,k)&
                + 3d0*pres(i+4*i_dir,j,k))&
                /(12d0*sign(1.D0,norm(1))*dx(min(i,i+i_dir))+1D-40)
      grad(2) = (25d0*pres(i,j,k)&
                -48d0*pres(i,j+j_dir,k)&
                +36d0*pres(i,j+2*j_dir,k)&
                -16d0*pres(i,j+3*j_dir,k)&
                + 3d0*pres(i,j+4*j_dir,k))&
                /(12d0*sign(1.D0,norm(2))*dy(min(j,j+j_dir))+1D-40)
      if (ndim==3) then 
        grad(3) = (25d0*pres(i,j,k)&
                  -48d0*pres(i,j,k+k_dir)&
                  +36d0*pres(i,j,k+2*k_dir)&
                  -16d0*pres(i,j,k+3*k_dir)&
                  + 3d0*pres(i,j,k+4*k_dir))&
                  /(12d0*sign(1.D0,norm(3))*dz(min(k,k+k_dir))+1D-40)
      end if 
    case default
      write(*,*) "STOP : ordre non implemente (Gradiant) "
      stop
    end select
  end subroutine TaylorInterpolation_PressureMesh_Gradiant

  !===============================================================================
end module Bib_VOFLag_TaylorInterpolation_PressureMesh_Gradiant
!===============================================================================
  


