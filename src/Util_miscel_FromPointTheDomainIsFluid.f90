!===============================================================================
module Bib_VOFLag_FromPointTheDomainIsFluid
  !===============================================================================
  contains
  !-----------------------------------------------------------------------------  
  !> @author amine chadil
  ! 
  !> @brief This routine assesses if the eulerian mesh points, starting
  !> from the extrapolation point, are fluid
  !-----------------------------------------------------------------------------
  subroutine FromPointTheDomainIsFluid(i,j,k,Order,normal,cou,ndim,IsFluid)
    !===============================================================================
    !modules
    !===============================================================================
    !-------------------------------------------------------------------------------
    !Modules de definition des structures et variables => src/mod/module_...f90
    !-------------------------------------------------------------------------------
    !Modules de subroutines de la librairie imft => src/bib_imft_...f90
    !-------------------------------------------------------------------------------
    use Bib_VOFLag_DirectionOnEulerianMeshFollowingNormalVector
    !-------------------------------------------------------------------------------

    !===============================================================================
    !declarations des variables
    !===============================================================================
    implicit none
    !-------------------------------------------------------------------------------
    !variables globales
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable, intent(in)  :: cou
    real(8), dimension(ndim)              , intent(in)  :: normal    
    integer                               , intent(in)  :: i,j,k,Order,ndim
    logical                               , intent(out) :: IsFluid
    !-------------------------------------------------------------------------------
    !variables locles
    !-------------------------------------------------------------------------------
    integer                                             :: dirx,diry,dirz,p1,p2,p3,&
                                                          i_tmp,j_tmp,k_tmp
    !-------------------------------------------------------------------------------
    IsFluid=.true.
    if (cou(i,j,k) .gt. 1.D-15) then
      IsFluid=.false.
      go to 1
    endif
    if (Order .ge. 2) then  
      call DirectionOnEulerianMeshFollowingNormalVector (normal(1),dirx)
      call DirectionOnEulerianMeshFollowingNormalVector (normal(2),diry)
      if (ndim==3) call DirectionOnEulerianMeshFollowingNormalVector (normal(3),dirz)
      if (ndim==2) then 
        do p1=1,Order
          i_tmp=i+dirx*p1
          do p2=1,Order
            j_tmp=j+diry*p2
            if ( cou(i_tmp,j_tmp,k_tmp).gt.1.D-15) then 
              IsFluid=.false.
              go to 1
            end if
          end do 
        end do
      else
        do p1=1,Order
          i_tmp=i+dirx*p1
          do p2=1,Order
            j_tmp=j+diry*p2
            do p3=1,Order
              k_tmp=k+dirz*p3
              if ( cou(i_tmp,j_tmp,k_tmp).gt.1.D-15) then 
                IsFluid=.false.
                go to 1
              end if
            end do
          end do 
        end do
      end if 
    endif
1 continue
  end subroutine FromPointTheDomainIsFluid

  !===============================================================================
end module Bib_VOFLag_FromPointTheDomainIsFluid
!===============================================================================
  

