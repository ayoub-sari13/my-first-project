
#include "precomp.h"
module mod_solver_new
  use mod_solver_new_def
  use mod_solver_new_hypre
  use mod_solver_new_mumps
  use mod_timers

#define OPTIM_PSCALEE 1

contains

  subroutine solve_linear_system(system,rhs,sol,solver,grid)
    use mod_struct_solver
    use mod_struct_grid
    implicit none
    !--------------------------------------------------------------------------
    ! global variables
    !--------------------------------------------------------------------------
    real(8), dimension(:), allocatable, intent(inout) :: rhs
    real(8), dimension(:), allocatable, intent(inout) :: sol
    type(system_t), intent(inout), target             :: system
    type(solver_t), intent(inout)                     :: solver
    type(grid_t), intent(in)                          :: grid 
    !--------------------------------------------------------------------------
    ! local variables
    !--------------------------------------------------------------------------
    integer                                           :: l
    !--------------------------------------------------------------------------
    
    call compute_time(TIMER_START,"[sub] solve_linear_system")

    call perturb_rhs(rhs,system%mat%npt)
    call print_system(system%mat,rhs)

    call diagonal_scaling(system%mat,rhs,grid)

    select case(solver%type)
    case(0)
#if MUMPS
       !--------------------------------------------------------------------------
       ! direct solver with MUMPS 
       !--------------------------------------------------------------------------
       call resol_MUMPS(system%mat,sol,rhs,solver,grid)
       !--------------------------------------------------------------------------
#endif
    case(1)
       !--------------------------------------------------------------------------
       ! in house (jacobi/iLU)-BiCGStab(2)
       !--------------------------------------------------------------------------
       call BiCGStab2(system,rhs,sol,solver,grid)
       !--------------------------------------------------------------------------
    case(2,4)
#if HYPRE
       !--------------------------------------------------------------------------
       ! HYPRE Solvers 
       !--------------------------------------------------------------------------
       ! Periodic matrix on 1 proc is note suitable with HYPRE solvers
       !--------------------------------------------------------------------------
       call check_and_print_error_parallel(solver,grid)
       !--------------------------------------------------------------------------
       call init_solver_HYPRE(solver,grid)
       call make_matrix_puvw_HYPRE(CREATE,system,solver,grid)
       select case(solver%type)
       case(2)
          call BiCGStab_HYPRE(system,rhs,sol,solver,grid)
       case(4)
          call PCG_HYPRE(system,rhs,sol,solver,grid)
       end select
       call make_matrix_puvw_HYPRE(DESTROY,system,solver,grid)
       !--------------------------------------------------------------------------
#endif
    case(3)
#if HYPRE
       !--------------------------------------------------------------------------
       ! BiCGStab(2) precond SMG/PFMG for Fully Coupled resolution
       !--------------------------------------------------------------------------
       call init_solver_HYPRE(solver,grid)
       call create_precond_multi_grid_HYPRE(solver)
       call make_matrix_puvw_HYPRE(CREATE,system,solver,grid)
       call make_setup_HYPRE(CREATE,solver)
       call BiCGStab2(system,rhs,sol,solver,grid)
       call make_setup_HYPRE(DESTROY,solver)
       call make_matrix_puvw_HYPRE(DESTROY,system,solver,grid)
       !--------------------------------------------------------------------------
#endif
    case DEFAULT
       call print_error_solver_choice(solver)
       stop
    end select

    call print_solver(solver)

    call compute_time(TIMER_END,"[sub] solve_linear_system")

  end subroutine solve_linear_system


  subroutine perturb_rhs(rhs,npt)
    implicit none
    !--------------------------------------------------------------------------
    ! global variables
    !--------------------------------------------------------------------------
    real(8), dimension(:), allocatable, intent(inout) :: rhs
    integer, intent(in)                               :: npt 
    !--------------------------------------------------------------------------
    ! local variables
    !--------------------------------------------------------------------------
    integer                                           :: l
    !--------------------------------------------------------------------------

    call compute_time(TIMER_START,"[sub] perturb_rhs")

    !--------------------------------------------------------------------------
    ! An epsilon is put inside the source term
    !--------------------------------------------------------------------------
    do l=1,npt
       rhs(l) = rhs(l) + THRESHOLD_RHS_PERTURB*sin(dble(l)/20)
    end do
    !--------------------------------------------------------------------------

    call compute_time(TIMER_END,"[sub] perturb_rhs")

  end subroutine perturb_rhs

  subroutine diagonal_scaling(mat,rhs,grid)
    use mod_struct_grid
    use mod_struct_solver
    implicit none
    !--------------------------------------------------------------------------
    ! global variables
    !--------------------------------------------------------------------------
    type(matrix_t),                     intent(inout) :: mat
    real(8), dimension(:), allocatable, intent(inout) :: rhs
    type(grid_t),                       intent(in)    :: grid
    !--------------------------------------------------------------------------
    ! local variables
    !--------------------------------------------------------------------------
    integer                                           :: k,l,lnpt
    real(8)                                           :: diag
    !--------------------------------------------------------------------------

    call compute_time(TIMER_START,"[sub] diagonal_scaling")

    !--------------------------------------------------------------------------
    ! Initial conditionning with jacobi matix
    !--------------------------------------------------------------------------
    select case(mat%eqs)
    case(SOLVE_UVP,SOLVE_UVWP)
       lnpt = mat%npt-grid%nb%tot%p
    case DEFAULT
       lnpt = mat%npt
    end select
    !--------------------------------------------------------------------------
    do l = 1,lnpt
       diag = mat%coef(mat%icof(l))
       do k = mat%icof(l)+1,mat%icof(l+1)-1
          mat%coef(k) = mat%coef(k) / diag
       enddo
       rhs(l) = rhs(l) / diag
    enddo
    !--------------------------------------------------------------------------
    do l = 1,lnpt
       mat%coef(mat%icof(l)) = 1
    end do
    !--------------------------------------------------------------------------
    do l=1,mat%nps
       if (abs(mat%coef(l)) < THRESHOLD_DIAG) mat%coef(l) = 0
    end do
    !--------------------------------------------------------------------------
    do l=1,mat%npt 
       if (abs(rhs(l)) < THRESHOLD_DIAG) rhs(l) = 0
    enddo
    !--------------------------------------------------------------------------

    call compute_time(TIMER_END,"[sub] diagonal_scaling")

  end subroutine diagonal_scaling

  subroutine factor(system,solver,grid,alu)
    use mod_struct_grid
    use mod_struct_solver
    implicit none
    !--------------------------------------------------------------------------
    ! global variables
    !--------------------------------------------------------------------------
    type(grid_t), intent(in)             :: grid
    type(system_t), intent(inout)        :: system
    type(solver_t), intent(inout)        :: solver
    real(8), dimension(:), intent(inout) :: alu
    !--------------------------------------------------------------------------
    ! local variables
    !--------------------------------------------------------------------------
    !--------------------------------------------------------------------------
    
    call compute_time(TIMER_START,"[sub] factor")

    alu=0
    
    !--------------------------------------------------------------------------
    select case(solver%eqs)
    case(SOLVE_UVWP,SOLVE_UVP)
       !--------------------------------------------------------------------------
       ! Fully coupled case 
       !--------------------------------------------------------------------------
       call extract_Bp(system%mat,system%bp,grid)
       call extract_Fuv(system%mat,system%fuv,grid)
       call extract_Fuw_Fvw(system%mat,system%fuvw,grid)
       !--------------------------------------------------------------------------
    case DEFAULT
       select case(solver%precond)
       case(PRECOND_JACOBI)
          !--------------------------------------------------------------------------
          ! Jacobi
          !--------------------------------------------------------------------------
          call facjac(system%mat,alu)
          !--------------------------------------------------------------------------
       case(PRECOND_ILU)
          !--------------------------------------------------------------------------
          ! iLU
          !--------------------------------------------------------------------------
          call facilke(system%mat,alu)
          !--------------------------------------------------------------------------
       end select
       !--------------------------------------------------------------------------
    end select
    !--------------------------------------------------------------------------

    call compute_time(TIMER_END,"[sub] factor")

  end subroutine factor

  subroutine facjac(mat,alu)
    use mod_struct_solver
    implicit none
    !--------------------------------------------------------------------------
    ! global variables
    !--------------------------------------------------------------------------
    type(matrix_t),        intent(inout) :: mat
    real(8), dimension(:), intent(inout) :: alu
    !--------------------------------------------------------------------------
    ! local variables
    !--------------------------------------------------------------------------
    !--------------------------------------------------------------------------

    call compute_time(TIMER_START,"[sub] facjac")

    alu = 0

    call compute_time(TIMER_END,"[sub] facjac")

  end subroutine facjac


  subroutine facilke(mat,alu)
    use mod_struct_solver
    implicit none
    !--------------------------------------------------------------------------
    ! global variables
    !--------------------------------------------------------------------------
    type(matrix_t),        intent(inout) :: mat
    real(8), dimension(:), intent(inout) :: alu
    !--------------------------------------------------------------------------
    ! local variables
    !--------------------------------------------------------------------------
    integer                   :: j,k,l,n
    real(8)                   :: prec
    !--------------------------------------------------------------------------

    call compute_time(TIMER_START,"[sub] facilke")

    prec = 1d-15

    !--------------------------------------------------------------------------
    do n = 1,mat%nic
       l = mat%kic(n)
       do k = mat%icof(l),mat%icof(l)+mat%ndd-1,2
          alu(k) = mat%coef(k)
       enddo
    enddo
    !--------------------------------------------------------------------------

    !--------------------------------------------------------------------------
    do j = 1,mat%nic
       l = mat%kic(j)
       do k = mat%icof(l)+1,mat%icof(l)+mat%ndd-1,2
          alu(k) = mat%coef(k) / ( alu(mat%icof(mat%jcof(k))) + prec )
       enddo
       n = 0
       do k = mat%icof(l)+1,mat%icof(l)+mat%ndd-1,2
          n = n + 2
          alu(mat%icof(l)) = alu(mat%icof(l)) - alu(k)*alu(mat%icof(mat%jcof(k))+n)
       enddo
    enddo
    !--------------------------------------------------------------------------

    call compute_time(TIMER_END,"[sub] facilke")

  end subroutine facilke


  subroutine precond(system,solver,grid,sol,ysa,alu)
    use mod_struct_grid
    use mod_struct_solver
    implicit none
    !--------------------------------------------------------------------------
    ! Global variables
    !--------------------------------------------------------------------------
    type(system_t), target             :: system
    type(grid_t)                       :: grid
    type(solver_t)                     :: solver
    real(8), dimension(system%mat%nps) :: alu
    real(8), dimension(system%mat%npt) :: sol,ysa
    !--------------------------------------------------------------------------
    ! Local variables
    !--------------------------------------------------------------------------
    type(matrix_t), pointer            :: mat
    !--------------------------------------------------------------------------

    call compute_time(TIMER_START,"[sub] precond")

    mat => system%mat
    
    select case(solver%eqs)
       
    case(SOLVE_P)

       select case(solver%precond)
       case(PRECOND_JACOBI)
          !--------------------------------------------------------------------------
          ! Jacobi
          !--------------------------------------------------------------------------
          call preconjac (mat%npt,sol,ysa,alu,mat%jcof,mat%icof,mat%ndd,mat%nps,mat%kic,mat%nic)
          !--------------------------------------------------------------------------
       case(PRECOND_ILU)
          !--------------------------------------------------------------------------
          ! iLU
          !--------------------------------------------------------------------------
          call lusolke (mat%npt,sol,ysa,alu,mat%jcof,mat%icof,mat%ndd,mat%nps,mat%kic,mat%nic)
          !--------------------------------------------------------------------------
       case(PRECOND_SMG,PRECOND_PFMG)
          !--------------------------------------------------------------------------
          ! SMG or PFMG preconditionner
          !--------------------------------------------------------------------------
          call precond_p_with_SMG_PFMG_HYPRE(system%mat,solver,grid,sol,ysa)
          !--------------------------------------------------------------------------
       case DEFAULT
          call print_error_solver_choice(solver)
          stop
       end select

    case(SOLVE_UV,SOLVE_UVW)
       
       select case(solver%precond)
       case(PRECOND_JACOBI)
          !--------------------------------------------------------------------------
          ! Jacobi
          !--------------------------------------------------------------------------
          call preconjac (mat%npt,sol,ysa,alu,mat%jcof,mat%icof,mat%ndd,mat%nps,mat%kic,mat%nic)
          !--------------------------------------------------------------------------
       case(PRECOND_ILU)
          !--------------------------------------------------------------------------
          ! iLU
          !--------------------------------------------------------------------------
          call lusolke (mat%npt,sol,ysa,alu,mat%jcof,mat%icof,mat%ndd,mat%nps,mat%kic,mat%nic)
          !--------------------------------------------------------------------------
       case DEFAULT
          call print_error_solver_choice(solver)
          stop
       end select

    case(SOLVE_UVP,SOLVE_UVWP)
       
       select case(solver%precond)
       case(PRECOND_SMG,PRECOND_PFMG)
          !--------------------------------------------------------------------------
          ! Fully coupled case
          !--------------------------------------------------------------------------
          call precond_uvwp_with_SMG_PFMG_HYPRE(system,solver,grid,sol,ysa)
          !--------------------------------------------------------------------------
       case DEFAULT
          call print_error_solver_choice(solver)
          stop
       end select
       
    case DEFAULT
       call print_error_solver_choice(solver)
       stop
    end select
    
    call compute_time(TIMER_END,"[sub] precond")

  end subroutine precond


  subroutine preconjac (npt,sol,ysa,alu,jcof,icof,ndd,nps,kic,nic)
    implicit none
    !--------------------------------------------------------------------------
    ! Global variables
    !--------------------------------------------------------------------------
    integer                   :: npt,ndd,nps,nic
    integer, dimension(npt+1) :: icof
    integer, dimension(nps)   :: jcof
    integer, dimension(npt)   :: kic
    real(8), dimension(nps)   :: alu
    real(8), dimension(npt)   :: sol,ysa
    !--------------------------------------------------------------------------
    ! Local variables
    !--------------------------------------------------------------------------
    integer                   :: k,l,n
    !--------------------------------------------------------------------------

    call compute_time(TIMER_START,"[sub] preconjac")

    sol=0
    do n = 1,nic
       l = kic(n)
       sol(l) = ysa(l)
    end do

    call compute_time(TIMER_END,"[sub] preconjac")

  end subroutine preconjac


  subroutine lusolke (npt,sol,ysa,alu,jcof,icof,ndd,nps,kic,nic)
    implicit none
    !--------------------------------------------------------------------------
    ! Global variables
    !--------------------------------------------------------------------------
    integer                   :: npt,ndd,nps,nic
    integer, dimension(npt+1) :: icof
    integer, dimension(nps)   :: jcof
    integer, dimension(npt)   :: kic
    real(8), dimension(nps)   :: alu
    real(8), dimension(npt)   :: sol,ysa
    !--------------------------------------------------------------------------
    ! Local variables
    !--------------------------------------------------------------------------
    integer                   :: k,l,n
    !--------------------------------------------------------------------------

    call compute_time(TIMER_START,"[sub] lusolke")

    sol=0
    !--------------------------------------------------------------------------
    ! Forward solve
    !--------------------------------------------------------------------------
    do n = 1,nic
       l = kic(n)
       sol(l) = ysa(l)
    end do

    do n = 1,nic
       l = kic(n)
       do k = icof(l)+1,icof(l)+ndd-1,2
          sol(l) = sol(l) - alu(k)*sol(jcof(k))
       enddo
    enddo
    !--------------------------------------------------------------------------

    !--------------------------------------------------------------------------
    ! Backward solve
    !--------------------------------------------------------------------------
    do n = nic,1,-1
       l = kic(n)
       do k = icof(l)+2,icof(l)+ndd-1,2
          sol(l) = sol(l) - alu(k)*sol(jcof(k))
       enddo
       sol(l) = sol(l) / ( alu(icof(l)) + 1d-20 )
    enddo
    !--------------------------------------------------------------------------

    call compute_time(TIMER_END,"[sub] lusolke")

  end subroutine lusolke


  subroutine BiCGStab2(system,rhs,sol,solver,grid)
    use mod_parameters, only: rank
    use mod_struct_solver
    use mod_struct_grid
    use mod_solver_new_math
    use mod_solver_new_num
    implicit none
    !--------------------------------------------------------------------------
    ! global variables
    !--------------------------------------------------------------------------
    real(8), dimension(:), allocatable, intent(inout) :: rhs
    real(8), dimension(:), allocatable, intent(inout) :: sol
    type(system_t), intent(inout), target             :: system
    type(solver_t), intent(inout)                     :: solver
    type(grid_t), intent(in)                          :: grid
    !--------------------------------------------------------------------------
    ! local variables
    !--------------------------------------------------------------------------
    integer                                           :: k,k_tmp
    real(8)                                           :: norm_b_precond,norm_res_precond
    real(8)                                           :: norm_b_base,norm_res_base
    real(8)                                           :: alp,ome2,rho0
    real(8)                                           :: res,res_tmp
    real(8)                                           :: res_rel_base,res_rel_precond
    real(8), dimension(system%mat%npt)                :: sol_tmp
    real(8), dimension(system%mat%npt)                :: psa,qsa,rsa,ssa,tsa
    real(8), dimension(system%mat%npt)                :: usa,vsa,wsa,ysa
    real(8), dimension(system%mat%nps)                :: alu
    real(8), dimension(system%mat%npt)                :: rhs0
    type(matrix_t), pointer                           :: mat
    !--------------------------------------------------------------------------

    call compute_time(TIMER_START,"[sub] BiCGStab2")

    mat => system%mat
    
    !--------------------------------------------------------------------------
    ! Factorization
    !--------------------------------------------------------------------------
    alu = 0
    call factor(system,solver,grid,alu)
    !--------------------------------------------------------------------------

    !--------------------------------------------------------------------------
    ! initial residual values
    !--------------------------------------------------------------------------
    call pscalee(rhs,rhs,norm_b_base,mat%npt,mat%kic,mat%nic)
    norm_b_base = sqrt(norm_b_base+1d-40)
    !--------------------------------------------------------------------------
    call matveke(mat,sol,psa,solver,grid)
    psa = psa-rhs
    call pscalee(psa,psa,norm_res_base,mat%npt,mat%kic,mat%nic)
    norm_res_base = sqrt(norm_res_base+1d-40)
    !--------------------------------------------------------------------------

    !--------------------------------------------------------------------------
    ! Initialization
    !--------------------------------------------------------------------------
    psa  = 0
    qsa  = 0
    rsa  = 0
    ssa  = 0
    tsa  = 0
    usa  = 0
    vsa  = 0
    wsa  = 0
    ysa  = 0
    qsa  = rhs
    rhs0 = rhs
    !--------------------------------------------------------------------------
    
    !--------------------------------------------------------------------------
    ! preconditionning
    !--------------------------------------------------------------------------
    call precond(system,solver,grid,rhs,qsa,alu)
    call pscalee(rhs,rhs,norm_b_precond,mat%npt,mat%kic,mat%nic)
    norm_b_precond = sqrt(norm_b_precond+1d-40)

    call matveke(mat,sol,psa,solver,grid)
    call precond(system,solver,grid,tsa,psa,alu)

    rsa = rhs - tsa
    qsa = rsa
    psa = rsa

    call pscalee(rsa,rsa,norm_res_precond,mat%npt,mat%kic,mat%nic)
    norm_res_precond = sqrt(norm_res_precond+1d-40)

    if (norm_b_precond <= 1d-80) then
       write (*,*) 'error in CG. : second member is zero', norm_b_precond
       stop
    endif
    if (norm_res_precond <= 1d-80) then
       write (*,*) 'error in CG : residual is zero', norm_res_precond
       stop
    endif
    !--------------------------------------------------------------------------
    
    res_tmp = 1d20
    !--------------------------------------------------------------------------
    !bicgstab(2)
    !--------------------------------------------------------------------------
    rho0    = 1
    alp     = 0
    ome2    = 1
    !--------------------------------------------------------------------------
    if (solver%ires>0) then 
       if (rank==0) then
          write(*,*)
          write(*,101) "-----", "----------------","----------------", &
               & "----------------","----------------","----------------"
          write(*,*) "      solver%eqs =",solver%eqs
          write(*,*) " solver%res_case =",solver%res_case
          write(*,*) "           ||b|| =",norm_b_base
          write(*,*) "        ||iP.b|| =",norm_b_precond
          write(*,101) "-----", "----------------","----------------", &
               & "----------------","----------------","----------------"
          !write(*,*) " |A.x0-b|_2=",norm_res_base
          !write(*,101) "Iter", "|A.x-b|_2", "conv.rate", "|A.x-b|_2/|b|_2"
          write(*,101) "    ", "         ", "||A.x-b||", "              ", "||iP.(A.x-b)||", "          ratio"
          write(*,101) "    ", "         ", "---------", "              ", "--------------", "   relative res"
          write(*,101) "Iter", "||A.x-b||", "  ||b||  ", "||iP.(A.x-b)||", "   ||iP.b|    ", "   base/precond"
          !write(*,101) "Iter", "|A.x-b|_2", "|A.x-b|_2/|b|_2", "|iP.(A.x-b)|_2", "|iP.(A.x-b)|_2/|iP.b|_2"
          write(*,101) "-----", "----------------","----------------", &
               & "----------------","----------------","----------------"
          !write(*,102) 0,res*norm_b,res/norm_res_base,res
          write(*,102) 0,                         &
               & norm_res_base,                   &
               & norm_res_base/norm_b_base,       &
               & norm_res_precond,                &
               & norm_res_precond/norm_b_precond, &
               & (norm_res_base/norm_b_base)/(norm_res_precond/norm_b_precond)
       end if
    end if

    do k = 1,solver%it_max
       !--------------------------------------------------------------------------
       call bstak2e(system,sol,alu,psa,qsa,rsa,ssa,tsa,usa,vsa, &
            wsa,rho0,alp,ome2,solver,grid)
       !--------------------------------------------------------------------------

       !--------------------------------------------------------------------------
       ! preconditionned system residuals
       !--------------------------------------------------------------------------
       call pscalee(rsa,rsa,norm_res_precond,mat%npt,mat%kic,mat%nic)
       norm_res_precond = sqrt(norm_res_precond+1d-40)
       res_rel_precond  = norm_res_precond/norm_b_precond
       !--------------------------------------------------------------------------

       !--------------------------------------------------------------------------
       ! base system residuals
       !--------------------------------------------------------------------------
       call matveke(mat,sol,ysa,solver,grid)
       ysa  = ysa-rhs0
       call pscalee(ysa,ysa,norm_res_base,mat%npt,mat%kic,mat%nic)
       norm_res_base = sqrt(norm_res_base+1d-40)
       res_rel_base  = norm_res_base/norm_b_base
       !--------------------------------------------------------------------------

       !--------------------------------------------------------------------------
       ! residual for exit test
       !--------------------------------------------------------------------------
       select case(solver%res_case)
       case(0)
          res = max(res_rel_base,res_rel_precond)
       case(1)
          res = res_rel_base
       case default
          res = res_rel_precond
       end select
       !--------------------------------------------------------------------------
       if (res >= 1d10) then
          res = 1d10
       elseif (res < 1d-16) then 
          res = 1d-16
       end if
       !--------------------------------------------------------------------------
       
       !--------------------------------------------------------------------------
       ! save best solution
       !--------------------------------------------------------------------------
       if (res < res_tmp) then
          res_tmp = res
          sol_tmp = sol
          k_tmp   = k
       endif
       
       if (solver%ires>0) then
          if (modulo(k,solver%ires)==0) then
             if (rank==0) then
                write(*,102) k,                         &
                     & norm_res_base,                   &
                     & norm_res_base/norm_b_base,       &
                     & norm_res_precond,                &
                     & norm_res_precond/norm_b_precond, &
                     & (norm_res_base/norm_b_base)/(norm_res_precond/norm_b_precond)
             end if
          end if
       end if
       !--------------------------------------------------------------------------
       if (res <= solver%res_max) exit
       !--------------------------------------------------------------------------
       if (k>30 .and. res>1) exit
       !--------------------------------------------------------------------------
    enddo
    !--------------------------------------------------------------------------
    rhs           = rsa        ! residual exits in rhs
    sol           = sol_tmp
    solver%res    = res_tmp
    solver%it     = min(k,solver%it_max)
    solver%it_res = k_tmp
    !--------------------------------------------------------------------------

    if (solver%ires>0) then
       if (rank==0) then
          write(*,101) "-----", "----------------", "----------------", &
               & "----------------","----------------","----------------"
          write(*,*)
       end if
    end if
    !--------------------------------------------------------------------------

    call solver_comm_mpi(sol,solver,grid)

    call compute_time(TIMER_END,"[sub] BiCGStab2")

101 format(a5,1x,9(a15,1x))
102 format(i5,1x,9(1pe15.8,1x))

  end subroutine BiCGStab2


  subroutine bstak2e(system,sol,alu,psa,qsa,rsa,ssa,tsa, &
       usa,vsa,wsa,rho0,alp,ome2,solver,grid)
    use mod_solver_new_math
    use mod_struct_grid
    use mod_struct_solver
    implicit none
    !--------------------------------------------------------------------------
    ! Global variables
    !--------------------------------------------------------------------------
    type(system_t), intent(in), target :: system
    type(solver_t), intent(in)         :: solver
    type(grid_t), intent(in)           :: grid
    real(8), dimension(system%mat%nps) :: alu
    real(8), dimension(system%mat%npt) :: sol
    real(8), dimension(system%mat%npt) :: psa,qsa,rsa,ssa,tsa,usa,vsa,wsa
    real(8)                            :: rho0,alp,ome2
    !--------------------------------------------------------------------------
    ! Local variables
    !--------------------------------------------------------------------------
    integer                            :: l,n
    real(8)                            :: bet,gam,ome1,rho1,smu,snu,tau
    real(8)                            :: prec
    type(matrix_t), pointer            :: mat
    !--------------------------------------------------------------------------

    call compute_time(TIMER_START,"[sub] bstak2e")

    mat => system%mat
    
    !--------------------------------------------------------------------------
    prec = acos(-1d0)*1d-60
    rho0 = -ome2*rho0
    !--------------------------------------------------------------------------

    !--------------------------------------------------------------------------
    ! Even step
    !--------------------------------------------------------------------------

    !===============================================================================
    call pscalee (qsa,rsa,rho1,mat%npt,mat%kic,mat%nic)  

    bet  = alp * rho1 / rho0
    rho0 = rho1
    usa  = rsa - bet * usa 

    call matveke(mat,usa,psa,solver,grid)
    call precond(system,solver,grid,vsa,psa,alu)
    call pscalee(qsa,vsa,gam,mat%npt,mat%kic,mat%nic)

    alp = rho0 / gam
    rsa = rsa - alp * vsa 

    call matveke(mat,rsa,psa,solver,grid)
    call precond(system,solver,grid,ssa,psa,alu)

    sol = sol + alp * usa

    !--------------------------------------------------------------------------
    ! Odd step
    !--------------------------------------------------------------------------

    !===============================================================================
    call pscalee(qsa,ssa,rho1,mat%npt,mat%kic,mat%nic)

    bet  = alp * rho1 / rho0
    rho0 = rho1
    vsa  = ssa - bet * vsa

    call matveke(mat,vsa,psa,solver,grid)
    call precond(system,solver,grid,wsa,psa,alu)
    call pscalee(qsa,wsa,gam,mat%npt,mat%kic,mat%nic)

    alp = (rho0+prec) / (gam+prec)

    usa = rsa - bet * usa
    rsa = rsa - alp * vsa
    ssa = ssa - alp * wsa

    call matveke(mat,ssa,psa,solver,grid)
    call precond(system,solver,grid,tsa,psa,alu)

    !--------------------------------------------------------------------------
    !cgr(2) part
    !--------------------------------------------------------------------------

#if OPTIM_PSCALEE
    call pscalee5x(rsa,ssa,tsa,ome1,smu,snu,tau,ome2,mat%npt,mat%kic,mat%nic)
#else
    call pscalee(rsa,ssa,ome1,mat%npt,mat%kic,mat%nic)
    call pscalee(ssa,ssa,smu,mat%npt,mat%kic,mat%nic)
    call pscalee(ssa,tsa,snu,mat%npt,mat%kic,mat%nic)
    call pscalee(tsa,tsa,tau,mat%npt,mat%kic,mat%nic)
    call pscalee(rsa,tsa,ome2,mat%npt,mat%kic,mat%nic)
#endif 

    tau  = tau - snu*snu / smu
    ome2 = (ome2-(snu+prec)*(ome1+prec)/(smu+prec)) / (tau+prec)
    ome1 = (ome1-snu*ome2) / smu

    sol  = sol + ome1*rsa + ome2*ssa + alp*usa
    rsa  = rsa - ome1*ssa - ome2*tsa
    usa  = usa - ome1*vsa - ome2*wsa

    call compute_time(TIMER_END,"[sub] bstak2e")

  end subroutine bstak2e


  subroutine extract_Bp(mat,bp,grid)
    !--------------------------------------------------------------------------
    ! Extract the Block Bp of pressure gradient
    !--------------------------------------------------------------------------
    use mod_struct_grid
    use mod_struct_solver
    implicit none
    !--------------------------------------------------------------------------
    ! global variables
    !--------------------------------------------------------------------------
    type(matrix_t), intent(in)    :: mat
    type(matrix_t), intent(inout) :: bp
    type(grid_t), intent(in)      :: grid
    !--------------------------------------------------------------------------
    ! Local variables
    !--------------------------------------------------------------------------
    integer                       :: i,j,k,l,lvs
    !--------------------------------------------------------------------------

    call compute_time(TIMER_START,"[sub] extract_Bp")

    if (.not.allocated(bp%coef)) then
       bp%npt = grid%nb%tot%uvw
       bp%nps = 2*grid%nb%tot%uvw
       allocate(bp%coef(bp%nps))
       allocate(bp%icof(bp%npt+1))
       allocate(bp%jcof(bp%nps))
       bp%coef = 0
       bp%jcof = 0
       bp%icof = 0
    end if
    
    if (grid%dim==2) then

       l   = 1
       lvs = 10
       do j = grid%syus,grid%eyus
          do i = grid%sxus,grid%exus
             bp%coef(l)   = mat%coef(lvs) 
             bp%coef(l+1) = mat%coef(lvs+1)
             bp%jcof(l)   = mat%jcof(lvs)   - grid%nb%tot%uvw
             bp%jcof(l+1) = mat%jcof(lvs+1) - grid%nb%tot%uvw
             l                = l+2
             lvs              = lvs+11
          enddo
       enddo
       l   = 1+2*grid%nb%tot%u
       lvs = 10+11*grid%nb%tot%u
       do j = grid%syvs,grid%eyvs
          do i = grid%sxvs,grid%exvs
             bp%coef(l)   = mat%coef(lvs) 
             bp%coef(l+1) = mat%coef(lvs+1)
             bp%jcof(l)   = mat%jcof(lvs)   - grid%nb%tot%uvw
             bp%jcof(l+1) = mat%jcof(lvs+1) - grid%nb%tot%uvw
             l                = l+2
             lvs              = lvs+11
          enddo
       enddo

    else

       l   = 1
       lvs = 16
       do k = grid%szus,grid%ezus
          do j = grid%syus,grid%eyus
             do i = grid%sxus,grid%exus
                bp%coef(l)   = mat%coef(lvs) 
                bp%coef(l+1) = mat%coef(lvs+1)
                bp%jcof(l)   = mat%jcof(lvs)   - grid%nb%tot%uvw
                bp%jcof(l+1) = mat%jcof(lvs+1) - grid%nb%tot%uvw
                l                = l+2
                lvs              = lvs+17
             enddo
          enddo
       enddo
       l   = 1+2*grid%nb%tot%u
       lvs = 16+17*grid%nb%tot%u
       do k = grid%szvs,grid%ezvs
          do j = grid%syvs,grid%eyvs
             do i = grid%sxvs,grid%exvs
                bp%coef(l)   = mat%coef(lvs) 
                bp%coef(l+1) = mat%coef(lvs+1)
                bp%jcof(l)   = mat%jcof(lvs)   - grid%nb%tot%uvw
                bp%jcof(l+1) = mat%jcof(lvs+1) - grid%nb%tot%uvw
                l                = l+2
                lvs              = lvs+17
             enddo
          enddo
       enddo
       l   = 1+2*grid%nb%tot%uv
       lvs = 16+17*grid%nb%tot%uv
       do k = grid%szws,grid%ezws
          do j = grid%syws,grid%eyws
             do i = grid%sxws,grid%exws
                bp%coef(l)   = mat%coef(lvs) 
                bp%coef(l+1) = mat%coef(lvs+1)
                bp%jcof(l)   = mat%jcof(lvs)   - grid%nb%tot%uvw
                bp%jcof(l+1) = mat%jcof(lvs+1) - grid%nb%tot%uvw
                l                = l+2
                lvs              = lvs+17
             enddo
          enddo
       enddo
       
    endif

    bp%icof(1) = 1
    do l=2,grid%nb%tot%uvw+1
       bp%icof(l) = bp%icof(l-1)+2 
    enddo

    call compute_time(TIMER_END,"[sub] extract_Bp")

  end subroutine extract_Bp


  subroutine extract_Fuv(mat,fuv,grid)
    !--------------------------------------------------------------------------
    ! Extract Block Fuv of velocity
    !--------------------------------------------------------------------------
    use mod_struct_grid
    use mod_struct_solver
    implicit none
    !--------------------------------------------------------------------------
    ! global variables
    !--------------------------------------------------------------------------
    type(matrix_t), intent(in)    :: mat
    type(matrix_t), intent(inout) :: fuv
    type(grid_t), intent(in)      :: grid
    !--------------------------------------------------------------------------
    ! Local variables
    !--------------------------------------------------------------------------
    integer                       :: i,j,k,l,lvs
    !--------------------------------------------------------------------------

    call compute_time(TIMER_START,"[sub] extract_Fuv")

    if (.not.allocated(fuv%coef)) then
       fuv%npt = grid%nb%tot%u
       fuv%nps = 4*grid%nb%tot%u
       allocate(fuv%coef(fuv%nps))
       allocate(fuv%icof(fuv%npt+1))
       allocate(fuv%jcof(fuv%nps))
       fuv%coef = 0
       fuv%jcof = 0
       fuv%icof = 0
    end if
    
    if (grid%dim==2) then

       l   = 1
       lvs = 6
       do j = grid%syus,grid%eyus
          do i = grid%sxus,grid%exus
             fuv%coef(l)   = mat%coef(lvs)
             fuv%coef(l+1) = mat%coef(lvs+1)
             fuv%coef(l+2) = mat%coef(lvs+2)
             fuv%coef(l+3) = mat%coef(lvs+3)
             fuv%jcof(l)   = mat%jcof(lvs)   - grid%nb%tot%u
             fuv%jcof(l+1) = mat%jcof(lvs+1) - grid%nb%tot%u
             fuv%jcof(l+2) = mat%jcof(lvs+2) - grid%nb%tot%u
             fuv%jcof(l+3) = mat%jcof(lvs+3) - grid%nb%tot%u
             l             = l+4
             lvs           = lvs+11
          enddo
       enddo
       
    else

       l   = 1
       lvs = 8
       do k = grid%szus,grid%ezus
          do j = grid%syus,grid%eyus
             do i = grid%sxus,grid%exus
                fuv%coef(l)   = mat%coef(lvs)
                fuv%coef(l+1) = mat%coef(lvs+1)
                fuv%coef(l+2) = mat%coef(lvs+2)
                fuv%coef(l+3) = mat%coef(lvs+3)
                fuv%jcof(l)   = mat%jcof(lvs)   - grid%nb%tot%u
                fuv%jcof(l+1) = mat%jcof(lvs+1) - grid%nb%tot%u
                fuv%jcof(l+2) = mat%jcof(lvs+2) - grid%nb%tot%u
                fuv%jcof(l+3) = mat%jcof(lvs+3) - grid%nb%tot%u
                l             = l+4
                lvs           = lvs+17
             enddo
          enddo
       enddo
       
    endif

    do l = 1,4*grid%nb%tot%u
       if (abs(fuv%coef(l)) < THRESHOLD_EXTRACT) then  ! WTF 
          fuv%coef(l) = 0
       endif
    enddo
    
    fuv%icof(1) = 1
    do l = 2,grid%nb%tot%u+1
       fuv%icof(l) = fuv%icof(l-1)+4
    enddo
    
    call compute_time(TIMER_END,"[sub] extract_Fuv")
    
  end subroutine extract_Fuv


  subroutine extract_Fuw_Fvw(mat,fuvw,grid)
    !--------------------------------------------------------------------------
    ! Extract Block Fuw+Fvw of velocity
    !--------------------------------------------------------------------------
    use mod_struct_grid
    use mod_struct_solver
    implicit none
    !--------------------------------------------------------------------------
    ! global variables
    !--------------------------------------------------------------------------
    type(matrix_t), intent(in)    :: mat
    type(matrix_t), intent(inout) :: fuvw
    type(grid_t), intent(in)      :: grid
    !--------------------------------------------------------------------------
    ! Local variables
    !--------------------------------------------------------------------------
    integer                       :: i,j,k,l,lvs
    !--------------------------------------------------------------------------

    if (grid%dim==2) return

    call compute_time(TIMER_START,"[sub] extract_Fuw_Fvw")

    if (.not.allocated(fuvw%coef)) then
       fuvw%npt = grid%nb%tot%uv
       fuvw%nps = 4*grid%nb%tot%uv
       allocate(fuvw%coef(fuvw%nps))
       allocate(fuvw%icof(fuvw%npt+1))
       allocate(fuvw%jcof(fuvw%nps))
       fuvw%coef = 0
       fuvw%jcof = 0
    end if

    l = 1
    lvs = 12
    do k = grid%szus,grid%ezus
       do j = grid%syus,grid%eyus
          do i = grid%sxus,grid%exus
             fuvw%coef(l)   = mat%coef(lvs)
             fuvw%coef(l+1) = mat%coef(lvs+1)
             fuvw%coef(l+2) = mat%coef(lvs+2)
             fuvw%coef(l+3) = mat%coef(lvs+3)
             fuvw%jcof(l)   = mat%jcof(lvs)   - grid%nb%tot%uv
             fuvw%jcof(l+1) = mat%jcof(lvs+1) - grid%nb%tot%uv
             fuvw%jcof(l+2) = mat%jcof(lvs+2) - grid%nb%tot%uv
             fuvw%jcof(l+3) = mat%jcof(lvs+3) - grid%nb%tot%uv
             l              = l+4
             lvs            = lvs+17
          enddo
       enddo
    enddo

    l   = 1+4*grid%nb%tot%u
    lvs = 12+17*grid%nb%tot%u
    do k = grid%szvs,grid%ezvs
       do j = grid%syvs,grid%eyvs
          do i = grid%sxvs,grid%exvs
             fuvw%coef(l)   = mat%coef(lvs)
             fuvw%coef(l+1) = mat%coef(lvs+1)
             fuvw%coef(l+2) = mat%coef(lvs+2)
             fuvw%coef(l+3) = mat%coef(lvs+3)
             fuvw%jcof(l)   = mat%jcof(lvs)   - grid%nb%tot%uv
             fuvw%jcof(l+1) = mat%jcof(lvs+1) - grid%nb%tot%uv
             fuvw%jcof(l+2) = mat%jcof(lvs+2) - grid%nb%tot%uv
             fuvw%jcof(l+3) = mat%jcof(lvs+3) - grid%nb%tot%uv
             l              = l+4
             lvs            = lvs+17
          enddo
       enddo
    enddo

    do l = 1,4*grid%nb%tot%uv
       if (abs(fuvw%coef(l)) < THRESHOLD_EXTRACT) then ! WTF
          fuvw%coef(l) = 0
       endif
    enddo
    
    fuvw%icof(1) = 1
    do l = 2,grid%nb%tot%uv+1
       fuvw%icof(l) = fuvw%icof(l-1)+4
    enddo
    
    call compute_time(TIMER_END,"[sub] extract_Fuw_Fvw")
    
  end subroutine extract_Fuw_Fvw

  
  subroutine print_solver(solver)
    use mod_parameters, only: rank
    use mod_struct_solver
    implicit none
    !--------------------------------------------------------------------------
    ! Global variables
    !--------------------------------------------------------------------------
    type(solver_t), intent(in) :: solver 
    !--------------------------------------------------------------------------
    ! Local variables
    !--------------------------------------------------------------------------
    !--------------------------------------------------------------------------

    call compute_time(TIMER_START,"[sub] print_solver")

    if (rank==0) then
       if (solver%print) then 
          select case(solver%type)
          case(0)
             write(*,*) trim(adjustl(NAME_SOLVER(solver%type)))
          case DEFAULT
             write(*,*) &
                  & trim(adjustl(NAME_SOLVER(solver%type)))//"/"//trim(adjustl(NAME_PRECON(solver%precond)))
          end select
          write(*,101) "  solver%eqs     = ", solver%eqs
          write(*,101) "  solver%type    = ", solver%type
          write(*,101) "  solver%precond = ", solver%precond
          write(*,101) "  solver%it      = ", solver%it
          write(*,101) "  solver%it_res  = ", solver%it_res
          write(*,102) "  solver%res     = ", solver%res
       end if
    end if

    call compute_time(TIMER_END,"[sub] print_solver")

101 format(a,i13)
102 format(a,(1pe13.6))

  end subroutine print_solver
  

  subroutine print_system(mat,rhs)
    use mod_mpi
    use mod_parameters, only: rank
    use mod_struct_solver
    implicit none
    !--------------------------------------------------------------------------
    ! Global variables
    !--------------------------------------------------------------------------
    type(matrix_t), intent(in)                     :: mat
    real(8), dimension(:), allocatable, intent(in) :: rhs 
    !--------------------------------------------------------------------------
    ! Local variables
    !--------------------------------------------------------------------------
    integer                                        :: l,lts
    character(len=100)                             :: fmt1
    !--------------------------------------------------------------------------

    if (mat%impp>0) then
       fmt1='(a,10i12)'
       write(fmt1(4:5),'(i2.2)') mat%kdv
       !write(100000+rank,*)    'Matrice Eq.  ',mat%eqs
       write(100000+rank,fmt1) 'coefv        ',(l,l=1,mat%kdv)
       do l=1,mat%npt
          fmt1='(1x,i9,e12.3,14e12.3)'
          write(fmt1(14:15),'(i2.2)') mat%icof(l+1)-mat%icof(l)
          write(100000+rank,fmt1) l,rhs(l),(mat%coef(lts),lts=mat%icof(l),mat%icof(l+1)-1)
       end do
       call mpi_barrier(comm3D,mpi_code)
       stop
    end if

  end subroutine print_system
  
end module mod_solver_new
