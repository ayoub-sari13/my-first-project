!========================================================================
!
!                   FUGU Version 1.0.0
!
!========================================================================
!**
!**   NAME       : DefMumps.f90
!**
!**   AUTHOR     : Benoit Trouette
!**
!**   FUNCTION   : MUMPS Dependencies
!**
!**   DATES      : Version 1.0.0  : from : fev, 2021
!**
!========================================================================

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#include "precomp.h"
module mod_hypre_dep
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  implicit none

#if HYPRE
  integer :: ierr_hypre
  
  integer*8 Au,Av,Aw,Ap
  integer*8 Sau,Sav,Saw,Sap
  integer*8 solveru,solverv,solverw,solverp

  ! partition number always 1
  integer :: npart_hypre=1

  integer*8 graph_hypre_u,graph_hypre_v,graph_hypre_p,graph_hypre_w
  integer*8 grid_hypre_u,grid_hypre_v,grid_hypre_p,grid_hypre_w
  integer*8 Struct_hypre_xu,Struct_hypre_yu
  integer*8 Struct_hypre_xv,Struct_hypre_yv
  integer*8 Struct_hypre_yw,Struct_hypre_xw
  integer*8 Struct_hypre_xp,Struct_hypre_yp

  integer*8 Aphi
#endif


end module mod_hypre_dep
