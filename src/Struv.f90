FUNCTION  STVH0(X)
  !       =============================================
  !       Purpose: Compute Struve function H0(x)
  !       Input :  x   --- Argument of H0(x) ( x ò 0 )
  !       Output:  SH0 --- H0(x)
  !       =============================================
  IMPLICIT real(8) (A-H,O-Z)
  REAL *8 STVH0
  PII=3.141592653589793D0
  S=1.0D0
  R=1.0D0
  IF (X.LE.20.0D0) THEN
     A0=2.0*X/PII
     DO 10 K=1,60
        R=-R*X/(2.0D0*K+1.0D0)*X/(2.0D0*K+1.0D0)
        S=S+R
        IF (DABS(R).LT.DABS(S)*1.0D-12) GO TO 15
10      CONTINUE
15      STVH0=A0*S
     ELSE
        KM=INT(.5*(X+1.0))
        IF (X.GE.50.0) KM=25
        DO 20 K=1,KM
           R=-R*((2.0D0*K-1.0D0)/X)**2
           S=S+R
           IF (DABS(R).LT.DABS(S)*1.0D-12) GO TO 25
20         CONTINUE
25         T=4.0D0/X
           T2=T*T
           P0=((((-.37043D-5*T2+.173565D-4)*T2-.487613D-4)*T2+.17343D-3)*T2-.1753062D-2)*T2+.3989422793D0
           Q0=T*(((((.32312D-5*T2-.142078D-4)*T2+.342468D-4)* T2-.869791D-4)*T2+.4564324D-3)*T2-.0124669441D0)
           TA0=X-.25D0*PII
           BY0=2.0D0/DSQRT(X)*(P0*DSIN(TA0)+Q0*DCOS(TA0))
           STVH0=2.0D0/(PII*X)*S+BY0
        ENDIF
        RETURN
     END

FUNCTION  STVH1(X)
  !       =============================================
  !       Purpose: Compute Struve function H1(x)
  !       Input :  x   --- Argument of H1(x) ( x ò 0 )
  !       Output:  SH1 --- H1(x)
  !       =============================================
  IMPLICIT real(8) (A-H,O-Z)
  REAL *8 STVH1
  PII=3.141592653589793D0
  R=1.0D0
  IF (X.LE.20.0D0) THEN
     S=0.0D0
     A0=-2.0D0/PII
     DO 10 K=1,60
        R=-R*X*X/(4.0D0*K*K-1.0D0)
        S=S+R
        IF (DABS(R).LT.DABS(S)*1.0D-12) GO TO 15
10      CONTINUE
15      STVH1=A0*S
     ELSE
        S=1.0D0
        KM=INT(.5*X)
        IF (X.GT.50.D0) KM=25
        DO 20 K=1,KM
           R=-R*(4.0D0*K*K-1.0D0)/(X*X)
           S=S+R
           IF (DABS(R).LT.DABS(S)*1.0D-12) GO TO 25
20         CONTINUE
25         T=4.0D0/X
           T2=T*T
           P1=((((.42414D-5*T2-.20092D-4)*T2+.580759D-4)*T2-.223203D-3)*T2+.29218256D-2)*T2+.3989422819D0
           Q1=T*(((((-.36594D-5*T2+.1622D-4)*T2-.398708D-4)*T2+.1064741D-3)*T2-.63904D-3)*T2+.0374008364D0)
           TA1=X-.75D0*PII
           BY1=2.0D0/DSQRT(X)*(P1*DSIN(TA1)+Q1*DCOS(TA1))
           STVH1=2.0/PII*(1.0D0+S/(X*X))+BY1
        ENDIF
        RETURN
     END
