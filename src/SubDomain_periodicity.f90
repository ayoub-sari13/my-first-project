!===============================================================================
module Bib_VOFLag_SubDomain_periodicity
   !===============================================================================
   contains
   !-----------------------------------------------------------------------------  
   !> @author jorge cesar brandle de motta, amine chadil
   ! 
   !> @brief initialise les distances a ajouter ou retrancher au centre des particules
   !! pour creer leurs copies en cas de periodicite
   !-----------------------------------------------------------------------------
   subroutine SubDomain_periodicity
   !===============================================================================
   !modules
   !===============================================================================
   !-------------------------------------------------------------------------------
   !Modules de definition des structures et variables => src/mod/module_...f90
   !-------------------------------------------------------------------------------
   use Module_VOFLag_ParticlesDataStructure
   use Module_VOFLag_SubDomain_Data,         only : m_period,m_coords,m_dims
   use mod_Parameters,                       only : rank,nproc,dim,deeptracking,&
                                                  xmin,xmax,ymin,ymax,zmin,zmax
   !-------------------------------------------------------------------------------

   !===============================================================================
   !declarations des variables
   !===============================================================================
   implicit none
   !-------------------------------------------------------------------------------
   !variables globales
   !-------------------------------------------------------------------------------
   !-------------------------------------------------------------------------------
   !variables locales 
   !-------------------------------------------------------------------------------
   integer                                   :: dm,dm2
   double precision, dimension(dim)          :: coord_deb,coord_fin
   !-------------------------------------------------------------------------------

   !-------------------------------------------------------------------------------
   if (deeptracking) write(*,*) 'entree SubDomain_periodicity'
   !-------------------------------------------------------------------------------

   !-------------------------------------------------------------------------------
   !Initialisation
   !-------------------------------------------------------------------------------
   coord_deb(1)=xmin
   coord_deb(2)=ymin
   coord_fin(1)=xmax
   coord_fin(2)=ymax
   if (dim.eq.3) then
      coord_deb(3)=zmin
      coord_fin(3)=zmax
   endif
   rank_nbtimes = 1
   rank_symper = 0.0d0

   do dm=1,dim
      if (m_period(dm)) then
         if (m_coords(dm) .eq. m_dims(dm)-1) then
            !-------------------------------------------------------------------------------
            !Si le proc courant (ME) est le dernier bloc dans la direction DM
            !-------------------------------------------------------------------------------
            !! EX. 
            !!
            !! --> direction DM
            !! |..ME(bis)..|----------|----------|----ME----|
            !!
            !!             <---------------------------------
            !!                       -(XLU-XLL)             
            !! ME etant le proc courant
            rank_nbtimes = rank_nbtimes + 1
            rank_symper(rank_nbtimes,:) = 0.0d0
            rank_symper(rank_nbtimes,dm) = - (coord_fin(dm) - coord_deb(dm))
         end if

         if ( m_coords(dm) .eq. 0 ) then 
            !-------------------------------------------------------------------------------
            !Si ME est le premier bloc dans la direction DM
            !-------------------------------------------------------------------------------
            !! EX. 
            !!
            !! --> direction DM
            !!             |----ME----|----------|----------|..ME(bis)..|
            !!
            !!             --------------------------------->
            !!                       (XLU-XLL) 
            rank_nbtimes = rank_nbtimes + 1
            rank_symper(rank_nbtimes,:) = 0.0d0
            rank_symper(rank_nbtimes,dm) = (coord_fin(dm) - coord_deb(dm) )
         end if
         !-------------------------------------------------------------------------------
         !a ce stade les six cotes du domaine sont traites
         !-------------------------------------------------------------------------------

         do dm2=dm+1,dim
            if (m_period(dm2)) then
               if ((m_coords(dm) .eq. m_dims(dm)-1).and.(m_coords(dm2).eq.m_dims(dm2)-1) ) then 
                  !-------------------------------------------------------------------------------
                  !Si ME est le dernier bloc selon DM et le dernier bloc selon DM2
                  !-------------------------------------------------------------------------------
                  !! EX
                  !!  DM2
                  !!   ^
                  !!   |
                  !!   |--> DM
                  !!
                  !!          |----------|----------|----------|
                  !!          |          |          |          |
                  !!          |          |          |   ME     |
                  !!          |          |          |          |
                  !!          |----------|----------|----------|
                  !!          |          |          |          |
                  !!          |          |          |          |
                  !!          |          |          |          |
                  !!          |----------|----------|----------|
                  !!          |          |          |          |
                  !!          |          |          |          |
                  !!          |          |          |          |
                  !! .........|----------|----------|----------|
                  !!          .
                  !!  ME(bis) .
                  !!          .
                  !!
                  !!
                  !!
                  !!
                  rank_nbtimes = rank_nbtimes + 1
                  rank_symper(rank_nbtimes,:) = 0.0d0
                  rank_symper(rank_nbtimes,dm) = - (coord_fin(dm) - coord_deb(dm) )
                  rank_symper(rank_nbtimes,dm2) = - (coord_fin(dm2) - coord_deb(dm2) )
               end if

               if ((m_coords(dm) .eq.0).and.(m_coords(dm2).eq.m_dims(dm2)-1) ) then
                  !-------------------------------------------------------------------------------
                  !Si ME est le premier bloc selon DM et le dernier bloc selon DM2
                  !-------------------------------------------------------------------------------
                  rank_nbtimes = rank_nbtimes + 1
                  rank_symper(rank_nbtimes,:) = 0.0d0
                  rank_symper(rank_nbtimes,dm) =  (coord_fin(dm) - coord_deb(dm) )
                  rank_symper(rank_nbtimes,dm2) = - (coord_fin(dm2) - coord_deb(dm2) )
               end if

               if ((m_coords(dm) .eq. m_dims(dm)-1).and.(m_coords(dm2).eq.0) ) then
                  !-------------------------------------------------------------------------------
                  !Si ME est le dernier bloc selon DM et le premier bloc selon DM2
                  !-------------------------------------------------------------------------------
                  rank_nbtimes = rank_nbtimes + 1
                  rank_symper(rank_nbtimes,:) = 0.0d0
                  rank_symper(rank_nbtimes,dm) = - (coord_fin(dm) - coord_deb(dm) )
                  rank_symper(rank_nbtimes,dm2) =  (coord_fin(dm2) - coord_deb(dm2) )
               end if

               if ((m_coords(dm) .eq. 0).and.(m_coords(dm2).eq.0) ) then
                  !-------------------------------------------------------------------------------
                  !Si ME est le premier bloc selon DM et le premier bloc selon DM2
                  !-------------------------------------------------------------------------------
                  rank_nbtimes = rank_nbtimes + 1
                  rank_symper(rank_nbtimes,:) = 0.0d0
                  rank_symper(rank_nbtimes,dm) =  (coord_fin(dm) - coord_deb(dm) )
                  rank_symper(rank_nbtimes,dm2) =  (coord_fin(dm2) - coord_deb(dm2) )
               end if
            end if
         end do
      end if
   end do
   !-------------------------------------------------------------------------------
   !a ce stade les six cotes du domaine sont traites ainsi que les copies restantes 
   !dans le meme plan (partage d'une arete en 3D ou d'un coin en 2D)
   !-------------------------------------------------------------------------------


   if (m_period(1).and.m_period(2).and.m_period(3).and.(dim.eq.3)) then
      if ((m_coords(1) .eq. m_dims(1)-1).and.(m_coords(2).eq.m_dims(2)-1).and.(m_coords(3).eq.m_dims(3)-1)) then
         !-------------------------------------------------------------------------------
         !si ME est le dernier bloc selon les trois directions
         !-------------------------------------------------------------------------------
         rank_nbtimes = rank_nbtimes + 1
         rank_symper(rank_nbtimes,1) = - (coord_fin(1) - coord_deb(1))
         rank_symper(rank_nbtimes,2) = - (coord_fin(2) - coord_deb(2))
         rank_symper(rank_nbtimes,3) = - (coord_fin(3) - coord_deb(3))
      end if

      if ((m_coords(1).eq.0).and.(m_coords(2).eq.m_dims(2)-1).and.(m_coords(3).eq.0)) then
         !-------------------------------------------------------------------------------
         !si ME est le dernier  bloc selon la deuxieme direction et le premier ailleurs
         !-------------------------------------------------------------------------------
         rank_nbtimes = rank_nbtimes + 1
         rank_symper(rank_nbtimes,1) =   (coord_fin(1) - coord_deb(1))
         rank_symper(rank_nbtimes,2) = - (coord_fin(2) - coord_deb(2))
         rank_symper(rank_nbtimes,3) =   (coord_fin(3) - coord_deb(3))
      end if

      if ((m_coords(1) .eq. m_dims(1)-1).and.(m_coords(2).eq.m_dims(2)-1).and.(m_coords(3).eq.0)) then
         !-------------------------------------------------------------------------------
         !Si ME est le premier bloc selon la dernier direction et le dernier ailleurs
         !-------------------------------------------------------------------------------
         rank_nbtimes = rank_nbtimes + 1
         rank_symper(rank_nbtimes,1) = - (coord_fin(1) - coord_deb(1))
         rank_symper(rank_nbtimes,2) = - (coord_fin(2) - coord_deb(2))
         rank_symper(rank_nbtimes,3) =   (coord_fin(3) - coord_deb(3))
      end if

      if (( m_coords(1).eq.0).and.(m_coords(2).eq.0).and.(m_coords(3).eq.0)) then
         !-------------------------------------------------------------------------------
         !si ME est le premier bloc selon les trois directions
         !-------------------------------------------------------------------------------
         rank_nbtimes = rank_nbtimes + 1
         rank_symper(rank_nbtimes,1) =   (coord_fin(1) - coord_deb(1))
         rank_symper(rank_nbtimes,2) =   (coord_fin(2) - coord_deb(2))
         rank_symper(rank_nbtimes,3) =   (coord_fin(3) - coord_deb(3))
      end if

      if ((m_coords(1).eq.m_dims(1)-1).and.(m_coords(2).eq.0).and.(m_coords(3).eq.0)) then
         !-------------------------------------------------------------------------------
         !Si ME est le dernier bloc selon la premiere direction et le premier ailleurs
         !-------------------------------------------------------------------------------
         rank_nbtimes = rank_nbtimes + 1
         rank_symper(rank_nbtimes,1) = - (coord_fin(1) - coord_deb(1))
         rank_symper(rank_nbtimes,2) =   (coord_fin(2) - coord_deb(2))
         rank_symper(rank_nbtimes,3) =   (coord_fin(3) - coord_deb(3))
      end if

      if ((m_coords(1).eq.0).and.(m_coords(2).eq.m_dims(2)-1).and.(m_coords(3).eq.m_dims(3)-1)) then
         !-------------------------------------------------------------------------------
         !Si ME est le premier bloc selon la premiere direction et le dernier ailleurs
         !-------------------------------------------------------------------------------
         rank_nbtimes = rank_nbtimes + 1
         rank_symper(rank_nbtimes,1) =   (coord_fin(1) - coord_deb(1))
         rank_symper(rank_nbtimes,2) = - (coord_fin(2) - coord_deb(2))
         rank_symper(rank_nbtimes,3) = - (coord_fin(3) - coord_deb(3))
      end if

      if ((m_coords(1).eq.m_dims(1)-1).and.(m_coords(2).eq.0).and.(m_coords(3).eq.m_dims(3)-1)) then
         !-------------------------------------------------------------------------------
         !Si ME est le dernier bloc selon la deuxieme direction et le premier ailleurs
         !-------------------------------------------------------------------------------
         rank_nbtimes = rank_nbtimes + 1
         rank_symper(rank_nbtimes,1) = - (coord_fin(1) - coord_deb(1))
         rank_symper(rank_nbtimes,2) =   (coord_fin(2) - coord_deb(2))
         rank_symper(rank_nbtimes,3) = - (coord_fin(3) - coord_deb(3))
      end if

      if ((m_coords(1).eq.0).and.(m_coords(2).eq.0).and.(m_coords(3).eq.m_dims(3)-1)) then
         !-------------------------------------------------------------------------------
         !Si ME est le dernier bloc selon la troisieme direction et le premier ailleurs
         !-------------------------------------------------------------------------------
         rank_nbtimes = rank_nbtimes + 1
         rank_symper(rank_nbtimes,1) =   (coord_fin(1) - coord_deb(1))
         rank_symper(rank_nbtimes,2) =   (coord_fin(2) - coord_deb(2))
         rank_symper(rank_nbtimes,3) = - (coord_fin(3) - coord_deb(3))
      end if
   end if

   !-------------------------------------------------------------------------------
   if (deeptracking) write(*,*) 'sortie SubDomain_periodicity'
   !-------------------------------------------------------------------------------
   end subroutine SubDomain_periodicity

   !===============================================================================
end module Bib_VOFLag_SubDomain_periodicity
!===============================================================================
  


