!-----------------------------------------------------------------------------  
!> @author amine chadil
!
!> @brief ce module contient les donnees 
!
!> @date 4/04/2013   
!-----------------------------------------------------------------------------
!===============================================================================
module Module_VOFLag_SubDomain_Data
  !===============================================================================
  integer, dimension(3),save              :: m_coords,m_dims
  logical, dimension(3), save             :: m_period
  double precision,  dimension(2,3), save :: s_domaines_int
  integer, dimension(7),save              :: m_vois
  !  rank_voisins(1:nb_voisins,1) donne le rang du voisin
  !  rank_voisins(1:nb_voisins,2) dit le type de voisin
  !                                0  rang
  !                                1  gauche
  !                                2  droite    
  !                                3  dessous 
  !                                4  dessus
  !                                5  arriere   
  !                                6  avant    
  !          1 + 3                 7  gauche -dessous
  !          1 + 4                 8  gauche -dessus 
  !          1 + 5                 9  gauche -arriere 
  !          1 + 6                 10 gauche -avant
  !          2 + 3                 11 droite -dessous
  !          2 + 4                 12 droite -dessus
  !          2 + 5                 13 droite -arriere
  !          2 + 6                 14 droite -avant
  !          3 + 5                 15 dessous-arriere
  !          3 + 6                 16 dessous-avant
  !          4 + 5                 17 dessus -arriere
  !          4 + 6                 18 dessus -avant
  !          7 + 5                 19 gauche -dessous-arriere
  !          7 + 6                 20 gauche -dessous-avant
  !          8 + 5                 21 gauche -dessus -arriere
  !          8 + 6                 22 gauche -dessus -avant
  !          11+ 5                 23 droite -dessous-arriere
  !          11+ 6                 24 droite -dessous-avant
  !          12+ 5                 25 droite -dessus -arriere
  !          12+ 6                 26 droite -dessus -avant
  !                               -[1-26] les memes mais avec passage par periodique 
  !   rank_voisins(1:nb_voisins,3) donne le nbt a chercher pour les particules

  integer, save                 :: nb_voisins
  integer, dimension(27,3),save :: rank_voisins

  !===============================================================================
end module Module_VOFLag_SubDomain_Data
!===============================================================================