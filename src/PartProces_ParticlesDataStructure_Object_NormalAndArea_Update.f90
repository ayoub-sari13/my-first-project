!===============================================================================
module Bib_VOFLag_ParticlesDataStructure_Object_NormalAndArea_Update
    !===============================================================================
    contains
    !-----------------------------------------------------------------------------  
    !> @author amine chadil
    ! 
    !> @brief donne la normale à un triangle en 3D ou à un segment en 2D   
    !
    !> @param [in] obj      : la structure contenant les caracteristiques des objets (
    !! maillage des peaux des particules).
    !> @param [in] 
    !-----------------------------------------------------------------------------
    subroutine ParticlesDataStructure_Object_NormalAndArea_Update(vl,np)
        !===============================================================================
        !modules
        !===============================================================================
        !-------------------------------------------------------------------------------
        !Modules de definition des structures et variables => src/mod/module_...f90
        !-------------------------------------------------------------------------------
        use Module_VOFLag_ParticlesDataStructure 
        use mod_Parameters,                      only : dim
        !-------------------------------------------------------------------------------

        !===============================================================================
        !declarations des variables
        !===============================================================================
        implicit none
        !-------------------------------------------------------------------------------
        !variables globales
        !-------------------------------------------------------------------------------
        type(struct_vof_lag), intent(inout)                     :: vl
        integer             , intent(in)                        :: np
        !-------------------------------------------------------------------------------
        !variables locales 
        !-------------------------------------------------------------------------------
        real(8), dimension(dim,dim,vl%objet(np)%kkl)            :: lag
        real(8), dimension(dim,vl%objet(np)%kkl)                :: normal
        real(8), dimension(dim-1,dim)                           :: tang
        real(8), dimension(vl%objet(np)%kkl)                    :: area
        integer                                                 :: lp,kkl
        !-------------------------------------------------------------------------------

        !-------------------------------------------------------------------------------
        ! Initialisation
        !-------------------------------------------------------------------------------
        lag = vl%objet(np)%lag_for_all_copies 
        kkl = vl%objet(np)%kkl
        normal = 0.D0
        area = 0.D0  

        !-------------------------------------------------------------------------------
        ! calcul de la normale sortante (de la particule) de l'elements lp 
        !-------------------------------------------------------------------------------
        do lp=1,kkl
          if (dim .eq. 2) then
            tang(1,1)=(lag(1,2,lp)-lag(1,1,lp))
            tang(1,2)=(lag(2,2,lp)-lag(2,1,lp))
            normal(1,lp)=tang(1,2); normal(2,lp)=-tang(1,1)
            area(lp)=sqrt((lag(1,1,lp)-lag(1,2,lp))**2.D0+(lag(2,1,lp)-lag(2,2,lp))**2.D0)
            normal(:,lp)=normal(:,lp)/sqrt(normal(1,lp)**2+normal(2,lp)**2+1D-40)
          else
            tang(1,1)=lag(1,3,lp)-lag(1,1,lp)
            tang(1,2)=lag(2,3,lp)-lag(2,1,lp)
            tang(1,3)=lag(3,3,lp)-lag(3,1,lp)
            tang(2,1)=lag(1,2,lp)-lag(1,1,lp)
            tang(2,2)=lag(2,2,lp)-lag(2,1,lp)
            tang(2,3)=lag(3,2,lp)-lag(3,1,lp)

            normal(1,lp)=tang(1,2)*tang(2,3)-tang(1,3)*tang(2,2)
            normal(2,lp)=tang(1,3)*tang(2,1)-tang(1,1)*tang(2,3)
            normal(3,lp)=tang(1,1)*tang(2,2)-tang(1,2)*tang(2,1)
            area(lp)=0.5D0*sqrt(normal(1,lp)**2.D0+normal(2,lp)**2.D0+normal(3,lp)**2.D0)  
            normal(:,lp)=normal(:,lp)/sqrt(normal(1,lp)**2+normal(2,lp)**2+normal(3,lp)**2+1D-40)
          endif
        enddo

        if (allocated(vl%objet(np)%normal)) deallocate(vl%objet(np)%normal)
        allocate(vl%objet(np)%normal(dim,kkl))
        if (allocated(vl%objet(np)%area)) deallocate(vl%objet(np)%area)
        allocate(vl%objet(np)%area(kkl))
        vl%objet(np)%normal = normal
        vl%objet(np)%area   = area

    end subroutine ParticlesDataStructure_Object_NormalAndArea_Update

    !===============================================================================
end module Bib_VOFLag_ParticlesDataStructure_Object_NormalAndArea_Update
!===============================================================================
    



