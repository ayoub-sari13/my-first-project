!========================================================================
!
!                   FUGU Version 1.0.0
!
!========================================================================
!
! Copyright  Stéphane Vincent
!
! stephane.vincent@u-pem.fr          straightparadigm@gmail.com
!
! This file is part of the FUGU Fortran library, a set of Computational 
! Fluid Dynamics subroutines devoted to the simulation of multi-scale
! multi-phase flows for resolved scale interfaces (liquid/gas/solid). 
! FUGU uses the initial developments of DyJeAT and SHALA codes from 
! Jean-Luc Estivalezes (jean-luc.estivalezes@onera.fr) and 
! Frédéric Couderc (couderc@math.univ-toulouse.fr).
!
!========================================================================
!**
!**   NAME       : MPI.f90
!**
!**   AUTHOR     : Stéphane Vincent
!**
!**   FUNCTION   : module for MPI variables of FUGU software
!**
!**   DATES      : Version 1.0.0  : from : january, 16, 2017
!**
!========================================================================

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


#define MPI_SYNCH 1
#include "precomp.h"

module mod_mpi
  use mod_Parameters, only:                    &
       & nproc,rank,mpi_dim,slx,sly,slz,       &  
       & Periodic,dim,nx,ny,nz,gx,gy,gz,       &
       & ex,sx,ey,sy,ez,sz,                    &
       & sxu,exu,syu,eyu,szu,ezu,              &
       & sxv,exv,syv,eyv,szv,ezv,              &
       & sxw,exw,syw,eyw,szw,ezw,              &
       & exs,sxs,eys,sys,ezs,szs,              &
       & sxus,exus,syus,eyus,szus,ezus,        &
       & sxvs,exvs,syvs,eyvs,szvs,ezvs,        &
       & sxws,exws,syws,eyws,szws,ezws,        &
       & gex,gsx,gey,gsy,gez,gsz,              &
       & gsxu,gexu,gsyu,geyu,gszu,gezu,        &
       & gsxv,gexv,gsyv,geyv,gszv,gezv,        &
       & gsxw,gexw,gsyw,geyw,gszw,gezw,        &
       & gxs,gys,gzs
  implicit none

  include 'mpif.h'

  !--------------------------------------------------------------
  ! 3D cartesian topology communicator 
  !--------------------------------------------------------------
  integer                     :: comm3d
  !--------------------------------------------------------------

  !--------------------------------------------------------------
  ! coordinates
  !--------------------------------------------------------------
  integer, dimension(3)       :: coords
  !--------------------------------------------------------------

  !--------------------------------------------------------------
  ! neighbouring
  !--------------------------------------------------------------
  integer, parameter          :: nb_ngbr=6
  integer, parameter          :: xb=1,xf=2
  integer, parameter          :: yb=3,yf=4
  integer, parameter          :: zb=5,zf=6
  integer, dimension(nb_ngbr) :: ngbr
  logical, dimension(3)       :: mpi_periodicity
  !--------------------------------------------------------------

  !--------------------------------------------------------------
  ! derived types
  !--------------------------------------------------------------
  integer                     :: xface_u,xface_v,xface_w,xface_p
  integer                     :: yface_u,yface_v,yface_w,yface_p
  integer                     :: zface_u,zface_v,zface_w,zface_p
  integer                     :: type_particle
  !--------------------------------------------------------------

  !--------------------------------------------------------------
  ! some constants
  !--------------------------------------------------------------
  integer                     :: code,mpi_code
  logical, parameter          :: mpi_print=.false.
  logical                     :: mpi_chief=.false.
  !--------------------------------------------------------------

contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  subroutine initialization_MPI
    !--------------------------------------------------------------
    ! global variables
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! local  variables
    !--------------------------------------------------------------

    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! Init
    !--------------------------------------------------------------
    call mpi_init(code)

    if (code/=0.and.mpi_print) then 
       write(*,*) "mpi_init(code)"
       write(*,*) "code=",code
       write(*,*)'--------------------------------------------------------------'
    end if
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! Who is who
    !--------------------------------------------------------------
    call mpi_comm_rank(mpi_comm_world,rank,code)

    if (rank==0) mpi_chief=.true.

!!$    if (mpi_print .and. mpi_chief) then 
!!$       write(*,*) "call mpi_comm_rank(mpi_comm_world,rank,code)"
!!$       write(*,*) "rank=",rank
!!$       write(*,*) "code=",code
!!$       write(*,*)'--------------------------------------------------------------'
!!$    end if
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! total number of process 
    !--------------------------------------------------------------
    call mpi_comm_size(mpi_comm_world,nproc,code)

!!$    if (mpi_print .and. mpi_chief) then 
!!$       write(*,*) "call mpi_comm_size(mpi_comm_world,nproc,code)"
!!$       write(*,*) "nproc=",nproc
!!$       write(*,*) "code=",code
!!$       write(*,*)'--------------------------------------------------------------'
!!$    end if
    !--------------------------------------------------------------
  end subroutine initialization_MPI


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!



  subroutine topology_MPI
    !--------------------------------------------------------------
    ! global variables
    !--------------------------------------------------------------
    !--------------------------------------------------------------
    ! local  variables
    !--------------------------------------------------------------
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! Init
    !--------------------------------------------------------------
    mpi_periodicity=Periodic
    !--------------------------------------------------------------


    call create_topology_MPI

    call neighbouring_MPI

    call derived_types(slx,sly,slz)

  end subroutine topology_MPI


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


  subroutine finalization_mpi
    !--------------------------------------------------------------
    ! global variables
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! local  variables
    !--------------------------------------------------------------
    integer :: code
    !--------------------------------------------------------------

    call mpi_barrier(comm3d,code)
    call mpi_barrier(mpi_comm_world,code)
    call mpi_finalize(code)
  end subroutine finalization_mpi

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  subroutine create_topology_MPI
    !--------------------------------------------------------------
    ! global variables
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! local  variables
    !--------------------------------------------------------------
    integer :: code
    integer :: itmp1,itmp2,mitmp1,mitmp2
    integer :: i,rank_comm3d
    real(8) :: tmp1,tmp2,mtmp1,mtmp2
    logical :: reorganization=.false. ! to keep numerotation of old communicator
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! fixed number of process in particular direction
    !--------------------------------------------------------------
    if (dim==2) mpi_dim(3)=1

    call mpi_dims_create(nproc,dim,mpi_dim,code)
!!$    if (mpi_print .and. mpi_chief) then
!!$       write(*,*) "call mpi_dims_create(nproc,dim,mpi_dim,code)"
!!$       write(*,*) "mpi_dim=",mpi_dim
!!$       write(*,*) "code=",code
!!$       write(*,*)'--------------------------------------------------------------'
!!$    end if
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! cartesian topology grid with or without periodicity
    !--------------------------------------------------------------
    call mpi_cart_create(mpi_comm_world,&
         & dim,                         &
         & mpi_dim,                     &
         & mpi_periodicity,             &
         & reorganization,              &
         & comm3d,                      &
         & code)

    if (mpi_chief) then
       write(*,'(99(1x,a,1x,i6))') 'FUGU executed with', nproc, 'mpi process.'
       if (dim==2) then
          write(*,'(99(1x,a,1x,i4))') 'domain size in: nx=', nx,', ny=', ny
          write(*,'(99(1x,a,1x,i4))') 'topology dimensions: ',&
               & mpi_dim(1),' in x- and',                     &
               & mpi_dim(2),' in y-direction.'
          write(*,'((1x,a,1x,i5,1x,a,1x,i5,1x,a))') 'computation should use between ',&
               & (nx+1)*(ny+1)/60**dim,'(60^2 block) and',(nx+1)*(ny+1)/30**dim, '(30^2) proc(s)'
          write(*,'((1x,a,1x,i5,a))') 'computation use ',floor(((nx+1)*(ny+1)/nproc)**(1d0/2)), '^2 (scalar) bloc size'
       else
          write(*,'(99(1x,a,1x,i4))') 'domain size in: nx=', nx,', ny=', ny,', nz=', nz
          write(*,'(99(1x,a,1x,i4))') 'topology dimensions: ',&
               & mpi_dim(1),' in x-,',                        &
               & mpi_dim(2),' in y- and',                     &
               & mpi_dim(3),' in z-direction.'
          write(*,'((1x,a,1x,i5,1x,a,1x,i5,1x,a))') 'computation should use between ',&
               & (nx+1)*(ny+1)*(nz+1)/60**dim,'(60^3 block) and',(nx+1)*(ny+1)*(nz+1)/30**dim, '(30^3) proc(s)'
          write(*,'((1x,a,1x,i5,a))') 'computation use ',floor(((nx+1)*(ny+1)*(nz+1)/nproc)**(1d0/3)), '^3 (scalar) bloc size'
       end if
       write(*,*)   '--------------------------------------------------------------'    
    end if
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! Who is who in new topology (comm3d)
    !--------------------------------------------------------------
    call mpi_comm_rank(comm3d,rank_comm3d,code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! indexes in new topology
    !--------------------------------------------------------------
    call mpi_cart_coords(comm3d,rank_comm3d,dim,coords,code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! starting end endding indexed in each directions (scalar mesh)
    !--------------------------------------------------------------

    !--------------------------------------
    ! x-direction
    !--------------------------------------
    gx=3
    ! scalar
    sx=coords(1)*(nx+1)/mpi_dim(1)
    ex=((coords(1)+1)*(nx+1))/mpi_dim(1)-1
    if (periodic(1).and.nproc>1.and.ex==nx) ex=ex-1
    ! U component
    sxu=sx
    exu=ex
    if (ex==nx) exu=ex+1
    ! V component
    sxv=sx
    exv=ex
    ! W component
    sxw=sx
    exw=ex
    !--------------------------------------

    !--------------------------------------
    ! y-direction
    !--------------------------------------
    gy=3
    ! scalar
    sy=coords(2)*(ny+1)/mpi_dim(2)
    ey=((coords(2)+1)*(ny+1))/mpi_dim(2)-1
    if (periodic(2).and.nproc>1.and.ey==ny) ey=ey-1
    ! U component
    syu=sy
    eyu=ey
    ! V component
    syv=sy
    eyv=ey
    if (ey==ny) eyv=ey+1
    ! W component
    syw=sy
    eyw=ey
    !--------------------------------------

    !--------------------------------------
    ! z-direction
    !--------------------------------------
    if (dim==3) then
       gz=3
       ! scalar
       sz=coords(3)*(nz+1)/mpi_dim(3)
       ez=((coords(3)+1)*(nz+1))/mpi_dim(3)-1
       if (periodic(3).and.nproc>1.and.ez==nz) ez=ez-1
       ! U component
       szu=sz
       ezu=ez
       ! V component
       szv=sz
       ezv=ez
       ! W component
       szw=sz
       ezw=ez
       if (ez==nz) ezw=ez+1
    else
       gz=0
       sz=1;ez=1
       szu=1;ezu=1
       szv=1;ezv=1
       szw=1;ezw=1
    end if
    !--------------------------------------


    !--------------------------------------
    ! Solver Indexes 
    !--------------------------------------
    if (nproc==1) then
       ! scalar
       sxs=sx;exs=ex
       sys=sy;eys=ey
       szs=sz;ezs=ez
       ! U component
       sxus=sxu;exus=exu
       syus=syu;eyus=eyu
       szus=szu;ezus=ezu
       ! V component
       sxvs=sxv;exvs=exv
       syvs=syv;eyvs=eyv
       szvs=szv;ezvs=ezv
       ! W component
       sxws=sxw;exws=exw
       syws=syw;eyws=eyw
       szws=szw;ezws=ezw
    else
       ! scalar
       sxs=sx-gxs;exs=ex+gxs
       sys=sy-gys;eys=ey+gys
       if (dim==3) then 
          szs=sz-gzs;ezs=ez+gzs
       else
          szs=sz;ezs=ez
       end if
       ! U component
       sxus=sxu-1;exus=exu+1
       syus=syu-1;eyus=eyu+1
       if (dim==3) then 
          szus=szu-1;ezus=ezu+1
       else
          szus=szu;ezus=ezu
       end if
       ! V component
       sxvs=sxv-1;exvs=exv+1
       syvs=syv-1;eyvs=eyv+1
       if (dim==3) then 
          szvs=szv-1;ezvs=ezv+1
       else
          szvs=szv;ezvs=ezv
       end if
       ! W component
       if (dim==3) then 
          sxws=sxw-1;exws=exw+1
          syws=syw-1;eyws=eyw+1
          szws=szw-1;ezws=ezw+1
       end if
    end if
    !--------------------------------------


!!$    !-------------------------------------------------
!!$    ! indexes for u-velocity
!!$    !-------------------------------------------------
!!$    sxu=sx; exu=ex+1
!!$    syu=sy; eyu=ey 
!!$    szu=sz; ezu=ez 
!!$    !-------------------------------------------------
!!$    ! indexes for v-velocity
!!$    !-------------------------------------------------
!!$    sxv=sx; exv=ex
!!$    syv=sy; eyv=ey+1
!!$    szv=sz; ezv=ez 
!!$    !-------------------------------------------------
!!$    ! indexes for w-velocity
!!$    !-------------------------------------------------
!!$    sxw=sx; exw=ex
!!$    syw=sy; eyw=ey
!!$    szw=sz; ezw=ez+1 
!!$    !-------------------------------------------------

    if (mpi_print) then
       if (mpi_chief) write(*,*) "Scalar (pressure) mesh"
       call mpi_barrier(mpi_comm_world,code)
       do i = 0,nproc-1
          if (rank==i) then
             if (rank==i) then
                if (dim==2) then 
                   write(*,'(5(1x,a,1x,i4),1x,a,1x,i10,1x,a)') 'rank in topology: ',&
                        & rank_comm3d, &
                        & 'tab indexes: ',    & 
                        & sx, ' to', ex, 'in x, ', &
                        & sy, ' to', ey, 'in y, or', &
                        & (ex-sx+1)*(ey-sy+1)*(ez-sz+1), 'cells'
                else
                   write(*,'(7(1x,a,1x,i4),1x,a,1x,i10,1x,a)') 'rank in topology: ',&
                        & rank_comm3d, &
                        & 'tab indexes: ',    & 
                        & sx, ' to', ex, 'in x, ', &
                        & sy, ' to', ey, 'in y, ', &
                        & sz, ' to', ez, 'in z, or', &
                        & (ex-sx+1)*(ey-sy+1)*(ez-sz+1), 'cells'
                end if
                call mpi_barrier(mpi_comm_world,code)
             end if
             call mpi_barrier(mpi_comm_world,code)
          end if
          call mpi_barrier(mpi_comm_world,code)
       end do
       call mpi_barrier(mpi_comm_world,code)
       if (mpi_chief) write(*,*)'--------------------------------------------------------------'
       call mpi_barrier(mpi_comm_world,code)
       flush(6)

       if (mpi_chief) write(*,*) "U-Velocity mesh"
       call mpi_barrier(mpi_comm_world,code)
       do i = 0,nproc-1
          if (rank==i) then
             if (dim==2) then
                write(*,'(5(1x,a,1x,i4),1x,a,1x,i10,1x,a)') 'rank in topology: ',&
                     & rank_comm3d, &
                     & 'tab indexes: ',    & 
                     & sxu, ' to', exu, 'in x, ', &
                     & syu, ' to', eyu, 'in y, or', &
                     & (exu-sxu+1)*(eyu-syu+1)*(ezu-szu+1), 'cells'
             else 
                write(*,'(7(1x,a,1x,i4),1x,a,1x,i10,1x,a)') 'rank in topology: ',&
                     & rank_comm3d, &
                     & 'tab indexes: ',    & 
                     & sxu, ' to', exu, 'in x, ', &
                     & syu, ' to', eyu, 'in y, ', &
                     & szu, ' to', ezu, 'in z, or', &
                     & (exu-sxu+1)*(eyu-syu+1)*(ezu-szu+1), 'cells'
             end if
             call mpi_barrier(mpi_comm_world,code)
          end if
          call mpi_barrier(mpi_comm_world,code)
       end do
       call mpi_barrier(mpi_comm_world,code)
       if (mpi_chief) write(*,*)'--------------------------------------------------------------'
       call mpi_barrier(mpi_comm_world,code)
       flush(6)

       if (mpi_chief) write(*,*) "V-Velocity mesh"
       call mpi_barrier(mpi_comm_world,code)
       do i = 0,nproc-1
          if (rank==i) then
             if (dim==2) then
                write(*,'(5(1x,a,1x,i4),1x,a,1x,i10,1x,a)') 'rank in topology: ',&
                     & rank_comm3d, &
                     & 'tab indexes: ',    & 
                     & sxv, ' to', exv, 'in x, ', &
                     & syv, ' to', eyv, 'in y, or', &
                     & (exv-sxv+1)*(eyv-syv+1)*(ezv-szv+1), 'cells'
             else 
                write(*,'(7(1x,a,1x,i4),1x,a,1x,i10,1x,a)') 'rank in topology: ',&
                     & rank_comm3d, &
                     & 'tab indexes: ',    & 
                     & sxv, ' to', exv, 'in x, ', &
                     & syv, ' to', eyv, 'in y, ', &
                     & szv, ' to', ezv, 'in z, or', &
                     & (exv-sxv+1)*(eyv-syv+1)*(ezv-szv+1), 'cells'
             endif
             call mpi_barrier(mpi_comm_world,code)
          end if
          call mpi_barrier(mpi_comm_world,code)
       end do
       call mpi_barrier(mpi_comm_world,code)
       if (mpi_chief) write(*,*)'--------------------------------------------------------------'
       call mpi_barrier(mpi_comm_world,code)
       flush(6)

       if (dim==3) then
          if (mpi_chief) write(*,*) "W-Velocity mesh"
          call mpi_barrier(mpi_comm_world,code)
          do i = 0,nproc-1
             if (rank==i) then
                write(*,'(7(1x,a,1x,i4),1x,a,1x,i10,1x,a)') 'rank in topology: ',&
                     & rank_comm3d, &
                     & 'tab indexes: ',    & 
                     & sxw, ' to', exw, 'in x, ', &
                     & syw, ' to', eyw, 'in y, ', &
                     & szw, ' to', ezw, 'in z, or', &
                     & (exw-sxw+1)*(eyw-syw+1)*(ezw-szw+1), 'cells'
                call mpi_barrier(mpi_comm_world,code)
             end if
             call mpi_barrier(mpi_comm_world,code)
          end do
          call mpi_barrier(mpi_comm_world,code)
          if (mpi_chief) write(*,*)'--------------------------------------------------------------'
          call mpi_barrier(mpi_comm_world,code)
          flush(6)
       end if

       if (mpi_chief) write(*,*) "Unknowns per block"

       if (dim==2) then
          itmp1=(exus-sxus+1)*(eyus-syus+1)+(exvs-sxvs+1)*(eyvs-syvs+1)
          itmp2=(exs-sxs+1)*(eys-sys+1)
       else
          itmp1=(exus-sxus+1)*(eyus-syus+1)*(ezus-szus+1)   &
               & +(exvs-sxvs+1)*(eyvs-syvs+1)*(ezvs-szvs+1) &
               & +(exws-sxws+1)*(eyws-syws+1)*(ezws-szws+1)
          itmp2=(exs-sxs+1)*(eys-sys+1)*(ezs-szs+1)
       end if

       call mpi_allreduce(itmp1,mitmp1,1,mpi_integer,mpi_sum,comm3d,code)
       call mpi_allreduce(itmp2,mitmp2,1,mpi_integer,mpi_sum,comm3d,code)

       tmp1=100d0*itmp1/mitmp1
       tmp2=100d0*itmp2/mitmp2

       call mpi_barrier(mpi_comm_world,code)
       do i = 0,nproc-1
          if (rank==i) then
             write(*,'(1x,a,1x,i4,1x,a,1x,i9,1x,f6.2,a,1x,i9,1x,f6.2,a)') &
                  & 'rank in topology: ',                                 &
                  & rank_comm3d,                                          &
                  & 'unknowns (uvw): ',                                   &
                  & itmp1,tmp1,                                           &
                  & '%, unknowns (sca): ',                                &
                  & itmp2,tmp2,"%"
             call mpi_barrier(mpi_comm_world,code)
          end if
          call mpi_barrier(mpi_comm_world,code)
       end do
       call mpi_barrier(mpi_comm_world,code)
       if (mpi_chief) write(*,*)'--------------------------------------------------------------'
       call mpi_barrier(mpi_comm_world,code)
       do i = 0,nproc-1
          if (rank==i) then
             write(*,'(1x,a,1x,i4,1x,a,1x,i5,a,i1,1x,a,1x,i5,a,i1)') &
                  & 'rank in topology: ',                            &
                  & rank_comm3d,                                     &
                  & 'equiv. mesh for uvw: ',                         &
                  & floor(itmp1**(1d0/dim)),                         &
                  & '^',dim,                                         &
                  & 'equiv. mesh for sca: ',                         &
                  & floor(itmp2**(1d0/dim)),                         &
                  & '^',dim 
             call mpi_barrier(mpi_comm_world,code)
          end if
          call mpi_barrier(mpi_comm_world,code)
       end do
       call mpi_barrier(mpi_comm_world,code)
       if (mpi_chief) write(*,*)'--------------------------------------------------------------'
       call mpi_barrier(mpi_comm_world,code)
       flush(6)


    end if
    !--------------------------------------------------------------

  end subroutine create_topology_MPI


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


  subroutine neighbouring_MPI
    !--------------------------------------------------------------
    ! global variables
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! local  variables
    !--------------------------------------------------------------
    integer :: i,dir,step
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! tab initialization
    !--------------------------------------------------------------
    ngbr=mpi_proc_null
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! neigbours
    !--------------------------------------------------------------
    ! x-direction, back and front
    dir=0
    step=1
    call mpi_cart_shift(comm3d,dir,step,ngbr(xb),ngbr(xf),code)
    ! y-direction, back and front
    dir=1
    step=1
    call mpi_cart_shift(comm3d,dir,step,ngbr(yb),ngbr(yf),code)
    if (dim==3) then 
       ! z-direction, back and front
       dir=2
       step=1
       call mpi_cart_shift(comm3d,dir,step,ngbr(zb),ngbr(zf),code)
    end if
    !--------------------------------------------------------------

    if (mpi_print) then 
       call mpi_barrier(mpi_comm_world,code)
       do i = 0,nproc-1
          if (rank==i) then
             if (dim==2) then 
                write(*,'(1x,a,1x,i4,1x,a,12(1x,a,i4))') 'process ', rank,&
                     & 'has the following neigbors:', &
                     & 'left', ngbr(xb),&
                     & ', right', ngbr(xf),&
                     & ', bottom', ngbr(yb),&
                     & ', top', ngbr(yf)
             else
                write(*,'(1x,a,1x,i4,1x,a,12(1x,a,i4))') 'process ', rank,&
                     & 'has the following neigbors:', &
                     & 'left', ngbr(xb),&
                     & ', right', ngbr(xf),&
                     & ', bottom', ngbr(yb),&
                     & ', top', ngbr(yf),&
                     & ', behind', ngbr(zb),&
                     & ', ahead', ngbr(zf)
             end if
             call mpi_barrier(mpi_comm_world,code)
          end if
          call mpi_barrier(mpi_comm_world,code)
       end do
       call mpi_barrier(mpi_comm_world,code)
       if (rank==0) then
          write(*,*)'--------------------------------------------------------------'
       end if
    end if
    !--------------------------------------------------------------

  end subroutine neighbouring_MPI


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  subroutine derived_types(lgx,lgy,lgz)
    !--------------------------------------------------------------
    ! global variables
    !--------------------------------------------------------------
    integer, intent(inout) :: lgx,lgy,lgz
    !--------------------------------------------------------------
    ! local  variables
    !--------------------------------------------------------------
    integer                :: code
    integer                :: type_line,type_face
    integer                :: xvar,yvar,zvar
    integer                :: lxvar,lyvar,lzvar
    integer                :: sizeofword,sizeofint
    integer                :: nb_block,size_block,step
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! size of real and integer in octets
    !--------------------------------------------------------------
    call mpi_type_extent(mpi_double_precision,sizeofword,code)
    call mpi_type_extent(mpi_integer,sizeofint,code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! exchange length
    !--------------------------------------------------------------
    if (dim==2) lgz=0
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! scalar (pressure) unknown per block
    !--------------------------------------------------------------
    xvar=(ex+gx)-(sx-gx)+1
    yvar=(ey+gy)-(sy-gy)+1
    zvar=(ez+gz)-(sz-gz)+1

    !--------------------------------------------------------------
    ! scalar (pressure) exchange values per block
    !--------------------------------------------------------------
    lxvar=(ex+lgx)-(sx-lgx)+1
    lyvar=(ey+lgy)-(sy-lgy)+1
    lzvar=(ez+lgz)-(sz-lgz)+1

    !--------------------------------------------------------------
    ! XY slice of normal Z --> zface_p
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! line definition, x-direction 
    !--------------------------------------------------------------
    nb_block   = lxvar 
    size_block = 1
    step       = 1
    call mpi_type_contiguous(nb_block,&
         & mpi_double_precision,      &
         & type_line,                 &
         & code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! type line creation
    !--------------------------------------------------------------
    call mpi_type_commit(type_line,code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! face definition using previous line type
    !--------------------------------------------------------------
    nb_block   = lyvar
    size_block = 1
    step       = xvar 
    call mpi_type_hvector(nb_block,&
         & size_block,             &
         & step*sizeofword,        &
         & type_line,              &
         & type_face,              &
         & code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! type face creation
    !--------------------------------------------------------------
    call mpi_type_commit(type_face,code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! slice definition using previous face type
    !--------------------------------------------------------------
    nb_block   = lgz
    size_block = 1
    step       = xvar*yvar 
    call mpi_type_hvector(nb_block,&
         & size_block,             &
         & step*sizeofword,        &
         & type_face,              &
         & zface_p,                &
         & code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! type creation
    !--------------------------------------------------------------
    call mpi_type_commit(zface_p,code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! free unused types
    !--------------------------------------------------------------
    call mpi_type_free(type_face,code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! XZ slice of normal Y --> yface_p
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! line creation, x-direction 
    !--------------------------------------------------------------
    ! already done in previous step
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! face definition using previous line type
    !--------------------------------------------------------------
    nb_block   = lzvar
    size_block = 1
    step       = xvar*yvar
    call mpi_type_hvector(nb_block,&
         & size_block,             &
         & step*sizeofword,        &
         & type_line,              &
         & type_face,              &
         & code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! type face creation 
    !--------------------------------------------------------------
    call mpi_type_commit(type_face,code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! slice definition using previous face type
    !--------------------------------------------------------------
    nb_block   = lgy
    size_block = 1
    step       = xvar
    call mpi_type_hvector(nb_block,&
         & size_block,             &
         & step*sizeofword,        &
         & type_face,              &
         & yface_p,                &
         & code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! type creation
    !--------------------------------------------------------------
    call mpi_type_commit(yface_p,code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! free unused types
    !--------------------------------------------------------------
    call mpi_type_free(type_face,code)
    call mpi_type_free(type_line,code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! YZ slice of normal X --> xface_p
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! line definition, y-direction 
    !--------------------------------------------------------------
    nb_block   = lyvar
    size_block = 1
    step       = xvar 
    call mpi_type_vector(nb_block,&
         & size_block,            &
         & step,                  &
         & mpi_double_precision,  &
         & type_line,             &
         & code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! type line creation 
    !--------------------------------------------------------------
    call mpi_type_commit(type_line,code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! face definition using previous line type
    !--------------------------------------------------------------
    nb_block   = lzvar
    size_block = 1
    step       = xvar*yvar
    call mpi_type_hvector(nb_block,&
         & size_block,             &
         & step*sizeofword,        &
         & type_line,              &
         & type_face,              &
         & code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! type face creation
    !--------------------------------------------------------------
    call mpi_type_commit(type_face,code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! slice definition using previous face type
    !--------------------------------------------------------------
    nb_block   = lgx
    size_block = 1
    step       = 1
    call mpi_type_hvector(nb_block,&
         & size_block,             &
         & step*sizeofword,        &
         & type_face,              &
         & xface_p,                &
         & code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! type face creation
    !--------------------------------------------------------------
    call mpi_type_commit(xface_p,code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! free unused types
    !--------------------------------------------------------------
    call mpi_type_free(type_face,code)
    call mpi_type_free(type_line,code)
    !--------------------------------------------------------------


    !--------------------------------------------------------------
    ! u-component
    !--------------------------------------------------------------
    xvar=(exu+gx)-(sxu-gx)+1
    yvar=(eyu+gy)-(syu-gy)+1
    zvar=(ezu+gz)-(szu-gz)+1

    lxvar=(exu+lgx)-(sxu-lgx)+1
    lyvar=(eyu+lgy)-(syu-lgy)+1
    lzvar=(ezu+lgz)-(szu-lgz)+1

    !--------------------------------------------------------------
    ! XY slice of normal Z --> zface_u
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! line definition, x-direction 
    !--------------------------------------------------------------
    nb_block   = lxvar 
    size_block = 1
    step       = 1
    call mpi_type_contiguous(nb_block,&
         & mpi_double_precision,      &
         & type_line,                 &
         & code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! type line creation
    !--------------------------------------------------------------
    call mpi_type_commit(type_line,code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! face definition using previous line type
    !--------------------------------------------------------------
    nb_block   = lyvar
    size_block = 1
    step       = xvar 
    call mpi_type_hvector(nb_block,&
         & size_block,             &
         & step*sizeofword,        &
         & type_line,              &
         & type_face,              &
         & code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! type face creation
    !--------------------------------------------------------------
    call mpi_type_commit(type_face,code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! slice definition using previous face type
    !--------------------------------------------------------------
    nb_block   = lgz
    size_block = 1
    step       = xvar*yvar 
    call mpi_type_hvector(nb_block,&
         & size_block,             &
         & step*sizeofword,        &
         & type_face,              &
         & zface_u,                &
         & code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! type creation
    !--------------------------------------------------------------
    call mpi_type_commit(zface_u,code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! free unused types
    !--------------------------------------------------------------
    call mpi_type_free(type_face,code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! XZ slice of normal Y --> yface_u
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! line creation, x-direction 
    !--------------------------------------------------------------
    ! already done in previous step
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! face definition using previous line type
    !--------------------------------------------------------------
    nb_block   = lzvar
    size_block = 1
    step       = xvar*yvar
    call mpi_type_hvector(nb_block,&
         & size_block,             &
         & step*sizeofword,        &
         & type_line,              &
         & type_face,              &
         & code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! type face creation 
    !--------------------------------------------------------------
    call mpi_type_commit(type_face,code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! slice definition using previous face type
    !--------------------------------------------------------------
    nb_block   = lgy
    size_block = 1
    step       = xvar
    call mpi_type_hvector(nb_block,&
         & size_block,             &
         & step*sizeofword,        &
         & type_face,              &
         & yface_u,                &
         & code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! type creation
    !--------------------------------------------------------------
    call mpi_type_commit(yface_u,code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! free unused types
    !--------------------------------------------------------------
    call mpi_type_free(type_face,code)
    call mpi_type_free(type_line,code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! YZ slice of normal X --> xface_u
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! line definition, y-direction 
    !--------------------------------------------------------------
    nb_block   = lyvar
    size_block = 1
    step       = xvar 
    call mpi_type_vector(nb_block,&
         & size_block,            &
         & step,                  &
         & mpi_double_precision,  &
         & type_line,             &
         & code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! type line creation 
    !--------------------------------------------------------------
    call mpi_type_commit(type_line,code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! face definition using previous line type
    !--------------------------------------------------------------
    nb_block   = lzvar
    size_block = 1
    step       = xvar*yvar
    call mpi_type_hvector(nb_block,&
         & size_block,             &
         & step*sizeofword,        &
         & type_line,              &
         & type_face,              &
         & code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! type face creation
    !--------------------------------------------------------------
    call mpi_type_commit(type_face,code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! slice definition using previous face type
    !--------------------------------------------------------------
    nb_block   = lgx
    size_block = 1
    step       = 1
    call mpi_type_hvector(nb_block,&
         & size_block,             &
         & step*sizeofword,        &
         & type_face,              &
         & xface_u,                &
         & code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! type face creation
    !--------------------------------------------------------------
    call mpi_type_commit(xface_u,code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! free unused types
    !--------------------------------------------------------------
    call mpi_type_free(type_face,code)
    call mpi_type_free(type_line,code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! v-component
    !--------------------------------------------------------------
    xvar=(exv+gx)-(sxv-gx)+1
    yvar=(eyv+gy)-(syv-gy)+1
    zvar=(ezv+gz)-(szv-gz)+1

    lxvar=(exv+lgx)-(sxv-lgx)+1
    lyvar=(eyv+lgy)-(syv-lgy)+1
    lzvar=(ezv+lgz)-(szv-lgz)+1

    !--------------------------------------------------------------
    ! XY slice of normal Z --> zface_v
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! line definition, x-direction 
    !--------------------------------------------------------------
    nb_block   = lxvar 
    size_block = 1
    step       = 1
    call mpi_type_contiguous(nb_block,&
         & mpi_double_precision,      &
         & type_line,                 &
         & code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! type line creation
    !--------------------------------------------------------------
    call mpi_type_commit(type_line,code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! face definition using previous line type
    !--------------------------------------------------------------
    nb_block   = lyvar
    size_block = 1
    step       = xvar 
    call mpi_type_hvector(nb_block,&
         & size_block,             &
         & step*sizeofword,        &
         & type_line,              &
         & type_face,              &
         & code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! type face creation
    !--------------------------------------------------------------
    call mpi_type_commit(type_face,code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! slice definition using previous face type
    !--------------------------------------------------------------
    nb_block   = lgz
    size_block = 1
    step       = xvar*yvar 
    call mpi_type_hvector(nb_block,&
         & size_block,             &
         & step*sizeofword,        &
         & type_face,              &
         & zface_v,                &
         & code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! type creation
    !--------------------------------------------------------------
    call mpi_type_commit(zface_v,code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! free unused types
    !--------------------------------------------------------------
    call mpi_type_free(type_face,code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! XZ slice of normal Y --> yface_v
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! line creation, x-direction 
    !--------------------------------------------------------------
    ! already done in previous step
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! face definition using previous line type
    !--------------------------------------------------------------
    nb_block   = lzvar
    size_block = 1
    step       = xvar*yvar
    call mpi_type_hvector(nb_block,&
         & size_block,             &
         & step*sizeofword,        &
         & type_line,              &
         & type_face,              &
         & code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! type face creation 
    !--------------------------------------------------------------
    call mpi_type_commit(type_face,code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! slice definition using previous face type
    !--------------------------------------------------------------
    nb_block   = lgy
    size_block = 1
    step       = xvar
    call mpi_type_hvector(nb_block,&
         & size_block,             &
         & step*sizeofword,        &
         & type_face,              &
         & yface_v,                &
         & code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! type creation
    !--------------------------------------------------------------
    call mpi_type_commit(yface_v,code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! free unused types
    !--------------------------------------------------------------
    call mpi_type_free(type_face,code)
    call mpi_type_free(type_line,code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! YZ slice of normal X --> xface_v
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! line definition, y-direction 
    !--------------------------------------------------------------
    nb_block   = lyvar
    size_block = 1
    step       = xvar 
    call mpi_type_vector(nb_block,&
         & size_block,            &
         & step,                  &
         & mpi_double_precision,  &
         & type_line,             &
         & code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! type line creation 
    !--------------------------------------------------------------
    call mpi_type_commit(type_line,code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! face definition using previous line type
    !--------------------------------------------------------------
    nb_block   = lzvar
    size_block = 1
    step       = xvar*yvar
    call mpi_type_hvector(nb_block,&
         & size_block,             &
         & step*sizeofword,        &
         & type_line,              &
         & type_face,              &
         & code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! type face creation
    !--------------------------------------------------------------
    call mpi_type_commit(type_face,code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! slice definition using previous face type
    !--------------------------------------------------------------
    nb_block   = lgx
    size_block = 1
    step       = 1
    call mpi_type_hvector(nb_block,&
         & size_block,             &
         & step*sizeofword,        &
         & type_face,              &
         & xface_v,                &
         & code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! type face creation
    !--------------------------------------------------------------
    call mpi_type_commit(xface_v,code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! free unused types
    !--------------------------------------------------------------
    call mpi_type_free(type_face,code)
    call mpi_type_free(type_line,code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! w-component
    !--------------------------------------------------------------
    xvar=(exw+gx)-(sxw-gx)+1
    yvar=(eyw+gy)-(syw-gy)+1
    zvar=(ezw+gz)-(szw-gz)+1

    lxvar=(exw+lgx)-(sxw-lgx)+1
    lyvar=(eyw+lgy)-(syw-lgy)+1
    lzvar=(ezw+lgz)-(szw-lgz)+1

    !--------------------------------------------------------------
    ! XY slice of normal Z --> zface_w
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! line definition, x-direction 
    !--------------------------------------------------------------
    nb_block   = lxvar 
    size_block = 1
    step       = 1
    call mpi_type_contiguous(nb_block,&
         & mpi_double_precision,      &
         & type_line,                 &
         & code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! type line creation
    !--------------------------------------------------------------
    call mpi_type_commit(type_line,code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! face definition using previous line type
    !--------------------------------------------------------------
    nb_block   = lyvar
    size_block = 1
    step       = xvar 
    call mpi_type_hvector(nb_block,&
         & size_block,             &
         & step*sizeofword,        &
         & type_line,              &
         & type_face,              &
         & code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! type face creation
    !--------------------------------------------------------------
    call mpi_type_commit(type_face,code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! slice definition using previous face type
    !--------------------------------------------------------------
    nb_block   = lgz
    size_block = 1
    step       = xvar*yvar 
    call mpi_type_hvector(nb_block,&
         & size_block,             &
         & step*sizeofword,        &
         & type_face,              &
         & zface_w,                &
         & code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! type creation
    !--------------------------------------------------------------
    call mpi_type_commit(zface_w,code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! free unused types
    !--------------------------------------------------------------
    call mpi_type_free(type_face,code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! XZ slice of normal Y --> yface_w
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! line creation, x-direction 
    !--------------------------------------------------------------
    ! already done in previous step
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! face definition using previous line type
    !--------------------------------------------------------------
    nb_block   = lzvar
    size_block = 1
    step       = xvar*yvar
    call mpi_type_hvector(nb_block,&
         & size_block,             &
         & step*sizeofword,        &
         & type_line,              &
         & type_face,              &
         & code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! type face creation 
    !--------------------------------------------------------------
    call mpi_type_commit(type_face,code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! slice definition using previous face type
    !--------------------------------------------------------------
    nb_block   = lgy
    size_block = 1
    step       = xvar
    call mpi_type_hvector(nb_block,&
         & size_block,             &
         & step*sizeofword,        &
         & type_face,              &
         & yface_w,                &
         & code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! type creation
    !--------------------------------------------------------------
    call mpi_type_commit(yface_w,code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! free unused types
    !--------------------------------------------------------------
    call mpi_type_free(type_face,code)
    call mpi_type_free(type_line,code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! YZ slice of normal X --> xface_w
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! line definition, y-direction 
    !--------------------------------------------------------------
    nb_block   = lyvar
    size_block = 1
    step       = xvar 
    call mpi_type_vector(nb_block,&
         & size_block,            &
         & step,                  &
         & mpi_double_precision,  &
         & type_line,             &
         & code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! type line creation 
    !--------------------------------------------------------------
    call mpi_type_commit(type_line,code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! face definition using previous line type
    !--------------------------------------------------------------
    nb_block   = lzvar
    size_block = 1
    step       = xvar*yvar
    call mpi_type_hvector(nb_block,&
         & size_block,             &
         & step*sizeofword,        &
         & type_line,              &
         & type_face,              &
         & code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! type face creation
    !--------------------------------------------------------------
    call mpi_type_commit(type_face,code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! slice definition using previous face type
    !--------------------------------------------------------------
    nb_block   = lgx
    size_block = 1
    step       = 1
    call mpi_type_hvector(nb_block,&
         & size_block,             &
         & step*sizeofword,        &
         & type_face,              &
         & xface_w,                &
         & code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! type face creation
    !--------------------------------------------------------------
    call mpi_type_commit(xface_w,code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! free unused types
    !--------------------------------------------------------------
    call mpi_type_free(type_face,code)
    call mpi_type_free(type_line,code)
    !--------------------------------------------------------------

  end subroutine derived_types

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  subroutine derived_types_particle(Part,n)
    use mod_struct_Particle_Tracking
    implicit none
    !--------------------------------------------------------------
    ! global variables
    !--------------------------------------------------------------
    integer, intent(in)                                   :: n
    type(particle_t), allocatable, dimension(:), intent(in) :: Part
    !--------------------------------------------------------------
    ! local  variables
    !--------------------------------------------------------------
    integer                                         :: i,code,itmp
    integer, dimension(16)                          :: types,lengths
    integer(kind=MPI_ADDRESS_KIND), dimension(16+1) :: lb,extent
    integer(kind=MPI_ADDRESS_KIND), dimension(16+1) :: displ,address
    !--------------------------------------------------------------

    if (mpi_chief) then
       write(*,*) "WARNING, CREATE MORE PARTICLE TYPES!!!" 
       write(*,*) "WARNING, CREATE MORE PARTICLE TYPES!!!" 
       write(*,*) "WARNING, CREATE MORE PARTICLE TYPES!!!" 
       write(*,*) "WARNING, CREATE MORE PARTICLE TYPES!!!" 
       write(*,*) "WARNING, CREATE MORE PARTICLE TYPES!!!" 
    end if
    
    !--------------------------------------------------------------
    ! types and lengths
    !--------------------------------------------------------------
    types=(/                     &
         & MPI_INTEGER,          &
         & MPI_INTEGER,          &
         & MPI_INTEGER,          &
         & MPI_DOUBLE_PRECISION, &
         & MPI_DOUBLE_PRECISION, &
         & MPI_DOUBLE_PRECISION, &
         & MPI_DOUBLE_PRECISION, &
         & MPI_DOUBLE_PRECISION, &
         & MPI_DOUBLE_PRECISION, &
         & MPI_DOUBLE_PRECISION, &
         & MPI_DOUBLE_PRECISION, &
         & MPI_DOUBLE_PRECISION, &
         & MPI_DOUBLE_PRECISION, &
         & MPI_DOUBLE_PRECISION, &
         & MPI_DOUBLE_PRECISION, &
         & MPI_LOGICAL/)
    !--------------------------------------------------------------
    lengths=(/ &
         & 1,  &
         & 1,  &
         & 3,  &
         & 1,  &
         & 1,  &
         & 1,  &
         & 1,  &
         & 1,  &
         & 2,  &
         & 3,  &
         & 3,  &
         & 3,  &
         & 3,  &
         & 3,  &
         & 3,  &
         & 1 /)
    !--------------------------------------------------------------
    
    !--------------------------------------------------------------
    ! Adresses
    !--------------------------------------------------------------
    call MPI_GET_ADDRESS(Part(1)%rk,      address(1), code)
    call MPI_GET_ADDRESS(Part(1)%interpol,address(2), code)
    call MPI_GET_ADDRESS(Part(1)%ijk,     address(3), code)
    call MPI_GET_ADDRESS(Part(1)%rho,     address(4), code)
    call MPI_GET_ADDRESS(Part(1)%mu,      address(5), code)
    call MPI_GET_ADDRESS(Part(1)%m,       address(6), code)
    call MPI_GET_ADDRESS(Part(1)%phi,     address(7), code)
    call MPI_GET_ADDRESS(Part(1)%time,    address(8), code)
    call MPI_GET_ADDRESS(Part(1)%radius,  address(9), code)
    call MPI_GET_ADDRESS(Part(1)%r,       address(11),code)
    call MPI_GET_ADDRESS(Part(1)%v,       address(12),code)
    call MPI_GET_ADDRESS(Part(1)%u,       address(13),code)
    call MPI_GET_ADDRESS(Part(1)%a,       address(14),code)
    call MPI_GET_ADDRESS(Part(1)%dl,      address(15),code)
    call MPI_GET_ADDRESS(Part(1)%active,  address(16),code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! relative displacement from starting address
    !--------------------------------------------------------------
    do i=1,16
       displ(i)=address(i)-address(1)
    end do
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! temporary type
    !--------------------------------------------------------------
    call MPI_TYPE_CREATE_STRUCT(15,lengths,displ,types,itmp,code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! address 2nd element
    !--------------------------------------------------------------
    call MPI_GET_ADDRESS(Part(2)%rk,address(16),code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! type particle from itmp
    !--------------------------------------------------------------
    lb=0
    extent=address(16+1)-address(1)
    call MPI_TYPE_CREATE_RESIZED(itmp,lb,extent,type_particle,code)
    call MPI_TYPE_COMMIT(type_particle,code)
    !--------------------------------------------------------------

    
  end subroutine derived_types_particle

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  subroutine comm_mpi(var,lgx,lgy,lgz,lsx,lex,lsy,ley,lsz,lez,xface,yface,zface)
    !--------------------------------------------------------------
    ! global variables
    !--------------------------------------------------------------
    integer, intent(in)                                   :: lgx,lgy,lgz
    integer, intent(in)                                   :: lsx,lex,lsy,ley,lsz,lez
    integer, intent(in)                                   :: xface,yface,zface
    real(8), allocatable, dimension(:,:,:), intent(inout) :: var
    !--------------------------------------------------------------
    ! local  variables
    !--------------------------------------------------------------
    integer                                               :: code 
    integer, parameter                                    :: tag=100
    integer, dimension(mpi_status_size)                   :: status
    !--------------------------------------------------------------

!!$    write(*,*) "lgx,lgy,lgz,lsx,lex,lsy,ley,lsz,lez"
!!$    write(*,*) lgx,lgy,lgz,lsx,lex,lsy,ley,lsz,lez
!!$    write(*,*) shape(var)
!!$    write(*,*) lbound(var)
!!$    write(*,*) ubound(var)
    
    !--------------------------------------------------------------
    ! x-direction
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! sending backward (left) and receiving from forward (right)
    !--------------------------------------------------------------
    call mpi_sendrecv(&
         & var(lsx,lsy-lgy,lsz-lgz),  & ! starting coordinates of sending object
         & 1,                         & ! number of object
         & xface,                     & ! type
         & ngbr(xb),                  & ! to: backward neighbour
         & tag,                       & ! tag
         & var(lex+1,lsy-lgy,lsz-lgz),& ! starting coordinates of received object
         & 1,                         & ! number of object
         & xface,                     & ! type
         & ngbr(xf),                  & ! from: forward neighbour
         & tag,                       & ! tag
         & comm3d,                    & ! communicator 
         & status,                    & ! status
         & code)                        ! code error
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! sending forward (right) and receiving from backward (left)
    !--------------------------------------------------------------
    call mpi_sendrecv(&
         & var(lex+1-lgx,lsy-lgy,lsz-lgz),& ! starting coordinates of sending object
         & 1,                             & ! number of object
         & xface,                         & ! type
         & ngbr(xf),                      & ! to: forward neighbour
         & tag,                           & ! tag
         & var(lsx-lgx,lsy-lgy,lsz-lgz),  & ! starting coordinates of received object
         & 1,                             & ! number of object
         & xface,                         & ! type
         & ngbr(xb),                      & ! from: backward neighbour
         & tag,                           & ! tag
         & comm3d,                        & ! communicator 
         & status,                        & ! status
         & code)                            ! code error
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! y-direction
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! sending backward (bottom) and receiving from forward (top)
    !--------------------------------------------------------------
    call mpi_sendrecv(&
         & var(lsx-lgx,lsy,lsz-lgz),  & ! starting coordinates of sending object
         & 1,                         & ! number of object
         & yface,                     & ! type
         & ngbr(yb),                  & ! to: backward neighbour
         & tag,                       & ! tag
         & var(lsx-lgx,ley+1,lsz-lgz),& ! starting coordinates of received object
         & 1,                         & ! number of object
         & yface,                     & ! type
         & ngbr(yf),                  & ! from: forward neighbour
         & tag,                       & ! tag
         & comm3d,                    & ! communicator 
         & status,                    & ! status
         & code)                        ! code error
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! sending forward (top) and receiving from backward (bottom)
    !--------------------------------------------------------------
    call mpi_sendrecv(&
         & var(lsx-lgx,ley+1-lgy,lsz-lgz),& ! starting coordinates of sending object
         & 1,                             & ! number of object
         & yface,                         & ! type
         & ngbr(yf),                      & ! to: forward neighbour
         & tag,                           & ! tag
         & var(lsx-lgx,lsy-lgy,lsz-lgz),  & ! starting coordinates of received object
         & 1,                             & ! number of object
         & yface,                         & ! type
         & ngbr(yb),                      & ! from: backward neighbour
         & tag,                           & ! tag
         & comm3d,                        & ! communicator 
         & status,                        & ! status
         & code)                            ! code error
    !--------------------------------------------------------------

    if (dim==3) then
       !--------------------------------------------------------------
       ! z-direction
       !--------------------------------------------------------------

       !--------------------------------------------------------------
       ! sending backward (behind) and receiving from forward (ahead)
       !--------------------------------------------------------------
       call mpi_sendrecv(&
            & var(lsx-lgx,lsy-lgy,lsz),  & ! starting coordinates of sending object
            & 1,                         & ! number of object
            & zface,                     & ! type
            & ngbr(zb),                  & ! to: backward neighbour
            & tag,                       & ! tag
            & var(lsx-lgx,lsy-lgy,lez+1),& ! starting coordinates of received object
            & 1,                         & ! number of object
            & zface,                     & ! type
            & ngbr(zf),                  & ! from: forward neighbour
            & tag,                       & ! tag
            & comm3d,                    & ! communicator 
            & status,                    & ! status
            & code)                        ! code error
       !--------------------------------------------------------------

       !--------------------------------------------------------------
       ! sending forward (ahead) and receiving from backward (behind)
       !--------------------------------------------------------------
       call mpi_sendrecv(&
            & var(lsx-lgx,lsy-lgy,lez+1-lgz),& ! starting coordinates of sending object
            & 1,                             & ! number of object
            & zface,                         & ! type
            & ngbr(zf),                      & ! to: forward neighbour
            & tag,                           & ! tag
            & var(lsx-lgx,lsy-lgy,lsz-lgz),  & ! starting coordinates of received object
            & 1,                             & ! number of object
            & zface,                         & ! type
            & ngbr(zb),                      & ! from: backward neighbour
            & tag,                           & ! tag
            & comm3d,                        & ! communicator 
            & status,                        & ! status
            & code)                            ! code error
       !--------------------------------------------------------------
    end if

  end subroutine comm_mpi

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  subroutine icomm_mpi(var,lgx,lgy,lgz,lsx,lex,lsy,ley,lsz,lez,xface,yface,zface)
    !--------------------------------------------------------------
    ! global variables
    !--------------------------------------------------------------
    integer, intent(in)                                   :: lgx,lgy,lgz
    integer, intent(in)                                   :: lsx,lex,lsy,ley,lsz,lez
    integer, intent(in)                                   :: xface,yface,zface
    real(8), allocatable, dimension(:,:,:), intent(inout) :: var
    !--------------------------------------------------------------
    ! local  variables
    !--------------------------------------------------------------
    integer                                               :: ireq,code
    integer, dimension(12)                                :: req
    integer, dimension(mpi_status_size,12)                :: status
    !--------------------------------------------------------------

    ireq=0

    !--------------------------------------------------------------
    ! x-direction
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! sending backward (left)
    !--------------------------------------------------------------
    ireq=ireq+1
    call mpi_isend(&
         & var(lsx,lsy-lgy,lsz-lgz),  & ! starting coordinates of sending object
         & 1,                         & ! number of object
         & xface,                     & ! type
         & ngbr(xb),                  & ! to: backward neighbour
         & 0,                         & ! tag
         & comm3d,                    & ! communicator 
         & req(ireq),                 & ! request
         & code)                        ! code error
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! receiving from forward (right)
    !--------------------------------------------------------------
    ireq=ireq+1
    call mpi_irecv(&
         & var(lex+1,lsy-lgy,lsz-lgz),& ! starting coordinates of received object
         & 1,                         & ! number of object
         & xface,                     & ! type
         & ngbr(xf),                  & ! from: forward neighbour
         & 0,                         & ! tag
         & comm3d,                    & ! communicator 
         & req(ireq),                 & ! request
         & code)                        ! code error
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! sending forward (right)
    !--------------------------------------------------------------
    ireq=ireq+1
    call mpi_isend(&
         & var(lex+1-lgx,lsy-lgy,lsz-lgz),& ! starting coordinates of sending object
         & 1,                             & ! number of object
         & xface,                         & ! type
         & ngbr(xf),                      & ! to: forward neighbour
         & 1,                             & ! tag
         & comm3d,                        & ! communicator 
         & req(ireq),                     & ! request
         & code)                            ! code error
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! receiving from backward (left)
    !--------------------------------------------------------------
    ireq=ireq+1
    call mpi_irecv(&
         & var(lsx-lgx,lsy-lgy,lsz-lgz),  & ! starting coordinates of received object
         & 1,                             & ! number of object
         & xface,                         & ! type
         & ngbr(xb),                      & ! from: backward neighbour
         & 1,                             & ! tag
         & comm3d,                        & ! communicator 
         & req(ireq),                     & ! request
         & code)                            ! code error
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! y-direction
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! sending backward (bottom)
    !--------------------------------------------------------------
    ireq=ireq+1
    call mpi_isend(&
         & var(lsx-lgx,lsy,lsz-lgz),  & ! starting coordinates of sending object
         & 1,                         & ! number of object
         & yface,                     & ! type
         & ngbr(yb),                  & ! to: backward neighbour
         & 2,                         & ! tag
         & comm3d,                    & ! communicator 
         & req(ireq),                 & ! request
         & code)                        ! code error
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! receiving from forward (top)
    !--------------------------------------------------------------
    if (ngbr(yf)>=0) then
       ireq=ireq+1
       call mpi_irecv(&
            & var(lsx-lgx,ley+1,lsz-lgz),& ! starting coordinates of received object
            & 1,                         & ! number of object
            & yface,                     & ! type
            & ngbr(yf),                  & ! from: forward neighbour
            & 2,                         & ! tag
            & comm3d,                    & ! communicator 
            & req(ireq),                 & ! request
            & code)                        ! code error
    end if
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! sending forward (top)
    !--------------------------------------------------------------
    ireq=ireq+1
    call mpi_isend(&
         & var(lsx-lgx,ley+1-lgy,lsz-lgz),& ! starting coordinates of sending object
         & 1,                             & ! number of object
         & yface,                         & ! type
         & ngbr(yf),                      & ! to: forward neighbour
         & 3,                             & ! tag
         & comm3d,                        & ! communicator 
         & req(ireq),                     & ! request
         & code)                            ! code error
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! receiving from backward (bottom)
    !--------------------------------------------------------------
    ireq=ireq+1
    call mpi_irecv(&
         & var(lsx-lgx,lsy-lgy,lsz-lgz),  & ! starting coordinates of received object
         & 1,                             & ! number of object
         & yface,                         & ! type
         & ngbr(yb),                      & ! from: backward neighbour
         & 3,                             & ! tag
         & comm3d,                        & ! communicator 
         & req(ireq),                     & ! request
         & code)                            ! code error
    !--------------------------------------------------------------

    if (dim==3) then
       !--------------------------------------------------------------
       ! z-direction
       !--------------------------------------------------------------

       !--------------------------------------------------------------
       ! sending backward (behind)
       !--------------------------------------------------------------
       ireq=ireq+1
       call mpi_isend(&
            & var(lsx-lgx,lsy-lgy,lsz),  & ! starting coordinates of sending object
            & 1,                         & ! number of object
            & zface,                     & ! type
            & ngbr(zb),                  & ! to: backward neighbour
            & 4,                         & ! tag
            & comm3d,                    & ! communicator 
            & req(ireq),                 & ! request
            & code)                        ! code error
       !--------------------------------------------------------------

       !--------------------------------------------------------------
       ! receiving from forward (ahead)
       !--------------------------------------------------------------
       ireq=ireq+1
       call mpi_irecv(&
            & var(lsx-lgx,lsy-lgy,lez+1),& ! starting coordinates of received object
            & 1,                         & ! number of object
            & zface,                     & ! type
            & ngbr(zf),                  & ! from: forward neighbour
            & 4,                         & ! tag
            & comm3d,                    & ! communicator 
            & req(ireq),                 & ! request
            & code)                        ! code error
       !--------------------------------------------------------------

       !--------------------------------------------------------------
       ! sending forward (ahead)
       !--------------------------------------------------------------
       ireq=ireq+1
       call mpi_isend(&
            & var(lsx-lgx,lsy-lgy,lez+1-lgz),& ! starting coordinates of sending object
            & 1,                             & ! number of object
            & zface,                         & ! type
            & ngbr(zf),                      & ! to: forward neighbour
            & 5,                             & ! tag
            & comm3d,                        & ! communicator 
            & req(ireq),                     & ! request
            & code)                            ! code error
       !--------------------------------------------------------------

       !--------------------------------------------------------------
       ! receiving from backward (behind)
       !--------------------------------------------------------------
       ireq=ireq+1
       call mpi_irecv(&
            & var(lsx-lgx,lsy-lgy,lsz-lgz),  & ! starting coordinates of received object
            & 1,                             & ! number of object
            & zface,                         & ! type
            & ngbr(zb),                      & ! from: backward neighbour
            & 5,                             & ! tag
            & comm3d,                        & ! communicator 
            & req(ireq),                     & ! request
            & code)                            ! code error
       !--------------------------------------------------------------
    end if

    call mpi_waitall(ireq,req,status,code)

  end subroutine icomm_mpi

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  subroutine comm_mpi_sca(var)
    use mod_timers
    !--------------------------------------------------------------
    ! Global variables
    !--------------------------------------------------------------
    real(8), allocatable, dimension(:,:,:), intent(inout) :: var
    !--------------------------------------------------------------
    ! Local  variables
    !--------------------------------------------------------------
    !--------------------------------------------------------------

    if (nproc==1) return
    
    call Compute_time(TIMER_START,"[sub] comm_mpi_sca")
    
#if MPI_SYNCH
    call comm_mpi(var,slx,sly,slz,sx,ex,sy,ey,sz,ez,xface_p,yface_p,zface_p)
#else 
    call icomm_mpi(var,slx,sly,slz,sx,ex,sy,ey,sz,ez,xface_p,yface_p,zface_p)
#endif

    call Compute_time(TIMER_END,"[sub] comm_mpi_sca")

  end subroutine comm_mpi_sca

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  subroutine comm_mpi_uvw(u,v,w)
    !--------------------------------------------------------------
    ! Modules
    !--------------------------------------------------------------
    use mod_timers
    !--------------------------------------------------------------
    ! Global variables
    !--------------------------------------------------------------
    real(8), allocatable, dimension(:,:,:), intent(inout) :: u,v,w
    !--------------------------------------------------------------
    ! Local variables
    !--------------------------------------------------------------
    !--------------------------------------------------------------

    if (nproc==1) return
    
    call Compute_time(TIMER_START,"[sub] comm_mpi_uvw")

#if MPI_SYNCH
    call comm_mpi(u,slx,sly,slz,sxu,exu,syu,eyu,szu,ezu,xface_u,yface_u,zface_u)
    call comm_mpi(v,slx,sly,slz,sxv,exv,syv,eyv,szv,ezv,xface_v,yface_v,zface_v)
    if (dim==3) then
       call comm_mpi(w,slx,sly,slz,sxw,exw,syw,eyw,szw,ezw,xface_w,yface_w,zface_w)
    end if
#else
    call icomm_mpi(u,slx,sly,slz,sxu,exu,syu,eyu,szu,ezu,xface_u,yface_u,zface_u)
    call icomm_mpi(v,slx,sly,slz,sxv,exv,syv,eyv,szv,ezv,xface_v,yface_v,zface_v)
    if (dim==3) then
       call icomm_mpi(w,slx,sly,slz,sxw,exw,syw,eyw,szw,ezw,xface_w,yface_w,zface_w)
    end if
#endif

    call Compute_time(TIMER_END,"[sub] comm_mpi_uvw")

  end subroutine comm_mpi_uvw

  subroutine comm_mpi_u(u)
    !--------------------------------------------------------------
    ! Modules
    !--------------------------------------------------------------
    !--------------------------------------------------------------
    ! Global variables
    !--------------------------------------------------------------
    real(8), allocatable, dimension(:,:,:), intent(inout) :: u
    !--------------------------------------------------------------
    ! Local variables
    !--------------------------------------------------------------
    !--------------------------------------------------------------

#if MPI_SYNCH
    call comm_mpi(u,slx,sly,slz,sxu,exu,syu,eyu,szu,ezu,xface_u,yface_u,zface_u)
#else
    call icomm_mpi(u,slx,sly,slz,sxu,exu,syu,eyu,szu,ezu,xface_u,yface_u,zface_u)
#endif

  end subroutine comm_mpi_u

  subroutine comm_mpi_v(v)
    !--------------------------------------------------------------
    ! Modules
    !--------------------------------------------------------------
    !--------------------------------------------------------------
    ! Global variables
    !--------------------------------------------------------------
    real(8), allocatable, dimension(:,:,:), intent(inout) :: v
    !--------------------------------------------------------------
    ! Local variables
    !--------------------------------------------------------------
    !--------------------------------------------------------------
    
#if MPI_SYNCH
    call comm_mpi(v,slx,sly,slz,sxv,exv,syv,eyv,szv,ezv,xface_v,yface_v,zface_v)
#else
    call icomm_mpi(v,slx,sly,slz,sxv,exv,syv,eyv,szv,ezv,xface_v,yface_v,zface_v)
#endif
    
  end subroutine comm_mpi_v
  
  subroutine comm_mpi_w(w)
    !--------------------------------------------------------------
    ! Modules
    !--------------------------------------------------------------
    !--------------------------------------------------------------
    ! Global variables
    !--------------------------------------------------------------
    real(8), allocatable, dimension(:,:,:), intent(inout) :: w
    !--------------------------------------------------------------
    ! Local variables
    !--------------------------------------------------------------
    !--------------------------------------------------------------

#if MPI_SYNCH
    call comm_mpi(w,slx,sly,slz,sxw,exw,syw,eyw,szw,ezw,xface_w,yface_w,zface_w)
#else
    call icomm_mpi(w,slx,sly,slz,sxw,exw,syw,eyw,szw,ezw,xface_w,yface_w,zface_w)
#endif

  end subroutine comm_mpi_w


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  subroutine comm_mpi_lag(Part)
    !--------------------------------------------------------------
    ! Modules
    !--------------------------------------------------------------
    use mod_struct_Particle_Tracking
    implicit none
    !--------------------------------------------------------------
    ! Global variables
    !--------------------------------------------------------------
    type(particle_t), allocatable, dimension(:), intent(inout) :: Part 
    !--------------------------------------------------------------
    ! Local variables
    !--------------------------------------------------------------
    !--------------------------------------------------------------

  end subroutine comm_mpi_lag
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  subroutine read_write_EQ_mpi(iter,time,cpt,&
       & dt_new,dt_old,                      &
       & u,v,w,u0,v0,w0,pres,                &
       & tp,tp0,cou,dist,                    &
       & sca01,sca02,sca03,code,             &
       & vl)
#if RESPECT
  use Module_VOFLag_ParticlesDataStructure
#endif
    use mod_Parameters, only: filerestart,restart_EQ
    implicit none
    !---------------------------------------------------------------------
    ! Global variables                                            
    !---------------------------------------------------------------------
    real(8), intent(inout)                                :: time
    real(8), intent(inout)                                :: dt_new,dt_old
    real(8), allocatable, dimension(:,:,:), intent(inout) :: pres,cou,dist
    real(8), allocatable, dimension(:,:,:), intent(inout) :: u,v,w,u0,v0,w0
    real(8), allocatable, dimension(:,:,:), intent(inout) :: tp,tp0
    real(8), allocatable, dimension(:,:,:), intent(inout) :: sca01,sca02,sca03
    integer, intent(in)                                   :: code
    integer, intent(inout)                                :: iter,cpt
#if RESPECT
    type(struct_vof_lag), intent(inout)                   :: vl
#endif
    !---------------------------------------------------------------------
    ! Local variables                                             
    !---------------------------------------------------------------------
    integer                                               :: lex,ley,lez
    integer                                               :: lexu,leyu,lezu
    integer                                               :: lexv,leyv,lezv
    integer                                               :: lexw,leyw,lezw
    integer                                               :: old_nx,old_ny,old_nz
    integer                                               :: max_writeable_nodes
    real(8)                                               :: sizes,gsize,size_tot
    integer, save                                         :: num=0
    character(len=100)                                    :: name
    real(8), allocatable, dimension(:,:,:)                :: work,worku,workv,workw
    logical                                               :: file_exists
    !---------------------------------------------------------------------    

    allocate(work(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
    allocate(worku(sxu-gx:exu+gx,syu-gy:eyu+gy,szu-gz:ezu+gz))
    allocate(workv(sxv-gx:exv+gx,syv-gy:eyv+gy,szv-gz:ezv+gz))
    allocate(workw(sxw-gx:exw+gx,syw-gy:eyw+gy,szw-gz:ezw+gz))
    work=0

    
    !---------------------------------------------------------------------
    ! By proc writing if
    !            nx*ny*nz > 2^31-1 (max integer number)
    !---------------------------------------------------------------------
    max_writeable_nodes=512**3
    if (nx*ny*nz>max_writeable_nodes) then
       call read_write_restart_by_proc(code,iter,time, &
            & dt_new,dt_old,                           & 
            & u,v,w,u0,v0,w0,pres,                     &
            & tp,tp0,cou,dist,                         &
            & sca01,sca02,sca03)
       return
    end if
    !---------------------------------------------------------------------
    
    !---------------------------------------------------------------------
    ! local indexes to take into account periodicity with mpi
    !---------------------------------------------------------------------
    lex=ex;ley=ey;lez=ez
    lexu=exu;leyu=eyu;lezu=ezu
    lexv=exv;leyv=eyv;lezv=ezv
    lexw=exw;leyw=eyw;lezw=ezw
    !---------------------------------------------------------------------
    if (nproc>1) then
       if (periodic(1)) then
          if (lex==gex-1) then
             lex=lex+1
             lexu=lexu+2
             lexv=lexv+1
             if (dim==3) lexw=lexw+1
          end if
       end if
       if (periodic(2)) then
          if (ley==gey-1) then
             ley=ley+1
             leyu=leyu+1
             leyv=leyv+2
             if (dim==3) leyw=leyw+1
          end if
       end if
       if (dim==3.and.periodic(3)) then
          if (lez==gez-1) then
             lez=lez+1
             lezu=lezu+1
             lezv=lezv+1
             lezw=lezw+2
          end if
       end if
    end if
    !---------------------------------------------------------------------

    size_tot=0
    if (code==1) then
       if (num<2) then
          num=num+1
       else
          num=0
       end if
       write(name,'(i2.2)') num
       name="restart_MPI_EQ"//trim(adjustl(name))//".bin"
       
       !---------------------------------------------------------------------    
       ! header 
       !---------------------------------------------------------------------    
       sizes=5*4+3*8
#if RESPECT
       sizes=sizes+size(vl%objet)*19*8
#endif
       size_tot=size_tot+sizes
       if (mpi_chief.and.mpi_print) write(*,'(a20,1x,i15,1x,a6)') "... Writing header",floor(sizes),"octets"
       call read_write_IO_mpi(0)
       !---------------------------------------------------------------------

       !---------------------------------------------------------------------    
       ! U
       !---------------------------------------------------------------------    
       if (allocated(u)) then
          sizes=8*(lexu-sxu+1)*(leyu-syu+1)*(lezu-szu+1)
          CALL MPI_ALLREDUCE(sizes,GSIZE,1,MPI_DOUBLE_PRECISION,MPI_SUM,COMM3D,MPI_CODE);sizes=GSIZE
          size_tot=size_tot+sizes
          if (mpi_chief.and.mpi_print) write(*,'(a20,1x,i15,1x,a6)') "... Writing u",floor(sizes),"octets"
          call read_write_IO_mpi(1,&
               & name,u,iter,time,cpt,               &
               & dt_new,dt_old,                      &
               & gx,gy,gz,sxu,lexu,syu,leyu,szu,lezu,&
               & gexu-gsxu+1,geyu-gsyu+1,gezu-gszu+1,&
               & vl)
       end if
       !---------------------------------------------------------------------    

       !---------------------------------------------------------------------    
       ! V
       !---------------------------------------------------------------------    
       if (allocated(v)) then
          sizes=8*(lexv-sxv+1)*(leyv-syv+1)*(lezv-szv+1)
          CALL MPI_ALLREDUCE(sizes,GSIZE,1,MPI_DOUBLE_PRECISION,MPI_SUM,COMM3D,MPI_CODE);sizes=GSIZE
          size_tot=size_tot+sizes
          if (mpi_chief.and.mpi_print) write(*,'(a20,1x,i15,1x,a6)') "... Writing v",floor(sizes),"octets"
!RESPECT
          call read_write_IO_mpi(1,&
               & name,v,iter,time,cpt,               &
               & dt_new,dt_old,                      &
               & gx,gy,gz,sxv,lexv,syv,leyv,szv,lezv,&
               & gexv-gsxv+1,geyv-gsyv+1,gezv-gszv+1,&
               & vl)
!RESPECT
       end if
       !---------------------------------------------------------------------    

       !---------------------------------------------------------------------    
       ! W
       !---------------------------------------------------------------------    
       if (allocated(w)) then
          sizes=8*(lexw-sxw+1)*(leyw-syw+1)*(lezw-szw+1)
          CALL MPI_ALLREDUCE(sizes,GSIZE,1,MPI_DOUBLE_PRECISION,MPI_SUM,COMM3D,MPI_CODE);sizes=GSIZE
          size_tot=size_tot+sizes
          if (mpi_chief.and.mpi_print) write(*,'(a20,1x,i15,1x,a6)') "... Writing w",floor(sizes),"octets"
!RESPECT
          call read_write_IO_mpi(1,&
               & name,w,iter,time,cpt,               &
               & dt_new,dt_old,                      &
               & gx,gy,gz,sxw,lexw,syw,leyw,szw,lezw,&
               & gexw-gsxw+1,geyw-gsyw+1,gezw-gszw+1,&
               & vl)
!RESPECT
       end if
       !---------------------------------------------------------------------    

       !---------------------------------------------------------------------    
       ! U0
       !---------------------------------------------------------------------    
       if (allocated(u0)) then
          sizes=8*(lexu-sxu+1)*(leyu-syu+1)*(lezu-szu+1)
          CALL MPI_ALLREDUCE(sizes,GSIZE,1,MPI_DOUBLE_PRECISION,MPI_SUM,COMM3D,MPI_CODE);sizes=GSIZE
          size_tot=size_tot+sizes
          if (mpi_chief.and.mpi_print) write(*,'(a20,1x,i15,1x,a6)') "... Writing u0",floor(sizes),"octets"
!RESPECT
          call read_write_IO_mpi(1,&
               & name,u0,iter,time,cpt,               &
               & dt_new,dt_old,                       &
               & gx,gy,gz,sxu,lexu,syu,leyu,szu,lezu, &
               & gexu-gsxu+1,geyu-gsyu+1,gezu-gszu+1, &
               & vl)
!RESPECT
       end if
       !---------------------------------------------------------------------    

       !---------------------------------------------------------------------    
       ! V0
       !---------------------------------------------------------------------    
       if (allocated(v0)) then
          sizes=8*(lexv-sxv+1)*(leyv-syv+1)*(lezv-szv+1)
          CALL MPI_ALLREDUCE(sizes,GSIZE,1,MPI_DOUBLE_PRECISION,MPI_SUM,COMM3D,MPI_CODE);sizes=GSIZE
          size_tot=size_tot+sizes
          if (mpi_chief.and.mpi_print) write(*,'(a20,1x,i15,1x,a6)') "... Writing v0",floor(sizes),"octets"
!RESPECT
          call read_write_IO_mpi(1,&
               & name,v0,iter,time,cpt,               &
               & dt_new,dt_old,                       &
               & gx,gy,gz,sxv,lexv,syv,leyv,szv,lezv, &
               & gexv-gsxv+1,geyv-gsyv+1,gezv-gszv+1, &
               & vl)
!RESPECT
       end if
       !---------------------------------------------------------------------    

       !---------------------------------------------------------------------    
       ! W0
       !---------------------------------------------------------------------    
       if (allocated(w0)) then
          sizes=8*(lexw-sxw+1)*(leyw-syw+1)*(lezw-szw+1)
          CALL MPI_ALLREDUCE(sizes,GSIZE,1,MPI_DOUBLE_PRECISION,MPI_SUM,COMM3D,MPI_CODE);sizes=GSIZE
          size_tot=size_tot+sizes
          if (mpi_chief.and.mpi_print) write(*,'(a20,1x,i15,1x,a6)') "... Writing w0",floor(sizes),"octets"
!RESPECT
          call read_write_IO_mpi(1,&
               & name,w0,iter,time,cpt,               &
               & dt_new,dt_old,                       &
               & gx,gy,gz,sxw,lexw,syw,leyw,szw,lezw, &
               & gexw-gsxw+1,geyw-gsyw+1,gezw-gszw+1, &
               & vl)
!RESPECT
       end if
       !---------------------------------------------------------------------    

       !---------------------------------------------------------------------    
       ! P
       !---------------------------------------------------------------------    
       if (allocated(pres)) then
          sizes=8*(lex-sx+1)*(ley-sy+1)*(lez-sz+1)
          CALL MPI_ALLREDUCE(sizes,GSIZE,1,MPI_DOUBLE_PRECISION,MPI_SUM,COMM3D,MPI_CODE);sizes=GSIZE
          size_tot=size_tot+sizes
          if (mpi_chief.and.mpi_print) write(*,'(a20,1x,i15,1x,a6)') "... Writing pres",floor(sizes),"octets"
!RESPECT
          call read_write_IO_mpi(1,&
               & name,pres,iter,time,cpt,             &
               & dt_new,dt_old,                       &
               & gx,gy,gz,sx,lex,sy,ley,sz,lez,       &
               & gex-gsx+1,gey-gsy+1,gez-gsz+1,       &
               & vl)
!RESPECT
       end if
       !---------------------------------------------------------------------    

       !---------------------------------------------------------------------    
       ! TP
       !---------------------------------------------------------------------    
       sizes=8*(lex-sx+1)*(ley-sy+1)*(lez-sz+1)
       CALL MPI_ALLREDUCE(sizes,GSIZE,1,MPI_DOUBLE_PRECISION,MPI_SUM,COMM3D,MPI_CODE);sizes=GSIZE
       size_tot=size_tot+sizes
       if (mpi_chief.and.mpi_print) write(*,'(a20,1x,i15,1x,a6)') "... Writing tp",floor(sizes),"octets"
       if (allocated(tp)) then
!RESPECT
          call read_write_IO_mpi(1,&
               & name,tp,iter,time,cpt,               &
               & dt_new,dt_old,                       &
               & gx,gy,gz,sx,lex,sy,ley,sz,lez,       &
               & gex-gsx+1,gey-gsy+1,gez-gsz+1,       &
               & vl)
       else
          call read_write_IO_mpi(1,&
               & name,work,iter,time,cpt,             &
               & dt_new,dt_old,                       &
               & gx,gy,gz,sx,lex,sy,ley,sz,lez,       &
               & gex-gsx+1,gey-gsy+1,gez-gsz+1,       &
               & vl)
!RESPECT
       end if
       !---------------------------------------------------------------------    

       !---------------------------------------------------------------------    
       ! TP0
       !---------------------------------------------------------------------    
       sizes=8*(lex-sx+1)*(ley-sy+1)*(lez-sz+1)
       CALL MPI_ALLREDUCE(sizes,GSIZE,1,MPI_DOUBLE_PRECISION,MPI_SUM,COMM3D,MPI_CODE);sizes=GSIZE
       size_tot=size_tot+sizes
       if (mpi_chief.and.mpi_print) write(*,'(a20,1x,i15,1x,a6)') "... Writing tp0",floor(sizes),"octets"
       if (allocated(tp0)) then
!RESPECT       
          call read_write_IO_mpi(1,&
               & name,tp0,iter,time,cpt,              &
               & dt_new,dt_old,                       &
               & gx,gy,gz,sx,lex,sy,ley,sz,lez,       &
               & gex-gsx+1,gey-gsy+1,gez-gsz+1,       &
               & vl)
       else
          call read_write_IO_mpi(1,&
               & name,work,iter,time,cpt,             &
               & dt_new,dt_old,                       &
               & gx,gy,gz,sx,lex,sy,ley,sz,lez,       &
               & gex-gsx+1,gey-gsy+1,gez-gsz+1,       &
               & vl)
       end if
!RESPECT
       !---------------------------------------------------------------------    

       !---------------------------------------------------------------------    
       ! COU
       !---------------------------------------------------------------------    
       sizes=8*(lex-sx+1)*(ley-sy+1)*(lez-sz+1)
       CALL MPI_ALLREDUCE(sizes,GSIZE,1,MPI_DOUBLE_PRECISION,MPI_SUM,COMM3D,MPI_CODE);sizes=GSIZE
       size_tot=size_tot+sizes
       if (mpi_chief.and.mpi_print) write(*,'(a20,1x,i15,1x,a6)') "... Writing cou",floor(sizes),"octets"
       if (allocated(cou)) then
!RESPECT
          call read_write_IO_mpi(1,&
               & name,cou,iter,time,cpt,              &
               & dt_new,dt_old,                       &
               & gx,gy,gz,sx,lex,sy,ley,sz,lez,       &
               & gex-gsx+1,gey-gsy+1,gez-gsz+1,       &
               & vl)
       else
          call read_write_IO_mpi(1,&
               & name,work,iter,time,cpt,             &
               & dt_new,dt_old,                       &
               & gx,gy,gz,sx,lex,sy,ley,sz,lez,       &
               & gex-gsx+1,gey-gsy+1,gez-gsz+1,       &
               & vl)
       end if
!RESPECT
       !---------------------------------------------------------------------
#if 0       
       !---------------------------------------------------------------------    
       ! DIST
       !---------------------------------------------------------------------    
       sizes=8*(lex-sx+1)*(ley-sy+1)*(lez-sz+1)
       CALL MPI_ALLREDUCE(sizes,GSIZE,1,MPI_DOUBLE_PRECISION,MPI_SUM,COMM3D,MPI_CODE);sizes=GSIZE
       size_tot=size_tot+sizes
       if (mpi_chief.and.mpi_print) write(*,'(a20,1x,i15,1x,a6)') "... Writing dist",floor(sizes),"octets"
       if (allocated(dist)) then
!RESPECT
          call read_write_IO_mpi(1,&
               & name,dist,iter,time,cpt,             &
               & dt_new,dt_old,                       &
               & gx,gy,gz,sx,lex,sy,ley,sz,lez,       &
               & gex-gsx+1,gey-gsy+1,gez-gsz+1,       &
               & vl)
       else
          call read_write_IO_mpi(1,&
               & name,work,iter,time,cpt,             &
               & dt_new,dt_old,                       &
               & gx,gy,gz,sx,lex,sy,ley,sz,lez,       &
               & gex-gsx+1,gey-gsy+1,gez-gsz+1,       &
               & vl)
!RESPECT          
       end if
       !---------------------------------------------------------------------    

       !---------------------------------------------------------------------    
       ! SCA01
       !---------------------------------------------------------------------    
       sizes=8*(lex-sx+1)*(ley-sy+1)*(lez-sz+1)
       CALL MPI_ALLREDUCE(sizes,GSIZE,1,MPI_DOUBLE_PRECISION,MPI_SUM,COMM3D,MPI_CODE);sizes=GSIZE
       size_tot=size_tot+sizes
       if (mpi_chief.and.mpi_print) write(*,'(a20,1x,i15,1x,a6)') "... Writing sca01",floor(sizes),"octets"
       if (allocated(sca01)) then
!RESPECT
          call read_write_IO_mpi(1,&
               & name,sca01,iter,time,cpt,            &
               & dt_new,dt_old,                       &
               & gx,gy,gz,sx,lex,sy,ley,sz,lez,       &
               & gex-gsx+1,gey-gsy+1,gez-gsz+1,       &
               & vl)
       else
          call read_write_IO_mpi(1,&
               & name,work,iter,time,cpt,             &
               & dt_new,dt_old,                       &
               & gx,gy,gz,sx,lex,sy,ley,sz,lez,       &
               & gex-gsx+1,gey-gsy+1,gez-gsz+1,       &
               & vl)
!RESPECT          
       end if
       !---------------------------------------------------------------------    

       !---------------------------------------------------------------------    
       ! SCA02
       !---------------------------------------------------------------------    
       sizes=8*(lex-sx+1)*(ley-sy+1)*(lez-sz+1)
       CALL MPI_ALLREDUCE(sizes,GSIZE,1,MPI_DOUBLE_PRECISION,MPI_SUM,COMM3D,MPI_CODE);sizes=GSIZE
       size_tot=size_tot+sizes
       if (mpi_chief.and.mpi_print) write(*,'(a20,1x,i15,1x,a6)') "... Writing sca02",floor(sizes),"octets"
       if (allocated(sca02)) then
!RESPECT
          call read_write_IO_mpi(1,&
               & name,sca02,iter,time,cpt,            &
               & dt_new,dt_old,                       &
               & gx,gy,gz,sx,lex,sy,ley,sz,lez,       &
               & gex-gsx+1,gey-gsy+1,gez-gsz+1,       &
               & vl)
       else
          call read_write_IO_mpi(1,&
               & name,work,iter,time,cpt,             &
               & dt_new,dt_old,                       &
               & gx,gy,gz,sx,lex,sy,ley,sz,lez,       &
               & gex-gsx+1,gey-gsy+1,gez-gsz+1,       &
               & vl)
!RESPECT
       end if
       !---------------------------------------------------------------------    

       !---------------------------------------------------------------------    
       ! SCA03
       !---------------------------------------------------------------------    
       sizes=8*(lex-sx+1)*(ley-sy+1)*(lez-sz+1)
       CALL MPI_ALLREDUCE(sizes,GSIZE,1,MPI_DOUBLE_PRECISION,MPI_SUM,COMM3D,MPI_CODE);sizes=GSIZE
       size_tot=size_tot+sizes
       if (mpi_chief.and.mpi_print) write(*,'(a20,1x,i15,1x,a6)') "... Writing sca03",floor(sizes),"octets"
       if (allocated(sca03)) then
!RESPECT
          call read_write_IO_mpi(1,&
               & name,sca03,iter,time,cpt,            &
               & dt_new,dt_old,                       &
               & gx,gy,gz,sx,lex,sy,ley,sz,lez,       &
               & gex-gsx+1,gey-gsy+1,gez-gsz+1,       &
               & vl)
       else
          call read_write_IO_mpi(1,&
               & name,work,iter,time,cpt,             &
               & dt_new,dt_old,                       &
               & gx,gy,gz,sx,lex,sy,ley,sz,lez,       &
               & gex-gsx+1,gey-gsy+1,gez-gsz+1,       &
               & vl)
!RESPECT
       end if
       !---------------------------------------------------------------------    
#endif 

       if (mpi_chief) then
          write(*,'(a,1x,a)') "End generating restart file:",&
               & trim(adjustl(name))
          write(*,'(a,1x,f15.0)') "File size (in octets) should be:", size_tot
       end if

    elseif (code==2) then
       write(name,'(i2.2)') restart_EQ
       name="restart_MPI_EQ"//trim(adjustl(name))//".bin"
       !name="restart_MPI_EQ.bin"
       if (mpi_chief) then
          write(*,*) ''//achar(27)//'[31mReading restart file'//achar(27)//'[0m'
       end if
       inquire(file=trim(adjustl(name)),exist=file_exists)
       if (file_exists) then
!RESPECT
          if (mpi_chief) write(*,'(a)') "... Reading header"
          call read_write_IO_mpi(0)
          if (allocated(u)) then
             if (mpi_chief) write(*,'(a)') "... Reading u"
             call read_write_IO_mpi(2,&
                  & name,u,iter,time,cpt,               &
                  & dt_new,dt_old,                      &
                  & gx,gy,gz,sxu,lexu,syu,leyu,szu,lezu,&
                  & gexu-gsxu+1,geyu-gsyu+1,gezu-gszu+1,&
                  & vl)
          end if
          if (allocated(v)) then
             if (mpi_chief) write(*,'(a)') "... Reading v"
             call read_write_IO_mpi(2,&
                  & name,v,iter,time,cpt,               &
                  & dt_new,dt_old,                      &
                  & gx,gy,gz,sxv,lexv,syv,leyv,szv,lezv,&
                  & gexv-gsxv+1,geyv-gsyv+1,gezv-gszv+1,&
                  & vl)
          end if
          if (allocated(w)) then
             if (mpi_chief) write(*,'(a)') "... Reading w"
             call read_write_IO_mpi(2,&
                  & name,w,iter,time,cpt,               &
                  & dt_new,dt_old,                      &
                  & gx,gy,gz,sxw,lexw,syw,leyw,szw,lezw,&
                  & gexw-gsxw+1,geyw-gsyw+1,gezw-gszw+1,&
                  & vl)
          end if
          if (allocated(u)) call comm_mpi_uvw(u,v,w)
          if (allocated(u0)) then
             if (mpi_chief) write(*,'(a)') "... Reading u0"
             call read_write_IO_mpi(2,&
                  & name,u0,iter,time,cpt,               &
                  & dt_new,dt_old,                       &
                  & gx,gy,gz,sxu,lexu,syu,leyu,szu,lezu, &
                  & gexu-gsxu+1,geyu-gsyu+1,gezu-gszu+1, &
                  & vl)
          end if
          if (allocated(v0)) then
             if (mpi_chief) write(*,'(a)') "... Reading v0"
             call read_write_IO_mpi(2,&
                  & name,v0,iter,time,cpt,               &
                  & dt_new,dt_old,                       &
                  & gx,gy,gz,sxv,lexv,syv,leyv,szv,lezv, &
                  & gexv-gsxv+1,geyv-gsyv+1,gezv-gszv+1, &
                  & vl)
          end if
          if (allocated(w0)) then
             if (mpi_chief) write(*,'(a)') "... Reading w0"
             call read_write_IO_mpi(2,&
                  & name,w0,iter,time,cpt,               &
                  & dt_new,dt_old,                       &
                  & gx,gy,gz,sxw,lexw,syw,leyw,szw,lezw, &
                  & gexw-gsxw+1,geyw-gsyw+1,gezw-gszw+1, &
                  & vl)
          end if
          if (allocated(u0)) call comm_mpi_uvw(u0,v0,w0)
          if (allocated(pres)) then
             if (mpi_chief) write(*,'(a)') "... Reading pres"
             call read_write_IO_mpi(2,&
                  & name,pres,iter,time,cpt,               &
                  & dt_new,dt_old,                         &
                  & gx,gy,gz,sx,lex,sy,ley,sz,lez,         &
                  & gex-gsx+1,gey-gsy+1,gez-gsz+1,         &
                  & vl)
             call comm_mpi_sca(pres)
          end if
          if (allocated(tp)) then
             if (mpi_chief) write(*,'(a)') "... Reading tp"
             call read_write_IO_mpi(2,&
                  & name,tp,iter,time,cpt,               &
                  & dt_new,dt_old,                       &
                  & gx,gy,gz,sx,lex,sy,ley,sz,lez,       &
                  & gex-gsx+1,gey-gsy+1,gez-gsz+1,       &
                  & vl)
             call comm_mpi_sca(tp)
          else
             call read_write_IO_mpi(2,&
                  & name,work,iter,time,cpt,             &
                  & dt_new,dt_old,                       &
                  & gx,gy,gz,sx,lex,sy,ley,sz,lez,       &
                  & gex-gsx+1,gey-gsy+1,gez-gsz+1,       &
                  & vl)
          end if
          if (allocated(tp0)) then
             if (mpi_chief) write(*,'(a)') "... Reading tp0"
             call read_write_IO_mpi(2,&
                  & name,tp0,iter,time,cpt,              &
                  & dt_new,dt_old,                       &
                  & gx,gy,gz,sx,lex,sy,ley,sz,lez,       &
                  & gex-gsx+1,gey-gsy+1,gez-gsz+1,       &
                  & vl)
             call comm_mpi_sca(tp0)
          else
             call read_write_IO_mpi(2,&
                  & name,work,iter,time,cpt,             &
                  & dt_new,dt_old,                       &
                  & gx,gy,gz,sx,lex,sy,ley,sz,lez,       &
                  & gex-gsx+1,gey-gsy+1,gez-gsz+1,       &
                  & vl)
          end if
          if (allocated(cou)) then
             if (mpi_chief) write(*,'(a)') "... Reading cou"
             call read_write_IO_mpi(2,&
                  & name,cou,iter,time,cpt,               &
                  & dt_new,dt_old,                        &
                  & gx,gy,gz,sx,lex,sy,ley,sz,lez,        &
                  & gex-gsx+1,gey-gsy+1,gez-gsz+1,        &
                  & vl)
             call comm_mpi_sca(cou)
          else
             call read_write_IO_mpi(2,&
                  & name,work,iter,time,cpt,             &
                  & dt_new,dt_old,                       &
                  & gx,gy,gz,sx,lex,sy,ley,sz,lez,       &
                  & gex-gsx+1,gey-gsy+1,gez-gsz+1,       &
                  & vl)
          end if
#if 0
          if (allocated(dist)) then
             if (mpi_chief) write(*,'(a)') "... Reading dist"
             call read_write_IO_mpi(2,&
                  & name,dist,iter,time,cpt,             &
                  & dt_new,dt_old,                       &
                  & gx,gy,gz,sx,lex,sy,ley,sz,lez,       &
                  & gex-gsx+1,gey-gsy+1,gez-gsz+1,       &
                  & vl)
             call comm_mpi_sca(dist)
          else
             call read_write_IO_mpi(2,&
                  & name,work,iter,time,cpt,             &
                  & dt_new,dt_old,                       &
                  & gx,gy,gz,sx,lex,sy,ley,sz,lez,       &
                  & gex-gsx+1,gey-gsy+1,gez-gsz+1,       &
                  & vl)
          end if
          if (allocated(sca01)) then
             if (mpi_chief) write(*,'(a)') "... Reading sca01"
             call read_write_IO_mpi(2,&
                  & name,sca01,iter,time,cpt,            &
                  & dt_new,dt_old,                       &
                  & gx,gy,gz,sx,lex,sy,ley,sz,lez,       &
                  & gex-gsx+1,gey-gsy+1,gez-gsz+1,       &
                  & vl)
             call comm_mpi_sca(sca01)
          else
             call read_write_IO_mpi(2,&
                  & name,work,iter,time,cpt,             &
                  & dt_new,dt_old,                       &
                  & gx,gy,gz,sx,lex,sy,ley,sz,lez,       &
                  & gex-gsx+1,gey-gsy+1,gez-gsz+1,       &
                  & vl)
          end if
          if (allocated(sca02)) then
             if (mpi_chief) write(*,'(a)') "... Reading sca02"
             call read_write_IO_mpi(2,&
                  & name,sca02,iter,time,cpt,            &
                  & dt_new,dt_old,                       &
                  & gx,gy,gz,sx,lex,sy,ley,sz,lez,       &
                  & gex-gsx+1,gey-gsy+1,gez-gsz+1,       &
                  & vl)
             call comm_mpi_sca(sca02)
          else
             call read_write_IO_mpi(2,&
                  & name,work,iter,time,cpt,             &
                  & dt_new,dt_old,                       &
                  & gx,gy,gz,sx,lex,sy,ley,sz,lez,       &
                  & gex-gsx+1,gey-gsy+1,gez-gsz+1,       &
                  & vl)
          end if
          if (allocated(sca03)) then
             if (mpi_chief) write(*,'(a)') "... Reading sca03"
             call read_write_IO_mpi(2,&
                  & name,sca03,iter,time,cpt,            &
                  & dt_new,dt_old,                       &
                  & gx,gy,gz,sx,lex,sy,ley,sz,lez,       &
                  & gex-gsx+1,gey-gsy+1,gez-gsz+1,       &
                  & vl)
             call comm_mpi_sca(sca03)
          else
             call read_write_IO_mpi(2,&
                  & name,work,iter,time,cpt,             &
                  & dt_new,dt_old,                       &
                  & gx,gy,gz,sx,lex,sy,ley,sz,lez,       &
                  & gex-gsx+1,gey-gsy+1,gez-gsz+1,       &
                  & vl)
          end if
#endif
!RESPECT          
          if (mpi_chief) write(*,'(a,1x,a)') "End reading restart file:   ",&
               & trim(adjustl(name))

       else

          if (mpi_chief) write(*,'(a,1x,a,1x,a)') "The file", trim(adjustl(name)), &
               & "does not exists, STOP"
          call mpi_finalize(MPI_CODE)
          stop

       end if
    end if

  end subroutine read_write_EQ_mpi

#define v1 1
#if v1
  
  subroutine read_write_IO_mpi(choice,name,var,iter,time,cpt, &
       & dt_new,dt_old,                                       &
       & lgx,lgy,lgz,lsx,lex,lsy,ley,lsz,lez,lnx,lny,lnz,     &
       & vl)
#if RESPECT
  use Module_VOFLag_ParticlesDataStructure
#endif
    !--------------------------------------------------------------
    ! Global variables
    !--------------------------------------------------------------
    integer, intent(in)                              :: choice
    integer, intent(in), optional                    :: lgx,lgy,lgz
    integer, intent(in), optional                    :: lsx,lex,lsy,ley,lsz,lez
    integer, intent(in), optional                    :: lnx,lny,lnz
    integer, intent(inout), optional                 :: iter,cpt
    real(8), intent(inout), optional                 :: time
    real(8), intent(inout), optional                 :: dt_new,dt_old
    real(8), allocatable, dimension(:,:,:), optional :: var
    character(len=*), intent(in), optional           :: name
#if RESPECT
    type(struct_vof_lag), intent(inout), optional    :: vl
#endif
    !--------------------------------------------------------------
    ! Local  variables
    !--------------------------------------------------------------
    integer                                          :: old_nx,old_ny,old_nz
    integer, parameter                               :: rank_tab=3
    integer                                          :: size_double_precision
    integer                                          :: size_integer
    integer                                          :: type_sub,type_sub_view
    integer                                          :: size_type_sub,size_type_sub_view
    integer(kind=mpi_offset_kind), save              :: step_init,size_step
    integer, dimension(rank_tab)                     :: profile,profile_sub,coords_start
    integer, dimension(rank_tab)                     :: profile_view,profile_sub_view
    integer, dimension(rank_tab)                     :: coords_start_view
    ! mpi_constant
    integer                                          :: descriptor
    integer                                          :: code,l
    integer, dimension(mpi_status_size)              :: status
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! size of base type mpi_double_precision & integer 
    !--------------------------------------------------------------
    call mpi_type_size(mpi_double_precision,&
         & size_double_precision,           &
         & code)
    call mpi_type_size(mpi_integer,         &
         & size_integer,                    &
         & code)
    !--------------------------------------------------------------

    if (choice==0) then
       !--------------------------------------------------------------
       ! Initilization
       !--------------------------------------------------------------
       step_init=0
       size_step=0
       return
       !--------------------------------------------------------------
    elseif (choice==1) then 
       !--------------------------------------------------------------
       ! open file "name" in writting mode
       !--------------------------------------------------------------
       if (step_init==0) then 
          call mpi_file_open(comm3d,             &
               & trim(adjustl(name)),            &
               & mpi_mode_wronly+mpi_mode_create,&
               & mpi_info_null,                  &
               & descriptor,                     &
               & code)
       else
          call mpi_file_open(comm3d,             &
               & trim(adjustl(name)),            &
               & mpi_mode_wronly+mpi_mode_append,&
               & mpi_info_null,                  &
               & descriptor,                     &
               & code)
       end if
       !--------------------------------------------------------------
    elseif (choice==2) then
       !--------------------------------------------------------------
       ! open file "name" in reading mode
       !--------------------------------------------------------------
       call mpi_file_open(comm3d, &
            & trim(adjustl(name)),&
            & mpi_mode_rdonly,    &
            & mpi_info_null,      &
            & descriptor,         &
            & code)
       !--------------------------------------------------------------
    end if

    !--------------------------------------------------------------
    ! testing file opening
    !--------------------------------------------------------------
    if (code/=mpi_success) then
       write(*,*) 'WARNING FILE OPENING IN MPI-IO'
       call mpi_abort(comm3d,2,code)
       call finalization_mpi
    end if
    !--------------------------------------------------------------

    if (step_init==0) then

       if (choice==1) then 
          !--------------------------------------------------------------
          ! Writing time and mesh
          !--------------------------------------------------------------
          if (mpi_chief) then
             call mpi_file_write(descriptor,&
                  & iter,                   &
                  & 1,                      &
                  & mpi_integer,            &
                  & status,                 &
                  & code)
             call mpi_file_write(descriptor,&
                  & time,                   &
                  & 1,                      &
                  & mpi_double_precision,   &
                  & status,                 &
                  & code)
             call mpi_file_write(descriptor,&
                  & cpt,                    &
                  & 1,                      &
                  & mpi_integer,            &
                  & status,                 &
                  & code)
             call mpi_file_write(descriptor,&
                  & dt_new,                 &
                  & 1,                      &
                  & mpi_double_precision,   &
                  & status,                 &
                  & code)
             call mpi_file_write(descriptor,&
                  & dt_old,                 &
                  & 1,                      &
                  & mpi_double_precision,   &
                  & status,                 &
                  & code)
             call mpi_file_write(descriptor,&
                  & nx,                     &
                  & 1,                      &
                  & mpi_integer,            &
                  & status,                 &
                  & code)
             call mpi_file_write(descriptor,&
                  & ny,                     &
                  & 1,                      &
                  & mpi_integer,            &
                  & status,                 &
                  & code)
             call mpi_file_write(descriptor,&
                  & nz,                     &
                  & 1,                      &
                  & mpi_integer,            &
                  & status,                 &
                  & code)
#if RESPECT
             if (allocated(vl%objet)) then
               do l=1,size(vl%objet)
                  call mpi_file_write(descriptor,&
                     & vl%objet(l)%pos,          &
                     & 3,                        &
                     & mpi_double_precision,     &
                     & status,                   &
                     & code)
                  call mpi_file_write(descriptor,&
                     & vl%objet(l)%v,            &
                     & 3,                        &
                     & mpi_double_precision,     &
                     & status,                   &
                     & code)
                  call mpi_file_write(descriptor,&
                     & vl%objet(l)%vn,           &
                     & 3,                        &
                     & mpi_double_precision,     &
                     & status,                   &
                     & code)
                  call mpi_file_write(descriptor,&
                     & vl%objet(l)%euler_angle,  &
                     & 3,                        &
                     & mpi_double_precision,     &
                     & status,                   &
                     & code)
                  call mpi_file_write(descriptor,&
                     & vl%objet(l)%orientation,  &
                     & 4,                        &
                     & mpi_double_precision,     &
                     & status,                   &
                     & code)
                  call mpi_file_write(descriptor,&
                     & vl%objet(l)%omeg,         &
                     & 3,                        &
                     & mpi_double_precision,     &
                     & status,                   &
                     & code)
               enddo
             else
               stop "Erreur dans l'ecriture du fichier de reprise: RESPECT"
             endif
#endif
          end if
       else
          !--------------------------------------------------------------
          ! Reading time and mesh
          !--------------------------------------------------------------
          call mpi_file_read_all(descriptor,&
               & iter,                      &
               & 1,                         &
               & mpi_integer,               &
               & status,                    &
               & code)
          call mpi_file_read_all(descriptor,&
               & time,                      &
               & 1,                         &
               & mpi_double_precision,      &
               & status,                    &
               & code)
          call mpi_file_read_all(descriptor,&
               & cpt,                       &
               & 1,                         &
               & mpi_integer,               &
               & status,                    &
               & code)
          call mpi_file_read_all(descriptor,&
               & dt_new,                    &
               & 1,                         &
               & mpi_double_precision,      &
               & status,                    &
               & code)
          call mpi_file_read_all(descriptor,&
               & dt_old,                    &
               & 1,                         &
               & mpi_double_precision,      &
               & status,                    &
               & code)
          call mpi_file_read_all(descriptor,&
               & old_nx,                    &
               & 1,                         &
               & mpi_integer,               &
               & status,                    &
               & code)
          call mpi_file_read_all(descriptor,&
               & old_ny,                    &
               & 1,                         &
               & mpi_integer,               &
               & status,                    &
               & code)
          call mpi_file_read_all(descriptor,&
               & old_nz,                    &
               & 1,                         &
               & mpi_integer,               &
               & status,                    &
               & code)
#if RESPECT
             if (allocated(vl%objet)) then
               do l=1,size(vl%objet)
                  call mpi_file_read_all(descriptor,&
                     & vl%objet(l)%pos,             &
                     & 3,                           &
                     & mpi_double_precision,        &
                     & status,                      &
                     & code)
                  call mpi_file_read_all(descriptor,&
                     & vl%objet(l)%v,               &
                     & 3,                           &
                     & mpi_double_precision,        &
                     & status,                      &
                     & code)
                  call mpi_file_read_all(descriptor,&
                     & vl%objet(l)%vn,              &
                     & 3,                           &
                     & mpi_double_precision,        &
                     & status,                      &
                     & code)
                  call mpi_file_read_all(descriptor,&
                     & vl%objet(l)%euler_angle,     &
                     & 3,                           &
                     & mpi_double_precision,        &
                     & status,                      &
                     & code)
                  call mpi_file_read_all(descriptor,&
                     & vl%objet(l)%orientation,     &
                     & 4,                           &
                     & mpi_double_precision,        &
                     & status,                      &
                     & code)
                  call mpi_file_read_all(descriptor,&
                     & vl%objet(l)%omeg,            &
                     & 3,                           &
                     & mpi_double_precision,        &
                     & status,                      &
                     & code)
               enddo
             else
               stop "Erreur dans la lecture du fichier de reprise: RESPECT"
             endif
#endif
          
          !--------------------------------------------------------------
          ! mesh checking
          !--------------------------------------------------------------
          if (nx/=old_nx.or.ny/=old_ny.or.nz/=old_nz) then
             if (mpi_chief) then
                write(*,*) "mesh has changed, interpolation needed, STOP"
                write(*,*) iter,time
                write(*,*) nx,old_nx
                write(*,*) ny,old_ny
                write(*,*) nz,old_nz
             end if
             STOP
          end if

       end if

       !--------------------------------------------------------------
       ! step 
       !--------------------------------------------------------------
       size_step=2*size_integer        & ! iter + cpt
            & +3*size_double_precision & ! time, dt_new,dt_old
            & +3*size_integer            ! mesh
#if RESPECT
       size_step=size_step+size(vl%objet)*19*size_double_precision
#endif
       !--------------------------------------------------------------

    end if

    !--------------------------------------------------------------
    ! shape local full matrix var
    ! with guard cells
    !--------------------------------------------------------------
    profile(:)=shape(var) 
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! shape matrix var
    ! WITHOUT guard cells
    !--------------------------------------------------------------
    profile_sub=shape(var(lsx:lex,lsy:ley,lsz:lez)) 
    !--------------------------------------------------------------
   
    !--------------------------------------------------------------
    ! starting coordinates in the full matrix (local indexes, from 0)
    !--------------------------------------------------------------
    coords_start=(/lgx,lgy,lgz/)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! creation type_sub (localisation in the local matix)
    !--------------------------------------------------------------
    call mpi_type_create_subarray(rank_tab,&
         & profile,                        &
         & profile_sub,                    &
         & coords_start,                   &
         & mpi_order_fortran,              &
         & mpi_double_precision,           &
         & type_sub,                       &
         & code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! type validation 
    !--------------------------------------------------------------
    call mpi_type_commit(type_sub,code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! new type size
    !--------------------------------------------------------------
    call mpi_type_size(type_sub,size_type_sub,code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! type creation for file view (global)
    !--------------------------------------------------------------
    profile_view=(/lnx,lny,lnz/)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! type creation for file view
    !--------------------------------------------------------------
    profile_sub_view=profile_sub
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! starting coordinates (local indexes, from 0)
    !--------------------------------------------------------------
    if (dim==2) then
       coords_start_view=(/lsx,lsy,0/)
    else
       coords_start_view=(/lsx,lsy,lsz/)
    end if
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! creation type_sub_view
    !--------------------------------------------------------------
    call mpi_type_create_subarray(rank_tab,&
         & profile_view,                   &
         & profile_sub_view,               &
         & coords_start_view,              &
         & mpi_order_fortran,              &
         & mpi_double_precision,           &
         & type_sub_view,                  &
         & code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! type validation 
    !--------------------------------------------------------------
    call mpi_type_commit(type_sub_view,code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! new type size
    !--------------------------------------------------------------
    call mpi_type_size(type_sub_view,size_type_sub_view,code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! view definition from starting point step_init=0
    !--------------------------------------------------------------
    step_init=step_init+size_step
    size_step=size_double_precision*product(profile_view)
    call mpi_file_set_view(descriptor,&
         & step_init,                 &
         & mpi_double_precision,      &
         & type_sub_view,             &
         & "native",                  &
         & mpi_info_null,&
         & code)
    !--------------------------------------------------------------

    if (choice==1) then 
       !--------------------------------------------------------------
       ! file writing from all process defined by the previous view
       !--------------------------------------------------------------
       call mpi_file_write_all(descriptor,&
            & var,                        &
            & 1,                          &
            & type_sub,                   &
            !& status,                     &
            & mpi_status_ignore,          &
            & code)
       !--------------------------------------------------------------
    else
       !--------------------------------------------------------------
       ! file reading from all process defined by the previous view
       !--------------------------------------------------------------
       call mpi_file_read_all(descriptor,&
            & var,                       &
            & 1,                         &
            & type_sub,                  &
            !& status,                    &
            & mpi_status_ignore,         &
            & code)
       !--------------------------------------------------------------
    end if

    !--------------------------------------------------------------
    ! close file
    !--------------------------------------------------------------
    call mpi_file_close(descriptor,code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! free types 
    !--------------------------------------------------------------
    call mpi_type_free(type_sub,code)
    call mpi_type_free(type_sub_view,code)
    !--------------------------------------------------------------
    
  end subroutine read_write_IO_mpi

#else 

  subroutine read_write_IO_mpi(choice,name,var,iter,time,cpt, &
       & lgx,lgy,lgz,lsx,lex,lsy,ley,lsz,lez,lnx,lny,lnz)
    !--------------------------------------------------------------
    ! Global variables
    !--------------------------------------------------------------
    integer, intent(in)                              :: choice
    integer, intent(in), optional                    :: lgx,lgy,lgz
    integer, intent(in), optional                    :: lsx,lex,lsy,ley,lsz,lez
    integer, intent(in), optional                    :: lnx,lny,lnz
    integer, intent(inout), optional                 :: iter,cpt
    real(8), intent(inout), optional                 :: time
    real(8), allocatable, dimension(:,:,:), optional :: var
    character(len=*), intent(in), optional           :: name
    !--------------------------------------------------------------
    ! Local  variables
    !--------------------------------------------------------------
    integer                                          :: i,j,k,ii,jj,kk
    integer                                          :: old_nx,old_ny,old_nz
    integer, parameter                               :: rank_tab=3
    integer                                          :: size_double_precision
    integer                                          :: size_integer
    integer                                          :: type_sub,type_sub_view
    integer                                          :: size_type_sub,size_type_sub_view
    integer(kind=mpi_offset_kind), save              :: step_init,size_step
    integer, dimension(rank_tab)                     :: profile,profile_sub,coords_start
    integer, dimension(rank_tab)                     :: profile_view,profile_sub_view
    integer, dimension(rank_tab)                     :: coords_start_view
    ! mpi_constant
    integer                                          :: descriptor
    integer                                          :: code 
    integer, dimension(mpi_status_size)              :: status
    real(8), dimension(:,:,:), allocatable           :: var_loc
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! size of base type mpi_double_precision & integer 
    !--------------------------------------------------------------
    call mpi_type_size(mpi_double_precision,&
         & size_double_precision,           &
         & code)
    call mpi_type_size(mpi_integer,         &
         & size_integer,                    &
         & code)
    !--------------------------------------------------------------

    if (choice==0) then
       !--------------------------------------------------------------
       ! Initilization
       !--------------------------------------------------------------
       step_init=0
       size_step=0
       return
       !--------------------------------------------------------------
    elseif (choice==1) then 
       !--------------------------------------------------------------
       ! open file "name" in writting mode
       !--------------------------------------------------------------
       if (step_init==0) then 
          call mpi_file_open(comm3d,             &
               & trim(adjustl(name)),            &
               & mpi_mode_wronly+mpi_mode_create,&
               & mpi_info_null,                  &
               & descriptor,                     &
               & code)
       else
          call mpi_file_open(comm3d,             &
               & trim(adjustl(name)),            &
               & mpi_mode_wronly+mpi_mode_append,&
               & mpi_info_null,                  &
               & descriptor,                     &
               & code)
       end if
       !--------------------------------------------------------------
    elseif (choice==2) then
       !--------------------------------------------------------------
       ! open file "name" in reading mode
       !--------------------------------------------------------------
       call mpi_file_open(comm3d, &
            & trim(adjustl(name)),&
            & mpi_mode_rdonly,    &
            & mpi_info_null,      &
            & descriptor,         &
            & code)
       !--------------------------------------------------------------
    end if

    !--------------------------------------------------------------
    ! testing file opening
    !--------------------------------------------------------------
    if (code/=mpi_success) then
       write(*,*) 'WARNING FILE OPENING IN MPI-IO'
       call mpi_abort(comm3d,2,code)
       call finalization_mpi
    end if
    !--------------------------------------------------------------

    if (step_init==0) then

       if (choice==1) then 
          !--------------------------------------------------------------
          ! Writing time and mesh
          !--------------------------------------------------------------
          if (mpi_chief) then
             call mpi_file_write(descriptor,&
                  & iter,                   &
                  & 1,                      &
                  & mpi_integer,            &
                  & status,                 &
                  & code)
             call mpi_file_write(descriptor,&
                  & time,                   &
                  & 1,                      &
                  & mpi_double_precision,   &
                  & status,                 &
                  & code)
             call mpi_file_write(descriptor,&
                  & cpt,                    &
                  & 1,                      &
                  & mpi_integer,            &
                  & status,                 &
                  & code)
             call mpi_file_write(descriptor,&
                  & nx,                     &
                  & 1,                      &
                  & mpi_integer,            &
                  & status,                 &
                  & code)
             call mpi_file_write(descriptor,&
                  & ny,                     &
                  & 1,                      &
                  & mpi_integer,            &
                  & status,                 &
                  & code)
             call mpi_file_write(descriptor,&
                  & nz,                     &
                  & 1,                      &
                  & mpi_integer,            &
                  & status,                 &
                  & code)
          end if
       else
          !--------------------------------------------------------------
          ! Reading time and mesh
          !--------------------------------------------------------------
          call mpi_file_read_all(descriptor,&
               & iter,                      &
               & 1,                         &
               & mpi_integer,               &
               & status,                    &
               & code)
          call mpi_file_read_all(descriptor,&
               & time,                      &
               & 1,                         &
               & mpi_double_precision,      &
               & status,                    &
               & code)
          call mpi_file_read_all(descriptor,&
               & cpt,                       &
               & 1,                         &
               & mpi_integer,               &
               & status,                    &
               & code)
          call mpi_file_read_all(descriptor,&
               & old_nx,                    &
               & 1,                         &
               & mpi_integer,               &
               & status,                    &
               & code)
          call mpi_file_read_all(descriptor,&
               & old_ny,                    &
               & 1,                         &
               & mpi_integer,               &
               & status,                    &
               & code)
          call mpi_file_read_all(descriptor,&
               & old_nz,                    &
               & 1,                         &
               & mpi_integer,               &
               & status,                    &
               & code)

          !--------------------------------------------------------------
          ! mesh checking
          !--------------------------------------------------------------
          if (nx/=old_nx.or.ny/=old_ny.or.nz/=old_nz) then
             if (mpi_chief) then
                write(*,*) "mesh has changed, interpolation needed, STOP"
                write(*,*) iter,time
                write(*,*) nx,old_nx
                write(*,*) ny,old_ny
                write(*,*) nz,old_nz
             end if
             STOP
          end if

       end if

       !--------------------------------------------------------------
       ! step 
       !--------------------------------------------------------------
       size_step=2*size_integer      & ! iter + cpt
            & +size_double_precision & ! time
            & +3*size_integer          ! mesh
       !--------------------------------------------------------------

    end if

    !--------------------------------------------------------------
    ! shape matrix var
    ! WITHOUT guard cells
    !--------------------------------------------------------------
    profile_sub_view=shape(var(lsx:lex,lsy:ley,lsz:lez)) 
    !write(*,*) rank, profile_sub_view
    !--------------------------------------------------------------
    
    !--------------------------------------------------------------
    ! type creation for file view (global)
    !--------------------------------------------------------------
    profile_view=(/lnx,lny,lnz/)
    !write(*,*) rank, profile_view
    !--------------------------------------------------------------

    allocate(var_loc(1:profile_sub_view(1), &
         & 1:profile_sub_view(2),           &
         & 1:profile_sub_view(3)))

    kk=1
    do k=lsz,lez
       jj=1
       do j=lsy,ley
          ii=1
          do i=lsx,lex
             var_loc(ii,jj,kk)=var(i,j,k)
             ii=ii+1
          end do
          jj=jj+1
       end do
       kk=kk+1
    end do

    
    !--------------------------------------------------------------
    ! starting coordinates (local indexes, from 0)
    !--------------------------------------------------------------
    if (dim==2) then
       coords_start_view=(/lsx,lsy,0/)
    else 
       coords_start_view=(/lsx,lsy,lsz/)
    end if
    !write(*,*) rank, coords_start_view
    !--------------------------------------------------------------


    !--------------------------------------------------------------
    ! creation type_sub_view
    !--------------------------------------------------------------
    call mpi_type_create_subarray(rank_tab,&
         & profile_view,                   &
         & profile_sub_view,               &
         & coords_start_view,              &
         & mpi_order_fortran,              &
         & mpi_double_precision,           &
         & type_sub_view,                  &
         & code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! type validation 
    !--------------------------------------------------------------
    call mpi_type_commit(type_sub_view,code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! new type size
    !--------------------------------------------------------------
    call mpi_type_size(type_sub_view,size_type_sub_view,code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! view definition from starting point step_init=0
    !--------------------------------------------------------------
    step_init=step_init+size_step
    size_step=size_double_precision*product(profile_view)
    call mpi_file_set_view(descriptor,&
         & step_init,                 &
         & mpi_double_precision,      &
         & type_sub_view,             &
         & "native",                  &
         & mpi_info_null,&
         & code)
    !--------------------------------------------------------------

    if (choice==1) then 
       !--------------------------------------------------------------
       ! file writing from all process defined by the previous view
       !--------------------------------------------------------------
       call mpi_file_write_all(descriptor,&
            & var_loc,                    &
            & product(profile_sub_view),  &
            & mpi_double_precision,       &
            & mpi_status_ignore,          &
            & code)
       !--------------------------------------------------------------
    else
       !--------------------------------------------------------------
       ! file reading from all process defined by the previous view
       !--------------------------------------------------------------
       call mpi_file_read_all(descriptor,&
            & var_loc,                   &
            & product(profile_sub_view), &
            & mpi_double_precision,      &
            & mpi_status_ignore,         &
            & code)
       !--------------------------------------------------------------
       
       kk=1
       do k=lsz,lez
          jj=1
          do j=lsy,ley
             ii=1
             do i=lsx,lex
                var(i,j,k)=var_loc(ii,jj,kk)
                ii=ii+1
             end do
             jj=jj+1
          end do
          kk=kk+1
       end do
       
    end if

    !--------------------------------------------------------------
    ! deallocate
    !--------------------------------------------------------------
    deallocate(var_loc)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! close file
    !--------------------------------------------------------------
    call mpi_file_close(descriptor,code)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! free types 
    !--------------------------------------------------------------
    call mpi_type_free(type_sub_view,code)
    !--------------------------------------------------------------

  end subroutine read_write_IO_mpi

#endif


  subroutine read_write_restart_by_proc(choice,iter,time, &
       & dt_new,dt_old,                                   &
       & u,v,w,u0,v0,w0,pres,                             &
       & sca01,sca02,sca03,sca04,sca05,                   &
       & sca06,sca07,sca08,sca09,sca10)
    use mod_Parameters
    implicit none
    
    integer, intent(in)                                             :: choice
    integer, intent(inout)                                          :: iter
    real(8), intent(inout)                                          :: time
    real(8), intent(inout)                                          :: dt_new,dt_old
    real(8), dimension(:,:,:), allocatable, intent(inout), optional :: u,v,w,u0,v0,w0,pres
    real(8), dimension(:,:,:), allocatable, intent(inout), optional :: sca01,sca02,sca03,sca04,sca05
    real(8), dimension(:,:,:), allocatable, intent(inout), optional :: sca06,sca07,sca08,sca09,sca10

    character(5)                                                    :: name_proc
    character(2)                                                    :: name_iter
    character(len=50)                                               :: name
    integer                                                         :: num=0,iunit

    iunit=1000000


    
    if (choice==1) then
       if (num<1) then
          num=num+1
       else
          num=0
       end if
    end if
    
    if (choice==1) then
       write(name_iter,'(i2.2)') num
       write(name_proc,'(i5.5)') rank
       name="restart_num"//name_iter//"_proc"//name_proc//".bin"
       open(unit=rank+iunit,file=name,form='unformatted')

       if (mpi_chief) then
          write(*,*) ''//achar(27)//'[31m===========Generating restart file (by proc)==================='//achar(27)//'[0m'
       end if

       
       write(rank+iunit) iter,time,dt_new,dt_old
       write(rank+iunit) u,v,w,pres,u0,v0,w0
       if (present(sca01)) then
          if (allocated(sca01)) write(rank+iunit) sca01
       end if
       if (present(sca02)) then
          if (allocated(sca02)) write(rank+iunit) sca02
       end if
       if (present(sca03)) then
          if (allocated(sca03)) write(rank+iunit) sca03
       end if
       if (present(sca04)) then
          if (allocated(sca04)) write(rank+iunit) sca04
       end if
       if (present(sca05)) then
          if (allocated(sca05)) write(rank+iunit) sca05
       end if
       if (present(sca06)) then
          if (allocated(sca06)) write(rank+iunit) sca06
       end if
       if (present(sca07)) then
          if (allocated(sca07)) write(rank+iunit) sca07
       end if
       if (present(sca08)) then
          if (allocated(sca08)) write(rank+iunit) sca08
       end if
       if (present(sca09)) then
          if (allocated(sca09)) write(rank+iunit) sca09
       end if
       if (present(sca10)) then
          if (allocated(sca10)) write(rank+iunit) sca10
       end if

       write(*,'(a,1x,a)') "Generating restart file:   ",&
            & trim(adjustl(name))


    else
       write(name_iter,'(i2.2)') restart_EQ
       write(name_proc,'(i5.5)') rank
       name="restart_num"//name_iter//"_proc"//name_proc//".bin"
       open(unit=rank+iunit,file=name,form='unformatted')
       
       if (mpi_chief) then
          write(*,*) ''//achar(27)//'[31m=============Reading restart file (by proc)===================='//achar(27)//'[0m'
       end if


       read(rank+iunit) iter,time,dt_new,dt_old
       read(rank+iunit) u,v,w,pres,u0,v0,w0

       call comm_mpi_uvw(u,v,w)
       call comm_mpi_uvw(u0,v0,w0)
       call comm_mpi_sca(pres)

       if (present(sca01)) then
          if (allocated(sca01)) then
             read(rank+iunit) sca01
             call comm_mpi_sca(sca01)
          end if
       end if
       if (present(sca02)) then
          if (allocated(sca02)) then
             read(rank+iunit) sca02
             call comm_mpi_sca(sca02)
          end if
       end if
       if (present(sca03)) then
          if (allocated(sca03)) then 
             read(rank+iunit) sca03
             call comm_mpi_sca(sca03)
          end if
       end if
       if (present(sca04)) then
          if (allocated(sca04)) then 
             read(rank+iunit) sca04
             call comm_mpi_sca(sca04)
          end if
       end if
       if (present(sca05)) then
          if (allocated(sca05)) then 
             read(rank+iunit) sca05
             call comm_mpi_sca(sca05)
          end if
       end if
       if (present(sca06)) then
          if (allocated(sca06)) then 
             read(rank+iunit) sca06
             call comm_mpi_sca(sca06)
          end if
       end if
       if (present(sca07)) then
          if (allocated(sca07)) then 
             read(rank+iunit) sca07
             call comm_mpi_sca(sca07)
          end if
       end if
       if (present(sca08)) then
          if (allocated(sca08)) then 
             read(rank+iunit) sca08
             call comm_mpi_sca(sca08)
          end if
       end if
       if (present(sca09)) then
          if (allocated(sca09)) then 
             read(rank+iunit) sca09
             call comm_mpi_sca(sca09)
          end if
       end if
       if (present(sca10)) then
          if (allocated(sca10)) then 
             read(rank+iunit) sca10
             call comm_mpi_sca(sca10)
          end if
       end if

       write(*,'(a,1x,a)') "Reading restart file:   ",&
            & trim(adjustl(name))
    end if

    close(unit=rank+iunit)
    
  end subroutine read_write_restart_by_proc
  
end module mod_mpi
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

