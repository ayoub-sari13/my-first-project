!===============================================================================
module Bib_VOFLag_SubDomain_Neighbouring
   !===============================================================================
   contains
   !-----------------------------------------------------------------------------  
   !> @author jorge cesar brandle de motta, amine chadil
   !
   !> @todo le nombre de copie nbt est faux! il n'est pas utiliser dans le code ainsi 
   !>  que le type de voisin 
   !
   !> @brief donne les voisins du proc courant et l'organisation de ceux-ci comme telle:
   !>  rank_voisins(1:nb_voisins,1) donne le rang du voisin
   !>  rank_voisins(1:nb_voisins,2) dit le type de voisin
   !>                                0  rank
   !>                                1  gauche
   !>                                2  droite    
   !>                                3  dessous 
   !>                                4  dessus
   !>                                5  arriere   
   !>                                6  avant    
   !>          1 + 3                 7  gauche -dessous
   !>          1 + 4                 8  gauche -dessus 
   !>          1 + 5                 9  gauche -arriere 
   !>          1 + 6                 10 gauche -avant
   !>          2 + 3                 11 droite -dessous
   !>          2 + 4                 12 droite -dessus
   !>          2 + 5                 13 droite -arriere
   !>          2 + 6                 14 droite -avant
   !>          3 + 5                 15 dessous-arriere
   !>          3 + 6                 16 dessous-avant
   !>          4 + 5                 17 dessus -arriere
   !>          4 + 6                 18 dessus -avant
   !>          7 + 5                 19 gauche -dessous-arriere
   !>          7 + 6                 20 gauche -dessous-avant
   !>          8 + 5                 21 gauche -dessus -arriere
   !>          8 + 6                 22 gauche -dessus -avant
   !>          11+ 5                 23 droite -dessous-arriere
   !>          11+ 6                 24 droite -dessous-avant
   !>          12+ 5                 25 droite -dessus -arriere
   !>          12+ 6                 26 droite -dessus -avant
   !>                               -[1-26] les memes mais avec passage par periodique 
   !>   rank_voisins(1:nb_voisins,3) dit le nbt a chercher pour les particules
   !-----------------------------------------------------------------------------
   subroutine SubDomain_Neighbouring(ndim)
      !===============================================================================
      !modules
      !===============================================================================
      !-------------------------------------------------------------------------------
      !Modules de definition des structures et variables => src/mod/module_...f90
      !-------------------------------------------------------------------------------
      use Module_VOFLag_ParticlesDataStructure
      use Module_VOFLag_SubDomain_Data
      use mod_Parameters,                       only : rank,nproc,deeptracking
      use mod_mpi
      !-------------------------------------------------------------------------------
      !===============================================================================
      !declarations des variables
      !===============================================================================
      implicit none
      !-------------------------------------------------------------------------------
      !variables globales
      !-------------------------------------------------------------------------------
      integer, intent(in)                       :: ndim
      !-------------------------------------------------------------------------------
      !variables locales 
      !-------------------------------------------------------------------------------
      integer                               :: i,j,nd,nbv,tdv,np
      integer, dimension(27,3)              :: depl_local,depl_vois ! vecteur pour savoir de combien on s'est deplace
      integer, dimension(0:nproc-1)         :: nb_voisins_np
      integer, dimension(0:nproc-1,27,3)    :: rank_voisins_np
      integer                               :: rank_nbtimes_local
      double precision, dimension(27,3)     :: rank_symper_local
      !-------------------------------------------------------------------------------

      !-------------------------------------------------------------------------------
      if (deeptracking) write(*,*) 'entree SubDomain_Neighbouring'
      !-------------------------------------------------------------------------------

      !-------------------------------------------------------------------------------
      !initialisation des variables locales 
      !-------------------------------------------------------------------------------
      depl_local(:,:) = 0
      nb_voisins = 0
      rank_voisins(:,1)=rank
      rank_voisins(:,2)=rank
      !-------------------------------------------------------------------------------
      !voisins dans les six faces du cube
      !-------------------------------------------------------------------------------
      do i=1,2*ndim+1
         if (m_vois(i) .ne. -1) then
            nb_voisins = nb_voisins + 1
            rank_voisins(nb_voisins,1) = m_vois(i)
            rank_voisins(nb_voisins,2) = i-1

            ! Dans les deux test suivants je verifie si le voisin est physique ou due a la periodicite. Si tel est le cas le type de voisin est multiplie par -1
            if (ndim.eq.3) then
               if  ((m_period(1).and.(m_coords(1)   .eq.    0    ).and.(i-1.eq.1)).or. &
                  (m_period(1).and.(m_coords(1)+1 .eq.m_dims(1)).and.(i-1.eq.2)).or. &
                  (m_period(2).and.(m_coords(2)   .eq.    0  ).and.(i-1.eq.3)).or. &  
                  (m_period(2).and.(m_coords(2)+1 .eq.m_dims(2)).and.(i-1.eq.4)).or. &  
                  (m_period(3).and.(m_coords(3)   .eq.    0  ).and.(i-1.eq.5)).or. &  
                  (m_period(3).and.(m_coords(3)+1 .eq.m_dims(3)).and.(i-1.eq.6))) then
                  rank_voisins(nb_voisins,2) = -1* rank_voisins(nb_voisins,2)
               end if
            else
               if  ((m_period(1).and.(m_coords(1)   .eq.    0  ).and.(i-1.eq.1)).or. &
                  (m_period(1).and.(m_coords(1)+1 .eq.m_dims(1)).and.(i-1.eq.2)).or. &
                  (m_period(2).and.(m_coords(2)   .eq.    0  ).and.(i-1.eq.3)).or. &
                  (m_period(2).and.(m_coords(2)+1 .eq.m_dims(2)).and.(i-1.eq.4))) then 
                  rank_voisins(nb_voisins,2) = -1* rank_voisins(nb_voisins,2)
               end if
            end if
         end if
      end do
      
      ! Dans la boucle qui suit je verifie si dans la direction ND je suis le seul proc. Dans ce cas la je suis mon propre voisin par copie.
      do nd=1,ndim
         if ((m_dims(nd) .eq. 1).and.m_period(nd).and.nproc.gt.1) then ! mottaaf : quand on a plus d'un proc il ne s'autoreconait en tant que voisin
            nb_voisins = nb_voisins + 1
            rank_voisins(nb_voisins,1) = rank
            rank_voisins(nb_voisins,2) = -(2*nd-1)

            nb_voisins = nb_voisins + 1
            rank_voisins(nb_voisins,1) = rank
            rank_voisins(nb_voisins,2) = -2*nd
         end if
      end do
      ! Afin de reperer les autres voisins je commence par stocker dans NB_VOISINS le nombre de voisins de chaque proceseur pour cela
      !-------------------------------------------------------------------------------
      !autres voisins
      !-------------------------------------------------------------------------------
      do np=0,nproc-1
         nb_voisins_np(np)=nb_voisins
         call mpi_bcast(nb_voisins_np(np),1,mpi_integer,np,comm3d,mpi_code)
         do i=1,nb_voisins_np(np)
            do j=1,2
               rank_voisins_np(np,i,j) = rank_voisins(i,j)
               call mpi_bcast(rank_voisins_np(np,i,j),1,mpi_integer,np,comm3d,mpi_code)
            end do
         end do
      end do
      
      nbv = 0
      do while (nbv.lt.nb_voisins )
         nbv = nbv + 1           ! si je fais"do nbv=1,nb_voisins" il n'actualise pas nb_voisins
         np = rank_voisins(nbv,1)
         if (abs(rank_voisins(nbv,2)) .eq.1) then
            do i=1,nb_voisins_np(np)
               if (abs(rank_voisins_np(np,i,2)).eq.3) then
                  nb_voisins = nb_voisins + 1
                  rank_voisins(nb_voisins,1) = rank_voisins_np(np,i,1)
                  rank_voisins(nb_voisins,2) =  min(rank_voisins_np(np,i,2),rank_voisins(nbv,2)) /&
                     abs(min( rank_voisins_np(np,i,2),rank_voisins(nbv,2)))*   7
               else if (abs(rank_voisins_np(np,i,2)).eq.4) then
                  nb_voisins = nb_voisins + 1
                  rank_voisins(nb_voisins,1) = rank_voisins_np(np,i,1)
                  rank_voisins(nb_voisins,2 ) =  min(rank_voisins_np(np,i,2),rank_voisins(nbv,2)) /&
                     abs(min( rank_voisins_np(np,i,2),rank_voisins(nbv,2))) *8
               else if (abs(rank_voisins_np(np,i,2)).eq.5) then
                  nb_voisins = nb_voisins + 1
                  rank_voisins(nb_voisins,1) = rank_voisins_np(np,i,1)
                  rank_voisins(nb_voisins,2 ) =  min(rank_voisins_np(np,i,2),rank_voisins(nbv,2)) /&
                     abs(min( rank_voisins_np(np,i,2),rank_voisins(nbv,2))) * 9
               else if (abs(rank_voisins_np(np,i,2)).eq.6) then
                  nb_voisins = nb_voisins + 1
                  rank_voisins(nb_voisins,1) = rank_voisins_np(np,i,1)
                  rank_voisins(nb_voisins,2 ) =  min(rank_voisins_np(np,i,2),rank_voisins(nbv,2)) /&
                     abs(min( rank_voisins_np(np,i,2),rank_voisins(nbv,2))) * 10
               end if
            end do
         else  if (abs(rank_voisins(nbv,2)) .eq. 2) then
            do i=1,nb_voisins_np(np)
               if (abs(rank_voisins_np(np,i,2)).eq.3) then
                  nb_voisins = nb_voisins + 1
                  rank_voisins(nb_voisins,1) = rank_voisins_np(np,i,1)
                  rank_voisins(nb_voisins,2 ) =  min(rank_voisins_np(np,i,2),rank_voisins(nbv,2)) /&
                     abs(min( rank_voisins_np(np,i,2),rank_voisins(nbv,2))) * 11
               else if (abs(rank_voisins_np(np,i,2)).eq.4) then
                  nb_voisins = nb_voisins + 1
                  rank_voisins(nb_voisins,1) = rank_voisins_np(np,i,1)
                  rank_voisins(nb_voisins,2 ) =  min(rank_voisins_np(np,i,2),rank_voisins(nbv,2)) /&
                     abs(min( rank_voisins_np(np,i,2),rank_voisins(nbv,2))) * 12
               else if (abs(rank_voisins_np(np,i,2)).eq.5) then
                  nb_voisins = nb_voisins + 1
                  rank_voisins(nb_voisins,1) = rank_voisins_np(np,i,1)
                  rank_voisins(nb_voisins,2 ) =  min(rank_voisins_np(np,i,2),rank_voisins(nbv,2)) /&
                     abs(min( rank_voisins_np(np,i,2),rank_voisins(nbv,2))) * 13
               else if (abs(rank_voisins_np(np,i,2)).eq.6) then
                  nb_voisins = nb_voisins + 1
                  rank_voisins(nb_voisins,1) = rank_voisins_np(np,i,1)
                  rank_voisins(nb_voisins,2 ) =  min(rank_voisins_np(np,i,2),rank_voisins(nbv,2)) /&
                     abs(min( rank_voisins_np(np,i,2),rank_voisins(nbv,2))) * 14
               end if
            end do
         else  if (abs(rank_voisins(nbv,2)) .eq. 3) then
            do i=1,nb_voisins_np(np)
               if (abs(rank_voisins_np(np,i,2)).eq.5) then
                  nb_voisins = nb_voisins + 1
                  rank_voisins(nb_voisins,1) = rank_voisins_np(np,i,1)
                  rank_voisins(nb_voisins,2 ) =  min(rank_voisins_np(np,i,2),rank_voisins(nbv,2)) /&
                     abs(min( rank_voisins_np(np,i,2),rank_voisins(nbv,2))) * 15
               else if (abs(rank_voisins_np(np,i,2)).eq.6) then
                  nb_voisins = nb_voisins + 1
                  rank_voisins(nb_voisins,1) = rank_voisins_np(np,i,1)
                  rank_voisins(nb_voisins,2 ) =  min(rank_voisins_np(np,i,2),rank_voisins(nbv,2)) /&
                     abs(min( rank_voisins_np(np,i,2),rank_voisins(nbv,2))) * 16
               end if
            end do
         else  if (abs(rank_voisins(nbv,2)) .eq. 4) then
            do i=1,nb_voisins_np(np)
               if (abs(rank_voisins_np(np,i,2)).eq.5) then
                  nb_voisins = nb_voisins + 1
                  rank_voisins(nb_voisins,1) = rank_voisins_np(np,i,1)
                  rank_voisins(nb_voisins,2 ) =  min(rank_voisins_np(np,i,2),rank_voisins(nbv,2)) /&
                     abs(min( rank_voisins_np(np,i,2),rank_voisins(nbv,2))) * 17
               else if (abs(rank_voisins_np(np,i,2)).eq.6) then
                  nb_voisins = nb_voisins + 1
                  rank_voisins(nb_voisins,1) = rank_voisins_np(np,i,1)
                  rank_voisins(nb_voisins,2 ) =  min(rank_voisins_np(np,i,2),rank_voisins(nbv,2)) /&
                     abs(min( rank_voisins_np(np,i,2),rank_voisins(nbv,2))) * 18
               end if
            end do
         else  if (abs(rank_voisins(nbv,2)) .eq. 7) then
            do i=1,nb_voisins_np(np)
               if (abs(rank_voisins_np(np,i,2)).eq.5) then
                  nb_voisins = nb_voisins + 1
                  rank_voisins(nb_voisins,1) = rank_voisins_np(np,i,1)
                  rank_voisins(nb_voisins,2 ) =  min(rank_voisins_np(np,i,2),rank_voisins(nbv,2)) /&
                     abs(min( rank_voisins_np(np,i,2),rank_voisins(nbv,2))) * 19
               else if (abs(rank_voisins_np(np,i,2)).eq.6) then
                  nb_voisins = nb_voisins + 1
                  rank_voisins(nb_voisins,1) = rank_voisins_np(np,i,1)
                  rank_voisins(nb_voisins,2 ) =  min(rank_voisins_np(np,i,2),rank_voisins(nbv,2)) /&
                     abs(min( rank_voisins_np(np,i,2),rank_voisins(nbv,2))) * 20
               end if
            end do
         else  if (abs(rank_voisins(nbv,2)) .eq. 8) then
            do i=1,nb_voisins_np(np)
               if (abs(rank_voisins_np(np,i,2)).eq.5) then
                  nb_voisins = nb_voisins + 1
                  rank_voisins(nb_voisins,1) = rank_voisins_np(np,i,1)
                  rank_voisins(nb_voisins,2 ) =  min(rank_voisins_np(np,i,2),rank_voisins(nbv,2)) /&
                     abs(min( rank_voisins_np(np,i,2),rank_voisins(nbv,2))) * 21
               else if (abs(rank_voisins_np(np,i,2)).eq.6) then
                  nb_voisins = nb_voisins + 1
                  rank_voisins(nb_voisins,1) = rank_voisins_np(np,i,1)
                  rank_voisins(nb_voisins,2 ) =  min(rank_voisins_np(np,i,2),rank_voisins(nbv,2)) /&
                     abs(min( rank_voisins_np(np,i,2),rank_voisins(nbv,2))) * 22
               end if
            end do
         else  if (abs(rank_voisins(nbv,2)) .eq. 11) then
            do i=1,nb_voisins_np(np)
               if (abs(rank_voisins_np(np,i,2)).eq.5) then
                  nb_voisins = nb_voisins + 1
                  rank_voisins(nb_voisins,1) = rank_voisins_np(np,i,1)
                  rank_voisins(nb_voisins,2 ) =  min(rank_voisins_np(np,i,2),rank_voisins(nbv,2)) /&
                     abs(min( rank_voisins_np(np,i,2),rank_voisins(nbv,2))) * 23
               else if (abs(rank_voisins_np(np,i,2)).eq.6) then
                  nb_voisins = nb_voisins + 1
                  rank_voisins(nb_voisins,1) = rank_voisins_np(np,i,1)
                  rank_voisins(nb_voisins,2 ) =  min(rank_voisins_np(np,i,2),rank_voisins(nbv,2)) /&
                     abs(min( rank_voisins_np(np,i,2),rank_voisins(nbv,2))) * 24
               end if
            end do
         else  if (abs(rank_voisins(nbv,2)) .eq. 12) then
            do i=1,nb_voisins_np(np)
               if (abs(rank_voisins_np(np,i,2)).eq.5) then
                  nb_voisins = nb_voisins + 1
                  rank_voisins(nb_voisins,1) = rank_voisins_np(np,i,1)
                  rank_voisins(nb_voisins,2 ) =  min(rank_voisins_np(np,i,2),rank_voisins(nbv,2)) /&
                     abs(min( rank_voisins_np(np,i,2),rank_voisins(nbv,2))) * 25
               else if (abs(rank_voisins_np(np,i,2)).eq.6) then
                  nb_voisins = nb_voisins + 1
                  rank_voisins(nb_voisins,1) = rank_voisins_np(np,i,1)
                  rank_voisins(nb_voisins,2 ) =  min(rank_voisins_np(np,i,2),rank_voisins(nbv,2)) /&
                     abs(min( rank_voisins_np(np,i,2),rank_voisins(nbv,2))) * 26
               end if
            end do
         end if
      end do

      !-------------------------------------------------------------------------------
      !recuperer le nbt
      !-------------------------------------------------------------------------------
      do i=1,nb_voisins
         tdv =  rank_voisins(i,2)
         if (tdv.lt.0) then
            if ((tdv.eq.- 1).or.(tdv.eq.- 7).or.(tdv.eq.- 8)&
            .or.(tdv.eq.- 9).or.(tdv.eq.-10).or.(tdv.eq.-19)&
            .or.(tdv.eq.-20).or.(tdv.eq.-21).or.(tdv.eq.-22)) then
               depl_local(i,1) = -1    
            else if ((tdv.eq.- 2).or.(tdv.eq.-11).or.(tdv.eq.-12)&
               .or.(tdv.eq.-13).or.(tdv.eq.-14).or.(tdv.eq.-23)&
               .or.(tdv.eq.-24).or.(tdv.eq.-25).or.(tdv.eq.-26)) then
               depl_local(i,1) = 1
            else if ((tdv.eq.- 3).or.(tdv.eq.- 7).or.(tdv.eq.-11)&
               .or.(tdv.eq.-15).or.(tdv.eq.-16).or.(tdv.eq.-19)&
               .or.(tdv.eq.-20).or.(tdv.eq.-23).or.(tdv.eq.-24)) then
               depl_local(i,2) = - 1
            else if ((tdv.eq.- 4).or.(tdv.eq.- 8).or.(tdv.eq.-12)&
               .or.(tdv.eq.-17).or.(tdv.eq.-18).or.(tdv.eq.-21)&
               .or.(tdv.eq.-22).or.(tdv.eq.-25).or.(tdv.eq.-26)) then
               depl_local(i,2) = 1
            else if ((tdv.eq.- 5).or.(tdv.eq.- 9).or.(tdv.eq.-13)&
               .or.(tdv.eq.-15).or.(tdv.eq.-17).or.(tdv.eq.-19)&
               .or.(tdv.eq.-21).or.(tdv.eq.-23).or.(tdv.eq.-25)) then
               depl_local(i,3) = -1
            else if ((tdv.eq.- 6).or.(tdv.eq.-10).or.(tdv.eq.-14)&
               .or.(tdv.eq.-16).or.(tdv.eq.-18).or.(tdv.eq.-20)&
               .or.(tdv.eq.-22).or.(tdv.eq.-24).or.(tdv.eq.-26)) then
               depl_local(i,3) = 1
            end if
         end if
      end do
      
      do np=0,nproc-1
         rank_nbtimes_local = rank_nbtimes
         call mpi_bcast(rank_nbtimes_local,1,mpi_integer,np,comm3d,mpi_code)
         do i=1,rank_nbtimes_local
            do j=1,3
               rank_symper_local(i,j) = rank_symper(i,j)
               call mpi_bcast(rank_symper_local(i,j),1,mpi_double_precision,np,comm3d,mpi_code)
            end do
         end do
         do i=1,rank_nbtimes_local  
            do j=1,3
               if (rank_symper_local(i,j).gt.0.0d0) depl_vois( i,j) = 1
               if (rank_symper_local(i,j).lt.0.0d0) depl_vois( i,j) = - 1
               if (dabs(rank_symper_local(i,j)).lt.1e-15) depl_vois( i,j) = 0
            end do
         end do
      
         do i=1,nb_voisins
            if ( rank_voisins(i,1).eq.np) then
               do j=1,rank_nbtimes_local
                  if ((depl_vois(j,1).eq.depl_local(i,1)).and. &
                     ( depl_vois(j,2).eq.depl_local(i,2)).and. &
                     ((depl_vois(j,3).eq.depl_local(i,3)).or.(ndim.lt.3))) then
                     rank_voisins(i,3) = j
                  end if
               end do
            end if
         end do
      end do

      !-------------------------------------------------------------------------------
      if (deeptracking) write(*,*) 'sortie SubDomain_Neighbouring'
      !-------------------------------------------------------------------------------
   end subroutine SubDomain_Neighbouring

   !===============================================================================
end module Bib_VOFLag_SubDomain_Neighbouring
!===============================================================================
