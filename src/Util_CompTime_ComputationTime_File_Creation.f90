!===============================================================================
module Bib_VOFLag_ComputationTime_File_Creation
    !===============================================================================
    contains
    !---------------------------------------------------------------------------  
    !> @author jorge cesar brandle de motta, amine chadil
    ! 
    !> @brief cette routine ecrit la premiere ligne du fichier CTIME
    !
    !> @param [in] ctime_file : le nom du fichier 
    !---------------------------------------------------------------------------  
    subroutine ComputationTime_File_Creation(ctime_file,vl) 
        !===============================================================================
        !modules
        !===============================================================================
        !-------------------------------------------------------------------------------
        !Modules de definition des structures et variables => src/mod/module_...f90
        !-------------------------------------------------------------------------------
        use Module_VOFLag_ParticlesDataStructure
        use mod_Parameters,                       only : deeptracking,EN_activate
        !-------------------------------------------------------------------------------
        !===============================================================================
        !declarations des variables
        !===============================================================================
        implicit none
        !-------------------------------------------------------------------------------
        !variables globales
        !-------------------------------------------------------------------------------
        type(struct_vof_lag), intent(inout) :: vl
        character(len=*)                    :: ctime_file
        !-------------------------------------------------------------------------------
        !variables locales 
        !-------------------------------------------------------------------------------
        !-------------------------------------------------------------------------------

        !-------------------------------------------------------------------------------
        if (deeptracking) write(*,*) 'entree ComputationTime_File_Creation'
        !-------------------------------------------------------------------------------

        !-------------------------------------------------------------------------------
        ! ouverture du fichier ctime_file
        !-------------------------------------------------------------------------------
        open(unit=45,file=ctime_file,status='replace')

        !-------------------------------------------------------------------------------
        ! choix des routines pour lequelles on veut calculer le temps d'execution
        !-------------------------------------------------------------------------------
        write(45,'(A1,7A13)',advance='no') '#',' ----NT----- ', & 
                                ' --thetis--- ',' --THETIS--- ', & ! 1
                                ' --imprime-- ',' --IMPRIME-- ', & ! 2
                                ' --naviers-- ',' --NAVIERS-- '    ! 3

        if (EN_activate) then
          write(45,'(2A13)',advance='no')  ' --energie-- ',' --ENERGIE-- '    ! 4
        endif
        if (vl%deplacement .or. vl%vrot) then
          write(45,'(2A13)',advance='no')  ' --actu_vl-- ',' --ACTU_VL-- '    ! 5
        endif
        if (vl%postforce) then
          write(45,'(2A13)',advance='no')  ' --efforvl-- ',' --EFFORVL-- '    ! 6
        endif
        if (vl%postHeatFlux) then
          write(45,'(2A13)',advance='no')  ' --heatfvl-- ',' --HEATFVL-- '    ! 7
        endif
        close(45)
        
        !-------------------------------------------------------------------------------
        if (deeptracking) write(*,*) 'sortie ComputationTime_File_Creation'
        !-------------------------------------------------------------------------------
    end subroutine ComputationTime_File_Creation

    !===============================================================================
end module Bib_VOFLag_ComputationTime_File_Creation
!===============================================================================
    


