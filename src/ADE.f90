!========================================================================
!
!                   FUGU Version 3.0.0
!
!========================================================================
!**
!**   NAME       : ADE.f90
!**
!**   AUTHOR     : Benoit Trouette
!**
!**   FUNCTION   : ADE Equation modules of FUGU software
!**
!**   DATES      : Version 1.0.0  : from : feb, 20, 2018
!**
!========================================================================

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#include "precomp.h"
module mod_ADE
  use mod_struct_solver
  implicit none
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  INTEGER, PARAMETER :: RESOL_START = 0
  INTEGER, PARAMETER :: RESOL_END   = 1


  ! Concentration variable 
  real(8), dimension(:,:,:), allocatable   :: conc,conc0,conc1
  real(8), dimension(:,:,:), allocatable   :: conc_in
  real(8), dimension(:,:,:,:), allocatable :: conc_pen

  real(8), dimension(:,:,:), allocatable   :: ade_diffu,ade_diffv,ade_diffw
  real(8), dimension(:,:,:), allocatable   :: ade_src,ade_lnr


  type boundary_value_t
     integer :: type   = 0
     real(8) :: phi_in = 0
  end type boundary_value_t

  type faces_t
     type(boundary_value_t) :: left
     type(boundary_value_t) :: right
     type(boundary_value_t) :: bottom
     type(boundary_value_t) :: top
     type(boundary_value_t) :: backward
     type(boundary_value_t) :: forward
  end type faces_t
  
  type(faces_t) :: bc_ade

  namelist /nml_bc_ade/ bc_ade

contains

  subroutine ADE_solve(iter,mesh,             &
       & u,v,w,phi,phi0,phi1,                 &
       & rho,diffu,diffv,diffw,cou,           &
       & phi_pen,phi_in,phi_src,phi_lnr,      &
       & coef_time_1,coef_time_2,coef_time_3, &
       & ade_eq,ade_resol)
    use mod_struct_grid
    use mod_struct_thermophysics
    use mod_timers
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)                                   :: iter
    real(8), intent(in)                                   :: coef_time_1,coef_time_2,coef_time_3
    real(8), dimension(:,:,:), allocatable, intent(inout) :: phi,phi0,phi1
    real(8), dimension(:,:,:), allocatable, intent(inout) :: u,v,w
    real(8), dimension(:,:,:), allocatable, intent(inout) :: diffu,diffv,diffw
    real(8), dimension(:,:,:), allocatable, intent(in)    :: cou,rho
    real(8), dimension(:,:,:), allocatable, intent(in)    :: phi_src,phi_in,phi_lnr
    real(8), dimension(:,:,:,:), allocatable, intent(in)  :: phi_pen
    type(grid_t), intent(in)                              :: mesh
    type(equation_t), intent(inout)                       :: ade_eq
    type(resol_ade_t), intent(inout)                      :: ade_resol
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------

    if (.not.ade_eq%activate) return
    !if (.not.ade_eq%solve)    return

    call compute_time(TIMER_START,"[sub] ADE_solve")

    call ADE_print(RESOL_START)

    call ADE_Init_BC(mesh,conc_in,conc_pen,ade_eq)

    call ADE_Init_EQ(iter,mesh,conc,conc0,conc1,ade_eq)

    call ADE_thermophy(mesh,cou,diffu,diffv,diffw)

    call ADE_Delta(mesh,                          &
         & u,v,w,conc,conc0,conc1,                &
         & rho,diffu,diffv,diffw,cou,             &
         & conc_pen,conc_in,ade_src,ade_lnr,      &
         & coef_time_1,coef_time_2,coef_time_3,   &
         & ade_eq,ade_resol%solver)

    call ADE_print(RESOL_END)

    call ADE_outputs

    call compute_time(TIMER_END,"[sub] ADE_solve")

  end subroutine ADE_solve
  

  subroutine ADE_Delta(mesh,u,v,w,phi,phi0,phi1, &
       & rho,diffu,diffv,diffw,cou,              &
       & phi_pen,phi_in,phi_src,phi_lnr,         &
       & coef_time_1,coef_time_2,coef_time_3,    &
       & ade_eq,solver)
    !*****************************************************************************************************
    use mod_borders
    use mod_connectivity
    use mod_mpi
    use mod_operators
    use mod_parameters, only: Euler,switch2D3D
    use mod_solver_new
    use mod_struct_solver
    use mod_timers
    !-------------------------------------------------------------------------------
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), intent(in)                                   :: coef_time_1,coef_time_2,coef_time_3
    real(8), dimension(:,:,:), allocatable, intent(inout) :: phi,phi0,phi1
    real(8), dimension(:,:,:), allocatable, intent(in)    :: u,v,w
    real(8), dimension(:,:,:), allocatable, intent(in)    :: diffu,diffv,diffw
    real(8), dimension(:,:,:), allocatable, intent(in)    :: cou,rho
    real(8), dimension(:,:,:), allocatable, intent(in)    :: phi_src,phi_in,phi_lnr
    real(8), dimension(:,:,:,:), allocatable, intent(in)  :: phi_pen
    type(grid_t), intent(in)                              :: mesh
    type(equation_t), intent(in)                          :: ade_eq
    type(solver_t), intent(inout)                         :: solver
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                               :: i,j,k,l
    integer                                               :: ii,jj,kk
    integer                                               :: nic
    integer, dimension(:), allocatable                    :: kic
    real(8), dimension(:), allocatable                    :: rhs,sol
    type(system_t)                                        :: system
    !-------------------------------------------------------------------------------

    if (.not.ade_eq%activate) return
    if (.not.ade_eq%solve)    return

    call compute_time(TIMER_START,"[sub] ADE_delta")

    !-------------------------------------------------------------------------------
    ! Periodicity for explicit variables 
    !-------------------------------------------------------------------------------
    call sca_periodicity(mesh,phi)
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Connectitivities
    !-------------------------------------------------------------------------------
    select case(ade_eq%inertial)
    case(4,5)
       call Energy_Connectivity_Large(system%mat,mesh)
    case DEFAULT
       call Energy_Connectivity(system%mat,mesh)
    end select
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! alloc
    !-------------------------------------------------------------------------------
    allocate(sol(system%mat%npt))
    allocate(rhs(system%mat%npt))
    !-------------------------------------------------------------------------------
    
    !-------------------------------------------------------------------------------
    ! Solved nodes
    !-------------------------------------------------------------------------------
    call energy_kic(system%mat,mesh)
    !-------------------------------------------------------------------------------
    
    !*******************************************************************************
    ! Solving of A . U = F
    ! coef = A
    ! sol  = U
    ! rhs  = F
    !*******************************************************************************
    
    
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------
    ! 2D/3D ADE equation Solving
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Second member of the problem
    !-------------------------------------------------------------------------------
    rhs = 0
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! External source terms
    !-------------------------------------------------------------------------------
    if (ade_eq%source_term) then
       do kk = mesh%szs,mesh%ezs 
          do jj = mesh%sys,mesh%eys
             do ii = mesh%sxs,mesh%exs
                i      = ii-mesh%sxs
                j      = jj-mesh%sys
                k      = kk-mesh%szs
                l      = i+j*(mesh%exs-mesh%sxs+1)+switch2D3D*k*(mesh%exs-mesh%sxs+1)*(mesh%eys-mesh%sys+1)+1
                rhs(l) = rhs(l) + phi_src(ii,jj,kk) * mesh%dx(ii) * mesh%dy(jj) * mesh%dz(kk) 
             enddo
          enddo
       end do
    endif
    !-------------------------------------------------------------------------------
    
    
    !-------------------------------------------------------------------------------
    ! Time integration for temperature time derivative
    !-------------------------------------------------------------------------------
    do kk = mesh%szs,mesh%ezs 
       do jj = mesh%sys,mesh%eys
          do ii = mesh%sxs,mesh%exs
             i      = ii-mesh%sxs
             j      = jj-mesh%sys
             k      = kk-mesh%szs
             l      = i+j*(mesh%exs-mesh%sxs+1)+switch2D3D*k*(mesh%exs-mesh%sxs+1)*(mesh%eys-mesh%sys+1)+1
             rhs(l) = rhs(l) - rho(ii,jj,kk)*coef_time_2*phi0(ii,jj,kk) * mesh%dx(ii) * mesh%dy(jj) * mesh%dz(kk) 
          enddo
       enddo
    end do
    !-------------------------------------------------------------------------------
    if (abs(Euler)==2) then
       do kk = mesh%szs,mesh%ezs 
          do jj = mesh%sys,mesh%eys
             do ii = mesh%sxs,mesh%exs
                i      = ii-mesh%sxs
                j      = jj-mesh%sys
                k      = kk-mesh%szs
                l      = i+j*(mesh%exs-mesh%sxs+1)+switch2D3D*k*(mesh%exs-mesh%sxs+1)*(mesh%eys-mesh%sys+1)+1
                rhs(l) = rhs(l) - rho(ii,jj,kk)*coef_time_3*phi1(ii,jj,kk) * mesh%dx(ii) * mesh%dy(jj) * mesh%dz(kk) 
             enddo
          enddo
       end do
    endif
    !-------------------------------------------------------------------------------
    
    !-------------------------------------------------------------------------------
    ! Boundary conditions
    !-------------------------------------------------------------------------------
    do kk = mesh%szs,mesh%ezs 
       do jj = mesh%sys,mesh%eys
          do ii = mesh%sxs,mesh%exs
             i      = ii-mesh%sxs
             j      = jj-mesh%sys
             k      = kk-mesh%szs
             l      = i+j*(mesh%exs-mesh%sxs+1)+switch2D3D*k*(mesh%exs-mesh%sxs+1)*(mesh%eys-mesh%sys+1)+1
             rhs(l) = rhs(l) + phi_pen(ii,jj,kk,1)*phi_in(ii,jj,kk) * mesh%dx(ii) * mesh%dy(jj) * mesh%dz(kk) 
          enddo
       enddo
    end do
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! init solution
    !-------------------------------------------------------------------------------
    do kk = mesh%szs,mesh%ezs 
       do jj = mesh%sys,mesh%eys
          do ii = mesh%sxs,mesh%exs
             i      = ii-mesh%sxs
             j      = jj-mesh%sys
             k      = kk-mesh%szs
             l      = i+j*(mesh%exs-mesh%sxs+1)+switch2D3D*k*(mesh%exs-mesh%sxs+1)*(mesh%eys-mesh%sys+1)+1
             sol(l) = phi0(ii,jj,kk)
          enddo
       enddo
    end do
    !-------------------------------------------------------------------------------
    
    !-------------------------------------------------------------------------------
    ! making the matrix
    !-------------------------------------------------------------------------------
    call ADE_Matrix(mesh,system%mat,rho,diffu,diffv,diffw,phi_pen,phi_lnr,coef_time_1,ade_eq)
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Solving the linear system
    !-------------------------------------------------------------------------------
    
    solver%eqs     = 0
    system%mat%eqs = solver%eqs
    
    call solve_linear_system(system,rhs,sol,solver,mesh)

    deallocate(system%mat%coef)
    deallocate(system%mat%icof)
    deallocate(system%mat%jcof)
    deallocate(system%mat%kic)
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! The new phi solution is updated
    !-------------------------------------------------------------------------------
    do kk = mesh%szs,mesh%ezs 
       do jj = mesh%sys,mesh%eys
          do ii = mesh%sxs,mesh%exs
             i             = ii-mesh%sxs
             j             = jj-mesh%sys
             k             = kk-mesh%szs
             l             = i+j*(mesh%exs-mesh%sxs+1)+switch2D3D*k*(mesh%exs-mesh%sxs+1)*(mesh%eys-mesh%sys+1)+1
             phi(ii,jj,kk) = sol(l)
          enddo
       enddo
    end do
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Time advencement
    !-------------------------------------------------------------------------------
    if (Euler==2) phi1 = phi0
    phi0 = phi
    !-------------------------------------------------------------------------------

    call compute_time(TIMER_END,"[sub] ADE_delta")
    
  end subroutine ADE_Delta

  
  subroutine ADE_Matrix (mesh,mat,rho,diffu,diffv,diffw,phi_pen,phi_lnr,coef_time_1,ade_eq)
    !-------------------------------------------------------------------------------
    ! Finite volume discretization of the ADE equation
    !-------------------------------------------------------------------------------
    use mod_parameters, only: nproc
    use mod_struct_grid
    use mod_struct_solver
    implicit none
    !-------------------------------------------------------------------------------
    ! global variables
    !-------------------------------------------------------------------------------
    real(8), intent(in)                      :: coef_time_1
    real(8), dimension(:,:,:), allocatable   :: rho,diffu,diffv,diffw,phi_lnr
    real(8), dimension(:,:,:,:), allocatable :: phi_pen
    type(grid_t), intent(in)                 :: mesh
    type(matrix_t), intent(inout)            :: mat
    type(equation_t), intent(in)             :: ade_eq
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                  :: lvs
    integer                                  :: i,j,k
    real(8)                                  :: contrib_b,contrib_f
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Matrix initialization
    !-------------------------------------------------------------------------------
    mat%coef = 0
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Finite volume discretization
    !-------------------------------------------------------------------------------
    lvs=1
    do k = mesh%szs,mesh%ezs 
       do j = mesh%sys,mesh%eys
          do i = mesh%sxs,mesh%exs

             !-------------------------------------------------------------------------------
             ! Penalty term for MPI exchange cells
             !-------------------------------------------------------------------------------
             if (nproc>1) then
                if (i<mesh%sx.or.i>mesh%ex.or.j<mesh%sy.or.j>mesh%ey.or.k<mesh%sz.or.k>mesh%ez) then
                   !-------------------------------------------------------------------------------
                   mat%coef(lvs) = 1
                   !-------------------------------------------------------------------------------
                   ! next line of the matrix
                   !-------------------------------------------------------------------------------
                   lvs = lvs + mat%kdv
                   cycle
                   !-------------------------------------------------------------------------------
                end if
             end if
             !-------------------------------------------------------------------------------

             !-------------------------------------------------------------------------------
             ! Time integration scheme
             !-------------------------------------------------------------------------------
             mat%coef(lvs) = rho(i,j,k) * coef_time_1 * mesh%dx(i) * mesh%dy(j) * mesh%dz(k)
             !---------------------------------------------------------------------------
             
             !---------------------------------------------------------------------------
             ! Conductivity
             !-------------------------------------------------------------------------------
             contrib_b       =  - diffu(i  ,j,k)/mesh%dxu(i  ) * mesh%dy(j) * mesh%dz(k)
             contrib_f       =  + diffu(i+1,j,k)/mesh%dxu(i+1) * mesh%dy(j) * mesh%dz(k)
             mat%coef(lvs  ) =  mat%coef(lvs)   - (contrib_b  - contrib_f)
             mat%coef(lvs+1) =  mat%coef(lvs+1) + contrib_b
             mat%coef(lvs+2) =  mat%coef(lvs+2) - contrib_f
             contrib_b       =  - diffv(i,j  ,k)/mesh%dyv(j  ) * mesh%dx(i) * mesh%dz(k)
             contrib_f       =  + diffv(i,j+1,k)/mesh%dyv(j+1) * mesh%dx(i) * mesh%dz(k)
             mat%coef(lvs  ) =  mat%coef(lvs)   - (contrib_b  - contrib_f)
             mat%coef(lvs+3) =  mat%coef(lvs+3) + contrib_b
             mat%coef(lvs+4) =  mat%coef(lvs+4) - contrib_f
             if (mesh%dim==3) then
                contrib_b       =  - diffw(i,j,k  )/mesh%dzw(k  ) * mesh%dx(i) * mesh%dy(j)
                contrib_f       =  + diffw(i,j,k+1)/mesh%dzw(k+1) * mesh%dx(i) * mesh%dy(j)
                mat%coef(lvs  ) =  mat%coef(lvs)   - (contrib_b  - contrib_f)
                mat%coef(lvs+5) =  mat%coef(lvs+5) + contrib_b
                mat%coef(lvs+6) =  mat%coef(lvs+6) - contrib_f
             end if
             !-------------------------------------------------------------------------------

             !-------------------------------------------------------------------------------
             ! Terme source 
             !-------------------------------------------------------------------------------
             if (ade_eq%source_term_implicit) then
             end if
             !-------------------------------------------------------------------------------

             !-------------------------------------------------------------------------------
             ! Inertial terms
             !-------------------------------------------------------------------------------
             select case(ade_eq%inertial)
             case default 
             end select
             !-------------------------------------------------------------------------------

             !-------------------------------------------------------------------------------
             ! Linear term
             !-------------------------------------------------------------------------------
             if (ade_eq%linear_term) then
                mat%coef(lvs) = mat%coef(lvs) + phi_lnr(i,j,k) * mesh%dx(i) * mesh%dy(j) * mesh%dz(k)
             end if
             !-------------------------------------------------------------------------------

             !-------------------------------------------------------------------------------
             ! Penalty term for boundary conditions
             !-------------------------------------------------------------------------------
             ! phi_pen (i,j,k,l)
             ! l = 1 central T component
             ! l = 2 left T component
             ! l = 3 right T component
             ! l = 4 bottom T component
             ! l = 5 top T component
             ! l = 6 backward T component
             ! l = 7 forward T component
             !-------------------------------------------------------------------------------
             mat%coef(lvs)   = mat%coef(lvs)   + phi_pen(i,j,k,1) * mesh%dx(i) * mesh%dy(j) * mesh%dz(k)
             mat%coef(lvs+1) = mat%coef(lvs+1) + phi_pen(i,j,k,2) * mesh%dx(i) * mesh%dy(j) * mesh%dz(k)
             mat%coef(lvs+2) = mat%coef(lvs+2) + phi_pen(i,j,k,3) * mesh%dx(i) * mesh%dy(j) * mesh%dz(k)
             mat%coef(lvs+3) = mat%coef(lvs+3) + phi_pen(i,j,k,4) * mesh%dx(i) * mesh%dy(j) * mesh%dz(k)
             mat%coef(lvs+4) = mat%coef(lvs+4) + phi_pen(i,j,k,5) * mesh%dx(i) * mesh%dy(j) * mesh%dz(k)
             if (mesh%dim==3) then
                mat%coef(lvs+5) = mat%coef(lvs+5) + phi_pen(i,j,k,6) * mesh%dx(i) * mesh%dy(j) * mesh%dz(k)
                mat%coef(lvs+6) = mat%coef(lvs+6) + phi_pen(i,j,k,7) * mesh%dx(i) * mesh%dy(j) * mesh%dz(k)
             end if
             !-------------------------------------------------------------------------------

             !-------------------------------------------------------------------------------
             ! End of discretization for phi
             !-------------------------------------------------------------------------------

             !-------------------------------------------------------------------------------
             ! next line of the matrix
             !-------------------------------------------------------------------------------
             lvs = lvs + mat%kdv
             !-------------------------------------------------------------------------------

          enddo
       enddo
    end do

  end subroutine ADE_Matrix

  subroutine ADE_Init_EQ(iter,mesh,phi,phi0,phi1,ade_eq) 
    use mod_allocate
    use mod_mpi
    use mod_parameters, only: Euler
    use mod_struct_grid
    use mod_struct_solver
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)                                   :: iter
    real(8), dimension(:,:,:), allocatable, intent(inout) :: phi,phi0,phi1
    type(grid_t), intent(in)                              :: mesh
    type(equation_t), intent(inout)                       :: ade_eq
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                               :: i,j,k
    !-------------------------------------------------------------------------------

    call allocate_array_3(ALLOCATE_ON_P_MESH,phi,mesh)
    call allocate_array_3(ALLOCATE_ON_P_MESH,phi0,mesh)
    if (Euler==2) then
       call allocate_array_3(ALLOCATE_ON_P_MESH,phi1,mesh)
    end if

    if (iter==1) then

       do j = mesh%sy-1,mesh%ey+1
          do i = mesh%sx-1,mesh%ex+1

             if (mesh%y(j)<1-mesh%x(i)) then
                phi(i,j,1) = 1
             else
                phi(i,j,1) = 0
             end if

          end do
       end do

       phi0 = phi
       if (allocated(phi1)) phi1 = phi

    end if

  end subroutine ADE_Init_EQ
  

  subroutine ADE_Init_BC(mesh,phi_in,phi_pen,ade_eq) 
    use mod_Constants, only: d1p2
    use mod_parameters, only: EN_pen_BC
    use mod_mpi
    use mod_struct_grid
    use mod_struct_solver
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable, intent(inout)   :: phi_in
    real(8), dimension(:,:,:,:), allocatable, intent(inout) :: phi_pen
    type(grid_t), intent(in)                                :: mesh
    type(equation_t), intent(inout)                         :: ade_eq
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                                 :: i,j,k
    real(8)                                                 :: half_pen,full_pen
    real(8)                                                 :: x,y,z
    !-------------------------------------------------------------------------------
    
    !-------------------------------------------------------------------------------
    if (.not.allocated(phi_in)) then
       allocate(phi_in(                        &
            & mesh%sx-mesh%gx:mesh%ex+mesh%gx, &
            & mesh%sy-mesh%gy:mesh%ey+mesh%gy, &
            & mesh%sz-mesh%gz:mesh%ez+mesh%gz ))
    end if
    if (.not.allocated(phi_pen)) then
       allocate(phi_pen(                       &
            & mesh%sx-mesh%gx:mesh%ex+mesh%gx, &
            & mesh%sy-mesh%gy:mesh%ey+mesh%gy, &
            & mesh%sz-mesh%gz:mesh%ez+mesh%gz, &
            & mesh%dim**2+1))
    end if
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! penalisation values
    !-------------------------------------------------------------------------------
    full_pen = EN_pen_BC
    half_pen = d1p2*EN_pen_BC
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! left, right, top, bottom, backward, forward = 
    !    0: Dirichlet
    !    1: Neumann
    !    2: not used
    !    3: not used
    !    4: Periodic
    !-------------------------------------------------------------------------------
    open(10,file="data.in",action="read")
    read(10,nml=nml_bc_ade)
    rewind(10)
    close(10)
    !-------------------------------------------------------------------------------
    ! check and force periodicity
    !-------------------------------------------------------------------------------
    if (Periodic(1)) then
       bc_ade%left%type  = 4
       bc_ade%right%type = 4
    end if
    if (Periodic(2)) then
       bc_ade%bottom%type = 4
       bc_ade%top%type    = 4
    end if
    if (Periodic(3)) then
       bc_ade%backward%type = 4
       bc_ade%forward%type  = 4
    end if
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ade_eq%bound%left     = bc_ade%left%type
    ade_eq%bound%right    = bc_ade%right%type
    ade_eq%bound%bottom   = bc_ade%bottom%type
    ade_eq%bound%top      = bc_ade%top%type
    ade_eq%bound%backward = bc_ade%backward%type
    ade_eq%bound%forward  = bc_ade%forward%type
    !-------------------------------------------------------------------------------
    
    
    !-------------------------------------------------------------------------------
    ! Order of imposition of velocity boundary conditions
    !
    ! Periodic first
    ! Neumann
    ! Dirichlet
    !-------------------------------------------------------------------------------
    phi_pen = 0
    phi_in  = 0

    !-------------------------------------------------------------------------------
    ! periodic --> nothing to do
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------


    !-------------------------------------------------------------------------------
    ! neumann --> nothing to do
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! symetry --> neumann --> nothing to do
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! dirichlet 0 or 3 
    !-------------------------------------------------------------------------------
    ! left 
    if (ade_eq%bound%left==0.or.ade_eq%bound%left==3) then
       do k = mesh%sz,mesh%ez
          do j = mesh%sy,mesh%ey
             do i = mesh%sx,mesh%ex
                if (i==mesh%gsx) then
                   phi_pen(i,j,k,1)=full_pen
                   phi_in(i,j,k)=bc_ade%left%phi_in
                end if
             end do
          end do
       end do
    end if
    ! right
    if (ade_eq%bound%right==0.or.ade_eq%bound%right==3) then
       do k = mesh%sz,mesh%ez
          do j = mesh%sy,mesh%ey
             do i = mesh%sx,mesh%ex
                if (i==mesh%gex) then
                   phi_pen(i,j,k,1)=full_pen
                   phi_in(i,j,k)=bc_ade%right%phi_in
                end if
             end do
          end do
       end do
    end if
    ! bottom
    if (ade_eq%bound%bottom==0.or.ade_eq%bound%bottom==3) then
       do k = mesh%sz,mesh%ez
          do j = mesh%sy,mesh%ey
             do i = mesh%sx,mesh%ex
                if (j==mesh%gsy) then
                   phi_pen(i,j,k,1)=full_pen
                   phi_in(i,j,k)=bc_ade%bottom%phi_in
                end if
             end do
          end do
       end do
    end if
    ! top
    if (ade_eq%bound%top==0.or.ade_eq%bound%top==3) then
       do k = mesh%sz,mesh%ez
          do j = mesh%sy,mesh%ey
             do i = mesh%sx,mesh%ex
                if (j==mesh%gey) then
                   phi_pen(i,j,k,1)=full_pen
                   phi_in(i,j,k)=bc_ade%top%phi_in
                end if
             end do
          end do
       end do
    end if
    if (dim==3) then
       ! backward
       if (ade_eq%bound%backward==0.or.ade_eq%bound%backward==3) then
          do k = mesh%sz,mesh%ez
             do j = mesh%sy,mesh%ey
                do i = mesh%sx,mesh%ex
                   if (k==mesh%gsz) then
                      phi_pen(i,j,k,1)=full_pen
                      phi_in(i,j,k)=bc_ade%backward%phi_in
                   end if
                end do
             end do
          end do
       end if
       ! forward
       if (ade_eq%bound%forward==0.or.ade_eq%bound%forward==3) then
          do k = mesh%sz,mesh%ez
             do j = mesh%sy,mesh%ey
                do i = mesh%sx,mesh%ex
                   if (k==mesh%gez) then
                      phi_pen(i,j,k,1)=full_pen
                      phi_in(i,j,k)=bc_ade%forward%phi_in
                   end if
                end do
             end do
          end do
       end if
    end if
    !-------------------------------------------------------------------------------
    ! end dirichlet
    !-------------------------------------------------------------------------------

  end subroutine ADE_Init_BC

  
  subroutine ADE_thermophy(mesh,cou,diffu,diffv,diffw)
    use mod_allocate
    use mod_constants, only: d1p2
    use mod_parameters, only: nb_phase
    use mod_struct_grid
    use mod_struct_thermophysics
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable, intent(inout) :: diffu,diffv,diffw
    real(8), dimension(:,:,:), allocatable, intent(in)    :: cou
    type(grid_t), intent(in)                              :: mesh
    !-------------------------------------------------------------------------------
    ! locale variables
    !-------------------------------------------------------------------------------
    integer                                               :: i,j,k
    real(8)                                               :: coul
    !-------------------------------------------------------------------------------


    call allocate_array_3(ALLOCATE_ON_U_MESH,diffu,mesh,fluids(1)%dif)
    call allocate_array_3(ALLOCATE_ON_V_MESH,diffv,mesh,fluids(1)%dif)
    call allocate_array_3(ALLOCATE_ON_W_MESH,diffw,mesh,fluids(1)%dif)

    if (nb_phase==2) then

       !-------------------------------------------------------------------------------
       ! arithmetic mean for face diff coefficient computation
       !-------------------------------------------------------------------------------
       if (mesh%dim==2) then
          do j = mesh%sy-1,mesh%ey+1
             do i = mesh%sx-1,mesh%ex+1
                coul         = d1p2*(cou(i-1,j,1)+cou(i,j,1))
                diffu(i,j,1) = coul*fluids(2)%dif+(1-coul)*fluids(1)%dif
                coul         = d1p2*(cou(i,j-1,1)+cou(i,j,1))
                diffv(i,j,1) = coul*fluids(2)%dif+(1-coul)*fluids(1)%dif
             enddo
          enddo
       else 
          do k = mesh%sz-1,mesh%ez+1
             do j = mesh%sy-1,mesh%ey+1
                do i = mesh%sx-1,mesh%ex+1
                   coul         = d1p2*(cou(i-1,j,k)+cou(i,j,k))
                   diffu(i,j,k) = coul*fluids(2)%dif+(1-coul)*fluids(1)%dif
                   coul         = d1p2*(cou(i,j-1,k)+cou(i,j,k))
                   diffv(i,j,k) = coul*fluids(2)%dif+(1-coul)*fluids(1)%dif
                   coul         = d1p2*(cou(i,j,k-1)+cou(i,j,k))
                   diffw(i,j,k) = coul*fluids(2)%dif+(1-coul)*fluids(1)%dif
                enddo
             enddo
          end do
       end if

    end if

  end subroutine ADE_thermophy

  subroutine ADE_print(icode)
    use mod_parameters, only: rank
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in) :: icode
    !-------------------------------------------------------------------------------

    if (rank>0) return

    select case(icode)
    case(RESOL_START)
       write(*,*)
       write(*,*) ''//achar(27)//'[1;35m=====================  ADE (START)  ==========================='//achar(27)//'[0;35m'
    case(RESOL_END)
       write(*,*) ''//achar(27)//'[1;35m=====================   ADE (END)  ============================'//achar(27)//'[0m'
       write(*,*)
    end select
    
  end subroutine ADE_print
  

  subroutine ADE_outputs
    implicit none

  end subroutine ADE_outputs

  
  subroutine ADE_time
    implicit none

  end subroutine ADE_time

end module mod_ADE
