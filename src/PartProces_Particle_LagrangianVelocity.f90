!===============================================================================
module Bib_VOFLag_Particle_LagrangianVelocity
  !===============================================================================
  contains
  !-----------------------------------------------------------------------------  
  !> @author amine chadil
  ! 
  !> @brief cette routine permet 
  !
  !> @param[in]  u,v,w           : vitesse au temps n+1.
  !> @param[in, out] vl          : la structure contenant toutes les informations
  !! relatives aux particules.
  !-----------------------------------------------------------------------------
  subroutine Particle_LagrangianVelocity(u,v,w,vl,ndim,grid_x,&
	      grid_y,grid_z,grid_xu,grid_yv,grid_zw,dx,dy,dz,dxu,dyv,dzw) 
    !===============================================================================
    !modules
    !===============================================================================
    !-------------------------------------------------------------------------------
    !Modules de definition des structures et variables => src/mod/module_...f90
    !-------------------------------------------------------------------------------
    use Module_VOFLag_ParticlesDataStructure
    use mod_Parameters,                      only : deeptracking,xmin,xmax,ymin,&
                                                    ymax,zmin,zmax
    !-------------------------------------------------------------------------------
    !Modules de subroutines de la librairie RESPECT => src/Bib_VOFLag_...f90
    !-------------------------------------------------------------------------------
    use Bib_VOFLag_Sphere_LagrangianVelocity
    use Bib_VOFLag_Ellipsoid_LagrangianVelocity
    !-------------------------------------------------------------------------------
    !===============================================================================
    !declarations des variables
    !===============================================================================
    implicit none
    !-------------------------------------------------------------------------------
    !variables globales
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable, intent(in)    :: u,v,w
    type(struct_vof_lag),                   intent(inout) :: vl
      integer,				      intent(in)  :: ndim
      real(8), dimension(:),     allocatable, intent(in)  :: grid_xu,grid_yv, grid_zw
      real(8), dimension(:),     allocatable, intent(in)  :: grid_x,grid_y, grid_z
      real(8), dimension(:),     allocatable, intent(in)  :: dx,dy,dz
      real(8), dimension(:),     allocatable, intent(in)  :: dxu,dyv,dzw
    
    !-------------------------------------------------------------------------------
    !variables locales 
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'entree Particle_LagrangianVelocity'
    !-------------------------------------------------------------------------------

    select case(vl%nature)
    
    case(1)
      !-------------------------------------------------------------------------------
      ! Si les particules sont spherique
      !-------------------------------------------------------------------------------
      call Sphere_LagrangianVelocity(u,v,w,vl,ndim,grid_x,&
	      grid_y,grid_z,grid_xu,grid_yv,grid_zw,dx,dy,dz,dxu,dyv,dzw)

    case(2)
      !-------------------------------------------------------------------------------
      ! Si les particules sont ellipsoidale
      !-------------------------------------------------------------------------------
      call Ellipsoid_LagrangianVelocity(u,v,w,vl,ndim,grid_x,&
	      grid_y,grid_z,grid_xu,grid_yv,grid_zw,dx,dy,dz,dxu,dyv,dzw) 
      !write(*,*) "STOP : Lagrangien velocity is not implemented for particles of ellipsoidal shape"
      !stop

    case default
      write(*,*) "STOP : Lagrangien velocity is not implemented for particles of any shape"
      stop

    end select

    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'sortie Particle_LagrangianVelocity'
    !-------------------------------------------------------------------------------
  end subroutine Particle_LagrangianVelocity

  !===============================================================================
end module Bib_VOFLag_Particle_LagrangianVelocity
!===============================================================================
