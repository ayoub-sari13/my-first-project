!===============================================================================
module Bib_VOFLag_Particle_PhaseFunction_PressureMesh
   !===============================================================================
   contains
   !-----------------------------------------------------------------------------  
   !> @author amine chadil, jorge cesar brandle de motta
   ! 
   !> @brief cette routine donne la fraction volumique du solide dans chaque point 
   !! de pression avec la taille physiques de la particule dans le cas de densite, et 
   !! et diemnsions reduites dans le cas de viscosite (le nombre de points fictifs est egale a 10)
   !
   !> @todo: gerer le chevauchement des particules, cou ne doit pas forcement s'additioner !! 
   !
   !> @param      cou     : la fonction couleur pour les viscosite calculees dans les points
   !! de pression.
   !> @param[out] vl      : la structure contenant toutes les informations
   !! relatives aux particules
   !> @param[in]  ndim     : dimension de l'espace. 
   !-----------------------------------------------------------------------------
   subroutine Particle_PhaseFunction_PressureMesh(cou,couin0,grid_xu,grid_yv,grid_zw,ndim,tot_pt_fict,vl)
      !===============================================================================
      !modules
      !===============================================================================
      !-------------------------------------------------------------------------------
      !Modules de definition des structures et variables => src/mod/module_...f90
      !-------------------------------------------------------------------------------
      use Module_VOFLag_ParticlesDataStructure
      use mod_Parameters,                       only : rank,nproc,deeptracking
      use mod_mpi
      !-------------------------------------------------------------------------------
      !Modules de subroutines de la librairie RESPECT => src/Bib_VOFLag_...f90
      !-------------------------------------------------------------------------------
      use Bib_VOFLag_Particle_PointIn
      !-------------------------------------------------------------------------------
      !===============================================================================
      !declarations des variables
      !===============================================================================
      implicit none
      !-------------------------------------------------------------------------------
      !variables globales
      !-------------------------------------------------------------------------------
      type(struct_vof_lag),                   intent(inout) :: vl
      real(8), dimension(:,:,:), allocatable, intent(inout) :: cou,couin0
      real(8), dimension(:),     allocatable, intent(in)    :: grid_xu,grid_yv,grid_zw
      integer,                                intent(in)    :: ndim,tot_pt_fict
      !-------------------------------------------------------------------------------
      !variables locales 
      !-------------------------------------------------------------------------------
      real(8), dimension(3)                                 :: ne_av,nw_av,se_av,sw_av,ne_der,&
                                                               nw_der,se_der,sw_der,pt_fictif
      real(8)                                               :: xxb,xxf,yyb,yyf,zzb,zzf
      integer                                               :: np,nbt,nd,ltp,total_pt_fictifs,&
                                                               nb_pt_fictifs_in_particle,ii,ik,&
                                                               ij,I,J,K,ltp_particle,l
      logical                                               :: ne_av_in,nw_av_in,se_av_in,sw_av_in,&
                                                               ne_der_in,nw_der_in,se_der_in,&
                                                               sw_der_in,pt_fictif_in
      !-------------------------------------------------------------------------------

      !-------------------------------------------------------------------------------
      if (deeptracking) write(*,*) 'entree Particle_PhaseFunction_PressureMesh'
      !-------------------------------------------------------------------------------

      !-------------------------------------------------------------------------------
      !initialisation
      !-------------------------------------------------------------------------------
      total_pt_fictifs= tot_pt_fict!10
      cou   = 0.0d0
      if ( allocated(couin0)) cou=couin0
      ne_av=0.0d0;nw_av=0.0d0;se_av=0.0d0;sw_av=0.0d0
      ne_der=0.0d0;nw_der=0.0d0;se_der=0.0d0;sw_der=0.0d0
      pt_fictif=0.0d0
      !-------------------------------------------------------------------------------
      !ne traiter que les particules (ou copies) qui intersecte le domaine du proc courant
      !-------------------------------------------------------------------------------
      do np=1,vl%kpt
         if (vl%objet(np)%on) then
            do nbt=1,vl%objet(np)%nbtimes
               if (vl%objet(np)%symperon(nbt)) then
                  !-------------------------------------------------------------------------------
                  !traitement des points de pression appartenant aux box
                  !-------------------------------------------------------------------------------
                  if (ndim==2) then
                     do j=sy,ey
                        do i=sx,ex
                           !-------------------------------------------------------------------------------
                           !construction de la cellule centree en lpt
                           !-------------------------------------------------------------------------------
                           xxb=grid_xu(i) ; xxf=grid_xu(i+1)
                           yyb=grid_yv(j) ; yyf=grid_yv(j+1)
                           ne_av(1) = xxf ; ne_av(2) = yyf ; nw_av(1) = xxb ; nw_av(2) = yyf
                           se_av(1) = xxf ; se_av(2) = yyb ; sw_av(1) = xxb ; sw_av(2) = yyb
                           !-------------------------------------------------------------------------------
                           !tester chacun des coins de la cellule (in ou out de la particule)
                           !-------------------------------------------------------------------------------
                           call Particle_PointIn(vl,np,nbt,vl%objet(np)%sca,ne_av,ndim,ne_av_in) 
                           call Particle_PointIn(vl,np,nbt,vl%objet(np)%sca,nw_av,ndim,nw_av_in) 
                           call Particle_PointIn(vl,np,nbt,vl%objet(np)%sca,se_av,ndim,se_av_in)
                           call Particle_PointIn(vl,np,nbt,vl%objet(np)%sca,sw_av,ndim,sw_av_in) 
                           !-------------------------------------------------------------------------------
                           !si tous les coins de la cellule sont a l'interieur de la particule
                           !-------------------------------------------------------------------------------
                           if (ne_av_in .and. nw_av_in .and. se_av_in .and. sw_av_in) then
                              cou(i,j,1)=1.d0
                           endif

                           !-------------------------------------------------------------------------------
                           !si la cellule n'est pas entierement a l'interieur de la particule
                           !-------------------------------------------------------------------------------
                           if (cou(i,j,1).lt.1.d0) then
                              if  ( ne_av_in .or. nw_av_in .or. se_av_in .or. sw_av_in ) then
                                 !-------------------------------------------------------------------------------
                                 !     version ibm
                                 !-------------------------------------------------------------------------------
                                 nb_pt_fictifs_in_particle=0
                                 do ij=1,total_pt_fictifs
                                    do ii=1,total_pt_fictifs
                                       pt_fictif(1)=xxb+dfloat(ii-1)*(xxf-xxb)/(dfloat(total_pt_fictifs)+1D-40)+&
                                                                     0.5d0*(xxf-xxb)/(dfloat(total_pt_fictifs)+1D-40)
                                       pt_fictif(2)=yyb+dfloat(ij-1)*(yyf-yyb)/(dfloat(total_pt_fictifs)+1D-40)+&
                                                                     0.5d0*(yyf-yyb)/(dfloat(total_pt_fictifs)+1D-40)
                                       call Particle_PointIn(vl,np,nbt,vl%objet(np)%sca,pt_fictif,ndim,pt_fictif_in)
                                       if (pt_fictif_in) nb_pt_fictifs_in_particle=nb_pt_fictifs_in_particle+1
                                    enddo
                                 enddo
                                 cou(i,j,1)=cou(i,j,1)+dfloat(nb_pt_fictifs_in_particle)/(dfloat(total_pt_fictifs)**ndim+1D-40)
                                 if (cou(i,j,1) .gt. 1.d0) cou(i,j,1)=1.d0
                              endif
                           endif
                        end do
                     end do
                  else
                     do k=sz,ez
                        do j=sy,ey
                           do i=sx,ex
                              !-------------------------------------------------------------------------------
                              !construction de la cellule centree en lpt
                              !-------------------------------------------------------------------------------
                              xxb=grid_xu(i);xxf=grid_xu(i+1)
                              yyb=grid_yv(j);yyf=grid_yv(j+1)
                              zzb=grid_zw(k);zzf=grid_zw(k+1)
                              ne_av(1) =xxf;ne_av(2) =yyf;ne_av(3) =zzf;nw_av(1) =xxb;nw_av(2) =yyf;nw_av(3) =zzf 
                              se_av(1) =xxf;se_av(2) =yyb;se_av(3) =zzf;sw_av(1) =xxb;sw_av(2) =yyb;sw_av(3) =zzf 
                              ne_der(1)=xxf;ne_der(2)=yyf;ne_der(3)=zzb;nw_der(1)=xxb;nw_der(2)=yyf;nw_der(3)=zzb 
                              se_der(1)=xxf;se_der(2)=yyb;se_der(3)=zzb;sw_der(1)=xxb;sw_der(2)=yyb;sw_der(3)=zzb

                              !-------------------------------------------------------------------------------
                              !tester chacun des coins de la cellule (in ou out de la particule)
                              !-------------------------------------------------------------------------------
                              call Particle_PointIn(vl,np,nbt,vl%objet(np)%sca,ne_av ,ndim,ne_av_in )   
                              call Particle_PointIn(vl,np,nbt,vl%objet(np)%sca,nw_av ,ndim,nw_av_in ) 
                              call Particle_PointIn(vl,np,nbt,vl%objet(np)%sca,se_av ,ndim,se_av_in )   
                              call Particle_PointIn(vl,np,nbt,vl%objet(np)%sca,sw_av ,ndim,sw_av_in ) 
                              call Particle_PointIn(vl,np,nbt,vl%objet(np)%sca,ne_der,ndim,ne_der_in) 
                              call Particle_PointIn(vl,np,nbt,vl%objet(np)%sca,nw_der,ndim,nw_der_in) 
                              call Particle_PointIn(vl,np,nbt,vl%objet(np)%sca,se_der,ndim,se_der_in) 
                              call Particle_PointIn(vl,np,nbt,vl%objet(np)%sca,sw_der,ndim,sw_der_in) 

                              !-------------------------------------------------------------------------------
                              !si tous les coins de la cellule sont a l'interieur de la particule
                              !-------------------------------------------------------------------------------
                              if (ne_av_in.and.nw_av_in.and.se_av_in.and.sw_av_in.and.&
                                    ne_der_in.and.nw_der_in.and.se_der_in.and.sw_der_in) then
                                 cou(i,j,k)=1.d0
                              endif

                              !-------------------------------------------------------------------------------
                              !si la cellule n'est pas entierement a l'interieur de la particule
                              !-------------------------------------------------------------------------------
                              if (cou(i,j,k).lt.1.d0) then
                                 if  (ne_av_in.or.nw_av_in.or.se_av_in.or.sw_av_in.or.&
                                       ne_der_in.or.nw_der_in.or.se_der_in.or.sw_der_in) then
                                    !-------------------------------------------------------------------------------
                                    !     version ibm
                                    !-------------------------------------------------------------------------------
                                    nb_pt_fictifs_in_particle=0
                                    do ik=1,total_pt_fictifs
                                       do ij=1,total_pt_fictifs
                                          do ii=1,total_pt_fictifs
                                             pt_fictif(1)=xxb+dfloat(ii-1)*(xxf-xxb)/(dfloat(total_pt_fictifs)+1D-40)+&
                                                                     0.5d0*(xxf-xxb)/(dfloat(total_pt_fictifs)+1D-40)
                                             pt_fictif(2)=yyb+dfloat(ij-1)*(yyf-yyb)/(dfloat(total_pt_fictifs)+1D-40)+&
                                                                     0.5d0*(yyf-yyb)/(dfloat(total_pt_fictifs)+1D-40)
                                             pt_fictif(3)=zzb+dfloat(ik-1)*(zzf-zzb)/(dfloat(total_pt_fictifs)+1D-40)+&
                                                                     0.5d0*(zzf-zzb)/(dfloat(total_pt_fictifs)+1D-40)
                                             call Particle_PointIn(vl,np,nbt,vl%objet(np)%sca,pt_fictif,ndim,pt_fictif_in)
                                             if (pt_fictif_in) nb_pt_fictifs_in_particle=nb_pt_fictifs_in_particle+1
                                          enddo
                                       enddo
                                    enddo
                                    cou(i,j,k)=cou(i,j,k)+dfloat(nb_pt_fictifs_in_particle)/(dfloat(total_pt_fictifs)**ndim+1D-40)
                                    if (cou(i,j,k) .gt. 1.d0) cou(i,j,k)=1.d0
                                 endif
                              endif
                           end do
                        enddo
                     enddo
                  end if
               end if
            end do
         end if
      end do

      !-------------------------------------------------------------------------------
      ! diffusion de cou a tous les procs 
      !-------------------------------------------------------------------------------
      if (nproc>1) then
        call comm_mpi_sca(cou)
      end if

      !-------------------------------------------------------------------------------
      if (deeptracking) write(*,*) 'sortie Particle_PhaseFunction_PressureMesh'
      !-------------------------------------------------------------------------------
   end subroutine Particle_PhaseFunction_PressureMesh

   !===============================================================================
end module Bib_VOFLag_Particle_PhaseFunction_PressureMesh
!===============================================================================