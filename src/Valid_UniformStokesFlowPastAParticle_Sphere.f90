!===============================================================================
module Bib_VOFLag_Valid_UniformStokesFlowPastAParticle_Sphere
  !===============================================================================
  contains
  !-----------------------------------------------------------------------------  
  !> @author  mohamed-amine chadil
  ! 
  !> @brief calcul des champs erreur sur la pression et vitesse du fluide pour un ecoulement
  !! uniforme autour d'une sphere dans le regime de stokes
  !-----------------------------------------------------------------------------
  subroutine Valid_UniformStokesFlowPastAParticle_Sphere(u,v,w,Ru0,Rv0,Rw0,pres,diff_pre,&
            diff_u_mean,diff_v_mean,diff_w_mean,U_inf,centre_sphere_bis,rayon_sphere_bis,&
            iteration_temps)
    !===============================================================================
    !modules
    !===============================================================================
    !-------------------------------------------------------------------------------
    !Modules de definition des structures et variables => src/mod/module_...f90
    !-------------------------------------------------------------------------------
    use mod_struct_thermophysics, only : fluids
    use mod_Parameters,           only : deeptracking,sx,ex,gx,sy,ey,gy,sz,ez,gz,sxu,  &
                                         exu,syu,eyu,sxv,exv,syv,eyv,szu,ezu,szv,ezv,  &
                                         szw,ezw,grid_xu,grid_yv,grid_zw,grid_x,grid_y,&
                                         grid_z,dx,dxu,dy,dyv,dz,dzw,rank,dim
    use mod_mpi
    !-------------------------------------------------------------------------------
    
    !===============================================================================
    !declarations des variables
    !===============================================================================
    implicit none
    !-------------------------------------------------------------------------------
    !variables globales
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable, intent(inout) :: diff_pre,diff_u_mean,diff_v_mean,&
                                                             diff_w_mean
    real(8), dimension(:,:,:), allocatable, intent(in)    :: u,v,w,Ru0,Rv0,Rw0,pres
    real(8), dimension(3)                                 :: centre_sphere_bis
    real(8)                                               :: U_inf,rayon_sphere_bis
    integer                                               :: iteration_temps
    !-------------------------------------------------------------------------------
    !variables locales 
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable :: pre_exact,u_exact_pre,v_exact_pre,w_exact_pre
    real(8), dimension(:,:,:), allocatable :: u_exact,v_exact,w_exact,diff_u,diff_v,diff_w
    real(8), dimension(:,:,:), allocatable :: u_pre,v_pre,w_pre
    real(8), dimension(3)                  :: centre_sphere
    real(8)                                :: rayon_sphere
    real(8)                                :: N_pre_exact,N_pre,N_pre_exact_loc,N_pre_loc
    real(8)                                :: N_u_exact_pre,N_u_pre,N_u_exact_pre_loc,N_u_pre_loc
    real(8)                                :: N_v_exact_pre,N_v_pre,N_v_exact_pre_loc,N_v_pre_loc
    real(8)                                :: N_w_exact_pre,N_w_pre,N_w_exact_pre_loc,N_w_pre_loc
    real(8)                                :: x,z,y,r,tmpu,tmpv,tmpw
    integer                                :: i,j,k,err
    !-------------------------------------------------------------------------------
    
    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'entree Valid_UniformStokesFlowPastAParticle_Sphere'
    !-------------------------------------------------------------------------------
    open(unit=1001,file="particles.in",status="old",action="read",iostat=err)
    read(1001,*)  centre_sphere(1),centre_sphere(2),centre_sphere(3),&
                     rayon_sphere
    close(1001)
    !-------------------------------------------------------------------------------
    !allocation des variables locales
    !-------------------------------------------------------------------------------
    allocate(u_exact_pre(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
    allocate(v_exact_pre(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
    allocate(w_exact_pre(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
    allocate(pre_exact  (sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
    allocate(u_pre      (sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
    allocate(v_pre      (sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
    allocate(w_pre      (sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))

    allocate(u_exact(sxu-gx:exu+gx,syu-gy:eyu+gy,szu-gz:ezu+gz))
    allocate(diff_u (sxu-gx:exu+gx,syu-gy:eyu+gy,szu-gz:ezu+gz))
    allocate(v_exact(sxv-gx:exv+gx,syv-gy:eyv+gy,szv-gz:ezv+gz))
    allocate(diff_v (sxv-gx:exv+gx,syv-gy:eyv+gy,szv-gz:ezv+gz))
    allocate(w_exact(sxw-gx:exw+gx,syw-gy:eyw+gy,szw-gz:ezw+gz))
    allocate(diff_w (sxw-gx:exw+gx,syw-gy:eyw+gy,szw-gz:ezw+gz))
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! calcul des pression ,des vitesses exactes et des erreurs sur les pression  au points de pression
    !-------------------------------------------------------------------------------
    pre_exact=0d0
    diff_pre =0d0 
    do k=sz,ez
      do j=sy,ey
         do i=sx,ex
            x=grid_x(i)-centre_sphere(1)
            z=grid_y(j)-centre_sphere(2)
            y=grid_z(k)-centre_sphere(3)
            r=sqrt(x**2+z**2+y**2)
            if (r.gt.rayon_sphere) then
               pre_exact(i,j,k)=-3d0*fluids(1)%mu*rayon_sphere/2d0*(x*U_inf/r**3)
            end if
            if (r.ge.rayon_sphere) then
               if (abs(pre_exact(i,j,k)) .gt. 1D-15) then
                  diff_pre(i,j,k)=abs(pre_exact(i,j,k)-pres(i,j,k))/abs(pre_exact(i,j,k))*100
               else
                  diff_pre(i,j,k)=abs(pres(i,j,k))*100
               end if
            else
               diff_pre(i,j,k)=0.d0
            end if
         end do
      end do
    end do
    !-------------------------------------------------------------------------------
    ! calcul des vitesses exactes et des erreurs sur les vitesses au points de vitesse
    !-------------------------------------------------------------------------------
    u_exact=0d0;u_exact_pre=0d0;diff_u=0d0
    v_exact=0d0;v_exact_pre=0d0;diff_v=0d0
    w_exact=0d0;w_exact_pre=0d0;diff_w=0d0
    do k=szu,ezu
      do j=syu,eyu
         do i=sxu,exu
            x=grid_xu(i)-centre_sphere(1)
            z=grid_y (j)-centre_sphere(2)
            y=grid_z (k)-centre_sphere(3)
            r=sqrt(x**2+z**2+y**2)
            if (r.gt.rayon_sphere) then
               !vitesse suivant x
               u_exact(i,j,k)=U_inf-3d0*rayon_sphere/4d0*(U_inf/r+x**2*U_inf/r**3) &
                              -3d0*rayon_sphere**3/4d0*(U_inf/(3d0*r**3)-x**2*U_inf/r**5)
            
               if (abs(u_exact(i,j,k)).gt.1D-14) then
                  diff_u(i,j,k)=abs(u_exact(i,j,k)-u(i,j,k))/abs(u_exact(i,j,k))*100
               else
                  diff_u(i,j,k)=abs(u(i,j,k))
               end if
            end if
         end do
      end do
    end do
    do k=szv,ezv
      do j=syv,eyv
         do i=sxv,exv
            x=grid_x (i)-centre_sphere(1)
            z=grid_yv(j)-centre_sphere(2)
            y=grid_z (k)-centre_sphere(3)
            r=sqrt(x**2+z**2+y**2)
            if (r.gt.rayon_sphere) then
               !vitesse suivant z
               v_exact(i,j,k)=-3d0*rayon_sphere/4d0*(x*z*U_inf/r**3)  & 
                              +3d0*rayon_sphere**3/4d0*(x*z*U_inf/r**5)
               if (abs(v_exact(i,j,k)).gt.1D-14) then
                  diff_v(i,j,k)=abs(v_exact(i,j,k)-v(i,j,k))/abs(v_exact(i,j,k))*100
               else
                  diff_v(i,j,k)=abs(v(i,j,k))
               end if
            end if 
         end do
      end do
    end do

    do k=szw,ezw
      do j=syw,eyw
         do i=sxw,exw
            x=grid_x (i)-centre_sphere(1)
            z=grid_y (j)-centre_sphere(2)
            y=grid_zw(k)-centre_sphere(3)
            r=sqrt(x**2+z**2+y**2)
            if (r.gt.rayon_sphere) then
               !vitesse suivant y
               w_exact(i,j,k)=-3d0*rayon_sphere/4d0*(x*y*U_inf/r**3) &
                              +3d0*rayon_sphere**3/4d0*(x*y*U_inf/r**5)
            
               if (abs(w_exact(i,j,k)) .gt. 1D-14) then
                  diff_w(i,j,k)=abs(w_exact(i,j,k)-w(i,j,k))/abs(w_exact(i,j,k))*100
               else
                  diff_w(i,j,k)=abs(w(i,j,k))
               end if
            end if
         end do
      end do
    end do
    call comm_mpi_uvw(u_exact,v_exact,w_exact)
    !-------------------------------------------------------------------------------
    ! calcul des erreurs sur les vitesses au points de pression
    !-------------------------------------------------------------------------------
    diff_u_mean=0d0 
    diff_v_mean=0d0 
    diff_w_mean=0d0 
    do k=sz,ez
      do j=sy,ey
         do i=sx,ex
            u_exact_pre(i,j,k) = (u_exact(i,j,k)+u_exact(i+1,j,k))/2d0
            u_pre      (i,j,k) = (u      (i,j,k)+u      (i+1,j,k))/2d0
            diff_u_mean(i,j,k) = (diff_u (i,j,k)+diff_u (i+1,j,k))/2d0

            v_exact_pre(i,j,k) = (v_exact(i,j,k)+v_exact(i,j+1,k))/2d0
            v_pre      (i,j,k) = (v      (i,j,k)+v      (i,j+1,k))/2d0
            diff_v_mean(i,j,k) = (diff_v (i,j,k)+diff_v (i,j+1,k))/2d0

            w_exact_pre(i,j,k) = (w_exact(i,j,k)+w_exact(i,j,k+1))/2d0
            w_pre      (i,j,k) = (w      (i,j,k)+w      (i,j,k+1))/2d0
            diff_w_mean(i,j,k) = (diff_w (i,j,k)+diff_w (i,j,k+1))/2d0
         end do
      end do
    end do
    
    N_pre_exact  =0d0;N_pre  =0d0
    N_u_exact_pre=0d0;N_u_pre=0d0
    N_v_exact_pre=0d0;N_v_pre=0d0
    N_w_exact_pre=0d0;N_w_pre=0d0
    do k=sz,ez
      do j=sy,ey
         do i=sx,ex
            N_pre_exact  =N_pre_exact  + pre_exact(i,j,k)**2*dx(i)*dy(j)*dz(k)
            N_pre        =N_pre        +(pre_exact(i,j,k)-pres(i,j,k))**2*dx(i)*dy(j)*dz(k)

            N_u_exact_pre=N_u_exact_pre+ u_exact_pre(i,j,k)**2*dxu(i)*dy(j)*dz(k)
            N_u_pre      =N_u_pre      +(u_exact_pre(i,j,k)-u_pre(i,j,k))**2*dxu(i)*dy(j)*dz(k)

            N_v_exact_pre=N_v_exact_pre+ v_exact_pre(i,j,k)**2*dx(i)*dyv(j)*dz(k)
            N_v_pre      =N_v_pre      +(v_exact_pre(i,j,k)-v_pre(i,j,k))**2*dx(i)*dyv(j)*dz(k)

            N_w_exact_pre=N_w_exact_pre+ w_exact_pre(i,j,k)**2*dx(i)*dy(j)*dzw(k)
            N_w_pre      =N_w_pre      +(w_exact_pre(i,j,k)-w_pre(i,j,k))**2*dx(i)*dy(j)*dzw(k)
         end do 
      end do 
    end do 
    N_pre_exact_loc  =N_pre_exact
    N_pre_loc        =N_pre
    N_u_exact_pre_loc=N_u_exact_pre
    N_v_exact_pre_loc=N_v_exact_pre
    N_w_exact_pre_loc=N_w_exact_pre
    N_u_pre_loc      =N_u_pre
    N_v_pre_loc      =N_v_pre
    N_w_pre_loc      =N_w_pre

    call mpi_allreduce(N_pre_exact_loc  ,N_pre_exact  ,1,mpi_double_precision,mpi_sum,comm3d,code)
    call mpi_allreduce(N_pre_loc        ,N_pre        ,1,mpi_double_precision,mpi_sum,comm3d,code)

    call mpi_allreduce(N_u_exact_pre_loc,N_u_exact_pre,1,mpi_double_precision,mpi_sum,comm3d,code)
    call mpi_allreduce(N_u_pre_loc      ,N_u_pre      ,1,mpi_double_precision,mpi_sum,comm3d,code)
    
    call mpi_allreduce(N_v_exact_pre_loc,N_v_exact_pre,1,mpi_double_precision,mpi_sum,comm3d,code)
    call mpi_allreduce(N_v_pre_loc      ,N_v_pre      ,1,mpi_double_precision,mpi_sum,comm3d,code)

    call mpi_allreduce(N_w_exact_pre_loc,N_w_exact_pre,1,mpi_double_precision,mpi_sum,comm3d,code)
    call mpi_allreduce(N_w_pre_loc      ,N_w_pre      ,1,mpi_double_precision,mpi_sum,comm3d,code)

    tmpu = 0d0 
    do k=szu,ezu
      do j=syu,eyu
         do i=sxu,exu
            tmpu = tmpu + ((u(i,j,k) - Ru0(i,j,k))**2)*(dxu(i)*dy(j)*dz(k))
         end do 
      end do 
    end do 
    if ( iteration_temps > 10 .and. sqrt(tmpu) < 1e-14) stop
    tmpv = 0d0
    do k=szv,ezv 
      do j=syv,eyv
         do i=sxv,exv
            tmpv = tmpv + ((v(i,j,k) - Rv0(i,j,k))**2)*(dx(i)*dyv(j)*dz(k))
         end do 
      end do 
    end do 
    if ( iteration_temps > 10 .and. sqrt(tmpv) < 1e-14) stop
    tmpw = 0d0
    do k=szw,ezw 
      do j=syw,eyw
         do i=sxw,exw
            tmpw = tmpw + ((w(i,j,k) - Rw0(i,j,k))**2)*(dx(i)*dy(j)*dzw(k))
         end do 
      end do 
    end do 
    if ( iteration_temps > 10 .and. sqrt(tmpv) < 1e-14) stop
    deallocate(u_exact_pre,v_exact_pre,w_exact_pre,pre_exact,u_pre,v_pre,w_pre,u_exact)
    deallocate(diff_u,v_exact,diff_v,w_exact,diff_w)
    !-------------------------------------------------------------------------------
    ! calcul de vitesse
    !-------------------------------------------------------------------------------
    if (rank .eq. 0) then 
       open(unit=1000,file='valid_uniform_flow.dat',status='old',position='append')
       write(1000,'(8E16.8)',advance='no') dfloat(iteration_temps),sqrt(N_pre/N_pre_exact)*100,&
                                           sqrt(N_u_pre/N_u_exact_pre)*100,&
                                           sqrt(N_v_pre/N_v_exact_pre)*100,&
                                           sqrt(N_w_pre/N_w_exact_pre)*100,&
                                           sqrt(tmpu/N_u_exact_pre),&
                                           sqrt(tmpv/N_v_exact_pre),&
                                           sqrt(tmpw/N_w_exact_pre)
       close(1000)
    end if
    !if (validation_uniform_flow_past_sphere_analytique) then
    !  pre=pre_exact
    !  vts=vts_exact
    !endif

    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'entree Valid_UniformStokesFlowPastAParticle_Sphere'
    !-------------------------------------------------------------------------------
  end subroutine Valid_UniformStokesFlowPastAParticle_Sphere

  !===============================================================================
end module Bib_VOFLag_Valid_UniformStokesFlowPastAParticle_Sphere
!===============================================================================
  


