!========================================================================
!
!                   FUGU Version 1.0.0
!
!========================================================================
!
! Copyright  Stéphane Vincent
!
! stephane.vincent@u-pem.fr          straightparadigm@gmail.com
!
! This file is part of the FUGU Fortran library, a set of Computational 
! Fluid Dynamics subroutines devoted to the simulation of multi-scale
! multi-phase flows for resolved scale interfaces (liquid/gas/solid). 
!
!========================================================================
!**
!**   NAME       : ExplicitAdvection.f90
!**
!**   AUTHOR     : Stéphane Vincent
!**                Benoit Trouette
!**
!**   FUNCTION   : Level Set subroutines of FUGU software
!**
!**   DATES      : Version 1.0.0  : from : feb, 2018
!**
!========================================================================

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
module mod_explicit_advection
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  use mod_Parameters
  use mod_borders
  use mod_Constants
  use mod_struct_grid
  use mod_struct_Particle_Tracking
  use mod_Lagrangian_Particle
  use mod_interpolation
  use mod_LevelSet
  use mod_mpi
  
contains
  !##################################################################################
  !
  !                              Explicit Scalar Advection Schemes
  !
  !##################################################################################
  
  !**********************************************************************************
  subroutine ScalarAdvection(nt,time,phi,phi0,phi1,u,v,w,u0,v0,w0,LPart,nPartL,scheme)
    !*******************************************************************************
    ! Modules
    !*******************************************************************************
    use mod_Lagrangian2Eulerian
    !*******************************************************************************
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)                                        :: scheme,nPartL,nt
    real(8), intent(in)                                        :: time 
    real(8), allocatable, dimension(:,:,:), intent(in)         :: u,v,w,u0,v0,w0
    real(8), allocatable, dimension(:,:,:), intent(inout)      :: phi,phi0,phi1
    type(particle_t), dimension(:), allocatable, intent(inout) :: LPart
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                                    :: nbr,i,j,k,n
    integer, save                                              :: step=1
    real(8)                                                    :: vel
    real(8)                                                    :: dv
    real(8)                                                    :: dphi,nphi
    real(8)                                                    :: lmaxphi,lminphi,maxphi,minphi
    real(8)                                                    :: iphi,iphi0,iphi1
    real(8), allocatable, dimension(:,:,:)                     :: ut,vt,wt
    real(8), allocatable, dimension(:,:,:)                     :: phib
    !-------------------------------------------------------------------------------

    select case (scheme)
    case(1,2,3)
       allocate(ut(sxu-gx:exu+gx,syu-gy:eyu+gy,szu-gz:ezu+gz))
       allocate(vt(sxv-gx:exv+gx,syv-gy:eyv+gy,szv-gz:ezv+gz))
       if (dim==3) allocate(wt(sxw-gx:exw+gx,syw-gy:eyw+gy,szw-gz:ezw+gz))
       call transformation_2d3d(u,ut,ddxu,1)
       call transformation_2d3d(v,vt,ddyv,2)
       if (dim==3) call transformation_2d3d(w,wt,ddzw,3) 
       call uvw_borders_and_periodicity(mesh,ut,vt,wt)
    end select
    
    select case (scheme)
    case (1)
       !---------------------------------------------------------------------------
       ! WENO5 non conservative
       !---------------------------------------------------------------------------
       if (dim==2) then 
          call schemewenonc_2d(phi,ut,vt)
       else if (dim==3) then
          call schemewenonc_3d(phi,ut,vt,wt)
       end if
       !---------------------------------------------------------------------------
    case(2)
       !---------------------------------------------------------------------------
       ! WENO5 conservative
       !---------------------------------------------------------------------------
       if (regular_mesh) then 
          if (dim==2) then 
             call schemewenoc_2d(phi,ut,vt)
          else if (dim==3) then
             call schemewenoc_3d(phi,ut,vt,wt)
          end if
       else
          write(*,*) "conservative weno5 scheme is not developped for irregular meshes, stop"
          stop
       end if
       !---------------------------------------------------------------------------
    case(3)
       !---------------------------------------------------------------------------
       ! Lax Wendroff_TVD
       !---------------------------------------------------------------------------
       if (dim==2) then
          call scheme_laxwendrofftvd_2d(phi,ut,vt)
       else if (dim==3) then
          stop
       end if
       !---------------------------------------------------------------------------
    case(4)
       !---------------------------------------------------------------------------
       ! FD 1st order Upwind explicit u grad T
       !---------------------------------------------------------------------------
       phi=phi0
       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                !---------------------------------------------------------------------------
                ! x-direction
                !---------------------------------------------------------------------------
                vel=(dxu2(i+1)*u(i,j,k)+dxu2(i)*u(i+1,j,k))/dx(i)
                phi(i,j,k)=phi(i,j,k) &
                     & - max(vel,zero)*dt/dxu(i  )*(phi0(i  ,j,k)-phi0(i-1,j,k)) &
                     & - min(vel,zero)*dt/dxu(i+1)*(phi0(i+1,j,k)-phi0(i,  j,k))
                !---------------------------------------------------------------------------
                ! y-direction
                !---------------------------------------------------------------------------
                vel=(dyv2(j+1)*v(i,j,k)+dyv2(j)*v(i,j+1,k))/dy(j)
                phi(i,j,k)=phi(i,j,k) &
                     & - max(vel,zero)*dt/dyv(j  )*(phi0(i,j  ,k)-phi0(i,j-1,k)) &
                     & - min(vel,zero)*dt/dyv(j+1)*(phi0(i,j+1,k)-phi0(i,j  ,k))
                !---------------------------------------------------------------------------
                ! z-direction
                !---------------------------------------------------------------------------
                if (dim==3) then
                   vel=(dzw2(k+1)*w(i,j,k)+dzw2(k)*w(i,j,k+1))/dz(k)
                   phi(i,j,k)=phi(i,j,k) &
                        & - max(vel,zero)*dt/dzw(k  )*(phi0(i,j,k  )-phi0(i,j,k-1)) &
                        & - min(vel,zero)*dt/dzw(k+1)*(phi0(i,j,k+1)-phi0(i,j,k  ))
                end if
                !---------------------------------------------------------------------------
             end do
          end do
       end do
       !---------------------------------------------------------------------------
    case(5)
       !---------------------------------------------------------------------------
       ! FD Centered explicit u grad T
       !---------------------------------------------------------------------------
       phi=phi0
       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                !---------------------------------------------------------------------------
                ! x-direction
                !---------------------------------------------------------------------------
                vel=(dxu2(i+1)*u(i,j,k)+dxu2(i)*u(i+1,j,k))/dx(i)
                phi(i,j,k)=phi(i,j,k) &
                     & - vel*dt/(dxu(i)+dxu(i+1))*(phi0(i+1,j,k)-phi0(i-1,j,k))
                !---------------------------------------------------------------------------
                ! y-direction
                !---------------------------------------------------------------------------
                vel=(dyv2(j+1)*v(i,j,k)+dyv2(j)*v(i,j+1,k))/dy(j)
                phi(i,j,k)=phi(i,j,k) &
                     & - vel*dt/(dyv(j)+dyv(j+1))*(phi0(i,j+1,k)-phi0(i,j-1,k))
                !---------------------------------------------------------------------------
                ! z-direction
                !---------------------------------------------------------------------------
                if (dim==3) then
                   vel=(dzw2(k+1)*w(i,j,k)+dzw2(k)*w(i,j,k+1))/dz(k)
                   phi(i,j,k)=phi(i,j,k) &
                        & - vel*dt/(dzw(k)+dzw(k+1))*(phi0(i,j,k+1)-phi0(i,j,k-1))
                end if
                !---------------------------------------------------------------------------
             end do
          end do
       end do
       !---------------------------------------------------------------------------
    case(10:19)
       PartAreMarkers=.true.
       select case(step)
       case(1)
          !-------------------------------------------------------------------------------
          ! pure transport (advection step)
          !-------------------------------------------------------------------------------
          if (iRealloc>0) then 
             if (modulo(nt,iRealloc)==0) call ReallocationParticle(LPart,nPartL,u,v,w,phi)
          end if
          call Lagrangian_Particle_Tracking(mesh,u,v,w,u0,v0,w0, &
               & LPart,nPartL,nt,dim)
          !-------------------------------------------------------------------------------
          phi = 0
          call ComputeScaAverages(mesh,LPart,nPartL,phi)
          !-------------------------------------------------------------------------------
          step=2
          !-------------------------------------------------------------------------------
       case(2)
          !-------------------------------------------------------------------------------
          ! diffusion step
          !-------------------------------------------------------------------------------
          ! maxval and minval eulerian phi values
          !-------------------------------------------------------------------------------
          lmaxphi=maxval(phi(sx:ex,sy:ey,sz:ez))
          lminphi=minval(phi(sx:ex,sy:ey,sz:ez))
          call mpi_allreduce(lmaxphi,maxphi,1,mpi_double_precision,mpi_max,comm3d,code)
          call mpi_allreduce(lminphi,minphi,1,mpi_double_precision,mpi_min,comm3d,code)
          !-------------------------------------------------------------------------------
          if (Euler==1) then
             do n=1,nPartL
                call interpolation_phi(time,phi ,LPart(n)%r(1:dim),iphi ,1,dim)
                call interpolation_phi(time,phi0,LPart(n)%r(1:dim),iphi0,1,dim)
                dphi=iphi-iphi0
                !-------------------------------------------------------------------------------
                ! clipping of the updated signal
                !-------------------------------------------------------------------------------
                if (PhiCutOff) then
                   nphi=LPart(n)%phi+dphi
                   if (nphi>maxphi.or.nphi<minphi) dphi=0
                end if
                !-------------------------------------------------------------------------------
                LPart(n)%phi=LPart(n)%phi+dphi
             end do
          else if (abs(Euler)==2) then
             do n=1,nPartL
                call interpolation_phi(time,phi ,LPart(n)%r(1:dim),iphi ,1,dim)
                call interpolation_phi(time,phi0,LPart(n)%r(1:dim),iphi0,1,dim)
                call interpolation_phi(time,phi1,LPart(n)%r(1:dim),iphi1,1,dim)
                dphi=iphi-iphi0
                !-------------------------------------------------------------------------------
                ! clipping of the updated signal
                !-------------------------------------------------------------------------------
                if (PhiCutOff) then
                   nphi=LPart(n)%phi+dphi
                   if (nphi>maxphi.or.nphi<minphi) dphi=0
                end if
                !-------------------------------------------------------------------------------
                LPart(n)%phi=LPart(n)%phi+dphi
             end do
          end if
          !-------------------------------------------------------------------------------
          ! boundary conditions applied on markers
          !-------------------------------------------------------------------------------
          do n=1,nPartL
             if (LPart(n)%active) then
                call search_cell_ijk(mesh,LPart(n)%r,LPart(n)%ijk,PRESS_MESH)
                i=LPart(n)%ijk(1)
                j=LPart(n)%ijk(2)
                k=LPart(n)%ijk(3)
                !-------------------------------------------------------------------------------
                ! neumann or dirichlet cases, marker takes the cell value
                !-------------------------------------------------------------------------------
                if (.not.Periodic(1)) then
                   if (i<=gsx.or.i>=gex) LPart(n)%phi=phi(i,j,k)
                end if
                if (.not.Periodic(2)) then
                   if (j<=gsy.or.j>=gey) LPart(n)%phi=phi(i,j,k)
                end if
                if (dim==3.and..not.Periodic(3)) then
                   if (k<=gsz.or.k>=gez) LPart(n)%phi=phi(i,j,k)
                end if
                !-------------------------------------------------------------------------------
             end if
          end do
          !-------------------------------------------------------------------------------
          step=1
          !-------------------------------------------------------------------------------
       end select
    case(20:29)
       PartAreMarkers=.true.
       select case(step)
       case(1)
          !-------------------------------------------------------------------------------
          ! PIC for scalar
          !-------------------------------------------------------------------------------
          call PIC_Sca_PartSplit_Remesh(nt,time,LPart,nPartL,phi,phi0,u,v,w)
          !-------------------------------------------------------------------------------
          step=2
          !-------------------------------------------------------------------------------
       case(2)
          !-------------------------------------------------------------------------------
          step=1
          !-------------------------------------------------------------------------------
       end select
    case(30:39)
       PartAreMarkers=.true.
       select case(scheme)
       case(30)
          !-------------------------------------------------------------------------------
          ! pure transport (advection step)
          !-------------------------------------------------------------------------------
          if (iRealloc>0) then 
             if (modulo(nt,iRealloc)==0) then
                !-------------------------------------------------------------------------------
                ! binary color 
                !-------------------------------------------------------------------------------
                allocate(phib(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
                phib = phi
                where (phib>d1p2)
                   phib = 1
                elsewhere
                   phib = 0
                end where
                !-------------------------------------------------------------------------------
                call ReallocationParticle(LPart,nPartL,u,v,w,phib)
                !-------------------------------------------------------------------------------

                !-------------------------------------------------------------------------------
                ! New particle value
                !-------------------------------------------------------------------------------
                do n = 1,nPartL
                   if (LPart(n)%active) then
                      if (LPart(n)%new) then
                         call interpolation_phi(time,phi,LPart(n)%r,LPart(n)%phi,1,dim)
                         if (LPart(n)%phi>d1p2) then
                            LPart(n)%phi = 1
                         else
                            LPart(n)%phi = 0
                         end if
                         LPart(n)%new = .false.
                      end if
                   end if
                end do
                !-------------------------------------------------------------------------------
                
             end if
          end if
          call Lagrangian_Particle_Tracking(mesh,u,v,w,u0,v0,w0,LPart,nPartL,nt,dim)
          call ComputeScaAverages2(0,mesh,LPart,nPartL,phi)
          !-------------------------------------------------------------------------------
       case(31)
          !-------------------------------------------------------------------------------
          ! pure transport (advection step)
          !-------------------------------------------------------------------------------
          if (iRealloc>0) then 
             if (modulo(nt,iRealloc)==0) then
                !-------------------------------------------------------------------------------
                call ReallocationParticle2(mesh,LPart,nPartL,phi)
                !-------------------------------------------------------------------------------

                !-------------------------------------------------------------------------------
                ! New particle value
                !-------------------------------------------------------------------------------
                do n = 1,nPartL
                   if (LPart(n)%active) then
                      if (LPart(n)%new) then
                         call interpolation_phi(time,phi,LPart(n)%r,LPart(n)%phi,1,dim)
                         if (LPart(n)%phi>d1p2) then
                            LPart(n)%phi = 1
                         else
                            LPart(n)%phi = 0
                         end if
                         LPart(n)%new = .false.
                      end if
                   end if
                end do
                !-------------------------------------------------------------------------------

             end if
          end if
          call Lagrangian_Particle_Tracking(mesh,u,v,w,u0,v0,w0,LPart,nPartL,nt,dim)
          call ComputeScaAverages2(0,mesh,LPart,nPartL,phi)
          !-------------------------------------------------------------------------------
       end select
    end select
  end subroutine ScalarAdvection


    !**********************************************************************************
  subroutine VelocityAdvection(nt,time,u,v,w,u0,v0,w0,LPart,nPartL,scheme)
    !*******************************************************************************
    !   Modules                                                                     !
    !*******************************************************************************!
    !*******************************************************************************!
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)                                      :: scheme,nPartL,nt
    real(8), intent(in)                                      :: time 
    real(8), allocatable, dimension(:,:,:), intent(inout)    :: u,v,w,u0,v0,w0
    type(particle_t), dimension(:), allocatable, intent(inout) :: LPart
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                                  :: nbr,i,j,k,n,dir
    integer, save                                            :: step=1
    real(8), allocatable, dimension(:,:,:)                   :: umag
    real(8), dimension(3)                                    :: ivel,ivel0,dvel
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! init
    !-------------------------------------------------------------------------------
    allocate(umag(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
    umag=0
    !-------------------------------------------------------------------------------
    
    
    select case (scheme)
    case(10)
       PartAreMarkers=.true.
       select case(step)
       case(1)
          !-------------------------------------------------------------------------------
          ! pure transport (advection step)
          !-------------------------------------------------------------------------------
          ! velocity magnitude
          !-------------------------------------------------------------------------------
          do k=sz-(dim-2),ez+(dim-2)
             do j=sy-1,ey+1
                do i=sx-1,ex+1
                   umag(i,j,k) = ((dxu2(i+1)*u(i,j,k)+dxu2(i)*u(i+1,j,k))/dx(i))**2 &
                        &       +((dyv2(j+1)*v(i,j,k)+dyv2(j)*v(i,j+1,k))/dy(j))**2
                   if (dim==3) then
                      umag(i,j,k)=umag(i,j,k) &
                           & +  ((dzw2(k+1)*w(i,j,k)+dzw2(k)*w(i,j,k+1))/dz(k))**2 
                   end if
                end do
             end do
          end do
          !-------------------------------------------------------------------------------
          call ReallocationParticle(LPart,nPartL,u,v,w,umag)
          call Lagrangian_Particle_Tracking(mesh,u,v,w,u0,v0,w0, &
               & LPart,nPartL,nt,dim)
          call ComputeVelAverages(mesh,LPart,nPartL,u,v,w)
          !-------------------------------------------------------------------------------
          step=2
          !-------------------------------------------------------------------------------
       case(2)
          !-------------------------------------------------------------------------------
          step=1
          !-------------------------------------------------------------------------------
       end select
    case(11)
       PartAreMarkers=.true.
       select case(step)
       case(1)
          !-------------------------------------------------------------------------------
          ! pure transport (advection step)
          !-------------------------------------------------------------------------------
          ! velocity magnitude
          !-------------------------------------------------------------------------------
          do k=sz-(dim-2),ez+(dim-2)
             do j=sy-1,ey+1
                do i=sx-1,ex+1
                   umag(i,j,k) = ((dxu2(i+1)*u(i,j,k)+dxu2(i)*u(i+1,j,k))/dx(i))**2 &
                        &       +((dyv2(j+1)*v(i,j,k)+dyv2(j)*v(i,j+1,k))/dy(j))**2
                   if (dim==3) then
                      umag(i,j,k)=umag(i,j,k) &
                           & +  ((dzw2(k+1)*w(i,j,k)+dzw2(k)*w(i,j,k+1))/dz(k))**2 
                   end if
                end do
             end do
          end do
          !-------------------------------------------------------------------------------
          if (iRealloc>0) then
             if (modulo(nt,iRealloc)==0) then
                call ReallocationParticle(LPart,nPartL,u,v,w,umag)
             end if
          end if
          call Lagrangian_Particle_Tracking(mesh,u,v,w,u0,v0,w0, &
               & LPart,nPartL,nt,dim)
!!$          do n=1,nPartL
!!$             LPart(n)%u0=LPart(n)%u
!!$          end do
          call ComputeVelAverages(mesh,LPart,nPartL,u,v,w)
          !-------------------------------------------------------------------------------
          step=2
          !-------------------------------------------------------------------------------
       case(2)
          !-------------------------------------------------------------------------------
          ! diffusion step
          !-------------------------------------------------------------------------------
          do n=1,nPartL
             call interpolation_uvw(time,u ,v ,w ,LPart(n)%r(1:dim),ivel ,1,dim)
             call interpolation_uvw(time,u0,v0,w0,LPart(n)%r(1:dim),ivel0,1,dim)
             dvel(1:dim)=ivel(1:dim)-ivel0(1:dim)
             LPart(n)%u0(1:dim)=LPart(n)%u0(1:dim)+dvel(1:dim)
          end do
          !-------------------------------------------------------------------------------
          ! boundary conditions applied on markers
          !-------------------------------------------------------------------------------
          do n=1,nPartL
             if (LPart(n)%active) then
                do dir=1,dim
                   call search_cell_ijk(mesh,LPart(n)%r,LPart(n)%ijk,dir)
                   i=LPart(n)%ijk(1)
                   j=LPart(n)%ijk(2)
                   k=LPart(n)%ijk(3)
!!$                   !-------------------------------------------------------------------------------
!!$                   ! neumann or dirichlet cases, marker takes the cell value
!!$                   !-------------------------------------------------------------------------------
!!$                   select case(dir)
!!$                   case(1)
!!$                      if (.not.Periodic(1)) then
!!$                         if (i<=gsxu.or.i>=gexu) LPart(n)%u0(dir)=u(i,j,k)
!!$                      end if
!!$                      if (.not.Periodic(2)) then
!!$                         if (j<=gsy.or.j>=gey) LPart(n)%u0(dir)=v(i,j,k)
!!$                      end if
!!$                   case(2)
!!$                      if (.not.Periodic(1)) then
!!$                         if (i<=gsx.or.i>=gex) LPart(n)%u0(dir)=u(i,j,k)
!!$                      end if
!!$                      if (.not.Periodic(2)) then
!!$                         if (j<=gsyv.or.j>=geyv) LPart(n)%u0(dir)=v(i,j,k)
!!$                      end if
!!$                   end select
!!$                   !-------------------------------------------------------------------------------
                end do
             end if
          end do
          !-------------------------------------------------------------------------------
          step=1
          !-------------------------------------------------------------------------------
       end select
    end select
    
    deallocate(umag)

  end subroutine VelocityAdvection



  subroutine solution_reference_tache(time,sca,case)
    !-------------------------------------------------------------------------------
    use mod_Parameters, only: dx,dy,dz,dt,sx,ex,sy,ey,sz,ez,&
         & xmin,xmax,ymin,ymax,zmin,zmax,gy,gx,pi
    use mod_struct_thermophysics
    implicit none
    !-------------------------------------------------------------------------------
    ! global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)                                   :: case
    real(8), intent(in)                                   :: time
    real(8), allocatable, dimension(:,:,:), intent(inout) :: sca
    !-------------------------------------------------------------------------------
    ! local varables
    !-------------------------------------------------------------------------------
    integer                                               :: i,j,m,p,k,nrak=200
    real(8)                                               :: alpha,h,diff,beta
    real(8)                                               :: x,y,r,xc,yc,theta
    real(8)                                               :: mu,sigma
    real(8), dimension(200), save                         :: rak,la,phi_0
    logical, save                                         :: once=.true.
    !- case -------------------------------------------------------------------------
    real(8) :: radius
    real(8) :: xc0
    real(8) :: yc0
    real(8) :: xcRot
    real(8) :: ycRot
    !-------------------------------------------------------------------------------

    if (.not.allocated(sca)) then
       allocate(sca(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
    end if
    
    !-------------------------------------------------------------------------------
    ! diffusion coefficient
    !-------------------------------------------------------------------------------
    diff=fluids(1)%lambda/(fluids(1)%rho*fluids(1)%cp)
    !-------------------------------------------------------------------------------

    !-------------------------------------------
    ! parameters
    !-------------------------------------------
    xc0=xmin+0.5_8*(xmax-xmin)
    yc0=ymin+0.75_8*(ymax-ymin)
    radius=0.1_8
    xcRot=(xmax+xmin)/2
    ycRot=(ymax+ymin)/2
    !-------------------------------------------


    !-------------------------------------------------------------------------------
    ! Racines de J0(alpha) = 0
    !-------------------------------------------------------------------------------
    if (once) then

       include 'LambdaBessel.f90'
       
       do p=1,nrak
          select case(case)
          case(2) ! Crenau
             la(p)=1._8/(bessj1(rak(p))**2)/rak(p)
          case(3) ! cone
             alpha=pi*(radius/2)*rak(p)*(STVH0(radius*rak(p))*bessj1(radius*rak(p))-STVH1(radius*rak(p))*bessj0(radius*rak(p)))
             phi_0(p)=(2/radius)/(bessj1(rak(p))**2)/(rak(p)**3)
             phi_0(p)=-phi_0(p)*(radius*radius*rak(p)*rak(p)*bessj1(radius*rak(p))-alpha)
             la(p)=phi_0(p)+2*radius*bessj1(rak(p)*radius)/((bessj1(rak(p))**2))/rak(p)
          case(4)
          case default
             stop
          end select
       end do

       once=.false.
       
    end if

    ! angle 
    theta=pi/2*time

    ! init
    sca=0

    ! local coordinates
    xc=cos(theta)*(xc0-xcRot)-sin(theta)*(yc0-ycRot)
    yc=sin(theta)*(xc0-xcRot)+cos(theta)*(yc0-ycRot)
    xc=xc+xcRot
    yc=yc+ycRot
    
    
    select case(case)
    case (2) ! creneau
       
       do j=sy,ey   
          do i=sx,ex
             
             x=grid_x(i)-xc
             y=grid_y(j)-yc
             r=sqrt(x*x+y*y)
             
             if (r<1) then
                do p=1,nrak
                   sca(i,j,1)=sca(i,j,1)&
                        & +la(p)*2*radius*bessj1(rak(p)*radius)*bessj0(rak(p)*r)&
                        & *exp(-time*diff*rak(p)**2)    
                enddo
             end if
             
          end do
       end do

       
    case(3)  ! cone
       
       do j=sy,ey   
          do i=sx,ex

             x=grid_x(i)-xc
             y=grid_y(j)-yc
             r=sqrt(x*x+y*y)
             
             if (r<1) then
                do p=1,nrak
                   sca(i,j,1)=sca(i,j,1)&
                        & +la(p)*bessj0(rak(p)*r)&
                        & *exp(-time*diff*rak(p)**2)
                enddo
             end if
             
          end do
       end do

    case(4)

       mu=0
       sigma=0.05d0*max((xmax-xmin),(ymax-ymin))
       
       do j=sy,ey   
          do i=sx,ex

             x=grid_x(i)-xc
             y=grid_y(j)-yc
             r=sqrt(x*x+y*y)

             sca(i,j,1)=0.125d0/sigma/sqrt(2*pi)*exp(-(r-mu)**2/2/sigma**2)


          end do
       end do

    case default 
       
    end select
    
  end subroutine solution_reference_tache

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  FUNCTION bessj0(x)
    real(8) ::  bessj0
    real(8) ::  x
    real(8) :: ax,xx,z
    real(8) p1,p2,p3,p4,p5,q1,q2,q3,q4,q5,r1,r2,r3,r4,r5,r6,s1,s2,s3,s4,s5,s6,y
    SAVE p1,p2,p3,p4,p5,q1,q2,q3,q4,q5,r1,r2,r3,r4,r5,r6,s1,s2,s3,s4,s5,s6
    DATA p1,p2,p3,p4,p5/1.d0,-.1098628627d-2,.2734510407d-4,-.2073370639d-5,.2093887211d-6/
    DATA q1,q2,q3,q4,q5/-.1562499995d-1,.1430488765d-3,-.6911147651d-5,.7621095161d-6,-.934945152d-7/
    DATA r1,r2,r3,r4,r5,r6/57568490574.d0,-13362590354.d0,651619640.7d0,-11214424.18d0,77392.33017d0,-184.9052456d0/
    DATA s1,s2,s3,s4,s5,s6/57568490411.d0,1029532985.d0,9494680.718d0,59272.64853d0,267.8532712d0,1.d0/

    if(abs(x).lt.8.D0)then
       y=x**2
       bessj0=(r1+y*(r2+y*(r3+y*(r4+y*(r5+y*r6)))))/(s1+y*(s2+y*(s3+y*(s4+y*(s5+y*s6)))))
    else
       ax=abs(x)
       z=8.D0/ax
       y=z**2
       xx=ax-.785398164D0
       bessj0=sqrt(.636619772d0/ax)*(cos(xx)*(p1+y*(p2+y*(p3+y*(p4+y*p5))))-z*sin(xx)*(q1+y*(q2+y*(q3+y*(q4+y*q5)))))
    endif
    return
  END FUNCTION bessj0
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  
  FUNCTION bessj1(x)
    real(8) bessj1,x
    real(8) ax,xx,z
    real(8) p1,p2,p3,p4,p5,q1,q2,q3,q4,q5,r1,r2,r3,r4,r5,r6,s1,s2,s3,s4,s5,s6,y
    SAVE p1,p2,p3,p4,p5,q1,q2,q3,q4,q5,r1,r2,r3,r4,r5,r6,s1,s2,s3,s4,s5,s6
    DATA r1,r2,r3,r4,r5,r6/72362614232.d0,-7895059235.d0,242396853.1d0,-2972611.439d0,15704.48260d0,-30.16036606d0/
    DATA s1,s2,s3,s4,s5,s6/144725228442.d0,2300535178.d0,18583304.74d0,99447.43394d0,376.9991397d0,1.d0/
    DATA p1,p2,p3,p4,p5/1.d0,.183105d-2,-.3516396496d-4,.2457520174d-5,-.240337019d-6/
    DATA q1,q2,q3,q4,q5/.04687499995d0,-.2002690873d-3,.8449199096d-5,-.88228987d-6,.105787412d-6/

    if(abs(x).lt.8.D0)then
       y=x**2
       bessj1=x*(r1+y*(r2+y*(r3+y*(r4+y*(r5+y*r6)))))/(s1+y*(s2+y*(s3+y*(s4+y*(s5+y*s6)))))
    else
       ax=abs(x)
       z=8.D0/ax
       y=z**2
       xx=ax-2.356194491D0
       bessj1=sqrt(.636619772d0/ax)*(cos(xx)*(p1+y*(p2+y*(p3+y*(p4+y*p5))))-z*sin(xx)*(q1+y*(q2+y*(q3+y*(q4+y*q5)))))*dsign(1.d0,x)
    endif
    return
  END FUNCTION bessj1

 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


  INCLUDE 'Struv.f90'
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

end module mod_explicit_advection
  

