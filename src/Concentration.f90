!========================================================================
!
!                   FUGU Version 1.0.0
!
!========================================================================
!
! Copyright  Stéphane Vincent
!
! stephane.vincent@u-pem.fr          straightparadigm@gmail.com
!
! This file is part of the FUGU Fortran library, a set of Computational 
! Fluid Dynamics subroutines devoted to the simulation of multi-scale
! multi-phase flows for resolved scale interfaces (liquid/gas/solid). 
! FUGU uses the initial developments of DyJeAT and SHALA codes from 
! Jean-Luc Estivalezes (jean-luc.estivalezes@onera.fr) and 
! Frédéric Couderc (couderc@math.univ-toulouse.fr).
!
!========================================================================
!**
!**   NAME       : Energy.f90
!**
!**   AUTHOR     : Stéphane Vincent
!**              : Benoit Trouette
!**
!**   FUNCTION   : Energy Equation modules of FUGU software
!**
!**   DATES      : Version 1.0.0  : from : feb, 20, 2018
!**
!========================================================================

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#include "precomp.h"
module mod_concentration
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
contains

  
  subroutine Concentration_Delta (u,v,w,con,cou,slcon,smcon,spcon,conin, &
       & con0,con1,difu,difv,difw,pencon,penu,penv,penw,energy)
    !*****************************************************************************************************
    !*****************************************************************************************************
    !*****************************************************************************************************
    !*****************************************************************************************************
    use mod_mpi
    use mod_borders
    use mod_Parameters, only: dim,nb_T,gx,gy,gz,            &
         & ex,sx,ey,sy,ez,sz,                               &
         & exs,sxs,eys,sys,ezs,szs,                         &
         & gsx,gex,gsy,gey,gsz,gez,                         &
         & Euler,                                           &
         & CO_source_term,CO_linear_term,CO_inertial_scheme,&
         & CO_solver_type,CO_solver_threshold,CO_solver_it, &
         & CO_solver_precond,CO_penalty_term,               &
         & dx,dy,dz,impp_concentration
    use mod_Constants, only: one
    use mod_Connectivity
    use mod_operators
    use mod_solver
    use mod_struct_solver
    use mod_struct_Energy
    use mod_mumps_dep, only : mumps_par_EN
    use mod_Heat
    !-------------------------------------------------------------------------------
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8),  dimension(:,:,:),   allocatable :: cou
    real(8),  dimension(:,:,:),   allocatable :: difu,difv,difw
    real(8),  dimension(:,:,:),   allocatable :: u,v,w,con,con0,con1
    real(8),  dimension(:,:,:),   allocatable :: slcon,smcon
    real(8),  dimension(:,:,:),   allocatable :: conin
    real(8),  dimension(:,:,:,:), allocatable :: pencon,penu,penv,penw
    real(8),  dimension(:,:,:,:), allocatable :: spcon
    type(struct_energy), intent(inout)        :: energy
    !-------------------------------------------------------------------------------
    
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                   :: i,j,k,l,lts,impp,it_solv,la
    integer                                   :: ii,jj,kk
    integer                                   :: nic
    integer, dimension(:), allocatable        :: jcof,icof
    integer, dimension(:), allocatable        :: kic
    real(8)                                   :: pe_m,pe_p,pe_l,pe_r,pe_b,pe_f
    real(8)                                   :: res,div_norm
    real(8),  dimension(:), allocatable       :: coef,smc,sol
    real(8),  dimension(:,:,:), allocatable   :: thetacon
    real(8),  dimension(:,:,:), allocatable   :: smc_tab
    character(len=100)                        :: fmt1
    real(8)                                   :: eps_slope=1d-15
    real(8),  dimension(:,:,:),   allocatable :: rho
    real(8),  dimension(:,:,:),   allocatable :: cp

    
    allocate(rho(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
    allocate(cp(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
    rho=1
    cp=1



    ! construction du terme de source smcon
    ! attention, il faut que CO_source_term=.true.

    do k=sz,ez
       do j=sy,ey
          do i=sx,ex

             smcon(i,j,k)=0   ! (cou(i+1,j,k)-cou(i-1,j,k))/(dxu(i)+dxu(i+1)) ! gradient x centre en i
             
          end do
       end do
    end do

    call Energy_Delta(u,v,w,con,rho,cou,slcon,smcon,spcon,  &
         & conin,con0,con1,difu,difv,difw,cp,               &
         & pencon,penu,penv,penw,energy,                    &
         & CO_source_term,CO_linear_term,CO_penalty_term,   &
         & CO_inertial_scheme,                              &
         & CO_solver_type,CO_solver_threshold,CO_solver_it, &
         & CO_solver_precond,impp_concentration)

    deallocate(rho,cp)
    

  end subroutine Concentration_Delta
  


  !*****************************************************************************************************
  subroutine CO_print (nt,concentration)
    !*****************************************************************************************************
    use mod_Parameters, only: rank,CO_solver_type
    use mod_struct_energy
    !-------------------------------------------------------------------------------
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)             :: nt
    type(struct_energy), intent(in) :: concentration
    !-------------------------------------------------------------------------------
    
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer :: l
    !-------------------------------------------------------------------------------

    if (rank==0) then 
       write(*,*) ''//achar(27)//'[36m======================Concentration============================'//achar(27)//'[0m'
       !write(*,*) 'Cummulative Time step',nt
       if (CO_solver_type==1) then 
          write(*,101) ' BiCGStab II iterations: ',concentration%output%it_solver(1)
          write(*,102) ' BiCGStab II residual: ',concentration%output%res_solver(1)
       else if(CO_solver_type==2) then
          write(*,102) ' MUMPS residual: ',concentration%output%res_solver(1)
       end if
    end if
    
101 format(a,1x,i3)
102 format(a,1x,1pe15.8)

    !*****************************************************************************************************
  end subroutine CO_print
  !*****************************************************************************************************


  subroutine CO_res(dt,con,con0)
    use mod_mpi
    use mod_Parameters, only: rank, &
         & dim,sx,ex,sy,ey,sz,ez
    !-------------------------------------------------------------------------------
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), intent(in)                                :: dt
    real(8), dimension(:,:,:), allocatable, intent(in) :: con,con0
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    real(8)                                            :: deltacon,normcon
    real(8)                                            :: ldeltacon,lnormcon
    real(8)                                            :: maxcon,mincon
    real(8)                                            :: val
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! con increment 
    !-------------------------------------------------------------------------------
    ldeltacon=maxval(abs(con(sx:ex,sy:ey,sz:ez)-con0(sx:ex,sy:ey,sz:ez)))
    lnormcon=maxval(abs(con(sx:ex,sy:ey,sz:ez)))
    
    call mpi_allreduce(ldeltacon,deltacon,1,mpi_double_precision,mpi_max,comm3d,code)
    call mpi_allreduce(lnormcon,normcon,1,mpi_double_precision,mpi_max,comm3d,code)
    
    if (rank==0) then
       write(*,101) 'Max(|con-con0|)/Max(|con|)/dt=', deltacon/normcon/dt
       write(*,101) 'Max(|con-con0|)/dt=', deltacon/dt
    end if
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Maximum values
    !-------------------------------------------------------------------------------
    val=maxval(con(sx:ex,sy:ey,sz:ez))
    call mpi_allreduce(val,maxcon,1,mpi_double_precision,mpi_max,comm3d,code)
    val=minval(con(sx:ex,sy:ey,sz:ez))
    call mpi_allreduce(val,mincon,1,mpi_double_precision,mpi_min,comm3d,code)

    if (rank==0) then
       write(*,102) 'Max(con)=',maxcon,'| Min(con)=',mincon
    end if
    !-------------------------------------------------------------------------------

101 format(a,1pe15.8)
102 format(999(a,1pe12.5,1x))
    
  end subroutine CO_res
  
    
  !*****************************************************************************************************
  subroutine CO_time(con,con0,con1,nt)
    !*****************************************************************************************************
    use mod_Parameters, only: Euler
    !-------------------------------------------------------------------------------
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)                                   :: nt
    real(8), dimension(:,:,:), allocatable, intent(inout) :: con,con0,con1
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------
    
    if (abs(Euler)==2) con1=con0
    con0=con 
    
    return
    
    !*****************************************************************************************************
  end subroutine CO_time
  !*****************************************************************************************************

end module mod_concentration



