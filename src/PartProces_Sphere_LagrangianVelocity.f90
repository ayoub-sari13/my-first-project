!===============================================================================
module Bib_VOFLag_Sphere_LagrangianVelocity
  !===============================================================================
  contains
  !-----------------------------------------------------------------------------  
  !> @author jorge cesar brandle de motta, amine chadil
  ! 
  !> @brief cette routine permet 
  !
  !> @param[in]  u,v,w             : vitesse au temps n+1.
  !> @param[in,out] vl             : la structure contenant toutes les informations
  !! relatives aux particules.
  !-----------------------------------------------------------------------------
  subroutine Sphere_LagrangianVelocity(u,v,w,vl,ndim,grid_x,grid_y,&
                                   grid_z,grid_xu,grid_yv,grid_zw,dx,dy,dz,dxu,dyv,dzw)
    !===============================================================================
    !modules
    !===============================================================================
    !-------------------------------------------------------------------------------
    !Modules de definition des structures et variables => src/mod/module_...f90
    !-------------------------------------------------------------------------------
    use Module_VOFLag_ParticlesDataStructure
    use Module_VOFLag_SubDomain_Data,        only : m_period
    use mod_Parameters,                      only : deeptracking,xmin,xmax,ymin,ymax,&
                                                    zmin,zmax
    use mod_mpi
    !-------------------------------------------------------------------------------
    !Modules de subroutines de la librairie RESPECT => src/Bib_VOFLag_...f90
    !-------------------------------------------------------------------------------
    use Bib_VOFLag_SubDomain_Limited_PointIn
    use Bib_VOFLag_VelocityInterpolationFromEulerianMesh2Point
    !-------------------------------------------------------------------------------
    !===============================================================================
    !declarations des variables
    !===============================================================================
    implicit none
    !-------------------------------------------------------------------------------
    !variables globales
    !-------------------------------------------------------------------------------
    type(struct_vof_lag),                   intent(inout) :: vl
    real(8), dimension(:,:,:), allocatable, intent(in)    :: u,v,w
    integer,                                intent(in)    :: ndim
    
    !-------------------------------------------------------------------------------
    !variables locales 
    !-------------------------------------------------------------------------------
    real(8), dimension (2*ndim,3,vl%kpt)                  :: vts_all_points
    real(8), dimension(3)                                 :: point,vts_point,coord_deb,&
                                                             coord_fin
    real(8)                                               :: vals
    integer                                               :: nd,np,i
    logical                                               :: point_in_domain_int
    real(8), dimension(:),       allocatable, intent(in)  :: grid_xu,grid_yv, grid_zw
    real(8), dimension(:),       allocatable, intent(in)  :: grid_x,grid_y, grid_z
    real(8), dimension(:),       allocatable, intent(in)  :: dx,dy,dz
    real(8), dimension(:),       allocatable, intent(in)  :: dxu,dyv,dzw

    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'entree Sphere_LagrangianVelocity'
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Initialisation
    !-------------------------------------------------------------------------------
    coord_deb(1)=xmin
    coord_deb(2)=ymin
    coord_fin(1)=xmax
    coord_fin(2)=ymax
    if (ndim.eq.3) then
      coord_deb(3)=zmin
      coord_fin(3)=zmax
    endif

    !-------------------------------------------------------------------------------
    !Calcul des vitesses des points fictifs a l'interieur des spheres
    !-------------------------------------------------------------------------------
    vts_all_points = 0.0d0
    do np=1,vl%kpt
      if (vl%objet(np)%on) then
        do nd = 1,ndim
          do i = 0,1
            point = 0.0d0
            vts_point = 0.0d0 
            !-------------------------------------------------------------------------------
            !Construction des points d'interpolation
            !-------------------------------------------------------------------------------
            point = vl%objet(np)%pos
            point(nd) = point(nd) + 0.5d0*(2*i-1)*vl%objet(np)%sca(1)

            !-------------------------------------------------------------------------------
            !Gestion de la periodicite
            !-------------------------------------------------------------------------------
            if (m_period(nd)) then
              if ( ( point(nd) .gt. coord_fin(nd) ) ) then  
                point(nd) =  point(nd) - (coord_fin(nd) - coord_deb(nd) )
              else if (  point(nd) .lt. coord_deb(nd)) then
                point(nd) =   point(nd) + (coord_fin(nd) - coord_deb(nd) )
              end if
            end if

            !-------------------------------------------------------------------------------
            !Interpolation de la vitesse
            !-------------------------------------------------------------------------------
            call SubDomain_Limited_PointIn(point,point_in_domain_int)
            if (point_in_domain_int) then
              call VelocityInterpolationFromEulerianMesh2Point(point,u,v,w,vts_point,ndim,grid_x,&
	      grid_y,grid_z,grid_xu,grid_yv,grid_zw,dx,dy,dz,dxu,dyv,dzw)
              vts_all_points(2*nd+i-1,1:ndim,np) =  vts_point(1:ndim)
	     !print*,'point:',point,'vitesse',vts_point(1:ndim) 
	     !print*, 'nd=',nd,'i=',i,' ',2*nd+i-1
            end if
          end do
        end do
      end if
      !do nd=1,ndim
      !  do i = 1,2*ndim 
      !    vals=vts_all_points(i,nd,np)
      !    call mpi_allreduce(vals,vts_all_points(i,nd,np),1,mpi_double_precision,mpi_sum,comm3d,code)
      !  end do
      !end do
    end do
     
    !-------------------------------------------------------------------------------
    !Actualisation des vitesses de translation: vn et v
    !-------------------------------------------------------------------------------
    if (vl%deplacement) then 
      do np=1,vl%kpt
        !-------------------------------------------------------------------------------
        !Initialisation des vitesses vn et v
        !-------------------------------------------------------------------------------
        vl%objet(np)%vn = vl%objet(np)%v
        vl%objet(np)%v  = 0.0d0
        do nd=1,ndim
          !-------------------------------------------------------------------------------
          !Calcul de la vitesse de la sphere a l'instant n+1
          !-------------------------------------------------------------------------------
          do i = 1,2*ndim 
            vl%objet(np)%v(nd) =  vl%objet(np)%v(nd) + vts_all_points(i,nd,np) / ( 2 * ndim+1D-40 )
	  end do
        end do
      end do
    end if
    !-------------------------------------------------------------------------------
    !calcul de la vitesse angulaire
    !-------------------------------------------------------------------------------
    if (vl%vrot) then
      do np=1,vl%kpt
        vl%objet(np)%omeg =   0d0
      end do

      do np=1,vl%kpt
        if (vl%objet(np)%on) then
          vl%objet(np)%omeg(3) =    ( (vts_all_points(3,1,np)-vts_all_points(4,1,np) ) - &
                (vts_all_points(1,2,np)-vts_all_points(2,2,np)) ) / ( 2.0d0*vl%objet(np)%sca(1)+1D-40 )

          if (ndim .eq. 3) then
            vl%objet(np)%omeg(1) = ( (vts_all_points(4,3,np)-vts_all_points(3,3,np)) -  &
                 (vts_all_points(6,2,np)-vts_all_points(5,2,np)) ) / ( 2.0d0*vl%objet(np)%sca(1)+1D-40 )
            vl%objet(np)%omeg(2) = ( (vts_all_points(6,1,np)-vts_all_points(5,1,np)) -  &
                 (vts_all_points(2,3,np)-vts_all_points(1,3,np)) ) / ( 2.0d0*vl%objet(np)%sca(1)+1D-40 )
          end if
        end if
      end do
      !do np=1,vl%kpt
      !  do nd=1,3
      !    call mpi_bcast(vl%objet(np)%omeg(nd),1,mpi_double_precision,vl%objet(np)%locpart,comm3d,code)
       ! end do
      !end do
    end if
    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'sortie Sphere_LagrangianVelocity'
    !-------------------------------------------------------------------------------
  end subroutine Sphere_LagrangianVelocity

  !===============================================================================
end module Bib_VOFLag_Sphere_LagrangianVelocity
!===============================================================================
