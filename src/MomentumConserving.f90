!========================================================================
!**
!**   NAME       : Momentumconserving.f90
!**
!**   AUTHOR     : M. Elouafa
!**                B. Trouette
!**                S. Fereka
!**              
!**   FUNCTION   : iterative and direct solver modules of FUGU software
!**
!**   DATES      : Version 1.0.0  : from : April 22
!**
!========================================================================

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


module mod_MomentumConserving

  INTEGER, PARAMETER :: EPS_INTERPOLATE_HR = 1d-13
  
contains
  
  subroutine MomentumConserving(dt,u,v,w,u0,v0,w0,smvu,smvv,smvw, &
       & rovu,rovv,rovw,rovu0,rovv0,rovw0,uadv2,vadv2,wadv2)
    use mod_Parameters, only: dim,mesh,   &
         & gsx,gex,gsy,gey,gsz,gez,       &
         & sxu,exu,syu,eyu,szu,ezu,       &
         & sxv,exv,syv,eyv,szv,ezv,       &
         & sxw,exw,syw,eyw,szw,ezw,       &
         & NS_MomentumConserving,         &
         & NS_MomentumConserving_scheme,  &
         & NS_MomentumConserving_velocity
    use mod_Constants, only: d1p2,d3p2,d1p3,d2p3,d3p4,d1p4
    use mod_allocate
    use mod_mpi
    use mod_Borders
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8)                                               :: dt
    real(8), dimension(:,:,:), allocatable, intent(in)    :: u,v,w
    real(8), dimension(:,:,:), allocatable, intent(in)    :: u0,v0,w0
    real(8), dimension(:,:,:), allocatable, intent(inout) :: smvu,smvv,smvw
    real(8), dimension(:,:,:), allocatable, intent(inout) :: rovu,rovv,rovw
    real(8), dimension(:,:,:), allocatable, intent(inout) :: rovu0,rovv0,rovw0
    real(8), dimension(:,:,:), allocatable, intent(inout) :: uadv2,vadv2,wadv2
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable   :: ru,rv,rw
    real(8), dimension(:,:,:), allocatable   :: momu,momv,momw
    real(8), dimension(:,:,:), allocatable   :: momu0,momv0,momw0
    real(8), dimension(:,:,:), allocatable   :: momu1,momv1,momw1
    real(8), dimension(:,:,:), allocatable   :: momu2,momv2,momw2
    real(8), dimension(:,:,:), allocatable   :: uadv1,vadv1,wadv1
    real(8), dimension(:,:,:), allocatable   :: rovu1,rovv1,rovw1
    real(8), dimension(:,:,:), allocatable   :: rovu2,rovv2,rovw2
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Alloc
    !-------------------------------------------------------------------------------
    call allocate_array_3(ALLOCATE_ON_U_MESH,ru,mesh)
    call allocate_array_3(ALLOCATE_ON_V_MESH,rv,mesh)
    call allocate_array_3(ALLOCATE_ON_W_MESH,rw,mesh)

    call allocate_array_3(ALLOCATE_ON_U_MESH,uadv1,mesh)
    call allocate_array_3(ALLOCATE_ON_V_MESH,vadv1,mesh)
    call allocate_array_3(ALLOCATE_ON_W_MESH,wadv1,mesh)

    call allocate_array_3(ALLOCATE_ON_U_MESH,momu,mesh)
    call allocate_array_3(ALLOCATE_ON_V_MESH,momv,mesh)
    call allocate_array_3(ALLOCATE_ON_W_MESH,momw,mesh)
    call allocate_array_3(ALLOCATE_ON_U_MESH,momu0,mesh)
    call allocate_array_3(ALLOCATE_ON_V_MESH,momv0,mesh)
    call allocate_array_3(ALLOCATE_ON_W_MESH,momw0,mesh)
    call allocate_array_3(ALLOCATE_ON_U_MESH,momu1,mesh)
    call allocate_array_3(ALLOCATE_ON_V_MESH,momv1,mesh)
    call allocate_array_3(ALLOCATE_ON_W_MESH,momw1,mesh)
    call allocate_array_3(ALLOCATE_ON_U_MESH,momu2,mesh)
    call allocate_array_3(ALLOCATE_ON_V_MESH,momv2,mesh)
    call allocate_array_3(ALLOCATE_ON_W_MESH,momw2,mesh)

    call allocate_array_3(ALLOCATE_ON_U_MESH,rovu1,mesh)
    call allocate_array_3(ALLOCATE_ON_V_MESH,rovv1,mesh)
    call allocate_array_3(ALLOCATE_ON_W_MESH,rovw1,mesh)
    call allocate_array_3(ALLOCATE_ON_U_MESH,rovu2,mesh)
    call allocate_array_3(ALLOCATE_ON_V_MESH,rovv2,mesh)
    call allocate_array_3(ALLOCATE_ON_W_MESH,rovw2,mesh)

    !-------------------------------------------------------------------------------
    ! 1st step RK 3
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Fluxes (general)
    !-------------------------------------------------------------------------------
    call compute_Convective_term(Ru,Rv,Rw,u,v,w,rovu0,rovv0,rovw0)
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! UVW-components
    !-------------------------------------------------------------------------------
    rovu1=rovu0-dt*ru
    rovv1=rovv0-dt*rv
    if (dim==3) rovw1=rovw0-dt*rw
    !-------------------------------------------------------------------------------
    call uvw_borders_and_periodicity(mesh,rovu1,rovv1,rovw1)
    call comm_mpi_uvw(rovu1,rovv1,rovw1)
    !-------------------------------------------------------------------------------
    if (NS_MomentumConserving_scheme<0) then
       momu0=rovu0*u
       momv0=rovv0*v
       if (dim==3) momw0=rovw0*w
       !-------------------------------------------------------------------------------
       ! Fluxes (general)
       !-------------------------------------------------------------------------------
       call compute_Convective_term(Ru,Rv,Rw,u,v,w,momu0,momv0,momw0)
       !-------------------------------------------------------------------------------

       !-------------------------------------------------------------------------------
       ! UVW-components
       !-------------------------------------------------------------------------------
       momu1=momu0-dt*ru
       momv1=momv0-dt*rv
       if (dim==3) momw1=momw0-dt*rw
       !-------------------------------------------------------------------------------
       call uvw_borders_and_periodicity(mesh,momu1,momv1,momw1)
       call comm_mpi_uvw(momu1,momv1,momw1)
    endif

    !-------------------------------------------------------------------------------
    ! 2nd step RK 3
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! advection velocity extrapolation
    ! pp. 14, Eq. (43)  paper https://arxiv.org/pdf/1809.01008.pdf
    !-------------------------------------------------------------------------------
    uadv1=2*u-u0
    vadv1=2*v-v0
    if (dim==3) wadv1=2*w-w0
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Fluxes (general)
    !-------------------------------------------------------------------------------
    call Compute_Convective_Term(Ru,Rv,Rw,uadv1,vadv1,wadv1,rovu1,rovv1,rovw1)
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! UVW-components
    !-------------------------------------------------------------------------------
    rovu2=d3p4*rovu0+d1p4*rovu1-d1p4*dt*ru
    rovv2=d3p4*rovv0+d1p4*rovv1-d1p4*dt*rv        
    if (dim==3) rovw2=d3p4*rovw0+d1p4*rovw1-d1p4*dt*rw        
    !-------------------------------------------------------------------------------
    call uvw_borders_and_periodicity(mesh,rovu2,rovv2,rovw2)
    call comm_mpi_uvw(rovu2,rovv2,rovw2)
    !-------------------------------------------------------------------------------
    if (NS_MomentumConserving_scheme<0) then
       !-------------------------------------------------------------------------------
       ! Fluxes (general)
       !-------------------------------------------------------------------------------
       call Compute_Convective_Term(Ru,Rv,Rw,uadv1,vadv1,wadv1,momu1,momv1,momw1)
       !-------------------------------------------------------------------------------

       !-------------------------------------------------------------------------------
       ! UVW-components
       !-------------------------------------------------------------------------------
       momu2=d3p4*momu0+d1p4*momu1-d1p4*dt*ru
       momv2=d3p4*momv0+d1p4*momv1-d1p4*dt*rv        
       if (dim==3) momw2=d3p4*momw0+d1p4*momw1-d1p4*dt*rw        
       !-------------------------------------------------------------------------------
       call uvw_borders_and_periodicity(mesh,momu2,momv2,momw2)
       call comm_mpi_uvw(momu2,momv2,momw2)
    end if

    !-------------------------------------------------------------------------------
    ! 3th step RK 3
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! advection velocity extrapolation
    ! pp. 14, Eq. (44)  paper https://arxiv.org/pdf/1809.01008.pdf
    !-------------------------------------------------------------------------------
    uadv2=d3p2*u-d1p2*u0
    vadv2=d3p2*v-d1p2*v0
    if (dim==3) wadv2=d3p2*w-d1p2*w0
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Fluxes (general)
    !-------------------------------------------------------------------------------
    call Compute_Convective_Term(ru,rv,rw,uadv2,vadv2,wadv2,rovu2,rovv2,rovw2)
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! UVW-components
    !-------------------------------------------------------------------------------
    rovu=d1p3*rovu0+d2p3*rovu2-d2p3*dt*ru
    rovv=d1p3*rovv0+d2p3*rovv2-d2p3*dt*rv
    if (dim==3) rovw=d1p3*rovw0+d2p3*rovw2-d2p3*dt*rw
    !-------------------------------------------------------------------------------
    if (NS_MomentumConserving_scheme>0) then 
       momu=rovu2*uadv2
       momv=rovv2*vadv2
       if (dim==3) momw=rovw2*wadv2
    end if
    !-------------------------------------------------------------------------------
    call uvw_borders_and_periodicity(mesh,rovu,rovv,rovw)
    call comm_mpi_uvw(rovu,rovv,rovw)
    !------------------------------------------------------------------------------- 

    !-------------------------------------------------------------------------------
    if (NS_MomentumConserving_scheme<0) then
       !-------------------------------------------------------------------------------
       ! Fluxes (general)
       !-------------------------------------------------------------------------------
       call Compute_Convective_Term(ru,rv,rw,uadv2,vadv2,wadv2,momu2,momv2,momw2)
       !-------------------------------------------------------------------------------
       
       !-------------------------------------------------------------------------------
       ! UVW-components
       !-------------------------------------------------------------------------------
       momu=d1p3*momu0+d2p3*momu2-d2p3*dt*ru
       momv=d1p3*momv0+d2p3*momv2-d2p3*dt*rv
       if (dim==3) then
          momw=d1p3*momw0+d2p3*momw2-d2p3*dt*rw
       end if
       !-------------------------------------------------------------------------------
    endif

    !------------------------------------------------------------------------------- 
    ! Mom periodicity both cases !!!
    !------------------------------------------------------------------------------- 
    call uvw_borders_and_periodicity(mesh,momu,momv,momw)
    call comm_mpi_uvw(momu,momv,momw)
    !------------------------------------------------------------------------------- 

    !-------------------------------------------------------------------------------
    ! END Advection face-centered density and momentum
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Compute convective term for NS equations
    !-------------------------------------------------------------------------------
    smvu=0
    smvv=0
    if (dim==3) smvw=0
#if 1
    if (NS_MomentumConserving_scheme>0) then
       !-------------------------------------------------------------------------------
       call Compute_Convective_Term(Ru,Rv,Rw,uadv2,vadv2,wadv2,momu,momv,momw)
       !-------------------------------------------------------------------------------
       ! UVW velocity components
       !-------------------------------------------------------------------------------
       smvu = smvu - ru + rovu0*u/dt
       smvv = smvv - rv + rovv0*v/dt
       if (dim==3) smvw = smvw - rw + rovw0*w/dt
       !-------------------------------------------------------------------------------
    else
       smvu = smvu + momu/dt
       smvv = smvv + momv/dt
       if (dim==3)  smvw = smvw + momw/dt
    endif
#else
    call Compute_Convective_Term(Ru,Rv,Rw,uadv2,vadv2,wadv2,momu,momv,momw)
    !-------------------------------------------------------------------------------
    ! UVW velocity components
    !-------------------------------------------------------------------------------
    smvu = smvu - ru
    smvv = smvv - rv
    if (dim==3) smvw = smvw - rw
#endif
    !-------------------------------------------------------------------------------
    
    !-------------------------------------------------------------------------------
    ! velocity for VOF ... 
    !-------------------------------------------------------------------------------
    select case(NS_MomentumConserving_velocity)
    case(1)
       !-------------------------------------------------------------------------------
       ! u^n
       !-------------------------------------------------------------------------------
       uadv2=u
       vadv2=v
       if (dim==3) wadv2=w
       !-------------------------------------------------------------------------------
    case(2)
       !-------------------------------------------------------------------------------
       ! u^(2) from scheme
       !-------------------------------------------------------------------------------
       ! done in previous lines
       !-------------------------------------------------------------------------------
    case(3)
       !-------------------------------------------------------------------------------
       ! from momentum
       !-------------------------------------------------------------------------------
       uadv2=momu/rovu
       vadv2=momv/rovv
       if (dim==3) wadv2=momw/rovw
       !-------------------------------------------------------------------------------
    case default
       stop "select 1,2,3 for NS_MomentumConserving_velocity"
    end select
    !-------------------------------------------------------------------------------

    deallocate(Ru,Rv,uadv1,vadv1,     &
         & momu,momv,momu0,momv0,   &
         & momu1,momv1,momu2,momv2, &
         & rovu1,rovv1,rovu2,rovv2)
    if (dim==3) deallocate(Rw,wadv1,momw,momw0,momw1,momw2,rovw1,rovw2)

  end subroutine MomentumConserving





  subroutine Compute_Convective_Term(ru,rv,rw,u,v,w,rovu,rovv,rovw)
    use mod_Parameters, only: dim
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable,  intent(in)    :: u,v,w
    real(8), dimension(:,:,:), allocatable,  intent(in)    :: rovu,rovv,rovw
    real(8), dimension(:,:,:), allocatable,  intent(inout) :: Ru,Rv,Rw
    !-------------------------------------------------------------------------------

    ru=0
    rv=0
    if (dim==2) then
       call Compute_Convective_Term2D(ru,rv,u,v,rovu,rovv)
    else
       rw=0
       call Compute_Convective_Term3D(ru,rv,rw,u,v,w,rovu,rovv,rovw)
    end if
  end subroutine Compute_Convective_Term


  subroutine Compute_Convective_Term2D(Ru,Rv,u,v,rovu,rovv)
    use mod_Parameters, only: nproc,dim,                &
         & dx,dy,dz,ddx,ddy,ddz,                        &
         & dxu,dyv,dzw,dxu2,dyv2,dzw2,ddxu,ddyv,ddzw,   &
         & sx,ex,sy,ey,sz,ez,                           &
         & gsx,gex,gsy,gey,gsz,gez,                     &
         & sxu,exu,syu,eyu,szu,ezu,                     &
         & sxv,exv,syv,eyv,szv,ezv,                     &
         & sxw,exw,syw,eyw,szw,ezw,                     &
         & sxs,exs,sys,eys,szs,ezs,                     &
         & sxus,exus,syus,eyus,szus,ezus,               &
         & sxvs,exvs,syvs,eyvs,szvs,ezvs,               &
         & sxws,exws,syws,eyws,szws,ezws,               &
         & gsxu,gexu,gsyu,geyu,gszu,gezu,               &
         & gsxv,gexv,gsyv,geyv,gszv,gezv,               &
         & gsxw,gexw,gsyw,geyw,gszw,gezw,               &
         & gx,gy,gz,dt,                                 &
         & NS_method,                                   &
         & NS_linear_term,NS_inertial_scheme,PJ_method, &
         & AL_comp,AL_method,AL_dr,                     &
         & grid_x,grid_xu,grid_y,grid_yv,grid_z,grid_zw,&
         & NS_MomentumConserving_scheme
    use mod_Constants, only: zero,one,d1p2
    use mod_mpi
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable,  intent(in)    :: u,v,rovu,rovv
    real(8), dimension(:,:,:), allocatable,  intent(inout) :: Ru,Rv
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                                :: i,j,k
    integer                                                :: icase
    real(8)                                                :: ue,uw,vn,vs
    real(8)                                                :: psiu,psic,psid,psie,psiw,psis,psin
    real(8)                                                :: fm,fp,fx,fy,phi
    real(8)                                                :: xC,xU,xD,xFa
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! init 
    !-------------------------------------------------------------------------------
    ue=0;uw=0
    vn=0;vs=0
    psiu=0
    psic=0
    psid=0
    psie=0
    psiw=0
    psis=0
    psin=0
    fm=0;fp=0
    fx=0;fy=0
    phi=0
    !-------------------------------------------------------------------------------

    select case(abs(NS_MomentumConserving_scheme))
       !-------------------------------------------------------------------------------
       ! 1 --> Cubic Upwind Interpolate (3th order)
       ! 2 --> WENO5 Conservative
       !-------------------------------------------------------------------------------
    case(1)
       !-------------------------------------------------------------------------------
       ! U component 
       !-------------------------------------------------------------------------------
       do j=syu,eyu
          do i=sxu,exu

             ue=0.5d0 * ( u(i,j,1) + u(i+1,j,1) )
             if(ue>0) then
                psiu=rovu(i-1,j,1)
                psic=rovu(i,j,1)
                psid=rovu(i+1,j,1)
             else
                psid=rovu(i,j,1)
                psic=rovu(i+1,j,1)
                psiu=rovu(i+2,j,1)
             endif

             call interpolate_cui_hr_quantity(psiu,psic,psid,psie)

             uw=0.5d0 * ( u(i-1,j,1) + u(i,j,1) )
             if(uw>0) then
                psiu=rovu(i-2,j,1)
                psic=rovu(i-1,j,1)
                psid=rovu(i,j,1)
             else
                psid=rovu(i-1,j,1)
                psic=rovu(i,j,1)
                psiu=rovu(i+1,j,1)
             endif

             call interpolate_cui_hr_quantity(psiu,psic,psid,psiw)

             vn=0.5d0 * ( v(i-1,j+1,1) + v(i,j+1,1) )
             if(vn>0) then
                psiu=rovu(i,j-1,1)
                psic=rovu(i,j,1)
                psid=rovu(i,j+1,1)
             else
                psid=rovu(i,j,1)
                psic=rovu(i,j+1,1)
                psiu=rovu(i,j+2,1)
             endif

             call interpolate_cui_hr_quantity(psiu,psic,psid,psin)

             vs=0.5d0 * ( v(i-1,j,1) + v(i,j,1) )
             if(vs>0) then
                psiu=rovu(i,j-2,1)
                psic=rovu(i,j-1,1)
                psid=rovu(i,j,1)
             else
                psid=rovu(i,j-1,1)
                psic=rovu(i,j,1)
                psiu=rovu(i,j+1,1)
             endif

             call interpolate_cui_hr_quantity(psiu,psic,psid,psis)

             Ru(i,j,1)=(ue*psie-uw*psiw)/dxu(i)+(vn*psin-vs*psis)/dy(j) 

          end do
       end do
       !-------------------------------------------------------------------------------
       ! V component 
       !-------------------------------------------------------------------------------
       do j=syv,eyv
          do i=sxv,exv

             ue=0.5d0 * ( u(i+1,j,1) + u(i+1,j-1,1) )
             if(ue>0) then
                psiu=rovv(i-1,j,1)
                psic=rovv(i,j,1)
                psid=rovv(i+1,j,1)
             else
                psid=rovv(i,j,1)
                psic=rovv(i+1,j,1)
                psiu=rovv(i+2,j,1)
             endif

             call interpolate_cui_hr_quantity(psiu,psic,psid,psie)

             uw=0.5d0 * ( u(i,j-1,1) + u(i,j,1) )
             if(uw>0) then
                psiu=rovv(i-2,j,1)
                psic=rovv(i-1,j,1)
                psid=rovv(i,j,1)
             else
                psid=rovv(i-1,j,1)
                psic=rovv(i,j,1)
                psiu=rovv(i+1,j,1)
             endif

             call interpolate_cui_hr_quantity(psiu,psic,psid,psiw)

             vn=0.5d0 * ( v(i,j,1) + v(i,j+1,1) )
             if(vn>0) then
                psiu=rovv(i,j-1,1)
                psic=rovv(i,j,1)
                psid=rovv(i,j+1,1)
             else
                psid=rovv(i,j,1)
                psic=rovv(i,j+1,1)
                psiu=rovv(i,j+2,1)
             endif

             call interpolate_cui_hr_quantity(psiu,psic,psid,psin)

             vs=0.5d0 * ( v(i,j,1) + v(i,j-1,1) )
             if(vs>0) then
                psiu=rovv(i,j-2,1)
                psic=rovv(i,j-1,1)
                psid=rovv(i,j,1)
             else
                psid=rovv(i,j-1,1)
                psic=rovv(i,j,1)
                psiu=rovv(i,j+1,1)
             endif

             call interpolate_cui_hr_quantity(psiu,psic,psid,psis)

             Rv(i,j,1)=(ue*psie-uw*psiw)/dx(i)+(vn*psin-vs*psis)/dyv(j)

          end do
       end do

    case(2)

       !-------------------------------------------------------------------------------
       ! U Component
       !-------------------------------------------------------------------------------
       do j=syu,eyu
          do i=sxu,exu

             !---------------------------------------
             ! x-direction
             !---------------------------------------
             ue = 0.5d0 * ( u(i,j,1) + u(i+1,j,1) )
             if(ue>0) then
                call sweno5_shala(phi, &
                     & rovu(i-2,j,1), &
                     & rovu(i-1,j,1), &
                     & rovu(i+0,j,1), &
                     & rovu(i+1,j,1), &
                     & rovu(i+2,j,1))
             else
                call sweno5_shala(phi, &
                     & rovu(i+3,j,1), &
                     & rovu(i+2,j,1), &
                     & rovu(i+1,j,1), &
                     & rovu(i+0,j,1), &
                     & rovu(i-1,j,1))
             endif
             fp=ue*phi

             uw = 0.5d0 * ( u(i-1,j,1) + u(i,j,1) )
             if (uw>zero) then
                call sweno5_shala(phi,   &
                     & rovu(i-3,j,1), &
                     & rovu(i-2,j,1), &
                     & rovu(i-1,j,1), &
                     & rovu(i+0,j,1), &
                     & rovu(i+1,j,1))
             else
                call sweno5_shala(phi,   &
                     & rovu(i+2,j,1), &
                     & rovu(i+1,j,1), &
                     & rovu(i+0,j,1), &
                     & rovu(i-1,j,1), &
                     & rovu(i-2,j,1))
             end if
             fm=uw*phi

             fx=(fp-fm)

             !---------------------------------------
             ! y-direction
             !---------------------------------------
             vn = 0.5d0 * ( v(i-1,j+1,1) + v(i,j+1,1) )
             if (vn>zero) then
                call sweno5_shala(phi, &
                     & rovu(i,j-2,1), &
                     & rovu(i,j-1,1), &
                     & rovu(i,j+0,1), &
                     & rovu(i,j+1,1), &
                     & rovu(i,j+2,1))
             else
                call sweno5_shala(phi,   &
                     & rovu(i,j+3,1), &
                     & rovu(i,j+2,1), &
                     & rovu(i,j+1,1), &
                     & rovu(i,j+0,1), &
                     & rovu(i,j-1,1))
             end if
             fp=vn*phi

             vs = 0.5d0 * ( v(i-1,j,1) + v(i,j,1) )
             if (vs>zero) then
                call sweno5_shala(phi,   &
                     & rovu(i,j-3,1), &
                     & rovu(i,j-2,1), &
                     & rovu(i,j-1,1), &
                     & rovu(i,j+0,1), &
                     & rovu(i,j+1,1))
             else
                call sweno5_shala(phi,   &
                     & rovu(i,j+2,1), &
                     & rovu(i,j+1,1), &
                     & rovu(i,j+0,1), &
                     & rovu(i,j-1,1), &
                     & rovu(i,j-2,1))
             end if
             fm = vs*phi

             fy=(fp-fm)

             Ru(i,j,1) =  fx/dxu(i) + fy/dy(j) 

          end do
       end do

       !-------------------------------------------------------------------------------
       ! V Component
       !-------------------------------------------------------------------------------

       do j=syv,eyv
          do i=sxv,exv

             !---------------------------------------
             ! x-direction
             !---------------------------------------
             ue = 0.5d0 * ( u(i+1,j,1) + u(i+1,j-1,1) )
             if(ue>0) then
                call sweno5_shala(phi,   &
                     & rovv(i-2,j,1), &
                     & rovv(i-1,j,1), &
                     & rovv(i+0,j,1), &
                     & rovv(i+1,j,1), &
                     & rovv(i+2,j,1))
             else
                call sweno5_shala(phi,   &
                     & rovv(i+3,j,1), &
                     & rovv(i+2,j,1), &
                     & rovv(i+1,j,1), &
                     & rovv(i+0,j,1), &
                     & rovv(i-1,j,1))
             endif
             fp=ue*phi

             uw = 0.5d0 * ( u(i,j-1,1) + u(i,j,1) )
             if (uw>zero) then
                call sweno5_shala(phi,   &
                     & rovv(i-3,j,1), &
                     & rovv(i-2,j,1), &
                     & rovv(i-1,j,1), &
                     & rovv(i+0,j,1), &
                     & rovv(i+1,j,1))
             else
                call sweno5_shala(phi,   &
                     & rovv(i+2,j,1), &
                     & rovv(i+1,j,1), &
                     & rovv(i+0,j,1), &
                     & rovv(i-1,j,1), &
                     & rovv(i-2,j,1))
             end if
             fm=uw*phi

             fx=(fp-fm)

             !---------------------------------------
             ! y-direction
             !---------------------------------------
             vn = 0.5d0 * ( v(i,j,1) + v(i,j+1,1) )
             if (vn>zero) then
                call sweno5_shala(phi, &
                     & rovv(i,j-2,1), &
                     & rovv(i,j-1,1), &
                     & rovv(i,j+0,1), &
                     & rovv(i,j+1,1), &
                     & rovv(i,j+2,1))
             else
                call sweno5_shala(phi, &
                     & rovv(i,j+3,1), &
                     & rovv(i,j+2,1), &
                     & rovv(i,j+1,1), &
                     & rovv(i,j+0,1), &
                     & rovv(i,j-1,1))
             end if
             fp =vn*phi

             vs = 0.5d0 * ( v(i,j,1) + v(i,j-1,1) )
             if (vs>zero) then
                call sweno5_shala(phi, &
                     & rovv(i,j-3,1), &
                     & rovv(i,j-2,1), &
                     & rovv(i,j-1,1), &
                     & rovv(i,j+0,1), &
                     & rovv(i,j+1,1))
             else
                call sweno5_shala(phi,&
                     & rovv(i,j+2,1), &
                     & rovv(i,j+1,1), &
                     & rovv(i,j+0,1), &
                     & rovv(i,j-1,1), &
                     & rovv(i,j-2,1))
             end if
             fm=vs*phi

             fy=(fp-fm)

             Rv(i,j,1) = fx /dx(i) + fy/dyv(j)

          end do
       end do

    case(3:8)
       icase=abs(NS_MomentumConserving_scheme)-2
       
       !-------------------------------------------------------------------------------
       ! U component 
       !-------------------------------------------------------------------------------
       do j=syu,eyu+1
          do i=sxu,exu+1

             ue =(dxu2(i  )*u(i+1,j,1)+dxu2(i+1)*u(i  ,j,1))/dx(i  )
             if(ue>0) then
                psiu=rovu(i-1,j,1)
                xU=grid_xu(i-1)

                psic=rovu(i,j,1)
                xC=grid_xu(i)

                psid=rovu(i+1,j,1)
                xD=grid_xu(i+1)

                xFa=grid_x(i)
             else
                psid=rovu(i,j,1)
                xD=grid_xu(i)
                
                psic=rovu(i+1,j,1)
                xC=grid_xu(i+1)
                
                psiu=rovu(i+2,j,1)
                xU=grid_xu(i+2)
                
                xFa=grid_x(i)!
             endif
             
             call interpolate_HR(icase,psiu,psic,psid,psie,xC,xU,xD,xFa)

             uw =  (dxu2(i-1)*u(i  ,j,1)+dxu2(i  )*u(i-1,j,1))/dx(i-1)
             if(uw>0) then
                psiu=rovu(i-2,j,1)
                xU=grid_xu(i-2)

                psic=rovu(i-1,j,1)
                xC=grid_xu(i-1)

                psid=rovu(i,j,1)
                xD=grid_xu(i)

                xFa=grid_x(i-1)!
             else
                psid=rovu(i-1,j,1)
                xD=grid_xu(i-1)

                psic=rovu(i,j,1)
                xC=grid_xu(i)

                psiu=rovu(i+1,j,1)
                xU=grid_xu(i+1)

                xFa=grid_x(i-1)!
             endif

             call interpolate_HR(icase,psiu,psic,psid,psiw,xC,xU,xD,xFa)

             vn=0.5d0 * ( v(i-1,j+1,1) + v(i,j+1,1) )
             if(vn>0) then
                psiu=rovu(i,j-1,1)
                xU=grid_y(j-1)
                
                psic=rovu(i,j,1)
                xC=grid_y(j)

                psid=rovu(i,j+1,1)
                xD=grid_y(j+1)

                xFa=grid_yv(j+1)!
             else
                psid=rovu(i,j,1)
                xD=grid_y(j)
                
                psic=rovu(i,j+1,1)
                xC=grid_y(j+1)

                psiu=rovu(i,j+2,1)
                xU=grid_y(j+2)

                xFa=grid_yv(j+1)!
             endif

             call interpolate_HR(icase,psiu,psic,psid,psin,xC,xU,xD,xFa)

             vs=0.5d0 * ( v(i-1,j,1) + v(i,j,1) )
             if(vs>0) then
                psiu=rovu(i,j-2,1)
                xU=grid_y(j-2)

                psic=rovu(i,j-1,1)
                xC=grid_y(j-1)

                psid=rovu(i,j,1)
                xD=grid_y(j)

                xFa=grid_yv(j)!
             else
                psid=rovu(i,j-1,1)
                xD=grid_y(j-1)

                psic=rovu(i,j,1)
                xC=grid_y(j)

                psiu=rovu(i,j+1,1)
                xU=grid_y(j+1)
                xFa=grid_yv(j)!
             endif

             call interpolate_HR(icase,psiu,psic,psid,psis,xC,xU,xD,xFa)

             Ru(i,j,1)=(ue*psie-uw*psiw)/dxu(i)+(vn*psin-vs*psis)/dy(j) 
             
          end do
       end do
       !-------------------------------------------------------------------------------
       ! V component 
       !-------------------------------------------------------------------------------
       do j=syv,eyv
          do i=sxv,exv

             ue=0.5d0 * ( u(i+1,j,1) + u(i+1,j-1,1) )
             if(ue>0) then
                psiu=rovv(i-1,j,1)
                xU=grid_x(i-1)

                psic=rovv(i,j,1)
                xC=grid_x(i)

                psid=rovv(i+1,j,1)
                xD=grid_x(i+1)

                xFa=grid_xu(i+1)!
             else
                psid=rovv(i,j,1)
                xD=grid_x(i)

                psic=rovv(i+1,j,1)
                xC=grid_x(i+1)

                psiu=rovv(i+2,j,1)
                xU=grid_x(i+2)

                xFa=grid_xu(i+1)!

             endif

             call interpolate_HR(icase,psiu,psic,psid,psie,xC,xU,xD,xFa)

             uw=0.5d0 * ( u(i,j-1,1) + u(i,j,1) )
             if(uw>0) then
                psiu=rovv(i-2,j,1)
                xU=grid_x(i-2)

                psic=rovv(i-1,j,1)
                xC=grid_x(i-1)

                psid=rovv(i,j,1)
                xD=grid_x(i)

                xFa=grid_xu(i)!
             else
                psid=rovv(i-1,j,1)
                xD=grid_x(i-1)

                psic=rovv(i,j,1)
                xC=grid_x(i)

                psiu=rovv(i+1,j,1)
                xU=grid_x(i+1)
                xFa=grid_xu(i)!
             endif

             call interpolate_HR(icase,psiu,psic,psid,psiw,xC,xU,xD,xFa)

             vn = (dyv2(j  )*v(i,j+1,1)+dyv2(j+1)*v(i,j  ,1))/dy(j  )
             if(vn>0) then
                psiu=rovv(i,j-1,1)
                xU=grid_yv(j-1)

                psic=rovv(i,j,1)
                xC=grid_yv(j)

                psid=rovv(i,j+1,1)
                xD=grid_yv(j+1)

                xFa=grid_y(j)!
             else
                psid=rovv(i,j,1)
                xD=grid_yv(j)

                psic=rovv(i,j+1,1)
                xC=grid_yv(j+1)

                psiu=rovv(i,j+2,1)
                xU=grid_yv(j+2)

                xFa=grid_y(j)!
             endif

             call interpolate_HR(icase,psiu,psic,psid,psin,xC,xU,xD,xFa)

             vs=(dyv2(j-1)*v(i,j  ,1)+dyv2(j  )*v(i,j-1,1))/dy(j-1)
             if(vs>0) then
                psiu=rovv(i,j-2,1)
                xU=grid_yv(j-2)

                psic=rovv(i,j-1,1)
                xC=grid_yv(j-1)

                psid=rovv(i,j,1)
                xD=grid_yv(j)

                xFa=grid_y(j-1)!
             else
                psid=rovv(i,j-1,1)
                xD=grid_yv(j-1)

                psic=rovv(i,j,1)
                xC=grid_yv(j)

                psiu=rovv(i,j+1,1)
                xU=grid_yv(j+1)

                xFa=grid_y(j-1)!
             endif

             call interpolate_HR(icase,psiu,psic,psid,psis,xC,xU,xD,xFa)
             
             Rv(i,j,1)=(ue*psie-uw*psiw)/dx(i)+(vn*psin-vs*psis)/dyv(j)
             
          end do
       end do

    case(10)
       !-------------------------------------------------------------------------------
       ! U Component
       !-------------------------------------------------------------------------------
       ! if (rank==0 .and. time==0) then
       !    write (*,*) 'Upwind 1 scheme'
       ! endif
       do j=syu,eyu
          do i=sxu,exu

             !---------------------------------------
             ! x-direction
             !---------------------------------------
             ue =(dxu2(i  )*u(i+1,j,1)+dxu2(i+1)*u(i  ,j,1))/dx(i  ) 
             !  ue = 0.5d0 * ( u(i,j,1) + u(i+1,j,1) )
             if(ue>0) then
                phi=rovu(i,j,1)
             else
                phi=rovu(i+1,j,1)
             endif
             fp=ue*phi
             uw =  (dxu2(i-1)*u(i  ,j,1)+dxu2(i  )*u(i-1,j,1))/dx(i-1)
             ! uw = 0.5d0 * ( u(i-1,j,1) + u(i,j,1) )
             if (uw>zero) then
                phi=rovu(i-1,j,1)
             else
                phi=rovu(i,j,1)
             end if
             fm=uw*phi

             fx=(fp-fm)

             !---------------------------------------
             ! y-direction
             !---------------------------------------
             vn = 0.5d0 * ( v(i-1,j+1,1) + v(i,j+1,1) )
             if (vn>zero) then
                phi=rovu(i,j,1)
             else
                phi=rovu(i,j+1,1)
             end if
             fp=vn*phi

             vs = 0.5d0 * ( v(i-1,j,1) + v(i,j,1) )
             if (vs>zero) then
                phi=rovu(i,j-1,1)
             else
                phi=rovu(i,j,1)
             end if
             fm = vs*phi

             fy=(fp-fm)

             Ru(i,j,1) =  fx/dxu(i) + fy/dy(j)

          end do
       end do

       !-------------------------------------------------------------------------------
       ! V Component
       !-------------------------------------------------------------------------------

       do j=syv,eyv
          do i=sxv,exv

             !---------------------------------------
             ! x-direction
             !---------------------------------------
             ue = 0.5d0 * ( u(i+1,j,1) + u(i+1,j-1,1) )
             if(ue>0) then
                phi=rovv(i,j,1)
             else
                phi=rovv(i+1,j,1)
             endif
             fp=ue*phi

             uw = 0.5d0 * ( u(i,j-1,1) + u(i,j,1) )
             if (uw>zero) then
                phi=rovv(i-1,j,1)
             else
                phi=rovv(i,j,1)
             end if
             fm=uw*phi

             fx=(fp-fm)

             !---------------------------------------
             ! y-direction
             !---------------------------------------
             vn = (dyv2(j  )*v(i,j+1,1)+dyv2(j+1)*v(i,j  ,1))/dy(j  )
             !vn = 0.5d0 * ( v(i,j,1) + v(i,j+1,1) )
             if (vn>zero) then
                phi=rovv(i,j,1)
             else

                phi=rovv(i,j+1,1)
             end if
             fp =vn*phi
             vs=(dyv2(j-1)*v(i,j  ,1)+dyv2(j  )*v(i,j-1,1))/dy(j-1)
             !vs = 0.5d0 * ( v(i,j,1) + v(i,j-1,1) )
             if (vs>zero) then
                phi=rovv(i,j-1,1)
             else
                phi=rovv(i,j,1)
             end if
             fm=vs*phi

             fy=(fp-fm)

             Rv(i,j,1) = fx /dx(i) + fy/dyv(j)

          end do
       end do

    case DEFAULT
       write(*,*) 'transport scheme does not exist'
       stop
    end select


  end subroutine Compute_Convective_Term2D
  
  subroutine Compute_Convective_Term3D(Ru,Rv,Rw,u,v,w,rovu,rovv,rovw)
    use mod_Parameters, only: nproc,dim,                &
         & dx,dy,dz,ddx,ddy,ddz,                        &
         & dxu,dyv,dzw,dxu2,dyv2,dzw2,ddxu,ddyv,ddzw,   &
         & sx,ex,sy,ey,sz,ez,                           &
         & gsx,gex,gsy,gey,gsz,gez,                     &
         & sxu,exu,syu,eyu,szu,ezu,                     &
         & sxv,exv,syv,eyv,szv,ezv,                     &
         & sxw,exw,syw,eyw,szw,ezw,                     &
         & sxs,exs,sys,eys,szs,ezs,                     &
         & sxus,exus,syus,eyus,szus,ezus,               &
         & sxvs,exvs,syvs,eyvs,szvs,ezvs,               &
         & sxws,exws,syws,eyws,szws,ezws,               &
         & gsxu,gexu,gsyu,geyu,gszu,gezu,               &
         & gsxv,gexv,gsyv,geyv,gszv,gezv,               &
         & gsxw,gexw,gsyw,geyw,gszw,gezw,               &
         & gx,gy,gz,dt,                                 &
         & NS_method,                                   &
         & NS_linear_term,NS_inertial_scheme,PJ_method, &
         & AL_comp,AL_method,AL_dr,                     &
         & grid_x,grid_xu,grid_y,grid_yv,grid_z,grid_zw,&
         & NS_MomentumConserving_scheme
    use mod_Constants, only: zero,one,d1p2
    use mod_mpi
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable,  intent(in)    :: u,v,w,rovu,rovv,rovw
    real(8), dimension(:,:,:), allocatable,  intent(inout) :: Ru,Rv,Rw
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                                :: i,j,k
    integer                                                :: icase
    real(8)                                                :: ue,uw,vn,vs,wf,wb
    real(8)                                                :: psiu,psic,psid,psie,psiw,psis,psin,psib,psif
    real(8)                                                :: fm,fp,fx,fy,fz,phi
    real(8)                                                :: xU,xD,xC,xFa
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! init 
    !-------------------------------------------------------------------------------
    ue=0;uw=0
    vn=0;vs=0
    wf=0;wb=0
    psiu=0
    psic=0
    psid=0
    psie=0
    psiw=0
    psis=0
    psin=0
    psib=0
    psif=0
    fm=0;fp=0
    fx=0;fy=0;fz=0
    phi=0
    !-------------------------------------------------------------------------------

    select case(abs(NS_MomentumConserving_scheme))
       !-------------------------------------------------------------------------------
       ! 1 --> Cubic Upwind Interpolate (3th order)
       ! 2 --> WENO5 Conservative
       !-------------------------------------------------------------------------------
    case(1)
       !-------------------------------------------------------------------------------
       ! U component 
       !-------------------------------------------------------------------------------
       do k=szu,ezu
          do j=syu,eyu
             do i=sxu,exu

                ue=0.5d0 * ( u(i,j,k) + u(i+1,j,k) )
                if(ue>0) then
                   psiu=rovu(i-1,j,k)
                   psic=rovu(i,j,k)
                   psid=rovu(i+1,j,k)
                else
                   psid=rovu(i,j,k)
                   psic=rovu(i+1,j,k)
                   psiu=rovu(i+2,j,k)
                endif

                call interpolate_cui_hr_quantity(psiu,psic,psid,psie)

                uw=0.5d0 * ( u(i-1,j,k) + u(i,j,k) )
                if(uw>0) then
                   psiu=rovu(i-2,j,k)
                   psic=rovu(i-1,j,k)
                   psid=rovu(i,j,k)
                else
                   psid=rovu(i-1,j,k)
                   psic=rovu(i,j,k)
                   psiu=rovu(i+1,j,k)
                endif

                call interpolate_cui_hr_quantity(psiu,psic,psid,psiw)

                vn=0.5d0 * ( v(i-1,j+1,k) + v(i,j+1,k) )
                if(vn>0) then
                   psiu=rovu(i,j-1,k)
                   psic=rovu(i,j,k)
                   psid=rovu(i,j+1,k)
                else
                   psid=rovu(i,j,k)
                   psic=rovu(i,j+1,k)
                   psiu=rovu(i,j+2,k)
                endif

                call interpolate_cui_hr_quantity(psiu,psic,psid,psin)

                vs=0.5d0 * ( v(i-1,j,k) + v(i,j,k) )
                if(vs>0) then
                   psiu=rovu(i,j-2,k)
                   psic=rovu(i,j-1,k)
                   psid=rovu(i,j,k)
                else
                   psid=rovu(i,j-1,k)
                   psic=rovu(i,j,k)
                   psiu=rovu(i,j+1,k)
                endif

                call interpolate_cui_hr_quantity(psiu,psic,psid,psis)

                wf=0.5d0 * ( w(i-1,j,k+1)+w(i,j,k+1) )
                if(wf>0) then
                   psiu=rovu(i,j,k-1)
                   psic=rovu(i,j,k)
                   psid=rovu(i,j,k+1)
                else
                   psid=rovu(i,j,k)
                   psic=rovu(i,j,k+1)
                   psiu=rovu(i,j,k+2)
                endif

                call interpolate_cui_hr_quantity(psiu,psic,psid,psif)

                wb=0.5d0 * ( w(i-1,j,k  )+w(i,j,k  ) )
                if(wb>0) then
                   psiu=rovu(i,j,k-2)
                   psic=rovu(i,j,k-1)
                   psid=rovu(i,j,k)
                else
                   psid=rovu(i,j,k-1)
                   psic=rovu(i,j,k)
                   psiu=rovu(i,j,k+1)
                endif

                call interpolate_cui_hr_quantity(psiu,psic,psid,psib)

                Ru(i,j,k) = (ue*psie-uw*psiw)/dxu(i) &
                     &     +(vn*psin-vs*psis)/dy(j)  &
                     &     +(wf*psif-wb*psib)/dz(k)

             end do
          end do
       end do
       !-------------------------------------------------------------------------------
       ! V component 
       !-------------------------------------------------------------------------------
       do k=szv,ezv
          do j=syv,eyv
             do i=sxv,exv

                ue=0.5d0 * ( u(i+1,j,k) + u(i+1,j-1,k) )
                if(ue>0) then
                   psiu=rovv(i-1,j,k)
                   psic=rovv(i,j,k)
                   psid=rovv(i+1,j,k)
                else
                   psid=rovv(i,j,k)
                   psic=rovv(i+1,j,k)
                   psiu=rovv(i+2,j,k)
                endif

                call interpolate_cui_hr_quantity(psiu,psic,psid,psie)

                uw=0.5d0 * ( u(i,j-1,k) + u(i,j,k) )
                if(uw>0) then
                   psiu=rovv(i-2,j,k)
                   psic=rovv(i-1,j,k)
                   psid=rovv(i,j,k)
                else
                   psid=rovv(i-1,j,k)
                   psic=rovv(i,j,k)
                   psiu=rovv(i+1,j,k)
                endif
                
                call interpolate_cui_hr_quantity(psiu,psic,psid,psiw)
                
                vn=0.5d0 * ( v(i,j,k) + v(i,j+1,k) )
                if(vn>0) then
                   psiu=rovv(i,j-1,k)
                   psic=rovv(i,j,k)
                   psid=rovv(i,j+1,k)
                else
                   psid=rovv(i,j,k)
                   psic=rovv(i,j+1,k)
                   psiu=rovv(i,j+2,k)
                endif

                call interpolate_cui_hr_quantity(psiu,psic,psid,psin)

                vs=0.5d0 * ( v(i,j,k) + v(i,j-1,k) )
                if(vs>0) then
                   psiu=rovv(i,j-2,k)
                   psic=rovv(i,j-1,k)
                   psid=rovv(i,j,k)
                else
                   psid=rovv(i,j-1,k)
                   psic=rovv(i,j,k)
                   psiu=rovv(i,j+1,k)
                endif
                
                call interpolate_cui_hr_quantity(psiu,psic,psid,psis)

                wf=0.5d0 * ( w(i,j-1,k+1)+w(i,j,k+1) )
                if(wf>0) then
                   psiu=rovv(i,j,k-1)
                   psic=rovv(i,j,k)
                   psid=rovv(i,j,k+1)
                else
                   psid=rovv(i,j,k)
                   psic=rovv(i,j,k+1)
                   psiu=rovv(i,j,k+2)
                endif
                
                call interpolate_cui_hr_quantity(psiu,psic,psid,psif)

                wb=0.5d0 * ( w(i,j-1,k  )+w(i,j,k  ) )
                if(wb>0) then
                   psiu=rovv(i,j,k-2)
                   psic=rovv(i,j,k-1)
                   psid=rovv(i,j,k)
                else
                   psid=rovv(i,j,k-1)
                   psic=rovv(i,j,k)
                   psiu=rovv(i,j,k+1)
                endif

                call interpolate_cui_hr_quantity(psiu,psic,psid,psib)

                Rv(i,j,k) = (ue*psie-uw*psiw)/dx(i)  &
                     &     +(vn*psin-vs*psis)/dyv(j) &
                     &     +(wf*psif-wb*psib)/dz(k)

             end do
          end do
       end do
       !-------------------------------------------------------------------------------
       ! W component 
       !-------------------------------------------------------------------------------
       do k=szw,ezw
          do j=syw,eyw
             do i=sxw,exw

                ue=0.5d0 * (u(i+1,j,k)+u(i+1,j,k-1) )
                if(ue>0) then
                   psiu=rovw(i-1,j,k)
                   psic=rovw(i,j,k)
                   psid=rovw(i+1,j,k)
                else
                   psid=rovw(i,j,k)
                   psic=rovw(i+1,j,k)
                   psiu=rovw(i+2,j,k)
                endif

                call interpolate_cui_hr_quantity(psiu,psic,psid,psie)

                uw=0.5d0 * (u(i  ,j,k)+u(i  ,j,k-1) )
                if(uw>0) then
                   psiu=rovw(i-2,j,k)
                   psic=rovw(i-1,j,k)
                   psid=rovw(i,j,k)
                else
                   psid=rovw(i-1,j,k)
                   psic=rovw(i,j,k)
                   psiu=rovw(i+1,j,k)
                endif

                call interpolate_cui_hr_quantity(psiu,psic,psid,psiw)

                vn=0.5d0 * ( v(i,j+1,k)+v(i,j+1,k-1) )
                if(vn>0) then
                   psiu=rovw(i,j-1,k)
                   psic=rovw(i,j,k)
                   psid=rovw(i,j+1,k)
                else
                   psid=rovw(i,j,k)
                   psic=rovw(i,j+1,k)
                   psiu=rovw(i,j+2,k)
                endif

                call interpolate_cui_hr_quantity(psiu,psic,psid,psin)

                vs=0.5d0 * ( v(i,j  ,k)+v(i,j  ,k-1) )
                if(vs>0) then
                   psiu=rovw(i,j-2,k)
                   psic=rovw(i,j-1,k)
                   psid=rovw(i,j,k)
                else
                   psid=rovw(i,j-1,k)
                   psic=rovw(i,j,k)
                   psiu=rovw(i,j+1,k)
                endif

                call interpolate_cui_hr_quantity(psiu,psic,psid,psis)

                wf=0.5d0 * ( w(i,j,k+1)+w(i,j,k) )
                if(wf>0) then
                   psiu=rovw(i,j,k-1)
                   psic=rovw(i,j,k)
                   psid=rovw(i,j,k+1)
                else
                   psid=rovw(i,j,k)
                   psic=rovw(i,j,k+1)
                   psiu=rovw(i,j,k+2)
                endif

                call interpolate_cui_hr_quantity(psiu,psic,psid,psif)

                wb=0.5d0 * ( w(i,j,k-1  )+w(i,j,k  ) )
                if(wb>0) then
                   psiu=rovw(i,j,k-2)
                   psic=rovw(i,j,k-1)
                   psid=rovw(i,j,k)
                else
                   psid=rovw(i,j,k-1)
                   psic=rovw(i,j,k)
                   psiu=rovw(i,j,k+1)
                endif

                call interpolate_cui_hr_quantity(psiu,psic,psid,psib)

                Rw(i,j,k) = (ue*psie-uw*psiw)/dx(i)  &
                     &     +(vn*psin-vs*psis)/dy(j)  &
                     &     +(wf*psif-wb*psib)/dzw(k)

             end do
          end do
       end do

    case(2)


       !-------------------------------------------------------------------------------
       ! U Component
       !-------------------------------------------------------------------------------
       do k=szu,ezu
          do j=syu,eyu
             do i=sxu,exu

                !---------------------------------------
                ! x-direction
                !---------------------------------------
                !ue = 0.5d0 * ( u(i,j,k) + u(i+1,j,k) )
                ue = (dxu2(i  )*u(i+1,j,k)+dxu2(i+1)*u(i  ,j,k))/dx(i  )
                if(ue>0) then
                   call sweno5_shala(phi, &
                        & rovu(i-2,j,k), &
                        & rovu(i-1,j,k), &
                        & rovu(i+0,j,k), &
                        & rovu(i+1,j,k), &
                        & rovu(i+2,j,k))
                else
                   call sweno5_shala(phi, &
                        & rovu(i+3,j,k), &
                        & rovu(i+2,j,k), &
                        & rovu(i+1,j,k), &
                        & rovu(i+0,j,k), &
                        & rovu(i-1,j,k))
                endif
                fp=ue*phi


                !uw = 0.5d0 * ( u(i-1,j,k) + u(i,j,k) )
                uw=(dxu2(i-1)*u(i  ,j,k)+dxu2(i  )*u(i-1,j,k))/dx(i-1)

                if (uw>zero) then
                   call sweno5_shala(phi,   &
                        & rovu(i-3,j,k), &
                        & rovu(i-2,j,k), &
                        & rovu(i-1,j,k), &
                        & rovu(i+0,j,k), &
                        & rovu(i+1,j,k))
                else
                   call sweno5_shala(phi,   &
                        & rovu(i+2,j,k), &
                        & rovu(i+1,j,k), &
                        & rovu(i+0,j,k), &
                        & rovu(i-1,j,k), &
                        & rovu(i-2,j,k))
                end if
                fm=uw*phi

                fx=(fp-fm)

                !---------------------------------------
                ! y-direction
                !---------------------------------------
                vn = 0.5d0 * ( v(i-1,j+1,k) + v(i,j+1,k) )
                if (vn>zero) then
                   call sweno5_shala(phi, &
                        & rovu(i,j-2,k), &
                        & rovu(i,j-1,k), &
                        & rovu(i,j+0,k), &
                        & rovu(i,j+1,k), &
                        & rovu(i,j+2,k))
                else
                   call sweno5_shala(phi,   &
                        & rovu(i,j+3,k), &
                        & rovu(i,j+2,k), &
                        & rovu(i,j+1,k), &
                        & rovu(i,j+0,k), &
                        & rovu(i,j-1,k))
                end if
                fp=vn*phi

                vs = 0.5d0 * ( v(i-1,j,k) + v(i,j,k) )
                if (vs>zero) then
                   call sweno5_shala(phi,   &
                        & rovu(i,j-3,k), &
                        & rovu(i,j-2,k), &
                        & rovu(i,j-1,k), &
                        & rovu(i,j+0,k), &
                        & rovu(i,j+1,k))
                else
                   call sweno5_shala(phi,   &
                        & rovu(i,j+2,k), &
                        & rovu(i,j+1,k), &
                        & rovu(i,j+0,k), &
                        & rovu(i,j-1,k), &
                        & rovu(i,j-2,k))
                end if
                fm = vs*phi

                fy=(fp-fm)

                !---------------------------------------
                ! z-direction
                !---------------------------------------
                wf=0.5d0 * ( w(i-1,j,k+1)+w(i,j,k+1) )
                if (wf>zero) then
                   call sweno5_shala(phi, &
                        & rovu(i,j,k-2), &
                        & rovu(i,j,k-1), &
                        & rovu(i,j,k+0), &
                        & rovu(i,j,k+1), &
                        & rovu(i,j,k+2))
                else
                   call sweno5_shala(phi,   &
                        & rovu(i,j,k+3), &
                        & rovu(i,j,k+2), &
                        & rovu(i,j,k+1), &
                        & rovu(i,j,k+0), &
                        & rovu(i,j,k-1))
                end if
                fp=wf*phi

                wb=0.5d0 * ( w(i-1,j,k  )+w(i,j,k  ) )
                if (wb>zero) then
                   call sweno5_shala(phi,   &
                        & rovu(i,j,k-3), &
                        & rovu(i,j,k-2), &
                        & rovu(i,j,k-1), &
                        & rovu(i,j,k+0), &
                        & rovu(i,j,k+1))
                else
                   call sweno5_shala(phi,   &
                        & rovu(i,j,k+2), &
                        & rovu(i,j,k+1), &
                        & rovu(i,j,k+0), &
                        & rovu(i,j,k-1), &
                        & rovu(i,j,k-2))
                end if
                fm = wb*phi

                fz=(fp-fm)

                Ru(i,j,k)=fx/dxu(i)+fy/dy(j)+fz/dz(k) 

             end do
          end do
       end do

       !-------------------------------------------------------------------------------
       ! V Component
       !-------------------------------------------------------------------------------

       do k=szv,ezv 
          do j=syv,eyv
             do i=sxv,exv

                !---------------------------------------
                ! x-direction
                !---------------------------------------
                ue = 0.5d0 * ( u(i+1,j,k) + u(i+1,j-1,k) )
                if(ue>0) then
                   call sweno5_shala(phi,   &
                        & rovv(i-2,j,k), &
                        & rovv(i-1,j,k), &
                        & rovv(i+0,j,k), &
                        & rovv(i+1,j,k), &
                        & rovv(i+2,j,k))
                else
                   call sweno5_shala(phi,   &
                        & rovv(i+3,j,k), &
                        & rovv(i+2,j,k), &
                        & rovv(i+1,j,k), &
                        & rovv(i+0,j,k), &
                        & rovv(i-1,j,k))
                endif
                fp=ue*phi

                uw = 0.5d0 * ( u(i,j-1,k) + u(i,j,k) )
                if (uw>zero) then
                   call sweno5_shala(phi,   &
                        & rovv(i-3,j,k), &
                        & rovv(i-2,j,k), &
                        & rovv(i-1,j,k), &
                        & rovv(i+0,j,k), &
                        & rovv(i+1,j,k))
                else

                   call sweno5_shala(phi,   &
                        & rovv(i+2,j,k), &
                        & rovv(i+1,j,k), &
                        & rovv(i+0,j,k), &
                        & rovv(i-1,j,k), &
                        & rovv(i-2,j,k))
                end if
                fm = uw *phi

                fx=(fp-fm)

                !---------------------------------------
                ! y-direction
                !---------------------------------------
                !vn = 0.5d0 * ( v(i,j,k) + v(i,j+1,k) )
                vn=(dyv2(j  )*v(i,j+1,k)+dyv2(j+1)*v(i,j  ,k))/dy(j  )

                if (vn>zero) then
                   call sweno5_shala(phi, &
                        & rovv(i,j-2,k), &
                        & rovv(i,j-1,k), &
                        & rovv(i,j+0,k), &
                        & rovv(i,j+1,k), &
                        & rovv(i,j+2,k))
                else
                   call sweno5_shala(phi, &
                        & rovv(i,j+3,k), &
                        & rovv(i,j+2,k), &
                        & rovv(i,j+1,k), &
                        & rovv(i,j+0,k), &
                        & rovv(i,j-1,k))
                end if
                fp =vn*phi

                !vs = 0.5d0 * ( v(i,j,k) + v(i,j-1,k) )
                vs=(dyv2(j-1)*v(i,j  ,k)+dyv2(j  )*v(i,j-1,k))/dy(j-1)
                if (vs>zero) then
                   call sweno5_shala(phi, &
                        & rovv(i,j-3,k), &
                        & rovv(i,j-2,k), &
                        & rovv(i,j-1,k), &
                        & rovv(i,j+0,k), &
                        & rovv(i,j+1,k))
                else
                   call sweno5_shala(phi,&
                        & rovv(i,j+2,k), &
                        & rovv(i,j+1,k), &
                        & rovv(i,j+0,k), &
                        & rovv(i,j-1,k), &
                        & rovv(i,j-2,k))
                end if
                fm=vs*phi

                fy=(fp-fm)

                !---------------------------------------
                ! z-direction
                !---------------------------------------
                wf=0.5d0 * ( w(i,j-1,k+1)+w(i,j,k+1) )
                if (wf>zero) then
                   call sweno5_shala(phi, &
                        & rovv(i,j,k-2), &
                        & rovv(i,j,k-1), &
                        & rovv(i,j,k+0), &
                        & rovv(i,j,k+1), &
                        & rovv(i,j,k+2))
                else
                   call sweno5_shala(phi, &
                        & rovv(i,j,k+3), &
                        & rovv(i,j,k+2), &
                        & rovv(i,j,k+1), &
                        & rovv(i,j,k+0), &
                        & rovv(i,j,k-1))
                end if
                fp =wf*phi

                wb=0.5d0 * ( w(i,j-1,k  )+w(i,j,k  ) )
                if (wb>zero) then
                   call sweno5_shala(phi, &
                        & rovv(i,j,k-3), &
                        & rovv(i,j,k-2), &
                        & rovv(i,j,k-1), &
                        & rovv(i,j,k+0), &
                        & rovv(i,j,k+1))
                else
                   call sweno5_shala(phi,&
                        & rovv(i,j,k+2), &
                        & rovv(i,j,k+1), &
                        & rovv(i,j,k+0), &
                        & rovv(i,j,k-1), &
                        & rovv(i,j,k-2))
                end if
                fm=wb*phi

                fz=(fp-fm)

                Rv(i,j,k)=fx/dx(i)+fy/dyv(j)+fz/dz(k)

             end do
          end do
       end do

       !-------------------------------------------------------------------------------
       ! W Component
       !-------------------------------------------------------------------------------

       do k=szw,ezw 
          do j=syw,eyw
             do i=sxw,exw

                !---------------------------------------
                ! x-direction
                !---------------------------------------
                ue=0.5d0 * (u(i+1,j,k)+u(i+1,j,k-1) )
                if(ue>0) then
                   call sweno5_shala(phi,   &
                        & rovw(i-2,j,k), &
                        & rovw(i-1,j,k), &
                        & rovw(i+0,j,k), &
                        & rovw(i+1,j,k), &
                        & rovw(i+2,j,k))
                else
                   call sweno5_shala(phi,   &
                        & rovw(i+3,j,k), &
                        & rovw(i+2,j,k), &
                        & rovw(i+1,j,k), &
                        & rovw(i+0,j,k), &
                        & rovw(i-1,j,k))
                endif
                fp=ue*phi

                uw=0.5d0 * (u(i  ,j,k)+u(i  ,j,k-1) )
                if (uw>zero) then
                   call sweno5_shala(phi,   &
                        & rovw(i-3,j,k), &
                        & rovw(i-2,j,k), &
                        & rovw(i-1,j,k), &
                        & rovw(i+0,j,k), &
                        & rovw(i+1,j,k))
                else
                   fm=uw
                   call sweno5_shala(phi,   &
                        & rovw(i+2,j,k), &
                        & rovw(i+1,j,k), &
                        & rovw(i+0,j,k), &
                        & rovw(i-1,j,k), &
                        & rovw(i-2,j,k))
                end if
                fm = uw *phi

                fx=(fp-fm)

                !---------------------------------------
                ! y-direction
                !---------------------------------------
                vn=0.5d0 * ( v(i,j+1,k)+v(i,j+1,k-1) )
                if (vn>zero) then
                   call sweno5_shala(phi, &
                        & rovw(i,j-2,k), &
                        & rovw(i,j-1,k), &
                        & rovw(i,j+0,k), &
                        & rovw(i,j+1,k), &
                        & rovw(i,j+2,k))
                else
                   call sweno5_shala(phi, &
                        & rovw(i,j+3,k), &
                        & rovw(i,j+2,k), &
                        & rovw(i,j+1,k), &
                        & rovw(i,j+0,k), &
                        & rovw(i,j-1,k))
                end if
                fp =vn*phi

                vs=0.5d0 * ( v(i,j  ,k)+v(i,j  ,k-1) )
                if (vs>zero) then
                   call sweno5_shala(phi, &
                        & rovw(i,j-3,k), &
                        & rovw(i,j-2,k), &
                        & rovw(i,j-1,k), &
                        & rovw(i,j+0,k), &
                        & rovw(i,j+1,k))
                else
                   call sweno5_shala(phi,&
                        & rovw(i,j+2,k), &
                        & rovw(i,j+1,k), &
                        & rovw(i,j+0,k), &
                        & rovw(i,j-1,k), &
                        & rovw(i,j-2,k))
                end if
                fm=vs*phi

                fy=(fp-fm)

                !---------------------------------------
                ! z-direction
                !---------------------------------------
                !wf=0.5d0 * ( w(i,j,k+1)+w(i,j,k) )
                wf=(dzw2(k+1)*w(i,j,k  )+dzw2(k  )*w(i,j,k+1))/dz(k  )

                if (wf>zero) then
                   call sweno5_shala(phi, &
                        & rovw(i,j,k-2), &
                        & rovw(i,j,k-1), &
                        & rovw(i,j,k+0), &
                        & rovw(i,j,k+1), &
                        & rovw(i,j,k+2))
                else
                   call sweno5_shala(phi, &
                        & rovw(i,j,k+3), &
                        & rovw(i,j,k+2), &
                        & rovw(i,j,k+1), &
                        & rovw(i,j,k+0), &
                        & rovw(i,j,k-1))
                end if
                fp =wf*phi

                !wb=0.5d0 * ( w(i,j,k-1  )+w(i,j,k  ) )
                wb=(dzw2(k  )*w(i,j,k-1)+dzw2(k-1)*w(i,j,k  ))/dz(k-1)

                if (wb>zero) then
                   call sweno5_shala(phi, &
                        & rovw(i,j,k-3), &
                        & rovw(i,j,k-2), &
                        & rovw(i,j,k-1), &
                        & rovw(i,j,k+0), &
                        & rovw(i,j,k+1))
                else
                   call sweno5_shala(phi,&
                        & rovw(i,j,k+2), &
                        & rovw(i,j,k+1), &
                        & rovw(i,j,k+0), &
                        & rovw(i,j,k-1), &
                        & rovw(i,j,k-2))
                end if
                fm=wb*phi

                fz=(fp-fm)

                Rw(i,j,k)=fx/dx(i)+fy/dy(j)+fz/dzw(k)

             end do
          end do
       end do

    case(3:8)
       icase=abs(NS_MomentumConserving_scheme)-2
       !-------------------------------------------------------------------------------
       ! U component 
       !-------------------------------------------------------------------------------
       do k=szu,ezu
          do j=syu,eyu
             do i=sxu,exu
                
                ue = (dxu2(i  )*u(i+1,j,k)+dxu2(i+1)*u(i  ,j,k))/dx(i  )
                if(ue>0) then
                   psiu=rovu(i-1,j,k)
                   xU=grid_xu(i-1)

                   psic=rovu(i,j,k)
                   xC=grid_xu(i)

                   psid=rovu(i+1,j,k)
                   xD=grid_xu(i+1)

                   xFa=grid_x(i)
                else
                   psid=rovu(i,j,k)
                   xD=grid_xu(i)

                   psic=rovu(i+1,j,k)
                   xC=grid_xu(i+1)

                   psiu=rovu(i+2,j,k)
                   xU=grid_xu(i+2)

                   xFa=grid_x(i)
                endif

                call interpolate_HR(icase,psiu,psic,psid,psie,xC,xU,xD,xFa)

                uw=(dxu2(i-1)*u(i  ,j,k)+dxu2(i  )*u(i-1,j,k))/dx(i-1)
                if(uw>0) then
                   psiu=rovu(i-2,j,k)
                   xU=grid_xu(i-2)

                   psic=rovu(i-1,j,k)
                   xC=grid_xu(i-1)

                   psid=rovu(i,j,k)
                   xD=grid_xu(i)

                   xFa=grid_x(i-1)
                else
                   psid=rovu(i-1,j,k)
                   xD=grid_xu(i-1)

                   psic=rovu(i,j,k)
                   xC=grid_xu(i)

                   psiu=rovu(i+1,j,k)
                   xU=grid_xu(i+1)

                   xFa=grid_x(i-1)
                endif

                call interpolate_HR(icase,psiu,psic,psid,psiw,xC,xU,xD,xFa)

                vn=0.5d0 * ( v(i-1,j+1,k) + v(i,j+1,k) )
                if(vn>0) then
                   psiu=rovu(i,j-1,k)
                   xU=grid_y(j-1)

                   psic=rovu(i,j,k)
                   xC=grid_y(j)

                   psid=rovu(i,j+1,k)
                   xD=grid_y(j+1)

                   xFa=grid_yv(j+1)

                else
                   psid=rovu(i,j,k)
                   xD=grid_y(j)

                   psic=rovu(i,j+1,k)
                   xC=grid_y(j+1)

                   psiu=rovu(i,j+2,k)
                   xU=grid_y(j+2)

                   xFa=grid_yv(j+1)
                endif

                call interpolate_HR(icase,psiu,psic,psid,psin,xC,xU,xD,xFa)
                
                vs=0.5d0 * ( v(i-1,j,k) + v(i,j,k) )
                if(vs>0) then
                   psiu=rovu(i,j-2,k)
                   xU=grid_y(j-2)

                   psic=rovu(i,j-1,k)
                   xC=grid_y(j-1)

                   psid=rovu(i,j,k)
                   xD=grid_y(j)

                   xFa=grid_yv(j)
                else
                   psid=rovu(i,j-1,k)
                   xD=grid_y(j-1)

                   psic=rovu(i,j,k)
                   xC=grid_y(j)

                   psiu=rovu(i,j+1,k)
                   xU=grid_y(j+1)

                   xFa=grid_yv(j)
                endif

                call interpolate_HR(icase,psiu,psic,psid,psis,xC,xU,xD,xFa)
                
                wf=0.5d0 * ( w(i-1,j,k+1)+w(i,j,k+1) )
                if(wf>0) then
                   psiu=rovu(i,j,k-1)
                   xU=grid_z(k-1)

                   psic=rovu(i,j,k)
                   xC=grid_z(k)

                   psid=rovu(i,j,k+1)
                   xD=grid_z(k+1)

                   xFa=grid_zw(k+1)
                else
                   psid=rovu(i,j,k)
                   xD=grid_z(k)

                   psic=rovu(i,j,k+1)
                   xC=grid_z(k+1)

                   psiu=rovu(i,j,k+2)
                   xU=grid_z(k+2)

                   xFa=grid_zw(k+1)
                endif

                call interpolate_HR(icase,psiu,psic,psid,psif,xC,xU,xD,xFa)
                
                wb=0.5d0 * ( w(i-1,j,k  )+w(i,j,k  ) )
                if(wb>0) then
                   psiu=rovu(i,j,k-2)
                   xU=grid_z(k-2)

                   psic=rovu(i,j,k-1)
                   xC=grid_z(k-1)

                   psid=rovu(i,j,k)
                   xD=grid_z(k)

                   xFa=grid_zw(k)
                else
                   psid=rovu(i,j,k-1)
                   xD=grid_z(k-1)

                   psic=rovu(i,j,k)
                   xC=grid_z(k)

                   psiu=rovu(i,j,k+1)
                   xU=grid_z(k+1)

                   xFa=grid_zw(k)
                endif

                call interpolate_HR(icase,psiu,psic,psid,psib,xC,xU,xD,xFa)

                Ru(i,j,k) = (ue*psie-uw*psiw)/dxu(i) &
                     &     +(vn*psin-vs*psis)/dy(j)  &
                     &     +(wf*psif-wb*psib)/dz(k)

             end do
          end do
       end do
       !-------------------------------------------------------------------------------
       ! V component 
       !-------------------------------------------------------------------------------
       do k=szv,ezv
          do j=syv,eyv
             do i=sxv,exv
                
                ue=0.5d0 * ( u(i+1,j,k) + u(i+1,j-1,k) )
                if(ue>0) then
                   psiu=rovv(i-1,j,k)
                   xU=grid_x(i-1)

                   psic=rovv(i,j,k)
                   xC=grid_x(i)

                   psid=rovv(i+1,j,k)
                   xD=grid_x(i+1)

                   xFa=grid_xu(i+1)
                else
                   psid=rovv(i,j,k)
                   xD=grid_x(i)

                   psic=rovv(i+1,j,k)
                   xC=grid_x(i+1)

                   psiu=rovv(i+2,j,k)
                   xU=grid_x(i+2)

                   xFa=grid_xu(i+1)
                endif

                call interpolate_HR(icase,psiu,psic,psid,psie,xC,xU,xD,xFa)

                uw=0.5d0 * ( u(i,j-1,k) + u(i,j,k) )
                if(uw>0) then
                   psiu=rovv(i-2,j,k)
                   xU=grid_x(i-2)

                   psic=rovv(i-1,j,k)
                   xC=grid_x(i-1)

                   psid=rovv(i,j,k)
                   xD=grid_x(i)

                   xFa=grid_xu(i)
                else
                   psid=rovv(i-1,j,k)
                   xD=grid_x(i-1)

                   psic=rovv(i,j,k)
                   xC=grid_x(i)

                   psiu=rovv(i+1,j,k)
                   xU=grid_x(i+1)

                   xFa=grid_xu(i)
                endif

                call interpolate_HR(icase,psiu,psic,psid,psiw,xC,xU,xD,xFa)

                vn=(dyv2(j  )*v(i,j+1,k)+dyv2(j+1)*v(i,j  ,k))/dy(j  )
                if(vn>0) then
                   psiu=rovv(i,j-1,k)
                   xU=grid_yv(j-1)

                   psic=rovv(i,j,k)
                   xC=grid_yv(j)

                   psid=rovv(i,j+1,k)
                   xD=grid_yv(j+1)

                   xFa=grid_y(j)
                else
                   psid=rovv(i,j,k)
                   xD=grid_yv(j)

                   psic=rovv(i,j+1,k)
                   xC=grid_yv(j+1)

                   psiu=rovv(i,j+2,k)
                   xU=grid_yv(j+2)

                   xFa=grid_y(j)
                endif

                call interpolate_HR(icase,psiu,psic,psid,psin,xC,xU,xD,xFa)

                vs=(dyv2(j-1)*v(i,j  ,k)+dyv2(j  )*v(i,j-1,k))/dy(j-1)
                if(vs>0) then
                   psiu=rovv(i,j-2,k)
                   xU=grid_yv(j-2)

                   psic=rovv(i,j-1,k)
                   xC=grid_yv(j-1)

                   psid=rovv(i,j,k)
                   xD=grid_yv(j)

                   xFa=grid_y(j-1)
                else
                   psid=rovv(i,j-1,k)
                   xD=grid_yv(j-1)

                   psic=rovv(i,j,k)
                   xC=grid_yv(j)

                   psiu=rovv(i,j+1,k)
                   xU=grid_yv(j+1)

                   xFa=grid_y(j-1)
                endif

                call interpolate_HR(icase,psiu,psic,psid,psis,xC,xU,xD,xFa)
                
                wf=0.5d0 * ( w(i,j-1,k+1)+w(i,j,k+1) )
                if(wf>0) then
                   psiu=rovv(i,j,k-1)
                   xU=grid_z(k-1)

                   psic=rovv(i,j,k)
                   xC=grid_z(k)

                   psid=rovv(i,j,k+1)
                   xD=grid_z(k+1)

                   xFa=grid_zw(k+1)
                else
                   psid=rovv(i,j,k)
                   xD=grid_z(k)

                   psic=rovv(i,j,k+1)
                   xC=grid_z(k+1)

                   psiu=rovv(i,j,k+2)
                   xU=grid_z(k+2)

                   xFa=grid_zw(k+1)
                endif

                call interpolate_HR(icase,psiu,psic,psid,psif,xC,xU,xD,xFa)

                wb=0.5d0 * ( w(i,j-1,k  )+w(i,j,k  ) )
                if(wb>0) then
                   psiu=rovv(i,j,k-2)
                   xU=grid_z(k-2)

                   psic=rovv(i,j,k-1)
                   xC=grid_z(k-1)

                   psid=rovv(i,j,k)
                   xD=grid_z(k)

                   xFa=grid_zw(k  )
                else
                   psid=rovv(i,j,k-1)
                   xD=grid_z(k-1)

                   psic=rovv(i,j,k)
                   xC=grid_z(k)

                   psiu=rovv(i,j,k+1)
                   xU=grid_z(k+1)

                   xFa=grid_zw(k  )
                endif

                call interpolate_HR(icase,psiu,psic,psid,psib,xC,xU,xD,xFa)
                
                Rv(i,j,k) = (ue*psie-uw*psiw)/dx(i)  &
                     &     +(vn*psin-vs*psis)/dyv(j) &
                     &     +(wf*psif-wb*psib)/dz(k)
             end do
          end do
       end do
       !-------------------------------------------------------------------------------
       ! W component 
       !-------------------------------------------------------------------------------
       do k=szw,ezw
          do j=syw,eyw
             do i=sxw,exw

                ue=0.5d0 * (u(i+1,j,k)+u(i+1,j,k-1) )
                if(ue>0) then
                   psiu=rovw(i-1,j,k)
                   xU=grid_x(i-1)

                   psic=rovw(i,j,k)
                   xC=grid_x(i)

                   psid=rovw(i+1,j,k)
                   xD=grid_x(i+1)

                   xFa=grid_xu(i+1)
                else
                   psid=rovw(i,j,k)
                   xd=grid_x(i)

                   psic=rovw(i+1,j,k)
                   xC=grid_x(i+1)

                   psiu=rovw(i+2,j,k)
                   xU=grid_x(i+2)

                   xFa=grid_xu(i+1)
                endif

                call interpolate_HR(icase,psiu,psic,psid,psie,xC,xU,xD,xFa)
                
                uw=0.5d0 * (u(i  ,j,k)+u(i  ,j,k-1) )
                if(uw>0) then
                   psiu=rovw(i-2,j,k)
                   xU=grid_x(i-2)

                   psic=rovw(i-1,j,k)
                   xC=grid_x(i-1)

                   psid=rovw(i,j,k)
                   xD=grid_x(i)

                   xFa=grid_xu(i)
                else
                   psid=rovw(i-1,j,k)
                   xD=grid_x(i-1)

                   psic=rovw(i,j,k)
                   xC=grid_x(i)

                   psiu=rovw(i+1,j,k)
                   xU=grid_x(i+1)

                   xFa=grid_xu(i)
                endif

                call interpolate_HR(icase,psiu,psic,psid,psiw,xC,xU,xD,xFa)
                
                vn=0.5d0 * ( v(i,j+1,k)+v(i,j+1,k-1) )
                if(vn>0) then
                   psiu=rovw(i,j-1,k)
                   xU=grid_y(j-1)

                   psic=rovw(i,j,k)
                   xC=grid_y(j)

                   psid=rovw(i,j+1,k)
                   xD=grid_y(j+1)

                   xFa=grid_yv(j+1)
                else
                   psid=rovw(i,j,k)
                   xD=grid_y(j)

                   psic=rovw(i,j+1,k)
                   xC=grid_y(j+1)

                   psiu=rovw(i,j+2,k)
                   xU=grid_y(j+2)

                   xFa=grid_yv(j+1)
                endif

                call interpolate_HR(icase,psiu,psic,psid,psin,xC,xU,xD,xFa)
                
                vs=0.5d0 * ( v(i,j  ,k)+v(i,j  ,k-1) )
                if(vs>0) then
                   psiu=rovw(i,j-2,k)
                   xU=grid_y(j-2)

                   psic=rovw(i,j-1,k)
                   xC=grid_y(j-1)

                   psid=rovw(i,j,k)
                   xD=grid_y(j)

                   xFa=grid_yv(j)
                else
                   psid=rovw(i,j-1,k)
                   xD=grid_y(j-1)

                   psic=rovw(i,j,k)
                   xC=grid_y(j)

                   psiu=rovw(i,j+1,k)
                   xU=grid_y(j+1)

                   xFa=grid_yv(j)
                endif

                call interpolate_HR(icase,psiu,psic,psid,psis,xC,xU,xD,xFa)

                wf=(dzw2(k+1)*w(i,j,k  )+dzw2(k  )*w(i,j,k+1))/dz(k  )
                if(wf>0) then
                   psiu=rovw(i,j,k-1)
                   xU=grid_zw(k-1)

                   psic=rovw(i,j,k)
                   xC=grid_zw(k)

                   psid=rovw(i,j,k+1)
                   xD=grid_zw(k+1)

                   xFa=grid_z(k)
                else
                   psid=rovw(i,j,k)
                   xD=grid_zw(k)

                   psic=rovw(i,j,k+1)
                   xC=grid_zw(k+1)

                   psiu=rovw(i,j,k+2)
                   xU=grid_zw(k+2)

                   xFa=grid_z(k)

                endif

                call interpolate_HR(icase,psiu,psic,psid,psif,xC,xU,xD,xFa)

                wb=(dzw2(k  )*w(i,j,k-1)+dzw2(k-1)*w(i,j,k  ))/dz(k-1)
                if(wb>0) then
                   psiu=rovw(i,j,k-2)
                   xU=grid_zw(k-2)

                   psic=rovw(i,j,k-1)
                   xC=grid_zw(k-1)

                   psid=rovw(i,j,k)
                   xD=grid_zw(k)

                   xFa=grid_z(k-1)
                else
                   psid=rovw(i,j,k-1)
                   xD=grid_zw(k-1)

                   psic=rovw(i,j,k)
                   xC=grid_zw(k)

                   psiu=rovw(i,j,k+1)
                   xU=grid_zw(k+1)

                   xFa=grid_z(k-1)
                endif

                call interpolate_HR(icase,psiu,psic,psid,psib,xC,xU,xD,xFa)
                
                Rw(i,j,k) = (ue*psie-uw*psiw)/dx(i)  &
                     &     +(vn*psin-vs*psis)/dy(j)  &
                     &     +(wf*psif-wb*psib)/dzw(k)
             end do
          end do
       end do
       
    case DEFAULT
       write(*,*) 'transport scheme does not exist'
       stop
    end select

  end subroutine Compute_Convective_Term3D

  !ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
  !c     Interpolate face values based on the CUI high resolution scheme
  !c     qf will be set as some function of qU, qC, and qD
  !ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
  !c
  subroutine interpolate_cui_hr_quantity(qU,qC,qD,qf)
    !c
    implicit none
    REAL(8),intent(in)     :: qU,qC,qD
    REAL(8), intent(inout) :: qf
    
    REAL(8)                :: ac,af

    !c     High-resolution scheme (HR)

    if (abs(qD - qU) <= EPS_INTERPOLATE_HR) then
       !-------------------------------------------
       ! default to upwinding
       !-------------------------------------------
       qf = qC
       !-------------------------------------------
    else

       ac = (qC - qU)/(qD - qU)
       
       if (ac > 0 .and. ac <= 2d0/13) then
          af = 3*ac
       else if (ac > 2d0/13 .and. ac <= 4d0/5) then
          af = ac*5d0/6+1d0/3
       else if (ac > 4d0/5 .and. ac <= 1) then
          af = 1
       else
          !-------------------------------------------
          ! default to upwinding
          !-------------------------------------------
          af = ac
          !-------------------------------------------
       endif
       qf = af*(qD - qU) + qU
    endif
    
  end subroutine interpolate_cui_hr_quantity

  subroutine sweno5_shala(phi,a,b,c,d,e)
    !*******************************************************************************
    use mod_Constants
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    real(8), intent(in) :: a,b,c,d,e
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    real(8)             :: q1,q2,q3
    real(8)             :: is1,is2,is3
    real(8)             :: alpha1,alpha2,alpha3,phi
    !-------------------------------------------------------------------------------

    q1 = a * d1p3  + b * d7p6m + c * d11p6
    q2 = b * d1p6m + c * d5p6  + d * d1p3
    q3 = c * d1p3  + d * d5p6  + e * d1p6m

    is1 = c13/12*( a - two*b + c )**2 + c3/12*( a - c4*b + c3*c )**2  + eps_weno
    is2 = c13/12*( b - two*c + d )**2 + c3/12*( d - b )**2            + eps_weno
    is3 = c13/12*( c - two*d + e )**2 + c3/12*( c3*c - c4*d + e )**2  + eps_weno

    alpha1 = d1p10/(is1+eps_weno)**2
    alpha2 = d6p10/(is2+eps_weno)**2
    alpha3 = d3p10/(is3+eps_weno)**2

    phi = alpha1 * q1 + alpha2 * q2 + alpha3 * q3
    phi = phi / ( alpha1 + alpha2 + alpha3 )

    return

  end subroutine sweno5_shala


  subroutine interpolate_HR(icase,qU,qC,qD,qf,xc,xu,xd,xF)
    use mod_Constants
    implicit none
    
    INTEGER, intent(in)    :: icase
    REAL(8), intent(in)    :: qU,qC,qD,xc,xu,xd,xF
    REAL(8), intent(inout) :: qf
    
    REAL(8)                :: ac,af,bc,bf


!!$    !c     High-resolution scheme (HR)
    
!!$    
    if (abs(qD - qU) <= EPS_INTERPOLATE_HR) then
       !c       default to upwinding
       qf = qC
    else

       ac = (qC - qU)/(qD - qU)
       bc = (xC - xU)/(xD - xU)
       !bf = (xf - xU)/(xD - xU)
       ! r=((1-aC)/aC)*(bC/(1-bC))
       bf=(xF-xU)/(xD - xU)

       select case(icase)
       case(1)
          ! !!!!!!!!!!!!!!!!!!!!! SMART!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!       
          if (0.0d0 .lt. ac .and. ac .lt.(bC/3) ) then
             af = -(bF*(1-3*bC+2*bF)/(bC*(bC-1)))*aC
          else if ((bC/3).le. ac .and. ac .le. ((bC/bF)*(1+bF-bC))) then
             af = ((bF*(bF-bC)/(1-bC))+aC*(bF*(bF-1)/(bC*(bC-1))))
          else if (((bC/bF)*(1+bF-bC)).lt. ac .and. ac .lt. 1.d0) then
             af = 1.0d0
          else
             af = ac
          endif
          qf = af*(qD - qU) + qU
       case(2)
          ! !!!!!!!!!!!!!!Soucoup!!!!!!!!!!!!!!
          if (0.0d0 .lt. ac .and. ac .lt.bC) then
             af = (bF/bC)*aC
          else if (bC.lt. ac .and. ac .lt. 1) then
             af = (bC-bF)/(bC-1)+aC*(bF-1)/(bC-1)
          else
             af = ac
          endif
          qf = af*(qD - qU) + qU
       case(3) 
          ! !!!!!!!!STOIC!!!!!!!!!!
          if (0.0d0 .lt. ac .and. ac .lt.(((bC-bF)*bC)/(bC+bF+(2*bF**2)-4*bF*bC))) then
             af = -(bF*(1-3*bC+2*bF)/(bC*(bC-1)))*aC
          else if ((((bC-bF)*bC)/(bC+bF+(2*bF**2)-4*bF*bC)).le. ac .and. ac .le. bC) then
             af = (bC-bF)/(bC-1)+aC*(bF-1)/(bC-1)
          else if (bC.lt. ac .and. ac .lt. ((bC/bF)*(1+bF-bC))) then
             af = (bF*(bF-bC)/(1-bC)+(bF*(bF-1)/(bC*(bC-1))))
          else if (((bC/bF)*(1+bF-bC)).lt. ac .and. ac .lt.1 ) then
             af = 1
          else
             af = ac
          endif
          qf = af*(qD - qU) + qU
       case(4)
          ! !!!!!!!!!!!!!!!!!!MUSCL!!!!!!!!!!!!!!
          if (0.0d0 .lt. ac .and. ac .lt.(bC/(2*bf)) ) then
             af = ((2*bf-bC)/bC)*ac
          else if ((bC/(2*bf)).le. ac .and. ac .le. 1+bC-bf) then
             af = bf-bC+ac
          else if (1+bC-bf.lt. ac .and. ac .lt. 1.d0) then
             af = 1.0d0
          else
             af = ac
          endif
          qf = af*(qD - qU) + qU
       case(5)
          ! !!!!!!!!!!!!!!!!!!!!!!!!!!!WECEB!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          if (0.0d0 .lt. ac .and. ac .lt.((bC*bF*(bF-bC))/(2*bC*(1-bC)-bF*(1-bF)))) then
             af = 2*aC
          else if (((bC*bF*(bF-bC))/(2*bC*(1-bC)-bF*(1-bF))).le. ac .and. ac .le.((bC/bF)*(1+bF-bC))) then
             af = (aC*(bF*(1-bF))/(bC*(1-bC))) + (bF*(bF-bC))/(1-bC)
          else if (((bC/bF)*(1+bF-bC)).lt. ac .and. ac .lt. 1.d0) then
             af = 1
          else
             af = ac
          endif
          qf = af*(qD - qU) + qU
       case(6)
          ! !!!!!!!!!!!!!!!!!!!!!!!!!!!CUBISTA!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!    
          if (0.0d0 .lt. ac .and. ac .lt.(d3p4*bC) ) then
             af = (1+(bF-bC)/(3*(1-bC)))*(bF/bC)*aC
          else if ((d3p4*bC).le. ac .and. ac .le. bC*(1+2*(bF-bC))/(2*bF-bC)) then
             af = aC*(bF*(1-bF)/(bC*(1-bC)))+(bF*(bF-bC)/(1-bC))
          else if (bC*(1+2*(bF-bC))/(2*bF-bC).lt. ac .and. ac .lt. 1.d0) then
             af = 1.0d0 - ((1-bF)/(2*(1-bC)))*(1-aC)
          else
             af = ac
          endif
          qf = af*(qD - qU) + qU
       end select
    end if
       
       
    return
  end subroutine interpolate_HR


end module mod_MomentumConserving
