!========================================================================
!**
!**   NAME       : THI.f90
!**
!**   AUTHOR     : Can Selcuk
!**                B. Trouette
!**
!**   FUNCTION   : module for MPI variables of FUGU software
!**
!**   DATES      : Version 1.0.0  : from : april, 2021
!**
!========================================================================

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

#include "precomp.h"
module mod_thi
!RESPECT
#if FFTW3 
  use mod_Parameters, only:                    &
       & nproc,rank,mpi_dim,slx,sly,slz,       &  
       & Periodic,dim,nx,ny,nz,gx,gy,gz,       &
       & ex,sx,ey,sy,ez,sz,                    &
       & sxu,exu,syu,eyu,szu,ezu,              &
       & sxv,exv,syv,eyv,szv,ezv,              &
       & sxw,exw,syw,eyw,szw,ezw,              &
       & exs,sxs,eys,sys,ezs,szs,              &
       & sxus,exus,syus,eyus,szus,ezus,        &
       & sxvs,exvs,syvs,eyvs,szvs,ezvs,        &
       & sxws,exws,syws,eyws,szws,ezws,        &
       & gex,gsx,gey,gsy,gez,gsz,              &
       & gsxu,gexu,gsyu,geyu,gszu,gezu,        &
       & gsxv,gexv,gsyv,geyv,gszv,gezv,        &
       & gsxw,gexw,gsyw,geyw,gszw,gezw,        &
       & gxs,gys,gzs,xmin,xmax,ymin,ymax,      &
       & zmin,zmax
  implicit none

contains
  
  subroutine thi3D(u,v,w)

#if FFTW3 
    use mod_mpi
    use ISO_C_BINDING
    implicit none
    include 'fftw3.f03'
#endif 
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable, intent(inout) :: u,v,w

#if FFTW3
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer(C_INT) :: i,j,k, gamma, nn, n
    complex(C_DOUBLE_COMPLEX), allocatable, dimension(:,:,:) :: in1, in2, in3, u1, u2, out1, out2, out3
    complex(C_DOUBLE_COMPLEX) :: im 
    real(C_DOUBLE) :: pi, qsq, AA,  theta, th1, th2, E, q, qq, sum_q, ncube, sqkxky, eps, kp
    real(C_DOUBLE), allocatable, dimension(:) ::  kx, ky, kz, rayon, Ereconstitue
    real(C_DOUBLE), dimension(:,:,:), allocatable :: ulocal, vlocal, wlocal, uscalar, vscalar, wscalar, utemp
    real(C_DOUBLE), allocatable, dimension(:,:,:) :: kappa 
    real(C_DOUBLE) :: d1, d2
    character(len=80) :: nom
    
    ! This routine generates a turbulent homogeneous and isotrope
    ! velocity field (thi). We construct the thi on a collocated N^3
    ! grid in sequential.  We then to broadcast these values
    ! to the other threads.

    ! init velocity components on all threads here
    u = 0; v = 0; w = 0

    ! give every thread the value of N
    N = nx
    ncube = N**3
    
    if (nx/=ny.or.nx/=nz.or.ny/=nz) then
       if (rank==0) then
          write(*,*) "YOU MUST HAVE nx = ny = nz !!!"
          write(*,*) "YOU MUST HAVE nx = ny = nz !!!"
          write(*,*) "YOU MUST HAVE nx = ny = nz !!!"
          write(*,*) "YOU MUST HAVE nx = ny = nz !!!"
          write(*,*) "YOU MUST HAVE nx = ny = nz !!!"
       end if
       call mpi_finalize(MPI_CODE)
       stop
    end if

    
    if (nproc==1) then
       if (rank==0) then
          write(*,*) "CASE IS DESIGNED FOR // ONLY"
          write(*,*) "CASE IS DESIGNED FOR // ONLY"
          write(*,*) "CASE IS DESIGNED FOR // ONLY"
          write(*,*) "CASE IS DESIGNED FOR // ONLY"
          write(*,*) "CASE IS DESIGNED FOR // ONLY"
          call system("touch done")
       end if
       call mpi_finalize(MPI_CODE)
       stop
    end if
    
    q = log(real(N,8)) / log(2d0)
    
    if (q/floor(q)-1 > 1d-8) then
       ! https://www.ritambhara.in/check-if-number-is-a-power-of-2/
       if (ibset(0, bit_size(n) - leadz(n))-N > N-ibset(0, bit_size(n) - leadz(n))/2) then
          if (rank==0) write(*,*) "le maillage le plus proche est nx=", ibset(0, bit_size(n) - leadz(n))/2-1
       else 
          if (rank==0) write(*,*) "le maillage le plus proche est nx=", ibset(0, bit_size(n) - leadz(n))-1
       end if
       call mpi_finalize(MPI_CODE)
       stop
    end if
    
    if (rank == 0) then

       write(*,*) "N=",N

       pi = 4.D0*ATAN(1.D0)
       im = (0d0, 1d0)
       
       ! Fourier space variables
       allocate (in1(0:N-1, 0:N-1, 0:N-1), in2(0:N-1, 0:N-1, 0:N-1), in3(0:N-1, 0:N-1, 0:N-1))
       allocate (out1(0:N-1, 0:N-1, 0:N-1), out2(0:N-1, 0:N-1, 0:N-1), out3(0:N-1, 0:N-1, 0:N-1))
       allocate (u1(0:N-1, 0:N-1, 0:N-1), u2(0:N-1, 0:N-1, 0:N-1), utemp(0:N-1, 0:N-1, 0:N-1))
       allocate (kx(0:N-1), ky(0:N-1), kz(0:N-1))
       allocate (kappa(0:N-1,0:N-1,0:N-1))

       qsq = 3; gamma = 4; AA = 0.1175d0; kp = 9; kx = 0; ky = 0; kz = 0;
       u1 = 0;  u2 = 0; 
       in1 = 0; in2 = 0; in3 = 0
       out1 = 0; out2 = 0; out3 = 0
       q = 0

       ! fftw imposes that negative wavenumbers are stored backwards from
       ! n/2+1 to N-1 such that -k corresponds to n-k we compute only the
       ! positive part and get the negative wavenumbers c from the complex
       ! conjugate of the positive wavenumbers
       
       do k=0, N-1
          kz(k) = computekiv2 (k,zmin,zmax,N) 
          do j=0, N-1 
             ky(j) = computekiv2 (j,ymin,ymax,N)        
             do i = 0, N/2
                kx(i) = computekiv2 (i,xmin,xmax,N) 
                kappa(i,j,k) = sqrt(kx(i)**2 + ky(j)**2 + kz(k)**2)
                sqkxky = sqrt(kx(i)**2 + ky(j)**2)

                ! generate a random number per discrete wavenumber
                call random_number(theta);
                theta = theta*2.*pi
                call random_number(th1);
                th1 = th1*2.*pi
                call random_number(th2);
                th2 = th2*2.*pi

                ! spectral density is defined for all kappa  
                E = 0.5d0*qsq*(kappa(i,j,k)**gamma)*exp(-0.5d0*real(gamma)*(kappa(i,j,k)/kp)**2)/(AA*kp**(gamma+1))

                if ((kappa(i,j,k) .eq. 0) .or. (sqkxky .eq. 0)) then
                   eps = 0.001
                else
                   eps = 0
                endif
                u1(i,j,k) = sqrt(E/(2.d0*pi*(kappa(i,j,k)**2) + eps))*cos(theta)*exp(im*th1)
                u2(i,j,k) = sqrt(E/(2.d0*pi*(kappa(i,j,k)**2) + eps))*sin(theta)*exp(im*th2)
                in1(i,j,k) = (u1(i,j,k)*ky(j)*kappa(i,j,k) + u2(i,j,k)*kx(i)*kz(k))/(eps + kappa(i,j,k)*sqkxky)
                in2(i,j,k) = (u2(i,j,k)*ky(j)*kz(k) - u1(i,j,k)*kx(i)*kappa(i,j,k))/(eps + kappa(i,j,k)*sqkxky)
                in3(i,j,k) = -u2(i,j,k)*sqkxky/(eps + kappa(i,j,k))
             enddo
          enddo
       enddo

       print*, 'computing now iffts ...'
       call fft3D_via_many_1D (in1,N,out1)
       call fft3D_via_many_1D (in2,N,out2)
       call fft3D_via_many_1D (in3,N,out3)
       
       ! still check it 
       q = 0
       qq =0
       do k=0,N-1
          do j = 0, N-1
             do i = 0, N-1
                q = q + imag(out1(i,j,k))**2
                qq = qq + imag(out2(i,j,k))**2
             enddo
          enddo
       enddo
       
       print*,' |imag(out1)|_L2 = ',sqrt(q)
       print*,' |imag(out2)|_L2 = ',sqrt(qq)
      
       allocate (ulocal(0:N-1,0:N-1,0:N-1), vlocal(0:N-1,0:N-1,0:N-1), wlocal(0:N-1,0:N-1,0:N-1))
       ulocal = 0; vlocal = 0; wlocal = 0;

       ! get only the real part of the Fourier transform 
       ulocal = realpart (out1)
       vlocal = realpart (out2)
       wlocal = realpart (out3)

       print*,'end of the initialization of the thi on thread 0'
       print*,'sending the velocity field through mpi now...'

       ! write down the Passot-pouquet spectrum for reference
       call passot_pouquet_spectrum (qsq, gamma, AA, int(kp), N, xmin, xmax)
       
       ! print*,'average on generated field for <u> =',  sum(ulocal)/N**3
       ! print*,'average on generated field for <v> =',  sum(vlocal)/N**3
       ! print*,'average on generated field for <w> =',  sum(wlocal)/N**3
    end if !endif thread 0

    ! allocate needed fields
    allocate(uscalar(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
    allocate(vscalar(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
    allocate(wscalar(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
    uscalar = 0; vscalar = 0; wscalar = 0;
   
    ! print*,'rank #, size(uscalar) = ', rank, size(uscalar)
    ! ulocal(gsx:gex, gsy:gey, gsz:gez), vlocal(gsx:gex, gsy:gey,
    ! gsz:gez), wlocal(gsx:gex, gsy:gey, gsz:gez) are allocated on
    ! thread 0 only
    call spread_vector_to_threads (ulocal,vlocal,wlocal,uscalar,vscalar,wscalar)

    ! ! print*,'rank #, ce = ', rank, ce
    ! ! d1 = sum (uscalar(sx:ce(1), sy:ce(2), sz:ce(3)))
    ! d1 = sum (uscalar)
    ! ! print*,'rank #, sum(uscalar) = ', rank, sum(uscalar)
    ! d2 = 0
    ! call MPI_ALLREDUCE (d1, d2,1, MPI_double_precision , MPI_sum , MPI_COMM_WORLD ,code)
    ! print*,'after spread -- rank = , sum(uscalar) = ',rank, d2

    ! d1 = sum (vscalar(sx:ce(1), sy:ce(2), sz:ce(3)))
    ! d2 = 0
    ! call MPI_ALLREDUCE (d1, d2,1, MPI_double_precision , MPI_sum , MPI_COMM_WORLD ,code)
    ! print*,'after spread -- rank = , sum(vscalar) = ',rank, d2

    ! d1 = sum (wscalar(sx:ce(1), sy:ce(2), sz:ce(3)))
    ! d2 = 0
    ! call MPI_ALLREDUCE (d1, d2,1, MPI_double_precision , MPI_sum , MPI_COMM_WORLD ,code)
    ! print*,'after spread -- rank = , sum(wscalar) = ',rank, d2

    ! we need to norm this velocity field because we want the characteristic velocity U' to be 1
    ! from P.Trontin 2010, we have U' \equiv \sqrt(\bar{u'^2}) = \sqrt{2/3 q} with
    ! q = Frac{1}{N^3}\sum_{ijk} \Frac{1}{2} u'_{ijk}^2 + v'_{ijk}^2 + w'_{ijk}^2    

    ! compute this q_norm
    sum_q = compute_q_norm (uscalar, vscalar, wscalar, N)
    if (rank==0) print*,'norm of the turbulent velocity field = ',sum_q

    ! norm the velocity field
    uscalar = uscalar/sum_q
    vscalar = vscalar/sum_q
    wscalar = wscalar/sum_q

    ! compute again here to check
    sum_q = compute_q_norm (uscalar,vscalar,wscalar,N)
    if (rank==0) print*,'norm of the velocity field after normalizing = ', sum_q

    ! affect values now
    u(sx:ex, sy:ey, sz:ez) = uscalar(sx:ex, sy:ey, sz:ez)
    v(sx:ex, sy:ey, sz:ez) = vscalar(sx:ex, sy:ey, sz:ez)
    w(sx:ex, sy:ey, sz:ez) = wscalar(sx:ex, sy:ey, sz:ez)
    
    ! sync the ghost cells for u,v,w and apply periodicity for values
    ! at boundaries
    call comm_mpi_uvw (u,v,w)

    call compute_long_trans_autocor(u,v,0)

    ! endif fftw3
#endif
  
  end subroutine thi3D

  subroutine spread_vector_to_threads (ulocal,vlocal,wlocal,uscalar,vscalar,wscalar)
    use mod_mpi
    implicit none

    real(8), dimension(:,:,:), allocatable,intent(inout) :: ulocal, vlocal, wlocal, uscalar, vscalar, wscalar
    integer, dimension(:,:), allocatable :: tab_indices
    integer, dimension(:), allocatable :: tab2_indices
    real(8), dimension(:), allocatable :: tabu, tabv, tabw, tab1u, tab1v, tab1w
    integer :: nn, ii, jj, kk, i, j, k, n_i, n_j, n_k, l
    
    ! this subroutine spreads a globally kwown field
    ! ulocal,vlocal,wlocal on thread = rank = 0 to the other treads
    ! via mpi. The results are in the fields uscalar,vscalar,wscalar
      
    ! allocate needed arrays
    allocate(tab_indices(6,nproc), tab2_indices(nproc))
    tab_indices = 0; tab2_indices = 0

    ! get the starting and ending local indices sx,ex,sy,ey,sz,ez and store them
    CALL MPI_ALLGATHER (sx,1,MPI_INTEGER,tab2_indices,1,MPI_INTEGER,MPI_COMM_WORLD,MPI_CODE)
    tab_indices(1,:) = tab2_indices
    
    CALL MPI_ALLGATHER (ex,1,MPI_INTEGER,tab2_indices,1,MPI_INTEGER,MPI_COMM_WORLD,MPI_CODE)
    tab_indices(2,:) = tab2_indices
       
    CALL MPI_ALLGATHER (sy,1,MPI_INTEGER,tab2_indices,1,MPI_INTEGER,MPI_COMM_WORLD,MPI_CODE)
    tab_indices(3,:)=tab2_indices
    
    CALL MPI_ALLGATHER (ey,1,MPI_INTEGER,tab2_indices,1,MPI_INTEGER,MPI_COMM_WORLD,MPI_CODE)
    tab_indices(4,:)=tab2_indices
  
    CALL MPI_ALLGATHER (sz,1,MPI_INTEGER,tab2_indices,1,MPI_INTEGER,MPI_COMM_WORLD,MPI_CODE)
    tab_indices(5,:)=tab2_indices
    
    CALL MPI_ALLGATHER (ez,1,MPI_INTEGER,tab2_indices,1,MPI_INTEGER,MPI_COMM_WORLD,MPI_CODE)
    tab_indices(6,:)=tab2_indices

    ! print*,'thread #, tab_indices(2,:) = ', rank, tab_indices(2,:)
    ! print*,'thread #, tab_indices(4,:) = ', rank, tab_indices(4,:)
    ! print*,'thread #, tab_indices(6,:) = ', rank, tab_indices(6,:)
    
    if (rank == 0) then
       ! on thread 0 we have all the fields ulocal,vlocal,wlocal globally so we directly affect them
       uscalar(sx:ex,sy:ey,sz:ez) = ulocal(sx:ex,sy:ey,sz:ez)
       vscalar(sx:ex,sy:ey,sz:ez) = vlocal(sx:ex,sy:ey,sz:ez)
       wscalar(sx:ex,sy:ey,sz:ez) = wlocal(sx:ex,sy:ey,sz:ez)
       
    endif
    
    do nn=2, nproc
       ! indexes of the thread we are dealing with
       n_i = tab_indices(2,nn) - tab_indices(1,nn) + 1
       n_j = tab_indices(4,nn) - tab_indices(3,nn) + 1
       n_k = tab_indices(6,nn) - tab_indices(5,nn) + 1

       if (rank == 0) then
          ! allocate single dimension arrays
          allocate(tabu(1:n_i*n_j*n_k), tabv(1:n_i*n_j*n_k), tabw(1:n_i*n_j*n_k))
          tabu = 999999999; tabv = 999999999; tabw = 999999999;
          
          ! print*,'thread 0 , sending to thread # size(tabu) = ', nn-1, size(tabu)

          ! print*, 'rank #, tab_indices(1,nn) = ',rank, tab_indices(1,nn)
          ! print*, 'rank #, tab_indices(2,nn) = ',rank, tab_indices(2,nn)
          ! wrap the 3D array on a single dimension array 
          do kk = tab_indices(5,nn), tab_indices(6,nn)
             do jj = tab_indices(3,nn), tab_indices(4,nn)
                do ii = tab_indices(1,nn), tab_indices(2,nn)

                   i=ii - tab_indices(1,nn)
                   j=jj - tab_indices(3,nn)
                   k=kk - tab_indices(5,nn)

                   ! wrap here
                   if (dim==2) then
                      l = i + j*n_i + 1
                   else
                      l = i + j*n_i + k*n_i*n_j + 1
                   end if

                   tabu(l) = ulocal(ii,jj,kk)
                   tabv(l) = vlocal(ii,jj,kk)
                   tabw(l) = wlocal(ii,jj,kk)

                end do
             end do
          end do

          ! send the single-dimension array to threads of rank > 0 
          call MPI_SEND(tabu,size(tabu),MPI_DOUBLE_PRECISION,nn-1,100,MPI_COMM_WORLD,code)
          call MPI_SEND(tabv,size(tabv),MPI_DOUBLE_PRECISION,nn-1,101,MPI_COMM_WORLD,code)
          call MPI_SEND(tabw,size(tabw),MPI_DOUBLE_PRECISION,nn-1,102,MPI_COMM_WORLD,code)
          deallocate(tabu,tabv,tabw)
 
       else  

          if (rank==nn-1) then

             ! allocate(tab1u((ex-sx+1)*(ey-sy+1)*(ez-sz+1)), tab1v((ex-sx+1)*(ey-sy+1)*(ez-sz+1)), tab1w((ex-sx+1)*(ey-sy+1)*(ez-sz+1)))
             allocate(tab1u(1:n_i*n_j*n_k), tab1v(1:n_i*n_j*n_k), tab1w(1:n_i*n_j*n_k))
             ! print*,'thread # size(tab1u) = ', rank, size(tab1u)
             tab1u = 999999999; tab1v = 999999999; tab1w = 999999999;
             
             ! receive the single-dimension array from thread of rank = 0 
             call MPI_RECV(tab1u,size(tab1u),MPI_DOUBLE_PRECISION,0,100,MPI_COMM_WORLD,MPI_STATUS_IGNORE,code)
             call MPI_RECV(tab1v,size(tab1v),MPI_DOUBLE_PRECISION,0,101,MPI_COMM_WORLD,MPI_STATUS_IGNORE,code)
             call MPI_RECV(tab1w,size(tab1w),MPI_DOUBLE_PRECISION,0,102,MPI_COMM_WORLD,MPI_STATUS_IGNORE,code)

             ! print*, 'rank #, tab_indices(1,nn) = ',rank, tab_indices(1,nn)
             ! print*, 'rank #, tab_indices(2,nn) = ',rank, tab_indices(2,nn)
             ! unwrap here the single-dimensional array in the 3D array
             do kk = tab_indices(5,nn), tab_indices(6,nn)
                do jj = tab_indices(3,nn), tab_indices(4,nn)
                   do ii = tab_indices(1,nn), tab_indices(2,nn)
                      i = ii - tab_indices(1,nn)
                      j = jj - tab_indices(3,nn)
                      k = kk - tab_indices(5,nn)

                      if (dim==2) then
                         l = i + j*n_i + 1
                      else
                         l = i + j*n_i + k*n_i*n_j + 1
                      end if

                      uscalar(ii,jj,kk) = tab1u(l)
                      vscalar(ii,jj,kk) = tab1v(l)
                      wscalar(ii,jj,kk) = tab1w(l)
                   end do
                end do
             end do
             ! print*,' rank # max l = ', rank, l
             deallocate(tab1u,tab1v,tab1w)
          end if
       end if
    end do

  end subroutine spread_vector_to_threads

  function compute_q_norm (u,v,w,N)
    use mod_mpi
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable, intent(in) :: u,v,w
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                               :: i,j,k
    real(8)                                               :: q, compute_q_norm
    integer, intent(in)                                   :: N    
  
    ! this function computes: q =
    ! sqrt{\Frac{2}{3}\Frac{1}{N^3}\sum_{ijk} \Frac{1}{2} u'_{ijk}^2 +
    ! v'_{ijk}^2 + w'_{ijk}^2}

    ! compute first q locally
    q = 0.
    do k=sz,ez
       do j=sy,ey
          do i=sx,ex
             q = q + u(i,j,k)**2 + v(i,j,k)**2 + w(i,j,k)**2
          enddo
       enddo
    enddo

    if (nproc > 1) then
       compute_q_norm = 0.

       ! sum in allthreads via allreduce mpi function
       call MPI_ALLREDUCE(q, compute_q_norm, 1, MPI_DOUBLE_PRECISION, MPI_SUM, comm3d, code)

       compute_q_norm = sqrt(2.d0*compute_q_norm/(6.d0*N**3))
    else
       compute_q_norm = q
       compute_q_norm = sqrt(2.*compute_q_norm/(6.*real(N)**3))
    endif
    return
  end function compute_q_norm

  subroutine gather_vector_from_threads (ulocal,vlocal,wlocal,uscalar,vscalar,wscalar)
    use mod_mpi
    implicit none
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable,intent(inout) :: ulocal, vlocal, wlocal, uscalar, vscalar, wscalar
    integer, dimension(:,:), allocatable :: tab_indices
    integer, dimension(:), allocatable :: tab2_indices
    real(8), dimension(:), allocatable :: tabu, tabv, tabw, tab1u, tab1v, tab1w
    integer :: nn, ii, jj, kk, i, j, k, n_i, n_j, n_k, l

    ! this subroutine gathers a distributed vector field
    ! uscalar,vscalar,wscalar on thread = rank = 0 in the fields
    ! ulocal,vlocal,wlocal.

    ! allocate needed arrays
    allocate(tab_indices(6,nproc), tab2_indices(nproc))
    tab_indices = 0; tab2_indices = 0

    ! get the starting and ending local indices sx,ex,sy,ey,sz,ez and store them
    CALL MPI_ALLGATHER (sx,1,MPI_INTEGER,tab2_indices,1,MPI_INTEGER,MPI_COMM_WORLD,MPI_CODE)
    tab_indices(1,:)=tab2_indices
    CALL MPI_ALLGATHER (ex,1,MPI_INTEGER,tab2_indices,1,MPI_INTEGER,MPI_COMM_WORLD,MPI_CODE)
    tab_indices(2,:)=tab2_indices
    
    CALL MPI_ALLGATHER (sy,1,MPI_INTEGER,tab2_indices,1,MPI_INTEGER,MPI_COMM_WORLD,MPI_CODE)
    tab_indices(3,:)=tab2_indices
    CALL MPI_ALLGATHER (ey,1,MPI_INTEGER,tab2_indices,1,MPI_INTEGER,MPI_COMM_WORLD,MPI_CODE)
    tab_indices(4,:)=tab2_indices

    CALL MPI_ALLGATHER (sz,1,MPI_INTEGER,tab2_indices,1,MPI_INTEGER,MPI_COMM_WORLD,MPI_CODE)
    tab_indices(5,:)=tab2_indices
    CALL MPI_ALLGATHER (ez,1,MPI_INTEGER,tab2_indices,1,MPI_INTEGER,MPI_COMM_WORLD,MPI_CODE)
    tab_indices(6,:)=tab2_indices
    
    if (rank == 0) then
       ulocal(sx:ex,sy:ey,sz:ez) = uscalar(sx:ex,sy:ey,sz:ez)
       vlocal(sx:ex,sy:ey,sz:ez) = vscalar(sx:ex,sy:ey,sz:ez)
       wlocal(sx:ex,sy:ey,sz:ez) = wscalar(sx:ex,sy:ey,sz:ez)
    endif
    
    do nn=2, nproc
       n_i=tab_indices(2,nn)-tab_indices(1,nn)+1
       n_j=tab_indices(4,nn)-tab_indices(3,nn)+1
       n_k=tab_indices(6,nn)-tab_indices(5,nn)+1

       if (rank == nn-1) then
          ! allocate single dimension arrays
          allocate(tabu(1:n_i*n_j*n_k), tabv(1:n_i*n_j*n_k),tabw(1:n_i*n_j*n_k) )
          
          ! wrap the local 3D array on a single dimension array 
          do kk=sz,ez
             do jj=sy,ey
                do ii=sx,ex

                   i=ii-sx
                   j=jj-sy
                   k=kk-sz
                   
                   ! wrap here
                   if (dim==2) then
                      l = i+j*n_i+1
                   else
                      l = i + j*n_i + k*n_i*n_j + 1
                   end if

                   tabu(l) = uscalar(ii,jj,kk)
                   tabv(l) = vscalar(ii,jj,kk)
                   tabw(l) = wscalar(ii,jj,kk)

                end do
             end do
          end do
     
          ! send the single-dimension array on thread of rank = 0
          call MPI_SEND(tabu,size(tabu),MPI_DOUBLE_PRECISION,0,100,MPI_COMM_WORLD,code)
          call MPI_SEND(tabv,size(tabv),MPI_DOUBLE_PRECISION,0,100,MPI_COMM_WORLD,code)
          call MPI_SEND(tabw,size(tabw),MPI_DOUBLE_PRECISION,0,100,MPI_COMM_WORLD,code)
          ! print*,'sending from rank data of size =', rank, size(tabu)
          deallocate(tabu,tabv,tabw)
          
       elseif (rank == 0) then

          ! allocate needed fields
          allocate(tab1u(1:n_i*n_j*n_k), tab1v(1:n_i*n_j*n_k), tab1w(1:n_i*n_j*n_k))
           
          ! receive the single-dimension array from thread of rank = nn - 1
          call MPI_RECV(tab1u,size(tab1u),MPI_DOUBLE_PRECISION,nn-1,100,MPI_COMM_WORLD,MPI_STATUS_IGNORE,code)
          call MPI_RECV(tab1v,size(tab1v),MPI_DOUBLE_PRECISION,nn-1,100,MPI_COMM_WORLD,MPI_STATUS_IGNORE,code)
          call MPI_RECV(tab1w,size(tab1w),MPI_DOUBLE_PRECISION,nn-1,100,MPI_COMM_WORLD,MPI_STATUS_IGNORE,code)
        
          ! unwrap here the single-dimensional array in the 3D array
          ! that is allocated with global indices
          do kk=tab_indices(5,nn),tab_indices(6,nn)
             do jj=tab_indices(3,nn),tab_indices(4,nn)
                do ii=tab_indices(1,nn),tab_indices(2,nn)
                   
                   i=ii-tab_indices(1,nn)
                   j=jj-tab_indices(3,nn)
                   k=kk-tab_indices(5,nn)

                   if (dim==2) then
                      l = i + j*n_i + 1
                   else
                      l = i + j*n_i + k*n_i*n_j + 1
                   end if
                   ulocal(ii,jj,kk) = tab1u(l)
                   vlocal(ii,jj,kk) = tab1v(l)
                   wlocal(ii,jj,kk) = tab1w(l)
                end do
             end do
          end do
          
          deallocate(tab1u,tab1v,tab1w)

       end if
    enddo
  end subroutine gather_vector_from_threads
  
  subroutine debug_init_can(u,v,w)
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable, intent(inout) :: u,v,w
    integer :: i,j,k
    do k=sz,ez
       do j=sy,ey
          do i=sx,ex
             u(i,j,k) = rank + 10
          enddo
       enddo
    enddo

  end subroutine debug_init_can

  subroutine compute_Ek_spectrum (u,v,w,it)
#if FFTW3 
    use mod_mpi
    use ISO_C_BINDING
    implicit none
    include 'fftw3.f03'
#endif 
    
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable, intent(inout) :: u,v,w
    integer, intent(in) :: it
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable :: ulocal, vlocal, wlocal
#if FFTW3 
    complex(C_DOUBLE_COMPLEX), allocatable, dimension(:,:,:) :: in1, in2, in3, out1, out2, out3
    real(C_DOUBLE) :: pi, ncube, um, vm, wm, q, d1, d2
    real(C_DOUBLE), allocatable, dimension(:) ::  kx, ky, kz, rayon, Ereconstitue
    real(C_DOUBLE), allocatable, dimension(:,:,:) :: Ek, kappa
    integer(C_INT), allocatable, dimension(:) :: nbreCouronne
    integer(C_INT):: N,i,j,k,nn
    character(len=80) :: nom
    type(C_PTR) :: plan1, plan2, plan3

    
    ! This routine computes the energy spectrum Ek(k) from the the
    ! velocity components in physical space

    ! As a fast solution we need to gather the distributed velocity
    ! field components u,v,w on thread 0 (this needs to be changed to
    ! a full mpi solution)

    ! give every thread the value of N
    N = nx
    ncube = N**3
    
    ! allocate needed fields on thread 0
    if (rank == 0) then
       allocate (ulocal(0:N-1, 0:N-1, 0:N-1), &
            vlocal(0:N-1, 0:N-1, 0:N-1), &
            wlocal(0:N-1, 0:N-1, 0:N-1))
       ulocal = 0; vlocal = 0; wlocal = 0
    endif

    pi = 4.D0*DATAN(1.D0)

    ! ! print*,'rank #, ce = ', rank, ce
    ! d1 = sum (u(sx:ce(1), sy:ce(2), sz:ce(3)))
    ! d2 = 0
    ! call MPI_ALLREDUCE (d1, d2,1, MPI_double_precision , MPI_sum , MPI_COMM_WORLD ,code)
    ! print*,'before gather -- rank = , sum(u) = ',rank, d2

    ! d1 = sum (v(sx:ce(1), sy:ce(2), sz:ce(3)))
    ! d2 = 0
    ! call MPI_ALLREDUCE (d1, d2,1, MPI_double_precision , MPI_sum , MPI_COMM_WORLD ,code)
    ! print*,'before gather -- rank = , sum(v) = ',rank, d2
    
    ! d1 = sum (w(sx:ce(1), sy:ce(2), sz:ce(3)))
    ! d2 = 0
    ! call MPI_ALLREDUCE (d1, d2,1, MPI_double_precision , MPI_sum , MPI_COMM_WORLD ,code)
    ! print*,'before gather -- rank = , sum(w) = ',rank, d2

    
    ! gather now u,v,w on thread 0 in the fields ulocal, vlocal,
    ! wlocal  
    call gather_vector_from_threads (ulocal,vlocal,wlocal,u,v,w)
 
    ! The following needs to be done only on thread 0
    if (rank == 0) then

       ! print*,'after u->ulocal gather -- rank = , sum(ulocal) = ', sum(ulocal)
       ! print*,'after v->vlocal gather -- rank = , sum(vlocal) = ', sum(vlocal)
       ! print*,'after w->wlocal gather -- rank = , sum(wlocal) = ', sum(wlocal)
      
       allocate (in1(0:N-1, 0:N-1, 0:N-1), in2(0:N-1, 0:N-1, 0:N-1), in3(0:N-1, 0:N-1, 0:N-1))
       allocate (out1(0:N-1, 0:N-1, 0:N-1), out2(0:N-1, 0:N-1, 0:N-1), out3(0:N-1, 0:N-1, 0:N-1))
       allocate (kx(0:N-1), ky(0:N-1), kz(0:N-1))
       allocate (Ek(0:N-1, 0:N-1, 0:N-1), kappa(0:N-1, 0:N-1, 0:N-1))
      
       kx = 0; ky = 0; kz = 0; Ek = 0; kappa = 0; Ek = 0
      
       plan1 = fftw_plan_dft_3d (N,N,N, in1, out1, FFTW_FORWARD, FFTW_ESTIMATE)
       plan2 = fftw_plan_dft_3d (N,N,N, in2, out2, FFTW_FORWARD, FFTW_ESTIMATE)
       plan3 = fftw_plan_dft_3d (N,N,N, in3, out3, FFTW_FORWARD, FFTW_ESTIMATE)

       in1 = 0; in2 = 0; in3 = 0; out1 = 0; out2 = 0; out3 = 0

       ! compute mean values and substract it from the instantaneous
       ! velocity to get the fluctuating part only
       um = sum(u)/ncube; vm = sum(v)/ncube; wm = sum(w)/ncube

       in1(:,:,:) = cmplx(ulocal(:,:,:) - um, 0.d0);
       in2(:,:,:) = cmplx(vlocal(:,:,:) - vm, 0.d0);
       in3(:,:,:) = cmplx(wlocal(:,:,:) - wm, 0.d0);

       ! compute the Fourier transforms
       call dfftw_execute_dft (plan1, in1, out1)
       call dfftw_execute_dft (plan2, in2, out2)
       call dfftw_execute_dft (plan3, in3, out3)
      
       ! fftw computes non-normalized transformations. In
       ! direct/forward ffts, one has to divide by N**3
       out1 = out1/ncube; out2 = out2/ncube; out3 = out3/ncube
       
       do k=0, N-1
          kz(k) = computekiv2 (k,zmin,zmax,N)  
          do j=0, N-1
             ky(j) = computekiv2 (j,ymin,ymax,N) 
             do i = 0, N-1
                kx(i) = computekiv2 (i,xmin,xmax,N) 
                kappa(i,j,k) = sqrt(kx(i)**2 + ky(j)**2 + kz(k)**2)
                Ek(i,j,k) = 2.d0*pi*(kappa(i,j,k)**2)&
                     *(abs(out1(i,j,k))**2 &
                     + abs(out2(i,j,k))**2 &
                     + abs(out3(i,j,k))**2)
             enddo
          enddo
       enddo

       allocate(nbreCouronne(0:N/2-1))
       allocate(rayon(0:N/2))
       allocate(Ereconstitue(0:N/2-1))

       ! get energy contribution from all wavenumbers
       do nn=0, N/2-1
          nbreCouronne(nn) = 0
          Ereconstitue(nn) = 0.D0
          rayon(nn) = computekiv2 (nn,xmin,xmax,N) 
          rayon(nn+1) = computekiv2 (nn+1,xmin,xmax,N)
          do k=0,N-1
             do j=0,N-1
                do i=0,N-1
                   if ((kappa(i,j,k) .ge. rayon(nn)) .and. (kappa(i,j,k) .lt. rayon(nn+1))) then
                      nbreCouronne(nn) = nbreCouronne(nn) + 1
                      Ereconstitue(nn) = Ereconstitue(nn) + Ek(i,j,k)
                   end if
                end do
             end do
          end do
       end do

       write(nom,'(A,I6.6,A)') 'spectre3d_',it,'.dat'
       open(unit=22,file=nom, status='replace')
       
       do nn=0,N/2-1
          if (nbreCouronne(nn) .ne. 0) then
             Ereconstitue(nn) = Ereconstitue(nn)/nbreCouronne(nn)
             write(22,'(3E13.5)') rayon(nn), Ereconstitue(nn), real(nbreCouronne(nn))
          end if
       end do
       
       print*, 'Initial kinetic inergy k-> Integral of E(kappa) = ', compute_integral_Ek(Ereconstitue, N, rayon)
       
       call flush(22)
      
       call dfftw_destroy_plan(plan1)
       call dfftw_destroy_plan(plan2)
       call dfftw_destroy_plan(plan3)
    endif !endif rank ==0
    !endif fftw3
#endif 
    
  end subroutine compute_Ek_spectrum
#if FFTW3 
!RESPECT
  function computekiv2 (i, Lmin, Lmax, N)
    use ISO_C_BINDING
    implicit none
#if FFTW3
    include 'fftw3.f03'
#endif 
    integer(C_INT), intent(in) :: i,N
    real(C_DOUBLE), intent(in) :: Lmin, Lmax
    real(C_double) :: computekiv2, pi
    pi = 4.D0*ATAN(1.D0)
    if (i .le. N/2) then
       computekiv2 =  2.d0*pi*i/(Lmax-Lmin)
    else
       computekiv2 =  2.d0*pi*(-N+i)/(Lmax-Lmin)
    endif
  end function computekiv2

  function compute_integral_Ek (Ereconstitue, N, rayon)
    integer, intent(in) :: N
    real(8), intent(in), dimension(0:N/2-1):: Ereconstitue
    real(8), intent(in), dimension(0:N/2):: rayon
    integer :: nn
    real(8) :: compute_integral_Ek, delta

    delta = rayon(1) - rayon(0)
    compute_integral_Ek = compute_integral_Simpson (Ereconstitue, delta, N/2-1)
    
    return 
  end function compute_integral_Ek
!RESPECT
#endif
#if FFTW3 
!RESPECT
  subroutine fft3D_via_many_1D (in1,N,out1)

    use ISO_C_BINDING
    implicit none
    include 'fftw3.f03'
    integer(C_INT), intent(in) :: N
    complex(C_DOUBLE_COMPLEX), allocatable, dimension(:) :: in1_1d, out1_1d
    complex(C_DOUBLE_COMPLEX), dimension(0:N-1,0:N-1,0:N-1),intent(in) :: in1
    complex(C_DOUBLE_COMPLEX), dimension(0:N-1,0:N-1,0:N-1),intent(out) :: out1
    integer(C_INT) :: i,j,k,ii,jj,kk
    type(C_PTR) :: plan1

    
    ! allocate needed 1d fields
    allocate (in1_1d(0:N-1), out1_1d(0:N-1))
    call  dfftw_plan_dft_1d (plan1, N, in1_1d, out1_1d, FFTW_BACKWARD, FFTW_ESTIMATE)
    
    do k = 0, N-1
       !transform in x direction
       do j = 0, N-1
          in1_1D(:) = in1(:,j,k)
          ! mode 0 and n/2 are complex conjugate pairs so they imaginary
          ! part are 0
          in1_1D(0) = cmplx(real(in1_1D(0)), 0d0)
          in1_1D(N/2) = cmplx(real(in1_1D(N/2)), 0d0)

          ! apply hermitian symmetry on coefficients
          do i = N/2+1, N-1
             ii = -i + N
             in1_1d(i) = conjg (in1_1d(ii))
          enddo
          call dfftw_execute_dft (plan1, in1_1d, out1_1d)
        
          ! store ifft, at this point we have \tilde{u}(x_i,ky_j,kz_k)
          out1(:,j,k) =  out1_1D(:)
       enddo

       ! transform in y direction
       do i = 0, N-1
          in1_1d(:) = out1(i,:,k)
          ! mode 0 and n/2 are complex conjugate pairs so they imaginary
          ! part are 0
          in1_1D(0) = cmplx(real(in1_1D(0)), 0d0)
          in1_1D(N/2) = cmplx(real(in1_1D(N/2)), 0d0)

          ! apply hermitian symmetry on coefficients
          do j = N/2+1, N-1
             jj = -j + N
             in1_1d(j) = conjg (in1_1d(jj))
          enddo
          call dfftw_execute_dft (plan1, in1_1d, out1_1d)
          
          ! store ifft, at this point we have \tilde{u}(x_i,y_j)
          out1(i,:,k) =  out1_1D(:)
       enddo
    enddo

    ! transform in the remainning z direction
    do i = 0, N-1
       do j = 0,N-1
          in1_1d(:) = out1(i,j,:)
          ! mode 0 and n/2 are complex conjugate pairs so they imaginary
          ! part are 0
          in1_1D(0) = cmplx(real(in1_1D(0)), 0d0)
          in1_1D(N/2) = cmplx(real(in1_1D(N/2)), 0d0)

          ! apply hermitian symmetry on coefficients
          do k = N/2+1, N-1
             kk = -k + N
             in1_1d(k) = conjg (in1_1d(kk))
          enddo
          call dfftw_execute_dft (plan1, in1_1d, out1_1d)

          ! store ifft, at this point we have \tilde{u}(x_i,y_j)
          out1(i,j,:) =  out1_1D(:)
       enddo
    enddo

    deallocate(in1_1d); deallocate(out1_1d);
    call dfftw_destroy_plan(plan1)

  end subroutine fft3D_via_many_1D
#endif
!RESPECT  
  subroutine compute_long_trans_autocor(u,v,it)
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable, intent(inout) :: u,v
    integer, intent(in) :: it
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable :: ulocal, vlocal, wlocal, work, work2
    real(8) :: usqmean, vsqmean,deltar, L_long, L_trans, lambda_long, lambda_trans

    integer :: i,j,k,dr,N
    real(8), dimension(:), allocatable :: fl, ft
    character(len=80) :: nom
    
    ! This routine computes the longitudinal and transverse
    ! autocorrelation functions (fl,ft). We follow here Pope's book
    ! (page 196 eq. 6.45)

    N = nx
    
    ! compute first <u_1^2> via mpi
    usqmean = 0
    allocate (work(sx:ex, sy:ey,sz:ez))
    work = u**2
    usqmean = compute_mean(work)
    work = v**2
    vsqmean = compute_mean(work)
    
    ! allocate needed fields on thread 0
    if (rank == 0) then
       allocate (ulocal(0:N-1, 0:N-1, 0:N-1), &
            vlocal(0:N-1, 0:N-1, 0:N-1))
       if (allocated(work)) then
          deallocate(work)
          allocate(work(0:N-1, 0:N-1, 0:N-1), work2(0:N-1, 0:N-1, 0:N-1))
          allocate(fl(0:N-1), ft(0:N-1))
       endif
       ulocal = 0; vlocal = 0; work = 0
    endif

    call gather_vector_from_threads (ulocal,vlocal,vlocal,u,v,v)

    if (rank == 0) then
    
       write (nom,'(A,I6.6,A)') 'auto_correlations_fl_ft_',it,'u.dat'
       open (unit=22,file=nom, status='replace')

       ! at this point thread 0 got whole u,v in ulocal and vlocal
       fl = 0; ft = 0
       do dr = 0, N-1
          deltar = real(dr)*(xmax-xmin)/real(N-1)
          work = 0; work2 = 0;
          do i = 0, N-dr-1
             work(i,:,:) = ulocal(i+dr,:,:)
             work2(i,:,:) = vlocal(i+dr,:,:)
          enddo
          work = work*ulocal
          work2 = work2*vlocal
          fl(dr) = compute_mean_seq(work,N)/usqmean
          ft(dr) = compute_mean_seq(work2,N)/vsqmean
          write(22,*) deltar, fl(dr), ft(dr)
       enddo
       
       ! perform integral to get longitudinal/transverse integral scale L_{11}, L_{22}
       deltar = (xmax-xmin)/real(N-1)
       L_long = compute_integral_Simpson (fl, deltar, N-2)
       L_trans = compute_integral_Simpson (ft, deltar, N-2)

       ! compute Taylor microscales \lambda_f = [-0.5*fl''(0)]^[-1/2]
       ! and \lambda_g = [-0.5*ft''(0)]^[-1/2]
       lambda_long = compute_second_order_derivative_upwind(fl(0:3), deltar)
       lambda_long = sqrt(2d0/(-lambda_long))

       lambda_trans = compute_second_order_derivative_upwind(ft(0:3), deltar)
       lambda_trans = sqrt(2d0/(-lambda_trans))
       
       ! write results
       write (nom,'(A,I6.6,A)') 'Integral_Taylor_Scales_L11_L22_lambda_f_lambda_g.dat'
       if (it == 0) then
             open (unit=22,file=nom, status='replace')
       else
          open (unit=22,file=nom, status="old", position="append", action="write")
       endif
       
       write (22,*) it, L_long, L_trans, lambda_long, lambda_trans, lambda_long/sqrt(2d0)
       
       deallocate (work,work2,fl,ft)

    endif
        
  end subroutine compute_long_trans_autocor

  function compute_mean (uu)
    use mod_mpi
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(sx:ex,sy:ey,sz:ez), intent(in) :: uu
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer :: i,j,k
    real(8) :: compute_mean, mean_per_tread
    compute_mean  = 0;  mean_per_tread = 0

    do k = sz, ez
       do j = sy, ey
          do i = sx, ex
             mean_per_tread = mean_per_tread + uu(i,j,k)
          enddo
       enddo
    enddo

    call MPI_ALLREDUCE (mean_per_tread, compute_mean, 1, MPI_double_precision, MPI_sum, MPI_COMM_WORLD, code)

    compute_mean = compute_mean/(nx*ny*nz)
    return
  end function compute_mean

  function compute_mean_seq (uu,N)
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(0:N-1,0:N-1,0:N-1), intent(in) :: uu
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer, intent(in) :: N
    integer :: i,j,k
    real(8) :: compute_mean_seq
    compute_mean_seq = 0;
    
    do k = 0, n-1
       do j = 0, n-1
          do i = 0, n-1
             compute_mean_seq = compute_mean_seq + uu(i,j,k)
          enddo
       enddo
    enddo

    compute_mean_seq = compute_mean_seq/(n*n*n)
    return
  end function compute_mean_seq

  function compute_integral_Simpson (E,h,N)

    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer, intent(in) :: N
    real(8), intent(in), dimension(0:N) :: E
    real(8), intent(in) :: h
    integer :: i,j
    real(8) :: compute_integral_Simpson

    ! this function computes the integral over the 1d interval that is
    ! splitted in N sub-intervals (i.e N+1 points). N has to be pair

    compute_integral_Simpson = E(0) + E(N)

    do i = 1, N/2
       ! get the odd values
       j = 2*i-1
       compute_integral_Simpson = compute_integral_Simpson + 4d0*E(2*i-1)
       ! get the pair values
       if (i .le. N/2-1) compute_integral_Simpson = compute_integral_Simpson + 2d0*E(2*i)
    enddo
    compute_integral_Simpson = compute_integral_Simpson*h/3d0
    return
  end function compute_integral_Simpson

  subroutine passot_pouquet_spectrum (qsq,gamma,AA,kp,N,Lmin,Lmax)
    implicit none
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer, intent(in) :: N, kp, gamma
    integer ::  i, j, k, nn
    integer, dimension(:), allocatable :: nbreCouronne
    real(8), intent(in) :: qsq, AA, Lmin, Lmax
    real(8), dimension(:,:,:), allocatable :: kappa, E
    real(8), dimension(:), allocatable :: kz, ky, kx, Ereconstitue, rayon
    character(len=80) :: nom
  
    
    allocate(kappa(0:N-1,0:N-1,0:N-1), E(0:N-1,0:N-1,0:N-1))
    allocate(kz(0:N-1), ky(0:N-1), kx(0:N-1))
    allocate(rayon(0:N/2), Ereconstitue(0:N/2-1), nbreCouronne(0:N/2-1))
    
    ! compute spectrum of Passot-pouquet E(kappa)
    do k = 0, N-1
       kz(k) = computekiv2 (k, Lmin, Lmax, N)
       do j = 0, N-1
          ky(j) = computekiv2 (j, Lmin, Lmax, N)
          do i = 0, N-1
             kz(i) = computekiv2 (i, Lmin, Lmax, N)
             kappa(i,j,k) = sqrt(kz(k)**2 + ky(j)**2 + kx(i)**2)
             E(i,j,k) = 0.5d0*qsq*(kappa(i,j,k)**gamma)*exp(-0.5d0*real(gamma)*(kappa(i,j,k)/kp)**2)/(AA*kp**(gamma+1))
          enddo
       enddo
    enddo
    
    
    ! get energy contribution from all wavenumbers
    do nn=0, N/2-1
       nbreCouronne(nn) = 0
       Ereconstitue(nn) = 0.D0
       rayon(nn) = computekiv2 (nn, Lmin, Lmax, N) 
       rayon(nn+1) = computekiv2 (nn+1, Lmin, Lmax, N)
       do k=0,N-1
          do j=0,N-1
             do i=0,N-1
                if ((kappa(i,j,k) .ge. rayon(nn)) .and. (kappa(i,j,k) .lt. rayon(nn+1))) then
                   nbreCouronne(nn) = nbreCouronne(nn) + 1
                   Ereconstitue(nn) = Ereconstitue(nn) + E(i,j,k)
                end if
             end do
          end do
       end do
    end do

    write(nom,'(A,I6.6,A)') 'spectre_Passot_Pouquet.dat'
    open(unit=22,file=nom, status='replace')

    do nn=0,N/2-1
       if (nbreCouronne(nn) .ne. 0) then
          Ereconstitue(nn) = Ereconstitue(nn)/nbreCouronne(nn)
          write(22,'(2E13.5)') rayon(nn), Ereconstitue(nn)
       end if
    end do

    deallocate(E, Ereconstitue, kappa, nbreCouronne, rayon)
  end subroutine passot_pouquet_spectrum

  function compute_second_order_derivative_upwind(f,h)
    real(8), intent(in) :: h
    real(8), intent(in), dimension(0:3) :: f
    real(8) :: compute_second_order_derivative_upwind

    ! this function computes the second order derivative at x_0 = 0 with
    ! an upwind scheme https://en.wikipedia.org/wiki/Finite_difference_coefficient
    ! 2 	−5 	4 	−1
    !35/12 	−26/3 	19/2 	−14/3 	11/12
    compute_second_order_derivative_upwind = 2d0*f(0) - 5d0*f(1) + 4d0*f(2) - f(3) 
    compute_second_order_derivative_upwind = compute_second_order_derivative_upwind/(h**2)

    return
  end function compute_second_order_derivative_upwind
#endif 
end module mod_thi
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

