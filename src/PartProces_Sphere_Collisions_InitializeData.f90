!===============================================================================
module Bib_VOFLag_Sphere_Collisions_InitializeData
  !===============================================================================
  contains
  !---------------------------------------------------------------------------  
  !> @author amine chadil
  ! 
  !> @brief cette routine initialise tous les champs de la structure vl ayant un
  !! rapport avec les collisions
  !
  !> @param[out] vl    : la structure contenant toutes les informations
  !! relatives aux particules   
  !---------------------------------------------------------------------------  
  subroutine Sphere_Collisions_InitializeData(vl)
    !===============================================================================
    !modules
    !===============================================================================
    !-------------------------------------------------------------------------------
    !Modules de definition des structures et variables => src/mod/module_...f90
    !-------------------------------------------------------------------------------
    use Module_VOFLag_ParticlesDataStructure
    use mod_Parameters,                       only : deeptracking,dx
    !-------------------------------------------------------------------------------

    !===============================================================================
    !declarations des variables
    !===============================================================================
    implicit none
    !-------------------------------------------------------------------------------
    !variables globales
    !-------------------------------------------------------------------------------
    type(struct_vof_lag), intent(inout)  :: vl
    !-------------------------------------------------------------------------------
    !variables locales 
    !-------------------------------------------------------------------------------
    integer                              :: np,nd,k
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'entree Sphere_Collisions_InitializeData'
    !-------------------------------------------------------------------------------

    if ((.not.vl%choc_lubri).and.(.not.vl%choc_sec).and.(.not.vl%choc_limit).and.vl%force_inter) then
       write(6,*) "warning : les forces interparticulaires sont actives mais aucun modele l'est"
    end if

    do np=2,vl%kpt  !pour l'instant on travaille avec des spheres de meme rayon
       if (abs(vl%objet(np)%sca(1)-vl%objet(1)%sca(1)).gt.1D-15) then
          stop "pour le calcul des collisions, toutes les particules doivent etre de la meme taille" 
       end if
    end do

    vl%lubri_eps_pm=minval(dx)/minval(vl%objet(:)%sca(1))
    vl%sec_distcri =  0.1d0*vl%objet(1)%sca(1) !mottaaf : 0.1d0*vl%objet(1)%sca(1)

    vl%lubri_lambda_pp      = 1.d0/(2.d0*vl%lubri_eps_pp+1D-40) - 9.0d0/20.0d0*log(vl%lubri_eps_pp) - (3.0d0/56.0d0)*vl%lubri_eps_pp*log(vl%lubri_eps_pp) + 1.346
    vl%lubri_lambda_pp_eps1 = 1.d0/(2.d0*vl%lubri_eps_1+1D-40)  - 9.0d0/20.0d0*log(vl%lubri_eps_1)  - (3.0d0/56.0d0)*vl%lubri_eps_1*log(vl%lubri_eps_1) + 1.346

    vl%lubri_lambda_pm      = 1.0d0/(vl%lubri_eps_pm+1D-40)      - 0.2*log(vl%lubri_eps_pm)          - (1.0d0/21.0d0)*vl%lubri_eps_pm*log(vl%lubri_eps_pm) + 0.9713
    vl%lubri_lambda_pm_eps1 = 1.0d0/(vl%lubri_eps_1+1D-40)        - 0.2d0*log(vl%lubri_eps_1)         - (1.0d0/21.d0)*vl%lubri_eps_1*log(vl%lubri_eps_1) + 0.9713



    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'sortie Sphere_Collisions_InitializeData'
    !-------------------------------------------------------------------------------
  end subroutine Sphere_Collisions_InitializeData

  !===============================================================================
end module Bib_VOFLag_Sphere_Collisions_InitializeData
!===============================================================================
  



