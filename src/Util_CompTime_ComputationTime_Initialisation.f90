!===============================================================================
module Bib_VOFLag_ComputationTime_Initialisation
    !===============================================================================
    contains
    !---------------------------------------------------------------------------  
    !> @author jorge cesar brandle de motta, amine chadil
    ! 
    !> @brief cette routine initialise les parametres d'impression du temps de calcul
    !---------------------------------------------------------------------------  
    subroutine ComputationTime_Initialisation(vl)
        !===============================================================================
        !modules
        !===============================================================================
        !-------------------------------------------------------------------------------
        !Modules de definition des structures et variables => src/mod/module_...f90
        !-------------------------------------------------------------------------------
        use Module_VOFLag_ParticlesDataStructure
        use Module_VOFLag_ComputationTime,        only : ctime_iter,ctime_total,ctime_n,ctime_file
        use mod_Parameters,                       only : deeptracking,rank
        !-------------------------------------------------------------------------------
        !Modules de subroutines de la librairie RESPECT => src/Bib_VOFLag_...f90
        !-------------------------------------------------------------------------------
        use Bib_VOFLag_ComputationTime_File_Creation
        !-------------------------------------------------------------------------------
        !===============================================================================
        !declarations des variables
        !===============================================================================
        implicit none
        !-------------------------------------------------------------------------------
        !variables globales
        !-------------------------------------------------------------------------------
        type(struct_vof_lag), intent(inout) :: vl
        !-------------------------------------------------------------------------------
        !variables locales 
        !-------------------------------------------------------------------------------
        integer                             :: ilen
        external ilen
        !-------------------------------------------------------------------------------

        !-------------------------------------------------------------------------------
        if (deeptracking) write(*,*) 'entree ComputationTime_Initialisation'
        !-------------------------------------------------------------------------------

        ctime_total = 0.0D0 ! le temps d'execution de la routine sur toute la simu
        ctime_iter  = 0.0D0 ! le temps d'execution de la routine sur l'iteration courante
        ctime_n = 7         ! le nombre de chrono que l'on veut lancer
        if (rank .eq. 0) then
          write(ctime_file,'(a)') 'ctime.dat' 
          call ComputationTime_File_Creation(ctime_file,vl)
        end if

        !-------------------------------------------------------------------------------
        if (deeptracking) write(*,*) 'sortie ComputationTime_Initialisation'
        !-------------------------------------------------------------------------------
    end subroutine ComputationTime_Initialisation

    !===============================================================================
end module Bib_VOFLag_ComputationTime_Initialisation
!===============================================================================
    

