#include "precomp.h"

module mod_struct_boundary

  type boundary_t
     integer :: left     = 0
     integer :: right    = 0
     integer :: top      = 0
     integer :: bottom   = 0
     integer :: backward = 0 
     integer :: forward  = 0
  end type boundary_t

end module mod_struct_boundary


module mod_struct_cfl

  type cfl_t
     logical :: active = .false.
     real(8) :: dt_new
     real(8) :: dt_old
     real(8) :: dt_min = -1
     real(8) :: dt_max = -1
     real(8) :: dt_obj
     real(8) :: val    = 5d-1
  end type cfl_t

end module mod_struct_cfl


module mod_struct_stl_file

  integer, parameter :: nObj_max = 10

  type stl_file_obj_t
     integer               :: icase  = 1
     real(8), dimension(3) :: angle  = 3.141592653589793238462643383279502884197169399d-12
     real(8), dimension(3) :: length = 0
     real(8), dimension(3) :: factor = 1
     real(8), dimension(3) :: center = 1d40
     real(8), dimension(3) :: min    = 1d40
     real(8), dimension(3) :: max    = 1d40
     character(len=100)    :: name   = ""
  end type stl_file_obj_t

  type stl_file_t
     integer                                   :: nObj = 0
     type(stl_file_obj_t), dimension(nObj_max) :: obj
  end type stl_file_t

end module mod_struct_stl_file


module mod_struct_init
  
  !---------------------------------------------------------------------
  ! Type for initialization cases
  !---------------------------------------------------------------------
  type init_case_t
     logical              :: active    = .false.
     integer              :: id        = 0
     integer              :: id_output = 0
     integer              :: n         = 0
     real(8)              :: radius    = 0
     real(8)              :: thick     = 0
     real(8),dimension(3) :: pos       = 0
     real(8),dimension(3) :: center    = 0
     real(8),dimension(3) :: normal    = 0
  end type init_case_t
  !---------------------------------------------------------------------
  
end module mod_struct_init

module mod_struct_Solver
  use mod_struct_boundary
  use mod_struct_init
#if MUMPS
  include 'dmumps_struc.h'
#endif

  !---------------------------------------------------------------------
  ! Type modelling
  !---------------------------------------------------------------------
  type equation_t
     integer           :: inertial             = 1
     logical           :: activate             = .false.
     logical           :: solve                = .true.
     logical           :: linear_term          = .false.
     logical           :: penalty_term         = .false.
     logical           :: source_term          = .false.
     logical           :: source_term_implicit = .false.
     type(boundary_t)  :: bound
     type(init_case_t) :: init
  end type equation_t
  
  type augmented_lagrangian_t
     integer                            :: method                ! number of augmented Lagrangian iterations done
     integer                            :: it_lag                ! number of augmented Lagrangian iterations done
     integer, dimension(:), allocatable :: it_solver             ! number of solver iterations done
     real(8), dimension(:), allocatable :: res_solver            ! solver residual
     real(8), dimension(:), allocatable :: div,divmax            ! norm of divergence of the solution
  end type augmented_lagrangian_t

  type solver_sca_t
     integer                            :: nb_coef_matrix        ! number of non zero matrix coefficient by line = kdv
     integer                            :: nb_matrix_element     ! number of matrix elements in CSR format = nps
     integer                            :: nb_diagonal_precond   ! number of diagonal used for preconditionning = ndd
     real(8)                            :: dt                    ! time step
     type(augmented_lagrangian_t)       :: output
     type(boundary_t)                   :: bound                 ! boundary conditions
  end type solver_sca_t

  type solver_ns_t
     integer                            :: nb_coef_matrix        ! number of non zero matrix coefficient by line = kdv
     integer                            :: nb_matrix_element     ! number of matrix elements in CSR format = nps
     integer                            :: nb_diagonal_precond   ! number of diagonal used for preconditionning = ndd
     integer                            :: nb_coef_matrix_pcd    ! ... for pressure convection diffusion operator
     integer                            :: nb_matrix_element_pcd ! ...
     real(8)                            :: dt                    ! time step
     type(augmented_lagrangian_t)       :: output                ! Output parameters of Navier-Stokes solving
     type(boundary_t)                   :: bound                 ! boundary conditions
  end type solver_ns_t

  type strct_hypre_t
     integer    :: npart         = 1      ! partition number always 1
     integer    :: MaxIt         = 1
     integer    :: it            = 0
     integer    :: nPreRelax     = 2
     integer    :: nPostRelax    = 2
     integer    :: maxLvL        = 2
     integer    :: rap           = 1
     integer    :: typeRelax     = 3
     integer    :: skipRelax     = 1
     integer    :: zeroGuess     = 0
     integer    :: printLvL      = 1
     integer    :: Log           = 1
     integer(8) :: matrix        = 0
     integer(8) :: matrixStorage = 0
     integer(8) :: solver        = 0
     integer(8) :: precon        = 0
     integer(8) :: graph         = 0
     integer(8) :: grid          = 0
     integer(8) :: structx       = 0
     integer(8) :: structy       = 0
     real(8)    :: res           = 0
     real(8)    :: tol           = 1d-4
  end type strct_hypre_t

  type strct_mumps_t
#if MUMPS 
     type(DMUMPS_STRUC) :: par
#else
     integer            :: par
#endif
     !-------------------------------------------------------------------------------
     ! choice of partitionner for MUMPS
     ! scotch = 3
     ! pord   = 4
     ! metis  = 5
     !-------------------------------------------------------------------------------
     integer            :: part = 5
     integer            :: mem  = 50 
  end type strct_mumps_t

  type solver_t
     integer                             :: dim
     integer                             :: rank
     integer                             :: type     = 1
     integer                             :: precond  = 1
     ! Eqs : 1:u, 2:v, 3:w, 0:p  and combination : 1230:uvwp, 123:uvw, etc ...
     integer                             :: eqs
     integer                             :: var
     integer                             :: ires     = 0
     integer                             :: it       = 0
     integer                             :: it_max   = 30
     integer                             :: it_res   = 0
     integer                             :: res_case = 2
     real(8)                             :: res      = 0 
     real(8)                             :: res_max  = 1d-6
     logical                             :: init     = .true.
     logical                             :: print    = .true.
     type(strct_hypre_t), dimension(0:3) :: hypre
     !type(strct_mumps_t)                 :: mumps
  end type solver_t

  type resol_ade_t
     real(8)           :: dt
     real(8)           :: phi_max
     type(boundary_t)  :: bound
     type(solver_t)    :: solver
  end type resol_ade_t

  type resol_poisson_t
     real(8)          :: phi_max
     type(boundary_t) :: bound
     type(solver_t)   :: solver 
  end type resol_poisson_t

  type resol_nvstks_t
     integer               :: method = 1
     integer               :: ila    = 1
     real(8)               :: dr     = 0
     real(8)               :: dt
     real(8)               :: div,div_max
     real(8)               :: p_max,u_max,v_max,w_max
     type(boundary_t)      :: bound
     type(solver_t)        :: solver
     type(resol_poisson_t) :: projection
  end type resol_nvstks_t

  type matrix_t
     integer                            :: impp = 0
     integer                            :: eqs  = -1
     integer                            :: kdv  = 0     ! per line element
     integer                            :: npt  = 0     ! unknowns
     integer                            :: nps  = 0     ! nb matrix element
     integer                            :: ndd  = 0     ! nb diagonal precond
     integer                            :: nic  = 0 
     integer, dimension(:), allocatable :: kic
     integer, dimension(:), allocatable :: icof
     integer, dimension(:), allocatable :: jcof
     real(8), dimension(:), allocatable :: coef
  end type matrix_t

  type system_t
     type(matrix_t)                         :: mat
     type(matrix_t)                         :: bp
     type(matrix_t)                         :: fuv
     type(matrix_t)                         :: fuvw
     type(matrix_t)                         :: pcd
     type(matrix_t)                         :: schur
     real(8), dimension(:,:,:), allocatable :: array3d_1
  end type system_t

end module mod_struct_Solver


module mod_struct_thermophysics

  type phase_t
     !------------------------------------------------------
     ! equation of state :
     ! --> 0,1 : color,liquid
     ! --> 2   : ideal gas, rho    = p / (r T)
     ! --> 3   : isotherm, rho     = p / (r T_0)
     ! --> -1  : compressible, xit = 1 / p
     !------------------------------------------------------
     integer       :: eos    = 0
     !------------------------------------------------------
     real(8)       :: rho    = 1
     real(8)       :: mu     = 1
     real(8)       :: xit    = 1
     real(8)       :: sigma  = 1
     real(8)       :: lambda = 1
     real(8)       :: cp     = 1
     real(8)       :: dif    = 1
     real(8)       :: r      = 1 ! specific gas constant
     !------------------------------------------------------
     ! beta and tpb for Boussinesq: rho * (1-beta(tp-tpb)) in rho g 
     !------------------------------------------------------
     real(8)       :: beta   = 0
     real(8)       :: tpb    = 0 
     !------------------------------------------------------
     ! type (overwrite all values according database)
     !------------------------------------------------------
     character(20) :: type   = ""
     !------------------------------------------------------
  end type phase_t

  type(phase_t), dimension(:), allocatable :: fluids

  namelist /nml_prop_fluids/ fluids

contains

  subroutine read_nml_prop_fluids
    implicit none

    integer :: i

    open(unit=10,file="data.in",status="old",action="read")
    read(10,nml=nml_prop_fluids)
    rewind(10)
    close(10)

    do i=1,size(Fluids(:)%type)
       select case(fluids(i)%type)
       case("")
       case("water","water-liquid")
          fluids(i)%rho    = 998.2d0
          fluids(i)%mu     = 1.003d-3
          fluids(i)%lambda = 6d-1
          fluids(i)%cp     = 4182
          fluids(i)%xit    = 4.6d-10
       case("air")
          fluids(i)%rho    = 1.225d0
          fluids(i)%mu     = 1.7894d-5
          fluids(i)%lambda = 2.42d-2
          fluids(i)%cp     = 1006.43d0
          fluids(i)%xit    = 1d-5            ! 1/p_ref (ideal gas at SPT)
       case default
          write(*,*) "Unknown material"
          write(*,*) "Check material properties"
          write(*,*) "STOP"
          stop 
       end select
    end do

  end subroutine read_nml_prop_fluids

end module mod_struct_thermophysics


module mod_struct_Particle_Tracking

  type particle_t
     integer               :: RK       = 2       ! choice of RK scheme order (1,2)
     integer               :: interpol = 2       ! type of the Lagrangian velocity interpolation
     integer, dimension(3) :: ijk                ! cell 
     real(8)               :: rho      = 1       ! density
     real(8)               :: mu       = 1       ! viscosity
     real(8)               :: m        = 1       ! mass
     real(8)               :: phi      = 1       ! scalar value (for exemple temperature)
     real(8)               :: time     = 0       ! associated time            
     real(8), dimension(2) :: radius   = 1       ! radius (step n and n-1)
     real(8), dimension(3) :: r        = 0       ! position 
     real(8), dimension(3) :: v        = 0       ! relative velocity
     real(8), dimension(3) :: u        = 0       ! absolute velocity
     real(8), dimension(3) :: u0       = 0       ! absolute velocity
     real(8), dimension(3) :: a        = 0       ! acceleration
     real(8), dimension(3) :: dl       = 0       ! associated (cubic) volume for Lagrangian transport
     logical               :: active   = .true.  ! particle active or not
     logical               :: new      = .false. ! new particle
  end type particle_t

  integer, dimension(3,2) :: bcs_case     = 1
  integer, dimension(3,2) :: bcs_case_sca = 1

  type(particle_t) :: LPartIn ! input fot lagrangian particles

  namelist /nml_prop_particles/ LPartIn

end module mod_struct_Particle_Tracking


module mod_struct_data

  !---------------------------------------------------------------------
  ! Type for file writing
  !---------------------------------------------------------------------
  type field_t
     logical :: uvw   = .true.
     logical :: pres  = .true.
     logical :: div   = .true.
     logical :: tp    = .true.
     logical :: phase = .true.
     logical :: obj   = .true.
     logical :: sca06 = .false.
     logical :: sca07 = .false.
     logical :: sca08 = .false.
     logical :: sca09 = .false.
     logical :: sca10 = .false.
     logical :: sca11 = .false.
     logical :: sca12 = .false.
     logical :: sca13 = .false.
     logical :: sca14 = .false.
     logical :: sca15 = .false.
     logical :: sca16 = .false.
     logical :: sca17 = .false.
     logical :: sca18 = .false.
     logical :: sca19 = .false.
     logical :: sca20 = .false.
  end type field_t

  type file_write_t
     integer       :: iter  = 0
     real(8)       :: dt    = 0
     real(8)       :: time  = 0
     logical       :: write = .false.
     type(field_t) :: write_field 
  end type file_write_t
  !---------------------------------------------------------------------

end module mod_struct_data


module mod_struct_grid

  type var_kind_t
     integer :: p
     integer :: u
     integer :: v
     integer :: w
  end type var_kind_t

  type index_kind_t
     type(var_kind_t) :: start 
     type(var_kind_t) :: global_start
     type(var_kind_t) :: end
     type(var_kind_t) :: global_end
  end type index_kind_t

  type index_t
     type(index_kind_t) :: index
  end type index_t

  type unknown_vars_t
     integer :: u
     integer :: v
     integer :: w
     integer :: uv
     integer :: uvw
     integer :: p
     integer :: uvwp
  end type unknown_vars_t

  type resolved_vars_t
     type(unknown_vars_t) :: tot
     type(unknown_vars_t) :: no_ghost
  end type resolved_vars_t

  type grid_t
     type(resolved_vars_t)              :: nb

     integer                            :: id = 0
     integer                            :: dim
     integer                            :: nx,ny,nz
     integer                            :: gx,gy,gz
     integer                            :: sx,ex,sy,ey,sz,ez
     integer                            :: gsx,gex,gsy,gey,gsz,gez
     integer                            :: sxu,exu,syu,eyu,szu,ezu
     integer                            :: sxv,exv,syv,eyv,szv,ezv
     integer                            :: sxw,exw,syw,eyw,szw,ezw
     integer                            :: gsxu,gexu,gsyu,geyu,gszu,gezu
     integer                            :: gsxv,gexv,gsyv,geyv,gszv,gezv
     integer                            :: gsxw,gexw,gsyw,geyw,gszw,gezw
     integer                            :: sxs,exs,sys,eys,szs,ezs
     integer                            :: sxus,exus,syus,eyus,szus,ezus
     integer                            :: sxvs,exvs,syvs,eyvs,szvs,ezvs
     integer                            :: sxws,exws,syws,eyws,szws,ezws

     logical                            :: active = .false.
     logical, dimension(3)              :: periodic

     real(8)                            :: xmin,xmax
     real(8)                            :: ymin,ymax
     real(8)                            :: zmin,zmax

     real(8), allocatable, dimension(:) :: x,xu
     real(8), allocatable, dimension(:) :: y,yv
     real(8), allocatable, dimension(:) :: z,zw
     real(8), allocatable, dimension(:) :: dx,dxu
     real(8), allocatable, dimension(:) :: dy,dyv
     real(8), allocatable, dimension(:) :: dz,dzw
     real(8), allocatable, dimension(:) :: ddx,ddxu
     real(8), allocatable, dimension(:) :: ddy,ddyv
     real(8), allocatable, dimension(:) :: ddz,ddzw
     real(8), allocatable, dimension(:) :: dx2,dxu2
     real(8), allocatable, dimension(:) :: dy2,dyv2
     real(8), allocatable, dimension(:) :: dz2,dzw2
  end type grid_t

end module mod_struct_grid


module mod_struct_interface_tracking

  type phase_field_t
     real(8):: W, W_sq, b, a_velo, init_val
  end type phase_field_t
  
end module mod_struct_interface_tracking
