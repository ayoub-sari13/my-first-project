module mod_solver_new_math
  use mod_timers

#define  OPTIM_MATVEKE_TRI_SUP 0

contains

  subroutine pscalee (psa,qsa,spa,npt,kic,nic)
    use mod_mpi
    implicit none
    !--------------------------------------------------------------------------
    ! Global variables
    !--------------------------------------------------------------------------
    integer, intent(in)                 :: npt,nic
    integer, dimension(npt)             :: kic
    real(8), dimension(npt), intent(in) :: psa,qsa
    real(8), intent(out)                :: spa
    !--------------------------------------------------------------------------
    ! Local variables
    !--------------------------------------------------------------------------
    integer                             :: l,n
    real(8)                             :: lspa
    !--------------------------------------------------------------------------

    call compute_time(TIMER_START,"[sub] pscalee")

    !--------------------------------------------------------------------------
    lspa = 0
    do n = 1,nic
       l = kic(n)
       lspa = lspa + psa(l) * qsa(l)
    enddo
    !--------------------------------------------------------------------------
    call mpi_allreduce(lspa,spa,1,mpi_double_precision,mpi_sum,comm3D,code)
    !--------------------------------------------------------------------------

    call compute_time(TIMER_END,"[sub] pscalee")

  end subroutine pscalee


  subroutine pscalee5x (psa,qsa,rsa,spa1,spa2,spa3,spa4,spa5,npt,kic,nic)
    !*******************************************************************************
    use mod_mpi
    implicit none
    !--------------------------------------------------------------------------
    ! Global variables
    !--------------------------------------------------------------------------
    integer, intent(in)                 :: npt,nic
    integer, dimension(npt)             :: kic
    real(8), dimension(npt), intent(in) :: psa,qsa,rsa
    real(8), intent(out)                :: spa1,spa2,spa3,spa4,spa5
    !--------------------------------------------------------------------------
    ! Local variables
    !--------------------------------------------------------------------------
    integer                             :: l,n
    real(8)                             :: lspa1,lspa2,lspa3,lspa4,lspa5
    !--------------------------------------------------------------------------

    call compute_time(TIMER_START,"[sub] pscalee5x")

    !--------------------------------------------------------------------------
    lspa1 = 0
    lspa2 = 0
    lspa3 = 0
    lspa4 = 0
    lspa5 = 0
    !--------------------------------------------------------------------------
    do n = 1,nic
       l = kic(n)
       lspa1 = lspa1 + psa(l) * qsa(l)
       lspa2 = lspa2 + qsa(l) * qsa(l)
       lspa3 = lspa3 + qsa(l) * rsa(l)
       lspa4 = lspa4 + rsa(l) * rsa(l)
       lspa5 = lspa5 + psa(l) * rsa(l)
    enddo
    !--------------------------------------------------------------------------
    call mpi_allreduce(lspa1,spa1,1,mpi_double_precision,mpi_sum,comm3D,code)
    call mpi_allreduce(lspa2,spa2,1,mpi_double_precision,mpi_sum,comm3D,code)
    call mpi_allreduce(lspa3,spa3,1,mpi_double_precision,mpi_sum,comm3D,code)
    call mpi_allreduce(lspa4,spa4,1,mpi_double_precision,mpi_sum,comm3D,code)
    call mpi_allreduce(lspa5,spa5,1,mpi_double_precision,mpi_sum,comm3D,code)
    !--------------------------------------------------------------------------

    call compute_time(TIMER_END,"[sub] pscalee5x")

  end subroutine pscalee5x


  subroutine matveke (mat,sol,ysa,solver,grid)
    use mod_struct_grid
    use mod_struct_solver
    use mod_solver_new_num
    implicit none
    !--------------------------------------------------------------------------
    ! Global variables
    !--------------------------------------------------------------------------
    type(matrix_t),              intent(in)    :: mat
    type(solver_t),              intent(in)    :: solver
    type(grid_t),                intent(in)    :: grid 
    real(8), dimension(mat%npt), intent(inout) :: sol
    real(8), dimension(mat%npt), intent(out)   :: ysa
    !--------------------------------------------------------------------------
    ! Local variables
    !--------------------------------------------------------------------------
    integer                                    :: i,k
    real(8)                                    :: t
    !--------------------------------------------------------------------------

    call compute_time(TIMER_START,"[sub] matveke")

    ysa=0

    !--------------------------------------------------------------------------
    call solver_comm_mpi(sol,solver,grid)
    !--------------------------------------------------------------------------
    do i = 1,mat%npt
       t = 0
       do k = mat%icof(i),mat%icof(i+1)-1
          t = t + mat%coef(k)*sol(mat%jcof(k))
       enddo
       ysa(i) = t
    enddo
    !--------------------------------------------------------------------------

    call compute_time(TIMER_END,"[sub] matveke")

  end subroutine matveke


  subroutine matveke_tri_sup (mat,npt,sol,ysa,solver,grid)
    use mod_struct_grid
    use mod_struct_solver
    use mod_solver_new_num
    implicit none
    !--------------------------------------------------------------------------
    ! Global variables
    !--------------------------------------------------------------------------
    type(matrix_t),                     intent(in)  :: mat
    type(solver_t),                     intent(in)  :: solver
    type(grid_t),                       intent(in)  :: grid 
    real(8), allocatable, dimension(:), intent(in)  :: sol
    real(8), dimension(npt),            intent(out) :: ysa
    integer,                            intent(in)  :: npt
    !--------------------------------------------------------------------------
    ! Local variables
    !--------------------------------------------------------------------------
    integer                                         :: i,k
    real(8)                                         :: t
    !--------------------------------------------------------------------------

    call compute_time(TIMER_START,"[sub] matveke_tri_sup")

    ysa = 0

    !--------------------------------------------------------------------------
#if OPTIM_MATVEKE_TRI_SUP
    do i = 1,npt
       t = 0
       do k = mat%icof(i),mat%icof(i+1)-1
          if (mat%jcof(k)>0) then
             t = t + mat%coef(k)*sol(mat%jcof(k))
          endif
       enddo
       ysa(i) = t
    enddo
#else
    do i = 1,npt
       t = 0
       do k = mat%icof(i),mat%icof(i+1)-1
          if (mat%jcof(k)<=0) then
             t = t
          else
             t = t + mat%coef(k)*sol(mat%jcof(k))
          endif
       enddo
       ysa(i) = t
    enddo
#endif 
    !--------------------------------------------------------------------------

    call compute_time(TIMER_END,"[sub] matveke_tri_sup")

  end subroutine matveke_tri_sup
  
end module mod_solver_new_math
