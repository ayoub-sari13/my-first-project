!===============================================================================
module Particle_Position
  !===============================================================================
  contains
  !-----------------------------------------------------------------------------  
  !> @author 
  ! 
  !> @brief 
  !
  !> @param[]            
  !-----------------------------------------------------------------------------
  subroutine Particle_Position_Calculation(u,v,w,cou,nt,time)
    !===============================================================================
    !modules
    !===============================================================================
    !-------------------------------------------------------------------------------
    !Modules de definition des structures et variables => src/mod/module_...f90
    !-------------------------------------------------------------------------------
    use mod_Parameters,                      only : deeptracking,grid_x,grid_y,grid_z,&
                                                    dx,dy,dz 
    use mod_mpi
    !-------------------------------------------------------------------------------
    !===============================================================================
    !declarations des variables
    !===============================================================================
    implicit none
    !-------------------------------------------------------------------------------
    !variables globales
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable, intent(in)    :: u,v,w,cou
    real(8)                               , intent(in)    :: time
    integer                               , intent(in)    :: nt
    !-------------------------------------------------------------------------------
    !variables locales 
    !-------------------------------------------------------------------------------
    real(8)                             :: cdm_numx,cdm_numy,cdm_numz,cdm_denX
    real(8)                             :: gcdm_numx,gcdm_numy,gcdm_numz,gcdm_denX
    real(8)                             :: cdm_numu,cdm_numv,cdm_numw,cdm_denV
    real(8)                             :: gcdm_numu,gcdm_numv,gcdm_numw,gcdm_denV
    real(8)                             :: X_Sphere,Y_Sphere,Z_Sphere
    real(8)                             :: U_Sphere,V_Sphere,W_Sphere
    integer                             :: nd,np,i,j,k
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'entree Particle_Position_Calculation'
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Initialisation
    !-------------------------------------------------------------------------------
    cdm_numx=0d0;cdm_numu=0d0
    cdm_numy=0d0;cdm_numv=0d0
    cdm_numz=0d0;cdm_numw=0d0
    cdm_denX=0d0;cdm_denV=0d0

    if (dim==2) then 
        do j=sy,ey
            do i=sx,ex
                cdm_numx = cdm_numx+cou(i,j,1)*grid_x(i)*dx(i)*dy(j)
                cdm_numy = cdm_numy+cou(i,j,1)*grid_y(j)*dx(i)*dy(j)
                cdm_denX = cdm_denX+cou(i,j,1)*dx(i)*dy(j)
            enddo
        enddo
         do j=sy,ey
            do i=sx,ex
                cdm_numu=cdm_numu+0.5d0*(u(i,j,1)+u(i+1,j,1))*cou(i,j,1)*dx(i)*dy(j)
                cdm_numv=cdm_numv+0.5d0*(v(i,j,1)+v(i,j+1,1))*cou(i,j,1)*dx(i)*dy(j)
                cdm_denV=cdm_denV+cou(i,j,1)*dx(i)*dy(j)
            enddo
         enddo
    else 
        do k=sz,ez
          do j=sy,ey
            do i=sx,ex
                cdm_numx = cdm_numx+cou(i,j,k)*grid_x(i)*dx(i)*dy(j)*dz(k)
                cdm_numy = cdm_numy+cou(i,j,k)*grid_y(j)*dx(i)*dy(j)*dz(k)
                cdm_numz = cdm_numz+cou(i,j,k)*grid_z(k)*dx(i)*dy(j)*dz(k)
                cdm_denX = cdm_denX+cou(i,j,k)*dx(i)*dy(j)*dz(k)
            enddo
          enddo
        enddo 
        do k=sz,ez
         do j=sy,ey
            do i=sx,ex
                cdm_numu=cdm_numu+0.5d0*(u(i,j,k)+u(i+1,j,k))*cou(i,j,k)*dx(i)*dy(j)*dz(k)
                cdm_numv=cdm_numv+0.5d0*(v(i,j,k)+v(i,j+1,k))*cou(i,j,k)*dx(i)*dy(j)*dz(k)
                cdm_numw=cdm_numw+0.5d0*(w(i,j,k)+w(i,j,k+1))*cou(i,j,k)*dx(i)*dy(j)*dz(k)
                cdm_denV=cdm_denV+cou(i,j,k)*dx(i)*dy(j)*dz(k)
            enddo
         enddo
        enddo 
    end if 
    if (nproc>1) then
        call mpi_allreduce(cdm_numx,gcdm_numx,1,mpi_double_precision,mpi_sum,comm3d,code)
        call mpi_allreduce(cdm_numy,gcdm_numy,1,mpi_double_precision,mpi_sum,comm3d,code)
        call mpi_allreduce(cdm_denX,gcdm_denX,1,mpi_double_precision,mpi_sum,comm3d,code)
        call mpi_allreduce(cdm_numu,gcdm_numu,1,mpi_double_precision,mpi_sum,comm3d,code)
        call mpi_allreduce(cdm_numv,gcdm_numv,1,mpi_double_precision,mpi_sum,comm3d,code)
        call mpi_allreduce(cdm_denV,gcdm_denV,1,mpi_double_precision,mpi_sum,comm3d,code)
        if (dim==3) call mpi_allreduce(cdm_numz,gcdm_numz,1,mpi_double_precision,mpi_sum,comm3d,code)
        if (dim==3) call mpi_allreduce(cdm_numw,gcdm_numw,1,mpi_double_precision,mpi_sum,comm3d,code)
    end if                         
    X_Sphere = gcdm_numx/gcdm_denX
    Y_Sphere = gcdm_numy/gcdm_denX
    if (dim==3) Z_Sphere = gcdm_numz/gcdm_denX  
    
    U_Sphere = gcdm_numu/gcdm_denV
    V_Sphere = gcdm_numv/gcdm_denV
    if (dim==3) W_Sphere = gcdm_numw/gcdm_denV
    if ( rank == 0) then 
        open(unit=1000,file='data_part_00001.dat',status='old',position='append')
        write(1000,'(4E13.5)',advance='no')  dfloat(nt),time,X_Sphere,Y_Sphere
        if (dim==3) write(1000,'(E13.5)' ,advance='no') Z_Sphere
        write(1000,'(2E13.5)',advance='no') U_Sphere,V_Sphere
        if (dim==3) write(1000,'(E13.5)' ,advance='no') W_Sphere
      close(1000)
    end if 
    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'sortie Particle_Position_Calculation'
    !-------------------------------------------------------------------------------
  end subroutine Particle_Position_Calculation

  !===============================================================================
end module Particle_Position
!===============================================================================