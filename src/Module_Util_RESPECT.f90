!-----------------------------------------------------------------------------  
!> @author amine chadil
!
!> @brief ce module contient les donnees pour le lit fluidise avec paroi cylindrique
!
!> @date 4/04/2013   
!-----------------------------------------------------------------------------
!===============================================================================
module Module_VOFLag_FluidizedBedCylinder 
  !===============================================================================
  logical,save :: cylindre= .false.
  double precision,save :: cylindre_c_x1=0.d0, cylindre_c_x3=0.d0, rayon_cylindre=0.d0
  double precision, dimension(:),allocatable::smv_n,smc_n

  namelist /nml_RESPECT/ cylindre,rayon_cylindre,cylindre_c_x1,cylindre_c_x3
  !===============================================================================
end module Module_VOFLag_FluidizedBedCylinder
!===============================================================================



!-----------------------------------------------------------------------------  
!> @author amine chadil
!
!> @brief ce module contient les donnees l'initialisation des ecoulements
!
!> @date 24/08/2022   
!-----------------------------------------------------------------------------
!===============================================================================
module Module_VOFLag_Flows
  !===============================================================================
  logical, save :: uniform_past_part= .false.
  real(8), save :: U_inf=0.d0
  integer, save :: direction=1
  namelist /nml_uniform_past_part/ uniform_past_part,U_inf,direction

  logical,save :: uniform_past_cylinder_Strouhal= .false.
  namelist /nml_uniform_past_cylinder_Strouhal/ uniform_past_cylinder_Strouhal,U_inf

  logical,save :: cooling_sphere= .false.
  double precision,save :: ParticleTemperature=0.d0, FluideTemperature=0.d0
  namelist /nml_cooling_sphere/ cooling_sphere,ParticleTemperature
  !===============================================================================
end module Module_VOFLag_Flows
!===============================================================================