!===============================================================================
module Bib_VOFLag_Particle_LimitedSubDomain_On
  !===============================================================================
  contains
  !-----------------------------------------------------------------------------  
  !> @author amine chadil
  ! 
  !> @brief cette routine renseigne sur l'etat de la copie nbt de la particle np par rapport 
  !! au domaine  du proc courant sans ses mailles de recouverement (la routine dit si 
  !! la particle touche ou pas le domaine)  
  !
  !> @param[in]   vl     : la structure contenant toutes les informations
  !! relatives aux particules
  !> @param[in]   np     : numero de la particule dans vl
  !> @param[in]   nbt    : numero de la copie de la particule np
  !> @param [out] on_int : vrai si la particle intersecte le domaine faux sinon.
  !-----------------------------------------------------------------------------
  subroutine Particle_LimitedSubDomain_On(vl,np,nbt,on_int)
    !===============================================================================
    !modules
    !===============================================================================
    !-------------------------------------------------------------------------------
    !Modules de definition des structures et variables => src/mod/module_...f90
    !-------------------------------------------------------------------------------
    use Module_VOFLag_ParticlesDataStructure
    use mod_Parameters,                       only : deeptracking
    !-------------------------------------------------------------------------------
    !Modules de subroutines de la librairie RESPECT => src/Bib_VOFLag_...f90
    !-------------------------------------------------------------------------------
    use Bib_VOFLag_Sphere_LimitedSubDomain_On
    !use Bib_VOFLag_Ellipsoid_LimitedSubDomain_On
    !use Bib_VOFLag_AnyShape_LimitedSubDomain_On
    !-------------------------------------------------------------------------------

    !===============================================================================
    !declarations des variables
    !===============================================================================
    implicit none
    !-------------------------------------------------------------------------------
    !variables globales
    !-------------------------------------------------------------------------------
    type(struct_vof_lag), intent(inout)  :: vl
    integer,              intent(in)     :: np,nbt
    logical,              intent(out)    :: on_int
    !-------------------------------------------------------------------------------
    !variables locales 
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'entree Particle_LimitedSubDomain_On'
    !-------------------------------------------------------------------------------
    
    select case(vl%nature)
    case(1)
      !-------------------------------------------------------------------------------
      ! Si les particules sont spherique
      !-------------------------------------------------------------------------------
      call Sphere_LimitedSubDomain_On(vl,np,nbt,on_int)
    case(2)
       !-------------------------------------------------------------------------------
       ! Si les particules sont ellipsoidale
       !-------------------------------------------------------------------------------
       !call Ellipsoid_LimitedSubDomain_On(vl,np,nbt,on_int)
       write(*,*) "STOP : NATURE_PARTICULES doit etre Spherique"

    case(3)
       !-------------------------------------------------------------------------------
       ! Si les particules de formes quelconques
       !-------------------------------------------------------------------------------
       !call AnyShape_LimitedSubDomain_On(vl,np,nbt,on_int)
       write(*,*) "STOP : NATURE_PARTICULES doit etre Spherique"

    case default
      write(*,*) "STOP : NATURE_PARTICULES doit etre 1,2 ou 3"
      stop
    end select
    
    !-------------------------------------------------------------------------------
    !if (deeptracking) write(*,*) 'sortie Particle_LimitedSubDomain_On'
    !-------------------------------------------------------------------------------
  end subroutine Particle_LimitedSubDomain_On

  !===============================================================================
end module Bib_VOFLag_Particle_LimitedSubDomain_On
!===============================================================================
  


