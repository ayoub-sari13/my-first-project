!========================================================================
!
!                   FUGU Version 3.0.0
!
!========================================================================
!**
!**   NAME        : Search.f90
!**
!**   AUTHOR      : Benoît Trouette
!**
!**   FUNCTION    : Searching functions
!**
!**   DATES       : Version 1.0.0  : from : December, 2018
!**
!========================================================================

module mod_search

  integer, parameter :: PRESS_MESH = 0
  integer, parameter :: U_VEL_MESH = 1
  integer, parameter :: V_VEL_MESH = 2
  integer, parameter :: W_VEL_MESH = 3

contains

  subroutine search_cell_ijk(mesh,xyz,ijk,code,found)
    !*******************************************************************************
    ! Modules                                                                     
    !*******************************************************************************
    use mod_struct_grid
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    integer                              :: code
    integer, dimension(3), intent(inout) :: ijk
    real(8), dimension(3), intent(in)    :: xyz
    logical, optional, intent(inout)     :: found
    type(grid_t), intent(in)             :: mesh
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                              :: i
    integer                              :: is,ie
    integer                              :: isearch=2
    logical                              :: lfound
    !-------------------------------------------------------------------------------

    if (present(found)) found = .true.
    
    select case(code)
       !-------------------------------------------------------------------------------
       ! pressure/scalar mesh
       !-------------------------------------------------------------------------------
    case(PRESS_MESH)
       !-------------------------------------------------------------------------------
       ! x direction
       !-------------------------------------------------------------------------------
       lfound = .false.
       is    = ijk(1)-isearch
       ie    = ijk(1)+isearch
       is    = max(is,lbound(mesh%x,1)+1)
       ie    = min(ie,ubound(mesh%x,1))
       do i = is,ie
          if (xyz(1)>=mesh%xu(i).and.xyz(1)<mesh%xu(i+1)) then
             ijk(1) = i
             lfound = .true.
             exit
          end if
       end do
       if (.not.lfound) then 
          do i = mesh%sxu-mesh%gx,mesh%exu+mesh%gx-1
             if (xyz(1)>=mesh%xu(i).and.xyz(1)<mesh%xu(i+1)) then
                ijk(1) = i
                lfound = .true.
                exit
             end if
          end do
       end if
       !-------------------------------------------------------------------------------
       if (present(found)) then
          if (.not.lfound) then
             found = .false.
             return
          end if
       end if
       !-------------------------------------------------------------------------------
       ! y direction
       !-------------------------------------------------------------------------------
       lfound = .false.
       is    = ijk(2)-isearch
       ie    = ijk(2)+isearch
       is    = max(is,lbound(mesh%y,1)+1)
       ie    = min(ie,ubound(mesh%y,1))
       do i = is,ie
          if (xyz(2)>=mesh%yv(i).and.xyz(2)<mesh%yv(i+1)) then
             ijk(2) = i
             lfound = .true.
             exit
          end if
       end do
       if (.not.lfound) then 
          do i = mesh%syv-mesh%gy,mesh%eyv+mesh%gy-1
             if (xyz(2)>=mesh%yv(i).and.xyz(2)<mesh%yv(i+1)) then
                ijk(2) = i
                lfound = .true.
                exit
             end if
          end do
       end if
       !-------------------------------------------------------------------------------
       if (present(found)) then
          if (.not.lfound) then
             found = .false.
             return
          end if
       end if
       !-------------------------------------------------------------------------------
       if (mesh%dim==3) then 
          !-------------------------------------------------------------------------------
          ! z direction
          !-------------------------------------------------------------------------------
          lfound = .false.
          is    = ijk(3)-isearch
          ie    = ijk(3)+isearch
          is    = max(is,lbound(mesh%z,1)+1)
          ie    = min(ie,ubound(mesh%z,1))
          do i = is,ie
             if (xyz(3)>=mesh%zw(i).and.xyz(3)<mesh%zw(i+1)) then
                ijk(3) = i
                lfound = .true.
                exit
             end if
          end do
          if (.not.lfound) then 
             do i = mesh%szw-mesh%gz,mesh%ezw+mesh%gz-1
                if (xyz(3)>=mesh%zw(i).and.xyz(3)<mesh%zw(i+1)) then
                   ijk(3) = i
                   lfound = .true.
                   exit
                end if
             end do
          end if
          !-------------------------------------------------------------------------------
          if (present(found)) then
             if (.not.lfound) then
                found = .false.
                return
             end if
          end if
          !-------------------------------------------------------------------------------
       end if
       !-------------------------------------------------------------------------------
       ! X-velocity mesh
       !-------------------------------------------------------------------------------
    case(U_VEL_MESH)
       !-------------------------------------------------------------------------------
       ! x direction
       !-------------------------------------------------------------------------------
       lfound = .false.
       is    = ijk(1)-isearch
       ie    = ijk(1)+isearch
       is    = max(is,lbound(mesh%x,1)+1)
       ie    = min(ie,ubound(mesh%x,1))
       do i = is,ie
          if (xyz(1)>=mesh%x(i-1).and.xyz(1)<mesh%x(i)) then
             ijk(1) = i
             lfound = .true.
             exit
          end if
       end do
       if (.not.lfound) then 
          do i = mesh%sx-mesh%gx+1,mesh%ex+mesh%gx
             if (xyz(1)>=mesh%x(i-1).and.xyz(1)<mesh%x(i)) then
                ijk(1) = i
                exit
             end if
          end do
       end if
       !-------------------------------------------------------------------------------
       ! y direction
       !-------------------------------------------------------------------------------
       lfound = .false.
       is    = ijk(2)-isearch
       ie    = ijk(2)+isearch
       is    = max(is,lbound(mesh%yv,1))
       ie    = min(ie,ubound(mesh%yv,1)-1)
       do i = is,ie
          if (xyz(2)>=mesh%yv(i).and.xyz(2)<mesh%yv(i+1)) then
             ijk(2) = i
             lfound = .true.
             exit
          end if
       end do
       if (.not.lfound) then
          do i = mesh%syv-mesh%gy,mesh%eyv+mesh%gy-1
             if (xyz(2)>=mesh%yv(i).and.xyz(2)<mesh%yv(i+1)) then
                ijk(2) = i
                exit
             end if
          end do
       end if
       if (mesh%dim==3) then 
          !-------------------------------------------------------------------------------
          ! z direction
          !-------------------------------------------------------------------------------
          lfound = .false.
          is    = ijk(3)-isearch
          ie    = ijk(3)+isearch
          is    = max(is,lbound(mesh%zw,1))
          ie    = min(ie,ubound(mesh%zw,1)-1)
          do i = is,ie
             if (xyz(3)>=mesh%zw(i).and.xyz(3)<mesh%zw(i+1)) then
                ijk(3) = i
                lfound = .true.
                exit
             end if
          end do
          if (.not.lfound) then 
             do i = mesh%szw-mesh%gz,mesh%ezw+mesh%gz-1
                if (xyz(3)>=mesh%zw(i).and.xyz(3)<mesh%zw(i+1)) then
                   ijk(3) = i
                   exit
                end if
             end do
          end if
          !-------------------------------------------------------------------------------
       end if
       !-------------------------------------------------------------------------------
       ! Y-velocity mesh
       !-------------------------------------------------------------------------------
    case(V_VEL_MESH)
       !-------------------------------------------------------------------------------
       ! x direction
       !-------------------------------------------------------------------------------
       lfound = .false.
       is    = ijk(1)-isearch
       ie    = ijk(1)+isearch
       is    = max(is,lbound(mesh%xu,1))
       ie    = min(ie,ubound(mesh%xu,1)-1)
       do i = is,ie
          if (xyz(1)>=mesh%xu(i).and.xyz(1)<mesh%xu(i+1)) then
             ijk(1) = i
             lfound = .true.
             exit
          end if
       end do
       if (.not.lfound) then 
          do i = mesh%sxu-mesh%gx,mesh%exu+mesh%gx-1
             if (xyz(1)>=mesh%xu(i).and.xyz(1)<mesh%xu(i+1)) then
                ijk(1) = i
                exit
             end if
          end do
       end if
       !-------------------------------------------------------------------------------
       ! y direction
       !-------------------------------------------------------------------------------
       lfound = .false.
       is = ijk(2)-isearch
       ie = ijk(2)+isearch
       is = max(is,lbound(mesh%y,1)+1)
       ie = min(ie,ubound(mesh%y,1))
       do i = is,ie
          if (xyz(2)>=mesh%y(i-1).and.xyz(2)<mesh%y(i)) then
             ijk(2) = i
             lfound = .true.
             exit
          end if
       end do
       if (.not.lfound) then 
          do i = mesh%sy-mesh%gy+1,mesh%ey+mesh%gy
             if (xyz(2)>=mesh%y(i-1).and.xyz(2)<mesh%y(i)) then
                ijk(2) = i
                exit
             end if
          end do
       end if
       if (mesh%dim==3) then 
          !-------------------------------------------------------------------------------
          ! z direction
          !-------------------------------------------------------------------------------
          lfound = .false.
          is    = ijk(3)-isearch
          ie    = ijk(3)+isearch
          is    = max(is,lbound(mesh%zw,1))
          ie    = min(ie,ubound(mesh%zw,1)-1)
          do i = is,ie
             if (xyz(3)>=mesh%zw(i).and.xyz(3)<mesh%zw(i+1)) then
                ijk(3) = i
                lfound = .true.
                exit
             end if
          end do
          if (.not.lfound) then 
             do i = mesh%szw-mesh%gz,mesh%ezw+mesh%gz-1
                if (xyz(3)>=mesh%zw(i).and.xyz(3)<mesh%zw(i+1)) then
                   ijk(3) = i
                   exit
                end if
             end do
          end if
          !-------------------------------------------------------------------------------
       end if
       !-------------------------------------------------------------------------------
       ! Z-velocity mesh
       !-------------------------------------------------------------------------------
    case(W_VEL_MESH)
       !-------------------------------------------------------------------------------
       ! x direction
       !-------------------------------------------------------------------------------
       lfound = .false.
       is    = ijk(1)-isearch
       ie    = ijk(1)+isearch
       is    = max(is,lbound(mesh%xu,1))
       ie    = min(ie,ubound(mesh%xu,1)-1)
       do i = is,ie
          if (xyz(1)>=mesh%xu(i).and.xyz(1)<mesh%xu(i+1)) then
             ijk(1) = i
             lfound = .true.
             exit
          end if
       end do
       if (.not.lfound) then 
          do i = mesh%sxu-mesh%gx,mesh%exu+mesh%gx-1
             if (xyz(1)>=mesh%xu(i).and.xyz(1)<mesh%xu(i+1)) then
                ijk(1) = i
                exit
             end if
          end do
       end if
       !-------------------------------------------------------------------------------
       ! y direction
       !-------------------------------------------------------------------------------
       lfound = .false.
       is    = ijk(2)-isearch
       ie    = ijk(2)+isearch
       is    = max(is,lbound(mesh%yv,1))
       ie    = min(ie,ubound(mesh%yv,1)-1)
       do i = is,ie
          if (xyz(2)>=mesh%yv(i).and.xyz(2)<mesh%yv(i+1)) then
             ijk(2) = i
             lfound = .true.
             exit
          end if
       end do
       if (.not.lfound) then 
          do i = mesh%syv-mesh%gy,mesh%eyv+mesh%gy-1
             if (xyz(2)>=mesh%yv(i).and.xyz(2)<mesh%yv(i+1)) then
                ijk(2) = i
                exit
             end if
          end do
       end if
       if (mesh%dim==3) then 
          !-------------------------------------------------------------------------------
          ! z direction
          !-------------------------------------------------------------------------------
          lfound = .false.
          is    = ijk(3)-isearch
          ie    = ijk(3)+isearch
          is    = max(is,lbound(mesh%z,1)+1)
          ie    = min(ie,ubound(mesh%z,1))
          do i = is,ie
             if (xyz(3)>=mesh%z(i-1).and.xyz(3)<mesh%z(i)) then
                ijk(3) = i
                lfound = .true.
                exit
             end if
          end do
          if (.not.lfound) then 
             do i = mesh%sz-mesh%gz+1,mesh%ez+mesh%gz
                if (xyz(3)>=mesh%z(i-1).and.xyz(3)<mesh%z(i)) then
                   ijk(3) = i
                   exit
                end if
             end do
          end if
          !-------------------------------------------------------------------------------
       end if
    end select

  end subroutine search_cell_ijk
  
  
  subroutine search_cell_ijk_global(mesh,xyz,ijk,code)
    !*******************************************************************************
    ! Modules                                                                     
    !*******************************************************************************
    use mod_struct_grid
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    integer                              :: code
    integer, dimension(3), intent(inout) :: ijk
    real(8), dimension(3), intent(in)    :: xyz
    type(grid_t), intent(in)             :: mesh
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                              :: i
    integer                              :: is,ie
    integer                              :: isearch=2
    logical                              :: lfound
    !-------------------------------------------------------------------------------

    select case(code)
       !-------------------------------------------------------------------------------
       ! pressure/scalar mesh
       !-------------------------------------------------------------------------------
    case(PRESS_MESH)
       !-------------------------------------------------------------------------------
       ! x direction
       !-------------------------------------------------------------------------------
       lfound = .false.
       is    = ijk(1)-isearch
       ie    = ijk(1)+isearch
       is    = max(is,lbound(mesh%x,1)+1)
       ie    = min(ie,ubound(mesh%x,1))
       do i = is,ie
          if (xyz(1)>=mesh%xu(i).and.xyz(1)<mesh%xu(i+1)) then
             ijk(1) = i
             lfound = .true.
             exit
          end if
       end do
       if (.not.lfound) then 
          do i = mesh%gsxu-mesh%gx,mesh%gexu+mesh%gx-1
             if (xyz(1)>=mesh%xu(i).and.xyz(1)<mesh%xu(i+1)) then
                ijk(1) = i
                exit
             end if
          end do
       end if
       !-------------------------------------------------------------------------------
       ! y direction
       !-------------------------------------------------------------------------------
       lfound = .false.
       is    = ijk(2)-isearch
       ie    = ijk(2)+isearch
       is    = max(is,lbound(mesh%y,1)+1)
       ie    = min(ie,ubound(mesh%y,1))
       do i = is,ie
          if (xyz(2)>=mesh%yv(i).and.xyz(2)<mesh%yv(i+1)) then
             ijk(2) = i
             lfound = .true.
             exit
          end if
       end do
       if (.not.lfound) then 
          do i = mesh%gsyv-mesh%gy,mesh%geyv+mesh%gy-1
             if (xyz(2)>=mesh%yv(i).and.xyz(2)<mesh%yv(i+1)) then
                ijk(2) = i
                exit
             end if
          end do
       end if
       !-------------------------------------------------------------------------------
       if (mesh%dim==3) then 
          !-------------------------------------------------------------------------------
          ! z direction
          !-------------------------------------------------------------------------------
          lfound = .false.
          is    = ijk(3)-isearch
          ie    = ijk(3)+isearch
          is    = max(is,lbound(mesh%z,1)+1)
          ie    = min(ie,ubound(mesh%z,1))
          do i = is,ie
             if (xyz(3)>=mesh%zw(i).and.xyz(3)<mesh%zw(i+1)) then
                ijk(3) = i
                lfound = .true.
                exit
             end if
          end do
          if (.not.lfound) then 
             do i = mesh%gszw-mesh%gz,mesh%gezw+mesh%gz-1
                if (xyz(3)>=mesh%zw(i).and.xyz(3)<mesh%zw(i+1)) then
                   ijk(3) = i
                   exit
                end if
             end do
          end if
          !-------------------------------------------------------------------------------
       end if
       !-------------------------------------------------------------------------------
       ! X-velocity mesh
       !-------------------------------------------------------------------------------
    case(U_VEL_MESH)
       !-------------------------------------------------------------------------------
       ! x direction
       !-------------------------------------------------------------------------------
       lfound = .false.
       is    = ijk(1)-isearch
       ie    = ijk(1)+isearch
       is    = max(is,lbound(mesh%x,1)+1)
       ie    = min(ie,ubound(mesh%x,1))
       do i = is,ie
          if (xyz(1)>=mesh%x(i-1).and.xyz(1)<mesh%x(i)) then
             ijk(1) = i
             lfound = .true.
             exit
          end if
       end do
       if (.not.lfound) then 
          do i = mesh%gsx-mesh%gx+1,mesh%gex+mesh%gx
             if (xyz(1)>=mesh%x(i-1).and.xyz(1)<mesh%x(i)) then
                ijk(1) = i
                exit
             end if
          end do
       end if
       !-------------------------------------------------------------------------------
       ! y direction
       !-------------------------------------------------------------------------------
       lfound = .false.
       is    = ijk(2)-isearch
       ie    = ijk(2)+isearch
       is    = max(is,lbound(mesh%yv,1))
       ie    = min(ie,ubound(mesh%yv,1)-1)
       do i = is,ie
          if (xyz(2)>=mesh%yv(i).and.xyz(2)<mesh%yv(i+1)) then
             ijk(2) = i
             lfound = .true.
             exit
          end if
       end do
       if (.not.lfound) then
          do i = mesh%gsyv-mesh%gy,mesh%geyv+mesh%gy-1
             if (xyz(2)>=mesh%yv(i).and.xyz(2)<mesh%yv(i+1)) then
                ijk(2) = i
                exit
             end if
          end do
       end if
       if (mesh%dim==3) then 
          !-------------------------------------------------------------------------------
          ! z direction
          !-------------------------------------------------------------------------------
          lfound = .false.
          is    = ijk(3)-isearch
          ie    = ijk(3)+isearch
          is    = max(is,lbound(mesh%zw,1))
          ie    = min(ie,ubound(mesh%zw,1)-1)
          do i = is,ie
             if (xyz(3)>=mesh%zw(i).and.xyz(3)<mesh%zw(i+1)) then
                ijk(3) = i
                lfound = .true.
                exit
             end if
          end do
          if (.not.lfound) then 
             do i = mesh%gszw-mesh%gz,mesh%gezw+mesh%gz-1
                if (xyz(3)>=mesh%zw(i).and.xyz(3)<mesh%zw(i+1)) then
                   ijk(3) = i
                   exit
                end if
             end do
          end if
          !-------------------------------------------------------------------------------
       end if
       !-------------------------------------------------------------------------------
       ! Y-velocity mesh
       !-------------------------------------------------------------------------------
    case(V_VEL_MESH)
       !-------------------------------------------------------------------------------
       ! x direction
       !-------------------------------------------------------------------------------
       lfound = .false.
       is    = ijk(1)-isearch
       ie    = ijk(1)+isearch
       is    = max(is,lbound(mesh%xu,1))
       ie    = min(ie,ubound(mesh%xu,1)-1)
       do i = is,ie
          if (xyz(1)>=mesh%xu(i).and.xyz(1)<mesh%xu(i+1)) then
             ijk(1) = i
             lfound = .true.
             exit
          end if
       end do
       if (.not.lfound) then 
          do i = mesh%gsxu-mesh%gx,mesh%gexu+mesh%gx-1
             if (xyz(1)>=mesh%xu(i).and.xyz(1)<mesh%xu(i+1)) then
                ijk(1) = i
                exit
             end if
          end do
       end if
       !-------------------------------------------------------------------------------
       ! y direction
       !-------------------------------------------------------------------------------
       lfound = .false.
       is    = ijk(2)-isearch
       ie    = ijk(2)+isearch
       is    = max(is,lbound(mesh%y,1)+1)
       ie    = min(ie,ubound(mesh%y,1))
       do i = is,ie
          if (xyz(2)>=mesh%y(i-1).and.xyz(2)<mesh%y(i)) then
             ijk(2) = i
             lfound = .true.
             exit
          end if
       end do
       if (.not.lfound) then 
          do i = mesh%gsy-mesh%gy+1,mesh%gey+mesh%gy
             if (xyz(2)>=mesh%y(i-1).and.xyz(2)<mesh%y(i)) then
                ijk(2) = i
                exit
             end if
          end do
       end if
       if (mesh%dim==3) then 
          !-------------------------------------------------------------------------------
          ! z direction
          !-------------------------------------------------------------------------------
          lfound = .false.
          is    = ijk(3)-isearch
          ie    = ijk(3)+isearch
          is    = max(is,lbound(mesh%zw,1))
          ie    = min(ie,ubound(mesh%zw,1)-1)
          do i = is,ie
             if (xyz(3)>=mesh%zw(i).and.xyz(3)<mesh%zw(i+1)) then
                ijk(3) = i
                lfound = .true.
                exit
             end if
          end do
          if (.not.lfound) then 
             do i = mesh%gszw-mesh%gz,mesh%gezw+mesh%gz-1
                if (xyz(3)>=mesh%zw(i).and.xyz(3)<mesh%zw(i+1)) then
                   ijk(3) = i
                   exit
                end if
             end do
          end if
          !-------------------------------------------------------------------------------
       end if
       !-------------------------------------------------------------------------------
       ! Z-velocity mesh
       !-------------------------------------------------------------------------------
    case(W_VEL_MESH)
       !-------------------------------------------------------------------------------
       ! x direction
       !-------------------------------------------------------------------------------
       lfound = .false.
       is    = ijk(1)-isearch
       ie    = ijk(1)+isearch
       is    = max(is,lbound(mesh%xu,1))
       ie    = min(ie,ubound(mesh%xu,1)-1)
       do i = is,ie
          if (xyz(1)>=mesh%xu(i).and.xyz(1)<mesh%xu(i+1)) then
             ijk(1) = i
             lfound = .true.
             exit
          end if
       end do
       if (.not.lfound) then 
          do i = mesh%gsxu-mesh%gx,mesh%gexu+mesh%gx-1
             if (xyz(1)>=mesh%xu(i).and.xyz(1)<mesh%xu(i+1)) then
                ijk(1) = i
                exit
             end if
          end do
       end if
       !-------------------------------------------------------------------------------
       ! y direction
       !-------------------------------------------------------------------------------
       lfound = .false.
       is    = ijk(2)-isearch
       ie    = ijk(2)+isearch
       is    = max(is,lbound(mesh%yv,1))
       ie    = min(ie,ubound(mesh%yv,1)-1)
       do i = is,ie
          if (xyz(2)>=mesh%yv(i).and.xyz(2)<mesh%yv(i+1)) then
             ijk(2) = i
             lfound = .true.
             exit
          end if
       end do
       if (.not.lfound) then 
          do i = mesh%gsyv-mesh%gy,mesh%geyv+mesh%gy-1
             if (xyz(2)>=mesh%yv(i).and.xyz(2)<mesh%yv(i+1)) then
                ijk(2) = i
                exit
             end if
          end do
       end if
       if (mesh%dim==3) then 
          !-------------------------------------------------------------------------------
          ! z direction
          !-------------------------------------------------------------------------------
          lfound = .false.
          is    = ijk(3)-isearch
          ie    = ijk(3)+isearch
          is    = max(is,lbound(mesh%z,1)+1)
          ie    = min(ie,ubound(mesh%z,1))
          do i = is,ie
             if (xyz(3)>=mesh%z(i-1).and.xyz(3)<mesh%z(i)) then
                ijk(3) = i
                lfound = .true.
                exit
             end if
          end do
          if (.not.lfound) then 
             do i = mesh%gsz-mesh%gz+1,mesh%gez+mesh%gz
                if (xyz(3)>=mesh%z(i-1).and.xyz(3)<mesh%z(i)) then
                   ijk(3) = i
                   exit
                end if
             end do
          end if
          !-------------------------------------------------------------------------------
       end if
       
    end select

  end subroutine search_cell_ijk_global
  
end module mod_search
