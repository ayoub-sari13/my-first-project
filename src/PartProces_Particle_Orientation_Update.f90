!===============================================================================
module Bib_VOFLag_Particle_Orientation_Update
   !===============================================================================
   contains
   !-----------------------------------------------------------------------------  
   !> @author amine chadil
   ! 
   !> @brief cette routine met a jour le quaterion lie a chaque particule (schema implicite)
   !
   !> @param[out] vl                      : la structure contenant toutes les informations
   !! relatives aux particules
   !-----------------------------------------------------------------------------
   subroutine Particle_Orientation_Update(vl)
      !===============================================================================
      !modules
      !===============================================================================
      !-------------------------------------------------------------------------------
      !Modules de definition des structures et variables => src/mod/module_...f90
      !-------------------------------------------------------------------------------
      use Module_VOFLag_ParticlesDataStructure
      use mod_Parameters,                      only : deeptracking,dt
      use mod_mpi
      !-------------------------------------------------------------------------------
      !Modules de subroutines de la librairie RESPECT => src/Bib_VOFLag_...f90
      !-------------------------------------------------------------------------------
      use Bib_VOFLag_Matrix4_Inverse
      !-------------------------------------------------------------------------------

      !===============================================================================
      !declarations des variables
      !===============================================================================
      implicit none
      !-------------------------------------------------------------------------------
      !variables globales
      !-------------------------------------------------------------------------------
      type(struct_vof_lag), intent(inout) :: vl 
      !------------------------------------------------------------------------------
      !variables locales 
      !-------------------------------------------------------------------------------
      real(8), dimension(4,4)             :: mat,mat_inverse
      real(8), dimension(4)               :: quaternion
      real(8)                             :: n
      integer                             :: i,j,np,nd
      !-------------------------------------------------------------------------------

      !-------------------------------------------------------------------------------
      if (deeptracking) write(*,*) 'entree Particle_Orientation_Update'
      !-------------------------------------------------------------------------------

      do np = 1,vl%kpt 
         if (vl%objet(np)%in) then

            !-------------------------------------------------------------------------------
            !En 2D
            !-------------------------------------------------------------------------------
            if (dim .eq. 2) then
               vl%objet(np)%orientation(1) = vl%objet(np)%orientation(1)+dt*vl%objet(np)%omeg(3)
            else
               !-------------------------------------------------------------------------------
               ! remplir la matrice de resolution du systeme des quaternions en 3D
               !-------------------------------------------------------------------------------
               mat(1,1) =  1.0d0  ; mat(1,2) =  dt*0.5d0*vl%objet(np)%omeg(1); 
               mat(1,3) =  dt*0.5d0*vl%objet(np)%omeg(2); mat(1,4) =  dt*0.5d0*vl%objet(np)%omeg(3) 

               mat(2,1) = -dt*0.5d0*vl%objet(np)%omeg(1); mat(2,2) =  1.0d0  ;
               mat(2,3) = -dt*0.5d0*vl%objet(np)%omeg(3); mat(2,4) =  dt*0.5d0*vl%objet(np)%omeg(2) 

               mat(3,1) = -dt*0.5d0*vl%objet(np)%omeg(2); mat(3,2) =  dt*0.5d0*vl%objet(np)%omeg(3); 
               mat(3,3) =  1.0d0  ; mat(3,4) = -dt*0.5d0*vl%objet(np)%omeg(1) 

               mat(4,1) = -dt*0.5d0*vl%objet(np)%omeg(3); mat(4,2) = -dt*0.5d0*vl%objet(np)%omeg(2); 
               mat(4,3) =  dt*0.5d0*vl%objet(np)%omeg(1); mat(4,4) =  1.0d0 

               !-------------------------------------------------------------------------------
               ! resolution du systeme des quaternions en 3D
               !-------------------------------------------------------------------------------
               call Matrix4_Inverse(mat,mat_inverse)

               !-------------------------------------------------------------------------------
               ! normer le quaternion
               !-------------------------------------------------------------------------------
               quaternion = 0.d0
               n = 0.d0
               do i=1,4
                  do j=1,4
                     quaternion(i) = quaternion(i) + mat_inverse(i,j)*vl%objet(np)%orientation(j)
                  end do
                  n = n + quaternion(i)**2
               end do
               n = sqrt(n)
               vl%objet(np)%orientation = quaternion/(n+1D-40)
            end if
         end if
      end do

      !-------------------------------------------------------------------------------
      ! diffuser l'orientation des particules a tous les procs
      !-------------------------------------------------------------------------------
      do np=1,vl%kpt 
         call mpi_bcast(vl%objet(np)%orientation(1),1,mpi_double_precision,vl%objet(np)%locpart,comm3d,code) 
         if (dim==3) then
            do nd=2,4
               call mpi_bcast(vl%objet(np)%orientation(nd),1,mpi_double_precision,vl%objet(np)%locpart,comm3d,code) 
            end do
         end if
      end do

      !-------------------------------------------------------------------------------
      if (deeptracking) write(*,*) 'sortie Particle_Orientation_Update'
      !-------------------------------------------------------------------------------
   end subroutine Particle_Orientation_Update

  !===============================================================================
end module Bib_VOFLag_Particle_Orientation_Update
!===============================================================================