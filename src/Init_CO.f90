!========================================================================
!**
!**   NAME       : Init_CO.f90
!**
!**   AUTHOR     : B. trouette
!**
!**   FUNCTION   : Modules of subroutines for variable initialization of Concentration equation
!**
!**   DATES      : Version 1.0.0  : from : june, 22
!**
!========================================================================

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#include "precomp.h"

module mod_concentration_thermophy

  use mod_Parameters, only: dim,                &
       & sx,ex,sy,ey,sz,ez,gx,gy,gz,            &
       & nb_phase,NS_activate,                  &
       & VOF_activate,LS_activate,FT_Delta,     &
       & NS_init_pre,FT_activate,pi
  use mod_thermophys

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
contains

  !*****************************************************************************************************
  subroutine Concentration_Thermophysics(difu,difv,difw,cou,fluids)
    !***************************************************************************************************
    use mod_Constants, only: d1p2
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable   :: difu,difv,difw
    real(8), dimension(:,:,:), allocatable   :: cou
    type(phase), dimension(:), allocatable   :: fluids
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                  :: i,j,k
    integer                                  :: mean_dif
    real(8)                                  :: coul
    !-------------------------------------------------------------------------------
    ! Initialization of phase characteristics
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    if (nb_phase==1) then

       difu=fluids(1)%dif
       difv=fluids(1)%dif 
       if (dim==3) difw=fluids(1)%dif

    elseif (nb_phase==2) then 

       if (VOF_activate.or.LS_activate.or.FT_Delta) then

          mean_dif=1
          select case(mean_dif)
          case(1)
             !-------------------------------------------------------------------------------
             ! harmonic mean for face diffusivity
             !-------------------------------------------------------------------------------
             if (dim==2) then
                do j=sy-1,ey+1
                   do i=sx-1,ex+1
                      coul=d1p2*(cou(i-1,j,1)+cou(i,j,1))
                      difu(i,j,1)=fluids(1)%dif*fluids(2)%dif/((1-coul)*fluids(2)%dif+coul*fluids(1)%dif)
                      coul=d1p2*(cou(i,j-1,1)+cou(i,j,1))
                      difv(i,j,1)=fluids(1)%dif*fluids(2)%dif/((1-coul)*fluids(2)%dif+coul*fluids(1)%dif)
                   enddo
                enddo
             else 
                do k=sz-1,ez+1
                   do j=sy-1,ey+1
                      do i=sx-1,ex+1
                         coul=d1p2*(cou(i-1,j,k)+cou(i,j,k))
                         difu(i,j,k)=fluids(1)%dif*fluids(2)%dif/((1-coul)*fluids(2)%dif+coul*fluids(1)%dif)
                         coul=d1p2*(cou(i,j-1,k)+cou(i,j,k))
                         difv(i,j,k)=fluids(1)%dif*fluids(2)%dif/((1-coul)*fluids(2)%dif+coul*fluids(1)%dif)
                         coul=d1p2*(cou(i,j,k)+cou(i,j,k-1))
                         difw(i,j,k)=fluids(1)%dif*fluids(2)%dif/((1-coul)*fluids(2)%dif+coul*fluids(1)%dif)
                      enddo
                   enddo
                end do
             end if
          case(2)
             
          end select
          !-------------------------------------------------------------------------------
       endif

    end if

    !-------------------------------------------------------------------------------
  end subroutine Concentration_Thermophysics
  !*****************************************************************************************************

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
END module mod_concentration_thermophy
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
MODULE mod_Init_Concentration
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  use mod_Parameters, only: CO_source_term,CO_linear_term,dt,Euler,&
       & gsx,gex,gsy,gey,gsz,gez,CO_pen_BC
  use mod_concentration_thermophy
  use mod_thermophys
  use mod_struct_energy

  type boundary_value
     integer               :: type 
     real(8)               :: conin=0
     real(8)               :: r=0,amp=0
     real(8), dimension(3) :: xyz=0
     real(8), dimension(3) :: dxyz2=0
  end type boundary_value

  type faces
     type(boundary_value) :: left,right,bottom,top,backward,forward
  end type faces

  type(faces) :: bc_concentration

  namelist /nml_bc_concentration/ bc_concentration

contains

  !*****************************************************************************************************
  subroutine Init_Concentration(con,con0,con1,difu,difv,difw,slcon,smcon,cou,fluids,Concentration)
    !***************************************************************************************************

    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable :: con,con0,con1
    real(8), dimension(:,:,:), allocatable :: difu,difv,difw
    real(8), dimension(:,:,:), allocatable :: slcon,smcon
    real(8), dimension(:,:,:), allocatable :: cou
    type(phase), dimension(:), allocatable :: fluids
    type(struct_energy)             :: Concentration
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Initialization of Energy equation structure
    !-------------------------------------------------------------------------------
    concentration%dt=dt
    allocate(concentration%output%it_solver(1),&
         & concentration%output%res_solver(1),&
         & concentration%output%div(1))
    concentration%output%it_solver=0
    concentration%output%res_solver=0
    !-------------------------------------------------------------------------------
    ! Initialization of temperature field
    !-------------------------------------------------------------------------------
    con=0
    con0=con
    if (abs(Euler)==2) con1=con0
    !-------------------------------------------------------------------------------
    ! Initialization of molecular fluid characteristics 
    !-------------------------------------------------------------------------------
    if (FT_activate) then
       write(*,*) "not done with concentration equation"
       stop
    else
       call Concentration_Thermophysics(difu,difv,difw,cou,fluids)
    end if
    !-------------------------------------------------------------------------------
    ! Initialization of specific physical terms
    !-------------------------------------------------------------------------------
    if (CO_linear_term) slcon=0
    if (CO_source_term) smcon=0
    !-------------------------------------------------------------------------------
  end subroutine Init_Concentration
  !*****************************************************************************************************


  subroutine read_BC_CO
    use mod_struct_energy
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------

    open(10,file="data.in",action="read")
    read(10,nml=nml_bc_concentration)
    close(10)

  end subroutine read_BC_CO


  !*****************************************************************************************************
  subroutine Init_BC_Concentration(conin,pencon,concentration) 
    !***************************************************************************************************
    use mod_Parameters, only: Periodic,nx,ny,nz,sx,ex,sy,ey,sz,ez,dx,dy,dz,&
         & xmin,ymin,zmin,xmax,ymax,zmax,grid_x,grid_y,grid_z,grid_xu,grid_yv,grid_zw
    use mod_Constants, only: d1p2
    use mod_struct_Energy
    use mod_mpi

    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable   :: conin
    real(8), dimension(:,:,:,:), allocatable :: pencon
    type(struct_energy)                      :: concentration
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                  :: i,j,k
    real(8)                                  :: half_pen,full_pen
    real(8)                                  :: x,y,z,r,rr
    !-------------------------------------------------------------------------------
#if INRS
    !INRS PEV
    real(8)                                  :: rc_in,rc_out
    real(8)                                  :: yc_in,yc_out,zc_in,zc_out
#endif
    !-------------------------------------------------------------------------------
    ! penalisation values
    !-------------------------------------------------------------------------------
    full_pen=CO_pen_BC
    half_pen=d1p2*CO_pen_BC
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! left, right, top, bottom, backward, forward = 
    !    0: Dirichlet
    !    1: Neumann
    !    2: not used
    !    3: not used
    !    4: Periodic
    !-------------------------------------------------------------------------------
    open(10,file="data.in",action="read")
    read(10,nml=nml_bc_concentration)
    close(10)
    !-------------------------------------------------------------------------------
    ! check and force periodicity
    !-------------------------------------------------------------------------------
    if (Periodic(1)) then
       bc_concentration%left%type=4
       bc_concentration%right%type=4
    end if
    if (Periodic(2)) then
       bc_concentration%bottom%type=4
       bc_concentration%top%type=4
    end if
    if (Periodic(3)) then
       bc_concentration%backward%type=4
       bc_concentration%forward%type=4
    end if
    !-------------------------------------------------------------------------------

    concentration%bound%left=bc_concentration%left%type
    concentration%bound%right=bc_concentration%right%type
    concentration%bound%bottom=bc_concentration%bottom%type
    concentration%bound%top=bc_concentration%top%type
    concentration%bound%backward=bc_concentration%backward%type
    concentration%bound%forward=bc_concentration%forward%type
    !-------------------------------------------------------------------------------


    !-------------------------------------------------------------------------------
    ! Order of imposition of velocity boundary conditions
    !
    ! Periodic first
    ! Neumann
    ! Dirichlet
    !-------------------------------------------------------------------------------
    pencon=0
    conin=0

    !-------------------------------------------------------------------------------
    ! periodic --> nothing to do
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------


    !-------------------------------------------------------------------------------
    ! neumann --> nothing to do
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! symetry --> neumann --> nothing to do
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! dirichlet 0 or 3 
    !-------------------------------------------------------------------------------
    ! left 
    if (concentration%bound%left==0.or.concentration%bound%left==3) then
       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                if (i==gsx) then
                   pencon(i,j,k,1)=full_pen
                   conin(i,j,k)=bc_concentration%left%conin
                end if
             end do
          end do
       end do
    end if
    ! right
    if (concentration%bound%right==0.or.concentration%bound%right==3) then
       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                if (i==gex) then
                   pencon(i,j,k,1)=full_pen
                   conin(i,j,k)=bc_concentration%right%conin
                end if
             end do
          end do
       end do
    end if
    ! bottom
    if (concentration%bound%bottom==0.or.concentration%bound%bottom==3) then
       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                if (j==gsy) then
                   pencon(i,j,k,1)=full_pen
                   conin(i,j,k)=bc_concentration%bottom%conin
                end if
             end do
          end do
       end do
    end if
    ! top
    if (concentration%bound%top==0.or.concentration%bound%top==3) then
       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                if (j==gey) then
                   pencon(i,j,k,1)=full_pen
                   conin(i,j,k)=bc_concentration%top%conin
                end if
             end do
          end do
       end do
    end if
    if (dim==3) then
       ! backward
       if (concentration%bound%backward==0.or.concentration%bound%backward==3) then
          do k=sz,ez
             do j=sy,ey
                do i=sx,ex
                   if (k==gsz) then
                      pencon(i,j,k,1)=full_pen
                      conin(i,j,k)=bc_concentration%backward%conin
                   end if
                end do
             end do
          end do
       end if
       ! forward
       if (concentration%bound%forward==0.or.concentration%bound%forward==3) then
          do k=sz,ez
             do j=sy,ey
                do i=sx,ex
                   if (k==gez) then
                      pencon(i,j,k,1)=full_pen
                      conin(i,j,k)=bc_concentration%forward%conin
                   end if
                end do
             end do
          end do
       end if
    end if
    !-------------------------------------------------------------------------------
    ! end dirichlet
    !-------------------------------------------------------------------------------


    ! ******************************************************************************
    ! ******************************************************************************
    ! ******************************************************************************
    ! ******************************************************************************
    !                     specific boundary conditions
    ! ******************************************************************************
    ! ******************************************************************************
    ! ******************************************************************************
    ! ******************************************************************************



    !-------------------------------------------------------------------------------
    ! cylindrical inlet
    !-------------------------------------------------------------------------------
    ! imposed BC on an adiabatic face
    !-------------------------------------------------------------------------------
    ! left
    select case(abs(concentration%bound%left))
    case(10,11,12,13)
       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                if (i==gsx) then
                   ! adiabatic / damp-proof
                   pencon(i,j,k,:)=0
                   rr=(grid_y(j)-bc_concentration%left%xyz(2))**2 &
                        & +(grid_z(k)-bc_concentration%left%xyz(3))**2
                   if ( rr <= bc_concentration%left%r**2 ) then
                      ! imposed 
                      pencon(i,j,k,1)=full_pen
                      conin(i,j,k)=bc_concentration%left%conin
                   end if
                end if
             end do
          end do
       end do
    end select
    ! right
    select case(abs(concentration%bound%right))
    case(10,11,12,13)
       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                if (i==gex) then
                   ! adiabatic / damp-proof
                   pencon(i,j,k,:)=0
                   rr=(grid_y(j)-bc_concentration%right%xyz(2))**2 &
                        & +(grid_z(k)-bc_concentration%right%xyz(3))**2
                   if ( rr <= bc_concentration%right%r**2 ) then
                      ! imposed
                      pencon(i,j,k,1)=full_pen
                      conin(i,j,k)=bc_concentration%right%conin
                   end if
                end if
             end do
          end do
       end do
    end select
    ! bottom
    select case(abs(concentration%bound%bottom))
    case(10,11,12,13)
       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                if (j==gsy) then
                   ! adiabatic / damp-proof
                   pencon(i,j,k,:)=0
                   rr=(grid_x(i)-bc_concentration%bottom%xyz(1))**2 &
                        & +(grid_z(k)-bc_concentration%bottom%xyz(3))**2
                   if ( rr <= bc_concentration%bottom%r**2 ) then
                      ! imposed
                      pencon(i,j,k,1)=full_pen
                      conin(i,j,k)=bc_concentration%bottom%conin
                   end if
                end if
             end do
          end do
       end do
    end select
    ! top
    select case(abs(concentration%bound%top))
    case(10,11,12,13)
       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                if (j==gey) then
                   ! adiabatic / damp-proof
                   pencon(i,j,k,:)=0
                   rr=(grid_x(i)-bc_concentration%top%xyz(1))**2 &
                        & +(grid_z(k)-bc_concentration%top%xyz(3))**2
                   if ( rr <= bc_concentration%top%r**2 ) then
                      ! imposed
                      pencon(i,j,k,1)=full_pen
                      conin(i,j,k)=bc_concentration%top%conin
                   end if
                end if
             end do
          end do
       end do
    end select
    if (dim==3) then
       ! backward
       select case(abs(concentration%bound%backward))
       case(10,11,12,13)
          do k=sz,ez
             do j=sy,ey
                do i=sx,ex
                   if (k==gsz) then
                      ! adiabatic / damp-proof
                      pencon(i,j,k,:)=0
                      rr=(grid_x(i)-bc_concentration%backward%xyz(1))**2 &
                           & +(grid_y(j)-bc_concentration%backward%xyz(2))**2
                      if ( rr <= bc_concentration%backward%r**2 ) then
                         ! imposed
                         pencon(i,j,k,1)=full_pen
                         conin(i,j,k)=bc_concentration%backward%conin
                      end if
                   end if
                end do
             end do
          end do
       end select
       ! forward
       select case(abs(concentration%bound%forward))
       case(10,11,12,13)
          do k=sz,ez
             do j=sy,ey
                do i=sx,ex
                   if (k==gez) then
                      ! adiabatic / damp-proof
                      pencon(i,j,k,:)=0
                      rr=(grid_x(i)-bc_concentration%forward%xyz(1))**2 &
                           & +(grid_y(j)-bc_concentration%forward%xyz(2))**2
                      if ( rr <= bc_concentration%forward%r**2 ) then
                         ! imposed
                         pencon(i,j,k,1)=full_pen
                         conin(i,j,k)=bc_concentration%forward%conin
                      end if
                   end if
                end do
             end do
          end do
       end select
    end if
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! rectangular inlet
    !-------------------------------------------------------------------------------
    ! imposed BC on an adiabatic face
    !-------------------------------------------------------------------------------
    ! left
    select case(abs(concentration%bound%left))
    case(30)
       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                if (i==gsx) then
                   ! adiabatic / damp-proof
                   pencon(i,j,k,:)=0
                   if (   abs(grid_y(j)-bc_concentration%left%xyz(2))<=bc_concentration%left%dxyz2(2) .and. &
                        & abs(grid_z(k)-bc_concentration%left%xyz(3))<=bc_concentration%left%dxyz2(3)) then
                      ! imposed 
                      pencon(i,j,k,1)=full_pen
                      conin(i,j,k)=bc_concentration%left%conin
                   end if
                end if
             end do
          end do
       end do
    end select
    ! right
    select case(abs(concentration%bound%right))
    case(30)
       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                if (i==gex) then
                   ! adiabatic / damp-proof
                   pencon(i,j,k,:)=0
                   if (   abs(grid_y(j)-bc_concentration%right%xyz(2))<=bc_concentration%right%dxyz2(2) .and. &
                        & abs(grid_z(k)-bc_concentration%right%xyz(3))<=bc_concentration%right%dxyz2(3)) then
                      ! imposed
                      pencon(i,j,k,1)=full_pen
                      conin(i,j,k)=bc_concentration%right%conin
                   end if
                end if
             end do
          end do
       end do
    end select
    ! bottom
    select case(abs(concentration%bound%bottom))
    case(30)
       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                if (j==gsy) then
                   ! adiabatic / damp-proof
                   pencon(i,j,k,:)=0
                   if (   abs(grid_x(i)-bc_concentration%bottom%xyz(1))<=bc_concentration%bottom%dxyz2(1) .and. &
                        & abs(grid_z(k)-bc_concentration%bottom%xyz(3))<=bc_concentration%bottom%dxyz2(3)) then
                      ! imposed
                      pencon(i,j,k,1)=full_pen
                      conin(i,j,k)=bc_concentration%bottom%conin
                   end if
                end if
             end do
          end do
       end do
    end select
    ! top
    select case(abs(concentration%bound%top))
    case(30)
       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                if (j==gey) then
                   ! adiabatic / damp-proof
                   pencon(i,j,k,:)=0
                   if (   abs(grid_x(i)-bc_concentration%top%xyz(1))<=bc_concentration%top%dxyz2(1) .and. &
                        & abs(grid_z(k)-bc_concentration%top%xyz(3))<=bc_concentration%top%dxyz2(3)) then
                      ! imposed
                      pencon(i,j,k,1)=full_pen
                      conin(i,j,k)=bc_concentration%top%conin
                   end if
                end if
             end do
          end do
       end do
    end select
    if (dim==3) then
       ! backward
       select case(abs(concentration%bound%backward))
       case(30)
          do k=sz,ez
             do j=sy,ey
                do i=sx,ex
                   if (k==gsz) then
                      ! adiabatic / damp-proof
                      pencon(i,j,k,:)=0
                      if (   abs(grid_x(i)-bc_concentration%backward%xyz(1))<=bc_concentration%backward%dxyz2(1) .and. &
                           & abs(grid_y(j)-bc_concentration%backward%xyz(2))<=bc_concentration%backward%dxyz2(2)) then
                         ! imposed
                         pencon(i,j,k,1)=full_pen
                         conin(i,j,k)=bc_concentration%backward%conin
                      end if
                   end if
                end do
             end do
          end do
       end select
       ! forward
       select case(abs(concentration%bound%forward))
       case(30)
          do k=sz,ez
             do j=sy,ey
                do i=sx,ex
                   if (k==gez) then
                      ! adiabatic / damp-proof
                      pencon(i,j,k,:)=0
                      if (   abs(grid_x(i)-bc_concentration%forward%xyz(1))<=bc_concentration%forward%dxyz2(1) .and. &
                           & abs(grid_y(j)-bc_concentration%forward%xyz(2))<=bc_concentration%forward%dxyz2(2)) then
                         ! imposed
                         pencon(i,j,k,1)=full_pen
                         conin(i,j,k)=bc_concentration%forward%conin
                      end if
                   end if
                end do
             end do
          end do
       end select
    end if
    !-------------------------------------------------------------------------------

#if INRS 
    !-------------------------------------------------------------------------------
    ! cylindrical inlet
    !-------------------------------------------------------------------------------
    !INRS PEV
    !-------------------------------------------------------------------------------
    ! INRS case 1 : input/ouconut in circles at left/right faces
    !             : input  : dirichlet+sliding
    !             : ouconut : neumann+dirichlet
    ! yc_in =ymin+(ymax-ymin)/2; zc_in =zmin+3d0*(zmax-zmin)/4
    ! yc_out=ymin+(ymax-ymin)/2; zc_out=zmin+1d0*(zmax-zmin)/4
    ! rc_in=0.02_8; rc_out=rc_in
    ! Q_in_exp=40 l/min
    !-------------------------------------------------------------------------------

    ! left 
    if (concentration%bound%left==50) then

       if (dim==2) then 
          yc_in=ymin+(ymax-ymin)-0.055_8
          yc_out=ymin+0.055_8
       else
          yc_in=ymin+d1p2*(ymax-ymin)
          zc_in=zmin+(zmax-zmin)-0.055_8
          yc_out=yc_in
          zc_out=zmin+0.055_8
       end if
       rc_in=0.02_8
       rc_out=rc_in

       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                if (i==gsx) then
                   ! adiabatic (damp-proof)
                   pencon(i,j,k,:)=0
                   !-----------------------------------------
                   ! specific con (or C) value
                   !------------------------------------------
                   y=grid_y(j)
                   z=grid_z(k)
                   if ( (y-yc_in)**2+(z-zc_in)**2 <= rc_in**2 ) then
                      pencon(i,j,k,1)=full_pen
                      conin(i,j,k)=1
                   end if
                   !------------------------------------------
                end if
             end do
          end do
       end do

    end if
    !-------------------------------------------------------------------------------
    ! end INRS case 1
    !-------------------------------------------------------------------------------
#endif


    !-------------------------------------------------------------------------------
    ! check
    !-------------------------------------------------------------------------------
    i=0
    if (concentration%bound%left==4.and.concentration%bound%right/=4)       i=1
    if (concentration%bound%left/=4.and.concentration%bound%right==4)       i=1
    if (concentration%bound%bottom==4.and.concentration%bound%top/=4)       i=1
    if (concentration%bound%bottom/=4.and.concentration%bound%top==4)       i=1
    if (concentration%bound%backward==4.and.concentration%bound%forward/=4) i=1
    if (concentration%bound%backward/=4.and.concentration%bound%forward==4) i=1
    if (i==1) then
       write(*,*) "PERIODICITY ERROR, CHECK BCS, STOP"
       STOP
    end if

    i=0
    j=concentration%bound%left           &
         & *concentration%bound%right    &
         & *concentration%bound%bottom   &
         & *concentration%bound%top
    if (dim==3) then
       j=j*concentration%bound%backward  &
            & *concentration%bound%forward
    end if
    if (j==1.or.j==2) i=1
    if (i==1.and.rank==0) then
       write(*,*) "WARNING, ALL BCS ARE NEUMANN LIKE"
    end if
    !-------------------------------------------------------------------------------



  end subroutine Init_BC_Concentration
  !*****************************************************************************************************


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
END MODULE mod_Init_Concentration
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
