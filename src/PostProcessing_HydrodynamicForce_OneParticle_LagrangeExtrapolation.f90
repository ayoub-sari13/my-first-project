!===============================================================================
module Bib_VOFLag_HydrodynamicForce_OneParticle_LagrangeExtrapolation
  !===============================================================================
  contains
  !-----------------------------------------------------------------------------  
  !> @author amine chadil
  ! 
  !> @brief cette routine force hydrodynamique appliquee sur un objet maille
  !
  !> @param [out] obj        : la structure contenant les caracteristiques des objets (
  !! maillage des peaux des particules).
  !> @param [in]  tensd      : le tenseur de deformation
  !> @param [in]  pre        : la valeur a extrapoler
  !> @param [in]  vie        : viscosite moleculaire sur la grille de pression
  !> @param [in]  vir        : viscosite de rotation sur la grille de viscosite
  !-----------------------------------------------------------------------------
  subroutine HydrodynamicForce_OneParticle_LagrangeExtrapolation(vl,np,nbt,pres,tensd,&
    cou,vie,vir,EachElementImpression,fichier_impression)
    !===============================================================================
    !modules
    !===============================================================================
    !-------------------------------------------------------------------------------
    !Modules de definition des structures et variables => src/mod/module_...f90
    !-------------------------------------------------------------------------------
    use Module_VOFLag_ParticlesDataStructure
    use mod_Parameters,                      only : deeptracking,dim,dx,dy,dz,grid_x,&
                                                    grid_y,grid_z
    !-------------------------------------------------------------------------------
    !Modules de subroutines de la librairie VOF-Lag
    !-------------------------------------------------------------------------------
    use Bib_VOFLag_LtpOfPressureCellContainingThePoint_IJK
    use Bib_VOFLag_SubDomain_Limited_PointIn
    use Bib_VOFLag_FromPointTheDomainIsFluid
    use Bib_VOFLag_TaylorInterpolation_PressureMesh
    !use Bib_VOFLag_TaylorInterpolation_ViscosityMesh
    !-------------------------------------------------------------------------------
    !===============================================================================
    !declarations des variables
    !===============================================================================
    implicit none
    !-------------------------------------------------------------------------------
    !variables globales
    !-------------------------------------------------------------------------------
    type(struct_vof_lag), intent(inout)                  :: vl
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:,:), allocatable, intent(in) :: vir
    real(8), dimension(:,:,:)  , allocatable, intent(in) :: tensd
    real(8), dimension(:,:,:)  , allocatable, intent(in) :: pres,vie,cou
    character(200)                          , intent(in) :: fichier_impression
    logical                                              :: EachElementImpression
    integer                                              :: np,nbt
    !-------------------------------------------------------------------------------
    !variables locales 
    !-------------------------------------------------------------------------------
    real(8), dimension(dim,dim,vl%objet(np)%kkl)         :: lag
    real(8), dimension(dim,vl%objet(np)%kkl)             :: normal
    real(8), dimension(vl%objet(np)%kkl)                 :: area
    !real(8), dimension(:,:,:)                            :: DiagonalComponentStressTensor
    real(8), dimension(dim,dim)                          :: tcon
    real(8), dimension(dim,4)                            :: point
    real(8), dimension(dim)                              :: Barycenter,fpre,fvis
    !real(8), dimension(:,:,:)                            :: NonDiagonalComponentStressTensor
    real(8), dimension(4)                                :: N
    integer, dimension(3,4)                              :: Cell
    integer, dimension(3)                                :: Cell_bis
    real(8)                                              :: ttp,EulerianSpaceStep,LagrangePolynome,&
                                                            DistanceFromParticle,p_in_element,     &
                                                            ElementLength
    integer                                              :: ltp,kkl,nd,npt,l,lp,dec,ltp_particle,  &
                                                            NumberOfExtrapolationPoints,           &
                                                            ExtrapolationOrder,i,j,k,              &
                                                            ExtrapolationOrderPoint,               &
                                                            InterpolationOrderPoint
    logical                                              :: in_domain,IsFluid,IsFluid_tmp
    !-------------------------------------------------------------------------------    
    
    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'entree HydrodynamicForce_OneParticle_LagrangeExtrapolation'
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! initialisation
    !-------------------------------------------------------------------------------
    InterpolationOrderPoint = 2
    ExtrapolationOrder = 3
    DistanceFromParticle = 1.D0
    normal=vl%objet(np)%normal
    area  =vl%objet(np)%area
    kkl   =vl%objet(np)%kkl 
    do nd = 1,dim
      lag(nd,:,:) = vl%objet(np)%lag_for_all_copies(nd,:,:)+vl%objet(np)%symper(nbt,nd)
    enddo
    call LtpOfPressureCellContainingThePoint_IJK(vl%objet(np)%symper(nbt,:),i,j,k,dim,&
                                                       grid_x,grid_y,grid_z)
    if (EachElementImpression .and. vl%objet(np)%on) then
      open(unit=1000,file=fichier_impression,status='old',position='append')
    endif

    !-------------------------------------------------------------------------------
    ! calcul de la partie visqueuse du tenseur de contrainte
    !-------------------------------------------------------------------------------
    !dec=dim*kkt
    !NonDiagonalComponentStressTensor(1:kkm)= 2.0*vir(1:kkm)*tensd(dec+1:dec+kkm)
    !do ltp=1,kkt
    !  do nd=1,dim
    !    DiagonalComponentStressTensor(ltp,nd) = 2.0*vie(ltp)*tensd(ltp+(nd-1)*kkt)
    !  enddo
    !enddo

    !-------------------------------------------------------------------------------
    ! calcul du pas d'espace
    !-------------------------------------------------------------------------------
    EulerianSpaceStep = (dx(i)+dy(j)+dz(k))/(dim+1D-40)

    fpre=0.D0
    fvis=0.D0

    !-------------------------------------------------------------------------------
    ! calcul des forces appliquees sur chaque element de l'objet maille
    !-------------------------------------------------------------------------------
    do lp=1,kkl

      !-------------------------------------------------------------------------------
      ! calcul du barycentre de l'elements lp 
      !-------------------------------------------------------------------------------
      Barycenter=0.D0
      do nd = 1,dim
        Barycenter(1:dim)=Barycenter(1:dim)+lag(1:dim,nd,lp)
      enddo 
      Barycenter = Barycenter / (dim+1D-40)
      call SubDomain_Limited_PointIn(Barycenter(:),in_domain)
      if (.not.in_domain) goto 123
      !-------------------------------------------------------------------------------
      ! contruction du point premier point d'extrapolation 
      !-------------------------------------------------------------------------------
      NumberOfExtrapolationPoints = 1
      N(1) = DistanceFromParticle
      point(:,1)=Barycenter(:)+N(1)*EulerianSpaceStep*normal(:,lp)
      call LtpOfPressureCellContainingThePoint_IJK(point(:,1),Cell(1,1),Cell(2,1),Cell(3,1),dim,&
                                                       grid_x,grid_y,grid_z)
      ExtrapolationOrderPoint=1
      if (NumberOfExtrapolationPoints.lt.ExtrapolationOrder) then

        !-------------------------------------------------------------------------------
        ! contruction du point second point d'extrapolation 
        !-------------------------------------------------------------------------------
        NumberOfExtrapolationPoints = 2
        N(2) = N(1) + 1.
        point(:,2)=Barycenter(:)+N(2)*EulerianSpaceStep*normal(:,lp)
        call LtpOfPressureCellContainingThePoint_IJK(point(:,2),Cell_bis(1),Cell_bis(2),Cell_bis(3),dim,&
                                                       grid_x,grid_y,grid_z)
        if (Cell_bis(1).eq.Cell(1,1) .and. Cell_bis(2).eq.Cell(2,1) .and. Cell_bis(3).eq.Cell(3,1)) then
          N(2) = N(1) + sqrt(dfloat(dim))
          point(:,2)=Barycenter(:)+N(2)*EulerianSpaceStep*normal(:,lp)
          call LtpOfPressureCellContainingThePoint_IJK(point(:,2),Cell(1,2),Cell(2,2),Cell(3,2),dim,&
                                                       grid_x,grid_y,grid_z)
        else
          Cell(:,2) = Cell_bis(:)
        endif
        call LtpOfPressureCellContainingThePoint_IJK(point(:,2),Cell(1,2),Cell(2,2),Cell(3,2),dim,&
                                                       grid_x,grid_y,grid_z)
        call SubDomain_Limited_PointIn(point(:,2),in_domain)
        IsFluid=.true.
        do npt = 2,NumberOfExtrapolationPoints
          call FromPointTheDomainIsFluid(Cell(1,npt),Cell(2,npt),Cell(3,npt),NumberOfExtrapolationPoints,&
                                         -normal(:,lp),cou,dim,IsFluid_tmp)
          IsFluid=IsFluid.and.IsFluid_tmp
        enddo
        if (in_domain .and. IsFluid) then 
          ExtrapolationOrderPoint=2
        else
          goto 100
        endif
        if (NumberOfExtrapolationPoints.lt.ExtrapolationOrder) then

          !-------------------------------------------------------------------------------
          ! contruction du point troisieme point d'extrapolation 
          !-------------------------------------------------------------------------------
          NumberOfExtrapolationPoints = 3
          N(3) = N(2) + 1.
          point(:,3)=Barycenter(:)+N(3)*EulerianSpaceStep*normal(:,lp)
          call LtpOfPressureCellContainingThePoint_IJK(point(:,3),Cell_bis(1),Cell_bis(2),Cell_bis(3),dim,&
                                                       grid_x,grid_y,grid_z)
          if (Cell_bis(1).eq.Cell(1,2) .and. Cell_bis(2).eq.Cell(2,2) .and. Cell_bis(3).eq.Cell(3,2)) then
            N(3) = N(2) + sqrt(dfloat(dim))
            point(:,3)=Barycenter(:)+N(3)*EulerianSpaceStep*normal(:,lp)
            call LtpOfPressureCellContainingThePoint_IJK(point(:,3),Cell(1,3),Cell(2,3),Cell(3,3),dim,&
                                                       grid_x,grid_y,grid_z)
          else
            Cell(:,3) = Cell_bis(:)
          endif
          call LtpOfPressureCellContainingThePoint_IJK(point(:,3),Cell(1,3),Cell(2,3),Cell(3,3),dim,&
                                                       grid_x,grid_y,grid_z)
          call SubDomain_Limited_PointIn(point(:,3),in_domain)
          IsFluid=.true.
          do npt = 2,NumberOfExtrapolationPoints
            call FromPointTheDomainIsFluid(Cell(1,npt),Cell(2,npt),Cell(3,npt),NumberOfExtrapolationPoints,&
                                            -normal(:,lp),cou,dim,IsFluid_tmp)
            IsFluid=IsFluid.and.IsFluid_tmp
          enddo
          if (in_domain .and. IsFluid) then 
            ExtrapolationOrderPoint=3
          else
            goto 100
          endif
          if (NumberOfExtrapolationPoints.lt.ExtrapolationOrder) then

            !-------------------------------------------------------------------------------
            ! contruction du point quatrieme point d'extrapolation 
            !-------------------------------------------------------------------------------
            NumberOfExtrapolationPoints = 4
            N(4) = N(3) + 1.
            point(:,4)=Barycenter(:)+N(4)*EulerianSpaceStep*normal(:,lp)
            call LtpOfPressureCellContainingThePoint_IJK(point(:,4),Cell_bis(1),Cell_bis(2),Cell_bis(3),dim,&
                                                       grid_x,grid_y,grid_z)
            if (Cell_bis(1).eq.Cell(1,3) .and. Cell_bis(2).eq.Cell(2,3) .and. Cell_bis(3).eq.Cell(3,3)) then
              N(4) = N(3) + sqrt(dfloat(dim))
              point(:,4)=Barycenter(:)+N(4)*EulerianSpaceStep*normal(:,lp)
              call LtpOfPressureCellContainingThePoint_IJK(point(:,4),Cell(1,4),Cell(2,4),Cell(3,4),dim,&
                                                       grid_x,grid_y,grid_z)
            else 
              Cell(:,4) = Cell_bis(:)
            endif
            call LtpOfPressureCellContainingThePoint_IJK(point(:,4),Cell(1,4),Cell(2,4),Cell(3,4),dim,&
                                                       grid_x,grid_y,grid_z)
            call SubDomain_Limited_PointIn(point(:,4),in_domain)
            IsFluid=.true.
            do npt = 2,NumberOfExtrapolationPoints
              call FromPointTheDomainIsFluid(Cell(1,npt),Cell(2,npt),Cell(3,npt),NumberOfExtrapolationPoints,&
                                             -normal(:,lp),cou,dim,IsFluid_tmp)
              IsFluid=IsFluid.and.IsFluid_tmp
            enddo
            if (in_domain .and. IsFluid) ExtrapolationOrderPoint = 4
          endif
        endif
      endif
      100 InterpolationOrderPoint=ExtrapolationOrderPoint

      !-------------------------------------------------------------------------------
      ! extrapolation de la pression au centre de l'element lp
      !------------------------------------------------------------------------------- 
      p_in_element=0.
      do npt = 1,ExtrapolationOrderPoint
        call TaylorInterpolation_PressureMesh (pres,Cell(1,npt),Cell(2,npt),Cell(3,npt),-normal(:,lp),&
                                               InterpolationOrderPoint,point(:,npt),ttp) 
        LagrangePolynome = 1.D0
        do l = 1,ExtrapolationOrderPoint
          if (l .ne. npt) LagrangePolynome = LagrangePolynome * (-N(l))/(N(npt)-N(l)+1D-40)
        enddo
        p_in_element=p_in_element + ttp*LagrangePolynome
      end do
      fpre(1:dim) = fpre(1:dim)-p_in_element*normal(1:dim,lp)*area(lp)

      tcon = 0.D0
      !-------------------------------------------------------------------------------
      ! extrapolation des composantes diagonales du tenseur de contraintes 
      !au centre de l'element lp
      !------------------------------------------------------------------------------- 
      !do nd = 1,dim
      !  do npt = 1,ExtrapolationOrderPoint
      !    call TaylorInterpolation_PressureMesh (ttp,Cell(npt),point(:,npt),DiagonalComponentStressTensor(:,nd),-normal(:,lp),InterpolationOrderPoint)
      !    LagrangePolynome = 1.D0
      !    do l = 1,ExtrapolationOrderPoint
      !      if (l .ne. npt) LagrangePolynome = LagrangePolynome * (-N(l))/(N(npt)-N(l)+1D-40)
      !    enddo
      !    tcon(nd,nd) = tcon(nd,nd) + ttp*LagrangePolynome
      !  end do
      !end do

      !-------------------------------------------------------------------------------
      ! interpolation des composantes non-diagonales du tenseur de contraintes au centre de l'element lp
      !------------------------------------------------------------------------------- 
      !do npt = 1,ExtrapolationOrderPoint
      !  call TaylorInterpolation_ViscosityMesh (ttp,Cell(npt),point(:,npt),NonDiagonalComponentStressTensor(1:kkm),-normal(:,lp),1,InterpolationOrderPoint)
      !  LagrangePolynome = 1.D0
      !  do l = 1,ExtrapolationOrderPoint
      !    if (l .ne. npt) LagrangePolynome = LagrangePolynome * (-N(l))/(N(npt)-N(l)+1D-40)
      !  enddo
      !  tcon(1,2) = tcon(1,2) + ttp*LagrangePolynome
      !enddo
      !tcon(2,1)=tcon(1,2) 
      !fvis(1) = fvis(1) + (normal(1,lp)*tcon(1,1)+normal(2,lp)*tcon(1,2)) * area(lp)
      !fvis(2) = fvis(2) + (normal(1,lp)*tcon(2,1)+normal(2,lp)*tcon(2,2)) * area(lp)

      !if (dim .eq. 3) then
      !  do npt = 1,ExtrapolationOrderPoint
      !    call TaylorInterpolation_ViscosityMesh (ttp,Cell(npt),point(:,npt),NonDiagonalComponentStressTensor(1:kkm),-normal(:,lp),3,InterpolationOrderPoint)
      !    LagrangePolynome = 1.D0
      !    do l = 1,ExtrapolationOrderPoint
      !      if (l .ne. npt) LagrangePolynome = LagrangePolynome * (-N(l))/(N(npt)-N(l)+1D-40)
      !    enddo
      !    tcon(1,3) = tcon(1,3) + ttp*LagrangePolynome
      !  end do
      !  tcon(3,1)=tcon(1,3)
      !  do npt = 1,ExtrapolationOrderPoint
      !    call TaylorInterpolation_ViscosityMesh (ttp,Cell(npt),point(:,npt),NonDiagonalComponentStressTensor(1:kkm),-normal(:,lp),2,InterpolationOrderPoint)
      !      LagrangePolynome = 1.D0
      !      do l = 1,ExtrapolationOrderPoint
      !        if (l .ne. npt) LagrangePolynome = LagrangePolynome * (-N(l))/(N(npt)-N(l)+1D-40)
      !      enddo
      !    tcon(2,3) = tcon(2,3) + ttp*LagrangePolynome
      !  end do
      !  tcon(3,2)=tcon(2,3)
      !  fvis(1) = fvis(1) + (                                              normal(3,lp)*tcon(1,3)) * area(lp)
      !  fvis(2) = fvis(2) + (                                              normal(3,lp)*tcon(2,3)) * area(lp)
      !  fvis(3) = fvis(3) + (normal(1,lp)*tcon(3,1)+normal(2,lp)*tcon(3,2)+normal(3,lp)*tcon(3,3)) * area(lp)
      !endif       

      if (EachElementImpression .and. vl%objet(np)%on) then
        if (dim .eq. 2) then
          !write(1000,'(8E13.5,/)',advance='no') dfloat(lp),Barycenter(1)-vl%objet(np)%symper(nbt,1),Barycenter(2)-vl%objet(np)%symper(nbt,2),&
          !    p_in_element,(normal(1,lp)*tcon(1,1)+normal(2,lp)*tcon(1,2)),Barycenter(1),Barycenter(2),dfloat(ExtrapolationOrderPoint)
          write(1000,'(8E13.5,/)',advance='no') dfloat(lp),Barycenter(1)-vl%objet(np)%symper(nbt,1),&
                                                           Barycenter(2)-vl%objet(np)%symper(nbt,2),&
                                                           p_in_element,p_in_element,Barycenter(1), &
                                                           Barycenter(2),dfloat(ExtrapolationOrderPoint)
        else
          !write(1000,'(10E13.5,/)',advance='no') dfloat(lp),Barycenter(1)-vl%objet(np)%symper(nbt,1),Barycenter(2)-vl%objet(np)%symper(nbt,2),&
          !    Barycenter(3)-vl%objet(np)%symper(nbt,3),p_in_element,(normal(1,lp)*tcon(1,1)+normal(2,lp)*tcon(1,2)+normal(3,lp)*tcon(1,3)),&
          !    Barycenter(1),Barycenter(2),Barycenter(3),dfloat(ExtrapolationOrderPoint)
          write(1000,'(10E13.5,/)',advance='no') dfloat(lp),Barycenter(1)-vl%objet(np)%symper(nbt,1),&
                                                            Barycenter(2)-vl%objet(np)%symper(nbt,2),&
                                                            Barycenter(3)-vl%objet(np)%symper(nbt,3),&
                                                            p_in_element,p_in_element,Barycenter(1), &
                                                            Barycenter(2),Barycenter(3),             &
                                                            dfloat(ExtrapolationOrderPoint)
        endif
      endif

      123   continue
    enddo
    if (EachElementImpression .and. vl%objet(np)%on) close(1000)

    !-------------------------------------------------------------------------------
    !ajouter aux forces calculees la contribution de la copie nbt 
    !-------------------------------------------------------------------------------
    vl%objet(np)%fp(1:dim) = vl%objet(np)%fp(1:dim) + fpre(1:dim)
    !vl%objet(np)%fv(1:dim) = vl%objet(np)%fv(1:dim) + fvis(1:dim)
    vl%objet(np)%f(1:dim)  = vl%objet(np)%fp(1:dim) !+ vl%objet(np)%fv(1:dim)

    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'sortie HydrodynamicForce_OneParticle_LagrangeExtrapolation'
    !-------------------------------------------------------------------------------
  end subroutine HydrodynamicForce_OneParticle_LagrangeExtrapolation

  !===============================================================================
end module Bib_VOFLag_HydrodynamicForce_OneParticle_LagrangeExtrapolation
!===============================================================================
  



