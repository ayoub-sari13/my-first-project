!===============================================================================
module Bib_VOFLag_WallCreaction
  !===============================================================================
  contains
  !---------------------------------------------------------------------------  
  !> @author Abdelhamid Hakkoum
  ! 
  !> @brief 
  !
  !>
  !
  !>
  !>
  !>
  !>
  !>
  !>
  !---------------------------------------------------------------------------  
  subroutine WallCreaction(cou,cou_vis,cou_vts,couin0,cou_visin,cou_vtsin,slvu, &
                           slvv,slvw,smvu,smvv,smvw,slvuin,slvvin,slvwin,smvuin,&
                           smvvin,smvwin,WallPos)
    !===============================================================================
    !modules
    !===============================================================================
    !-------------------------------------------------------------------------------
    !Modules de definition des structures et variables => src/mod/module_...f90
    !-------------------------------------------------------------------------------
    use Bib_VOFLag_Wall_PhaseFunction
    use mod_Parameters                       , only : deeptracking,sx,ex,sy,ey,sz,   &
                                                      ez,sxu,syu,exu,eyu,szu,ezu,sxv,&
                                                      exv,syv,eyv,szv,ezv,sxw,exw,   &
                                                      syw,eyw,szw,ezw,grid_x,grid_y, &
                                                      grid_z,grid_xu,grid_yv,grid_zw,&
                                                      NS_pen_BC,dim,gsx,gsy,gsz,gsxu,&
                                                      gsyu,gszu,gsxv,gsyv,gszv,gsxw, &
                                                      gsyw,gszw

    !-------------------------------------------------------------------------------

    !===============================================================================
    !declarations des variables
    !===============================================================================
    implicit none
    !-------------------------------------------------------------------------------
    !variables globales
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:,:), allocatable, intent(inout) :: cou_vis,cou_vts,&
                                                               cou_visin,cou_vtsin
    real(8), dimension(:,:,:)  , allocatable, intent(inout) :: smvu,smvv,smvw
    real(8), dimension(:,:,:)  , allocatable, intent(inout) :: slvu,slvv,slvw
    real(8), dimension(:,:,:),   allocatable, intent(inout) :: slvuin,slvvin,slvwin
    real(8), dimension(:,:,:),   allocatable, intent(inout) :: smvuin,smvvin,smvwin
    real(8), dimension(:,:,:)  , allocatable, intent(inout) :: cou,couin0
    real(8), dimension(3)                   , intent(in)    :: WallPos
    !-------------------------------------------------------------------------------
    !variables locales 
    !-------------------------------------------------------------------------------
    real(8)                                                 :: pen_amp
    integer                                                 :: i,j,k
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'entree WallCreaction'
    !-------------------------------------------------------------------------------
    couin0   =0d0
    cou_visin=0d0
    slvuin   =0d0
    smvuin   =0d0
    slvvin   =0d0
    smvvin   =0d0
    if (dim==3) then 
      slvwin=0d0
      smvwin=0d0
    end if 
    pen_amp=NS_pen_BC
    call Wall_PhaseFunction(couin0,cou_visin,cou_vtsin,grid_x,grid_y,grid_z,&
                                 grid_xu,grid_yv,grid_zw,dim)
    if (dim==3) then 
      do k=szu,ezu
        do j=syu,eyu
          do i=sxu,exu
            if (grid_xu(i)<=0.5d0*WallPos(1).and.grid_xu(i)>grid_xu(gsxu+1)) then
              slvuin(i,j,k)=pen_amp
              smvuin(i,j,k)=0d0
            end if 
            if (grid_y(j)<=0.5d0*WallPos(2).and.grid_y(j)>grid_y(gsyu)) then
              slvuin(i,j,k)=pen_amp
              smvuin(i,j,k)=0d0
            end if 
            if (grid_z(k)<=0.5d0*WallPos(3).and.grid_z(k)>grid_z(gszu)) then
              slvuin(i,j,k)=pen_amp
              smvuin(i,j,k)=0d0
            end if 
          end do 
        end do
      end do
      do k=szv,ezv
        do j=syv,eyv
           do i=sxv,exv
              if (grid_x(i)<=0.5d0*WallPos(1).and.grid_x(i)>grid_x(gsxv)) then
                slvvin(i,j,k)=pen_amp
                smvvin(i,j,k)=0d0
              end if 
              if (grid_yv(j)<=0.5d0*WallPos(2).and.grid_yv(j)>grid_yv(gsyv)) then
                slvvin(i,j,k)=pen_amp
                smvvin(i,j,k)=0d0
              end if 
              if (grid_z(k)<=0.5d0*WallPos(3).and.grid_z(k)>grid_z(gszv)) then
                slvvin(i,j,k)=pen_amp
                smvvin(i,j,k)=0d0
              end if 
           end do 
        end do
      end do
      do k=szw,ezw
        do j=syw,eyw
           do i=sxw,exw
              if (grid_x(i)<=0.5d0*WallPos(1).and.grid_x(i)>grid_x(gsxw)) then
                slvwin(i,j,k)=pen_amp
                smvwin(i,j,k)=0d0
              end if 
              if (grid_y(j)<=0.5d0*WallPos(2).and.grid_y(j)>grid_y(gsyw)) then
                slvwin(i,j,k)=pen_amp
                smvwin(i,j,k)=0d0
              end if 
              if (grid_zw(k)<=0.5d0*WallPos(3).and.grid_zw(k)>grid_zw(gszw)) then
                slvwin(i,j,k)=pen_amp
                smvwin(i,j,k)=0d0
              end if 
           end do 
        end do
      end do 
    end if 
    slvu=slvuin
    slvv=slvvin
    if (dim==3) slvw=slvwin
    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'sortie WallCreaction'
    !-------------------------------------------------------------------------------
  end subroutine WallCreaction

  !===============================================================================
end module Bib_VOFLag_WallCreaction
!===============================================================================