!===============================================================================
module Bib_VOFLag_Ellipsoid_PointIn
  !===============================================================================
  contains
  !-----------------------------------------------------------------------------  
  !> @author amine chadil
  ! 
  !> @brief cette routine situe le point p (a l'interieur ou a l'exterieur de l'ellipsoide) 
  !
  !> @param[in]  vl           : la structure contenant toutes les informations
  !! relatives aux particules
  !> @param[in]  np           : numero de la particule dans vl
  !> @param[in]  nbt          : numero de la copie de la particule np
  !> @param[in]  sca         : les dimensions de copie nbt de la particule np avec lesquelles
  !! le test sera fait
  !> @param[in]  p            : le point a tester
  !> @param[out] in_ellipsoid : vrai si le point est a l'interieur de l'ellipsoide faux sinon
  !-----------------------------------------------------------------------------
  subroutine Ellipsoid_PointIn(vl,np,nbt,sca,p,ndim,in_ellipsoid)
    !===============================================================================
    !modules
    !===============================================================================
    !-------------------------------------------------------------------------------
    !Modules de definition des structures et variables => src/mod/module_...f90
    !-------------------------------------------------------------------------------
    use Module_VOFLag_ParticlesDataStructure
    use mod_Parameters,                      only : deeptracking
    !-------------------------------------------------------------------------------
    !Modules de subroutines de la librairie RESPECT => src/Bib_VOFLag_...f90
    !-------------------------------------------------------------------------------
    use Bib_VOFLag_Vector_FromInertial2ParticleFrame
    !-------------------------------------------------------------------------------

    !===============================================================================
    !declarations des variables
    !===============================================================================
    implicit none
    !-------------------------------------------------------------------------------
    !variables globales
    !-------------------------------------------------------------------------------
    real(8), dimension(3) ,intent(in)  :: p,sca
    type(struct_vof_lag), intent(in)            :: vl
    logical, intent(out)                        :: in_ellipsoid
    integer, intent(in)                         :: np,nbt 
    integer, intent(in)                         :: ndim
    !-------------------------------------------------------------------------------
    !variables locales 
    !-------------------------------------------------------------------------------
    double precision, dimension(3)              :: pos_ellipsoid,p_in_ellipsoid_frame
    double precision                            :: distance,equation_ellipsoid,grand_axe
    integer                                     :: nd
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    !if (deeptracking) write(*,*) 'entree Ellipsoid_PointIn'
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    !Initialisation
    !-------------------------------------------------------------------------------
    in_ellipsoid = .false.
    pos_ellipsoid(1:ndim)=vl%objet(np)%symper(nbt,1:ndim)
    distance = 0.0d0
    grand_axe = max(sca(1),sca(2))
    if (ndim .eq. 3) grand_axe = max(grand_axe,sca(3))

    !-------------------------------------------------------------------------------
    ! calcul de la distance entre p et l'origine de la particule
    !-------------------------------------------------------------------------------
    do nd=1,ndim
      distance = distance + (p(nd)-pos_ellipsoid(nd))**2
    end do
    distance = sqrt(distance)
    
    !-------------------------------------------------------------------------------
    ! si le point appartient a la sphere englobante 
    !-------------------------------------------------------------------------------
    if (distance .le. grand_axe) then
      call Vector_FromInertial2ParticleFrame(vl,np,nbt,ndim,.true.,p,p_in_ellipsoid_frame)

      !-------------------------------------------------------------------------------
      ! utiliser l'equation de l'ellipsoide pour situer le point par rapport a ce dernier.
      !-------------------------------------------------------------------------------
      equation_ellipsoid = 0.0d0
      do nd=1,ndim
        equation_ellipsoid = equation_ellipsoid + (p_in_ellipsoid_frame(nd)/(sca(nd)+1D-40))**2
      end do
      if (equation_ellipsoid .le. 1.0d0) in_ellipsoid = .true.

    end if
    !-------------------------------------------------------------------------------
    !if (deeptracking) write(*,*) 'sortie Ellipsoid_PointIn'
    !-------------------------------------------------------------------------------
  end subroutine Ellipsoid_PointIn

  !===============================================================================
end module Bib_VOFLag_Ellipsoid_PointIn
!===============================================================================
  



