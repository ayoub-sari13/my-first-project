!===============================================================================
module Bib_VOFLag_TaylorInterpolation_PressureMesh
  !===============================================================================
  contains
  !-----------------------------------------------------------------------------  
  !> @author amine chadil
  ! 
  !> @brief cette routine interpole la grandeur pre de la grille de pression dans 
  !! le point ptlag en utilisant Taylor
  !
  !> @param [out]  ttp        : la valeur extrapolee au point ptlag.
  !> @param [in]  i,j,k      : le point de pression le plus proche du point ptlag.
  !> @param [in] ptlag      : le point ou on veut extrapoler la pression.
  !> @param [in]  pres       : la grandeur a extrapolee de la grille de pression
  !-----------------------------------------------------------------------------
  subroutine TaylorInterpolation_PressureMesh (pres,i,j,k,norm,Order,ptlag,ttp) 
    !===============================================================================
    !modules
    !===============================================================================
    !-------------------------------------------------------------------------------
    !Modules de definition des structures et variables => src/mod/module_...f90
    !-------------------------------------------------------------------------------
    use mod_Parameters,                       only : deeptracking,dim,grid_x,grid_y,&
                                                     grid_z,dx,dy,dz
    !-------------------------------------------------------------------------------
    !Modules de subroutines de la librairie VOF-Lag
    !-------------------------------------------------------------------------------
    use Bib_VOFLag_TaylorInterpolation_PressureMesh_ThirdDerivative
    use Bib_VOFLag_TaylorInterpolation_PressureMesh_Gradiant
    use Bib_VOFLag_TaylorInterpolation_PressureMesh_Hessian    
    use Bib_VOFLag_TaylorInterpolation
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------

    !===============================================================================
    !declarations des variables
    !===============================================================================
    implicit none
    !-------------------------------------------------------------------------------
    !variables globales
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable, intent(in)  :: pres
    real(8), dimension(dim)               , intent(in)  :: norm
    real(8), dimension(dim)               , intent(in)  :: ptlag
    integer                               , intent(in)  :: i,j,k,Order
    real(8)                               , intent(out) :: ttp
    
    !-------------------------------------------------------------------------------
    !variables locales 
    !-------------------------------------------------------------------------------
    real(8), dimension(dim,dim,dim)                    :: ThirdDerivative
    real(8), dimension(dim,dim)                        :: Hess
    real(8), dimension(dim)                            :: grad
    !-------------------------------------------------------------------------------
    if (Order .ge. 2) then
      call TaylorInterpolation_PressureMesh_Gradiant (pres,i,j,k,Order,norm,grad,&
                                                      dim,dx,dy,dz)
      if (Order .ge. 3) then
        call TaylorInterpolation_PressureMesh_Hessian (pres,i,j,k,Order,norm,Hess,dim,&
                                                       dx,dy,dz)
        if (Order .ge. 4) then
          call TaylorInterpolation_PressureMesh_ThirdDerivative (pres,i,j,k,Order,norm,&
                                                                 ThirdDerivative,dim, &
                                                                 dx,dy,dz)
        endif
      endif
    endif
    !-------------------------------------------------------------------------------
    call TaylorInterpolation(pres,i,j,k,norm,Order,ptlag,ttp,grad,Hess,&
                                 ThirdDerivative,dim,grid_x,grid_y,grid_z) 
    !-------------------------------------------------------------------------------
  end subroutine TaylorInterpolation_PressureMesh

  !===============================================================================
end module Bib_VOFLag_TaylorInterpolation_PressureMesh
!===============================================================================
  

