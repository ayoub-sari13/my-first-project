module mod_stl_num
  implicit none

  integer, parameter :: EPS_MT              = 1d-7
  integer, parameter :: EPS_SHIFT_MT        = 1d-7
  integer, parameter :: EPS_DET_WATER_TIGHT = 1d-12

  integer, parameter :: MOLLER_TRUMBORE     = 1
  integer, parameter :: WATER_TIGHT         = 2

contains

  subroutine TriangleVectorIntersection(imethod,xyzA,xyzB,xyzC,rayOrigin,rayVector,intersect,intersectPoint,intersectError)
    implicit none
    !-------------------------------------------------------------------------------
    ! global variable
    !-------------------------------------------------------------------------------
    integer, intent(in)                :: imethod
    real(8), dimension(3), intent(in)  :: xyzA,xyzB,xyzC,rayOrigin,rayVector
    real(8), dimension(3), intent(out) :: intersectPoint
    logical, intent(out)               :: intersect,intersectError
    !-------------------------------------------------------------------------------
    ! local variables
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------

    select case(imethod)
    case(MOLLER_TRUMBORE)
       call VecIntersectsTriangle_MT(xyzA, xyzB, xyzC,   &
            rayOrigin, rayVector, intersect, intersectPoint)
       intersectError=.false.
    case(WATER_TIGHT)
       call watertight_intersection(rayOrigin,rayVector,xyzA,xyzB,xyzC, &
            & maxloc(abs(rayVector),dim=1),                             &
            & intersect,intersectError,intersectPoint)
    end select

  end subroutine TriangleVectorIntersection


  subroutine RayIntersectsTriangle_MT(xyzA,xyzB,xyzC,rayOrigin,rayVector,intersect,intersectPoint)
    use mod_math
    implicit none
    !-------------------------------------------------------------------------------
    ! global variable
    !-------------------------------------------------------------------------------
    real(8), dimension(3), intent(in)  :: xyzA,xyzB,xyzC,rayOrigin,rayVector
    real(8), dimension(3), intent(out) :: intersectPoint
    logical, intent(out)               :: intersect
    !-------------------------------------------------------------------------------
    ! local variables
    !-------------------------------------------------------------------------------
    real(8)                            :: a,f,t,u,v
    real(8)                            :: eps 
    real(8), dimension(3)              :: edge1,edge2,h,s,q
    !-------------------------------------------------------------------------------

    eps = EPS_MT

    edge1 = xyzB-xyzA
    edge2 = xyzC-xyzA

    h = cross_product(rayVector,edge2)
    a = dot_product(edge1,h)

    if (a>-eps .and. a<eps) then
       !write(*,*) "le triangle est parallele au rayon lance"
       intersect = .false.
       return
    end if

    f = 1/a
    s = rayOrigin-xyzA

    u = f*dot_product(s,h)

    if (u<0 .or. u>1) then
       !write(*,*) "intersection 1 mais pas dans le triangle"
       intersect = .false.
       return
    end if

    q = cross_product(s,edge1)
    v = f*dot_product(rayVector,q)
    if (v<0 .or. u+v>1) then
       !write(*,*) "intersection 2 mais pas dans le triangle"
       intersect = .false.
       return
    end if

    t = f*dot_product(edge2,q)
    if (t>eps) then
       intersect      = .true.
       intersectPoint = rayOrigin + rayVector * t
    else
       intersect = .false.
    end if

  end subroutine RayIntersectsTriangle_MT


  subroutine VecIntersectsTriangle_MT(xyzA,xyzB,xyzC,rayOrigin,rayVector,intersect,intersectPoint)
    implicit none
    !-------------------------------------------------------------------------------
    ! global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(3), intent(in)  :: xyzA,xyzB,xyzC,rayOrigin,rayVector
    real(8), dimension(3), intent(out) :: intersectPoint
    logical, intent(out)               :: intersect
    !-------------------------------------------------------------------------------
    ! local variables
    !-------------------------------------------------------------------------------
    real(8), dimension(3)              :: s
    !-------------------------------------------------------------------------------

    intersect = .false.
    call RayIntersectsTriangle_MT(xyzA,xyzB,xyzC,rayOrigin,rayVector,intersect,intersectPoint)

    if (intersect) then 
       s = intersectPoint-rayOrigin
       if (dot_product(s,s) < dot_product(rayVector,rayVector)) then
          intersect = .true.
       else
          intersect = .false.
       end if
    end if

  end subroutine VecIntersectsTriangle_MT

  
  subroutine watertight_intersection(M,dir,A,B,C,ray_axis,wt_intersection,intersection_error,intersection)
    implicit none
    !--------------------------------------------------------------------------
    integer, intent(in)                :: ray_axis
    real(8), dimension(3), intent(in)  :: M,dir,A,B,C
    real(8), dimension(3), intent(out) :: intersection
    logical, intent(inout)             :: intersection_error,wt_intersection
    !--------------------------------------------------------------------------
    integer                            :: kx,ky,kz,k_tmp
    real(8)                            :: Ax,Ay,Az,Bx,By,Bz,Cx,Cy,Cz,U,V,W
    real(8)                            :: det,T,shearx,sheary,shearz,epsilon 
    real(8), dimension(3)              :: A_rel,B_rel,C_rel
    !--------------------------------------------------------------------------


    !--------------------------------------------------------------------------
    ! Watertight Ray/Triangle Intersection Sven Woop Carsten Benthin Intel Labs Ingo Wald 2013
    !--------------------------------------------------------------------------
    wt_intersection = .false.
    kz              = ray_axis
    kx              = modulo(kz,3)+1
    ky              = modulo(kx,3)+1
    !--------------------------------------------------------------------------

    !--------------------------------------------------------------------------
    ! Swap kx and ky dimension to preserve winding direction of triangle
    !--------------------------------------------------------------------------
    if (dir(kz)<0) then
       k_tmp = ky
       ky    = kx
       kx    = k_tmp
    endif
    !--------------------------------------------------------------------------

    !--------------------------------------------------------------------------
    ! Calculate the shear constants
    !--------------------------------------------------------------------------
    shearx = dir(kx)/dir(kz)
    sheary = dir(ky)/dir(kz)
    shearz = 1/dir(kz)
    !--------------------------------------------------------------------------

    !--------------------------------------------------------------------------
    ! Calculate the vertices relative to the ray origin 
    ! The coordinates are permuted according to kx,ky,kz
    !--------------------------------------------------------------------------
    A_rel = (/ A(kx)-M(kx), A(ky)-M(ky), A(kz)-M(kz) /)
    B_rel = (/ B(kx)-M(kx), B(ky)-M(ky), B(kz)-M(kz) /)
    C_rel = (/ C(kx)-M(kx), C(ky)-M(ky), C(kz)-M(kz) /)
    !--------------------------------------------------------------------------

    !--------------------------------------------------------------------------
    ! Perform shear and scale on vertices
    !--------------------------------------------------------------------------
    Ax = A_rel(1) - shearx * A_rel(3)
    Ay = A_rel(2) - sheary * A_rel(3)
    Bx = B_rel(1) - shearx * B_rel(3)
    By = B_rel(2) - sheary * B_rel(3)
    Cx = C_rel(1) - shearx * C_rel(3)
    Cy = C_rel(2) - sheary * C_rel(3)
    !--------------------------------------------------------------------------

    !--------------------------------------------------------------------------
    ! Calculate scaled barycentric coordinates
    ! U,V,W are signed distances for each edge of the triangle
    !--------------------------------------------------------------------------
    U = Cx * By - Cy * Bx
    V = Ax * Cy - Ay * Cx
    W = Bx * Ay - By * Ax
    !--------------------------------------------------------------------------

    !--------------------------------------------------------------------------
    ! We are already in double precision with real(8)
    ! no need to recompute U,V,W for a "fallback" to double precision
    !  backface_culling=.false. !hides the back
    !--------------------------------------------------------------------------
    epsilon = 0!+0!zero!1.d-8
    !--------------------------------------------------------------------------


    !  if (backface_culling) then
    !    if ((U<epsilon) .or. (V<epsilon) .or. (W<epsilon)) then
    !      return
    !    endif
    !  else 
    if ( ( (U<epsilon) .or. (V<epsilon) .or. (W<epsilon) ) .and. &
         ( (U>epsilon) .or. (V>epsilon) .or. (W>epsilon) ) ) then
       ! If the signed distances are not of the same sign, 
       ! the point is outside of the triangle
       ! The edge functions can be of negative sign
       return !triangle edge test :false 
    endif
    !  endif

    !--------------------------------------------------------------------------
    ! Calculate determinant
    !--------------------------------------------------------------------------
    det = U + V + W
    !--------------------------------------------------------------------------

    !--------------------------------------------------------------------------
    !C o-planar case :
    !--------------------------------------------------------------------------
    !  if (det == zero) return !determinant test : the ray is coplanar to the triangle
    !  if (det == zero) return !determinant test : the ray is coplanar to the triangle
    !  if (abs(det) < 1d-7) return !determinant test : the ray is coplanar to the triangle
    if (abs(det) < EPS_DET_WATER_TIGHT) return !determinant test : the ray is coplanar to the triangle
    !--------------------------------------------------------------------------

    !  if (det == zero) then
    !    intersection_error=.true.
    !    return !determinant test : the ray is coplanar to the triangle
    !  endif

    ! The inverse of det can be then used later

    !--------------------------------------------------------------------------
    ! Calculate scaled z-coordinates of vertices and use them to calculate the
    ! hit distance
    !--------------------------------------------------------------------------
    Az = shearz * A_rel(3)
    Bz = shearz * B_rel(3)
    Cz = shearz * C_rel(3)
    !--------------------------------------------------------------------------

    !--------------------------------------------------------------------------
    ! Parametric distance from ray origin to intersection : t=T/det
    ! Real distance = t * dir(kz) = T/det*dir(kz)
    !--------------------------------------------------------------------------
    T = U * Az + V * Bz + W * Cz
    !--------------------------------------------------------------------------

    !--------------------------------------------------------------------------
    !  if ((det < 0) .and. ((T >= 0) .or. (T < det))) then !T<1*det
    !     return
    ! else if ((det > 0) .and. ((T <= 0) .or. (T > det))) then
    !     return
    !  endif
    !TODO we want <=det to go up to the node and not strictly before
    !--------------------------------------------------------------------------
    if ((det < 0) .and. ((T > 0) .or. (T <= det))) then !T<1*det
       return
    else if ((det > 0) .and. ((T < 0) .or. (T >= det))) then
       return
    endif
    !--------------------------------------------------------------------------

    intersection = T/det*dir+M

    !--------------------------------------------------------------------------
    !we do not want to count the summits tangently
    !if we are right under the summit there is no problem :
    !the rays will hit one triangle and then another on the other side
    !TODO : edge tangentially : there is a local max/min but with current decompte we count markers out of the 
    !volume of control 
    !--------------------------------------------------------------------------
    if ((U==0) .or. (V==0) .or. (W==0)) then
       !We are either on a vertex or an edge
       !Behind this edge/vertex there may be a bubble or the same fluid if the ray arrives tangentially
       intersection_error=.true.
       if (intersection_error) then
          write(*,*) 'Intersection error'
          write(*,*) 'Intersection error'
          write(*,*) 'Intersection error'
          write(*,*) 'Intersection error'
          write(*,*) 'Intersection error'
       endif
    endif
    !--------------------------------------------------------------------------
    
    !--------------------------------------------------------------------------
    ! Else there is an intersection
    !--------------------------------------------------------------------------
    wt_intersection = .true.
    !--------------------------------------------------------------------------

  end subroutine watertight_intersection


end module mod_stl_num
