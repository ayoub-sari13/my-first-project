!===============================================================================
module Bib_VOFLag_Vector_FromInertial2ParticleFrame
  !===============================================================================
  contains
  !-----------------------------------------------------------------------------  
  !> @author mohamed-amine chadil
  ! 
  !> @brief this function give the coordinate in the inertial frame of a vector expressed in 
  !! in the particle frame and vice-versa depending of the "in_particle_frame":
  !!  - from the particle frame to the inertial frame if in_particle_frame is true
  !!  - from the inertial frame to the particle frame if in_particle_frame is false
  !
  !> @param[in]   vl                 : la structure contenant toutes les informations
  !! relatives aux particules
  !> @param[in]   np                 : numero de la particule dans vl
  !> @param[in]   nbt                : numero de la copie de la particule np
  !> @param[in]  in_particle_frame   : 
  !> @param[in]  p_in                : Les coordonnees de p_in dans le repere fixe
  !> @param[out] p_out               : Les coordonnees de p dans le repere de sortie         
  !-----------------------------------------------------------------------------
  subroutine Vector_FromInertial2ParticleFrame(vl,np,nbt,ndim,in_particle_frame,p_in,p_out)   
    !===============================================================================
    !modules
    !===============================================================================
    !-------------------------------------------------------------------------------
    !Modules de definition des structures et variables => src/mod/module_...f90
    !-------------------------------------------------------------------------------
    use Module_VOFLag_ParticlesDataStructure
    use mod_Parameters,                      only : deeptracking
    !-------------------------------------------------------------------------------
    !Modules de subroutines de la librairie VOF-Lag
    !-------------------------------------------------------------------------------
    use Bib_VOFLag_Vector_FromComoving2ParticleFrame
    !-------------------------------------------------------------------------------
    !===============================================================================
    !declarations des variables
    !===============================================================================
    implicit none
    !-------------------------------------------------------------------------------
    !variables globales
    !-------------------------------------------------------------------------------
    real(8), dimension(3), intent(out)           :: p_out
    type(struct_vof_lag),intent(in)              :: vl
    real(8), dimension(3), intent(in)            :: p_in
    integer,intent(in)                           :: np,nbt,ndim  
    logical,intent(in)                           :: in_particle_frame
    !-------------------------------------------------------------------------------
    !variables locales 
    !-------------------------------------------------------------------------------
    real(8), dimension(3)               :: pos_part,p_in_comoving,p_out_comoving
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    !if (deeptracking) write(*,*) 'entree Vector_FromInertial2ParticleFrame'
    !-------------------------------------------------------------------------------

    pos_part = vl%objet(np)%symper(nbt,:)
    
    if (in_particle_frame) then
       !-------------------------------------------------------------------------------
       !p_in from_inertial_to_comoving
       !-------------------------------------------------------------------------------
       p_in_comoving=p_in-pos_part

       !-------------------------------------------------------------------------------
       !calcul des coordonnees de p dans le repere de la particule
       !-------------------------------------------------------------------------------
       call Vector_FromComoving2ParticleFrame(vl,np,ndim,in_particle_frame,p_in_comoving,p_out)

    else
       !-------------------------------------------------------------------------------
       !calcul des coordonnees de p dans le repere fixe
       !-------------------------------------------------------------------------------
       call Vector_FromComoving2ParticleFrame(vl,np,ndim,in_particle_frame,p_in,p_out_comoving)

       !-------------------------------------------------------------------------------
       !p_out from_comoving_to_inertial
       !-------------------------------------------------------------------------------
       p_out = p_out_comoving+pos_part

    end if

    !-------------------------------------------------------------------------------
    !if (deeptracking) write(*,*) 'sortie Vector_FromInertial2ParticleFrame'
    !-------------------------------------------------------------------------------
  end subroutine Vector_FromInertial2ParticleFrame

  !===============================================================================
end module Bib_VOFLag_Vector_FromInertial2ParticleFrame
!===============================================================================
  


