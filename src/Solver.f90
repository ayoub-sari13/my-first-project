!========================================================================
!
!                   FUGU Version 1.0.0
!
!========================================================================
!
! Copyright  Stéphane Vincent
!
! stephane.vincent@u-pem.fr          straightparadigm@gmail.com
!
! This file is part of the FUGU Fortran library, a set of Computational 
! Fluid Dynamics subroutines devoted to the simulation of multi-scale
! multi-phase flows for resolved scale interfaces (liquid/gas/solid). 
! FUGU uses the initial developments of DyJeAT and SHALA codes from 
! Jean-Luc Estivalezes (jean-luc.estivalezes@onera.fr) and 
! Frédéric Couderc (couderc@math.univ-toulouse.fr).
!
!========================================================================
!**
!**   NAME       : solver.f90
!**
!**   AUTHOR     : Stéphane Vincent
!**
!**   FUNCTION   : iterative and direct solver modules of FUGU software
!**
!**   DATES      : Version 1.0.0  : from : january, 16, 2017
!**
!========================================================================

#define OPTIM_PSCALEE 1
#define OPTIM_MATVEKE 0

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#include "precomp.h"
module mod_solver
  use mod_timers
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  !--------------------------------------------------------------
  real(8) :: coeff=1!acos(-1d0)
  !--------------------------------------------------------------
  ! Equation solved
  !   mpi_ieq_solve = 0 --> Navier-Stokes
  !                   1 --> Scalar equation
  !--------------------------------------------------------------
  integer :: mpi_ieq_solve=0
  !--------------------------------------------------------------

contains

  !-------------------------------------------------------------------------------
  ! Scalar product
  !-------------------------------------------------------------------------------

  !*******************************************************************************
  subroutine pscalee (psa,qsa,spa,npt,kic,nic)
    !*******************************************************************************
    use mod_mpi

    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)                :: npt,nic
    integer, dimension(npt)            :: kic
    real(8), dimension(npt),intent(in) :: psa,qsa
    real(8), intent(out)               :: spa
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                            :: l,n
    real(8)                            :: spag
    !-------------------------------------------------------------------------------
    
    call compute_time(TIMER_START,"[sub] pscalee")

    !-------------------------------------------------------------------------------
    spa = 0
    do n = 1,nic
       l = kic(n)
       spa = spa + psa(l) * qsa(l)
    enddo
    
    if (nproc>1) then 
       call mpi_allreduce(spa,spag,1,mpi_double_precision,mpi_sum,comm3D,code)
       spa=spag
    end if

    !-------------------------------------------------------------------------------
    
    call compute_time(TIMER_END,"[sub] pscalee")
  end subroutine pscalee

  subroutine pscalee5 (psa,qsa,rsa,spa1,spa2,spa3,spa4,spa5,npt,kic,nic)
    !*******************************************************************************
    use mod_mpi

    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)                :: npt,nic
    integer, dimension(npt)            :: kic
    real(8), dimension(npt),intent(in) :: psa,qsa,rsa
    real(8), intent(out)               :: spa1,spa2,spa3,spa4,spa5
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                            :: l,n
    real(8)                            :: spag
    !-------------------------------------------------------------------------------

    call compute_time(TIMER_START,"[sub] pscalee5")

    !-------------------------------------------------------------------------------
    spa1 = 0
    spa2 = 0
    spa3 = 0
    spa4 = 0
    spa5 = 0
    do n = 1,nic
       l = kic(n)
       spa1 = spa1 + psa(l) * qsa(l)
       spa2 = spa2 + qsa(l) * qsa(l)
       spa3 = spa3 + qsa(l) * rsa(l)
       spa4 = spa4 + rsa(l) * rsa(l)
       spa5 = spa5 + psa(l) * rsa(l)
    enddo

    if (nproc>1) then 
       call mpi_allreduce(spa1,spag,1,mpi_double_precision,mpi_sum,comm3D,code);spa1=spag
       call mpi_allreduce(spa2,spag,1,mpi_double_precision,mpi_sum,comm3D,code);spa2=spag
       call mpi_allreduce(spa3,spag,1,mpi_double_precision,mpi_sum,comm3D,code);spa3=spag
       call mpi_allreduce(spa4,spag,1,mpi_double_precision,mpi_sum,comm3D,code);spa4=spag
       call mpi_allreduce(spa5,spag,1,mpi_double_precision,mpi_sum,comm3D,code);spa5=spag
    end if

    !-------------------------------------------------------------------------------
    
    call compute_time(TIMER_END,"[sub] pscalee5")
  end subroutine pscalee5


  !-------------------------------------------------------------------------------
  ! Matrix-vector product
  !-------------------------------------------------------------------------------

  !*******************************************************************************
  subroutine matveke (npt,sol,ysa,coef,jcof,icof,nps)
    !*******************************************************************************
    use mod_Parameters, only: rank,nb_V,nproc,     &
         & nb_Vx,nb_Vy,ex,sx,ey,sy,ez,sz,gx,gy,gz, &
         & sxu,exu,syu,eyu,szu,ezu,                &
         & sxv,exv,syv,eyv,szv,ezv,                &
         & sxw,exw,syw,eyw,szw,ezw,                &
         & sxs,exs,sys,eys,szs,ezs,                &
         & sxus,exus,syus,eyus,szus,ezus,          &
         & sxvs,exvs,syvs,eyvs,szvs,ezvs,          &
         & sxws,exws,syws,eyws,szws,ezws
    use mod_solver_comm_mpi
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)                    :: npt,nps
    integer, dimension(npt+1), intent(in)  :: icof
    integer, dimension(nps), intent(in)    :: jcof
    real(8), dimension(nps), intent(in)    :: coef
    real(8), dimension(npt), intent(inout) :: sol
    real(8), dimension(npt), intent(out)   :: ysa
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                :: i,j,k,l
    real(8)                                :: t
    !-------------------------------------------------------------------------------

    call compute_time(TIMER_START,"[sub] matveke")
    
    ysa=0

    if (nproc>1) call solver_comm_mpi(sol,npt,mpi_ieq_solve)

    !-------------------------------------------------------------------------------
    do i = 1,npt
       t = 0
       do k = icof(i),icof(i+1)-1
          t = t + coef(k)*sol(jcof(k))
       enddo
       ysa(i) = t
    enddo
    !-------------------------------------------------------------------------------
    call compute_time(TIMER_END,"[sub] matveke")
  end subroutine matveke

  !-------------------------------------------------------------------------------
  ! Matrix factorization
  !-------------------------------------------------------------------------------

  subroutine factor(pcase,coef,jcof,icof,alu,npt,nps,ndd,kic,nic)
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    integer                   :: npt,nps,ndd,nic,pcase
    integer, dimension(npt+1) :: icof
    integer, dimension(nps)   :: jcof
    integer, dimension(npt)   :: kic
    real(8)                   :: preci
    real(8), dimension(nps)   :: coef
    real(8), dimension(nps)   :: alu
    !-------------------------------------------------------------------------------

    select case(pcase)
    case(0) ! Jacobi
       call facjac(coef,jcof,icof,alu,npt,nps,ndd,kic,nic)
    case(1) ! ILU
       call faciluke(coef,jcof,icof,alu,npt,nps,ndd,kic,nic)
    end select

  end subroutine factor
  
  !*******************************************************************************

  subroutine faciluke (coef,jcof,icof,alu,npt,nps,ndd,kic,nic)
    use mod_mpi
    !*******************************************************************************
    ! ilu(0) factorization for ndd diagonals
    !-------------------------------------------------------------------------------
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    integer                   :: npt,nps,ndd,nic
    integer, dimension(npt+1) :: icof
    integer, dimension(nps)   :: jcof
    integer, dimension(npt)   :: kic
    real(8)                   :: preci
    real(8), dimension(nps)   :: coef
    real(8), dimension(nps)   :: alu
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                   :: j,k,l,n
    !-------------------------------------------------------------------------------

    preci=1d-15
    alu=0

    !-------------------------------------------------------------------------------
    do n = 1,nic
       l = kic(n)
       do k = icof(l),icof(l)+ndd-1,2
          alu(k) = coef(k)
       enddo
    enddo
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    do j = 1,nic
       l = kic(j)
       do k = icof(l)+1,icof(l)+ndd-1,2
          alu(k) = coef(k) / ( alu(icof(jcof(k))) + preci )
       enddo
       n = 0
       do k = icof(l)+1,icof(l)+ndd-1,2
          n = n + 2
          alu(icof(l)) = alu(icof(l)) - alu(k)*alu(icof(jcof(k))+n)
       enddo
    enddo

    return
  end subroutine faciluke
  
  !*******************************************************************************
  
  subroutine facjac (coef,jcof,icof,alu,npt,nps,ndd,kic,nic)
    use mod_mpi
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    integer                   :: npt,nps,ndd,nic
    integer, dimension(npt+1) :: icof
    integer, dimension(nps)   :: jcof
    integer, dimension(npt)   :: kic
    real(8)                   :: preci
    real(8), dimension(nps)   :: coef
    real(8), dimension(nps)   :: alu
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------

    alu=0
    
    return
  end subroutine facjac

  !-------------------------------------------------------------------------------
  ! Preconditionning
  !-------------------------------------------------------------------------------
  
  subroutine precond(pcase,npt,sol,ysa,alu,jcof,icof,ndd,nps,kic,nic)
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    integer                   :: npt,ndd,nps,nic,pcase
    integer, dimension(npt+1) :: icof
    integer, dimension(nps)   :: jcof
    integer, dimension(npt)   :: kic
    real(8), dimension(nps)   :: alu
    real(8), dimension(npt)   :: sol,ysa
    !------------------------ -------------------------------------------------------

    select case(pcase)
    case(0) ! Jacobi
       call preconjac (npt,sol,ysa,alu,jcof,icof,ndd,nps,kic,nic)
    case(1) ! ILU
       call lusolke (npt,sol,ysa,alu,jcof,icof,ndd,nps,kic,nic)
    end select

  end subroutine precond
  
  !*******************************************************************************

  subroutine lusolke (npt,sol,ysa,alu,jcof,icof,ndd,nps,kic,nic)
    !*******************************************************************************
    ! LU preconditionning
    !*******************************************************************************
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    integer                   :: npt,ndd,nps,nic
    integer, dimension(npt+1) :: icof
    integer, dimension(nps)   :: jcof
    integer, dimension(npt)   :: kic
    real(8), dimension(nps)   :: alu
    real(8), dimension(npt)   :: sol,ysa
    !------------------------ -------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                   :: k,l,n
    !-------------------------------------------------------------------------------

    sol=0
    !-------------------------------------------------------------------------------
    ! Forward solve
    !-------------------------------------------------------------------------------
    do n = 1,nic
       l = kic(n)
       sol(l) = ysa(l)
    end do
    
    do n = 1,nic
       l = kic(n)
       do k = icof(l)+1,icof(l)+ndd-1,2
          sol(l) = sol(l) - alu(k)*sol(jcof(k))
       enddo
    enddo
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Backward solve
    !-------------------------------------------------------------------------------
    do n = nic,1,-1
       l = kic(n)
       do k = icof(l)+2,icof(l)+ndd-1,2
          sol(l) = sol(l) - alu(k)*sol(jcof(k))
       enddo
       sol(l) = sol(l) / ( alu(icof(l)) + 1.d-20 )
    enddo
    !-------------------------------------------------------------------------------

    return
  end subroutine lusolke
  
  !*******************************************************************************

  subroutine preconjac (npt,sol,ysa,alu,jcof,icof,ndd,nps,kic,nic)
    !*******************************************************************************
    ! Jacobi preconditionning
    !*******************************************************************************

    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    integer                   :: npt,ndd,nps,nic
    integer, dimension(npt+1) :: icof
    integer, dimension(nps)   :: jcof
    integer, dimension(npt)   :: kic
    real(8), dimension(nps)   :: alu
    real(8), dimension(npt)   :: sol,ysa
    !------------------------ -------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                   :: k,l,n
    !-------------------------------------------------------------------------------

    sol=0
    do n = 1,nic
       l = kic(n)
       sol(l) = ysa(l)
    end do

    return
  end subroutine preconjac

  !-------------------------------------------------------------------------------
  ! BiCGStab II iterative solver
  !-------------------------------------------------------------------------------

  !*******************************************************************************
  subroutine bstak2e(coef,jcof,sol,icof,alu,psa,qsa,rsa,ssa,tsa, &
       usa,vsa,wsa,rho0,alp,ome2,ndd,npt,nps,kic,nic,pcase)
    !*******************************************************************************


    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    integer                   :: nps,ndd,npt,nic,pcase
    integer, dimension(npt+1) :: icof
    integer, dimension(nps)   :: jcof
    integer, dimension(npt)   :: kic
    real(8), dimension(nps)   :: coef,alu
    real(8), dimension(npt)   :: sol
    real(8), dimension(npt)   :: psa,qsa,rsa,ssa,tsa,usa,vsa,wsa
    real(8)                   :: rho0,alp,ome2
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                   :: l,n
    real(8)                   :: bet,gam,ome1,rho1,smu,snu,tau
    real(8)                   :: prec
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    prec= coeff*1d-60
    rho0 = -ome2*rho0
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Even step
    !-------------------------------------------------------------------------------

    !===============================================================================
    call pscalee (qsa,rsa,rho1,npt,kic,nic)  

    bet = alp * rho1 / rho0
    rho0 = rho1

    do l = 1,npt
       usa(l) = rsa(l) - bet*usa(l)
    enddo

    call matveke(npt,usa,psa,coef,jcof,icof,nps)
    call precond(pcase,npt,vsa,psa,alu,jcof,icof,ndd,nps,kic,nic)
    call pscalee(qsa,vsa,gam,npt,kic,nic)


    alp = rho0 / gam

    do l = 1,npt
       rsa(l) = rsa(l) - alp*vsa(l)
    enddo

    call matveke(npt,rsa,psa,coef,jcof,icof,nps)
    call precond(pcase,npt,ssa,psa,alu,jcof,icof,ndd,nps,kic,nic)

    sol = sol + alp*usa

    !-------------------------------------------------------------------------------
    ! Odd step
    !-------------------------------------------------------------------------------

    !===============================================================================
    call pscalee(qsa,ssa,rho1,npt,kic,nic)

    bet = alp * rho1 / rho0
    rho0 = rho1
    vsa = ssa - bet*vsa

    call matveke(npt,vsa,psa,coef,jcof,icof,nps)
    call precond(pcase,npt,wsa,psa,alu,jcof,icof,ndd,nps,kic,nic)
    call pscalee(qsa,wsa,gam,npt,kic,nic)

    alp = (rho0+prec) / (gam+prec)

    usa = rsa - bet*usa
    rsa = rsa - alp*vsa
    ssa = ssa - alp*wsa

    call matveke(npt,ssa,psa,coef,jcof,icof,nps)
    call precond(pcase,npt,tsa,psa,alu,jcof,icof,ndd,nps,kic,nic)

    !-------------------------------------------------------------------------------
    !cgr(2) part
    !-------------------------------------------------------------------------------

#if OPTIM_PSCALEE
    call pscalee5(rsa,ssa,tsa,ome1,smu,snu,tau,ome2,npt,kic,nic)
#else
    !===============================================================================
    call pscalee(rsa,ssa,ome1,npt,kic,nic)
    call pscalee(ssa,ssa,smu,npt,kic,nic)
    call pscalee(ssa,tsa,snu,npt,kic,nic)
    call pscalee(tsa,tsa,tau,npt,kic,nic)
    !===============================================================================
    call pscalee(rsa,tsa,ome2,npt,kic,nic)
#endif 

    tau = tau - snu*snu/smu
    ome2 = (ome2-(snu+prec)*(ome1+prec)/(smu+prec))/(tau+prec)
    ome1 = (ome1-snu*ome2) / smu

    sol = sol + ome1*rsa + ome2*ssa + alp*usa
    rsa = rsa - ome1*ssa - ome2*tsa

    usa = usa - ome1*vsa - ome2*wsa

    return
  end subroutine bstak2e

  !*******************************************************************************
  subroutine bicgstak2e (coef,jcof,icof,rhs,sol,eps,res,ngc,kt,ndd,npt,nps,kic,nic,pcase)
    !*******************************************************************************
    use mod_Parameters, only: ires
    use mod_mpi
    !-------------------------------------------------------------------------------
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    integer                            :: ngc,nps,npt,ndd,kt,nic
    integer                            :: pcase
    integer, dimension(npt+1)          :: icof
    integer, dimension(nps)            :: jcof
    integer, dimension(npt)            :: kic
    real(8), dimension(nps)            :: coef
    real(8), dimension(npt)            :: sol,rhs
    real(8)                            :: eps,res,res0
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                            :: l,n
    real(8), dimension(:), allocatable :: alu
    real(8), dimension(npt)            :: psa,qsa,rsa,soltemp,ssa,tsa,usa,vsa,wsa,ysa
    real(8), dimension(npt)            :: tmp,rhs0
    real(8)                            :: alp,bl2,ome2,restemp,rho0,rl2
    real(8)                            :: bl20,rl20
    !-------------------------------------------------------------------------------
    
    !-------------------------------------------------------------------------------
    ! Factorization
    !-------------------------------------------------------------------------------
    allocate (alu(nps))
    alu = 0
    call factor(pcase,coef,jcof,icof,alu,npt,nps,ndd,kic,nic)
    !-------------------------------------------------------------------------------
    
    !-------------------------------------------------------------------------------
    ! Printing alu
    !-------------------------------------------------------------------------------
    ! rewind(16)
    ! write(16,616) (alu(l),l=1,nps)
    ! 616 format(10d15.6)
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! initial residual values
    !-------------------------------------------------------------------------------
    rhs0=rhs
    call pscalee(rhs,rhs,bl20,npt,kic,nic) !
    bl20 = sqrt(bl20+1.d-40)
    !-------------------------------------------------------------------------------
    call matveke(npt,sol,psa,coef,jcof,icof,nps)
    psa=psa-rhs
    call pscalee(psa,psa,rl20,npt,kic,nic)
    rl20 = sqrt(rl20+1.d-40)
    !-------------------------------------------------------------------------------

    
    !-------------------------------------------------------------------------------
    ! Initialization
    !-------------------------------------------------------------------------------
    psa = 0
    qsa = 0
    rsa = 0
    ssa = 0
    tsa = 0
    usa = 0
    vsa = 0
    wsa = 0
    ysa = 0
    qsa = rhs
    !-------------------------------------------------------------------------------
    
    !-------------------------------------------------------------------------------
    ! preconditionning
    !-------------------------------------------------------------------------------
    call precond(pcase,npt,rhs,qsa,alu,jcof,icof,ndd,nps,kic,nic)   ! call precon (in thetis version)
    !-------------------------------------------------------------------------------

    call pscalee(rhs,rhs,bl2,npt,kic,nic) !
    bl2 = sqrt(bl2+1.d-40)

    call matveke(npt,sol,psa,coef,jcof,icof,nps)
    call precond(pcase,npt,tsa,psa,alu,jcof,icof,ndd,nps,kic,nic)

    rsa = rhs - tsa
    qsa = rsa
    psa = rsa

    call pscalee(rsa,rsa,rl2,npt,kic,nic)
    rl2 = sqrt(rl2+1.d-40)

    if (bl2 <= 1.d-80) then
       write (6,*) 'error in CG. : second member is zero', bl2
       stop
    endif
    if (rl2 <= 1.d-80) then
       write (6,*) 'error in CG : residual is zero', rl2
       stop
    endif
    !-------------------------------------------------------------------------------

    restemp = 1.d20
    !-------------------------------------------------------------------------------
    !bicgstab(2)
    !-------------------------------------------------------------------------------
    rho0 = 1
    alp = 0
    ome2 = 1
    res=rl20/bl20
    !-------------------------------------------------------------------------------
    if (ires>0) then 
       if (rank==0) then
          write(*,*)
          write(*,101) "-----", "--------------------", &
               & "--------------------","--------------------"
          write(*,*) "  resol.sys.",mpi_ieq_solve
          write(*,*) "      |b|_2=",bl20
          write(*,*) " |A.x0-b|_2=",rl20
!!$          write(*,*) "     |Pb|_2=",bl2
!!$          write(*,*) "|PA.x-Pb|_2=",rl2
          write(*,101) "Iters", "|A.x-b|_2", "conv.rate", "|A.x-b|_2/|b|_2"
          write(*,101) "-----", "--------------------", &
               & "--------------------","--------------------"
       end if
    end if
    
    do kt = 1,ngc
       !-------------------------------------------------------------------------------
       call bstak2e(coef,jcof,sol,icof,alu,psa,qsa,rsa,ssa,tsa,usa,vsa, &
            wsa,rho0,alp,ome2,ndd,npt,nps,kic,nic,pcase)
       !-------------------------------------------------------------------------------
       
       rl20=res
       tmp=0
       call matveke(npt,sol,tmp,coef,jcof,icof,nps)
       tmp=tmp-rhs0
       call pscalee(tmp,tmp,res,npt,kic,nic)
       
!!$       call pscalee(rsa,rsa,res,npt,kic,nic)
       !-------------------------------------------------------------------------------
       ! residual
       !-------------------------------------------------------------------------------
       if (res<1.d20 .and. res>1.d-32) then
          res = sqrt(res+1.d-40)
       else
          if (res>=1d20) then
             res = 1d10
          else
             res = 1d-16
          end if
       endif
       !-------------------------------------------------------------------------------
       
       !-------------------------------------------------------------------------------
       ! normalisation with sec_membre
       !-------------------------------------------------------------------------------
       res=res/bl20
       !-------------------------------------------------------------------------------
       
       !-------------------------------------------------------------------------------
       ! save best solution
       !-------------------------------------------------------------------------------
       if (res < restemp) then
          restemp = res
          soltemp = sol
       endif
!!$       !-------------------------------------------------------------------------------
!!$       ! normalized residuals
!!$       !-------------------------------------------------------------------------------
!!$       if (kt<=5) then
!!$          res0=max(res0,res)
!!$       end if
!!$       !-------------------------------------------------------------------------------
       if (ires>0.and.modulo(kt,ires)==0) then
          if (rank==0) then
             write(*,102) kt,res*bl20,res/rl20,res
          end if
       end if
       !-------------------------------------------------------------------------------
       if (res <= eps) exit
       !-------------------------------------------------------------------------------
       if (kt>30.and.res>1) exit
       !-------------------------------------------------------------------------------
    enddo
    !-------------------------------------------------------------------------------
    kt=min(kt,ngc)
    !-------------------------------------------------------------------------------
    sol = soltemp
    res = restemp
    rhs = rsa
    !-------------------------------------------------------------------------------
    
    if (ires>0) then
       if (rank==0) then
          write(*,101) "-----", "--------------------", &
               & "--------------------","--------------------"
          write(*,*)
       end if
    end if

    !-------------------------------------------------------------------------------
    deallocate (alu)

    !-------------------------------------------------------------------------------

    return
    !*******************************************************************************

101 format(a5,1x,3(a20,1x))
102 format(i5,1x,9(1pe20.6,1x))
    
  end subroutine bicgstak2e
  !*******************************************************************************

  !*******************************************************************************
  subroutine solver_BiCGStab2_iLU(ieq,coef,jcof,icof,sol,rhs,kic,nic,dt,res,kt,npt,nps,ndd,ldim,&
       & solver_it,solver_threshold,solver_precond)
    !*******************************************************************************
    !-------------------------------------------------------------------------------
    use mod_Parameters, only: nproc,               &
         & nb_Vx,nb_Vy,ex,sx,ey,sy,ez,sz,gx,gy,gz, &
         & sxu,exu,syu,eyu,szu,ezu,                &
         & sxv,exv,syv,eyv,szv,ezv,                &
         & sxw,exw,syw,eyw,szw,ezw,                &
         & sxs,exs,sys,eys,szs,ezs,                &
         & sxus,exus,syus,eyus,szus,ezus,          &
         & sxvs,exvs,syvs,eyvs,szvs,ezvs,          &
         & sxws,exws,syws,eyws,szws,ezws
    use mod_solver_comm_mpi
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    integer                                :: solver_it,solver_precond,ieq
    real(8)                                :: solver_threshold
    integer                                :: nps,kt,npt,ndd,ldim,nic
    integer, dimension(npt+1)              :: icof
    integer, dimension(nps)                :: jcof
    integer, dimension(npt)                :: kic
    real(8), dimension(nps)                :: coef
    real(8), dimension(npt)                :: sol,rhs
    real(8)                                :: dt,res
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                :: i,j,l,k
    integer                                :: ii,jj,kk
    real(8)                                :: diag
    real(8), allocatable, dimension(:,:,:) :: sca,u,v,w
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! solved equation (0:nvstks,1:energy)
    !-------------------------------------------------------------------------------
    mpi_ieq_solve=ieq
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Init sol
    !-------------------------------------------------------------------------------
!!$    do l=1,npt
!!$       sol(l)=1.d-28*sin(dble(l)/20)
!!$    end do
!!$    call random_number(sol)
!!$    sol=1d-28*sol
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Initial conditionning with jacobi matix
    !-------------------------------------------------------------------------------
    do l = 1,npt
       diag = coef(icof(l))
       do k = icof(l)+1,icof(l+1)-1
          coef(k) = coef(k) / diag
       enddo
       rhs(l) = rhs(l) / diag
    enddo
    do l = 1,npt
       coef(icof(l)) = 1
    enddo
    do l = 1,npt
       if (abs(coef(l)) < 1.d-35) then
          coef(l) = 0
       endif
    enddo
    do l = 1,npt
       if (abs(rhs(l)) < 1.d-35) then
          rhs(l) = 0
       endif
    enddo
    
    call bicgstak2e(coef,jcof,icof,rhs,sol,solver_threshold,res,solver_it,kt, &
         ndd,npt,nps,kic,nic,solver_precond)

    if (nproc>1) call solver_comm_mpi(sol,npt,mpi_ieq_solve)
    
    !-------------------------------------------------------------------------------
  end subroutine solver_BiCGStab2_iLU
  !*******************************************************************************


#if MUMPS 
  !*******************************************************************************
  subroutine MUMPS_solver(mumps_par,a,ja,ia,sol,rhs,kic,nic,res,npt,nps,mumps_mem,mumps_part)
    !*******************************************************************************
    use mod_struct_solver
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    integer                   :: npt,nps,mumps_mem,mumps_part,nic
    real(8)                   :: res
    real(8), dimension(nps)   :: a
    real(8), dimension(npt)   :: sol,rhs
    integer, dimension(nps)   :: ja
    integer, dimension(npt+1) :: ia
    integer, dimension(npt)   :: kic
    type(DMUMPS_STRUC)        :: mumps_par
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                   :: l,k,ns,en,tr,ke,v2,la,kl,pv,ps,po,he
    real(8)                   :: diag
    real(8), dimension(npt)   :: rsa,ysa
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Initial preconditionning of the matrix by Jacobi
    !-------------------------------------------------------------------------------
    do l=1,npt
       diag=a(ia(l))
       do k=ia(l)+1,ia(l+1)-1
          a(k)=a(k)/diag
       enddo
       rhs(l) = rhs(l)/diag
    enddo

    do l=1,npt
       a(ia(l))=1
    enddo

    do l=1,nps
       if (abs(a(l))<1.d-35) then
          a(l) = 0
       endif
    enddo

    do l=1,npt
       if (abs(rhs(l))<1.d-35) then
          rhs(l) = 0
       endif
    enddo
    !-------------------------------------------------------------------------------

    call resolmumps(mumps_par,a,ja,ia,sol,rhs,npt,nps,mumps_mem,mumps_part)

    !-------------------------------------------------------------------------------
    ! calcul du residu
    !-------------------------------------------------------------------------------
    call matveke (npt,sol,ysa,a,ja,ia,nps)
    rsa = ysa - rhs
    call pscalee(rsa,rsa,res,npt,kic,nic)
    res = sqrt(res+1.d-40)
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
  end subroutine MUMPS_solver
  !*******************************************************************************

  !*******************************************************************************
  subroutine resolmumps(mumps_par,coefp,jcofp,icofp,sol,rhs,npt,nps,mumps_mem,mumps_part)
    !*******************************************************************************
    use mod_mpi
    use mod_struct_solver
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables 
    !-------------------------------------------------------------------------------
    type (DMUMPS_STRUC),intent(inout) :: mumps_par
    integer                           :: npt,nps,mumps_mem,mumps_part
    real(8), dimension(nps)           :: coefp
    real(8), dimension(npt)           :: sol,rhs
    integer, dimension(nps)           :: jcofp
    integer, dimension(npt+1)         :: icofp
    !-------------------------------------------------------------------------------
    ! Local variables 
    !-------------------------------------------------------------------------------
    logical                           :: lyse
    integer                           :: i,j,ierr
    real(8)                           :: moy
    real(8)                           :: tdeb1,tdeb2,tdeb3,tfin1,tfin2,tfin3
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    lyse   = .false.
    if (.not.associated(mumps_par%irn)) then
       lyse  = .true.
    endif
    !-------------------------------------------------------------------------------
    mumps_par%comm = mpi_comm_world  ! MPI comm3d
    !-------------------------------------------------------------------------------
    if (lyse) then
       mumps_par%job = -1
       mumps_par%sym = 0
       mumps_par%par = 1
       call dmumps(mumps_par)
    endif

    if ( mumps_par%myid .eq. 0 ) then
       mumps_par%n=npt !! MPI NOMBRE INCONNUS GLOBAL
       mumps_par%nz=nps
       if (lyse) then
          allocate( mumps_par%irn ( mumps_par%nz ) )
          allocate( mumps_par%jcn ( mumps_par%nz ) )
          allocate( mumps_par%a( mumps_par%nz ) )
          allocate( mumps_par%rhs ( mumps_par%n  ) )
       endif

       do i=1,npt
          do j=icofp(i),icofp(i+1)-1
             mumps_par%irn(j)=i
             mumps_par%jcn(j)=jcofp(j)
             mumps_par%a(j)  =coefp(j)
          enddo
       enddo
       do i=1,npt
          mumps_par%rhs(i)=rhs(i)
       enddo
    else
       print*,'error mumps',mumps_par%myid,ierr,mpi_comm_world
    endif

    mumps_par%icntl(7)=mumps_part
    mumps_par%icntl(3)=-1
    
    !-------------------------------------------------------------------------------
    ! Analyze
    !-------------------------------------------------------------------------------
    mumps_par%icntl(14)=mumps_mem
    if (lyse) then
       mumps_par%job = 1      
       !call cpu_time(tdeb1)
       call dmumps(mumps_par)
       !call cpu_time(tfin1)
    endif
    !-------------------------------------------------------------------------------


    !-------------------------------------------------------------------------------
    ! Factorization
    !-------------------------------------------------------------------------------
    mumps_par%job = 2   
    !call cpu_time(tdeb2)
    call dmumps(mumps_par)
    !call cpu_time(tfin2)
    !-------------------------------------------------------------------------------


    !-------------------------------------------------------------------------------
    ! Solving
    !-------------------------------------------------------------------------------
    mumps_par%job = 3      
    !call cpu_time(tdeb3)
    call dmumps(mumps_par)
    !call cpu_time(tfin3)
    !-------------------------------------------------------------------------------
    sol=mumps_par%rhs
    ! res=mumps_par%rinfog(6)
    !-------------------------------------------------------------------------------
    !End of mumps
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
  end subroutine resolmumps
  !*******************************************************************************
#endif

end module mod_solver
