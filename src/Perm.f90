!========================================================================
!
!                   FUGU Version 1.0.0
!
!========================================================================
!
! Copyright  St�phane Vincent
!
! stephane.vincent@u-pem.fr          straightparadigm@gmail.com
!
! This file is part of the FUGU Fortran library, a set of Computational 
! Fluid Dynamics subroutines devoted to the simulation of multi-scale
! multi-phase flows for resolved scale interfaces (liquid/gas/solid). 
! FUGU uses the initial developments of DyJeAT and SHALA codes from 
! Jean-Luc Estivalezes (jean-luc.estivalezes@onera.fr) and 
! Fr�d�ric Couderc (couderc@math.univ-toulouse.fr).
!
!========================================================================
!**
!**   NAME        : Perm.f90
!**
!**   AUTHOR      : St�phane Vincent
!**                 Georges Halim Atallah
!**
!**   FUNCTION    : Perm interpolation routines for velocity fields in 2D
!**                 and 3D
!**
!**   DATES       : Version 1.0.0  : from : May, 2018
!**
!**   SUBROUTINES : - subroutine Init_Perm3D
!**                 - subroutine Perm3D
!**                 - subroutine Perm2D
!========================================================================

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
MODULE mod_Perm
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  use mod_Parameters, only: regular_mesh,mesh

  !****************************************************************************************
  ! Global variables for Perm_3D
  !****************************************************************************************
  real(8), dimension(12,8) :: Bplus=0
  real(8), dimension(8,12) :: Aperm=0,Bperm=0
  !****************************************************************************************

contains

  !****************************************************************************************
  !****************************************************************************************
  !                            3D P.E.R.M. INTERPOLATION METHOD                           !
  !****************************************************************************************
  !****************************************************************************************

  subroutine Init_Perm3D
    implicit none
    !-------------------------------------------------------------------------
    ! global variable
    !-------------------------------------------------------------------------
    ! local variable
    !-------------------------------------------------------------------------
    integer :: irow,q,r,s
    !-------------------------------------------------------------------------

    irow=0
    do q=0,1
       do r=0,1
          do s=0,1
             irow=irow+1
             Aperm(irow,1)=(1-s)*(1-r)
             Aperm(irow,2)=(1-s)*r
             Aperm(irow,3)=s*(1-r)
             Aperm(irow,4)=s*r
             Aperm(irow,5)=(1-s)*(1-q)
             Aperm(irow,6)=(1-s)*q
             Aperm(irow,7)=s*(1-q)
             Aperm(irow,8)=s*q
             Aperm(irow,9)=(1-q)*(1-r)
             Aperm(irow,10)=(1-q)*r
             Aperm(irow,11)=q*(1-r)
             Aperm(irow,12)=q*r
          end do
       end do
    end do

    irow=0
    do q=0,1
       do r=0,1
          do s=0,1
             irow=irow+1
             Bperm(irow,1)=(1-s)*(1-r)*(q-0.5d0)
             Bperm(irow,2)=(1-s)*r*(q-0.5d0)
             Bperm(irow,3)=s*(1-r)*(q-0.5d0)
             Bperm(irow,4)=s*r*(q-0.5d0)
             Bperm(irow,5)=(1-s)*(1-q)*(r-0.5d0)
             Bperm(irow,6)=(1-s)*q*(r-0.5d0)
             Bperm(irow,7)=s*(1-q)*(r-0.5d0)
             Bperm(irow,8)=s*q*(r-0.5d0)
             Bperm(irow,9)=(1-q)*(1-r)*(s-0.5d0)
             Bperm(irow,10)=(1-q)*r*(s-0.5d0)
             Bperm(irow,11)=q*(1-r)*(s-0.5d0)
             Bperm(irow,12)=q*r*(s-0.5d0)
          end do
       end do
    end do
    
    ! B+ (pseudo inverse of matrix B) matrix components calculated with Maple
    Bplus(1,1)=-0.7D1/0.48D2  ; Bplus(1,2)=-0.1D1/0.24D2  ; Bplus(1,3)=-0.1D1/0.24D2  ; Bplus(1,4)=-0.1D1/0.48D2
    Bplus(1,5)=0.7D1/0.48D2   ; Bplus(1,6)=0.1D1/0.24D2   ; Bplus(1,7)=0.1D1/0.24D2   ; Bplus(1,8)=0.1D1/0.48D2
    Bplus(2,1)=-0.1D1/0.24D2  ; Bplus(2,2)=-0.1D1/0.48D2  ; Bplus(2,3)=-0.7D1/0.48D2  ; Bplus(2,4)=-0.1D1/0.24D2
    Bplus(2,5)=0.1D1/0.24D2   ; Bplus(2,6)=0.1D1/0.48D2   ; Bplus(2,7)=0.7D1/0.48D2   ; Bplus(2,8)=0.1D1/0.24D2
    Bplus(3,1)=-0.1D1/0.24D2  ; Bplus(3,2)=-0.7D1/0.48D2  ; Bplus(3,3)=-0.1D1/0.48D2  ; Bplus(3,4)=-0.1D1/0.24D2
    Bplus(3,5)=0.1D1/0.24D2   ; Bplus(3,6)=0.7D1/0.48D2   ; Bplus(3,7)=0.1D1/0.48D2   ; Bplus(3,8)=0.1D1/0.24D2
    Bplus(4,1)=-0.1D1/0.48D2  ; Bplus(4,2)=-0.1D1/0.24D2  ; Bplus(4,3)=-0.1D1/0.24D2  ; Bplus(4,4)=-0.7D1/0.48D2
    Bplus(4,5)=0.1D1/0.48D2   ; Bplus(4,6)=0.1D1/0.24D2   ; Bplus(4,7)=0.1D1/0.24D2   ; Bplus(4,8)=0.7D1/0.48D2
    Bplus(5,1)=-0.7D1/0.48D2  ; Bplus(5,2)=-0.1D1/0.24D2  ; Bplus(5,3)=0.7D1/0.48D2   ; Bplus(5,4)=0.1D1/0.24D2
    Bplus(5,5)=-0.1D1/0.24D2  ; Bplus(5,6)=-0.1D1/0.48D2  ; Bplus(5,7)=0.1D1/0.24D2   ; Bplus(5,8)=0.1D1/0.48D2
    Bplus(6,1)=-0.1D1/0.24D2  ; Bplus(6,2)=-0.1D1/0.48D2  ; Bplus(6,3)=0.1D1/0.24D2   ; Bplus(6,4)=0.1D1/0.48D2
    Bplus(6,5)=-0.7D1/0.48D2  ; Bplus(6,6)=-0.1D1/0.24D2  ; Bplus(6,7)=0.7D1/0.48D2   ; Bplus(6,8)=0.1D1/0.24D2
    Bplus(7,1)=-0.1D1/0.24D2  ; Bplus(7,2)=-0.7D1/0.48D2  ; Bplus(7,3)=0.1D1/0.24D2   ; Bplus(7,4)=0.7D1/0.48D2
    Bplus(7,5)=-0.1D1/0.48D2  ; Bplus(7,6)=-0.1D1/0.24D2  ; Bplus(7,7)=0.1D1/0.48D2   ; Bplus(7,8)=0.1D1/0.24D2
    Bplus(8,1)=-0.1D1/0.48D2  ; Bplus(8,2)=-0.1D1/0.24D2  ; Bplus(8,3)=0.1D1/0.48D2   ; Bplus(8,4)=0.1D1/0.24D2
    Bplus(8,5)=-0.1D1/0.24D2  ; Bplus(8,6)=-0.7D1/0.48D2  ; Bplus(8,7)=0.1D1/0.24D2   ; Bplus(8,8)=0.7D1/0.48D2
    Bplus(9,1)=-0.7D1/0.48D2  ; Bplus(9,2)=0.7D1/0.48D2   ; Bplus(9,3)=-0.1D1/0.24D2  ; Bplus(9,4)=0.1D1/0.24D2
    Bplus(9,5)=-0.1D1/0.24D2  ; Bplus(9,6)=0.1D1/0.24D2   ; Bplus(9,7)=-0.1D1/0.48D2  ; Bplus(9,8)=0.1D1/0.48D2
    Bplus(10,1)=-0.1D1/0.24D2 ; Bplus(10,2)=0.1D1/0.24D2  ; Bplus(10,3)=-0.7D1/0.48D2 ; Bplus(10,4)=0.7D1/0.48D2
    Bplus(10,5)=-0.1D1/0.48D2 ; Bplus(10,6)=0.1D1/0.48D2  ; Bplus(10,7)=-0.1D1/0.24D2 ; Bplus(10,8)=0.1D1/0.24D2
    Bplus(11,1)=-0.1D1/0.24D2 ; Bplus(11,2)=0.1D1/0.24D2  ; Bplus(11,3)=-0.1D1/0.48D2 ; Bplus(11,4)=0.1D1/0.48D2
    Bplus(11,5)=-0.7D1/0.48D2 ; Bplus(11,6)=0.7D1/0.48D2  ; Bplus(11,7)=-0.1D1/0.24D2 ; Bplus(11,8)=0.1D1/0.24D2
    Bplus(12,1)=-0.1D1/0.48D2 ; Bplus(12,2)=0.1D1/0.48D2  ; Bplus(12,3)=-0.1D1/0.24D2 ; Bplus(12,4)=0.1D1/0.24D2
    Bplus(12,5)=-0.1D1/0.24D2 ; Bplus(12,6)=0.1D1/0.24D2  ; Bplus(12,7)=-0.7D1/0.48D2 ; Bplus(12,8)=0.7D1/0.48D2

    ! factor 4 missing LDKB 15/10/2019
    Bplus=4*Bplus

  end subroutine Init_Perm3D

  subroutine Perm3D(u,v,w,xyz,vp)
    implicit none
    !***************************************************************************************
    !Global variables
    !***************************************************************************************
    real(8), dimension(:), intent(in)                  :: xyz
    real(8), allocatable, dimension(:,:,:), intent(in) :: u,v,w
    real(8), dimension(:), intent(inout)               :: vp
    !***************************************************************************************
    !Local variables
    !***************************************************************************************
    integer                                            :: i,j,k,num,q,rr,s
    real(8), save                                      :: H,aa,bb
    real(8)                                            :: DIV1,DIV2,DIV3,DIV4,DIV5,DIV6,DIV7,DIV8,DIV9
    real(8)                                            :: DIV10,DIV11,DIV12,DIV13,DIV14,DIV15,DIV16,DIV17,DIV18,DIV19
    real(8)                                            :: DIV20,DIV21,DIV22,DIV23,DIV24,DIV25,DIV26,DIV27
    real(8)                                            :: DIVEX1,DIVEX2,DIVEX3,DIVEX4,DIVEX5,DIVEX6,DIVEX7,DIVEX8
    real(8)                                            :: DUNB,DUNBCHAP,DUNT,DUNTCHAP,DUSB,DUSBCHAP,DUST
    real(8)                                            :: DUSTCHAP,DVEB,DVEBCHAP,DVET,DVETCHAP,DVWB,DVWBCHAP
    real(8)                                            :: DVWT,DVWTCHAP,DWNE,DWNECHAP,DWNW,DWNWCHAP,DWSE,DWSECHAP,DWSW,DWSWCHAP
    real(8)                                            :: VITTEXU1,VITTEXU2,VITTEXU3,VITTEXU4,VITTEXU5
    real(8)                                            :: VITTEXU6,VITTEXU7,VITTEXU8,VITTEXU9,VITTEXU10
    real(8)                                            :: VITTEXU11,VITTEXU12,VITTEXU13,VITTEXU14,VITTEXU15,VITTEXU16
    real(8)                                            :: VITTEXV1,VITTEXV2,VITTEXV3,VITTEXV4,VITTEXV5
    real(8)                                            :: VITTEXV6,VITTEXV7,VITTEXV8,VITTEXV9,VITTEXV10,VITTEXV11,VITTEXV12,VITTEXV13
    real(8)                                            :: VITTEXV14,VITTEXV15,VITTEXV16,VITTEXV17,VITTEXV18,VITTEXV19,VITTEXV20
    real(8)                                            :: VITTEXV21,VITTEXV22,VITTEXV23,VITTEXV24
    real(8)                                            :: VITTEXW1,VITTEXW2,VITTEXW25,VITTEXW26,VITTEXW27
    real(8)                                            :: VITTEXW28,VITTEXW29,VITTEXW3,VITTEXW30,VITTEXW31
    real(8)                                            :: VITTEXW32,VITTEXW4,VITTEXW5,VITTEXW6,VITTEXW7,VITTEXW8
    real(8)                                            :: VITTUE,VITTUW,VITTVS,VITTVN,VITEXU1,VITEXU2,VITEXU3,VITEXU4
    real(8)                                            :: VITEXU5,VITEXU6,VITEXU7,VITEXU8,VITEXV1,VITEXV2,VITEXV3,VITEXV4
    real(8)                                            :: VITEXV5,VITEXV6,VITEXV7,VITEXV8,VITEXW1,VITEXW2,VITEXW3,VITEXW4
    real(8)                                            :: VITEXW5,VITEXW6,VITEXW7,VITEXW8,VITUS,VITUN,VITVE,VITVW
    real(8)                                            :: VITTWB,VITTWT,VITUBN,VITUBS,VITUTN,VITUTS,VITVBE
    real(8)                                            :: VITVBW,VITVTE,VITVTW,VITWNE,VITWNW,VITWSE,VITWSW
    real(8), dimension(3)                              :: R
    real(8), dimension(8)                              :: Theta
    real(8), dimension(12)                             :: Delta1,Deltah,Delta2,delta
    logical                                            :: Init=.true.
    !***************************************************************************************
    
    if (regular_mesh) then 
       !--------------------------------------------------------------------------------------------
       ! Initialization 
       !--------------------------------------------------------------------------------------------
       if (Init) then
          call Init_Perm3D
          H=mesh%dx(mesh%sx)
          aa=mesh%dx(mesh%sx)/mesh%dy(mesh%sy)
          bb=mesh%dx(mesh%sx)/mesh%dz(mesh%sz)
          Init=.false.
       end if
       !--------------------------------------------------------------------------------------------

       !---------------------------velocity interpolation-------------------------------------- 
       i=(xyz(1)-(mesh%xmin-mesh%dx(mesh%sx)/2))/mesh%dx(mesh%sx)
       j=(xyz(2)-(mesh%ymin-mesh%dy(mesh%sy)/2))/mesh%dy(mesh%sy)
       k=(xyz(3)-(mesh%zmin-mesh%dz(mesh%sz)/2))/mesh%dz(mesh%sz)
       !-----------------------------local coordinates-----------------------------------------
       R(1)=(xyz(1)-(mesh%xmin+i*mesh%dx(mesh%sx)))/(aa*mesh%dy(mesh%sy))+0.5d0
       R(2)=(xyz(2)-(mesh%ymin+j*mesh%dy(mesh%sy)))/mesh%dy(mesh%sy)+0.5d0
       R(3)=(xyz(3)-(mesh%zmin+k*mesh%dz(mesh%sz)))/((aa/bb)*mesh%dy(mesh%sy))+0.5d0

       !---------------------------Divergence discretization ----------------------------------
       ! equation (23)
       DIV1=(u(i+1,j,k)-u(i,j,k)+v(i,j+1,k)-v(i,j,k)+w(i,j,k+1)-w(i,j,k))/H                                ! (i,j,k)
       DIV2=(u(i,j,k)-u(i-1,j,k)+v(i-1,j+1,k)-v(i-1,j,k)+w(i-1,j,k+1)-w(i-1,j,k))/H                        ! (i-1,j,k)
       DIV3=(u(i+1,j-1,k)-u(i,j-1,k)+v(i,j,k)-v(i,j-1,k)+w(i,j-1,k+1)-w(i,j-1,k))/H                        ! (i,j-1,k)
       DIV4=(u(i,j-1,k)-u(i-1,j-1,k)+v(i-1,j,k)-v(i-1,j-1,k)+w(i-1,j-1,k+1)-w(i-1,j-1,k))/H                ! (i-1,j-1,k)
       DIV5=(u(i+2,j,k)-u(i+1,j,k)+v(i+1,j+1,k)-v(i+1,j,k)+w(i+1,j,k+1)-w(i+1,j,k))/H                      ! (i+1,j,k)
       DIV6=(u(i+2,j-1,k)-u(i+1,j-1,k)+v(i+1,j,k)-v(i+1,j-1,k)+w(i+1,j-1,k+1)-w(i+1,j-1,k))/H              ! (i+1,j-1,k)
       DIV7=(u(i+1,j+1,k)-u(i,j+1,k)+v(i,j+2,k)-v(i,j+1,k)+w(i,j+1,k+1)-w(i,j+1,k))/H                      ! (i,j+1,k)
       DIV8=(u(i,j+1,k)-u(i-1,j+1,k)+v(i-1,j+2,k)-v(i-1,j+1,k)+w(i-1,j+1,k+1)-w(i-1,j+1,k))/H              ! (i-1,j+1,k)
       DIV9=(u(i+2,j+1,k)-u(i+1,j+1,k)+v(i+1,j+2,k)-v(i+1,j+1,k)+w(i+1,j+1,k+1)-w(i+1,j+1,k))/H            ! (i+1,j+1,k)
       DIV10=(u(i+1,j,k-1)-u(i,j,k-1)+v(i,j+1,k-1)-v(i,j,k-1)+w(i,j,k)-w(i,j,k-1))/H                       ! (i,j,k-1)
       DIV11=(u(i,j,k-1)-u(i-1,j,k-1)+v(i-1,j+1,k-1)-v(i-1,j,k-1)+w(i-1,j,k)-w(i-1,j,k-1))/H               ! (i-1,j,k-1)
       DIV12=(u(i+1,j-1,k-1)-u(i,j-1,k-1)+v(i,j,k-1)-v(i,j-1,k-1)+w(i,j-1,k)-w(i,j-1,k-1))/H               ! (i,j-1,k-1)
       DIV13=(u(i,j-1,k-1)-u(i-1,j-1,k-1)+v(i-1,j,k-1)-v(i-1,j-1,k-1)+w(i-1,j-1,k)-w(i-1,j-1,k-1))/H       ! (i-1,j-1,k-1)
       DIV14=(u(i+2,j,k-1)-u(i+1,j,k-1)+v(i+1,j+1,k-1)-v(i+1,j,k-1)+w(i+1,j,k)-w(i+1,j,k-1))/H             ! (i+1,j,k-1)
       DIV15=(u(i+2,j-1,k-1)-u(i+1,j-1,k-1)+v(i+1,j,k-1)-v(i+1,j-1,k-1)+w(i+1,j-1,k)-w(i+1,j-1,k-1))/H     ! (i+1,j-1,k-1)
       DIV16=(u(i+1,j+1,k-1)-u(i,j+1,k-1)+v(i,j+2,k-1)-v(i,j+1,k-1)+w(i,j+1,k)-w(i,j+1,k-1))/H             ! (i,j+1,k-1)
       DIV17=(u(i,j+1,k-1)-u(i-1,j+1,k-1)+v(i-1,j+2,k-1)-v(i-1,j+1,k-1)+w(i-1,j+1,k)-w(i-1,j+1,k-1))/H     ! (i-1,j+1,k-1)
       DIV18=(u(i+2,j+1,k-1)-u(i+1,j+1,k-1)+v(i+1,j+2,k-1)-v(i+1,j+1,k-1)+w(i+1,j+1,k)-w(i+1,j+1,k-1))/H   ! (i+1,j+1,k-1)
       DIV19=(u(i+1,j,k+1)-u(i,j,k+1)+v(i,j+1,k+1)-v(i,j,k+1)+w(i,j,k+2)-w(i,j,k+1))/H                     ! (i,j,k+1)
       DIV20=(u(i,j,k+1)-u(i-1,j,k+1)+v(i-1,j+1,+k+1)-v(i-1,j,k+1)+w(i-1,j,k+2)-w(i-1,j,k+1))/H            ! (i-1,j,k+1)
       DIV21=(u(i+1,j-1,k+1)-u(i,j-1,k+1)+v(i,j,k+1)-v(i,j-1,k+1)+w(i,j-1,k+2)-w(i,j-1,k+1))/H             ! (i,j-1,k+1)
       DIV22=(u(i,j-1,k+1)-u(i-1,j-1,k+1)+v(i-1,j,k+1)-v(i-1,j-1,k+1)+w(i-1,j-1,k+2)-w(i-1,j-1,k+1))/H     ! (i-1,j-1,k+1)
       DIV23=(u(i+2,j,k+1)-u(i+1,j,k+1)+v(i+1,j+1,k+1)-v(i+1,j,k+1)+w(i+1,j,k+2)-w(i+1,j,k+1))/H           ! (i+1,j,k+1)
       DIV24=(u(i+2,j-1,k+1)-u(i+1,j-1,k+1)+v(i+1,j,k+1)-v(i+1,j-1,k+1)+w(i+1,j-1,k+2)-w(i+1,j-1,k+1))/H   ! (i+1,j-1,k+1)
       DIV25=(u(i+1,j+1,k+1)-u(i,j+1,k+1)+v(i,j+2,k+1)-v(i,j+1,k+1)+w(i,j+1,k+2)-w(i,j+1,k+1))/H           ! (i,j+1,k+1)
       DIV26=(u(i,j+1,k+1)-u(i-1,j+1,k+1)+v(i-1,j+2,k+1)-v(i-1,j+1,k+1)+w(i-1,j+1,k+2)-w(i-1,j+1,k+1))/H   ! (i-1,j+1,k+1)
       DIV27=(u(i+2,j+1,k+1)-u(i+1,j+1,k+1)+v(i+1,j+2,k+1)-v(i+1,j+1,k+1)+w(i+1,j+1,k+2)-w(i+1,j+1,k+1))/H ! (i+1,j+1,k+1)

       !---------------------------Divergence interpolation at nodes---------------------------
       !***************************************
       ! On d�termine les divergences dans les mailles cubiques en fonction de leurs 8 noeuds
       !! Interpolation of divergence at nodes 
       ! equation (25)

       ! Selon l'ordre de l'article de Pope, on doit avoir :
       DIVEX1=H*0.125_8*(DIV1+DIV2+DIV3+DIV4+DIV10+DIV11+DIV12+DIV13) ! doit avoir (i,j,k,i-1,j-1,k-1)
       DIVEX2=H*0.125_8*(DIV1+DIV2+DIV3+DIV4+DIV19+DIV20+DIV21+DIV22) ! doit avoir (i,j,k,i-1,j-1,k+1)
       DIVEX3=H*0.125_8*(DIV1+DIV2+DIV7+DIV8+DIV10+DIV11+DIV16+DIV17) ! doit avoir (i,j,k,i-1,j+1,k-1)
       DIVEX4=H*0.125_8*(DIV1+DIV2+DIV7+DIV8+DIV19+DIV20+DIV25+DIV26) ! doit avoir (i,j,k,i-1,j+1,k+1)
       DIVEX5=H*0.125_8*(DIV1+DIV3+DIV5+DIV6+DIV10+DIV12+DIV14+DIV15) ! doit avoir (i,j,k,i+1,j-1,k-1)
       DIVEX6=H*0.125_8*(DIV1+DIV3+DIV5+DIV6+DIV19+DIV21+DIV23+DIV24) ! doit avoir (i,j,k,i+1,j-1,k+1)
       DIVEX7=H*0.125_8*(DIV1+DIV5+DIV7+DIV9+DIV10+DIV14+DIV16+DIV18) ! doit avoir (i,j,k,i+1,j+1,k-1)
       DIVEX8=H*0.125_8*(DIV1+DIV5+DIV7+DIV9+DIV19+DIV23+DIV25+DIV27) ! doit avoir (i,j,k,i+1,j+1,k+1)

       !******************************************
       ! On d�termine chaque composante de la vitesse sur les faces
       !! Velocity interpolation at nodes
       ! equation (32 --> 35 et 40 --> 43) 
       !! U component

       VITTEXU1=0.25_8*(u(i,j,k)+u(i,j-1,k)+u(i,j-1,k-1)+u(i,j,k-1))          ! (i,j,k)
       VITTEXU2=0.25_8*(u(i+1,j,k)+u(i+1,j-1,k)+u(i+1,j,k-1)+u(i+1,j-1,k-1))  ! (i+1,j,k)
       VITTEXU3=0.25_8*(u(i,j+1,k)+u(i,j,k)+u(i,j+1,k-1)+u(i,j,k-1))          ! (i,j+1,k)
       VITTEXU4=0.25_8*(u(i+1,j+1,k)+u(i+1,j,k)+u(i+1,j+1,k-1)+u(i+1,j,k-1))  ! (i+1,j+1,k)
       VITTEXU5=0.25_8*(u(i,j,k+1)+u(i,j-1,k+1)+u(i,j,k)+u(i,j-1,k))          ! (i,j,k+1)
       VITTEXU6=0.25_8*(u(i+1,j,k+1)+u(i+1,j-1,k+1)+u(i+1,j,k)+u(i+1,j-1,k))  ! (i+1,j,k+1)
       VITTEXU7=0.25_8*(u(i,j+1,k+1)+u(i,j,k+1)+u(i,j+1,k)+u(i,j,k))          ! (i,j+1,k+1)
       VITTEXU8=0.25_8*(u(i+1,j+1,k+1)+u(i+1,j,k+1)+u(i+1,j+1,k)+u(i+1,j,k))  ! (i+1,j+1,k+1)
       VITTEXU9=0.25_8*(u(i-1,j,k)+u(i-1,j-1,k)+u(i-1,j,k-1)+u(i-1,j-1,k-1))  ! (i-1,j,k)
       VITTEXU10=0.25_8*(u(i-1,j,k+1)+u(i-1,j-1,k+1)+u(i-1,j,k)+u(i-1,j-1,k)) ! (i-1,j,k+1)
       VITTEXU11=0.25_8*(u(i-1,j+1,k)+u(i-1,j,k)+u(i-1,j+1,k-1)+u(i-1,j,k-1)) ! (i-1,j+1,k)
       VITTEXU12=0.25_8*(u(i-1,j+1,k+1)+u(i-1,j,k+1)+u(i-1,j+1,k)+u(i-1,j,k)) ! (i-1,j+1,k+1)
       VITTEXU13=0.25_8*(u(i+2,j,k)+u(i+2,j-1,k)+u(i+2,j,k-1)+u(i+2,j-1,k-1)) ! (i+2,j,k)
       VITTEXU14=0.25_8*(u(i+2,j,k+1)+u(i+2,j-1,k+1)+u(i+2,j,k)+u(i+2,j-1,k)) ! (i+2,j,k+1)
       VITTEXU15=0.25_8*(u(i+2,j+1,k)+u(i+2,j,k)+u(i+2,j+1,k-1)+u(i+2,j,k-1)) ! (i+2,j+1,k)
       VITTEXU16=0.25_8*(u(i+2,j+1,k+1)+u(i+2,j,k+1)+u(i+2,j+1,k)+u(i+2,j,k)) ! (i+2,j+1,k+1)

       !! V component
       VITTEXV1=0.25_8*(v(i,j,k)+v(i,j,k-1)+v(i-1,j,k)+v(i-1,j,k-1))          ! (i,j,k)
       VITTEXV2=0.25_8*(v(i+1,j,k)+v(i+1,j,k-1)+v(i,j,k)+v(i,j,k-1))          ! (i+1,j,k)
       VITTEXV3=0.25_8*(v(i,j+1,k)+v(i,j+1,k-1)+v(i-1,j+1,k)+v(i-1,j+1,k-1))  ! (i,j+1,k)
       VITTEXV4=0.25_8*(v(i+1,j+1,k)+v(i+1,j+1,k-1)+v(i,j+1,k)+v(i,j+1,k-1))  ! (i+1,j+1,k)
       VITTEXV5=0.25_8*(v(i,j,k+1)+v(i,j,k)+v(i-1,j,k+1)+v(i-1,j,k))          ! (i,j,k+1)
       VITTEXV6=0.25_8*(v(i+1,j,k+1)+v(i+1,j,k)+v(i,j,k+1)+v(i,j,k))          ! (i+1,j,k+1)
       VITTEXV7=0.25_8*(v(i,j+1,k+1)+v(i,j+1,k)+v(i-1,j+1,k+1)+v(i-1,j+1,k))  ! (i,j+1,k+1)
       VITTEXV8=0.25_8*(v(i+1,j+1,k+1)+v(i+1,j+1,k)+v(i,j+1,k+1)+v(i,j+1,k))  ! (i+1,j+1,k+1)
       VITTEXV17=0.25_8*(v(i,j-1,k)+v(i,j-1,k-1)+v(i-1,j-1,k)+v(i-1,j-1,k-1)) ! (i,j-1,k)
       VITTEXV18=0.25_8*(v(i,j-1,k+1)+v(i,j-1,k)+v(i-1,j-1,k+1)+v(i-1,j-1,k)) ! (i,j-1,k+1)
       VITTEXV19=0.25_8*(v(i+1,j-1,k+1)+v(i+1,j-1,k)+v(i,j-1,k+1)+v(i,j-1,k)) ! (i+1,j-1,k+1)
       VITTEXV20=0.25_8*(v(i+1,j-1,k)+v(i+1,j-1,k-1)+v(i,j-1,k)+v(i,j-1,k-1)) ! (i+1,j-1,k)
       VITTEXV21=0.25_8*(v(i,j+2,k)+v(i,j+2,k-1)+v(i-1,j+2,k)+v(i-1,j+2,k-1)) ! (i,j+2,k)
       VITTEXV22=0.25_8*(v(i,j+2,k+1)+v(i,j+2,k)+v(i-1,j+2,k+1)+v(i-1,j+2,k)) ! (i,j+2,k+1)
       VITTEXV23=0.25_8*(v(i+1,j+2,k+1)+v(i+1,j+2,k)+v(i,j+2,k+1)+v(i,j+2,k)) ! (i+1,j+2,k+1)
       VITTEXV24=0.25_8*(v(i+1,j+2,k)+v(i+1,j+2,k-1)+v(i,j+2,k)+v(i,j+2,k-1)) ! (i+1,j+2,k)


       !! W component
       VITTEXW1=0.25_8*(w(i,j,k)+w(i,j-1,k)+w(i-1,j,k)+w(i-1,j-1,k))          ! (i,j,k)
       VITTEXW2=0.25_8*(w(i+1,j,k)+w(i+1,j-1,k)+w(i,j,k)+w(i,j-1,k))          ! (i+1,j,k)
       VITTEXW3=0.25_8*(w(i,j+1,k)+w(i,j,k)+w(i-1,j+1,k)+w(i-1,j,k))          ! (i,j+1,k)
       VITTEXW4=0.25_8*(w(i+1,j+1,k)+w(i+1,j,k)+w(i,j+1,k)+w(i,j,k))          ! (i+1,j+1,k)
       VITTEXW5=0.25_8*(w(i,j,k+1)+w(i,j-1,k+1)+w(i-1,j,k+1)+w(i-1,j-1,k+1))  ! (i,j,k+1)
       VITTEXW6=0.25_8*(w(i+1,j,k+1)+w(i+1,j-1,k+1)+w(i,j,k+1)+w(i,j-1,k+1))  ! (i+1,j,k+1)
       VITTEXW7=0.25_8*(w(i,j+1,k+1)+w(i,j,k+1)+w(i-1,j+1,k+1)+w(i-1,j,k+1))  ! (i,j+1,k+1)
       VITTEXW8=0.25_8*(w(i+1,j+1,k+1)+w(i+1,j,k+1)+w(i,j+1,k+1)+w(i,j,k+1))  ! (i+1,j+1,k+1)
       VITTEXW25=0.25_8*(w(i,j,k-1)+w(i,j-1,k-1)+w(i-1,j,k-1)+w(i-1,j-1,k-1)) ! (i,j,k-1)
       VITTEXW26=0.25_8*(w(i+1,j,k-1)+w(i+1,j-1,k-1)+w(i,j,k-1)+w(i,j-1,k-1)) ! (i+1,j,k-1)
       VITTEXW27=0.25_8*(w(i+1,j+1,k-1)+w(i+1,j,k-1)+w(i,j+1,k-1)+w(i,j,k-1)) ! (i+1,j+1,k-1)
       VITTEXW28=0.25_8*(w(i,j+1,k-1)+w(i,j,k-1)+w(i-1,j+1,k-1)+w(i-1,j,k-1)) ! (i,j+1,k-1)
       VITTEXW29=0.25_8*(w(i,j,k+2)+w(i,j-1,k+2)+w(i-1,j,k+2)+w(i-1,j-1,k+2)) ! (i,j,k+2)
       VITTEXW30=0.25_8*(w(i+1,j,k+2)+w(i+1,j-1,k+2)+w(i,j,k+2)+w(i,j-1,k+2)) ! (i+1,j,k+2)
       VITTEXW31=0.25_8*(w(i+1,j+1,k+2)+w(i+1,j,k+2)+w(i,j+1,k+2)+w(i,j,k+2)) ! (i+1,j+1,k+2)
       VITTEXW32=0.25_8*(w(i,j+1,k+2)+w(i,j,k+2)+w(i-1,j+1,k+2)+w(i-1,j,k+2)) ! (i,j+1,k+2)

       !*****************************************
       !! Temporary velocity on edges !! vitesse temporaire sur les faces
       ! texte avant equation (36)
       !! for component U
       VITTUE=0.25_8*(VITTEXU2+VITTEXU6+VITTEXU4+VITTEXU8)
       VITTUW=0.25_8*(VITTEXU1+VITTEXU5+VITTEXU3+VITTEXU7)
       !! for component V 
       VITTVS=0.25_8*(VITTEXV1+VITTEXV5+VITTEXV2+VITTEXV6)
       VITTVN=0.25_8*(VITTEXV4+VITTEXV8+VITTEXV3+VITTEXV7)
       !! for component W
       VITTWT=0.25_8*(VITTEXW5+VITTEXW6+VITTEXW7+VITTEXW8)
       VITTWB=0.25_8*(VITTEXW4+VITTEXW3+VITTEXW2+VITTEXW1)

       !***************************************
       !! Node velocity
       ! (36 --> 39)
       !! for component U
       VITEXU1=VITTEXU1+u(i,j,k)-VITTUW   ! u(0,0,0)
       VITEXU2=VITTEXU2+u(i+1,j,k)-VITTUE ! u(1,0,0)
       VITEXU3=VITTEXU3+u(i,j,k)-VITTUW   ! u(0,1,0)
       VITEXU4=VITTEXU4+u(i+1,j,k)-VITTUE ! u(1,1,0)
       VITEXU5=VITTEXU5+u(i,j,k)-VITTUW   ! u(0,0,1)
       VITEXU6=VITTEXU6+u(i+1,j,k)-VITTUE ! u(1,0,1)
       VITEXU7=VITTEXU7+u(i,j,k)-VITTUW   ! u(0,1,1)
       VITEXU8=VITTEXU8+u(i+1,j,k)-VITTUE ! u(1,1,1)
       ! (44 --> 47)
       !! for component V
       VITEXV1=VITTEXV1+v(i,j,k)-VITTVS   ! v(0,0,0)
       VITEXV2=VITTEXV2+v(i,j,k)-VITTVS   ! v(1,0,0)
       VITEXV3=VITTEXV3+v(i,j+1,k)-VITTVN ! v(0,1,0)
       VITEXV4=VITTEXV4+v(i,j+1,k)-VITTVN ! v(1,1,0)
       VITEXV5=VITTEXV5+v(i,j,k)-VITTVS   ! v(0,0,1)
       VITEXV6=VITTEXV6+v(i,j,k)-VITTVS   ! v(1,0,1)
       VITEXV7=VITTEXV7+v(i,j+1,k)-VITTVN ! v(0,1,1)
       VITEXV8=VITTEXV8+v(i,j+1,k)-VITTVN ! v(1,1,1)

       !! for component W
       VITEXW1=VITTEXW1+w(i,j,k)-VITTWB 
       VITEXW2=VITTEXW2+w(i,j,k)-VITTWB
       VITEXW3=VITTEXW3+w(i,j,k)-VITTWB
       VITEXW4=VITTEXW4+w(i,j,k)-VITTWB
       VITEXW5=VITTEXW5+w(i,j,k+1)-VITTWT   
       VITEXW6=VITTEXW6+w(i,j,k+1)-VITTWT 
       VITEXW7=VITTEXW7+w(i,j,k+1)-VITTWT 
       VITEXW8=VITTEXW8+w(i,j,k+1)-VITTWT

       !*******************************************  
       !! Velocity on edges
       ! (48 --> 51)
       ! U South (ok � priori)
       VITUBS=0.5_8*(VITEXU1+VITEXU2)
       VITUTS=0.5_8*(VITEXU5+VITEXU6)
       ! U North
       VITUBN=0.5_8*(VITEXU3+VITEXU4)
       VITUTN=0.5_8*(VITEXU7+VITEXU8)
       ! V Est
       VITVBE=0.5_8*(VITEXV4+VITEXV2)
       VITVTE=0.5_8*(VITEXV8+VITEXV6)
       ! V West
       VITVBW=0.5_8*(VITEXV1+VITEXV3)
       VITVTW=0.5_8*(VITEXV5+VITEXV7)
       ! W North                     
       VITWNE=0.5_8*(VITEXW4+VITEXW8)
       VITWNW=0.5_8*(VITEXW3+VITEXW7)
       ! W South
       VITWSE=0.5_8*(VITEXW2+VITEXW6)
       VITWSW=0.5_8*(VITEXW1+VITEXW5)

       !*************************************

       ! (52 --> 55)
       !! Slopes first order

       !  B : Bottom, T : Top
       ! U South
       DUSB=VITEXU2-VITEXU1
       DUST=VITEXU6-VITEXU5
       ! U North
       DUNB=VITEXU4-VITEXU3
       DUNT=VITEXU8-VITEXU7
       ! V West
       DVWB=VITEXV3-VITEXV1
       DVWT=VITEXV7-VITEXV5
       ! V Est
       DVEB=VITEXV4-VITEXV2
       DVET=VITEXV8-VITEXV6
       ! W North
       DWNW=VITEXW7-VITEXW3
       DWNE=VITEXW8-VITEXW4
       ! W South
       DWSW=VITEXW5-VITEXW1
       DWSE=VITEXW6-VITEXW2

       !*************************************  
       !! Slopes second order
       ! (70)
       ! First slope term: DELTA hat
       ! 0.5D0 ou 0.25D0 (� v�rifier)
       !! for U component
       ! U South
       DUSBCHAP=0.5D0*(VITTEXU13-VITTEXU2-VITTEXU1+VITTEXU9)
       DUSTCHAP=0.5D0*(VITTEXU14-VITTEXU6-VITTEXU5+VITTEXU10)
       ! U North
       DUNBCHAP=0.5D0*(VITTEXU15-VITTEXU4-VITTEXU3+VITTEXU11)
       DUNTCHAP=0.5D0*(VITTEXU16-VITTEXU8-VITTEXU7+VITTEXU12)

       !! for V component
       ! V West
       DVWBCHAP=0.5D0*(VITTEXV21-VITTEXV3-VITTEXV1+VITTEXV17)
       DVWTCHAP=0.5D0*(VITTEXV22-VITTEXV7-VITTEXV5+VITTEXV18)
       ! V Est
       DVEBCHAP=0.5D0*(VITTEXV24-VITTEXV4-VITTEXV2+VITTEXV20)
       DVETCHAP=0.5D0*(VITTEXV23-VITTEXV8-VITTEXV6+VITTEXV19)

       !! for W component
       ! W North
       DWNWCHAP=0.5D0*(VITTEXW32-VITTEXW7-VITTEXW3+VITTEXW28)
       DWNECHAP=0.5D0*(VITTEXW31-VITTEXW8-VITTEXW4+VITTEXW27)
       ! W South
       DWSWCHAP=0.5D0*(VITTEXW29-VITTEXW5-VITTEXW1+VITTEXW25)
       DWSECHAP=0.5D0*(VITTEXW30-VITTEXW6-VITTEXW2+VITTEXW26)

       !*************************************   
       ! (71)
       ! Second slope term: DELTA

       ! Theta vector components from Pope's article
       Theta(1)=DIVEX1
       Theta(2)=DIVEX2
       Theta(3)=DIVEX3
       Theta(4)=DIVEX4
       Theta(5)=DIVEX5
       Theta(6)=DIVEX6
       Theta(7)=DIVEX7
       Theta(8)=DIVEX8

       ! Delta1 vector components from Pope's article
       Delta1(1)=DUSB
       Delta1(2)=DUNB
       Delta1(3)=DUST
       Delta1(4)=DUNT
       Delta1(5)=DVWB
       Delta1(6)=DVEB
       Delta1(7)=DVWT
       Delta1(8)=DVET
       Delta1(9)=DWSW
       Delta1(10)=DWNW
       Delta1(11)=DWSE
       Delta1(12)=DWNE

       ! Delta Hat vector components from Pope's article
       Deltah(1)=DUSBCHAP
       Deltah(2)=DUNBCHAP
       Deltah(3)=DUSTCHAP
       Deltah(4)=DUNTCHAP
       Deltah(5)=DVWBCHAP
       Deltah(6)=DVEBCHAP
       Deltah(7)=DVWTCHAP
       Deltah(8)=DVETCHAP
       Deltah(9)=DWSWCHAP
       Deltah(10)=DWNWCHAP
       Deltah(11)=DWSECHAP
       Deltah(12)=DWNECHAP

       ! Compute of delta, the vector for correcting Deltah
       delta=matmul(Bplus,Theta-matmul(Aperm,Delta1)-matmul(Bperm,Deltah))

       ! Correction of Delta h
       ! Delta2 vector components from Pope's article
       Delta2 = Deltah + delta

       !! Interpolated Velocity
       VP(1) = (1._8-R(3))*((1._8-R(2))*(VITUBS+(R(1)-0.5_8)*DUSB+0.5_8*((R(1)-0.5_8)**2-0.25_8)*Delta2(1))&
            & +R(2)*(VITUBN+(R(1)-0.5_8)*DUNB+0.5_8*((R(1)-0.5_8)**2-0.25_8)*Delta2(2)))&
            & +R(3)*((1._8-R(2))*(VITUTS+(R(1)-0.5_8)*DUST+0.5_8*((R(1)-0.5_8)**2-0.25_8)*Delta2(3))&
            & +R(2)*(VITUTN+(R(1)-0.5_8)*DUNT+0.5_8*((R(1)-0.5_8)**2-0.25_8)*Delta2(4)))

       VP(2) = (1._8-R(3))*((1._8-R(1))*(VITVBW+(R(2)-0.5_8)*DVWB+0.5_8*((R(2)-0.5_8)**2-0.25_8)*Delta2(5))&
            & +R(1)*(VITVBE+(R(2)-0.5_8)*DVEB+0.5_8*((R(2)-0.5_8)**2-0.25_8)*Delta2(6)))&
            & +R(3)*((1._8-R(1))*(VITVTW+(R(2)-0.5_8)*DVWT+0.5_8*((R(2)-0.5_8)**2-0.25_8)*Delta2(7))&
            & +R(1)*(VITVTE+(R(2)-0.5_8)*DVET+0.5_8*((R(2)-0.5_8)**2-0.25_8)*Delta2(8)))

       VP(3) = (1._8-R(1))*((1._8-R(2))*(VITWSW+(R(3)-0.5_8)*DWSW+0.5_8*((R(2)-0.5_8)**2-0.25_8)*Delta2(9))&
            & +R(2)*(VITWNW+(R(3)-0.5_8)*DWNW+0.5_8*((R(3)-0.5_8)**2-0.25_8)*Delta2(10)))&
            & +R(1)*((1._8-R(2))*(VITWSE+(R(3)-0.5_8)*DWSE+0.5_8*((R(3)-0.5_8)**2-0.25_8)*Delta2(11))&
            & +R(2)*(VITWNE+(R(3)-0.5_8)*DWNE+0.5_8*((R(3)-0.5_8)**2-0.25_8)*Delta2(12)))
    else
       write(*,*) "Perm for irregular meshes is not yet implemented"
       stop
    end if
       
  end subroutine Perm3D
  
  subroutine Perm2D(u,v,xyz,vp)
    !***************************************************************************************
    implicit none
    !***************************************************************************************
    !Global variables
    !***************************************************************************************
    real(8), dimension(:), intent(in)                  :: xyz
    real(8), allocatable, dimension(:,:,:), intent(in) :: u,v
    real(8), dimension(:), intent(inout)               :: vp
    !***************************************************************************************
    !Local variables
    !***************************************************************************************
    integer                                            :: i,j,num
    real(8), dimension(2)                              :: R
    real(8)                                            :: DUS,DUN,DVW,DVE,DUSS,DUNN,DVWW,DVEE
    real(8)                                            :: DUSCHAP,DUNCHAP,DVECHAP,DVWCHAP
    real(8)                                            :: DIV1,DIV2,DIV3,DIV4,DIV5,DIV6,DIV7,DIV8,DIV9
    real(8)                                            :: DIVEX1,DIVEX2,DIVEX3,DIVEX4
    real(8)                                            :: VITTEXU1,VITTEXU2,VITTEXU3,VITTEXU4,VITTEXU5
    real(8)                                            :: VITTEXU6,VITTEXU7,VITTEXU8,VITTEXV1,VITTEXV2,VITTEXV3
    real(8)                                            :: VITTEXV4,VITTEXV9,VITTEXV10,VITTEXV11,VITTEXV12
    real(8)                                            :: VITTUE,VITTUW,VITTVS,VITTVN
    real(8)                                            :: VITEXU1,VITEXU2,VITEXU3,VITEXU4,VITEXV1
    real(8)                                            :: VITEXV2,VITEXV3,VITEXV4
    real(8)                                            :: VITUS,VITUN,VITVE,VITVW
    real(8)                                            :: DELTA1,DELTA2,DELTA3,DELTA4
    real(8)                                            :: H,aa,bb
    !***************************************************************************************

    if (regular_mesh) then 
       H=mesh%dx(mesh%sx)
       aa=mesh%dx(mesh%sx)/mesh%dy(mesh%sy)
       !---------------------------velocity interpolation-------------------------------------- 
       i=(xyz(1)-(mesh%xmin-mesh%dx(mesh%sx)/2))/mesh%dx(mesh%sx)
       j=(xyz(2)-(mesh%ymin-mesh%dy(mesh%sy)/2))/mesh%dy(mesh%sy)
       !-----------------------------local coordinates-----------------------------------------
       ! (21 --> 22)
       R(1)=(xyz(1)-(mesh%xmin+i*mesh%dx(mesh%sx)))/(aa*mesh%dy(mesh%sy))+0.5d0
       R(2)=(xyz(2)-(mesh%ymin+j*mesh%dy(mesh%sy)))/mesh%dy(mesh%sy)+0.5d0
       !---------------------------Divergence discretization ----------------------------------
       ! equation (23)
       DIV1=(u(i+1,j,1)-u(i,j,1)+v(i,j+1,1)-v(i,j,1))/H
       DIV2=(u(i,j,1)-u(i-1,j,1)+v(i-1,j+1,1)-v(i-1,j,1))/H
       DIV3=(u(i+1,j-1,1)-u(i,j-1,1)+v(i,j,1)-v(i,j-1,1))/H
       DIV4=(u(i,j-1,1)-u(i-1,j-1,1)+v(i-1,j,1)-v(i-1,j-1,1))/H
       DIV5=(u(i+2,j,1)-u(i+1,j,1)+v(i+1,j+1,1)-v(i+1,j,1))/H
       DIV6=(u(i+2,j-1,1)-u(i+1,j-1,1)+v(i+1,j,1)-v(i+1,j-1,1))/H
       DIV7=(u(i+1,j+1,1)-u(i,j+1,1)+v(i,j+2,1)-v(i,j+1,1))/H
       DIV8=(u(i,j+1,1)-u(i-1,j+1,1)+v(i-1,j+2,1)-v(i-1,j+1,1))/H
       DIV9=(u(i+2,j+1,1)-u(i+1,j+1,1)+v(i+1,j+2,1)-v(i+1,j+1,1))/H
       !---------------------------Divergence interpolation at nodes---------------------------

       !***************************************
       !! Interpolation of divergence at nodes
       ! equation (25)
       DIVEX1=H*0.25D0*(DIV1+DIV2+DIV3+DIV4)
       DIVEX2=H*0.25D0*(DIV5+DIV1+DIV6+DIV3)
       DIVEX3=H*0.25D0*(DIV7+DIV8+DIV1+DIV2)
       DIVEX4=H*0.25D0*(DIV9+DIV7+DIV5+DIV1)

       !******************************************  
       !! Velocity interpolation at nodes
       ! equation (32 --> 35)
       !! U component
       VITTEXU1=0.5D0*(U(I,J,1)+U(I,J-1,1))
       VITTEXU2=0.5D0*(U(I+1,J,1)+U(I+1,J-1,1))
       VITTEXU3=0.5D0*(U(I,J+1,1)+U(I,J,1))
       VITTEXU4=0.5D0*(U(I+1,J+1,1)+U(I+1,J,1))
       VITTEXU5=0.5D0*(U(I-1,J,1)+U(I-1,J-1,1))
       VITTEXU6=0.5D0*(U(I+2,J,1)+U(I+2,J-1,1))
       VITTEXU7=0.5D0*(U(I-1,J+1,1)+U(I-1,J,1))
       VITTEXU8=0.5D0*(U(I+2,J+1,1)+U(I+2,J,1))
       ! equation (40 --> 43)
       !! V component 
       VITTEXV1=0.5D0*(V(I,J,1)+V(I-1,J,1))
       VITTEXV2=0.5D0*(V(I+1,J,1)+V(I,J,1))
       VITTEXV3=0.5D0*(V(I,J+1,1)+V(I-1,J+1,1))
       VITTEXV4=0.5D0*(V(I+1,J+1,1)+V(I,J+1,1))
       VITTEXV9=0.5D0*(V(I,J-1,1)+V(I-1,J-1,1))
       VITTEXV10=0.5D0*(V(I+1,J-1,1)+V(I,J-1,1))
       VITTEXV11=0.5D0*(V(I,J+2,1)+V(I-1,J+2,1))
       VITTEXV12=0.5D0*(V(I+1,J+2,1)+V(I,J+2,1))

       !*****************************************
       !! Temporary velocity on edges
       ! texte avant equation (36)
       !! for component U
       VITTUE=0.5D0*(VITTEXU4+VITTEXU2)
       VITTUW=0.5D0*(VITTEXU3+VITTEXU1)
       !! for component V 
       VITTVS=0.5D0*(VITTEXV2+VITTEXV1)
       VITTVN=0.5D0*(VITTEXV4+VITTEXV3)

       !***************************************
       !! Node velocity
       ! (36 --> 39)
       !! for component U
       VITEXU1=VITTEXU1+U(I,J,1)-VITTUW
       VITEXU2=VITTEXU2+U(I+1,J,1)-VITTUE
       VITEXU3=VITTEXU3+U(I,J,1)-VITTUW
       VITEXU4=VITTEXU4+U(I+1,J,1)-VITTUE
       ! (44 --> 47)
       !! for component V
       VITEXV1=VITTEXV1+V(I,J,1)-VITTVS
       VITEXV2=VITTEXV2+V(I,J,1)-VITTVS
       VITEXV3=VITTEXV3+V(I,J+1,1)-VITTVN
       VITEXV4=VITTEXV4+V(I,J+1,1)-VITTVN

       !*******************************************  
       !! Velocity on edges
       ! (48 --> 51)
       !! for component U
       VITUS=0.5D0*(VITEXU2+VITEXU1)
       VITUN=0.5D0*(VITEXU4+VITEXU3)

       !! for component V 
       VITVE=0.5D0*(VITEXV4+VITEXV2)
       VITVW=0.5D0*(VITEXV3+VITEXV1)

       !*************************************
       ! (52 --> 55)
       !! Slopes first order
       DUS=VITEXU2-VITEXU1
       DUN=VITEXU4-VITEXU3
       DVW=VITEXV3-VITEXV1
       DVE=VITEXV4-VITEXV2

       !*************************************  
       !! Slopes second order
       ! (70)
       ! First slope term: DELTA hat
       DUSCHAP=0.5D0*(VITTEXU6-VITTEXU2-VITTEXU1+VITTEXU5)
       DUNCHAP=0.5D0*(VITTEXU8-VITTEXU4-VITTEXU3+VITTEXU7)
       DVWCHAP=0.5D0*(VITTEXV11-VITTEXV3-VITTEXV1+VITTEXV9)
       DVECHAP=0.5D0*(VITTEXV12-VITTEXV4-VITTEXV2+VITTEXV10)

       ! (71)
       ! Second slope term: DELTA 
       DELTA1=0.25D0*(-3.D0*DIVEX1+4.D0*DVW-3.D0*DUSCHAP-DVWCHAP-DIVEX3-DUNCHAP+3.D0*DIVEX2&
            -4.D0*DVE+DVECHAP+DIVEX4)
       DELTA2=0.25D0*(-DIVEX1+4.D0*DVW-DUSCHAP+DVWCHAP-3.D0*DIVEX3-3.D0*DUNCHAP+DIVEX2-4.D0*DVE& 
            -DVECHAP+3.D0*DIVEX4)
       DELTA3=0.25D0*(-3.D0*DIVEX1+4.D0*DUS-DUSCHAP-3.D0*DVWCHAP+3.D0*DIVEX3-4.D0*DUN+DUNCHAP&
            -DIVEX2-DVECHAP+DIVEX4)
       DELTA4=0.25D0*(-DIVEX1+4.D0*DUS+DUSCHAP-DVWCHAP+DIVEX3-4.D0*DUN-DUNCHAP-3.D0*DIVEX2&
            -3.D0*DVECHAP+3.D0*DIVEX4)

       ! (68)
       ! 2nd order slope
       DUSS=DUSCHAP+DELTA1
       DUNN=DUNCHAP+DELTA2
       DVWW=DVWCHAP+DELTA3
       DVEE=DVECHAP+DELTA4

       ! (26 --> 27)
       !! INTERPOLATED VELOCITY
       VP(1)=(1.D0-R(2))*(VITUS+(R(1)-0.5D0)*DUS+0.5D0*((R(1)-0.5D0)**2.D0-0.25D0)*DUSS)&
            +R(2)*(VITUN+(R(1)-0.5D0)*DUN+0.5D0*((R(1)-0.5D0)**2.D0-0.25D0)*DUNN)
       VP(2)=(1.D0-R(1))*(VITVW+(R(2)-0.5D0)*DVW +0.5D0*((R(2)-0.5D0)**2.D0-0.25D0)*DVWW)&
            +R(1)*(VITVE+(R(2)-0.5D0)*DVE+0.5D0*((R(2)-0.5D0)**2.D0-0.25D0)*DVEE)

       !print*,'VP par perm',VP(1)
    else
       write(*,*) "Perm for irregular meshes is not yet implemented"
       stop
    end if
    
  end subroutine Perm2D

end MODULE Mod_Perm
