!========================================================================
!
!                   FUGU Version 1.0.0
!
!========================================================================
!
! Copyright  Stéphane Vincent
!
! stephane.vincent@u-pem.fr          straightparadigm@gmail.com
!
! This file is part of the FUGU Fortran library, a set of Computational 
! Fluid Dynamics subroutines devoted to the simulation of multi-scale
! multi-phase flows for resolved scale interfaces (liquid/gas/solid). 
! FUGU uses the initial developments of DyJeAT and SHALA codes from 
! Jean-Luc Estivalezes (jean-luc.estivalezes@onera.fr) and 
! Frédéric Couderc (couderc@math.univ-toulouse.fr).
!
!========================================================================
!**
!**   NAME       : CFL.f90
!**
!**   AUTHOR     : Benoit Trouette
!**
!**   FUNCTION   : adaptative time step
!**
!**   DATES      : Version 1.0.0  : from : May, 2022
!**
!========================================================================

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
module mod_cfl
  use mod_timers

contains
  
  subroutine adapt_time_step(cfl,iter,u,v,w,coef,coef0,coef1)
    !-------------------------------------------------------------------------------
    ! adapt time step and change the coefficient of eq. 
    !    du/dt = coef * u + coef + coef0 * u0 + coef1* u1
    ! according the discretisation scheme
    !-------------------------------------------------------------------------------
    use mod_Parameters, only: dim,  &
         & ddxu,ddyv,ddzw,          &
         & Euler,dt,                &
         & gx,gy,gz,                &
         & sxu,exu,syu,eyu,szu,ezu, &
         & sxv,exv,syv,eyv,szv,ezv, &
         & sxw,exw,syw,eyw,szw,ezw
    use mod_struct_cfl
    use mod_mpi
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)                                :: iter
    real(8), intent(inout)                             :: coef,coef0,coef1
    real(8), dimension(:,:,:), allocatable, intent(in) :: u,v,w
    type(cfl_t), intent(inout)                         :: cfl
    !-------------------------------------------------------------------------------
    real(8)                                            :: variation
    real(8)                                            :: max_u_cfl,max_abs_u,maxval_abs_u
    real(8)                                            :: prod_dt,sum_dt
    real(8), dimension(:,:,:), allocatable             :: ut,vt,wt
    !-------------------------------------------------------------------------------

    call compute_time(TIMER_START,"[sub] adapt_time_step")
    
    !-------------------------------------------------------------------------------
    ! activate conditions
    !-------------------------------------------------------------------------------
    if (.not. cfl%active) then
       cfl%dt_min = dt
       cfl%dt_max = dt
       cfl%dt_new = dt
    end if
    !-------------------------------------------------------------------------------
    if (cfl%dt_min<=0 .or. cfl%dt_max<=0 .or. cfl%dt_min > cfl%dt_max) then
       cfl%dt_min = dt
       cfl%dt_max = dt
       cfl%dt_new = dt
       cfl%active = .false.
    end if
    !-------------------------------------------------------------------------------
       
    !-------------------------------------------------------------------------------
    ! CFL = u * dt / dx
    !     = ut * dt
    ! <=> dt = CFL / ut
    !-------------------------------------------------------------------------------
    
    !-------------------------------------------------------------------------------
    ! allocation
    !-------------------------------------------------------------------------------
    allocate(ut(sxu-gx:exu+gx,syu-gy:eyu+gy,szu-gz:ezu+gz)) 
    allocate(vt(sxv-gx:exv+gx,syv-gy:eyv+gy,szv-gz:ezv+gz))
    if (dim==3) allocate(wt(sxw-gx:exw+gx,syw-gy:eyw+gy,szw-gz:ezw+gz)) 
    !-------------------------------------------------------------------------------
    
    !-------------------------------------------------------------------------------
    ! modified velocity (1/s)
    !-------------------------------------------------------------------------------
    call velocity_transformation_2d3d(u,ut,ddxu,1)
    call velocity_transformation_2d3d(v,vt,ddyv,2)
    if (dim==3) call velocity_transformation_2d3d(w,wt,ddzw,3) 
    !-------------------------------------------------------------------------------
    
    !-------------------------------------------------------------------------------
    ! compute maximum transformed velocity u/dx
    !-------------------------------------------------------------------------------
    max_u_cfl=0
    !-------------------------------------------------------------------------------
    ! x-direction
    !-------------------------------------------------------------------------------
    maxval_abs_u=maxval(abs(ut(sxu:exu,syu:eyu,szu:ezu)))
    CALL MPI_ALLREDUCE(maxval_abs_u,max_abs_u,1,MPI_DOUBLE_PRECISION,MPI_MAX,COMM3D,MPI_CODE)
    if (max_abs_u > max_u_cfl) max_u_cfl=max_abs_u
    !-------------------------------------------------------------------------------
    ! y-direction
    !-------------------------------------------------------------------------------
    maxval_abs_u=maxval(abs(vt(sxv:exv,syv:eyv,szv:ezv)))
    CALL MPI_ALLREDUCE(maxval_abs_u,max_abs_u,1,MPI_DOUBLE_PRECISION,MPI_MAX,COMM3D,MPI_CODE)
    if (max_abs_u > max_u_cfl) max_u_cfl=max_abs_u
    !-------------------------------------------------------------------------------
    ! z-direction
    !-------------------------------------------------------------------------------
    if (dim==3) then
       maxval_abs_u=maxval(abs(wt(sxw:exw,syw:eyw,szw:ezw)))
       CALL MPI_ALLREDUCE(maxval_abs_u,max_abs_u,1,MPI_DOUBLE_PRECISION,MPI_MAX,COMM3D,MPI_CODE)
       if (max_abs_u > max_u_cfl) max_u_cfl=max_abs_u
    end if
    !-------------------------------------------------------------------------------
    
    !-------------------------------------------------------------------------------
    ! adapt current time step and save previous
    !-------------------------------------------------------------------------------
    cfl%dt_old=cfl%dt_new
    cfl%dt_new=max(min(cfl%val/(max_u_cfl+1d-40),cfl%dt_max),cfl%dt_min)
    !-------------------------------------------------------------------------------
    ! limits : +/- variation
    !-------------------------------------------------------------------------------
    variation=0.01d0
    if (cfl%dt_new/cfl%dt_old > 1d0 ) then
       cfl%dt_obj=cfl%dt_new
       cfl%dt_new=min(cfl%dt_new,cfl%dt_old*(1+variation))
    else
       cfl%dt_obj=-1
    end if
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Time discretization coefficients
    !-------------------------------------------------------------------------------
    if (iter<=1) then
       cfl%dt_new = dt
       coef       = 1d0/cfl%dt_new
       coef0      = -coef
       coef1      = 0
    else
       select case(Euler)
       case(1)
          coef    = 1d0/cfl%dt_new
          coef0   = -coef
          coef1   = 0
       case(2)
          prod_dt = cfl%dt_new*cfl%dt_old
          sum_dt  = cfl%dt_new+cfl%dt_old

          coef    = prod_dt*sum_dt
          coef    = 1d0/coef

          coef0   = coef
          coef1   = coef

          coef    =  coef  * (2*prod_dt+cfl%dt_old**2)
          coef0   = -coef0 * (cfl%dt_new**2+2*prod_dt+cfl%dt_old**2)
          coef1   =  coef1 * cfl%dt_new**2
       end select
    end if
    !-------------------------------------------------------------------------------
    
    call compute_time(TIMER_END,"[sub] adapt_time_step")
  end subroutine adapt_time_step

  subroutine CFL_print(cfl)
    use mod_Parameters, only: rank
    use mod_struct_cfl
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    type(cfl_t), intent(in) :: cfl
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    real(8)                 :: var,var_obj
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! screen print
    !-------------------------------------------------------------------------------
    if (rank==0) then 
       write(*,*) ''//achar(27)//'[0m==========================CFL=================================='//achar(27)//'[0m'
       if (cfl%active) then

          var     = 10*(cfl%dt_new/cfl%dt_old-1)  ! WTF
          var_obj = 10*(cfl%dt_obj/cfl%dt_new-1)  ! WTF
          
          write(*,'(a,1x,1pe7.1)') "Time step was adapted with CFL=",cfl%val
          write(*,'(a,1pe12.5,a,1pe11.5,a)') "Time step interval is [",cfl%dt_min,",",cfl%dt_max,"]"
          write(*,'(a,1pe12.5)') "dt_old     = ",cfl%dt_old
          write(*,'(a,1pe12.5,a,f7.2,a)') "dt_new     = ",cfl%dt_new," (",var,"%)"
          !write(*,'(a,1x,f6.2)')       "dt_var (%) =",100*(cfl%dt_new/cfl%dt_old-1)
          if (cfl%dt_obj>0) then
             write(*,'(a,1pe12.5,a,f7.2,a)') "dt_obj     = ",cfl%dt_obj," (",var_obj,"%)"
          end if
       else  
          write(*,'(a,1x,1pe7.1)') "Time step was fixed, dt =",cfl%dt_new
       end if
    end if
    !-------------------------------------------------------------------------------
  end subroutine CFL_print

  subroutine Adams_Bashforth_velocity_extrapolation(u,u0,u1,dt_new,dt_old)
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable, intent(inout) :: u
    real(8), dimension(:,:,:), allocatable, intent(in)    :: u0,u1
    real(8), intent(in)                                   :: dt_new,dt_old
    !-------------------------------------------------------------------------------
    ! Local variable
    !-------------------------------------------------------------------------------
    real(8)                                               :: ratio_dt
    !-------------------------------------------------------------------------------

    call compute_time(TIMER_START,"[sub] Adams_Bashforth_velocity_extrapolation")

    ratio_dt=dt_new/dt_old
    u=u0*(ratio_dt+1)-u1*ratio_dt
    
    call compute_time(TIMER_END,"[sub] Adams_Bashforth_velocity_extrapolation")
  end subroutine Adams_Bashforth_velocity_extrapolation

  
  subroutine velocity_transformation_2d3d (var,vart,ddx,dir)
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)                                   :: dir
    real(8), dimension(:,:,:), allocatable, intent(in)    :: var
    real(8), dimension(:,:,:), allocatable, intent(inout) :: vart
    real(8), dimension(:), allocatable, intent(in)        :: ddx
    !-------------------------------------------------------------------------------
    ! Local variable
    !-------------------------------------------------------------------------------
    integer                                               :: i,imax,imin
    !-------------------------------------------------------------------------------

    imin=lbound(var,dir)
    imax=ubound(var,dir)

    if (dir==1) then
       do i=imin,imax
          vart(i,:,:)=var(i,:,:)*ddx(i)
       end do
    elseif (dir==2) then
       do i=imin,imax
          vart(:,i,:)=var(:,i,:)*ddx(i)
       end do
    elseif (dir==3) then
       do i=imin,imax
          vart(:,:,i)=var(:,:,i)*ddx(i)
       end do
    end if

  end subroutine velocity_transformation_2d3d

end module mod_cfl
