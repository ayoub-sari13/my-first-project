
!========================================================================
!
!                   FUGU Version 1.0.0
!
!========================================================================
!
! Copyright  St�phane Vincent
!
! stephane.vincent@u-pem.fr          straightparadigm@gmail.com
!
! This file is part of the FUGU Fortran library, a set of Computational 
! Fluid Dynamics subroutines devoted to the simulation of multi-scale
! multi-phase flows for resolved scale interfaces (liquid/gas/solid). 
! FUGU uses the initial developments of DyJeAT and SHALA codes from %
! Jean-Luc Estivalezes (jean-luc.estivalezes@onera.fr) and 
! Fr�d�ric Couderc (couderc@math.univ-toulouse.fr).
!
!========================================================================
!**
!**   NAME       : Deriv_xyz_3D.f90
!**
!**   AUTHOR     : Benoit Trouette
!**                Georges Halim Attalah
!**
!**   FUNCTION   : Modules for computation of derivatives and SijSij
!**
!**   DATES      : Version 1.0.0  : from : nov, 2018
!**
!========================================================================

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
MODULE Mod_deriv_xyz
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  use mod_Parameters, only: dim, &
       & dx,dy,dz,               &
       & dxu,dyv,dzw,            &
       & dxu2,dyv2,dzw2,         &
       & nx,ny,nz,               &
       & sx,ex,sy,ey,sz,ez,gx,gy,gz

contains

  !****************************************************************************************
  !****************************************************************************************
  !                            3D SPATIAL DERIVATIVES
  !****************************************************************************************
  !****************************************************************************************
  
  subroutine deriv_xyz(u,v,w,&
       & dudx,dudy,dudz,     &
       & dvdx,dvdy,dvdz,     &
       & dwdx,dwdy,dwdz,     &
       & dudyijk,dudzijk,    &
       & dvdxijk,dvdzijk,    &
       & dwdxijk,dwdyijk)
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), allocatable, dimension(:,:,:), intent(in)    :: u,v,w
    real(8), allocatable, dimension(:,:,:), intent(inout) :: dudx,dudy,dudz,dudyijk,dudzijk
    real(8), allocatable, dimension(:,:,:), intent(inout) :: dvdx,dvdy,dvdz,dvdxijk,dvdzijk
    real(8), allocatable, dimension(:,:,:), intent(inout) :: dwdx,dwdy,dwdz,dwdxijk,dwdyijk
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                               :: i,j,k,i1,j1,k1
    real(8), dimension(3,3)                               :: tensD
    !-------------------------------------------------------------------------------

    !------------------------------------------------------
    ! compute velocity gradients
    !          for rotationnal, shear stess, transport, ...
    !------------------------------------------------------
    !
    !       > horizontal velocity grid
    !       ^ vertical velocity grid
    !       o pressure grid
    !       x rotationnal grid
    !       * particle position (x_p,y_p)
    !
    !   
    !       (0,1)     (1,1)     (1,1)
    !         o--------->---------o        ---
    !         |                   |         |
    !         |     *             |         |
    !         | (x_p,y_p)         |         |
    !         |                   |         |
    !   (0,1) ^         x         ^ (1,1)   dy
    !         |       (1,1)       |         |
    !         |                   |         |
    !         |                   |         |
    !         |       (1,0)       |         |
    !         o--------->---------o        ---
    !       (0,0)               (1,0)
    !
    !         |--------dx---------|
    !
    !------------------------------------------------------

    k1=1
    if (dim==2) k1=0

    do k=sz-k1,ez+k1
       do j=sy-1,ey+1
          do i=sx-1,ex+1

             !------------------------------
             ! derivatives at velocity nodes
             !------------------------------
             ! ijk --> pressure or scalar node
             ! else at rot nodes
             !------------------------------

             dudx(i,j,k)=(u(i+1,j,k)-u(i,j,k))/dx(i)
             dudy(i,j,k)=(u(i,j,k)-u(i,j-1,k))/dyv(j)
             if (dim==3) dudz(i,j,k)=(u(i,j,k)-u(i,j,k-1))/dzw(k)

             dvdx(i,j,k)=(v(i,j,k)-v(i-1,j,k))/dxu(i) 
             dvdy(i,j,k)=(v(i,j+1,k)-v(i,j,k))/dy(j)
             if (dim==3) dvdz(i,j,k)=(v(i,j,k)-v(i,j,k-1))/dzw(k)

             if (dim==3) then 
                dwdx(i,j,k)=(w(i,j,k)-w(i-1,j,k))/dxu(i)
                dwdy(i,j,k)=(w(i,j,k)-w(i,j-1,k))/dyv(j)
                dwdz(i,j,k)=(w(i,j,k+1)-w(i,j,k))/dz(k)
             end if

          end do
       end do
    end do

    !------------------------------------
    ! derivatives at pressure nodes i,j,k
    !------------------------------------
    do k=sz,ez
       do j=sy,ey
          do i=sx,ex

             dudyijk(i,j,k)=                                  &
                  & + dudy(i  ,j  ,k) * dxu2(i+1) * dyv2(j+1) &
                  & + dudy(i+1,j  ,k) * dxu2(i  ) * dyv2(j+1) &
                  & + dudy(i  ,j+1,k) * dxu2(i+1) * dyv2(j  ) &
                  & + dudy(i+1,j+1,k) * dxu2(i  ) * dyv2(j  )
             dudyijk(i,j,k)=dudyijk(i,j,k)/dx(i)/dy(j)
             
             if (dim==3) then 
                dudzijk(i,j,k)=                                  &
                     & + dudz(i  ,j,k  ) * dxu2(i+1) * dzw2(k+1) &
                     & + dudz(i+1,j,k  ) * dxu2(i  ) * dzw2(k+1) &
                     & + dudz(i  ,j,k+1) * dxu2(i+1) * dzw2(k  ) &
                     & + dudz(i+1,j,k+1) * dxu2(i  ) * dzw2(k  )
                dudzijk(i,j,k)=dudzijk(i,j,k)/dx(i)/dz(k)
             end if

             dvdxijk(i,j,k)=                                  &
                  & + dvdx(i  ,j  ,k) * dxu2(i+1) * dyv2(j+1) &
                  & + dvdx(i+1,j  ,k) * dxu2(i  ) * dyv2(j+1) &
                  & + dvdx(i  ,j+1,k) * dxu2(i+1) * dyv2(j  ) &
                  & + dvdx(i+1,j+1,k) * dxu2(i  ) * dyv2(j  )
             dvdxijk(i,j,k)=dvdxijk(i,j,k)/dx(i)/dy(j)
             
             if (dim==3) then
                dvdzijk(i,j,k)=                                  &
                     & + dvdz(i,j  ,k  ) * dyv2(j+1) * dzw2(k+1) &
                     & + dvdz(i,j  ,k+1) * dyv2(j+1) * dzw2(k  ) &
                     & + dvdz(i,j+1,k  ) * dyv2(j  ) * dzw2(k+1) &
                     & + dvdz(i,j+1,k+1) * dyv2(j  ) * dzw2(k  )
                dvdzijk(i,j,k)=dvdzijk(i,j,k)/dy(j)/dz(k)
             end if
             
             if (dim==3) then 
                dwdxijk(i,j,k)= &
                     & + dwdx(i  ,j,k  ) * dxu2(i+1) * dzw2(k+1) &
                     & + dwdx(i+1,j,k  ) * dxu2(i  ) * dzw2(k+1) &
                     & + dwdx(i  ,j,k+1) * dxu2(i+1) * dzw2(k  ) &
                     & + dwdx(i+1,j,k+1) * dxu2(i  ) * dzw2(k  ) 
                dwdxijk(i,j,k)=dwdxijk(i,j,k)/dx(i)/dz(k)
                
                dwdyijk(i,j,k)= &
                     & + dwdy(i,j  ,k  ) * dyv2(j+1) * dzw2(k+1) &
                     & + dwdy(i,j+1,k  ) * dyv2(j  ) * dzw2(k+1) &
                     & + dwdy(i,j  ,k+1) * dyv2(j+1) * dzw2(k  ) &
                     & + dwdy(i,j+1,k+1) * dyv2(j  ) * dzw2(k  ) 
                dwdyijk(i,j,k)=dwdyijk(i,j,k)/dy(j)/dz(k)
             end if

          end do
       end do
    end do
             
  end subroutine deriv_xyz

  subroutine compute_SijSji(u,v,w,SijSji)
    use mod_Constants, only: d1p2
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), allocatable, dimension(:,:,:), intent(in)    :: u,v,w
    real(8), allocatable, dimension(:,:,:), intent(inout) :: SijSji
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                               :: i,j,k,i1,j1,k1
    real(8), dimension(3,3)                               :: tensD
    real(8), allocatable, dimension(:,:,:)                :: dudx,dudy,dudz,dudyijk,dudzijk
    real(8), allocatable, dimension(:,:,:)                :: dvdx,dvdy,dvdz,dvdxijk,dvdzijk
    real(8), allocatable, dimension(:,:,:)                :: dwdx,dwdy,dwdz,dwdxijk,dwdyijk    
    !-------------------------------------------------------------------------------

    allocate(dudx(sx-1:ex+1,sy-1:ey+1,sz-1:ez+1))
    allocate(dudy(sx-1:ex+1,sy-1:ey+1,sz-1:ez+1))
    if (dim==3) allocate(dudz(sx-1:ex+1,sy-1:ey+1,sz-1:ez+1))
    allocate(dudyijk(sx-1:ex+1,sy-1:ey+1,sz-1:ez+1))
    if (dim==3) allocate(dudzijk(sx-1:ex+1,sy-1:ey+1,sz-1:ez+1))

    allocate(dvdx(sx-1:ex+1,sy-1:ey+1,sz-1:ez+1))
    allocate(dvdy(sx-1:ex+1,sy-1:ey+1,sz-1:ez+1))
    if (dim==3) allocate(dvdz(sx-1:ex+1,sy-1:ey+1,sz-1:ez+1))
    allocate(dvdxijk(sx-1:ex+1,sy-1:ey+1,sz-1:ez+1))
    if (dim==3) allocate(dvdzijk(sx-1:ex+1,sy-1:ey+1,sz-1:ez+1))

    if (dim==3) then 
       allocate(dwdx(sx-1:ex+1,sy-1:ey+1,sz-1:ez+1))
       allocate(dwdy(sx-1:ex+1,sy-1:ey+1,sz-1:ez+1))
       allocate(dwdz(sx-1:ex+1,sy-1:ey+1,sz-1:ez+1))
       allocate(dwdxijk(sx-1:ex+1,sy-1:ey+1,sz-1:ez+1))
       allocate(dwdyijk(sx-1:ex+1,sy-1:ey+1,sz-1:ez+1))
    end if

    call deriv_xyz(u,v,w,   &
         & dudx,dudy,dudz,  &
         & dvdx,dvdy,dvdz,  &
         & dwdx,dwdy,dwdz,  &
         & dudyijk,dudzijk, &
         & dvdxijk,dvdzijk, &
         & dwdxijk,dwdyijk)

    SijSji=0
    
    do k=sz,ez
       do j=sy,ey
          do i=sx,ex

             !----------------------------
             ! tensor D at pressure nodes
             !
             !           d u_i     d u_j
             ! 2 D_ij = ------- + -------
             !           d x_j     d x_i
             !
             !----------------------------

             tensD=0

             tensD(1,1)=dudx(i,j,k)
             tensD(1,2)=d1p2*(dudyijk(i,j,k)+dvdxijk(i,j,k))
             tensD(2,1)=tensD(1,2)
             tensD(2,2)=dvdy(i,j,k)

             if (dim==3) then 
                tensD(1,3)=d1p2*(dudzijk(i,j,k)+dwdxijk(i,j,k))
                tensD(2,3)=d1p2*(dvdzijk(i,j,k)+dwdyijk(i,j,k))
                tensD(3,1)=tensD(1,3)
                tensD(3,2)=tensD(2,3)
                tensD(3,3)=dwdz(i,j,k)
             end if

             !----------------------------
             ! Sij = Sji ==> SijSij = SijSji
             !----------------------------

             do j1=1,dim
                do i1=1,dim
                   SijSji(i,j,k)=SijSji(i,j,k)+tensD(i1,j1)*tensD(j1,i1)
                end do
             end do

          end do
       end do
    end do
    
  end subroutine Compute_SijSji
  
  subroutine compute_Sij(u,v,w,Sij)
    use mod_Constants, only: d1p2
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), allocatable, dimension(:,:,:), intent(in)    :: u,v,w
    real(8), allocatable, dimension(:,:,:), intent(inout) :: Sij
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                               :: i,j,k,i1,j1,k1
    real(8), dimension(3,3)                               :: tensD
    real(8), allocatable, dimension(:,:,:)                :: dudx,dudy,dudz,dudyijk,dudzijk
    real(8), allocatable, dimension(:,:,:)                :: dvdx,dvdy,dvdz,dvdxijk,dvdzijk
    real(8), allocatable, dimension(:,:,:)                :: dwdx,dwdy,dwdz,dwdxijk,dwdyijk    
    !-------------------------------------------------------------------------------

    allocate(dudx(sx-1:ex+1,sy-1:ey+1,sz-1:ez+1))
    allocate(dudy(sx-1:ex+1,sy-1:ey+1,sz-1:ez+1))
    if (dim==3) allocate(dudz(sx-1:ex+1,sy-1:ey+1,sz-1:ez+1))
    allocate(dudyijk(sx-1:ex+1,sy-1:ey+1,sz-1:ez+1))
    if (dim==3) allocate(dudzijk(sx-1:ex+1,sy-1:ey+1,sz-1:ez+1))

    allocate(dvdx(sx-1:ex+1,sy-1:ey+1,sz-1:ez+1))
    allocate(dvdy(sx-1:ex+1,sy-1:ey+1,sz-1:ez+1))
    if (dim==3) allocate(dvdz(sx-1:ex+1,sy-1:ey+1,sz-1:ez+1))
    allocate(dvdxijk(sx-1:ex+1,sy-1:ey+1,sz-1:ez+1))
    if (dim==3) allocate(dvdzijk(sx-1:ex+1,sy-1:ey+1,sz-1:ez+1))

    if (dim==3) then 
       allocate(dwdx(sx-1:ex+1,sy-1:ey+1,sz-1:ez+1))
       allocate(dwdy(sx-1:ex+1,sy-1:ey+1,sz-1:ez+1))
       allocate(dwdz(sx-1:ex+1,sy-1:ey+1,sz-1:ez+1))
       allocate(dwdxijk(sx-1:ex+1,sy-1:ey+1,sz-1:ez+1))
       allocate(dwdyijk(sx-1:ex+1,sy-1:ey+1,sz-1:ez+1))
    end if

    call deriv_xyz(u,v,w,   &
         & dudx,dudy,dudz,  &
         & dvdx,dvdy,dvdz,  &
         & dwdx,dwdy,dwdz,  &
         & dudyijk,dudzijk, &
         & dvdxijk,dvdzijk, &
         & dwdxijk,dwdyijk)

    Sij=0
    
    do k=sz,ez
       do j=sy,ey
          do i=sx,ex

             !----------------------------
             ! tensor D at pressure nodes
             !
             !           d u_i     d u_j
             ! 2 D_ij = ------- + -------
             !           d x_j     d x_i
             !
             !----------------------------

             tensD=0

             tensD(1,1)=dudx(i,j,k)
             tensD(1,2)=d1p2*(dudyijk(i,j,k)+dvdxijk(i,j,k))
             tensD(2,1)=tensD(1,2)
             tensD(2,2)=dvdy(i,j,k)

             if (dim==3) then 
                tensD(1,3)=d1p2*(dudzijk(i,j,k)+dwdxijk(i,j,k))
                tensD(2,3)=d1p2*(dvdzijk(i,j,k)+dwdyijk(i,j,k))
                tensD(3,1)=tensD(1,3)
                tensD(3,2)=tensD(2,3)
                tensD(3,3)=dwdz(i,j,k)
             end if

             do j1=1,dim
                do i1=1,dim
                   Sij(i,j,k)=Sij(i,j,k)+tensD(i1,j1)
                end do
             end do
             
          end do
       end do
    end do
    
  end subroutine Compute_Sij

  ! ==============================================================
  
  subroutine compute_SijdSjid(u,v,w,SijdSjid)
    use mod_Constants, only: d1p2,d1p3
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), allocatable, dimension(:,:,:), intent(in)    :: u,v,w
    real(8), allocatable, dimension(:,:,:), intent(inout) :: SijdSjid
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                               :: i,j,k,i1,j1,k1,m1,n1
    real(8), dimension(3,3)                               :: tensD,tensO,Sijd
    real(8), allocatable, dimension(:,:,:)                :: dudx,dudy,dudz,dudyijk,dudzijk
    real(8), allocatable, dimension(:,:,:)                :: dvdx,dvdy,dvdz,dvdxijk,dvdzijk
    real(8), allocatable, dimension(:,:,:)                :: dwdx,dwdy,dwdz,dwdxijk,dwdyijk    
    !-------------------------------------------------------------------------------

    allocate(dudx(sx-1:ex+1,sy-1:ey+1,sz-1:ez+1))
    allocate(dudy(sx-1:ex+1,sy-1:ey+1,sz-1:ez+1))
    if (dim==3) allocate(dudz(sx-1:ex+1,sy-1:ey+1,sz-1:ez+1))
    allocate(dudyijk(sx-1:ex+1,sy-1:ey+1,sz-1:ez+1))
    if (dim==3) allocate(dudzijk(sx-1:ex+1,sy-1:ey+1,sz-1:ez+1))

    allocate(dvdx(sx-1:ex+1,sy-1:ey+1,sz-1:ez+1))
    allocate(dvdy(sx-1:ex+1,sy-1:ey+1,sz-1:ez+1))
    if (dim==3) allocate(dvdz(sx-1:ex+1,sy-1:ey+1,sz-1:ez+1))
    allocate(dvdxijk(sx-1:ex+1,sy-1:ey+1,sz-1:ez+1))
    if (dim==3) allocate(dvdzijk(sx-1:ex+1,sy-1:ey+1,sz-1:ez+1))

    if (dim==3) then 
       allocate(dwdx(sx-1:ex+1,sy-1:ey+1,sz-1:ez+1))
       allocate(dwdy(sx-1:ex+1,sy-1:ey+1,sz-1:ez+1))
       allocate(dwdz(sx-1:ex+1,sy-1:ey+1,sz-1:ez+1))
       allocate(dwdxijk(sx-1:ex+1,sy-1:ey+1,sz-1:ez+1))
       allocate(dwdyijk(sx-1:ex+1,sy-1:ey+1,sz-1:ez+1))
    end if

    call deriv_xyz(u,v,w,   &
         & dudx,dudy,dudz,  &
         & dvdx,dvdy,dvdz,  &
         & dwdx,dwdy,dwdz,  &
         & dudyijk,dudzijk, &
         & dvdxijk,dvdzijk, &
         & dwdxijk,dwdyijk)

    SijdSjid=0

    do k=sz,ez
       do j=sy,ey
          do i=sx,ex
             
             !-------------------------------------
             ! tensor D at pressure nodes (tensD)
             !
             !           d u_i     d u_j
             ! 2 D_ij = ------- + -------
             !           d x_j     d x_i
             !
             !-------------------------------------
             
             tensD=0
             tensO=0

             tensD(1,1)=dudx(i,j,k)
             tensD(1,2)=d1p2*(dudyijk(i,j,k)+dvdxijk(i,j,k))
             tensD(2,1)=tensD(1,2)
             tensD(2,2)=dvdy(i,j,k)

              if (dim==3) then 
                tensD(1,3)=d1p2*(dudzijk(i,j,k)+dwdxijk(i,j,k))
                tensD(2,3)=d1p2*(dvdzijk(i,j,k)+dwdyijk(i,j,k))
                tensD(3,1)=tensD(1,3)
                tensD(3,2)=tensD(2,3)
                tensD(3,3)=dwdz(i,j,k)
             end if
             
             !-------------------------------------
             ! tensor Omega at pressure nodes (tensO)
             !
             !               d u_i       d u_j
             ! 2 Omega_ij = -------  -  -------
             !               d x_j       d x_i
             !
             !-------------------------------------
             
             tensO=0

             tensO(1,2)=d1p2*(dudyijk(i,j,k)-dvdxijk(i,j,k))
             tensO(2,1)=-tensO(1,2)

             if (dim==3) then 
                tensO(1,3)=d1p2*(dudzijk(i,j,k)-dwdxijk(i,j,k))
                tensO(2,3)=d1p2*(dvdzijk(i,j,k)-dwdyijk(i,j,k))
                tensO(3,1)=-tensO(1,3)
                tensO(3,2)=-tensO(2,3)
             end if

             Sijd=0
             do j1=1,dim
                do i1=1,dim
                   do k1=1,dim
                      Sijd(i1,j1)=Sijd(i1,j1) &
                           + tensD(i1,k1)*tensD(k1,j1)+tensO(i1,k1)*tensO(k1,j1)
                      if (i1==j1) then
                         do m1=1,dim
                            do n1=1,dim
                               Sijd(i1,j1)=Sijd(i1,j1) &
                                    - d1p3*(tensD(m1,n1)*tensD(m1,n1)-tensO(m1,n1)*tensO(m1,n1))
                            end do
                         end do
                      end if
                   end do
                end do
             end do

             !-------------------------------------
             ! Sijd=Sjid ==> SijdSijd = SijdSjid
             !-------------------------------------

             do j1=1,dim
                do i1=1,dim
                   SijdSjid(i,j,k)=SijdSjid(i,j,k)+Sijd(i1,j1)*Sijd(j1,i1)
                end do
             end do
             
          end do
       end do
    end do
    
  end subroutine Compute_SijdSjid
  
end MODULE Mod_deriv_xyz
