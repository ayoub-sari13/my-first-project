!========================================================================
!
!                   FUGU Version 3.0.0
!
!========================================================================
!
! Copyright  Stéphane Vincent
!
! stephane.vincent@u-pem.fr          straightparadigm@gmail.com
!
! This file is part of the FUGU Fortran library, a set of Computational 
! Fluid Dynamics subroutines devoted to the simulation of multi-scale
! multi-phase flows for resolved scale interfaces (liquid/gas/solid).
!
!========================================================================
!**
!**   NAME       : Phase_field.f90
!**
!**   AUTHOR     : Stéphane Vincent
!**                Benoit Trouette
!**                Can Selcuk
!**
!**   FUNCTION   : Modules of variables of the phase field implementation
!**
!**   DATES      : Version 3.0.0  : from : january, 12, 2023
!**
!========================================================================

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#include "precomp.h"
module mod_phase_field
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  use mod_Parameters
  use mod_Constants
  use mod_math
  use mod_operators
  use mod_mpi
  use mod_struct_thermophysics
  use mod_struct_interface_tracking
  use mod_Init_EQ
  implicit none

contains

  subroutine initial_phase_field_setup(u, v, tp, curvature, norm_grad, g_of_phi, phase_field)
    implicit none
    real(8), dimension(:,:,:), allocatable, intent(inout) :: u, v, tp, curvature, norm_grad, g_of_phi
    type(phase_field_t), intent(inout) :: phase_field
    
    ! allocate needed fields for the phase field to work 
    allocate(curvature(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
    allocate(norm_grad(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
    allocate(g_of_phi(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))

    ! do some preliminary tests
    if (euler == 1) then
       print*,'Warning all test cases are not coded for euler =1, switch to euler = 2'
       stop
    end if

    if (EN_activate .eqv. .false.) then
       print*,'phase field implementation uses (fow now) the energy equation. Activate it to continue'
       stop
    endif


    
    ! Now depending on the test cases id, initialize variables and phase field's structure accordingly

    ! 2D stationnary solution in a disk
    ! use the PF_2D_stationnary.in file
    if (PF_init_case%id == 1) then
       PF_init_case%radius = 20
       PF_init_case%center(1) = 0
       PF_init_case%center(2) = 0

       ! for this case, the "width" of the hyperbolic tangent PF_W is set to 1
       phase_field%W = 1
       phase_field%W_sq = phase_field%W**2
       
       ! for this case the relaxation parameter b is set via the data.in too
       phase_field%b = PF_bprime
       fluids(1)%lambda = phase_field%b
       phase_field%init_val = 1.

       ! init initial phase field
       call init_circle_const (PF_init_case%center(1), PF_init_case%center(2), PF_init_case%radius, &
            & tp, phase_field%init_val, 1)

       ! print some user warnings
       print*, 'W/dx = ', phase_field%W/dx(sx)
       print*,'W should be less than R/4.2', PF_init_case%radius/4.2d0
       print*,'Be sure that Xmax = 60, Xmin = -60, Ymax = 60, Ymin = -60'

    end if

    
    ! Test case 4 of Sun et al 2007 jcp: Interface motion with a constant normal speed of 1
    ! use the PF_normal_motion.in file
    if (PF_init_case%id == 2) then
       PF_interface_normal_velo = .true.
       phase_field%a_velo = 1.
       
       phase_field%W = PF_W_delta_ratio*dx(sx)
       phase_field%W_sq = phase_field%W**2

       fluids(1)%lambda = PF_bprime*phase_field%W*phase_field%a_velo
       phase_field%init_val = 1

       ! init initial phase fiel
       call init_wavy_cosine(tp, phase_field%init_val, 1)
       
    endif

    ! Test case 6 of Sun et al 2007 jcp: Interface motion due to external flow fields
    ! use the PF_general_motion.in file
    if (PF_init_case%id == 3) then
       PF_general_advection = .true.
       
       PF_init_case%radius = 0.15
       PF_init_case%center(1) = xmin+0.5d0*(xmax-xmin)
       PF_init_case%center(2) = ymin+0.75d0*(ymax-ymin)    
             
       phase_field%W = 1.001*PF_W_delta_ratio*dx(sx)
       phase_field%W_sq = phase_field%W**2
       
       phase_field%a_velo = max(maxval(abs(u)),maxval(abs(v)))
       phase_field%b = PF_bprime*phase_field%W*phase_field%a_velo
       fluids(1)%lambda = phase_field%b 

       phase_field%init_val = 1
       
       ! init initial phase field
       call init_circle_const(PF_init_case%center(1), PF_init_case%center(2), PF_init_case%radius, tp, phase_field%init_val,1)
    end if

  end subroutine initial_phase_field_setup

  subroutine curvature_2D_can (phi, curvature)
    !*******************************************************************************
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable,intent(in) :: phi
    real(8), dimension(:,:,:), allocatable,intent(inout):: curvature
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    integer                                           :: i,j,k
    real(8)                                           :: num1, num2, num3, num4
    real(8)                                           :: denom1, denom2, denom3, denom4
    real(8)                                           :: un_sur_seize,eps

    curvature = 0
    eps = 1.e-13
    un_sur_seize = 1./16.
    k = 1
 
    do i = sx, ex
       do j = sy, ey
          num1 = phi(i+1,j,k) - phi(i,j,k);
          denom1 = sqrt((phi(i+1,j,k) - phi(i,j,k))**2 + un_sur_seize*(phi(i+1,j+1,k) + phi(i,j+1,k) - phi(i+1,j-1,k) - phi(i,j-1,k))**2);

          num2 = phi(i,j,k) - phi(i-1,j,k);
          denom2 = sqrt((phi(i,j,k) - phi(i-1,j,k))**2 + un_sur_seize*(phi(i-1,j+1,k) + phi(i,j+1,k) - phi(i-1,j-1,k) - phi(i,j-1,k))**2);

          num3 = phi(i,j+1,k) - phi(i,j,k);
          denom3 = sqrt(un_sur_seize*(phi(i+1,j+1,k) + phi(i+1,j,k) - phi(i-1,j+1,k) - phi(i-1,j,k))**2 + (phi(i,j+1,k) - phi(i,j,k))**2);

          num4 = phi(i,j,k) - phi(i,j-1,k);
          denom4 = sqrt(un_sur_seize*(phi(i+1,j-1,k) + phi(i+1,j,k) - phi(i-1,j-1,k) - phi(i-1,j,k))**2 + (phi(i,j,k) - phi(i,j-1,k))**2);

          if (num1 < eps) then
             denom1 = denom1 + eps;
          end if

          if (num2 < eps) then
             denom2 = denom2 + eps;
          end if

          if (num3 < eps) then
             denom3 = denom3 + eps;
          end if

          if (num4 < eps) then
             denom4 = denom4 + eps;
          end if

          curvature(i,j,k) =  num1/denom1 - num2/denom2 + num3/denom3 - num4/denom4;
          curvature(i,j,k) = curvature(i,j,k)*ddx(i);
       end do
    end do
    if (nproc>1) call comm_mpi_sca(curvature)
  end subroutine curvature_2D_can

  subroutine curvature_2D_a_la_tension (phi, curvature)
    !-------------------------------------------------------------------------------
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable, intent(in)    :: phi
    real(8), dimension(:,:,:), allocatable, intent(inout) :: curvature
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                               :: i,j,k
    real(8)                                               :: kappa,norm,curv,maxi
    real(8), dimension(:,:,:), allocatable                :: kap
    real(8), dimension(:,:,:), allocatable                :: noru,norv,norw
    real(8), dimension(:,:,:), allocatable                :: noruu,norvu,norwu
    logical, save                                         :: once = .true.

    allocate(noru(sxu-gx:exu+gx,syu-gy:eyu+gy,szu-gz:ezu+gz),  &
               & norv(sxv-gx:exv+gx,syv-gy:eyv+gy,szv-gz:ezv+gz),  &
               & noruu(sxu-gx:exu+gx,syu-gy:eyu+gy,szu-gz:ezu+gz), &
               & norvu(sxv-gx:exv+gx,syv-gy:eyv+gy,szv-gz:ezv+gz))

    call normal (phi,noru,norv,norw,noruu,norvu,norwu)
    call diverge_vec_2D (curvature,norm,maxi,noruu,norvu)

    if (nproc>1) call comm_mpi_sca(curvature)

     deallocate(noru, norv, noruu, norvu)
    
  end subroutine curvature_2D_a_la_tension
  
  subroutine norm_grad_phi_2D_can (phi2D, norm_gradd)
    !*******************************************************************************
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable,intent(in) :: phi2D
    real(8), dimension(:,:,:), allocatable,intent(inout):: norm_gradd
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    integer                                           :: i,j,k

    norm_gradd = 0
    k = 1
    do i = sx,ex
       do j = sy,ey
          norm_gradd(i,j,k) = sqrt(0.25*(ddx(i)**2)*(phi2D(i+1,j,k) - phi2D(i-1,j,k))**2 + 0.25*(ddy(j)**2)*(phi2D(i,j+1,k) - phi2D(i,j-1,k))**2);
       end do
    end do


  end subroutine norm_grad_phi_2D_can

  subroutine compute_g_of_phi (phi, W, g)
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable,intent(in) :: phi
    real(8), dimension(:,:,:), allocatable,intent(inout):: g
    real(8), intent(in) :: W
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    integer                                           :: i,j,k
    real(8) :: alpha
    g = 0
    
    k = 1
    do i = sx,ex
           alpha = tanh(dx(i)/W)
       do j = sy,ey
          g(i,j,k) = -4 + 8*phi(i,j,k) + 4*(alpha**2 - 1)*(2*phi(i,j,k) - 1)/(1 - alpha**2*(2*phi(i,j,k) -1)**2)
          g(i,j,k) = ddx(i)**2*g(i,j,k)/8
       end do
    end do
  end subroutine compute_g_of_phi
   
  subroutine PF_Delta(phase_field, smtp, tp, tp0, curvature, norm_grad, u, v)
    real(8), dimension(:,:,:), allocatable, intent(inout) :: smtp, tp, tp0, curvature, norm_grad, u, v
    type(phase_field_t), intent(inout) :: phase_field
    
    ! update b for cases with Interfacial motion due to external flow fields
    if (PF_general_advection) then
       phase_field%a_velo = max(maxval(abs(u)),maxval(abs(v)))
       fluids(1)%lambda = PF_bprime*phase_field%W*phase_field%a_velo
    end if

    ! add nonlinear term as source term now, coefficient b is set via fluids(1)%lambda
    ! We use euler == 2 and use adams bashforth extrapolation of nonlinear terms NL^{n+1} = 2NL^{n} - NL^{n-1}
    ! at this point tp is tp0 and tp0 is tp1. tp1 is not used actually
    smtp(:,:,:) = fluids(1)%lambda*(2.d0*tp(:,:,:)*(1.d0 - tp(:,:,:)**2) - tp0(:,:,:)*(1.d0 - tp0(:,:,:)**2))/phase_field%W_sq

    if (PF_interface_normal_velo) then
       call norm_grad_phi_2D_can(tp, norm_grad)
       smtp(:,:,:) = smtp(:,:,:) - 2.d0*phase_field%a_velo*norm_grad

       call norm_grad_phi_2D_can(tp0, norm_grad)
       smtp(:,:,:) = smtp(:,:,:) + phase_field%a_velo*norm_grad
    endif

    call curvature_2D_can(tp, curvature)
    call norm_grad_phi_2D_can(tp, norm_grad)
    smtp(:,:,:) = smtp(:,:,:) - 2.d0*(fluids(1)%lambda)*norm_grad(:,:,:)*curvature(:,:,:)

    call curvature_2D_can(tp0, curvature)
    call norm_grad_phi_2D_can(tp0, norm_grad)
    smtp(:,:,:) = smtp(:,:,:) + (fluids(1)%lambda)*norm_grad(:,:,:)*curvature(:,:,:)

    !feed now smtp to the energy equation

  end subroutine PF_Delta

end module mod_phase_field
