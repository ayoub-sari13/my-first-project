!========================================================================
!
!                   FUGU Version 1.0.0
!
!========================================================================
!
! Copyright  Stéphane Vincent
!
! stephane.vincent@u-pem.fr          straightparadigm@gmail.com
!
! This file is part of the FUGU Fortran library, a set of Computational 
! Fluid Dynamics subroutines devoted to the simulation of multi-scale
! multi-phase flows for resolved scale interfaces (liquid/gas/solid). 
!
!========================================================================
!**
!**   NAME       : Navier-Stokes.f90
!**
!**   AUTHOR     : Stéphane Vincent
!**
!**   FUNCTION   : Navier-Stokes modules of FUGU software
!**
!**   DATES      : Version 1.0.0  : from : feb, 19, 2017
!**
!========================================================================

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#include "precomp.h"
module mod_Navier
  use mod_timers
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

contains


  subroutine Navier_Stokes_Matrix(mat,u,v,w,vie,vis,vir,rep,rho,rovu,rovv,rovw, &
       xit,slvu,slvv,slvw,penu,penv,penw,thetau,thetav,thetaw,ns)
    !*****************************************************************************************************
    !*****************************************************************************************************
    !*****************************************************************************************************
    !*****************************************************************************************************
    !-------------------------------------------------------------------------------
    ! Finite volume discretization of Navier-Stokes equations in advective form
    ! Pressure-velocity coupling with augmented Lagrangian method
    !-------------------------------------------------------------------------------
    use mod_Parameters, only: dim
    use mod_struct_solver
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:),   allocatable, intent(inout) :: u,v,w,rep
    real(8), dimension(:,:,:),   allocatable, intent(in)    :: vie,rho,rovu,rovv,rovw
    real(8), dimension(:,:,:,:), allocatable, intent(in)    :: vis,vir
    real(8), dimension(:,:,:),   allocatable, intent(in)    :: xit,slvu,slvv,slvw,thetau,thetav,thetaw
    real(8), dimension(:,:,:,:), allocatable, intent(in)    :: penu,penv,penw
    type(matrix_t),                           intent(inout) :: mat 
    type(solver_ns_t),                        intent(inout) :: ns
    !-------------------------------------------------------------------------------

    if (dim==2) then 
       call Navier_Stokes_Matrix_2D (mat%coef,mat%jcof,mat%icof,u,v,vie,vis,vir,rep,rho,rovu,rovv, &
            & xit,slvu,slvv,penu,penv,thetau,thetav,mat%kdv,mat%nps,ns)   
    else
       call Navier_Stokes_Matrix_3D (mat%coef,mat%jcof,mat%icof,u,v,w,vie,vis,vir,rep,rho,rovu,rovv,rovw, &
            xit,slvu,slvv,slvw,penu,penv,penw,thetau,thetav,thetaw,mat%kdv,mat%nps,ns)
    end if
    
  end subroutine Navier_Stokes_Matrix
  
  !*****************************************************************************************************
  !*****************************************************************************************************
  !*****************************************************************************************************
  !*****************************************************************************************************
  subroutine Navier_Stokes_Matrix_2D (coef,jcof,icof,u,v,vie,vis,vir,rep,rho,rovu,rovv, &
       xit,slvu,slvv,penu,penv,thetau,thetav,nb_coef_matrix,nb_matrix_element,ns)
    !*****************************************************************************************************
    !*****************************************************************************************************
    !*****************************************************************************************************
    !*****************************************************************************************************
    !-------------------------------------------------------------------------------
    ! Finite volume discretization of Navier-Stokes equations in advective form
    ! Pressure-velocity coupling with augmented Lagrangian method
    !-------------------------------------------------------------------------------
    use mod_Parameters, only: nproc,dim,coef_time_1,    &
         & dx,dy,dz,ddx,ddy,ddz,                        &
         & dxu,dyv,dzw,dxu2,dyv2,dzw2,ddxu,ddyv,ddzw,   &
         & sx,ex,sy,ey,sz,ez,                           &
         & gsx,gex,gsy,gey,gsz,gez,                     &
         & sxu,exu,syu,eyu,szu,ezu,                     &
         & sxv,exv,syv,eyv,szv,ezv,                     &
         & sxw,exw,syw,eyw,szw,ezw,                     &
         & sxs,exs,sys,eys,szs,ezs,                     &
         & sxus,exus,syus,eyus,szus,ezus,               &
         & sxvs,exvs,syvs,eyvs,szvs,ezvs,               &
         & sxws,exws,syws,eyws,szws,ezws,               &
         & gsxu,gexu,gsyu,geyu,gszu,gezu,               &
         & gsxv,gexv,gsyv,geyv,gszv,gezv,               &
         & gsxw,gexw,gsyw,geyw,gszw,gezw,               &
         & gx,gy,gz,                                    &
         & NS_method,                                   &
         & NS_linear_term,NS_inertial_scheme,PJ_method, &
         & AL_comp,AL_method,AL_dr,                     &
         & NS_compressible
    use mod_Constants, only: zero,one,d1p2
    use mod_struct_solver
    use mod_mpi
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)                                     :: nb_coef_matrix,nb_matrix_element
    integer, dimension(:),       allocatable, intent(in)    :: jcof,icof
    real(8), dimension(:),       allocatable, intent(inout) :: coef
    real(8), dimension(:,:,:),   allocatable, intent(inout) :: u,v,rep
    real(8), dimension(:,:,:),   allocatable, intent(in)    :: vie,rho,rovu,rovv
    real(8), dimension(:,:,:,:), allocatable, intent(in)    :: vis,vir
    real(8), dimension(:,:,:),   allocatable, intent(in)    :: xit,slvu,slvv,thetau,thetav
    real(8), dimension(:,:,:,:), allocatable, intent(in)    :: penu,penv
    type(solver_ns_t), intent(inout)                        :: ns
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    real(8)                                                 :: difb,diff
    real(8)                                                 :: inerb,inerf
    real(8)                                                 :: inerbb,inerff,inerbf,inerfb
    real(8), dimension(:,:,:),   allocatable                :: repu,repv,repw
    integer                                                 :: i,j,lvs
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Compressible augmented Lagrangian method
    !-------------------------------------------------------------------------------
    if (AL_comp.and.PJ_method==0) rep = ns%dt/xit
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Algebraic Augmented Lagrangian method
    !-------------------------------------------------------------------------------
    if (AL_method==3) then
       allocate(repu(sxu-gx:exu+gx,syu-gy:eyu+gy,szu-gz:ezu+gz))
       allocate(repv(sxv-gx:exv+gx,syv-gy:eyv+gy,szv-gz:ezv+gz))
       repu=0
       repv=0
    end if
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Matrix initialization
    !-------------------------------------------------------------------------------
    do lvs = 1,nb_matrix_element
       coef(lvs) = 0
    end do
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Finite volume discretization
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------
    ! U velocity component
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------
    lvs=1
    do j=syus,eyus
       do i=sxus,exus

          if (nproc>1.and.(i<sxu.or.i>exu.or.j<syu.or.j>eyu)) then
             !-------------------------------------------------------------------------------
             ! Penalty term for MPI exchange cells
             !-------------------------------------------------------------------------------
             coef(lvs)=1!;coef(lvs+1:lvs+8)=0
          else 
             !-------------------------------------------------------------------------------
             ! Time integration scheme
             !-------------------------------------------------------------------------------
             coef(lvs) = rovu(i,j,1) * coef_time_1 * dxu(i) * dy(j)
             !-------------------------------------------------------------------------------
             ! Elongational viscosity
             !-------------------------------------------------------------------------------
             difb= - vie(i-1,j,1)/dx(i-1) * dy(j) ! backward contribution
             diff=   vie(i  ,j,1)/dx(i  ) * dy(j) ! forward contribution
             coef(lvs  )= coef(lvs)   - (difb  - diff)
             coef(lvs+1)= coef(lvs+1) + difb
             coef(lvs+2)= coef(lvs+2) - diff
             !-------------------------------------------------------------------------------
             ! Shearing viscosity
             !-------------------------------------------------------------------------------
             difb= - vis(i,j  ,1,1)/dyv(j  ) * dxu(i) ! backward contribution
             diff=   vis(i,j+1,1,1)/dyv(j+1) * dxu(i) ! forward contribution
             coef(lvs  )= coef(lvs)   - (difb - diff)
             coef(lvs+3)= coef(lvs+3) + difb
             coef(lvs+4)= coef(lvs+4) - diff
             !-------------------------------------------------------------------------------
             ! Rotation viscosity
             !-------------------------------------------------------------------------------
             difb=   vir(i,j  ,1,1)/dyv(j  ) * dxu(i) ! backward contribution
             diff= - vir(i,j+1,1,1)/dyv(j+1) * dxu(i) ! forward contribution
             coef(lvs  )= coef(lvs)   - (difb  - diff)
             coef(lvs+3)= coef(lvs+3) + difb
             coef(lvs+4)= coef(lvs+4) - diff
             coef(lvs+5)= coef(lvs+5) - vir(i,j  ,1,1)/dxu(i) * dxu(i)
             coef(lvs+6)= coef(lvs+6) + vir(i,j  ,1,1)/dxu(i) * dxu(i)
             coef(lvs+7)= coef(lvs+7) + vir(i,j+1,1,1)/dxu(i) * dxu(i)
             coef(lvs+8)= coef(lvs+8) - vir(i,j+1,1,1)/dxu(i) * dxu(i)
             !-------------------------------------------------------------------------------
             ! Inertial terms
             !-------------------------------------------------------------------------------
             select case(NS_inertial_scheme)
             case(-10,-11)
             case(0) ! stokes flow
             case(1,10) ! centered scheme with density at velocity node (non conservative for two phase flows)
                inerbb= - rovu(i,j,1) * (dxu2(i-1)*u(i  ,j,1)+dxu2(i  )*u(i-1,j,1))/dx(i-1) & ! backward-backward
                     & * dxu2(i  )/dx(i-1) * dy(j)
                inerbf= - rovu(i,j,1) * (dxu2(i-1)*u(i  ,j,1)+dxu2(i  )*u(i-1,j,1))/dx(i-1) & ! backward-forward
                     & * dxu2(i-1)/dx(i-1) * dy(j)
                inerff=   rovu(i,j,1) * (dxu2(i  )*u(i+1,j,1)+dxu2(i+1)*u(i  ,j,1))/dx(i  ) & ! forward-forward
                     & * dxu2(i  )/dx(i  ) * dy(j)
                inerfb=   rovu(i,j,1) * (dxu2(i  )*u(i+1,j,1)+dxu2(i+1)*u(i  ,j,1))/dx(i  ) & ! forward-backward
                     & * dxu2(i+1)/dx(i  ) * dy(j)
                coef(lvs  )= coef(lvs  ) + inerbf + inerfb
                coef(lvs+1)= coef(lvs+1) + inerbb
                coef(lvs+2)= coef(lvs+2) + inerff
                inerb= - rovu(i,j,1) * (v(i-1,j  ,1)+v(i,j  ,1))/2 * d1p2 * dxu(i) ! backward contribution
                inerf=   rovu(i,j,1) * (v(i-1,j+1,1)+v(i,j+1,1))/2 * d1p2 * dxu(i) ! forward contribution
                coef(lvs  )= coef(lvs  ) + inerb + inerf
                coef(lvs+3)= coef(lvs+3) + inerb
                coef(lvs+4)= coef(lvs+4) + inerf
             case(2) ! upwind
                inerb= - rovu(i,j,1)*(dxu2(i-1)*u(i,j,1)+dxu2(i)*u(i-1,j,1))/dx(i-1) * dy(j) ! backward contribution
                inerf=   rovu(i,j,1)*(dxu2(i+1)*u(i,j,1)+dxu2(i)*u(i+1,j,1))/dx(i  ) * dy(j) ! forward contribution
                coef(lvs  )= coef(lvs  ) + (max( inerb,zero)+max(inerf,zero))
                coef(lvs+1)= coef(lvs+1) -  max(-inerb,zero)
                coef(lvs+2)= coef(lvs+2) -  max(-inerf,zero)
                inerb= - rovu(i,j,1)*(v(i,j  ,1)+v(i-1,j  ,1))/2 * dxu(i) ! backward contribution
                inerf=   rovu(i,j,1)*(v(i,j+1,1)+v(i-1,j+1,1))/2 * dxu(i) ! forward contribution
                coef(lvs  )= coef(lvs  ) + (max( inerb,zero)+max(inerf,zero))
                coef(lvs+3)= coef(lvs+3) -  max(-inerb,zero)
                coef(lvs+4)= coef(lvs+4) -  max(-inerf,zero)
             case(3)
                inerbb= - rovu(i,j,1) * (dxu2(i-1)*u(i  ,j,1)+dxu2(i  )*u(i-1,j,1))/dx(i-1) & ! backward-backward
                     & * dxu2(i  )/dx(i-1) * dy(j)
                inerbf= - rovu(i,j,1) * (dxu2(i-1)*u(i  ,j,1)+dxu2(i  )*u(i-1,j,1))/dx(i-1) & ! backward-forward
                     & * dxu2(i-1)/dx(i-1) * dy(j)
                inerff=   rovu(i,j,1) * (dxu2(i  )*u(i+1,j,1)+dxu2(i+1)*u(i  ,j,1))/dx(i  ) & ! forward-forward
                     & * dxu2(i  )/dx(i  ) * dy(j)
                inerfb=   rovu(i,j,1) * (dxu2(i  )*u(i+1,j,1)+dxu2(i+1)*u(i  ,j,1))/dx(i  ) & ! forward-backward
                     & * dxu2(i+1)/dx(i  ) * dy(j)
                coef(lvs  )= coef(lvs  ) + thetau(i,j,1)*(inerbf + inerfb)
                coef(lvs+1)= coef(lvs+1) + thetau(i,j,1)* inerbb
                coef(lvs+2)= coef(lvs+2) + thetau(i,j,1)* inerff
                inerb= - rovu(i,j,1) * (v(i-1,j  ,1)+v(i,j  ,1))/2 * d1p2 * dxu(i) ! backward contribution
                inerf=   rovu(i,j,1) * (v(i-1,j+1,1)+v(i,j+1,1))/2 * d1p2 * dxu(i) ! forward contribution
                coef(lvs  )= coef(lvs  ) + thetau(i,j,1)*(inerb + inerf)
                coef(lvs+3)= coef(lvs+3) + thetau(i,j,1)* inerb
                coef(lvs+4)= coef(lvs+4) + thetau(i,j,1)* inerf

                inerb= - rovu(i,j,1)*(dxu2(i-1)*u(i,j,1)+dxu2(i)*u(i-1,j,1))/dx(i-1) * dy(j) ! backward contribution
                inerf=   rovu(i,j,1)*(dxu2(i+1)*u(i,j,1)+dxu2(i)*u(i+1,j,1))/dx(i  ) * dy(j) ! forward contribution
                coef(lvs  )= coef(lvs  ) + (1-thetau(i,j,1))*(max( inerb,zero)+max(inerf,zero))
                coef(lvs+1)= coef(lvs+1) - (1-thetau(i,j,1))* max(-inerb,zero)
                coef(lvs+2)= coef(lvs+2) - (1-thetau(i,j,1))* max(-inerf,zero)
                inerb= - rovu(i,j,1)*(v(i,j  ,1)+v(i-1,j  ,1))/2 * dxu(i) ! backward contribution
                inerf=   rovu(i,j,1)*(v(i,j+1,1)+v(i-1,j+1,1))/2 * dxu(i) ! forward contribution
                coef(lvs  )= coef(lvs  ) + (1-thetau(i,j,1))*(max( inerb,zero)+max(inerf,zero))
                coef(lvs+3)= coef(lvs+3) - (1-thetau(i,j,1))* max(-inerb,zero)
                coef(lvs+4)= coef(lvs+4) - (1-thetau(i,j,1))* max(-inerf,zero)
             case DEFAULT
                write(*,*) 'Navier-Stokes inertial scheme does not exist'
                stop
             end select
             !-------------------------------------------------------------------------------
             ! Coupled terms
             !-------------------------------------------------------------------------------
             if (NS_method==1) then
                !-------------------------------------------------------------------------------
                !  Pressure gradient
                !-------------------------------------------------------------------------------
                coef(lvs+9) = coef(lvs+9)  - ddxu(i) * dxu(i) * dy(j)
                coef(lvs+10)= coef(lvs+10) + ddxu(i) * dxu(i) * dy(j)
                !-------------------------------------------------------------------------------
             end if
             !-------------------------------------------------------------------------------
             ! Augmented Lagrangian
             !-------------------------------------------------------------------------------
             if (AL_method<3) then
                coef(lvs  )= coef(lvs)   + rep(i  ,j,1) * ddx(i)   * dy(j)
                coef(lvs  )= coef(lvs)   + rep(i-1,j,1) * ddx(i-1) * dy(j)
                coef(lvs+1)= coef(lvs+1) - rep(i-1,j,1) * ddx(i-1) * dy(j)
                coef(lvs+2)= coef(lvs+2) - rep(i  ,j,1) * ddx(i)   * dy(j)
                coef(lvs+5)= coef(lvs+5) - rep(i-1,j,1) * ddy(j)   * dy(j)
                coef(lvs+6)= coef(lvs+6) + rep(i  ,j,1) * ddy(j)   * dy(j)
                coef(lvs+7)= coef(lvs+7) + rep(i-1,j,1) * ddy(j)   * dy(j)
                coef(lvs+8)= coef(lvs+8) - rep(i  ,j,1) * ddy(j)   * dy(j)
             else
                repu(i,j,1)=max(                                    &
                     & maxval(abs(coef(lvs:lvs+4)*dx(i)*ddy(j))),   &
                     & maxval(abs(coef(lvs+5:lvs+8)*dy(j)*ddy(j))) )
             end if
             !-------------------------------------------------------------------------------
             ! Linear/Brinkman term
             !-------------------------------------------------------------------------------
             if (NS_linear_term) coef(lvs) = coef(lvs) + slvu(i,j,1) * dxu(i) * dy(j)
             !-------------------------------------------------------------------------------
             ! Penalty term for boundary conditions and immersed objects
             ! penu (i,j,k,l)
             ! l=1 central U component
             ! l=2 left U component
             ! l=3 right U component
             ! l=4 bottom U component
             ! l=5 top U component
             !-------------------------------------------------------------------------------
             coef(lvs)   = coef(lvs)   + penu(i,j,1,1) * dxu(i) * dy(j)
             coef(lvs+1) = coef(lvs+1) + penu(i,j,1,2) * dxu(i) * dy(j)
             coef(lvs+2) = coef(lvs+2) + penu(i,j,1,3) * dxu(i) * dy(j)
             coef(lvs+3) = coef(lvs+3) + penu(i,j,1,4) * dxu(i) * dy(j)
             coef(lvs+4) = coef(lvs+4) + penu(i,j,1,5) * dxu(i) * dy(j)
             !-------------------------------------------------------------------------------
             ! End of discretization for U
             !-------------------------------------------------------------------------------
          end if

          !-------------------------------------------------------------------------------
          ! next line of the matrix
          !-------------------------------------------------------------------------------
          lvs=lvs+nb_coef_matrix 
       end do
    end do
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------
    ! V velocity component
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------
    do j=syvs,eyvs
       do i=sxvs,exvs

          if (nproc>1.and.(i<sxv.or.i>exv.or.j<syv.or.j>eyv)) then
             !-------------------------------------------------------------------------------
             ! Penalty term for MPI exchange cells
             !-------------------------------------------------------------------------------
             coef(lvs)=1!;coef(lvs+1:lvs+8)=0
          else 
             !-------------------------------------------------------------------------------
             ! Time integration scheme
             !-------------------------------------------------------------------------------
             coef(lvs) = rovv(i,j,1) * coef_time_1 * dx(i) * dyv(j)
             !-------------------------------------------------------------------------------
             ! Elongational viscosity
             !-------------------------------------------------------------------------------
             difb= - vie(i,j-1,1)/dy(j-1) * dx(i) ! backward contribution
             diff=   vie(i,j  ,1)/dy(j  ) * dx(i) ! forward contribution
             coef(lvs  )= coef(lvs)   - (difb  - diff)
             coef(lvs+3)= coef(lvs+3) + difb
             coef(lvs+4)= coef(lvs+4) - diff
             !-------------------------------------------------------------------------------
             ! Shearing viscosity
             !-------------------------------------------------------------------------------
             difb= - vis(i  ,j,1,1)/dxu(i  ) * dyv(j) ! backward contribution
             diff=   vis(i+1,j,1,1)/dxu(i+1) * dyv(j) ! forward contribution
             coef(lvs  )= coef(lvs)   - (difb - diff)
             coef(lvs+1)= coef(lvs+1) + difb
             coef(lvs+2)= coef(lvs+2) - diff
             !-------------------------------------------------------------------------------
             ! Rotation viscosity
             !-------------------------------------------------------------------------------
             difb=   vir(i  ,j,1,1)/dxu(i  ) * dyv(j) ! backward contribution
             diff= - vir(i+1,j,1,1)/dxu(i+1) * dyv(j) ! forward contribution
             coef(lvs  )= coef(lvs)   - (difb - diff)
             coef(lvs+1)= coef(lvs+1) + difb
             coef(lvs+2)= coef(lvs+2) - diff
             coef(lvs+5)= coef(lvs+5) - vir(i  ,j,1,1)/dyv(j) * dyv(j)
             coef(lvs+6)= coef(lvs+6) + vir(i+1,j,1,1)/dyv(j) * dyv(j)
             coef(lvs+7)= coef(lvs+7) + vir(i  ,j,1,1)/dyv(j) * dyv(j)
             coef(lvs+8)= coef(lvs+8) - vir(i+1,j,1,1)/dyv(j) * dyv(j)
             !-------------------------------------------------------------------------------
             ! Inertial terms
             !-------------------------------------------------------------------------------
             select case(NS_inertial_scheme)
             case(-10,-11)
             case(0) ! stokes flow
             case(1,10) ! centered scheme
                inerb= - rovv(i,j,1) * (u(i  ,j-1,1)+u(i  ,j,1))/2 * d1p2 * dyv(j) ! backward contribution
                inerf=   rovv(i,j,1) * (u(i+1,j-1,1)+u(i+1,j,1))/2 * d1p2 * dyv(j) ! forward contribution
                coef(lvs  )= coef(lvs  ) + inerb + inerf
                coef(lvs+1)= coef(lvs+1) + inerb
                coef(lvs+2)= coef(lvs+2) + inerf 
                inerbb= - rovv(i,j,1) * (dyv2(j-1)*v(i,j  ,1)+dyv2(j  )*v(i,j-1,1))/dy(j-1) &
                     & * dyv2(j  )/dy(j-1) * dx(i) ! backward-backward
                inerbf= - rovv(i,j,1) * (dyv2(j-1)*v(i,j  ,1)+dyv2(j  )*v(i,j-1,1))/dy(j-1) &
                     & * dyv2(j-1)/dy(j-1) * dx(i) ! backward-forward
                inerff=   rovv(i,j,1) * (dyv2(j  )*v(i,j+1,1)+dyv2(j+1)*v(i,j  ,1))/dy(j  ) &
                     & * dyv2(j  )/dy(j  ) * dx(i) ! forward-forward
                inerfb=   rovv(i,j,1) * (dyv2(j  )*v(i,j+1,1)+dyv2(j+1)*v(i,j  ,1))/dy(j  ) &
                     & * dyv2(j+1)/dy(j  ) * dx(i) ! forward-forward
                coef(lvs  )= coef(lvs  ) + inerbf + inerfb
                coef(lvs+3)= coef(lvs+3) + inerbb
                coef(lvs+4)= coef(lvs+4) + inerff
             case(2) ! upwind
                inerb= - rovv(i,j,1)*(u(i  ,j,1)+u(i  ,j-1,1))/2 * dyv(j) ! backward contribution
                inerf=   rovv(i,j,1)*(u(i+1,j,1)+u(i+1,j-1,1))/2 * dyv(j) ! forward contribution
                coef(lvs  )= coef(lvs  ) + (max( inerb,zero)+max(inerf,zero))
                coef(lvs+1)= coef(lvs+1) -  max(-inerb,zero)
                coef(lvs+2)= coef(lvs+2) -  max(-inerf,zero)
                inerb= - rovv(i,j,1)*(dyv2(j-1)*v(i,j,1)+dyv2(j)*v(i,j-1,1))/dy(j-1) * dx(i) ! backward contribution
                inerf=   rovv(i,j,1)*(dyv2(j+1)*v(i,j,1)+dyv2(j)*v(i,j+1,1))/dy(j  ) * dx(i) ! forward contribution
                coef(lvs  )= coef(lvs  ) + (max( inerb,zero)+max(inerf,zero))
                coef(lvs+3)= coef(lvs+3) -  max(-inerb,zero)
                coef(lvs+4)= coef(lvs+4) -  max(-inerf,zero)
             case(3)
                inerb= - rovv(i,j,1) * (u(i  ,j-1,1)+u(i  ,j,1))/2 * d1p2 * dyv(j) ! backward contribution
                inerf=   rovv(i,j,1) * (u(i+1,j-1,1)+u(i+1,j,1))/2 * d1p2 * dyv(j) ! forward contribution
                coef(lvs  )= coef(lvs  ) + thetav(i,j,1)*(inerb + inerf)
                coef(lvs+1)= coef(lvs+1) + thetav(i,j,1)* inerb
                coef(lvs+2)= coef(lvs+2) + thetav(i,j,1)* inerf 
                inerbb= - rovv(i,j,1) * (dyv2(j-1)*v(i,j  ,1)+dyv2(j  )*v(i,j-1,1))/dy(j-1) &
                     & * dyv2(j  )/dy(j-1) * dx(i) ! backward-backward
                inerbf= - rovv(i,j,1) * (dyv2(j-1)*v(i,j  ,1)+dyv2(j  )*v(i,j-1,1))/dy(j-1) &
                     & * dyv2(j-1)/dy(j-1) * dx(i) ! backward-forward
                inerff=   rovv(i,j,1) * (dyv2(j  )*v(i,j+1,1)+dyv2(j+1)*v(i,j  ,1))/dy(j  ) &
                     & * dyv2(j  )/dy(j  ) * dx(i) ! forward-forward
                inerfb=   rovv(i,j,1) * (dyv2(j  )*v(i,j+1,1)+dyv2(j+1)*v(i,j  ,1))/dy(j  ) &
                     & * dyv2(j+1)/dy(j  ) * dx(i) ! forward-forward
                coef(lvs  )= coef(lvs  ) + thetav(i,j,1)*(inerbf + inerfb)
                coef(lvs+3)= coef(lvs+3) + thetav(i,j,1)* inerbb
                coef(lvs+4)= coef(lvs+4) + thetav(i,j,1)* inerff

                inerb= - rovv(i,j,1)*(u(i  ,j,1)+u(i  ,j-1,1))/2 * dyv(j) ! backward contribution
                inerf=   rovv(i,j,1)*(u(i+1,j,1)+u(i+1,j-1,1))/2 * dyv(j) ! forward contribution
                coef(lvs  )= coef(lvs  ) + (1-thetav(i,j,1))*(max( inerb,zero)+max(inerf,zero))
                coef(lvs+1)= coef(lvs+1) - (1-thetav(i,j,1))* max(-inerb,zero)
                coef(lvs+2)= coef(lvs+2) - (1-thetav(i,j,1))* max(-inerf,zero)
                inerb= - rovv(i,j,1)*(dyv2(j-1)*v(i,j,1)+dyv2(j)*v(i,j-1,1))/dy(j-1) * dx(i) ! backward contribution
                inerf=   rovv(i,j,1)*(dyv2(j+1)*v(i,j,1)+dyv2(j)*v(i,j+1,1))/dy(j  ) * dx(i) ! forward contribution
                coef(lvs  )= coef(lvs  ) + (1-thetav(i,j,1))*(max( inerb,zero)+max(inerf,zero))
                coef(lvs+3)= coef(lvs+3) - (1-thetav(i,j,1))* max(-inerb,zero)
                coef(lvs+4)= coef(lvs+4) - (1-thetav(i,j,1))* max(-inerf,zero)
             case DEFAULT
                write(*,*) 'Navier-Stokes inertial scheme does not exist'
                stop
             end select
             !-------------------------------------------------------------------------------
             ! Coupled terms
             !-------------------------------------------------------------------------------
             if (NS_method==1) then
                !-------------------------------------------------------------------------------
                !  Pressure gradient
                !-------------------------------------------------------------------------------
                coef(lvs+9) = coef(lvs+9)  - ddyv(j) * dx(i) * dyv(j)
                coef(lvs+10)= coef(lvs+10) + ddyv(j) * dx(i) * dyv(j)
                !-------------------------------------------------------------------------------
             end if
             !-------------------------------------------------------------------------------
             ! Augmented Lagrangian
             !-------------------------------------------------------------------------------
             if (AL_method<3) then 
                coef(lvs  )= coef(lvs)   + rep(i,j  ,1) * ddy(j)   * dx(i)
                coef(lvs  )= coef(lvs)   + rep(i,j-1,1) * ddy(j-1) * dx(i)
                coef(lvs+3)= coef(lvs+3) - rep(i,j-1,1) * ddy(j-1) * dx(i)
                coef(lvs+4)= coef(lvs+4) - rep(i,j  ,1) * ddy(j)   * dx(i)
                coef(lvs+5)= coef(lvs+5) - rep(i,j-1,1) * ddx(i)   * dx(i)
                coef(lvs+6)= coef(lvs+6) + rep(i,j-1,1) * ddx(i)   * dx(i)
                coef(lvs+7)= coef(lvs+7) + rep(i,j  ,1) * ddx(i)   * dx(i)
                coef(lvs+8)= coef(lvs+8) - rep(i,j  ,1) * ddx(i)   * dx(i)
             else
                repv(i,j,1)=max(                                    &
                     & maxval(abs(coef(lvs:lvs+4)*dy(j)*ddx(i))),   & 
                     & maxval(abs(coef(lvs+5:lvs+8))*dx(i)*ddx(i)) )
             end if
             !-------------------------------------------------------------------------------
             ! Linear/Brinkman term
             !-------------------------------------------------------------------------------
             if (NS_linear_term) coef(lvs) = coef(lvs) + slvv(i,j,1) * dx(i) * dyv(j)
             !-------------------------------------------------------------------------------
             ! Penalty term for boundary conditions and immersed objects
             ! penv (i,j,k,l)
             ! l=1 central V component
             ! l=2 left V component
             ! l=3 right V component
             ! l=4 bottom V component
             ! l=5 top V component
             !-------------------------------------------------------------------------------
             coef(lvs)   = coef(lvs)   + penv(i,j,1,1) * dx(i) * dyv(j)
             coef(lvs+1) = coef(lvs+1) + penv(i,j,1,2) * dx(i) * dyv(j)
             coef(lvs+2) = coef(lvs+2) + penv(i,j,1,3) * dx(i) * dyv(j)
             coef(lvs+3) = coef(lvs+3) + penv(i,j,1,4) * dx(i) * dyv(j)
             coef(lvs+4) = coef(lvs+4) + penv(i,j,1,5) * dx(i) * dyv(j)
             !-------------------------------------------------------------------------------
             ! End of discretization for V
             !-------------------------------------------------------------------------------
          end if

          !-------------------------------------------------------------------------------
          ! next line of the matrix
          !-------------------------------------------------------------------------------
          lvs=lvs+nb_coef_matrix 
       end do
    end do
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! divergence on the P Pressure component
    !-------------------------------------------------------------------------------
    if (NS_method==1) then
       do j=sys,eys
          do i=sxs,exs
             !-------------------------------------------------------------------------------
             ! Divergence velocity
             !-------------------------------------------------------------------------------
             if (nproc>1.and.(i==sxs.or.i==exs.or.j==sys.or.j==eys)) then
                coef(lvs) = 1
                if (NS_compressible) then
                   coef(lvs+1:lvs+4)=0
                   lvs=lvs+5
                else
                   coef(lvs+1:lvs+3)=0
                   lvs=lvs+4
                endif
             else
                if (NS_compressible) then
                   coef(lvs)  = coef(lvs)   + dx(i)  * dy(j) * xit(i,j,1) / ns%dt
                   coef(lvs+1)= coef(lvs+1) - ddx(i) * dx(i) * dy(j)
                   coef(lvs+2)= coef(lvs+2) + ddx(i) * dx(i) * dy(j)
                   coef(lvs+3)= coef(lvs+3) - ddy(j) * dx(i) * dy(j)
                   coef(lvs+4)= coef(lvs+4) + ddy(j) * dx(i) * dy(j)
                   lvs=lvs+5
                else
                   coef(lvs)  = coef(lvs)   - ddx(i) * dx(i) * dy(j)
                   coef(lvs+1)= coef(lvs+1) + ddx(i) * dx(i) * dy(j)
                   coef(lvs+2)= coef(lvs+2) - ddy(j) * dx(i) * dy(j)
                   coef(lvs+3)= coef(lvs+3) + ddy(j) * dx(i) * dy(j)
                   lvs=lvs+4
                endif
             end if
          end do
       end do
    end if
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Algebraic Augmented Lagrangian method
    !-------------------------------------------------------------------------------
    if (AL_method==3) then
       rep=1
       lvs=1
       !-------------------------------------------------------------------------------
       if (nproc>1) call comm_mpi_uvw(repu,repv,repw)
       !-------------------------------------------------------------------------------
       do j=sys,eys
          do i=sxs,exs
             rep(i,j,1)=max(repu(i,j,1),repu(i+1,j,1), &
                  &         repv(i,j,1),repv(i,j+1,1) )
          enddo
       enddo
       rep=rep*AL_dr
       !-------------------------------------------------------------------------------
       ! U velocity component
       !-------------------------------------------------------------------------------
       do j=syus,eyus
          do i=sxus,exus
             coef(lvs  )= coef(lvs)   + rep(i  ,j,1) * ddx(i  ) * dy(j)
             coef(lvs  )= coef(lvs)   + rep(i-1,j,1) * ddx(i-1) * dy(j)
             coef(lvs+1)= coef(lvs+1) - rep(i-1,j,1) * ddx(i-1) * dy(j)
             coef(lvs+2)= coef(lvs+2) - rep(i  ,j,1) * ddx(i  ) * dy(j)
             coef(lvs+5)= coef(lvs+5) - rep(i-1,j,1) * ddy(j  ) * dy(j)
             coef(lvs+6)= coef(lvs+6) + rep(i  ,j,1) * ddy(j  ) * dy(j)
             coef(lvs+7)= coef(lvs+7) + rep(i-1,j,1) * ddy(j  ) * dy(j)
             coef(lvs+8)= coef(lvs+8) - rep(i  ,j,1) * ddy(j  ) * dy(j)
             !-------------------------------------------------------------------------------
             ! next line of the matrix
             !-------------------------------------------------------------------------------
             lvs=lvs+nb_coef_matrix 
          enddo
       enddo
       !-------------------------------------------------------------------------------
       ! V velocity component
       !-------------------------------------------------------------------------------
       do j=syvs,eyvs
          do i=sxvs,exvs
             coef(lvs  )= coef(lvs)   + rep(i,j  ,1) * ddy(j  ) * dx(i)
             coef(lvs  )= coef(lvs)   + rep(i,j-1,1) * ddy(j-1) * dx(i)
             coef(lvs+3)= coef(lvs+3) - rep(i,j-1,1) * ddy(j-1) * dx(i)
             coef(lvs+4)= coef(lvs+4) - rep(i,j  ,1) * ddy(j  ) * dx(i)
             coef(lvs+5)= coef(lvs+5) - rep(i,j-1,1) * ddx(i  ) * dx(i)
             coef(lvs+6)= coef(lvs+6) + rep(i,j-1,1) * ddx(i  ) * dx(i)
             coef(lvs+7)= coef(lvs+7) + rep(i,j  ,1) * ddx(i  ) * dx(i)
             coef(lvs+8)= coef(lvs+8) - rep(i,j  ,1) * ddx(i  ) * dx(i)
             !-------------------------------------------------------------------------------
             ! next line of the matrix
             !-------------------------------------------------------------------------------
             lvs=lvs+nb_coef_matrix 
          enddo
       enddo
       !-------------------------------------------------------------------------------
       deallocate(repu,repv)
    end if
    !-------------------------------------------------------------------------------
    ! End of algebraic Augmented Lagrangian method
    !-------------------------------------------------------------------------------

    return
  end subroutine Navier_Stokes_Matrix_2D
  !*****************************************************************************************************
  !*****************************************************************************************************
  !*****************************************************************************************************
  !*****************************************************************************************************


  !*****************************************************************************************************
  !*****************************************************************************************************
  !*****************************************************************************************************
  !*****************************************************************************************************
  subroutine Navier_Stokes_Matrix_3D (coef,jcof,icof,u,v,w,vie,vis,vir,rep,rho,rovu,rovv,rovw, &
       xit,slvu,slvv,slvw,penu,penv,penw,thetau,thetav,thetaw,nb_coef_matrix,nb_matrix_element,ns)
    !*****************************************************************************************************
    !*****************************************************************************************************
    !*****************************************************************************************************
    !*****************************************************************************************************
    !-------------------------------------------------------------------------------
    ! Finite volume discretization of Navier-Stokes equations in advective form
    ! Pressure-velocity coupling with augmented Lagrangian method
    !-------------------------------------------------------------------------------
    use mod_Parameters, only: nproc,dim,coef_time_1,    &
         & dx,dy,dz,ddx,ddy,ddz,                        &
         & dxu,dyv,dzw,dxu2,dyv2,dzw2,ddxu,ddyv,ddzw,   &
         & sx,ex,sy,ey,sz,ez,                           &
         & gsx,gex,gsy,gey,gsz,gez,                     &
         & sxu,exu,syu,eyu,szu,ezu,                     &
         & sxv,exv,syv,eyv,szv,ezv,                     &
         & sxw,exw,syw,eyw,szw,ezw,                     &
         & sxs,exs,sys,eys,szs,ezs,                     &
         & sxus,exus,syus,eyus,szus,ezus,               &
         & sxvs,exvs,syvs,eyvs,szvs,ezvs,               &
         & sxws,exws,syws,eyws,szws,ezws,               &
         & gsxu,gexu,gsyu,geyu,gszu,gezu,               &
         & gsxv,gexv,gsyv,geyv,gszv,gezv,               &
         & gsxw,gexw,gsyw,geyw,gszw,gezw,               &
         & gx,gy,gz,                                    &
         & NS_method,                                   &
         & NS_linear_term,NS_inertial_scheme,PJ_method, &
         & AL_comp,AL_method,AL_dr
    use mod_Constants, only: zero,d1p2
    use mod_struct_solver
    use mod_mpi
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)                                     :: nb_coef_matrix,nb_matrix_element
    integer, dimension(:),       allocatable, intent(in)    :: jcof,icof
    real(8), dimension(:),       allocatable, intent(inout) :: coef
    real(8), dimension(:,:,:),   allocatable, intent(inout) :: u,v,w,rep
    real(8), dimension(:,:,:),   allocatable, intent(in)    :: vie,rho,rovu,rovv,rovw
    real(8), dimension(:,:,:,:), allocatable, intent(in)    :: vis,vir
    real(8), dimension(:,:,:),   allocatable, intent(in)    :: xit,slvu,slvv,slvw,thetau,thetav,thetaw
    real(8), dimension(:,:,:,:), allocatable, intent(in)    :: penu,penv,penw
    type(solver_ns_t), intent(inout)                        :: ns
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    real(8)                                                 :: difb,diff
    real(8)                                                 :: inerb,inerf
    real(8)                                                 :: inerbb,inerff,inerbf,inerfb
    real(8), dimension(:,:,:),   allocatable                :: repu,repv,repw
    integer                                                 :: i,j,k,lvs
    !-------------------------------------------------------------------------------

    call compute_time(TIMER_START,"[sub] Navier_Stokes_Matrix_3D")
    
    !-------------------------------------------------------------------------------
    ! Compressible augmented Lagrangian method
    !-------------------------------------------------------------------------------
    if (AL_comp.and.PJ_method==0) rep = ns%dt/xit
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Algebraic Augmented Lagrangian method
    !-------------------------------------------------------------------------------
    if (AL_method==3) then
       allocate(repu(sxu-gx:exu+gx,syu-gy:eyu+gy,szu-gz:ezu+gz))
       allocate(repv(sxv-gx:exv+gx,syv-gy:eyv+gy,szv-gz:ezv+gz))
       allocate(repw(sxw-gx:exw+gx,syw-gy:eyw+gy,szw-gz:ezw+gz))
       repu = 0
       repv = 0
       repw = 0
    end if
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Matrix initialization
    !-------------------------------------------------------------------------------
    do lvs = 1,nb_matrix_element
       coef(lvs) = 0
    end do
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Finite volume discretization
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------
    ! U velocity component
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------
    lvs = 1
    do k = szus,ezus
       do j = syus,eyus
          do i = sxus,exus

             if (nproc>1.and.(i<sxu.or.i>exu.or.j<syu.or.j>eyu.or.k<szu.or.k>ezu)) then
                !-------------------------------------------------------------------------------
                ! Penalty term for MPI exchange cells
                !-------------------------------------------------------------------------------
                coef(lvs) = 1!;coef(lvs+1:lvs+14) = 0
             else 
                !-------------------------------------------------------------------------------
                ! Time integration scheme
                !-------------------------------------------------------------------------------
                coef(lvs) = rovu(i,j,k) * coef_time_1 * dxu(i) * dy(j) *dz(k)
                !-------------------------------------------------------------------------------
                ! Elongational viscosity
                !-------------------------------------------------------------------------------
                difb = - vie(i-1,j,k)/dx(i-1) * dy(j) *dz(k) ! backward contribution
                diff =   vie(i  ,j,k)/dx(i  ) * dy(j) *dz(k) ! forward contribution
                coef(lvs  ) = coef(lvs)   - (difb  - diff)
                coef(lvs+1) = coef(lvs+1) + difb
                coef(lvs+2) = coef(lvs+2) - diff
                !-------------------------------------------------------------------------------
                ! Shearing viscosity
                !-------------------------------------------------------------------------------
                difb = - vis(i,j  ,k,3)/dyv(j  ) * dxu(i) * dz(k) ! backward contribution
                diff =   vis(i,j+1,k,3)/dyv(j+1) * dxu(i) * dz(k) ! forward contribution
                coef(lvs  ) = coef(lvs)   - (difb - diff)
                coef(lvs+3) = coef(lvs+3) + difb
                coef(lvs+4) = coef(lvs+4) - diff
                difb = - vis(i,j,k  ,2)/dzw(k  ) * dxu(i) * dy(j) ! backward contribution
                diff =   vis(i,j,k+1,2)/dzw(k+1) * dxu(i) * dy(j) ! forward contribution
                coef(lvs  ) = coef(lvs)   - (difb - diff)
                coef(lvs+5) = coef(lvs+5) + difb
                coef(lvs+6) = coef(lvs+6) - diff
                !-------------------------------------------------------------------------------
                ! Rotation viscosity
                !-------------------------------------------------------------------------------
                difb =   vir(i,j  ,k,3)/dyv(j  ) * dxu(i) * dz(k) ! backward contribution
                diff = - vir(i,j+1,k,3)/dyv(j+1) * dxu(i) * dz(k) ! forward contribution
                coef(lvs  ) = coef(lvs)   - (difb - diff)
                coef(lvs+3) = coef(lvs+3) + difb
                coef(lvs+4) = coef(lvs+4) - diff
                coef(lvs+7) = coef(lvs+7)  - vir(i,j  ,k,3)/dxu(i) * dxu(i) * dz(k)
                coef(lvs+8) = coef(lvs+8)  + vir(i,j  ,k,3)/dxu(i) * dxu(i) * dz(k)
                coef(lvs+9) = coef(lvs+9)  + vir(i,j+1,k,3)/dxu(i) * dxu(i) * dz(k)
                coef(lvs+10) = coef(lvs+10) - vir(i,j+1,k,3)/dxu(i) * dxu(i) * dz(k)
                difb =   vir(i,j,k  ,2)/dzw(k  ) * dxu(i) * dy(j) ! backward contribution
                diff = - vir(i,j,k+1,2)/dzw(k+1) * dxu(i) * dy(j) ! forward contribution
                coef(lvs  ) = coef(lvs)   - (difb - diff)
                coef(lvs+5) = coef(lvs+5) + difb
                coef(lvs+6) = coef(lvs+6) - diff
                coef(lvs+11) = coef(lvs+11) - vir(i,j,k  ,2)/dxu(i) * dxu(i) * dy(j)
                coef(lvs+12) = coef(lvs+12) + vir(i,j,k  ,2)/dxu(i) * dxu(i) * dy(j)
                coef(lvs+13) = coef(lvs+13) + vir(i,j,k+1,2)/dxu(i) * dxu(i) * dy(j)
                coef(lvs+14) = coef(lvs+14) - vir(i,j,k+1,2)/dxu(i) * dxu(i) * dy(j)
                !-------------------------------------------------------------------------------
                ! Inertial terms
                !-------------------------------------------------------------------------------
                select case(NS_inertial_scheme)
                case(-10,-11)
                case(0) ! Stokes flow 
                case(1,10) ! centered scheme         
                   inerbb = - rovu(i,j,k) * (dxu2(i-1)*u(i  ,j,k)+dxu2(i  )*u(i-1,j,k))/dx(i-1) & ! backward-backward
                        & * dxu2(i  )/dx(i-1) * dy(j) * dz(k)
                   inerbf = - rovu(i,j,k) * (dxu2(i-1)*u(i  ,j,k)+dxu2(i  )*u(i-1,j,k))/dx(i-1) & ! backward-forward
                        & * dxu2(i-1)/dx(i-1) * dy(j) * dz(k)
                   inerff =   rovu(i,j,k) * (dxu2(i  )*u(i+1,j,k)+dxu2(i+1)*u(i  ,j,k))/dx(i  ) & ! forward-forward
                        & * dxu2(i  )/dx(i  ) * dy(j) * dz(k)
                   inerfb =   rovu(i,j,k) * (dxu2(i  )*u(i+1,j,k)+dxu2(i+1)*u(i  ,j,k))/dx(i  ) & ! forward-backward
                        & * dxu2(i+1)/dx(i  ) * dy(j) * dz(k)
                   coef(lvs  ) = coef(lvs  ) + inerbf + inerfb
                   coef(lvs+1) = coef(lvs+1) + inerbb
                   coef(lvs+2) = coef(lvs+2) + inerff
                   inerb = - rovu(i,j,k) * (v(i-1,j  ,k)+v(i,j  ,k))/2 * d1p2 * dxu(i) * dz(k) ! backward contribution
                   inerf =   rovu(i,j,k) * (v(i-1,j+1,k)+v(i,j+1,k))/2 * d1p2 * dxu(i) * dz(k) ! forward contribution
                   coef(lvs  ) = coef(lvs  ) + inerb + inerf
                   coef(lvs+3) = coef(lvs+3) + inerb
                   coef(lvs+4) = coef(lvs+4) + inerf
                   inerb = - rovu(i,j,k) * (w(i-1,j,k  )+w(i,j,k  ))/2 * d1p2 * dxu(i) * dy(j) ! backward contribution
                   inerf =   rovu(i,j,k) * (w(i-1,j,k+1)+w(i,j,k+1))/2 * d1p2 * dxu(i) * dy(j) ! forward contribution
                   coef(lvs  ) = coef(lvs  ) + inerb + inerf
                   coef(lvs+5) = coef(lvs+5) + inerb
                   coef(lvs+6) = coef(lvs+6) + inerf
                case(2)
                   inerb = - rovu(i,j,k)*(dxu2(i-1)*u(i,j,k)+dxu2(i)*u(i-1,j,k))/dx(i-1) * dy(j) * dz(k) ! backward contribution
                   inerf =   rovu(i,j,k)*(dxu2(i+1)*u(i,j,k)+dxu2(i)*u(i+1,j,k))/dx(i  ) * dy(j) * dz(k) ! forward contribution
                   coef(lvs  ) = coef(lvs  ) + (max( inerb,zero)+max(inerf,zero))
                   coef(lvs+1) = coef(lvs+1) -  max(-inerb,zero)
                   coef(lvs+2) = coef(lvs+2) -  max(-inerf,zero)
                   inerb = - rovu(i,j,k)*(v(i,j  ,k)+v(i-1,j  ,k))/2 * dxu(i) * dz(k) ! backward contribution
                   inerf =   rovu(i,j,k)*(v(i,j+1,k)+v(i-1,j+1,k))/2 * dxu(i) * dz(k) ! forward contribution
                   coef(lvs  ) = coef(lvs  ) + (max( inerb,zero)+max(inerf,zero))
                   coef(lvs+3) = coef(lvs+3) -  max(-inerb,zero)
                   coef(lvs+4) = coef(lvs+4) -  max(-inerf,zero)
                   inerb = - rovu(i,j,k)*(w(i,j,k  )+w(i-1,j,k  ))/2 * dxu(i) * dy(j) ! backward contribution
                   inerf =   rovu(i,j,k)*(w(i,j,k+1)+w(i-1,j,k+1))/2 * dxu(i) * dy(j) ! forward contribution
                   coef(lvs  ) = coef(lvs  ) + (max( inerb,zero)+max(inerf,zero))
                   coef(lvs+5) = coef(lvs+5) -  max(-inerb,zero)
                   coef(lvs+6) = coef(lvs+6) -  max(-inerf,zero)
                case(3)
                   inerbb = - rovu(i,j,k) * (dxu2(i-1)*u(i  ,j,k)+dxu2(i  )*u(i-1,j,k))/dx(i-1) & ! backward-backward
                        & * dxu2(i  )/dx(i-1) * dy(j) * dz(k)
                   inerbf = - rovu(i,j,k) * (dxu2(i-1)*u(i  ,j,k)+dxu2(i  )*u(i-1,j,k))/dx(i-1) & ! backward-forward
                        & * dxu2(i-1)/dx(i-1) * dy(j) * dz(k)
                   inerff =   rovu(i,j,k) * (dxu2(i  )*u(i+1,j,k)+dxu2(i+1)*u(i  ,j,k))/dx(i  ) & ! forward-forward
                        & * dxu2(i  )/dx(i  ) * dy(j) * dz(k)
                   inerfb =   rovu(i,j,k) * (dxu2(i  )*u(i+1,j,k)+dxu2(i+1)*u(i  ,j,k))/dx(i  ) & ! forward-backward
                        & * dxu2(i+1)/dx(i  ) * dy(j) * dz(k)
                   coef(lvs  ) = coef(lvs  ) + thetau(i,j,k)*(inerbf + inerfb)
                   coef(lvs+1) = coef(lvs+1) + thetau(i,j,k)* inerbb
                   coef(lvs+2) = coef(lvs+2) + thetau(i,j,k)* inerff
                   inerb = - rovu(i,j,k) * (v(i-1,j  ,k)+v(i,j  ,k))/2 * d1p2 * dxu(i) * dz(k) ! backward contribution
                   inerf =   rovu(i,j,k) * (v(i-1,j+1,k)+v(i,j+1,k))/2 * d1p2 * dxu(i) * dz(k) ! forward contribution
                   coef(lvs  ) = coef(lvs  ) + thetau(i,j,k)*(inerb + inerf)
                   coef(lvs+3) = coef(lvs+3) + thetau(i,j,k)* inerb
                   coef(lvs+4) = coef(lvs+4) + thetau(i,j,k)* inerf
                   inerb = - rovu(i,j,k) * (w(i-1,j,k  )+w(i,j,k  ))/2 * d1p2 * dxu(i) * dy(j) ! backward contribution
                   inerf =   rovu(i,j,k) * (w(i-1,j,k+1)+w(i,j,k+1))/2 * d1p2 * dxu(i) * dy(j) ! forward contribution
                   coef(lvs  ) = coef(lvs  ) + thetau(i,j,k)*(inerb + inerf)
                   coef(lvs+5) = coef(lvs+5) + thetau(i,j,k)* inerb
                   coef(lvs+6) = coef(lvs+6) + thetau(i,j,k)* inerf

                   inerb = - rovu(i,j,k)*(dxu2(i-1)*u(i,j,k)+dxu2(i)*u(i-1,j,k))/dx(i-1) * dy(j) * dz(k) ! backward contribution
                   inerf =   rovu(i,j,k)*(dxu2(i+1)*u(i,j,k)+dxu2(i)*u(i+1,j,k))/dx(i  ) * dy(j) * dz(k) ! forward contribution
                   coef(lvs  ) = coef(lvs  ) + (1-thetau(i,j,k))*(max( inerb,zero)+max(inerf,zero))
                   coef(lvs+1) = coef(lvs+1) - (1-thetau(i,j,k))* max(-inerb,zero)
                   coef(lvs+2) = coef(lvs+2) - (1-thetau(i,j,k))* max(-inerf,zero)
                   inerb = - rovu(i,j,k)*(v(i,j  ,k)+v(i-1,j  ,k))/2 * dxu(i) * dz(k) ! backward contribution
                   inerf =   rovu(i,j,k)*(v(i,j+1,k)+v(i-1,j+1,k))/2 * dxu(i) * dz(k) ! forward contribution
                   coef(lvs  ) = coef(lvs  ) + (1-thetau(i,j,k))*(max( inerb,zero)+max(inerf,zero))
                   coef(lvs+3) = coef(lvs+3) - (1-thetau(i,j,k))* max(-inerb,zero)
                   coef(lvs+4) = coef(lvs+4) - (1-thetau(i,j,k))* max(-inerf,zero)
                   inerb = - rovu(i,j,k)*(w(i,j,k  )+w(i-1,j,k  ))/2 * dxu(i) * dy(j) ! backward contribution
                   inerf =   rovu(i,j,k)*(w(i,j,k+1)+w(i-1,j,k+1))/2 * dxu(i) * dy(j) ! forward contribution
                   coef(lvs  ) = coef(lvs  ) + (1-thetau(i,j,k))*(max( inerb,zero)+max(inerf,zero))
                   coef(lvs+5) = coef(lvs+5) - (1-thetau(i,j,k))* max(-inerb,zero)
                   coef(lvs+6) = coef(lvs+6) - (1-thetau(i,j,k))* max(-inerf,zero)
                case DEFAULT
                   write(*,*) 'Navier-Stokes inertial scheme does not exist'
                   stop
                end select
                !-------------------------------------------------------------------------------
                ! Coupled terms
                !-------------------------------------------------------------------------------
                if (NS_method==1) then
                   !-------------------------------------------------------------------------------
                   !  Pressure gradient
                   !-------------------------------------------------------------------------------
                   coef(lvs+15) = coef(lvs+15) - ddxu(i) * dxu(i) * dy(j) * dz(k)
                   coef(lvs+16) =  coef(lvs+16) + ddxu(i) * dxu(i) * dy(j) * dz(k)
                   !-------------------------------------------------------------------------------
                end if

                !-------------------------------------------------------------------------------
                ! Augmented Lagrangian
                !-------------------------------------------------------------------------------
                if (AL_method<3) then
                   coef(lvs  ) = coef(lvs)    + rep(i  ,j,k) * ddx(i)   * dy(j) * dz(k) 
                   coef(lvs  ) = coef(lvs)    + rep(i-1,j,k) * ddx(i-1) * dy(j) * dz(k) 
                   coef(lvs+1) = coef(lvs+1)  - rep(i-1,j,k) * ddx(i-1) * dy(j) * dz(k) 
                   coef(lvs+2) = coef(lvs+2)  - rep(i  ,j,k) * ddx(i)   * dy(j) * dz(k)
                   coef(lvs+7) = coef(lvs+7)  - rep(i-1,j,k) * ddy(j)   * dy(j) * dz(k)
                   coef(lvs+8) = coef(lvs+8)  + rep(i  ,j,k) * ddy(j)   * dy(j) * dz(k)
                   coef(lvs+9) = coef(lvs+9)  + rep(i-1,j,k) * ddy(j)   * dy(j) * dz(k)
                   coef(lvs+10) = coef(lvs+10) - rep(i  ,j,k) * ddy(j)   * dy(j) * dz(k)
                   coef(lvs+11) = coef(lvs+11) - rep(i-1,j,k) * ddz(k)   * dy(j) * dz(k)
                   coef(lvs+12) = coef(lvs+12) + rep(i  ,j,k) * ddz(k)   * dy(j) * dz(k)
                   coef(lvs+13) = coef(lvs+13) + rep(i-1,j,k) * ddz(k)   * dy(j) * dz(k)
                   coef(lvs+14) = coef(lvs+14) - rep(i  ,j,k) * ddz(k)   * dy(j) * dz(k)
                else
                   repu(i,j,k) = max(                                            &
                        & maxval(abs(coef(lvs:lvs+6)*dx(i)*ddy(j)*ddz(k))),    &
                        & maxval(abs(coef(lvs+7:lvs+10))*dy(j)*ddy(j)*ddz(k)), &
                        & maxval(abs(coef(lvs+11:lvs+14))*dz(k)*ddy(j)*ddz(k)))
                end if
                !-------------------------------------------------------------------------------
                ! Linear/Brinkman term
                !-------------------------------------------------------------------------------
                if (NS_linear_term) coef(lvs) = coef(lvs) + slvu(i,j,k) * dxu(i) * dy(j) * dz(k)
                !-------------------------------------------------------------------------------
                ! Penalty term for boundary conditions and immersed objects
                ! penu (i,j,k,l)
                ! l = 1 central U component
                ! l = 2 left U component
                ! l = 3 right U component
                ! l = 4 bottom U component
                ! l = 5 top U component
                ! l = 6 backward U component
                ! l = 7 forward U component
                !-------------------------------------------------------------------------------
                coef(lvs)   = coef(lvs)   + penu(i,j,k,1) * dxu(i) * dy(j) * dz(k)
                coef(lvs+1) = coef(lvs+1) + penu(i,j,k,2) * dxu(i) * dy(j) * dz(k)
                coef(lvs+2) = coef(lvs+2) + penu(i,j,k,3) * dxu(i) * dy(j) * dz(k)
                coef(lvs+3) = coef(lvs+3) + penu(i,j,k,4) * dxu(i) * dy(j) * dz(k)
                coef(lvs+4) = coef(lvs+4) + penu(i,j,k,5) * dxu(i) * dy(j) * dz(k)
                coef(lvs+5) = coef(lvs+5) + penu(i,j,k,6) * dxu(i) * dy(j) * dz(k)
                coef(lvs+6) = coef(lvs+6) + penu(i,j,k,7) * dxu(i) * dy(j) * dz(k)
                !-------------------------------------------------------------------------------
                ! End of discretization for U
                !-------------------------------------------------------------------------------
             end if

             !-------------------------------------------------------------------------------
             ! next line of the matrix
             !-------------------------------------------------------------------------------
             lvs = lvs+nb_coef_matrix 
          end do
       end do
    end do
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------
    ! V velocity component
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------
    do k = szvs,ezvs
       do j = syvs,eyvs
          do i = sxvs,exvs

             if (nproc>1.and.(i<sxv.or.i>exv.or.j<syv.or.j>eyv.or.k<szv.or.k>ezv)) then
                !-------------------------------------------------------------------------------
                ! Penalty term for MPI exchange cells
                !-------------------------------------------------------------------------------
                coef(lvs) = 1!;coef(lvs+1:lvs+14) = 0
             else 
                !-------------------------------------------------------------------------------
                ! Time integration scheme
                !-------------------------------------------------------------------------------
                coef(lvs) = rovv(i,j,k) * coef_time_1 * dx(i) * dyv(j) * dz(k)
                !-------------------------------------------------------------------------------
                ! Elongational viscosity
                !-------------------------------------------------------------------------------
                difb = - vie(i,j-1,k)/dy(j-1) * dx(i) * dz(k) ! backward contribution
                diff =   vie(i,j  ,k)/dy(j  ) * dx(i) * dz(k) ! forward contribution
                coef(lvs  ) = coef(lvs)   - (difb  - diff)
                coef(lvs+3) = coef(lvs+3) + difb
                coef(lvs+4) = coef(lvs+4) - diff
                !-------------------------------------------------------------------------------
                ! Shearing viscosity
                !-------------------------------------------------------------------------------
                difb = - vis(i  ,j,k,3)/dxu(i  ) * dyv(j) * dz(k) ! backward contribution
                diff =   vis(i+1,j,k,3)/dxu(i+1) * dyv(j) * dz(k) ! forward contribution
                coef(lvs  ) = coef(lvs)   - (difb - diff)
                coef(lvs+1) = coef(lvs+1) + difb
                coef(lvs+2) = coef(lvs+2) - diff
                difb = - vis(i,j,k  ,1)/dzw(k  ) * dx(i) * dyv(j) ! backward contribution
                diff =   vis(i,j,k+1,1)/dzw(k+1) * dx(i) * dyv(j) ! forward contribution
                coef(lvs  ) = coef(lvs)   - (difb - diff)
                coef(lvs+5) = coef(lvs+5) + difb
                coef(lvs+6) = coef(lvs+6) - diff
                !-------------------------------------------------------------------------------
                ! Rotation viscosity
                !-------------------------------------------------------------------------------
                difb =   vir(i  ,j,k,3)/dxu(i  ) * dyv(j) * dz(k) ! backward contribution
                diff = - vir(i+1,j,k,3)/dxu(i+1) * dyv(j) * dz(k) ! forward contribution
                coef(lvs  ) = coef(lvs)   - (difb - diff)
                coef(lvs+1) = coef(lvs+1) + difb
                coef(lvs+2) = coef(lvs+2) - diff
                coef(lvs+7) = coef(lvs+7)  - vir(i  ,j,k,3)/dyv(j) * dyv(j) * dz(k)
                coef(lvs+8) = coef(lvs+8)  + vir(i+1,j,k,3)/dyv(j) * dyv(j) * dz(k)
                coef(lvs+9) = coef(lvs+9)  + vir(i  ,j,k,3)/dyv(j) * dyv(j) * dz(k)
                coef(lvs+10) = coef(lvs+10) - vir(i+1,j,k,3)/dyv(j) * dyv(j) * dz(k)
                difb =   vir(i,j,k  ,1)/dzw(k  ) * dx(i) * dyv(j) ! backward contribution
                diff = - vir(i,j,k+1,1)/dzw(k+1) * dx(i) * dyv(j) ! forward contribution
                coef(lvs  ) = coef(lvs)   - (difb - diff)
                coef(lvs+5) = coef(lvs+5) + difb
                coef(lvs+6) = coef(lvs+6) - diff
                coef(lvs+11) = coef(lvs+11) - vir(i,j,k  ,1)/dyv(j) * dx(i) * dyv(j)
                coef(lvs+12) = coef(lvs+12) + vir(i,j,k+1,1)/dyv(j) * dx(i) * dyv(j)
                coef(lvs+13) = coef(lvs+13) + vir(i,j,k  ,1)/dyv(j) * dx(i) * dyv(j)
                coef(lvs+14) = coef(lvs+14) - vir(i,j,k+1,1)/dyv(j) * dx(i) * dyv(j)
                !-------------------------------------------------------------------------------
                ! Inertial terms
                !-------------------------------------------------------------------------------
                select case(NS_inertial_scheme)
                case(-10,-11)
                case(0) ! Stokes flow
                case(1,10) ! centered scheme
                   inerb = - rovv(i,j,k) * (u(i  ,j-1,k)+u(i  ,j,k))/2 * d1p2 * dyv(j) * dz(k) ! backward contribution
                   inerf =   rovv(i,j,k) * (u(i+1,j-1,k)+u(i+1,j,k))/2 * d1p2 * dyv(j) * dz(k) ! forward contribution
                   coef(lvs  ) = coef(lvs  ) + inerb + inerf
                   coef(lvs+1) = coef(lvs+1) + inerb
                   coef(lvs+2) = coef(lvs+2) + inerf 
                   inerbb = - rovv(i,j,k) * (dyv2(j-1)*v(i,j  ,k)+dyv2(j  )*v(i,j-1,k))/dy(j-1) &
                        & * dyv2(j  )/dy(j-1) * dx(i) * dz(k) ! backward-backward
                   inerbf = - rovv(i,j,k) * (dyv2(j-1)*v(i,j  ,k)+dyv2(j  )*v(i,j-1,k))/dy(j-1) &
                        & * dyv2(j-1)/dy(j-1) * dx(i) * dz(k) ! backward-forward
                   inerff =   rovv(i,j,k) * (dyv2(j  )*v(i,j+1,k)+dyv2(j+1)*v(i,j  ,k))/dy(j  ) &
                        & * dyv2(j  )/dy(j  ) * dx(i) * dz(k) ! forward-forward
                   inerfb =   rovv(i,j,k) * (dyv2(j  )*v(i,j+1,k)+dyv2(j+1)*v(i,j  ,k))/dy(j  ) &
                        & * dyv2(j+1)/dy(j  ) * dx(i) * dz(k) ! forward-forward
                   coef(lvs  ) = coef(lvs  ) + inerbf + inerfb
                   coef(lvs+3) = coef(lvs+3) + inerbb
                   coef(lvs+4) = coef(lvs+4) + inerff
                   inerb = - rovv(i,j,k) * (w(i,j-1,k  )+w(i,j,k  ))/2 * d1p2 * dx(i) * dyv(j) ! backward contribution
                   inerf =   rovv(i,j,k) * (w(i,j-1,k+1)+w(i,j,k+1))/2 * d1p2 * dx(i) * dyv(j)! forward contribution
                   coef(lvs  ) = coef(lvs  ) + inerb + inerf
                   coef(lvs+5) = coef(lvs+5) + inerb
                   coef(lvs+6) = coef(lvs+6) + inerf
                case(2)
                   inerb = - rovv(i,j,k)*(u(i  ,j,k)+u(i  ,j-1,k))/2 * dyv(j) * dz(k) ! backward contribution
                   inerf =   rovv(i,j,k)*(u(i+1,j,k)+u(i+1,j-1,k))/2 * dyv(j) * dz(k) ! forward contribution
                   coef(lvs  ) = coef(lvs  ) + (max( inerb,zero)+max(inerf,zero))
                   coef(lvs+1) = coef(lvs+1) -  max(-inerb,zero)
                   coef(lvs+2) = coef(lvs+2) -  max(-inerf,zero)
                   inerb = - rovv(i,j,k)*(dyv2(j-1)*v(i,j,k)+dyv2(j)*v(i,j-1,k))/dy(j-1) * dx(i) * dz(k) ! backward contribution
                   inerf =   rovv(i,j,k)*(dyv2(j+1)*v(i,j,k)+dyv2(j)*v(i,j+1,k))/dy(j  ) * dx(i) * dz(k) ! forward contribution
                   coef(lvs  ) = coef(lvs  ) + (max( inerb,zero)+max(inerf,zero))
                   coef(lvs+3) = coef(lvs+3) -  max(-inerb,zero)
                   coef(lvs+4) = coef(lvs+4) -  max(-inerf,zero)
                   inerb = - rovv(i,j,k)*(w(i,j,k  )+w(i,j-1,k  ))/2 * dx(i) * dyv(j) ! backward contribution
                   inerf =   rovv(i,j,k)*(w(i,j,k+1)+w(i,j-1,k+1))/2 * dx(i) * dyv(j) ! forward contribution
                   coef(lvs  ) = coef(lvs  ) + (max( inerb,zero)+max(inerf,zero))
                   coef(lvs+5) = coef(lvs+5) -  max(-inerb,zero)
                   coef(lvs+6) = coef(lvs+6) -  max(-inerf,zero)
                case(3)
                   inerb = - rovv(i,j,k) * (u(i  ,j-1,k)+u(i  ,j,k))/2 * d1p2 * dyv(j) * dz(k) ! backward contribution
                   inerf =   rovv(i,j,k) * (u(i+1,j-1,k)+u(i+1,j,k))/2 * d1p2 * dyv(j) * dz(k) ! forward contribution
                   coef(lvs  ) = coef(lvs  ) + thetav(i,j,k)*(inerb + inerf)
                   coef(lvs+1) = coef(lvs+1) + thetav(i,j,k)* inerb
                   coef(lvs+2) = coef(lvs+2) + thetav(i,j,k)* inerf 
                   inerbb = - rovv(i,j,k) * (dyv2(j-1)*v(i,j  ,k)+dyv2(j  )*v(i,j-1,k))/dy(j-1) &
                        & * dyv2(j  )/dy(j-1) * dx(i) * dz(k) ! backward-backward
                   inerbf = - rovv(i,j,k) * (dyv2(j-1)*v(i,j  ,k)+dyv2(j  )*v(i,j-1,k))/dy(j-1) &
                        & * dyv2(j-1)/dy(j-1) * dx(i) * dz(k) ! backward-forward
                   inerff =   rovv(i,j,k) * (dyv2(j  )*v(i,j+1,k)+dyv2(j+1)*v(i,j  ,k))/dy(j  ) &
                        & * dyv2(j  )/dy(j  ) * dx(i) * dz(k) ! forward-forward
                   inerfb =   rovv(i,j,k) * (dyv2(j  )*v(i,j+1,k)+dyv2(j+1)*v(i,j  ,k))/dy(j  ) &
                        & * dyv2(j+1)/dy(j  ) * dx(i) * dz(k) ! forward-forward
                   coef(lvs  ) = coef(lvs  ) + thetav(i,j,k)*(inerbf + inerfb)
                   coef(lvs+3) = coef(lvs+3) + thetav(i,j,k)* inerbb
                   coef(lvs+4) = coef(lvs+4) + thetav(i,j,k)* inerff
                   inerb = - rovv(i,j,k) * (w(i,j-1,k  )+w(i,j,k  ))/2 * d1p2 * dx(i) * dyv(j) ! backward contribution
                   inerf =   rovv(i,j,k) * (w(i,j-1,k+1)+w(i,j,k+1))/2 * d1p2 * dx(i) * dyv(j)! forward contribution
                   coef(lvs  ) = coef(lvs  ) + thetav(i,j,k)*(inerb + inerf)
                   coef(lvs+5) = coef(lvs+5) + thetav(i,j,k)* inerb
                   coef(lvs+6) = coef(lvs+6) + thetav(i,j,k)* inerf

                   inerb = - rovv(i,j,k)*(u(i  ,j,k)+u(i  ,j-1,k))/2 * dyv(j) * dz(k) ! backward contribution
                   inerf =   rovv(i,j,k)*(u(i+1,j,k)+u(i+1,j-1,k))/2 * dyv(j) * dz(k) ! forward contribution
                   coef(lvs  ) = coef(lvs  ) + (1-thetav(i,j,k))*(max( inerb,zero)+max(inerf,zero))
                   coef(lvs+1) = coef(lvs+1) - (1-thetav(i,j,k))* max(-inerb,zero)
                   coef(lvs+2) = coef(lvs+2) - (1-thetav(i,j,k))* max(-inerf,zero)
                   inerb = - rovv(i,j,k)*(dyv2(j-1)*v(i,j,k)+dyv2(j)*v(i,j-1,k))/dy(j-1) * dx(i) * dz(k) ! backward contribution
                   inerf =   rovv(i,j,k)*(dyv2(j+1)*v(i,j,k)+dyv2(j)*v(i,j+1,k))/dy(j  ) * dx(i) * dz(k) ! forward contribution
                   coef(lvs  ) = coef(lvs  ) + (1-thetav(i,j,k))*(max( inerb,zero)+max(inerf,zero))
                   coef(lvs+3) = coef(lvs+3) - (1-thetav(i,j,k))* max(-inerb,zero)
                   coef(lvs+4) = coef(lvs+4) - (1-thetav(i,j,k))* max(-inerf,zero)
                   inerb = - rovv(i,j,k)*(w(i,j,k  )+w(i,j-1,k  ))/2 * dx(i) * dyv(j) ! backward contribution
                   inerf =   rovv(i,j,k)*(w(i,j,k+1)+w(i,j-1,k+1))/2 * dx(i) * dyv(j) ! forward contribution
                   coef(lvs  ) = coef(lvs  ) + (1-thetav(i,j,k))*(max( inerb,zero)+max(inerf,zero))
                   coef(lvs+5) = coef(lvs+5) - (1-thetav(i,j,k))* max(-inerb,zero)
                   coef(lvs+6) = coef(lvs+6) - (1-thetav(i,j,k))* max(-inerf,zero)
                case DEFAULT
                   write(*,*) 'Navier-Stokes inertial scheme does not exist'
                   stop
                end select
                !-------------------------------------------------------------------------------
                ! Coupled terms
                !-------------------------------------------------------------------------------
                if (NS_method==1) then
                   !-------------------------------------------------------------------------------
                   !  Pressure gradient
                   !-------------------------------------------------------------------------------
                   coef(lvs+15) = coef(lvs+15) - ddyv(j) * dx(i) * dyv(j) * dz(k)
                   coef(lvs+16) = coef(lvs+16) + ddyv(j) * dx(i) * dyv(j) * dz(k)
                   !-------------------------------------------------------------------------------
                end if
                !-------------------------------------------------------------------------------
                ! Augmented Lagrangian
                !-------------------------------------------------------------------------------
                if (AL_method<3) then
                   coef(lvs  ) = coef(lvs)    + rep(i,j  ,k) * ddy(j)   * dx(i) * dz(k)
                   coef(lvs  ) = coef(lvs)    + rep(i,j-1,k) * ddy(j-1) * dx(i) * dz(k)
                   coef(lvs+3) = coef(lvs+3)  - rep(i,j-1,k) * ddy(j-1) * dx(i) * dz(k)
                   coef(lvs+4) = coef(lvs+4)  - rep(i,j  ,k) * ddy(j)   * dx(i) * dz(k)
                   coef(lvs+7) = coef(lvs+7)  - rep(i,j-1,k) * ddx(i)   * dx(i) * dz(k)
                   coef(lvs+8) = coef(lvs+8)  + rep(i,j-1,k) * ddx(i)   * dx(i) * dz(k)
                   coef(lvs+9) = coef(lvs+9)  + rep(i,j  ,k) * ddx(i)   * dx(i) * dz(k)
                   coef(lvs+10) = coef(lvs+10) - rep(i,j  ,k) * ddx(i)   * dx(i) * dz(k)
                   coef(lvs+11) = coef(lvs+11) - rep(i,j-1,k) * ddz(k)   * dx(i) * dz(k)
                   coef(lvs+12) = coef(lvs+12) + rep(i,j-1,k) * ddz(k)   * dx(i) * dz(k)
                   coef(lvs+13) = coef(lvs+13) + rep(i,j  ,k) * ddz(k)   * dx(i) * dz(k)
                   coef(lvs+14) = coef(lvs+14) - rep(i,j  ,k) * ddz(k)   * dx(i) * dz(k)
                else
                   repv(i,j,k) = max(                                            &
                        & maxval(abs(coef(lvs:lvs+6)*dy(j)*ddx(i)*ddz(k))),    &
                        & maxval(abs(coef(lvs+7:lvs+10))*dx(i)*ddx(i)*ddz(k)), &
                        & maxval(abs(coef(lvs+11:lvs+14))*dz(k)*ddx(i)*ddz(k)))
                end if
                !-------------------------------------------------------------------------------
                ! Linear/Brinkman term
                !-------------------------------------------------------------------------------
                if (NS_linear_term) coef(lvs) = coef(lvs) + slvv(i,j,k) * dx(i) * dyv(j) * dz(k)
                !-------------------------------------------------------------------------------
                ! Penalty term for boundary conditions and immersed objects
                ! penv (i,j,k,l)
                ! l = 1 central V component
                ! l = 2 left V component
                ! l = 3 right V component
                ! l = 4 bottom V component
                ! l = 5 top V component
                ! l = 6 backward V component
                ! l = 7 forward V component
                !-------------------------------------------------------------------------------
                coef(lvs)   = coef(lvs)   + penv(i,j,k,1) * dx(i) * dyv(j) * dz(k)
                coef(lvs+1) = coef(lvs+1) + penv(i,j,k,2) * dx(i) * dyv(j) * dz(k)
                coef(lvs+2) = coef(lvs+2) + penv(i,j,k,3) * dx(i) * dyv(j) * dz(k)
                coef(lvs+3) = coef(lvs+3) + penv(i,j,k,4) * dx(i) * dyv(j) * dz(k)
                coef(lvs+4) = coef(lvs+4) + penv(i,j,k,5) * dx(i) * dyv(j) * dz(k)
                coef(lvs+5) = coef(lvs+5) + penv(i,j,k,6) * dx(i) * dyv(j) * dz(k)
                coef(lvs+6) = coef(lvs+6) + penv(i,j,k,7) * dx(i) * dyv(j) * dz(k)
                !-------------------------------------------------------------------------------
                ! End of discretization for V
                !-------------------------------------------------------------------------------
             end if

             !-------------------------------------------------------------------------------
             ! next line of the matrix
             !-------------------------------------------------------------------------------
             lvs = lvs+nb_coef_matrix 
          end do
       end do
    end do
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------
    ! W velocity component
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------
    do k = szws,ezws
       do j = syws,eyws
          do i = sxws,exws

             if (nproc>1.and.(i<sxw.or.i>exw.or.j<syw.or.j>eyw.or.k<szw.or.k>ezw)) then
                !-------------------------------------------------------------------------------
                ! Penalty term for MPI exchange cells
                !-------------------------------------------------------------------------------
                coef(lvs) = 1!;coef(lvs+1:lvs+14) = 0
             else 
                !-------------------------------------------------------------------------------
                ! Time integration scheme
                !-------------------------------------------------------------------------------
                coef(lvs) = rovw(i,j,k) * coef_time_1 * dx(i) * dy(j) * dzw(k)
                !-------------------------------------------------------------------------------
                ! Elongational viscosity
                !-------------------------------------------------------------------------------
                difb = - vie(i,j,k-1)/dz(k-1) * dx(i) * dy(j) ! backward contribution
                diff =   vie(i,j,k  )/dz(k  ) * dx(i) * dy(j) ! forward contribution
                coef(lvs  ) = coef(lvs)   - (difb  - diff)
                coef(lvs+5) = coef(lvs+5) + difb
                coef(lvs+6) = coef(lvs+6) - diff
                !-------------------------------------------------------------------------------
                ! Shearing viscosity
                !-------------------------------------------------------------------------------
                difb = - vis(i  ,j,k,2)/dxu(i  ) * dy(j) * dzw(k) ! backward contribution
                diff =   vis(i+1,j,k,2)/dxu(i+1) * dy(j) * dzw(k) ! forward contribution
                coef(lvs  ) = coef(lvs)   - (difb - diff)
                coef(lvs+1) = coef(lvs+1) + difb
                coef(lvs+2) = coef(lvs+2) - diff
                difb = - vis(i,j,k  ,1)/dyv(j  ) * dx(i) * dzw(k) ! backward contribution
                diff =   vis(i,j+1,k,1)/dyv(j+1) * dx(i) * dzw(k) ! forward contribution
                coef(lvs  ) = coef(lvs)   - (difb - diff)
                coef(lvs+3) = coef(lvs+3) + difb
                coef(lvs+4) = coef(lvs+4) - diff
                !-------------------------------------------------------------------------------
                ! Rotation viscosity
                !-------------------------------------------------------------------------------
                difb =   vir(i  ,j,k,2)/dxu(i  ) * dy(j) * dzw(k) ! backward contribution
                diff = - vir(i+1,j,k,2)/dxu(i+1) * dy(j) * dzw(k) ! forward contribution
                coef(lvs  ) = coef(lvs)   - (difb - diff)
                coef(lvs+1) = coef(lvs+1) + difb
                coef(lvs+2) = coef(lvs+2) - diff
                coef(lvs+7) = coef(lvs+7)  - vir(i  ,j,k,2)/dzw(k) * dy(j) * dzw(k) 
                coef(lvs+8) = coef(lvs+8)  + vir(i+1,j,k,2)/dzw(k) * dy(j) * dzw(k) 
                coef(lvs+9) = coef(lvs+9)  + vir(i  ,j,k,2)/dzw(k) * dy(j) * dzw(k) 
                coef(lvs+10) = coef(lvs+10) - vir(i+1,j,k,2)/dzw(k) * dy(j) * dzw(k) 
                difb =   vir(i,j,k  ,1)/dyv(j  ) * dx(i) * dzw(k) ! backward contribution
                diff = - vir(i,j+1,k,1)/dyv(j+1) * dx(i) * dzw(k) ! forward contribution
                coef(lvs  ) = coef(lvs)   - (difb - diff)
                coef(lvs+3) = coef(lvs+3) + difb
                coef(lvs+4) = coef(lvs+4) - diff
                coef(lvs+11) = coef(lvs+11) - vir(i,j,k  ,1)/dzw(k) * dx(i) * dzw(k)
                coef(lvs+12) = coef(lvs+12) + vir(i,j,k  ,1)/dzw(k) * dx(i) * dzw(k)
                coef(lvs+13) = coef(lvs+13) + vir(i,j+1,k,1)/dzw(k) * dx(i) * dzw(k)
                coef(lvs+14) = coef(lvs+14) - vir(i,j+1,k,1)/dzw(k) * dx(i) * dzw(k)
                !-------------------------------------------------------------------------------
                ! Inertial terms
                !-------------------------------------------------------------------------------
                select case(NS_inertial_scheme)
                case(-10,-11)
                case(0) ! Stokes Flow
                case(1,10) ! centered scheme
                   inerb = - rovw(i,j,k) * (u(i  ,j,k)+u(i  ,j,k-1))/2 * d1p2 * dy(j) * dzw(k) ! backward contribution
                   inerf =   rovw(i,j,k) * (u(i+1,j,k)+u(i+1,j,k-1))/2 * d1p2 * dy(j) * dzw(k) ! forward contribution
                   coef(lvs  ) = coef(lvs  ) + inerb + inerf
                   coef(lvs+1) = coef(lvs+1) + inerb
                   coef(lvs+2) = coef(lvs+2) + inerf 
                   inerb = - rovw(i,j,k) * (v(i,j  ,k)+v(i,j  ,k-1))/2 * d1p2 * dx(i) * dzw(k) ! backward contribution
                   inerf =   rovw(i,j,k) * (v(i,j+1,k)+v(i,j+1,k-1))/2 * d1p2 * dx(i) * dzw(k) ! forward contribution
                   coef(lvs  ) = coef(lvs  ) + inerb + inerf
                   coef(lvs+3) = coef(lvs+3) + inerb
                   coef(lvs+4) = coef(lvs+4) + inerf
                   inerbb = - rovw(i,j,k) * (dzw2(k  )*w(i,j,k-1)+dzw2(k-1)*w(i,j,k  ))/dz(k-1) &
                        & * dzw2(k  )/dz(k-1) * dx(i) * dy(j) ! backward-backward
                   inerbf = - rovw(i,j,k) * (dzw2(k  )*w(i,j,k-1)+dzw2(k-1)*w(i,j,k  ))/dz(k-1) &
                        & * dzw2(k-1)/dz(k-1) * dx(i) * dy(j) ! backward-forward
                   inerff =   rovw(i,j,k) * (dzw2(k+1)*w(i,j,k  )+dzw2(k  )*w(i,j,k+1))/dz(k  ) &
                        & * dzw2(k  )/dz(k  ) * dx(i) * dy(j) ! forward-forward
                   inerfb =   rovw(i,j,k) * (dzw2(k+1)*w(i,j,k  )+dzw2(k  )*w(i,j,k+1))/dz(k  ) &
                        & * dzw2(k+1)/dz(k  ) * dx(i) * dy(j) ! forward-backward
                   coef(lvs  ) = coef(lvs  ) + inerbf + inerfb
                   coef(lvs+5) = coef(lvs+5) + inerbb
                   coef(lvs+6) = coef(lvs+6) + inerff
                case(2)
                   inerb = - rovw(i,j,k)*(u(i  ,j,k)+u(i  ,j,k-1))/2 * dy(j) * dzw(k) ! backward contribution
                   inerf =   rovw(i,j,k)*(u(i+1,j,k)+u(i+1,j,k-1))/2 * dy(j) * dzw(k) ! forward contribution
                   coef(lvs  ) = coef(lvs  ) + (max( inerb,zero)+max(inerf,zero))
                   coef(lvs+1) = coef(lvs+1) -  max(-inerb,zero)
                   coef(lvs+2) = coef(lvs+2) -  max(-inerf,zero)
                   inerb = - rovw(i,j,k)*(v(i,j  ,k)+v(i,j  ,k-1))/2 * dx(i) * dzw(k) ! backward contribution
                   inerf =   rovw(i,j,k)*(v(i,j+1,k)+v(i,j+1,k-1))/2 * dx(i) * dzw(k) ! forward contribution
                   coef(lvs  ) = coef(lvs  ) + (max( inerb,zero)+max(inerf,zero))
                   coef(lvs+3) = coef(lvs+3) -  max(-inerb,zero)
                   coef(lvs+4) = coef(lvs+4) -  max(-inerf,zero)
                   inerb = - rovw(i,j,k)*(dzw2(k-1)*w(i,j,k)+dzw2(k)*w(i,j,k-1))/dz(k-1) * dx(i) * dy(j) ! backward contribution
                   inerf =   rovw(i,j,k)*(dzw2(k+1)*w(i,j,k)+dzw2(k)*w(i,j,k+1))/dz(k  ) * dx(i) * dy(j) ! forward contribution
                   coef(lvs  ) = coef(lvs  ) + (max( inerb,zero)+max(inerf,zero))
                   coef(lvs+5) = coef(lvs+5) -  max(-inerb,zero)
                   coef(lvs+6) = coef(lvs+6) -  max(-inerf,zero)
                case(3)
                   inerb = - rovw(i,j,k) * (u(i  ,j,k)+u(i  ,j,k-1))/2 * d1p2 * dy(j) * dzw(k) ! backward contribution
                   inerf =   rovw(i,j,k) * (u(i+1,j,k)+u(i+1,j,k-1))/2 * d1p2 * dy(j) * dzw(k) ! forward contribution
                   coef(lvs  ) = coef(lvs  ) + thetaw(i,j,k)*(inerb + inerf)
                   coef(lvs+1) = coef(lvs+1) + thetaw(i,j,k)* inerb
                   coef(lvs+2) = coef(lvs+2) + thetaw(i,j,k)* inerf 
                   inerb = - rovw(i,j,k) * (v(i,j  ,k)+v(i,j  ,k-1))/2 * d1p2 * dx(i) * dzw(k) ! backward contribution
                   inerf =   rovw(i,j,k) * (v(i,j+1,k)+v(i,j+1,k-1))/2 * d1p2 * dx(i) * dzw(k) ! forward contribution
                   coef(lvs  ) = coef(lvs  ) + thetaw(i,j,k)*(inerb + inerf)
                   coef(lvs+3) = coef(lvs+3) + thetaw(i,j,k)* inerb
                   coef(lvs+4) = coef(lvs+4) + thetaw(i,j,k)* inerf
                   inerbb = - rovw(i,j,k) * (dzw2(k  )*w(i,j,k-1)+dzw2(k-1)*w(i,j,k  ))/dz(k-1) &
                        & * dzw2(k  )/dz(k-1) * dx(i) * dy(j) ! backward-backward
                   inerbf = - rovw(i,j,k) * (dzw2(k  )*w(i,j,k-1)+dzw2(k-1)*w(i,j,k  ))/dz(k-1) &
                        & * dzw2(k-1)/dz(k-1) * dx(i) * dy(j) ! backward-forward
                   inerff =   rovw(i,j,k) * (dzw2(k+1)*w(i,j,k  )+dzw2(k  )*w(i,j,k+1))/dz(k  ) &
                        & * dzw2(k  )/dz(k  ) * dx(i) * dy(j) ! forward-forward
                   inerfb =   rovw(i,j,k) * (dzw2(k+1)*w(i,j,k  )+dzw2(k  )*w(i,j,k+1))/dz(k  ) &
                        & * dzw2(k+1)/dz(k  ) * dx(i) * dy(j) ! forward-backward
                   coef(lvs  ) = coef(lvs  ) + thetaw(i,j,k)*(inerbf + inerfb)
                   coef(lvs+5) = coef(lvs+5) + thetaw(i,j,k)* inerbb
                   coef(lvs+6) = coef(lvs+6) + thetaw(i,j,k)* inerff

                   inerb = - rovw(i,j,k)*(u(i  ,j,k)+u(i  ,j,k-1))/2 * dy(j) * dzw(k) ! backward contribution
                   inerf =   rovw(i,j,k)*(u(i+1,j,k)+u(i+1,j,k-1))/2 * dy(j) * dzw(k) ! forward contribution
                   coef(lvs  ) = coef(lvs  ) + (1-thetaw(i,j,k))*(max( inerb,zero)+max(inerf,zero))
                   coef(lvs+1) = coef(lvs+1) - (1-thetaw(i,j,k))* max(-inerb,zero)
                   coef(lvs+2) = coef(lvs+2) - (1-thetaw(i,j,k))* max(-inerf,zero)
                   inerb = - rovw(i,j,k)*(v(i,j  ,k)+v(i,j  ,k-1))/2 * dx(i) * dzw(k) ! backward contribution
                   inerf =   rovw(i,j,k)*(v(i,j+1,k)+v(i,j+1,k-1))/2 * dx(i) * dzw(k) ! forward contribution
                   coef(lvs  ) = coef(lvs  ) + (1-thetaw(i,j,k))*(max( inerb,zero)+max(inerf,zero))
                   coef(lvs+3) = coef(lvs+3) - (1-thetaw(i,j,k))* max(-inerb,zero)
                   coef(lvs+4) = coef(lvs+4) - (1-thetaw(i,j,k))* max(-inerf,zero)
                   inerb = - rovw(i,j,k)*(dzw2(k-1)*w(i,j,k)+dzw2(k)*w(i,j,k-1))/dz(k-1) * dx(i) * dy(j) ! backward contribution
                   inerf =   rovw(i,j,k)*(dzw2(k+1)*w(i,j,k)+dzw2(k)*w(i,j,k+1))/dz(k  ) * dx(i) * dy(j) ! forward contribution
                   coef(lvs  ) = coef(lvs  ) + (1-thetaw(i,j,k))*(max( inerb,zero)+max(inerf,zero))
                   coef(lvs+5) = coef(lvs+5) - (1-thetaw(i,j,k))* max(-inerb,zero)
                   coef(lvs+6) = coef(lvs+6) - (1-thetaw(i,j,k))* max(-inerf,zero)
                case DEFAULT
                   write(*,*) 'Navier-Stokes inertial scheme does not exist'
                   stop
                end select
                !-------------------------------------------------------------------------------
                ! Coupled terms
                !-------------------------------------------------------------------------------
                if (NS_method==1) then
                   !-------------------------------------------------------------------------------
                   !  Pressure gradient
                   !-------------------------------------------------------------------------------
                   coef(lvs+15) = coef(lvs+15) - ddzw(k) * dx(i) * dy(j) * dzw(k)
                   coef(lvs+16) = coef(lvs+16) + ddzw(k) * dx(i) * dy(j) * dzw(k)
                   !-------------------------------------------------------------------------------
                end if
                !-------------------------------------------------------------------------------
                ! Augmented Lagrangian
                !-------------------------------------------------------------------------------
                if (AL_method<3) then
                   coef(lvs  ) = coef(lvs)    + rep(i,j,k  ) * ddz(k)   * dx(i) * dy(j)
                   coef(lvs  ) = coef(lvs)    + rep(i,j,k-1) * ddz(k-1) * dx(i) * dy(j)
                   coef(lvs+5) = coef(lvs+5)  - rep(i,j,k-1) * ddz(k-1) * dx(i) * dy(j)
                   coef(lvs+6) = coef(lvs+6)  - rep(i,j,k  ) * ddz(k)   * dx(i) * dy(j)
                   coef(lvs+7) = coef(lvs+7)  - rep(i,j,k-1) * ddx(i)   * dx(i) * dy(j)
                   coef(lvs+8) = coef(lvs+8)  + rep(i,j,k-1) * ddx(i)   * dx(i) * dy(j)
                   coef(lvs+9) = coef(lvs+9)  + rep(i,j,k  ) * ddx(i)   * dx(i) * dy(j)
                   coef(lvs+10) = coef(lvs+10) - rep(i,j,k  ) * ddx(i)   * dx(i) * dy(j)
                   coef(lvs+11) = coef(lvs+11) - rep(i,j,k-1) * ddy(j)   * dx(i) * dy(j)
                   coef(lvs+12) = coef(lvs+12) + rep(i,j,k  ) * ddy(j)   * dx(i) * dy(j)
                   coef(lvs+13) = coef(lvs+13) + rep(i,j,k-1) * ddy(j)   * dx(i) * dy(j)
                   coef(lvs+14) = coef(lvs+14) - rep(i,j,k  ) * ddy(j)   * dx(i) * dy(j)
                else
                   repw(i,j,k) = max(                                            &
                        & maxval(abs(coef(lvs:lvs+6)*dz(k)*ddx(i)*ddy(j))),    &
                        & maxval(abs(coef(lvs+7:lvs+10))*dx(i)*ddx(i)*ddy(j)), &
                        & maxval(abs(coef(lvs+11:lvs+14))*dy(j)*ddx(i)*ddy(j)))
                end if
                !-------------------------------------------------------------------------------
                ! Linear/Brinkman term
                !-------------------------------------------------------------------------------
                if (NS_linear_term) coef(lvs) = coef(lvs) + slvw(i,j,k) * dx(i) * dy(j) * dzw(k)
                !-------------------------------------------------------------------------------
                ! Penalty term for boundary conditions and immersed objects
                ! penv (i,j,k,l)
                ! l = 1 central W component
                ! l = 2 left W component
                ! l = 3 right W component
                ! l = 4 bottom W component
                ! l = 5 top W component
                ! l = 6 backward W component
                ! l = 7 forward W component
                !-------------------------------------------------------------------------------
                coef(lvs)   = coef(lvs)   + penw(i,j,k,1) * dx(i) * dy(j) * dzw(k)
                coef(lvs+1) = coef(lvs+1) + penw(i,j,k,2) * dx(i) * dy(j) * dzw(k)
                coef(lvs+2) = coef(lvs+2) + penw(i,j,k,3) * dx(i) * dy(j) * dzw(k)
                coef(lvs+3) = coef(lvs+3) + penw(i,j,k,4) * dx(i) * dy(j) * dzw(k)
                coef(lvs+4) = coef(lvs+4) + penw(i,j,k,5) * dx(i) * dy(j) * dzw(k)
                coef(lvs+5) = coef(lvs+5) + penw(i,j,k,6) * dx(i) * dy(j) * dzw(k)
                coef(lvs+6) = coef(lvs+6) + penw(i,j,k,7) * dx(i) * dy(j) * dzw(k)
                !-------------------------------------------------------------------------------
                ! End of discretization for W
                !-------------------------------------------------------------------------------
             end if

             !-------------------------------------------------------------------------------
             ! next line of the matrix
             !-------------------------------------------------------------------------------
             lvs = lvs+nb_coef_matrix 
          end do
       end do
    end do
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! divergence on the P Pressure component
    !-------------------------------------------------------------------------------
    if (NS_method==1) then
       do k = szs,ezs
          do j = sys,eys
             do i = sxs,exs
                !-------------------------------------------------------------------------------
                ! Divergence velocity
                !-------------------------------------------------------------------------------
                if (nproc>1.and.(i==sxs.or.i==exs.or.j==sys.or.j==eys &
                     & .or.k==szs.or.k==ezs)) then
                   coef(lvs) = 1
                   coef(lvs+1:lvs+5) = 0
                else
                   coef(lvs)  = coef(lvs)   - ddx(i) * dx(i) * dy(j) * dz(k) !* 1d-4
                   coef(lvs+1) = coef(lvs+1) + ddx(i) * dx(i) * dy(j) * dz(k) !* 1d-4
                   coef(lvs+2) = coef(lvs+2) - ddy(j) * dx(i) * dy(j) * dz(k) !* 1d-4
                   coef(lvs+3) = coef(lvs+3) + ddy(j) * dx(i) * dy(j) * dz(k) !* 1d-4
                   coef(lvs+4) = coef(lvs+4) - ddz(k) * dx(i) * dy(j) * dz(k) !* 1d-4
                   coef(lvs+5) = coef(lvs+5) + ddz(k) * dx(i) * dy(j) * dz(k) !* 1d-4
                end if

                !-------------------------------------------------------------------------------
                ! next line of the matrix
                !-------------------------------------------------------------------------------
                lvs = lvs+6
             end do
          end do
       end do
    end if
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Algebraic Augmented Lagrangian method
    !-------------------------------------------------------------------------------
    if (AL_method==3) then
       rep = 1
       lvs = 1
       !-------------------------------------------------------------------------------
       call comm_mpi_uvw(repu,repv,repw)
       !-------------------------------------------------------------------------------
       do k = szs,ezs
          do j = sys,eys
             do i = sxs,exs
                rep(i,j,k) = max(repu(i,j,k),repu(i+1,j,k), &
                     &         repv(i,j,k),repv(i,j+1,k), &
                     &         repw(i,j,k),repw(i,j,k+1) )
             end do
          end do
       end do
       rep = rep*AL_dr
       !-------------------------------------------------------------------------------
       ! U velocity component
       !-------------------------------------------------------------------------------
       do k = szus,ezus
          do j = syus,eyus
             do i = sxus,exus
                coef(lvs  ) = coef(lvs)    + rep(i  ,j,k) * ddx(i  ) * dy(j) * dz(k) 
                coef(lvs  ) = coef(lvs)    + rep(i-1,j,k) * ddx(i-1) * dy(j) * dz(k) 
                coef(lvs+1) = coef(lvs+1)  - rep(i-1,j,k) * ddx(i-1) * dy(j) * dz(k) 
                coef(lvs+2) = coef(lvs+2)  - rep(i  ,j,k) * ddx(i  ) * dy(j) * dz(k) 
                coef(lvs+7) = coef(lvs+7)  - rep(i-1,j,k) * ddy(j  ) * dy(j) * dz(k) 
                coef(lvs+8) = coef(lvs+8)  + rep(i  ,j,k) * ddy(j  ) * dy(j) * dz(k) 
                coef(lvs+9) = coef(lvs+9)  + rep(i-1,j,k) * ddy(j  ) * dy(j) * dz(k) 
                coef(lvs+10) = coef(lvs+10) - rep(i  ,j,k) * ddy(j  ) * dy(j) * dz(k) 
                coef(lvs+11) = coef(lvs+11) - rep(i-1,j,k) * ddz(k  ) * dy(j) * dz(k) 
                coef(lvs+12) = coef(lvs+12) + rep(i  ,j,k) * ddz(k  ) * dy(j) * dz(k) 
                coef(lvs+13) = coef(lvs+13) + rep(i-1,j,k) * ddz(k  ) * dy(j) * dz(k) 
                coef(lvs+14) = coef(lvs+14) - rep(i  ,j,k) * ddz(k  ) * dy(j) * dz(k) 
                !-------------------------------------------------------------------------------
                ! next line of the matrix
                !-------------------------------------------------------------------------------
                lvs = lvs+nb_coef_matrix 
             end do
          end do
       end do
       !-------------------------------------------------------------------------------
       ! V velocity component
       !-------------------------------------------------------------------------------
       do k = szvs,ezvs
          do j = syvs,eyvs
             do i = sxvs,exvs
                coef(lvs  ) = coef(lvs)    + rep(i,j  ,k) * ddy(j  ) * dx(i) * dz(k) 
                coef(lvs  ) = coef(lvs)    + rep(i,j-1,k) * ddy(j-1) * dx(i) * dz(k) 
                coef(lvs+3) = coef(lvs+3)  - rep(i,j-1,k) * ddy(j-1) * dx(i) * dz(k) 
                coef(lvs+4) = coef(lvs+4)  - rep(i,j  ,k) * ddy(j  ) * dx(i) * dz(k) 
                coef(lvs+7) = coef(lvs+7)  - rep(i,j-1,k) * ddx(i  ) * dx(i) * dz(k) 
                coef(lvs+8) = coef(lvs+8)  + rep(i,j-1,k) * ddx(i  ) * dx(i) * dz(k) 
                coef(lvs+9) = coef(lvs+9)  + rep(i,j  ,k) * ddx(i  ) * dx(i) * dz(k) 
                coef(lvs+10) = coef(lvs+10) - rep(i,j  ,k) * ddx(i  ) * dx(i) * dz(k) 
                coef(lvs+11) = coef(lvs+11) - rep(i,j-1,k) * ddz(k  ) * dx(i) * dz(k) 
                coef(lvs+12) = coef(lvs+12) + rep(i,j-1,k) * ddz(k  ) * dx(i) * dz(k) 
                coef(lvs+13) = coef(lvs+13) + rep(i,j  ,k) * ddz(k  ) * dx(i) * dz(k) 
                coef(lvs+14) = coef(lvs+14) - rep(i,j  ,k) * ddz(k  ) * dx(i) * dz(k) 
                !-------------------------------------------------------------------------------
                ! next line of the matrix
                !-------------------------------------------------------------------------------
                lvs = lvs+nb_coef_matrix 
             end do
          end do
       end do
       !-------------------------------------------------------------------------------
       ! W velocity component
       !-------------------------------------------------------------------------------
       do k = szws,ezws
          do j = syws,eyws
             do i = sxws,exws
                coef(lvs  ) = coef(lvs)    + rep(i,j,k  ) * ddz(k  ) * dx(i) * dy(j)
                coef(lvs  ) = coef(lvs)    + rep(i,j,k-1) * ddz(k-1) * dx(i) * dy(j)
                coef(lvs+5) = coef(lvs+5)  - rep(i,j,k-1) * ddz(k-1) * dx(i) * dy(j)
                coef(lvs+6) = coef(lvs+6)  - rep(i,j,k  ) * ddz(k  ) * dx(i) * dy(j)
                coef(lvs+7) = coef(lvs+7)  - rep(i,j,k-1) * ddx(i  ) * dx(i) * dy(j)
                coef(lvs+8) = coef(lvs+8)  + rep(i,j,k-1) * ddx(i  ) * dx(i) * dy(j)
                coef(lvs+9) = coef(lvs+9)  + rep(i,j,k  ) * ddx(i  ) * dx(i) * dy(j)
                coef(lvs+10) = coef(lvs+10) - rep(i,j,k  ) * ddx(i  ) * dx(i) * dy(j)
                coef(lvs+11) = coef(lvs+11) - rep(i,j,k-1) * ddy(j  ) * dx(i) * dy(j)
                coef(lvs+12) = coef(lvs+12) + rep(i,j,k  ) * ddy(j  ) * dx(i) * dy(j)
                coef(lvs+13) = coef(lvs+13) + rep(i,j,k-1) * ddy(j  ) * dx(i) * dy(j)
                coef(lvs+14) = coef(lvs+14) - rep(i,j,k  ) * ddy(j  ) * dx(i) * dy(j)
                !-------------------------------------------------------------------------------
                ! next line of the matrix
                !-------------------------------------------------------------------------------
                lvs = lvs+nb_coef_matrix 
             end do
          end do
       end do
       !-------------------------------------------------------------------------------
       deallocate(repu,repv,repw)

    end if
    !-------------------------------------------------------------------------------
    ! End of algebraic Augmented Lagrangian method
    !-------------------------------------------------------------------------------

    call compute_time(TIMER_END,"[sub] Navier_Stokes_Matrix_3D")
  end subroutine  Navier_Stokes_Matrix_3D
  !*****************************************************************************************************
  !*****************************************************************************************************
  !*****************************************************************************************************
  !*****************************************************************************************************

  subroutine Pressure_Convection_diffusion_Matrix(mat,u,v,w,ns)
    use mod_Parameters, only: dim
    use mod_struct_solver
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable, intent(in)    :: u,v,w
    type(solver_ns_t),                      intent(inout) :: ns
    type(matrix_t),                         intent(inout) :: mat
    !-------------------------------------------------------------------------------
    
    if (dim==2) then
       call Pressure_Convection_diffusion_Matrix_2D(mat%coef,mat%jcof,mat%icof,u,v,mat%kdv,mat%nps,ns)
    else
       call Pressure_Convection_diffusion_Matrix_3D(mat%coef,mat%jcof,mat%icof,u,v,w,mat%kdv,mat%nps,ns)
    end if

  end subroutine Pressure_Convection_diffusion_Matrix
  
  subroutine Pressure_Convection_diffusion_Matrix_2D(coef,jcof,icof,u,v, &
       & nb_coef_matrix,nb_matrix_element,ns)
    !*****************************************************************************************************
    !*****************************************************************************************************
    !*****************************************************************************************************
    !*****************************************************************************************************
    !-------------------------------------------------------------------------------
    ! Finite volume discretization of the Energy equation
    !-------------------------------------------------------------------------------
    use mod_Parameters, only: nproc,         &
         & dim,coef_time_1,dx,dy,dxu,dyv,    &
         & sx,ex,sy,ey,gex,gsy,gey,gsx,      &
         & sxs,exs,sys,eys,                  &
         & EN_linear_term,EN_inertial_scheme,&
         & NS_inertial_scheme
    use mod_Constants, only: zero,d1p2
    use mod_struct_solver
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)                                 :: nb_coef_matrix,nb_matrix_element
    integer, dimension(:), intent(in), allocatable      :: jcof,icof
    real(8), dimension(:), intent(inout), allocatable   :: coef
    real(8), dimension(:,:,:), allocatable, intent(in)  :: u,v
    type(solver_ns_t), intent(inout)                    :: ns
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    real(8)                                             :: difb,diff,inerb,inerf,alpha
    integer                                             :: i,j,lvs
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Matrix initialization
    !-------------------------------------------------------------------------------
    coef = 0
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Finite volume discretization
    !-------------------------------------------------------------------------------
    lvs = 1
    do j = sys,eys
       do i = sxs,exs

          if (nproc>1.and.(i==sxs.or.i==exs.or.j==sys.or.j==eys)) then
             !-------------------------------------------------------------------------------
             ! Penalty term for MPI exchange cells
             !-------------------------------------------------------------------------------
             coef(lvs) = 1;coef(lvs+1:lvs+4) = 0
          else           
             !-------------------------------------------------------------------------------
             ! Time integration scheme
             !-------------------------------------------------------------------------------
             coef(lvs) =   1 / ns%dt * dx(i) * dy(j)
             !-------------------------------------------------------------------------------
             ! Inertial terms
             !-------------------------------------------------------------------------------
             if (NS_inertial_scheme/=0) then
                inerb           = -  u(i,j,1) * d1p2 * dy(j) ! backward contribution
                inerf           =    u(i+1,j,1) * d1p2 * dy(j) ! forward contribution
                coef(lvs  ) = coef(lvs  ) + inerb + inerf
                coef(lvs+1) = coef(lvs+1) + inerb
                coef(lvs+2) = coef(lvs+2) + inerf
                inerb           = -   v(i,j,1)* d1p2 * dx(i) ! backward contribution
                inerf           =     v(i,j+1,1) * d1p2 * dx(i) ! forward contribution
                coef(lvs  ) = coef(lvs  ) + inerb + inerf
                coef(lvs+3) = coef(lvs+3) + inerb
                coef(lvs+4) = coef(lvs+4) + inerf
                !-------------------------------------------------------------------------------
                ! symetric BC
                !-------------------------------------------------------------------------------
                if (i==gsx.and.j==gsy) then
                   coef(lvs)   = coef(lvs)
                   coef(lvs+1) = 0
                   coef(lvs+2) = 2 * coef(lvs+2)
                   coef(lvs+3) = 0
                   coef(lvs+4) = 2 * coef(lvs+4)
                elseif(i==gsx.and.j==gey) then
                   coef(lvs)   = coef(lvs)
                   coef(lvs+1) = 0
                   coef(lvs+2) = 2 * coef(lvs+2)
                   coef(lvs+3) = 2 * coef(lvs+3)
                   coef(lvs+4) = 0
                elseif (i==gex.and.j==gsy) then
                   coef(lvs)   = coef(lvs)
                   coef(lvs+1) = 2 * coef(lvs+1)
                   coef(lvs+2) = 0
                   coef(lvs+3) = 0
                   coef(lvs+4) = 2 * coef(lvs+4)
                elseif(i==gex.and.j==gey) then
                   coef(lvs)   = coef(lvs)
                   coef(lvs+1) = 2 * coef(lvs+1)
                   coef(lvs+2) = 0
                   coef(lvs+3) = 2 * coef(lvs+3)
                   coef(lvs+4) = 0
                elseif(i==gsx) then
                   coef(lvs)   = coef(lvs)
                   coef(lvs+1) = 0
                   coef(lvs+2) = 2 * coef(lvs+2)
                   coef(lvs+3) = coef(lvs+3)
                   coef(lvs+4) = coef(lvs+4)
                elseif(i==gex) then
                   coef(lvs)   = coef(lvs)
                   coef(lvs+1) = 2 * coef(lvs+1)
                   coef(lvs+2) = 0
                   coef(lvs+3) = coef(lvs+3)
                   coef(lvs+4) = coef(lvs+4)
                elseif(j==gsy) then
                   coef(lvs)   = coef(lvs)
                   coef(lvs+1) = coef(lvs+1)
                   coef(lvs+2) = coef(lvs+2)
                   coef(lvs+3) = 0
                   coef(lvs+4) = 2 * coef(lvs+4)
                elseif(j==gey) then
                   coef(lvs)   = coef(lvs)
                   coef(lvs+1) = coef(lvs+1)
                   coef(lvs+2) = coef(lvs+2)
                   coef(lvs+3) = 2 * coef(lvs+3)
                   coef(lvs+4) = 0
                end if
             end if
             !-------------------------------------------------------------------------------
             ! End of discretization for Pressure connvection diffusion
             !-------------------------------------------------------------------------------
          end if
          !-------------------------------------------------------------------------------
          ! next line of the matrix
          !-------------------------------------------------------------------------------
          lvs = lvs+nb_coef_matrix 
       end do
    end do
    return
  end subroutine  Pressure_Convection_diffusion_Matrix_2D
  !*****************************************************************************************************
  !*****************************************************************************************************
  !*****************************************************************************************************
  !*****************************************************************************************************
  subroutine Pressure_Convection_diffusion_Matrix_3D(coef,jcof,icof,u,v,w, &
       & nb_coef_matrix,nb_matrix_element,ns)
    !*****************************************************************************************************
    !*****************************************************************************************************
    !*****************************************************************************************************
    !*****************************************************************************************************
    !-------------------------------------------------------------------------------
    ! Finite volume discretization of the Energy equation
    !-------------------------------------------------------------------------------
    use mod_Parameters, only: nproc,                            &
         & dim,coef_time_1,dx,dy,dxu,dyv,dz,                    &
         & sx,ex,sy,ey,gsy,gex,gsz,gey,gez,gsx,                 &
         & sxs,exs,sys,eys,szs,ezs,                             &
         & EN_linear_term,EN_inertial_scheme,NS_inertial_scheme
    use mod_Constants, only: zero,d1p2
    use mod_struct_solver
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)                                :: nb_coef_matrix,nb_matrix_element
    integer, dimension(:), intent(in), allocatable     :: jcof,icof
    real(8), dimension(:), intent(inout), allocatable  :: coef
    real(8), dimension(:,:,:), allocatable, intent(in) :: u,v,w
    type(solver_ns_t), intent(inout)                   :: ns
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    real(8)                                            :: difb,diff,inerb,inerf,alpha
    integer                                            :: i,j,lvs,k
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Matrix initialization
    !-------------------------------------------------------------------------------
    coef = 0
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Finite volume discretization
    !-------------------------------------------------------------------------------
    lvs = 1
    do k = szs,ezs
       do j = sys,eys 
          do i = sxs,exs
             if (nproc>1.and.(i==sxs.or.i==exs.or.j==sys.or.j==eys.or.k==szs.or.k==ezs)) then
                !-------------------------------------------------------------------------------
                ! Penalty term for MPI exchange cells
                !-------------------------------------------------------------------------------
                coef(lvs) = 1;coef(lvs+1:lvs+6) = 0
             else           
                !-------------------------------------------------------------------------------
                ! Time integration scheme
                !-------------------------------------------------------------------------------
                coef(lvs) =   1 / ns%dt * dx(i) * dy(j) * dz(k)
                !-------------------------------------------------------------------------------
                ! Inertial terms
                !-------------------------------------------------------------------------------
                if (NS_inertial_scheme/=0) then
                   inerb           = -  u(i,j  ,k) * d1p2 * dy(j)* dz(k) ! backward contribution
                   inerf           =    u(i+1,j,k) * d1p2 * dy(j)* dz(k) ! forward contribution
                   coef(lvs  ) = coef(lvs  ) + inerb + inerf
                   coef(lvs+1) = coef(lvs+1) + inerb
                   coef(lvs+2) = coef(lvs+2) + inerf
                   inerb           = -   v(i,j  ,k)* d1p2 * dx(i)* dz(k) ! backward contribution
                   inerf           =     v(i,j+1,k) * d1p2 * dx(i)* dz(k) ! forward contribution
                   coef(lvs  ) = coef(lvs  ) + inerb + inerf
                   coef(lvs+3) = coef(lvs+3) + inerb
                   coef(lvs+4) = coef(lvs+4) + inerf
                   inerb           = -   w(i,j,k  ) * d1p2 * dx(i) * dy(j) ! backward contribution
                   inerf           =     w(i,j,k+1) * d1p2 * dx(i) * dy(j) ! forward contribution
                   coef(lvs  ) = coef(lvs  ) + inerb + inerf
                   coef(lvs+5) = coef(lvs+5) + inerb
                   coef(lvs+6) = coef(lvs+6) + inerf
                   !-------------------------------------------------------------------------------
                   ! Symetric BC
                   !-------------------------------------------------------------------------------
                   if (j==gsy.and.i==gsx.and.k==gsz) then ! BOTTOM / LEFT / BACKWARD CORNER (1/8)
                      coef(lvs+1)  = 0
                      coef(lvs+2)  = 2*coef(lvs+2)
                      coef(lvs+3)  = 0
                      coef(lvs+4)  = 2*coef(lvs+4)
                      coef(lvs+5)  = 0
                      coef(lvs+6)  = 2*coef(lvs+6)
                   elseif (j==gsy.and.i==gex.and.k==gsz) then ! BOTTOM / RIGHT / BACKWARD CORNER (2/8)
                      coef(lvs+2)  = 0
                      coef(lvs+1)  = 2*coef(lvs+1)
                      coef(lvs+3)  = 0
                      coef(lvs+4)  = 2*coef(lvs+4)
                      coef(lvs+5)  = 0
                      coef(lvs+6)  = 2*coef(lvs+6)
                   elseif (j==gey.and.i==gsx.and.k==gsz) then ! TOP / LEFT / BACKWARD CORNER (3/8)
                      coef(lvs+1)  = 0
                      coef(lvs+2)  = 2*coef(lvs+2)
                      coef(lvs+4)  = 0
                      coef(lvs+3)  = 2*coef(lvs+3)
                      coef(lvs+5)  = 0
                      coef(lvs+6)  = 2*coef(lvs+6)
                   elseif (j==gey.and.i==gex.and.k==gsz) then ! TOP / RIGHT / BACKWARD CORNER (4/8)
                      coef(lvs+2)  = 0
                      coef(lvs+1)  = 2*coef(lvs+1)
                      coef(lvs+4)  = 0
                      coef(lvs+3)  = 2*coef(lvs+3)
                      coef(lvs+5)  = 0
                      coef(lvs+6)  = 2*coef(lvs+6)
                   elseif (j==gsy.and.i==gsx.and.k==gez) then ! BOTTOM / LEFT / FORWARD CORNER (5/8)
                      coef(lvs+1)  = 0
                      coef(lvs+2)  = 2*coef(lvs+2)
                      coef(lvs+3)  = 0
                      coef(lvs+4)  = 2*coef(lvs+4)
                      coef(lvs+6)  = 0
                      coef(lvs+5)  = 2*coef(lvs+5)
                   elseif (j==gsy.and.i==gex.and.k==gez) then ! BOTTOM / RIGHT / FORWARD CORNER (6/8)
                      coef(lvs+2)  = 0
                      coef(lvs+1)  = 2*coef(lvs+1)
                      coef(lvs+3)  = 0
                      coef(lvs+4)  = 2*coef(lvs+4)
                      coef(lvs+6)  = 0
                      coef(lvs+5)  = 2*coef(lvs+5)
                   elseif (j==gey.and.i==gsx.and.k==gez) then ! TOP / LEFT / FORWARD CORNER (7/8)
                      coef(lvs+1)  = 0
                      coef(lvs+2)  = 2*coef(lvs+2)
                      coef(lvs+4)  = 0
                      coef(lvs+3)  = 2*coef(lvs+3)
                      coef(lvs+6)  = 0
                      coef(lvs+5)  = 2*coef(lvs+5)
                   elseif (j==gey.and.i==gex.and.k==gez) then ! TOP / RIGHT / FORWARD CORNER (8/8)
                      coef(lvs+2)  = 0
                      coef(lvs+1)  = 2*coef(lvs+1)
                      coef(lvs+4)  = 0
                      coef(lvs+3)  = 2*coef(lvs+3)
                      coef(lvs+6)  = 0
                      coef(lvs+5)  = 2*coef(lvs+5)
                   elseif (k==gsz.and.i==gsx) then ! BACKWARD / LEFT EDGE (1/12)
                      coef(lvs+1)  = 0
                      coef(lvs+2)  = 2*coef(lvs+2)
                      coef(lvs+5)  = 0
                      coef(lvs+6)  = 2*coef(lvs+6)
                   elseif (k==gsz.and.i==gex) then ! BACKWARD / RIGHT EDGE (2/12)
                      coef(lvs+2)  = 0
                      coef(lvs+1)  = 2*coef(lvs+1)
                      coef(lvs+5)  = 0
                      coef(lvs+6)  = 2*coef(lvs+6)
                   elseif (k==gsz.and.j==gsy) then ! BACKWARD / BOTTOM EDGE (3/12)
                      coef(lvs+3)  = 0
                      coef(lvs+4)  = 2*coef(lvs+4)
                      coef(lvs+5)  = 0
                      coef(lvs+6)  = 2*coef(lvs+6)
                   elseif (k==gsz.and.j==gey) then ! BACKWARD / TOP EDGE (4/12)
                      coef(lvs+4)  = 0
                      coef(lvs+3)  = 2*coef(lvs+3)
                      coef(lvs+5)  = 0
                      coef(lvs+6)  = 2*coef(lvs+6)
                   elseif (i==gsx.and.j==gsy) then ! LEFT / BOTTOM EDGE (5/12)
                      coef(lvs+1)  = 0
                      coef(lvs+2)  = 2*coef(lvs+2)
                      coef(lvs+3)  = 0
                      coef(lvs+4)  = 2*coef(lvs+4)
                   elseif (i==gex.and.j==gsy) then ! RIGHT / BOTTOM EDGE (6/12)
                      coef(lvs+2)  = 0
                      coef(lvs+1)  = 2*coef(lvs+1)
                      coef(lvs+3)  = 0
                      coef(lvs+4)  = 2*coef(lvs+4)
                   elseif (i==gsx.and.j==gey) then ! LEFT / TOP EDGE (7/12)
                      coef(lvs+1)  = 0
                      coef(lvs+2)  = 2*coef(lvs+2)
                      coef(lvs+4)  = 0
                      coef(lvs+3)  = 2*coef(lvs+3)
                   elseif (i==gex.and.j==gey) then ! RIGHT / TOP EDGE (8/12)
                      coef(lvs+2)  = 0
                      coef(lvs+1)  = 2*coef(lvs+1)
                      coef(lvs+4)  = 0
                      coef(lvs+3)  = 2*coef(lvs+3)
                   elseif (k==gez.and.i==gsx) then ! FORWARD / LEFT EDGE (9/12)
                      coef(lvs+1)  = 0
                      coef(lvs+2)  = 2*coef(lvs+2)
                      coef(lvs+6)  = 0
                      coef(lvs+5)  = 2*coef(lvs+5)
                   elseif (k==gez.and.i==gex) then ! FORWARD / RIGHT EDGE (10/12)
                      coef(lvs+2)  = 0
                      coef(lvs+1)  = 2*coef(lvs+1)
                      coef(lvs+6)  = 0
                      coef(lvs+5)  = 2*coef(lvs+5)
                   elseif (k==gez.and.j==gsy) then ! FORWARD / BOTTOM EDGE (11/12)
                      coef(lvs+3)  = 0
                      coef(lvs+4)  = 2*coef(lvs+4)
                      coef(lvs+6)  = 0
                      coef(lvs+5)  = 2*coef(lvs+5)
                   elseif (k==gez.and.j==gey) then ! FORWARD / TOP EDGE (12/12)
                      coef(lvs+4)  = 0
                      coef(lvs+3)  = 2*coef(lvs+3)
                      coef(lvs+6)  = 0
                      coef(lvs+5)  = 2*coef(lvs+5)
                   elseif (i==gsx) then ! LEFT FACE (1/6)
                      coef(lvs+1)  = 0
                      coef(lvs+2)  = 2*coef(lvs+2)
                   elseif (i==gex) then ! RIGHT FACE (2/6)
                      coef(lvs+2)  = 0
                      coef(lvs+1)  = 2*coef(lvs+1)
                   elseif (j==gsy) then ! BOTTOM FACE (3/6)
                      coef(lvs+3)  = 0
                      coef(lvs+4)  = 2*coef(lvs+4)
                   elseif (j==gey) then ! TOP FACE (4/6)
                      coef(lvs+4)  = 0
                      coef(lvs+3)  = 2*coef(lvs+3)
                   elseif (k==gsz) then ! BACKWARD FACE (5/6)
                      coef(lvs+5)  = 0
                      coef(lvs+6)  = 2*coef(lvs+6)
                   elseif (k==gez) then ! FORWARD FACE (6/6)
                      coef(lvs+6)  = 0
                      coef(lvs+5)  = 2*coef(lvs+5)
                   end if
                end if
                !-------------------------------------------------------------------------------
                ! End of discretization for Pressure connvection diffusion
                !-------------------------------------------------------------------------------
             end if
             !-------------------------------------------------------------------------------
             ! next line of the matrix
             !-------------------------------------------------------------------------------
             lvs = lvs+7
          end do
       end do
    end do
    return
  end subroutine  Pressure_Convection_diffusion_Matrix_3D
  !*****************************************************************************************************
  !*****************************************************************************************************

  
  !*****************************************************************************************************
  !*****************************************************************************************************
  subroutine VOF_tension(mesh,cou,smvu,smvv,smvw,kappa_out)
    use mod_allocate
    use mod_borders
    use mod_height_function
    use mod_math
    use mod_operators
    use mod_parameters, only: NS_tension,VOF_ismooth,VOF_vsmooth
    use mod_struct_grid
    use mod_struct_thermophysics
    use mod_VOF_tools
    !-------------------------------------------------------------------------------
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable, intent(in)              :: cou
    real(8), dimension(:,:,:), allocatable, intent(inout)           :: smvu,smvv,smvw
    real(8), dimension(:,:,:), allocatable, intent(inout), optional :: kappa_out
    type(grid_t), intent(in)                                        :: mesh
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                                         :: i,j,k
    real(8)                                                         :: kappa,norm,maxi
    real(8)                                                         :: EPS_KAPPA
    real(8), dimension(:,:,:), allocatable                          :: col,kap
    real(8), dimension(:,:,:), allocatable                          :: noru,norv,norw
    real(8), dimension(:,:,:), allocatable                          :: noruu,norvu,norwu
    real(8), dimension(:,:,:), allocatable                          :: couin
    real(8), dimension(:,:,:,:), allocatable                        :: pencou
    !-------------------------------------------------------------------------------

    smvu = 0
    smvv = 0
    if (mesh%dim==3) smvw = 0

    !-------------------------------------------------------------------------------
    ! VOF surface tension force with Eulerian discrete curvature
    !-------------------------------------------------------------------------------
    
    select case(NS_tension)
    case(0) ! nothing
    case(1) ! iterative smoothing
       
       call allocate_array_3(ALLOCATE_ON_P_MESH,col,mesh)
       call allocate_array_3(ALLOCATE_ON_P_MESH,kap,mesh)
       call allocate_array_3(ALLOCATE_ON_U_MESH,noru,mesh)
       call allocate_array_3(ALLOCATE_ON_V_MESH,norv,mesh)
       call allocate_array_3(ALLOCATE_ON_W_MESH,norw,mesh)
       call allocate_array_3(ALLOCATE_ON_U_MESH,noruu,mesh)
       call allocate_array_3(ALLOCATE_ON_V_MESH,norvu,mesh)
       call allocate_array_3(ALLOCATE_ON_W_MESH,norwu,mesh)
       
       col = cou
       
       call smoothing(mesh,col,VOF_ismooth)
       
       call normal(col,noru,norv,norw,noruu,norvu,norwu)
       call diverge_vec(kap,norm,maxi,noruu,norvu,norwu)
       
       call sca_borders_and_periodicity(mesh,kap)
       call comm_mpi_sca(kap)
       
       !-------------------------------------------------------------------------------
       ! smv = sigma * kappa * n * \delta_i
       ! with
       ! n * \delta_i = grad ( cou ) 
       !-------------------------------------------------------------------------------
       
       !-------------------------------------------------------------------------------
       ! U component
       !-------------------------------------------------------------------------------
       do k = szus,ezus
          do j = syus,eyus
             do i = sxus,exus
                kappa       = (kap(i,j,k) + kap(i-1,j,k)) / 2
                smvu(i,j,k) = -fluids(1)%sigma * kappa * noru(i,j,k)
             end do
          end do
       end do
       !-------------------------------------------------------------------------------
       ! V component
       !-------------------------------------------------------------------------------
       do k = szvs,ezvs
          do j = syvs,eyvs
             do i = sxvs,exvs
                kappa       = (kap(i,j,k)+kap(i,j-1,k)) / 2
                smvv(i,j,k) = -fluids(1)%sigma * kappa * norv(i,j,k)
             end do
          end do
       end do
       !-------------------------------------------------------------------------------
       ! W component
       !-------------------------------------------------------------------------------
       if (mesh%dim==3) then
          do k = szws,ezws
             do j = syws,eyws
                do i = sxws,exws
                   kappa       = (kap(i,j,k)+kap(i,j,k-1)) / 2
                   smvw(i,j,k) = -fluids(1)%sigma * kappa * norw(i,j,k)
                end do
             end do
          end do
       end if
       !-------------------------------------------------------------------------------
       
    case(2) ! implicit smoothing with suitable diffusivity coefficient

       !--------------------------------------------------------------------
       ! VOF smoothing with diffusion equation
       ! Diffusion coefficient
       ! D = vsmooth * \Delta h * \Delta h ^ 2
       ! with
       ! vsmooth \in [0.2:2]
       !--------------------------------------------------------------------
       
       call allocate_array_3(ALLOCATE_ON_P_MESH,col,mesh)
       call allocate_array_3(ALLOCATE_ON_P_MESH,kap,mesh)
       call allocate_array_3(ALLOCATE_ON_U_MESH,noru,mesh)
       call allocate_array_3(ALLOCATE_ON_V_MESH,norv,mesh)
       call allocate_array_3(ALLOCATE_ON_W_MESH,norw,mesh)
       call allocate_array_3(ALLOCATE_ON_U_MESH,noruu,mesh)
       call allocate_array_3(ALLOCATE_ON_V_MESH,norvu,mesh)
       call allocate_array_3(ALLOCATE_ON_W_MESH,norwu,mesh)

       call allocate_array_3(ALLOCATE_ON_P_MESH,couin,mesh)

       allocate(pencou(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz,2*dim+1))
       pencou = 0
       
       col = cou
       
       call Diffusion_cou(col,VOF_vsmooth,couin,pencou)
       call sca_borders_and_periodicity(mesh,col)

       call normal(col,noru,norv,norw,noruu,norvu,norwu)
       call diverge_vec(kap,norm,maxi,noruu,norvu,norwu)
       
       call sca_borders_and_periodicity(mesh,kap)
       call comm_mpi_sca(kap)
       
       !-------------------------------------------------------------------------------
       ! smv = sigma * kappa * n * \delta_i
       ! with
       ! n * \delta_i = grad ( cou ) 
       !-------------------------------------------------------------------------------
       
       !-------------------------------------------------------------------------------
       ! U component
       !-------------------------------------------------------------------------------
       do k = szus,ezus
          do j = syus,eyus
             do i = sxus,exus
                kappa       = (kap(i,j,k) + kap(i-1,j,k)) / 2
                smvu(i,j,k) = -fluids(1)%sigma * kappa * noru(i,j,k)
             end do
          end do
       end do
       !-------------------------------------------------------------------------------
       ! V component
       !-------------------------------------------------------------------------------
       do k = szvs,ezvs
          do j = syvs,eyvs
             do i = sxvs,exvs
                kappa       = (kap(i,j,k)+kap(i,j-1,k)) / 2
                smvv(i,j,k) = -fluids(1)%sigma * kappa * norv(i,j,k)
             end do
          end do
       end do
       !-------------------------------------------------------------------------------
       ! W component
       !-------------------------------------------------------------------------------
       if (mesh%dim==3) then
          do k = szws,ezws
             do j = syws,eyws
                do i = sxws,exws
                   kappa       = (kap(i,j,k)+kap(i,j,k-1)) / 2
                   smvw(i,j,k) = -fluids(1)%sigma * kappa * norw(i,j,k)
                end do
             end do
          end do
       end if
       !-------------------------------------------------------------------------------
       
    case(3) ! height functions

       !--------------------------------------------------------------------
       ! Height function
       !--------------------------------------------------------------------

       EPS_KAPPA = 1d-14
       
       call allocate_array_3(ALLOCATE_ON_P_MESH,kap,mesh)
       call allocate_array_3(ALLOCATE_ON_U_MESH,noru,mesh)
       call allocate_array_3(ALLOCATE_ON_V_MESH,norv,mesh)
       call allocate_array_3(ALLOCATE_ON_W_MESH,norw,mesh)
       call allocate_array_3(ALLOCATE_ON_U_MESH,noruu,mesh)
       call allocate_array_3(ALLOCATE_ON_V_MESH,norvu,mesh)
       call allocate_array_3(ALLOCATE_ON_W_MESH,norwu,mesh)

       call normal(cou,noru,norv,norw,noruu,norvu,norwu)
       call HF_compute_kappa(mesh,cou,kap)
       call comm_mpi_sca(kap)

       !-------------------------------------------------------------------------------
       ! U component
       !-------------------------------------------------------------------------------
       do k = szus,ezus
          do j = syus,eyus
             do i = sxus,exus
                if (abs(kap(i,j,k))<=EPS_KAPPA) then
                   kappa = kap(i-1,j,k)
                else if (abs(kap(i-1,j,k))<=EPS_KAPPA) then
                   kappa = kap(i,j,k)
                else 
                   kappa = (kap(i,j,k) + kap(i-1,j,k)) / 2
                end if
                smvu(i,j,k) = -fluids(1)%sigma * kappa * noru(i,j,k)
             end do
          end do
       end do
       !-------------------------------------------------------------------------------
       ! V component
       !-------------------------------------------------------------------------------
       do k = szvs,ezvs
          do j = syvs,eyvs
             do i = sxvs,exvs
                if (abs(kap(i,j,k))<=EPS_KAPPA) then
                   kappa = kap(i,j-1,k)
                else if (abs(kap(i,j-1,k))<=EPS_KAPPA) then
                   kappa = kap(i,j,k)
                else 
                   kappa = (kap(i,j,k) + kap(i,j-1,k)) / 2
                end if
                smvv(i,j,k) = -fluids(1)%sigma * kappa * norv(i,j,k)
             end do
          end do
       end do
       !-------------------------------------------------------------------------------
       ! W component
       !-------------------------------------------------------------------------------
       if (mesh%dim==3) then
          do k = szws,ezws
             do j = syws,eyws
                do i = sxws,exws
                   if (abs(kap(i,j,k))<=EPS_KAPPA) then
                      kappa = kap(i,j,k-1)
                   else if (abs(kap(i,j,k-1))<=EPS_KAPPA) then
                      kappa = kap(i,j,k)
                   else 
                      kappa = (kap(i,j,k) + kap(i,j,k-1)) / 2
                   end if
                   smvw(i,j,k) = -fluids(1)%sigma * kappa * norw(i,j,k)
                end do
             end do
          end do
       end if
       !-------------------------------------------------------------------------------
       
    end select

    !-------------------------------------------------------------------------------
    ! output kappa
    !-------------------------------------------------------------------------------
    if (present(kappa_out)) then
       if (allocated(kappa_out)) then
          kappa_out = kap
       end if
    end if
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! outside nodes
    !-------------------------------------------------------------------------------
    call uvw_borders_and_periodicity(mesh,smvu,smvv,smvw)
    call comm_mpi_uvw(smvu,smvv,smvw)
    !-------------------------------------------------------------------------------
    
  end subroutine VOF_tension

  

  !*****************************************************************************************************
  !*****************************************************************************************************
  subroutine tension(cou,rho,smvu,smvv,smvw,mean_curvu,mean_curvv,kappa1)
    !*****************************************************************************************************
    !*****************************************************************************************************
    !*****************************************************************************************************
    !*****************************************************************************************************
    use mod_borders
    use mod_Parameters
    use mod_math
    use mod_operators
    use mod_struct_thermophysics
    !-------------------------------------------------------------------------------
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable, intent(in)    :: cou,rho
    real(8), dimension(:,:,:), allocatable, intent(inout) :: smvu,smvv,smvw
    real(8), dimension(:,:,:), allocatable, intent(inout) :: mean_curvu,mean_curvv
    real(8), dimension(:,:,:), allocatable, intent(inout), optional :: kappa1
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                               :: i,j,k
    real(8)                                               :: kappa,norm,curv,maxi
    real(8), dimension(:,:,:), allocatable                :: col,kap
    real(8), dimension(:,:,:), allocatable                :: noru,norv,norw
    real(8), dimension(:,:,:), allocatable                :: noruu,norvu,norwu
    logical, save                                         :: once = .true.
   !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! 2D
    !-------------------------------------------------------------------------------
    if (dim==2) then
       smvu = 0; smvv = 0
       !-------------------------------------------------------------------------------
       ! VOF surface tension force with Eulerian discrete curvature
       !-------------------------------------------------------------------------------
       if (VOF_activate) then
          allocate(col(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz),       &
               & kap(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz),         &
               & noru(sxu-gx:exu+gx,syu-gy:eyu+gy,szu-gz:ezu+gz),  &
               & norv(sxv-gx:exv+gx,syv-gy:eyv+gy,szv-gz:ezv+gz),  &
               & noruu(sxu-gx:exu+gx,syu-gy:eyu+gy,szu-gz:ezu+gz), &
               & norvu(sxv-gx:exv+gx,syv-gy:eyv+gy,szv-gz:ezv+gz))
          kap = 0
          noru = 0; norv = 0; noruu = 0; norvu = 0
          col = cou ! rho
          !-------------------------------------------------------------------------------
          if (VOF_ismooth > 0) then
             call smoothing(mesh,col,VOF_ismooth)
          end if

          call normal (col,noru,norv,norw,noruu,norvu,norwu)
          call diverge_vec_2D (kap,norm,maxi,noruu,norvu)

          if (present(kappa1)) kappa1 = kap
          
          !-------------------------------------------------------------------------------
          call sca_borders_and_periodicity(mesh,kap)
          call comm_mpi_sca(kap)
          !-------------------------------------------------------------------------------
          ! smv = sigma * kappa * n * \delta_i
          ! with
          ! n * \delta_i = grad ( cou ) 
          !-------------------------------------------------------------------------------
          ! U component
          !-------------------------------------------------------------------------------
          do j = syus,eyus
             do i = sxus,exus
                kappa = (kap(i,j,1)+kap(i-1,j,1)) / 2
                smvu(i,j,1) = -fluids(1)%sigma * kappa * noru(i,j,1)
             end do
          end do
          !-------------------------------------------------------------------------------
          ! V component
          !-------------------------------------------------------------------------------
          do j = syvs,eyvs
             do i = sxvs,exvs
                kappa = (kap(i,j,1)+kap(i,j-1,1)) / 2
                smvv(i,j,1) = -fluids(1)%sigma * kappa * norv(i,j,1)
             end do
          end do
          !-------------------------------------------------------------------------------
          deallocate(col,kap,noru,norv,noruu,norvu)
          !-------------------------------------------------------------------------------
          ! Level Set surface tension force with Eulerian discrete curvature
          !-------------------------------------------------------------------------------
       elseif (LS_activate) then
          write(*,*) 'faire tension sup Level Set'
          !-------------------------------------------------------------------------------
          ! Front Tracking Delta surface tension force with Lagrangian discrete curvature
          !-------------------------------------------------------------------------------
       elseif (FT_activate) then
          allocate(noru(sxu-gx:exu+gx,syu-gy:eyu+gy,szu-gz:ezu+gz), &
               & norv(sxv-gx:exv+gx,syv-gy:eyv+gy,szv-gz:ezv+gz),   &
               & noruu(sxu-gx:exu+gx,syu-gy:eyu+gy,szu-gz:ezu+gz),  &
               & norvu(sxv-gx:exv+gx,syv-gy:eyv+gy,szv-gz:ezv+gz))
          noru = 0; norv = 0; noruu = 0; norvu = 0
          !-------------------------------------------------------------------------------
          call normal (cou,noru,norv,norw,noruu,norvu,norwu)
          !-------------------------------------------------------------------------------
          ! U component
          !-------------------------------------------------------------------------------
          do j = syus,eyus
             do i = sxus,exus
                curv = -mean_curvu(i,j,1)
                if (abs(curv)<=1.d-12) then
                   curv = -max(mean_curvu(i-1,j,1), &
                        & mean_curvu(i+1,j,1),    &
                        & mean_curvu(i,j-1,1),    &
                        & mean_curvu(i,j+1,1))
                end if
                smvu(i,j,1) = - fluids(1)%sigma * curv * noru(i,j,1)
             end do
          end do
          !-------------------------------------------------------------------------------
          ! V component
          !-------------------------------------------------------------------------------
          do j = syvs,eyvs
             do i = sxvs,exvs
                curv = -mean_curvv(i,j,1)
                if (abs(curv)<=1.d-12) then
                   curv = -max(mean_curvv(i-1,j,1),&
                        & mean_curvv(i+1,j,1),   &
                        & mean_curvv(i,j-1,1),   &
                        & mean_curvv(i,j+1,1))
                end if
                smvv(i,j,1) = - fluids(1)%sigma * curv * norv(i,j,1)
             end do
          end do
          !-------------------------------------------------------------------------------
         ! deallocate(noru,norv,noruu,norvu)
       end if
    else
       !-------------------------------------------------------------------------------
       ! 3D
       !-------------------------------------------------------------------------------
       smvu = 0; smvv = 0; smvw = 0
       !-------------------------------------------------------------------------------
       ! VOF surface tension force with Eulerian discrete curvature
       !-------------------------------------------------------------------------------
       if (VOF_activate) then
          allocate(col(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz),       &
               & kap(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz),         &
               & noru(sxu-gx:exu+gx,syu-gy:eyu+gy,szu-gz:ezu+gz),  &
               & norv(sxv-gx:exv+gx,syv-gy:eyv+gy,szv-gz:ezv+gz),  &
               & norw(sxw-gx:exw+gx,syw-gy:eyw+gy,szw-gz:ezw+gz),  &
               & noruu(sxu-gx:exu+gx,syu-gy:eyu+gy,szu-gz:ezu+gz), &
               & norvu(sxv-gx:exv+gx,syv-gy:eyv+gy,szv-gz:ezv+gz), &
               & norwu(sxw-gx:exw+gx,syw-gy:eyw+gy,szw-gz:ezw+gz))
          kap = 0
          noru = 0; norv = 0; norw = 0; noruu = 0; norvu = 0; norwu = 0
          col = cou ! rho
          !-------------------------------------------------------------------------------
          if (VOF_ismooth > 0) then
             call smoothing(mesh,col,VOF_ismooth)
          end if

          call normal (col,noru,norv,norw,noruu,norvu,norwu)
          call diverge_vec_3D (kap,norm,maxi,noruu,norvu,norwu)
          !-------------------------------------------------------------------------------
          call sca_borders_and_periodicity(mesh,kap)
          call comm_mpi_sca(kap)
          !-------------------------------------------------------------------------------
          ! smv = sigma * kappa * n * \delta_i
          ! with
          ! n * \delta_i = grad ( cou ) 
          !-------------------------------------------------------------------------------
          ! U component
          !-------------------------------------------------------------------------------
          do k = szus,ezus
             do j = syus,eyus
                do i = sxus,exus
                   kappa = (kap(i,j,k)+kap(i-1,j,k)) / 2
                   smvu(i,j,k) = -fluids(1)%sigma * kappa * noru(i,j,k)
                end do
             end do
          end do
          !-------------------------------------------------------------------------------
          ! V component
          !-------------------------------------------------------------------------------
          do k = szvs,ezvs
             do j = syvs,eyvs
                do i = sxvs,exvs
                   kappa = (kap(i,j,k)+kap(i,j-1,k)) / 2
                   smvv(i,j,k) = -fluids(1)%sigma * kappa * norv(i,j,k)
                end do
             end do
          end do
          !-------------------------------------------------------------------------------
          ! W component
          !-------------------------------------------------------------------------------
          do k = szws,ezws
             do j = syws,eyws
                do i = sxws,exws
                   kappa = (kap(i,j,k)+kap(i,j,k-1)) / 2
                   smvw(i,j,k) = -fluids(1)%sigma * kappa * norw(i,j,k)
                end do
             end do
          end do
          !-------------------------------------------------------------------------------
          !   deallocate(col,kap,noru,norv,norw,noruu,norvu,norwu)
          !-------------------------------------------------------------------------------
          ! Level Set surface tension force with Eulerian discrete curvature
          !-------------------------------------------------------------------------------
       elseif (LS_activate) then
          write(*,*) 'faire tension sup Level Set'
          !-------------------------------------------------------------------------------
          ! Front Tracking Delta surface tension force with Lagrangian discrete curvature
          !-------------------------------------------------------------------------------
       elseif (FT_activate) then
          write(*,*) 'faire tension sup Level Set'
       end if
    end if

    !-------------------------------------------------------------------------------
    ! outside nodes
    !-------------------------------------------------------------------------------
    call uvw_borders_and_periodicity(mesh,smvu,smvv,smvw)
    call comm_mpi_uvw(smvu,smvv,smvw)
    !-------------------------------------------------------------------------------
    
  end subroutine tension
  !*****************************************************************************************************
  !*****************************************************************************************************
  !*****************************************************************************************************
  !*****************************************************************************************************

  !*****************************************************************************************************
  !*****************************************************************************************************
  !*****************************************************************************************************
  !*****************************************************************************************************
  ! Navier-Stokes equations for one fluid model in delta formulation
  !*****************************************************************************************************
  !*****************************************************************************************************
  !*****************************************************************************************************
  !*****************************************************************************************************
  subroutine Navier_Stokes_Delta (pres,pres0,u,v,w,div,rho,cou,tp, &
       & slvu,slvv,slvw,smvu,smvv,smvw,                            &
       & uin,vin,win,bip,pin,                                      &
       & u0,u1,v0,v1,w0,w1,                                        &
       & grdpu,grdpv,grdpw,dpres,                                  &
       & vie,vis,vir,xit,                                          &
       & rovu,rovv,rovw,                                           &
       & beta,tpb,                                                 &
       & penu,penv,penw,penp,                                      &
       & ns,phi,                                                   &
       & mean_curvu,mean_curvv)
    !*****************************************************************************************************
    !*****************************************************************************************************
    !*****************************************************************************************************
    !*****************************************************************************************************
    use mod_mpi
    use mod_borders
    use mod_Parameters, only: rank,nproc,                             &
         & dim,dx,dy,dz,dxu,dyv,dzw,                                  &
         & nb_V,nb_Vx,nb_Vy,nb_Vz,nb_T,nb_VP,nb_P,                    &
         & nx,ny,nz,ex,sx,ey,sy,ez,sz,gx,gy,gz,                       &
         & gsx,gex,gsy,gey,gsz,gez,                                   &
         & sxu,exu,syu,eyu,szu,ezu,                                   &
         & sxv,exv,syv,eyv,szv,ezv,                                   &
         & sxw,exw,syw,eyw,szw,ezw,                                   &
         & sxs,exs,sys,eys,szs,ezs,                                   &
         & sxus,exus,syus,eyus,szus,ezus,                             &
         & sxvs,exvs,syvs,eyvs,szvs,ezvs,                             &
         & sxws,exws,syws,eyws,szws,ezws,                             &
         & gsxu,gexu,gsyu,geyu,gszu,gezu,                             &
         & gsxv,gexv,gsyv,geyv,gszv,gezv,                             &
         & gsxw,gexw,gsyw,geyw,gszw,gezw,                             &
         & mesh,switch2D3D,                                           &
         & Euler,grav_x,grav_y,grav_z,                                &
         & coef_time_1,coef_time_2,coef_time_3,                       &
         & NS_method,NS_pressure_pen,NS_source_term,NS_linear_term,   &
         & NS_tension,NS_inertial_scheme,NS_compressible,             &
         & AL_dr,AL_it,AL_threshold,AL_method,                        &
         & NS_solver_type,NS_solver_it,NS_solver_threshold,           &
         & PJ_method,PJ_solver_type,PJ_solver_it,PJ_solver_threshold, &
         & PJ_HYPRE_precond,                                          &
         & NB_phase,NS_solver_precond,PJ_solver_precond,              &
         & EN_Boussinesq,                                             &
         & NS_HYPRE_precond,impp_nvstks,impp_poisson,impp_poisson_ksp,&
         & NS_pressure_fix, resol => nvstks_solver
    use mod_Constants, only: one,d1p2
    use mod_Connectivity
    use mod_operators
    use mod_solver_new
    use mod_struct_solver
    use mod_struct_thermophysics
    !-------------------------------------------------------------------------------
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:),   allocatable, intent(inout) :: pres,pres0,rho,div,tp
    real(8), dimension(:,:,:),   allocatable, intent(inout) :: dpres
    real(8), dimension(:,:,:),   allocatable, intent(inout) :: vie,mean_curvu,mean_curvv
    real(8), dimension(:,:,:,:), allocatable, intent(inout) :: vis,vir
    real(8), dimension(:,:,:),   allocatable, intent(inout) :: u,v,w,rovu,rovv,rovw
    real(8), dimension(:,:,:),   allocatable, intent(inout) :: beta,tpb
    real(8), dimension(:,:,:),   allocatable, intent(inout) :: u0,v0,w0,u1,v1,w1
    real(8), dimension(:,:,:),   allocatable, intent(inout) :: grdpu,grdpv,grdpw
    real(8), dimension(:,:,:),   allocatable, intent(inout) :: uin,vin,win,cou,xit
    real(8), dimension(:,:,:),   allocatable, intent(inout) :: slvu,slvv,slvw
    real(8), dimension(:,:,:),   allocatable, intent(inout) :: smvu,smvv,smvw
    real(8), dimension(:,:,:),   allocatable, intent(inout) :: bip,pin
    real(8), dimension(:,:,:,:), allocatable, intent(inout) :: penu,penv,penw
    real(8), dimension(:,:,:,:), allocatable, intent(inout) :: penp
    type(solver_ns_t), intent(inout)                        :: ns
    type(solver_sca_t), intent(inout)                       :: phi
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                                 :: i,j,k,l,lts,it_solv,la
    integer                                                 :: ii,jj,kk
    integer                                                 :: istep,jstep,kstep
    real(8)                                                 :: var,varg
    real(8)                                                 :: pe_m,pe_p,pe_l,pe_r 
    real(8)                                                 :: pe_b,pe_f 
    real(8)                                                 :: res,div_norm,ldiv_norm,div_max
    real(8), dimension(:),       allocatable                :: smc,sol
    real(8), dimension(:),       allocatable                :: smc_phi,sol_phi
    real(8), dimension(:,:,:),   allocatable                :: thetau,thetav,thetaw
    real(8), dimension(:,:,:),   allocatable                :: sknu,sknv,sknw,rep
    real(8), dimension(:,:,:),   allocatable                :: gravu,gravv,gravw
    real(8), dimension(:,:,:),   allocatable                :: var_phi,diffu_phi,diffv_phi,diffw_phi
    real(8)                                                 :: eps_slope = 1d-15
    type(system_t), target                                  :: system,system_phi
    !-------------------------------------------------------------------------------
    ! developpment local variable (will be deleted ???)
    !-------------------------------------------------------------------------------
    logical                                                 :: apply
    logical, save                                           :: once       = .true.
    !-------------------------------------------------------------------------------
    integer, save                                           :: cpt_pres0  = 0
    logical, save                                           :: once_pres0 = .true.
    !-------------------------------------------------------------------------------

    call compute_time(TIMER_START,"[sub] Navier_Stokes_Delta")

    !-------------------------------------------------------------------------------
    ! Gravity term (needed for pre-treatment in KSP methods)
    !-------------------------------------------------------------------------------
    allocate(gravu(sxu-gx:exu+gx,syu-gy:eyu+gy,szu-gz:ezu+gz))
    allocate(gravv(sxv-gx:exv+gx,syv-gy:eyv+gy,szv-gz:ezv+gz))
    if (dim==3) then
       allocate(gravw(sxw-gx:exw+gx,syw-gy:eyw+gy,szw-gz:ezw+gz))
    end if
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Hybrid and theta schemes
    !-------------------------------------------------------------------------------
    if (NS_inertial_scheme==3) then
       allocate(thetau(sxu-gx:exu+gx,syu-gy:eyu+gy,szu-gz:ezu+gz))
       allocate(thetav(sxv-gx:exv+gx,syv-gy:eyv+gy,szv-gz:ezv+gz))
       thetau = 1
       thetav = 1
       if (dim==3) then
          allocate(thetaw(sxw-gx:exw+gx,syw-gy:eyw+gy,szw-gz:ezw+gz))
          thetaw = 1
       end if
    end if
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Diffusion like coefficient for poisson eq.
    !-------------------------------------------------------------------------------
    select case(NS_method)
    case(1,2)
       allocate(diffu_phi(sxu-gx:exu+gx,syu-gy:eyu+gy,szu-gz:ezu+gz))
       allocate(diffv_phi(sxv-gx:exv+gx,syv-gy:eyv+gy,szv-gz:ezv+gz))
       if (dim==3) then
          allocate(diffw_phi(sxw-gx:exw+gx,syw-gy:eyw+gy,szw-gz:ezw+gz))
       end if
       if (abs(PJ_method)/=0) then
          allocate(var_phi(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
       end if
    end select
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Surface tension forces
    !-------------------------------------------------------------------------------
    if (NS_tension>0) then
       allocate(sknu(sxu-gx:exu+gx,syu-gy:eyu+gy,szu-gz:ezu+gz))
       allocate(sknv(sxv-gx:exv+gx,syv-gy:eyv+gy,szv-gz:ezv+gz))
       if (dim==3) allocate(sknw(sxw-gx:exw+gx,syw-gy:eyw+gy,szw-gz:ezw+gz))
       !call tension(cou,rho,sknu,sknv,sknw,mean_curvu,mean_curvv)
       call VOF_tension(mesh,cou,sknu,sknv,sknw)
    end if
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Periodicity for explicit variables 
    !-------------------------------------------------------------------------------
    call uvw_periodicity(mesh,u,v,w)
    call uvw_periodicity(mesh,grdpu,grdpv,grdpw)
    call sca_periodicity(mesh,pres)
    !-------------------------------------------------------------------------------
    
    !-------------------------------------------------------------------------------
    ! Connectitivities and Solved nodes
    !-------------------------------------------------------------------------------
    select case(NS_method)
    case(0,2)
       call Navier_Stokes_Connectivity(system%mat,mesh)
       call Navier_Stokes_kic(system%mat,mesh)
    case(1)
       call Navier_Stokes_Connectivity_Velocity_Pressure(system%mat,mesh)
       call Pressure_Convection_Diffusion_Connectivity(system%pcd,mesh)
       call Energy_Connectivity(system%schur,mesh)
       call Navier_Stokes_kic(system%mat,mesh)
    end select
    !-------------------------------------------------------------------------------
    ! alloc
    !-------------------------------------------------------------------------------
    allocate(sol(system%mat%npt))
    allocate(smc(system%mat%npt))
    !-------------------------------------------------------------------------------
    if (abs(PJ_method)/=0) then
       call Energy_Connectivity(system_phi%mat,mesh)
       call Energy_kic(system_phi%mat,mesh)
       !-------------------------------------------------------------------------------
       ! alloc
       !-------------------------------------------------------------------------------
       allocate(sol_phi(system_phi%mat%npt))
       allocate(smc_phi(system_phi%mat%npt))
       !-------------------------------------------------------------------------------
    end if
    
    !-------------------------------------------------------------------------------
    ! Augmented Lagrangian initialization
    !-------------------------------------------------------------------------------
    allocate(rep(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
    rep = 0

    rep = AL_dr
    if (AL_method==2) then
       do k = szs,ezs
          do j = sys,eys
             do i = sxs,exs
                rep(i,j,k) = rep(i,j,k)*(cou(i,j,k)*fluids(2)%rho+(1-cou(i,j,k))*fluids(1)%rho)
             end do
          end do
       end do
    end if
    !-------------------------------------------------------------------------------

    !*******************************************************************************
    ! Solving of A . U = F
    ! coef = A
    ! sol  = U
    ! smc  = F
    !*******************************************************************************

    !*******************************************************************************
    !*******************************************************************************
    !*******************************************************************************
    !*******************************************************************************
    ! Augmented Lagrangian loop for minimization procedure
    !*******************************************************************************
    !*******************************************************************************
    !*******************************************************************************
    !*******************************************************************************

    !-------------------------------------------------------------------------------
    do la = 1,AL_it
       !-------------------------------------------------------------------------------
       
       !-------------------------------------------------------------------------------
       !-------------------------------------------------------------------------------
       !-------------------------------------------------------------------------------
       ! 2D/3D Navier-Stokes solving
       !-------------------------------------------------------------------------------
       !-------------------------------------------------------------------------------
       !-------------------------------------------------------------------------------
       
       !-------------------------------------------------------------------------------
       ! Gravity term applied on velocity components
       !-------------------------------------------------------------------------------
       ! U component
       !-------------------------------------------------------------------------------
       do k = szus,ezus
          do j = syus,eyus
             do i = sxus,exus
                gravu(i,j,k) = rovu(i,j,k) * grav_x
             end do
          end do
       end do
       !-------------------------------------------------------------------------------
       ! V component
       !-------------------------------------------------------------------------------
       do k = szvs,ezvs
          do j = syvs,eyvs
             do i = sxvs,exvs
                gravv(i,j,k) = rovv(i,j,k) * grav_y
             end do
          end do
       end do
       !-------------------------------------------------------------------------------
       ! W component
       !-------------------------------------------------------------------------------
       if (mesh%dim==3) then 
          do k = szws,ezws
             do j = syws,eyws
                do i = sxws,exws
                   gravw(i,j,k) = rovw(i,j,k) * grav_z
                end do
             end do
          end do
       end if
       !-------------------------------------------------------------------------------

       !-------------------------------------------------------------------------------
       ! Boussiesq terms added
       !-------------------------------------------------------------------------------
       if (EN_Boussinesq) then 
          !-------------------------------------------------------------------------------
          ! U component
          !-------------------------------------------------------------------------------
          do k = szus,ezus
             do j = syus,eyus
                do i = sxus,exus
                   gravu(i,j,k) = gravu(i,j,k) &
                        & * ( 1 - d1p2*(beta(i,j,k)+beta(i-1,j,k)) &
                        &     * ( d1p2*(tp(i,j,k)-tpb(i,j,k)+tp(i-1,j,k)-tpb(i-1,j,k)) ) )
                end do
             end do
          end do
          !-------------------------------------------------------------------------------
          ! V component
          !-------------------------------------------------------------------------------
          do k = szvs,ezvs
             do j = syvs,eyvs
                do i = sxvs,exvs
                   gravv(i,j,k) = gravv(i,j,k) &
                        & * ( 1 - d1p2*(beta(i,j,k)+beta(i,j-1,k)) &
                        &     * ( d1p2*(tp(i,j,k)-tpb(i,j,k)+tp(i,j-1,k)-tpb(i,j-1,k)) ) )
                end do
             end do
          end do
          !-------------------------------------------------------------------------------
          ! W component
          !-------------------------------------------------------------------------------
          if (mesh%dim==3) then 
             do k = szws,ezws
                do j = syws,eyws
                   do i = sxws,exws
                      gravw(i,j,k) = gravw(i,j,k) &
                           & * ( 1 - d1p2*(beta(i,j,k)+beta(i,j,k-1)) &
                           &     * ( d1p2*(tp(i,j,k)-tpb(i,j,k)+tp(i,j,k-1)-tpb(i,j,k-1)) ) )
                   end do
                end do
             end do
          end if
          !-------------------------------------------------------------------------------
       end if
       !-------------------------------------------------------------------------------


       !-------------------------------------------------------------------------------
       ! Second member of the problem
       !-------------------------------------------------------------------------------
       smc = 0
       !-------------------------------------------------------------------------------
       ! Gravity term
       !-------------------------------------------------------------------------------
       ! U component
       !-------------------------------------------------------------------------------
       do kk = szus,ezus
          do jj = syus,eyus
             do ii = sxus,exus
                i      = ii-sxus
                j      = jj-syus
                k      = kk-szus 
                l      = i+j*(exus-sxus+1)+switch2D3D*k*(exus-sxus+1)*(eyus-syus+1)+1
                smc(l) = smc(l)+gravu(ii,jj,kk) * dxu(ii) * dy(jj) * dz(kk) 
             end do
          end do
       end do
       !-------------------------------------------------------------------------------
       ! V component
       !-------------------------------------------------------------------------------
       do kk = szvs,ezvs
          do jj = syvs,eyvs
             do ii = sxvs,exvs
                i      = ii-sxvs
                j      = jj-syvs
                k      = kk-szvs 
                l      = i+j*(exvs-sxvs+1)+switch2D3D*k*(exvs-sxvs+1)*(eyvs-syvs+1)+1+nb_Vx
                smc(l) = smc(l)+gravv(ii,jj,kk) * dx(ii) * dyv(jj) * dz(kk)
             end do
          end do
       end do
       !-------------------------------------------------------------------------------
       ! W component
       !-------------------------------------------------------------------------------
       if (mesh%dim==3) then
          do kk = szws,ezws
             do jj = syws,eyws
                do ii = sxws,exws
                   i      = ii-sxws
                   j      = jj-syws
                   k      = kk-szws
                   l      = i+j*(exws-sxws+1)+k*(exws-sxws+1)*(eyws-syws+1)+1+nb_Vx+nb_Vy
                   smc(l) = smc(l)+gravw(ii,jj,kk) * dx(ii) * dy(jj) * dzw(kk)
                end do
             end do
          end do
       end if
       !-------------------------------------------------------------------------------


       !-------------------------------------------------------------------------------
       ! Surface tension term
       !-------------------------------------------------------------------------------
       if (NS_tension>0) then
          !-------------------------------------------------------------------------------
          ! U component
          !-------------------------------------------------------------------------------
          do kk = szus,ezus
             do jj = syus,eyus
                do ii = sxus,exus
                   i      = ii-sxus
                   j      = jj-syus
                   k      = kk-szus 
                   l      = i+j*(exus-sxus+1)+switch2D3D*k*(exus-sxus+1)*(eyus-syus+1)+1
                   smc(l) = smc(l)+sknu(ii,jj,kk) * dxu(ii) * dy(jj) * dz(kk) 
                end do
             end do
          end do
          !-------------------------------------------------------------------------------
          ! V component
          !-------------------------------------------------------------------------------
          do kk = szvs,ezvs
             do jj = syvs,eyvs
                do ii = sxvs,exvs
                   i      = ii-sxvs
                   j      = jj-syvs
                   k      = kk-szvs 
                   l      = i+j*(exvs-sxvs+1)+switch2D3D*k*(exvs-sxvs+1)*(eyvs-syvs+1)+1+nb_Vx
                   smc(l) = smc(l)+sknv(ii,jj,kk) * dx(ii) * dyv(jj) * dz(kk)
                end do
             end do
          end do
          !-------------------------------------------------------------------------------
          ! W component
          !-------------------------------------------------------------------------------
          if (mesh%dim==3) then
             do kk = szws,ezws
                do jj = syws,eyws
                   do ii = sxws,exws
                      i      = ii-sxws
                      j      = jj-syws
                      k      = kk-szws
                      l      = i+j*(exws-sxws+1)+k*(exws-sxws+1)*(eyws-syws+1)+1+nb_Vx+nb_Vy
                      smc(l) = smc(l)+sknw(ii,jj,kk) * dx(ii) * dy(jj) * dzw(kk)
                   end do
                end do
             end do
          end if
       end if
       !-------------------------------------------------------------------------------

       !-------------------------------------------------------------------------------
       ! External source terms
       !-------------------------------------------------------------------------------
       if (NS_source_term) then
          !-------------------------------------------------------------------------------
          ! U component
          !-------------------------------------------------------------------------------
          do kk = szus,ezus
             do jj = syus,eyus
                do ii = sxus,exus
                   i      = ii-sxus
                   j      = jj-syus
                   k      = kk-szus 
                   l      = i+j*(exus-sxus+1)+switch2D3D*k*(exus-sxus+1)*(eyus-syus+1)+1
                   smc(l) = smc(l)+smvu(ii,jj,kk) * dxu(ii) * dy(jj) * dz(kk)
                end do
             end do
          end do
          !-------------------------------------------------------------------------------
          ! V component
          !-------------------------------------------------------------------------------
          do kk = szvs,ezvs
             do jj = syvs,eyvs
                do ii = sxvs,exvs
                   i      = ii-sxvs
                   j      = jj-syvs
                   k      = kk-szvs 
                   l      = i+j*(exvs-sxvs+1)+switch2D3D*k*(exvs-sxvs+1)*(eyvs-syvs+1)+1+nb_Vx
                   smc(l) = smc(l)+smvv(ii,jj,kk) * dx(ii) * dyv(jj) * dz(kk)
                end do
             end do
          end do
          !-------------------------------------------------------------------------------
          ! W component
          !-------------------------------------------------------------------------------
          if (mesh%dim==3) then
             do kk = szws,ezws
                do jj = syws,eyws
                   do ii = sxws,exws
                      i      = ii-sxws
                      j      = jj-syws
                      k      = kk-szws
                      l      = i+j*(exws-sxws+1)+k*(exws-sxws+1)*(eyws-syws+1)+1+nb_Vx+nb_Vy
                      smc(l) = smc(l)+smvw(ii,jj,kk) * dx(ii) * dy(jj) * dzw(kk)
                   end do
                end do
             end do
          end if
       end if
       !-------------------------------------------------------------------------------

       !-------------------------------------------------------------------------------
       ! Pressure gradient for time integration scheme
       !-------------------------------------------------------------------------------
       select case (NS_method)
       case(0,2)
          !-------------------------------------------------------------------------------
          ! U component
          !-------------------------------------------------------------------------------
          do kk = szus,ezus
             do jj = syus,eyus
                do ii = sxus,exus
                   i      = ii-sxus
                   j      = jj-syus
                   k      = kk-szus 
                   l      = i+j*(exus-sxus+1)+switch2D3D*k*(exus-sxus+1)*(eyus-syus+1)+1
                   smc(l) = smc(l)-(pres(ii,jj,kk)-pres(ii-1,jj,kk)) * dy(jj) * dz(kk)
                end do
             end do
          end do
          !-------------------------------------------------------------------------------
          ! V component
          !-------------------------------------------------------------------------------
          do kk = szvs,ezvs
             do jj = syvs,eyvs
                do ii = sxvs,exvs
                   i      = ii-sxvs
                   j      = jj-syvs
                   k      = kk-szvs 
                   l      = i+j*(exvs-sxvs+1)+switch2D3D*k*(exvs-sxvs+1)*(eyvs-syvs+1)+1+nb_Vx
                   smc(l) = smc(l)-(pres(ii,jj,kk)-pres(ii,jj-1,kk)) * dx(ii) * dz(kk)
                end do
             end do
          end do
          !-------------------------------------------------------------------------------
          ! W component
          !-------------------------------------------------------------------------------
          if (mesh%dim==3) then
             do kk = szws,ezws
                do jj = syws,eyws
                   do ii = sxws,exws
                      i      = ii-sxws
                      j      = jj-syws
                      k      = kk-szws
                      l      = i+j*(exws-sxws+1)+k*(exws-sxws+1)*(eyws-syws+1)+1+nb_Vx+nb_Vy
                      smc(l) = smc(l)-(pres(ii,jj,kk)-pres(ii,jj,kk-1)) * dx(ii) * dy(jj)
                   end do
                end do
             end do
          end if
          !-------------------------------------------------------------------------------
       end select
       !-------------------------------------------------------------------------------

       !-------------------------------------------------------------------------------
       ! Time integration for velocity time derivative
       !-------------------------------------------------------------------------------
       ! U component
       !-------------------------------------------------------------------------------
       do kk = szus,ezus
          do jj = syus,eyus
             do ii = sxus,exus
                i = ii-sxus
                j = jj-syus
                k = kk-szus 
                l = i+j*(exus-sxus+1)+switch2D3D*k*(exus-sxus+1)*(eyus-syus+1)+1
                smc(l) = smc(l)-rovu(ii,jj,kk)*coef_time_2*u0(ii,jj,kk) * dxu(ii) * dy(jj) * dz(kk)
             end do
          end do
       end do
       !-------------------------------------------------------------------------------
       ! V component
       !-------------------------------------------------------------------------------
       do kk = szvs,ezvs
          do jj = syvs,eyvs
             do ii = sxvs,exvs
                i = ii-sxvs
                j = jj-syvs
                k = kk-szvs 
                l = i+j*(exvs-sxvs+1)+switch2D3D*k*(exvs-sxvs+1)*(eyvs-syvs+1)+1+nb_Vx
                smc(l) = smc(l)-rovv(ii,jj,kk)*coef_time_2*v0(ii,jj,kk) * dx(ii) * dyv(jj) * dz(kk)
             end do
          end do
       end do
       !-------------------------------------------------------------------------------
       ! W component
       !-------------------------------------------------------------------------------
       if (mesh%dim==3) then
          do kk = szws,ezws
             do jj = syws,eyws
                do ii = sxws,exws
                   i = ii-sxws
                   j = jj-syws
                   k = kk-szws
                   l = i+j*(exws-sxws+1)+k*(exws-sxws+1)*(eyws-syws+1)+1+nb_Vx+nb_Vy
                   smc(l) = smc(l)-rovw(ii,jj,kk)*coef_time_2*w0(ii,jj,kk) * dx(ii) * dy(jj) * dzw(kk)
                end do
             end do
          end do
          !-------------------------------------------------------------------------------
          if (abs(Euler)==2) then
             !-------------------------------------------------------------------------------
             ! U component
             !-------------------------------------------------------------------------------
             do kk = szus,ezus
                do jj = syus,eyus
                   do ii = sxus,exus
                      i = ii-sxus
                      j = jj-syus
                      k = kk-szus 
                      l = i+j*(exus-sxus+1)+switch2D3D*k*(exus-sxus+1)*(eyus-syus+1)+1
                      smc(l) = smc(l)-rovu(ii,jj,kk)*coef_time_3*u1(ii,jj,kk) * dxu(ii) * dy(jj) * dz(kk)
                   end do
                end do
             end do
             !-------------------------------------------------------------------------------
             ! V component
             !-------------------------------------------------------------------------------
             do kk = szvs,ezvs
                do jj = syvs,eyvs
                   do ii = sxvs,exvs
                      i = ii-sxvs
                      j = jj-syvs
                      k = kk-szvs 
                      l = i+j*(exvs-sxvs+1)+switch2D3D*k*(exvs-sxvs+1)*(eyvs-syvs+1)+1+nb_Vx
                      smc(l) = smc(l)-rovv(ii,jj,kk)*coef_time_3*v1(ii,jj,kk) * dx(ii) * dyv(jj) * dz(kk)
                   end do
                end do
             end do
             !-------------------------------------------------------------------------------
             ! W component
             !-------------------------------------------------------------------------------
             if (mesh%dim==3) then
                do kk = szws,ezws
                   do jj = syws,eyws
                      do ii = sxws,exws
                         i = ii-sxws
                         j = jj-syws
                         k = kk-szws
                         l = i+j*(exws-sxws+1)+k*(exws-sxws+1)*(eyws-syws+1)+1+nb_Vx+nb_Vy
                         smc(l) = smc(l)-rovw(ii,jj,kk)*coef_time_3*w1(ii,jj,kk) * dx(ii) * dy(jj) * dzw(kk)
                      end do
                   end do
                end do
             end if
          end if
          !-------------------------------------------------------------------------------
       end if
       !-------------------------------------------------------------------------------
       
       !-------------------------------------------------------------------------------
       ! Boundary conditions
       !-------------------------------------------------------------------------------
       ! U component
       !-------------------------------------------------------------------------------
       do kk = szus,ezus
          do jj = syus,eyus
             do ii = sxus,exus
                i = ii-sxus
                j = jj-syus
                k = kk-szus 
                l = i+j*(exus-sxus+1)+switch2D3D*k*(exus-sxus+1)*(eyus-syus+1)+1
                smc(l) = smc(l)+penu(ii,jj,kk,1)*uin(ii,jj,kk) * dxu(ii) * dy(jj) * dz(kk) 
             end do
          end do
       end do
       !-------------------------------------------------------------------------------
       ! V component
       !-------------------------------------------------------------------------------
       do kk = szvs,ezvs
          do jj = syvs,eyvs
             do ii = sxvs,exvs
                i = ii-sxvs
                j = jj-syvs
                k = kk-szvs 
                l = i+j*(exvs-sxvs+1)+switch2D3D*k*(exvs-sxvs+1)*(eyvs-syvs+1)+1+nb_Vx
                smc(l) = smc(l)+penv(ii,jj,kk,1)*vin(ii,jj,kk) * dx(ii) * dyv(jj) * dz(kk) 
             end do
          end do
       end do
       !-------------------------------------------------------------------------------
       ! W component
       !-------------------------------------------------------------------------------
       if (mesh%dim==3) then
          do kk = szws,ezws
             do jj = syws,eyws
                do ii = sxws,exws
                   i = ii-sxws
                   j = jj-syws
                   k = kk-szws
                   l = i+j*(exws-sxws+1)+k*(exws-sxws+1)*(eyws-syws+1)+1+nb_Vx+nb_Vy
                   smc(l) = smc(l)+penw(ii,jj,kk,1)*win(ii,jj,kk) * dx(ii) * dy(jj) * dzw(kk) 
                end do
             end do
          end do
       end if
       !-------------------------------------------------------------------------------

       !-------------------------------------------------------------------------------
       ! Hybride Scheme
       !-------------------------------------------------------------------------------
       if (NS_inertial_scheme==3) then
          thetau = 1
          thetav = 1
          thetaw = 1
          !-------------------------------------------------------------------------------
          ! U component
          !-------------------------------------------------------------------------------
          do k = szus,ezus
             do j = syus,eyus
                do i = sxus,exus
                   pe_p = 1
                   pe_m = 1
                   pe_b = (u(i-1,j,k)-u(i-2,j,k))*(u(i,j,k)-u(i-1,j,k)) ! slope backward
                   if (abs(pe_b)>eps_slope) pe_m = sign(one,pe_b)
                   pe_f = (u(i+1,j,k)-u(i,j,k))*(u(i+2,j,k)-u(i+1,j,k)) ! slope forward
                   if (abs(pe_f)>eps_slope) pe_p = sign(one,pe_f)
                   if((pe_m<0).or.(pe_p<0)) thetau(i,j,k) = 0
                   pe_p = 1
                   pe_m = 1
                   pe_b = (u(i,j-1,k)-u(i,j-2,k))*(u(i,j,k)-u(i,j-1,k)) ! slope backward
                   if (abs(pe_b)>eps_slope) pe_m = sign(one,pe_b)
                   pe_f = (u(i,j+1,k)-u(i,j,k))*(u(i,j+2,k)-u(i,j+1,k)) ! slope forward
                   if (abs(pe_f)>eps_slope) pe_p = sign(one,pe_f)
                   if((pe_m<0).or.(pe_p<0)) thetau(i,j,k) = 0
                   if (mesh%dim==3) then
                      pe_p = 1
                      pe_m = 1
                      pe_b = (u(i,j,k-1)-u(i,j,k-2))*(u(i,j,k)-u(i,j,k-1)) ! slope backward
                      if (abs(pe_b)>eps_slope) pe_m = sign(one,pe_b)
                      pe_f = (u(i,j,k+1)-u(i,j,k))*(u(i,j,k+2)-u(i,j,k+1)) ! slope forward
                      if (abs(pe_f)>eps_slope) pe_p = sign(one,pe_f)
                      if((pe_m<0).or.(pe_p<0)) thetau(i,j,k) = 0
                   end if
                end do
             end do
          end do
          !-------------------------------------------------------------------------------
          ! V component
          !-------------------------------------------------------------------------------
          do k = szvs,ezvs
             do j = syvs,eyvs
                do i = sxvs,exvs
                   pe_p = 1
                   pe_m = 1
                   pe_b = (v(i-1,j,k)-v(i-2,j,k))*(v(i,j,k)-v(i-1,j,k)) ! slope backward
                   if (abs(pe_b)>eps_slope) pe_m = sign(one,pe_b)
                   pe_f = (v(i+1,j,k)-v(i,j,k))*(v(i+2,j,k)-v(i+1,j,k)) ! slope forward
                   if (abs(pe_f)>eps_slope) pe_p = sign(one,pe_f)
                   if((pe_m<0).or.(pe_p<0)) thetav(i,j,k) = 0
                   pe_p = 1
                   pe_m = 1
                   pe_b = (v(i,j-1,k)-v(i,j-2,k))*(v(i,j,k)-v(i,j-1,k)) ! slope backward
                   if (abs(pe_b)>eps_slope) pe_m = sign(one,pe_b)
                   pe_f = (v(i,j+1,k)-v(i,j,k))*(v(i,j+2,k)-v(i,j+1,k)) ! slope forward
                   if (abs(pe_f)>eps_slope) pe_p = sign(one,pe_f)
                   if((pe_m<0).or.(pe_p<0)) thetav(i,j,k) = 0
                   if (mesh%dim==3) then
                      pe_p = 1
                      pe_m = 1
                      pe_b = (v(i,j,k-1)-v(i,j,k-2))*(v(i,j,k)-v(i,j,k-1)) ! slope backward
                      if (abs(pe_b)>eps_slope) pe_m = sign(one,pe_b)
                      pe_f = (v(i,j,k+1)-v(i,j,k))*(v(i,j,k+2)-v(i,j,k+1)) ! slope forward
                      if (abs(pe_f)>eps_slope) pe_p = sign(one,pe_f)
                      if((pe_m<0).or.(pe_p<0)) thetav(i,j,k) = 0
                   end if
                end do
             end do
          end do
          !-------------------------------------------------------------------------------
          ! W component
          !-------------------------------------------------------------------------------
          if (mesh%dim==3) then
             do k = szws,ezws
                do j = syws,eyws
                   do i = sxws,exws
                      pe_p = 1
                      pe_m = 1
                      pe_b = (w(i-1,j,k)-w(i-2,j,k))*(w(i,j,k)-w(i-1,j,k)) ! slope backward
                      if (abs(pe_b)>eps_slope) pe_m = sign(one,pe_b)
                      pe_f = (w(i+1,j,k)-w(i,j,k))*(w(i+2,j,k)-w(i+1,j,k)) ! slope forward
                      if (abs(pe_f)>eps_slope) pe_p = sign(one,pe_f)
                      if((pe_m<0).or.(pe_p<0)) thetaw(i,j,k) = 0
                      pe_p = 1
                      pe_m = 1
                      pe_b = (w(i,j-1,k)-w(i,j-2,k))*(w(i,j,k)-w(i,j-1,k)) ! slope backward
                      if (abs(pe_b)>eps_slope) pe_m = sign(one,pe_b)
                      pe_f = (w(i,j+1,k)-w(i,j,k))*(w(i,j+2,k)-w(i,j+1,k)) ! slope forward
                      if (abs(pe_f)>eps_slope) pe_p = sign(one,pe_f)
                      if((pe_m<0).or.(pe_p<0)) thetaw(i,j,k) = 0
                      pe_p = 1
                      pe_m = 1
                      pe_b = (w(i,j,k-1)-w(i,j,k-2))*(w(i,j,k)-w(i,j,k-1)) ! slope backward
                      if (abs(pe_b)>eps_slope) pe_m = sign(one,pe_b)
                      pe_f = (w(i,j,k+1)-w(i,j,k))*(w(i,j,k+2)-w(i,j,k+1)) ! slope forward
                      if (abs(pe_f)>eps_slope) pe_p = sign(one,pe_f)
                      if((pe_m<0).or.(pe_p<0)) thetaw(i,j,k) = 0
                   end do
                end do
             end do
          end if
       end if
       !-------------------------------------------------------------------------------

       !-------------------------------------------------------------------------------
       ! Making the Matrix
       !-------------------------------------------------------------------------------
       if (Euler==1.or.la>1) then
          !-------------------------------------------------------------------------------
          ! U component
          !-------------------------------------------------------------------------------
          do kk = szus,ezus
             do jj = syus,eyus
                do ii = sxus,exus
                   i = ii-sxus
                   j = jj-syus
                   k = kk-szus 
                   l = i+j*(exus-sxus+1)+switch2D3D*k*(exus-sxus+1)*(eyus-syus+1)+1
                   sol(l) = u0(ii,jj,kk)
                end do
             end do
          end do
          !-------------------------------------------------------------------------------
          ! V component
          !-------------------------------------------------------------------------------
          do kk = szvs,ezvs
             do jj = syvs,eyvs
                do ii = sxvs,exvs
                   i = ii-sxvs
                   j = jj-syvs
                   k = kk-szvs 
                   l = i+j*(exvs-sxvs+1)+switch2D3D*k*(exvs-sxvs+1)*(eyvs-syvs+1)+1+nb_Vx
                   sol(l) = v0(ii,jj,kk)
                end do
             end do
          end do
          !-------------------------------------------------------------------------------
          ! W component
          !-------------------------------------------------------------------------------
          if (mesh%dim==3) then
             do kk = szws,ezws
                do jj = syws,eyws
                   do ii = sxws,exws
                      i = ii-sxws
                      j = jj-syws
                      k = kk-szws
                      l = i+j*(exws-sxws+1)+k*(exws-sxws+1)*(eyws-syws+1)+1+nb_Vx+nb_Vy
                      sol(l) = w0(ii,jj,kk)
                   end do
                end do
             end do
          end if
          !-------------------------------------------------------------------------------
          ! P component
          !-------------------------------------------------------------------------------
          if (NS_method==1) then
             do kk = szs,ezs
                do jj = sys,eys
                   do ii = sxs,exs
                      i = ii-sxs                
                      j = jj-sys
                      k = kk-szs
                      l = i+j*(exs-sxs+1)+switch2D3D*k*(exs-sxs+1)*(eys-sys+1)+1+nb_Vx+nb_Vy+nb_vz
                      sol(l) = pres(ii,jj,kk)
                   end do
                end do
             end do
          end if
          !-------------------------------------------------------------------------------
       else
          !-------------------------------------------------------------------------------
          ! U component
          !-------------------------------------------------------------------------------
          do kk = szus,ezus
             do jj = syus,eyus
                do ii = sxus,exus
                   i = ii-sxus
                   j = jj-syus
                   k = kk-szus 
                   l = i+j*(exus-sxus+1)+switch2D3D*k*(exus-sxus+1)*(eyus-syus+1)+1
                   sol(l) = 2*u0(ii,jj,kk)-u1(ii,jj,kk)
                end do
             end do
          end do
          !-------------------------------------------------------------------------------
          ! V component
          !-------------------------------------------------------------------------------
          do kk = szvs,ezvs
             do jj = syvs,eyvs
                do ii = sxvs,exvs
                   i = ii-sxvs
                   j = jj-syvs
                   k = kk-szvs 
                   l = i+j*(exvs-sxvs+1)+switch2D3D*k*(exvs-sxvs+1)*(eyvs-syvs+1)+1+nb_Vx
                   sol(l) = 2*v0(ii,jj,kk)-v1(ii,jj,kk)
                end do
             end do
          end do
          !-------------------------------------------------------------------------------
          ! W component
          !-------------------------------------------------------------------------------
          if (mesh%dim==3) then
             do kk = szws,ezws
                do jj = syws,eyws
                   do ii = sxws,exws
                      i = ii-sxws
                      j = jj-syws
                      k = kk-szws
                      l = i+j*(exws-sxws+1)+k*(exws-sxws+1)*(eyws-syws+1)+1+nb_Vx+nb_Vy
                      sol(l) = 2*w0(ii,jj,kk)-w1(ii,jj,kk)
                   end do
                end do
             end do
          end if
          !-------------------------------------------------------------------------------
          ! P component
          !-------------------------------------------------------------------------------
          if (NS_method==1) then
             do kk = szs,ezs
                do jj = sys,eys
                   do ii = sxs,exs
                      i = ii-sxs                
                      j = jj-sys
                      k = kk-szs
                      l = i+j*(exs-sxs+1)+switch2D3D*k*(exs-sxs+1)*(eys-sys+1)+1+nb_Vx+nb_Vy+nb_vz
                      sol(l) = pres(ii,jj,kk)
                   end do
                end do
             end do
          end if
          !-------------------------------------------------------------------------------
       end if
       !-------------------------------------------------------------------------------
       call Navier_Stokes_Matrix (system%mat,u,v,w,vie,vis,vir,rep,rho,rovu,rovv,rovw, &
            xit,slvu,slvv,slvw,penu,penv,penw,thetau,thetav,thetaw,ns)
       !-------------------------------------------------------------------------------

       !-------------------------------------------------------------------------------
       ! Pressure Schur operator and PCD
       !-------------------------------------------------------------------------------
       select case(NS_method)
       case(1)
          !-------------------------------------------------------------------------------
          ! U component 
          !-------------------------------------------------------------------------------
          do k = szus,ezus
             do j = syus,eyus
                do i = sxus,exus
                   !------------------------------------------------------------------------
                   ! diffusion coefficient at faces
                   !------------------------------------------------------------------------
                   diffu_phi(i,j,k) = 1d0/rovu(i,j,k)
                end do
             end do
          end do
          !-------------------------------------------------------------------------------
          ! V component 
          !-------------------------------------------------------------------------------
          do k = szvs,ezvs
             do j = syvs,eyvs
                do i = sxvs,exvs
                   !------------------------------------------------------------------------
                   ! diffusion coefficient at faces
                   !------------------------------------------------------------------------
                   diffv_phi(i,j,k) = 1d0/rovv(i,j,k)
                end do
             end do
          end do
          !-------------------------------------------------------------------------------
          ! W component 
          !-------------------------------------------------------------------------------
          if (mesh%dim==3) then 
             do k = szws,ezws
                do j = syws,eyws
                   do i = sxws,exws
                      !------------------------------------------------------------------------
                      ! diffusion coefficient at faces
                      !------------------------------------------------------------------------
                      diffw_phi(i,j,k) = 1d0/rovw(i,j,k)
                   end do
                end do
             end do
          end if
          !-------------------------------------------------------------------------------
          call PoissonSchur_Matrix(system%schur,diffu_phi,diffv_phi,diffw_phi,penp)
          call Pressure_Convection_diffusion_Matrix(system%pcd,u,v,w,ns)
          !-------------------------------------------------------------------------------
       end select
       !-------------------------------------------------------------------------------

       !-------------------------------------------------------------------------------
       ! exchange cells penalization 
       !-------------------------------------------------------------------------------
       if (nproc>1) then
#if 0
          !-------------------------------------------------------------------------------
          ! U component
          !-------------------------------------------------------------------------------
          istep = exus-sxus
          jstep = eyus-syus
          kstep = ezus-szus + (3-mesh%dim)
          do kk = szus,ezus,kstep
             apply = .false.
             if (dim==3) then
                if (kk==szus) apply = .true.
                if (kk==ezus) apply = .true.
             end if
             do jj = syus,eyus,jstep
                if (jj==syus) apply = .true.
                if (jj==eyus) apply = .true.
                do ii = sxus,exus,istep
                   if (ii==sxus) apply = .true.
                   if (ii==exus) apply = .true.
                   if (apply) then
                      i      = ii-sxus
                      j      = jj-syus
                      k      = kk-szus 
                      l      = i+j*(exus-sxus+1)+switch2D3D*k*(exus-sxus+1)*(eyus-syus+1)+1
                      smc(l) = u(ii,jj,kk)
                   end if
                end do
             end do
          end do
          !-------------------------------------------------------------------------------
          ! V component
          !-------------------------------------------------------------------------------
          istep = exvs-sxvs
          jstep = eyvs-syvs
          kstep = ezvs-szvs + (3-mesh%dim)
          do kk = szvs,ezvs,kstep
             apply = .false.
             if (dim==3) then
                if (kk==szvs) apply = .true.
                if (kk==ezvs) apply = .true.
             end if
             do jj = syvs,eyvs,jstep
                if (jj==syvs) apply = .true.
                if (jj==eyvs) apply = .true.
                do ii = sxvs,exvs,istep
                   if (ii==sxvs) apply = .true.
                   if (ii==exvs) apply = .true.
                   if (apply) then
                      i      = ii-sxvs
                      j      = jj-syvs
                      k      = kk-szvs 
                      l      = i+j*(exvs-sxvs+1)+switch2D3D*k*(exvs-sxvs+1)*(eyvs-syvs+1)+1+mesh%nb%tot%u
                      smc(l) = v(ii,jj,kk)
                   end if
                end do
             end do
          end do
          !-------------------------------------------------------------------------------
          ! W component
          !-------------------------------------------------------------------------------
          if (mesh%dim==3) then 
             istep = exws-sxws
             jstep = eyws-syws
             kstep = ezws-szws + (3-mesh%dim)
             do kk = szws,ezws,kstep
                apply = .false.
                if (dim==3) then
                   if (kk==szws) apply = .true.
                   if (kk==ezws) apply = .true.
                end if
                do jj = syws,eyws,jstep
                   if (jj==syws) apply = .true.
                   if (jj==eyws) apply = .true.
                   do ii = sxws,exws,istep
                      if (ii==sxws) apply = .true.
                      if (ii==exws) apply = .true.
                      if (apply) then
                         i      = ii-sxws
                         j      = jj-syws
                         k      = kk-szws 
                         l      = i+j*(exws-sxws+1)+switch2D3D*k*(exws-sxws+1)*(eyws-syws+1)+1+mesh%nb%tot%uv
                         smc(l) = w(ii,jj,kk)
                      end if
                   end do
                end do
             end do
          end if
          !-------------------------------------------------------------------------------
          ! P component
          !-------------------------------------------------------------------------------
          if (NS_method==1) then
             istep = exs-sxs
             jstep = eys-sys
             kstep = ezs-szs + (3-mesh%dim)
             do kk = szs,ezs,kstep
                apply = .false.
                if (dim==3) then
                   if (kk==szs) apply = .true.
                   if (kk==ezs) apply = .true.
                end if
                do jj = sys,eys,jstep
                   if (jj==sys) apply = .true.
                   if (jj==eys) apply = .true.
                   do ii = sxs,exs,istep
                      if (ii==sxs) apply = .true.
                      if (ii==exs) apply = .true.
                      if (apply) then
                         i      = ii-sxs
                         j      = jj-sys
                         k      = kk-szs 
                         l      = i+j*(exs-sxs+1)+switch2D3D*k*(exs-sxs+1)*(eys-sys+1)+1+mesh%nb%tot%uvw
                         smc(l) = pres(ii,jj,kk)
                      end if
                   end do
                end do
             end do
          end if
          !-------------------------------------------------------------------------------
#else
          if (mesh%dim==2) then
             !-------------------------------------------------------------------------------
             ! U component
             !-------------------------------------------------------------------------------
             do jj=syus,eyus
                do ii=sxus,exus
                   if (ii==sxus.or.ii==exus.or.jj==syus.or.jj==eyus) then 
                      i=ii-sxus
                      j=jj-syus
                      l=i+j*(exus-sxus+1)+1
                      smc(l)=u(ii,jj,1)
                   end if
                end do
             end do
             !-------------------------------------------------------------------------------
             ! V component
             !-------------------------------------------------------------------------------
             do jj=syvs,eyvs
                do ii=sxvs,exvs
                   if (ii==sxvs.or.ii==exvs.or.jj==syvs.or.jj==eyvs) then 
                      i=ii-sxvs
                      j=jj-syvs
                      l=i+j*(exvs-sxvs+1)+1 + nb_Vx
                      smc(l)=v(ii,jj,1)
                   end if
                end do
             end do
             !-------------------------------------------------------------------------------
             ! P component
             !-------------------------------------------------------------------------------
             if (NS_method==1) then
                do jj=sys,eys
                   do ii=sxs,exs
                      if (ii==sxs.or.ii==exs.or.jj==sys.or.jj==eys) then 
                         i=ii-sxs
                         j=jj-sys
                         l=i+j*(exs-sxs+1)+1 + nb_Vx + nb_Vy
                         smc(l)=pres(ii,jj,1)
                      end if
                   end do
                end do
             endif
             !-------------------------------------------------------------------------------
          else
             !-------------------------------------------------------------------------------
             ! U component
             !-------------------------------------------------------------------------------
             do kk=szus,ezus 
                do jj=syus,eyus
                   do ii=sxus,exus
                      if (ii==sxus.or.ii==exus.or.jj==syus.or.jj==eyus.or.kk==szus.or.kk==ezus) then 
                         i=ii-sxus
                         j=jj-syus
                         k=kk-szus 
                         l=i+j*(exus-sxus+1)+k*(exus-sxus+1)*(eyus-syus+1)+1
                         smc(l)=u(ii,jj,kk)
                      end if
                   end do
                end do
             end do
             !-------------------------------------------------------------------------------
             ! V component
             !-------------------------------------------------------------------------------
             do kk=szvs,ezvs
                do jj=syvs,eyvs
                   do ii=sxvs,exvs
                      if (ii==sxvs.or.ii==exvs.or.jj==syvs.or.jj==eyvs.or.kk==szvs.or.kk==ezvs) then 
                         i=ii-sxvs
                         j=jj-syvs
                         k=kk-szvs 
                         l=i+j*(exvs-sxvs+1)+k*(exvs-sxvs+1)*(eyvs-syvs+1)+1+nb_Vx
                         smc(l)=v(ii,jj,kk)
                      end if
                   end do
                end do
             end do
             !-------------------------------------------------------------------------------
             ! W component
             !-------------------------------------------------------------------------------
             do kk=szws,ezws
                do jj=syws,eyws
                   do ii=sxws,exws
                      if (ii==sxws.or.ii==exws.or.jj==syws.or.jj==eyws.or.kk==szws.or.kk==ezws) then
                         i=ii-sxws
                         j=jj-syws
                         k=kk-szws
                         l=i+j*(exws-sxws+1)+k*(exws-sxws+1)*(eyws-syws+1)+1+nb_Vx+nb_Vy
                         smc(l)=w(ii,jj,kk)
                      end if
                   end do
                end do
             end do
             !-------------------------------------------------------------------------------
             ! P component
             !-------------------------------------------------------------------------------
             if (NS_method==1) then
                do kk=szs,ezs
                   do jj=sys,eys
                      do ii=sxs,exs
                         if (ii==sxs.or.ii==exs.or.jj==sys.or.jj==eys.or.kk==szs.or.kk==ezs) then
                            i=ii-sxs
                            j=jj-sys
                            k=kk-szs
                            l=i+j*(exs-sxs+1)+k*(exs-sxs+1)*(eys-sys+1)+1+nb_Vx+nb_Vy+nb_Vz
                            smc(l)=pres(ii,jj,kk)
                         end if
                      end do
                   end do
                end do
             endif
             !-------------------------------------------------------------------------------
          end if
#endif 
       end if
       !-------------------------------------------------------------------------------

       !-------------------------------------------------------------------------------
       ! Solving the linear system
       !-------------------------------------------------------------------------------

       resol%solver%type    = NS_solver_type
       resol%solver%precond = NS_solver_precond
       resol%solver%res_max = NS_solver_threshold
       resol%solver%it_max  = NS_solver_it
       system%mat%impp      = impp_nvstks
       
       if (NS_method==1) then
          resol%solver%eqs     = 1230*(mesh%dim-2)+120*(3-mesh%dim)
          system%mat%eqs       = resol%solver%eqs
          system%array3d_1     = vie
       else
          resol%solver%eqs     = 123*(mesh%dim-2)+12*(3-mesh%dim)
          system%mat%eqs       = resol%solver%eqs
       end if
       
       call solve_linear_system(system,smc,sol,resol%solver,mesh)
       
       deallocate(system%mat%coef)
       deallocate(system%mat%icof)
       deallocate(system%mat%jcof)
       deallocate(system%mat%kic)

       !-------------------------------------------------------------------------------
       ! Saving solving parameters
       !-------------------------------------------------------------------------------
       ns%output%it_lag         = la
       ns%output%res_solver(la) = resol%solver%res
       ns%output%it_solver(la)  = resol%solver%it
       !-------------------------------------------------------------------------------

       !-------------------------------------------------------------------------------
       ! The new velocity solution is updated
       !-------------------------------------------------------------------------------
       ! U component
       !-------------------------------------------------------------------------------
       do kk = szus,ezus
          do jj = syus,eyus
             do ii = sxus,exus
                i = ii-sxus
                j = jj-syus
                k = kk-szus 
                l = i+j*(exus-sxus+1)+switch2D3D*k*(exus-sxus+1)*(eyus-syus+1)+1
                u(ii,jj,kk) = sol(l)
             end do
          end do
       end do
       !-------------------------------------------------------------------------------
       ! V component
       !-------------------------------------------------------------------------------
       do kk = szvs,ezvs
          do jj = syvs,eyvs
             do ii = sxvs,exvs
                i = ii-sxvs
                j = jj-syvs
                k = kk-szvs 
                l = i+j*(exvs-sxvs+1)+switch2D3D*k*(exvs-sxvs+1)*(eyvs-syvs+1)+1+nb_Vx
                v(ii,jj,kk) = sol(l)
             end do
          end do
       end do
       !-------------------------------------------------------------------------------
       ! W component
       !-------------------------------------------------------------------------------
       if (mesh%dim==3) then
          do kk = szws,ezws
             do jj = syws,eyws
                do ii = sxws,exws
                   i = ii-sxws
                   j = jj-syws
                   k = kk-szws
                   l = i+j*(exws-sxws+1)+k*(exws-sxws+1)*(eyws-syws+1)+1+nb_Vx+nb_Vy
                   w(ii,jj,kk) = sol(l)
                end do
             end do
          end do
       end if
       !-------------------------------------------------------------------------------
       ! P component
       !-------------------------------------------------------------------------------
       if (NS_method==1) then
          do kk = szs,ezs
             do jj = sys,eys
                do ii = sxs,exs
                   i = ii-sxs                
                   j = jj-sys
                   k = kk-szs
                   l = i+j*(exs-sxs+1)+switch2D3D*k*(exs-sxs+1)*(eys-sys+1)+1+nb_Vx+nb_Vy+nb_Vz
                   pres(ii,jj,kk) = sol(l)
                end do
             end do
          end do
       end if
       !-------------------------------------------------------------------------------

       if (nproc>1) then
          call comm_mpi_uvw(u,v,w)
          if (NS_method==1) call comm_mpi_sca(pres)
       end if

       if (PJ_method/=0) then

          !-------------------------------------------------------------------------------
          ! prediction step divergence
          !-------------------------------------------------------------------------------
          if (mesh%dim==2) then
             call diverge_vec_2D(div,div_norm,div_max,u,v)
          else 
             call diverge_vec_3D(div,div_norm,div_max,u,v,w)
          end if
          if (nproc>1) call comm_mpi_sca(div)
          call NS_scal_boundary(div,ns)
          ldiv_norm = 0
          do k = sz,ez
             do j = sy,ey
                do i = sx,ex
                   ldiv_norm = ldiv_norm+div(i,j,k)**2*dx(i)*dy(j)*dz(k)
                end do
             end do
          end do
          call mpi_allreduce(ldiv_norm,div_norm,1,mpi_double_precision,mpi_sum,comm3d,code)
          phi%output%div(la) = sqrt(div_norm)
          phi%output%divmax(la) = div_max
          !-------------------------------------------------------------------------------

          !-------------------------------------------------------------------------------
          ! build smc_phi
          !-------------------------------------------------------------------------------
          smc_phi = 0
          !-------------------------------------------------------------------------------
          
          !-------------------------------------------------------------------------------
          ! div(u*)
          !-------------------------------------------------------------------------------
          do kk = szs,ezs
             do jj = sys,eys
                do ii = sxs,exs
                   i = ii-sxs
                   j = jj-sys
                   k = kk-szs
                   l = i+j*(exs-sxs+1)+switch2D3D*k*(exs-sxs+1)*(eys-sys+1)+1
                   smc_phi(l) = smc_phi(l) + (u(ii+1,jj,kk)-u(ii,jj,kk)) * dy(jj) * dz(kk)
                   smc_phi(l) = smc_phi(l) + (v(ii,jj+1,kk)-v(ii,jj,kk)) * dx(ii) * dz(kk)
                   if (mesh%dim==3) smc_phi(l) = smc_phi(l) + (w(ii,jj,kk+1)-w(ii,jj,kk)) * dx(ii) * dy(jj)
                end do
             end do
          end do
          !-------------------------------------------------------------------------------

          !-------------------------------------------------------------------------------
          ! boundary conditions
          !-------------------------------------------------------------------------------
          do kk = szs,ezs
             do jj = sys,eys
                do ii = sxs,exs
                   i = ii-sxs
                   j = jj-sys
                   k = kk-szs
                   l = i+j*(exs-sxs+1)+switch2D3D*k*(exs-sxs+1)*(eys-sys+1)+1
                   smc_phi(l) = smc_phi(l) + penp(ii,jj,kk,1) * pin(ii,jj,kk) * dx(ii) * dy(jj) * dz(kk)
                end do
             end do
          end do
          !-------------------------------------------------------------------------------

          !-------------------------------------------------------------------------------
          select case(abs(PJ_method))
          case(1,10,11)
             !-------------------------------------------------------------------------------
             ! Scalar projection 
             !-------------------------------------------------------------------------------
             ! U component 
             !-------------------------------------------------------------------------------
             do k = szus,ezus
                do j = syus,eyus
                   do i = sxus,exus
                      !------------------------------------------------------------------------
                      ! diffusion coefficient at faces
                      !------------------------------------------------------------------------
                      diffu_phi(i,j,k) = rovu(i,j,k)*coef_time_1*ns%dt
                      if (NS_linear_term) then
                         diffu_phi(i,j,k) = diffu_phi(i,j,k)+ns%dt*slvu(i,j,k)
                      end if
                      diffu_phi(i,j,k) = ns%dt/diffu_phi(i,j,k)
                   end do
                end do
             end do
             !-------------------------------------------------------------------------------
             ! V component 
             !-------------------------------------------------------------------------------
             do k = szvs,ezvs
                do j = syvs,eyvs
                   do i = sxvs,exvs
                      !------------------------------------------------------------------------
                      ! diffusion coefficient at faces
                      !------------------------------------------------------------------------
                      diffv_phi(i,j,k) = rovv(i,j,k)*coef_time_1*ns%dt
                      if (NS_linear_term) then
                         diffv_phi(i,j,k) = diffv_phi(i,j,k)+ns%dt*slvv(i,j,k)
                      end if
                      diffv_phi(i,j,k) = ns%dt/diffv_phi(i,j,k)
                   end do
                end do
             end do
             !-------------------------------------------------------------------------------
             ! W component 
             !-------------------------------------------------------------------------------
             if (mesh%dim==3) then
                do k = szws,ezws
                   do j = syws,eyws
                      do i = sxws,exws
                         !------------------------------------------------------------------------
                         ! diffusion coefficient at faces
                         !------------------------------------------------------------------------
                         diffw_phi(i,j,k) = rovw(i,j,k)*coef_time_1*ns%dt
                         if (NS_linear_term) then
                            diffw_phi(i,j,k) = diffw_phi(i,j,k)+ns%dt*slvw(i,j,k)
                         end if
                         diffw_phi(i,j,k) = ns%dt/diffw_phi(i,j,k)
                      end do
                   end do
                end do
             end if
             !-------------------------------------------------------------------------------
          case(2,20,21,3,30,31)
             !-------------------------------------------------------------------------------
             ! Kinetic Scalar projection 
             !-------------------------------------------------------------------------------
             diffu_phi = 1
             diffv_phi = 1
             diffw_phi = 1
             !-------------------------------------------------------------------------------
          case DEFAULT
             write(*,*) "this projection method does not exist"
             write(*,*) "stop"
             stop
          end select

          !-------------------------------------------------------------------------------
          ! Making the Matrix (Poisson Problem)
          !-------------------------------------------------------------------------------
          call Poisson_Matrix(system_phi%mat,diffu_phi,diffv_phi,diffw_phi,penp,phi)
          !-------------------------------------------------------------------------------

          !-------------------------------------------------------------------------------
          ! old pressure delta in pres0
          !-------------------------------------------------------------------------------
          if (once_pres0) then
             call random_number(sol_phi)
             once_pres0 = .false.
          else 
             do kk = szs,ezs
                do jj = sys,eys
                   do ii = sxs,exs
                      i = ii-sxs
                      j = jj-sys
                      k = kk-szs
                      l = i+j*(exs-sxs+1)+switch2D3D*k*(exs-sxs+1)*(eys-sys+1)+1
                      sol_phi(l) = dpres(ii,jj,kk)
                   end do
                end do
             end do
          end if
          !-------------------------------------------------------------------------------

          !-------------------------------------------------------------------------------
          ! Solving the linear system
          !-------------------------------------------------------------------------------

          resol%projection%solver%type    = PJ_solver_type
          resol%projection%solver%eqs     = 0
          resol%projection%solver%precond = PJ_solver_precond
          resol%projection%solver%res_max = PJ_solver_threshold
          resol%projection%solver%it_max  = PJ_solver_it
          
          system%mat%eqs = resol%projection%solver%eqs
          
          call solve_linear_system(system_phi,smc_phi,sol_phi,resol%projection%solver,mesh)

          deallocate(system_phi%mat%coef)
          deallocate(system_phi%mat%icof)
          deallocate(system_phi%mat%jcof)
          deallocate(system_phi%mat%kic)

          if (resol%projection%solver%it >= resol%projection%solver%it_max) then
             cpt_pres0 = cpt_pres0 + 1
             if (cpt_pres0 > 5) then
                once_pres0 = .true.
                cpt_pres0  = 0
             end if
          end if

          !-------------------------------------------------------------------------------
          ! Saving solving parameters
          !-------------------------------------------------------------------------------
          phi%output%it_lag         = la
          phi%output%res_solver(la) = resol%projection%solver%res
          phi%output%it_solver(la)  = resol%projection%solver%it
          !-------------------------------------------------------------------------------

          !-------------------------------------------------------------------------------
          ! The solution phi is updated
          !-------------------------------------------------------------------------------
          var_phi = 0
          do kk = szs,ezs
             do jj = sys,eys
                do ii = sxs,exs
                   i = ii-sxs
                   j = jj-sys
                   k = kk-szs 
                   l = i+j*(exs-sxs+1)+switch2D3D*k*(exs-sxs+1)*(eys-sys+1)+1
                   var_phi(ii,jj,kk) = sol_phi(l)
                end do
             end do
          end do
          !-------------------------------------------------------------------------------

          !-------------------------------------------------------------------------------
          ! Construction of outside points
          !-------------------------------------------------------------------------------
          if (nproc==1) then
             !-------------------------------------------------------------------------------
             ! X-direction
             !-------------------------------------------------------------------------------
             do i = sx,ex
                if (i==gsx) then 
                   if (phi%bound%left==4) then
                      var_phi(i-1,:,:) = var_phi(i-1+gex,:,:)
                   elseif (phi%bound%left==1) then
                      var_phi(i-1,:,:) = var_phi(i-1+2,:,:)
                   else
                      stop 
                   end if
                elseif (i==gex) then 
                   if (phi%bound%right==4) then
                      var_phi(i+1,:,:) = var_phi(i+1-gex,:,:)
                   elseif (phi%bound%right==1) then
                      var_phi(i+1,:,:) = var_phi(i+1-2,:,:)
                   else
                      stop 
                   end if
                end if
             end do
             !-------------------------------------------------------------------------------
             ! Y-direction
             !-------------------------------------------------------------------------------
             do j = sy,ey 
                if (j==gsy) then
                   if (phi%bound%bottom==4) then
                      var_phi(:,j-1,:) = var_phi(:,j-1+gey,:)
                   elseif (phi%bound%bottom==1) then
                      var_phi(:,j-1,:) = var_phi(:,j-1+2,:)
                   else
                      stop 
                   end if
                elseif (j==gey) then 
                   if (phi%bound%top==4) then
                      var_phi(:,j+1,:) = var_phi(:,j+1-gey,:)
                   elseif (phi%bound%top==1) then
                      var_phi(:,j+1,:) = var_phi(:,j+1-2,:)
                   else
                      stop 
                   end if
                end if
             end do
             !-------------------------------------------------------------------------------
             ! Z-direction
             !-------------------------------------------------------------------------------
             if (mesh%dim==3) then 
                do k = sz,ez
                   if (k==gsz) then 
                      if (phi%bound%backward==4) then 
                         var_phi(:,:,k-1) = var_phi(:,:,k-1+gez)
                      elseif (phi%bound%backward==1) then
                         var_phi(:,:,k-1) = var_phi(:,:,k-1+2)
                      else
                         stop
                      end if
                   elseif (k==gez) then 
                      if (phi%bound%forward==4) then 
                         var_phi(:,:,k+1) = var_phi(:,:,k+1-gez)
                      elseif (phi%bound%forward==1) then
                         var_phi(:,:,k+1) = var_phi(:,:,k+1-2)
                      else
                         stop
                      end if
                   end if
                end do
             end if
                !-------------------------------------------------------------------------------
          else
             call comm_mpi_sca(var_phi)
             !-------------------------------------------------------------------------------
             ! X-direction
             !-------------------------------------------------------------------------------
             do i = sx,ex
                if (i==gsx) then 
                   if (phi%bound%left==4) then
                   elseif (phi%bound%left==1) then
                      var_phi(i-1,:,:) = var_phi(i-1+2,:,:)
                   else
                      stop 
                   end if
                elseif (i==gex) then 
                   if (phi%bound%right==4) then
                   elseif (phi%bound%right==1) then
                      var_phi(i+1,:,:) = var_phi(i+1-2,:,:)
                   else
                      stop 
                   end if
                end if
             end do
             !-------------------------------------------------------------------------------
             ! Y-direction
             !-------------------------------------------------------------------------------
             do j = sy,ey 
                if (j==gsy) then
                   if (phi%bound%bottom==4) then
                   elseif (phi%bound%bottom==1) then
                      var_phi(:,j-1,:) = var_phi(:,j-1+2,:)
                   else
                      stop 
                   end if
                elseif (j==gey) then 
                   if (phi%bound%top==4) then
                   elseif (phi%bound%top==1) then
                      var_phi(:,j+1,:) = var_phi(:,j+1-2,:)
                   else
                      stop 
                   end if
                end if
             end do
             !-------------------------------------------------------------------------------
             ! Z-direction
             !-------------------------------------------------------------------------------
             if (mesh%dim==3) then 
                do k = sz,ez
                   if (k==gsz) then 
                      if (phi%bound%backward==4) then
                      elseif (phi%bound%backward==1) then
                         var_phi(:,:,k-1) = var_phi(:,:,k-1+2)
                      else
                         stop
                      end if
                   elseif (k==gez) then 
                      if (phi%bound%forward==4) then
                      elseif (phi%bound%forward==1) then
                         var_phi(:,:,k+1) = var_phi(:,:,k+1-2)
                      else
                         stop
                      end if
                   end if
                end do
             end if
             !-------------------------------------------------------------------------------
          end if
          !-------------------------------------------------------------------------------

          !-------------------------------------------------------------------------------
          ! pressure correction --> in pres0
          !-------------------------------------------------------------------------------
          select case(abs(PJ_method))
          case(1,10,11)
             !-------------------------------------------------------------------------------
             ! SP
             !-------------------------------------------------------------------------------
             pres0 = var_phi
             !-------------------------------------------------------------------------------
          end select

          !-------------------------------------------------------------------------------
          ! pressure correction equation 
          !-------------------------------------------------------------------------------
          dpres = pres0
          pres  = pres+dpres
          !-------------------------------------------------------------------------------
          select case(NS_pressure_fix)
          case(2)
             !-------------------------------------------------------------------------------
             ! mean pressure (~approx)
             !-------------------------------------------------------------------------------
             var = sum(pres(sx:ex,sy:ey,sz:ez))
             call MPI_ALLREDUCE(var,varg,1,MPI_DOUBLE_PRECISION,MPI_SUM,COMM3D,MPI_CODE)
             varg = varg/(gex-gsx)/(gey-gsy)
             if (mesh%dim==3) varg = varg/(gez-gsz)
             !-------------------------------------------------------------------------------
             ! p = p - <p>
             !-------------------------------------------------------------------------------
             pres = pres-varg
             !pres = pres-pres(1,1,1)
             write(*,*) "sum(pres)",sum(pres)
             write(*,*) "mean(pres)",varg
             !-------------------------------------------------------------------------------
          end select
          !-------------------------------------------------------------------------------

          !-------------------------------------------------------------------------------
          ! velocity correction
          !-------------------------------------------------------------------------------
          ! U component
          !-------------------------------------------------------------------------------
          do k = szus,ezus
             do j = syus,eyus
                do i = sxus,exus
                   u(i,j,k) = u(i,j,k)-diffu_phi(i,j,k)*(var_phi(i,j,k)-var_phi(i-1,j,k))/dxu(i)
                end do
             end do
          end do
          !-------------------------------------------------------------------------------
          ! V component
          !-------------------------------------------------------------------------------
          do k = szvs,ezvs
             do j = syvs,eyvs
                do i = sxvs,exvs
                   v(i,j,k) = v(i,j,k)-diffv_phi(i,j,k)*(var_phi(i,j,k)-var_phi(i,j-1,k))/dyv(j)
                end do
             end do
          end do
          !-------------------------------------------------------------------------------
          ! W component
          !-------------------------------------------------------------------------------
          if (mesh%dim==3) then
             do k = szws,ezws
                do j = syws,eyws
                   do i = sxws,exws
                      w(i,j,k) = w(i,j,k)-diffw_phi(i,j,k)*(var_phi(i,j,k)-var_phi(i,j,k-1))/dzw(k)
                   end do
                end do
             end do
          end if
          call comm_mpi_uvw(u,v,w)
       end if

       !=============================================================================== 
       ! End of solving
       !=============================================================================== 

       !-------------------------------------------------------------------------------
       if (mesh%dim==2) then
          call diverge_vec_2D(div,div_norm,div_max,u,v)
       else 
          call diverge_vec_3D(div,div_norm,div_max,u,v,w)
       end if
       call comm_mpi_sca(div)
       call NS_scal_boundary(div,ns)
       ldiv_norm = 0
       do k = sz,ez
          do j = sy,ey
             do i = sx,ex
                ldiv_norm = ldiv_norm+div(i,j,k)**2*dx(i)*dy(j)*dz(k)
             end do
          end do
       end do
       call mpi_allreduce(ldiv_norm,div_norm,1,mpi_double_precision,mpi_sum,comm3d,code)
       div_norm = sqrt(div_norm)
       !-------------------------------------------------------------------------------

       !-------------------------------------------------------------------------------
       ! Pressure estimate
       !-------------------------------------------------------------------------------
       select case (NS_method)
       case(0,2)
          if (PJ_method==0) then
             pres = pres - rep * div
             if (NS_pressure_pen) then
                pres = (pres + bip * pin)/(1 + bip)
             end if
             call NS_scal_boundary(pres,ns)
          end if
       end select
       !-------------------------------------------------------------------------------
       
       !-------------------------------------------------------------------------------
       ! Saving solving parameters
       !-------------------------------------------------------------------------------
       ns%output%div(la) = div_norm
       ns%output%divmax(la) = div_max
       if (div_norm < AL_threshold) exit
       !-------------------------------------------------------------------------------

       if (PJ_method>0) exit 
       
    end do
    !-------------------------------------------------------------------------------
    !*******************************************************************************
    !*******************************************************************************
    !*******************************************************************************
    !*******************************************************************************
    !  End of Augmented Lagrangian loop for minimization procedure
    !*******************************************************************************
    !*******************************************************************************
    !*******************************************************************************
    !*******************************************************************************

    call compute_time(TIMER_END,"[sub] Navier_Stokes_Delta")
    
  end subroutine Navier_Stokes_Delta
  !*******************************************************************************

  subroutine NS_print (nt,ns,phi)
    !*****************************************************************************************************
    use mod_struct_solver
    use mod_struct_solver
    use mod_Parameters, only: rank,         &
         & NS_solver_type,PJ_method,        &
         & PJ_solver_type,AL_method,        &
         & AL_dr,AL_comp,                   &
         & NS_method,NS_HYPRE_precond,      &
         & PJ_HYPRE_precond
    !-------------------------------------------------------------------------------
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)            :: nt
    type(solver_ns_t), intent(in)  :: ns
    type(solver_sca_t), intent(in) :: phi
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                        :: l
    !-------------------------------------------------------------------------------

    if (rank==0) then 
       write(*,*) ''//achar(27)//'[34m======================Navier-Stokes============================ '//achar(27)//'[0m'
       select case(NS_method)
       case(0:2)
          write(*,103) 'Velocity divergence:', &
               & '||div(v)||_2 = ',ns%output%div(1), &
               & '| Max|div(v)| = ',ns%output%divmax(1)

       end select
    end if

101 format(a,1x,i4)
102 format(a,1x,1pe15.8)
103 format(a,1x,999(a,1pe12.5,1x))

    !*****************************************************************************************************
  end subroutine NS_print


  !*****************************************************************************************************
  subroutine NS_print2(nt,mesh,u,v,w,p,cou)
    use mod_allocate
    use mod_mpi
    use mod_parameters, only: rank
    use mod_struct_grid
    use mod_struct_solver
    !-------------------------------------------------------------------------------
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)                                :: nt
    real(8), dimension(:,:,:), allocatable, intent(in) :: u,v,w,p,cou
    type(grid_t), intent(in)                           :: mesh
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                            :: i,j,k
    real(8)                                            :: div,div0,div1
    real(8)                                            :: max_p,max_u,max_v,max_w
    real(8)                                            :: max_p0,max_u0,max_v0,max_w0
    real(8)                                            :: max_p1,max_u1,max_v1,max_w1
    real(8)                                            :: max_div,max_div0,max_div1
    real(8)                                            :: vol,vol0,vol1
    real(8)                                            :: dvol
    real(8)                                            :: tmp
    real(8), dimension(:,:,:), allocatable             :: us,vs,ws
    logical                                            :: two_phases=.false.
    !-------------------------------------------------------------------------------

    if (allocated(cou)) two_phases = .true.

    call allocate_array_3(ALLOCATE_ON_P_MESH,us,mesh)
    call allocate_array_3(ALLOCATE_ON_P_MESH,vs,mesh)
    call allocate_array_3(ALLOCATE_ON_P_MESH,ws,mesh)

    div  = 0
    div0 = 0
    div1 = 0

    vol  = 0
    vol0 = 0
    vol1 = 0
    
    max_div  = 0
    max_div0 = 0
    max_div1 = 0
    
    if (mesh%dim == 2) then
       
       do j = mesh%sy,mesh%ey
          do i = mesh%sx,mesh%ex
             
             dvol = mesh%dx(i)*mesh%dy(j)
             
             tmp =    (u(i+1,j,1)-u(i,j,1)) * mesh%dy(j) &
                  & + (v(i,j+1,1)-v(i,j,1)) * mesh%dx(i)

             us(i,j,1) = (mesh%dxu2(i+1)*u(i,j,1)+mesh%dxu2(i)*u(i+1,j,1))/mesh%dx(i) 
             vs(i,j,1) = (mesh%dyv2(j+1)*v(i,j,1)+mesh%dyv2(j)*v(i,j+1,1))/mesh%dy(j) 
             
             div  = div + tmp**2 * dvol
             vol  = vol + dvol
             if (abs(tmp) > max_div) max_div = abs(tmp)

             if (two_phases) then
                if (cou(i,j,1)>0.5d0) then
                   div1 = div1 + tmp**2 * dvol
                   vol1 = vol1 + dvol
                   if (abs(tmp) > max_div1) max_div1 = abs(tmp)
                else
                   div0 = div0 + tmp**2 * dvol
                   vol0 = vol0 + dvol
                   if (abs(tmp) > max_div0) max_div0 = abs(tmp)
                end if
             end if

          enddo
       end do

    else
       
       do k = mesh%sz,mesh%ez
          do j = mesh%sy,mesh%ey
             do i = mesh%sx,mesh%ex

                dvol = mesh%dx(i)*mesh%dy(j)*mesh%dz(k)

                tmp =    (u(i+1,j,k)-u(i,j,k)) * mesh%dy(j)*mesh%dz(k) &
                     & + (v(i,j+1,k)-v(i,j,k)) * mesh%dz(k)*mesh%dx(i) &
                     & + (w(i,j,k+1)-w(i,j,k)) * mesh%dx(i)*mesh%dy(j)

                us(i,j,k) = (mesh%dxu2(i+1)*u(i,j,k)+mesh%dxu2(i)*u(i+1,j,k))/mesh%dx(i) 
                vs(i,j,k) = (mesh%dyv2(j+1)*v(i,j,k)+mesh%dyv2(j)*v(i,j+1,k))/mesh%dy(j) 
                ws(i,j,k) = (mesh%dzw2(k+1)*w(i,j,k)+mesh%dzw2(k)*w(i,j,k+1))/mesh%dz(k) 
                
                div  = div + tmp**2 * dvol
                vol  = vol + dvol
                if (abs(tmp) > max_div) max_div = abs(tmp)

                if (two_phases) then
                   if (cou(i,j,k)>0.5d0) then
                      div1 = div1 + tmp**2 * dvol
                      vol1 = vol1 + dvol
                      if (abs(tmp) > max_div1) max_div1 = abs(tmp)
                   else
                      div0 = div0 + tmp**2 * dvol
                      vol0 = vol0 + dvol
                      if (abs(tmp) > max_div0) max_div0 = abs(tmp)
                   end if
                end if

             enddo
          enddo
       end do

    end if

    call mpi_allreduce(div,tmp,1,mpi_double_precision,mpi_sum,comm3d,mpi_code);     div     = tmp
    call mpi_allreduce(vol,tmp,1,mpi_double_precision,mpi_sum,comm3d,mpi_code);     vol     = tmp
    call mpi_allreduce(max_div,tmp,1,mpi_double_precision,mpi_max,comm3d,mpi_code); max_div = tmp

    if (two_phases) then
       call mpi_allreduce(div0,tmp,1,mpi_double_precision,mpi_sum,comm3d,mpi_code);     div0     = tmp
       call mpi_allreduce(div1,tmp,1,mpi_double_precision,mpi_sum,comm3d,mpi_code);     div1     = tmp
       call mpi_allreduce(vol0,tmp,1,mpi_double_precision,mpi_sum,comm3d,mpi_code);     vol0     = tmp
       call mpi_allreduce(vol1,tmp,1,mpi_double_precision,mpi_sum,comm3d,mpi_code);     vol1     = tmp
       call mpi_allreduce(max_div0,tmp,1,mpi_double_precision,mpi_max,comm3d,mpi_code); max_div0 = tmp
       call mpi_allreduce(max_div1,tmp,1,mpi_double_precision,mpi_max,comm3d,mpi_code); max_div1 = tmp
    end if
    
    max_p = maxval(abs(p))
    call mpi_allreduce(max_p,tmp,1,mpi_double_precision,mpi_max,comm3d,mpi_code); max_p = tmp
    
    max_u = maxval(abs(us))
    call mpi_allreduce(max_u,tmp,1,mpi_double_precision,mpi_max,comm3d,mpi_code); max_u = tmp
    
    max_v = maxval(abs(vs))
    call mpi_allreduce(max_v,tmp,1,mpi_double_precision,mpi_max,comm3d,mpi_code); max_v = tmp

    if (mesh%dim==3) then
       max_w = maxval(abs(ws))
       call mpi_allreduce(max_w,tmp,1,mpi_double_precision,mpi_max,comm3d,mpi_code); max_w = tmp
    end if
    
    if (two_phases) then
       max_p0 = maxval(abs(p*(1-cou)))
       max_p1 = maxval(abs(p*cou))
       call mpi_allreduce(max_p0,tmp,1,mpi_double_precision,mpi_max,comm3d,mpi_code); max_p0 = tmp
       call mpi_allreduce(max_p1,tmp,1,mpi_double_precision,mpi_max,comm3d,mpi_code); max_p1 = tmp

       max_u0 = maxval(abs(us*(1-cou)))
       max_u1 = maxval(abs(us*cou))
       call mpi_allreduce(max_u0,tmp,1,mpi_double_precision,mpi_max,comm3d,mpi_code); max_u0 = tmp
       call mpi_allreduce(max_u1,tmp,1,mpi_double_precision,mpi_max,comm3d,mpi_code); max_u1 = tmp

       max_v0 = maxval(abs(vs*(1-cou)))
       max_v1 = maxval(abs(vs*cou))
       call mpi_allreduce(max_v0,tmp,1,mpi_double_precision,mpi_max,comm3d,mpi_code); max_v0 = tmp
       call mpi_allreduce(max_v1,tmp,1,mpi_double_precision,mpi_max,comm3d,mpi_code); max_v1 = tmp
       
       if (mesh%dim==3) then
          max_w0 = maxval(abs(ws*(1-cou)))
          max_w1 = maxval(abs(ws*cou))
          call mpi_allreduce(max_w0,tmp,1,mpi_double_precision,mpi_max,comm3d,mpi_code); max_w0 = tmp
          call mpi_allreduce(max_w1,tmp,1,mpi_double_precision,mpi_max,comm3d,mpi_code); max_w1 = tmp
       end if
    end if
    
    if (rank>0) return

    write(*,*) "Mass-conservation         max      norm2"
    write(*,102) "  mixture          ",  max_div, sqrt(div/vol)
    if (two_phases) then
       write(*,102) "  phase-0          ", max_div0, sqrt(div0/vol0)
       write(*,102) "  phase-1          ", max_div1, sqrt(div1/vol1)
    end if

    if (mesh%dim==2) then 
       write(*,*) "Solved quantities      max(p)     max(u)     max(v)"
       write(*,102) "  mixture          ",max_p,max_u,max_v
    else
       write(*,*) "Solved quantities      max(p)     max(u)     max(v)     max(w)"
       write(*,102) "  mixture          ",max_p,max_u,max_v,max_w
    end if

    if (two_phases) then
       if (mesh%dim==2) then 
          write(*,102) "  phase-0          ",max_p0,max_u0,max_v0
          write(*,102) "  phase-1          ",max_p1,max_u1,max_v1
       else
          write(*,102) "  phase-0          ",max_p0,max_u0,max_v0,max_w0
          write(*,102) "  phase-1          ",max_p1,max_u1,max_v1,max_w1
       end if
    end if
   
102 format(a,999(1x,1pe10.3))

  end subroutine NS_print2
  !*****************************************************************************************************
  
  subroutine NS_res(dt,p,u,v,w,p0,u0,v0,w0)
    use mod_mpi
    use mod_Parameters, only: rank, &
         & dim,sx,ex,sy,ey,sz,ez,   &
         & sxu,exu,syu,eyu,szu,ezu, &
         & sxv,exv,syv,eyv,szv,ezv, &
         & sxw,exw,syw,eyw,szw,ezw
    !-------------------------------------------------------------------------------
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), intent(in)                                :: dt
    real(8), dimension(:,:,:), allocatable, intent(in) :: p,u,v,w,p0,u0,v0,w0
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    real(8)                                            :: deltau,normu
    real(8)                                            :: ldeltau,lnormu
    real(8)                                            :: deltap,normp
    real(8)                                            :: maxu,maxv,maxw,maxp
    real(8)                                            :: lmaxu,lmaxv,lmaxw,lmaxp
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Vec U increment 
    !-------------------------------------------------------------------------------
    ldeltau = max(maxval(abs(u(sxu:exu,syu:eyu,szu:ezu)-u0(sxu:exu,syu:eyu,szu:ezu))),&
         & maxval(abs(v(sxv:exv,syv:eyv,szv:ezv)-v0(sxv:exv,syv:eyv,szv:ezv))))
    lnormu = max(maxval(abs(u(sxu:exu,syu:eyu,szu:ezu))),maxval(abs(v(sxv:exv,syv:eyv,szv:ezv))))
    if (dim==3) then
       ldeltau = max(ldeltau,&
            & maxval(abs(w(sxw:exw,syw:eyw,szw:ezw)-w0(sxw:exw,syw:eyw,szw:ezw))))
       lnormu = max(lnormu,maxval(abs(w(sxw:exw,syw:eyw,szw:ezw))))
    end if

    call mpi_allreduce(ldeltau,deltau,1,mpi_double_precision,mpi_max,comm3d,code)
    call mpi_allreduce(lnormu,normu,1,mpi_double_precision,mpi_max,comm3d,code)

    if (rank==0) then
       write(*,101) 'Max(|U-U0|)/Max(|U|)/dt = ', deltau/normu/dt
       write(*,101) 'Max(|U-U0|)/dt = ', deltau/dt
    end if
    !-------------------------------------------------------------------------------

!!$    !-------------------------------------------------------------------------------
!!$    ! pressure p increment 
!!$    !-------------------------------------------------------------------------------
!!$    deltap = maxval(abs(p(sx:ex,sy:ey,sz:ez)-p0(sx:ex,sy:ey,sz:ez)))
!!$    normp = maxval(abs(p(sx:ex,sy:ey,sz:ez)))
!!$    write(*,101) 'Max(|p-p0|)/Max(|p|)/dt = ', deltap/normp/dt
!!$    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Maximum values
    !-------------------------------------------------------------------------------
    lmaxp = maxval(abs(p(sx:ex,sy:ey,sz:ez)))
    lmaxu = maxval(abs(u(sxu:exu,syu:eyu,szu:ezu)))
    lmaxv = maxval(abs(v(sxv:exv,syv:eyv,szv:ezv)))
    if (dim==3) lmaxw = maxval(abs(w(sxw:exw,syw:eyw,szw:ezw)))

    call mpi_allreduce(lmaxp,maxp,1,mpi_double_precision,mpi_max,comm3d,code)
    call mpi_allreduce(lmaxu,maxu,1,mpi_double_precision,mpi_max,comm3d,code)
    call mpi_allreduce(lmaxv,maxv,1,mpi_double_precision,mpi_max,comm3d,code)
    if (dim==3) call mpi_allreduce(lmaxw,maxw,1,mpi_double_precision,mpi_max,comm3d,code)

    if (rank==0) then
       if (dim==2) then
          write(*,102) &
               & 'Max(|p|) = ',maxp,&
               & '| Max(|u|) = ',maxu,&
               & '| Max(|v|) = ',maxv
       else
          write(*,102) &
               & 'Max(|p|) = ',maxp,&
               & '| Max(|u|) = ',maxu,&
               & '| Max(|v|) = ',maxv,&
               & '| Max(|w|) = ',maxw
       end if
    end if
    !-------------------------------------------------------------------------------

101 format(a,1pe15.8)
102 format(999(a,1pe12.5,1x))


  end subroutine NS_res


  subroutine Poisson_Matrix(mat,diffu,diffv,diffw,penp,phi)
    use mod_Parameters, only: dim
    use mod_struct_solver
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:),   allocatable, intent(in)    :: diffu,diffv,diffw
    real(8), dimension(:,:,:,:), allocatable, intent(in)    :: penp
    type(matrix_t),                           intent(inout) :: mat
    type(solver_sca_t),                       intent(inout) :: phi
    !-------------------------------------------------------------------------------

    if (dim==2) then
       call Poisson_Matrix_2D(mat%coef,mat%jcof,mat%icof,diffu,diffv,penp,mat%kdv,mat%nps,phi)
    else
       call Poisson_Matrix_3D(mat%coef,mat%jcof,mat%icof,diffu,diffv,diffw,penp,mat%kdv,mat%nps,phi)
    end if
    
  end subroutine Poisson_Matrix

  subroutine Poisson_Matrix_2D (coef,jcof,icof,diffu,diffv,penp, &
       & nb_coef_matrix,nb_matrix_element,phi)
    !*****************************************************************************************************
    !*****************************************************************************************************
    !*****************************************************************************************************
    !*****************************************************************************************************
    !-------------------------------------------------------------------------------
    ! Finite volume discretization of projection step
    !-------------------------------------------------------------------------------
    use mod_Parameters, only: nproc,dim, &
         & dx,dy,dxu,dyv,                &
         & sx,ex,sy,ey,                  &
         & sxs,exs,sys,eys
    use mod_struct_solver
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)                                  :: nb_coef_matrix,nb_matrix_element
    integer, dimension(:), intent(in), allocatable       :: jcof,icof
    real(8), dimension(:), intent(inout), allocatable    :: coef
    real(8), dimension(:,:,:), allocatable, intent(in)   :: diffu,diffv
    real(8), dimension(:,:,:,:), allocatable, intent(in) :: penp
    type(solver_sca_t), intent(inout)                    :: phi
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    real(8)                                              :: difb,diff
    integer                                              :: i,j,lvs
    !-------------------------------------------------------------------------------


    !-------------------------------------------------------------------------------
    ! Matrix initialization
    !-------------------------------------------------------------------------------
    coef(1:nb_matrix_element) = 0
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Finite volume discretization
    !-------------------------------------------------------------------------------
    lvs = 1
    do j = sys,eys
       do i = sxs,exs

          if (nproc>1.and.(i<sx.or.i>ex.or.j<sy.or.j>ey)) then
             !-------------------------------------------------------------------------------
             ! Penalty term for MPI exchange cells
             !-------------------------------------------------------------------------------
             coef(lvs) = 1!;coef(lvs+1:lvs+4) = 0
          else 
             !-------------------------------------------------------------------------------
             ! Lap = div(grad)
             !-------------------------------------------------------------------------------
             difb =   diffu(i  ,j,1) / dxu(i  ) * dy(j) ! backward contribution
             diff = - diffu(i+1,j,1) / dxu(i+1) * dy(j) ! forward contribution
             coef(lvs  ) = coef(lvs)   - (difb  - diff)
             coef(lvs+1) = coef(lvs+1) + difb
             coef(lvs+2) = coef(lvs+2) - diff
             difb =   diffv(i,j  ,1) / dyv(j  ) * dx(i) ! backward contribution
             diff = - diffv(i,j+1,1) / dyv(j+1) * dx(i) ! forward contribution
             coef(lvs  ) = coef(lvs)   - (difb  - diff)
             coef(lvs+3) = coef(lvs+3) + difb
             coef(lvs+4) = coef(lvs+4) - diff
             !-------------------------------------------------------------------------------
             ! Penalty term for boundary conditions and immersed objects
             ! penu (i,j,k,l)
             ! l = 1 central phi component
             ! l = 2 left phi component
             ! l = 3 right phi component
             ! l = 4 bottom phi component
             ! l = 5 top phi component
             !-------------------------------------------------------------------------------
             coef(lvs)   = coef(lvs)   + penp(i,j,1,1) * dx(i) * dy(j)
             coef(lvs+1) = coef(lvs+1) + penp(i,j,1,2) * dx(i) * dy(j)
             coef(lvs+2) = coef(lvs+2) + penp(i,j,1,3) * dx(i) * dy(j)
             coef(lvs+3) = coef(lvs+3) + penp(i,j,1,4) * dx(i) * dy(j)
             coef(lvs+4) = coef(lvs+4) + penp(i,j,1,5) * dx(i) * dy(j)
             !-------------------------------------------------------------------------------
             ! End of discretization for phi
             !-------------------------------------------------------------------------------
          end if

          !-------------------------------------------------------------------------------
          ! next line of the matrix
          !-------------------------------------------------------------------------------
          lvs = lvs+nb_coef_matrix 
       end do
    end do
  end subroutine  Poisson_Matrix_2D

  subroutine Poisson_Matrix_3D (coef,jcof,icof,diffu,diffv,diffw,penp, &
       & nb_coef_matrix,nb_matrix_element,phi)
    !*****************************************************************************************************
    !*****************************************************************************************************
    !*****************************************************************************************************
    !*****************************************************************************************************
    !-------------------------------------------------------------------------------
    ! Finite volume discretization of projection step
    !-------------------------------------------------------------------------------
    use mod_Parameters, only: nproc,dim, &
         & dx,dy,dz,dxu,dyv,dzw,         &
         & sx,ex,sy,ey,sz,ez,            &
         & sxs,exs,sys,eys,szs,ezs
    use mod_struct_solver
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)                                  :: nb_coef_matrix,nb_matrix_element
    integer, dimension(:), intent(in), allocatable       :: jcof,icof
    real(8), dimension(:), intent(inout), allocatable    :: coef
    real(8), dimension(:,:,:), allocatable, intent(in)   :: diffu,diffv,diffw
    real(8), dimension(:,:,:,:), allocatable, intent(in) :: penp
    type(solver_sca_t), intent(inout)                    :: phi
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    real(8)                                              :: difb,diff
    integer                                              :: i,j,k,lvs
    !-------------------------------------------------------------------------------


    !-------------------------------------------------------------------------------
    ! Matrix initialization
    !-------------------------------------------------------------------------------
    coef(1:nb_matrix_element) = 0
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Finite volume discretization
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------
    lvs = 1
    do k = szs,ezs
       do j = sys,eys
          do i = sxs,exs

             if (nproc>1.and.(i<sx.or.i>ex.or.j<sy.or.j>ey.or.k<sz.or.k>ez)) then
                !-------------------------------------------------------------------------------
                ! Penalty term for MPI exchange cells
                !-------------------------------------------------------------------------------
                coef(lvs) = 1!;coef(lvs+1:lvs+6) = 0
             else 
                !-------------------------------------------------------------------------------
                ! Lap = div(grad)
                !-------------------------------------------------------------------------------
                difb =   diffu(i  ,j,k) / dxu(i  ) * dy(j) * dz(k) ! backward contribution
                diff = - diffu(i+1,j,k) / dxu(i+1) * dy(j) * dz(k) ! forward contribution
                coef(lvs  ) = coef(lvs)   - (difb  - diff)
                coef(lvs+1) = coef(lvs+1) + difb
                coef(lvs+2) = coef(lvs+2) - diff
                difb =   diffv(i,j  ,k) / dyv(j  ) * dx(i) * dz(k) ! backward contribution
                diff = - diffv(i,j+1,k) / dyv(j+1) * dx(i) * dz(k) ! forward contribution
                coef(lvs  ) = coef(lvs)   - (difb  - diff)
                coef(lvs+3) = coef(lvs+3) + difb
                coef(lvs+4) = coef(lvs+4) - diff
                difb =   diffw(i,j,k  ) / dzw(k  ) * dx(i) * dy(j) ! backward contribution
                diff = - diffw(i,j,k+1) / dzw(k+1) * dx(i) * dy(j) ! forward contribution
                coef(lvs  ) = coef(lvs)   - (difb  - diff)
                coef(lvs+5) = coef(lvs+5) + difb
                coef(lvs+6) = coef(lvs+6) - diff
                !-------------------------------------------------------------------------------
                ! Penalty term for boundary conditions and immersed objects
                ! penu (i,j,k,l)
                ! l = 1 central phi component
                ! l = 2 left    phi component
                ! l = 3 right   phi component
                ! l = 4 bottom  phi component
                ! l = 5 top     phi component
                !-------------------------------------------------------------------------------
                coef(lvs)   = coef(lvs)   + penp(i,j,k,1) * dx(i) * dy(j) * dz(k)
                coef(lvs+1) = coef(lvs+1) + penp(i,j,k,2) * dx(i) * dy(j) * dz(k)
                coef(lvs+2) = coef(lvs+2) + penp(i,j,k,3) * dx(i) * dy(j) * dz(k)
                coef(lvs+3) = coef(lvs+3) + penp(i,j,k,4) * dx(i) * dy(j) * dz(k)
                coef(lvs+4) = coef(lvs+4) + penp(i,j,k,5) * dx(i) * dy(j) * dz(k)
                coef(lvs+5) = coef(lvs+5) + penp(i,j,k,6) * dx(i) * dy(j) * dz(k)
                coef(lvs+6) = coef(lvs+6) + penp(i,j,k,7) * dx(i) * dy(j) * dz(k)
                !-------------------------------------------------------------------------------
                ! End of discretization for phi
                !-------------------------------------------------------------------------------
             end if
             
             !-------------------------------------------------------------------------------
             ! next line of the matrix
             !-------------------------------------------------------------------------------
             lvs = lvs+nb_coef_matrix 
          end do
       end do
    end do
  end subroutine  Poisson_Matrix_3D

  subroutine PoissonSchur_Matrix(mat,coeff_diff_u,coeff_diff_v,coeff_diff_w,penp)
    use mod_Parameters, only: dim
    use mod_struct_solver
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:),   allocatable, intent(in)    :: coeff_diff_u,coeff_diff_v,coeff_diff_w
    real(8), dimension(:,:,:,:), allocatable, intent(in)    :: penp
    type(matrix_t),                           intent(inout) :: mat
    !-------------------------------------------------------------------------------

    if (dim==2) then
       call PoissonSchur_Matrix_2D(mat%coef,mat%jcof,mat%icof,coeff_diff_u,coeff_diff_v,penp,mat%kdv)
    else
       call PoissonSchur_Matrix_3D(mat%coef,mat%jcof,mat%icof,coeff_diff_u,coeff_diff_v,coeff_diff_w,penp,mat%kdv)
    end if
    
  end subroutine PoissonSchur_Matrix
  
  subroutine PoissonSchur_Matrix_2D(coef,jcof,icof, &
       & coeff_diff_u,coeff_diff_v,penp,            &
       & nb_coef_matrix)
    !*****************************************************************************************************
    !*****************************************************************************************************
    !*****************************************************************************************************
    !*****************************************************************************************************
    !-------------------------------------------------------------------------------
    ! Finite volume discretization of ...
    !-------------------------------------------------------------------------------
    use mod_Parameters, only: dim, &
         & sxs,exs,sys,eys,        &
         & dx,dy,dxu,dyv
    use mod_struct_solver
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)                                  :: nb_coef_matrix
    integer, dimension(:), intent(in), allocatable       :: jcof,icof
    real(8), dimension(:), intent(inout), allocatable    :: coef
    real(8), dimension(:,:,:), allocatable, intent(in)   :: coeff_diff_u,coeff_diff_v
    real(8), dimension(:,:,:,:), allocatable, intent(in) :: penp
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    real(8)                                              :: contribb,contribf
    integer                                              :: i,j,lvs
    !-------------------------------------------------------------------------------
    
    !-------------------------------------------------------------------------------
    ! Matrix initialization
    !-------------------------------------------------------------------------------
    coef = 0
    !-------------------------------------------------------------------------------
    
    !-------------------------------------------------------------------------------
    ! Finite volume discretization for Laplacian pressure operator
    !-------------------------------------------------------------------------------
    lvs = 1
    do j = sys,eys
       do i = sxs,exs
          !-------------------------------------------------------------------------------
          ! Lap = div(grad)
          !-------------------------------------------------------------------------------
          contribb = -coeff_diff_u(i  ,j,1) / dxu(i  ) * dy(j)
          contribf = +coeff_diff_u(i+1,j,1) / dxu(i+1) * dy(j)
          coef(lvs  ) = coef(lvs)   - (contribb - contribf)
          coef(lvs+1) = coef(lvs+1) + contribb
          coef(lvs+2) = coef(lvs+2) - contribf
          contribb = -coeff_diff_v(i,j  ,1) / dyv(j  ) * dx(i)
          contribf = +coeff_diff_v(i,j+1,1) / dyv(j+1) * dx(i)
          coef(lvs  ) = coef(lvs)   - (contribb - contribf)
          coef(lvs+3) = coef(lvs+3) + contribb
          coef(lvs+4) = coef(lvs+4) - contribf
          !-------------------------------------------------------------------------------
          ! Penalty term for boundary conditions and immersed objects
          !-------------------------------------------------------------------------------
          coef(lvs)   = coef(lvs)   + penp(i,j,1,1) * dx(i) * dy(j)
          coef(lvs+1) = coef(lvs+1) + penp(i,j,1,2) * dx(i) * dy(j)
          coef(lvs+2) = coef(lvs+2) + penp(i,j,1,3) * dx(i) * dy(j)
          coef(lvs+3) = coef(lvs+3) + penp(i,j,1,4) * dx(i) * dy(j)
          coef(lvs+4) = coef(lvs+4) + penp(i,j,1,5) * dx(i) * dy(j)
          !-------------------------------------------------------------------------------
          ! End of discretization
          !-------------------------------------------------------------------------------
          
          !-------------------------------------------------------------------------------
          ! next line of the matrix
          !-------------------------------------------------------------------------------
          lvs = lvs+nb_coef_matrix
          !-------------------------------------------------------------------------------
       end do
    end do
    
  end subroutine PoissonSchur_Matrix_2D
  
  subroutine PoissonSchur_Matrix_3D(coef,jcof,icof,   &
       & coeff_diff_u,coeff_diff_v,coeff_diff_w,penp, &
       & nb_coef_matrix)
    !*****************************************************************************************************
    !*****************************************************************************************************
    !*****************************************************************************************************
    !*****************************************************************************************************
    !-------------------------------------------------------------------------------
    ! Finite volume discretization of ...
    !-------------------------------------------------------------------------------
    use mod_Parameters, only: dim,  &
         & sxs,exs,sys,eys,szs,ezs, &
         & dx,dy,dz,dxu,dyv,dzw
    use mod_struct_solver
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)                                  :: nb_coef_matrix
    integer, dimension(:), intent(in), allocatable       :: jcof,icof
    real(8), dimension(:), intent(inout), allocatable    :: coef
    real(8), dimension(:,:,:), allocatable, intent(in)   :: coeff_diff_u,coeff_diff_v,coeff_diff_w
    real(8), dimension(:,:,:,:), allocatable, intent(in) :: penp
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    real(8)                                              :: contribb,contribf
    integer                                              :: i,j,k,lvs
    !-------------------------------------------------------------------------------
    
    !-------------------------------------------------------------------------------
    ! Matrix initialization
    !-------------------------------------------------------------------------------
    coef = 0
    !-------------------------------------------------------------------------------
    
    !-------------------------------------------------------------------------------
    ! Finite volume discretization for Laplacian pressure operator
    !-------------------------------------------------------------------------------
    lvs = 1
    do k = szs,ezs
       do j = sys,eys
          do i = sxs,exs
             !-------------------------------------------------------------------------------
             ! Lap = div(grad)
             !-------------------------------------------------------------------------------
             contribb = -coeff_diff_u(i  ,j,k) / dxu(i  ) * dy(j) * dz(k)
             contribf = +coeff_diff_u(i+1,j,k) / dxu(i+1) * dy(j) * dz(k)
             coef(lvs  ) = coef(lvs)   - (contribb - contribf)
             coef(lvs+1) = coef(lvs+1) + contribb
             coef(lvs+2) = coef(lvs+2) - contribf
             contribb = -coeff_diff_v(i,j  ,k) / dyv(j  ) * dx(i) * dz(k)
             contribf = +coeff_diff_v(i,j+1,k) / dyv(j+1) * dx(i) * dz(k)
             coef(lvs  ) = coef(lvs)   - (contribb - contribf)
             coef(lvs+3) = coef(lvs+3) + contribb
             coef(lvs+4) = coef(lvs+4) - contribf
             contribb = -coeff_diff_w(i,j,k  ) / dzw(k  ) * dx(i) * dy(j)
             contribf = +coeff_diff_w(i,j,k+1) / dzw(k+1) * dx(i) * dy(j)
             coef(lvs  ) = coef(lvs)   - (contribb - contribf)
             coef(lvs+5) = coef(lvs+5) + contribb
             coef(lvs+6) = coef(lvs+6) - contribf
             !-------------------------------------------------------------------------------
             ! Penalty term for boundary conditions and immersed objects
             !-------------------------------------------------------------------------------
             coef(lvs)   = coef(lvs)   + penp(i,j,k,1) * dx(i) * dy(j) * dz(k)
             coef(lvs+1) = coef(lvs+1) + penp(i,j,k,2) * dx(i) * dy(j) * dz(k)
             coef(lvs+2) = coef(lvs+2) + penp(i,j,k,3) * dx(i) * dy(j) * dz(k)
             coef(lvs+3) = coef(lvs+3) + penp(i,j,k,4) * dx(i) * dy(j) * dz(k)
             coef(lvs+4) = coef(lvs+4) + penp(i,j,k,5) * dx(i) * dy(j) * dz(k)
             coef(lvs+5) = coef(lvs+5) + penp(i,j,k,6) * dx(i) * dy(j) * dz(k)
             coef(lvs+6) = coef(lvs+6) + penp(i,j,k,7) * dx(i) * dy(j) * dz(k)
             !-------------------------------------------------------------------------------
             ! End of discretization
             !-------------------------------------------------------------------------------
             
             !-------------------------------------------------------------------------------
             ! next line of the matrix
             !-------------------------------------------------------------------------------
             lvs = lvs+nb_coef_matrix
             !-------------------------------------------------------------------------------
          end do
       end do
    end do

  end subroutine PoissonSchur_Matrix_3D

end module mod_Navier
