!===============================================================================
module Bib_VOFLag_Init_Flow
  !===============================================================================
  contains
  !-----------------------------------------------------------------------------  
  !> @author  amine chadil
  ! 
  !> @brief initialisation of flow field
  !
  !> @param[out]   vl     : la structure contenant toutes les informations
  !! relatives aux particules.
  !> @param u,v,w          : velocity field components
  !> @param pres           : pressure
  !-----------------------------------------------------------------------------
  subroutine Init_Flow(u,v,w,u0,v0,w0,u1,v1,w1,pres,pres0,vl)
    !===============================================================================
    !modules
    !===============================================================================
    !-------------------------------------------------------------------------------
    !Modules de definition des structures et variables => src/mod/module_...f90
    !-------------------------------------------------------------------------------
    use Module_VOFLag_ParticlesDataStructure 
    use Module_VOFLag_Flows
    use mod_Parameters,                       only : deeptracking,dim
    !-------------------------------------------------------------------------------
    !Modules de subroutines de la librairie RESPECT => src/Bib_VOFLag_...f90
    !-------------------------------------------------------------------------------
    use Bib_VOFLag_Init_UniformStokesFlowPastAParticle_Cylinder
    use Bib_VOFLag_Init_UniformStokesFlowPastAParticle_Sphere
    !-------------------------------------------------------------------------------

    !===============================================================================
    !declarations des variables
    !===============================================================================
    implicit none
    !-------------------------------------------------------------------------------
    !variables globales
    !-------------------------------------------------------------------------------
    type(struct_vof_lag),                   intent(inout) :: vl
    real(8), dimension(:,:,:), allocatable, intent(inout) :: u,v,w,u0,v0,w0,u1,v1,&
                                                             w1,pres,pres0
    !-------------------------------------------------------------------------------
    !variables locales 
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------
    
    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'entree Init_Flow'
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------------------
    if (uniform_past_part) then 
      if (dim .eq. 2) then
        call Init_UniformStokesFlowPastAParticle_Cylinder(u,v,pres,vl%objet(1)%pos,vl%objet(1)%sca(1))
      else
        call Init_UniformStokesFlowPastAParticle_Sphere(u,v,w,pres,U_inf,vl%objet(1)%pos,vl%objet(1)%sca(1))
      end if
    end if
    !u=1
    !v=1
    !z=1

    !-------------------------------------------------------------------------------
    ! n and n-1 variables
    !-------------------------------------------------------------------------------
    u0=u
    v0=v
    if (dim==3) w0=w
    if (allocated(u1)) then
      u1=u0
      v1=v0
      if (dim==3) w1=w0
    end if
    pres0=pres

    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'sortie Init_Flow'
    !-------------------------------------------------------------------------------
  end subroutine Init_Flow

  !===============================================================================
end module Bib_VOFLag_Init_Flow
!===============================================================================