!===============================================================================
module Bib_VOFLag_Particle_PointIn
  !===============================================================================
  contains
  !-----------------------------------------------------------------------------  
  !> @author amine chadil
  ! 
  !> @brief cette routine renseigne sur l'etat du point p par rapport a la copie nbt de la 
  !! particle np (la routine dit si p est a l'interieur ou a l'exterieur de la particle) 
  !
  !> @param[in]  vl          : la structure contenant toutes les informations
  !! relatives aux particules
  !> @param[in]  np          : numero de la particule dans vl
  !> @param[in]  nbt         : numero de la copie de la particule np
  !> @param[in]  sca         : les dimensions de copie nbt de la particule np avec lesquelles
  !! le test sera fait
  !> @param[in]  p           : le point a tester
  !> @param[out] in_particle : vrai si le point est a l'interieur de la particle faux sinon
  !-----------------------------------------------------------------------------
  subroutine Particle_PointIn(vl,np,nbt,sca,p,ndim,in_particle)
    !===============================================================================
    !modules
    !===============================================================================
    !-------------------------------------------------------------------------------
    !Modules de definition des structures et variables => src/mod/module_...f90
    !-------------------------------------------------------------------------------
    use Module_VOFLag_ParticlesDataStructure
    use mod_Parameters,                      only : deeptracking
    !-------------------------------------------------------------------------------
    !Modules de subroutines de la librairie RESPECT => src/Bib_VOFLag_...f90
    !-------------------------------------------------------------------------------
    use Bib_VOFLag_Sphere_PointIn
    use Bib_VOFLag_Ellipsoid_PointIn
    !-------------------------------------------------------------------------------

    !===============================================================================
    !declarations des variables
    !===============================================================================
    implicit none
    !-------------------------------------------------------------------------------
    !variables globales
    !-------------------------------------------------------------------------------
    type(struct_vof_lag), intent(inout) :: vl
    logical,              intent(out)   :: in_particle
    real(8), dimension(3),intent(in)    :: p,sca
    integer,              intent(in)    :: np,nbt  
    integer,              intent(in)    :: ndim
    !-------------------------------------------------------------------------------
    !variables locales 
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------
    
    !-------------------------------------------------------------------------------
    !if (deeptracking) write(*,*) 'entree Particle_PointIn'
    !-------------------------------------------------------------------------------

    select case(vl%nature)
    case(1)
       !-------------------------------------------------------------------------------
       ! Si les particules sont spherique
       !-------------------------------------------------------------------------------
       call Sphere_PointIn(vl,np,nbt,sca,p,ndim,in_particle) 

    case(2)
       !-------------------------------------------------------------------------------
       ! Si les particules sont ellipsoidale
       !-------------------------------------------------------------------------------
       call Ellipsoid_PointIn(vl,np,nbt,sca,p,ndim,in_particle)
       !write(*,*) "STOP : point_in_particle n'est faite que pour le cas des spheres"

    case default
       write(*,*) "STOP : point_in_particle n'est faite que pour le cas des spheres et des ellipsoides"
       stop
    end select

    !-------------------------------------------------------------------------------
    !if (deeptracking) write(*,*) 'sortie Particle_PointIn'
    !-------------------------------------------------------------------------------
  end subroutine Particle_PointIn

  !===============================================================================
end module Bib_VOFLag_Particle_PointIn
!===============================================================================
  


