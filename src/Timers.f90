module mod_timers
  implicit none

  !---------------------------------------------------------------
  ! some constants
  !---------------------------------------------------------------
  integer, parameter :: LEN_MAX     = 80
  integer, parameter :: TIMER_RESET = 0
  integer, parameter :: TIMER_START = 1
  integer, parameter :: TIMER_END   = 2
  integer, parameter :: TIMER_PRINT = 3
  !---------------------------------------------------------------
  ! timer type
  !---------------------------------------------------------------
  type timer_t
     integer                :: id
     integer                :: depth
     real(8)                :: ncall
     real(8)                :: time_start
     real(8)                :: time_end
     real(8)                :: time_cpu
     character(len=LEN_MAX) :: name
     logical                :: opened
     type(timer_t), pointer :: next
  end type timer_t
  type(timer_t), pointer    :: first,current
  !---------------------------------------------------------------

contains

  subroutine compute_time(timer_code,name)
    use mod_Parameters, only: rank,nproc,nx,ny,nz,deeptracking
    implicit none
    include 'mpif.h'
    !----------------------------------------------------------------------------
    ! global variables
    !---------------------------------------------------------------------------
    character(len=*), intent(in), optional :: name
    integer, intent(in)                    :: timer_code
    !----------------------------------------------------------------------------
    ! local variables
    !---------------------------------------------------------------------------
    integer                                :: i,unit_write=11
    integer                                :: nb_iter
    integer, save                          :: cpt_open=0,nptr=0
    real(8)                                :: wall_clock_time
    character(len=LEN_MAX)                 :: local_name
    logical                                :: ptr_exist
    logical, save                          :: once=.true.
    !---------------------------------------------------------------------------

    !---------------------------------------------------------------------------
    ! init first pointer
    !---------------------------------------------------------------------------
    if (once) then
       nullify(first)
       nullify(current)
       once=.false.
    end if
    !---------------------------------------------------------------------------
    
    !---------------------------------------------------------------------------
    ! check if timer exits
    !---------------------------------------------------------------------------
    local_name=name
    ptr_exist=.false.
    if (associated(first)) then 
       current=>first
       do
          if (.not. associated(current)) exit 
          if (current%name == local_name) then
             ptr_exist=.true.
             exit
          end if
          current=>current%next
       end do
    end if
    !---------------------------------------------------------------------------
    
    !---------------------------------------------------------------------------
    ! start, stop, print or reset timer 
    !---------------------------------------------------------------------------
    select case (timer_code)
    case(TIMER_START)
       cpt_open=cpt_open+1

       !---------------------------------------------------------------------------
       ! create new timer
       !---------------------------------------------------------------------------
       if (.not. ptr_exist) then
          nptr=nptr+1
          call init_ptr_timer(current,first)
          current%id=nptr
          current%name=trim(adjustl(name))
       end if
       !---------------------------------------------------------------------------

       !---------------------------------------------------------------------------
       ! associate time and values
       !---------------------------------------------------------------------------
       !call cpu_time(current%time_start)
       current%time_start=MPI_Wtime()
       current%ncall=current%ncall+1
       current%depth=cpt_open
       current%opened=.true.
       !---------------------------------------------------------------------------
       
    case(TIMER_END)
       cpt_open=cpt_open-1

       !---------------------------------------------------------------------------
       ! timer depth, cummulative time and closing
       !---------------------------------------------------------------------------
       current%depth=cpt_open
       !call cpu_time(current%time_end)
       current%time_end=MPI_Wtime()
       current%time_cpu=current%time_cpu &
            & + current%time_end-current%time_start
       current%opened=.false.
       !---------------------------------------------------------------------------
    case(TIMER_PRINT)

       !---------------------------------------------------------------------------
       ! file open
       !---------------------------------------------------------------------------
       if (rank==0) then 
          open(unit=unit_write,file='profiling.out')
          rewind(unit_write)
       end if
       !---------------------------------------------------------------------------

       !---------------------------------------------------------------------------
       ! Get Time of 1st timers (wall clock time)
       !---------------------------------------------------------------------------
       current=>first 
       do
          if (.not. associated(current)) exit
          if (current%id==1) then
             !call cpu_time(current%time_end)
             current%time_end=MPI_Wtime()
             current%time_cpu=current%time_end-current%time_start
             wall_clock_time=current%time_cpu
             exit
          end if
          current=>current%next
       end do
       !---------------------------------------------------------------------------

       !---------------------------------------------------------------------------
       ! write header
       !---------------------------------------------------------------------------
       if (rank==0) then 
          write(unit_write,102)
          write(unit_write,100) &
               & "Name",        &
               & "Id",          &
               & "d.",          &
               & "Time (s)",    &
               & "(%)",         &
               & "Calls",       &
               & "Call cost"
          write(unit_write,102)
       end if
       !---------------------------------------------------------------------------
       
       !---------------------------------------------------------------------------
       ! write closed timers 
       !---------------------------------------------------------------------------
       nb_iter = 1000
       current=>first
       do
          if (.not. associated(current)) exit
          if (rank==0) then
             if (.not.current%opened) then 
                write(unit_write,101)                        &
                     & trim(adjustl(current%name)),          &
                     & current%id,                           &
                     & current%depth,                        &  
                     & current%time_cpu,                     &
                     & current%time_cpu/wall_clock_time*1d1, &    !??!?!?? WTF !!?!??!
                     & current%ncall,                        &
                     & current%time_cpu/current%ncall
                !---------------------------------------------------------------------------
                ! get iteration number
                !---------------------------------------------------------------------------
                if (current%ncall<nb_iter) then
                   if (current%ncall>1) nb_iter = current%ncall
                end if
                !---------------------------------------------------------------------------
             end if
          end if
          current=>current%next
       end do
       !---------------------------------------------------------------------------
       write(unit_write,102)
       if (rank==0) then 
          write(unit_write,103) " Perfs (t_cpu*nproc/niter/dof) [µs]", wall_clock_time*nproc/nb_iter/nx/ny/nz*1d6
       end if
       write(unit_write,102)
       flush(unit_write)

    end select

    !---------------------------------------------------------------------------
    if (deeptracking) then
       write(*,*) rank, timer_code, current%name
    end if
    !---------------------------------------------------------------------------
    


100 format("| ",a50," | ",a3," | ",a2," | ",  a9  ," | ",a5  ," | ",a9," | ",  a9,  " |")
!!$101 format("| ",a30," | ",i3," | ",i2," | ",1pe9.3," | ",1pe7.1," | ",i9," |")
101 format("| ",a50," | ",i3," | ",i2," | ",1pe9.3," | ",f5.1," | ",1pe9.3," | ",1pe9.3," |")
103 format("| ",a94," | ",1pe9.3," |")
102 format("------------------------------------------------------&
         &-------------------------------------------------------")
    
  end subroutine compute_time
  
  subroutine init_ptr_timer(current,first)
    implicit none
    !---------------------------------------------------------------------------
    ! global variables
    !---------------------------------------------------------------------------
    type(timer_t), pointer, intent(inout) :: current,first
    !---------------------------------------------------------------------------
    
    allocate(current)
    current%id         = 0
    current%depth      = 0
    current%ncall      = 0
    current%time_start = 0
    current%time_end   = 0
    current%time_cpu   = 0
    current%name       = ''
    current%opened     = .true.
    current%next       => first
    first              => current
    
  end subroutine init_ptr_timer
  
end module mod_timers

