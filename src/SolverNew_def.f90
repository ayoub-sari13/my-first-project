module mod_solver_new_def
  implicit none

  integer, parameter :: PRECOND_JACOBI              = 0
  integer, parameter :: PRECOND_ILU                 = 1
  integer, parameter :: PRECOND_SMG                 = 2
  integer, parameter :: PRECOND_PFMG                = 3

  integer, parameter :: SOLVE_P                     = 0
  integer, parameter :: SOLVE_U                     = 1
  integer, parameter :: SOLVE_V                     = 2
  integer, parameter :: SOLVE_W                     = 3
  integer, parameter :: SOLVE_UV                    = 12 
  integer, parameter :: SOLVE_UVW                   = 123
  integer, parameter :: SOLVE_UVP                   = 120
  integer, parameter :: SOLVE_UVWP                  = 1230

  integer, parameter :: VAR_P                       = 0
  integer, parameter :: VAR_U                       = 1
  integer, parameter :: VAR_V                       = 2
  integer, parameter :: VAR_W                       = 3

  integer, parameter :: CREATE                      = 1
  integer, parameter :: DESTROY                     = 0

  real(8), parameter :: THRESHOLD_DIAG              = 1d-35
  real(8), parameter :: THRESHOLD_EXTRACT           = 1d-30
  real(8), parameter :: THRESHOLD_RHS_PERTURB       = 1d-28
  real(8), parameter :: THRESHOLD_ADD_TO_ZERO       = 1d-40
  real(8), parameter :: THRESHOLD_ADD_TO_ZERO2      = THRESHOLD_ADD_TO_ZERO**2
  real(8), parameter :: THRESHOLD_RES_MAX           = 1d20
  real(8), parameter :: THRESHOLD_RES_MIN           = 1d-16
  real(8), parameter :: THRESHOLD_RES_MIN2          = THRESHOLD_RES_MIN**2
  real(8), parameter :: THRESHOLD_SCHUR_PCD_TO_ZERO = 1d-16

  character(31), DIMENSION(0:4), PARAMETER :: NAME_SOLVER = &
       & (/ "Direct (MUMPS)  ", &
       &    "BiCGStab(2)     ", &
       &    "BiCGStab (HYPRE)", &
       &    "BiCGStab(2)     ", &
       &    "PCG (HYPRE)     " /)
  character(31), DIMENSION(0:3), PARAMETER :: NAME_PRECON = &
       & (/ "Jacobi      ", &
       &    "iLU         ", &
       &    "SMG (HYPRE) ", &
       &    "PFMG (HYPRE)" /)

contains
  
  subroutine print_error_solver_choice(solver)
    use mod_parameters, only: rank
    use mod_struct_solver
    implicit none
    !--------------------------------------------------------------------------
    ! Global variables
    !--------------------------------------------------------------------------
    type(solver_t), intent(in) :: solver
    !--------------------------------------------------------------------------
    ! Local variables
    !--------------------------------------------------------------------------
    !--------------------------------------------------------------------------

    if (rank==0) then 
       write(*,*)
       write(*,*) " |--------------------------------------------------------------|"
       write(*,*) " |            ERROR IN SOLVER/PRECONDITIONNER CHOICE            |"
       write(*,*) " |--------------------------------------------------------------|"
       write(*,*) "   - solver%eqs     = ", solver%eqs
       write(*,*) "   - solver%type    = ", solver%type
       write(*,*) "   - solver%precond = ", solver%precond
       write(*,*) " |--------------------------------------------------------------|"
       write(*,*) " |                   |    vars     |        solver%precond      |"
       write(*,*) " |                   |             |             (id)           |"
       write(*,*) " |-------------------|-------------|----------------------------|"
       write(*,*) " |                   | P | V | V&P | Jacobi | iLu | SMG | PFMG  |"
       write(*,*) " | (id) solver%type  |   |   |     |  (0)   | (1) | (2) |  (3)  |"
       write(*,*) " |-------------------|---|---|-----|--------|-----|-----|-------|"
       write(*,*) " | (0)  MUMPS        | x | x |     |   -    |  -  |  -  |   -   |"
       write(*,*) " |-------------------|---|---|-----|--------|-----|-----|-------|"
       write(*,*) " | (1)  BiCGStab(2)  | x |   |     |   x    |  x  |     |       |"
       write(*,*) " |                   |   | x |     |   x    |  x  |     |       |"
       write(*,*) " |-------------------|---|---|-----|--------|-----|-----|-------|"
       write(*,*) " | (2)  BiCGStab     | x |   |     |   x    |     |  x  |   x   |"
       write(*,*) " |-------------------|---|---|-----|--------|-----|-----|-------|"
       write(*,*) " | (3)  BiCGStab(2)  | x |   |  x  |        |     |  x  |   x   |"
       write(*,*) " |-------------------|---|---|-----|--------|-----|-----|-------|"
       write(*,*) " | (4)  PCG          | x |   |     |        |     |     |   x   |"
       write(*,*) " |--------------------------------------------------------------|"
       write(*,*) "   - solvers 2 and 4 do not work with periodic BC on 1 proc      "
       write(*,*) " |--------------------------------------------------------------|"
    end if
    
  end subroutine print_error_solver_choice
  
  subroutine check_and_print_error_parallel(solver,grid)
    use mod_parameters, only: rank,nproc
    use mod_struct_grid
    use mod_struct_solver
    implicit none
    !--------------------------------------------------------------------------
    ! Global variables
    !--------------------------------------------------------------------------
    type(grid_t), intent(in)   :: grid
    type(solver_t), intent(in) :: solver
    !--------------------------------------------------------------------------
    ! Local variables
    !--------------------------------------------------------------------------
    !--------------------------------------------------------------------------
    
    if (nproc==1) then
       if (grid%periodic(1).or.grid%periodic(2).or.grid%periodic(3)) then
          call system("rm -f running done; touch done")
          call print_error_solver_choice(solver)
          stop
       end if
    end if
    
  end subroutine check_and_print_error_parallel

end module mod_solver_new_def
