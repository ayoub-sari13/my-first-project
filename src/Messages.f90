module mod_messages
  implicit none
  
  !---------------------------------------------------------------
  ! some constants
  !---------------------------------------------------------------
  integer, parameter :: MESSAGE_FATAL_ERROR = 7
  integer, parameter :: MESSAGE_ERROR       = 5
  integer, parameter :: MESSAGE_WARNING     = 3
  !---------------------------------------------------------------
  integer, parameter :: PRINT_START         = 0
  integer, parameter :: PRINT_STOP          = 1
  integer, parameter :: COLOR_NAVIER_STOKES = 34
  integer, parameter :: COLOR_RESTART       = 31
  integer, parameter :: COLOR_VOF           = 33
  !---------------------------------------------------------------

contains
  
  subroutine print_message(name,message_code)
    use mod_Parameters, only: rank
    implicit none
    !----------------------------------------------------------------------------
    ! global variables
    !---------------------------------------------------------------------------
    character(len=*), intent(in)  :: name
    integer, intent(in), optional :: message_code
    !----------------------------------------------------------------------------
    ! local variables
    !---------------------------------------------------------------------------
    character(len=20)             :: prefix
    integer                       :: n,nmessage
    !---------------------------------------------------------------------------

    !---------------------------------------------------------------------------
    ! init and prefix
    !---------------------------------------------------------------------------
    nmessage=1
    prefix=""
    if (present(message_code)) then
       nmessage=message_code
       select case(message_code)
       case(MESSAGE_FATAL_ERROR)
          prefix="[ FATAL ERROR ]"
       case(MESSAGE_ERROR)
          prefix="[ ERROR ]"
       case(MESSAGE_WARNING)
          prefix="[ WARNING ]"
       end select
    end if
    !---------------------------------------------------------------------------
    
    !---------------------------------------------------------------------------
    ! message
    !---------------------------------------------------------------------------
    if (rank==0) then
       do n=1,nmessage
          write(*,*) trim(adjustl(prefix))//" "//name
       end do
    end if
    !---------------------------------------------------------------------------

  end subroutine print_message
  
  subroutine print_step(name,icolor,istep)
    use mod_Parameters, only: rank
    implicit none
    !----------------------------------------------------------------------------
    ! global variables
    !---------------------------------------------------------------------------
    integer, intent(in)           :: icolor,istep
    character(len=*), intent(in)  :: name
    !----------------------------------------------------------------------------
    ! local variables
    !---------------------------------------------------------------------------
    character(len=2)             :: color_name
    character(len=64)            :: step_name
    character(len=100)           :: tmp_name
    !---------------------------------------------------------------------------

    if (rank>0) return

    write(color_name,100) icolor
    
    select case(istep)
    case(PRINT_START)
       write(tmp_name,101) "=================================================", &
            & trim(adjustl(name)),                                               &
            & "======================================================================================="
       write(step_name,102) tmp_name
       write(*,*)
       write(*,*) achar(27)//'[1;'//color_name//'m'//step_name//achar(27)//'[0;'//color_name//'m'
    case(PRINT_STOP)
       write(*,*) achar(27)//'[1;'//color_name//'m====================  END  ====================================='//achar(27)//'[0m'
       write(*,*)
    end select
    
100 format(i2.2)
101 format(a20,2x,a,2x,a40)
102 format(a64)

  end subroutine print_step

end module mod_messages

