!===============================================================================
module Bib_VOFLag_Vector_FromComoving2ParticleFrame
   !===============================================================================
   contains
   !-----------------------------------------------------------------------------  
   !> @author amine chadil
   ! 
   !> @brief this function gives the coordinates in the particle frame(which rotate with the 
   !! quaternion) of a vector expressed in the comoving frame and vice-versa depending of 
   !! the "in_particle_frame":
   !!  - from the comoving frame to the particle frame if in_particle_frame is true
   !!  - from the particle frame to the comoving frame if in_particle_frame is false (this is the same 
   !!  that to rotate the vector with the stored quaternion)
   !
   !> @param[in]   vl                    : la structure contenant toutes les informations
   !! relatives aux particules
   !> @param[in]   np                    : numero de la particule dans vl
   !> @param[in]  in_particle_frame      : variable donnant le sens de conversion
   !> @param[in]  vect_in                : Les coordonnees de vecteur d'entree 
   !> @param[out] vect_out               : Les coordonnees de veceur de sortie   
   !-----------------------------------------------------------------------------
   subroutine Vector_FromComoving2ParticleFrame(vl,np,ndim,in_particle_frame,vect_in,vect_out)
      !===============================================================================
      !modules
      !===============================================================================
      !-------------------------------------------------------------------------------
      !Modules de definition des structures et variables => src/mod/module_...f90
      !-------------------------------------------------------------------------------
      use Module_VOFLag_ParticlesDataStructure
      use mod_Parameters,                      only : deeptracking
      !-------------------------------------------------------------------------------

      !===============================================================================
      !declarations des variables
      !===============================================================================
      implicit none
      !-------------------------------------------------------------------------------
      !variables globales
      !-------------------------------------------------------------------------------
      real(8), dimension(3), intent(out)  :: vect_out
      real(8), dimension(3), intent(in)   :: vect_in
      integer,               intent(in)   :: np, ndim
      logical,               intent(in)   :: in_particle_frame
      type(struct_vof_lag)                :: vl
      !-------------------------------------------------------------------------------
      !variables locales 
      !-------------------------------------------------------------------------------
      real(8), dimension(4)               :: quaternion
      real(8)                             :: cos_angle,sin_angle
      !-------------------------------------------------------------------------------

      !-------------------------------------------------------------------------------
      !if (deeptracking) write(*,*) 'entree Vector_FromComoving2ParticleFrame'
      !-------------------------------------------------------------------------------

      !-------------------------------------------------------------------------------
      !initialisation
      !-------------------------------------------------------------------------------
      if (in_particle_frame) then
         cos_angle = cos(vl%objet(np)%orientation(1))
         sin_angle = sin(vl%objet(np)%orientation(1))
         quaternion = vl%objet(np)%orientation
      else
         cos_angle = cos(-vl%objet(np)%orientation(1))
         sin_angle = sin(-vl%objet(np)%orientation(1))
         quaternion(1) = vl%objet(np)%orientation(1)
         quaternion(2:4) = -vl%objet(np)%orientation(2:4)
      end if

      !-------------------------------------------------------------------------------
      !Rotation 2D
      !-------------------------------------------------------------------------------
      if (ndim==2) then
         vect_out(1) =  cos_angle * vect_in(1) + sin_angle * vect_in(2)
         vect_out(2) = -sin_angle * vect_in(1) + cos_angle * vect_in(2)

         !-------------------------------------------------------------------------------
         !Rotation 3D realisee grace aux quaternions
         !-------------------------------------------------------------------------------
      else
         vect_out(1) = (quaternion(1)**2+quaternion(2)**2-quaternion(3)**2-quaternion(4)**2) * vect_in(1) + &
               2.0*( quaternion(2) * quaternion(3) + quaternion(1) * quaternion(4) )          * vect_in(2) + &
               2.0*( quaternion(2) * quaternion(4) - quaternion(1) * quaternion(3) )          * vect_in(3)

         vect_out(2) = 2.0*( quaternion(2)*quaternion(3) - quaternion(1)*quaternion(4) )     * vect_in(1) + &
               (quaternion(1)**2-quaternion(2)**2+quaternion(3)**2-quaternion(4)**2)          * vect_in(2) + &
               2.0*( quaternion(3) * quaternion(4) + quaternion(1) * quaternion(2) )          * vect_in(3) 

         vect_out(3) = 2.0*( quaternion(2) * quaternion(4) + quaternion(1) * quaternion(3) ) * vect_in(1) + &
               2.0*( quaternion(3) * quaternion(4) - quaternion(1) * quaternion(2) )          * vect_in(2) + &
               (quaternion(1)**2-quaternion(2)**2-quaternion(3)**2+quaternion(4)**2)          * vect_in(3)
      end if

      !-------------------------------------------------------------------------------
      !if (deeptracking) write(*,*) 'sortie Vector_FromComoving2ParticleFrame'
      !-------------------------------------------------------------------------------
   end subroutine Vector_FromComoving2ParticleFrame

   !===============================================================================
end module Bib_VOFLag_Vector_FromComoving2ParticleFrame
!===============================================================================
