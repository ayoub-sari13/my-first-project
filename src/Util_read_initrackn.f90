!===============================================================================
 module Bib_module_initrackn
 contains
!===============================================================================
!*******************************************************************************
 subroutine initrackn (ftp,fichobj,kft,nfich,lchar,ndim)
!*******************************************************************************
!-------------------------------------------------------------------------------
 implicit none
!-------------------------------------------------------------------------------
!variables globales
!-------------------------------------------------------------------------------
 real(8), dimension(:,:,:), allocatable      :: ftp
 integer                                     :: kft,ndim,nfich,lchar
 character(len=lchar)                        :: fichobj
!-------------------------------------------------------------------------------
!variables locales
!-------------------------------------------------------------------------------
real(8), dimension(:,:,:), allocatable       :: ftp1
real(8), dimension(:,:),   allocatable       :: point 
integer, dimension(:,:),   allocatable       :: segment
character(len=2)                             :: c1
character(len=7)                             :: c2 
integer                                      :: lp,npoint,kft1,nf,debv,debf,end,i
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
   if (ndim.eq.2) then
!-------------------------------------------------------------------------------
    kft=0
    deallocate(ftp)
    allocate(ftp(ndim,ndim,kft)); ftp=0.d0
!    do nf=1,size(fichobj)
!     fichobj(nf)=trim(fichobj(nf))
     fichobj=trim(fichobj)
     
!     if ( fichobj(nf)(len_trim(fichobj(nf))-2:len_trim(fichobj(nf)))=='mxa') then
     if ( fichobj(len_trim(fichobj)-2:len_trim(fichobj))=='mxa') then

!      open(unit=666,file=fichobj(nf),status='unknown')
      open(unit=666,file=fichobj,status='unknown')
      rewind 666
      read(666,*) npoint,kft1
      allocate(ftp1(ndim,ndim,kft)); ftp1=ftp
      deallocate(ftp)
      allocate(ftp(ndim,ndim,kft+kft1)); ftp(:,:,1:kft)=ftp1
      deallocate(ftp1)
      allocate(point(npoint,ndim),segment(kft1,ndim))
      do lp=1,npoint
       read(666,*) point(lp,1),point(lp,2)
      enddo
      do lp=1,kft1
       read(666,*) segment(lp,1),segment(lp,2)
      enddo
      do lp=1,kft1
       ftp(1,1,kft+lp)=point(segment(lp,1),1)
       ftp(1,2,kft+lp)=point(segment(lp,2),1)
       ftp(2,1,kft+lp)=point(segment(lp,1),2)
       ftp(2,2,kft+lp)=point(segment(lp,2),2)
      enddo
      kft=kft+kft1
      deallocate(segment,point)
      close(666)

!     elseif ( fichobj(nf)(len_trim(fichobj(nf))-2:len_trim(fichobj(nf)))=='obj') then
     elseif ( fichobj(len_trim(fichobj)-2:len_trim(fichobj))=='obj') then

!      open(unit=666,file=fichobj(nf),status='unknown')
      open(unit=666,file=fichobj,status='unknown')
      rewind 666 
      npoint=0
      kft1=0
      debv=0
      debf=0
      end=1
      i=0
      do while (end>=0)
       read(666,*,iostat=end) c1
       if (end>=0) then
        i=i+1
        if (c1=='v ')     npoint=npoint+1
        if (npoint==1)    debv=i
        if (c1=='f ')     kft1=kft1+1
        if (kft1==1)      debf=i
       endif
      enddo
      close(unit=666)
      allocate(ftp1(ndim,ndim,kft)); ftp1=ftp
      deallocate(ftp)
      allocate(ftp(ndim,ndim,kft+kft1)); ftp(:,:,1:kft)=ftp1
      deallocate(ftp1)
      allocate(point(npoint,ndim),segment(kft1,ndim))
!      open(unit=666,file=fichobj(nf),status='unknown')
      open(unit=666,file=fichobj,status='unknown')
      rewind 666 
      do lp=1,debv-1
       read(666,*) c1
      enddo
      do lp=1,npoint
       read(666,*) c1,point(lp,1),point(lp,2)
      enddo
      do lp=1,debf-debv-npoint
       read(666,*) c1
      enddo
      do lp=1,kft1
       read(666,*) c1,segment(lp,1),segment(lp,2)
      enddo
      do lp=1,kft1
       ftp(1,1,kft+lp)=point(segment(lp,1),1)
       ftp(1,2,kft+lp)=point(segment(lp,2),1)
       ftp(2,1,kft+lp)=point(segment(lp,1),2)
       ftp(2,2,kft+lp)=point(segment(lp,2),2)
      enddo
      kft=kft+kft1
      deallocate(segment,point)
      close(666)

! elseif ( fichobj(nf)(len_trim(fichobj(nf))-2:len_trim(fichobj(nf)))=='stl') then
 elseif ( fichobj(len_trim(fichobj)-2:len_trim(fichobj))=='stl') then

!      open(unit=666,file=fichobj(nf),status='unknown')
      open(unit=666,file=fichobj,status='unknown')
      rewind 666 
      npoint=0
      kft1=0
      debv=0
      debf=0
      end=1
      i=0
      do while (end>=0)
       read(666,*,iostat=end) c2
       if (end>=0) then
        i=i+1
        if (c2=='vertex ') then
           npoint=npoint+2
           kft1=kft1+1
           read(666,*,iostat=end) c2
        endif
       endif
      enddo
      close(unit=666)
      allocate(ftp1(ndim,ndim,kft)); ftp1=ftp
      deallocate(ftp)
      allocate(ftp(ndim,ndim,kft+kft1)); ftp(:,:,1:kft)=ftp1
      deallocate(ftp1)
      allocate(point(npoint,ndim),segment(kft1,ndim))
!      open(unit=666,file=fichobj(nf),status='unknown')
      open(unit=666,file=fichobj,status='unknown')
      rewind 666 
      read(666,*) c2
      do lp=1,kft1
       read(666,*) c1
       read(666,*) c1
       read(666,*,iostat=end) c2,point((lp-1)*2+1,1),point((lp-1)*3+1,2)
       read(666,*,iostat=end) c2,point((lp-1)*2+2,1),point((lp-1)*3+2,2)
       read(666,*) c1
       read(666,*) c1
      enddo
      do lp=1,kft1
       ftp(1,1,kft+lp)=point((lp-1)*2+1,1)
       ftp(1,2,kft+lp)=point((lp-1)*2+2,1)
       ftp(2,1,kft+lp)=point((lp-1)*2+1,2)
       ftp(2,2,kft+lp)=point((lp-1)*2+2,2)
      enddo
      kft=kft+kft1
      deallocate(segment,point)
      close(666)

     endif
!    enddo
!-------------------------------------------------------------------------------
   endif
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
   if (ndim.eq.3) then
!-------------------------------------------------------------------------------
    kft=0
    deallocate(ftp)
    allocate(ftp(ndim,ndim,kft)); ftp=0.d0
!    do nf=1,size(fichobj)
!     fichobj(nf)=trim(fichobj(nf))
     fichobj=trim(fichobj)
    
!     if ( fichobj(nf)(len_trim(fichobj(nf))-2:len_trim(fichobj(nf)))=='mxa') then
     if ( fichobj(len_trim(fichobj)-2:len_trim(fichobj))=='mxa') then

!      open(unit=666,file=fichobj(nf),status='unknown')
      open(unit=666,file=fichobj,status='unknown')
      rewind 666
      read(666,*) npoint,kft1
      allocate(ftp1(ndim,ndim,kft)); ftp1=ftp
      deallocate(ftp)
      allocate(ftp(ndim,ndim,kft+kft1)); ftp(:,:,1:kft)=ftp1
      deallocate(ftp1)
      allocate(point(npoint,ndim),segment(kft1,ndim))
      do lp=1,npoint
       read(666,*) point(lp,1),point(lp,2),point(lp,3)
      enddo
      do lp=1,kft1
       read(666,*) segment(lp,1),segment(lp,2),segment(lp,3)
      enddo
      do lp=1,kft1
       ftp(1,1,kft+lp)=point(segment(lp,1),1)
       ftp(1,2,kft+lp)=point(segment(lp,2),1)
       ftp(1,3,kft+lp)=point(segment(lp,3),1)
       ftp(2,1,kft+lp)=point(segment(lp,1),2)
       ftp(2,2,kft+lp)=point(segment(lp,2),2)
       ftp(2,3,kft+lp)=point(segment(lp,3),2)
       ftp(3,1,kft+lp)=point(segment(lp,1),3)
       ftp(3,2,kft+lp)=point(segment(lp,2),3)
       ftp(3,3,kft+lp)=point(segment(lp,3),3)
      enddo
      kft=kft+kft1
      deallocate(segment,point)
      close(666)

!     elseif ( fichobj(nf)(len_trim(fichobj(nf))-2:len_trim(fichobj(nf)))=='obj') then
     elseif ( fichobj(len_trim(fichobj)-2:len_trim(fichobj))=='obj') then

!      open(unit=666,file=fichobj(nf),status='unknown')
      open(unit=666,file=fichobj,status='unknown')
      rewind 666 
      npoint=0
      kft1=0
      debv=0
      debf=0
      end=1
      i=0
      do while (end>=0)
       read(666,*,iostat=end) c1
       if (end>=0) then
        i=i+1
        if (c1=='v ')     npoint=npoint+1
        if (npoint==1)    debv=i
        if (c1=='f ')     kft1=kft1+1
        if (kft1==1)      debf=i
       endif
      enddo
      close(unit=666)
      allocate(ftp1(ndim,ndim,kft)); ftp1=ftp
      deallocate(ftp)
      allocate(ftp(ndim,ndim,kft+kft1)); ftp(:,:,1:kft)=ftp1
      deallocate(ftp1)
      allocate(point(npoint,ndim),segment(kft1,ndim))
!      open(unit=666,file=fichobj(nf),status='unknown')
      open(unit=666,file=fichobj,status='unknown')
      rewind 666 
      do lp=1,debv-1
       read(666,*) c1
      enddo
      do lp=1,npoint
       read(666,*) c1,point(lp,1),point(lp,2),point(lp,3)
      enddo
      do lp=1,debf-debv-npoint
       read(666,*) c1
      enddo
      do lp=1,kft1
       read(666,*) c1,segment(lp,1),segment(lp,2),segment(lp,3)
      enddo
      do lp=1,kft1
       ftp(1,1,kft+lp)=point(segment(lp,1),1)
       ftp(1,2,kft+lp)=point(segment(lp,2),1)
       ftp(1,3,kft+lp)=point(segment(lp,3),1)
       ftp(2,1,kft+lp)=point(segment(lp,1),2)
       ftp(2,2,kft+lp)=point(segment(lp,2),2)
       ftp(2,3,kft+lp)=point(segment(lp,3),2)
       ftp(3,1,kft+lp)=point(segment(lp,1),3)
       ftp(3,2,kft+lp)=point(segment(lp,2),3)
       ftp(3,3,kft+lp)=point(segment(lp,3),3)
      enddo
      kft=kft+kft1
      deallocate(segment,point)
      close(666)
      
! elseif ( fichobj(nf)(len_trim(fichobj(nf))-2:len_trim(fichobj(nf)))=='stl') then
 elseif ( fichobj(len_trim(fichobj)-2:len_trim(fichobj))=='stl') then

!      open(unit=666,file=fichobj(nf),status='unknown')
      open(unit=666,file=fichobj,status='unknown')
      rewind 666 
      npoint=0
      kft1=0
      debv=0
      debf=0
      end=1
      i=0
      do while (end>=0)
       read(666,*,iostat=end) c2
       if (end>=0) then
        i=i+1
        if (c2=='vertex ') then
           npoint=npoint+3
           kft1=kft1+1
           read(666,*,iostat=end) c2
           read(666,*,iostat=end) c2
        endif
       endif
      enddo
      close(unit=666)
      allocate(ftp1(ndim,ndim,kft)); ftp1=ftp
      deallocate(ftp)
      allocate(ftp(ndim,ndim,kft+kft1)); ftp(:,:,1:kft)=ftp1
      deallocate(ftp1)
      allocate(point(npoint,ndim),segment(kft1,ndim))
!      open(unit=666,file=fichobj(nf),status='unknown')
      open(unit=666,file=fichobj,status='unknown')
      rewind 666 
      read(666,*) c2
      do lp=1,kft1
       read(666,*) c1
       read(666,*) c1
       read(666,*,iostat=end) c2,point((lp-1)*3+1,1),point((lp-1)*3+1,2),point((lp-1)*3+1,3)
       read(666,*,iostat=end) c2,point((lp-1)*3+2,1),point((lp-1)*3+2,2),point((lp-1)*3+2,3)
       read(666,*,iostat=end) c2,point((lp-1)*3+3,1),point((lp-1)*3+3,2),point((lp-1)*3+3,3)
       read(666,*) c1
       read(666,*) c1
      enddo
      do lp=1,kft1
       ftp(1,1,kft+lp)=point((lp-1)*3+1,1)
       ftp(1,2,kft+lp)=point((lp-1)*3+2,1)
       ftp(1,3,kft+lp)=point((lp-1)*3+3,1)
       ftp(2,1,kft+lp)=point((lp-1)*3+1,2)
       ftp(2,2,kft+lp)=point((lp-1)*3+2,2)
       ftp(2,3,kft+lp)=point((lp-1)*3+3,2)
       ftp(3,1,kft+lp)=point((lp-1)*3+1,3)
       ftp(3,2,kft+lp)=point((lp-1)*3+2,3)
       ftp(3,3,kft+lp)=point((lp-1)*3+3,3)
      enddo
      kft=kft+kft1
      deallocate(segment,point)
      close(666)
     endif

!    enddo
!-------------------------------------------------------------------------------
   endif
!-------------------------------------------------------------------------------

!
 return
!
 end subroutine initrackn
!===============================================================================
 end module bib_module_initrackn
!===============================================================================