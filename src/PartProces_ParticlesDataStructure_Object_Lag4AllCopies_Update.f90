!===============================================================================
module Bib_VOFLag_ParticlesDataStructure_Object_Lag4AllCopies_Update
  !===============================================================================
  contains
  !---------------------------------------------------------------------------  
  !> @author amine chadil
  ! 
  !> @brief remplir le tableau des coordonnees lagrangiens des elements de l'objet 
  !
  !> @param[out] vl  : la structure contenant toutes les informations
  !! relatives aux particules    
  !> @param[in]  np   : le numero de l'objet 
  !---------------------------------------------------------------------------  
  subroutine ParticlesDataStructure_Object_Lag4AllCopies_Update(vl,np)
    !===============================================================================
    !modules
    !===============================================================================
    !-------------------------------------------------------------------------------
    !Modules de definition des structures et variables => src/mod/module_...f90
    !-------------------------------------------------------------------------------
    use Module_VOFLag_ParticlesDataStructure
    use mod_Parameters,                      only : deeptracking,dim
    !-------------------------------------------------------------------------------
    !Modules de subroutines de la librairie RESPECT => src/Bib_VOFLag_...f90
    !-------------------------------------------------------------------------------
    use Bib_VOFLag_Vector_FromComoving2ParticleFrame
    !-------------------------------------------------------------------------------

    !===============================================================================
    !declarations des variables
    !===============================================================================
    implicit none
    !-------------------------------------------------------------------------------
    !variables globales
    !-------------------------------------------------------------------------------
    integer,               intent(in)            :: np
    type(struct_vof_lag)                         :: vl
    !-------------------------------------------------------------------------------
    !variables locales 
    !-------------------------------------------------------------------------------
    real(8), dimension(dim,dim,vl%objet(np)%kkl) :: lag_before,lag_after
    integer                                      :: k,nd
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'entree ParticlesDataStructure_Object_Lag4AllCopies_Update'
    !-------------------------------------------------------------------------------

    lag_before = vl%objet(np)%lag_for_all_copies
    lag_after  = 0.D0

    do k=1,vl%objet(np)%kkl
       do nd=1,dim
          !-------------------------------------------------------------------------------
          ! rotation des point du tableau lag_all_copies pour avoir l'orientation actuelle
          ! de la particule
          !-------------------------------------------------------------------------------
          call Vector_FromComoving2ParticleFrame(vl,np,dim,.false.,lag_before(:,nd,k),&
               lag_after(:,nd,k))
       end do
    end do

    vl%objet(np)%lag_for_all_copies = lag_after

    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'sortie ParticlesDataStructure_Object_Lag4AllCopies_Update'
    !-------------------------------------------------------------------------------
  end subroutine ParticlesDataStructure_Object_Lag4AllCopies_Update
  !===============================================================================
end module Bib_VOFLag_ParticlesDataStructure_Object_Lag4AllCopies_Update
!===============================================================================
