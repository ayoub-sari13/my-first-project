module mod_allocate
  implicit none
  
  integer, parameter :: ALLOCATE_ON_P_MESH = 0
  integer, parameter :: ALLOCATE_ON_U_MESH = 1
  integer, parameter :: ALLOCATE_ON_V_MESH = 2
  integer, parameter :: ALLOCATE_ON_W_MESH = 3
  
contains
  
  subroutine allocate_array_3(ialloc,array,grid,init_value)
    use mod_struct_grid
    implicit none
    !--------------------------------------------------------------------------
    ! Global variables
    !--------------------------------------------------------------------------
    integer, intent(in)                                   :: ialloc
    real(8), optional, intent(in)                         :: init_value
    real(8), dimension(:,:,:), allocatable, intent(inout) :: array
    type(grid_t), intent(in)                              :: grid
    !--------------------------------------------------------------------------
    ! Local variables
    !--------------------------------------------------------------------------
    !--------------------------------------------------------------------------

    if (allocated(array)) return
    
    select case(ialloc) 
    case(ALLOCATE_ON_P_MESH)
       allocate(array(                         &
            & grid%sx-grid%gx:grid%ex+grid%gx, &
            & grid%sy-grid%gy:grid%ey+grid%gy, &
            & grid%sz-grid%gz:grid%ez+grid%gz ))
    case (ALLOCATE_ON_U_MESH)
       allocate(array(                           &
            & grid%sxu-grid%gx:grid%exu+grid%gx, &
            & grid%syu-grid%gy:grid%eyu+grid%gy, &
            & grid%szu-grid%gz:grid%ezu+grid%gz ))
    case(ALLOCATE_ON_V_MESH)
       allocate(array(                           &
            & grid%sxv-grid%gx:grid%exv+grid%gx, &
            & grid%syv-grid%gy:grid%eyv+grid%gy, &
            & grid%szv-grid%gz:grid%ezv+grid%gz ))
    case(ALLOCATE_ON_W_MESH)
       if (grid%dim==2) return
       allocate(array(                           &
            & grid%sxw-grid%gx:grid%exw+grid%gx, &
            & grid%syw-grid%gy:grid%eyw+grid%gy, &
            & grid%szw-grid%gz:grid%ezw+grid%gz ))
    case default
    end select

    array = 0

    if (present(init_value)) array = init_value
    
  end subroutine allocate_array_3

  subroutine deallocate_array_3(array)
    implicit none
    !--------------------------------------------------------------------------
    ! Global variables
    !--------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable, intent(inout) :: array
    !--------------------------------------------------------------------------
    ! Local variables
    !--------------------------------------------------------------------------
    !--------------------------------------------------------------------------

    if (allocated(array)) deallocate(array)
    
  end subroutine deallocate_array_3

end module mod_allocate
