!===============================================================================
module Bib_VOFLag_SubDomain_Limited_PointIn
  !===============================================================================
  contains
  !-----------------------------------------------------------------------------  
  !> @author jorge cesar brandle de motta, amine chadil
  ! 
  !> @brief dit si le point p appartient au domaine du proc courant sans les mailles 
  !! de recouverement     
  !
  !> @param [in]  p              : les coordonnees du point
  !> @param [out] in_domain_int  : vrai si le point est dans le domaine interieur 
  !! du proc courant
  !-----------------------------------------------------------------------------
  subroutine SubDomain_Limited_PointIn(p,in_domain_int)
    !===============================================================================
    !modules
    !===============================================================================
    !-------------------------------------------------------------------------------
    !Modules de definition des structures et variables => src/mod/module_...f90
    !-------------------------------------------------------------------------------
    use Module_VOFLag_SubDomain_Data, only : s_domaines_int
    use mod_Parameters,               only : deeptracking,dim
    !-------------------------------------------------------------------------------

    !===============================================================================
    !declarations des variables
    !===============================================================================
    implicit none
    !-------------------------------------------------------------------------------
    !variables globales
    !-------------------------------------------------------------------------------
    real(8), dimension(3), intent(in)   :: p
    logical              , intent(out)  :: in_domain_int
    !-------------------------------------------------------------------------------
    !variables locales 
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    !if (deeptracking) write(*,*) 'entree SubDomain_Limited_PointIn'
    !-------------------------------------------------------------------------------

    in_domain_int =.true.

    in_domain_int = ( ((s_domaines_int(2,1)-p(1)).gt.1d-15).and.&
                      ((s_domaines_int(1,1)-p(1)).le.1d-15).and.&
                      ((s_domaines_int(2,2)-p(2)).gt.1d-15).and.&
                      ((s_domaines_int(1,2)-p(2)).le.1d-15) )
    if (dim==3) in_domain_int=in_domain_int.and.&
                    ( ((s_domaines_int(2,3)-p(3)).gt.1d-15).and.&
                      ((s_domaines_int(1,3)-p(3)).le.1d-15) )

    !-------------------------------------------------------------------------------
    !if (deeptracking) write(*,*) 'sortie SubDomain_Limited_PointIn'
    !-------------------------------------------------------------------------------
  end subroutine SubDomain_Limited_PointIn

  !===============================================================================
end module Bib_VOFLag_SubDomain_Limited_PointIn
!===============================================================================