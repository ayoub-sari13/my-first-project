
  if (once) then
     call InitCoeffVS
     once=.false.
  end if
  
  if (rank==0 .and. print_info) then
     write(*,*) "AEROCAB V2"
     write(*,*) "CHAUSSETTE"
  end if

  ! debit total
  qtot=0

  !-------------------------------------------------------
  ! CHAUSSETTE
  !-------------------------------------------------------
  ! 2 x 
  ! largeur 40 cm
  ! hauteur 250 cm
  !-------------------------------------------------------
  xc=0.4d0

  !-------------------------------------------------------
  ! dimensions
  !-------------------------------------------------------
  x_max=xmax
  x_min=x_max-xc
  y_min=ymin
  y_max=y_min+xc
  z_min=zmin
  z_max=zmax
  !-------------------------------------------------------
  
  !-------------------------------------------------------
  ! amplitude vitesse/debit
  !-------------------------------------------------------
  surfu=1
  surfv=1
  surfw=1
  qinf=0.3d0 ! m3/s total chaussette
  !-------------------------------------------------------

  !-------------------------------------------------------
  ! surface computation (1er passage) penalisation (2nd)
  !-------------------------------------------------------
  
  do n=1,2

     uinf=-qinf/2/surfu
     vinf=+qinf/2/surfv
     winf=0

     !write(*,*) uinf,vinf

     surfu=0
     surfv=0
     do k=szu,ezu
        do j=syu,eyu
           do i=sxu,exu
              if (grid_x(i)>=x_min.and.grid_x(i)<=x_max) then
                 if (grid_y(j)>=y_min.and.grid_y(j)<=y_max) then
                    if (grid_z(k)>=z_min.and.grid_z(k)<=z_max) then
                       !-------------------------------------------------------
                       ! U component
                       !-------------------------------------------------------
                       if (j>gsy.and.k>gsz) then
                          if (i==gex) then
                             surfu=surfu+CoeffS(i,j,k)*dy(j)*dz(k)
                             uin(i+1,j,k)=2*uinf
                             penu(i+1,j,k,:)=0
                             penu(i+1,j,k,1)=pen_amp/2
                             penu(i+1,j,k,2)=pen_amp/2
                          else
                             slvu(i,j,k)=pen_amp
                             smvu(i,j,k)=uinf*pen_amp
                             slvu(i-1,j,k)=pen_amp
                             smvu(i-1,j,k)=uinf*pen_amp
                          end if
                       end if
                    end if
                 end if
              end if
           end do
        end do
     end do
     do k=szv,ezv
        do j=syv,eyv
           do i=sxv,exv
              if (grid_x(i)>=x_min.and.grid_x(i)<=x_max) then
                 if (grid_y(j)>=y_min.and.grid_y(j)<=y_max) then
                    if (grid_z(k)>=z_min.and.grid_z(k)<=z_max) then
                       !-------------------------------------------------------
                       ! V component
                       !-------------------------------------------------------
                       if (i<gex.and.k>gsz) then
                          if (j==gsy) then
                             surfv=surfv+CoeffS(i,j,k)*dx(i)*dz(k)
                             vin(i,j,k)=2*vinf
                             penv(i,j,k,:)=0
                             penv(i,j,k,1)=pen_amp/2
                             penv(i,j,k,5)=pen_amp/2
                          else
                             slvv(i,j,k)=pen_amp
                             smvv(i,j,k)=vinf*pen_amp
                             slvv(i,j+1,k)=pen_amp
                             smvv(i,j+1,k)=vinf*pen_amp
                          end if
                       end if
                       !-------------------------------------------------------
                    end if
                 end if
              end if
           end do
        end do
     end do

     call MPI_ALLREDUCE(surfu,tmp,1,MPI_DOUBLE_PRECISION,MPI_SUM,COMM3D,CODE); surfu=tmp
     call MPI_ALLREDUCE(surfv,tmp,1,MPI_DOUBLE_PRECISION,MPI_SUM,COMM3D,CODE); surfv=tmp
     
  end do
  
  qtot=qtot+qinf
  if (rank==0 .and. print_info) then
     write(*,*) "--------------------------------------------------------------------------"
     write(*,*) "Q_CHAUSETTE : ", vinf*surfv, uinf*surfu, qinf/2
     write(*,*) "Q_TOT       : ", abs(vinf)*surfv+abs(uinf)*surfu, qinf
     write(*,*) "--------------------------------------------------------------------------"
     write(*,*) "VITESSE X   : ", abs(uinf)
     write(*,*) "SURFACE X   : ", surfu,(y_max-y_min)*(z_max-z_min)
     write(*,*) "DEBIT X     : ", surfu*abs(uinf)
     write(*,*) "--------------------------------------------------------------------------"
     write(*,*) "VITESSE Y   : ", abs(vinf)
     write(*,*) "SURFACE Y   : ", surfv,(x_max-x_min)*(z_max-z_min)
     write(*,*) "DEBIT Y     : ", surfv*abs(vinf)
  end if


  !-------------------------------------------------------
  ! FIN CHAUSSETTE
  !-------------------------------------------------------


  !-------------------------------------------------------
  !  SUPPORT ET SORBONNE
  !-------------------------------------------------------
  ! centre en         xc
  ! 1/2 largeur       x0
  ! epaisseur parois  x1
  !-------------------------------------------------------
  xc=xmin+(xmax-xmin)/2 ! ?
  x0_sorbonne=xc
  x0=l_sorbonne/2
  x1=0.06d0
  !-------------------------------------------------------
  ! dimensions support 
  !-------------------------------------------------------
  x_min=xc-(x0+x1)
  x_max=xc+(x0+x1)
  y_max=ymax
  y_min=y_max-0.8d0
  z_min=zmin
  z_max=z_min+1
  !-------------------------------------------------------
  call Penalize_cubic_block( &
       & x_min,x_max,        &
       & y_min,y_max,        &
       & z_min,z_max,        &
       & slvu,slvv,slvw)
  !-------------------------------------------------------
  ! dimensions face gauche 
  !-------------------------------------------------------
  x_max=xc-x0
  x_min=x_max-x1
  y_max=ymax
  y_min=y_max-0.8d0
  z_min=zmin+1
  z_max=z_min+1.4d0
  !-------------------------------------------------------
  x_min_injection=x_max
  !-------------------------------------------------------
  call Penalize_cubic_block( &
       & x_min,x_max,        &
       & y_min,y_max,        &
       & z_min,z_max,        &
       & slvu,slvv,slvw)
  !-------------------------------------------------------
  ! dimensions face droite 
  !-------------------------------------------------------
  x_min=xc+x0
  x_max=x_min+x1
  !-------------------------------------------------------
  x_max_injection=x_min
  !-------------------------------------------------------
  call Penalize_cubic_block( &
       & x_min,x_max,        &
       & y_min,y_max,        &
       & z_min,z_max,        &
       & slvu,slvv,slvw)
  !-------------------------------------------------------
  ! dimensions face haute 
  !-------------------------------------------------------
  x_min=xc-(x0+x1)
  x_max=xc+(x0+x1)
  y_max=ymax
  y_min=y_max-0.8d0
  z_min=z_max ! precedente valeur
  z_max=zmax
  !-------------------------------------------------------
  call Penalize_cubic_block( &
       & x_min,x_max,        &
       & y_min,y_max,        &
       & z_min,z_max,        &
       & slvu,slvv,slvw)
  !-------------------------------------------------------
  ! dimensions face frontale
  !-------------------------------------------------------
  x_min=xc-(x0+x1)
  x_max=xc+(x0+x1)
  y_min=ymax-0.8d0
  y_max=y_min+x1
  z_min=zmin+1+0.4d0
  z_max=zmax
  !-------------------------------------------------------
  call Penalize_cubic_block( &
       & x_min,x_max,        &
       & y_min,y_max,        &
       & z_min,z_max,        &
       & slvu,slvv,slvw)
  !-------------------------------------------------------

  !-------------------------------------------------------
  ! INTERIEUR 
  !-------------------------------------------------------
  ! epaisseur
  !-------------------------------------------------------
  x1=0.02d0 
  
  !-------------------------------------------------------
  ! plaque vertical fond
  !-------------------------------------------------------
  x_min=xc-(x0+x1)
  x_max=xc+(x0+x1)
  y_max=ymax-0.1d0
  y_min=y_max-x1
  z_min=zmin+1+0.1d0
  z_max=z_min+0.6d0
  !-------------------------------------------------------
  call Penalize_cubic_block( &
       & x_min,x_max,        &
       & y_min,y_max,        &
       & z_min,z_max,        &
       & slvu,slvv,slvw)
  !-------------------------------------------------------

  !-------------------------------------------------------
  ! plaque horizontal haut
  !-------------------------------------------------------
  x_min=xc-(x0+x1)
  x_max=xc+(x0+x1)
  y_min=ymax-0.8d0
  y_max=y_min+0.5d0
  z_min=zmin+1+1.4d0-0.1d0
  z_max=z_min+x1
  !-------------------------------------------------------
  call Penalize_cubic_block( &
       & x_min,x_max,        &
       & y_min,y_max,        &
       & z_min,z_max,        &
       & slvu,slvv,slvw)
  !-------------------------------------------------------

  !-------------------------------------------------------
  ! plaque oblique
  !-------------------------------------------------------
  alpha=max(6*(maxval(dx)*maxval(dy)*maxval(dz))**d1p3,3*x1)
  x_min=xc-(x0+x1)
  x_max=xc+(x0+x1)
  ! dans le plan (y,z)
  y_max=ymax-0.1d0
  y_min=y_max-0.2d0
  z_min=zmin+1+0.1d0+0.6d0+0.1d0
  z_max=z_min+0.4d0
  a=(z_max-z_min)/(y_min-y_max)
  b=z_max-a*y_min
  !-------------------------------------------------------
  uinf=0
  vinf=0
  winf=0
  !-------------------------------------------------------
  do k=sz,ez
     do j=sy,ey
        do i=sx,ex
           if ( grid_z(k)>=z_min .and. grid_z(k)<=z_max ) then
              if ( grid_y(j)>=y_min .and. grid_y(j)<=y_max ) then
                 if ( grid_x(i)>=x_min .and. grid_x(i)<=x_max ) then
                    if ( grid_z(k) >= a*grid_y(j) + b ) then
                       if ( grid_z(k) <= a*grid_y(j) + b + alpha ) then

                          slvu(i,j,k)=pen_amp
                          smvu(i,j,k)=uinf*pen_amp
                          slvu(i+1,j,k)=pen_amp
                          smvu(i+1,j,k)=uinf*pen_amp

                          slvv(i,j,k)=pen_amp
                          smvv(i,j,k)=vinf*pen_amp
                          slvv(i,j+1,k)=pen_amp
                          smvv(i,j+1,k)=vinf*pen_amp

                          slvw(i,j,k)=pen_amp
                          smvw(i,j,k)=winf*pen_amp
                          slvw(i,j,k+1)=pen_amp
                          smvw(i,j,k+1)=winf*pen_amp

                       end if
                    end if
                 end if
              end if
           end if
        end do
     end do
  end do
  !-------------------------------------------------------

  
  !-------------------------------------------------------
  ! FIN SUPPORT ET SORBONNE
  !-------------------------------------------------------


  if (rank==0 .and. print_info) then
     write(*,*) "--------------------------------------------------------------------------"
     write(*,*) "PLATEAU INJECTION"
  end if

  
  !-------------------------------------------------------
  ! PLATEAU INJECTION
  !-------------------------------------------------------
  
  !-------------------------------------------------------
  ! dimensions
  !-------------------------------------------------------
  x_min=x_min_injection
  x_max=x_max_injection
  y_min=ymax-0.8d0+0.02d0 ! 2 cm a partir du bord
  y_max=y_min+0.2d0
  z_min=zmin
  z_max=z_min+1
  !-------------------------------------------------------
  y_min_injection=y_min
  y_max_injection=y_max
  z_min_injection=z_min
  z_max_injection=z_max
  !-------------------------------------------------------

  !-------------------------------------------------------
  ! amplitude vitesse/debit
  !-------------------------------------------------------
  surfu=1
  surfv=1
  surfw=1
  qinf=q_gaz
  !-------------------------------------------------------

  !-------------------------------------------------------
  ! surface computation (1er passage) penalisation (2nd)
  !-------------------------------------------------------

  do n=1,2

     uinf=0
     vinf=0
     winf=+qinf/surfw

     !write(*,*) winf

     surfw=0
     do k=szw,ezw
        do j=syw,eyw
           do i=sxw,exw
              if (grid_x(i)>x_min.and.grid_x(i)<x_max) then
                 if (grid_y(j)>=y_min.and.grid_y(j)<=y_max) then
                    if (grid_z(k)>=z_min.and.grid_z(k)<=z_max) then
                       !-------------------------------------------------------
                       ! W component
                       !-------------------------------------------------------
                       if (k==gsz) then
                          surfw=surfw+CoeffS(i,j,k)*dx(i)*dy(j)
                          win(i,j,k)=2*winf
                          penw(i,j,k,:)=0
                          penw(i,j,k,1)=pen_amp/2
                          penw(i,j,k,7)=pen_amp/2
                       else
                          
!!$                          ! remove penalisation 
!!$                          slvu(i,j,k)=0
!!$                          smvu(i,j,k)=0
!!$                          slvu(i+1,j,k)=0
!!$                          smvu(i+1,j,k)=0
!!$                          
!!$                          slvv(i,j,k)=0
!!$                          smvv(i,j,k)=0
!!$                          slvv(i,j+1,k)=0
!!$                          smvv(i,j+1,k)=0
!!$                          
!!$                          slvw(i,j,k)=0
!!$                          smvw(i,j,k)=0
!!$                          slvw(i,j,k+1)=0
!!$                          smvw(i,j,k+1)=0

                          
                          slvw(i,j,k)=pen_amp
                          smvw(i,j,k)=winf*pen_amp
                          slvw(i,j,k+1)=pen_amp
                          smvw(i,j,k+1)=winf*pen_amp
                       end if
                       !-------------------------------------------------------
                    end if
                 end if
              end if
           end do
        end do
     end do

     call MPI_ALLREDUCE(surfw,tmp,1,MPI_DOUBLE_PRECISION,MPI_SUM,COMM3D,CODE); surfw=tmp
     
  end do
  
  qtot=qtot+qinf
  
  if (rank==0 .and. print_info) then
     write(*,*) "--------------------------------------------------------------------------"
     write(*,*) "S_INJ       : ", surfw
     write(*,*) "Q_INJ       : ", abs(winf)*surfw, qinf
     write(*,*) "Q_TOT       : ", qtot
     write(*,*) "W INJECTION : ", winf
  end if
  
  !-------------------------------------------------------
  ! FIN PLATEAU INJECTION
  !-------------------------------------------------------

  if (rank==0 .and. print_info) then
     write(*,*) "--------------------------------------------------------------------------"
     write(*,*) "EXTRATION CYL DANS CAISSON"
  end if

  !-------------------------------------------------------
  ! EXTRACTION CYL DANS SORBONNE 
  !-------------------------------------------------------
  
  !-------------------------------------------------------
  ! dimensions
  !-------------------------------------------------------
  yc=ymax-0.2d0
  z_min=zmin+1+1.4d0
  z_max=zmax
  rc2=0.1d0**2

  !-------------------------------------------------------
  ! amplitude vitesse/debit
  !-------------------------------------------------------
  surfu=1
  surfv=1
  surfw=1
  qinf=q_sorbonne
  !-------------------------------------------------------

  !-------------------------------------------------------
  ! surface computation (1er passage) penalisation (2nd)
  !-------------------------------------------------------
  
  do n=1,2

     uinf=0
     vinf=0
     winf=+qinf/surfw
     
     surfw=0
     do k=szw,ezw
        do j=syw,eyw
           do i=sxw,exw
              if (grid_z(k)>=z_min.and.grid_z(k)<=z_max) then
                 if ( (grid_x(i)-xc)**2+(grid_y(j)-yc)**2<rc2) then 
                    !-------------------------------------------------------
                    ! W component
                    !-------------------------------------------------------
                    if (k==gez) then
                       surfw=surfw+CoeffS(i,j,k)*dx(i)*dy(j)
                       win(i,j,k+1)=2*winf
                       penw(i,j,k+1,:)=0
                       penw(i,j,k+1,1)=pen_amp/2
                       penw(i,j,k+1,6)=pen_amp/2
                    else
                       ! remove penalisation 
                       slvu(i,j,k)=0
                       smvu(i,j,k)=0
                       slvu(i+1,j,k)=0
                       smvu(i+1,j,k)=0

                       slvv(i,j,k)=0
                       smvv(i,j,k)=0
                       slvv(i,j+1,k)=0
                       smvv(i,j+1,k)=0

                       slvw(i,j,k)=0
                       smvw(i,j,k)=0
                       slvw(i,j,k+1)=0
                       smvw(i,j,k+1)=0

                       
!!$                       slvw(i,j,k)=pen_amp
!!$                       smvw(i,j,k)=winf*pen_amp
!!$                       slvw(i,j,k-1)=pen_amp
!!$                       smvw(i,j,k-1)=winf*pen_amp
                    end if
                 end if
              end if
           end do
        end do
     end do
     
     call MPI_ALLREDUCE(surfw,tmp,1,MPI_DOUBLE_PRECISION,MPI_SUM,COMM3D,CODE); surfw=tmp
     
  end do
  
  qtot=qtot-qinf
  
  if (rank==0 .and. print_info) then
     write(*,*) "--------------------------------------------------------------------------"
     write(*,*) "W_EXTRACTION : ", abs(winf)
     write(*,*) "S_EXTRACTION : ", surfw
     write(*,*) "Q_EXTRACTION : ", abs(winf)*surfw, qinf
     write(*,*) "Q_TOT        : ", qtot
  end if

  !-------------------------------------------------------
  ! FIN EXTRACTION CYL DANS SORBONNE
  !-------------------------------------------------------
  
  
  
  if (rank==0 .and. print_info) then
     write(*,*) "--------------------------------------------------------------------------"
     write(*,*) "EXTRACTION SECONDAIRE"
  end if

  !-------------------------------------------------------
  ! EXTRACTION SECONDAIRE
  !-------------------------------------------------------

  !-------------------------------------------------------
  ! dimensions
  !-------------------------------------------------------
  x_min=xmin+(xmax-xmin)/4
  y_min=ymax-1d0
  z_min=0.1d0 ! rayon
  !-------------------------------------------------------

  !-------------------------------------------------------
  ! amplitude vitesse/debit
  !-------------------------------------------------------
  surfu=1
  surfv=1
  surfw=1
  qinf=qtot
  !-------------------------------------------------------
  
  !-------------------------------------------------------
  ! surface computation (1er passage) penalisation (2nd)
  !-------------------------------------------------------

  do n=1,2

     uinf=0
     vinf=0
     winf=qinf/surfw

     surfw=0
     do k=szw,ezw
        do j=syw,eyw
           do i=sxw,exw 
              if (k==gez) then
                 if ((grid_x(i)-x_min)**2+(grid_y(j)-y_min)**2<=z_min**2) then 
                    !-------------------------------------------------------
                    ! W component
                    !-------------------------------------------------------
                    surfw=surfw+CoeffS(i,j,k)*dx(i)*dy(j)
                    win(i,j,k+1)=2*winf
                    penw(i,j,k+1,:)=0
                    penw(i,j,k+1,1)=pen_amp/2
                    penw(i,j,k+1,6)=pen_amp/2
                    !-------------------------------------------------------
                 end if
              end if
           end do
        end do
     end do
     
     call MPI_ALLREDUCE(surfw,tmp,1,MPI_DOUBLE_PRECISION,MPI_SUM,COMM3D,CODE); surfw=tmp
     
  end do
  
  qtot=qtot-qinf
  
  if (rank==0 .and. print_info) then
     write(*,*) "--------------------------------------------------------------------------"
     write(*,*) "Q_EXT_SECONDAIRE   :", abs(winf)*surfw, qinf
     write(*,*) "Q_TOT              :", qtot
     write(*,*) "SURF_EXTRACTION    :", surfw,pi*z_min**2
     write(*,*) "VITESSE EXTRACTION :", winf
  end if

  !-------------------------------------------------------
  ! OPERATEUR
  !-------------------------------------------------------

  !-------------------------------------------------------
  ! dimensions
  !-------------------------------------------------------
  x_min = xmin+(xmax-xmin)/2 - 0.2d0
  x_max = x_min + 0.4d0
  y_max = ymax-0.8d0-0.1d0
  y_min = y_max - 0.2d0
  z_min = 0.1d0 ! rayon
  z_max = z_min+1.65d0
  !-------------------------------------------------------
  
  !-------------------------------------------------------
  ! objet
  !-------------------------------------------------------
  do k = sz,ez
     do j = sy,ey
        do i = sx,ex
           
           if (   grid_x(i)>=x_min .and. grid_x(i)<=x_max .and. &
                & grid_y(j)>=y_min .and. grid_y(j)<=y_max .and. &
                & grid_z(k)>=z_min .and. grid_z(k)<=z_max ) then
              
              slvu(i,j,k)=pen_amp
              smvu(i,j,k)=uinf*pen_amp
              slvu(i+1,j,k)=pen_amp
              smvu(i+1,j,k)=uinf*pen_amp
              
              slvv(i,j,k)=pen_amp
              smvv(i,j,k)=vinf*pen_amp
              slvv(i,j+1,k)=pen_amp
              smvv(i,j+1,k)=vinf*pen_amp
              
              slvw(i,j,k)=pen_amp
              smvw(i,j,k)=winf*pen_amp
              slvw(i,j,k+1)=pen_amp
              smvw(i,j,k+1)=winf*pen_amp
              
           end if
           
        end do
     end do
  end do
  
  
  !-------------------------------------------------------
  ! PURGE BORDS 
  !-------------------------------------------------------
  do k=szu,ezu
     do j=syu,eyu
        do i=sxu,exu
           !-------------------------------------------------------
           if (i<=gsxu .or. i>=gexu) then
              slvu(i,j,k)=0
              smvu(i,j,k)=0
           end if
           !-------------------------------------------------------
           if (j<=gsyu .or. j>=geyu) then
              slvu(i,j,k)=0
              smvu(i,j,k)=0
           end if
           !-------------------------------------------------------
           if (k<=gszu .or. k>=gezu) then
              slvu(i,j,k)=0
              smvu(i,j,k)=0
           end if
           !-------------------------------------------------------
        end do
     end do
  end do
  do k=szv,ezv
     do j=syv,eyv
        do i=sxv,exv
           !-------------------------------------------------------
           if (i<=gsxv .or. i>=gexv) then
              slvv(i,j,k)=0
              smvv(i,j,k)=0
           end if
           !-------------------------------------------------------
           if (j<=gsyv .or. j>=geyv) then
              slvv(i,j,k)=0
              smvv(i,j,k)=0
           end if
           !-------------------------------------------------------
           if (k<=gszv .or. k>=gezv) then
              slvv(i,j,k)=0
              smvv(i,j,k)=0
           end if
           !-------------------------------------------------------
        end do
     end do
  end do
  do k=szw,ezw
     do j=syw,eyw
        do i=sxw,exw
           !-------------------------------------------------------
           if (i<=gsxw .or. i>=gexw) then
              slvw(i,j,k)=0
              smvw(i,j,k)=0
           end if
           !-------------------------------------------------------
           if (j<=gsyw .or. j>=geyw) then
              slvw(i,j,k)=0
              smvw(i,j,k)=0
           end if
           !-------------------------------------------------------
           if (k<=gszw .or. k>=gezw) then
              slvw(i,j,k)=0
              smvw(i,j,k)=0
           end if
           !-------------------------------------------------------
        end do
     end do
  end do
  !-------------------------------------------------------

  !-------------------------------------------------------
  ! FIN PURGE BORDS 
  !-------------------------------------------------------

  print_info=.false.

!!$  smvu=0
!!$  slvu=0
!!$  smvv=0
!!$  slvv=0
!!$  smvw=0
!!$  slvw=0

  
