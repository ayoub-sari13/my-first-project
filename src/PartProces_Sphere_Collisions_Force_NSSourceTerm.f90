!===============================================================================
module Bib_VOFLag_Sphere_Collisions_Force_NSSourceTerm
  !===============================================================================
  contains
  !-----------------------------------------------------------------------------  
  !> @author jorge cesar brandle de motta, amine chadil
  ! 
  !> @brief  Cette subroutine permet de distribuer dans le volume d'une particule donnee,
  !! la force et le couple donnes.
  !>       
  !> @todo Ne pas parcourir l'ensemble du domaine.
  !
  !> @param[in]  c      : centre de la particule
  !> @param[in]  r      : rayon
  !> @param[in]  f      : force
  !> @param[in]  m      : couple
  !> @param[out] smvu   : tableau des forces discretes dans le maillage de vitesse u
  !> @param[out] smvv   : tableau des forces discretes dans le maillage de vitesse v
  !> @param[out] smvw   : tableau des forces discretes dans le maillage de vitesse w
  !-----------------------------------------------------------------------------
  subroutine Sphere_Collisions_Force_NSSourceTerm(c,r,f,m,smvu,smvv,smvw,ndim)
    !===============================================================================
    !modules
    !===============================================================================
    !-------------------------------------------------------------------------------
    !Modules de definition des structures et variables => src/mod/module_...f90
    !-------------------------------------------------------------------------------
    use mod_Parameters,           only : deeptracking,grid_x,grid_y,grid_z,&
                                       & grid_xu,grid_yv,grid_zw,sx,ex,sxu,exu,sxv, &
                                       & exv,sxw,exw,sy,ey,syv,eyv,syu,eyu,syw,eyw, &
                                       & sz,ez,szw,ezw,szu,ezu,szv,ezv
    !-------------------------------------------------------------------------------
    !Modules de subroutines de la librairie imft => src/bib_imft_...f90
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------
    !===============================================================================
    !declarations des variables
    !===============================================================================
    implicit none
    !-------------------------------------------------------------------------------
    !variables globales
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable, intent(inout) :: smvu,smvv,smvw
    real(8), dimension(3)                 , intent(in)    :: f,m,c
    real(8)                               , intent(in)    :: r
    integer                               , intent(in)    :: ndim
    !-------------------------------------------------------------------------------
    !variables locales 
    !-------------------------------------------------------------------------------
    real(8)                                   :: pi = acos(-1.d0),ddd
    integer                                   :: i,j,k
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'entree Sphere_Collisions_Force_NSSourceTerm'
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    !
    !-------------------------------------------------------------------------------
    if (ndim .eq. 3) then
      do k=szu,ezu
         do j=syu,eyu
            do i=sxu,exu
               ddd = (grid_xu(i)-c(1))*(grid_xu(i)-c(1))+&
                     (grid_y (j)-c(2))*(grid_y (j)-c(2))+&
                     (grid_z (k)-c(3))*(grid_z (k)-c(3))
               if (ddd.le.r*r) then
                  smvu(i,j,k)=f(1)/(4d0/3d0*pi*r*r*r)-((grid_z(k)-c(3))*m(2)-(grid_y(j)-c(2)))*m(3)/(8.d0/(15.d0*pi*r**5+1D-40))
               end if
            end do 
         end do 
      end do 
      do k=szv,ezv
         do j=syv,eyv
            do i=sxv,exv
               ddd = (grid_x (i)-c(1))*(grid_x (i)-c(1))+&
                     (grid_yv(j)-c(2))*(grid_yv(j)-c(2))+&
                     (grid_z (k)-c(3))*(grid_z (k)-c(3))
               if (ddd.le.r*r) then
                  smvv(i,j,k)=f(2)/(4d0/3d0*pi*r*r*r)-((grid_x(i)-c(1))*m(3)-(grid_z(k)-c(3)))*m(1)/(8.d0/(15.d0*pi*r**5+1D-40))
               end if
            end do 
         end do 
      end do
      do k=szw,ezw
         do j=syw,eyw
            do i=sxw,exw
               ddd = (grid_x (i)-c(1))*(grid_x (i)-c(1))+&
                     (grid_y (j)-c(2))*(grid_y (j)-c(2))+&
                     (grid_zw(k)-c(3))*(grid_zw(k)-c(3))
               if (ddd.le.r*r) then
                  smvw(i,j,k)=f(3)/(4d0/3d0*pi*r*r*r)-((grid_y(j)-c(2))*m(1)-(grid_x(i)-c(1)))*m(2)/(8.d0/(15.d0*pi*r**5+1D-40))
               end if
            end do 
         end do 
      end do   
    else
      do j=syu,eyu
         do i=sxu,exu
            ddd = (grid_xu(i)-c(1))*(grid_xu(i)-c(1))+&
                  (grid_y (j)-c(2))*(grid_y (j)-c(2))
            if (ddd.le.r*r) then
               smvu(i,j,1)=f(1)/(pi*r**2+1D-40)-(grid_y(j)-c(2))*m(3)/((pi*r**4+1D-40)/2.0d0)
            end if
         end do 
      end do 
      do j=syv,eyv
         do i=sxv,exv
            ddd = (grid_x (i)-c(1))*(grid_x (i)-c(1))+&
                  (grid_yv(j)-c(2))*(grid_yv(j)-c(2))
            if (ddd.le.r*r) then
               smvv(i,j,1)=f(2)/(pi*r**2+1D-40)-(grid_x(i)-c(1))*m(3)/((pi*r**4+1D-40)/2.0d0)
            end if
         end do 
      end do 
    end if

    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'sortie Sphere_Collisions_Force_NSSourceTerm'
    !-------------------------------------------------------------------------------
  end subroutine Sphere_Collisions_Force_NSSourceTerm

  !===============================================================================
end module Bib_VOFLag_Sphere_Collisions_Force_NSSourceTerm
!===============================================================================
  

