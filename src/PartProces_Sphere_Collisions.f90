!===============================================================================
module Bib_VOFLag_Sphere_Collisions
  !===============================================================================
  contains
  !-----------------------------------------------------------------------------  
  !> @author jorge cesar brandle de motta, ali ozel, Abdelhamid Hakkoum
  ! 
  !> @brief 
  !
  !> @todo epurer et commenter cette routine
  !
  !> @param[out] vl              : la structure contenant toutes les informations
  !! relatives aux particules.
  !> @param[in]  pas_de_temps              : 
  !-----------------------------------------------------------------------------
  subroutine Sphere_Collisions (vl,ndim,smvuin,smvvin,smvwin,smvu,smvv,smvw,pas_de_temps)
    !===============================================================================
    !modules
    !===============================================================================
    !-------------------------------------------------------------------------------
    !Modules de definition des structures et variables => src/mod/module_...f90
    !-------------------------------------------------------------------------------
    use Module_VOFLag_FluidizedBedCylinder, only : cylindre,rayon_cylindre,cylindre_c_x1,cylindre_c_x3
    use Bib_VOFLag_Sphere_Collisions_Force_NSSourceTerm
    use Module_VOFLag_ParticlesDataStructure
    use Module_VOFLag_SubDomain_Data,       only : s_domaines_int,m_period
    use mod_struct_thermophysics,           only : fluids
    use mod_Parameters,                     only : deeptracking,nproc,grid_x,grid_y,&
                                                   grid_z,dx,gsx,gex,gsy,gey,gsz,gez
    use mod_mpi
    !-------------------------------------------------------------------------------
    !===============================================================================
    !declarations des variables
    !===============================================================================
    implicit none
    !-------------------------------------------------------------------------------
    !variables globales
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable, intent(inout) :: smvu,smvv,smvw
    real(8), dimension(:,:,:), allocatable, intent(in)    :: smvuin,smvvin,smvwin
    real(8)                               , intent(in)    :: pas_de_temps
    integer                               , intent(in)    :: ndim
    !-------------------------------------------------------------------------------
    type(struct_vof_lag)     , intent(inout)     :: vl
    !-------------------------------------------------------------------------------
    !parameter locales 
    !-------------------------------------------------------------------------------
    !real(8), parameter  :: pi = 3.14159265358979323846264338327950288419716939937510
    !-------------------------------------------------------------------------------
    !variables locales 
    !-------------------------------------------------------------------------------
    real(8), dimension(vl%kpt,27,3)           :: pred_pos  
    real(8), dimension(vl%kpt,3)              :: pred_v
    real(8), dimension(ndim)                  :: disp,disp_n,disp_n1,disp2,disp2_n, &
                                               & disp2_n1,dist_mail

    real(8)                                   :: ddd,ddd_n,ddd_n1,ddd2,ddd2_n,      &
                                               & ddd2_n1,recouvrement,dist_mail_n,  &
                                                 dist_mail_n2
    integer                                   :: nd,np,np2,nbt,lvp
    !-------------------------------------------------------------------------------
    !Hakkoum 19_01_2022
    logical                            	      :: choc_lubri_intg, choc_sec_intg
    integer                                   :: position_mur
    double precision                          :: delta_x, prod_sca_vrel
    !Hakkoum 19_01_2022
    !-------------------------------------------------------------------------------
    
    !  logical, dimension(vl%kpt) :: interparwall
    !!$-------------------------------------------------------------------------------
    !!$variables locales a mettre quelque part
    !!$-------------------------------------------------------------------------------
    double precision :: pi = acos(-1.d0),tmp
    !!$-------------------------------------------------------------------------------
    !!$ variables temporaires (pour choc cylindre)
    !!$-------------------------------------------------------------------------------
        !! logical :: cyl = .false.
        !! double precision :: rayon_cylindre = 0.02d0
    !!$-------------------------------------------------------------------------------
    !!$ajout variables bertrand
    !!$-------------------------------------------------------------------------------
    real(8), dimension(ndim) ::  v_rel
    real(8)                  :: k_n, eta_n, lambda_e, epsilon,me,cosinus,sinus,     &
                              & rayon_t,v_n,epsilon_n, epsilon_n1, Int_ep, Int_ep2, &
                                lambda_e_n, lambda_e_n1, pent_v,b_v,epsilon_n1_2
    real(8), dimension(3)    :: f_total
    !-------------------------------------------------------------------------------

    call mpi_barrier(comm3d,code)

    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    !Initialisation
    !-------------------------------------------------------------------------------
    ! Lit fluidise
    !cylindre_c_x1=0.04d0; cylindre_c_x3=0.04d0; rayon_cylindre=0.04d0
    ! cylindre = .false.
    ! call lecval("lit_cylindre",cylindre)


    do np = 1,vl%kpt
       vl%objet(np)%chocparpar = .false.
    end do

    !-------------------------------------------------------------------------------
    ! Prediction de la vitesse et de la position
    !-------------------------------------------------------------------------------
    do nd = 1,ndim
       do np=1,vl%kpt
          if (vl%objet(np)%ar) then
             do nbt = 1,vl%objet(np)%nbtimes
		          pred_pos(np,nbt,nd) = vl%objet(np)%symper(nbt,nd) + pas_de_temps*vl%objet(np)%v(nd)
             end do
             pred_v(np,nd) =2*vl%objet(np)%v(nd) - vl%objet(np)%vn(nd)
             vl%objet(np)%fparpar(nd) = 0.0d0
          end if
       end do
    end do
    !-------------------------------------------------------------------------------
    ! Traitement de force de collision pour la particule np
    !-------------------------------------------------------------------------------
    !Hakkoum_10/28/2021
    !-------------------------------------------------------------------------------
    !vl%lubri_eps_pp = 0.03125d0
    !vl%lubri_eps_pp = 0.125d0 ! imposer ca à l'initialisation de ma structure
    
    choc_lubri_intg  = .false.
    choc_sec_intg    = .false.

    do np=1,vl%kpt
       vl%objet(np)%fparpar(1:ndim) = 0.0d0
       vl%objet(np)%chocparpar=.false.
       if (vl%objet(np)%in) then
          do np2=1,vl%kpt
             if (vl%objet(np2)%ar) then
                if (np.ne.np2) then
                  ddd_n =10d8
         		   ddd   =10d8
         		   ddd_n1=10d8
                  do nbt= 1,vl%objet(np2)%nbtimes
                     ddd2_n =0d0
         		      ddd2   =0d0
         		      ddd2_n1=0d0
                     do nd=1,ndim 
                        disp2_n(nd)=vl%objet(np2)%symper(nbt,nd) &
                              -vl%objet(np)%symper(1,nd)+1.d-40 
                        disp2(nd)=(pred_pos(np2,nbt,nd)+vl%objet(np2)%symper(nbt,nd))*&
                             		 0.5d0-(pred_pos(np,1,nd)+vl%objet(np)%symper(1,nd))*0.5d0+1.d-40 
                        disp2_n1(nd)=pred_pos(np2,nbt,nd)-pred_pos(np,1,nd)+1.d-40   

                        ddd2_n =ddd2_n+disp2_n(nd)*disp2_n(nd)     
                        ddd2   =ddd2+disp2(nd)*disp2(nd)
                        ddd2_n1=ddd2_n1+disp2_n1(nd)*disp2_n1(nd)
                     end do
                     if (ddd .gt. ddd2) then
                        ddd_n  =ddd2_n
                        disp_n =disp2_n
                        ddd    =ddd2
                        disp   =disp2
                        ddd_n1 =ddd2_n1
                        disp_n1=disp2_n1
                     end if
                  end do
                  disp_n(1:ndim) =disp_n(1:ndim)/sqrt(ddd_n+1D-40)
                  disp(1:ndim)   =disp(1:ndim)/sqrt(ddd+1D-40)
                  disp_n1(1:ndim)=disp_n1(1:ndim)/sqrt(ddd_n1+1D-40)
		   
                  epsilon_n =sqrt(ddd_n)/(vl%objet(np)%sca(1)) -2d0
                  epsilon   =sqrt(ddd)/(vl%objet(np)%sca(1))   -2d0
                  epsilon_n1=sqrt(ddd_n1)/(vl%objet(np)%sca(1))-2d0

                  v_rel(1:ndim) = ((vl%objet(np)%v(1:ndim)+pred_v(np,1:ndim))-&
                                   (vl%objet(np2)%v(1:ndim)+pred_v(np2,1:ndim)))*0.5d0
                  prod_sca_vrel = 0d0
                  do nd =1, ndim 
                     prod_sca_vrel = prod_sca_vrel + abs(v_rel(nd))*abs(disp(nd))
                  end do 
		  
                  if (vl%choc_lubri) then
                      !! ajout de force 
                      if (epsilon.lt.vl%lubri_eps_pp) then
                         vl%objet(np)%chocparpar =.true.
				
                         if (epsilon.gt.vl%lubri_eps_1) then
                            !! lambda_e variable
                            lambda_e = 1.d0/(2.d0*epsilon) - (9.0d0/20.0d0)*log(epsilon) - &
                                 (3.0d0/56.0d0)*epsilon*log(epsilon) + 1.346

                         else if ((epsilon .le. vl%lubri_eps_1) .and. (epsilon.gt.-vl%lubri_eps_2)) then
                            !! lambda_e = lambda_e1
                            lambda_e = vl%lubri_lambda_pp_eps1

                         else
                            !write(6,*) "warning : la particule",np,"est trop loin pour la lubrification"
                            lambda_e = vl%lubri_lambda_pp
                         end if


                         do nd = 1,ndim
                            vl%objet(np)%fparpar(nd) =  vl%objet(np)%fparpar(nd) - 6.0d0*pi*fluids(1)%mu*&
                                 vl%objet(np)%sca(1)*v_rel(nd)*( lambda_e - vl%lubri_lambda_pp)

                         end do
                      end if
		   
		   
		             else if (choc_lubri_intg) then	
	                   if (epsilon_n1.lt.vl%lubri_eps_pp) then
                         vl%objet(np)%chocparpar =.true.
	 	      
			               if ( (epsilon_n1.gt.vl%lubri_eps_1) .and. (epsilon_n.gt.vl%lubri_eps_pp) ) then 
				              do nd = 1, ndim
			  
               			  		! vl%objet(np)%fparmur(nd) = vl%objet(np)%fparmur(nd) - 6.0d0*pi*fluids(1)%mu*&
               			  		!  			 vl%objet(np)%sca(1)*( (pent_v/(epsilon_n1-epsilon_n))*(Intg_e_lambda(vl%lubri_eps_pm) - Intg_e_lambda(epsilon_n) -&
               			  		!  			  0.5d0*(vl%lubri_eps_pm*vl%lubri_eps_pm - epsilon_n*epsilon_n)*vl%lubri_lambda_pm) + &
               			  		!			   (b_v/(epsilon_n1 - epsilon_n))*(Intg_lambda(vl%lubri_eps_pm) - Intg_lambda(epsilon_n) -&
               			  		!  			    (vl%lubri_eps_pm - epsilon_n)*vl%lubri_lambda_pm))
               			  		!
               			  		!vl%objet(np)%fparmur(nd) = vl%objet(np)%fparmur(nd) - 6.0d0*pi*fluids(1)%mu*&
               			  		!				vl%objet(np)%sca(1)*(1d0/(epsilon_n1-epsilon_n))*(pent_v*vl%objet(np)%sca(1)*(Intg_e_lambda(vl%lubri_eps_pm) - &
               			  		!			Intg_e_lambda(epsilon_n) + Intg_lambda(vl%lubri_eps_pm) - Intg_lambda(epsilon_n) + vl%lubri_lambda_pm* &
               			  		!			(vl%lubri_eps_pm - epsilon_n)*(-1d0 - (vl%lubri_eps_pm - epsilon_n)*0.5d0)) + b_v*(Intg_lambda(vl%lubri_eps_pm) - &
               			  		!			 Intg_lambda(epsilon_n) - (vl%lubri_eps_pm - epsilon_n)*vl%lubri_lambda_pm))
			  
			  
            			  		  vl%objet(np)%fparpar(nd) = vl%objet(np)%fparpar(nd) - 6.0d0*pi*fluids(1)%mu*v_rel(nd)*vl%objet(np)%sca(1)*&
            			  					 ((1d0/(epsilon_n-epsilon_n1))*(Intg_lambda_pp(vl%lubri_eps_pp) - Intg_lambda_pp(epsilon_n1) -&
            			  					(vl%lubri_eps_pp - epsilon_n1)*vl%lubri_lambda_pp)) 
			  
			  		
			  	               end do
  			  
			               else if ( (epsilon_n1.gt.vl%lubri_eps_1) .and. (epsilon_n.lt.vl%lubri_eps_pp) .and. (epsilon_n.gt.vl%lubri_eps_1)) then 
  		         
			  	               do nd = 1, ndim
               			  		!vl%objet(np)%fparmur(nd) = vl%objet(np)%fparmur(nd) - 6.0d0*pi*fluids(1)%mu*&
               			  		! 			 vl%objet(np)%sca(1)*( (pent_v/(epsilon_n1-epsilon_n))*(Intg_e_lambda(epsilon_n1) - Intg_e_lambda(epsilon_n) -&
               			  		! 			  0.5d0*(epsilon_n1*epsilon_n1 - epsilon_n*epsilon_n)*vl%lubri_lambda_pm) + &
               			  		!			   (b_v/(epsilon_n1 - epsilon_n))*(Intg_lambda(epsilon_n1) - Intg_lambda(epsilon_n) -&
               			  		! 			    (epsilon_n1 - epsilon_n)*vl%lubri_lambda_pp))
               			  		!
               			  		!vl%objet(np)%fparmur(nd) = vl%objet(np)%fparmur(nd) - 6.0d0*pi*fluids(1)%mu*&
               			 	 	!				vl%objet(np)%sca(1)*(1d0/(epsilon_n1-epsilon_n))*(pent_v*vl%objet(np)%sca(1)*(Intg_e_lambda(epsilon_n1) - &
               			 	 	!			Intg_e_lambda(epsilon_n) + Intg_lambda(epsilon_n1) - Intg_lambda(epsilon_n) + vl%lubri_lambda_pm* &
               			 		!			(epsilon_n1 - epsilon_n)*(-1d0 - (epsilon_n1 - epsilon_n)*0.5d0)) + b_v*(Intg_lambda(epsilon_n1) - &
               			  		!			 Intg_lambda(epsilon_n) - (epsilon_n1 - epsilon_n)*vl%lubri_lambda_pp))
			  
			  			
               			  		vl%objet(np)%fparpar(nd) = vl%objet(np)%fparpar(nd) - 6.0d0*pi*fluids(1)%mu*v_rel(nd)*vl%objet(np)%sca(1)*&
               			  					((1d0/(epsilon_n-epsilon_n1))*(Intg_lambda_pp(epsilon_n) - Intg_lambda_pp(epsilon_n1) -&
               			 				 	(epsilon_n - epsilon_n1)*vl%lubri_lambda_pp)) 
			  	               end do

			  
			               else if ( (epsilon_n1.lt.vl%lubri_eps_1) .and. (epsilon_n.lt.vl%lubri_eps_pp) .and. (epsilon_n.gt.vl%lubri_eps_1) ) then 
			  	               do nd=1, ndim
               			  		!vl%objet(np)%fparmur(nd) = vl%objet(np)%fparmur(nd) - 6.0d0*pi*fluids(1)%mu*&
               			 		! 			 vl%objet(np)%sca(1)*( (pent_v/(epsilon_n1-epsilon_n))*((Intg_e_lambda(epsilon_n1) - Intg_e_lambda(vl%lubri_eps_1))+&
               			  		! 			  vl%lubri_eps_1*vl%lubri_eps_1*vl%lubri_lambda_pm_eps1 - 0.5d0*(epsilon_n1*epsilon_n1)*vl%lubri_lambda_pm) + &
               			  		!			    (b_v/(epsilon_n1-epsilon_n))*((Intg_lambda(epsilon_n1) - Intg_lambda(vl%lubri_eps_1)) + vl%lubri_eps_1*vl%lubri_lambda_pm_eps1 -&
               			  		! 			    epsilon_n1*vl%lubri_lambda_pm ))
               			    	    ! 	
               			  		! vl%objet(np)%fparmur(nd) = vl%objet(np)%fparmur(nd) - 6.0d0*pi*fluids(1)%mu*&
               			  		!				vl%objet(np)%sca(1)*(1d0/(epsilon_n1-epsilon_n))*(pent_v*vl%objet(np)%sca(1)*(Intg_e_lambda(epsilon_n1) - &
               			  		!			Intg_e_lambda(vl%lubri_eps_1) + Intg_lambda(epsilon_n1) - Intg_lambda(vl%lubri_eps_1) + vl%lubri_lambda_pm* &
               			  		!			(epsilon_n1 - vl%lubri_eps_1)*(-1d0 - (epsilon_n1 - vl%lubri_eps_1)*0.5d0)) + b_v*(Intg_lambda(epsilon_n1) - &
               			  		!			 Intg_lambda(vl%lubri_eps_1) - (epsilon_n1 - vl%lubri_eps_1)*vl%lubri_lambda_pm))
			  
			  
               			  		vl%objet(np)%fparpar(nd) = vl%objet(np)%fparpar(nd) - 6.0d0*pi*fluids(1)%mu*v_rel(nd)*&
               			  					vl%objet(np)%sca(1)*((1d0/(epsilon_n-epsilon_n1))*(Intg_lambda_pp(epsilon_n) - Intg_lambda_pp(vl%lubri_eps_1)+  &
               			  					vl%lubri_eps_1*vl%lubri_lambda_pp_eps1 - (epsilon_n -vl%lubri_eps_1)*vl%lubri_lambda_pp)) 
			  	
			  
			 	                end do  
			             else if ( (epsilon_n1.lt.vl%lubri_eps_1) .and. (epsilon_n.lt.vl%lubri_eps_1) .and. (epsilon_n1.gt.-vl%lubri_eps_2) .and. (epsilon_n.gt.-vl%lubri_eps_2)) then 
			  	            do nd=1, ndim
		  		              vl%objet(np)%fparpar(nd) =  vl%objet(np)%fparpar(nd) - 6.0d0*pi*fluids(1)%mu*&
                                				 vl%objet(np)%sca(1)*v_rel(nd)*( vl%lubri_lambda_pp_eps1 - vl%lubri_lambda_pp)
			  
				            end do  
			  
			            else if ( (epsilon_n.lt.vl%lubri_eps_1) .and. (epsilon_n1.lt.vl%lubri_eps_pp) .and. (epsilon_n1.gt.vl%lubri_eps_1) ) then 
			  	            do nd=1, ndim
            			  		!vl%objet(np)%fparmur(nd) = vl%objet(np)%fparmur(nd) - 6.0d0*pi*fluids(1)%mu*&
            			  		! 			 vl%objet(np)%sca(1)*( (pent_v/(epsilon_n-epsilon_n1))*((Intg_e_lambda(epsilon_n) - Intg_e_lambda(vl%lubri_eps_1))+&
            			 		! 			  vl%lubri_eps_1*vl%lubri_eps_1*vl%lubri_lambda_pm_eps1 - 0.5d0*(epsilon_n*epsilon_n - epsilon_n1*epsilon_n1)*vl%lubri_lambda_pm) + &
            			  		!			    (b_v/(epsilon_n-epsilon_n1))*((Intg_lambda(epsilon_n) - Intg_lambda(vl%lubri_eps_1)) + vl%lubri_eps_1*vl%lubri_lambda_pm_eps1) -&
            			  		! 			    b_v*(vl%lubri_lambda_pm ))
            			        	!
            			  		!vl%objet(np)%fparmur(nd) = vl%objet(np)%fparmur(nd) - 6.0d0*pi*fluids(1)%mu*&
            			  		!			vl%objet(np)%sca(1)*(1d0/(epsilon_n1-epsilon_n))*(pent_v*vl%objet(np)%sca(1)*(Intg_e_lambda(epsilon_n1) - &
            			  		!			Intg_e_lambda(vl%lubri_eps_1) + Intg_lambda(epsilon_n1) - Intg_lambda(vl%lubri_eps_1) + vl%lubri_lambda_pm* &
            			  		!			(epsilon_n1 - vl%lubri_eps_1)*(-1d0 - (epsilon_n1 - vl%lubri_eps_1)*0.5d0)) + b_v*(Intg_lambda(epsilon_n1) - &
            			  		!			Intg_lambda(vl%lubri_eps_1) - (epsilon_n1 - vl%lubri_eps_1)*vl%lubri_lambda_pm))
			  
            			  		vl%objet(np)%fparpar(nd) = vl%objet(np)%fparpar(nd) - 6.0d0*pi*fluids(1)%mu*v_rel(nd)*vl%objet(np)%sca(1)*&
            			  					((1d0/(epsilon_n1-epsilon_n))*(Intg_lambda_pp(epsilon_n1) - Intg_lambda_pp(vl%lubri_eps_1)+  &
            			  					vl%lubri_eps_1*vl%lubri_lambda_pp_eps1 - (epsilon_n1-vl%lubri_eps_1)*vl%lubri_lambda_pp)) 
			  
			
				            end do 
			  
			            else if ( (epsilon_n1.lt.vl%lubri_eps_1)  .and. (epsilon_n.lt.-vl%lubri_eps_2) .and.  (epsilon_n1.gt.-vl%lubri_eps_2)) then 
            			  	do nd=1, ndim
            			  		vl%objet(np)%fparpar(nd) =  vl%objet(np)%fparpar(nd) - 6.0d0*pi*fluids(1)%mu*&
            			  				vl%objet(np)%sca(1)*v_rel(nd)*(epsilon_n1/(epsilon_n1-epsilon_n))*( vl%lubri_lambda_pp_eps1 - vl%lubri_lambda_pp)
			 	
                        end do  
			            else
            			 	do nd=1, ndim
            			  		vl%objet(np)%fparpar(nd) = 0d0
            				end do
			            end if
			 
		             end if
               end if
		         v_rel(1:ndim) = prod_sca_vrel*disp(1:ndim)
               if (vl%choc_sec) then
                   me =1.0d0/(1.0d0/(vl%objet(np)%masse)+1.0d0/(vl%objet(np2)%masse))
                   k_n = me*(pi**2+(log(vl%sec_ed))**2)/(((vl%sec_nc)*pas_de_temps)**2)
                   eta_n = -2.0d0*me*log(vl%sec_ed)/((vl%sec_nc)*pas_de_temps)
         
                   if (epsilon<0.0) then
                      vl%objet(np)%chocparpar=.true.
                      do nd=1,ndim
                         vl%objet(np)%fparpar(nd)=  vl%objet(np)%fparpar(nd) - k_n*(-epsilon)*vl%objet(np)%sca(1)*(disp(nd))&
                               - eta_n*v_rel(nd)
                      end do
                   end if
		          else if (choc_sec_intg) then  
      		      me =1.0d0/(1.0d0/(vl%objet(np)%masse)+1.0d0/(vl%objet(np2)%masse)) ! motta : faire ce calcul une seule fois
      		      k_n = me*(pi**2+(log(vl%sec_ed))**2)/(((vl%sec_nc)*pas_de_temps)**2)
      		      eta_n = -2.0d0*me*log(vl%sec_ed)/((vl%sec_nc)*pas_de_temps)
		      
		             if ( (epsilon_n1 <0d0) .and. (epsilon_n>0d0 )) then 
		      
		      	      vl%objet(np)%chocparpar=.true.
      		      	do nd=1,ndim
      		      		vl%objet(np)%fparpar(nd) =  vl%objet(np)%fparpar(nd) + (1d0/(epsilon_n-epsilon_n1))*k_n*0.5d0*(-epsilon_n1*epsilon_n1*vl%objet(np)%sca(1))*(disp(nd)) -&
      		      					eta_n*v_rel(nd)
      		      	end do
		      	
		             else if ( (epsilon_n1 <0d0) .and. (epsilon_n<0d0 )) then
		      	      vl%objet(np)%chocparpar=.true.
		      	
      		      	if ( epsilon_n1 < epsilon_n) then 
      		      		do nd=1,ndim
      		      			vl%objet(np)%fparpar(nd) =  vl%objet(np)%fparpar(nd) + (1d0/(epsilon_n-epsilon_n1))*k_n*0.5d0*((epsilon_n*epsilon_n-epsilon_n1*epsilon_n1)*&
      		      			vl%objet(np)%sca(1))*(disp(nd)) - eta_n*v_rel(nd)
      		      		end do
      		      	else if ( epsilon_n < epsilon_n1) then
      		      		do nd=1,ndim
      		      			vl%objet(np)%fparpar(nd) =  vl%objet(np)%fparpar(nd) + (1d0/(epsilon_n1-epsilon_n))*k_n*0.5d0*((epsilon_n1*epsilon_n1-epsilon_n*epsilon_n)*&
      		      			vl%objet(np)%sca(1))*(disp(nd)) - eta_n*v_rel(nd)
      		      		end do 
      		      
      		      	end if 
		      
		             else if ( (epsilon_n1 >0d0) .and. (epsilon_n<0d0 )) then 
      		      	vl%objet(np)%chocparpar=.true.
      		      	do nd=1,ndim
      		      		vl%objet(np)%fparpar(nd) =  vl%objet(np)%fparpar(nd) + (1d0/(epsilon_n1-epsilon_n))*k_n*0.5d0*(-epsilon_n*epsilon_n*vl%objet(np)%sca(1))*(disp(nd)) -&
      		      					eta_n*v_rel(nd)
      		      	end do
		             end if
		
               end if
            end if
         end if
      end do
   end if

   end do
    !fin du traitement de force de collision pour la particule np
    !-------------------------------------------------------------------------------

    if (nproc.gt.1) then 
       ! je m'en sors sans le if de la dimension
       do np=1,vl%kpt
          call mpi_bcast(vl%objet(np)%chocparpar,1,mpi_logical,vl%objet(np)%locpart,comm3d,code)
           
          do nd = 1,ndim
             do nbt =1,vl%objet(np)%nbtimes
               
                call mpi_bcast(pred_pos(np,nbt,nd) ,1,mpi_double_precision,vl%objet(np)%locpart,comm3d,code)    ! mottaaf : ne passer que les nbt premiers 
               
             end do
             call mpi_bcast(pred_v(np,nd)   ,1,mpi_double_precision,vl%objet(np)%locpart,comm3d,code)
            
             call mpi_bcast(vl%objet(np)%fparpar(nd),1,mpi_double_precision,vl%objet(np)%locpart,comm3d,code)
          end do
       enddo
    endif
    ! je m'en sors sans le if de la dimension
    vl%choc_lubri  =.false.
    vl%choc_sec    =.false.
    choc_lubri_intg=.true.
    choc_sec_intg  =.true.
    !-------------------------------------------------------------------------------
    !test sur le choc particule / paroi
    !-------------------------------------------------------------------------------
    if (.not.cylindre) then
       vl%objet(1:vl%kpt)%chocparmur = .false.
       do np=1,vl%kpt
          do nd=1,ndim
             vl%objet(np)%fparmur(nd) = 0.0d0
          end do
       end do
       do np=1,vl%kpt
          ! a initaliser ailleurs :
          if (vl%objet(np)%in) then
             do nd=1,ndim
                if (.not.m_period(nd)) then
                   !if ( rank == 0 .and. nd==1) then 
                   !  print*, vl%objet(np)%symper(1,nd), 'particle position'
                   !  !print*, vl%objet(np)%sca(1), ' particle rayon'
                   !  print*, (vl%objet(np)%symper(1,1)+pred_pos(np,1,1))*0.5d0, ' particle position X^1+2'
                   !end if 
                  !Hakkoum_10/28/2021
                  !-------------------------------------------------------------------------------
                  if (nd==1) then 
                     ! epsilon_n1 = min ((grid_x(gex)-WallPos(1)-vl%objet(np)%symper(1,nd))/(vl%objet(np)%sca(1)+1D-40) - 1.0d0,&
                     !    (vl%objet(np)%symper(1,nd)-WallPos(1)-grid_x(gsx))/(vl%objet(np)%sca(1)+1D-40) - 1.0d0 ) 

                     ! epsilon_n1_2 = min ((grid_x(gex)-WallPos(1)-(vl%objet(np)%symper(1,nd) + pred_pos(np,1,nd))*0.5d0)/(vl%objet(np)%sca(1)+1D-40) - 1.0d0,&
                     !    ((vl%objet(np)%symper(1,nd) + pred_pos(np,1,nd))*0.5d0-WallPos(1)-grid_x(gsx))/(vl%objet(np)%sca(1)+1D-40) - 1.0d0 )

                     ! epsilon_n  = min ((grid_x(gex)-WallPos(1)-pred_pos(np,1,nd))/(vl%objet(np)%sca(1)+1D-40) - 1.0d0,&
                     !    ((pred_pos(np,1,nd))-WallPos(1)-grid_x(gsx))/(vl%objet(np)%sca(1)+1D-40) - 1.0d0 )

                     epsilon_n1 = (vl%objet(np)%symper(1,nd)-WallPos(1)-grid_x(gsx))/(vl%objet(np)%sca(1)+1D-40) - 1.0d0 

                     epsilon_n1_2 = ((vl%objet(np)%symper(1,nd) + pred_pos(np,1,nd))*0.5d0-WallPos(1)-grid_x(gsx))/(vl%objet(np)%sca(1)+1D-40) - 1.0d0 

                     epsilon_n  = ((pred_pos(np,1,nd))-WallPos(1)-grid_x(gsx))/(vl%objet(np)%sca(1)+1D-40) - 1.0d0 
                  end if 

                  if (nd==2) then 
                     epsilon_n1 = min ((grid_y(gey)-WallPos(2)-vl%objet(np)%symper(1,nd))/(vl%objet(np)%sca(1)+1D-40) - 1.0d0,&
                        (vl%objet(np)%symper(1,nd)-WallPos(2)-grid_y(gsy))/(vl%objet(np)%sca(1)+1D-40) - 1.0d0 )  

                     epsilon_n1_2 = min ((grid_y(gey)-WallPos(2)-(vl%objet(np)%symper(1,nd) + pred_pos(np,1,nd))*0.5d0)/(vl%objet(np)%sca(1)+1D-40) - 1.0d0,&
                        ((vl%objet(np)%symper(1,nd) + pred_pos(np,1,nd))*0.5d0 -WallPos(2)-grid_y(gsy))/(vl%objet(np)%sca(1)+1D-40) - 1.0d0 )

                     epsilon_n  = min ((grid_y(gey)-WallPos(2)-pred_pos(np,1,nd))/(vl%objet(np)%sca(1)+1D-40) - 1.0d0,&
                        ((pred_pos(np,1,nd))-WallPos(2)-grid_y(gsy))/(vl%objet(np)%sca(1)+1D-40) - 1.0d0 )
                  end if 

                  if (nd==3) then 
                     epsilon_n1 = min ((grid_z(gez)-WallPos(3)-vl%objet(np)%symper(1,nd))/(vl%objet(np)%sca(1)+1D-40) - 1.0d0,&
                        (vl%objet(np)%symper(1,nd)-WallPos(3)-grid_z(gsz))/(vl%objet(np)%sca(1)+1D-40) - 1.0d0 )  

                     epsilon_n1_2 = min ((grid_z(gez)-WallPos(3)-(vl%objet(np)%symper(1,nd) + pred_pos(np,1,nd))*0.5d0)/(vl%objet(np)%sca(1)+1D-40) - 1.0d0,&
                        ((vl%objet(np)%symper(1,nd) + pred_pos(np,1,nd))*0.5d0 -WallPos(3)-grid_z(gsz))/(vl%objet(np)%sca(1)+1D-40) - 1.0d0 )

                     epsilon_n  = min ((grid_z(gez)-WallPos(3)-pred_pos(np,1,nd))/(vl%objet(np)%sca(1)+1D-40) - 1.0d0,&
                        ((pred_pos(np,1,nd))-WallPos(3)-grid_z(gsz))/(vl%objet(np)%sca(1)+1D-40) - 1.0d0 )
                  end if 

                  if ( nd==1) then 
                     print*, epsilon_n1, ' epsilon n'
                     print*, epsilon_n1_2, ' epsilon n+1/2'
                     print*, epsilon_n, ' epsilon n+1'
                     print*, vl%lubri_eps_pm, 'epsilon al'
                     print*, vl%objet(1)%vn(1) , ' vitesse n-1'
                     print*, vl%objet(1)%v(1) , ' vitesse n'
                     print*, (vl%objet(1)%v(1)+pred_v(1,1))*0.5d0 , ' vitesse n+1/2'
                   end if 

                  pent_v = (vl%objet(np)%v(nd) - vl%objet(np)%vn(nd))/(pas_de_temps*vl%objet(np)%v(nd))
                  b_v = (vl%objet(np)%v(nd)+pred_v(np,nd))*0.5d0 - pent_v*(epsilon_n1_2*vl%objet(np)%sca(1))

                   !! ajout de force ?
                  if (vl%choc_lubri) then
                     if ( rank == 0 .and. nd==1) then 
                        print*, epsilon_n1_2*vl%objet(np)%sca(1), ' distance'
                        print*, epsilon_n1_2, ' epsilon'
                        print*, vl%lubri_eps_pm, ' epsilon al '
                        print*, minval(dx),' dx '
                     end if 
         		      if (epsilon_n1_2.lt.vl%lubri_eps_pm) then
                        vl%objet(np)%chocparmur=.true.
		      	 
         		      	if (epsilon_n1_2.gt.vl%lubri_eps_1) then
                                     !! lambda_e variable
                           lambda_e = 1.d0/(epsilon_n1_2) - 0.2d0*log(epsilon_n1_2) - &
                                 (1.0d0/21.0d0)*epsilon_n1_2*log(epsilon_n1_2) + 0.9713
                      
                        else if ((epsilon_n1_2 .le. vl%lubri_eps_1) .and. (epsilon_n1_2.gt.-vl%lubri_eps_2)) then
                           !! lambda_e = lambda_e1
                           lambda_e = vl%lubri_lambda_pm_eps1
                      
                        else
                           !write(6,*) "warning : la particule",np,"est trop loin pour la lubrification"
                           lambda_e = vl%lubri_lambda_pm
                        end if
                        vl%objet(np)%fparmur(nd) =  vl%objet(np)%fparmur(nd) - 6.0d0*pi*fluids(1)%mu*&
                              vl%objet(np)%sca(1)*(vl%objet(np)%v(nd) + pred_v(np,nd))*0.5d0*( lambda_e - vl%lubri_lambda_pm) 
		               end if 
                  else if (choc_lubri_intg) then	
            		   if (epsilon_n.lt.vl%lubri_eps_pm) then
                        vl%objet(np)%chocparmur=.true. 
                        print*, ' entree dans la modele de lubrification '
   			            if ( (epsilon_n.gt.vl%lubri_eps_1) .and. (epsilon_n1.gt.vl%lubri_eps_pm) ) then 

   			            print*, ' entree dans la case epsilon n+1 < e_al epsilon n+1 > e1 et epsilon n > e_al 111 '
                  		! vl%objet(np)%fparmur(nd) = vl%objet(np)%fparmur(nd) - 6.0d0*pi*fluids(1)%mu*&
                  		!  			 vl%objet(np)%sca(1)*( (pent_v/(epsilon_n1-epsilon_n))*(Intg_e_lambda(vl%lubri_eps_pm) - Intg_e_lambda(epsilon_n) -&
                  		!  			  0.5d0*(vl%lubri_eps_pm*vl%lubri_eps_pm - epsilon_n*epsilon_n)*vl%lubri_lambda_pm) + &
                  		!			   (b_v/(epsilon_n1 - epsilon_n))*(Intg_lambda(vl%lubri_eps_pm) - Intg_lambda(epsilon_n) -&
                  		!  			    (vl%lubri_eps_pm - epsilon_n)*vl%lubri_lambda_pm))
                  		!
                  		!vl%objet(np)%fparmur(nd) = vl%objet(np)%fparmur(nd) - 6.0d0*pi*fluids(1)%mu*&
                  		!				vl%objet(np)%sca(1)*(1d0/(epsilon_n1-epsilon_n))*(pent_v*vl%objet(np)%sca(1)*(Intg_e_lambda(vl%lubri_eps_pm) - &
                  		!			Intg_e_lambda(epsilon_n) + Intg_lambda(vl%lubri_eps_pm) - Intg_lambda(epsilon_n) + vl%lubri_lambda_pm* &
                  		!			(vl%lubri_eps_pm - epsilon_n)*(-1d0 - (vl%lubri_eps_pm - epsilon_n)*0.5d0)) + b_v*(Intg_lambda(vl%lubri_eps_pm) - &
                  		!			 Intg_lambda(epsilon_n) - (vl%lubri_eps_pm - epsilon_n)*vl%lubri_lambda_pm))
               			vl%objet(np)%fparmur(nd) = vl%objet(np)%fparmur(nd) - 6.0d0*pi*fluids(1)%mu*(vl%objet(np)%v(nd)+pred_v(np,nd))*0.5d0*&
               							vl%objet(np)%sca(1)* ((1d0/(epsilon_n1-epsilon_n))*(Intg_lambda(vl%lubri_eps_pm) - Intg_lambda(epsilon_n) -&
               			  				(vl%lubri_eps_pm - epsilon_n)*vl%lubri_lambda_pm)) 
   			  
   			            else if ( (epsilon_n.gt.vl%lubri_eps_1) .and. (epsilon_n1.lt.vl%lubri_eps_pm) .and. (epsilon_n1.gt.vl%lubri_eps_1)) then 
  		         
                  	   print*, ' entree dans la case epsilon n+1 < e_al epsilon n+1 > e1 et epsilon n < e_al epsilon n > e 1 222'
                        !vl%objet(np)%fparmur(nd) = vl%objet(np)%fparmur(nd) - 6.0d0*pi*fluids(1)%mu*&
                        ! 			 vl%objet(np)%sca(1)*( (pent_v/(epsilon_n1-epsilon_n))*(Intg_e_lambda(epsilon_n1) - Intg_e_lambda(epsilon_n) -&
                        ! 			  0.5d0*(epsilon_n1*epsilon_n1 - epsilon_n*epsilon_n)*vl%lubri_lambda_pm) + &
                        !			   (b_v/(epsilon_n1 - epsilon_n))*(Intg_lambda(epsilon_n1) - Intg_lambda(epsilon_n) -&
                        ! 			    (epsilon_n1 - epsilon_n)*vl%lubri_lambda_pm))
                        !
                        !vl%objet(np)%fparmur(nd) = vl%objet(np)%fparmur(nd) - 6.0d0*pi*fluids(1)%mu*&
                        !				vl%objet(np)%sca(1)*(1d0/(epsilon_n1-epsilon_n))*(pent_v*vl%objet(np)%sca(1)*(Intg_e_lambda(epsilon_n1) - &
                        !			Intg_e_lambda(epsilon_n) + Intg_lambda(epsilon_n1) - Intg_lambda(epsilon_n) + vl%lubri_lambda_pm* &
                        !			(epsilon_n1 - epsilon_n)*(-1d0 - (epsilon_n1 - epsilon_n)*0.5d0)) + b_v*(Intg_lambda(epsilon_n1) - &
                        !			 Intg_lambda(epsilon_n) - (epsilon_n1 - epsilon_n)*vl%lubri_lambda_pm))
                        vl%objet(np)%fparmur(nd) = vl%objet(np)%fparmur(nd) - 6.0d0*pi*fluids(1)%mu*(vl%objet(np)%v(nd)+pred_v(np,nd))*0.5d0*&
                  			  			vl%objet(np)%sca(1)*((1d0/(epsilon_n1-epsilon_n))*(Intg_lambda(epsilon_n1) - Intg_lambda(epsilon_n) -&
                  			 		 	(epsilon_n1 - epsilon_n)*vl%lubri_lambda_pm)) 
			  
			  
                        else if ( (epsilon_n.lt.vl%lubri_eps_1) .and. (epsilon_n1.lt.vl%lubri_eps_pm) .and. (epsilon_n1.gt.vl%lubri_eps_1) ) then 
			                  print*, ' entree dans la case epsilon n+1 < e_al epsilon n+1 < e1 et epsilon n < e_al epsilon n > e 1 333'
                           !vl%objet(np)%fparmur(nd) = vl%objet(np)%fparmur(nd) - 6.0d0*pi*fluids(1)%mu*&
                           ! 			 vl%objet(np)%sca(1)*( (pent_v/(epsilon_n1-epsilon_n))*((Intg_e_lambda(epsilon_n1) - Intg_e_lambda(vl%lubri_eps_1))+&
                           ! 			  vl%lubri_eps_1*vl%lubri_eps_1*vl%lubri_lambda_pm_eps1 - 0.5d0*(epsilon_n1*epsilon_n1)*vl%lubri_lambda_pm) + &
                           !			    (b_v/(epsilon_n1-epsilon_n))*((Intg_lambda(epsilon_n1) - Intg_lambda(vl%lubri_eps_1)) + vl%lubri_eps_1*vl%lubri_lambda_pm_eps1 -&
                           ! 			    epsilon_n1*vl%lubri_lambda_pm ))
                           ! 	
                           ! vl%objet(np)%fparmur(nd) = vl%objet(np)%fparmur(nd) - 6.0d0*pi*fluids(1)%mu*&
                           !				vl%objet(np)%sca(1)*(1d0/(epsilon_n1-epsilon_n))*(pent_v*vl%objet(np)%sca(1)*(Intg_e_lambda(epsilon_n1) - &
                           !			Intg_e_lambda(vl%lubri_eps_1) + Intg_lambda(epsilon_n1) - Intg_lambda(vl%lubri_eps_1) + vl%lubri_lambda_pm* &
                           !			(epsilon_n1 - vl%lubri_eps_1)*(-1d0 - (epsilon_n1 - vl%lubri_eps_1)*0.5d0)) + b_v*(Intg_lambda(epsilon_n1) - &
                           !			 Intg_lambda(vl%lubri_eps_1) - (epsilon_n1 - vl%lubri_eps_1)*vl%lubri_lambda_pm))   			  
                           vl%objet(np)%fparmur(nd) = vl%objet(np)%fparmur(nd) - 6.0d0*pi*fluids(1)%mu*(vl%objet(np)%v(nd)+pred_v(np,nd))*0.5d0*&
                  			  			vl%objet(np)%sca(1)*((1d0/(epsilon_n1-epsilon_n))*(Intg_lambda(epsilon_n1) - Intg_lambda(vl%lubri_eps_1)+  &
                  			  			vl%lubri_eps_1*vl%lubri_lambda_pm_eps1 - (epsilon_n1 - vl%lubri_eps_1)*vl%lubri_lambda_pm)) 
						
						
                        else if ( (epsilon_n.lt.vl%lubri_eps_1) .and. (epsilon_n1.lt.vl%lubri_eps_1) .and. (epsilon_n.gt.-vl%lubri_eps_2) .and. (epsilon_n1.gt.-vl%lubri_eps_2)) then 
                  			print*, ' entree dans la case epsilon n+1 > 0 epsilon n+1 < e1 et epsilon n < e 1 444'
                           vl%objet(np)%fparmur(nd) =  vl%objet(np)%fparmur(nd) - 6.0d0*pi*fluids(1)%mu*&
                  			  	   	vl%objet(np)%sca(1)*(vl%objet(np)%v(nd)+pred_v(np,nd))*0.5d0*( vl%lubri_lambda_pm_eps1 - vl%lubri_lambda_pm)
			  
			               else if ( (epsilon_n1.lt.vl%lubri_eps_1) .and. (epsilon_n.lt.vl%lubri_eps_pm) .and. (epsilon_n.gt.vl%lubri_eps_1) ) then 
			                  print*, ' entree dans la case epsilon n < e 1  epsilon n+1 > e al  et epsilon n +1 > e 1 555'
                           !vl%objet(np)%fparmur(nd) = vl%objet(np)%fparmur(nd) - 6.0d0*pi*fluids(1)%mu*&
                           ! 			 vl%objet(np)%sca(1)*( (pent_v/(epsilon_n-epsilon_n1))*((Intg_e_lambda(epsilon_n) - Intg_e_lambda(vl%lubri_eps_1))+&
                           ! 			  vl%lubri_eps_1*vl%lubri_eps_1*vl%lubri_lambda_pm_eps1 - 0.5d0*(epsilon_n*epsilon_n - epsilon_n1*epsilon_n1)*vl%lubri_lambda_pm) + &
                           !			    (b_v/(epsilon_n-epsilon_n1))*((Intg_lambda(epsilon_n) - Intg_lambda(vl%lubri_eps_1)) + vl%lubri_eps_1*vl%lubri_lambda_pm_eps1) -&
                           ! 			    b_v*(vl%lubri_lambda_pm ))
                           !
                           !vl%objet(np)%fparmur(nd) = vl%objet(np)%fparmur(nd) - 6.0d0*pi*fluids(1)%mu*&
                           !			vl%objet(np)%sca(1)*(1d0/(epsilon_n1-epsilon_n))*(pent_v*vl%objet(np)%sca(1)*(Intg_e_lambda(epsilon_n1) - &
                           !			Intg_e_lambda(vl%lubri_eps_1) + Intg_lambda(epsilon_n1) - Intg_lambda(vl%lubri_eps_1) + vl%lubri_lambda_pm* &
                           !			(epsilon_n1 - vl%lubri_eps_1)*(-1d0 - (epsilon_n1 - vl%lubri_eps_1)*0.5d0)) + b_v*(Intg_lambda(epsilon_n1) - &
                           !			Intg_lambda(vl%lubri_eps_1) - (epsilon_n1 - vl%lubri_eps_1)*vl%lubri_lambda_pm))
                           vl%objet(np)%fparmur(nd) = vl%objet(np)%fparmur(nd) - 6.0d0*pi*fluids(1)%mu*(vl%objet(np)%v(nd)+pred_v(np,nd))*0.5d0*&
                  			  			vl%objet(np)%sca(1)*((1d0/(epsilon_n-epsilon_n1))*(Intg_lambda(epsilon_n) - Intg_lambda(vl%lubri_eps_1)+  &
                  			  			vl%lubri_eps_1*vl%lubri_lambda_pm_eps1 - (epsilon_n-vl%lubri_eps_1)*vl%lubri_lambda_pm)) 
						
			  
                        else if ( (epsilon_n.lt.vl%lubri_eps_1)  .and. (epsilon_n1.lt.-vl%lubri_eps_2) .and.  (epsilon_n.gt.-vl%lubri_eps_2)) then 
			                  print*, ' entree dans la case epsilon n +1 < e 1  epsilon n < e al  et epsilon n +1 > 0 666'
                           vl%objet(np)%fparmur(nd) =  vl%objet(np)%fparmur(nd) - 6.0d0*pi*fluids(1)%mu*&
                  			  		vl%objet(np)%sca(1)*(vl%objet(np)%v(nd)+pred_v(np,nd))*0.5d0*(vl%lubri_eps_1/(epsilon_n-epsilon_n1))*( vl%lubri_lambda_pm_eps1 - vl%lubri_lambda_pm)
			  
			  
                        else
               			  	vl%objet(np)%fparmur(nd) = 0d0
               			end if

		               end if
                  end if
                  if (vl%choc_sec) then 
                     k_n = vl%objet(np)%masse*(pi**2+(log(vl%sec_ed))**2)/(((vl%sec_nc)*pas_de_temps)**2+1D-40)			
                     eta_n = -2.0d0*vl%objet(np)%masse*log(vl%sec_ed)/((vl%sec_nc)*pas_de_temps+1D-40)
            		   if ( epsilon_n1_2 < 0d0 ) then 
            		     	vl%objet(np)%chocparmur = .true. 
            		     	vl%objet(np)%fparmur(nd) = vl%objet(np)%fparmur(nd) - k_n*(epsilon_n1_2*vl%objet(np)%sca(1)) - eta_n*(vl%objet(np)%v(nd)+pred_v(np,nd))*0.5d0
            		   end if 
                  else if (choc_sec_intg) then

         		     	k_n = vl%objet(np)%masse*(pi**2+(log(vl%sec_ed))**2)/(((vl%sec_nc)*pas_de_temps)**2+1D-40)			
         		     	eta_n = -2.0d0*vl%objet(np)%masse*log(vl%sec_ed)/((vl%sec_nc)*pas_de_temps+1D-40)
         		      if ( (epsilon_n <0d0) .and. (epsilon_n1>0d0 )) then 
                        print*, ' entree dans la modele de collision '
         		         vl%objet(np)%chocparmur =.true.
		      		
         		         ! Vitesse constante 
         		         vl%objet(np)%fparmur(nd) =  vl%objet(np)%fparmur(nd) - (1d0/(epsilon_n1-epsilon_n))*k_n*0.5d0*(-epsilon_n*epsilon_n*vl%objet(np)%sca(1)) -& 
         		           				eta_n*(vl%objet(np)%v(nd)+pred_v(np,nd))*0.5d0    
         		         ! Vitesse lineaire V = a X + b
         			   
         			      !pent_v = (vl%objet(np)%v(nd) - vl%objet(np)%vn(nd))/(pas_de_temps*vl%objet(np)%v(nd))
         		         !b_v = (vl%objet(np)%v(nd)+pred_v(np,nd))*0.5d0 - pent_v*(epsilon_n1_2*vl%objet(np)%sca(1))
         		         !
         		         !vl%objet(np)%fparmur(nd) =  vl%objet(np)%fparmur(nd) - (1d0/(epsilon_n1-epsilon_n))*(k_n*0.5d0*(-epsilon_n*epsilon_n*vl%objet(np)%sca(1)) &
         		         !				- eta_n*(pent_v*vl%objet(np)%sca(1)*(0.5d0*(-epsilon_n*epsilon_n)  )  - b_v*epsilon_n))
         		         !
		      	      else if ( (epsilon_n <0d0) .and. (epsilon_n1<0d0 )) then
                        vl%objet(np)%chocparmur =.true.
           
                        if ( epsilon_n < epsilon_n1) then 
      			   
                           !Vitesse constante
                           vl%objet(np)%fparmur(nd) =  vl%objet(np)%fparmur(nd) - (1d0/(epsilon_n1-epsilon_n))*k_n*0.5d0*(epsilon_n1*epsilon_n1-epsilon_n*epsilon_n)*vl%objet(np)%sca(1) - &
         		           					eta_n*(vl%objet(np)%v(nd)+pred_v(np,nd))*0.5d0
         		          
                           ! Vitesse lineaire V = a X + b 
                           !pent_v = (vl%objet(np)%v(nd) - vl%objet(np)%vn(nd))/(pas_de_temps*vl%objet(np)%v(nd))
                           !b_v = (vl%objet(np)%v(nd)+pred_v(np,nd))*0.5d0 - pent_v*(epsilon_n1_2*vl%objet(np)%sca(1))
                           !
                           !vl%objet(np)%fparmur(nd) =  vl%objet(np)%fparmur(nd) - (1d0/(epsilon_n1-epsilon_n))*(k_n*0.5d0*(epsilon_n1*epsilon_n1-epsilon_n*epsilon_n)*vl%objet(np)%sca(1) &
                           !			  - eta_n*(pent_v*vl%objet(np)%sca(1)*(0.5d0*(epsilon_n1*epsilon_n1-epsilon_n*epsilon_n)  )  + b_v*(epsilon_n1- epsilon_n)))
                           !
                        else if ( epsilon_n > epsilon_n1) then 
                           !Vitesse constante
                           vl%objet(np)%fparmur(nd) =  vl%objet(np)%fparmur(nd) - (1d0/(epsilon_n-epsilon_n1))*k_n*0.5d0*(epsilon_n*epsilon_n-epsilon_n1*epsilon_n1)*vl%objet(np)%sca(1) - &
         		           					eta_n*(vl%objet(np)%v(nd)+pred_v(np,nd))*0.5d0
         							
                           ! Vitesse lineaire V = a X + b 
                           !pent_v = (vl%objet(np)%v(nd) - vl%objet(np)%vn(nd))/(pas_de_temps*vl%objet(np)%v(nd))
                           !b_v = (vl%objet(np)%v(nd)+pred_v(np,nd))*0.5d0 - pent_v*(epsilon_n1_2*vl%objet(np)%sca(1))
                           !
                           !vl%objet(np)%fparmur(nd) =  vl%objet(np)%fparmur(nd) - (1d0/(epsilon_n-epsilon_n1))*(k_n*0.5d0*(epsilon_n*epsilon_n-epsilon_n1*epsilon_n1)*vl%objet(np)%sca(1) &
                           !				- eta_n*(pent_v*vl%objet(np)%sca(1)*(0.5d0*(epsilon_n*epsilon_n-epsilon_n1*epsilon_n1)  )  + b_v*(epsilon_n- epsilon_n1)))
                           !
                        end if 
   
      		      else if ( (epsilon_n >0d0) .and. (epsilon_n1<0d0 )) then 
                     vl%objet(np)%chocparmur =.true.
                     !Vitesse constante
                     vl%objet(np)%fparmur(nd) =  vl%objet(np)%fparmur(nd) - (1d0/(epsilon_n-epsilon_n1))*k_n*0.5d0*(-epsilon_n1*epsilon_n1)*vl%objet(np)%sca(1) - &
            			   				eta_n*(vl%objet(np)%v(nd)+pred_v(np,nd))*0.5d0
							
                     !pent_v = (vl%objet(np)%v(nd) - vl%objet(np)%vn(nd))/(pas_de_temps*vl%objet(np)%v(nd))
                     !b_v = (vl%objet(np)%v(nd)+pred_v(np,nd))*0.5d0 - pent_v*(epsilon_n1_2*vl%objet(np)%sca(1))
                     !
                     !vl%objet(np)%fparmur(nd) =  vl%objet(np)%fparmur(nd) - (1d0/(epsilon_n-epsilon_n1))*(k_n*0.5d0*(-epsilon_n1*epsilon_n1)*vl%objet(np)%sca(1) &
                     !				- eta_n*(pent_v*vl%objet(np)%sca(1)*(0.5d0*(-epsilon_n1*epsilon_n1)  )  - b_v*epsilon_n1))
                     !
  
		      	   end if
               end if
               end if
             end do
          end if
            !-------------------------------------------------------------------------------
            !Hakkoum_10/28/2021
            if (nproc.gt.1) then
               call mpi_bcast(vl%objet(np)%chocparmur,1,mpi_logical,vl%objet(np)%locpart,comm3d,code)
               do nd =1,ndim
                  call mpi_bcast(vl%objet(np)%fparmur(nd),ndim,mpi_double_precision,vl%objet(np)%locpart,comm3d,code)
               enddo
            endif
      end do
       
       !-------------------------------------------------------------------------------
       !fin du test sur le choc particule / paroi
       !-------------------------------------------------------------------------------

        
       !-------------------------------------------------------------------------------
       !TEST SUR LE CHOC PARTICULE / cylindre
       !-------------------------------------------------------------------------------

    ELSE
       if(NDIM.EQ.3) then
          VL%OBJET(1:VL%KPT)%CHOCPARMUR = .FALSE.
          DO NP=1,VL%KPT
             DO ND=1,NDIM
                VL%OBJET(NP)%FPARMUR(ND) = 0.0D0
             END DO
          END DO

          DO NP=1,VL%KPT
             if (VL%OBJET(NP)%IN) then
                RAYON_T =  SQRT(((VL%OBJET(NP)%SYMPER(1,1)+PRED_POS(NP,1,1))*0.5D0-cylindre_C_X1)**2 + &
                     ((VL%OBJET(NP)%SYMPER(1,2)+PRED_POS(NP,1,2))*0.5D0-cylindre_c_x3)**2)
                EPSILON = (RAYON_cylindre - RAYON_T)/(VL%OBJET(NP)%SCA(1)+1D-40) - 1.0D0
                if (RAYON_T .GT. 0.0D0) then
                   COSINUS = ((VL%OBJET(NP)%SYMPER(1,1)+PRED_POS(NP,1,1))*0.5D0-cylindre_C_X1)/(RAYON_T+1D-40)
                   SINUS   = ((VL%OBJET(NP)%SYMPER(1,2)+PRED_POS(NP,1,2))*0.5D0-cylindre_c_x3)/(RAYON_T+1D-40)
                   V_n = ((VL%OBJET(NP)%V(1)+PRED_V(NP,1))*COSINUS+(VL%OBJET(NP)%V(2)+PRED_V(NP,2))*SINUS)*0.5D0

                END if
                if (VL%CHOC_LUBRI) then
                   if (EPSILON.LT.VL%LUBRI_EPS_PM) then
                      VL%OBJET(NP)%CHOCPARMUR=.TRUE.
                      if (EPSILON.GT.VL%LUBRI_EPS_1) then
                         !! LAMBDA_E VARIABLE
                         LAMBDA_E = 1.0D0/(EPSILON+1D-40) - 0.2D0*LOG(EPSILON) - (1.0D0/21.0D0)*EPSILON*LOG(EPSILON)

                      ELSE if (EPSILON.LE.VL%LUBRI_EPS_1.AND.EPSILON.GT.-VL%LUBRI_EPS_2) then
                         !! LAMBDA_E = LAMBDA_E1
                         LAMBDA_E = VL%LUBRI_LAMBDA_PM_EPS1
                      ELSE
                         !WRITE(6,*) "WARNING : LA PARTICULE",NP,"EST TROP LOIN POUR LA LUBRifICATION"
                         
                         LAMBDA_E = VL%LUBRI_LAMBDA_PM
                      END if
                      VL%OBJET(NP)%FPARMUR(1) =  VL%OBJET(NP)%FPARMUR(1) -  (6.0D0*PI*fluids(1)%mu*VL%OBJET(NP)%SCA(1)*V_N*&
                           ( LAMBDA_E - VL%LUBRI_LAMBDA_PM))*COSINUS
                      VL%OBJET(NP)%FPARMUR(2) =  VL%OBJET(NP)%FPARMUR(2) -  (6.0D0*PI*fluids(1)%mu*VL%OBJET(NP)%SCA(1)*V_N*&
                           ( LAMBDA_E - VL%LUBRI_LAMBDA_PM))*SINUS

                   END if
                END if
                if (VL%CHOC_SEC) then

                   K_N = VL%OBJET(NP)%MASSE*(PI**2+(LOG(VL%SEC_ED))**2)/(((VL%SEC_NC)*pas_de_temps)**2+1D-40)
                   ETA_N = -2.0D0*VL%OBJET(NP)%MASSE*LOG(VL%SEC_ED)/((VL%SEC_NC)*pas_de_temps+1D-40)
                   if (EPSILON<VL%SEC_DISTCRI/(VL%OBJET(NP)%SCA(1)+1D-40)) then
                      if (EPSILON<-0.15) then
                         WRITE(6,*) "WARNING : FAUSSE COLLISION-PAROI A SEC",NP,NP2,EPSILON
                      ENDif
                      VL%OBJET(NP)%CHOCPARMUR=.TRUE.
                      VL%OBJET(NP)%FPARMUR(1)=  VL%OBJET(NP)%FPARMUR(1) - K_N*ABS(EPSILON*VL%OBJET(NP)%SCA(1)-&
                           VL%SEC_DISTCRI)*COSINUS - ETA_N*V_N*COSINUS
                      VL%OBJET(NP)%FPARMUR(2)=  VL%OBJET(NP)%FPARMUR(2) - K_N*ABS(EPSILON*VL%OBJET(NP)%SCA(1)-&
                           VL%SEC_DISTCRI)*SINUS - ETA_N*V_N*SINUS

                   END if
                END if
             END if
            if (nproc.gt.1) then
              call mpi_bcast(vl%objet(np)%chocparmur,1,mpi_logical,vl%objet(np)%locpart,comm3d,code)
              do nd =1,ndim
                call mpi_bcast(vl%objet(np)%fparmur(nd),ndim,mpi_double_precision,vl%objet(np)%locpart,comm3d,code)
              enddo
            endif
             ! CALL MPI_BCAST(VL%OBJET(NP)%CHOCPARMUR,1,MPI_LOGICAL,VL%OBJET(NP)%LOCPART,comm3d,CODE)
             ! CALL MPI_BCAST(VL%OBJET(NP)%FPARMUR(1),3,MPI_DOUBLE_PRECISION,VL%OBJET(NP)%LOCPART,comm3d,CODE)
          END DO
       END if
    END if
   
    !-------------------------------------------------------------------------------
    !fin du TEST SUR LE CHOC PARTICULE / cylindre
    !-------------------------------------------------------------------------------



    !-------------------------------------------------------------------------------
    ! inclusion de la force dans smv 
    !-------------------------------------------------------------------------------
    !print*, vl%objet(1)%moment(1),vl%objet(1)%moment(2),vl%objet(1)%moment(3), 'Moment'
    !print*, vl%objet(1)%fparmur(1), vl%objet(1)%fparmur(2), vl%objet(3)%fparmur(3), ' Foce de collision'
    smvu=0d0;smvv=0d0;smvw=0d0
    if (allocated(smvuin)) smvu=smvuin
    if (allocated(smvvin)) smvv=smvvin
    if (allocated(smvwin)) smvw=smvwin
    do np=1,vl%kpt
       if (vl%objet(np)%on) then 
          f_total = 0.0d0
          if (vl%objet(np)%chocparpar) f_total = vl%objet(np)%fparpar
          if (vl%objet(np)%chocparmur) f_total = f_total + vl%objet(np)%fparmur
          if ((dabs(f_total(1)).gt.0.0d0).or.(dabs(f_total(2)).gt.0.0d0).or.(dabs(vl%objet(np)%moment(3)).gt.0.0d0).or. & 
               ((ndim.eq.3).and.((dabs(f_total(3)).gt.0.0d0).or.(dabs(vl%objet(np)%moment(1)).gt.0.0d0).or.(dabs(vl%objet(np)%moment(2)).gt.0.0d0)))) then
             do nbt=1,vl%objet(np)%nbtimes
                call Sphere_Collisions_Force_NSSourceTerm(vl%objet(np)%symper(nbt,1:3),vl%objet(np)%sca(1),f_total,vl%objet(np)%moment,smvu,smvv,smvw,ndim)
             end do
          end if
       end if
    end do
       !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'sortie Sphere_Collisions'
    !-------------------------------------------------------------------------------
  end subroutine Sphere_Collisions
  
  !-----------------------------------------------------------------------------
  function Intg_lambda(e)
  	real(8) :: e
	real(8) :: Intg_lambda
	Intg_lambda = log(e) - (1d0/5d0)*e*(log(e) -1d0) - &
			(1.0d0/42.0d0)*e*e*(log(e) - 0.5d0) + 0.9713*e
  end function Intg_lambda
  
  function Intg_lambda_pp(e)
  	real(8) :: e
	real(8) :: Intg_lambda_pp
	Intg_lambda_pp = 0.5d0*log(e) - (9d0/20d0)*e*(log(e) -1d0) - &
			(3.0d0/112.0d0)*e*e*(log(e) - 0.5d0) + 1.346*e
  end function Intg_lambda_pp
  
  function Intg_e_lambda(e)
  	real(8) :: e
	real(8) :: Intg_e_lambda
	Intg_e_lambda = e  - (1d0/5d0)*e*e*(log(e) -0.5d0) - &
			  (1d0/63.0d0)*e*e*e*(log(e) - 1d0/3d0) + &
        		     0.5d0*0.9713d0*e*e
  end function Intg_e_lambda

  !===============================================================================
end module Bib_VOFLag_Sphere_Collisions
!===============================================================================
  


