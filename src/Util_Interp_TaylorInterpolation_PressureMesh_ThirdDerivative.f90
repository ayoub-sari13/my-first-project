!===============================================================================
module Bib_VOFLag_TaylorInterpolation_PressureMesh_ThirdDerivative
    !===============================================================================
    contains
    !-----------------------------------------------------------------------------  
    !> @author amine chadil
    ! 
    !> @brief cette routine 
    !
    !> @param [in]  
    !> @param [in]  
    !> @param [out] 
    !> @param [in]  
    !-----------------------------------------------------------------------------
    subroutine TaylorInterpolation_PressureMesh_ThirdDerivative (pres,i,j,k,Order,norm,&
                                                                 ThirdDerivative,ndim, &
                                                                 dx,dy,dz)
   !===============================================================================
   !modules
   !===============================================================================
   !-------------------------------------------------------------------------------
   !Modules de definition des structures et variables => src/mod/module_...f90
   !-------------------------------------------------------------------------------

   !Modules de subroutines de la librairie VOF-Lag
   !-------------------------------------------------------------------------------
   use Bib_VOFLag_DirectionOnEulerianMeshFollowingNormalVector
   !-------------------------------------------------------------------------------

   !===============================================================================
   !declarations des variables
   !===============================================================================
   implicit none
   !-------------------------------------------------------------------------------
   !variables globales
   !-------------------------------------------------------------------------------
   real(8), dimension(:,:,:), allocatable, intent(in)  :: pres
   real(8), dimension(:),     allocatable, intent(in)  :: dx,dy,dz
   real(8), dimension(ndim)              , intent(in)  :: norm         
   integer                               , intent(in)  :: i,j,k,Order,ndim
   real(8), dimension(ndim,ndim,ndim)    , intent(out) :: ThirdDerivative
   !-------------------------------------------------------------------------------
   !variables locales 
   !-------------------------------------------------------------------------------
   integer                                      :: i_dir,j_dir,k_dir
   !-------------------------------------------------------------------------------

   ThirdDerivative = 0.D0
   select case(Order)
   case(4)
     call DirectionOnEulerianMeshFollowingNormalVector (norm(1),i_dir)
     call DirectionOnEulerianMeshFollowingNormalVector (norm(2),j_dir)
     if (ndim==3) call DirectionOnEulerianMeshFollowingNormalVector (norm(3),k_dir)
     ThirdDerivative(1,1,1) = ( 49d0*pres(i,j,k)&
                              -232d0*pres(i+i_dir,j,k)&
                              +461d0*pres(i+2*i_dir,j,k)&
                              -496d0*pres(i+3*i_dir,j,k)&
                              +307d0*pres(i+4*i_dir,j,k)&
                              -104d0*pres(i+5*i_dir,j,k)&
                              + 15d0*pres(i+6*i_dir,j,k))&
                              /( 8d0*sign(1.D0,norm(1))*dx(min(i,i+i_dir))**3+1D-40)
     ThirdDerivative(2,2,2) = ( 49d0*pres(i,j,k)&
                              -232d0*pres(i,j+j_dir,k)&
                              +461d0*pres(i,j+2*j_dir,k)&
                              -496d0*pres(i,j+3*j_dir,k)&
                              +307d0*pres(i,j+4*j_dir,k)&
                              -104d0*pres(i,j+5*j_dir,k)&
                              + 15d0*pres(i,j+6*j_dir,k))&
                              /( 8d0*sign(1.D0,norm(2))*dy(min(j,j+j_dir))**3+1D-40)

     ThirdDerivative(1,1,2) = ( 2775d0*pres(i,j,k)&
                              - 8725d0*pres(i+i_dir,j,k)&
                              +10895d0*pres(i+2*i_dir,j,k)&
                              - 7105d0*pres(i+3*i_dir,j,k)&
                              + 2550d0*pres(i+4*i_dir,j,k)&
                              -  390d0*pres(i+5*i_dir,j,k)&
                              - 4085d0*pres(i,j+j_dir,k)&
                              +11931d0*pres(i+i_dir,j+j_dir,k)&
                              -13489d0*pres(i+2*i_dir,j+j_dir,k)&
                              + 7911d0*pres(i+3*i_dir,j+j_dir,k)&
                              - 2654d0*pres(i+4*i_dir,j+j_dir,k)&
                              +  386d0*pres(i+5*i_dir,j+j_dir,k)&
                              + 1675d0*pres(i,j+2*j_dir,k)&
                              - 3889d0*pres(i+i_dir,j+2*j_dir,k)&
                              + 2856d0*pres(i+2*i_dir,j+2*j_dir,k)&
                              -  744d0*pres(i+3*i_dir,j+2*j_dir,k)&
                              +  101d0*pres(i+4*i_dir,j+2*j_dir,k)&
                              +    1d0*pres(i+5*i_dir,j+2*j_dir,k)&
                              -  385d0*pres(i,j+3*j_dir,k)&
                              +  711d0*pres(i+i_dir,j+3*j_dir,k)&
                              -  264d0*pres(i+2*i_dir,j+3*j_dir,k)&
                              -   64d0*pres(i+3*i_dir,j+3*j_dir,k)&
                              +    1d0*pres(i+4*i_dir,j+3*j_dir,k)&
                              +    1d0*pres(i+5*i_dir,j+3*j_dir,k)&
                              +   10d0*pres(i,j+4*j_dir,k)&
                              -   14d0*pres(i+i_dir,j+4*j_dir,k)&
                              +    1d0*pres(i+2*i_dir,j+4*j_dir,k)&
                              +    1d0*pres(i+3*i_dir,j+4*j_dir,k)&
                              +    1d0*pres(i+4*i_dir,j+4*j_dir,k)&
                              +    1d0*pres(i+5*i_dir,j+4*j_dir,k)&
                              +   10d0*pres(i,j+5*j_dir,k)&
                              -   14d0*pres(i+i_dir,j+5*j_dir,k)&
                              +    1d0*pres(i+2*i_dir,j+5*j_dir,k)&
                              +    1d0*pres(i+3*i_dir,j+5*j_dir,k)&
                              +    1d0*pres(i+4*i_dir,j+5*j_dir,k)&
                              +    1d0*pres(i+5*i_dir,j+5*j_dir,k))&
                              /( 480d0*dx(min(i,i+i_dir))**2*sign(1.D0,norm(2))*dy(min(j,j+j_dir))+1D-40)

     ThirdDerivative(1,2,1)=ThirdDerivative(1,1,2)
     ThirdDerivative(2,1,1)=ThirdDerivative(1,1,2)

     ThirdDerivative(2,2,1) = ( 2775d0*pres(i,j,k)&
                              - 8725d0*pres(i,j+j_dir,k)&
                              +10895d0*pres(i,j+2*j_dir,k)&
                              - 7105d0*pres(i,j+3*j_dir,k)&
                              + 2550d0*pres(i,j+4*j_dir,k)&
                              -  390d0*pres(i,j+5*j_dir,k)&
                              - 4085d0*pres(i+i_dir,j,k)&
                              +11931d0*pres(i+i_dir,j+j_dir,k)&
                              -13489d0*pres(i+i_dir,j+2*j_dir,k)&
                              + 7911d0*pres(i+i_dir,j+3*j_dir,k)&
                              - 2654d0*pres(i+i_dir,j+4*j_dir,k)&
                              +  386d0*pres(i+i_dir,j+5*j_dir,k)&
                              + 1675d0*pres(i+2*i_dir,j,k)&
                              - 3889d0*pres(i+2*i_dir,j+j_dir,k)&
                              + 2856d0*pres(i+2*i_dir,j+2*j_dir,k)&
                              -  744d0*pres(i+2*i_dir,j+3*j_dir,k)&
                              +  101d0*pres(i+2*i_dir,j+4*j_dir,k)&
                              +    1d0*pres(i+2*i_dir,j+5*j_dir,k)&
                              -  385d0*pres(i+3*i_dir,j,k)&
                              +  711d0*pres(i+3*i_dir,j+j_dir,k)&
                              -  264d0*pres(i+3*i_dir,j+2*j_dir,k)&
                              -   64d0*pres(i+3*i_dir,j+3*j_dir,k)&
                              +    1d0*pres(i+3*i_dir,j+4*j_dir,k)&
                              +    1d0*pres(i+3*i_dir,j+5*j_dir,k)&
                              +   10d0*pres(i+4*i_dir,j,k)&
                              -   14d0*pres(i+4*i_dir,j+j_dir,k)&
                              +    1d0*pres(i+4*i_dir,j+2*j_dir,k)&
                              +    1d0*pres(i+4*i_dir,j+3*j_dir,k)&
                              +    1d0*pres(i+4*i_dir,j+4*j_dir,k)&
                              +    1d0*pres(i+4*i_dir,j+5*j_dir,k)&
                              +   10d0*pres(i+5*i_dir,j,k)&
                              -   14d0*pres(i+5*i_dir,j+j_dir,k)&
                              +    1d0*pres(i+5*i_dir,j+2*j_dir,k)&
                              +    1d0*pres(i+5*i_dir,j+3*j_dir,k)&
                              +    1d0*pres(i+5*i_dir,j+4*j_dir,k)&
                              +    1d0*pres(i+5*i_dir,j+5*j_dir,k))&
                              /( 480d0*dy(min(j,j+j_dir))**2*sign(1.D0,norm(1))*dx(min(i,i+i_dir))+1D-40)

     ThirdDerivative(1,2,2)=ThirdDerivative(2,2,1)
     ThirdDerivative(2,1,2)=ThirdDerivative(2,2,1)
     if (ndim==3) then 
       ThirdDerivative(3,3,3) = ( 49d0*pres(i,j,k)&
                                -232d0*pres(i,j,k+k_dir)&
                                +461d0*pres(i,j,k+2*k_dir)&
                                -496d0*pres(i,j,k+3*k_dir)&
                                +307d0*pres(i,j,k+4*k_dir)&
                                -104d0*pres(i,j,k+5*k_dir)&
                                + 15d0*pres(i,j,k+6*k_dir))&
                                /( 8d0*sign(1.D0,norm(3))*dz(min(k,k+k_dir))**3+1D-40)

       ThirdDerivative(3,3,2) = ( 2775d0*pres(i,j,k)&
                              - 8725d0*pres(i,j,k+k_dir)&
                              +10895d0*pres(i,j,k+2*k_dir)&
                              - 7105d0*pres(i,j,k+3*k_dir)&
                              + 2550d0*pres(i,j,k+4*k_dir)&
                              -  390d0*pres(i,j,k+5*k_dir)&
                              - 4085d0*pres(i,j+j_dir,k)&
                              +11931d0*pres(i,j+j_dir,k+k_dir)&
                              -13489d0*pres(i,j+j_dir,k+2*k_dir)&
                              + 7911d0*pres(i,j+j_dir,k+3*k_dir)&
                              - 2654d0*pres(i,j+j_dir,k+4*k_dir)&
                              +  386d0*pres(i,j+j_dir,k+5*k_dir)&
                              + 1675d0*pres(i,j+2*j_dir,k)&
                              - 3889d0*pres(i,j+2*j_dir,k+k_dir)&
                              + 2856d0*pres(i,j+2*j_dir,k+2*k_dir)&
                              -  744d0*pres(i,j+2*j_dir,k+3*k_dir)&
                              +  101d0*pres(i,j+2*j_dir,k+4*k_dir)&
                              +    1d0*pres(i,j+2*j_dir,k+5*k_dir)&
                              -  385d0*pres(i,j+3*j_dir,k)&
                              +  711d0*pres(i,j+3*j_dir,k+k_dir)&
                              -  264d0*pres(i,j+3*j_dir,k+2*k_dir)&
                              -   64d0*pres(i,j+3*j_dir,k+3*k_dir)&
                              +    1d0*pres(i,j+3*j_dir,k+4*k_dir)&
                              +    1d0*pres(i,j+3*j_dir,k+5*k_dir)&
                              +   10d0*pres(i,j+4*j_dir,k)&
                              -   14d0*pres(i,j+4*j_dir,k+k_dir)&
                              +    1d0*pres(i,j+4*j_dir,k+2*k_dir)&
                              +    1d0*pres(i,j+4*j_dir,k+3*k_dir)&
                              +    1d0*pres(i,j+4*j_dir,k+4*k_dir)&
                              +    1d0*pres(i,j+4*j_dir,k+5*k_dir)&
                              +   10d0*pres(i,j+5*j_dir,k)&
                              -   14d0*pres(i,j+5*j_dir,k+k_dir)&
                              +    1d0*pres(i,j+5*j_dir,k+2*k_dir)&
                              +    1d0*pres(i,j+5*j_dir,k+3*k_dir)&
                              +    1d0*pres(i,j+5*j_dir,k+4*k_dir)&
                              +    1d0*pres(i,j+5*j_dir,k+5*k_dir))&
                              /( 480d0*dz(min(k,k+k_dir))**2*sign(1.D0,norm(2))*dy(min(j,j+j_dir))+1D-40)

       ThirdDerivative(3,2,3)=ThirdDerivative(3,3,2)
       ThirdDerivative(2,3,3)=ThirdDerivative(3,3,2)

       ThirdDerivative(3,3,1) = ( 2775d0*pres(i,j,k)&
                              - 8725d0*pres(i,j,k+k_dir)&
                              +10895d0*pres(i,j,k+2*k_dir)&
                              - 7105d0*pres(i,j,k+3*k_dir)&
                              + 2550d0*pres(i,j,k+4*k_dir)&
                              -  390d0*pres(i,j,k+5*k_dir)&
                              - 4085d0*pres(i+i_dir,j,k)&
                              +11931d0*pres(i+i_dir,j,k+k_dir)&
                              -13489d0*pres(i+i_dir,j,k+2*k_dir)&
                              + 7911d0*pres(i+i_dir,j,k+3*k_dir)&
                              - 2654d0*pres(i+i_dir,j,k+4*k_dir)&
                              +  386d0*pres(i+i_dir,j,k+5*k_dir)&
                              + 1675d0*pres(i+2*i_dir,j,k)&
                              - 3889d0*pres(i+2*i_dir,j,k+k_dir)&
                              + 2856d0*pres(i+2*i_dir,j,k+2*k_dir)&
                              -  744d0*pres(i+2*i_dir,j,k+3*k_dir)&
                              +  101d0*pres(i+2*i_dir,j,k+4*k_dir)&
                              +    1d0*pres(i+2*i_dir,j,k+5*k_dir)&
                              -  385d0*pres(i+3*i_dir,j,k)&
                              +  711d0*pres(i+3*i_dir,j,k+k_dir)&
                              -  264d0*pres(i+3*i_dir,j,k+2*k_dir)&
                              -   64d0*pres(i+3*i_dir,j,k+3*k_dir)&
                              +    1d0*pres(i+3*i_dir,j,k+4*k_dir)&
                              +    1d0*pres(i+3*i_dir,j,k+5*k_dir)&
                              +   10d0*pres(i+4*i_dir,j,k)&
                              -   14d0*pres(i+4*i_dir,j,k+k_dir)&
                              +    1d0*pres(i+4*i_dir,j,k+2*k_dir)&
                              +    1d0*pres(i+4*i_dir,j,k+3*k_dir)&
                              +    1d0*pres(i+4*i_dir,j,k+4*k_dir)&
                              +    1d0*pres(i+4*i_dir,j,k+5*k_dir)&
                              +   10d0*pres(i+5*i_dir,j,k)&
                              -   14d0*pres(i+5*i_dir,j,k+k_dir)&
                              +    1d0*pres(i+5*i_dir,j,k+2*k_dir)&
                              +    1d0*pres(i+5*i_dir,j,k+3*k_dir)&
                              +    1d0*pres(i+5*i_dir,j,k+4*k_dir)&
                              +    1d0*pres(i+5*i_dir,j,k+5*k_dir))&
                              /( 480d0*dz(min(k,k+k_dir))**2*sign(1.D0,norm(2))*dx(min(i,i+i_dir))+1D-40)

       ThirdDerivative(3,1,3)=ThirdDerivative(3,3,1)
       ThirdDerivative(1,3,3)=ThirdDerivative(3,3,1)

       ThirdDerivative(2,2,3) = ( 2775d0*pres(i,j,k)&
                              - 8725d0*pres(i,j+j_dir,k)&
                              +10895d0*pres(i,j+2*j_dir,k)&
                              - 7105d0*pres(i,j+3*j_dir,k)&
                              + 2550d0*pres(i,j+4*j_dir,k)&
                              -  390d0*pres(i,j+5*j_dir,k)&
                              - 4085d0*pres(i,j,k+k_dir)&
                              +11931d0*pres(i,j+j_dir,k+k_dir)&
                              -13489d0*pres(i,j+2*j_dir,k+k_dir)&
                              + 7911d0*pres(i,j+3*j_dir,k+k_dir)&
                              - 2654d0*pres(i,j+4*j_dir,k+k_dir)&
                              +  386d0*pres(i,j+5*j_dir,k+k_dir)&
                              + 1675d0*pres(i,j,k+2*i_dir)&
                              - 3889d0*pres(i,j+j_dir,k+2*k_dir)&
                              + 2856d0*pres(i,j+2*j_dir,k+2*k_dir)&
                              -  744d0*pres(i,j+3*j_dir,k+2*k_dir)&
                              +  101d0*pres(i,j+4*j_dir,k+2*k_dir)&
                              +    1d0*pres(i,j+5*j_dir,k+2*k_dir)&
                              -  385d0*pres(i,j,k+3*k_dir)&
                              +  711d0*pres(i,j+j_dir,k+3*k_dir)&
                              -  264d0*pres(i,j+2*j_dir,k+3*k_dir)&
                              -   64d0*pres(i,j+3*j_dir,k+3*k_dir)&
                              +    1d0*pres(i,j+4*j_dir,k+3*k_dir)&
                              +    1d0*pres(i,j+5*j_dir,k+3*k_dir)&
                              +   10d0*pres(i,j,k+4*k_dir)&
                              -   14d0*pres(i,j+j_dir,k+4*k_dir)&
                              +    1d0*pres(i,j+2*j_dir,k+4*k_dir)&
                              +    1d0*pres(i,j+3*j_dir,k+4*k_dir)&
                              +    1d0*pres(i,j+4*j_dir,k+4*k_dir)&
                              +    1d0*pres(i,j+5*j_dir,k+4*k_dir)&
                              +   10d0*pres(i,j,k+5*k_dir)&
                              -   14d0*pres(i,j+j_dir,k+5*k_dir)&
                              +    1d0*pres(i,j+2*j_dir,k+5*k_dir)&
                              +    1d0*pres(i,j+3*j_dir,k+5*k_dir)&
                              +    1d0*pres(i,j+4*j_dir,k+5*k_dir)&
                              +    1d0*pres(i,j+5*j_dir,k+5*k_dir))&
                              /( 480d0*dy(min(j,j+j_dir))**2*sign(1.D0,norm(1))*dz(min(k,k+k_dir))+1D-40)

       ThirdDerivative(2,3,2)=ThirdDerivative(2,2,3)
       ThirdDerivative(3,2,2)=ThirdDerivative(2,2,3)

       ThirdDerivative(1,1,3) = ( 2775d0*pres(i,j,k)&
                              - 8725d0*pres(i+i_dir,j,k)&
                              +10895d0*pres(i+2*i_dir,j,k)&
                              - 7105d0*pres(i+3*i_dir,j,k)&
                              + 2550d0*pres(i+4*i_dir,j,k)&
                              -  390d0*pres(i+5*i_dir,j,k)&
                              - 4085d0*pres(i,j,k+k_dir)&
                              +11931d0*pres(i+i_dir,j,k+k_dir)&
                              -13489d0*pres(i+2*i_dir,j,k+k_dir)&
                              + 7911d0*pres(i+3*i_dir,j,k+k_dir)&
                              - 2654d0*pres(i+4*i_dir,j,k+k_dir)&
                              +  386d0*pres(i+5*i_dir,j,k+k_dir)&
                              + 1675d0*pres(i,j,k+2*k_dir)&
                              - 3889d0*pres(i+i_dir,j,k+2*k_dir)&
                              + 2856d0*pres(i+2*i_dir,j,k+2*k_dir)&
                              -  744d0*pres(i+3*i_dir,j,k+2*k_dir)&
                              +  101d0*pres(i+4*i_dir,j,k+2*k_dir)&
                              +    1d0*pres(i+5*i_dir,j,k+2*k_dir)&
                              -  385d0*pres(i,j,k+3*k_dir)&
                              +  711d0*pres(i+i_dir,j,k+3*k_dir)&
                              -  264d0*pres(i+2*i_dir,j,k+3*k_dir)&
                              -   64d0*pres(i+3*i_dir,j,k+3*k_dir)&
                              +    1d0*pres(i+4*i_dir,j,k+3*k_dir)&
                              +    1d0*pres(i+5*i_dir,j,k+3*k_dir)&
                              +   10d0*pres(i,j,k+4*k_dir)&
                              -   14d0*pres(i+i_dir,j,k+4*k_dir)&
                              +    1d0*pres(i+2*i_dir,j,k+4*k_dir)&
                              +    1d0*pres(i+3*i_dir,j,k+4*k_dir)&
                              +    1d0*pres(i+4*i_dir,j,k+4*k_dir)&
                              +    1d0*pres(i+5*i_dir,j,k+4*k_dir)&
                              +   10d0*pres(i,j,k+5*k_dir)&
                              -   14d0*pres(i+i_dir,j,k+5*k_dir)&
                              +    1d0*pres(i+2*i_dir,j,k+5*k_dir)&
                              +    1d0*pres(i+3*i_dir,j,k+5*k_dir)&
                              +    1d0*pres(i+4*i_dir,j,k+5*k_dir)&
                              +    1d0*pres(i+5*i_dir,j,k+5*k_dir))&
                              /( 480d0*dx(min(i,i+i_dir))**2*sign(1.D0,norm(2))*dz(min(k,k+k_dir))+1D-40)
       
       ThirdDerivative(1,3,1)=ThirdDerivative(1,1,3)
       ThirdDerivative(3,1,1)=ThirdDerivative(1,1,3)

       ThirdDerivative(1,2,3) = (  4069d0*pres(i,j,k)&
                                - 13656d0*pres(i+i_dir,j,k)&
                                -     6d0*pres(i+2*i_dir,j,k)&
                                -     6d0*pres(i+3*i_dir,j,k)&
                                -     6d0*pres(i+4*i_dir,j,k)&
                                + 12194d0*pres(i,j+j_dir,k)&
                                - 23256d0*pres(i+i_dir,j+j_dir,k)&
                                + 10194d0*pres(i+2*i_dir,j+j_dir,k)&
                                +   444d0*pres(i+3*i_dir,j+j_dir,k)&
                                -   306d0*pres(i+4*i_dir,j+j_dir,k)&
                                - 16681d0*pres(i,j+2*j_dir,k)&
                                + 77094d0*pres(i+i_dir,j+2*j_dir,k)&
                                - 19206d0*pres(i+2*i_dir,j+2*j_dir,k)&
                                -   456d0*pres(i+3*i_dir,j+2*j_dir,k)&
                                +   294d0*pres(i+4*i_dir,j+2*j_dir,k)&
                                -     6d0*pres(i,j+3*j_dir,k)&
                                - 52956d0*pres(i+i_dir,j+3*j_dir,k)&
                                + 11244d0*pres(i+2*i_dir,j+3*j_dir,k)&
                                +   244d0*pres(i+3*i_dir,j+3*j_dir,k)&
                                -     6d0*pres(i+4*i_dir,j+3*j_dir,k)&
                                -     6d0*pres(i,j+4*j_dir,k)&
                                + 13794d0*pres(i+i_dir,j+4*j_dir,k)&
                                -  3006d0*pres(i+2*i_dir,j+4*j_dir,k)&
                                -     6d0*pres(i+3*i_dir,j+4*j_dir,k)&
                                -     6d0*pres(i+4*i_dir,j+4*j_dir,k)&
                                -     6d0*pres(i,j,k+k_dir)&
                                + 18294d0*pres(i+i_dir,j,k+k_dir)&
                                +   744d0*pres(i+2*i_dir,j,k+k_dir)&
                                -     6d0*pres(i+3*i_dir,j,k+k_dir)&
                                -     6d0*pres(i+4*i_dir,j,k+k_dir)&
                                + 59869d0*pres(i,j+j_dir,k+k_dir)&
                                -     6d0*pres(i+i_dir,j+j_dir,k+k_dir)&
                                -     6d0*pres(i+2*i_dir,j+j_dir,k+k_dir)&
                                -     6d0*pres(i+3*i_dir,j+j_dir,k+k_dir)&
                                -     6d0*pres(i+4*i_dir,j+j_dir,k+k_dir)&
                                -177281d0*pres(i,j+2*j_dir,k+k_dir)&
                                - 40506d0*pres(i+i_dir,j+2*j_dir,k+k_dir)&
                                -   756d0*pres(i+2*i_dir,j+2*j_dir,k+k_dir)&
                                -     6d0*pres(i+3*i_dir,j+2*j_dir,k+k_dir)&
                                -     6d0*pres(i+4*i_dir,j+2*j_dir,k+k_dir)&
                                +153019d0*pres(i,j+3*j_dir,k+k_dir)&
                                + 26994d0*pres(i+i_dir,j+3*j_dir,k+k_dir)&
                                +  1494d0*pres(i+2*i_dir,j+3*j_dir,k+k_dir)&
                                -     6d0*pres(i+3*i_dir,j+3*j_dir,k+k_dir)&
                                -     6d0*pres(i+4*i_dir,j+3*j_dir,k+k_dir)&
                                - 34281d0*pres(i,j+4*j_dir,k+k_dir)&
                                -  7506d0*pres(i+i_dir,j+4*j_dir,k+k_dir)&
                                -     6d0*pres(i+2*i_dir,j+4*j_dir,k+k_dir)&
                                -     6d0*pres(i+3*i_dir,j+4*j_dir,k+k_dir)&
                                -     6d0*pres(i+4*i_dir,j+4*j_dir,k+k_dir)&
                                -  7581d0*pres(i,j,k+2*k_dir)&
                                +  3444d0*pres(i+i_dir,j,k+2*k_dir)&
                                -  4356d0*pres(i+2*i_dir,j,k+2*k_dir)&
                                -   606d0*pres(i+3*i_dir,j,k+2*k_dir)&
                                +   144d0*pres(i+4*i_dir,j,k+2*k_dir)&
                                -136281d0*pres(i,j+j_dir,k+2*k_dir)&
                                -  4506d0*pres(i+i_dir,j+j_dir,k+2*k_dir)&
                                +  3744d0*pres(i+2*i_dir,j+j_dir,k+2*k_dir)&
                                -     6d0*pres(i+3*i_dir,j+j_dir,k+2*k_dir)&
                                -     6d0*pres(i+4*i_dir,j+j_dir,k+2*k_dir)&
                                +348519d0*pres(i,j+2*j_dir,k+2*k_dir)&
                                +  3744d0*pres(i+i_dir,j+2*j_dir,k+2*k_dir)&
                                -     6d0*pres(i+2*i_dir,j+2*j_dir,k+2*k_dir)&
                                -     6d0*pres(i+3*i_dir,j+2*j_dir,k+2*k_dir)&
                                -     6d0*pres(i+4*i_dir,j+2*j_dir,k+2*k_dir)&
                                -257481d0*pres(i,j+3*j_dir,k+2*k_dir)&
                                -     6d0*pres(i+i_dir,j+3*j_dir,k+2*k_dir)&
                                -     6d0*pres(i+2*i_dir,j+3*j_dir,k+2*k_dir)&
                                -     6d0*pres(i+3*i_dir,j+3*j_dir,k+2*k_dir)&
                                -     6d0*pres(i+4*i_dir,j+3*j_dir,k+2*k_dir)&
                                + 51294d0*pres(i,j+4*j_dir,k+2*k_dir)&
                                -     6d0*pres(i+i_dir,j+4*j_dir,k+2*k_dir)&
                                -     6d0*pres(i+2*i_dir,j+4*j_dir,k+2*k_dir)&
                                -     6d0*pres(i+3*i_dir,j+4*j_dir,k+2*k_dir)&
                                -     6d0*pres(i+4*i_dir,j+4*j_dir,k+2*k_dir)&
                                -     6d0*pres(i,j,k+3*k_dir)&
                                -  1206d0*pres(i+i_dir,j,k+3*k_dir)&
                                -     6d0*pres(i+2*i_dir,j,k+3*k_dir)&
                                +   494d0*pres(i+3*i_dir,j,k+3*k_dir)&
                                -     6d0*pres(i+4*i_dir,j,k+3*k_dir)&
                                + 97519d0*pres(i,j+j_dir,k+3*k_dir)&
                                -     6d0*pres(i+i_dir,j+j_dir,k+3*k_dir)&
                                -     6d0*pres(i+2*i_dir,j+j_dir,k+3*k_dir)&
                                -     6d0*pres(i+3*i_dir,j+j_dir,k+3*k_dir)&
                                -     6d0*pres(i+4*i_dir,j+j_dir,k+3*k_dir)&
                                -212981d0*pres(i,j+2*j_dir,k+3*k_dir)&
                                -     6d0*pres(i+i_dir,j+2*j_dir,k+3*k_dir)&
                                -     6d0*pres(i+2*i_dir,j+2*j_dir,k+3*k_dir)&
                                -     6d0*pres(i+3*i_dir,j+2*j_dir,k+3*k_dir)&
                                -     6d0*pres(i+4*i_dir,j+2*j_dir,k+3*k_dir)&
                                +136469d0*pres(i,j+3*j_dir,k+3*k_dir)&
                                -     6d0*pres(i+i_dir,j+3*j_dir,k+3*k_dir)&
                                -     6d0*pres(i+2*i_dir,j+3*j_dir,k+3*k_dir)&
                                -     6d0*pres(i+3*i_dir,j+3*j_dir,k+3*k_dir)&
                                -     6d0*pres(i+4*i_dir,j+3*j_dir,k+3*k_dir)&
                                - 20181d0*pres(i,j+4*j_dir,k+3*k_dir)&
                                -     6d0*pres(i+i_dir,j+4*j_dir,k+3*k_dir)&
                                -     6d0*pres(i+2*i_dir,j+4*j_dir,k+3*k_dir)&
                                -     6d0*pres(i+3*i_dir,j+4*j_dir,k+3*k_dir)&
                                -     6d0*pres(i+4*i_dir,j+4*j_dir,k+3*k_dir)&
                                -     6d0*pres(i,j,k+4*k_dir)&
                                +   294d0*pres(i+i_dir,j,k+4*k_dir)&
                                -     6d0*pres(i+2*i_dir,j,k+4*k_dir)&
                                -     6d0*pres(i+3*i_dir,j,k+4*k_dir)&
                                -     6d0*pres(i+4*i_dir,j,k+4*k_dir)&
                                - 19531d0*pres(i,j+j_dir,k+4*k_dir)&
                                -     6d0*pres(i+i_dir,j+j_dir,k+4*k_dir)&
                                -     6d0*pres(i+2*i_dir,j+j_dir,k+4*k_dir)&
                                -     6d0*pres(i+3*i_dir,j+j_dir,k+4*k_dir)&
                                -     6d0*pres(i+4*i_dir,j+j_dir,k+4*k_dir)&
                                + 38294d0*pres(i,j+2*j_dir,k+4*k_dir)&
                                -     6d0*pres(i+i_dir,j+2*j_dir,k+4*k_dir)&
                                -     6d0*pres(i+2*i_dir,j+2*j_dir,k+4*k_dir)&
                                -     6d0*pres(i+3*i_dir,j+2*j_dir,k+4*k_dir)&
                                -     6d0*pres(i+4*i_dir,j+2*j_dir,k+4*k_dir)&
                                - 18931d0*pres(i,j+3*j_dir,k+4*k_dir)&
                                -     6d0*pres(i+i_dir,j+3*j_dir,k+4*k_dir)&
                                -     6d0*pres(i+2*i_dir,j+3*j_dir,k+4*k_dir)&
                                -     6d0*pres(i+3*i_dir,j+3*j_dir,k+4*k_dir)&
                                -     6d0*pres(i+4*i_dir,j+3*j_dir,k+4*k_dir)&
                                -     6d0*pres(i,j+4*j_dir,k+4*k_dir)&
                                -     6d0*pres(i+i_dir,j+4*j_dir,k+4*k_dir)&
                                -     6d0*pres(i+2*i_dir,j+4*j_dir,k+4*k_dir)&
                                -     6d0*pres(i+3*i_dir,j+4*j_dir,k+4*k_dir)&
                                -     6d0*pres(i+4*i_dir,j+4*j_dir,k+4*k_dir))&
                                /( 9000d0*sign(1.D0,norm(1))*dx(min(i,i+i_dir))*sign(1.D0,norm(2))*dy(min(j,j+j_dir))*sign(1.D0,norm(3))*dz(min(k,k+k_dir))+1D-40)

       ThirdDerivative(2,1,3)=ThirdDerivative(1,2,3)
       ThirdDerivative(2,3,1)=ThirdDerivative(1,2,3)
       ThirdDerivative(3,2,1)=ThirdDerivative(1,2,3)
       ThirdDerivative(3,1,2)=ThirdDerivative(1,2,3)
       ThirdDerivative(1,3,2)=ThirdDerivative(1,2,3)
     end if 
   case default
     write(*,*) "STOP : ordre non implemente (ThirdDerivative) "
     stop
   end select

end subroutine TaylorInterpolation_PressureMesh_ThirdDerivative

    !===============================================================================
end module Bib_VOFLag_TaylorInterpolation_PressureMesh_ThirdDerivative
!===============================================================================
    


