!========================================================================
!**
!**   NAME       : Init_EN.f90
!**
!**   AUTHOR     : B. trouette
!**
!**   FUNCTION   : Modules of subroutines for variable initialization of energy equation
!**
!**   DATES      : Version 1.0.0  : from : june, 16, 2015
!**
!========================================================================

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#include "precomp.h"

module mod_energy_thermophy
  
  use mod_Parameters, only: dim,                &
       & sx,ex,sy,ey,sz,ez,gx,gy,gz,            &
       & nb_phase,NS_activate,                  &
       & VOF_activate,LS_activate,FT_Delta,     &
       & NS_init_pre,FT_activate,EN_Boussinesq, &
       & pi
  use mod_struct_thermophysics
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
contains
  
  !*****************************************************************************************************
  subroutine Energy_Thermophysics(rho,condu,condv,condw,cp,cou,beta,tpb,fluids)
    !***************************************************************************************************
    use mod_Parameters, only: dxu,dyv,dzw,grid_x,grid_y,grid_z, &
         & VOF_init_case, VOF_mean_lambda
    use mod_Constants, only: d1p2
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable   :: rho,condu,condv,condw,cp
    real(8), dimension(:,:,:), allocatable   :: cou,beta,tpb
    type(phase_t), dimension(:), allocatable   :: fluids
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                  :: i,j,k
    integer                                  :: mean_cond
    real(8)                                  :: coul,dl,dl1,dl2
    real(8), dimension(:,:,:), allocatable   :: cond
    !-------------------------------------------------------------------------------
    ! Initialization of phase characteristics
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    if (nb_phase==1) then

       if (.not.NS_activate) rho=fluids(1)%rho
       condu=fluids(1)%lambda
       condv=fluids(1)%lambda
       if (dim==3) condw=fluids(1)%lambda
       cp=fluids(1)%cp
       if (EN_Boussinesq) then 
          beta=fluids(1)%beta
          tpb=fluids(1)%tpb
       end if
       
    elseif (nb_phase==2) then

       mean_cond=VOF_mean_lambda
       
       if (VOF_activate.or.LS_activate.or.FT_Delta) then

          if (.not.NS_activate) rho=cou*fluids(2)%rho+(1-cou)*fluids(1)%rho
          
          !-------------------------------------------------------------------------------
          ! heat capacity
          !-------------------------------------------------------------------------------
          cp=cou*fluids(2)%cp+(1-cou)*fluids(1)%cp
          !-------------------------------------------------------------------------------

          if (EN_Boussinesq) then 
             !-------------------------------------------------------------------------------
             ! beta value for Boussinesq approximation
             !-------------------------------------------------------------------------------
             beta=cou*fluids(2)%beta+(1-cou)*fluids(1)%beta
             !-------------------------------------------------------------------------------
             ! Boussinesq reference temperature
             !-------------------------------------------------------------------------------
             tpb=cou*fluids(2)%tpb+(1-cou)*fluids(1)%tpb
          end if

          select case(mean_cond)
          case(0)
             !-------------------------------------------------------------------------------
             ! arithmetic mean for face conductivity computation
             !-------------------------------------------------------------------------------
             if (dim==2) then
                do j=sy-1,ey+1
                   do i=sx-1,ex+1
                      coul=d1p2*(cou(i-1,j,1)+cou(i,j,1))
                      condu(i,j,1)=coul*fluids(2)%lambda+(1-coul)*fluids(1)%lambda
                      coul=d1p2*(cou(i,j-1,1)+cou(i,j,1))
                      condv(i,j,1)=coul*fluids(2)%lambda+(1-coul)*fluids(1)%lambda
                   enddo
                enddo
             else 
                do k=sz-1,ez+1
                   do j=sy-1,ey+1
                      do i=sx-1,ex+1
                         coul=d1p2*(cou(i-1,j,k)+cou(i,j,k))
                         condu(i,j,k)=coul*fluids(2)%lambda+(1-coul)*fluids(1)%lambda
                         coul=d1p2*(cou(i,j-1,k)+cou(i,j,k))
                         condv(i,j,k)=coul*fluids(2)%lambda+(1-coul)*fluids(1)%lambda
                         coul=d1p2*(cou(i,j,k-1)+cou(i,j,k))
                         condw(i,j,k)=coul*fluids(2)%lambda+(1-coul)*fluids(1)%lambda
                      enddo
                   enddo
                end do
             end if
          case(1)
             !-------------------------------------------------------------------------------
             ! harmonic mean for face conductivity computation
             !-------------------------------------------------------------------------------
             if (dim==2) then
                do j=sy-1,ey+1
                   do i=sx-1,ex+1
                      coul=d1p2*(cou(i-1,j,1)+cou(i,j,1))
                      condu(i,j,1)=fluids(1)%lambda*fluids(2)%lambda/ &
                           & ((1-coul)*fluids(2)%lambda+coul*fluids(1)%lambda)
                      coul=d1p2*(cou(i,j-1,1)+cou(i,j,1))
                      condv(i,j,1)=fluids(1)%lambda*fluids(2)%lambda/ &
                           & ((1-coul)*fluids(2)%lambda+coul*fluids(1)%lambda)
                   enddo
                enddo
             else 
                do k=sz-1,ez+1
                   do j=sy-1,ey+1
                      do i=sx-1,ex+1
                         coul=d1p2*(cou(i-1,j,k)+cou(i,j,k))
                         condu(i,j,k)=fluids(1)%lambda*fluids(2)%lambda/ &
                              & ((1-coul)*fluids(2)%lambda+coul*fluids(1)%lambda)
                         coul=d1p2*(cou(i,j-1,k)+cou(i,j,k))
                         condv(i,j,k)=fluids(1)%lambda*fluids(2)%lambda/ &
                              & ((1-coul)*fluids(2)%lambda+coul*fluids(1)%lambda)
                         coul=d1p2*(cou(i,j,k-1)+cou(i,j,k))
                         condw(i,j,k)=fluids(1)%lambda*fluids(2)%lambda/ &
                              & ((1-coul)*fluids(2)%lambda+coul*fluids(1)%lambda)
                      enddo
                   enddo
                end do
             end if
          case(2)
             allocate(cond(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
             cond=cou
             if (dim==2) then
                do j=sy-1,ey+1
                   do i=sx-1,ex+1
                      if (cond(i,j,1)>d1p2) then
                         cond(i,j,1)=fluids(2)%lambda
                      else
                         cond(i,j,1)=fluids(1)%lambda
                      end if
                   end do
                end do
             else
                do k=sz-1,ez+1
                   do j=sy-1,ey+1
                      do i=sx-1,ex+1
                         if (cond(i,j,k)>d1p2) then
                            cond(i,j,k)=fluids(2)%lambda
                         else
                            cond(i,j,k)=fluids(1)%lambda
                         end if
                      end do
                   end do
                end do
             end if
             
             if (dim==2) then
                do j=sy-1,ey+1
                   do i=sx-1,ex+1
                      !-------------------------------------------------------------------------------
                      ! x-direction
                      !-------------------------------------------------------------------------------
                      dl=dxu(i)
                      !-------------------------------------------------------------------------------
                      ! intersect with cou=1/2
                      !-------------------------------------------------------------------------------
                      dl1=(d1p2-cou(i-1,j,1))*dl/(cou(i,j,1)-cou(i-1,j,1)+1d-40)
                      !dl1=-(VOF_init_case%thick-grid_x(i)) ! exact
                      !-------------------------------------------------------------------------------
                      if (dl1<0) then
                         dl1=0
                         dl2=dl
                      else if (dl1>dl) then
                         dl1=dl
                         dl2=0
                      else
                         dl2=dl-dl1
                      end if
                      !-------------------------------------------------------------------------------
                      ! relative distance
                      !-------------------------------------------------------------------------------
                      dl1=dl1/dl
                      dl2=dl2/dl
                      !-------------------------------------------------------------------------------
                      ! effective face conductivity
                      !-------------------------------------------------------------------------------
                      condu(i,j,1)=cond(i-1,j,1)*cond(i,j,1)/ &
                           & ((1-dl1)*cond(i,j,1)+dl1*cond(i-1,j,1))
                      !-------------------------------------------------------------------------------
                      
                      !-------------------------------------------------------------------------------
                      ! y-direction
                      !-------------------------------------------------------------------------------
                      dl=dyv(j)
                      !-------------------------------------------------------------------------------
                      ! intersect with cou=1/2
                      !-------------------------------------------------------------------------------
                      dl1=(d1p2-cou(i,j-1,1))*dl/(cou(i,j,1)-cou(i,j-1,1)+1d-40)
                      !-------------------------------------------------------------------------------
                      if (dl1<0) then
                         dl1=0
                         dl2=dl 
                      else if (dl1>dl) then
                         dl1=dl
                         dl2=0
                      else
                         dl2=dl-dl1
                      end if
                      !-------------------------------------------------------------------------------
                      ! relative distance
                      !-------------------------------------------------------------------------------
                      dl1=dl1/dl
                      dl2=dl2/dl
                      !-------------------------------------------------------------------------------
                      ! effective face conductivity
                      !-------------------------------------------------------------------------------
                      condv(i,j,1)=cond(i,j-1,1)*cond(i,j,1)/ &
                           & ((1-dl1)*cond(i,j,1)+dl1*cond(i,j-1,1))
                      !-------------------------------------------------------------------------------
                   end do
                end do
             else
                do k=sz-1,ez+1
                   do j=sy-1,ey+1
                      do i=sx-1,ex+1
                         !-------------------------------------------------------------------------------
                         ! x-direction
                         !-------------------------------------------------------------------------------
                         dl=dxu(i)
                         !-------------------------------------------------------------------------------
                         ! intersect with cou=1/2
                         !-------------------------------------------------------------------------------
                         dl1=(d1p2-cou(i-1,j,k))*dl/(cou(i,j,k)-cou(i-1,j,k)+1d-40)
                         !dl1=-(VOF_init_case%thick-grid_x(i)) ! exact
                         !-------------------------------------------------------------------------------
                         if (dl1<0) then
                            dl1=0
                            dl2=dl
                         else if (dl1>dl) then
                            dl1=dl
                            dl2=0
                         else
                            dl2=dl-dl1
                         end if
                         !-------------------------------------------------------------------------------
                         ! relative distance
                         !-------------------------------------------------------------------------------
                         dl1=dl1/dl
                         dl2=dl2/dl
                         !-------------------------------------------------------------------------------
                         ! effective face conductivity
                         !-------------------------------------------------------------------------------
                         condu(i,j,k)=cond(i-1,j,k)*cond(i,j,k)/ &
                              & ((1-dl1)*cond(i,j,k)+dl1*cond(i-1,j,k))
                         !-------------------------------------------------------------------------------
                         
                         !-------------------------------------------------------------------------------
                         ! y-direction
                         !-------------------------------------------------------------------------------
                         dl=dyv(j)
                         !-------------------------------------------------------------------------------
                         ! intersect with cou=1/2
                         !-------------------------------------------------------------------------------
                         dl1=(d1p2-cou(i,j-1,k))*dl/(cou(i,j,k)-cou(i,j-1,k)+1d-40)
                         !-------------------------------------------------------------------------------
                         if (dl1<0) then
                            dl1=0
                            dl2=dl
                         else if (dl1>dl) then
                            dl1=dl
                            dl2=0
                         else
                            dl2=dl-dl1
                         end if
                         !-------------------------------------------------------------------------------
                         ! relative distance
                         !-------------------------------------------------------------------------------
                         dl1=dl1/dl
                         dl2=dl2/dl
                         !-------------------------------------------------------------------------------
                         ! effective face conductivity
                         !-------------------------------------------------------------------------------
                         condv(i,j,k)=cond(i,j-1,k)*cond(i,j,k)/ &
                              & ((1-dl1)*cond(i,j,k)+dl1*cond(i,j-1,k))
                         !-------------------------------------------------------------------------------

                         !-------------------------------------------------------------------------------
                         ! z-direction
                         !-------------------------------------------------------------------------------
                         dl=dzw(k)
                         !-------------------------------------------------------------------------------
                         ! intersect with cou=1/2
                         !-------------------------------------------------------------------------------
                         dl1=(d1p2-cou(i,j,k-1))*dl/(cou(i,j,k)-cou(i,j,k-1)+1d-40)
                         !-------------------------------------------------------------------------------
                         if (dl1<0) then
                            dl1=0
                            dl2=dl
                         else if (dl1>dl) then
                            dl1=dl
                            dl2=0
                         else
                            dl2=dl-dl1
                         end if
                         !-------------------------------------------------------------------------------
                         ! relative distance
                         !-------------------------------------------------------------------------------
                         dl1=dl1/dl
                         dl2=dl2/dl
                         !-------------------------------------------------------------------------------
                         ! effective face conductivity
                         !-------------------------------------------------------------------------------
                         condw(i,j,k)=cond(i,j,k-1)*cond(i,j,k)/ &
                              & ((1-dl1)*cond(i,j,k)+dl1*cond(i,j,k-1))
                         !-------------------------------------------------------------------------------
                      end do
                   end do
                end do
             end if
             deallocate(cond)
          case(3)
             !-------------------------------------------------------------------------------
             ! Average, Maes and Soulaine, JCP 2020
             !-------------------------------------------------------------------------------

             !-------------------------------------------------------------------------------
          end select
          !-------------------------------------------------------------------------------
       endif

    end if

    !-------------------------------------------------------------------------------
  end subroutine Energy_Thermophysics
  !*****************************************************************************************************

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
END module mod_energy_thermophy
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
MODULE mod_Init_Heat
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  use mod_Parameters, only: EN_source_term,EN_linear_term,dt,Euler,&
       & gsx,gex,gsy,gey,gsz,gez,EN_pen_BC
  use mod_energy_thermophy
  use mod_struct_thermophysics
  use mod_struct_solver

  type boundary_value
     integer               :: type 
     real(8)               :: tpin=0
     real(8)               :: r=0,amp=0
     real(8), dimension(3) :: xyz=0
     real(8), dimension(3) :: dxyz2=0
  end type boundary_value

  type faces
     type(boundary_value) :: left,right,bottom,top,backward,forward
  end type faces

  type(faces) :: bc_energy

  namelist /nml_bc_heat/ bc_energy



contains

  !*****************************************************************************************************
  subroutine Init_Energy(tp,tp0,tp1,rho,condu,condv,condw,cp,sltp,smtp,cou,beta,tpb,fluids,energy)
    !***************************************************************************************************

    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable :: tp,tp0,tp1
    real(8), dimension(:,:,:), allocatable :: rho,condu,condv,condw,cp
    real(8), dimension(:,:,:), allocatable :: sltp,smtp
    real(8), dimension(:,:,:), allocatable :: cou,beta,tpb
    type(phase_t), dimension(:), allocatable :: fluids
    type(solver_sca_t)                    :: energy
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Initialization of Energy equation structure
    !-------------------------------------------------------------------------------
    energy%dt=dt
    allocate(energy%output%it_solver(1),&
         & energy%output%res_solver(1),&
         & energy%output%div(1))
    energy%output%it_solver=0
    energy%output%res_solver=0
    !-------------------------------------------------------------------------------
    ! Initialization of temperature field
    !-------------------------------------------------------------------------------
    tp=0
    call random_number(tp)
    tp=0
    tp0=tp
    if (abs(Euler)==2) tp1=tp0
    !-------------------------------------------------------------------------------
    ! Initialization of molecular fluid characteristics 
    !-------------------------------------------------------------------------------
    if (FT_activate) then
       write(*,*) "not done with energy equation"
       stop
    else
       call Energy_Thermophysics(rho,condu,condv,condw,cp,cou,beta,tpb,fluids)
    end if
    !-------------------------------------------------------------------------------
    ! Initialization of specific physical terms
    !-------------------------------------------------------------------------------
    if (EN_linear_term) sltp=0
    if (EN_source_term) smtp=0
    !-------------------------------------------------------------------------------
 end subroutine Init_Energy
  !*****************************************************************************************************

 
 subroutine read_BC_EN
   use mod_struct_solver
   implicit none
   !-------------------------------------------------------------------------------
   ! Global variables
   !-------------------------------------------------------------------------------
   !-------------------------------------------------------------------------------
   ! Local variables
   !-------------------------------------------------------------------------------
   !-------------------------------------------------------------------------------
   
   open(10,file="data.in",action="read")
   read(10,nml=nml_bc_heat) ; rewind(10)
   close(10)

 end subroutine read_BC_EN

 !*****************************************************************************************************
  subroutine Init_BC_Energy(tpin,pentp,energy) 
    !***************************************************************************************************
    use mod_Parameters, only: Periodic,nx,ny,nz,sx,ex,sy,ey,sz,ez,dx,dy,dz,&
         & xmin,ymin,zmin,xmax,ymax,zmax,grid_x,grid_y,grid_z,grid_xu,grid_yv,grid_zw
    use mod_Constants, only: d1p2
    use mod_struct_solver
    use mod_mpi

    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable   :: tpin
    real(8), dimension(:,:,:,:), allocatable :: pentp
    type(solver_sca_t)                      :: energy
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                  :: i,j,k
    real(8)                                  :: half_pen,full_pen
    real(8)                                  :: x,y,z,r,rr
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! penalisation values
    !-------------------------------------------------------------------------------
    full_pen=EN_pen_BC
    half_pen=d1p2*EN_pen_BC
    !-------------------------------------------------------------------------------
    
    !-------------------------------------------------------------------------------
    ! left, right, top, bottom, backward, forward = 
    !    0: Dirichlet
    !    1: Neumann
    !    2: not used
    !    3: not used
    !    4: Periodic
    !-------------------------------------------------------------------------------
    open(10,file="data.in",action="read")
    read(10,nml=nml_bc_heat) ; rewind(10)
    close(10)
    !-------------------------------------------------------------------------------
    ! check and force periodicity
    !-------------------------------------------------------------------------------
    if (Periodic(1)) then
       bc_energy%left%type=4
       bc_energy%right%type=4
    end if
    if (Periodic(2)) then
       bc_energy%bottom%type=4
       bc_energy%top%type=4
    end if
    if (Periodic(3)) then
       bc_energy%backward%type=4
       bc_energy%forward%type=4
    end if
    !-------------------------------------------------------------------------------

    energy%bound%left=bc_energy%left%type
    energy%bound%right=bc_energy%right%type
    energy%bound%bottom=bc_energy%bottom%type
    energy%bound%top=bc_energy%top%type
    energy%bound%backward=bc_energy%backward%type
    energy%bound%forward=bc_energy%forward%type
    !-------------------------------------------------------------------------------


    !-------------------------------------------------------------------------------
    ! Order of imposition of velocity boundary conditions
    !
    ! Periodic first
    ! Neumann
    ! Dirichlet
    !-------------------------------------------------------------------------------
    pentp=0
    tpin=0

    !-------------------------------------------------------------------------------
    ! periodic --> nothing to do
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------


    !-------------------------------------------------------------------------------
    ! neumann --> nothing to do
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! symetry --> neumann --> nothing to do
    !-------------------------------------------------------------------------------
    
    !-------------------------------------------------------------------------------
    ! dirichlet 0 or 3 
    !-------------------------------------------------------------------------------
    ! left 
    if (energy%bound%left==0.or.energy%bound%left==3) then
       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                if (i==gsx) then
                   pentp(i,j,k,1)=full_pen
                   tpin(i,j,k)=bc_energy%left%tpin
                end if
             end do
          end do
       end do
    end if
    ! right
    if (energy%bound%right==0.or.energy%bound%right==3) then
       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                if (i==gex) then
                   pentp(i,j,k,1)=full_pen
                   tpin(i,j,k)=bc_energy%right%tpin
                end if
             end do
          end do
       end do
    end if
    ! bottom
    if (energy%bound%bottom==0.or.energy%bound%bottom==3) then
       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                if (j==gsy) then
                   pentp(i,j,k,1)=full_pen
                   tpin(i,j,k)=bc_energy%bottom%tpin
                end if
             end do
          end do
       end do
    end if
    ! top
    if (energy%bound%top==0.or.energy%bound%top==3) then
       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                if (j==gey) then
                   pentp(i,j,k,1)=full_pen
                   tpin(i,j,k)=bc_energy%top%tpin
                end if
             end do
          end do
       end do
    end if
    if (dim==3) then
       ! backward
       if (energy%bound%backward==0.or.energy%bound%backward==3) then
          do k=sz,ez
             do j=sy,ey
                do i=sx,ex
                   if (k==gsz) then
                      pentp(i,j,k,1)=full_pen
                      tpin(i,j,k)=bc_energy%backward%tpin
                   end if
                end do
             end do
          end do
       end if
       ! forward
       if (energy%bound%forward==0.or.energy%bound%forward==3) then
          do k=sz,ez
             do j=sy,ey
                do i=sx,ex
                   if (k==gez) then
                      pentp(i,j,k,1)=full_pen
                      tpin(i,j,k)=bc_energy%forward%tpin
                   end if
                end do
             end do
          end do
       end if
    end if
    !-------------------------------------------------------------------------------
    ! end dirichlet
    !-------------------------------------------------------------------------------


    ! ******************************************************************************
    ! ******************************************************************************
    ! ******************************************************************************
    ! ******************************************************************************
    !                     specific boundary conditions
    ! ******************************************************************************
    ! ******************************************************************************
    ! ******************************************************************************
    ! ******************************************************************************



    !-------------------------------------------------------------------------------
    ! cylindrical inlet
    !-------------------------------------------------------------------------------
    ! imposed BC on an adiabatic face
    !-------------------------------------------------------------------------------
    ! left
    select case(abs(energy%bound%left))
    case(10,11,12,13)
       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                if (i==gsx) then
                   ! adiabatic / damp-proof
                   pentp(i,j,k,:)=0
                   rr=(grid_y(j)-bc_energy%left%xyz(2))**2 &
                        & +(grid_z(k)-bc_energy%left%xyz(3))**2
                   if ( rr <= bc_energy%left%r**2 ) then
                      ! imposed 
                      pentp(i,j,k,1)=full_pen
                      tpin(i,j,k)=bc_energy%left%tpin
                   end if
                end if
             end do
          end do
       end do
    end select
    ! right
    select case(abs(energy%bound%right))
    case(10,11,12,13)
       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                if (i==gex) then
                   ! adiabatic / damp-proof
                   pentp(i,j,k,:)=0
                   rr=(grid_y(j)-bc_energy%right%xyz(2))**2 &
                        & +(grid_z(k)-bc_energy%right%xyz(3))**2
                   if ( rr <= bc_energy%right%r**2 ) then
                      ! imposed
                      pentp(i,j,k,1)=full_pen
                      tpin(i,j,k)=bc_energy%right%tpin
                   end if
                end if
             end do
          end do
       end do
    end select
    ! bottom
    select case(abs(energy%bound%bottom))
    case(10,11,12,13)
       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                if (j==gsy) then
                   ! adiabatic / damp-proof
                   pentp(i,j,k,:)=0
                   rr=(grid_x(i)-bc_energy%bottom%xyz(1))**2 &
                        & +(grid_z(k)-bc_energy%bottom%xyz(3))**2
                   if ( rr <= bc_energy%bottom%r**2 ) then
                      ! imposed
                      pentp(i,j,k,1)=full_pen
                      tpin(i,j,k)=bc_energy%bottom%tpin
                   end if
                end if
             end do
          end do
       end do
    end select
    ! top
    select case(abs(energy%bound%top))
    case(10,11,12,13)
       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                if (j==gey) then
                   ! adiabatic / damp-proof
                   pentp(i,j,k,:)=0
                   rr=(grid_x(i)-bc_energy%top%xyz(1))**2 &
                        & +(grid_z(k)-bc_energy%top%xyz(3))**2
                   if ( rr <= bc_energy%top%r**2 ) then
                      ! imposed
                      pentp(i,j,k,1)=full_pen
                      tpin(i,j,k)=bc_energy%top%tpin
                   end if
                end if
             end do
          end do
       end do
    end select
    if (dim==3) then
       ! backward
       select case(abs(energy%bound%backward))
       case(10,11,12,13)
          do k=sz,ez
             do j=sy,ey
                do i=sx,ex
                   if (k==gsz) then
                      ! adiabatic / damp-proof
                      pentp(i,j,k,:)=0
                      rr=(grid_x(i)-bc_energy%backward%xyz(1))**2 &
                           & +(grid_y(j)-bc_energy%backward%xyz(2))**2
                      if ( rr <= bc_energy%backward%r**2 ) then
                         ! imposed
                         pentp(i,j,k,1)=full_pen
                         tpin(i,j,k)=bc_energy%backward%tpin
                      end if
                   end if
                end do
             end do
          end do
       end select
       ! forward
       select case(abs(energy%bound%forward))
       case(10,11,12,13)
          do k=sz,ez
             do j=sy,ey
                do i=sx,ex
                   if (k==gez) then
                      ! adiabatic / damp-proof
                      pentp(i,j,k,:)=0
                      rr=(grid_x(i)-bc_energy%forward%xyz(1))**2 &
                           & +(grid_y(j)-bc_energy%forward%xyz(2))**2
                      if ( rr <= bc_energy%forward%r**2 ) then
                         ! imposed
                         pentp(i,j,k,1)=full_pen
                         tpin(i,j,k)=bc_energy%forward%tpin
                      end if
                   end if
                end do
             end do
          end do
       end select
    end if
    !-------------------------------------------------------------------------------
    
    !-------------------------------------------------------------------------------
    ! rectangular inlet
    !-------------------------------------------------------------------------------
    ! imposed BC on an adiabatic face
    !-------------------------------------------------------------------------------
    ! left
    select case(abs(energy%bound%left))
    case(30)
       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                if (i==gsx) then
                   ! adiabatic / damp-proof
                   pentp(i,j,k,:)=0
                   if (   abs(grid_y(j)-bc_energy%left%xyz(2))<=bc_energy%left%dxyz2(2) .and. &
                        & abs(grid_z(k)-bc_energy%left%xyz(3))<=bc_energy%left%dxyz2(3)) then
                      ! imposed 
                      pentp(i,j,k,1)=full_pen
                      tpin(i,j,k)=bc_energy%left%tpin
                   end if
                end if
             end do
          end do
       end do
    end select
    ! right
    select case(abs(energy%bound%right))
    case(30)
       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                if (i==gex) then
                   ! adiabatic / damp-proof
                   pentp(i,j,k,:)=0
                   if (   abs(grid_y(j)-bc_energy%right%xyz(2))<=bc_energy%right%dxyz2(2) .and. &
                        & abs(grid_z(k)-bc_energy%right%xyz(3))<=bc_energy%right%dxyz2(3)) then
                      ! imposed
                      pentp(i,j,k,1)=full_pen
                      tpin(i,j,k)=bc_energy%right%tpin
                   end if
                end if
             end do
          end do
       end do
    end select
    ! bottom
    select case(abs(energy%bound%bottom))
    case(30)
       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                if (j==gsy) then
                   ! adiabatic / damp-proof
                   pentp(i,j,k,:)=0
                   if (   abs(grid_x(i)-bc_energy%bottom%xyz(1))<=bc_energy%bottom%dxyz2(1) .and. &
                        & abs(grid_z(k)-bc_energy%bottom%xyz(3))<=bc_energy%bottom%dxyz2(3)) then
                      ! imposed
                      pentp(i,j,k,1)=full_pen
                      tpin(i,j,k)=bc_energy%bottom%tpin
                   end if
                end if
             end do
          end do
       end do
    end select
    ! top
    select case(abs(energy%bound%top))
    case(30)
       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                if (j==gey) then
                   ! adiabatic / damp-proof
                   pentp(i,j,k,:)=0
                   if (   abs(grid_x(i)-bc_energy%top%xyz(1))<=bc_energy%top%dxyz2(1) .and. &
                        & abs(grid_z(k)-bc_energy%top%xyz(3))<=bc_energy%top%dxyz2(3)) then
                      ! imposed
                      pentp(i,j,k,1)=full_pen
                      tpin(i,j,k)=bc_energy%top%tpin
                   end if
                end if
             end do
          end do
       end do
    end select
    if (dim==3) then
       ! backward
       select case(abs(energy%bound%backward))
       case(30)
          do k=sz,ez
             do j=sy,ey
                do i=sx,ex
                   if (k==gsz) then
                      ! adiabatic / damp-proof
                      pentp(i,j,k,:)=0
                      if (   abs(grid_x(i)-bc_energy%backward%xyz(1))<=bc_energy%backward%dxyz2(1) .and. &
                           & abs(grid_y(j)-bc_energy%backward%xyz(2))<=bc_energy%backward%dxyz2(2)) then
                         ! imposed
                         pentp(i,j,k,1)=full_pen
                         tpin(i,j,k)=bc_energy%backward%tpin
                      end if
                   end if
                end do
             end do
          end do
       end select
       ! forward
       select case(abs(energy%bound%forward))
       case(30)
          do k=sz,ez
             do j=sy,ey
                do i=sx,ex
                   if (k==gez) then
                      ! adiabatic / damp-proof
                      pentp(i,j,k,:)=0
                      if (   abs(grid_x(i)-bc_energy%forward%xyz(1))<=bc_energy%forward%dxyz2(1) .and. &
                           & abs(grid_y(j)-bc_energy%forward%xyz(2))<=bc_energy%forward%dxyz2(2)) then
                         ! imposed
                         pentp(i,j,k,1)=full_pen
                         tpin(i,j,k)=bc_energy%forward%tpin
                      end if
                   end if
                end do
             end do
          end do
       end select
    end if
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! check
    !-------------------------------------------------------------------------------
    i=0
    if (energy%bound%left==4.and.energy%bound%right/=4)       i=1
    if (energy%bound%left/=4.and.energy%bound%right==4)       i=1
    if (energy%bound%bottom==4.and.energy%bound%top/=4)       i=1
    if (energy%bound%bottom/=4.and.energy%bound%top==4)       i=1
    if (energy%bound%backward==4.and.energy%bound%forward/=4) i=1
    if (energy%bound%backward/=4.and.energy%bound%forward==4) i=1
    if (i==1) then
       write(*,*) "PERIODICITY ERROR, CHECK BCS, STOP"
       STOP
    end if

    i=0
    j=energy%bound%left           &
         & *energy%bound%right    &
         & *energy%bound%bottom   &
         & *energy%bound%top
    if (dim==3) then
       j=j*energy%bound%backward  &
            & *energy%bound%forward
    end if
    if (j==1.or.j==2) i=1
    if (i==1.and.rank==0) then
       write(*,*) "WARNING, ALL BCS ARE NEUMANN LIKE"
    end if
    !-------------------------------------------------------------------------------
    

    
  end subroutine Init_BC_Energy
  !*****************************************************************************************************


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
END MODULE mod_Init_Heat
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
