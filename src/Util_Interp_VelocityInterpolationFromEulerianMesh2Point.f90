!===============================================================================
module Bib_VOFLag_VelocityInterpolationFromEulerianMesh2Point
   !===============================================================================
   contains
   !-----------------------------------------------------------------------------  
   !> @author amine chadil, jorge cesar brandle de motta
   ! 
   !> @brief   interpolation de la vitesse en un point fictif
   !
   !> @param [in]  u,v,w      : les vitesses dans le domaine 
   !> @param [in]  point      : le point d'interpolation de la vitesse
   !> @param [out] vts_point  : la vitesse interpollee
   !-----------------------------------------------------------------------------
   subroutine VelocityInterpolationFromEulerianMesh2Point(point,u,v,w,vts_point,ndim,grid_x,grid_y,&
                                   grid_z,grid_xu,grid_yv,grid_zw,dx,dy,dz,dxu,dyv,dzw)
      !===============================================================================
      !modules
      !===============================================================================
      !-------------------------------------------------------------------------------
      !Modules de definition des structures et variables => src/mod/module_...f90
      !-------------------------------------------------------------------------------
      use mod_Parameters, only : deeptracking,regular_mesh
      !-------------------------------------------------------------------------------
      !Modules de subroutines de la librairie RESPECT => src/Bib_VOFLag_...f90
      !-------------------------------------------------------------------------------
      use Bib_VOFLag_LinearInterpolation
      use Bib_VOFLag_NearestNode
      !-------------------------------------------------------------------------------

      !===============================================================================
      !declarations des variables
      !===============================================================================
      implicit none
      !-------------------------------------------------------------------------------
      !variables globales
      !-------------------------------------------------------------------------------
      real(8), dimension(:,:,:), allocatable, intent(in)  :: u,v,w
      real(8), dimension(3),                  intent(out) :: vts_point
      real(8), dimension(3),                  intent(in)  :: point
      integer,				      intent(in)  :: ndim
      real(8), dimension(:),     allocatable, intent(in)  :: grid_xu,grid_yv, grid_zw
      real(8), dimension(:),     allocatable, intent(in)  :: grid_x,grid_y, grid_z
      real(8), dimension(:),     allocatable, intent(in)  :: dx,dy,dz
      real(8), dimension(:),     allocatable, intent(in)  :: dxu,dyv,dzw
      
      !-------------------------------------------------------------------------------
      !variables locales 
      !-------------------------------------------------------------------------------
      real(8), dimension(4*ndim-4)                         :: x,y,z,val
      integer, dimension(4*ndim-4,3)                       :: points_iterpolation
      integer, dimension(3,3)                             :: ijk
      integer                                             :: nd,i
      !-------------------------------------------------------------------------------

      !-------------------------------------------------------------------------------
      if (deeptracking) write(*,*) 'entree VelocityInterpolationFromEulerianMesh2Point'
      !-------------------------------------------------------------------------------
      
      !-------------------------------------------------------------------------------
      ! Trouver le noeud de vitesse le plus proche du point
      !-------------------------------------------------------------------------------
      do nd=1,ndim
         call NearestNode(grid_x,grid_y,grid_z,grid_xu,grid_yv,grid_zw,dx,dy,dz,dxu, &
                         dyv,dzw,point,nd,.true.,regular_mesh,ndim,ijk(nd,:))
      end do
      !-------------------------------------------------------------------------------
      ! faire en sorte que le noeud gauche bas arriere soit toujours le premier
      !-------------------------------------------------------------------------------
      ! maillage de la premiere composante de la vitesse
      if (point(1).lt.grid_xu(ijk(1,1)))             ijk(1,1)=ijk(1,1)-1
      if (point(2).lt.grid_y (ijk(1,2)))             ijk(1,2)=ijk(1,2)-1
      if (ndim==3 .and. point(3).lt.grid_z(ijk(1,3))) ijk(1,3)=ijk(1,3)-1
      ! maillage de la deuxieme composante de la vitesse
      if (point(1).lt.grid_x (ijk(2,1)))             ijk(2,1)=ijk(2,1)-1
      if (point(2).lt.grid_yv(ijk(2,2)))             ijk(2,2)=ijk(2,2)-1
      if (ndim==3 .and. point(3).lt.grid_z(ijk(2,3))) ijk(2,3)=ijk(2,3)-1

      if (ndim==3) then
         ! maillage de la troisieme composante de la vitesse
         if (point(1).lt.grid_xu(ijk(3,1))) ijk(3,1)=ijk(3,1)-1
         if (point(2).lt.grid_y (ijk(3,2))) ijk(3,2)=ijk(3,2)-1
         if (point(3).lt.grid_z (ijk(3,3))) ijk(3,3)=ijk(3,3)-1
      end if

      do nd =  1,ndim  ! direction sur laquelle on va interpoler
         !-------------------------------------------------------------------------------
         ! Construire les points d'interpolation
         !-------------------------------------------------------------------------------
         do i=1,4*ndim-4
            points_iterpolation(i,:) = ijk(nd,:)
         end do
         points_iterpolation(2,1) = ijk(nd,1)+1
         points_iterpolation(3,1) = ijk(nd,1)+1 
         points_iterpolation(3,2) = ijk(nd,2)+1
         points_iterpolation(4,2) = ijk(nd,2)+1
         if (ndim==3) then
            points_iterpolation(5:8,3) = ijk(nd,3)+1        
            points_iterpolation(6,1)   = points_iterpolation(2,1)
            points_iterpolation(7,1)   = points_iterpolation(3,1)
            points_iterpolation(7,2)   = points_iterpolation(3,2)
            points_iterpolation(8,2)   = points_iterpolation(4,2)
         end if

         !-------------------------------------------------------------------------------
         ! Preparation de l'interpolation
         !-------------------------------------------------------------------------------
         x=0.;y=0.;z=0.;val=0.
         do i=1,4*ndim-4
            x(i) = grid_x(points_iterpolation(i,1)) 
            if (nd==1) then
               x(i)   = grid_xu(points_iterpolation(i,1)) 
               val(i) = u(points_iterpolation(i,1),points_iterpolation(i,2),points_iterpolation(i,3))
            end if
            y(i) = grid_y(points_iterpolation(i,2)) 
            if (nd==2) then
               y(i)   = grid_yv(points_iterpolation(i,2)) 
               val(i) = v(points_iterpolation(i,1),points_iterpolation(i,2),points_iterpolation(i,3))
            end if
            if (ndim==3) then
               z(i) = grid_z(points_iterpolation(i,3)) 
               if (nd==3) then
                  z(i)   = grid_zw(points_iterpolation(i,3)) 
                  val(i) = w(points_iterpolation(i,1),points_iterpolation(i,2),points_iterpolation(i,3))
               end if
            end if
         end do
         
         !-------------------------------------------------------------------------------
         ! Interpolation de la vitesse
         !-------------------------------------------------------------------------------
	 call LinearInterpolation(point,x,y,z,val,ndim,vts_point(nd))

      end do
      
      !-------------------------------------------------------------------------------
      if (deeptracking) write(*,*) 'sortie VelocityInterpolationFromEulerianMesh2Point'
      !-------------------------------------------------------------------------------
      !print*, 'vts_point=', vts_point
      !print*, grid_x
      !print*, grid_y
      !print*, grid_z
      !print*, grid_xu
      !print*, grid_yv
      !print*, grid_zw

   end subroutine VelocityInterpolationFromEulerianMesh2Point

   !===============================================================================
end module Bib_VOFLag_VelocityInterpolationFromEulerianMesh2Point
!===============================================================================
