!===============================================================================
module Bib_VOFLag_Ellipsoid_ParticlesDataStructure_Objet_Initialize
  !===============================================================================
  contains
  !---------------------------------------------------------------------------  
  !> @author mohamed-amine chadil
  ! 
  !> @brief cette routine remplie la structure vl%par avec les caracteristiques des
  !! ellipsoides lues dans le fichier "**.don".
  !
  !> @param[out] vl   : la structure contenant toutes les informations
  !! relatives aux particules      
  !---------------------------------------------------------------------------  
  subroutine Ellipsoid_ParticlesDataStructure_Objet_Initialize(vl) !initialiser_struct_objet_ellipsoid
    !===============================================================================
    !modules
    !===============================================================================
    !-------------------------------------------------------------------------------
    !Modules de definition des structures et variables => src/mod/module_...f90
    !-------------------------------------------------------------------------------
    !use module_lecture_fichier, only : fichierdon,repdon,l80,ligne,n1,n2,finlig
    !use Module_VOFLag_ParticlesDataStructure
    !use module_thermophysique,  only : phases
    !use module_commun_gui,      only : is_debug
    !use module_grille,          only : ndim
    !use module_unites,          only : idon
    !use module_bcast
    !use module_mpif,            only : mpi_integer,mpi_double_precision,mpi_character
    !use module_mpi,             only : rang,nb_proc,comut,code
    
    use Module_VOFLag_ParticlesDataStructure
    use mod_Parameters,                      only : rank,nproc,dim,deeptracking
    use mod_struct_thermophysics,            only : fluids
    use mod_mpi
    
    !-------------------------------------------------------------------------------
    !Modules de subroutines de la librairie RESPECT => src/Bib_VOFLag_...f90
    !-------------------------------------------------------------------------------
    use Bib_VOFLag_ParticlesDataStructure_Object_allocate
    !-------------------------------------------------------------------------------

    !===============================================================================
    !declarations des variables
    !===============================================================================
    implicit none
    !-------------------------------------------------------------------------------
    !variables globales
    !-------------------------------------------------------------------------------
    type(struct_vof_lag), intent(inout)  :: vl
    !-------------------------------------------------------------------------------
    !variables locales 
    !-------------------------------------------------------------------------------
    character(len=80)                    :: motclef
    integer                              :: err,np,nd
    double precision, dimension(3)       :: vecteur_direction
    double precision                     :: angle,n,pi = acos(-1.d0)
    
    !integer                                :: end2,err,ltes,l,i,kpt,np,nd
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'entree Ellipsoid_ParticlesDataStructure_Objet_Initialize'
    !-------------------------------------------------------------------------------

    if_proc_0: if (rank.eq.0) then
    !-------------------------------------------------------------------------------
    !ouverture et lecture du fichier particles.in
    !-------------------------------------------------------------------------------
    open(unit=1001,file="particles.in",status="old",action="read",iostat=err)
    !-------------------------------------------------------------------------------
    !lire le nombre de spheres(kpt) et allocation de vl 
    !-------------------------------------------------------------------------------
    call ParticlesDataStructure_Object_allocate(vl)
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! lecture des caracteristiques des ellipsoids
    !-------------------------------------------------------------------------------
    do np=1,vl%kpt
    	if (vl%postforce .or. vl%postHeatFlux) then 
    		if (dim==2) then
                	read(1001,*)  vl%objet(np)%pos(1),vl%objet(np)%pos(2),&
                	vl%objet(np)%sca(1),vl%objet(np)%sca(2),vl%objet(np)%orientation(1),motclef
                else
                	read(1001,*)  vl%objet(np)%pos(1),vl%objet(np)%pos(2),&
                	vl%objet(np)%pos(3),vl%objet(np)%sca(1),vl%objet(np)%sca(2),vl%objet(np)%sca(3),&
                	angle,vecteur_direction(1),vecteur_direction(2),vecteur_direction(3),motclef
               		n = sqrt(vecteur_direction(1)**2 + vecteur_direction(2)**2 + vecteur_direction(3)**2 )
              		vl%objet(np)%orientation(1)   = cos(angle/2)
             		vl%objet(np)%orientation(2:4) = sin(angle/2) * vecteur_direction(:)/(n+1D-40) 
           	end if
                vl%objet(np)%ObjectFile='objets/'//motclef
        else
        	if (dim==2) then
        		read(1001,*)  vl%objet(np)%pos(1),vl%objet(np)%pos(2),&
         		vl%objet(np)%sca(1),vl%objet(np)%sca(2),vl%objet(np)%orientation(1)
         	else
                	read(1001,*)  vl%objet(np)%pos(1),vl%objet(np)%pos(2),&
                	vl%objet(np)%pos(3),vl%objet(np)%sca(1),vl%objet(np)%sca(2),vl%objet(np)%sca(3),&
                	angle,vecteur_direction(1),vecteur_direction(2),vecteur_direction(3)
               	 	n = sqrt(vecteur_direction(1)**2 + vecteur_direction(2)**2 + vecteur_direction(3)**2 )
               		vl%objet(np)%orientation(1)   = cos(angle/2)
              	 	vl%objet(np)%orientation(2:4) = sin(angle/2) * vecteur_direction(:)/(n+1D-40)  
            	end if
     	end if
  	end do
       	close(1001)

       do np = 1,vl%kpt 
          !-------------------------------------------------------------------------------
          !masse volumique de la objet
          !-------------------------------------------------------------------------------
          vl%objet(np)%rho=fluids(2)%rho !une seule espece pour l'instant
          if (dim .eq. 2) then
             vl%objet(np)%masse = pi*vl%objet(np)%sca(1)*vl%objet(np)%sca(2) * vl%objet(np)%rho
          else
             vl%objet(np)%masse = (4.0d0/3.0d0)*pi*vl%objet(np)%sca(1)*vl%objet(np)%sca(2)*&
                  vl%objet(np)%sca(3) * vl%objet(np)%rho
          end if
       end do

    end if if_proc_0

    !-------------------------------------------------------------------------------
    ! Communiquer les infos aux autres procs
    !-------------------------------------------------------------------------------
    if (nproc .gt. 1) then
       if (rank .ne. 0)  call ParticlesDataStructure_Object_allocate(vl)
       do np=1,vl%kpt
          if (dim==2) then
             do nd=1,dim
                call mpi_bcast(vl%objet(np)%pos(nd),1,mpi_double_precision,0,comm3d,code)
                call mpi_bcast(vl%objet(np)%sca(nd),1,mpi_double_precision,0,comm3d,code)
             end do
             call mpi_bcast(vl%objet(np)%orientation(1),1,mpi_double_precision,0,comm3d,code)
          end if
          if (dim==3) then
             do nd=1,dim
                call mpi_bcast(vl%objet(np)%pos(nd),1,mpi_double_precision,0,comm3d,code)
                call mpi_bcast(vl%objet(np)%sca(nd),1,mpi_double_precision,0,comm3d,code)
                call mpi_bcast(vl%objet(np)%orientation(nd),1,mpi_double_precision,0,comm3d,code)
             end do
             call mpi_bcast(vl%objet(np)%orientation(4),1,mpi_double_precision,0,comm3d,code)
          end if
          call mpi_bcast(vl%objet(np)%rho,1,mpi_double_precision,0,comm3d,code)
          call mpi_bcast(vl%objet(np)%masse,1,mpi_double_precision,0,comm3d,code)
          if (vl%postforce .or. vl%postHeatFlux) call mpi_bcast(vl%objet(np)%ObjectFile,len(vl%objet(np)%ObjectFile),mpi_character,0,comm3d,code)
       enddo
    end if 
    
    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'sortie Ellipsoid_ParticlesDataStructure_Objet_Initialize'
    !-------------------------------------------------------------------------------
  end subroutine Ellipsoid_ParticlesDataStructure_Objet_Initialize

  !===============================================================================
end module Bib_VOFLag_Ellipsoid_ParticlesDataStructure_Objet_Initialize
!===============================================================================
  


