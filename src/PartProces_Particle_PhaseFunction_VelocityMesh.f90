!===============================================================================
module Bib_VOFLag_Particle_PhaseFunction_VelocityMesh
  !===============================================================================
  contains
  !-----------------------------------------------------------------------------  
  !> @author amine chadil
  ! 
  !
  !
  !
  !     
  !> 
  !
  !> 
  !>
  !!
  !>
  !-----------------------------------------------------------------------------
  subroutine Particle_PhaseFunction_VelocityMesh(cou_vts,cou_vtsin,grid_x,grid_y,    &
                                                 grid_z,grid_xu,grid_yv,grid_zw,ndim,&
                                                 tot_pt_fict,vl)
    !===============================================================================
    !modules
    !===============================================================================
    !-------------------------------------------------------------------------------
    !Modules de definition des structures et variables => src/mod/module_...f90
    !-------------------------------------------------------------------------------
    use Module_VOFLag_ParticlesDataStructure
    use mod_Parameters,                       only : rank,nproc,deeptracking,sxu,exu,&
                                                     syu,eyu,szu,ezu,sxv,exv,syv,eyv,&
                                                     szv,ezv,sxw,exw,syw,eyw,szw,ezw
    use mod_mpi
    !-------------------------------------------------------------------------------
    !Modules de subroutines de la librairie RESPECT => src/Bib_VOFLag_...f90
    !-------------------------------------------------------------------------------
    use Bib_VOFLag_Particle_PointIn
    !-------------------------------------------------------------------------------
    !===============================================================================
    !declarations des variables
    !===============================================================================
    implicit none
    !-------------------------------------------------------------------------------
    !variables globales
    !-------------------------------------------------------------------------------
    type(struct_vof_lag)                    , intent(inout) :: vl
    real(8), dimension(:,:,:,:), allocatable, intent(inout) :: cou_vts,cou_vtsin
    real(8), dimension(:)      , allocatable, intent(in)    :: grid_x,grid_y,grid_z
    real(8), dimension(:)      , allocatable, intent(in)    :: grid_xu,grid_yv,grid_zw
    integer,                                  intent(in)    :: ndim,tot_pt_fict
    !-------------------------------------------------------------------------------
    !variables locales 
    !-------------------------------------------------------------------------------
    real(8), dimension(3)                                 :: ne_av,nw_av,se_av,sw_av,  &
                                                           & ne_der,nw_der,se_der,     &
                                                           & sw_der,pt_fictif
    real(8)                                               :: xxg,xxd,zzb,zzh,yyd,yyf
    integer                                               :: np,nbt,nd,lmp,            &
                                                           & total_pt_fictifs,         &
                                                           & nb_pt_fictifs_in_particle,&
                                                           & ii,ik,ij,ltp,k,i,j
    logical                                               :: ne_av_in,nw_av_in,        &
                                                           & se_av_in,sw_av_in,        &
                                                           & ne_der_in,nw_der_in,      &
                                                           & se_der_in,sw_der_in,      &
                                                           & pt_fictif_in
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'entree Particle_PhaseFunction_VelocityMesh'
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    !initialisation
    !-------------------------------------------------------------------------------
    total_pt_fictifs=tot_pt_fict
    cou_vts=0d0
    if (allocated(cou_vtsin)) cou_vts=cou_vtsin
    ne_av=0d0;ne_der=0d0
    nw_av=0d0;nw_der=0d0
    se_av=0d0;se_der=0d0
    sw_av=0d0;sw_der=0d0
    
    pt_fictif=0.0d0
    !-------------------------------------------------------------------------------
    ! Traitement du cas 2D
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------
    !ne traiter que les particules (ou copies) qui intersecte le domaine du proc courant
    !-------------------------------------------------------------------------------
    do np=1,vl%kpt
     if (vl%objet(np)%on) then
      do nbt=1,vl%objet(np)%nbtimes
        if (vl%objet(np)%symperon(nbt)) then
         !-------------------------------------------------------------------------------
         !
         !-------------------------------------------------------------------------------
         if (ndim==2) then
            do j=syu,eyu
               do i=sxu,exu
                  !-------------------------------------------------------------------------------
                  !construction de la cellule centree en i,j
                  !-------------------------------------------------------------------------------
                  xxg=grid_x(i-1);xxd=grid_x(i)
                  zzb=grid_yv(j); zzh=grid_yv(j+1)
                  ne_av(1)=xxd;ne_av(2)=zzh;nw_av(1)=xxg;nw_av(2)=zzh
                  se_av(1)=xxd;se_av(2)=zzb;sw_av(1)=xxg;sw_av(2)=zzb
                  !-------------------------------------------------------------------------------
                  !tester chacun des coins de la cellule (in ou out de la particule)
                  !-------------------------------------------------------------------------------
                  call Particle_PointIn(vl,np,nbt,vl%objet(np)%sca,ne_av,ndim,ne_av_in)
                  call Particle_PointIn(vl,np,nbt,vl%objet(np)%sca,nw_av,ndim,nw_av_in) 
                  call Particle_PointIn(vl,np,nbt,vl%objet(np)%sca,se_av,ndim,se_av_in)
                  call Particle_PointIn(vl,np,nbt,vl%objet(np)%sca,sw_av,ndim,sw_av_in) 

                  !-------------------------------------------------------------------------------
                  !si tous les coins de la cellule sont a l'interieur de la particule
                  !-------------------------------------------------------------------------------
                  if (ne_av_in .and. nw_av_in .and. se_av_in .and. sw_av_in) then
                   cou_vts(i,j,1,1)=1.d0
                  endif

                  !-------------------------------------------------------------------------------
                  !si la cellule n'est pas entierement a l'interieur de la particule
                  !-------------------------------------------------------------------------------
                  if (cou_vts(i,j,1,1).lt.1.d0) then
                    if  ( ne_av_in .or. nw_av_in .or. se_av_in .or. sw_av_in ) then
                     !-------------------------------------------------------------------------------
                     !     version ibm
                     !-------------------------------------------------------------------------------
                     nb_pt_fictifs_in_particle=0
                     do ii=1,total_pt_fictifs
                      do ik=1,total_pt_fictifs
                        pt_fictif(1)=xxg+dfloat(ii-1)*(xxd-xxg)/(dfloat(total_pt_fictifs)+1D-40)+&
                                         0.5d0*(xxd-xxg)/(dfloat(total_pt_fictifs)+1D-40)
                        pt_fictif(2)=zzb+dfloat(ik-1)*(zzh-zzb)/(dfloat(total_pt_fictifs)+1D-40)+&
                                         0.5d0*(zzh-zzb)/(dfloat(total_pt_fictifs)+1D-40)
                        call Particle_PointIn(vl,np,nbt,vl%objet(np)%sca,pt_fictif,ndim,pt_fictif_in)
                        if (pt_fictif_in) nb_pt_fictifs_in_particle=nb_pt_fictifs_in_particle+1
                      enddo
                     enddo
                     cou_vts(i,j,1,1)=cou_vts(i,j,1,1)+dfloat(nb_pt_fictifs_in_particle)/(dfloat(total_pt_fictifs)**ndim+1D-40)
                     if (cou_vts(i,j,1,1) .gt. 1.d0) cou_vts(i,j,1,1)=1.d0
                   endif
                  end if 
               end do 
            end do 
            do j=syv,eyv
               do i=sxv,exv
                  !-------------------------------------------------------------------------------
                  !construction de la cellule centree en i,j
                  !-------------------------------------------------------------------------------
                  xxg=grid_xu(i) ;xxd=grid_xu(i+1)
                  zzb=grid_y(j-1);zzh=grid_y(j)
                  ne_av(1)=xxd;ne_av(2)=zzh;nw_av(1)=xxg;nw_av(2)=zzh
                  se_av(1)=xxd;se_av(2)=zzb;sw_av(1)=xxg;sw_av(2)=zzb
                  !-------------------------------------------------------------------------------
                  !tester chacun des coins de la cellule (in ou out de la particule)
                  !-------------------------------------------------------------------------------
                  call Particle_PointIn(vl,np,nbt,vl%objet(np)%sca,ne_av,ndim,ne_av_in)
                  call Particle_PointIn(vl,np,nbt,vl%objet(np)%sca,nw_av,ndim,nw_av_in) 
                  call Particle_PointIn(vl,np,nbt,vl%objet(np)%sca,se_av,ndim,se_av_in)
                  call Particle_PointIn(vl,np,nbt,vl%objet(np)%sca,sw_av,ndim,sw_av_in) 

                  !-------------------------------------------------------------------------------
                  !si tous les coins de la cellule sont a l'interieur de la particule
                  !-------------------------------------------------------------------------------
                  if (ne_av_in .and. nw_av_in .and. se_av_in .and. sw_av_in) then
                   cou_vts(i,j,1,2)=1.d0
                  endif

                  !-------------------------------------------------------------------------------
                  !si la cellule n'est pas entierement a l'interieur de la particule
                  !-------------------------------------------------------------------------------
                  if (cou_vts(i,j,1,2).lt.1.d0) then
                    if  ( ne_av_in .or. nw_av_in .or. se_av_in .or. sw_av_in ) then
                     !-------------------------------------------------------------------------------
                     !     version ibm
                     !-------------------------------------------------------------------------------
                     nb_pt_fictifs_in_particle=0
                     do ii=1,total_pt_fictifs
                      do ik=1,total_pt_fictifs
                        pt_fictif(1)=xxg+dfloat(ii-1)*(xxd-xxg)/(dfloat(total_pt_fictifs)+1D-40)+&
                                         0.5d0*(xxd-xxg)/(dfloat(total_pt_fictifs)+1D-40)
                        pt_fictif(2)=zzb+dfloat(ik-1)*(zzh-zzb)/(dfloat(total_pt_fictifs)+1D-40)+&
                                         0.5d0*(zzh-zzb)/(dfloat(total_pt_fictifs)+1D-40)
                        call Particle_PointIn(vl,np,nbt,vl%objet(np)%sca,pt_fictif,ndim,pt_fictif_in)
                        if (pt_fictif_in) nb_pt_fictifs_in_particle=nb_pt_fictifs_in_particle+1
                      enddo
                     enddo
                     cou_vts(i,j,1,2)=cou_vts(i,j,1,2)+dfloat(nb_pt_fictifs_in_particle)/(dfloat(total_pt_fictifs)**ndim+1D-40)
                     if (cou_vts(i,j,1,2) .gt. 1.d0) cou_vts(i,j,1,2)=1.d0
                   endif
                  end if 
               end do 
            end do 
                      
         else
            do k=szu,ezu+1
               do j=syu,eyu+1
                  do i=sxu,exu+1
                     !-------------------------------------------------
                     ! plane with z-normal vector
                     !-------------------------------------------------
                     xxg=grid_x(i-1);xxd=grid_x(i)
                     yyd=grid_yv(j) ;yyf=grid_yv(j+1)
                     zzb=grid_zw(k) ;zzh=grid_zw(k+1)

                     ne_av(1) =xxd;ne_av(2) =yyf;ne_av(3) =zzh;nw_av(1) =xxg;nw_av(2) =yyf;nw_av(3) =zzh 
                     se_av(1) =xxd;se_av(2) =yyd;se_av(3) =zzh;sw_av(1) =xxg;sw_av(2) =yyd;sw_av(3) =zzh 
                     ne_der(1)=xxd;ne_der(2)=yyf;ne_der(3)=zzb;nw_der(1)=xxg;nw_der(2)=yyf;nw_der(3)=zzb 
                     se_der(1)=xxd;se_der(2)=yyd;se_der(3)=zzb;sw_der(1)=xxg;sw_der(2)=yyd;sw_der(3)=zzb
                     !-------------------------------------------------------------------------------
                     !tester chacun des coins de la cellule (in ou out de la particule)
                     !-------------------------------------------------------------------------------
                     call Particle_PointIn(vl,np,nbt,vl%objet(np)%sca,ne_av,ndim,ne_av_in) 
                     call Particle_PointIn(vl,np,nbt,vl%objet(np)%sca,nw_av,ndim,nw_av_in) 
                     call Particle_PointIn(vl,np,nbt,vl%objet(np)%sca,se_av,ndim,se_av_in) 
                     call Particle_PointIn(vl,np,nbt,vl%objet(np)%sca,sw_av,ndim,sw_av_in) 
                     call Particle_PointIn(vl,np,nbt,vl%objet(np)%sca,ne_der,ndim,ne_der_in) 
                     call Particle_PointIn(vl,np,nbt,vl%objet(np)%sca,nw_der,ndim,nw_der_in) 
                     call Particle_PointIn(vl,np,nbt,vl%objet(np)%sca,se_der,ndim,se_der_in) 
                     call Particle_PointIn(vl,np,nbt,vl%objet(np)%sca,sw_der,ndim,sw_der_in) 
                     !-------------------------------------------------------------------------------
                     !si tous les coins de la cellule sont a l'interieur de la particule
                     !-------------------------------------------------------------------------------
                     if (ne_av_in.and.nw_av_in.and.se_av_in.and.sw_av_in.and.&
                                 ne_der_in.and.nw_der_in.and.se_der_in.and.sw_der_in) then
                        cou_vts(i,j,k,1)=1.d0
                     endif
                     !-------------------------------------------------------------------------------
                     !si la cellule n'est pas entierement a l'interieur de la particule
                     !-------------------------------------------------------------------------------
                     if (cou_vts(i,j,k,1).lt.1.d0) then
                      if  (ne_av_in.or.nw_av_in.or.se_av_in.or.sw_av_in.or.&
                                    ne_der_in.or.nw_der_in.or.se_der_in.or.sw_der_in) then
                        !-------------------------------------------------------------------------------
                        !      
                        !-------------------------------------------------------------------------------
                        nb_pt_fictifs_in_particle=0
                        do ii=1,total_pt_fictifs
                          do ij=1,total_pt_fictifs
                           do ik=1,total_pt_fictifs
                              pt_fictif(1)=xxg+dfloat(ii-1)*(xxd-xxg)/(dfloat(total_pt_fictifs)+1D-40)+&
                                           0.5d0*(xxd-xxg)/(dfloat(total_pt_fictifs)+1D-40)
                              pt_fictif(2)=yyd+dfloat(ij-1)*(yyf-yyd)/(dfloat(total_pt_fictifs)+1D-40)+&
                                           0.5d0*(yyf-yyd)/(dfloat(total_pt_fictifs)+1D-40)
                              pt_fictif(3)=zzb+dfloat(ik-1)*(zzh-zzb)/(dfloat(total_pt_fictifs)+1D-40)+&
                                          0.5d0*(zzh-zzb)/(dfloat(total_pt_fictifs)+1D-40)
                              call Particle_PointIn(vl,np,nbt,vl%objet(np)%sca,pt_fictif,ndim,pt_fictif_in)
                              if (pt_fictif_in) nb_pt_fictifs_in_particle=nb_pt_fictifs_in_particle+1
                           enddo
                          enddo
                        enddo
                        cou_vts(i,j,k,1)=cou_vts(i,j,k,1)+dfloat(nb_pt_fictifs_in_particle)/(dfloat(total_pt_fictifs)**(ndim)+1D-40)
                        if (cou_vts(i,j,k,1) .gt. 1.d0) cou_vts(i,j,k,1)=1.d0
                      endif
                     end if 
                  end do 
               end do 
            end do 
            do k=szv,ezv+1
               do j=syv,eyv+1
                  do i=sxv,exv+1
                     !-------------------------------------------------
                     ! 
                     !-------------------------------------------------
                     xxg=grid_xu(i) ;xxd=grid_xu(i+1)
                     yyd=grid_y(j-1);yyf=grid_y(j)
                     zzb=grid_zw(k) ;zzh=grid_zw(k+1)
                     ne_av(1) =xxd;ne_av(2) =yyf;ne_av(3) =zzh;nw_av(1) =xxg;nw_av(2) =yyf;nw_av(3) =zzh 
                     se_av(1) =xxd;se_av(2) =yyd;se_av(3) =zzh;sw_av(1) =xxg;sw_av(2) =yyd;sw_av(3) =zzh 
                     ne_der(1)=xxd;ne_der(2)=yyf;ne_der(3)=zzb;nw_der(1)=xxg;nw_der(2)=yyf;nw_der(3)=zzb 
                     se_der(1)=xxd;se_der(2)=yyd;se_der(3)=zzb;sw_der(1)=xxg;sw_der(2)=yyd;sw_der(3)=zzb
                     !-------------------------------------------------------------------------------
                     !tester chacun des coins de la cellule (in ou out de la particule)
                     !-------------------------------------------------------------------------------
                     call Particle_PointIn(vl,np,nbt,vl%objet(np)%sca,ne_av,ndim,ne_av_in) 
                     call Particle_PointIn(vl,np,nbt,vl%objet(np)%sca,nw_av,ndim,nw_av_in) 
                     call Particle_PointIn(vl,np,nbt,vl%objet(np)%sca,se_av,ndim,se_av_in) 
                     call Particle_PointIn(vl,np,nbt,vl%objet(np)%sca,sw_av,ndim,sw_av_in)
                     call Particle_PointIn(vl,np,nbt,vl%objet(np)%sca,ne_der,ndim,ne_der_in) 
                     call Particle_PointIn(vl,np,nbt,vl%objet(np)%sca,nw_der,ndim,nw_der_in) 
                     call Particle_PointIn(vl,np,nbt,vl%objet(np)%sca,se_der,ndim,se_der_in) 
                     call Particle_PointIn(vl,np,nbt,vl%objet(np)%sca,sw_der,ndim,sw_der_in) 
                     !-------------------------------------------------------------------------------
                     !si tous les coins de la cellule sont a l'interieur de la particule
                     !-------------------------------------------------------------------------------
                     if (ne_av_in.and.nw_av_in.and.se_av_in.and.sw_av_in.and.&
                                 ne_der_in.and.nw_der_in.and.se_der_in.and.sw_der_in) then
                        cou_vts(i,j,k,2)=1.d0
                     endif
                     !-------------------------------------------------------------------------------
                     !si la cellule n'est pas entierement a l'interieur de la particule
                     !-------------------------------------------------------------------------------
                     if (cou_vts(i,j,k,2).lt.1.d0) then
                      if  (ne_av_in.or.nw_av_in.or.se_av_in.or.sw_av_in.or.&
                                    ne_der_in.or.nw_der_in.or.se_der_in.or.sw_der_in) then
                        !-------------------------------------------------------------------------------
                        !      
                        !-------------------------------------------------------------------------------
                        nb_pt_fictifs_in_particle=0
                        do ii=1,total_pt_fictifs
                         do ij=1,total_pt_fictifs
                           do ik=1,total_pt_fictifs
                              pt_fictif(1)=xxg+dfloat(ii-1)*(xxd-xxg)/(dfloat(total_pt_fictifs)+1D-40)+&
                                           0.5d0*(xxd-xxg)/(dfloat(total_pt_fictifs)+1D-40)
                              pt_fictif(2)=yyd+dfloat(ij-1)*(yyf-yyd)/(dfloat(total_pt_fictifs)+1D-40)+&
                                           0.5d0*(yyf-yyd)/(dfloat(total_pt_fictifs)+1D-40)
                              pt_fictif(3)=zzb+dfloat(ik-1)*(zzh-zzb)/(dfloat(total_pt_fictifs)+1D-40)+&
                                          0.5d0*(zzh-zzb)/(dfloat(total_pt_fictifs)+1D-40)
                              call Particle_PointIn(vl,np,nbt,vl%objet(np)%sca,pt_fictif,ndim,pt_fictif_in)
                              if (pt_fictif_in) nb_pt_fictifs_in_particle=nb_pt_fictifs_in_particle+1
                           enddo
                          enddo
                        enddo
                        cou_vts(i,j,k,2)=cou_vts(i,j,k,2)+dfloat(nb_pt_fictifs_in_particle)/(dfloat(total_pt_fictifs)**(ndim)+1D-40)
                        if (cou_vts(i,j,k,2) .gt. 1.d0) cou_vts(i,j,k,2)=1.d0
                      endif
                     end if 
                  end do 
               end do 
            end do 
            do k=szw,ezw+1
               do j=syw,eyw+1
                  do i=sxw,exw+1
                     !-------------------------------------------------
                     ! 
                     !-------------------------------------------------
                     xxg=grid_xu(i) ;xxd=grid_xu(i+1)
                     yyd=grid_yv(j) ;yyf=grid_yv(j+1)
                     zzb=grid_z(k-1);zzh=grid_z(k)
                     ne_av(1) =xxd;ne_av(2) =yyf;ne_av(3) =zzh;nw_av(1) =xxg;nw_av(2) =yyf;nw_av(3) =zzh 
                     se_av(1) =xxd;se_av(2) =yyd;se_av(3) =zzh;sw_av(1) =xxg;sw_av(2) =yyd;sw_av(3) =zzh 
                     ne_der(1)=xxd;ne_der(2)=yyf;ne_der(3)=zzb;nw_der(1)=xxg;nw_der(2)=yyf;nw_der(3)=zzb 
                     se_der(1)=xxd;se_der(2)=yyd;se_der(3)=zzb;sw_der(1)=xxg;sw_der(2)=yyd;sw_der(3)=zzb
                     !-------------------------------------------------------------------------------
                     !tester chacun des coins de la cellule (in ou out de la particule)
                     !-------------------------------------------------------------------------------
                     call Particle_PointIn(vl,np,nbt,vl%objet(np)%sca,ne_av,ndim,ne_av_in) 
                     call Particle_PointIn(vl,np,nbt,vl%objet(np)%sca,nw_av,ndim,nw_av_in) 
                     call Particle_PointIn(vl,np,nbt,vl%objet(np)%sca,se_av,ndim,se_av_in) 
                     call Particle_PointIn(vl,np,nbt,vl%objet(np)%sca,sw_av,ndim,sw_av_in) 
                     call Particle_PointIn(vl,np,nbt,vl%objet(np)%sca,ne_der,ndim,ne_der_in) 
                     call Particle_PointIn(vl,np,nbt,vl%objet(np)%sca,nw_der,ndim,nw_der_in) 
                     call Particle_PointIn(vl,np,nbt,vl%objet(np)%sca,se_der,ndim,se_der_in) 
                     call Particle_PointIn(vl,np,nbt,vl%objet(np)%sca,sw_der,ndim,sw_der_in) 
                     !-------------------------------------------------------------------------------
                     !si tous les coins de la cellule sont a l'interieur de la particule
                     !-------------------------------------------------------------------------------
                     if (ne_av_in.and.nw_av_in.and.se_av_in.and.sw_av_in.and.&
                                 ne_der_in.and.nw_der_in.and.se_der_in.and.sw_der_in) then
                        cou_vts(i,j,k,3)=1.d0
                     endif
                     !-------------------------------------------------------------------------------
                     !si la cellule n'est pas entierement a l'interieur de la particule
                     !-------------------------------------------------------------------------------
                     if (cou_vts(i,j,k,3).lt.1.d0) then
                      if  (ne_av_in.or.nw_av_in.or.se_av_in.or.sw_av_in.or.&
                                    ne_der_in.or.nw_der_in.or.se_der_in.or.sw_der_in) then
                        !-------------------------------------------------------------------------------
                        !      
                        !-------------------------------------------------------------------------------
                        nb_pt_fictifs_in_particle=0
                        do ii=1,total_pt_fictifs
                         do ij=1,total_pt_fictifs
                           do ik=1,total_pt_fictifs
                              pt_fictif(1)=xxg+dfloat(ii-1)*(xxd-xxg)/(dfloat(total_pt_fictifs)+1D-40)+&
                                           0.5d0*(xxd-xxg)/(dfloat(total_pt_fictifs)+1D-40)
                              pt_fictif(2)=yyd+dfloat(ij-1)*(yyf-yyd)/(dfloat(total_pt_fictifs)+1D-40)+&
                                           0.5d0*(yyf-yyd)/(dfloat(total_pt_fictifs)+1D-40)
                              pt_fictif(3)=zzb+dfloat(ik-1)*(zzh-zzb)/(dfloat(total_pt_fictifs)+1D-40)+&
                                          0.5d0*(zzh-zzb)/(dfloat(total_pt_fictifs)+1D-40)
                              call Particle_PointIn(vl,np,nbt,vl%objet(np)%sca,pt_fictif,ndim,pt_fictif_in)
                              if (pt_fictif_in) nb_pt_fictifs_in_particle=nb_pt_fictifs_in_particle+1
                           enddo
                          enddo
                        enddo
                        cou_vts(i,j,k,3)=cou_vts(i,j,k,3)+dfloat(nb_pt_fictifs_in_particle)/(dfloat(total_pt_fictifs)**(ndim)+1D-40)
                        if (cou_vts(i,j,k,3) .gt. 1.d0) cou_vts(i,j,k,3)=1.d0
                      endif
                     end if 
                  end do 
               end do 
            end do 
         end if 
        end if 
      end do 
     end if 
    end do 

    !-------------------------------------------------------------------------------
    ! diffusion de cou a tous les procs 
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'sortie Particle_PhaseFunction_VelocityMesh'
    !-------------------------------------------------------------------------------
  end subroutine Particle_PhaseFunction_VelocityMesh

  !===============================================================================
end module Bib_VOFLag_Particle_PhaseFunction_VelocityMesh
!===============================================================================
  



