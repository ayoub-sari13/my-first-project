!========================================================================
!
!                   FUGU Version 1.0.0
!
!========================================================================
!
! Copyright  Stéphane Vincent
!
! stephane.vincent@u-pem.fr          straightparadigm@gmail.com
!
! This file is part of the FUGU Fortran library, a set of Computational 
! Fluid Dynamics subroutines devoted to the simulation of multi-scale
! multi-phase flows for resolved scale interfaces (liquid/gas/solid). 
!
!========================================================================
!**
!**   NAME       : type_struct_Lag.f90
!**
!**   AUTHOR     : Beno\^it Trouette
!**
!**   FUNCTION   : modules for Lagrangian tracking of particles of FUGU software
!**
!**   DATES      : Version 1.0.0  : from : jan 2021
!**
!========================================================================

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!===============================================================================
module mod_struct_Particle_Tracking
  
  use mod_Parameters

  type Particle
     integer               :: RK=2            ! choice of RK scheme order (1,2)
     integer               :: interpol=1      ! type of the Lagrangian velocity interpolation 
     integer, dimension(3) :: ijk             ! cell 
     real(8)               :: rho=1           ! density
     real(8)               :: mu=1            ! viscosity
     real(8)               :: m=1             ! mass
     real(8)               :: phi=1           ! scalar value (for exemple temperature)
     real(8)               :: time=0          ! associated time            
     real(8), dimension(2) :: radius=1        ! radius (step n and n-1)
     real(8), dimension(3) :: r=0             ! position 
     real(8), dimension(3) :: v=0             ! relative velocity
     real(8), dimension(3) :: u=0             ! absolute velocity
     real(8), dimension(3) :: u0=0            ! absolute velocity
     real(8), dimension(3) :: a=0             ! acceleration
     real(8), dimension(3) :: dl=0            ! associated (cubic) volume for Lagrangian transport
     logical               :: active=.true.   ! particle active or not
  end type Particle
  
  integer, dimension(3,2)                   :: bcs_case=1
  integer, dimension(3,2)                   :: bcs_case_sca=1

  type(Particle)                            :: LPartIn
  type(Particle), allocatable, dimension(:) :: LPart,SPart,VPart
  
  namelist /nml_prop_particles/ LPartIn
  
  !===============================================================================
end module mod_struct_Particle_Tracking
!===============================================================================
