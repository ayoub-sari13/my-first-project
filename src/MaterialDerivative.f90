
!========================================================================
!
!                   FUGU Version 1.0.0
!
!========================================================================
!
! Copyright  St�phane Vincent
!
! stephane.vincent@u-pem.fr          straightparadigm@gmail.com
!
! This file is part of the FUGU Fortran library, a set of Computational 
! Fluid Dynamics subroutines devoted to the simulation of multi-scale
! multi-phase flows for resolved scale interfaces (liquid/gas/solid). 
! FUGU uses the initial developments of DyJeAT and SHALA codes from 
! Jean-Luc Estivalezes (jean-luc.estivalezes@onera.fr) and 
! Fr�d�ric Couderc (couderc@math.univ-toulouse.fr).
!
!========================================================================
!**
!**   NAME       : Material_derivative3D.f90
!**
!**   AUTHOR     : St�phane Vincent
!**
!**   FUNCTION   : Modules of variables and initialization subroutines
!**
!**   DATES      : Version 1.0.0  : from : june, 16, 2015
!**
!========================================================================

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
MODULE Mod_material_derivative
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  use mod_Parameters
  use mod_interpolation

contains
  
  subroutine material_derivative(u,v,w,dudt,dvdt,dwdt)
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), allocatable, dimension(:,:,:), intent(in)    :: u,v,w
    real(8), allocatable, dimension(:,:,:), intent(inout) :: dudt,dvdt,dwdt
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                               :: i1,j1,i,j,k,l
    real(8), dimension(dim)                               :: vf
    real(8), dimension(3)                                 :: var
    real(8)                                               :: x,y,z
    real(8)                                               :: tmp=0
    !-------------------------------------------------------------------------------
    
    do k=sz,ez
       do j=sy,ey
          do i=sx,ex

             !-------------------------------------------------------------------
             ! x-dir interpolation for velocity vector
             !    at x-velocity nodes
             !-------------------------------------------------------------------
             x=grid_xu(i)
             y=grid_y(j)
             z=grid_z(k)
             call interpolation_uvw(tmp,u,v,w,(/x,y,z/),vf(1:dim),2,dim)
             !-------------------------------------------------------------------

             !-------------------------------------------------------------------
             ! gradients at x-velocity nodes
             !-------------------------------------------------------------------
             ! first order
             var(1)=(u(i+1,j,k)-u(i-1,j,k))/(dx(i-1)+dx(i))
             var(2)=(u(i,j+1,k)-u(i,j-1,k))/(dyv(j)+dyv(j+1))
             if (dim==3) var(3)=(u(i,j,k+1)-u(i,j,k+1))/(dzw(k)+dzw(k+1))
!!$             ! second order
!!$             var(1)=(dx(i-1)**2*(u(i+1,j,k)-u(i,j,k))&
!!$                  & -dx(i)**2*(u(i-1,j,k)-u(i,j,k)))/(dx(i-1)**2*dx(i)+dx(i-1)*dx(i)**2)
!!$             var(2)=(dyv(j)**2*(u(i,j+1,k)-u(i,j,k))&
!!$                  & -dyv(j+1)**2*(u(i,j-1,k)-u(i,j,k)))/(dyv(j)**2*dyv(j+1)+dyv(j+1)*dyv(j)**2)
!!$             if (dim==3) then
!!$                var(3)=(dzw(k)**2*(u(i,j,k+1)-u(i,j,k))&
!!$                     & -dzw(k+1)**2*(u(i,j,k-1)-u(i,j,k)))/(dzw(k)**2*dzw(k+1)+dzw(k+1)*dzw(k)**2)
!!$             end if
             !-------------------------------------------------------------------
             
             !-------------------------------------------------------------------
             ! add u grad u at x-velocity nodes
             !-------------------------------------------------------------------
	     do l=1,dim
	        dudt(i,j,k)=dudt(i,j,k)+vf(l)*var(l)
	     end do
             !-------------------------------------------------------------------
             
             !-------------------------------------------------------------------
             ! y-dir interpolation for velocity vector
             !    at y-velocity nodes
             !-------------------------------------------------------------------
             x=grid_x(i)
             y=grid_yv(j)
             z=grid_z(k)
             call interpolation_uvw(tmp,u,v,w,(/x,y,z/),vf(1:dim),2,dim)
             !-------------------------------------------------------------------
             
             !-------------------------------------------------------------------
             ! gradients at y-velocity nodes
             !-------------------------------------------------------------------
             ! first order
             var(1)=(v(i+1,j,k)-v(i-1,j,k))/(dxu(i)+dxu(i+1))
             var(2)=(v(i,j+1,k)-v(i,j-1,k))/(dy(j-1)+dy(j))
             if (dim==3) var(3)=(v(i,j,k+1)-v(i,j,k+1))/(dzw(k)+dzw(k+1))
             !-------------------------------------------------------------------
             
             !-------------------------------------------------------------------
             ! add u grad u at y-velocity nodes
             !-------------------------------------------------------------------
	     do l=1,dim
	        dvdt(i,j,k)=dvdt(i,j,k)+vf(l)*var(l)
	     end do             
	     !-------------------------------------------------------------------

             if (dim==3) then 
                !-------------------------------------------------------------------
                ! z-dir interpolation for velocity vector
                !    at z-velocity nodes
                !-------------------------------------------------------------------
                x=grid_x(i)
                y=grid_yv(j)
                z=grid_z(k)
                call interpolation_uvw(tmp,u,v,w,(/x,y,z/),vf(1:dim),2,dim)
                !-------------------------------------------------------------------

                !-------------------------------------------------------------------
                ! gradients at z-velocity nodes
                !-------------------------------------------------------------------
                var(1)=(w(i+1,j,k)-w(i-1,j,k))/(dxu(i)+dxu(i+1))
                var(2)=(w(i,j+1,k)-w(i,j-1,k))/(dyv(j)+dyv(j+1))
                var(3)=(w(i,j,k+1)-w(i,j,k+1))/(dz(k-1)+dz(k))
                !-------------------------------------------------------------------

                !-------------------------------------------------------------------
                ! add u grad u at z-velocity nodes
                !-------------------------------------------------------------------
	        do l=1,dim
	           dwdt(i,j,k)=dwdt(i,j,k)+vf(l)*var(l)
	        end do                
		!-------------------------------------------------------------------
             end if

          end do
       end do
    end do

  end subroutine material_derivative

end MODULE Mod_material_derivative
