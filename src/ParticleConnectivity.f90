!========================================================================
!
!                   FUGU Version 1.0.0
!
!========================================================================
!
! Copyright  Stéphane Vincent
!
! stephane.vincent@u-pem.fr          straightparadigm@gmail.com
!
! This file is part of the FUGU Fortfran library, a set of Computational 
! Fluid Dynamics subroutines devoted to the simulation of multi-scale
! multi-phase flows for resolved scale interfaces (liquid/gas/solid). 
!
!========================================================================
!**
!**   NAME       : ParticleConnectivity.f90
!**
!**   AUTHOR     : Benoit Trouette
!**
!**   FUNCTION   : modules for Lagrangian particle tracking of FUGU software
!**
!**   DATES      : Version 1.0.0  : from : december, 2018
!**
!========================================================================

!===============================================================================
module mod_ParticleConnectivity
  use mod_Parameters
  use mod_struct_grid
  use mod_struct_Particle_Tracking
  use mod_search

  
  !***************************************************************!
  ! variables for particles connectivity
  !***************************************************************!
  integer                              :: nNgbr,ncNgbr
  integer, dimension(100000)           :: NgbrList
  integer, dimension(3)                :: cells
  integer, dimension(:), allocatable   :: CellList
  integer, dimension(:,:), allocatable :: vcNgbr
  !***************************************************************!


contains

#if 1
  
  subroutine compute_CellList(mesh,CellList,cells,ncNgbr,vcNgbr,LPart,nPart)
    !-------------------------------------------------------------
    ! Global variables 
    !-------------------------------------------------------------
    integer, intent(in)                                        :: nPart
    integer, intent(inout)                                     :: ncNgbr
    integer, dimension(3), intent(in)                          :: cells
    integer, allocatable, dimension(:), intent(inout)          :: CellList
    integer, allocatable, dimension(:,:), intent(inout)        :: vcNgbr
    type(grid_t), intent(in)                                   :: mesh
    type(particle_t), allocatable, dimension(:), intent(inout) :: LPart
    !-------------------------------------------------------------
    ! Local variables 
    !-------------------------------------------------------------
    integer                                                    :: c,i,j,k,n
    integer                                                    :: Ngbr
    integer, dimension(3)                                      :: cc
    real(8), dimension(3)                                      :: r
    real(8), dimension(3)                                      :: inv
    logical                                                    :: once=.true.
    !-------------------------------------------------------------

    !-------------------------------------------------------------
    ! CellList Building 
    !-------------------------------------------------------------
    if (once) then
       once=.false.

       allocate(CellList(1:nPart+product(cells)))

       !------------------------------------
       ! initilisation vector cells neighbor
       !------------------------------------
       if (dim==2) then
          ncNgbr=5
          allocate(vcNgbr(ncNgbr,3))
          vcNgbr(1,1:3)=(/0,0,0/)
          vcNgbr(2,1:3)=(/1,0,0/)
          vcNgbr(3,1:3)=(/1,1,0/)
          vcNgbr(4,1:3)=(/0,1,0/)
          vcNgbr(5,1:3)=(/-1,1,0/)
       else
          ncNgbr=14
          allocate(vcNgbr(ncNgbr,3))
          vcNgbr(1,1:3)=(/0,0,0/)
          vcNgbr(2,1:3)=(/1,0,0/)
          vcNgbr(3,1:3)=(/1,1,0/)
          vcNgbr(4,1:3)=(/0,1,0/)
          vcNgbr(5,1:3)=(/-1,1,0/)
          vcNgbr(6,1:3)=(/0,0,1/)
          vcNgbr(7,1:3)=(/1,0,1/)
          vcNgbr(8,1:3)=(/1,1,1/)
          vcNgbr(9,1:3)=(/0,1,1/)
          vcNgbr(10,1:3)=(/-1,1,1/)
          vcNgbr(11,1:3)=(/-1,0,1/)
          vcNgbr(12,1:3)=(/-1,-1,1/)
          vcNgbr(13,1:3)=(/0,-1,1/)
          vcNgbr(14,1:3)=(/1,-1,1/)
       end if
    end if
    !CellList(1:nPart+product(cells))=-1
    CellList(nPart+1:nPart+product(cells))=-1
    !-------------------------------------------------------------

    if (regular_mesh) then 
       !-------------------------------------------------------------
       ! inv = nx / L_x = 1/dx
       !-------------------------------------------------------------
!!$       inv(1:3)=(/cells(1)/(xmax-xmin+dx(1)),&
!!$            & cells(2)/(ymax-ymin+dy(1)),    &
!!$            & cells(3)/(zmax-zmin+dz(1))/)
       if (dim==2) then
!!$          inv(1:3)=(/cells(1)/(grid_xu(ex+1)-grid_xu(sx)),&
!!$               & cells(2)/(grid_yv(ey+1)-grid_yv(sy)),    &
!!$               & cells(3)/(zmax-zmin+dz(1)) /)
          inv(1:3)=(/cells(1)/(grid_xu(ex+2)-grid_xu(sx-1)),&
               & cells(2)/(grid_yv(ey+2)-grid_yv(sy-1)),    &
               & cells(3)/(zmax-zmin+dz(1)) /)
       else
!!$          inv(1:3)=(/cells(1)/(grid_xu(ex+1)-grid_xu(sx)),&
!!$               & cells(2)/(grid_yv(ey+1)-grid_yv(sy)),    &
!!$               & cells(3)/(grid_zw(ez+1)-grid_zw(sz))/)
          inv(1:3)=(/cells(1)/(grid_xu(ex+2)-grid_xu(sx-1)),&
               & cells(2)/(grid_yv(ey+2)-grid_yv(sy-1)),    &
               & cells(3)/(grid_zw(ez+2)-grid_zw(sz-1))/)
       end if
       !-------------------------------------------------------------

       !-------------------------------------------------------------
       ! loop over all particles
       !-------------------------------------------------------------
       do n=1,nPart
          !----------------------------------------------------------
          ! localisation of the cell including the ACTIVE particle
          !----------------------------------------------------------
          if (LPart(n)%active) then
             !------------------------------------------------
             ! grid shift, centered on scalar nodes
             !------------------------------------------------
!!$             r=LPart(n)%r+(/-(xmin-dx2(sx)),-(ymin-dy2(sy)),-(zmin-dz2(sz))/)
!!$             r=LPart(n)%r+(/-(grid_x(sx)-dx2(sx)),-(grid_y(sy)-dy2(sy)),-(grid_z(sz)-dz2(sz))/)
!!$             r=LPart(n)%r+(/-(grid_xu(sx)),-(grid_yv(sy)),-(grid_zw(sz))/)
             if (dim==2) then
                r=LPart(n)%r+(/-(grid_xu(sx-1)),-(grid_yv(sy-1)),-(grid_zw(sz))/)
             else 
                r=LPart(n)%r+(/-(grid_xu(sx-1)),-(grid_yv(sy-1)),-(grid_zw(sz-1))/)
             end if
             !------------------------------------------------
             ! grid index
             !------------------------------------------------
             cc=floor(r*inv)+1
             !------------------------------------------------
             ! check
             !------------------------------------------------
             do i=1,dim
                if (cc(i) > cells(i) .or. cc(i)<1) then
                   write(*,*)'---------------'
                   write(*,*)'rank:',rank
                   write(*,*)'dir:',i
                   write(*,*)'n=',n
                   write(*,*)'cells=',cells
                   write(*,*)'gmin,gmax=',xyzBounds(i,:)
                   if (i==1) write(*,*)'min,max=',grid_xu(sx-1),grid_xu(ex+2)
                   do j=sx-1,ex+1
                      write(*,*) grid_xu(j)
                   end do
                   if (i==2) write(*,*)'min,max=',grid_yv(sy-1),grid_yv(ey+2)
                   if (i==3) write(*,*)'min,max=',grid_zw(sz-1),grid_zw(ez+2)
                   write(*,*)'r=',r(i)
                   write(*,*)'LPart(n)%r(i)=',LPart(n)%r(i)
                   write(*,*)'LPart(n)%v(i)=',LPart(n)%v(i)
                   write(*,*)'LPart(n)%u(i)=',LPart(n)%u(i)
                   write(*,*)'cc=',cc
                   stop
                end if
             end do
             !------------------------------------------------
             c=nPart+vlinear(cc,cells)
             CellList(n)=CellList(c)
             CellList(c)=n
          end if
       end do
       !-------------------------------------------------------------
    else
       !-------------------------------------------------------------
       ! Irregular meshes
       !-------------------------------------------------------------
       do n=1,nPart
          if (LPart(n)%active) then
             !write(*,*) n,LPart(n)%ijk
             call search_cell_ijk(mesh,LPart(n)%r,LPart(n)%ijk,PRESS_MESH)
             !write(*,*) n,LPart(n)%ijk
             if (dim==2) then
                cc=LPart(n)%ijk+(/1,1,0/)
                cc=cc+(/1,1,0/) ! from sx-1 !!!
             else
                cc=LPart(n)%ijk+(/1,1,1/)
                cc=cc+(/1,1,1/) ! from sx-1 !!!
             end if
             !write(*,*) n,cc             
             !------------------------------------------------
             ! check
             !------------------------------------------------
             do i=1,dim
                if (cc(i) > cells(i) .or. cc(i)<1) then
                   write(*,*)'---------------'
                   write(*,*)'dir:',i
                   write(*,*)'n=',n
                   write(*,*)'cells=',cells(i)
                   write(*,*)'min,max=',xyzBounds(i,:)
                   write(*,*)'r=',LPart(n)%r
                   if (i==1) write(*,*)'min,max=',grid_xu(sx-1),grid_xu(ex+2)
                   if (i==2) write(*,*)'min,max=',grid_yv(sy-1),grid_yv(ey+2)
                   if (i==3) write(*,*)'min,max=',grid_zw(sz-1),grid_zw(ez+2)
                   write(*,*)'cc=',cc(i)
                   stop
                end if
             end do
             !------------------------------------------------
             c=nPart+vlinear(cc,cells)
             CellList(n)=CellList(c)
             CellList(c)=n
          end if
       end do
    end if

#if 0
    !-------------------------------------------------------------
    ! Check CellList
    !-------------------------------------------------------------
    do n=nPart+1,nPart+product(cells)
       write(*,*) "In cell", n-nPart, ","
       write(*,*) "the following particles are present:"
       j=CellList(n)
       if (j<0) then
          write(*,*) "cell is empty, j=",j
          write(*,*) "--------------------------"
       else
          c=0
          do while (j>0)
             write(*,*) '', j
             c=c+1
             j=CellList(j)
          end do
          write(*,*) 'Total', c, 'particle(s)'
          write(*,*) "--------------------------"
       end if
    end do
    !-------------------------------------------------------------
#endif

    !-------------------------------------------------------------
    ! end CellList Building 
    !-------------------------------------------------------------

  end subroutine compute_CellList


  subroutine compute_NeighborList(NgbrList,nNgbr,ncNgbr,vcNgbr,rCut,LPart,nPart,CellList,cells)
    !-------------------------------------------------------------
    ! Global variables 
    !-------------------------------------------------------------
    integer, intent(out)                                  :: nNgbr
    integer, intent(in)                                   :: nPart,ncNgbr
    integer, dimension(3), intent(in)                     :: cells
    integer, allocatable, dimension(:), intent(in)        :: CellList
    integer, allocatable, dimension(:,:), intent(in)      :: vcNgbr
    integer, dimension(:), intent(inout)                  :: NgbrList
    real(8), intent(in)                                   :: rCut
    type(particle_t), allocatable, dimension(:), intent(in) :: LPart
    !-------------------------------------------------------------
    ! Local variables 
    !-------------------------------------------------------------
    integer                                               :: c,i,j,k,n,nNgbr_max
    integer                                               :: j1,j2,m1,m2
    integer                                               :: Ngbr
    integer, dimension(3)                                 :: cc,m1v,m2v
    real(8)                                               :: rcut2
    real(8), dimension(3)                                 :: r,rs,dr,shift
    real(8), dimension(3)                                 :: inv
    logical                                               :: once=.true.
    logical                                               :: loop_cycle
    !-------------------------------------------------------------

    nNgbr_max=size(NgbrList)

    !-------------------------------------------------------------
    ! initialization
    !-------------------------------------------------------------
    rCut2=rCut*rCut
    !-------------------------------------------------------------

    !-------------------------------------------------------------
    ! number of couple of neighbor to consider
    !-------------------------------------------------------------
    nNgbr=0
    !-------------------------------------------------------------

    !-------------------------------------------------------------
    ! loops over all cells
    !-------------------------------------------------------------
    do k=1,cells(3)
       do j=1,cells(2)
          do i=1,cells(1)
             !-------------------------------------------------------------
             ! current cell index and vectorized index
             !-------------------------------------------------------------
             m1v=(/i,j,k/)
             m1=nPart+vlinear(m1v,cells)
             !-------------------------------------------------------------

             !-------------------------------------------------------------
             ! loop over all cell neighbor
             !-------------------------------------------------------------
             do Ngbr=1,ncNgbr
                !-------------------------------------------------------------
                ! neighbor cell index
                !-------------------------------------------------------------
                m2v=m1v+vcNgbr(Ngbr,1:3)
                !-------------------------------------------------------------

                !-------------------------------------------------------------
                ! periodicity 
                !-------------------------------------------------------------
                shift=0
                loop_cycle=.false.
                do c=1,dim 
                   if (periodic(c)) then
                      call cells_periodicity(cells,m2v,c,xyzBounds,shift)
                   else
                      ! cycle if not
                      if (m2v(c)<1 .or. m2v(c)>cells(c)) then
                         loop_cycle=.true.
                      end if
                   end if
                end do
                if (loop_cycle) cycle 
                !-------------------------------------------------------------

                !-------------------------------------------------------------
                ! vectorized index
                !-------------------------------------------------------------
                m2=nPart+vlinear(m2v,cells)
                !-------------------------------------------------------------

                j1=CellList(m1)
                do while (j1>0)
                   j2=CellList(m2)
                   do while(j2>0)
                      if (m1/=m2.or.j2<j1) then
                         !----------------------------------------------------
                         ! distance between particles j1 and j2
                         !----------------------------------------------------
                         dr=LPart(j1)%r-LPart(j2)%r 
                         dr=dr-shift
                         !----------------------------------------------------

                         !----------------------------------------------------
                         ! neighbor if distance < rcut
                         !----------------------------------------------------
                         if (sum(dr(1:dim)**2)<rcut2) then
                            nNgbr=nNgbr+1
                            if ((2*nNgbr)>nNgbr_max) then 
                               write(*,*) "ERROR, neighbor list is too short"
                               write(*,*) "increase size(NgbrList), current value is ", nNgbr_max
                               write(*,*) "STOP"
                               stop
                            end if
                            NgbrList(2*(nNgbr-1)+1)=j1
                            NgbrList(2*(nNgbr-1)+2)=j2 
                         end if
                         !----------------------------------------------------
                      end if
                      j2=CellList(j2)
                   end do
                   j1=CellList(j1)
                end do
             end do
          end do
       end do
    end do


#if 0
    !----------------------------------------------------
    ! check neighbor list 
    !----------------------------------------------------
    do n=1,nNgbr,2
       write(*,*) NgbrList(n),NgbrList(n+1)
    end do
    write(*,*) "nNgbr",nNgbr
    !----------------------------------------------------
#endif 

  end subroutine compute_NeighborList





  function vlinear(a,b)
    !---------------------------------------------
    ! global variables
    !---------------------------------------------
    integer                         :: vlinear
    integer,dimension(3),intent(in) :: a,b
    !---------------------------------------------

    vlinear=((a(3)-1)*b(2)+(a(2)-1))*b(1)+a(1)

  end function vlinear





  subroutine cells_periodicity(cells,vec,dir,X,shift)
    !---------------------------------------------
    ! global variables
    !---------------------------------------------
    integer, intent(in)                  :: dir
    integer, dimension(3), intent(in)    :: cells
    integer, dimension(3), intent(inout) :: vec
    real(8), dimension(3), intent(inout) :: shift
    real(8), dimension(3,2), intent(in)  :: X
    !---------------------------------------------

    if (vec(dir)>cells(dir)) then
       vec(dir)=1
       shift(dir)=X(dir,2)-X(dir,1) 
    else if (vec(dir)<1) then
       vec(dir)=cells(dir)
       shift(dir)=-(X(dir,2)-X(dir,1))
    end if

  end subroutine cells_periodicity





  subroutine vec_periodicity(vec,dir,X)
    !---------------------------------------------
    ! global variables
    !---------------------------------------------
    real(8), dimension(3), intent(inout) :: vec
    real(8), dimension(3,2), intent(in)  :: X
    integer, intent(in)                  :: dir
    !---------------------------------------------
    ! Local variables
    !---------------------------------------------
    real(8), dimension(3)                :: L
    !---------------------------------------------

    L=X(:,2)-X(:,1)
    if (vec(dir)>=L(dir)/2) then
       vec(dir)=vec(dir)-L(dir) 
    else if (vec(dir)<-L(dir)/2) then
       vec(dir)=vec(dir)+L(dir)
    end if

  end subroutine vec_periodicity

#endif
  
  !===============================================================================
end module mod_ParticleConnectivity
!===============================================================================
