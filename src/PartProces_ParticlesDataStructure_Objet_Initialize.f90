!===============================================================================
module Bib_VOFLag_ParticlesDataStructure_Objet_Initialize
  !===============================================================================
  contains
  !-----------------------------------------------------------------------------  
  !> @author amine chadil
  ! 
  !> @brief cette routine remplie la structure vl%objet avec les caracteristiques des
  !! particules lues dans le fichier "data.in". 
  !
  !> @param[out] vl       : la structure contenant toutes les informations
  !! relatives aux particules.
  !-----------------------------------------------------------------------------
  subroutine ParticlesDataStructure_Objet_Initialize(vl)  
    !===============================================================================
    !modules
    !===============================================================================
    !-------------------------------------------------------------------------------
    !Modules de definition des structures et variables => src/mod/module_...f90
    !-------------------------------------------------------------------------------
    use Module_VOFLag_ParticlesDataStructure
    use Module_VOFLag_Flows
    use mod_Parameters,                       only : deeptracking
    !-------------------------------------------------------------------------------
    !Modules de subroutines de la librairie RESPECT => src/Bib_VOFLag_...f90
    !-------------------------------------------------------------------------------
    use Bib_VOFLag_Sphere_ParticlesDataStructure_Objet_Initialize
    use Bib_VOFLag_Ellipsoid_ParticlesDataStructure_Objet_Initialize
    !use Bib_VOFLag_AnyShape_ParticlesDataStructure_Objet_Initialize
    !-------------------------------------------------------------------------------
    !===============================================================================
    !declarations des variables
    !===============================================================================
    implicit none
    !-------------------------------------------------------------------------------
    !variables globales
    !-------------------------------------------------------------------------------
    type(struct_vof_lag), intent(inout)  :: vl
    !-------------------------------------------------------------------------------
    !variables locales 
    !-------------------------------------------------------------------------------
    integer                            :: err
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'entree ParticlesDataStructure_Objet_Initialize'
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    !ouverture et lecture du fichier RESPECT.in
    !-------------------------------------------------------------------------------
    open(unit=1001,file="RESPECT.in",status="old",action="read",iostat=err) 
    read(1001,nml=nml_RESPECT)
    read(1001,nml=nml_uniform_past_part)
    close(1001)
    vl%nature      =nature
    vl%kpt         =kpt
    vl%deplacement =deplacement
    vl%vrot        =vrot
    vl%force_inter =force_inter
    vl%choc_lubri  =choc_lubri
    vl%choc_sec    =choc_sec
    vl%postchoc_fPM=postchoc_fPM
    vl%postforce   =postforce
    !-------------------------------------------------------------------------------

    select case(vl%nature)
    
    case(1)
      !-------------------------------------------------------------------------------
      ! Si les particules sont spherique
      !-------------------------------------------------------------------------------
      call Sphere_ParticlesDataStructure_Objet_Initialize(vl)

    case(2)
      !-------------------------------------------------------------------------------
      ! Si les particules sont ellipsoidale
      !-------------------------------------------------------------------------------
      call Ellipsoid_ParticlesDataStructure_Objet_Initialize(vl)
      !write(*,*) "STOP : NATURE_PARTICULES doit etre Spherique"

    case(3)
      !-------------------------------------------------------------------------------
      ! Si les particules sont ellipsoidale
      !-------------------------------------------------------------------------------
      !call AnyShape_ParticlesDataStructure_Objet_Initialize(vl)
      write(*,*) "STOP : NATURE_PARTICULES doit etre Spherique"

    case default
      write(*,*) "STOP : NATURE_PARTICULES doit etre 1,2 ou 3"
      stop

    end select

    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'sortie ParticlesDataStructure_Objet_Initialize'
    !-------------------------------------------------------------------------------
  end subroutine ParticlesDataStructure_Objet_Initialize

  !===============================================================================
end module Bib_VOFLag_ParticlesDataStructure_Objet_Initialize
!===============================================================================
  


