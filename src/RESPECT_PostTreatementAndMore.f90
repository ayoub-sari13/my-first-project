!===============================================================================
module Bib_VOFLag_RESPECT_PostTreatementAndMore
  !===============================================================================
  contains
  !-----------------------------------------------------------------------------  
  !> @author amine chadil
  ! 
  !> @brief cette routine appelle les routines de preparation du Module RESPECT
  !
  !> @param[out]   vl     : la structure contenant toutes les informations
  !! relatives aux particules.
  !-----------------------------------------------------------------------------
  subroutine RESPECT_PostTreatementAndMore(vl,pres,u,v,w,cou,nt,time)
    !===============================================================================
    !modules
    !===============================================================================
    !-------------------------------------------------------------------------------
    !Modules de definition des structures et variables => src/mod/module_...f90
    !-------------------------------------------------------------------------------
    use Module_VOFLag_ParticlesDataStructure
    use mod_Parameters,                       only : deeptracking
    !-------------------------------------------------------------------------------
    !Modules de subroutines de la librairie RESPECT => src/Bib_VOFLag_...f90
    !-------------------------------------------------------------------------------
    use Bib_VOFLag_HydrodynamicForce_HeatFlux_Preparation
    use Bib_VOFLag_ParticlesDataStructure_Impression
    use Bib_VOFLag_HydrodynamicForce
    !-------------------------------------------------------------------------------
    !===============================================================================
    !declarations des variables
    !===============================================================================
    implicit none
    !-------------------------------------------------------------------------------
    !variables globales
    !-------------------------------------------------------------------------------
    type(struct_vof_lag),                   intent(inout)  :: vl
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable, intent(in)     :: cou,pres
    real(8), dimension(:,:,:), allocatable, intent(in)     :: u,v,w
    real(8),                                intent(in)     :: time
    integer,                                intent(in)     :: nt
    !-------------------------------------------------------------------------------
    !variables locales 
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'entree RESPECT_PostTreatementAndMore'
    !-------------------------------------------------------------------------------
    if (vl%postforce .or. vl%postHeatFlux) call HydrodynamicForce_HeatFlux_Preparation(vl)

    if (vl%postforce) then
      call HydrodynamicForce(vl,pres,u,v,w,cou)
    end if
    !-------------------------------------------------------------------------------
    ! Impression des donnees relatives aux particules
    !-------------------------------------------------------------------------------
    call ParticlesDataStructure_Impression(vl,nt,time)

    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'sortie RESPECT_PostTreatementAndMore'
    !-------------------------------------------------------------------------------
  end subroutine RESPECT_PostTreatementAndMore

  !===============================================================================
end module Bib_VOFLag_RESPECT_PostTreatementAndMore
!===============================================================================