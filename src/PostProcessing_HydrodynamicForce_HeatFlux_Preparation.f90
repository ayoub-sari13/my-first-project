!===============================================================================
module Bib_VOFLag_HydrodynamicForce_HeatFlux_Preparation
  !===============================================================================
  contains
  !---------------------------------------------------------------------------  
  !> @author amine chadil
  ! 
  !> @brief lecture du maillage lagrangien, pour le calcul de la force appliquee sur la 
  ! particule en chaqun des centres des elements de ce maillage
  !
  !> @todo vaut-il mieux, en terme de temps de calcul, de calculer tous avant de communiquer
  ! ou de faire les deux en meme temps? ou cela n'a pas d'importance 
  !
  !> @param[out] vl    : la structure contenant toutes les informations
  !! relatives aux particules   
  !---------------------------------------------------------------------------   
  subroutine HydrodynamicForce_HeatFlux_Preparation(vl)
    !===============================================================================
    !modules
    !===============================================================================
    !-------------------------------------------------------------------------------
    !Modules de definition des structures et variables => src/mod/module_...f90
    !-------------------------------------------------------------------------------
    use Module_VOFLag_ParticlesDataStructure
    use mod_Parameters,                       only : dim,deeptracking,rank,nproc
    use mod_mpi
    !-------------------------------------------------------------------------------
    !Modules de subroutines de la librairie VOF-Lag
    !-------------------------------------------------------------------------------
    use Bib_VOFLag_ParticlesDataStructure_Object_Lag4AllCopies_Update
    use Bib_VOFLag_ParticlesDataStructure_Object_NormalAndArea_Update
    use Bib_module_initrackn
    !-------------------------------------------------------------------------------

    !===============================================================================
    !declarations des variables
    !===============================================================================
    implicit none
    !-------------------------------------------------------------------------------
    !variables globales
    !-------------------------------------------------------------------------------
    type(struct_vof_lag), intent(inout)            :: vl
    !-------------------------------------------------------------------------------
    !variables locales 
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable         :: lag
    integer                                        :: np,nd,k,p,nb_elements
    !-------------------------------------------------------------------------------
    
    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'entree HydrodynamicForce_HeatFlux_Preparation'
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    !remplir le tableau des coordonnees lagrangiens des elements de l'objet: ce calcul
    !est fait avant la boucle car toutes les particules ont le meme maillage 
    !-------------------------------------------------------------------------------
    allocate(lag(1,1,1))
    call initrackn(lag,vl%objet(1)%ObjectFile,nb_elements,1,80,dim)
    lag = lag + 1D-8
    do np=1,vl%kpt
      vl%objet(np)%kkl = nb_elements
      !-------------------------------------------------------------------------------
      !boucle sur tous les objets
      !-------------------------------------------------------------------------------
      if (vl%objet(np)%in) then

        !-------------------------------------------------------------------------------
        ! allocation du tableau
        !-------------------------------------------------------------------------------
        if (allocated(vl%objet(np)%lag_for_all_copies)) deallocate(vl%objet(np)%lag_for_all_copies)
        allocate(vl%objet(np)%lag_for_all_copies(dim,dim,nb_elements))

        do nd=1,dim
           !-------------------------------------------------------------------------------
           ! transformation de l'objet stocke dans le fichier pour avoir la forme des particules 
           ! etudiees dans son repere attache
           !-------------------------------------------------------------------------------
           vl%objet(np)%lag_for_all_copies(nd,:,:) = (lag(nd,:,:)*2) * vl%objet(np)%sca(1)
        end do
        if (vl%nature .ne. 1) call ParticlesDataStructure_Object_Lag4AllCopies_Update(vl,np)

        call ParticlesDataStructure_Object_NormalAndArea_Update(vl,np)
      end if


      !-------------------------------------------------------------------------------
      ! Communiquer les infos aux autres procs
      !-------------------------------------------------------------------------------
      if (nproc .gt. 1) then

        call mpi_bcast(vl%objet(np)%kkl,1,mpi_integer,vl%objet(np)%locpart,comm3d,code)
          
        if (rank .ne. vl%objet(np)%locpart) then
          if (allocated(vl%objet(np)%lag_for_all_copies)) deallocate(vl%objet(np)%lag_for_all_copies)
          allocate(vl%objet(np)%lag_for_all_copies(dim,dim,vl%objet(np)%kkl))
        endif
        do k=1,vl%objet(np)%kkl
          do p=1,dim
            do nd=1,dim
              call mpi_bcast(vl%objet(np)%lag_for_all_copies(nd,p,k),1,mpi_double_precision,vl%objet(np)%locpart,comm3d,code)
            end do
          end do
        end do

        if (rank .ne. vl%objet(np)%locpart) then
          if (allocated(vl%objet(np)%normal)) deallocate(vl%objet(np)%normal)
          allocate(vl%objet(np)%normal(dim,vl%objet(np)%kkl))
        endif
        do k=1,vl%objet(np)%kkl
          do nd=1,dim
            call mpi_bcast(vl%objet(np)%normal(nd,k),1,mpi_double_precision,vl%objet(np)%locpart,comm3d,code)
          end do
        end do
        
        if (rank .ne. vl%objet(np)%locpart) then
          if (allocated(vl%objet(np)%area)) deallocate(vl%objet(np)%area)
          allocate(vl%objet(np)%area(vl%objet(np)%kkl))
        endif
        do k=1,vl%objet(np)%kkl
          call mpi_bcast(vl%objet(np)%area(k),1,mpi_double_precision,vl%objet(np)%locpart,comm3d,code)
        end do
      endif
      
    end do

    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'sortie HydrodynamicForce_HeatFlux_Preparation'
    !-------------------------------------------------------------------------------
  end subroutine HydrodynamicForce_HeatFlux_Preparation

  !===============================================================================
end module Bib_VOFLag_HydrodynamicForce_HeatFlux_Preparation
!===============================================================================
  


