!===============================================================================
module Bib_VOFLag_HydrodynamicForce_LagrangeExtrapolation
  !===============================================================================
  contains
  !---------------------------------------------------------------------------  
  !> @author jorge cesar brandle de motta, amine chadil
  ! 
  !> @brief calcul des efforts hydrodynamiques appliquees sur les particules
  !
  !> @param[out] vl              : la structure contenant toutes les informations
  !! relatives aux particules.
  !> @param[in]  pre             : pression
  !> @param[in]  vts             : vitesse
  !---------------------------------------------------------------------------  
  subroutine HydrodynamicForce_LagrangeExtrapolation(vl,pres,u,v,w,cou)
    !===============================================================================
    !modules
    !===============================================================================
    !-------------------------------------------------------------------------------
    !Modules de definition des structures et variables => src/mod/module_...f90
    !-------------------------------------------------------------------------------
    use Module_VOFLag_ParticlesDataStructure
    use mod_struct_thermophysics,            only : fluids
    use mod_Parameters,                      only : deeptracking,dim,rank
    use mod_mpi
    !-------------------------------------------------------------------------------
    !Modules de subroutines de la librairie 
    !-------------------------------------------------------------------------------
    use Bib_VOFLag_HydrodynamicForce_OneParticle_LagrangeExtrapolation
    !-------------------------------------------------------------------------------
    !===============================================================================
    !declarations des variables
    !===============================================================================
    implicit none
    !-------------------------------------------------------------------------------
    !variables globales
    !-------------------------------------------------------------------------------
    type(struct_vof_lag), intent(inout)                 :: vl
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable, intent(in)  :: pres,cou
    real(8), dimension(:,:,:), allocatable, intent(in)  :: u,v,w
    !-------------------------------------------------------------------------------
    !variables locales 
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:,:), allocatable            :: vir
    real(8), dimension(:,:,:)  , allocatable            :: vie
    real(8), dimension(:,:,:)  , allocatable            :: tensd
    character(len=200)                                  :: fichier_impression
    logical                                             :: EachElementImpression
    real                                                :: vals
    integer                                             :: np,nd,nbt
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'entree HydrodynamicForce_LagrangeExtrapolation'
    !-------------------------------------------------------------------------------
    EachElementImpression=.True.
    !-------------------------------------------------------------------------------
    ! Calcul des forces sans la penalisation des viscosité car elle modifie le resultat
    !-------------------------------------------------------------------------------
    !vie=fluids%mu(1)
    !vir=fluids%mu(1)

    !-------------------------------------------------------------------------------
    ! Calcul du tenseur de deformation
    !-------------------------------------------------------------------------------
    !tensd=0.D0
    !call tensdef_imft(vts,tensd)

    do np=1,vl%kpt
      vl%objet(np)%fp=0.D0
      vl%objet(np)%fv=0.D0
      vl%objet(np)%f =0.D0

      !-------------------------------------------------------------------------------
      ! On parcourt toutes les particules chevauchant le domaine du proc 
      !-------------------------------------------------------------------------------
      if (vl%objet(np)%on) then

        !-------------------------------------------------------------------------------
        ! Ecriture de la force sur chaque element
        !-------------------------------------------------------------------------------
        if (EachElementImpression .and. vl%objet(np)%on) then
          write(fichier_impression,'(a,i4.4,a,i4.4,a)') 'HydroForce_part_',np,'_proc_',rank,'.dat'
          open(unit=1000,file=fichier_impression,status='replace')
          if (dim .eq. 2) then
            write(1000,'(a1,8a13)',advance='no') '#',' ----lp----- ',' ---Coordx-- ',&
                   ' ---Coordz-- ',' -----p----- ',' ----Fvx---- ',' ---CoordX-- ',&
                   ' ---CoordZ-- ',' ---Order--- '
          else
            write(1000,'(a1,10a13)',advance='no') '#',' ----lp----- ',' ---Coordx-- ',&
                   ' ---Coordz-- ',' ---Coordy-- ',' -----p----- ',' ----Fvx---- ',&
                   ' ---CoordX-- ',' ---CoordZ-- ',' ---CoordY-- ',' ---Order--- '
          endif
          close(1000)
        endif

        do nbt=1,vl%objet(np)%nbtimes 
          if (vl%objet(np)%symperon(nbt)) then

            !-------------------------------------------------------------------------------
            ! calcul des forces hydrodynamiques appliquees sur la copie nbt de la particule np
            !-------------------------------------------------------------------------------
            call HydrodynamicForce_OneParticle_LagrangeExtrapolation(vl,np,nbt,pres,tensd,cou,vie,vir,EachElementImpression,fichier_impression)
          endif
        end do
      end if
    enddo

    !-------------------------------------------------------------------------------
    !sommer les forces calculees par chaque proc
    !-------------------------------------------------------------------------------
    if (nproc>1) then 
      do np=1,vl%kpt
         do nd = 1,dim
            vals=vl%objet(np)%fp (nd)
            call mpi_allreduce(vals,vl%objet(np)%fp (nd),1,mpi_double_precision,mpi_sum,comm3d,code)
            vals=vl%objet(np)%fv (nd)
            call mpi_allreduce(vals,vl%objet(np)%fv (nd),1,mpi_double_precision,mpi_sum,comm3d,code)
            vals=vl%objet(np)%f  (nd)
            call mpi_allreduce(vals,vl%objet(np)%f  (nd),1,mpi_double_precision,mpi_sum,comm3d,code)   
         end do
      end do
    end if 

    !-------------------------------------------------------------------------------
    if (deeptracking) write (*,*) 'sortie HydrodynamicForce_LagrangeExtrapolation'
    !-------------------------------------------------------------------------------
  end subroutine HydrodynamicForce_LagrangeExtrapolation

  !===============================================================================
end module Bib_VOFLag_HydrodynamicForce_LagrangeExtrapolation
!===============================================================================
  

