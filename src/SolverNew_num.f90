module mod_solver_new_num 

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

contains

  subroutine solver_comm_mpi(sol,solver,grid,force_icase)
    use mod_mpi
    use mod_solver_new_def
    use mod_struct_solver
    use mod_struct_grid
    use mod_timers
    implicit none
    !--------------------------------------------------------------------------
    ! Global variables
    !--------------------------------------------------------------------------
    type(solver_t), intent(in)             :: solver
    type(grid_t), intent(in)               :: grid 
    real(8), dimension(:), intent(inout)   :: sol
    integer, optional, intent(in)          :: force_icase
    !--------------------------------------------------------------------------
    ! Local variables
    !--------------------------------------------------------------------------
    integer                                :: icase
    integer                                :: m
    integer                                :: i,j,k,l
    integer                                :: ii,jj,kk
    integer                                :: lgx,lgy,lgz
    integer                                :: lsx,lex,lsy,ley,lsz,lez
    integer                                :: lsxu,lexu,lsyu,leyu,lszu,lezu
    integer                                :: lsxv,lexv,lsyv,leyv,lszv,lezv
    integer                                :: lsxw,lexw,lsyw,leyw,lszw,lezw
    integer                                :: lsxs,lexs,lsys,leys,lszs,lezs
    integer                                :: lsxus,lexus,lsyus,leyus,lszus,lezus
    integer                                :: lsxvs,lexvs,lsyvs,leyvs,lszvs,lezvs
    integer                                :: lsxws,lexws,lsyws,leyws,lszws,lezws
    real(8), allocatable, dimension(:,:,:) :: p,u,v,w
    !--------------------------------------------------------------------------

    call compute_time(TIMER_START,"[sub] solver_comm_mpi")

    m   = grid%dim-2 ! generalize 2D/3D
    lgx = grid%gx
    lgy = grid%gy
    lgz = grid%gz

    if (present(force_icase)) then
       icase = force_icase
    else
       icase = solver%eqs
    end if

    select case(icase)
    case(SOLVE_P)
       !--------------------------------------------------------------------------
       ! scalar
       !--------------------------------------------------------------------------

       !--------------------------------------------------------------------------
       ! uvwp
       !--------------------------------------------------------------------------

       lsx  = grid%sx
       lex  = grid%ex
       lsy  = grid%sy
       ley  = grid%ey
       lsz  = grid%sz
       lez  = grid%ez

       lsxs = grid%sxs
       lexs = grid%exs
       lsys = grid%sys
       leys = grid%eys
       lszs = grid%szs
       lezs = grid%ezs

       !--------------------------------------------------------------------------
       ! alloc
       !--------------------------------------------------------------------------
       allocate(p(lsx-lgx:lex+lgx,lsy-lgy:ley+lgy,lsz-lgz:lez+lgz))
       p=0
       !--------------------------------------------------------------------------

       !--------------------------------------------------------------------------
       ! sol --> p
       !--------------------------------------------------------------------------
       do kk=lszs,lezs
          do jj=lsys,leys
             do ii=lsxs,lexs
                i=ii-lsxs
                j=jj-lsys
                k=kk-lszs
                l=i+j*(lexs-lsxs+1)+(k*(lexs-lsxs+1)*(leys-lsys+1)+1)**m
                p(ii,jj,kk)=sol(l)
             enddo
          enddo
       end do
       !--------------------------------------------------------------------------

       !--------------------------------------------------------------------------
       ! mpi lexchange
       !--------------------------------------------------------------------------
       call comm_mpi_sca(p)
       !--------------------------------------------------------------------------

       !--------------------------------------------------------------------------
       ! sol --> p
       !--------------------------------------------------------------------------
       do kk=lszs,lezs
          do jj=lsys,leys
             do ii=lsxs,lexs
                i=ii-lsxs
                j=jj-lsys
                k=kk-lszs
                l=i+j*(lexs-lsxs+1)+(k*(lexs-lsxs+1)*(leys-lsys+1)+1)**m
                sol(l)=p(ii,jj,kk)
             enddo
          enddo
       end do
       !--------------------------------------------------------------------------
    case(SOLVE_U)
       !--------------------------------------------------------------------------
       ! u-vec
       !--------------------------------------------------------------------------
       !--------------------------------------------------------------------------
       ! uvw
       !--------------------------------------------------------------------------
       
       lsxu  = grid%sxu
       lexu  = grid%exu
       lsyu  = grid%syu
       leyu  = grid%eyu
       lszu  = grid%szu
       lezu  = grid%ezu

       lsxus = grid%sxus
       lexus = grid%exus
       lsyus = grid%syus
       leyus = grid%eyus
       lszus = grid%szus
       lezus = grid%ezus

       !--------------------------------------------------------------------------
       ! alloc
       !--------------------------------------------------------------------------
       allocate(u(lsxu-lgx:lexu+lgx,lsyu-lgy:leyu+lgy,lszu-lgz:lezu+lgz))
       u=0
       !--------------------------------------------------------------------------

       !--------------------------------------------------------------------------
       ! sol --> u
       !--------------------------------------------------------------------------
       ! U component
       !--------------------------------------------------------------------------
       do kk=lszus,lezus
          do jj=lsyus,leyus
             do ii=lsxus,lexus
                i=ii-lsxus
                j=jj-lsyus
                k=kk-lszus
                l=i+j*(lexus-lsxus+1)+(k*(lexus-lsxus+1)*(leyus-lsyus+1)+1)**m
                u(ii,jj,kk)=sol(l)
             enddo
          enddo
       end do
       !--------------------------------------------------------------------------

       !--------------------------------------------------------------------------
       ! mpi exchange
       !--------------------------------------------------------------------------
       call comm_mpi_u(u)
       !--------------------------------------------------------------------------

       !--------------------------------------------------------------------------
       ! u,v,w --> sol
       !--------------------------------------------------------------------------
       ! U component
       !--------------------------------------------------------------------------
       do kk=lszus,lezus
          do jj=lsyus,leyus
             do ii=lsxus,lexus
                i=ii-lsxus
                j=jj-lsyus
                k=kk-lszus
                l=i+j*(lexus-lsxus+1)+(k*(lexus-lsxus+1)*(leyus-lsyus+1)+1)**m
                sol(l)=u(ii,jj,kk)
             enddo
          enddo
       end do
       !--------------------------------------------------------------------------
    case(SOLVE_V)
       !--------------------------------------------------------------------------
       ! v-vec
       !--------------------------------------------------------------------------

       lsxv  = grid%sxv
       lexv  = grid%exv
       lsyv  = grid%syv
       leyv  = grid%eyv
       lszv  = grid%szv
       lezv  = grid%ezv

       lsxvs = grid%sxvs
       lexvs = grid%exvs
       lsyvs = grid%syvs
       leyvs = grid%eyvs
       lszvs = grid%szvs
       lezvs = grid%ezvs

       !--------------------------------------------------------------------------
       ! alloc
       !--------------------------------------------------------------------------
       allocate(v(lsxv-lgx:lexv+lgx,lsyv-lgy:leyv+lgy,lszv-lgz:lezv+lgz))
       v=0
       !--------------------------------------------------------------------------

       !--------------------------------------------------------------------------
       ! sol --> v
       !--------------------------------------------------------------------------
       ! V component
       !--------------------------------------------------------------------------
       do kk=lszvs,lezvs
          do jj=lsyvs,leyvs
             do ii=lsxvs,lexvs
                i=ii-lsxvs
                j=jj-lsyvs
                k=kk-lszvs
                l=i+j*(lexvs-lsxvs+1)++(k*(lexvs-lsxvs+1)*(leyvs-lsyvs+1)+1)**m
                v(ii,jj,kk)=sol(l)
             enddo
          enddo
       end do
       !--------------------------------------------------------------------------

       !--------------------------------------------------------------------------
       ! mpi lexchange
       !--------------------------------------------------------------------------
       call comm_mpi_v(v)
       !--------------------------------------------------------------------------

       !--------------------------------------------------------------------------
       ! V component
       !--------------------------------------------------------------------------
       do kk=lszvs,lezvs
          do jj=lsyvs,leyvs
             do ii=lsxvs,lexvs
                i=ii-lsxvs
                j=jj-lsyvs
                k=kk-lszvs
                l=i+j*(lexvs-lsxvs+1)+(k*(lexvs-lsxvs+1)*(leyvs-lsyvs+1)+1)**m
                sol(l)=v(ii,jj,kk)
             enddo
          enddo
       end do
       !--------------------------------------------------------------------------
    case(SOLVE_W)
       !--------------------------------------------------------------------------
       ! w-vec
       !--------------------------------------------------------------------------

       lsxw = grid%sxw
       lexw = grid%exw
       lsyw = grid%syw
       leyw = grid%eyw
       lszw = grid%szw
       lezw = grid%ezw

       lsxws = grid%sxws
       lexws = grid%exws
       lsyws = grid%syws
       leyws = grid%eyws
       lszws = grid%szws
       lezws = grid%ezws

       !--------------------------------------------------------------------------
       ! alloc
       !--------------------------------------------------------------------------
       allocate(w(lsxw-lgx:lexw+lgx,lsyw-lgy:leyw+lgy,lszw-lgz:lezw+lgz))
       w=0
       !--------------------------------------------------------------------------

       !--------------------------------------------------------------------------
       ! sol --> w
       !--------------------------------------------------------------------------
       ! W component
       !--------------------------------------------------------------------------
       do kk=lszws,lezws
          do jj=lsyws,leyws
             do ii=lsxws,lexws
                i=ii-lsxws
                j=jj-lsyws
                k=kk-lszws
                l=i+j*(lexws-lsxws+1)+(k*(lexws-lsxws+1)*(leyws-lsyws+1)+1)**m
                w(ii,jj,kk)=sol(l)
             enddo
          enddo
       end do
       !--------------------------------------------------------------------------

       !--------------------------------------------------------------------------
       ! mpi lexchange
       !--------------------------------------------------------------------------
       call comm_mpi_w(w)
       !--------------------------------------------------------------------------

       !--------------------------------------------------------------------------
       ! W component
       !--------------------------------------------------------------------------
       do kk=lszws,lezws
          do jj=lsyws,leyws
             do ii=lsxws,lexws
                i=ii-lsxws
                j=jj-lsyws
                k=kk-lszws
                l=i+j*(lexws-lsxws+1)+(k*(lexws-lsxws+1)*(leyws-lsyws+1)+1)**m
                sol(l)=w(ii,jj,kk)
             enddo
          enddo
       end do
       !--------------------------------------------------------------------------
    case(SOLVE_UV,SOLVE_UVW)
       !--------------------------------------------------------------------------
       ! uvw
       !--------------------------------------------------------------------------

       lsxu = grid%sxu
       lexu = grid%exu
       lsyu = grid%syu
       leyu = grid%eyu
       lszu = grid%szu
       lezu = grid%ezu

       lsxv = grid%sxv
       lexv = grid%exv
       lsyv = grid%syv
       leyv = grid%eyv
       lszv = grid%szv
       lezv = grid%ezv

       lsxw = grid%sxw
       lexw = grid%exw
       lsyw = grid%syw
       leyw = grid%eyw
       lszw = grid%szw
       lezw = grid%ezw

       lsxus = grid%sxus
       lexus = grid%exus
       lsyus = grid%syus
       leyus = grid%eyus
       lszus = grid%szus
       lezus = grid%ezus

       lsxvs = grid%sxvs
       lexvs = grid%exvs
       lsyvs = grid%syvs
       leyvs = grid%eyvs
       lszvs = grid%szvs
       lezvs = grid%ezvs

       lsxws = grid%sxws
       lexws = grid%exws
       lsyws = grid%syws
       leyws = grid%eyws
       lszws = grid%szws
       lezws = grid%ezws

       !--------------------------------------------------------------------------
       ! alloc
       !--------------------------------------------------------------------------
       allocate(u(lsxu-lgx:lexu+lgx,lsyu-lgy:leyu+lgy,lszu-lgz:lezu+lgz))
       allocate(v(lsxv-lgx:lexv+lgx,lsyv-lgy:leyv+lgy,lszv-lgz:lezv+lgz))
       u=0
       v=0
       if (grid%dim==3) then 
          allocate(w(lsxw-lgx:lexw+lgx,lsyw-lgy:leyw+lgy,lszw-lgz:lezw+lgz))
          w=0
       end if
       !--------------------------------------------------------------------------

       !--------------------------------------------------------------------------
       ! sol --> u,v,w
       !--------------------------------------------------------------------------
       ! U component
       !--------------------------------------------------------------------------
       do kk=lszus,lezus
          do jj=lsyus,leyus
             do ii=lsxus,lexus
                i=ii-lsxus
                j=jj-lsyus
                k=kk-lszus
                l=i+j*(lexus-lsxus+1)+(k*(lexus-lsxus+1)*(leyus-lsyus+1)+1)**m
                u(ii,jj,kk)=sol(l)
             enddo
          enddo
       end do
       !--------------------------------------------------------------------------
       ! V component
       !--------------------------------------------------------------------------
       do kk=lszvs,lezvs
          do jj=lsyvs,leyvs
             do ii=lsxvs,lexvs
                i=ii-lsxvs
                j=jj-lsyvs
                k=kk-lszvs
                l=i+j*(lexvs-lsxvs+1)+(k*(lexvs-lsxvs+1)*(leyvs-lsyvs+1)+1)**m+grid%nb%tot%u
                v(ii,jj,kk)=sol(l)
             enddo
          enddo
       end do
       !--------------------------------------------------------------------------
       ! W component
       !--------------------------------------------------------------------------
       if (grid%dim==3) then 
          do kk=lszws,lezws
             do jj=lsyws,leyws
                do ii=lsxws,lexws
                   i=ii-lsxws
                   j=jj-lsyws
                   k=kk-lszws
                   l=i+j*(lexws-lsxws+1)+(k*(lexws-lsxws+1)*(leyws-lsyws+1)+1)**m+grid%nb%tot%uv
                   w(ii,jj,kk)=sol(l)
                enddo
             enddo
          end do
       end if
       !--------------------------------------------------------------------------

       !--------------------------------------------------------------------------
       ! mpi lexchange
       !--------------------------------------------------------------------------
       call comm_mpi_uvw(u,v,w)
       !--------------------------------------------------------------------------

       !--------------------------------------------------------------------------
       ! u,v,w --> sol
       !--------------------------------------------------------------------------
       ! U component
       !--------------------------------------------------------------------------
       do kk=lszus,lezus
          do jj=lsyus,leyus
             do ii=lsxus,lexus
                i=ii-lsxus
                j=jj-lsyus
                k=kk-lszus
                l=i+j*(lexus-lsxus+1)+(k*(lexus-lsxus+1)*(leyus-lsyus+1)+1)**m
                sol(l)=u(ii,jj,kk)
             enddo
          enddo
       end do
       !--------------------------------------------------------------------------
       ! V component
       !--------------------------------------------------------------------------
       do kk=lszvs,lezvs
          do jj=lsyvs,leyvs
             do ii=lsxvs,lexvs
                i=ii-lsxvs
                j=jj-lsyvs
                k=kk-lszvs
                l=i+j*(lexvs-lsxvs+1)+(k*(lexvs-lsxvs+1)*(leyvs-lsyvs+1)+1)**m+grid%nb%tot%u
                sol(l)=v(ii,jj,kk)
             enddo
          enddo
       end do
       !--------------------------------------------------------------------------
       ! W component
       !--------------------------------------------------------------------------
       if (grid%dim==3) then 
          do kk=lszws,lezws
             do jj=lsyws,leyws
                do ii=lsxws,lexws
                   i=ii-lsxws
                   j=jj-lsyws
                   k=kk-lszws
                   l=i+j*(lexws-lsxws+1)+(k*(lexws-lsxws+1)*(leyws-lsyws+1)+1)**m+grid%nb%tot%uv
                   sol(l)=w(ii,jj,kk)
                enddo
             enddo
          end do
       end if
       !--------------------------------------------------------------------------
    case(SOLVE_UVWP,SOLVE_UVP)
       !--------------------------------------------------------------------------
       ! uvwp
       !--------------------------------------------------------------------------

       lsxu  = grid%sxu
       lexu  = grid%exu
       lsyu  = grid%syu
       leyu  = grid%eyu
       lszu  = grid%szu
       lezu  = grid%ezu

       lsxv  = grid%sxv
       lexv  = grid%exv
       lsyv  = grid%syv
       leyv  = grid%eyv
       lszv  = grid%szv
       lezv  = grid%ezv

       lsxw  = grid%sxw
       lexw  = grid%exw
       lsyw  = grid%syw
       leyw  = grid%eyw
       lszw  = grid%szw
       lezw  = grid%ezw

       lsx   = grid%sx
       lex   = grid%ex
       lsy   = grid%sy
       ley   = grid%ey
       lsz   = grid%sz
       lez   = grid%ez

       lsxus = grid%sxus
       lexus = grid%exus
       lsyus = grid%syus
       leyus = grid%eyus
       lszus = grid%szus
       lezus = grid%ezus

       lsxvs = grid%sxvs
       lexvs = grid%exvs
       lsyvs = grid%syvs
       leyvs = grid%eyvs
       lszvs = grid%szvs
       lezvs = grid%ezvs

       lsxws = grid%sxws
       lexws = grid%exws
       lsyws = grid%syws
       leyws = grid%eyws
       lszws = grid%szws
       lezws = grid%ezws

       lsxs  = grid%sxs
       lexs  = grid%exs
       lsys  = grid%sys
       leys  = grid%eys
       lszs  = grid%szs
       lezs  = grid%ezs

       !--------------------------------------------------------------------------
       ! alloc
       !--------------------------------------------------------------------------
       allocate(u(lsxu-lgx:lexu+lgx,lsyu-lgy:leyu+lgy,lszu-lgz:lezu+lgz))
       allocate(v(lsxv-lgx:lexv+lgx,lsyv-lgy:leyv+lgy,lszv-lgz:lezv+lgz))
       allocate(p(lsx-lgx:lex+lgx,lsy-lgy:ley+lgy,lsz-lgz:lez+lgz))
       u=0
       v=0
       p=0
       if (grid%dim==3) then 
          allocate(w(lsxw-lgx:lexw+lgx,lsyw-lgy:leyw+lgy,lszw-lgz:lezw+lgz))
          w=0
       end if
       !--------------------------------------------------------------------------

       !--------------------------------------------------------------------------
       ! sol --> u,v,w
       !--------------------------------------------------------------------------
       ! U component
       !--------------------------------------------------------------------------
       do kk=lszus,lezus
          do jj=lsyus,leyus
             do ii=lsxus,lexus
                i=ii-lsxus
                j=jj-lsyus
                k=kk-lszus
                l=i+j*(lexus-lsxus+1)+(k*(lexus-lsxus+1)*(leyus-lsyus+1)+1)**m
                u(ii,jj,kk)=sol(l)
             enddo
          enddo
       end do
       !--------------------------------------------------------------------------
       ! V component
       !--------------------------------------------------------------------------
       do kk=lszvs,lezvs
          do jj=lsyvs,leyvs
             do ii=lsxvs,lexvs
                i=ii-lsxvs
                j=jj-lsyvs
                k=kk-lszvs
                l=i+j*(lexvs-lsxvs+1)+(k*(lexvs-lsxvs+1)*(leyvs-lsyvs+1)+1)**m+grid%nb%tot%u
                v(ii,jj,kk)=sol(l)
             enddo
          enddo
       end do
       !--------------------------------------------------------------------------
       ! W component
       !--------------------------------------------------------------------------
       if (grid%dim==3) then 
          do kk=lszws,lezws
             do jj=lsyws,leyws
                do ii=lsxws,lexws
                   i=ii-lsxws
                   j=jj-lsyws
                   k=kk-lszws
                   l=i+j*(lexws-lsxws+1)+(k*(lexws-lsxws+1)*(leyws-lsyws+1)+1)**m+grid%nb%tot%uv
                   w(ii,jj,kk)=sol(l)
                enddo
             enddo
          end do
       end if
       !--------------------------------------------------------------------------
       ! sol --> p
       !--------------------------------------------------------------------------
       do kk=lszs,lezs
          do jj=lsys,leys
             do ii=lsxs,lexs
                i=ii-lsxs
                j=jj-lsys
                k=kk-lszs
                l=i+j*(lexs-lsxs+1)+(k*(lexs-lsxs+1)*(leys-lsys+1)+1)**m+grid%nb%tot%uvw
                p(ii,jj,kk)=sol(l)
             enddo
          enddo
       end do
       !--------------------------------------------------------------------------

       !--------------------------------------------------------------------------
       ! mpi lexchange
       !--------------------------------------------------------------------------
       call comm_mpi_sca(p)
       call comm_mpi_uvw(u,v,w)
       !--------------------------------------------------------------------------
       
       !--------------------------------------------------------------------------
       ! u,v,w --> sol
       !--------------------------------------------------------------------------
       ! U component
       !--------------------------------------------------------------------------
       do kk=lszus,lezus
          do jj=lsyus,leyus
             do ii=lsxus,lexus
                i=ii-lsxus
                j=jj-lsyus
                k=kk-lszus
                l=i+j*(lexus-lsxus+1)+(k*(lexus-lsxus+1)*(leyus-lsyus+1)+1)**m
                sol(l)=u(ii,jj,kk)
             enddo
          enddo
       end do
       !--------------------------------------------------------------------------
       ! V component
       !--------------------------------------------------------------------------
       do kk=lszvs,lezvs
          do jj=lsyvs,leyvs
             do ii=lsxvs,lexvs
                i=ii-lsxvs
                j=jj-lsyvs
                k=kk-lszvs
                l=i+j*(lexvs-lsxvs+1)+(k*(lexvs-lsxvs+1)*(leyvs-lsyvs+1)+1)**m+grid%nb%tot%u
                sol(l)=v(ii,jj,kk)
             enddo
          enddo
       end do
       !--------------------------------------------------------------------------
       ! W component
       !--------------------------------------------------------------------------
       if (grid%dim==3) then 
          do kk=lszws,lezws
             do jj=lsyws,leyws
                do ii=lsxws,lexws
                   i=ii-lsxws
                   j=jj-lsyws
                   k=kk-lszws
                   l=i+j*(lexws-lsxws+1)+(k*(lexws-lsxws+1)*(leyws-lsyws+1)+1)**m+grid%nb%tot%uv
                   sol(l)=w(ii,jj,kk)
                enddo
             enddo
          end do
       end if
       !--------------------------------------------------------------------------
       ! sol --> p
       !--------------------------------------------------------------------------
       do kk=lszs,lezs
          do jj=lsys,leys
             do ii=lsxs,lexs
                i=ii-lsxs
                j=jj-lsys
                k=kk-lszs
                l=i+j*(lexs-lsxs+1)+(k*(lexs-lsxs+1)*(leys-lsys+1)+1)**m+grid%nb%tot%uvw
                sol(l)=p(ii,jj,kk)
             enddo
          enddo
       end do
       !--------------------------------------------------------------------------

    end select

    call compute_time(TIMER_END,"[sub] solver_comm_mpi")

  end subroutine solver_comm_mpi

end module mod_solver_new_num
