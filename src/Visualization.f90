!========================================================================
!**
!**   NAME       : InOut.f90
!**
!**   AUTHOR     : Stéphane Vincent
!**                Benoît Trouette
!**
!**   FUNCTION   : subroutines for I/O management
!**
!**   DATES      : from : Jan, 2022
!**
!========================================================================

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#include "precomp.h"
module mod_InOut
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  !*******************************************************************************!
  !   Modules                                                                 
  !*******************************************************************************!
  use mod_Parameters,only: dim,rank,xmin,xmax,ymin,ymax,zmin,zmax,  &
       & file_vtk,file_tec,file_egld,file_egld_prtcl,file_vtk_prtcl
  use mod_Constants
  use mod_struct_grid
  use mod_struct_particle_tracking
  use mod_mpi
  use mod_tecplot
  use mod_timers
  !*******************************************************************************!

contains

  subroutine Write_fields(itime,time,mesh,u,v,w,                      &
       & sca01,sca02,sca03,sca04,sca05,sca06,sca07,sca08,sca09,sca10, &
       & sca11,sca12,sca13,sca14,sca15,sca16,sca17,sca18,sca19,sca20)
    !*******************************************************************************!
    implicit none
    !---------------------------------------------------------------------
    ! Global variables                                            
    !---------------------------------------------------------------------
    integer, intent(in)                                             :: itime
    real(8), intent(in)                                             :: time
    type(grid_t), intent(in)                                        :: mesh
    real(8), dimension(:,:,:), allocatable, intent(in)              :: u,v,w
    real(8), dimension(:,:,:), allocatable, intent(inout), optional ::  &
         & sca01,sca02,sca03,sca04,sca05,sca06,sca07,sca08,sca09,sca10, &
         & sca11,sca12,sca13,sca14,sca15,sca16,sca17,sca18,sca19,sca20
    !---------------------------------------------------------------------
    ! Local variables                                             
    !---------------------------------------------------------------------
    integer                                                         :: nbVar,cVar
    real(8), dimension(:,:,:,:), allocatable                        :: var
    integer, dimension(:), allocatable                              :: varType
    character(len=80), dimension(:), allocatable                    :: varName
    character(len=80)                                               :: caseName
    character(len=80)                                               :: iterName
    character(len=80)                                               :: varIterName
    !---------------------------------------------------------------------
    
    call compute_time(TIMER_START,"[sub] Write_fields")
    
    !---------------------------------------------------------------------
    ! VTK
    !---------------------------------------------------------------------
    if (itime>0) then 
       if (file_vtk%dt>0) then
          if (time>=file_vtk%time) then
             file_vtk%time=time+file_vtk%dt
             file_vtk%write=.true.
          end if
       else 
          if (file_vtk%iter>0) then
             if (itime==1.or.modulo(itime,file_vtk%iter)==0) then
                file_vtk%write=.true.
             end if
          end if
       end if
    end if
    !---------------------------------------------------------------------
    if (file_vtk%write) then
       call Write_field_vtk(666,itime,time,mesh,u,v,w,                     &
            & sca01,sca02,sca03,sca04,sca05,sca06,sca07,sca08,sca09,sca10, &
            & sca11,sca12,sca13,sca14,sca15,sca16,sca17,sca18,sca19,sca20)
       file_vtk%write=.false.
    end if
    !---------------------------------------------------------------------

    !---------------------------------------------------------------------
    ! Ensight Gold
    !---------------------------------------------------------------------
    if (itime>0) then  ! ne gere pas encore l'ajout d'une variable ecrite au cours du calcul
       if (file_egld%dt>0) then
          if (time>=file_egld%time) then
             file_egld%time=time+file_egld%dt
             file_egld%write=.true.
          end if
       else 
          if (file_egld%iter>0) then
             if (itime==1.or.modulo(itime,file_egld%iter)==0) then
                file_egld%write=.true.
             end if
          end if
       end if
    end if
    !---------------------------------------------------------------------
    if (file_egld%write) then
       call Write_field_ensight_gold(666,itime,time,mesh,u,v,w,            &
            & sca01,sca02,sca03,sca04,sca05,sca06,sca07,sca08,sca09,sca10, &
            & sca11,sca12,sca13,sca14,sca15,sca16,sca17,sca18,sca19,sca20)
       file_egld%write=.false.
    end if
    !---------------------------------------------------------------------
    
    
    !---------------------------------------------------------------------
    ! PLT (Tecplot)
    !---------------------------------------------------------------------
    if (itime>0) then 
       if (file_tec%dt>0) then
          if (time>=file_tec%time) then
             file_tec%time=time+file_tec%dt
             file_tec%write=.true.
          end if
       else 
          if (file_tec%iter>0) then
             if (itime==1.or.modulo(itime,file_tec%iter)==0) then
                file_tec%write=.true.
             end if
          end if
       end if
    end if
    !---------------------------------------------------------------------
    if (file_tec%write) then
       call Write_field_tec(666,itime,time,mesh,u,v,w,                     &
               & sca01,sca02,sca03,sca04,sca05,sca06,sca07,sca08,sca09,sca10, &
               & sca11,sca12,sca13,sca14,sca15,sca16,sca17,sca18,sca19,sca20)
       file_tec%write=.false.
    end if
    !---------------------------------------------------------------------
    
    call compute_time(TIMER_END,"[sub] Write_fields")

  end subroutine Write_fields
  !*******************************************************************************!

  subroutine Write_particles(itime,time,LPart,nPart,SPart,nPartS,VPart,nPartV,CPart,nPartC)
    !*******************************************************************************!
    implicit none
    !---------------------------------------------------------------------
    ! Global variables                                            
    !---------------------------------------------------------------------
    integer, intent(in)                                     :: itime,nPart,nPartS,nPartV,nPartC
    real(8), intent(in)                                     :: time
    type(particle_t), allocatable, dimension(:), intent(in) :: LPart,SPart,VPart,CPart
    !---------------------------------------------------------------------
    ! Local variables                                             
    !---------------------------------------------------------------------
    !---------------------------------------------------------------------


    !---------------------------------------------------------------------
    ! Ensight gold
    !---------------------------------------------------------------------

    if (itime>0) then 
       if (file_egld_prtcl%dt>0) then
          if (time>=file_egld_prtcl%time) then
             file_egld_prtcl%time=time+file_egld_prtcl%dt
             file_egld_prtcl%write=.true.
          end if
       else 
          if (file_egld_prtcl%iter>0) then
             if (itime==1.or.modulo(itime,file_egld_prtcl%iter)==0) then
                file_egld_prtcl%write=.true.
             end if
          end if
       end if
    end if
    !---------------------------------------------------------------------
    if (file_egld_prtcl%write) then
       if (allocated(LPart)) then 
          call write_prtcl_ensight_gold(666,itime,time,LPart,nPart)
       elseif (allocated(SPart)) then
          call write_prtcl_ensight_gold(666,itime,time,SPart,nPartS)
       elseif (allocated(VPart)) then 
          call write_prtcl_ensight_gold(666,itime,time,VPart,nPartV)
       elseif (allocated(CPart)) then 
          call write_prtcl_ensight_gold(666,itime,time,CPart,nPartC)
       end if
       file_egld_prtcl%write=.false.
    end if
    !---------------------------------------------------------------------

    !---------------------------------------------------------------------
    ! VTK
    !---------------------------------------------------------------------
    if (itime>0) then 
       if (file_vtk_prtcl%dt>0) then
          if (time>=file_vtk_prtcl%time) then
             file_vtk_prtcl%time=time+file_vtk_prtcl%dt
             file_vtk_prtcl%write=.true.
          end if
       else 
          if (file_vtk_prtcl%iter>0) then
             if (itime==1.or.modulo(itime,file_vtk_prtcl%iter)==0) then
                file_vtk_prtcl%write=.true.
             end if
          end if
       end if
    end if
    !---------------------------------------------------------------------
    if (file_vtk_prtcl%write) then
       if (allocated(LPart)) then 
          call write_particle_vtk(666,itime,time,LPart,nPart)
       elseif (allocated(SPart)) then
          call write_particle_vtk(666,itime,time,SPart,nPartS)
       elseif (allocated(VPart)) then 
          call write_particle_vtk(666,itime,time,VPart,nPartV)
       end if
       file_vtk_prtcl%write=.false.
    end if
    !---------------------------------------------------------------------

  end subroutine Write_particles
  !*******************************************************************************!


  !*******************************************************************************!
  subroutine Write_field_tec(unit,itime,time,mesh,u,v,w,              &
       & sca01,sca02,sca03,sca04,sca05,sca06,sca07,sca08,sca09,sca10, &
       & sca11,sca12,sca13,sca14,sca15,sca16,sca17,sca18,sca19,sca20)
    !*******************************************************************************!
    implicit none
    !---------------------------------------------------------------------
    ! Global variables                                            
    !---------------------------------------------------------------------
    integer, intent(in)                                             :: unit,itime
    real(8), intent(in)                                             :: time
    type(grid_t), intent(in)                                        :: mesh
    real(8), dimension(:,:,:), allocatable, intent(in)              :: u,v,w
    real(8), dimension(:,:,:), allocatable, intent(inout), optional ::  &
         & sca01,sca02,sca03,sca04,sca05,sca06,sca07,sca08,sca09,sca10, &
         & sca11,sca12,sca13,sca14,sca15,sca16,sca17,sca18,sca19,sca20
    !---------------------------------------------------------------------
    ! Local variables                                             
    !---------------------------------------------------------------------
    integer                                                      :: i,j,k
    integer                                                      :: lex,ley,lez
    character(500)                                               :: name
    character(500)                                               :: File
    !---------------------------------------------------------------------

    call Write_binary_field_tec(unit,itime,time,mesh,u,v,w,             &
         & sca01,sca02,sca03,sca04,sca05,sca06,sca07,sca08,sca09,sca10, &
         & sca11,sca12,sca13,sca14,sca15,sca16,sca17,sca18,sca19,sca20)
    return

  end subroutine Write_field_tec
  !*******************************************************************************!

  subroutine Write_binary_field_tec(unit,itime,time,mesh,u,v,w,       &
       & sca01,sca02,sca03,sca04,sca05,sca06,sca07,sca08,sca09,sca10, &
       & sca11,sca12,sca13,sca14,sca15,sca16,sca17,sca18,sca19,sca20)
    !*******************************************************************************!
    implicit none
    !---------------------------------------------------------------------
    ! Global variables                                            
    !---------------------------------------------------------------------
    integer, intent(in)                                             :: unit,itime
    real(8), intent(in)                                             :: time
    type(grid_t), intent(in)                                        :: mesh
    real(8), dimension(:,:,:), allocatable, intent(in)              :: u,v,w
    real(8), dimension(:,:,:), allocatable, intent(inout), optional ::  &
         & sca01,sca02,sca03,sca04,sca05,sca06,sca07,sca08,sca09,sca10, &
         & sca11,sca12,sca13,sca14,sca15,sca16,sca17,sca18,sca19,sca20
    !---------------------------------------------------------------------
    ! Local variables                                             
    !---------------------------------------------------------------------
    integer                                                      :: i,j,k
    integer                                                      :: lsx,lsy,lsz
    integer                                                      :: lex,ley,lez
    character(500)                                               :: name
    character(500)                                               :: File
    !---------------------------------------------------------------------
    ! Local intrisic variables of the subroutine
    !---------------------------------------------------------------------
    ! define a tecplot object
    !---------------------------------------------------------------------
    type(tecplot_time_file)                  :: plt_file
    integer                                  :: num_of_variables
    integer                                  :: n,ii,jj,kk
    integer, allocatable                     :: locations(:)
    integer, allocatable                     :: type_list(:)
    integer, allocatable                     :: shared_list(:)
    real(4), dimension(:,:,:,:), allocatable :: your_datas
    character(len=100)                       :: filename
    !---------------------------------------------------------------------

    num_of_variables=0

    !---------------------------------------------------------------------
    ! mpi 
    !---------------------------------------------------------------------
    if (nproc>1) then
       if (present(sca01)) then
          if (allocated(sca01)) call comm_mpi_sca(sca01)
       end if
       if (present(sca02)) then
          if (allocated(sca02)) call comm_mpi_sca(sca02)
       end if
       if(present(sca03)) then
          if (allocated(sca03)) call comm_mpi_sca(sca03)
       end if
       if (present(sca04)) then
          if (allocated(sca04)) call comm_mpi_sca(sca04)
       end if
       if (present(sca05)) then
          if (allocated(sca05)) call comm_mpi_sca(sca05)
       end if
       if (present(sca06)) then
          if (allocated(sca06)) call comm_mpi_sca(sca06)
       end if
       if (present(sca07)) then
          if (allocated(sca07)) call comm_mpi_sca(sca07)
       end if
       if (present(sca08)) then
          if (allocated(sca08)) call comm_mpi_sca(sca08)
       end if
       if (present(sca09)) then
          if (allocated(sca09)) call comm_mpi_sca(sca09)
       end if
       if (present(sca10)) then
          if (allocated(sca10)) call comm_mpi_sca(sca10)
       end if
       if (present(sca11)) then
          if (allocated(sca11)) call comm_mpi_sca(sca11)
       end if
       if (present(sca12)) then
          if (allocated(sca12)) call comm_mpi_sca(sca12)
       end if
       if (present(sca13)) then
          if (allocated(sca13)) call comm_mpi_sca(sca13)
       end if
       if (present(sca14)) then
          if (allocated(sca14)) call comm_mpi_sca(sca14)
       end if
       if (present(sca15)) then
          if (allocated(sca15)) call comm_mpi_sca(sca15)
       end if
       if (present(sca16)) then
          if (allocated(sca16)) call comm_mpi_sca(sca16)
       end if
       if (present(sca17)) then
          if (allocated(sca17)) call comm_mpi_sca(sca17)
       end if
       if (present(sca18)) then
          if (allocated(sca18)) call comm_mpi_sca(sca18)
       end if
       if (present(sca19)) then
          if (allocated(sca19)) call comm_mpi_sca(sca19)
       end if
       if (present(sca20)) then
          if (allocated(sca20)) call comm_mpi_sca(sca20)
       end if
    end if
    !---------------------------------------------------------------------

    !-------------------------------------------
    ! initialization of final indexes
    !-------------------------------------------
    lsx=mesh%sx
    lsy=mesh%sy
    lsz=mesh%sz
    lex=mesh%ex
    ley=mesh%ey
    lez=mesh%ez
    !-------------------------------------------


    if (nproc==1) then
       write(name,'(i8.8)') itime
       File="fields_i"//trim(adjustl(name))//".plt"
    else
       write(name,'(i8.8)') itime
       File="fields_i"//trim(adjustl(name))
       write(name,'(i6.6)') rank
       File=trim(adjustl(File))//"_p"//trim(adjustl(name))//".plt"
       !-------------------------------------------
       if (lex/=gex) lex=lex+1
       if (ley/=gey) ley=ley+1
       if (lez/=gez) lez=lez+1
       if (dim==2) lez=mesh%sz
       !------------------------------------------
    end if

    !-------------------------------------------
    if (dim==2) then
       name='X,Y,U,V'
       num_of_variables=num_of_variables+4
    else
       name='X,Y,Z,U,V,W'
       num_of_variables=num_of_variables+6
    end if
    n=num_of_variables
    !-------------------------------------------

    if (present(sca01)) then
       if (allocated(sca01)) then
          name=trim(adjustl(name))//',sca01'
          num_of_variables=num_of_variables+1
       end if
    end if
    if (present(sca02)) then
       if (allocated(sca02)) then
          name=trim(adjustl(name))//',sca02'
          num_of_variables=num_of_variables+1
       end if
    end if
    if (present(sca03)) then
       if (allocated(sca03)) then
          name=trim(adjustl(name))//',sca03'
          num_of_variables=num_of_variables+1
       end if
    end if
    if (present(sca04)) then
       if (allocated(sca04)) then
          name=trim(adjustl(name))//',sca04'
          num_of_variables=num_of_variables+1
       end if
    end if
    if (present(sca05)) then
       if (allocated(sca05)) then
          name=trim(adjustl(name))//',sca05'
          num_of_variables=num_of_variables+1
       end if
    end if
    if (present(sca06)) then
       if (allocated(sca06)) then
          name=trim(adjustl(name))//',sca06'
          num_of_variables=num_of_variables+1
       end if
    end if
    if (present(sca07)) then
       if (allocated(sca07)) then
          name=trim(adjustl(name))//',sca07'
          num_of_variables=num_of_variables+1
       end if
    end if
    if (present(sca08)) then
       if (allocated(sca08)) then
          name=trim(adjustl(name))//',sca08'
          num_of_variables=num_of_variables+1
       end if
    end if
    if (present(sca09)) then
       if (allocated(sca09)) then
          name=trim(adjustl(name))//',sca09'
          num_of_variables=num_of_variables+1
       end if
    end if
    if (present(sca10)) then
       if (allocated(sca10)) then
          name=trim(adjustl(name))//',sca10'
          num_of_variables=num_of_variables+1
       end if
    end if
    if (present(sca11)) then
       if (allocated(sca11)) then
          name=trim(adjustl(name))//',sca11'
          num_of_variables=num_of_variables+1
       end if
    end if
    if (present(sca12)) then
       if (allocated(sca12)) then
          name=trim(adjustl(name))//',sca12'
          num_of_variables=num_of_variables+1
       end if
    end if
    if (present(sca13)) then
       if (allocated(sca13)) then
          name=trim(adjustl(name))//',sca13'
          num_of_variables=num_of_variables+1
       end if
    end if
    if (present(sca14)) then
       if (allocated(sca14)) then
          name=trim(adjustl(name))//',sca14'
          num_of_variables=num_of_variables+1
       end if
    end if
    if (present(sca15)) then
       if (allocated(sca15)) then
          name=trim(adjustl(name))//',sca15'
          num_of_variables=num_of_variables+1
       end if
    end if
    if (present(sca16)) then
       if (allocated(sca16)) then
          name=trim(adjustl(name))//',sca16'
          num_of_variables=num_of_variables+1
       end if
    end if
    if (present(sca17)) then
       if (allocated(sca17)) then
          name=trim(adjustl(name))//',sca17'
          num_of_variables=num_of_variables+1
       end if
    end if
    if (present(sca18)) then
       if (allocated(sca18)) then
          name=trim(adjustl(name))//',sca18'
          num_of_variables=num_of_variables+1
       end if
    end if
    if (present(sca19)) then
       if (allocated(sca19)) then
          name=trim(adjustl(name))//',sca19'
          num_of_variables=num_of_variables+1
       end if
    end if
    if (present(sca20)) then
       if (allocated(sca20)) then
          name=trim(adjustl(name))//',sca20'
          num_of_variables=num_of_variables+1
       end if
    end if

    if (.not.allocated(your_datas))  allocate(your_datas(lsx+1:lex+1,lsy+1:ley+1,lsz+1:lez+1,num_of_variables))
    if (.not.allocated(locations))   allocate(locations(num_of_variables))
    if (.not.allocated(type_list))   allocate(type_list(num_of_variables))
    if (.not.allocated(shared_list)) allocate(shared_list(num_of_variables))

    locations   = 0
    shared_list = -1
    type_list   = 1


    if (dim==2) then
       do k=lsz,lez
          do j=lsy,ley
             do i=lsx,lex
                kk=k+1
                jj=j+1
                ii=i+1
                your_datas(ii,jj,kk,1)=mesh%x(i)
                your_datas(ii,jj,kk,3)=(mesh%dxu2(i+1)*u(i,j,k)+mesh%dxu2(i)*u(i+1,j,k))/mesh%dx(i)
             end do
          end do
       end do
       do k=lsz,lez
          do j=lsy,ley
             do i=lsx,lex
                kk=k+1
                jj=j+1
                ii=i+1
                your_datas(ii,jj,kk,2)=mesh%y(j)
                your_datas(ii,jj,kk,4)=(mesh%dyv2(j+1)*v(i,j,k)+mesh%dyv2(j)*v(i,j+1,k))/mesh%dy(j)
             end do
          end do
       end do
    else
       do k=lsz,lez
          do j=lsy,ley
             do i=lsx,lex
                kk=k+1
                jj=j+1
                ii=i+1
                your_datas(ii,jj,kk,1)=mesh%x(i)
                your_datas(ii,jj,kk,4)=(mesh%dxu2(i+1)*u(i,j,k)+mesh%dxu2(i)*u(i+1,j,k))/mesh%dx(i)
             end do
          end do
       end do
       do k=lsz,lez
          do j=lsy,ley
             do i=lsx,lex
                kk=k+1
                jj=j+1
                ii=i+1
                your_datas(ii,jj,kk,2)=mesh%y(j)
                your_datas(ii,jj,kk,5)=(mesh%dyv2(j+1)*v(i,j,k)+mesh%dyv2(j)*v(i,j+1,k))/mesh%dy(j)
             end do
          end do
       end do
       do k=lsz,lez
          do j=lsy,ley
             do i=lsx,lex
                kk=k+1
                jj=j+1
                ii=i+1
                your_datas(ii,jj,kk,3)=mesh%z(k)
                your_datas(ii,jj,kk,6)=(mesh%dzw2(k+1)*w(i,j,k)+mesh%dzw2(k)*w(i,j,k+1))/mesh%dz(k)
             end do
          end do
       end do
    end if

    if (present(sca01)) then
       if (allocated(sca01)) then
          n=n+1
          do k=lsz,lez
             do j=lsy,ley
                do i=lsx,lex
                   kk=k+1
                   jj=j+1
                   ii=i+1
                   your_datas(ii,jj,kk,n) = sca01(i,j,k)
                end do
             end do
          end do
       end if
    end if
    if (present(sca02)) then
       if (allocated(sca02)) then
          n=n+1
          do k=lsz,lez
             do j=lsy,ley
                do i=lsx,lex
                   kk=k+1
                   jj=j+1
                   ii=i+1
                   your_datas(ii,jj,kk,n) = sca02(i,j,k)
                end do
             end do
          end do
       end if
    end if
    if (present(sca03)) then
       if (allocated(sca03)) then
          n=n+1
          do k=lsz,lez
             do j=lsy,ley
                do i=lsx,lex
                   kk=k+1
                   jj=j+1
                   ii=i+1
                   your_datas(ii,jj,kk,n) = sca03(i,j,k)
                end do
             end do
          end do
       end if
    end if
    if (present(sca04)) then
       if (allocated(sca04)) then
          n=n+1
          do k=lsz,lez
             do j=lsy,ley
                do i=lsx,lex
                   kk=k+1
                   jj=j+1
                   ii=i+1
                   your_datas(ii,jj,kk,n) = sca04(i,j,k)
                end do
             end do
          end do
       end if
    end if
    if (present(sca05)) then
       if (allocated(sca05)) then
          n=n+1
          do k=lsz,lez
             do j=lsy,ley
                do i=lsx,lex
                   kk=k+1
                   jj=j+1
                   ii=i+1
                   your_datas(ii,jj,kk,n) = sca05(i,j,k)
                end do
             end do
          end do
       end if
    end if
    if (present(sca06)) then
       if (allocated(sca06)) then
          n=n+1
          do k=lsz,lez
             do j=lsy,ley
                do i=lsx,lex
                   kk=k+1
                   jj=j+1
                   ii=i+1
                   your_datas(ii,jj,kk,n) = sca06(i,j,k)
                end do
             end do
          end do
       end if
    end if
    if (present(sca07)) then
       if (allocated(sca07)) then
          n=n+1
          do k=lsz,lez
             do j=lsy,ley
                do i=lsx,lex
                   kk=k+1
                   jj=j+1
                   ii=i+1
                   your_datas(ii,jj,kk,n) = sca07(i,j,k)
                end do
             end do
          end do
       end if
    end if
    if (present(sca08)) then
       if (allocated(sca08)) then
          n=n+1
          do k=lsz,lez
             do j=lsy,ley
                do i=lsx,lex
                   kk=k+1
                   jj=j+1
                   ii=i+1
                   your_datas(ii,jj,kk,n) = sca08(i,j,k)
                end do
             end do
          end do
       end if
    end if
    if (present(sca09)) then
       if (allocated(sca09)) then
          n=n+1
          do k=lsz,lez
             do j=lsy,ley
                do i=lsx,lex
                   kk=k+1
                   jj=j+1
                   ii=i+1
                   your_datas(ii,jj,kk,n) = sca09(i,j,k)
                end do
             end do
          end do
       end if
    end if
    if (present(sca10)) then
       if (allocated(sca10)) then
          n=n+1
          do k=lsz,lez
             do j=lsy,ley
                do i=lsx,lex
                   kk=k+1
                   jj=j+1
                   ii=i+1
                   your_datas(ii,jj,kk,n) = sca10(i,j,k)
                end do
             end do
          end do
       end if
    end if
    if (present(sca11)) then
       if (allocated(sca11)) then
          n=n+1
          do k=lsz,lez
             do j=lsy,ley
                do i=lsx,lex
                   kk=k+1
                   jj=j+1
                   ii=i+1
                   your_datas(ii,jj,kk,n) = sca11(i,j,k)
                end do
             end do
          end do
       end if
    end if
    if (present(sca12)) then
       if (allocated(sca12)) then
          n=n+1
          do k=lsz,lez
             do j=lsy,ley
                do i=lsx,lex
                   kk=k+1
                   jj=j+1
                   ii=i+1
                   your_datas(ii,jj,kk,n) = sca12(i,j,k)
                end do
             end do
          end do
       end if
    end if
    if (present(sca13)) then
       if (allocated(sca13)) then
          n=n+1
          do k=sz,lez
             do j=sy,ley
                do i=sx,lex
                   kk=k+1
                   jj=j+1
                   ii=i+1
                   your_datas(ii,jj,kk,n) = sca13(i,j,k)
                end do
             end do
          end do
       end if
    end if
    if (present(sca14)) then
       if (allocated(sca14)) then
          n=n+1
          do k=lsz,lez
             do j=lsy,ley
                do i=lsx,lex
                   kk=k+1
                   jj=j+1
                   ii=i+1
                   your_datas(ii,jj,kk,n) = sca14(i,j,k)
                end do
             end do
          end do
       end if
    end if
    if (present(sca15)) then
       if (allocated(sca15)) then
          n=n+1
          do k=lsz,lez
             do j=lsy,ley
                do i=lsx,lex
                   kk=k+1
                   jj=j+1
                   ii=i+1
                   your_datas(ii,jj,kk,n) = sca15(i,j,k)
                end do
             end do
          end do
       end if
    end if
    if (present(sca16)) then
       if (allocated(sca16)) then
          n=n+1
          do k=lsz,lez
             do j=lsy,ley
                do i=lsx,lex
                   kk=k+1
                   jj=j+1
                   ii=i+1
                   your_datas(ii,jj,kk,n) = sca16(i,j,k)
                end do
             end do
          end do
       end if
    end if
    if (present(sca17)) then
       if (allocated(sca17)) then
          n=n+1
          do k=lsz,lez
             do j=lsy,ley
                do i=lsx,lex
                   kk=k+1
                   jj=j+1
                   ii=i+1
                   your_datas(ii,jj,kk,n) = sca17(i,j,k)
                end do
             end do
          end do
       end if
    end if
    if (present(sca18)) then
       if (allocated(sca18)) then
          n=n+1
          do k=lsz,lez
             do j=lsy,ley
                do i=lsx,lex
                   kk=k+1
                   jj=j+1
                   ii=i+1
                   your_datas(ii,jj,kk,n) = sca18(i,j,k)
                end do
             end do
          end do
       end if
    end if
    if (present(sca19)) then
       if (allocated(sca19)) then
          n=n+1
          do k=lsz,lez
             do j=lsy,ley
                do i=lsx,lex
                   kk=k+1
                   jj=j+1
                   ii=i+1
                   your_datas(ii,jj,kk,n) = sca19(i,j,k)
                end do
             end do
          end do
       end if
    end if
    if (present(sca20)) then
       if (allocated(sca20)) then
          n=n+1
          do k=lsz,lez
             do j=lsy,ley
                do i=lsx,lex
                   kk=k+1
                   jj=j+1
                   ii=i+1
                   your_datas(ii,jj,kk,n) = sca20(i,j,k)
                end do
             end do
          end do
       end if
    end if

    ! 'x,y,z,u,v,w' is a string contains names of variables, must be divided by ','
    call plt_file%init(File,lex-lsx+1,ley-lsy+1,lez-lsz+1,'Tecplot File Title',trim(adjustl(name)))

    ! for each zone, call the two subroutines
    ! physics_time can be any value, it will only be used when there are more than 1 zone in a file.

    call plt_file%write_zone_header('zone name', real(time,4), 0, locations) 
    ! your_datas(:,:,:,1:3) =  x,y,z ! coordinates(Variable assignment is omitted in this example)
    ! your_datas(:,:,:,4:6) =  u,v,w ! datas (Variable assignment is omitted in this example)
    ! ALL datas are stored in sequence like (((x(ix,iy,iz),ix=1,nx),iy=1,ny),iz=1,nz)
    call plt_file%write_zone_data(type_list, shared_list, your_datas)

    ! before exit, you must call complete subroutine
    call plt_file%complete

    return

  end subroutine Write_binary_field_tec
  !*******************************************************************************!

  subroutine Write_field_ensight_gold(unit,itime,time,mesh,u,v,w,     &
       & sca01,sca02,sca03,sca04,sca05,sca06,sca07,sca08,sca09,sca10, &
       & sca11,sca12,sca13,sca14,sca15,sca16,sca17,sca18,sca19,sca20)
    !*******************************************************************************!
    implicit none
    !---------------------------------------------------------------------
    ! Global variables                                            
    !---------------------------------------------------------------------
    integer, intent(in)                                             :: unit,itime
    real(8), intent(in)                                             :: time
    type(grid_t), intent(in)                                        :: mesh
    real(8), dimension(:,:,:), allocatable, intent(in)              :: u,v,w
    real(8), dimension(:,:,:), allocatable, intent(inout), optional ::  &
         & sca01,sca02,sca03,sca04,sca05,sca06,sca07,sca08,sca09,sca10, &
         & sca11,sca12,sca13,sca14,sca15,sca16,sca17,sca18,sca19,sca20
    !---------------------------------------------------------------------
    ! Local variables                                             
    !---------------------------------------------------------------------
    integer                                                         :: i,j,k
    integer                                                         :: lsx,lsy,lsz
    integer                                                         :: lex,ley,lez
    integer                                                         :: lgx,lgy,lgz
    integer                                                         :: nbVar,cVar
    real(8), dimension(:,:,:,:), allocatable                        :: var
    integer, dimension(:), allocatable                              :: varType
    character(len=1)                                                :: imesh
    character(len=80), dimension(:), allocatable                    :: varName
    character(len=80)                                               :: caseName
    character(len=80)                                               :: iterName
    character(len=80)                                               :: varIterName
    !---------------------------------------------------------------------
    
    !---------------------------------------------------------------------
    ! Test ensight gold format 
    !---------------------------------------------------------------------


    
    !---------------------------------------------------------------------
    ! nbVar count
    !---------------------------------------------------------------------
    nbVar=0

    if (allocated(u)) then 
       if (file_egld%write_field%uvw) nbVar=nbVar+1
    end if
    
    if (present(sca01)) then
       if (allocated(sca01)) then
          if (file_egld%write_field%pres) nbVar=nbVar+1
       end if
    end if
    if (present(sca02)) then
       if (allocated(sca02)) then
          if (file_egld%write_field%div) nbVar=nbVar+1
       end if
    end if
    if (present(sca03)) then
       if (allocated(sca03)) then
          if (file_egld%write_field%tp) nbVar=nbVar+1
       end if
    end if
    if (present(sca04)) then
       if (allocated(sca04)) then
          if (file_egld%write_field%phase) nbVar=nbVar+1
       end if
    end if
    if (present(sca05)) then
       if (allocated(sca05)) then
          if (file_egld%write_field%obj) nbVar=nbVar+1
       end if
    end if
    if (present(sca06)) then
       if (allocated(sca06)) then
          if (file_egld%write_field%sca06) nbVar=nbVar+1
       end if
    end if
    if (present(sca07)) then
       if (allocated(sca07)) then
          if (file_egld%write_field%sca07) nbVar=nbVar+1
       end if
    end if
    if (present(sca08)) then
       if (allocated(sca08)) then
          if (file_egld%write_field%sca08) nbVar=nbVar+1
       end if
    end if
    if (present(sca09)) then
       if (allocated(sca09)) then
          if (file_egld%write_field%sca09) nbVar=nbVar+1
       end if
    end if
    if (present(sca10)) then
       if (allocated(sca10)) then
          if (file_egld%write_field%sca10) nbVar=nbVar+1
       end if
    end if
    if (present(sca11)) then
       if (allocated(sca11)) then
          if (file_egld%write_field%sca11) nbVar=nbVar+1
       end if
    end if
    if (present(sca12)) then
       if (allocated(sca12)) then
          if (file_egld%write_field%sca12) nbVar=nbVar+1
       end if
    end if
    if (present(sca13)) then
       if (allocated(sca13)) then
          if (file_egld%write_field%sca13) nbVar=nbVar+1
       end if
    end if
    if (present(sca14)) then
       if (allocated(sca14)) then
          if (file_egld%write_field%sca14) nbVar=nbVar+1
       end if
    end if
    if (present(sca15)) then
       if (allocated(sca15)) then
          if (file_egld%write_field%sca15) nbVar=nbVar+1
       end if
    end if
    if (present(sca16)) then
       if (allocated(sca16)) then
          if (file_egld%write_field%sca16) nbVar=nbVar+1
       end if
    end if
    if (present(sca17)) then
       if (allocated(sca17)) then
          if (file_egld%write_field%sca17) nbVar=nbVar+1
       end if
    end if
    if (present(sca18)) then
       if (allocated(sca18)) then
          if (file_egld%write_field%sca18) nbVar=nbVar+1
       end if
    end if
    if (present(sca19)) then
       if (allocated(sca19)) then
          if (file_egld%write_field%sca19) nbVar=nbVar+1
       end if
    end if
    if (present(sca20)) then
       if (allocated(sca20)) then
          if (file_egld%write_field%sca20) nbVar=nbVar+1
       end if
    end if
    !---------------------------------------------------------------------

    allocate(varName(nbVar))
    allocate(varType(nbVar))
    write(iterName,'(i10.10)') itime
    write(imesh,'(i1.1)') mesh%id
    cVar=0

    !-------------------------------------------
    ! initialization of final indexes
    !-------------------------------------------
    lgx=mesh%gx
    lgy=mesh%gy
    lgz=mesh%gz
    
    lsx=mesh%sx
    lsy=mesh%sy
    lsz=mesh%sz
    lex=mesh%ex
    ley=mesh%ey
    lez=mesh%ez
    !-------------------------------------------
    
    !---------------------------------------------------------------------
    ! velocity field
    !---------------------------------------------------------------------
    if (file_egld%write_field%uvw) then
       if (allocated(u)) then
          cVar=cVar+1
          allocate(var(3,lsx-lgx:lex+lgx,lsy-lgy:ley+lgy,lsz-lgz:lez+lgz)); var=0
          write(varName(cVar),'(a)') 'velocity'
          varType(cVar)=3

          do i=lsx-2,lex+2 ! +-2 for mpi periodicity
             var(1,i,:,:)=(mesh%dxu2(i+1)*u(i,:,:)+mesh%dxu2(i)*u(i+1,:,:))/mesh%dx(i)
          end do

          do j=lsy-2,ley+2
             var(2,:,j,:)=(mesh%dyv2(j+1)*v(:,j,:)+mesh%dyv2(j)*v(:,j+1,:))/mesh%dy(j)
          end do

          if (dim==3) then 
             do k=lsz-2,lez+2
                var(3,:,:,k)=(mesh%dzw2(k+1)*w(:,:,k)+mesh%dzw2(k)*w(:,:,k+1))/mesh%dz(k)
             end do
          end if

          varIterName=trim(adjustl(varName(cVar)))//trim(adjustl(iterName))
          call WriteEnsightVar(varType(cVar),var,varName(cVar),iterName,mesh)
          deallocate(var)
       end if
    end if
    !---------------------------------------------------------------------
    ! pression
    !---------------------------------------------------------------------
    if (file_egld%write_field%pres) then
       if (present(sca01)) then
          if (allocated(sca01)) then
             cVar=cVar+1
             allocate(var(1,lsx-lgx:lex+lgx,lsy-lgy:ley+lgy,lsz-lgz:lez+lgz))
             write(varName(cVar),'(a)') 'pressure'
             varType(cVar)=1
             var(1,:,:,:)=sca01
             varIterName=trim(adjustl(varName(cVar)))//trim(adjustl(iterName))
             call WriteEnsightVar(varType(cVar),var,varName(cVar),iterName,mesh)
             deallocate(var) 
          end if
       end if
    end if
    !---------------------------------------------------------------------
    ! divergence
    !---------------------------------------------------------------------
    if (file_egld%write_field%div) then
       if (present(sca02)) then
          if (allocated(sca02)) then
             cVar=cVar+1
             allocate(var(1,lsx-lgx:lex+lgx,lsy-lgy:ley+lgy,lsz-lgz:lez+lgz))
             write(varName(cVar),'(a)') 'divergence'
             varType(cVar)=1
             var(1,:,:,:)=sca02
             varIterName=trim(adjustl(varName(cVar)))//trim(adjustl(iterName))
             call WriteEnsightVar(varType(cVar),var,varName(cVar),iterName,mesh)
             deallocate(var) 
          end if
       end if
    end if
    !---------------------------------------------------------------------
    ! temperature
    !---------------------------------------------------------------------
    if (file_egld%write_field%tp) then
       if (present(sca03)) then
          if (allocated(sca03)) then
             cVar=cVar+1
             allocate(var(1,lsx-lgx:lex+lgx,lsy-lgy:ley+lgy,lsz-lgz:lez+lgz))
             write(varName(cVar),'(a)') 'temperature'
             varType(cVar)=1
             var(1,:,:,:)=sca03
             varIterName=trim(adjustl(varName(cVar)))//trim(adjustl(iterName))
             call WriteEnsightVar(varType(cVar),var,varName(cVar),iterName,mesh)
             deallocate(var) 
          end if
       end if
    end if
    !---------------------------------------------------------------------
    ! phase
    !---------------------------------------------------------------------
    if (file_egld%write_field%phase) then
       if (present(sca04)) then
          if (allocated(sca04)) then
             cVar=cVar+1
             allocate(var(1,lsx-lgx:lex+lgx,lsy-lgy:ley+lgy,lsz-lgz:lez+lgz))
             write(varName(cVar),'(a)') 'phase'
             varType(cVar)=1
             var(1,:,:,:)=sca04
             varIterName=trim(adjustl(varName(cVar)))//trim(adjustl(iterName))
             call WriteEnsightVar(varType(cVar),var,varName(cVar),iterName,mesh)
             deallocate(var) 
          end if
       end if
    end if
    !---------------------------------------------------------------------
    ! obj
    !---------------------------------------------------------------------
    if (file_egld%write_field%obj) then
       if (present(sca05)) then
          if (allocated(sca05)) then
             cVar=cVar+1
             allocate(var(1,lsx-lgx:lex+lgx,lsy-lgy:ley+lgy,lsz-lgz:lez+lgz))
             write(varName(cVar),'(a)') 'object'
             varType(cVar)=1
             var(1,:,:,:)=sca05
             varIterName=trim(adjustl(varName(cVar)))//trim(adjustl(iterName))
             call WriteEnsightVar(varType(cVar),var,varName(cVar),iterName,mesh)
             deallocate(var) 
          end if
       end if
    end if
    !---------------------------------------------------------------------
    ! sca06
    !---------------------------------------------------------------------
    if (file_egld%write_field%sca06) then
       if (present(sca06)) then
          if (allocated(sca06)) then
             cVar=cVar+1
             allocate(var(1,lsx-lgx:lex+lgx,lsy-lgy:ley+lgy,lsz-lgz:lez+lgz))
             write(varName(cVar),'(a)') 'sca06'
             varType(cVar)=1
             var(1,:,:,:)=sca06
             varIterName=trim(adjustl(varName(cVar)))//trim(adjustl(iterName))
             call WriteEnsightVar(varType(cVar),var,varName(cVar),iterName,mesh)
             deallocate(var) 
          end if
       end if
    end if
    !---------------------------------------------------------------------
    ! sca07
    !---------------------------------------------------------------------
    if (file_egld%write_field%sca07) then
       if (present(sca07)) then
          if (allocated(sca07)) then
             cVar=cVar+1
             allocate(var(1,lsx-lgx:lex+lgx,lsy-lgy:ley+lgy,lsz-lgz:lez+lgz))
             write(varName(cVar),'(a)') 'sca07'
             varType(cVar)=1
             var(1,:,:,:)=sca07
             varIterName=trim(adjustl(varName(cVar)))//trim(adjustl(iterName))
             call WriteEnsightVar(varType(cVar),var,varName(cVar),iterName,mesh)
             deallocate(var) 
          end if
       end if
    end if
    !---------------------------------------------------------------------
    ! sca08
    !---------------------------------------------------------------------
    if (file_egld%write_field%sca08) then
       if (present(sca08)) then
          if (allocated(sca08)) then
             cVar=cVar+1
             allocate(var(1,lsx-lgx:lex+lgx,lsy-lgy:ley+lgy,lsz-lgz:lez+lgz))
             write(varName(cVar),'(a)') 'sca08'
             varType(cVar)=1
             var(1,:,:,:)=sca08
             varIterName=trim(adjustl(varName(cVar)))//trim(adjustl(iterName))
             call WriteEnsightVar(varType(cVar),var,varName(cVar),iterName,mesh)
             deallocate(var) 
          end if
       end if
    end if
    !---------------------------------------------------------------------
    ! sca09
    !---------------------------------------------------------------------
    if (file_egld%write_field%sca09) then
       if (present(sca09)) then
          if (allocated(sca09)) then
             cVar=cVar+1
             allocate(var(1,lsx-lgx:lex+lgx,lsy-lgy:ley+lgy,lsz-lgz:lez+lgz))
             write(varName(cVar),'(a)') 'sca09'
             varType(cVar)=1
             var(1,:,:,:)=sca09
             varIterName=trim(adjustl(varName(cVar)))//trim(adjustl(iterName))
             call WriteEnsightVar(varType(cVar),var,varName(cVar),iterName,mesh)
             deallocate(var) 
          end if
       end if
    end if
    !---------------------------------------------------------------------
    ! sca10
    !---------------------------------------------------------------------
    if (file_egld%write_field%sca10) then
       if (present(sca10)) then
          if (allocated(sca10)) then
             cVar=cVar+1
             allocate(var(1,lsx-lgx:lex+lgx,lsy-lgy:ley+lgy,lsz-lgz:lez+lgz))
             write(varName(cVar),'(a)') 'sca10'
             varType(cVar)=1
             var(1,:,:,:)=sca10
             varIterName=trim(adjustl(varName(cVar)))//trim(adjustl(iterName))
             call WriteEnsightVar(varType(cVar),var,varName(cVar),iterName,mesh)
             deallocate(var) 
          end if
       end if
    end if
    !---------------------------------------------------------------------
    ! sca11
    !---------------------------------------------------------------------
    if (file_egld%write_field%sca11) then
       if (present(sca11)) then
          if (allocated(sca11)) then
             cVar=cVar+1
             allocate(var(1,lsx-lgx:lex+lgx,lsy-lgy:ley+lgy,lsz-lgz:lez+lgz))
             write(varName(cVar),'(a)') 'sca11'
             varType(cVar)=1
             var(1,:,:,:)=sca11
             varIterName=trim(adjustl(varName(cVar)))//trim(adjustl(iterName))
             call WriteEnsightVar(varType(cVar),var,varName(cVar),iterName,mesh)
             deallocate(var) 
          end if
       end if
    end if
    !---------------------------------------------------------------------
    ! sca12
    !---------------------------------------------------------------------
    if (file_egld%write_field%sca12) then
       if (present(sca12)) then
          if (allocated(sca12)) then
             cVar=cVar+1
             allocate(var(1,lsx-lgx:lex+lgx,lsy-lgy:ley+lgy,lsz-lgz:lez+lgz))
             write(varName(cVar),'(a)') 'sca12'
             varType(cVar)=1
             var(1,:,:,:)=sca12
             varIterName=trim(adjustl(varName(cVar)))//trim(adjustl(iterName))
             call WriteEnsightVar(varType(cVar),var,varName(cVar),iterName,mesh)
             deallocate(var) 
          end if
       end if
    end if
    !---------------------------------------------------------------------
    ! sca13
    !---------------------------------------------------------------------
    if (file_egld%write_field%sca13) then
       if (present(sca13)) then
          if (allocated(sca13)) then
             cVar=cVar+1
             allocate(var(1,lsx-lgx:lex+lgx,lsy-lgy:ley+lgy,lsz-lgz:lez+lgz))
             write(varName(cVar),'(a)') 'sca13'
             varType(cVar)=1
             var(1,:,:,:)=sca13
             varIterName=trim(adjustl(varName(cVar)))//trim(adjustl(iterName))
             call WriteEnsightVar(varType(cVar),var,varName(cVar),iterName,mesh)
             deallocate(var) 
          end if
       end if
    end if
    !---------------------------------------------------------------------
    ! sca14
    !---------------------------------------------------------------------
    if (file_egld%write_field%sca14) then
       if (present(sca14)) then
          if (allocated(sca14)) then
             cVar=cVar+1
             allocate(var(1,lsx-lgx:lex+lgx,lsy-lgy:ley+lgy,lsz-lgz:lez+lgz))
             write(varName(cVar),'(a)') 'sca14'
             varType(cVar)=1
             var(1,:,:,:)=sca14
             varIterName=trim(adjustl(varName(cVar)))//trim(adjustl(iterName))
             call WriteEnsightVar(varType(cVar),var,varName(cVar),iterName,mesh)
             deallocate(var) 
          end if
       end if
    end if
    !---------------------------------------------------------------------
    ! sca15
    !---------------------------------------------------------------------
    if (file_egld%write_field%sca15) then
       if (present(sca15)) then
          if (allocated(sca15)) then
             cVar=cVar+1
             allocate(var(1,lsx-lgx:lex+lgx,lsy-lgy:ley+lgy,lsz-lgz:lez+lgz))
             write(varName(cVar),'(a)') 'sca15'
             varType(cVar)=1
             var(1,:,:,:)=sca15
             varIterName=trim(adjustl(varName(cVar)))//trim(adjustl(iterName))
             call WriteEnsightVar(varType(cVar),var,varName(cVar),iterName,mesh)
             deallocate(var) 
          end if
       end if
    end if
    !---------------------------------------------------------------------

    !---------------------------------------------------------------------
    ! mesh and case
    !---------------------------------------------------------------------
    if (rank==0) then
       caseName="fields_m"//imesh
       call WriteEnsightCase(cVar,varName,caseName,VarType,itime,time,mesh%id)
       call WriteRectEnsightGeo(caseName,mesh)
    end if
    !---------------------------------------------------------------------
    
  end subroutine Write_field_ensight_gold


  subroutine Write_prtcl_ensight_gold(unit,itime,time,particles,nparticles)
    !*******************************************************************************!
    implicit none
    !---------------------------------------------------------------------
    ! Global variables                                            
    !---------------------------------------------------------------------
    integer, intent(in)                                     :: unit,itime
    real(8), intent(in)                                     :: time
    type(particle_t), allocatable, dimension(:), intent(in) :: particles
    integer, intent(in)                                     :: nparticles
    !---------------------------------------------------------------------
    ! Local variables                                             
    !---------------------------------------------------------------------
    character(len=80)                                       :: caseName
    character(len=80)                                       :: iterName
    !---------------------------------------------------------------------
    
    !---------------------------------------------------------------------
    ! Test ensight gold format 
    !---------------------------------------------------------------------
    
    write(iterName,'(i10.10)') itime
    call WriteEnsightPartGeoVar(particles,nparticles,iterName)
    
    !---------------------------------------------------------------------
    ! mesh and case
    !---------------------------------------------------------------------
    if (rank==0) then
       caseName="EnSight_prtcls"
       call WriteEnsightPartCase(caseName,itime,time)
    end if
    !---------------------------------------------------------------------
    
  end subroutine Write_prtcl_ensight_gold

  
  subroutine Write_field_vtk(unit,itime,time,mesh,u,v,w,              &
       & sca01,sca02,sca03,sca04,sca05,sca06,sca07,sca08,sca09,sca10, &
       & sca11,sca12,sca13,sca14,sca15,sca16,sca17,sca18,sca19,sca20)
    !*******************************************************************************!
    implicit none
    !---------------------------------------------------------------------
    ! Global variables                                            
    !---------------------------------------------------------------------
    integer, intent(in)                                             :: unit,itime
    real(8), intent(in)                                             :: time
    type(grid_t), intent(in)                                        :: mesh
    real(8), dimension(:,:,:), allocatable, intent(in)              :: u,v,w
    real(8), dimension(:,:,:), allocatable, intent(inout), optional ::  &
         & sca01,sca02,sca03,sca04,sca05,sca06,sca07,sca08,sca09,sca10, &
         & sca11,sca12,sca13,sca14,sca15,sca16,sca17,sca18,sca19,sca20
    !---------------------------------------------------------------------
    ! Local variables                                             
    !---------------------------------------------------------------------
    integer                                   :: i,j,k
    integer                                   :: lsx,lsy,lsz
    integer                                   :: lex,ley,lez
    integer                                   :: dim_XYZ,dim_X,dim_Y,dim_Z
    character(len=1), parameter               :: eol=achar(10)
    character(len=500)                        :: field_name
    character(len=500)                        :: header_main
    character(len=200)                        :: header_struc
    character(len=200)                        :: header_points
    character(len=200)                        :: header_cells
    character(len=200)                        :: header_cell_types
    character(len=200)                        :: header_data
    character(len=200)                        :: main_header_data
    character(len=200)                        :: header_dim
    character(len=200)                        :: header_table
    character(len=12)                         :: c_points,x_points,y_points,z_points
    character(100)                            :: name
    character (LEN=100)                       :: File
    !---------------------------------------------------------------------

    !---------------------------------------------------------------------
    ! mpi 
    !---------------------------------------------------------------------
    if (nproc>1) then
       if (present(sca01)) then
          if (allocated(sca01)) call comm_mpi_sca(sca01)
       end if
       if (present(sca02)) then
          if (allocated(sca02)) call comm_mpi_sca(sca02)
       end if
       if(present(sca03)) then
          if (allocated(sca03)) call comm_mpi_sca(sca03)
       end if
       if (present(sca04)) then
          if (allocated(sca04)) call comm_mpi_sca(sca04)
       end if
       if (present(sca05)) then
          if (allocated(sca05)) call comm_mpi_sca(sca05)
       end if
       if (present(sca06)) then
          if (allocated(sca06)) call comm_mpi_sca(sca06)
       end if
       if (present(sca07)) then
          if (allocated(sca07)) call comm_mpi_sca(sca07)
       end if
       if (present(sca08)) then
          if (allocated(sca08)) call comm_mpi_sca(sca08)
       end if
       if (present(sca09)) then
          if (allocated(sca09)) call comm_mpi_sca(sca09)
       end if
       if (present(sca10)) then
          if (allocated(sca10)) call comm_mpi_sca(sca10)
       end if
       if (present(sca11)) then
          if (allocated(sca11)) call comm_mpi_sca(sca11)
       end if
       if (present(sca12)) then
          if (allocated(sca12)) call comm_mpi_sca(sca12)
       end if
       if (present(sca13)) then
          if (allocated(sca13)) call comm_mpi_sca(sca13)
       end if
       if (present(sca14)) then
          if (allocated(sca14)) call comm_mpi_sca(sca14)
       end if
       if (present(sca15)) then
          if (allocated(sca15)) call comm_mpi_sca(sca15)
       end if
       if (present(sca16)) then
          if (allocated(sca16)) call comm_mpi_sca(sca16)
       end if
       if (present(sca17)) then
          if (allocated(sca17)) call comm_mpi_sca(sca17)
       end if
       if (present(sca18)) then
          if (allocated(sca18)) call comm_mpi_sca(sca18)
       end if
       if (present(sca19)) then
          if (allocated(sca19)) call comm_mpi_sca(sca19)
       end if
       if (present(sca20)) then
          if (allocated(sca20)) call comm_mpi_sca(sca20)
       end if
    end if
    !---------------------------------------------------------------------

    !-------------------------------------------
    ! initialization of final indexes
    !-------------------------------------------
    lsx=mesh%sx
    lsy=mesh%sy
    lsz=mesh%sz
    lex=mesh%ex
    ley=mesh%ey
    lez=mesh%ez
    !-------------------------------------------

    if (nproc==1) then 
       write(name,'(i8.8)') itime
       File="fields_i"//trim(adjustl(name))//".vtk"
    else
!!$       write(name,'(i8.8)') itime
!!$       File=trim(adjustl(cas))//"_i"//trim(adjustl(name))
!!$       write(name,'(i6.6)') rank
!!$       File=trim(adjustl(File))//"_p"//trim(adjustl(name))//".vtk"
       write(name,'(i6.6)') rank
       File="fields_p"//trim(adjustl(name))
       write(name,'(i8.8)') itime
       File=trim(adjustl(File))//"_i"//trim(adjustl(name))//".vtk"
       !-------------------------------------------
       if (lex/=gex) lex=lex+1
       if (ley/=gey) ley=ley+1
       if (lez/=gez) lez=lez+1
       !------------------------------------------
    end if

    open(unit,FILE=File,         &
         & STATUS='UNKNOWN',     &
         & form='unformatted',   &
         & access='stream',      &
         & convert='big_endian')
    rewind unit


    dim_X=lex-lsx+1
    dim_Y=ley-lsy+1
    if (dim==2) then
       dim_Z=1
    elseif (dim==3) then
       dim_Z=lez-lsz+1
    end if
    dim_XYZ=dim_X*dim_Y*dim_Z 

    write(c_points,'(i10)') dim_XYZ
    write(x_points,'(i10)') dim_X
    write(y_points,'(i10)') dim_Y
    write(z_points,'(i10)') dim_Z

    header_main='# vtk DataFile Version 2.0'//eol//&
         "TEST"//eol//&
         "BINARY"//eol

    header_struc='DATASET STRUCTURED_GRID'//eol
    header_dim='DIMENSIONS '//trim(x_points)//trim(y_points)//trim(z_points)//eol
    header_points='POINTS '//trim(c_points)//' double'//eol
    header_table='LOOKUP_TABLE default'//eol

    write(unit) trim(header_main)
    write(unit) trim(header_struc)
    write(unit) trim(header_dim)
    write(unit) trim(header_points)
    ! ----------------MESH ---------------------
    do k=lsz,lez
       do j=lsy,ley
          do i=lsx,lex
             if (dim==2) then
                write(unit) mesh%x(i),mesh%y(j),zero
             else 
                write(unit) mesh%x(i),mesh%y(j),mesh%z(k)
             end if
          enddo
       enddo
    enddo
    write(unit) eol
    ! ---------------------- HEADER --------------------------
    main_header_data='POINT_DATA '//trim(c_points)//eol
    write(unit) trim(main_header_data)
    ! --------------------  VECTOR FIELD  --------------------------
    field_name='V'
    header_data='VECTORS '//trim(field_name)//' double'//eol
    write(unit) trim(header_data)
    do k=lsz,lez
       do j=lsy,ley
          do i=lsx,lex
             if (dim==2) then
                write(unit) (mesh%dxu2(i+1)*u(i,j,k)+mesh%dxu2(i)*u(i+1,j,k))/mesh%dx(i),&
                     & (mesh%dyv2(j+1)*v(i,j,k)+mesh%dyv2(j)*v(i,j+1,k))/mesh%dy(j),     &
                     & zero
             else if (dim==3) then 
                write(unit) (mesh%dxu2(i+1)*u(i,j,k)+mesh%dxu2(i)*u(i+1,j,k))/mesh%dx(i),&
                     & (mesh%dyv2(j+1)*v(i,j,k)+mesh%dyv2(j)*v(i,j+1,k))/mesh%dy(j),     &
                     & (mesh%dzw2(k+1)*w(i,j,k)+mesh%dzw2(k)*w(i,j,k+1))/mesh%dz(k)
             end if
          enddo
       enddo
    enddo
    write(unit) eol
    ! --------------------  SCALAR FIELDS  --------------------------
    if (present(sca01)) then
       if (allocated(sca01)) then
          write(z_points,'(i10)') 1
          field_name='sca01'
          header_data='SCALARS '//trim(field_name)//' double '//trim(adjustl(z_points))//eol
          write(unit) trim(header_data)
          write(unit) trim(header_table)
          do k=lsz,lez
             do j=lsy,ley
                do i=lsx,lex
                   write(unit) sca01(i,j,k)
                enddo
             enddo
          enddo
          write(unit) eol
       end if
    end if
    ! --------------------  SCALAR FIELDS  --------------------------
    if (present(sca02)) then
       if (allocated(sca02)) then
          write(z_points,'(i10)') 1
          field_name='sca02'
          header_data='SCALARS '//trim(field_name)//' double '//trim(adjustl(z_points))//eol
          write(unit) trim(header_data)
          write(unit) trim(header_table)
          do k=lsz,lez
             do j=lsy,ley
                do i=lsx,lex
                   write(unit) sca02(i,j,k)
                enddo
             enddo
          enddo
          write(unit) eol
       end if
    end if
    ! --------------------  SCALAR FIELDS  --------------------------
    if (present(sca03)) then
       if (allocated(sca03)) then
          write(z_points,'(i10)') 1
          field_name='sca03'
          header_data='SCALARS '//trim(field_name)//' double '//trim(adjustl(z_points))//eol
          write(unit) trim(header_data)
          write(unit) trim(header_table)
          do k=lsz,lez
             do j=lsy,ley
                do i=lsx,lex
                   write(unit) sca03(i,j,k)
                enddo
             enddo
          enddo
          write(unit) eol
       end if
    end if
    ! --------------------  SCALAR FIELDS  --------------------------
    if (present(sca04)) then
       if (allocated(sca04)) then
          write(z_points,'(i10)') 1
          field_name='sca04'
          header_data='SCALARS '//trim(field_name)//' double '//trim(adjustl(z_points))//eol
          write(unit) trim(header_data)
          write(unit) trim(header_table)
          do k=lsz,lez
             do j=lsy,ley
                do i=lsx,lex
                   write(unit) sca04(i,j,k)
                enddo
             enddo
          enddo
          write(unit) eol
       end if
    end if
    ! --------------------  SCALAR FIELDS  --------------------------
    if (present(sca05)) then
       if (allocated(sca05)) then
          write(z_points,'(i10)') 1
          field_name='sca05'
          header_data='SCALARS '//trim(field_name)//' double '//trim(adjustl(z_points))//eol
          write(unit) trim(header_data)
          write(unit) trim(header_table)
          do k=lsz,lez
             do j=lsy,ley
                do i=lsx,lex
                   write(unit) sca05(i,j,k)
                enddo
             enddo
          enddo
          write(unit) eol
       end if
    end if
    ! --------------------  SCALAR FIELDS  --------------------------
    if (present(sca06)) then
       if (allocated(sca06)) then
          write(z_points,'(i10)') 1
          field_name='sca06'
          header_data='SCALARS '//trim(field_name)//' double '//trim(adjustl(z_points))//eol
          write(unit) trim(header_data)
          write(unit) trim(header_table)
          do k=lsz,lez
             do j=lsy,ley
                do i=lsx,lex
                   write(unit) sca06(i,j,k)
                enddo
             enddo
          enddo
          write(unit) eol
       end if
    end if
    ! --------------------  SCALAR FIELDS  --------------------------
    if (present(sca07)) then
       if (allocated(sca07)) then
          write(z_points,'(i10)') 1
          field_name='sca07'
          header_data='SCALARS '//trim(field_name)//' double '//trim(adjustl(z_points))//eol
          write(unit) trim(header_data)
          write(unit) trim(header_table)
          do k=lsz,lez
             do j=lsy,ley
                do i=lsx,lex
                   write(unit) sca07(i,j,k)
                enddo
             enddo
          enddo
          write(unit) eol
       end if
    end if
    ! --------------------  SCALAR FIELDS  --------------------------
    if (present(sca08)) then
       if (allocated(sca08)) then
          write(z_points,'(i10)') 1
          field_name='sca08'
          header_data='SCALARS '//trim(field_name)//' double '//trim(adjustl(z_points))//eol
          write(unit) trim(header_data)
          write(unit) trim(header_table)
          do k=lsz,lez
             do j=lsy,ley
                do i=lsx,lex
                   write(unit) sca08(i,j,k)
                enddo
             enddo
          enddo
          write(unit) eol
       end if
    end if
    ! --------------------  SCALAR FIELDS  --------------------------
    if (present(sca09)) then
       if (allocated(sca09)) then
          write(z_points,'(i10)') 1
          field_name='sca09'
          header_data='SCALARS '//trim(field_name)//' double '//trim(adjustl(z_points))//eol
          write(unit) trim(header_data)
          write(unit) trim(header_table)
          do k=lsz,lez
             do j=lsy,ley
                do i=lsx,lex
                   write(unit) sca09(i,j,k)
                enddo
             enddo
          enddo
          write(unit) eol
       end if
    end if
    ! --------------------  SCALAR FIELDS  --------------------------
    if (present(sca10)) then
       if (allocated(sca10)) then
          write(z_points,'(i10)') 1
          field_name='sca10'
          header_data='SCALARS '//trim(field_name)//' double '//trim(adjustl(z_points))//eol
          write(unit) trim(header_data)
          write(unit) trim(header_table)
          do k=lsz,lez
             do j=lsy,ley
                do i=lsx,lex
                   write(unit) sca10(i,j,k)
                enddo
             enddo
          enddo
          write(unit) eol
       end if
    end if
    ! --------------------  SCALAR FIELDS  --------------------------
    if (present(sca11)) then
       if (allocated(sca11)) then
          write(z_points,'(i10)') 1
          field_name='sca11'
          header_data='SCALARS '//trim(field_name)//' double '//trim(adjustl(z_points))//eol
          write(unit) trim(header_data)
          write(unit) trim(header_table)
          do k=lsz,lez
             do j=lsy,ley
                do i=lsx,lex
                   write(unit) sca11(i,j,k)
                enddo
             enddo
          enddo
          write(unit) eol
       end if
    end if
    ! --------------------  SCALAR FIELDS  --------------------------
    if (present(sca12)) then
       if (allocated(sca12)) then
          write(z_points,'(i10)') 1
          field_name='sca12'
          header_data='SCALARS '//trim(field_name)//' double '//trim(adjustl(z_points))//eol
          write(unit) trim(header_data)
          write(unit) trim(header_table)
          do k=lsz,lez
             do j=lsy,ley
                do i=lsx,lex
                   write(unit) sca12(i,j,k)
                enddo
             enddo
          enddo
          write(unit) eol
       end if
    end if
    ! --------------------  SCALAR FIELDS  --------------------------
    if (present(sca13)) then
       if (allocated(sca13)) then
          write(z_points,'(i10)') 1
          field_name='sca13'
          header_data='SCALARS '//trim(field_name)//' double '//trim(adjustl(z_points))//eol
          write(unit) trim(header_data)
          write(unit) trim(header_table)
          do k=lsz,lez
             do j=lsy,ley
                do i=lsx,lex
                   write(unit) sca13(i,j,k)
                enddo
             enddo
          enddo
          write(unit) eol
       end if
    end if
    ! --------------------  SCALAR FIELDS  --------------------------
    if (present(sca14)) then
       if (allocated(sca14)) then
          write(z_points,'(i10)') 1
          field_name='sca14'
          header_data='SCALARS '//trim(field_name)//' double '//trim(adjustl(z_points))//eol
          write(unit) trim(header_data)
          write(unit) trim(header_table)
          do k=lsz,lez
             do j=lsy,ley
                do i=lsx,lex
                   write(unit) sca14(i,j,k)
                enddo
             enddo
          enddo
          write(unit) eol
       end if
    end if
    ! --------------------  SCALAR FIELDS  --------------------------
    if (present(sca15)) then
       if (allocated(sca15)) then
          write(z_points,'(i10)') 1
          field_name='sca15'
          header_data='SCALARS '//trim(field_name)//' double '//trim(adjustl(z_points))//eol
          write(unit) trim(header_data)
          write(unit) trim(header_table)
          do k=lsz,lez
             do j=lsy,ley
                do i=lsx,lex
                   write(unit) sca15(i,j,k)
                enddo
             enddo
          enddo
          write(unit) eol
       end if
    end if
    ! --------------------  SCALAR FIELDS  --------------------------
    if (present(sca16)) then
       if (allocated(sca16)) then
          write(z_points,'(i10)') 1
          field_name='sca16'
          header_data='SCALARS '//trim(field_name)//' double '//trim(adjustl(z_points))//eol
          write(unit) trim(header_data)
          write(unit) trim(header_table)
          do k=lsz,lez
             do j=lsy,ley
                do i=lsx,lex
                   write(unit) sca16(i,j,k)
                enddo
             enddo
          enddo
          write(unit) eol
       end if
    end if
    ! --------------------  SCALAR FIELDS  --------------------------
    if (present(sca17)) then
       if (allocated(sca17)) then
          write(z_points,'(i10)') 1
          field_name='sca17'
          header_data='SCALARS '//trim(field_name)//' double '//trim(adjustl(z_points))//eol
          write(unit) trim(header_data)
          write(unit) trim(header_table)
          do k=lsz,lez
             do j=lsy,ley
                do i=lsx,lex
                   write(unit) sca17(i,j,k)
                enddo
             enddo
          enddo
          write(unit) eol
       end if
    end if
    ! --------------------  SCALAR FIELDS  --------------------------
    if (present(sca18)) then
       if (allocated(sca18)) then
          write(z_points,'(i10)') 1
          field_name='sca18'
          header_data='SCALARS '//trim(field_name)//' double '//trim(adjustl(z_points))//eol
          write(unit) trim(header_data)
          write(unit) trim(header_table)
          do k=lsz,lez
             do j=lsy,ley
                do i=lsx,lex
                   write(unit) sca18(i,j,k)
                enddo
             enddo
          enddo
          write(unit) eol
       end if
    end if
    ! --------------------  SCALAR FIELDS  --------------------------
    if (present(sca19)) then
       if (allocated(sca19)) then
          write(z_points,'(i10)') 1
          field_name='sca19'
          header_data='SCALARS '//trim(field_name)//' double '//trim(adjustl(z_points))//eol
          write(unit) trim(header_data)
          write(unit) trim(header_table)
          do k=lsz,lez
             do j=lsy,ley
                do i=lsx,lex
                   write(unit) sca19(i,j,k)
                enddo
             enddo
          enddo
          write(unit) eol
       end if
    end if
    ! --------------------  SCALAR FIELDS  --------------------------
    if (present(sca20)) then
       if (allocated(sca20)) then
          write(z_points,'(i10)') 1
          field_name='sca20'
          header_data='SCALARS '//trim(field_name)//' double '//trim(adjustl(z_points))//eol
          write(unit) trim(header_data)
          write(unit) trim(header_table)
          do k=lsz,lez
             do j=lsy,ley
                do i=lsx,lex
                   write(unit) sca20(i,j,k)
                enddo
             enddo
          enddo
          write(unit) eol
       end if
    end if

    close(unit)
    return
    
  end subroutine Write_field_vtk
  !*******************************************************************************!


    !*******************************************************************************!
  subroutine Write_Particle_vtk(unit,itime,time,LPart,nPart)
    !*******************************************************************************!
    implicit none
    !---------------------------------------------------------------------
    ! Global variables                                            
    !---------------------------------------------------------------------
    integer, intent(in)                                   :: unit,itime,nPart
    real(8), intent(in)                                   :: time
    type(particle_t), allocatable, dimension(:), intent(in) :: LPart
    !---------------------------------------------------------------------
    ! Local variables                                             
    !---------------------------------------------------------------------
    integer                                   :: i,n,cpt
    character(len=1), parameter               :: eol=achar(10)
    character(len=500)                        :: field_name
    character(len=500)                        :: header_main
    character(len=200)                        :: header_struc
    character(len=200)                        :: header_points
    character(len=200)                        :: header_cells
    character(len=200)                        :: header_cell_types
    character(len=200)                        :: header_data
    character(len=200)                        :: main_header_data
    character(len=200)                        :: header_dim
    character(len=200)                        :: header_table
    character(len=12)                         :: c_points,x_points,y_points,z_points
    character(100)                            :: name
    character (LEN=100)                       :: File
    !---------------------------------------------------------------------

    !---------------------------------------------------------------------
    ! mpi 
    !---------------------------------------------------------------------
    if (nproc>1) then
    end if
    !---------------------------------------------------------------------

    
    !---------------------------------------------------------------------
    ! File name
    !---------------------------------------------------------------------
    if (nproc==1) then 
       write(name,'(i8.8)') itime
       File="prtcls_i"//trim(adjustl(name))//".vtk"
    else
       write(name,'(i6.6)') rank
       File="prtcls_p"//trim(adjustl(name))
       write(name,'(i8.8)') itime
       File=trim(adjustl(File))//"_i"//trim(adjustl(name))//".vtk"
    end if
    !---------------------------------------------------------------------
    open(unit,FILE=File,         &
         & STATUS='UNKNOWN',     &
         & form='unformatted',   &
         & access='stream',      &
         & convert='big_endian')
    rewind unit
    !---------------------------------------------------------------------

    !---------------------------------------------------------------------
    ! number of points
    !---------------------------------------------------------------------
    cpt=0
    do n=1,nPart
       if (LPart(n)%active) cpt=cpt+1
    end do
    write(c_points,'(i10)') cpt
    !---------------------------------------------------------------------
    
    header_main='# vtk DataFile Version 2.0'//eol//&
         "Unstructured Particles"//eol//&
         "BINARY"//eol
    
    header_struc='DATASET UNSTRUCTURED_GRID'//eol
    header_points='POINTS '//trim(c_points)//' double'//eol
    header_table='LOOKUP_TABLE default'//eol
    
    write(unit) trim(header_main)
    write(unit) trim(header_struc)
    write(unit) trim(header_points)
    ! ---------------- POINTS ---------------------
    do n=1,nPart
       if (LPart(n)%active) write(unit) LPart(n)%r
    enddo
    write(unit) eol
    ! ---------------------- CELLS --------------------------
    write(z_points,'(i10)') 2*cpt
    header_cells='CELLS '//trim(c_points)//' '//trim(z_points)//eol
    write(unit) trim(header_cells)
    i=0
    do n=1,nPart
       if (LPart(n)%active) then
          write(unit) 1, i
          i=i+1
       end if
    enddo
    write(unit) eol
    ! ---------------------- CELLS TYPE --------------------------
    header_cell_types='CELL_TYPES '//trim(c_points)//eol
    write(unit) trim(header_cell_types)
    do n=1,nPart
       if (LPart(n)%active) write(unit) 1
    enddo
    write(unit) eol
    ! ----------------------  MAIN HEADER --------------------------
    main_header_data='POINT_DATA '//trim(c_points)//eol
    write(unit) trim(main_header_data)
    ! --------------------  VECTOR FIELD  --------------------------
    field_name='V'
    header_data='VECTORS '//trim(field_name)//' double'//eol
    write(unit) trim(header_data)
    do n=1,nPart
       if (LPart(n)%active) write(unit) LPart(n)%u
    enddo
    write(unit) eol
    ! --------------------  SCALAR FIELDS  --------------------------
    write(z_points,'(i10)') 1
    field_name='phi'
    header_data='SCALARS '//trim(field_name)//' double '//trim(adjustl(z_points))//eol
    write(unit) trim(header_data)
    write(unit) trim(header_table)
    do n=1,nPart
       if (LPart(n)%active) write(unit) LPart(n)%phi
    enddo
    write(unit) eol
    ! --------------------  SCALAR FIELDS  --------------------------
    write(z_points,'(i10)') 1
    field_name='time'
    header_data='SCALARS '//trim(field_name)//' double '//trim(adjustl(z_points))//eol
    write(unit) trim(header_data)
    write(unit) trim(header_table)
    do n=1,nPart
       if (LPart(n)%active) write(unit) LPart(n)%time
    enddo
    write(unit) eol

    close(unit)
    
  end subroutine Write_Particle_vtk
  

  !*******************************************************************************!
  !*******************************************************************************!

  subroutine WriteEnsightVar(ndv,var,VarName,iterName,mesh)
    ! ******************************************************************************
    ! WriteEnsightSca writes result data in Ensight's format
    !    from J.-L. Estivalezes
    !
    ! ndv.........: number of dimension of the variable (1=>scalar   3=>vector)  
    ! var.........: data to be written
    ! Varname.....: word used to build filenames
    !
    ! m1,m2,m3....: size of the variable in the x1,x2,x3 direction
    ! imin,imax...: range of writting data in the x1 direction
    ! jmin,jmax...: range of writting data in the x2 direction
    ! kmin,kmax...: range of writting data in the x3 direction
    !  ******************************************************************************
    implicit none
    
    !---------------------------------------------------------------------
    ! Global variables                                            
    !---------------------------------------------------------------------
    integer, intent(in)                                  :: ndv ! 1 = scalar , 2 = vector
    real(8), dimension(:,:,:,:), allocatable, intent(in) :: var
    character*80, intent(in)                             :: VarName
    character*80, intent(in)                             :: iterName
    type(grid_t), intent(in)                             :: mesh
    !---------------------------------------------------------------------
    ! Local variables                                            
    !---------------------------------------------------------------------
    integer                                              :: lsx,lsy,lsz
    integer                                              :: lex,ley,lez
    real(4), allocatable, dimension(:,:,:)               :: var_sngl
    character(LEN=1)                                     :: imesh
    character(LEN=80)                                    :: VarFileName
    character(LEN=80)                                    :: part,blocck
    integer                                              :: FileUnit
    integer                                              :: i,j,k,ii,npart,m
    integer                                              :: iskip
    integer                                              :: descripteur
    integer                                              :: tailleDoublePrecision,tailleCharacter,&
         & tailleInteger,tailleReel
    integer                                              :: iaux,jaux,kaux
    integer(KIND=MPI_OFFSET_KIND)                        :: positionInitiale
    integer, dimension(MPI_STATUS_SIZE)                  :: statut
    integer                                              :: loc_size
    integer, dimension(3)                                :: ucount,ustart,dimsuids
    integer                                              :: filetype
    !---------------------------------------------------------------------

    FileUnit = 666
    part     = 'part'
    npart    = 1
    blocck   = 'block rectilinear'
    
    write(imesh,'(i1.1)') mesh%id
    
    !---------------------------------------------------------------------
    ! MPI init types
    !---------------------------------------------------------------------
    call MPI_TYPE_SIZE(MPI_DOUBLE_PRECISION,tailleDoublePrecision,MPI_CODE)
    call MPI_TYPE_SIZE(MPI_REAL,tailleReel,MPI_CODE)
    call MPI_TYPE_SIZE(MPI_CHARACTER,tailleCharacter,MPI_CODE)
    call MPI_TYPE_SIZE(MPI_INTEGER,tailleInteger,MPI_CODE)
    !---------------------------------------------------------------------

    !---------------------------------------------------------------------
    ! File creation
    !---------------------------------------------------------------------
    if (ndv==1) then
       VarFileName="fields_m"//imesh//'_'//trim(VarName)//'_'//trim(iterName)//'.scl'
    else 
       VarFileName="fields_m"//imesh//'_'//trim(VarName)//'_'//trim(iterName)//'.vec'
    end if
    
    !---------------------------------------------------------------------
    call MPI_FILE_OPEN(COMM3D,VarFileName,MPI_MODE_WRONLY+MPI_MODE_CREATE,MPI_INFO_NULL,descripteur,MPI_CODE)
    !---------------------------------------------------------------------

    !---------------------------------------------------------------------
    ! Header
    !---------------------------------------------------------------------
    positionInitiale = 0
    !---------------------------------------------------------------------
    call MPI_FILE_WRITE_AT_ALL(descripteur,positionInitiale,VarFileName,80,MPI_CHARACTER,statut,MPI_CODE)     
    positionInitiale = positionInitiale+80*tailleCharacter
    !---------------------------------------------------------------------
    call MPI_FILE_WRITE_AT_ALL(descripteur,positionInitiale,part,80,MPI_CHARACTER,statut,MPI_CODE)  
    positionInitiale = positionInitiale+80*tailleCharacter
    !---------------------------------------------------------------------
    call MPI_FILE_WRITE_AT_ALL(descripteur,positionInitiale,npart,1,MPI_INTEGER,statut,MPI_CODE)  
    positionInitiale = positionInitiale+tailleInteger
    !---------------------------------------------------------------------
    call MPI_FILE_WRITE_AT_ALL(descripteur,positionInitiale,blocck,80,MPI_CHARACTER,statut,MPI_CODE)
    positionInitiale = positionInitiale+80*tailleCharacter  
    !---------------------------------------------------------------------

    lsx=mesh%sx
    lsy=mesh%sy
    lsz=mesh%sz
    lex=mesh%ex
    ley=mesh%ey
    lez=mesh%ez

    if (mesh%periodic(1)) then
       if (lex==mesh%gex-1) lex=lex+1
    end if
    if (mesh%periodic(2)) then
       if (ley==mesh%gey-1) ley=ley+1
    end if
    if (mesh%dim==3.and.mesh%periodic(3)) then
       if (lez==mesh%gez-1) lez=lez+1
    end if


    !---------------------------------------------------------------------
    ! Taille du bloc de données à écrire par le proc
    !---------------------------------------------------------------------
    iskip=1
    ucount(1)=(lex-lsx+1)/iskip
    ucount(2)=(ley-lsy+1)/iskip
    ucount(3)=(lez-lsz+1)/iskip
    !---------------------------------------------------------------------
 
    !---------------------------------------------------------------------
    ! Dimension totale du domaine à écrire
    !---------------------------------------------------------------------
    dimsuids(1)=(mesh%gex-mesh%gsx+1)/iskip
    dimsuids(2)=(mesh%gey-mesh%gsy+1)/iskip
    dimsuids(3)=(mesh%gez-mesh%gsz+1)/iskip
    !---------------------------------------------------------------------
!!$    IF (dim == 2) THEN !Correction nécessaire pour le 2D quand iskip > 1
!!$       ucount  (3) = 1
!!$       dimsuids(3) = 1
!!$    END IF
    !---------------------------------------------------------------------

    !---------------------------------------------------------------------
    ! En cas de vecteur, on "empile" les différentes composantes sur la direction z
    ! afin qu'elles soient écrites les unes à la suite des autres dans le même fichier
    !---------------------------------------------------------------------
    dimsuids(3)=dimsuids(3)*ndv
    !---------------------------------------------------------------------

    allocate(var_sngl(1:ucount(1),1:ucount(2),1:ucount(3)))

    do m = 1,ndv !Pour chacune des composantes du vecteur

       !---------------------------------------------------------------------
       !Passage en simple précision
       !---------------------------------------------------------------------
       kaux = 1
       do k=sz,lez,iskip
          jaux = 1
          do j=sy,ley,iskip   
             iaux = 1
             do i=sx,lex,iskip
                var_sngl(iaux,jaux,kaux) = sngl(var(m,i,j,k))
                iaux = iaux + 1
             end do
             jaux = jaux + 1
          end do
          kaux = kaux+1
       end do
       !---------------------------------------------------------------------
       
       !---------------------------------------------------------------------
       ! Position du sous-domaine à écrire dans le domaine complet
       ! Convention du langage C pour les indices   
       !---------------------------------------------------------------------
       !write(*,*) rank,coords
!!$       ustart(1)=0+coords(1)*ucount(1)
!!$       ustart(2)=0+coords(2)*ucount(2)
!!$       ustart(3)=0+coords(3)*ucount(3)+(m-1)*(dimsuids(3))/ndv ! Empilage des composantes du vecteur
       ustart(1)=lsx
       ustart(2)=lsy
       if (dim==2) then
          ustart(3)=(m-1)*(dimsuids(3))/ndv ! Empilage des composantes du vecteur
       else
          ustart(3)=lsz+(m-1)*(dimsuids(3))/ndv ! Empilage des composantes du vecteur
       end if
       !---------------------------------------------------------------------

       !---------------------------------------------------------------------
       ! Création du sous tableau du proc local
       !---------------------------------------------------------------------
       !CALL mpi_type_create_subarray(3,dimsuids, ucount, ustart,mpi_order_fortran  ,mpi_real4,filetype, code)
       call mpi_type_create_subarray(3,dimsuids,ucount,ustart,mpi_order_fortran,mpi_real,filetype,MPI_CODE)
       call mpi_type_commit(filetype,MPI_CODE)
       loc_size=product(ucount)
       !---------------------------------------------------------------------

       !---------------------------------------------------------------------
       ! Ecriture
       !---------------------------------------------------------------------
       call mpi_file_set_view(descripteur, positionInitiale, mpi_real, filetype, "native", mpi_info_null, code)
!!$       CALL mpi_file_set_view(descripteur,positionInitiale,mpi_real,filetype,"native",mpi_info_null,MPI_CODE)
       call mpi_file_write_all(descripteur, var_sngl, loc_size, mpi_real,mpi_status_ignore, code)
!!$       CALL mpi_file_write_all(descripteur,var_sngl,1,filetype,mpi_status_ignore,MPI_CODE)
       !---------------------------------------------------------------------

    end do
    
    deallocate(var_sngl)  
    call mpi_type_free(filetype, code)
    call MPI_FILE_CLOSE(descripteur,MPI_CODE)

  end subroutine WriteEnsightVar

  
  subroutine WriteEnsightPartGeoVar(particles,nparticles,iterName)
    implicit none
    !---------------------------------------------------------------------
    ! Global variables                                            
    !---------------------------------------------------------------------
    integer, intent(in)                                     :: nparticles 
    type(particle_t), allocatable, dimension(:), intent(in) :: particles
    character(LEN=80), intent(in)                           :: iterName
    !---------------------------------------------------------------------
    ! Local variables                                            
    !---------------------------------------------------------------------
    integer                                                 :: n,m
    integer                                                 :: i,j,k
    integer, dimension(:), allocatable                      :: deplacements
    integer                                                 :: ndv,ntot
    real(4)                                                 :: var
    real(4), allocatable, dimension(:,:)                    :: var_sngl
    character(LEN=80)                                       :: VarFileName,localName
    character(LEN=80)                                       :: part,blocck
    character(LEN=80)                                       :: binary_form
    character(LEN=80)                                       :: file_description1,file_description2
    character(LEN=80)                                       :: node_id,element_id,description_part
    integer                                                 :: FileUnit
    integer                                                 :: npart
    integer                                                 :: descripteur
    integer                                                 :: tailleDoublePrecision,tailleCharacter
    integer                                                 :: tailleInteger,tailleReel
    integer                                                 :: iaux,jaux,kaux
    integer(KIND=MPI_OFFSET_KIND)                           :: positionInitiale
    integer, dimension(MPI_STATUS_SIZE)                     :: statut
    integer                                                 :: loc_size
    integer, dimension(2)                                   :: ucount,ustart,dimsuids
    integer                                                 :: filetype
    !---------------------------------------------------------------------

    !---------------------------------------------------------------------
    ! calcul des deplacements et nombre tot de part
    !---------------------------------------------------------------------
    allocate(deplacements(0:nproc-1))
    call mpi_allgather(nparticles,1,MPI_INTEGER,deplacements,1,MPI_INTEGER,COMM3D,mpi_code)
    !---------------------------------------------------------------------
    if (rank>0) then
       deplacements(rank)=sum(deplacements(0:rank-1))
    end if
    deplacements(0)=0
    !---------------------------------------------------------------------
    call MPI_allreduce(nparticles,ntot,1,MPI_INTEGER,MPI_SUM,COMM3D,MPI_CODE)
    !---------------------------------------------------------------------
    
    !---------------------------------------------------------------------
    ! MPI init types
    !---------------------------------------------------------------------
    call MPI_TYPE_SIZE(MPI_DOUBLE_PRECISION,tailleDoublePrecision,MPI_CODE)
    call MPI_TYPE_SIZE(MPI_REAL,tailleReel,MPI_CODE)
    call MPI_TYPE_SIZE(MPI_CHARACTER,tailleCharacter,MPI_CODE)
    call MPI_TYPE_SIZE(MPI_INTEGER,tailleInteger,MPI_CODE)
    !---------------------------------------------------------------------

    binary_form       = 'C Binary'
    file_description1 = 'Ensight Model Geometry File Created by '
    file_description2 = 'WriteRectEnsightGeo Routine'
    node_id           = 'node id off'
    element_id        = 'element id off'
    part              = 'part'
    npart             = 1
    description_part  = 'Point field'
    blocck            = 'coordinates'

    !---------------------------------------------------------------------
    ! Geometry creation
    !---------------------------------------------------------------------
    ndv         = 3
    localName   = "prtcls"
    VarFileName = trim(localName)//trim(iterName)//'.geo'
    
    !---------------------------------------------------------------------
    call MPI_FILE_OPEN(COMM3D,VarFileName,MPI_MODE_WRONLY+MPI_MODE_CREATE,MPI_INFO_NULL,descripteur,MPI_CODE)
    !---------------------------------------------------------------------
    
    !---------------------------------------------------------------------
    ! Header
    !---------------------------------------------------------------------
    positionInitiale = 0
    !---------------------------------------------------------------------
    call MPI_FILE_WRITE_AT_ALL(descripteur,positionInitiale,binary_form,80,MPI_CHARACTER,statut,MPI_CODE)     
    positionInitiale = positionInitiale+80*tailleCharacter
!!$    !---------------------------------------------------------------------
    call MPI_FILE_WRITE_AT_ALL(descripteur,positionInitiale,file_description1,80,MPI_CHARACTER,statut,MPI_CODE)
    positionInitiale = positionInitiale+80*tailleCharacter
    !---------------------------------------------------------------------
    call MPI_FILE_WRITE_AT_ALL(descripteur,positionInitiale,file_description2,80,MPI_CHARACTER,statut,MPI_CODE)    
    positionInitiale = positionInitiale+80*tailleCharacter
    !---------------------------------------------------------------------
    call MPI_FILE_WRITE_AT_ALL(descripteur,positionInitiale,node_id,80,MPI_CHARACTER,statut,MPI_CODE)     
    positionInitiale = positionInitiale+80*tailleCharacter
    !---------------------------------------------------------------------
    call MPI_FILE_WRITE_AT_ALL(descripteur,positionInitiale,element_id,80,MPI_CHARACTER,statut,MPI_CODE)     
    positionInitiale = positionInitiale+80*tailleCharacter
    !---------------------------------------------------------------------
    call MPI_FILE_WRITE_AT_ALL(descripteur,positionInitiale,part,80,MPI_CHARACTER,statut,MPI_CODE)     
    positionInitiale = positionInitiale+80*tailleCharacter
    !---------------------------------------------------------------------
    call MPI_FILE_WRITE_AT_ALL(descripteur,positionInitiale,npart,1,MPI_INTEGER,statut,MPI_CODE)  
    positionInitiale = positionInitiale+tailleInteger
    !---------------------------------------------------------------------
    call MPI_FILE_WRITE_AT_ALL(descripteur,positionInitiale,description_part,80,MPI_CHARACTER,statut,MPI_CODE)     
    positionInitiale = positionInitiale+80*tailleCharacter
    !---------------------------------------------------------------------
    call MPI_FILE_WRITE_AT_ALL(descripteur,positionInitiale,blocck,80,MPI_CHARACTER,statut,MPI_CODE)     
    positionInitiale = positionInitiale+80*tailleCharacter
    !---------------------------------------------------------------------
    call MPI_FILE_WRITE_AT_ALL(descripteur,positionInitiale,ntot,1,MPI_INTEGER,statut,MPI_CODE)  
    positionInitiale = positionInitiale+tailleInteger
    !---------------------------------------------------------------------

    !---------------------------------------------------------------------
    ! Taille du bloc de données à écrire par le proc
    !---------------------------------------------------------------------
    ucount(1) = nparticles
    ucount(2) = ndv
    !---------------------------------------------------------------------
    ! Dimension totale du domaine à écrire
    !---------------------------------------------------------------------
    dimsuids(1) = ntot
    dimsuids(2) = ndv  ! nb de composantes si vecteur
    !---------------------------------------------------------------------
    allocate(var_sngl(1:ucount(1),1:ucount(2)))
    !---------------------------------------------------------------------
    !Passage en simple précision
    !---------------------------------------------------------------------
    do n=1,nparticles
       if (particles(n)%active) then 
          var_sngl(n,1:3) = particles(n)%r(1:3)
          if ( (var_sngl(n,1)<xmin) .or. (var_sngl(n,1)>xmax) ) var_sngl(n,1:3) = (/xmin, ymin, zmin/)
          if ( (var_sngl(n,2)<zmin) .or. (var_sngl(n,2)>ymax) ) var_sngl(n,1:3) = (/xmin, ymin, zmin/)
          if ( (var_sngl(n,3)<zmin) .or. (var_sngl(n,3)>zmax) ) var_sngl(n,1:3) = (/xmin, ymin, zmin/)
       else
          var_sngl(n,1:3) = (/xmin, ymin, zmin/)
       end if
    end do
    !---------------------------------------------------------------------

    !---------------------------------------------------------------------
    ! Position du sous-domaine à écrire dans le domaine complet
    ! Convention du langage C pour les indices   
    !---------------------------------------------------------------------
    ustart(1) = deplacements(rank)
    ustart(2) = 0
    !---------------------------------------------------------------------

    !---------------------------------------------------------------------
    ! Création du sous tableau du proc local
    !---------------------------------------------------------------------
    call mpi_type_create_subarray(2,dimsuids,ucount,ustart,mpi_order_fortran,mpi_real,filetype,MPI_CODE)
    call mpi_type_commit(filetype,MPI_CODE)
    loc_size=product(ucount)
    !---------------------------------------------------------------------

    !---------------------------------------------------------------------
    ! Ecriture
    !---------------------------------------------------------------------
    call mpi_file_set_view(descripteur, positionInitiale, mpi_real, filetype, "native", mpi_info_null, code)
    call mpi_file_write_all(descripteur, var_sngl, loc_size, mpi_real,mpi_status_ignore, code)
    !---------------------------------------------------------------------
    
    deallocate(var_sngl)

    call mpi_type_free(filetype, code)
    call MPI_FILE_CLOSE(descripteur,MPI_CODE)
    
    !---------------------------------------------------------------------
    ! field velocity
    !---------------------------------------------------------------------
    part     = 'part'
    npart    = 1
    blocck   = 'coordinates'
    !---------------------------------------------------------------------

    !---------------------------------------------------------------------
    ! File creation
    !---------------------------------------------------------------------
    ndv         = 3
    localName   = "prtcls_velocity"
    VarFileName = trim(localName)//trim(iterName)//'.vec'
    !---------------------------------------------------------------------

    !---------------------------------------------------------------------
    call MPI_FILE_OPEN(COMM3D,VarFileName,MPI_MODE_WRONLY+MPI_MODE_CREATE,MPI_INFO_NULL,descripteur,MPI_CODE)
    !---------------------------------------------------------------------

    !---------------------------------------------------------------------
    ! Header
    !---------------------------------------------------------------------
    positionInitiale = 0
    !---------------------------------------------------------------------
    call MPI_FILE_WRITE_AT_ALL(descripteur,positionInitiale,localName,80,MPI_CHARACTER,statut,MPI_CODE)
    positionInitiale = positionInitiale+80*tailleCharacter
    !---------------------------------------------------------------------
    call MPI_FILE_WRITE_AT_ALL(descripteur,positionInitiale,part,80,MPI_CHARACTER,statut,MPI_CODE)  
    positionInitiale = positionInitiale+80*tailleCharacter
    !---------------------------------------------------------------------
    call MPI_FILE_WRITE_AT_ALL(descripteur,positionInitiale,npart,1,MPI_INTEGER,statut,MPI_CODE)  
    positionInitiale = positionInitiale+tailleInteger
    !---------------------------------------------------------------------
    call MPI_FILE_WRITE_AT_ALL(descripteur,positionInitiale,blocck,80,MPI_CHARACTER,statut,MPI_CODE)
    positionInitiale = positionInitiale+80*tailleCharacter  
    !---------------------------------------------------------------------

    !---------------------------------------------------------------------
    ! Taille du bloc de données à écrire par le proc
    !---------------------------------------------------------------------
    ucount(1) = nparticles
    ucount(2) = ndv
    !---------------------------------------------------------------------
    ! Dimension totale du domaine à écrire
    !---------------------------------------------------------------------
    dimsuids(1) = ntot
    dimsuids(2) = ndv  ! nb de composantes si vecteur
    !---------------------------------------------------------------------
    allocate(var_sngl(1:ucount(1),1:ucount(2)))
    !---------------------------------------------------------------------
    !Passage en simple précision
    !---------------------------------------------------------------------
    do n=1,nparticles
       var_sngl(n,1:ndv) = particles(n)%u(1:3)
    end do
    !---------------------------------------------------------------------
    
    !---------------------------------------------------------------------
    ! Position du sous-domaine à écrire dans le domaine complet
    ! Convention du langage C pour les indices   
    !---------------------------------------------------------------------
    ustart(1) = deplacements(rank)
    ustart(2) = 0
    !---------------------------------------------------------------------
    
    !---------------------------------------------------------------------
    ! Création du sous tableau du proc local
    !---------------------------------------------------------------------
    call mpi_type_create_subarray(2,dimsuids,ucount,ustart,mpi_order_fortran,mpi_real,filetype,MPI_CODE)
    call mpi_type_commit(filetype,MPI_CODE)
    loc_size=product(ucount)
    !---------------------------------------------------------------------
    
    !---------------------------------------------------------------------
    ! Ecriture
    !---------------------------------------------------------------------
    call mpi_file_set_view(descripteur, positionInitiale, mpi_real, filetype, "native", mpi_info_null, code)
    call mpi_file_write_all(descripteur, var_sngl, loc_size, mpi_real,mpi_status_ignore, code)
    !---------------------------------------------------------------------
    
    deallocate(var_sngl)  

    call mpi_type_free(filetype, code)
    call MPI_FILE_CLOSE(descripteur,MPI_CODE)

    
    !---------------------------------------------------------------------
    ! field velocity
    !---------------------------------------------------------------------
    part     = 'part'
    npart    = 1
    blocck   = 'coordinates'
    !---------------------------------------------------------------------

    !---------------------------------------------------------------------
    ! File creation
    !---------------------------------------------------------------------
    ndv         = 1
    localName   = "prtcls_phi"
    VarFileName = trim(localName)//trim(iterName)//'.scl'
    !---------------------------------------------------------------------

    !---------------------------------------------------------------------
    call MPI_FILE_OPEN(COMM3D,VarFileName,MPI_MODE_WRONLY+MPI_MODE_CREATE,MPI_INFO_NULL,descripteur,MPI_CODE)
    !---------------------------------------------------------------------

    !---------------------------------------------------------------------
    ! Header
    !---------------------------------------------------------------------
    positionInitiale = 0
    !---------------------------------------------------------------------
    call MPI_FILE_WRITE_AT_ALL(descripteur,positionInitiale,localName,80,MPI_CHARACTER,statut,MPI_CODE)
    positionInitiale = positionInitiale+80*tailleCharacter
    !---------------------------------------------------------------------
    call MPI_FILE_WRITE_AT_ALL(descripteur,positionInitiale,part,80,MPI_CHARACTER,statut,MPI_CODE)  
    positionInitiale = positionInitiale+80*tailleCharacter
    !---------------------------------------------------------------------
    call MPI_FILE_WRITE_AT_ALL(descripteur,positionInitiale,npart,1,MPI_INTEGER,statut,MPI_CODE)  
    positionInitiale = positionInitiale+tailleInteger
    !---------------------------------------------------------------------
    call MPI_FILE_WRITE_AT_ALL(descripteur,positionInitiale,blocck,80,MPI_CHARACTER,statut,MPI_CODE)
    positionInitiale = positionInitiale+80*tailleCharacter  
    !---------------------------------------------------------------------

    !---------------------------------------------------------------------
    ! Taille du bloc de données à écrire par le proc
    !---------------------------------------------------------------------
    ucount(1) = nparticles
    ucount(2) = ndv
    !---------------------------------------------------------------------
    ! Dimension totale du domaine à écrire
    !---------------------------------------------------------------------
    dimsuids(1) = ntot
    dimsuids(2) = ndv  ! nb de composantes si vecteur
    !---------------------------------------------------------------------
    allocate(var_sngl(1:ucount(1),1:ucount(2)))
    !---------------------------------------------------------------------
    !Passage en simple précision
    !---------------------------------------------------------------------
    do n=1,nparticles
       var_sngl(n,1:ndv) = particles(n)%phi
    end do
    !---------------------------------------------------------------------

    !---------------------------------------------------------------------
    ! Position du sous-domaine à écrire dans le domaine complet
    ! Convention du langage C pour les indices   
    !---------------------------------------------------------------------
    ustart(1) = deplacements(rank)
    ustart(2) = 0
    !---------------------------------------------------------------------
    
    !---------------------------------------------------------------------
    ! Création du sous tableau du proc local
    !---------------------------------------------------------------------
    call mpi_type_create_subarray(2,dimsuids,ucount,ustart,mpi_order_fortran,mpi_real,filetype,MPI_CODE)
    call mpi_type_commit(filetype,MPI_CODE)
    loc_size=product(ucount)
    !---------------------------------------------------------------------
    
    !---------------------------------------------------------------------
    ! Ecriture
    !---------------------------------------------------------------------
    call mpi_file_set_view(descripteur, positionInitiale, mpi_real, filetype, "native", mpi_info_null, code)
    call mpi_file_write_all(descripteur, var_sngl, loc_size, mpi_real,mpi_status_ignore, code)
    !---------------------------------------------------------------------
    
    deallocate(var_sngl)  

    call mpi_type_free(filetype, code)
    call MPI_FILE_CLOSE(descripteur,MPI_CODE)
    
  end subroutine WriteEnsightPartGeoVar

  
  subroutine WriteRectEnsightGeo(FileName,mesh)
    !
    !    ******************************************************************************
    !                      subrout  WriteRectEnsightGeo
    !    ******************************************************************************
    !
    !    writes mesh data in Ensight's ascii format for rectilinear geometry
    !
    !    n1,n2,n3....: number of nodes in the x1,x2,x3 direction
    !    x1,x2,x3....: coordinates
    !

    implicit none

    character(LEN=80), intent(IN) :: FileName
    type(grid_t), intent(IN)      :: mesh

    character(LEN=80)             :: binary_form
    character(LEN=80)             :: file_description1,file_description2
    character(LEN=80)             :: node_id,element_id
    character(LEN=80)             :: part,description_part,blocck

    integer                       :: FileUnit
    integer                       :: i,j,k,npart,iskip
    integer                       :: isize,jsize,ksize
    integer                       :: reclength

    FileUnit = 666

    binary_form       = 'C Binary'
    file_description1 = 'Ensight Model Geometry File Created by '
    file_description2 = 'WriteRectEnsightGeo Routine'
    node_id           = 'node id off'
    element_id        = 'element id off'
    part              = 'part'
    npart             = 1
    description_part  = 'Sortie Ensight'
    blocck            = 'block rectilinear'

    iskip=1
    isize=(mesh%gex-mesh%gsx+1)/iskip
    jsize=(mesh%gey-mesh%gsy+1)/iskip
    ksize=(mesh%gez-mesh%gsz+1)/iskip
    
    if (ksize == 0) ksize = 1 !AV

    reclength=80*8+4*(4+isize+jsize+ksize)

    open (unit=FileUnit,                &
         & file=trim(FileName)//'.geo', &
         & form='UNFORMATTED',          &
         & access="direct",             &
         & recl=reclength,              &
         & status='replace')

    write(unit=FileUnit,rec=1) binary_form,           &
         & file_description1,                         &
         & file_description2,                         &
         & node_id,                                   &
         & element_id,                                &
         & part,npart,                                &
         & description_part,                          &
         & blocck,                                    &
         & isize,jsize,ksize,                         &
         & (real(sngl(mesh%x(i)),4),i=mesh%gsx,mesh%gex,iskip), &
         & (real(sngl(mesh%y(j)),4),j=mesh%gsy,mesh%gey,iskip), &
         & (real(sngl(mesh%z(k)),4),k=mesh%gsz,mesh%gez,iskip)

    close(FileUnit)

  end subroutine WriteRectEnsightGeo
  
  
  subroutine WriteEnsightCase(nbVar,varName,caseName,VarType,current_iteration,current_time,id_mesh)

    !
    !    ******************************************************************************
    !   EnsightCase helps to write a Ensight's case file
    !
    !    VarName.....: Name of the variable
    !    VarType.....: 1 => Scalar       3 => Vector
    
    
    implicit none

    integer                                       :: nbVar
    integer, dimension(nbVar),intent(IN)          :: VarType

    integer, intent(IN)                           :: id_mesh
    integer, intent(IN)                           :: current_iteration
    real(8), intent(IN)                           :: current_time

    character(LEN=80),dimension(nbVar),intent(IN) :: Varname
    character(LEN=80),intent(IN)                  :: caseName
    integer                                       :: FileUnit,i,j

    character (LEN=30)                            :: dummy
    integer                                       :: nb_fichiers
    integer , dimension(:) , allocatable          :: filenames
    real(8) , dimension(:) , allocatable          :: timevalues
    real(8)                                       :: temps_final
    logical, dimension(0:3)                       :: ok_ecriture
    logical, dimension(0:3), save                 :: sortie_case_existant=.false.

    FileUnit             = 666
    nb_fichiers          = 0 
    temps_final          = 0
    ok_ecriture(id_mesh) = .true.

!!$    IF(reprise .NE. 0) THEN 
!!$       sortie_case_existant = .TRUE.
!!$    END IF

    if (sortie_case_existant(id_mesh)) then

       !Ouverture du fichier
       open(FileUnit,FILE="EnSight_"//trim(caseName)//'.case')

       !On passe toutes les lignes qui ne nous intéressent pas
       do i = 1,5
          read(FileUnit,*) dummy     !FORMAT ... => ... VARIABLE
       end do

       do i = 1, nbVar
          read(FileUnit,*) dummy        !on passe les *.vec et *.scl
       end do

       read(FileUnit,*) dummy     !TIME
       read(FileUnit,*) dummy     !time set
       read(FileUnit,*) dummy,dummy,dummy, nb_fichiers !number of steps: X
       
       !On alloue et on lit dans la foulée les noms des variables    
       read(FileUnit,*) dummy !Ligne "filename numbers:"
       allocate(filenames(1:nb_fichiers))
       do i = 1,nb_fichiers
          read(FileUnit,*) filenames(i)
       end do

       !On lit les différents pas de temps à reprendre
       read(FileUnit,*) dummy !Ligne "timevalues"
       allocate(timevalues(1:nb_fichiers))
       do i = 1,nb_fichiers
          read(FileUnit,*) timevalues(i)
       end do

       !On note le temps final
       temps_final = timevalues(nb_fichiers)

       !Fermeture
       close(FileUnit)

    end if   
    
    if (ok_ecriture(id_mesh)) then

       open (FileUnit,FILE="EnSight_"//trim(caseName)//'.case',STATUS='replace')
       write(FileUnit,10) trim(caseName)//'.geo'    !format en fin de routine

       do i=1,nbVar
          if(VarType(i)==1) then 
             write(FileUnit,15) i,trim(Varname(i)),trim(caseName)//'_'//trim(Varname(i))//'_'//'**********.scl'
          else 
             write(FileUnit,25) i,trim(Varname(i)),trim(caseName)//'_'//trim(Varname(i))//'_'//'**********.vec'
          end if
       end do

       write(FileUnit,45) nb_fichiers+1    !Format en fin de routine

       !Ecriture numéro de fichiers
       write(FileUnit,'(A)') 'filename numbers: '
       if (sortie_case_existant(id_mesh)) then
          do j = 1, nb_fichiers
             write(FileUnit,'(I10.10)') filenames(j)
          end do
       end if
       write(FileUnit,'(I10.10)') current_iteration
       
       !Ecriture temps correspondants
       write(FileUnit,'(A)') 'time values: '   
       if ( sortie_case_existant(id_mesh)) then
          do j = 1, nb_fichiers
             write(FileUnit,'(E13.5)') timevalues(j)    
          end do
       endif
       write(FileUnit,'(E13.5)') current_time

       
       close(FileUnit)
    end if

    if (sortie_case_existant(id_mesh)) then
       deallocate(timevalues,filenames)
    end if

    sortie_case_existant(id_mesh) = .true.

10  format('FORMAT'            ,/ ,'type: ensight gold',//,'GEOMETRY'          ,/ ,'model:    ',A         ,//,'VARIABLE')   
15  format('scalar per node: V',i2.2,'-',A,'   ', A)
25  format('vector per node: V',i2.2,'-',A,'   ', A)
45  format(/,'TIME            '      ,/,'time set: 1     '      ,/,'number of steps: '      ,i10 )

  end subroutine WriteEnsightCase

  subroutine WriteEnsightPartCase(caseName,current_iteration,current_time)
    implicit none
    
    integer, intent(IN)                           :: current_iteration
    real(8), intent(IN)                           :: current_time
    character(LEN=80),intent(IN)                  :: caseName

    integer                                       :: FileUnit,i,j,nbVar
    character (LEN=30)                            :: dummy
    integer                                       :: nb_fichiers
    integer , dimension(:) , allocatable          :: filenames
    real(8) , dimension(:) , allocatable          :: timevalues
    real(8)                                       :: temps_final
    logical                                       :: ok_ecriture
    logical, save                                 :: sortie_case_existant=.false.

    FileUnit    = 666
    nb_fichiers = 0 
    temps_final = 0
    ok_ecriture = .true.

    nbVar =      2

    if (sortie_case_existant) then

       !Ouverture du fichier
       open(FileUnit,FILE=trim(caseName)//'.case')

       !On passe toutes les lignes qui ne nous intéressent pas
       do i = 1,5
          read(FileUnit,*) dummy     !FORMAT ... => ... VARIABLE
       end do

       do i = 1, nbVar
          read(FileUnit,*) dummy        !on passe les *.vec et *.scl
       end do

       read(FileUnit,*) dummy     !TIME
       read(FileUnit,*) dummy     !time set
       read(FileUnit,*) dummy,dummy,dummy, nb_fichiers !number of steps: X
       
       !On alloue et on lit dans la foulée les noms des variables    
       read(FileUnit,*) dummy !Ligne "filename numbers:"
       allocate(filenames(1:nb_fichiers))
       do i = 1,nb_fichiers
          read(FileUnit,*) filenames(i)
       end do

       !On lit les différents pas de temps à reprendre
       read(FileUnit,*) dummy !Ligne "timevalues"
       allocate(timevalues(1:nb_fichiers))
       do i = 1,nb_fichiers
          read(FileUnit,*) timevalues(i)
       end do

       !On note le temps final
       temps_final = timevalues(nb_fichiers)

       !Fermeture
       close(FileUnit)

    end if   

!!$    !Evite les doublons dans les écritures
!!$    IF(reprise .NE. 0) THEN
!!$       IF( current_iteration == filenames(nb_fichiers)) THEN
!!$          ok_ecriture = .FALSE.
!!$       END IF
!!$    END IF
    
    if(ok_ecriture) then

       open (FileUnit,FILE=trim(caseName)//'.case',STATUS='replace')
       write(FileUnit,10) 'prtcls**********.geo'    !format en fin de routine
       
       write(FileUnit,25) 1,"Velocity","prtcls_velocity"//'**********.vec'
       write(FileUnit,15) 2,"phi","prtcls_phi"//'**********.scl'

       write(FileUnit,45) nb_fichiers+1    !Format en fin de routine

       !Ecriture numéro de fichiers
       write(FileUnit,'(A)') 'filename numbers: '
       if (sortie_case_existant) then
          do j = 1, nb_fichiers
             write(FileUnit,'(I10.10)') filenames(j)
          end do
       end if
       write(FileUnit,'(I10.10)') current_iteration
       
       !Ecriture temps correspondants
       write(FileUnit,'(A)') 'time values: '   
       if ( sortie_case_existant) then
          do j = 1, nb_fichiers
             write(FileUnit,'(E13.5)') timevalues(j)    
          end do
       endif
       write(FileUnit,'(E13.5)') current_time

       
       close(FileUnit)
    end if

    if( sortie_case_existant) then
       deallocate(timevalues,filenames)
    end if

    sortie_case_existant = .true.
    
10  format('FORMAT'            ,/ ,'type: ensight gold',//,'GEOMETRY'          ,/ ,'model:    ',A         ,//,'VARIABLE')   
15  format('scalar per node: V',i2.2,'-',A,'   ', A)
25  format('vector per node: V',i2.2,'-',A,'   ', A)
45  format(/,'TIME            '      ,/,'time set: 1     '      ,/,'number of steps: '      ,i10 )

  end subroutine WriteEnsightPartCase

  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
end module mod_InOut
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
