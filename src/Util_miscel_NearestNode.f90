!===============================================================================
module Bib_VOFLag_NearestNode
  !===============================================================================
  contains
  !---------------------------------------------------------------------------  
  !> @author amine chadil, benoit trouette
  ! 
  !> @brief cette routine retourne les indices du noeud de la grille le plus proche du
  !> point 
  !
  !> @todo attention la recherche dans un maillage regulier avec un halo n'est pas 
  !> implémentée
  !
  !> @param[in]  point      : les coordonnées du point 
  !> @param[in]  gridChoice : 0 pour le maillage de pression, 1 pour le maillage de la 
  !>   premiere composante de la vitesse, 2 pour le maillage de la deuxieme composante 
  !>   de la vitesse, 3 pour le maillage de la troisieme composante de la vitesse
  !> @param[in]  InD        : "true" si uniquement pour le maillage appartenant strictement
  !>   au proc courant, "false" si etendu au halo
  !> @param[out] ijk        : les indices du noeud de la grille le plus proche du point
  !---------------------------------------------------------------------------  
  subroutine NearestNode(grid_x,grid_y,grid_z,grid_xu,grid_yv,grid_zw,dx,dy,dz,dxu, &
                         dyv,dzw,point,gridChoice,InD,regular_mesh,ndim,ijk)
    !===============================================================================
    !modules
    !===============================================================================
    !-------------------------------------------------------------------------------
    !Modules de definition des structures et variables => src/mod/module_...f90
    !-------------------------------------------------------------------------------
    use mod_Parameters, only : deeptracking,sx,ex,sy,ey,sz,ez,sxu,exu,syv,eyv, &
                               szw,ezw,gx,gy,gz
    !-------------------------------------------------------------------------------

    !===============================================================================
    !declarations des variables
    !===============================================================================
    implicit none
    !-------------------------------------------------------------------------------
    !variables globales
    !-------------------------------------------------------------------------------
    integer, dimension(3)             , intent(out) :: ijk
    real(8), dimension(3)             , intent(in)  :: point
    real(8), dimension(:), allocatable, intent(in)  :: grid_xu,grid_yv, grid_zw
    real(8), dimension(:), allocatable, intent(in)  :: grid_x,grid_y, grid_z
    real(8), dimension(:), allocatable, intent(in)  :: dx,dy,dz
    real(8), dimension(:), allocatable, intent(in)  :: dxu,dyv,dzw
    integer                           , intent(in)  :: gridChoice
    integer                           , intent(in)  :: ndim
    logical                           , intent(in)  :: InD,regular_mesh
    !-------------------------------------------------------------------------------
    !variables locales 
    !-------------------------------------------------------------------------------
    integer                            :: i,is,ie
    logical                            :: found
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'entree NearestNode'
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Initialization
    !-------------------------------------------------------------------------------
    ijk=1

    !-------------------------------------------------------------------------------
    ! pressure mesh
    !-------------------------------------------------------------------------------
    if (gridChoice==0 .or. gridChoice==2 .or. gridChoice==3) then
      !-------------------------------------------------------------------------------
      ! x direction
      !-------------------------------------------------------------------------------
      is=sx
      ie=ex
      if (.not. InD) is=is-gx;ie=ie+gx
      if (regular_mesh) then
        ijk(1)=nint((point(1)-grid_x(0))/dx(is))
        found = (ijk(1).ge.is-1 .and. ijk(1).le.ie+1)
      else
        found=.false.
        do i=is-1,ie
          if (point(1)>=grid_x(i).and.point(1)<grid_x(i+1)) then
            ijk(1)=i
            found=.true.
            exit
          end if
        end do
      end if
      if (.not.found) then 
        stop "Impossible de localiser le point demandé: x direction"
      end if
    end if

    if (gridChoice==0 .or. gridChoice==1 .or. gridChoice==3) then
      !-------------------------------------------------------------------------------
      ! y direction
      !-------------------------------------------------------------------------------
      is=sy
      ie=ey
      if (.not. InD) is=is-gy;ie=ie+gy
      if (regular_mesh) then
        ijk(2)=nint((point(2)-grid_y(0))/dy(is))
        found = (ijk(2).ge.is-1 .and. ijk(2).le.ie+1)
      else
        found=.false.
        do i=is-1,ie
          if (point(2)>=grid_y(i).and.point(2)<grid_y(i+1)) then
            ijk(2)=i
            found=.true.
            exit
          end if
        end do
      end if
      if (.not.found) then 
        stop "Impossible de localiser le point demandé: y direction"
      end if
    end if

    if (ndim==3 .and. (gridChoice==0 .or. gridChoice==1 .or. gridChoice==2)) then 
      !-------------------------------------------------------------------------------
      ! z direction
      !-------------------------------------------------------------------------------
      is=sz
      ie=ez
      if (.not. InD) is=is-gz;ie=ie+gz
      if (regular_mesh) then
        ijk(3)=nint((point(3)-grid_z(0))/dz(is))
        found = (ijk(3).ge.is-1 .and. ijk(3).le.ie+1)
      else
        found=.false.
        do i=is-1,ie
            if (point(3)>=grid_z(i).and.point(3)<grid_z(i+1)) then
              ijk(3)=i
              found=.true.
              exit
            end if
        end do
      end if
      if (.not.found) then 
        stop "Impossible de localiser le point demandé: z direction"
      end if
    end if 
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! X-velocity mesh
    !-------------------------------------------------------------------------------
    if (gridChoice==1) then
      is=sxu
      ie=exu
      if (.not. InD) is=is-gx;ie=ie+gx
      if (regular_mesh) then
        ijk(1)=nint((point(1)-grid_xu(0))/dxu(is))
        found = (ijk(1).ge.is-1 .and. ijk(1).le.ie+1)
      else
        found=.false.
        do i=is-1,ie
          if (point(1)>=grid_xu(i-1).and.point(1)<grid_xu(i)) then
            ijk(1)=i
            found=.true.
            exit
          end if
        end do
      end if
      if (.not.found) then 
        stop "Impossible de localiser le point demandé: X-velocity mesh"
      end if
    end if

    !-------------------------------------------------------------------------------
    ! Y-velocity mesh
    !-------------------------------------------------------------------------------
    if (gridChoice==2) then
      is=syv
      ie=eyv
      if (.not. InD) is=is-gy;ie=ie+gy
      if (regular_mesh) then
        ijk(2)=nint((point(2)-grid_yv(0))/dyv(is))
        found = (ijk(2).ge.is-1 .and. ijk(2).le.ie+1)
      else
        found=.false.
        do i=is-1,ie
          if (point(2)>=grid_yv(i-1).and.point(2)<grid_yv(i)) then
            ijk(2)=i
            found=.true.
            exit
          end if
        end do
      end if
      if (.not.found) then 
        stop "Impossible de localiser le point demandé: Y-velocity mesh"
      end if
    end if

    !-------------------------------------------------------------------------------
    ! Z-velocity mesh
    !-------------------------------------------------------------------------------
    if (ndim==3 .and. gridChoice==3) then
      is=szw
      ie=ezw
      if (.not. InD) is=is-gz;ie=ie+gz
      if (regular_mesh) then
        ijk(3)=nint((point(3)-grid_zw(0))/dzw(is))
        found = (ijk(3).ge.is-1 .and. ijk(3).le.ie+1)
      else
        found=.false.
        do i=is-1,ie
          if (point(3)>=grid_zw(i-1).and.point(3)<grid_zw(i)) then
            ijk(3)=i
            found=.true.
            exit
          end if
        end do
      end if
      if (.not.found) then 
        stop "Impossible de localiser le point demandé: Z-velocity mesh"
      end if
    end if

    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'sortie NearestNode'
    !-------------------------------------------------------------------------------
  end subroutine NearestNode

  !===============================================================================
end module Bib_VOFLag_NearestNode
!===============================================================================