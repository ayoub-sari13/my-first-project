!===============================================================================
!Front-tracking structure
module mod_struct_front_tracking
  use mod_Parameters, only:dim,dx,pi
  !===============================================================================

  type struct_front_tracking 
     integer :: multiplicateur_de_sommet=2,multiplicateur_d_element=2
     integer :: multiplicateur_de_bulle=2
     integer :: nbbul,nbbulmax,a
     real(8) :: xc,yc,r0,minxy,minxyz,xa,ya
     real(8) :: ftdmax,ftdmin,ftd,ftdeb
     real(8) :: ratio_compact=0.5_8
     real(8) :: sin_theta_lim=0.5_8,cos_theta_lim=sqrt(3._8)/2
     real(8) :: theta_lim=pi/6._8
     real(8) :: ds_lim
     integer :: compteur_coalescence=20
     integer :: compteur_init=-1
     logical :: compteur_desenrichissement=.false.
  end type struct_front_tracking
  !-------------------------------------------------------------------------------

  type element
     integer, allocatable, dimension(:) :: nusom,ele_vois
     logical                            :: mask
     logical                            :: enrichir,desenrichir
     logical                            :: break
  end type element
  !-------------------------------------------------------------------------------

  type sommet
     real(8), allocatable, dimension(:) :: xyz,xyz_old,vp,gradu1,gradv1,gradu2,gradv2,n_node
     real(8), allocatable, dimension(:) :: vp_old,gradu1_old,gradv1_old,gradu2_old,gradv2_old
     real(8), allocatable, dimension(:) :: vp_star,gradu1_star,gradv1_star,gradu2_star,gradv2_star
     real(8)                            :: P1,P2,kappa
     logical                            :: mask,notfix,desenrichissement
     integer                            :: compteur
  end type sommet
  !-------------------------------------------------------------------------------
  
  type bulle
     type(sommet),  allocatable, dimension(:) :: som
     type(element), allocatable, dimension(:) :: ele
     integer                                  :: nbele,nbelemax,nbsom,nbsommax,nbelebis,nbsombis,nbele_vrai,nbsom_vrai
     real(8)                                  :: volume_2d,r0,xc,yc
     logical                                  :: mask
     integer                                  :: nt
  end type bulle
  !-------------------------------------------------------------------------------
  
  type subdivision
     integer,dimension(:),allocatable :: selement
     integer                          :: nb_intersection
  end type subdivision
  !-------------------------------------------------------------------------------
  
  type raycasting
     type(subdivision), allocatable, dimension(:,:,:) :: smaille
     integer, dimension(:), allocatable               :: element
     integer, dimension(:), allocatable               :: bulbis
     logical, dimension(:), allocatable               :: mask
  end type raycasting
  !-------------------------------------------------------------------------------
  
  type intersect
     real(8), allocatable, dimension(:,:) :: xyzI,vI,gradu1,gradv1,gradu2,gradv2
     logical, allocatable, dimension(:)   :: mask
     integer, allocatable, dimension(:)   :: num_ele
     integer, allocatable, dimension(:)   :: num_bul
     real(8), allocatable, dimension(:)   :: PI1,PI2,kappaI
  end type intersect
  !-------------------------------------------------------------------------------
  
  type facet
     type(intersect), dimension(:), allocatable :: face_x,face_y
  end type facet
  !===============================================================================
end module mod_struct_front_tracking
!===============================================================================

