!===============================================================================
module Bib_VOFLag_SubDomain_Limited
    !===============================================================================
    contains
    !-----------------------------------------------------------------------------  
    !> @author jorge cesar brandle de motta, amine chadil
    ! 
    !> @brief calcule les coordonnees des sous-domaines.
    !> @todo il faut enlever les dx,dy,dz (ne servent à rien)
    !
    !> @todo remplacer grid_(x,y,z) par grid_(ux,vy,wz),
    !        les coordonnees des sous-domaines sont donnees directement par
    !        grid_(ux,vy,wz) 
    !-----------------------------------------------------------------------------
    subroutine SubDomain_Limited(s_domaines_int,grid_x,grid_y,grid_z)
        !===============================================================================
        !modules
        !===============================================================================
        !-------------------------------------------------------------------------------
        !Modules de definition des structures et variables => src/mod/module_...f90
        !-------------------------------------------------------------------------------
        use Module_VOFLag_SubDomain_Data, only : m_dims,m_coords
        use mod_Parameters,               only : deeptracking,gx,gy,gz,&
                                                 sx,sy,sz,ex,ey,ez,nproc
        !-------------------------------------------------------------------------------

        !===============================================================================
        !declarations des variables
        !===============================================================================
        implicit none
        !-------------------------------------------------------------------------------
        !variables globales
        !-------------------------------------------------------------------------------
        real(8), dimension(2,3)           , intent(inout) :: s_domaines_int
        real(8), dimension(:), allocatable, intent(in)    :: grid_x
        real(8), dimension(:), allocatable, intent(in)    :: grid_y
        real(8), dimension(:), allocatable, intent(in)    :: grid_z
        !-------------------------------------------------------------------------------
        !variables locales 
        !-------------------------------------------------------------------------------
        !-------------------------------------------------------------------------------

        !-------------------------------------------------------------------------------
        if (deeptracking) write(*,*) 'entree SubDomain_Limited'
        !------------------------------------------------------------------------------- 
        ! debut
        s_domaines_int(1,1)=grid_x(sx)
        s_domaines_int(1,2)=grid_y(sy)
        s_domaines_int(1,3)=grid_z(sz)
        ! fin 
        s_domaines_int(2,1)=grid_x(ex)
        s_domaines_int(2,2)=grid_y(ey)
        s_domaines_int(2,3)=grid_z(ez)
        !-------------------------------------------------------------------------------
        ! ajuster les coordonnees pour que les domaines se collent
        !------------------------------------------------------------------------------- 
        if(nproc .gt. 1) then
            if (m_coords(1).ne.0) &
                 s_domaines_int(1,1) = s_domaines_int(1,1)-(grid_x(sx)-grid_x(sx-1))*0.5d0
            if (m_coords(2).ne.0) &
                 s_domaines_int(1,2) = s_domaines_int(1,2)-(grid_y(sy)-grid_y(sy-1))*0.5d0
            if (m_coords(3).ne.0) &
                 s_domaines_int(1,3) = s_domaines_int(1,3)-(grid_z(sz)-grid_z(sz-1))*0.5d0
            if (m_coords(1).ne.m_dims(1)-1) &
                 s_domaines_int(2,1) = s_domaines_int(2,1)+(grid_x(ex+1)-grid_x(ex))*0.5d0
            if (m_coords(2).ne.m_dims(2)-1) &
                 s_domaines_int(2,2) = s_domaines_int(2,2)+(grid_y(ey+1)-grid_y(ey))*0.5d0
            if (m_coords(3).ne.m_dims(3)-1) &
                 s_domaines_int(2,3) = s_domaines_int(2,3)+(grid_z(ez+1)-grid_z(ez))*0.5d0
        end if
        
        !-------------------------------------------------------------------------------
        if (deeptracking) write(*,*) 'sortie SubDomain_Limited'
        !-------------------------------------------------------------------------------
    end subroutine SubDomain_Limited

    !===============================================================================
end module Bib_VOFLag_SubDomain_Limited
!===============================================================================
    


