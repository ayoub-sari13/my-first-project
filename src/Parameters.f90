!========================================================================
!
!                   FUGU Version 1.0.0
!
!========================================================================
!
! Copyright  St�phane Vincent
!
! stephane.vincent@u-pem.fr          straightparadigm@gmail.com
!
! This file is part of the FUGU Fortran library, a set of Computational 
! Fluid Dynamics subroutines devoted to the simulation of multi-scale
! multi-phase flows for resolved scale interfaces (liquid/gas/solid).
!
!========================================================================
!**
!**   NAME       : Parameters.f90
!**
!**   AUTHOR     : St�phane Vincent
!**
!**   FUNCTION   : Modules of variables and initialization subroutines
!**
!**   DATES      : Version 1.0.0  : from : june, 16, 2015
!**
!========================================================================

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#include "precomp.h"
module mod_Parameters
  use mod_struct_cfl
  use mod_struct_data
  use mod_struct_grid
  use mod_struct_init
  use mod_struct_solver
  use mod_struct_stl_file
  implicit none


  

  !---------------------------------------------------------------------
  !  MPI constants : rank and number of process
  !---------------------------------------------------------------------
  integer               :: rank
  integer               :: nproc
  !--------------------------------------------------------------
  ! exchange length in each direction (slice length)
  !--------------------------------------------------------------
  integer               :: slx     = 1
  integer               :: sly     = 1
  integer               :: slz     = 1
  !--------------------------------------------------------------
  ! number of process in each direction 
  !--------------------------------------------------------------
  integer, dimension(3) :: mpi_dim = 0
  !--------------------------------------------------------------

  namelist /nml_mpi/ mpi_dim,slx,sly,slz,nproc

  !********************************************************************!
  !********************************************************************!
  !********************************************************************!
  ! Geometrical and mesh properties of the problem 
  !********************************************************************!
  !********************************************************************!
  !********************************************************************!
  !---------------------------------------------------------------------
  ! Space dimensions (2 or 3)
  !---------------------------------------------------------------------
  integer, protected                 :: dim      = 2
  integer, protected                 :: switch2D3D
  logical, dimension(3), protected   :: periodic = .false.
  !---------------------------------------------------------------------
  ! Problem physical dimension along x, y (and z in 3D)
  !---------------------------------------------------------------------
  real(8), protected                 :: xmin     = 0
  real(8), protected                 :: xmax     = 1
  real(8), protected                 :: ymin     = 0
  real(8), protected                 :: ymax     = 1
  real(8), protected                 :: zmin     = 0
  real(8), protected                 :: zmax     = 1
  real(8), protected, dimension(3,2) :: xyzBounds
  !---------------------------------------------------------------------
  !   Grid properties
  !---------------------------------------------------------------------
  integer, protected                 :: nx
  integer, protected                 :: ny
  integer, protected                 :: nz
  
  type type_block
     !----------------------------------------
     !      nc = number of cells
     !    type = 1 ! regular
     !           2 ! chebychev
     !           3 ! expo
     !    dmin = smallest requiered cell size
     ! connect = 0 ! xmin
     !           1 ! xmax
     !----------------------------------------
     integer :: nc
     integer :: shift   = 0
     integer :: type    = 1
     integer :: connect = 0
     real(8) :: dmin    = 0
     real(8) :: min,max
  end type type_block
  
  type type_grid
     integer                         :: nblck
     type(type_block), dimension(10) :: blck
  end type type_grid

  type(type_grid), dimension(3)      :: grid 
  logical, protected                 :: regular_mesh=.true.
  
  !---------------------------------------------------------------------
  ! namelist for input parameters
  !---------------------------------------------------------------------
  namelist /nml_grid/ grid
  !---------------------------------------------------------------------

  !---------------------------------------------------------------------
  !   Grid Global Indexes
  !---------------------------------------------------------------------
  integer, protected :: gsx,gex,gsy,gey,gsz,gez
  integer, protected :: gsxu,gexu,gsyu,geyu,gszu,gezu
  integer, protected :: gsxv,gexv,gsyv,geyv,gszv,gezv
  integer, protected :: gsxw,gexw,gsyw,geyw,gszw,gezw
  !---------------------------------------------------------------------
  !   Grid Indexes
  !        for 2D and 3D simulations (the values are set in Fugu.f90 file)
  !---------------------------------------------------------------------
  integer :: sx,ex,sy,ey,sz,ez             ! Starting and ending indices for scalar mesh
  integer :: sxu,exu,syu,eyu,szu,ezu       ! Starting and ending indices for u-velocity mesh
  integer :: sxv,exv,syv,eyv,szv,ezv       ! Starting and ending indices for v-velocity mesh
  integer :: sxw,exw,syw,eyw,szw,ezw       ! Starting and ending indices for w-velocity mesh
  integer :: sxs,exs,sys,eys,szs,ezs       ! Solvers indexes 
  integer :: sxus,exus,syus,eyus,szus,ezus ! 
  integer :: sxvs,exvs,syvs,eyvs,szvs,ezvs ! 
  integer :: sxws,exws,syws,eyws,szws,ezws ! 
  integer :: gxs,gys,gzs                   ! 
  integer :: gx,gy,gz                      ! Indices for space shift of arrays
  !---------------------------------------------------------------------
  !   Unknowns numbers
  !---------------------------------------------------------------------
  integer :: nb_Vx                  ! number of value for u
  integer :: nb_Vy
  integer :: nb_Vz
  integer :: nb_V
  integer :: nb_VP                   ! number of value for V & P (if fully coupled)
  integer :: nb_P,nb_T
  !---------------------------------------------------------------------
  integer :: nb_Vx_no_ghost
  integer :: nb_Vy_no_ghost
  integer :: nb_Vz_no_ghost
  integer :: nb_V_no_ghost
  integer :: nb_VP_no_ghost
  integer :: nb_P_no_ghost
  !---------------------------------------------------------------------
  ! Cell properties
  !---------------------------------------------------------------------
  real(8), protected                            :: dx_min,dx_max
  real(8), protected                            :: dy_min,dy_max
  real(8), protected                            :: dz_min,dz_max
  real(8), allocatable, dimension(:), protected :: dx,dxu
  real(8), allocatable, dimension(:), protected :: dy,dyv
  real(8), allocatable, dimension(:), protected :: dz,dzw  
  real(8), allocatable, dimension(:), protected :: dx2,dxu2
  real(8), allocatable, dimension(:), protected :: dy2,dyv2
  real(8), allocatable, dimension(:), protected :: dz2,dzw2
  real(8), allocatable, dimension(:), protected :: ddx,ddxu
  real(8), allocatable, dimension(:), protected :: ddy,ddyv
  real(8), allocatable, dimension(:), protected :: ddz,ddzw
  real(8), allocatable, dimension(:), protected :: grid_x,grid_xu
  real(8), allocatable, dimension(:), protected :: grid_y,grid_yv
  real(8), allocatable, dimension(:), protected :: grid_z,grid_zw
  !---------------------------------------------------------------------
  ! mesh structure that will eventually replace the indexes
  !---------------------------------------------------------------------
  integer, protected :: mesh_dual_LvL     = 1
  logical, protected :: mesh_dual_is_even = .false.
  type(grid_t)       :: mesh
  type(grid_t)       :: mesh_dual
  !---------------------------------------------------------------------
  ! namelist for input parameters
  !---------------------------------------------------------------------
  namelist /nml_space_integration/ dim,periodic, &
       & xmin,xmax,ymin,ymax,zmin,zmax,nx,ny,nz, &
       & mesh_dual_LvL
  !********************************************************************!
  !********************************************************************!
  !********************************************************************!
  ! End of geometrical and mesh properties of the problem 
  !********************************************************************!
  !********************************************************************!
  !********************************************************************!















  !********************************************************************!
  !********************************************************************!
  !********************************************************************!
  ! Time discretization parameters
  !********************************************************************!
  !********************************************************************!
  !********************************************************************!
  integer, protected :: nstep        = 1000   ! Number of time iterations
  !---------------------------------------------------------------------
  real(8)            :: dt           = 1d-2    ! time step
  real(8), protected :: time_obj     = -1      ! time objective if >= 0
  type(cfl_t)        :: cfl
  !---------------------------------------------------------------------
  ! First order Euler time integration scheme Euler=1
  !---------------------------------------------------------------------
  integer, protected :: Euler        = 1
  real(8)            :: coef_time_1  = 1
  real(8)            :: coef_time_2  = -1
  real(8)            :: coef_time_3  = 0
  !---------------------------------------------------------------------
  ! exit time loop
  !---------------------------------------------------------------------
  logical            :: time_loop_exit=.false.
  !---------------------------------------------------------------------
  ! namelist for input parameters
  !---------------------------------------------------------------------
  namelist /nml_time_integration/ dt,cfl,     &
       & nstep,Euler,time_loop_exit,time_obj
  !********************************************************************!
  !********************************************************************!
  !********************************************************************!
  ! End of time discretization parameters
  !********************************************************************!
  !********************************************************************!
  !********************************************************************!













  !********************************************************************!
  !********************************************************************!
  !********************************************************************!
  ! Storing datas and files
  !********************************************************************!
  !********************************************************************!
  !********************************************************************!


  !---------------------------------------------------------------------
  integer                       :: restart_EQ = -1    ! Restart of a simulation (# >= 0 => read restart_EQ#.bin)
  !---------------------------------------------------------------------
  ! screen display
  !---------------------------------------------------------------------
  integer, protected            :: istep = 10
  !---------------------------------------------------------------------

  !---------------------------------------------------------------------
  ! Inputs new-reading
  !---------------------------------------------------------------------
  integer, protected            :: ireadin = 10
  !---------------------------------------------------------------------

  !---------------------------------------------------------------------
  ! tecplot file
  ! - if value /= 0
  !---------------------------------------------------------------------
  type(file_write_t)            :: file_tec
  !---------------------------------------------------------------------
  ! VTK File
  ! - if value /= 0
  !---------------------------------------------------------------------
  type(file_write_t)            :: file_vtk
  type(file_write_t)            :: file_vtk_prtcl
  !---------------------------------------------------------------------
  ! Ensight Gold
  ! - if value /= 0
  !---------------------------------------------------------------------
  type(file_write_t)            :: file_egld
  type(file_write_t)            :: file_egld_prtcl
  !---------------------------------------------------------------------
  ! Outputs plots
  !---------------------------------------------------------------------
  integer, protected            :: iplot = 0
  !---------------------------------------------------------------------
  ! Solver residuals in fort.50 if ires > 0
  !---------------------------------------------------------------------
  integer, protected            :: ires  = -1
  !---------------------------------------------------------------------
  ! Generating a restart file
  !---------------------------------------------------------------------
  integer, protected            :: irestart       = 0
  character(len=100), parameter :: filerestart    = "restart_EQ.bin"
  !---------------------------------------------------------------------
  ! random seed
  !---------------------------------------------------------------------
  integer, protected            :: seed = 0
  !---------------------------------------------------------------------
  ! maximum time limit (in seconds)
  ! allocated computation time is
  !      time_limit - time restart
  ! if time_limit > 0
  !---------------------------------------------------------------------
  real(8)                       :: time_limit   = 0
  real(8)                       :: time_restart = 0
  !---------------------------------------------------------------------
  ! namelist for datas and files
  !---------------------------------------------------------------------
  namelist /nml_data_files/ istep,ireadin, &
       & iplot,irestart,restart_EQ,seed,   &
       & time_limit,time_restart,          &
       & file_egld,file_tec,file_vtk,      &
       & file_egld_prtcl,file_vtk_prtcl
  !********************************************************************!
  !********************************************************************!
  !********************************************************************!
  ! End of storing datas and files
  !********************************************************************!
  !********************************************************************!
  !********************************************************************!











  !********************************************************************!
  !********************************************************************!
  !********************************************************************!
  ! Some general physical properties and constants                                      
  !********************************************************************!
  !********************************************************************!
  !********************************************************************!
  !---------------------------------------------------------------------
  ! gravity
  !---------------------------------------------------------------------
  real(8), protected :: grav_x = 0
  real(8), protected :: grav_y = 0
  real(8), protected :: grav_z = 0
  !---------------------------------------------------------------------
  ! namelist for gravity
  !---------------------------------------------------------------------
  namelist /nml_gravity/ grav_x,grav_y,grav_z
  !---------------------------------------------------------------------
  ! reference properties for EOS
  !---------------------------------------------------------------------
  real(8), protected :: ref_pres=101325
  real(8), protected :: ref_tp=300
  !---------------------------------------------------------------------
  ! namelist for reference properties
  !---------------------------------------------------------------------
  namelist /nml_ref_props/ ref_pres,ref_tp
  !********************************************************************!
  !********************************************************************!
  !********************************************************************!
  ! End of some general physical properties and constants                                      
  !********************************************************************!
  !********************************************************************!
  !********************************************************************!




  !********************************************************************!
  !********************************************************************!
  !********************************************************************!
  ! Debug option     
  !********************************************************************!
  !********************************************************************!
  !********************************************************************!
  integer :: impp_nvstks=0
  integer :: impp_poisson=0,impp_poisson_ksp=0
  integer :: impp_energy=0
  logical :: deeptracking = .false.
  logical, protected :: locked_mode = .true.
  !---------------------------------------------------------------------
  ! namelist for debugging option
  !---------------------------------------------------------------------
  namelist /nml_debug/ impp_nvstks,impp_poisson,impp_poisson_ksp, &
       & impp_energy,deeptracking,locked_mode
  
  !********************************************************************!
  !********************************************************************!
  !********************************************************************!
  ! End Debug option     
  !********************************************************************!
  !********************************************************************!
  !********************************************************************!







  !********************************************************************!
  !********************************************************************!
  !********************************************************************!
  ! General variables for Stokes, Navier-Stokes or Euler equations     
  !********************************************************************!
  !********************************************************************!
  !********************************************************************!
  !---------------------------------------------------------------------
  ! Navier-Stokes/Stokes/Euler activation
  !---------------------------------------------------------------------
  logical, protected :: NS_activate     = .true.
  logical            :: NS_invicid      = .false.
  logical, protected :: NS_compressible = .false.
  !---------------------------------------------------------------------
  ! Navier-Stokes/Stokes/Euler solve
  !---------------------------------------------------------------------
  logical, protected :: NS_solve        = .true.
  !---------------------------------------------------------------------
  ! Space integration scheme for conservation equations
  !    <0 Explicit schemes
  !     0 Stokes Flow
  !     1 Centered
  !     2 Upwind 1st order
  !     3 Hybrid (flux based) scheme
  !---------------------------------------------------------------------
  integer, protected :: NS_inertial_scheme = 1
  !---------------------------------------------------------------------
  ! Momentum Conserving Formulation
  !     1 3th order Cubic Upwind
  !     2 WENO5 Conservativex
  !---------------------------------------------------------------------
  logical, protected :: NS_MomentumConserving          = .false.
  integer, protected :: NS_MomentumConserving_scheme   = 2
  integer, protected :: NS_MomentumConserving_velocity = 2
  !---------------------------------------------------------------------
  ! Augmented Lagrangian parameters
  !---------------------------------------------------------------------
  logical, protected :: AL_comp      = .false.
  real(8)            :: AL_dr        = 0
  real(8)            :: AL_threshold = 1d-14
  integer            :: AL_it        = 1
  !---------------------------------------------------------------------
  ! Navier-Stokes coupling
  !     0 --> AL, u,v,w and p ARE NOT coupled
  !     1 --> u,v,w and p ARE coupled
  !     2 --> Projection methods
  !---------------------------------------------------------------------
  integer, protected :: NS_method = 0
  !---------------------------------------------------------------------
  ! Space integration scheme for conservation equations
  !     1 --> constant Augmented Lagrangian (single phase flows)
  !     2 --> constant Augmented Lagrangian by phase (two-phase flows)
  !     3 --> adaptive Augmented Lagrangian (algebraic method)
  !---------------------------------------------------------------------
  integer, protected :: AL_method = 1
  !---------------------------------------------------------------------
  ! Solvers
  ! NS_solver_type = 1 BICGSTAB II + ILU prDpdteconditionning
  ! NS_solver_type = 0 MUMPS direct solver
  !---------------------------------------------------------------------
  integer, protected :: NS_solver_type      = 1
  integer, protected :: NS_solver_it        = 30     ! Number of iterations of the iterative solver
  integer, protected :: NS_solver_precond   = 1      ! 0:Jacobi;1:ILU
  real(8), protected :: NS_solver_threshold = 1d-5   ! Threshold for the residual of the iterative solver
  !---------------------------------------------------------------------
  ! Hypre multi-grid preconditioner for coupled resolution
  ! 0 : PFMG
  ! 1 : SMG
  !---------------------------------------------------------------------
  integer, protected :: NS_HYPRE_precond            = 0  ! 
  integer, protected :: NS_HYPRE_nb_pre_post_relax  = 2  ! both SMG and PFMG
  integer, protected :: NS_HYPRE_maxLvL             = 1  ! PFMG only, increase with cell number
  integer, protected :: NS_HYPRE_typeRelax          = 3  ! PFMG only (Stokes one fluid --> 2, 3 otherwise)
  integer, protected :: NS_HYPRE_skipRelax          = 1  ! PFMG only
  !---------------------------------------------------------------------
  ! Projection method parameters
  !    PJ_method =
  !                0 : nothing (augmented lagrangian)
  !                1 : Scalar Projection method
  !               -1 : same as 1 with 1 AL iteration in the prediction step
  !                2 : Kinetic Scalar Projection (KSP) method
  !               -2 : same as 2 with 1 AL iteration in the prediction step
  !                3 : Scalar Projection method with full explicip NL term
  !---------------------------------------------------------------------
  integer            :: PJ_method           = 0
  integer, protected :: PJ_solver_type      = 1
  integer, protected :: PJ_solver_it        = 50
  integer, protected :: PJ_solver_precond   = 1          ! 0:Jacobi;1:ILU
  real(8), protected :: PJ_solver_threshold = 1d-5
  !---------------------------------------------------------------------
  ! Hypre multi-grid preconditioner for Projection 
  ! 0 : PFMG
  ! 1 : SMG
  !---------------------------------------------------------------------
  integer, protected :: PJ_HYPRE_precond            = 0    ! 
  integer, protected :: PJ_HYPRE_nb_pre_post_relax  = 2    ! both SMG and PFMG
  integer, protected :: PJ_HYPRE_maxLvL             = 2    ! PFMG only, increase with cell number
  integer, protected :: PJ_HYPRE_typeRelax          = 3    ! PFMG only (Stokes one fluid --> 2, 3 otherwise)
  integer, protected :: PJ_HYPRE_skipRelax          = 1    ! PFMG only
  integer, protected :: PJ_HYPRE_MaxIt              = 1    ! number of precond iteration
  real(8), protected :: PJ_HYPRE_tol                = 1d-6 ! useless
  !---------------------------------------------------------------------
  ! Order Neuman pressure
  !    0 : in discretization (HYPRE)
  !    1 : 1st order penalized
  !    2 : 2nd order with connectivities (default)
  !---------------------------------------------------------------------
  integer, protected :: NS_Neumann_p    = 2
  integer, protected :: NS_pressure_fix = 1
  !---------------------------------------------------------------------
  ! Pressure penalty
  !---------------------------------------------------------------------
  logical, protected :: NS_pressure_pen = .false.
  !---------------------------------------------------------------------
  ! Linear term
  !---------------------------------------------------------------------
  logical, protected :: NS_linear_term  = .false.
  !---------------------------------------------------------------------
  ! Source term
  !---------------------------------------------------------------------
  logical, protected :: NS_source_term  = .false.
  !---------------------------------------------------------------------
  ! Penalty method
  ! NS_ipen =
  !         1 => 2D circle
  !         2 => 2D rotating circle
  !         else (=DEFAULT) => no penalty
  !---------------------------------------------------------------------
  integer, protected :: NS_ipen = 0
  !---------------------------------------------------------------------
  ! Time dependent BC
  !---------------------------------------------------------------------
  logical            :: NS_TimeBC=.false.
  !---------------------------------------------------------------------
  ! Pressure and velocity initialization and time evolution
  !---------------------------------------------------------------------
  integer, protected :: NS_init_vel_icase = 0
  logical, protected :: NS_init_vel       = .false.
  logical, protected :: NS_init_div       = .false.
  logical, protected :: NS_time_vel       = .false.
  integer, protected :: NS_init_pre_icase = 0
  logical, protected :: NS_init_pre       = .false.
  logical, protected :: NS_time_pre       = .false.
  !---------------------------------------------------------------------
  ! Penalty values
  !---------------------------------------------------------------------
  real(8), protected :: NS_pen_BC = 1d40
  real(8), protected :: NS_pen    = 1d15
  !---------------------------------------------------------------------
  ! new solver type
  !---------------------------------------------------------------------
  type(resol_nvstks_t) :: nvstks_solver
  !---------------------------------------------------------------------
  ! namelist for nvstks resolution
  !---------------------------------------------------------------------
  namelist /nml_nvstks_solver/ NS_activate,NS_solve,NS_compressible,                  &
       & NS_method,NS_inertial_scheme,AL_method,AL_dr,AL_comp,                        &
       & NS_init_vel_icase,NS_init_vel,NS_init_div,NS_time_vel,                       &
       & NS_init_pre_icase,NS_init_pre,NS_time_pre,                                   &
       & NS_linear_term,NS_source_term,NS_ipen,NS_pen_BC,NS_pen,NS_pressure_pen,      &
       & NS_solver_type,NS_solver_it,NS_solver_threshold,NS_solver_precond,           &
       & NS_HYPRE_precond,NS_HYPRE_nb_pre_post_relax,NS_HYPRE_maxLvL,                 &
       & NS_HYPRE_typeRelax,NS_HYPRE_skipRelax,                                       &
       & PJ_method,PJ_solver_type,PJ_solver_it,PJ_solver_threshold,PJ_solver_precond, &
       & PJ_HYPRE_precond,PJ_HYPRE_nb_pre_post_relax,PJ_HYPRE_maxLvL,                 &
       & PJ_HYPRE_typeRelax,PJ_HYPRE_skipRelax,PJ_HYPRE_MaxIt,PJ_HYPRE_tol,           &
       & NS_MomentumConserving,NS_MomentumConserving_scheme,                          &
       & NS_MomentumConserving_velocity,NS_Neumann_p,NS_pressure_fix,                 &
       & nvstks_solver

  !********************************************************************!
  !********************************************************************!
  !********************************************************************!
  ! End of general variables for Stokes, Navier-Stokes or Euler equations
  !********************************************************************!
  !********************************************************************!
  !********************************************************************!





  !********************************************************************!
  !********************************************************************!
  !********************************************************************!
  ! General variables for LES turbulence
  !   and for Scalar diffusion turbulence (SDT)
  !********************************************************************!
  !********************************************************************!
  !********************************************************************!
  !---------------------------------------------------------------------
  ! statistics
  !    - reset if <= 0 (default)
  !    - else continue
  !---------------------------------------------------------------------
  integer            :: cStats=0
  !---------------------------------------------------------------------
  ! LES activation
  !---------------------------------------------------------------------
  logical, protected :: LES_activate  = .false.
  logical, protected :: LES_selection = .false.
  logical, protected :: WF_activate   = .false.
  logical, protected :: SDT_activate  = .false.
  !---------------------------------------------------------------------
  ! LES scheme for turbulence
  !     0 Only statistics (DNS)
  !     1 Smagorinsky
  !     2 Turbulent Kinetic Energy (TKE)
  !     3 Mixed scale (alpha = 0.5)
  !     4 WALE
  !---------------------------------------------------------------------
  integer, protected :: LES_scheme      = 1
  real(8), protected :: LES_const_Smago = 0.18D0
  real(8), protected :: LES_const_TKE   = 0.2D0
  real(8), protected :: LES_const_WALE  = 0.75d0
  !---------------------------------------------------------------------
  ! SDT scheme for turbulence
  !     1 Default model
  !     2 Correlation from GOLDMAN
  !---------------------------------------------------------------------
  integer, protected :: SDT_scheme = 1
  !---------------------------------------------------------------------
  !---------------------------------------------------------------------
  ! namelist for LES
  !---------------------------------------------------------------------
  namelist /nml_les/ LES_activate,WF_activate,         &
       & LES_scheme,LES_selection,                     &
       & LES_const_Smago,LES_const_TKE,LES_const_WALE, &
       & SDT_activate,SDT_scheme,cStats
  !********************************************************************!
  !********************************************************************!
  !********************************************************************!
  ! End of general variables for LES turbulence
  !********************************************************************!
  !********************************************************************!
  !********************************************************************!









  !********************************************************************!
  !********************************************************************!
  !********************************************************************!
  ! General variables for Energy equation
  !********************************************************************!
  !********************************************************************!
  !********************************************************************!
  !---------------------------------------------------------------------
  ! Energy activation
  !---------------------------------------------------------------------
  logical, protected :: EN_activate = .false.
  !---------------------------------------------------------------------
  ! Energy solve
  !---------------------------------------------------------------------
  logical, protected :: EN_solve    = .true.
  !---------------------------------------------------------------------
  ! Space integration scheme for conservation equations
  !    <0 Explicit schemes
  !       -1 : WENO5 Non-Conservative
  !       -2 : WENO5 Conservative
  !       -3 : Lax-Wendroff TVD
  !       -2 : Explicit upwind
  !       -2 : Explicit centered
  !       -10: Lagrangian scheme
  !     0 No advection
  !     1 Centered
  !     2 Upwind
  !---------------------------------------------------------------------
  integer, protected :: EN_inertial_scheme = 1
  !---------------------------------------------------------------------
  ! Solvers
  ! NS_solver_type = 1 BICGSTAB II + ILU preconditionning
  ! NS_solver_type = 2 MUMPS direct solver
  !---------------------------------------------------------------------
  integer, protected :: EN_solver_type      = 1
  integer, protected :: EN_solver_it        = 30     ! Number of iterations of the iterative solver
  integer, protected :: EN_solver_precond   = 1      ! 0:Jacobi;1:ILU
  real(8), protected :: EN_solver_threshold = 1d-8   ! Threshold for the residual of the iterative solver
  !---------------------------------------------------------------------
  ! Linear term
  !---------------------------------------------------------------------
  logical, protected :: EN_linear_term = .false.
  !---------------------------------------------------------------------
  ! Source term
  !---------------------------------------------------------------------
  logical, protected :: EN_source_term = .false.
  !---------------------------------------------------------------------
  ! Penalty method
  !---------------------------------------------------------------------
  logical, protected :: EN_penalty_term = .false.
  integer, protected :: EN_ipen         = 0
  !---------------------------------------------------------------------
  ! Boussinesq
  !---------------------------------------------------------------------
  logical, protected :: EN_Boussinesq   = .false.
  !---------------------------------------------------------------------
  ! Initialization and time evolution
  !---------------------------------------------------------------------
  integer, protected :: EN_init_icase   = 0
  logical, protected :: EN_init         = .false.
  logical, protected :: EN_time_tp      = .false.
  !---------------------------------------------------------------------
  ! Penalty values
  !---------------------------------------------------------------------
  real(8), protected :: EN_pen_BC       = 1d40
  real(8), protected :: EN_pen          = 1d15
  !---------------------------------------------------------------------
  ! namelist for Energy equation
  !---------------------------------------------------------------------
  namelist /nml_energy_solver/ EN_activate,EN_inertial_scheme,   &
       & EN_solver_type,EN_solver_it,EN_solver_threshold,        &
       & EN_solver_precond,EN_init_icase,EN_init,                &
       & EN_time_tp,EN_source_term,EN_pen_BC,EN_pen,EN_ipen,     &
       & EN_source_term,EN_linear_term,EN_penalty_term,          &
       & EN_Boussinesq,EN_solve
  !********************************************************************!
  !********************************************************************!
  !********************************************************************!
  ! End of general variables for Energy equation
  !********************************************************************!
  !********************************************************************!
  !********************************************************************!




  
  
  !********************************************************************!
  !********************************************************************!
  !********************************************************************!
  ! General variables for ADE equation
  !********************************************************************!
  !********************************************************************!
  !********************************************************************!
  !---------------------------------------------------------------------
  type(equation_t)  :: ade_eq
  type(resol_ade_t) :: ade_resol
  !---------------------------------------------------------------------
  ! namelist for ADE equation
  !---------------------------------------------------------------------
  namelist /nml_ade_solver/ ade_eq,ade_resol
  !********************************************************************!
  !********************************************************************!
  !********************************************************************!
  ! End of general variables for Energy equation
  !********************************************************************!
  !********************************************************************!
  !********************************************************************!






  
  !********************************************************************!
  !********************************************************************!
  !********************************************************************!
  ! Two-phase flows
  !********************************************************************!
  !********************************************************************!
  !********************************************************************!
  ! Number of phases (2 for two-phase flows)
  integer, protected :: NB_phase = 1
  !---------------------------------------------------------------------
  !---------------------------------------------------------------------
  !---------------------------------------------------------------------
  ! Delta formulations
  !---------------------------------------------------------------------
  !---------------------------------------------------------------------
  !---------------------------------------------------------------------
  ! Surface tension source term only if NB_phase = 2
  !---------------------------------------------------------------------
  integer, protected :: NS_tension = 0
  !---------------------------------------------------------------------
  ! Volume Of Fluid (VOF)
  !---------------------------------------------------------------------
  ! VOF activation
  logical, protected :: VOF_activate    = .false.
  logical, protected :: VOF_solve       = .true.
  integer, protected :: VOF_method      = 1
  integer, protected :: VOF_ismooth     = 3
  real(8), protected :: VOF_vsmooth     = 0.2d0 ! [0.2:2]
  integer, protected :: VOF_mean_mu     = 1
  integer, protected :: VOF_mean_lambda = 2
  integer, protected :: VOF_init_icase  = 0
  logical, protected :: VOF_init        = .false.
  !---------------------------------------------------------------------
  type(init_case_t)  :: VOF_init_case
  !---------------------------------------------------------------------
  ! Manifold Death (MD)
  !---------------------------------------------------------------------
  logical, protected :: VOF_MD_activate = .false.
  integer, protected :: VOF_MD_istep    = 100
  integer, protected :: VOF_MD_NHole    = 10
  real(8), protected :: VOF_MD_length   = 1d-6
  real(8), protected :: VOF_MD_dt       = -0.05d0
  !---------------------------------------------------------------------
  real(8)            :: VOF_cfl         = 0.5d0
  !---------------------------------------------------------------------
  ! Level-Set
  !---------------------------------------------------------------------
  ! Level Set activation
  logical, parameter :: LS_activate = .false.
  !---------------------------------------------------------------------
  ! Phase-field (PF experimental)
  !---------------------------------------------------------------------
  ! Phase-field activation
  logical, protected :: PF_activate = .false.
  logical :: PF_interface_normal_velo = .false.
  logical :: PF_general_advection = .false.
  real(8) :: PF_bprime = 0.5
  real(8) :: PF_W_delta_ratio = 2
  real(8) :: PF_W = 0
  real(8) :: PF_cfl = 0.5
  type(init_case_t)  :: PF_init_case
  !---------------------------------------------------------------------
  !---------------------------------------------------------------------
  ! Front-Tracking
  !---------------------------------------------------------------------
  !---------------------------------------------------------------------
  !---------------------------------------------------------------------
  !---------------------------------------------------------------------
  ! Two-phase formulation (only with Front-Tracking)
  !---------------------------------------------------------------------
  !---------------------------------------------------------------------
  !---------------------------------------------------------------------
  ! Front-Tracking
  !---------------------------------------------------------------------
  ! time integration scheme
  integer, parameter :: FT_RK = 2
  !---------------------------------------------------------------------
  ! Front Tracking activation
  logical, parameter :: FT_activate = .false.
  logical, parameter :: FT_Delta    = .false.
  logical, parameter :: FT_2_phases = .false.
  logical, parameter :: FT_write    = .false.
  !---------------------------------------------------------------------
  ! FT_interp = 1 bilinear
  ! FT_interp = 2 PERM
  ! FT_interp = 3 Peskin
  ! FT_interp = 4 jump relations
  !---------------------------------------------------------------------
  integer, parameter :: FT_interp = 4
!   !---------------------------------------------------------------------
!   ! RESPECT
!   logical, parameter :: RESPECT_activate = .false.
  !---------------------------------------------------------------------
  ! namelist for interface tracking
  !---------------------------------------------------------------------
  namelist /nml_interface_tracking/ NB_phase,NS_tension,VOF_ismooth, &
       & VOF_vsmooth,VOF_activate,VOF_method,VOF_init_icase,         &
       & VOF_init,VOF_mean_mu,                                       &
       & VOF_MD_activate,VOF_MD_istep,VOF_MD_NHole,VOF_MD_dt,        &
       & VOF_MD_length,VOF_init_case,VOF_mean_lambda,VOF_solve,      &
       & PF_activate,PF_interface_normal_velo,PF_general_advection,  &
       & PF_init_case!,RESPECT_activate
  !********************************************************************!
  !********************************************************************!
  !********************************************************************!
  ! End of two-phase flows
  !********************************************************************!
  !********************************************************************!
  !********************************************************************!












  !********************************************************************!
  !********************************************************************!
  !********************************************************************!
  ! Lagrangian particle tracking and Lagrangian Advection
  !********************************************************************!
  !********************************************************************!
  !********************************************************************!
  !---------------------------------------------------------------------
  ! Time discretization schemes for Lagrangian integrations
  !---------------------------------------------------------------------
  integer, protected :: LG_RK        = 2
  !---------------------------------------------------------------------
  integer, parameter :: restart_Lag  = 0
  integer, protected :: nPart        = 0
  integer            :: nPartS       = 0
  integer            :: nPartV       = 0
  integer            :: nPartC       = 0
  integer            :: nppdpc       = 2 ! number of particle per direction and per cell
  integer            :: nppdpc_min   = -1 ! min number of nppdpc if >= 0
  integer, protected :: iRealloc     = 1 ! iter reallocation particles
  real(8), protected :: dt_LG        = 0
  real(8), protected :: CFL_LG       = 0.5d0
  logical, protected :: LG_activate  = .false.
  logical, protected :: LG_advection = .false.
  !---------------------------------------------------------------------
  ! gravity (can be different from Eulerian values)
  !---------------------------------------------------------------------
  real(8), dimension(3) :: gravity = 0
  !---------------------------------------------------------------------
  ! Applied forces 
  !---------------------------------------------------------------------
  logical, protected :: LG_drag = .false.
  logical, protected :: LG_lift = .false.
  logical, protected :: LG_grav = .false.
  logical, protected :: LG_arch = .false.
  logical, protected :: LG_bass = .false.
  logical, protected :: LG_addm = .false.
  !---------------------------------------------------------------------
  logical            :: PartAreMarkers,PhiCutOff=.false.
  !---------------------------------------------------------------------
  ! Two Way Coupling
  !---------------------------------------------------------------------
  logical, protected :: LG_2Way                   = .false.
  !---------------------------------------------------------------------
  real(8), parameter :: cfl_lag                   = 0.5d0
  integer, parameter :: order_extra_max           = 2
  integer, parameter :: order_time_derivative_max = order_extra_max-1
  !---------------------------------------------------------------------
  ! namelist for Lagrangian particle traking or advection
  !---------------------------------------------------------------------
  namelist /nml_lagrangian_particle/ nPart,nppdpc,nppdpc_min, &
       & iRealloc,LG_activate,LG_advection,LG_2Way,LG_RK,     &
       & LG_drag,LG_lift,LG_grav,LG_arch,LG_bass,LG_addm,     &
       & PhiCutOff
  !********************************************************************!
  !********************************************************************!
  !********************************************************************!
  ! End of Lagrangian particle tracking                                     
  !********************************************************************!
  !********************************************************************!
  !********************************************************************!


  !********************************************************************!
  !********************************************************************!
  !********************************************************************!
  ! STL
  !********************************************************************!
  !********************************************************************!
  !********************************************************************!
  !---------------------------------------------------------------------
  type(stl_file_t) :: STL_infile
  !---------------------------------------------------------------------
  ! namelist for STL
  !---------------------------------------------------------------------
  namelist /nml_stl/ STL_infile
  !********************************************************************!
  !********************************************************************!
  !********************************************************************!
  ! End STL
  !********************************************************************!
  !********************************************************************!
  !********************************************************************!







  !********************************************************************!
  !********************************************************************!
  !********************************************************************!
  ! Probes
  !********************************************************************!
  !********************************************************************!
  !********************************************************************!
  !---------------------------------------------------------------------
  ! probing iteration if > 0
  !---------------------------------------------------------------------
  integer, protected          :: iprobe=-1
  !---------------------------------------------------------------------
  ! number of sca probes
  !---------------------------------------------------------------------
  integer, protected          :: nProbe_sca=0 
  real(8), dimension(10000,3) :: xyzProbe_sca=0
  !---------------------------------------------------------------------
  ! number of velocity probes
  !---------------------------------------------------------------------
  integer, protected          :: nProbe_uvw=0 
  real(8), dimension(10000,3) :: xyzProbe_uvw=0
  !---------------------------------------------------------------------
  !---------------------------------------------------------------------
  ! number of pressure probes
  !---------------------------------------------------------------------
  integer, protected          :: nProbe_p=0 
  real(8), dimension(10000,3) :: xyzProbe_p=0
  !---------------------------------------------------------------------
  ! namelist 
  !---------------------------------------------------------------------
  namelist /nml_probes/ iprobe,   &
       & nProbe_sca,xyzProbe_sca, &
       & nProbe_uvw,xyzProbe_uvw, &
       & nProbe_p,xyzProbe_p
  !---------------------------------------------------------------------
  !********************************************************************!
  !********************************************************************!
  !********************************************************************!
  ! End of Lagrangian particle tracking                                     
  !********************************************************************!
  !********************************************************************!
  !********************************************************************!





  !********************************************************************!
  !********************************************************************!
  !********************************************************************!
  ! Various usefull variables - DO NOT TOUCH
  !********************************************************************!
  !********************************************************************!
  !********************************************************************!
  real(8), parameter :: pi  = acos(-1._8)
  real(8), parameter :: pi2 = pi*pi
  real(8), parameter :: dpi = 1/pi
  !********************************************************************!
  !********************************************************************!
  !********************************************************************!
  ! End of various usefull variables
  !********************************************************************!
  !********************************************************************!



contains

  subroutine read_parameters
    !--------------------------------------------------------------
    ! Modules
    !--------------------------------------------------------------
    implicit none
    !--------------------------------------------------------------
    ! Global variables
    !--------------------------------------------------------------
    !--------------------------------------------------------------
    ! Local variables
    !--------------------------------------------------------------
    character(len=8)                 :: date
    character(len=10)                :: time
    character(len=100)               :: machine
    !------------------------------------------------------------------


    !*******************************************************************************!  
    ! Reading inputs
    !*******************************************************************************!  
    open(unit=10,file="data.in",status="old",action="read")
    read(10,nml=nml_mpi)               ; rewind(10)
    read(10,nml=nml_space_integration) ; rewind(10)
    !------------------------------------------------------------------
    ! default case --> regular grids / 1 block 
    !------------------------------------------------------------------
    grid(1)%nblck=1
    grid(2)%nblck=1
    grid(3)%nblck=1
    !------------------------------------------------------------------
    grid(1)%blck(1)%nc=nx
    grid(2)%blck(1)%nc=ny
    grid(3)%blck(1)%nc=nz
    !------------------------------------------------------------------
    grid(1)%blck(1)%type=1
    grid(2)%blck(1)%type=1
    grid(3)%blck(1)%type=1
    !------------------------------------------------------------------
    grid(1)%blck(1)%min=xmin;grid(1)%blck(1)%max=xmax
    grid(2)%blck(1)%min=ymin;grid(2)%blck(1)%max=ymax
    grid(3)%blck(1)%min=zmin;grid(3)%blck(1)%max=zmax
    !------------------------------------------------------------------
    read(10,nml=nml_grid)                 ; rewind(10)
    read(10,nml=nml_time_integration)     ; rewind(10)
    read(10,nml=nml_data_files)           ; rewind(10)
    read(10,nml=nml_debug)                ; rewind(10)
    read(10,nml=nml_gravity)              ; rewind(10)
    read(10,nml=nml_nvstks_solver)        ; rewind(10)
    read(10,nml=nml_les)                  ; rewind(10)
    read(10,nml=nml_energy_solver)        ; rewind(10)
    read(10,nml=nml_ade_solver)           ; rewind(10)
    read(10,nml=nml_interface_tracking)   ; rewind(10)
    read(10,nml=nml_lagrangian_particle)  ; rewind(10)
    read(10,nml=nml_stl)                  ; rewind(10)
    read(10,nml=nml_probes)               ; rewind(10)
    close(10)
    !*******************************************************************************!  
    ! to be continued after allocation for fluid properties
    !*******************************************************************************!  

    !-----------------------------------------------------
    ! 2D natural switch
    !-----------------------------------------------------
    switch2D3D = dim-2
    if (dim==2) then
       if (abs(grav_z)>0 .and. grav_y==0) grav_y = grav_z
    end if
    !-----------------------------------------------------
    
    !-----------------------------------------------------
    ! MPI size exchange if VOF-Plic
    !-----------------------------------------------------
    if (VOF_activate) then

       slx = max(2,slx)
       sly = max(2,sly)
       slz = max(2**(dim-2),slz)

       if (NS_tension == 3) then ! Height functions
          slx = 3
          sly = 3
          slz = 3**(dim-2)
       end if

    end if
    !-----------------------------------------------------

    !-----------------------------------------------------
    ! MPI size exchange if high order transport schemes
    !-----------------------------------------------------
    if (EN_activate) then
       select case(EN_inertial_scheme)
       case(4,5)
       slx=max(2,slx)
       sly=max(2,sly)
       slz=max(2**(dim-2),slz)
       case(-1,-2,-3)
          slx=3
          sly=3
          slz=3**(dim-2)
       end select
    end if
    !-----------------------------------------------------
    
    !--------------------------------------------------------------
    !  MPI size exchange if Momentum Conserving
    !  and active NS_source_term
    !--------------------------------------------------------------
    if (NS_MomentumConserving) then
       !--------------------------------------------------------------
       slx=3
       sly=3
       slz=3**(dim-2)
       !--------------------------------------------------------------
       NS_source_term=.true.
       !--------------------------------------------------------------
       ! MC for rho is useless for monophasic flows
       !--------------------------------------------------------------
       if (NB_phase==1) then
          if (NS_MomentumConserving_scheme>0) then
             if (rank==0) then
                write(*,*) "WARNING, MomentumConserving scheme is changed to rho*u transport version"
                write(*,*) "WARNING, MomentumConserving scheme is changed to rho*u transport version"
                write(*,*) "WARNING, MomentumConserving scheme is changed to rho*u transport version"
             end if
             NS_MomentumConserving_scheme=-NS_MomentumConserving_scheme
          end if
       end if
       !--------------------------------------------------------------
    end if
    !--------------------------------------------------------------

    !-----------------------------------------------------
    ! MPI extra solved cells 
    !-----------------------------------------------------
    gxs=1
    gys=1
    gzs=0
    if (dim==3) gzs=1
    
    if (EN_activate) then
       select case(EN_inertial_scheme)
       case(4,5)
          gxs=2
          gys=2
          if (dim==3) gzs=2
       end select
    end if
    !-----------------------------------------------------

    !-----------------------------------------------------
    ! Neumann pressure switch
    !-----------------------------------------------------
    if (locked_mode) then
       select case(NS_method)
       case(1)
          NS_Neumann_p=0
       case(2)
          if (PJ_solver_type==2) NS_Neumann_p=0
       end select
    end if
    !-----------------------------------------------------
    
    !-----------------------------------------------------
    ! STL 
    !-----------------------------------------------------
    if (STL_infile%nObj>0) then
       NS_linear_term = .true.
    end if
    !-----------------------------------------------------
    
    
  end subroutine read_parameters



  subroutine init_parameters
    !--------------------------------------------------------------
    ! Modules
    !--------------------------------------------------------------
    implicit none
    !--------------------------------------------------------------
    ! Global variables
    !--------------------------------------------------------------
    !--------------------------------------------------------------
    ! Local variables
    !--------------------------------------------------------------
    integer                            :: inewton
    real(8)                            :: dxl,dyl,dzl
    real(8)                            :: h,coef_exp,inc,f,df
    character(len=8)                   :: date
    character(len=10)                  :: time
    character(len=100)                 :: machine
    character(len=14)                  :: name
    logical                            :: file_exists,direct
    !--------------------------------------------------------------
    ! random numbers 
    !------------------------------------------------------------------
    integer                            :: i,j,k,m,n
    integer                            :: s,clock
    integer, dimension(:), allocatable :: p
    !------------------------------------------------------------------


    !--------------------------------------------------------------
    ! protected parameters init
    !--------------------------------------------------------------
    if (nz<2.and.dim==3) dim=2
    if (dim==2) nz=1
    !--------------------------------------------------------------
    xyzBounds(1,:)=(/xmin,xmax/)
    xyzBounds(2,:)=(/ymin,ymax/)
    xyzBounds(3,:)=(/zmin,zmax/)
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! scalars 
    !--------------------------------------------------------------
    gsx=0; gex=nx
    gsy=0; gey=ny
!    if (periodic(1).and.nproc>1) gex=nx-1
!    if (periodic(2).and.nproc>1) gey=ny-1
    if (dim==2) then
       gsz=1; gez=1
       gz=0
    else
       gsz=0; gez=nz
!       if (periodic(3).and.nproc>1) gez=nz-1
    end if
    !--------------------------------------------------------------
    ! u-velocity
    !--------------------------------------------------------------
    gsxu=0; gexu=gex+1
    gsyu=0; geyu=gey
    if (dim==2) then
       gszu=1; gezu=1
    else
       gszu=0; gezu=gez
    end if
    !--------------------------------------------------------------
    ! v-velocity
    !--------------------------------------------------------------
    gsxv=0; gexv=gex
    gsyv=0; geyv=gey+1
    if (dim==2) then
       gszv=1; gezv=1
    else
       gszv=0; gezv=gez
    end if
    !--------------------------------------------------------------
    ! w-velocity
    !--------------------------------------------------------------
    gsxw=0; gexw=gex
    gsyw=0; geyw=gey
    if (dim==2) then
       gszw=1; gezw=1
    else
       gszw=0; gezw=gez+1
    end if
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! Numbers of unknowns
    !--------------------------------------------------------------
    if (dim==2) then 
       nb_Vx=(exus-sxus+1)*(eyus-syus+1)
       nb_Vy=(exvs-sxvs+1)*(eyvs-syvs+1)
       nb_Vz=0
       nb_P=(exs-sxs+1)*(eys-sys+1)
       nb_T=nb_P
       
       nb_Vx_no_ghost=(exu-sxu+1)*(eyu-syu+1)*(ezu-szu+1)
       nb_Vy_no_ghost=(exv-sxv+1)*(eyv-syv+1)*(ezv-szv+1)
       nb_p_no_ghost=(ex-sx+1)*(ey-sy+1)*(ez-sz+1)
    else
       nb_Vx=(exus-sxus+1)*(eyus-syus+1)*(ezus-szus+1)
       nb_Vy=(exvs-sxvs+1)*(eyvs-syvs+1)*(ezvs-szvs+1)
       nb_Vz=(exws-sxws+1)*(eyws-syws+1)*(ezws-szws+1)
       nb_P=(exs-sxs+1)*(eys-sys+1)*(ezs-szs+1)
       nb_T=nb_P
       
       nb_Vx_no_ghost=(exu-sxu+1)*(eyu-syu+1)*(ezu-szu+1)
       nb_Vy_no_ghost=(exv-sxv+1)*(eyv-syv+1)*(ezv-szv+1)
       nb_Vz_no_ghost=(exw-sxw+1)*(eyw-syw+1)*(ezw-szw+1)
       nb_p_no_ghost=(ex-sx+1)*(ey-sy+1)*(ez-sz+1)
    end if
    nb_V=nb_Vx+nb_Vy+nb_Vz
    nb_VP=nb_V+nb_P
    !--------------------------------------------------------------

    select case(NS_method)
    case(0)
       !--------------------------------------------------------------
       ! Augmented lagrangian
       !--------------------------------------------------------------
       PJ_method=0
       !--------------------------------------------------------------
       if (AL_dr==0) then
          if (rank==0) then
             write(*,*) "WARNING, AL_dr=0 in AL method"
             write(*,*) "WARNING, AL_dr=0 in AL method"
             write(*,*) "WARNING, AL_dr=0 in AL method"
             write(*,*) "WARNING, AL_dr=0 in AL method"
             write(*,*) "WARNING, AL_dr=0 in AL method"
          end if
       end if
    case(1)
       !--------------------------------------------------------------
       ! Fully coupled case
       !--------------------------------------------------------------
       if (AL_dr/=0) then
          if (rank==0) then
             write(*,*) "WARNING, AL_dr/=0 in FC method"
             write(*,*) "WARNING, AL_dr/=0 in FC method"
             write(*,*) "WARNING, AL_dr/=0 in FC method"
             write(*,*) "WARNING, AL_dr/=0 in FC method"
             write(*,*) "WARNING, AL_dr/=0 in FC method"
          end if
       end if
       !--------------------------------------------------------------
       PJ_method=0
       !--------------------------------------------------------------
       NS_solver_type=3
       !--------------------------------------------------------------
    case(2)
       !--------------------------------------------------------------
       ! Projection methods
       !--------------------------------------------------------------
       if (PJ_method==0) PJ_method=1
       if (PJ_method==3) NS_inertial_scheme=0
       !--------------------------------------------------------------
       if (PJ_method>0)  AL_dr=0
       !--------------------------------------------------------------
    end select
    
    !--------------------------------------------------------------
    ! Momentum Conserving
    !--------------------------------------------------------------
    if (NS_MomentumConserving) NS_inertial_scheme=0
    !--------------------------------------------------------------
    
    !--------------------------------------------------------------
    ! grid and space steps allocation
    !--------------------------------------------------------------
    ! dx
    allocate(dx(sx-gx:ex+gx))
    allocate(dxu(sxu-gx:exu+gx))
    allocate(dy(sy-gy:ey+gy))
    allocate(dyv(syv-gy:eyv+gy))
    allocate(dz(sz-gz:ez+gz))
    allocate(dzw(szw-gz:ezw+gz))
    ! dx/2
    allocate(dx2(sx-gx:ex+gx))
    allocate(dxu2(sxu-gx:exu+gx))
    allocate(dy2(sy-gy:ey+gy))
    allocate(dyv2(syv-gy:eyv+gy))
    allocate(dz2(sz-gz:ez+gz))
    allocate(dzw2(szw-gz:ezw+gz))
    ! 1/dx
    allocate(ddx(sx-gx:ex+gx))
    allocate(ddxu(sxu-gx:exu+gx))
    allocate(ddy(sy-gy:ey+gy))
    allocate(ddyv(syv-gy:eyv+gy))
    allocate(ddz(sz-gz:ez+gz))
    allocate(ddzw(szw-gz:ezw+gz))
    ! grids
    ! global 
    allocate(grid_x(gsx-gx:gex+gx))
    allocate(grid_y(gsy-gy:gey+gy))
    allocate(grid_z(gsz-gz:gez+gz))
    allocate(grid_xu(gsxu-gx:gexu+gx))
    allocate(grid_yv(gsyv-gy:geyv+gy))
    allocate(grid_zw(gszw-gz:gezw+gz))
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! test regular mesh
    !--------------------------------------------------------------
    test_regular_mesh1: do i=1,dim 
       do n=1,grid(i)%nblck
          if (grid(i)%blck(n)%type/=1) then
             regular_mesh=.false.
             exit test_regular_mesh1
          end if
       end do
    end do test_regular_mesh1
    
    if (regular_mesh) then 
       test_regular_mesh2: do i=1,dim
          if (grid(i)%nblck>1) then
             do n=2,grid(i)%nblck
                if ( (grid(i)%blck(n-1)%max-grid(i)%blck(n-1)%min)/grid(i)%blck(n-1)%nc /= &
                     & (grid(i)%blck(n)%max-grid(i)%blck(n  )%min)/grid(i)%blck(n  )%nc ) then
                   regular_mesh=.false.
                   exit test_regular_mesh2
                end if
             end do
          end if
       end do test_regular_mesh2
    end if
    !--------------------------------------------------------------
    
    !--------------------------------------------------------------
    ! grid generation 
    !--------------------------------------------------------------
    ! Scalar variables 
    !--------------------------------------------------------------
    ! X-direction
    !--------------------------------------------------------------

    if (grid(1)%nblck>1) then
       if (nx/=sum(grid(1)%blck(1:grid(1)%nblck)%nc)) then
          stop "nx not equal to the sum of blocks cells"
       else 
          grid(1)%blck(1)%shift=0
          do n=2,grid(1)%nblck
             grid(1)%blck(n)%shift=grid(1)%blck(n-1)%shift+grid(1)%blck(n-1)%nc
          end do
       end if
    end if
    
    do n=1,grid(1)%nblck
       select case(grid(1)%blck(n)%type)
       case(1)
          !--------------------------------------------------------------
          ! regular scalar grid
          !--------------------------------------------------------------
          do m=0,grid(1)%blck(n)%nc
             i=grid(1)%blck(n)%shift+m
             grid_x(i)=grid(1)%blck(n)%min &
                  & + m*((grid(1)%blck(n)%max-grid(1)%blck(n)%min)/grid(1)%blck(n)%nc)
          end do
          !--------------------------------------------------------------
       case(2)
          !--------------------------------------------------------------
          ! gauss lobatto points in [-1:1] --> [xmin:xmax]
          !--------------------------------------------------------------
          do m=0,grid(1)%blck(n)%nc
             i=grid(1)%blck(n)%shift+m
             grid_x(i)=-cos(pi*m/grid(1)%blck(n)%nc)
             grid_x(i)=grid(1)%blck(n)%min &
                  & + (grid_x(i)+1)/2*(grid(1)%blck(n)%max-grid(1)%blck(n)%min)
          end do
          !--------------------------------------------------------------
       case(3)
          !--------------------------------------------------------------
          ! expo
          !--------------------------------------------------------------

          !--------------------------------------------------------------
          ! auto dmin
          !--------------------------------------------------------------
          if (grid(1)%blck(n)%dmin==0) then
             if (grid(1)%blck(n)%connect==0) then
                if (n>1) then 
                   grid(1)%blck(n)%dmin= &
                        & (grid(1)%blck(n-1)%max-grid(1)%blck(n-1)%min)/grid(1)%blck(n-1)%nc
                else
                   write(*,*) "dir x"
                   write(*,*) "block",n-1," does not exist"
                   stop 
                end if
             else
                if (n<grid(1)%nblck) then 
                   grid(1)%blck(n)%dmin= &
                        & (grid(1)%blck(n+1)%max-grid(1)%blck(n+1)%min)/grid(1)%blck(n+1)%nc
                else
                   write(*,*) "dir x"
                   write(*,*) "block",n+1," does not exist"
                   stop 
                end if
             end if
          end if
          !--------------------------------------------------------------
          
          !--------------------------------------------------------------
          ! regular grid space size
          !--------------------------------------------------------------
          h=(grid(1)%blck(n)%max-grid(1)%blck(n)%min)/grid(1)%blck(n)%nc
          !--------------------------------------------------------------
          if ( grid(1)%blck(n)%dmin < h ) then
             coef_exp=1
             direct=.true.
          elseif ( grid(1)%blck(n)%dmin > h ) then
             coef_exp=-1
             direct=.false.
          else
             write(*,*) "grid_x"
             write(*,*) "block",n
             stop "chose a regular grid for this block"
          end if
          !--------------------------------------------------------------

          !--------------------------------------------------------------
          ! newton to find coef_exp such as dx_min = dmin
          !--------------------------------------------------------------
          inewton=0
          inc=1
          do while (abs(inc)>1d-12.and.inewton<200)
             inewton=inewton+1
             call compute_expo_f_df(      &
                  & grid(1)%blck(n)%min,  &
                  & grid(1)%blck(n)%max,  &
                  & coef_exp,             &
                  & h,                    &
                  & grid(1)%blck(n)%dmin, &
                  & f,                    &
                  & df)

             inc=-f/df
             coef_exp=coef_exp+inc
          end do
          !--------------------------------------------------------------

          !--------------------------------------------------------------
          ! nodes
          !--------------------------------------------------------------
          do m=0,grid(1)%blck(n)%nc
             i=grid(1)%blck(n)%shift+m

             call compute_expo_point(        &
                  & grid(1)%blck(n)%min,     &
                  & grid(1)%blck(n)%max,     &
                  & coef_exp,                &
                  & grid(1)%blck(n)%min+m*h, &
                  & f)

             ! sort nodes
             if (grid(1)%blck(n)%connect==0) then
                i=grid(1)%blck(n)%shift+m
                if (.not.direct) then
                   f=grid(1)%blck(n)%min-(f-grid(1)%blck(n)%min)+(grid(1)%blck(n)%max-grid(1)%blck(n)%min)
                end if
             else
                i=grid(1)%blck(n)%shift+grid(1)%blck(n)%nc-m
                if (direct) then
                   f=grid(1)%blck(n)%min-(f-grid(1)%blck(n)%min)+(grid(1)%blck(n)%max-grid(1)%blck(n)%min)
                end if
             end if
             grid_x(i)=f
          end do
          !--------------------------------------------------------------
          
       end select

    end do
    
    !--------------------------------------------------------------
    ! outside nodes
    !--------------------------------------------------------------
    do i=1,gx
       grid_x(gsx-i)=xmin-(grid_x(gsx+i)-xmin)
       grid_x(gex+i)=xmax+(xmax-grid_x(gex-i))
    end do
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! check
    !--------------------------------------------------------------
    if (rank==0) then
       do i=gsx-gx,gex+gx
          write(31,*) i,grid_x(i)
       end do
       write(31,*)
       write(31,*)
       flush(31)
    end if
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! Y-direction
    !--------------------------------------------------------------

    if (grid(2)%nblck>1) then
       if (ny/=sum(grid(2)%blck(1:grid(2)%nblck)%nc)) then
          stop "ny not equal to the sum of blocks cells"
       else 
          grid(2)%blck(1)%shift=0
          do n=2,grid(2)%nblck
             grid(2)%blck(n)%shift=grid(2)%blck(n-1)%shift+grid(2)%blck(n-1)%nc
          end do
       end if
    end if
    
    do n=1,grid(2)%nblck
       select case(grid(2)%blck(n)%type)
       case(1)
          !--------------------------------------------------------------
          ! regular scalar grid
          !--------------------------------------------------------------
          do m=0,grid(2)%blck(n)%nc
             i=grid(2)%blck(n)%shift+m
             grid_y(i)=grid(2)%blck(n)%min &
                  & + m*((grid(2)%blck(n)%max-grid(2)%blck(n)%min)/grid(2)%blck(n)%nc)
          end do
          !--------------------------------------------------------------
       case(2)
          !--------------------------------------------------------------
          ! gauss lobatto points in [-1:1] --> [xmin:xmax]
          !--------------------------------------------------------------
          do m=0,grid(2)%blck(n)%nc
             i=grid(2)%blck(n)%shift+m
             grid_y(i)=-cos(pi*m/grid(2)%blck(n)%nc)
             grid_y(i)=grid(2)%blck(n)%min &
                  & + (grid_y(i)+1)/2*(grid(2)%blck(n)%max-grid(2)%blck(n)%min)
          end do
          !--------------------------------------------------------------
       case(3)
          !--------------------------------------------------------------
          ! expo
          !--------------------------------------------------------------
          
          !--------------------------------------------------------------
          ! auto dmin
          !--------------------------------------------------------------
          if (grid(2)%blck(n)%dmin==0) then
             if (grid(2)%blck(n)%connect==0) then
                if (n>1) then 
                   grid(2)%blck(n)%dmin= &
                        & (grid(2)%blck(n-1)%max-grid(2)%blck(n-1)%min)/grid(2)%blck(n-1)%nc
                else
                   write(*,*) "dir y"
                   write(*,*) "block",n-1," does not exist"
                   stop 
                end if
             else
                if (n<grid(2)%nblck) then
                   grid(2)%blck(n)%dmin= &
                        & (grid(2)%blck(n+1)%max-grid(2)%blck(n+1)%min)/grid(2)%blck(n+1)%nc
                else
                   write(*,*) "dir y"
                   write(*,*) "block",n+1," does not exist"
                   stop 
                end if
             end if
          end if
          !--------------------------------------------------------------

          !--------------------------------------------------------------
          ! regular grid space size
          !--------------------------------------------------------------
          h=(grid(2)%blck(n)%max-grid(2)%blck(n)%min)/grid(2)%blck(n)%nc
          !--------------------------------------------------------------
          if ( grid(2)%blck(n)%dmin < h ) then
             coef_exp=1
             direct=.true.
          elseif ( grid(2)%blck(n)%dmin > h ) then
             coef_exp=-1
             direct=.false.
          else
             write(*,*) "grid_y"
             write(*,*) "block",n
             stop "chose a regular grid for this block"
          end if
          !--------------------------------------------------------------

          !--------------------------------------------------------------
          ! newton to find coef_exp such as dx_min = dmin
          !--------------------------------------------------------------
          inewton=0
          inc=1
          do while (abs(inc)>1d-12.and.inewton<200)
             inewton=inewton+1
             call compute_expo_f_df(      &
                  & grid(2)%blck(n)%min,  &
                  & grid(2)%blck(n)%max,  &
                  & coef_exp,             &
                  & h,                    &
                  & grid(2)%blck(n)%dmin, &
                  & f,                    &
                  & df)
             
             inc=-f/df
             coef_exp=coef_exp+inc
!!$             write(*,*) n,coef_exp,inc
          end do
          !--------------------------------------------------------------

          
          !--------------------------------------------------------------
          ! nodes
          !--------------------------------------------------------------
          do m=0,grid(2)%blck(n)%nc
             i=grid(2)%blck(n)%shift+m

             call compute_expo_point(        &
                  & grid(2)%blck(n)%min,     &
                  & grid(2)%blck(n)%max,     &
                  & coef_exp,                &
                  & grid(2)%blck(n)%min+m*h, &
                  & f)

             ! sort nodes
             if (grid(2)%blck(n)%connect==0) then
                i=grid(2)%blck(n)%shift+m
                if (.not.direct) then
                   f=grid(2)%blck(n)%min-(f-grid(2)%blck(n)%min)+(grid(2)%blck(n)%max-grid(2)%blck(n)%min)
                end if
             else
                i=grid(2)%blck(n)%shift+grid(2)%blck(n)%nc-m
                if (direct) then
                   f=grid(2)%blck(n)%min-(f-grid(2)%blck(n)%min)+(grid(2)%blck(n)%max-grid(2)%blck(n)%min)
                end if
             end if
             grid_y(i)=f
          end do
          !--------------------------------------------------------------
          
       end select

    end do
    
    !--------------------------------------------------------------
    ! outside nodes
    !--------------------------------------------------------------
    do i=1,gy
       grid_y(gsy-i)=ymin-(grid_y(gsy+i)-ymin)
       grid_y(gey+i)=ymax+(ymax-grid_y(gey-i))
    end do
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! check
    !--------------------------------------------------------------
    if (rank==0) then
       do i=gsy-gy,gey+gy
          write(31,*) i,grid_y(i)
       end do
       write(31,*)
       write(31,*)
       flush(31)
    end if
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! Z-direction 
    !--------------------------------------------------------------
    if (dim==2) then
       grid_z=0
    else
       if (grid(3)%nblck>1) then
          if (nz/=sum(grid(3)%blck(1:grid(3)%nblck)%nc)) then
             stop "nz not equal to the sum of blocks cells"
          else 
             grid(3)%blck(1)%shift=0
             do n=2,grid(3)%nblck
                grid(3)%blck(n)%shift=grid(3)%blck(n-1)%shift+grid(3)%blck(n-1)%nc
             end do
          end if
       end if

       do n=1,grid(3)%nblck
          select case(grid(3)%blck(n)%type)
          case(1)
             !--------------------------------------------------------------
             ! regular scalar grid
             !--------------------------------------------------------------
             do m=0,grid(3)%blck(n)%nc
                i=grid(3)%blck(n)%shift+m
                grid_z(i)=grid(3)%blck(n)%min &
                     & + m*((grid(3)%blck(n)%max-grid(3)%blck(n)%min)/grid(3)%blck(n)%nc)
             end do
             !--------------------------------------------------------------
          case(2)
             !--------------------------------------------------------------
             ! gauss lobatto points in [-1:1] --> [xmin:xmax]
             !--------------------------------------------------------------
             do m=0,grid(3)%blck(n)%nc
                i=grid(3)%blck(n)%shift+m
                grid_z(i)=-cos(pi*m/grid(3)%blck(n)%nc)
                grid_z(i)=grid(3)%blck(n)%min &
                     & + (grid_z(i)+1)/2*(grid(3)%blck(n)%max-grid(3)%blck(n)%min)
             end do
             !--------------------------------------------------------------
          case(3)
             !--------------------------------------------------------------
             ! expo
             !--------------------------------------------------------------
             
             !--------------------------------------------------------------
             ! auto dmin
             !--------------------------------------------------------------
             if (grid(3)%blck(n)%dmin==0) then
                if (grid(3)%blck(n)%connect==0) then
                   if (n>1) then 
                      grid(3)%blck(n)%dmin= &
                           & (grid(3)%blck(n-1)%max-grid(3)%blck(n-1)%min)/grid(3)%blck(n-1)%nc
                   else
                      write(*,*) "dir z"
                      write(*,*) "block",n-1," does not exist"
                      stop 
                   end if
                else
                   if (n<grid(3)%nblck) then 
                      grid(3)%blck(n)%dmin= &
                           & (grid(3)%blck(n+1)%max-grid(3)%blck(n+1)%min)/grid(3)%blck(n+1)%nc
                   else
                      write(*,*) "dir z"
                      write(*,*) "block",n+1," does not exist"
                      stop 
                   end if
                end if
             end if
             !--------------------------------------------------------------

             !--------------------------------------------------------------
             ! regular grid space size
             !--------------------------------------------------------------
             h=(grid(3)%blck(n)%max-grid(3)%blck(n)%min)/grid(3)%blck(n)%nc
             !--------------------------------------------------------------
             if ( grid(3)%blck(n)%dmin < h ) then
                coef_exp=1
                direct=.true.
             elseif ( grid(3)%blck(n)%dmin > h ) then
                coef_exp=-1
                direct=.false.
             else
                write(*,*) "grid_z"
                write(*,*) "block",n
                stop "chose a regular grid for this block"
             end if
             !--------------------------------------------------------------

             !--------------------------------------------------------------
             ! newton to find coef_exp such as dx_min = dmin
             !--------------------------------------------------------------
             inewton=0
             inc=1
             do while (abs(inc)>1d-12.and.inewton<200)
                inewton=inewton+1
                call compute_expo_f_df(      &
                     & grid(3)%blck(n)%min,  &
                     & grid(3)%blck(n)%max,  &
                     & coef_exp,             &
                     & h,                    &
                     & grid(3)%blck(n)%dmin, &
                     & f,                    &
                     & df)

                inc=-f/df
                coef_exp=coef_exp+inc
             end do
             !--------------------------------------------------------------

             !--------------------------------------------------------------
             ! nodes
             !--------------------------------------------------------------
             do m=0,grid(3)%blck(n)%nc
                i=grid(3)%blck(n)%shift+m

                call compute_expo_point(         &
                     & grid(3)%blck(n)%min,      &
                     & grid(3)%blck(n)%max,      &
                     & coef_exp,                 &
                     & grid(3)%blck(n)%min+m*h,  &
                     & f)

                ! sort nodes
                if (grid(3)%blck(n)%connect==0) then
                   i=grid(3)%blck(n)%shift+m
                   if (.not.direct) then
                      f=grid(3)%blck(n)%min &
                           & -(f-grid(3)%blck(n)%min)+(grid(3)%blck(n)%max-grid(3)%blck(n)%min)
                   end if
                else
                   i=grid(3)%blck(n)%shift+grid(3)%blck(n)%nc-m
                   if (direct) then
                      f=grid(3)%blck(n)%min &
                           & -(f-grid(3)%blck(n)%min)+(grid(3)%blck(n)%max-grid(3)%blck(n)%min)
                   end if
                end if
                grid_z(i)=f
             end do
             !--------------------------------------------------------------

          end select

       end do

       !--------------------------------------------------------------
       ! outside nodes
       !--------------------------------------------------------------
       do i=1,gz
          grid_z(gsz-i)=zmin-(grid_z(gsz+i)-zmin)
          grid_z(gez+i)=zmax+(zmax-grid_z(gez-i))
       end do
       !--------------------------------------------------------------

       !--------------------------------------------------------------
       ! check
       !--------------------------------------------------------------
       if (rank==0) then
          do i=gsz-gz,gez+gz
             write(31,*) i,grid_z(i)
          end do  
          write(31,*)
          write(31,*)
          flush(31)
       end if
       !--------------------------------------------------------------

    end if
    !--------------------------------------------------------------
    ! velocity grids and steps
    !--------------------------------------------------------------
    ! X-Velocity
    !--------------------------------------------------------------
    ! inside (compared to physical pressure nodes) 
    do i=gsxu+1,gexu-1
       grid_xu(i)=(grid_x(i)+grid_x(i-1))/2
    end do
    ! outside nodes
    do j=0,gx
       grid_xu(gsxu-j)=xmin-(grid_xu(gsxu+j+1)-xmin)
       grid_xu(gexu+j)=xmax+(xmax-grid_xu(gexu-j-1))
    end do
    
    !--------------------------------------------------------------
    ! check
    !--------------------------------------------------------------
    if (rank==0) then
       do i=gsxu-gx,gexu+gx
          write(32,*) i,grid_xu(i)
       end do
       write(32,*)
       write(32,*)
       flush(32)
    end if
    !--------------------------------------------------------------
    
    !--------------------------------------------------------------
    ! Y-Velocity
    !--------------------------------------------------------------
    ! inside (compared to physical pressure nodes) 
    do i=gsyv+1,geyv-1
       grid_yv(i)=(grid_y(i)+grid_y(i-1))/2
    end do
    ! outside nodes
    do j=0,gy
       grid_yv(gsyv-j)=ymin-(grid_yv(gsyv+j+1)-ymin)
       grid_yv(geyv+j)=ymax+(ymax-grid_yv(geyv-j-1))
    end do
    
    !--------------------------------------------------------------
    ! check
    !--------------------------------------------------------------
    if (rank==0) then
       do i=gsyv-gy,geyv+gy
          write(32,*) i,grid_yv(i)
       end do
       write(32,*)
       write(32,*)
       flush(32)
    end if
    !--------------------------------------------------------------
    
    !--------------------------------------------------------------
    ! Z-Velocity
    !--------------------------------------------------------------
    if (dim==2) then
       grid_zw=0
    else
       ! inside (compared to physical pressure nodes) 
       do i=gszw+1,gezw-1
          grid_zw(i)=(grid_z(i)+grid_z(i-1))/2
       end do
       ! outside nodes
       do j=0,gz
          grid_zw(gszw-j)=zmin-(grid_zw(gszw+j+1)-zmin)
          grid_zw(gezw+j)=zmax+(zmax-grid_zw(gezw-j-1))
       end do
       
       !--------------------------------------------------------------
       ! check
       !--------------------------------------------------------------
       if (rank==0) then
          do i=gszw-gz,gezw+gz
             write(32,*) i,grid_zw(i)
          end do
          write(32,*)
          write(32,*)
          flush(32)
       end if
       !--------------------------------------------------------------
    end if
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! space steps
    !--------------------------------------------------------------
    ! Scalar
    !--------------------------------------------------------------
    dx=1
    do i=sx-gx,ex+gx
       dx(i)=grid_xu(i+1)-grid_xu(i)
       dx2(i)=dx(i)/2
       ddx(i)=1/dx(i)
    end do
    dy=1
    do j=sy-gy,ey+gy
       dy(j)=grid_yv(j+1)-grid_yv(j)
       dy2(j)=dy(j)/2
       ddy(j)=1/dy(j)
    end do
    if (dim==2) then
       dz=1
       dz2=1
       ddz=1
    else
       dz=1
       do k=sz-gz,ez+gz
          dz(k)=grid_zw(k+1)-grid_zw(k)
          dz2(k)=dz(k)/2
          ddz(k)=1/dz(k)
       end do
    end if
    !--------------------------------------------------------------
    ! X-Velocity
    !--------------------------------------------------------------
    dxu=1
    do i=sxu-gx+1,exu+gx-1
       dxu(i)=grid_x(i)-grid_x(i-1)
    end do
    do i=sxu-gx,exu+gx
       if (i==gsxu-gx) then 
          dxu(i)=dxu(i+1)
       else if (i==gexu+gx) then
          dxu(i)=dxu(i-1)
       end if
       dxu2(i)=dxu(i)/2
       ddxu(i)=1/dxu(i)
    end do
    !--------------------------------------------------------------
    ! Y-Velocity
    !--------------------------------------------------------------
    dyv=1
    do i=syv-gy+1,eyv+gy-1
       dyv(i)=grid_y(i)-grid_y(i-1)
    end do
    do i=syv-gy,eyv+gy
       if (i==gsyv-gy) then 
          dyv(i)=dyv(i+1)
       else if (i==geyv+gy) then
          dyv(i)=dyv(i-1)
       end if
       dyv2(i)=dyv(i)/2
       ddyv(i)=1/dyv(i)
    end do
    !--------------------------------------------------------------
    ! Z-Velocity
    !--------------------------------------------------------------
    if (dim==2) then
       dzw=1
       dzw2=1
       ddzw=1
    else
       dzw=1
       do i=szw-gz+1,ezw+gz-1
          dzw(i)=grid_z(i)-grid_z(i-1)
       end do
       do i=szw-gz,ezw+gz
          if (i==gszw-gz) then 
             dzw(i)=dzw(i+1)
          else if (i==gezw+gz) then
             dzw(i)=dzw(i-1)
          end if
          dzw2(i)=dzw(i)/2
          ddzw(i)=1/dzw(i)
       end do
    end if
    !--------------------------------------------------------------
    dx_min=minval(dx);dx_max=maxval(dx)
    dy_min=minval(dy);dy_max=maxval(dy)
    dz_min=minval(dz);dz_max=maxval(dz)
    !--------------------------------------------------------------

    
    !--------------------------------------------------------------
    ! fill mesh structure
    !--------------------------------------------------------------

    mesh%active      = .true.

    mesh%dim         = dim

    mesh%nx          = nx
    mesh%ny          = ny
    mesh%nz          = nz

    mesh%xmin        = xmin
    mesh%xmax        = xmax
    mesh%ymin        = ymin
    mesh%ymax        = ymax
    mesh%zmin        = zmin
    mesh%zmax        = zmax

    mesh%periodic(1) = periodic(1)
    mesh%periodic(2) = periodic(2)
    mesh%periodic(3) = periodic(3)
    
    
    mesh%gx   = gx
    mesh%gy   = gy
    mesh%gz   = gz

    mesh%gsx  = gsx
    mesh%gsy  = gsy
    mesh%gsz  = gsz
    mesh%gex  = gex
    mesh%gey  = gey
    mesh%gez  = gez

    mesh%gsxu = gsxu
    mesh%gsyu = gsyu
    mesh%gszu = gszu
    mesh%gexu = gexu
    mesh%geyu = geyu
    mesh%gezu = gezu

    mesh%gsxv = gsxv
    mesh%gsyv = gsyv
    mesh%gszv = gszv
    mesh%gexv = gexv
    mesh%geyv = geyv
    mesh%gezv = gezv

    mesh%gsxw = gsxw
    mesh%gsyw = gsyw
    mesh%gszw = gszw
    mesh%gexw = gexw
    mesh%geyw = geyw
    mesh%gezw = gezw
    
    mesh%sx   = sx
    mesh%sy   = sy
    mesh%sz   = sz 
    mesh%ex   = ex
    mesh%ey   = ey
    mesh%ez   = ez 

    mesh%sxu  = sxu
    mesh%syu  = syu
    mesh%szu  = szu 
    mesh%exu  = exu
    mesh%eyu  = eyu
    mesh%ezu  = ezu 

    mesh%sxv  = sxv
    mesh%syv  = syv
    mesh%szv  = szv 
    mesh%exv  = exv
    mesh%eyv  = eyv
    mesh%ezv  = ezv

    mesh%sxw  = sxw
    mesh%syw  = syw
    mesh%szw  = szw 
    mesh%exw  = exw
    mesh%eyw  = eyw
    mesh%ezw  = ezw

    mesh%sxs  = sxs
    mesh%sys  = sys
    mesh%szs  = szs
    mesh%exs  = exs
    mesh%eys  = eys
    mesh%ezs  = ezs

    mesh%sxus = sxus
    mesh%syus = syus
    mesh%szus = szus
    mesh%exus = exus
    mesh%eyus = eyus
    mesh%ezus = ezus

    mesh%sxvs = sxvs
    mesh%syvs = syvs
    mesh%szvs = szvs
    mesh%exvs = exvs
    mesh%eyvs = eyvs
    mesh%ezvs = ezvs

    mesh%sxws = sxws
    mesh%syws = syws
    mesh%szws = szws 
    mesh%exws = exws
    mesh%eyws = eyws
    mesh%ezws = ezws
    
    ! dx
    allocate(mesh%dx(mesh%sx-gx:mesh%ex+gx))
    allocate(mesh%dxu(mesh%sxu-gx:mesh%exu+gx))
    allocate(mesh%dy(mesh%sy-gy:mesh%ey+gy))
    allocate(mesh%dyv(mesh%syv-gy:mesh%eyv+gy))
    allocate(mesh%dz(mesh%sz-gz:mesh%ez+gz))
    allocate(mesh%dzw(mesh%szw-gz:mesh%ezw+gz))
    ! dx/2
    allocate(mesh%dx2(mesh%sx-gx:mesh%ex+gx))
    allocate(mesh%dxu2(mesh%sxu-gx:mesh%exu+gx))
    allocate(mesh%dy2(mesh%sy-gy:mesh%ey+gy))
    allocate(mesh%dyv2(mesh%syv-gy:mesh%eyv+gy))
    allocate(mesh%dz2(mesh%sz-gz:mesh%ez+gz))
    allocate(mesh%dzw2(mesh%szw-gz:mesh%ezw+gz))
    ! 1/dx
    allocate(mesh%ddx(mesh%sx-gx:mesh%ex+gx))
    allocate(mesh%ddxu(mesh%sxu-gx:mesh%exu+gx))
    allocate(mesh%ddy(mesh%sy-gy:mesh%ey+gy))
    allocate(mesh%ddyv(mesh%syv-gy:mesh%eyv+gy))
    allocate(mesh%ddz(mesh%sz-gz:mesh%ez+gz))
    allocate(mesh%ddzw(mesh%szw-gz:mesh%ezw+gz))
    ! grids (global)
    allocate(mesh%x(mesh%gsx-gx:mesh%gex+gx))
    allocate(mesh%y(mesh%gsy-gy:mesh%gey+gy))
    allocate(mesh%z(mesh%gsz-gz:mesh%gez+gz))
    allocate(mesh%xu(mesh%gsxu-gx:mesh%gexu+gx))
    allocate(mesh%yv(mesh%gsyv-gy:mesh%geyv+gy))
    allocate(mesh%zw(mesh%gszw-gz:mesh%gezw+gz))
    
    mesh%dx   = dx
    mesh%dxu  = dxu
    mesh%dy   = dy
    mesh%dyv  = dyv
    mesh%dz   = dz
    mesh%dzw  = dzw
    ! dx/2
    mesh%dx2  = dx2
    mesh%dxu2 = dxu2
    mesh%dy2  = dy2
    mesh%dyv2 = dyv2
    mesh%dz2  = dz2
    mesh%dzw2 = dzw2
    ! 1/dx
    mesh%ddx  = ddx
    mesh%ddxu = ddxu
    mesh%ddy  = ddy
    mesh%ddyv = ddyv
    mesh%ddz  = ddz
    mesh%ddzw = ddzw
    ! grids (global)
    mesh%x    = grid_x
    mesh%y    = grid_y
    mesh%z    = grid_z
    mesh%xu   = grid_xu
    mesh%yv   = grid_yv
    mesh%zw   = grid_zw
    
    mesh%nb%no_ghost%u = (mesh%exu-mesh%sxu+1)*(mesh%eyu-mesh%syu+1)*(mesh%ezu-mesh%szu+1)
    mesh%nb%no_ghost%v = (mesh%exv-mesh%sxv+1)*(mesh%eyv-mesh%syv+1)*(mesh%ezv-mesh%szv+1)
    mesh%nb%no_ghost%w = (mesh%exw-mesh%sxw+1)*(mesh%eyw-mesh%syw+1)*(mesh%ezw-mesh%szw+1) * (mesh%dim-2)
    mesh%nb%no_ghost%p = (mesh%ex -mesh%sx +1)*(mesh%ey -mesh%sy +1)*(mesh%ez -mesh%sz +1)
    
    mesh%nb%tot%u = (mesh%exus-mesh%sxus+1)*(mesh%eyus-mesh%syus+1)*(mesh%ezus-mesh%szus+1)
    mesh%nb%tot%v = (mesh%exvs-mesh%sxvs+1)*(mesh%eyvs-mesh%syvs+1)*(mesh%ezvs-mesh%szvs+1)
    mesh%nb%tot%w = (mesh%exws-mesh%sxws+1)*(mesh%eyws-mesh%syws+1)*(mesh%ezws-mesh%szws+1) * (mesh%dim-2)
    mesh%nb%tot%p = (mesh%exs -mesh%sxs +1)*(mesh%eys -mesh%sys +1)*(mesh%ezs -mesh%szs +1)

    mesh%nb%no_ghost%uv   = mesh%nb%no_ghost%u   + mesh%nb%no_ghost%v
    mesh%nb%no_ghost%uvw  = mesh%nb%no_ghost%uv  + mesh%nb%no_ghost%w * (mesh%dim-2)
    mesh%nb%no_ghost%uvwp = mesh%nb%no_ghost%uvw + mesh%nb%no_ghost%p

    mesh%nb%tot%uv   = mesh%nb%tot%u   + mesh%nb%tot%v
    mesh%nb%tot%uvw  = mesh%nb%tot%uv  + mesh%nb%tot%w * (mesh%dim-2)
    mesh%nb%tot%uvwp = mesh%nb%tot%uvw + mesh%nb%tot%p
    !--------------------------------------------------------------
    
    !--------------------------------------------------------------
    ! mesh dual
    !--------------------------------------------------------------
    if (mesh_dual_LvL>1) then

       mesh_dual%id     = 1
       mesh_dual%active = .true.
       mesh_dual%dim    = mesh%dim

       mesh_dual%gx     = mesh%gx
       mesh_dual%gy     = mesh%gy
       mesh_dual%gz     = mesh%gz
       
       mesh_dual%xmin   = mesh%xmin
       mesh_dual%xmax   = mesh%xmax
       mesh_dual%ymin   = mesh%ymin
       mesh_dual%ymax   = mesh%ymax
       mesh_dual%zmin   = mesh%zmin
       mesh_dual%zmax   = mesh%zmax

       !--------------------------------------------------------------
       ! grid interp
       !--------------------------------------------------------------
       if (.not.regular_mesh) then
          write(*,*) "DUAL GRIDS ONLY WITH REGULAR MESHES !!!"
          write(*,*) "DUAL GRIDS ONLY WITH REGULAR MESHES !!!"
          write(*,*) "DUAL GRIDS ONLY WITH REGULAR MESHES !!!"
          write(*,*) "DUAL GRIDS ONLY WITH REGULAR MESHES !!!"
          write(*,*) "DUAL GRIDS ONLY WITH REGULAR MESHES !!!"
          stop 
       end if
       !--------------------------------------------------------------

       !--------------------------------------------------------------
       ! even/odd case
       !--------------------------------------------------------------
       if (modulo(mesh_dual_LvL,2)==0) then
          mesh_dual_is_even = .true.
       else
          mesh_dual_is_even = .false.
       end if
       !--------------------------------------------------------------


       !--------------------------------------------------------------
       ! scalar mesh
       !--------------------------------------------------------------
       ! indexes (global)
       !--------------------------------------------------------------
       ! X-direction
       !--------------------------------------------------------------
       mesh_dual%gsx = mesh%gsx
       mesh_dual%gex = mesh_dual_LvL * mesh%gex

       !--------------------------------------------------------------
       ! even case
       !--------------------------------------------------------------
       if (mesh_dual_is_even) then
          mesh_dual%gex = mesh_dual%gex-1
       end if
       !--------------------------------------------------------------
       ! array grid interp
       !--------------------------------------------------------------
       allocate(mesh_dual%x(mesh_dual%gsx-mesh_dual%gx:mesh_dual%gex+mesh_dual%gx))
       do i = mesh_dual%gsx,mesh_dual%gex
          if (mesh_dual_is_even) then
             mesh_dual%x(i) = mesh%xmin + (i+0.5d0)*mesh%dx(sx)/mesh_dual_LvL
          else
             mesh_dual%x(i) = mesh%xmin + i*mesh%dx(sx)/mesh_dual_LvL
          end if
       end do
       !--------------------------------------------------------------
       ! outside nodes
       !--------------------------------------------------------------
       do i = 1,mesh_dual%gx
          if (mesh_dual_is_even) then
             mesh_dual%x(mesh_dual%gsx-i) = mesh%xmin - (mesh_dual%x(mesh_dual%gsx+i-1)-mesh%xmin)
             mesh_dual%x(mesh_dual%gex+i) = mesh%xmax + (mesh%xmax-mesh_dual%x(mesh_dual%gex-i+1))
          else 
             mesh_dual%x(mesh_dual%gsx-i) = mesh%xmin - (mesh_dual%x(mesh_dual%gsx+i)-mesh%xmin)
             mesh_dual%x(mesh_dual%gex+i) = mesh%xmax + (mesh%xmax-mesh_dual%x(mesh_dual%gex-i))
          end if
       end do
       !--------------------------------------------------------------

       !--------------------------------------------------------------
       ! Y-direction
       !--------------------------------------------------------------
       mesh_dual%gsy = mesh%gsy
       mesh_dual%gey = mesh_dual_LvL * mesh%gey

       !--------------------------------------------------------------
       ! even case
       !--------------------------------------------------------------
       if (mesh_dual_is_even) then
          mesh_dual%gey = mesh_dual%gey-1
       end if
       !--------------------------------------------------------------
       ! array grid interp
       !--------------------------------------------------------------
       allocate(mesh_dual%y(mesh_dual%gsy-mesh_dual%gy:mesh_dual%gey+mesh_dual%gy))
       do i=mesh_dual%gsy,mesh_dual%gey
          if (mesh_dual_is_even) then
             mesh_dual%y(i) = mesh%ymin + (i+0.5d0)*mesh%dy(sy)/mesh_dual_LvL
          else
             mesh_dual%y(i) = mesh%ymin + i*mesh%dy(sy)/mesh_dual_LvL
          end if
       end do
       !--------------------------------------------------------------
       ! outside nodes
       !--------------------------------------------------------------
       do i = 1,mesh_dual%gy
          if (mesh_dual_is_even) then
             mesh_dual%y(mesh_dual%gsy-i) = mesh%ymin - (mesh_dual%y(mesh_dual%gsy+i-1)-mesh%ymin)
             mesh_dual%y(mesh_dual%gey+i) = mesh%ymax + (mesh%ymax-mesh_dual%y(mesh_dual%gey-i+1))
          else 
             mesh_dual%y(mesh_dual%gsy-i) = mesh%ymin - (mesh_dual%y(mesh_dual%gsy+i)-mesh%ymin)
             mesh_dual%y(mesh_dual%gey+i) = mesh%ymax + (mesh%ymax-mesh_dual%y(mesh_dual%gey-i))
          end if
       end do

       !--------------------------------------------------------------
       ! Z-direction
       !--------------------------------------------------------------
       if (mesh%dim==3) then
          mesh_dual%gsz = mesh%gsz
          mesh_dual%gez = mesh_dual_LvL * mesh%gez
       else
          mesh_dual%gsz = mesh%gsz
          mesh_dual%gez = mesh%gez
       end if
       !--------------------------------------------------------------
       ! even case
       !--------------------------------------------------------------
       if (mesh_dual_is_even) then
          if (mesh%dim==3) mesh_dual%ez = mesh_dual%ez-1
       end if

       !--------------------------------------------------------------
       ! array grid interp
       !--------------------------------------------------------------
       allocate(mesh_dual%z(mesh_dual%gsz-mesh_dual%gz:mesh_dual%gez+mesh_dual%gz))
       do i = mesh_dual%gsz,mesh_dual%gez
          if (mesh_dual_is_even) then
             mesh_dual%z(i) = zmin + (i+0.5d0)*mesh%dz(sz)/mesh_dual_LvL
          else
             mesh_dual%z(i) = zmin + i*mesh%dz(sz)/mesh_dual_LvL
          end if
       end do

       !--------------------------------------------------------------
       ! outside nodes
       !--------------------------------------------------------------
       do i = 1,mesh_dual%gz
          if (mesh_dual_is_even) then
             mesh_dual%z(mesh_dual%gsz-i) = mesh%zmin - (mesh_dual%z(mesh_dual%gsz+i-1)-mesh%zmin)
             mesh_dual%z(mesh_dual%gez+i) = mesh%zmax + (mesh%zmax-mesh_dual%z(mesh_dual%gez-i+1))
          else 
             mesh_dual%y(mesh_dual%gsz-i) = mesh%zmin - (mesh_dual%z(mesh_dual%gsz+i)-mesh%zmin)
             mesh_dual%y(mesh_dual%gez+i) = mesh%zmax + (mesh%zmax-mesh_dual%z(mesh_dual%gez-i))
          end if
       end do
       !--------------------------------------------------------------


       !--------------------------------------------------------------
       ! velocity meshes
       !--------------------------------------------------------------
       ! indexes
       !--------------------------------------------------------------
       ! X-Velocity
       !--------------------------------------------------------------
       ! u-velocity interp grid from scalar interp grid
       !--------------------------------------------------------------
       ! indexes
       !--------------------------------------------------------------
       mesh_dual%gsxu = mesh_dual%gsx
       mesh_dual%gexu = mesh_dual%gex+1
       mesh_dual%gsyu = mesh_dual%gsy
       mesh_dual%geyu = mesh_dual%gey
       if (mesh%dim==3) then
          mesh_dual%gszu = mesh_dual%gsz
          mesh_dual%gezu = mesh_dual%gez
       else
          mesh_dual%gszu = mesh%gszu
          mesh_dual%gezu = mesh%gezu
       end if
       !--------------------------------------------------------------
       ! array grid interp
       !--------------------------------------------------------------
       ! X-direction
       !--------------------------------------------------------------
       allocate(mesh_dual%xu(mesh_dual%gsxu-mesh_dual%gx:mesh_dual%gexu+mesh_dual%gx))
       do i = mesh_dual%gsxu,mesh_dual%gexu
          mesh_dual%xu(i) = 0.5d0*(mesh_dual%x(i)+mesh_dual%x(i-1))
       end do
       !--------------------------------------------------------------

       !--------------------------------------------------------------
       ! outside nodes
       !--------------------------------------------------------------
       ! X-direction
       !--------------------------------------------------------------
       do i = 1,mesh_dual%gx
          if (mesh_dual_is_even) then
             mesh_dual%xu(mesh_dual%gsxu-i) = mesh%xmin - (mesh_dual%xu(mesh_dual%gsxu+i)-mesh%xmin)
             mesh_dual%xu(mesh_dual%gexu+i) = mesh%xmax + (mesh%xmax-mesh_dual%xu(mesh_dual%gexu-i))
          else
             mesh_dual%xu(mesh_dual%gsxu-i) = mesh%xmin - (mesh_dual%xu(mesh_dual%gsxu+i+1)-mesh%xmin)
             mesh_dual%xu(mesh_dual%gexu+i) = mesh%xmax + (mesh%xmax-mesh_dual%xu(mesh_dual%gexu-i-1))
          end if
       end do
       !--------------------------------------------------------------
       
       !--------------------------------------------------------------
       ! Y-velocity
       !--------------------------------------------------------------
       ! v-velocity interp grid from scalar interp grid
       !--------------------------------------------------------------
       ! indexes
       !--------------------------------------------------------------
       mesh_dual%gsxv = mesh_dual%gsx
       mesh_dual%gexv = mesh_dual%gex
       mesh_dual%gsyv = mesh_dual%gsy
       mesh_dual%geyv = mesh_dual%gey+1
       if (mesh%dim==3) then
          mesh_dual%gszv = mesh_dual%gsz
          mesh_dual%gezv = mesh_dual%gez
       else
          mesh_dual%gszv = mesh%gszv
          mesh_dual%gezv = mesh%gezv
       end if
       !--------------------------------------------------------------
       ! array grid interp
       !--------------------------------------------------------------
       ! Y-direction
       !--------------------------------------------------------------
       allocate(mesh_dual%yv(mesh_dual%gsyv-mesh_dual%gy:mesh_dual%geyv+mesh_dual%gy))
       do i = mesh_dual%gsyv,mesh_dual%geyv
          mesh_dual%yv(i) = 0.5d0*(mesh_dual%y(i)+mesh_dual%y(i-1))
       end do
       !--------------------------------------------------------------

       !--------------------------------------------------------------
       ! outside nodes
       !--------------------------------------------------------------
       ! Y-direction
       !--------------------------------------------------------------
       do i = 1,mesh_dual%gy
          if (mesh_dual_is_even) then
             mesh_dual%yv(mesh_dual%gsyv-i) = mesh%ymin - (mesh_dual%yv(mesh_dual%gsyv+i)-mesh%ymin)
             mesh_dual%yv(mesh_dual%geyv+i) = mesh%ymax + (mesh%ymax-mesh_dual%yv(mesh_dual%geyv-i))
          else
             mesh_dual%yv(mesh_dual%gsyv-i) = mesh%ymin - (mesh_dual%yv(mesh_dual%gsyv+i+1)-mesh%ymin)
             mesh_dual%yv(mesh_dual%geyv+i) = mesh%ymax + (mesh%ymax-mesh_dual%yv(mesh_dual%geyv-i-1))
          end if
       end do
       !--------------------------------------------------------------

       !--------------------------------------------------------------
       ! Z-velocity
       !--------------------------------------------------------------
       ! w-velocity interp grid from scalar interp grid
       !--------------------------------------------------------------
       ! indexes
       !--------------------------------------------------------------
       mesh_dual%gsxw = mesh_dual%gsx
       mesh_dual%gexw = mesh_dual%gex
       mesh_dual%gsyw = mesh_dual%gsy
       mesh_dual%geyw = mesh_dual%gey
       if (mesh%dim==3) then
          mesh_dual%gszw = mesh_dual%gsz
          mesh_dual%gezw = mesh_dual%gez+1
       else
          mesh_dual%gszw = mesh%gszw
          mesh_dual%gezw = mesh%gezw
       end if
       !--------------------------------------------------------------
       ! array grid interp
       !--------------------------------------------------------------
       ! Z-direction
       !--------------------------------------------------------------
       allocate(mesh_dual%zw(mesh_dual%gszw-mesh_dual%gz:mesh_dual%gezw+mesh_dual%gz))
       mesh_dual%zw = 1
       if (mesh%dim==3) then 
          do i = mesh_dual%gszw,mesh_dual%gezw
             mesh_dual%zw(i) = 0.5d0*(mesh_dual%z(i)+mesh_dual%z(i-1))
          end do
       end if
       !--------------------------------------------------------------

       !--------------------------------------------------------------
       ! outside nodes
       !--------------------------------------------------------------
       ! Z-direction
       !--------------------------------------------------------------
       if (mesh%dim==3) then
          do i=1,gz
             if (mesh_dual_is_even) then
                mesh_dual%zw(mesh_dual%gszw-i) = mesh%zmin-(mesh_dual%zw(mesh_dual%gszw+i)-mesh%zmin)
                mesh_dual%zw(mesh_dual%gezw+i) = mesh%zmax+(mesh%zmax-mesh_dual%zw(mesh_dual%gezw-i))
             else
                mesh_dual%zw(mesh_dual%gszw-i) = mesh%zmin-(mesh_dual%zw(mesh_dual%gszw+i+1)-mesh%zmin)
                mesh_dual%zw(mesh_dual%gezw+i) = mesh%zmax+(mesh%zmax-mesh_dual%zw(mesh_dual%gezw-i-1))
             end if
          end do
       end if
       !--------------------------------------------------------------

       !--------------------------------------------------------------
       ! local indexes computations
       !--------------------------------------------------------------
       ! X-direction 
       !--------------------------------------------------------------
       mesh_dual%sx = mesh_dual_LvL * mesh%sx
       mesh_dual%ex = mesh_dual_LvL * mesh%ex + mesh_dual_LvL - 1
       if (mesh%ex==mesh%gex) then
          if (mesh_dual_is_even) then 
             mesh_dual%ex = mesh_dual_LvL * mesh%ex - 1
          else
             mesh_dual%ex = mesh_dual_LvL * mesh%ex
          end if
       end if
       !--------------------------------------------------------------
       ! Y-direction
       !--------------------------------------------------------------
       mesh_dual%sy = mesh_dual_LvL * mesh%sy
       mesh_dual%ey = mesh_dual_LvL * mesh%ey + mesh_dual_LvL - 1
       if (mesh%ey==mesh%gey) then
          if (mesh_dual_is_even) then 
             mesh_dual%ey = mesh_dual_LvL * mesh%ey - 1
          else
             mesh_dual%ey = mesh_dual_LvL * mesh%ey
          end if
       end if
       !--------------------------------------------------------------
       ! Z-direction
       !--------------------------------------------------------------
       if (mesh%dim==3) then
          mesh_dual%sz = mesh_dual_LvL * mesh%sz
          mesh_dual%ez = mesh_dual_LvL * mesh%ez + mesh_dual_LvL - 1
          if (mesh%ez==mesh%gez) then
             if (mesh_dual_is_even) then 
                mesh_dual%ez = mesh_dual_LvL * mesh%ez - 1
             else
                mesh_dual%ez = mesh_dual_LvL * mesh%ez
             end if
          end if
       else
          mesh_dual%sz = mesh%ez
          mesh_dual%ez = mesh%ez
       end if
       !--------------------------------------------------------------
       ! velocity local indexes computations
       !--------------------------------------------------------------
       ! X-direction 
       !--------------------------------------------------------------
       mesh_dual%sxu = mesh_dual%sx
       mesh_dual%exu = mesh_dual%ex
       mesh_dual%syu = mesh_dual%sy
       mesh_dual%eyu = mesh_dual%ey
       mesh_dual%szu = mesh_dual%sz
       mesh_dual%ezu = mesh_dual%ez
       if (mesh%ex==mesh%gex) mesh_dual%exu = mesh_dual%exu + 1
       !--------------------------------------------------------------
       ! Y-direction 
       !--------------------------------------------------------------
       mesh_dual%sxv = mesh_dual%sx
       mesh_dual%exv = mesh_dual%ex
       mesh_dual%syv = mesh_dual%sy
       mesh_dual%eyv = mesh_dual%ey
       mesh_dual%szv = mesh_dual%sz
       mesh_dual%ezv = mesh_dual%ez
       if (mesh%ey==mesh%gey) mesh_dual%eyv = mesh_dual%eyv + 1
       !--------------------------------------------------------------
       ! Z-direction 
       !--------------------------------------------------------------
       mesh_dual%sxw = mesh_dual%sx
       mesh_dual%exw = mesh_dual%ex
       mesh_dual%syw = mesh_dual%sy
       mesh_dual%eyw = mesh_dual%ey
       mesh_dual%szw = mesh_dual%sz
       mesh_dual%ezw = mesh_dual%ez
       if (mesh%dim==3) then
          if (mesh%ez==mesh%gez) mesh_dual%exw = mesh_dual%exw + 1
       end if
       !--------------------------------------------------------------

       !write(*,*) rank, mesh_dual%sx, mesh_dual%ex, mesh_dual%sxu, mesh_dual%exu
       
       !--------------------------------------------------------------

       
       !--------------------------------------------------------------
       ! space steps
       !--------------------------------------------------------------
       ! dx_interp, dxu_interp
       !--------------------------------------------------------------
       allocate(mesh_dual%dx(mesh_dual%sx-mesh_dual%gx:mesh_dual%ex+mesh_dual%gx))
       mesh_dual%dx = 1
       do i = mesh_dual%sx-mesh_dual%gx,mesh_dual%ex+mesh_dual%gx
          mesh_dual%dx(i) = mesh_dual%xu(i+1)-mesh_dual%xu(i)
       end do
       !--------------------------------------------------------------
       allocate(mesh_dual%dxu(mesh_dual%sxu-mesh_dual%gx:mesh_dual%exu+mesh_dual%gx))
       mesh_dual%dxu = 1
       do i = mesh_dual%sxu-mesh_dual%gx+1,mesh_dual%exu+mesh_dual%gx-1
          mesh_dual%dxu(i) = mesh_dual%x(i)-mesh_dual%x(i-1)
       end do
       !--------------------------------------------------------------

       !--------------------------------------------------------------
       ! dy_interp, dyv_interp
       !-------------------------------------------------------------- 
       allocate(mesh_dual%dy(mesh_dual%sy-mesh_dual%gy:mesh_dual%ey+mesh_dual%gy))
       mesh_dual%dy = 1
       do i = mesh_dual%sy-mesh_dual%gy,mesh_dual%ey+mesh_dual%gy
          mesh_dual%dy(i) = mesh_dual%yv(i+1)-mesh_dual%yv(i)
       end do
       !--------------------------------------------------------------
       allocate(mesh_dual%dyv(mesh_dual%syv-mesh_dual%gy:mesh_dual%eyv+mesh_dual%gy))
       mesh_dual%dyv = 1
       do i = mesh_dual%syv-mesh_dual%gy+1,mesh_dual%eyv+mesh_dual%gy-1
          mesh_dual%dyv(i) = mesh_dual%y(i)-mesh_dual%y(i-1)
       end do
       !--------------------------------------------------------------

       !--------------------------------------------------------------
       ! dz_interp, dzw_interp
       !-------------------------------------------------------------- 
       allocate(mesh_dual%dz(mesh_dual%sz-mesh_dual%gz:mesh_dual%ez+mesh_dual%gz))
       mesh_dual%dz = 1
       if (mesh%dim==3) then 
          do i = mesh_dual%sz-gz,mesh_dual%ez+gz
             mesh_dual%dz(i) = mesh_dual%zw(i+1)-mesh_dual%zw(i)
          end do
       end if
       !--------------------------------------------------------------
       allocate(mesh_dual%dzw(mesh_dual%szw-mesh_dual%gz:mesh_dual%ezw+mesh_dual%gz))
       mesh_dual%dzw = 1
       if (mesh%dim==3) then 
          do i = mesh_dual%szw-mesh_dual%gz+1,mesh_dual%ezw+mesh_dual%gz-1
             mesh_dual%dzw(i) = mesh_dual%z(i)-mesh_dual%z(i-1)
          end do
       end if
       !-------------------------------------------------------------
       
       
       !-------------------------------------------------------------
       if (rank==0) then
          do i = mesh_dual%gsx-mesh_dual%gx,mesh_dual%gex+mesh_dual%gx
             write(41,*) i,mesh_dual%x(i)
          end do
          write(41,*)
          write(41,*)

          do i = mesh_dual%gsy-mesh_dual%gy,mesh_dual%gey+mesh_dual%gy
             write(41,*) i,mesh_dual%y(i)
          end do
          write(41,*)
          write(41,*)

          if (mesh_dual%dim==3) then 
             do i = mesh_dual%gsz-mesh_dual%gz,mesh_dual%gez+mesh_dual%gz
                write(41,*) i,mesh_dual%z(i)
             end do
             write(41,*)
             write(41,*)
          end if
          
          do i = mesh_dual%sxu-mesh_dual%gx,mesh_dual%exu+mesh_dual%gx
             write(41,*) i,mesh_dual%xu(i)
          end do
          write(41,*)
          write(41,*)

          do i = mesh_dual%syv-mesh_dual%gy,mesh_dual%eyv+mesh_dual%gy
             write(41,*) i,mesh_dual%yv(i)
          end do
          write(41,*)
          write(41,*)

          if (mesh_dual%dim==3) then 
             do i = mesh_dual%szw-mesh_dual%gz,mesh_dual%ezw+mesh_dual%gz
                write(41,*) i,mesh_dual%zw(i)
             end do
             write(41,*)
             write(41,*)
          end if

          flush(41)
       end if
       !--------------------------------------------------------------

    end if
    !--------------------------------------------------------------
    
    !--------------------------------------------------------------
    ! fill solver structure 
    !--------------------------------------------------------------
    nvstks_solver%solver%hypre(0)%maxLvL = NS_HYPRE_maxLvL
    nvstks_solver%solver%hypre(1)%maxLvL = NS_HYPRE_maxLvL
    nvstks_solver%solver%hypre(2)%maxLvL = NS_HYPRE_maxLvL
    nvstks_solver%solver%hypre(3)%maxLvL = NS_HYPRE_maxLvL
    !--------------------------------------------------------------
    

    
    !--------------------------------------------------------------
    ! Lagrangian advection scheme
    !--------------------------------------------------------------
    select case(EN_inertial_scheme)
    case(-29:-10)
       LG_advection=.true.
    end select
    !--------------------------------------------------------------
    select case(NS_inertial_scheme)
    case(-29:-10)
       LG_advection=.true.
    end select
    !--------------------------------------------------------------
    select case(VOF_method)
    case(-39:-10)
       LG_advection=.true.
    end select
    !--------------------------------------------------------------
    if (nPart==0.and..not.LG_advection) LG_activate=.false.
    if (LG_advection) then
       if (nppdpc<1) nppdpc=1
       LG_activate=.false.
       LG_drag=.false.
       LG_lift=.false.
       LG_grav=.false.
       LG_arch=.false.
       LG_bass=.false.
       LG_addm=.false.
       LG_2Way=.false.
    end if
    !--------------------------------------------------------------
    if (dt_LG==0) dt_LG=dt 
    !--------------------------------------------------------------
    if (nppdpc_min<0) nppdpc_min=nppdpc
    !--------------------------------------------------------------
    
    !--------------------------------------------------------------
    ! temperature field variables
    !--------------------------------------------------------------
    if (.not.EN_activate) then
       EN_init=.false.
    end if
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! VOF field variables
    !--------------------------------------------------------------
    if (.not.VOF_activate) then
       VOF_init=.false.
    end if
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! restart 
    !--------------------------------------------------------------
    if (irestart==0)    irestart=nstep
    if (irestart>nstep) irestart=nstep
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! probes
    !--------------------------------------------------------------
    if (dim==2) then
       xyzProbe_sca(:,3)=zmin
       xyzProbe_uvw(:,3)=zmin
       xyzProbe_p(:,3)=zmin
    end if
    !--------------------------------------------------------------
   
    !--------------------------------------------------------------
    ! Outputs
    ! if > 0, frequency
    !    < 0, number
    !--------------------------------------------------------------
    if (cfl%active) then
    else
       if (iplot<0)    iplot=nstep/abs(iplot)
       if (irestart<0) irestart=nstep/abs(irestart)
       !--------------------------------------------------------------
       if (rank==0) then
          write(*,*) "-------------------------------------------------"
          write(*,*) "Outputs frequency"
          write(*,'(1x,a,1x,i5)') "irestart =",irestart
          write(*,'(1x,a,1x,i5)') "iplot    =",iplot
          write(*,'(1x,a,1x,i5)') "istep    =",istep
          write(*,*) "-------------------------------------------------"
       end if
       !--------------------------------------------------------------
    end if
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! VOF Manifold death frequency
    !--------------------------------------------------------------
    if (VOF_activate) then
       if (VOF_MD_activate) then
          if (VOF_MD_dt>0) then
             VOF_MD_istep=(nstep*dt)/VOF_MD_dt
          end if
       end if
    end if
    !--------------------------------------------------------------

    !-----------------------------------------------------
    ! random numbers initialization
    !-----------------------------------------------------
    call random_seed(size=s)
    allocate(p(s))
    if (seed>0) then 
       !-----------------------------------------------------
       ! fixed (determinist random draw)
       !-----------------------------------------------------
       clock=seed
       !-----------------------------------------------------
    else
       !-----------------------------------------------------
       ! random number based on clock time (pseudo stochastic) 
       !-----------------------------------------------------
       call system_clock(count=clock)
       !-----------------------------------------------------
    end if
    p(:) = (rank+1) * 100 * clock + 100 * (/ (i - 1, i = 1, s) /)
    !-----------------------------------------------------
    call random_seed(put=p)
    deallocate(p)
    !-----------------------------------------------------

    

    !--------------------------------------------------------------
    ! prints
    !--------------------------------------------------------------
    if (rank==0) then
       open(11,file='run.info',action='write')
#if IFORT
#else 
       write(11,*) "The current process ID is ", getpid()
       write(11,*) "Your numerical user ID is ", getuid()
       write(11,*) "Your numerical group ID is ", getgid()
#endif 

       call system("hostname > host.tmp")
       open(10,file='host.tmp')
       read(10,*) machine
       close(10)    
       call system("rm -f host.tmp")    
       !--------------------------------------------------------------
       call date_and_time (date,time)
       !--------------------------------------------------------------
       write(11,'(1x,a,30(a))') 'Executed on ', trim(adjustl(machine)) 
       write(11,'(1x,a,6(a))') 'date: ', date(7:8),"/",date(5:6),"/",date(1:4) 
       write(11,'(1x,a,5(a))') 'time: ', time(1:2),":",time(3:4),":",time(5:6)
       !--------------------------------------------------------------
       write(11,*) "The random number seed is", clock
       close(11)
       call system("cat run.info")
    end if
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! remove data.change
    !--------------------------------------------------------------
    if (rank==0) then
       name="data.change"
       inquire(file=trim(adjustl(name)),exist=file_exists)
       if (file_exists) then
          call system("rm -r data.change")
       end if
    end if
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! remove *.case and create ensight_gold rep for vizu files
    !--------------------------------------------------------------
    if (rank==0) then
       call system("rm -f *.case")
    end if
    !--------------------------------------------------------------
    
  end subroutine init_parameters

  subroutine change_parameters
    !--------------------------------------------------------------
    ! modules
    !--------------------------------------------------------------
    implicit none
    !--------------------------------------------------------------
    ! global variables
    !--------------------------------------------------------------

    !--------------------------------------------------------------
    ! local variables
    !--------------------------------------------------------------
    character(len=14) :: name
    logical           :: file_exists
    !--------------------------------------------------------------


    !*******************************************************************************!  
    ! Reading variables inputs
    !*******************************************************************************!  
    name="data.change"
    inquire(file=trim(adjustl(name)),exist=file_exists)
    if (file_exists) then 
       open(unit=10,file=name,status="old",action="read")
       read(10,nml=nml_time_integration) ; rewind(10)
       read(10,nml=nml_data_files)       ; rewind(10)
       read(10,nml=nml_nvstks_solver)    ; rewind(10)
       read(10,nml=nml_energy_solver)    ; rewind(10)
       close(10)
    end if
    !*******************************************************************************!  

  end subroutine change_parameters


  function gauss_lobatto(n)
    ! gauss-lobatto points in range [-1:1]
    !--------------------------------------------------------------
    ! modules
    !--------------------------------------------------------------
    implicit none
    !--------------------------------------------------------------
    ! global variables
    !--------------------------------------------------------------
    integer, intent(in)                :: n
    real(8), allocatable, dimension(:) :: gauss_lobatto
    !--------------------------------------------------------------
    ! local variables
    !--------------------------------------------------------------
    integer                            :: i
    !--------------------------------------------------------------

    do i=0,n
       gauss_lobatto(i)=-cos(pi*i/n)
    end do

  end function gauss_lobatto

  
  subroutine compute_expo_f_df(xmin,xmax,coef,h,dmin,f,df)
    implicit none 
    !--------------------------------------------------------------
    ! global variables
    !--------------------------------------------------------------
    real(8), intent(in)  :: xmin,xmax,h,dmin,coef
    real(8), intent(out) :: f,df
    !--------------------------------------------------------------
    ! local variables
    !--------------------------------------------------------------
    real(8)              :: L
    !--------------------------------------------------------------

    L=xmax-xmin
    
    if (coef>0) then
       ! increasing exp
       f =xmin+(exp(coef*h/L)-1)/(exp(coef)-1)*L-dmin-xmin
       df=-(exp(coef*(h/L))-1)*L*exp(coef)/(exp(coef)-1)**2
    else
       ! decreasing exp
       f =xmin+(exp(coef*h/L)-exp(coef))/(1-exp(coef))*L-xmax+dmin
       df=(h*exp(coef*h/L)/L-exp(coef))*L/(1-exp(coef)) &
            & + (exp(coef*h/L)-exp(coef))*L*exp(coef)/(1-exp(coef))**2
    end if
    
  end subroutine compute_expo_f_df


  subroutine compute_expo_point(xmin,xmax,coef,x,fx)
    implicit none 
    !--------------------------------------------------------------
    ! global variables
    !--------------------------------------------------------------
    real(8), intent(in)  :: xmin,xmax,coef,x
    real(8), intent(out) :: fx
    !--------------------------------------------------------------
    ! local variables
    !--------------------------------------------------------------
    real(8)              :: L
    !--------------------------------------------------------------

    L=xmax-xmin

    if (coef>0) then
       ! increasing exp
       fx=(exp(coef*(x-xmin)/L)-1)/(exp(coef)-1)*L+xmin
    else
       ! decreasing exp
       fx=(exp(coef*(x-xmin)/L)-exp(coef))/(1-exp(coef))*L+xmin
    end if

  end subroutine compute_expo_point



!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
end module Mod_Parameters
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
module mod_Constants
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  use mod_Parameters, only:pi

  implicit none

  real(8), parameter :: d1p2     =   1.d0/2.d0
  real(8), parameter :: d1p3     =   1.d0/3.d0
  real(8), parameter :: d2p3     =   2.d0/3.d0
  real(8), parameter :: d1p4     =   1.d0/4.d0
  real(8), parameter :: d1p5     =   1.d0/5.d0
  real(8), parameter :: d3p2     =   3.d0/2.d0
  real(8), parameter :: d3p4     =   3.d0/4.d0
  real(8), parameter :: d1p6     =   1.d0/6.d0
  real(8), parameter :: d1p7     =   1.d0/7.d0
  real(8), parameter :: d5p2     =   5.d0/2.d0
  real(8), parameter :: d5p4     =   5.d0/4.d0
  real(8), parameter :: d5p6     =   5.d0/6.d0
  real(8), parameter :: d1p6m    = - 1.d0/6.d0
  real(8), parameter :: d7p6     =   7.d0/6.d0
  real(8), parameter :: d7p6m    = - 7.d0/6.d0
  real(8), parameter :: d11p6    =   11.d0/6.d0
  real(8), parameter :: d13p12   =   13.d0/12.d0
  real(8), parameter :: d1p12    =   1.d0/12.d0
  real(8), parameter :: d1p18    =   1.d0/18.d0
  real(8), parameter :: d1p8     =   1.d0/8.d0
  real(8), parameter :: d3p8     =   3.d0/8.d0
  real(8), parameter :: d6p8     =   6.d0/8.d0
  real(8), parameter :: d1p10    =   0.1d0
  real(8), parameter :: d6p10    =   0.6d0
  real(8), parameter :: d3p10    =   0.3d0
  real(8), parameter :: d3p16    =   1._8/16
  real(8), parameter :: d9p160   =   9._8/160

  real(8), parameter :: eps_weno = 1.d-6
  real(8), parameter :: eps_zero = 1.d-10

  real(8), parameter :: zero     = 0.0d0
  real(8), parameter :: one      = 1.0d0
  real(8), parameter :: two      = 2.0d0
  real(8), parameter :: demi     = 0.5d0
  real(8), parameter :: infty    = 1e15_8

  real(8), parameter :: c13      = 13.0d0
  real(8), parameter :: c3       = 3.0d0
  real(8), parameter :: c4       = 4.0d0
  real(8), parameter :: c6       = 6.0d0

  real(8), parameter :: d4p3pi   = 4._8/3*pi

  !---------------------------------------------------------------------
  ! Generating a restart file
  !---------------------------------------------------------------------
  integer, parameter :: c_init   = 0
  integer, parameter :: c_write  = 1
  integer, parameter :: c_read   = 2

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
end module Mod_Constants
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

