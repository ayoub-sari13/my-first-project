!========================================================================
!**
!**   NAME       : Outputs.f90
!**
!**   AUTHOR     : Benoit Trouette
!**
!**   FUNCTION   : subroutines for files Outputs
!**
!**   DATES      : Version 1.0.0  : from : May, 2018
!**
!========================================================================

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
module mod_outputs
  use mod_mpi
  use mod_CoeffVS
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  implicit none
  
  !-------------------------------------------------------------------------------
  ! averaged quantities 
  !-------------------------------------------------------------------------------
  real(8), allocatable, dimension(:,:,:) :: mean_SijSji,mean_mu_sgs
  real(8), allocatable, dimension(:,:,:) :: mean_ubar,mean_vbar,mean_wbar
  real(8), allocatable, dimension(:,:,:) :: mean_ubar2,mean_vbar2,mean_wbar2
  real(8), allocatable, dimension(:,:,:) :: mean_k,mean_kbar,mean_epsilon
  !-------------------------------------------------------------------------------

contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  
  subroutine InitAvrgs
    !-------------------------------------------------------------------------------
    ! Modules
    !-------------------------------------------------------------------------------
    use mod_Parameters, only: dim,sx,ex,sy,ey,sz,ez  
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! alloc
    !-------------------------------------------------------------------------------
    allocate(mean_SijSji(sx:ex,sy:ey,sz:ez))
    allocate(mean_mu_sgs(sx:ex,sy:ey,sz:ez))
    allocate(mean_ubar(sx:ex,sy:ey,sz:ez))
    allocate(mean_vbar(sx:ex,sy:ey,sz:ez))
    if (dim==3) allocate(mean_wbar(sx:ex,sy:ey,sz:ez))
    allocate(mean_ubar2(sx:ex,sy:ey,sz:ez))
    allocate(mean_vbar2(sx:ex,sy:ey,sz:ez))
    if (dim==3) allocate(mean_wbar2(sx:ex,sy:ey,sz:ez))
    allocate(mean_k(sx:ex,sy:ey,sz:ez))
    allocate(mean_kbar(sx:ex,sy:ey,sz:ez))
    allocate(mean_epsilon(sx:ex,sy:ey,sz:ez))
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Init
    !-------------------------------------------------------------------------------
    mean_SijSji=0
    mean_mu_sgs=0
    mean_ubar=0
    mean_vbar=0
    if (dim==3) mean_wbar=0
    mean_ubar2=0
    mean_vbar2=0
    if (dim==3) mean_wbar2=0
    mean_k=0
    mean_kbar=0
    mean_epsilon=0
    !-------------------------------------------------------------------------------

  end subroutine InitAvrgs


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


  subroutine outputs_general(unit,time,u,v,w,rho)
    !-------------------------------------------------------------------------------
    ! Modules
    !-------------------------------------------------------------------------------
    use mod_Parameters, only: nproc,rank,dim,sx,ex,sy,ey,sz,ez,nx,ny,nz,&
         & dx,dy,dz,dxu2,dyv2,dzw2,                                     &
         & xmin,xmax,ymin,ymax,zmin,zmax
    use mod_Constants
    implicit none
    !----------------------------------------------------------------------------
    ! global variables
    !---------------------------------------------------------------------------
    integer, intent(in)                                :: unit
    real(8), intent(in)                                :: time
    real(8), allocatable, dimension(:,:,:), intent(in) :: u,v,w,rho
    !----------------------------------------------------------------------------
    ! local variables
    !----------------------------------------------------------------------------
    integer                                            :: i,j,k
    real(8)                                            :: varl1,varl2
    real(8)                                            :: Ek,mxb,mxf,myb,myf,mzb,mzf
    real(8)                                            :: gEk,gmxb,gmxf,gmyb,gmyf,gmzb,gmzf
    logical                                            :: once=.true.
    !----------------------------------------------------------------------------

    !----------------------------------------------------------------------------
    ! Initialisation
    !----------------------------------------------------------------------------
    Ek=0
    mxb=0;mxf=0
    myb=0;myf=0
    mzb=0;mzf=0
    !----------------------------------------------------------------------------
    if (once) then
       call InitCoeffVS
       once=.false.
    end if
    !----------------------------------------------------------------------------

    if (dim==2) then

       !----------------------------------------------------------------------------
       ! mean kinetic energy
       !----------------------------------------------------------------------------
       k=1
       do j=sy,ey
          do i=sx,ex
             varl1=( ((dxu2(i+1)*u(i,j,1)+dxu2(i)*u(i+1,j,1))/dx(i))**2 &
                  & +((dyv2(j+1)*v(i,j,1)+dyv2(j)*v(i,j+1,1))/dy(j))**2 )
             varl2=rho(i,j,1)
             Ek=Ek+d1p2*CoeffV(i,j,k)*varl2*varl1*dx(i)*dy(j)
          end do
       end do

       if (nproc>1) then
          call mpi_allreduce(Ek,gEk,1,mpi_double_precision,mpi_sum,comm3d,code)
          Ek=gEk
       end if

       Ek=Ek/(xmax-xmin)/(ymax-ymin)
       !----------------------------------------------------------------------------

       !----------------------------------------------------------------------------
       ! boundary mass fluxes 
       !----------------------------------------------------------------------------
       do j=sy,ey+1
          do i=sx,ex+1
             if (i==gsx) then
                !----------------------------------------------------------------------------
                ! left mass flux
                !----------------------------------------------------------------------------
                varl1=(dxu2(i+1)*u(i,j,1)+dxu2(i)*u(i+1,j,1))/dx(i)
                varl2=rho(i,j,1)
                mxb=mxb+2*coeffV(i,j,1)*varl1*varl2*dy(j)
                !----------------------------------------------------------------------------
             else if (i==gex) then 
                !----------------------------------------------------------------------------
                ! right mass flux
                !----------------------------------------------------------------------------
                varl1=(dxu2(i+1)*u(i,j,1)+dxu2(i)*u(i+1,j,1))/dx(i)
                varl2=rho(i,j,1)
                mxf=mxf+CoeffS(i,j,1)*varl1*varl2*dy(j)
                !----------------------------------------------------------------------------
             end if
          end do
       end do

       do j=sy,ey+1
          do i=sx,ex+1
             if (j==gsy) then 
                !----------------------------------------------------------------------------
                ! bottom mass flux
                !----------------------------------------------------------------------------
                varl1=(dyv2(j+1)*v(i,j,1)+dyv2(j)*v(i,j+1,1))/dy(j)
                varl2=rho(i,j,1)
                myb=myb+CoeffS(i,j,1)*varl1*varl2*dx(i)
                !----------------------------------------------------------------------------
             else if (j==gey) then 
                !----------------------------------------------------------------------------
                ! right mass flux
                !----------------------------------------------------------------------------
                varl1=(dyv2(j+1)*v(i,j,1)+dyv2(j)*v(i,j+1,1))/dy(j)
                varl2=rho(i,j,1)
                myf=myf+CoeffS(i,j,1)*varl1*varl2*dx(i)
                !----------------------------------------------------------------------------
             end if
          end do
       end do

    else

       !----------------------------------------------------------------------------
       ! kinetic energy
       !----------------------------------------------------------------------------
       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                varl1=( ((dxu2(i+1)*u(i,j,k)+dxu2(i)*u(i+1,j,k))/dx(i))**2 &
                     & +((dyv2(j+1)*v(i,j,k)+dyv2(j)*v(i,j+1,k))/dy(j))**2 &
                     & +((dzw2(k+1)*w(i,j,k)+dzw2(k)*w(i,j,k+1))/dz(k))**2 )
                varl2=rho(i,j,k)
                Ek=Ek+d1p2*CoeffV(i,j,k)*varl2*varl1*dx(i)*dy(j)*dz(k) 
             end do
          end do
       end do

       if (nproc>1) then
          call mpi_allreduce(Ek,gEk,1,mpi_double_precision,mpi_sum,comm3d,code)
          Ek=gEk
       end if

       Ek=Ek/(xmax-xmin)/(ymax-ymin)/(zmax-zmin)
       !----------------------------------------------------------------------------

       !----------------------------------------------------------------------------
       ! boundary mass fluxes 
       !----------------------------------------------------------------------------
       do k=sz,ez+1
          do j=sy,ey+1
             do i=sx,ex+1
                if (i==gsx) then 
                   !----------------------------------------------------------------------------
                   ! left mass flux
                   !----------------------------------------------------------------------------
                   varl1=(dxu2(i+1)*u(i,j,k)+dxu2(i)*u(i+1,j,k))/dx(i)
                   varl2=rho(i,j,k)
                   mxb=mxb+CoeffS(i,j,k)*varl1*varl2*dy(j)*dz(k)
                   !----------------------------------------------------------------------------
                elseif (i==gex) then 
                   !----------------------------------------------------------------------------
                   ! right mass flux
                   !----------------------------------------------------------------------------
                   varl1=(dxu2(i+1)*u(i,j,k)+dxu2(i)*u(i+1,j,k))/dx(i)
                   varl2=rho(i,j,k)
                   mxf=mxf+CoeffS(i,j,k)*varl1*varl2*dy(j)*dz(k)
                   !----------------------------------------------------------------------------
                end if
             end do
          end do
       end do

       do k=sz,ez+1
          do j=sy,ey+1
             do i=sx,ex+1
                if  (j==gsy) then 
                   !----------------------------------------------------------------------------
                   ! bottom mass flux
                   !----------------------------------------------------------------------------
                   varl1=(dyv2(j+1)*v(i,j,k)+dyv2(j)*v(i,j+1,k))/dy(j)
                   varl2=rho(i,j,k)
                   myb=myb+CoeffS(i,j,k)*varl1*varl2*dx(i)*dz(k)
                   !----------------------------------------------------------------------------
                else if (j==gey) then 
                   !----------------------------------------------------------------------------
                   ! top mass flux
                   !----------------------------------------------------------------------------
                   varl1=(dyv2(j+1)*v(i,j,k)+dyv2(j)*v(i,j+1,k))/dy(j)
                   varl2=rho(i,j,k)
                   myf=myf+CoeffS(i,j,k)*varl1*varl2*dx(i)*dz(k)
                   !----------------------------------------------------------------------------
                end if
             end do
          end do
       end do

       do k=sz,ez +1
          do j=sy,ey+1
             do i=sx,ex+1
                if (k==gsz) then 
                   !----------------------------------------------------------------------------
                   ! backward mass flux
                   !----------------------------------------------------------------------------
                   varl1=(dzw2(k+1)*w(i,j,k)+dzw2(k)*w(i,j,k+1))/dz(k)
                   varl2=rho(i,j,k)
                   mzb=mzb+CoeffS(i,j,k)*varl1*varl2*dx(i)*dy(j)
                   !----------------------------------------------------------------------------
                else if (k==gez) then 
                   !----------------------------------------------------------------------------
                   ! forward mass flux
                   !----------------------------------------------------------------------------
                   varl1=(dzw2(k+1)*w(i,j,k)+dzw2(k)*w(i,j,k+1))/dz(k)
                   varl2=rho(i,j,k)
                   mzf=mzf+CoeffS(i,j,k)*varl1*varl2*dx(i)*dy(j)
                   !----------------------------------------------------------------------------
                end if
             end do
          end do
       end do

    end if

    !----------------------------------------------------------------------------
    ! Ecritures
    !----------------------------------------------------------------------------

    if (nproc>1) then
       call mpi_allreduce(mxf,gmxf,1,mpi_double_precision,mpi_sum,comm3d,code)
       mxf=gmxf
       call mpi_allreduce(mxb,gmxb,1,mpi_double_precision,mpi_sum,comm3d,code)
       mxb=gmxb
       call mpi_allreduce(myf,gmyf,1,mpi_double_precision,mpi_sum,comm3d,code)
       myf=gmyf
       call mpi_allreduce(myb,gmyb,1,mpi_double_precision,mpi_sum,comm3d,code)
       myb=gmyb
       if (dim==3) then 
          call mpi_allreduce(mzf,gmzf,1,mpi_double_precision,mpi_sum,comm3d,code)
          mzf=gmzf
          call mpi_allreduce(mzb,gmzb,1,mpi_double_precision,mpi_sum,comm3d,code)
          mzb=gmzb
       end if
    end if

    if (mpi_chief) then 
       if (dim==2) then
          write(unit,'(999(1x,1pe13.6))') time,Ek,mxf-mxb+myf-myb,&
               & mxb,mxf,myb,myf
       else
          write(unit,'(999(1x,1pe13.6))') time,Ek,mxf-mxb+myf-myb+mzf-mzb,&
               & mxb,mxf,myb,myf,mzb,mzf
       end if
       flush(unit)
    end if
    !----------------------------------------------------------------------------


  end subroutine outputs_general


  subroutine outputs_general_energy(unit,time,u,v,w,rho,cp,tp,sltp)
    !-------------------------------------------------------------------------------
    ! Modules
    !-------------------------------------------------------------------------------
    use mod_Parameters, only: nproc,rank,dim,sx,ex,sy,ey,sz,ez,nx,ny,nz,&
         & dx,dy,dz,dxu2,dyv2,dzw2,                                     &
         & xmin,xmax,ymin,ymax,zmin,zmax
    use mod_Constants
    implicit none
    !----------------------------------------------------------------------------
    ! global variables
    !---------------------------------------------------------------------------
    integer, intent(in)                                :: unit
    real(8), intent(in)                                :: time
    real(8), allocatable, dimension(:,:,:), intent(in) :: u,v,w,rho,cp,tp,sltp
    !----------------------------------------------------------------------------
    ! local variables
    !----------------------------------------------------------------------------
    integer                                            :: i,j,k
    real(8)                                            :: varl1,varl2
    real(8)                                            :: intT,mxb,mxf,myb,myf,mzb,mzf
    real(8)                                            :: gintT,gmxb,gmxf,gmyb,gmyf,gmzb,gmzf
    logical                                            :: once=.true.
    !----------------------------------------------------------------------------

    !----------------------------------------------------------------------------
    ! Initialisation
    !----------------------------------------------------------------------------
    intT=0
    mxb=0;mxf=0
    myb=0;myf=0
    mzb=0;mzf=0
    !----------------------------------------------------------------------------
    if (once) then
       call InitCoeffVS
       once=.false.
    end if
    !----------------------------------------------------------------------------

    if (dim==2) then

       !----------------------------------------------------------------------------
       ! mean value in domain
       !----------------------------------------------------------------------------
       k=1
       if (allocated(sltp)) then
          do j=sy,ey
             do i=sx,ex
                varl1=tp(i,j,k)
                varl2=rho(i,j,k)*cp(i,j,k)
                if (sltp(i,j,k)/=0) intT=intT+CoeffV(i,j,k)*varl2*varl1*dx(i)*dy(j)
             end do
          end do
       else
          do j=sy,ey
             do i=sx,ex
                varl1=tp(i,j,k)
                varl2=rho(i,j,k)*cp(i,j,k)
                intT=intT+CoeffV(i,j,k)*varl2*varl1*dx(i)*dy(j)
             end do
          end do
       end if
       !----------------------------------------------------------------------------
       
       if (nproc>1) then
          call mpi_allreduce(intT,gintT,1,mpi_double_precision,mpi_sum,comm3d,code)
          intT=gintT
       end if

       intT=intT/(xmax-xmin)/(ymax-ymin)
       !----------------------------------------------------------------------------

       !----------------------------------------------------------------------------
       ! boundary fluxes 
       !----------------------------------------------------------------------------
       do j=sy,ey
          do i=sx,ex
             if (i==gsx) then
                !----------------------------------------------------------------------------
                ! left flux
                !----------------------------------------------------------------------------
                varl1=(dxu2(i+1)*u(i,j,1)+dxu2(i)*u(i+1,j,1))/dx(i)*tp(i,j,1)
                varl2=rho(i,j,1)*cp(i,j,1)
                mxb=mxb+2*coeffV(i,j,1)*varl1*varl2*dy(j)
                !----------------------------------------------------------------------------
             else if (i==gex) then 
                !----------------------------------------------------------------------------
                ! right flux
                !----------------------------------------------------------------------------
                varl1=(dxu2(i+1)*u(i,j,1)+dxu2(i)*u(i+1,j,1))/dx(i)*tp(i,j,1)
                varl2=rho(i,j,1)*cp(i,j,1)
                mxf=mxf+CoeffS(i,j,1)*varl1*varl2*dy(j)
                !----------------------------------------------------------------------------
             end if
          end do
       end do

       do j=sy,ey
          do i=sx,ex
             if (j==gsy) then 
                !----------------------------------------------------------------------------
                ! bottom flux
                !----------------------------------------------------------------------------
                varl1=(dyv2(j+1)*v(i,j,1)+dyv2(j)*v(i,j+1,1))/dy(j)*tp(i,j,1)
                varl2=rho(i,j,1)*cp(i,j,1)
                myb=myb+CoeffS(i,j,1)*varl1*varl2*dx(i)
                !----------------------------------------------------------------------------
             else if (j==gey) then 
                !----------------------------------------------------------------------------
                ! right flux
                !----------------------------------------------------------------------------
                varl1=(dyv2(j+1)*v(i,j,1)+dyv2(j)*v(i,j+1,1))/dy(j)*tp(i,j,1)
                varl2=rho(i,j,1)*cp(i,j,1)
                myf=myf+CoeffS(i,j,1)*varl1*varl2*dx(i)
                !----------------------------------------------------------------------------
             end if
          end do
       end do

    else

       !----------------------------------------------------------------------------
       ! mean value in domain
       !----------------------------------------------------------------------------
       if (allocated(sltp)) then
          do k=sz,ez
             do j=sy,ey
                do i=sx,ex
                   varl1=tp(i,j,k)
                   varl2=rho(i,j,k)*cp(i,j,k)
                   if (sltp(i,j,k)/=0) intT=intT+CoeffV(i,j,k)*varl2*varl1*dx(i)*dy(j)*dz(k) 
                end do
             end do
          end do
       else
          do k=sz,ez
             do j=sy,ey
                do i=sx,ex
                   varl1=tp(i,j,k)
                   varl2=rho(i,j,k)*cp(i,j,k)
                   intT=intT+CoeffV(i,j,k)*varl2*varl1*dx(i)*dy(j)*dz(k) 
                end do
             end do
          end do
       end if
       !----------------------------------------------------------------------------
       
       if (nproc>1) then
          call mpi_allreduce(intT,gintT,1,mpi_double_precision,mpi_sum,comm3d,code)
          intT=gintT
       end if

       intT=intT/(xmax-xmin)/(ymax-ymin)/(zmax-zmin)
       !----------------------------------------------------------------------------

       !----------------------------------------------------------------------------
       ! boundary fluxes 
       !----------------------------------------------------------------------------
       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                if (i==gsx) then 
                   !----------------------------------------------------------------------------
                   ! left flux
                   !----------------------------------------------------------------------------
                   varl1=(dxu2(i+1)*u(i,j,k)+dxu2(i)*u(i+1,j,k))/dx(i)*tp(i,j,k)
                   varl2=rho(i,j,k)*cp(i,j,k)
                   mxb=mxb+CoeffS(i,j,k)*varl1*varl2*dy(j)*dz(k)
                   !----------------------------------------------------------------------------
                elseif (i==gex) then 
                   !----------------------------------------------------------------------------
                   ! right flux
                   !----------------------------------------------------------------------------
                   varl1=(dxu2(i+1)*u(i,j,k)+dxu2(i)*u(i+1,j,k))/dx(i)*tp(i,j,k)
                   varl2=rho(i,j,k)*cp(i,j,k)
                   mxf=mxf+CoeffS(i,j,k)*varl1*varl2*dy(j)*dz(k)
                   !----------------------------------------------------------------------------
                end if
             end do
          end do
       end do

       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                if  (j==gsy) then 
                   !----------------------------------------------------------------------------
                   ! bottom flux
                   !----------------------------------------------------------------------------
                   varl1=(dyv2(j+1)*v(i,j,k)+dyv2(j)*v(i,j+1,k))/dy(j)*tp(i,j,k)
                   varl2=rho(i,j,k)*cp(i,j,k)
                   myb=myb+CoeffS(i,j,k)*varl1*varl2*dx(i)*dz(k)
                   !----------------------------------------------------------------------------
                else if (j==gey) then 
                   !----------------------------------------------------------------------------
                   ! top flux
                   !----------------------------------------------------------------------------
                   varl1=(dyv2(j+1)*v(i,j,k)+dyv2(j)*v(i,j+1,k))/dy(j)*tp(i,j,k)
                   varl2=rho(i,j,k)*cp(i,j,k)
                   myf=myf+CoeffS(i,j,k)*varl1*varl2*dx(i)*dz(k)
                   !----------------------------------------------------------------------------
                end if
             end do
          end do
       end do

       do k=sz,ez 
          do j=sy,ey
             do i=sx,ex
                if (k==gsz) then 
                   !----------------------------------------------------------------------------
                   ! backward flux
                   !----------------------------------------------------------------------------
                   varl1=(dzw2(k+1)*w(i,j,k)+dzw2(k)*w(i,j,k+1))/dz(k)*tp(i,j,k)
                   varl2=rho(i,j,k)*cp(i,j,k)
                   mzb=mzb+CoeffS(i,j,k)*varl1*varl2*dx(i)*dy(j)
                   !----------------------------------------------------------------------------
                else if (k==gez) then 
                   !----------------------------------------------------------------------------
                   ! forward flux
                   !----------------------------------------------------------------------------
                   varl1=(dzw2(k+1)*w(i,j,k)+dzw2(k)*w(i,j,k+1))/dz(k)*tp(i,j,k)
                   varl2=rho(i,j,k)*cp(i,j,k)
                   mzf=mzf+CoeffS(i,j,k)*varl1*varl2*dx(i)*dy(j)
                   !----------------------------------------------------------------------------
                end if
             end do
          end do
       end do

    end if

    !----------------------------------------------------------------------------
    ! Ecritures
    !----------------------------------------------------------------------------

    if (nproc>1) then
       call mpi_allreduce(mxf,gmxf,1,mpi_double_precision,mpi_sum,comm3d,code)
       mxf=gmxf
       call mpi_allreduce(mxb,gmxb,1,mpi_double_precision,mpi_sum,comm3d,code)
       mxb=gmxb
       call mpi_allreduce(myf,gmyf,1,mpi_double_precision,mpi_sum,comm3d,code)
       myf=gmyf
       call mpi_allreduce(myb,gmyb,1,mpi_double_precision,mpi_sum,comm3d,code)
       myb=gmyb
       if (dim==3) then 
          call mpi_allreduce(mzf,gmzf,1,mpi_double_precision,mpi_sum,comm3d,code)
          mzf=gmzf
          call mpi_allreduce(mzb,gmzb,1,mpi_double_precision,mpi_sum,comm3d,code)
          mzb=gmzb
       end if
    end if

    if (mpi_chief) then 
       if (dim==2) then
          write(unit,'(999(1x,1pe13.6))') time,intT,mxf-mxb+myf-myb,&
               & mxb,mxf,myb,myf
       else
          write(unit,'(999(1x,1pe13.6))') time,intT,mxf-mxb+myf-myb+mzf-mzb,&
               & mxb,mxf,myb,myf,mzb,mzf
       end if
       flush(unit)
    end if
    !----------------------------------------------------------------------------


  end subroutine outputs_general_energy


  subroutine outputs_cavity(unit,time,u,v,w,pres,tp)
    !----------------------------------------------------------------------------
    ! modules 
    !----------------------------------------------------------------------------
    use mod_Parameters, only: nproc,dim,        &
         & dx,dy,dz,dxu,                        &
         & sx,ex,sy,ey,sz,ez,gx,gy,gz,          &
         & xmin,xmax,ymin,ymax,zmin,zmax,       &
         & grid_x,grid_y,grid_z
    use mod_Constants, only: d1p2
    use mod_struct_thermophysics, only: fluids
    use mod_interpolation
    implicit none
    !----------------------------------------------------------------------------
    ! global variables
    !---------------------------------------------------------------------------
    integer, intent(in)                                :: unit
    real(8), intent(in)                                :: time
    real(8), allocatable, dimension(:,:,:), intent(in) :: u,v,w,pres,tp
    !----------------------------------------------------------------------------
    ! local variables
    !----------------------------------------------------------------------------
    integer                                            :: i,j,k
    real(8)                                            :: x,y,z,dTdx
    real(8), allocatable, dimension(:)                 :: tab_interp
    logical, save                                      :: once=.true.
    !----------------------------------------------------------------------------

    !----------------------------------------------------------------------------
    ! Initialization
    !----------------------------------------------------------------------------
    if (once) then
       call InitCoeffVS
       once=.false.
    end if
    allocate(tab_interp(dim+1))
    !----------------------------------------------------------------------------

    if (nproc==1) then 
       if (dim==2) then
          !---------------------------------------------------------------!
          !   2D Cavity Outputs                                           !
          !---------------------------------------------------------------!
          do i=sx,ex
             x=grid_x(i)
             y=d1p2*(ymax+ymin)
             call interpolation_uvw(time,u,v,w,(/x,y/),tab_interp(1:dim),2,dim)
             call interpolation_phi(time,pres,(/x,y/),tab_interp(dim+1),2,dim)
             write(unit,'(999(1x,1pe11.4))') x, tab_interp
          end do
          write(unit,*)
          write(unit,*)
          flush(unit)

          do j=sy,ey
             x=d1p2*(xmax+xmin)
             y=grid_y(j)
             call interpolation_uvw(time,u,v,w,(/x,y/),tab_interp(1:dim),2,dim)
             call interpolation_phi(time,pres,(/x,y/),tab_interp(dim+1),2,dim)
             write(unit+1,'(999(1x,1pe11.4))') y, tab_interp
          end do
          write(unit+1,*)
          write(unit+1,*)
          flush(unit+1)
          !---------------------------------------------------------------!
       else 
          !---------------------------------------------------------------!
          !   3D Cavity Outputs                                           !
          !---------------------------------------------------------------!
          do i=sx,ex
             x=grid_x(i)
             y=d1p2*(ymax+ymin)
             z=d1p2*(zmax+zmin)
             call interpolation_uvw(time,u,v,w,(/x,y,z/),tab_interp(1:dim),2,dim)
             call interpolation_phi(time,pres,(/x,y,z/),tab_interp(dim+1),2,dim)
             write(unit,'(999(1x,1pe11.4))') x, tab_interp
          end do
          write(unit,*)
          write(unit,*)
          flush(unit)

          do j=sy,ey
             x=d1p2*(xmax+xmin)
             y=grid_y(j)
             z=d1p2*(zmax+zmin)
             call interpolation_uvw(time,u,v,w,(/x,y,z/),tab_interp(1:dim),2,dim)
             call interpolation_phi(time,pres,(/x,y,z/),tab_interp(dim+1),2,dim)
             write(unit+1,'(999(1x,1pe11.4))') y, tab_interp
          end do
          write(unit+1,*)
          write(unit+1,*)
          flush(unit+1)

          do k=sz,ez
             x=d1p2*(xmax+xmin)
             y=d1p2*(ymax+ymin)
             z=grid_z(k)
             call interpolation_uvw(time,u,v,w,(/x,y,z/),tab_interp(1:dim),2,dim)
             call interpolation_phi(time,pres,(/x,y,z/),tab_interp(dim+1),2,dim)
             write(unit+2,'(999(1x,1pe11.4))') z, tab_interp
          end do
          write(unit+2,*)
          write(unit+2,*)
          flush(unit+2)
          !---------------------------------------------------------------!
       end if
    else
    end if

    !---------------------------------------------------------------!
    ! Nusselt computation
    !---------------------------------------------------------------!
    if (nproc==1) then
       if (allocated(tp)) then
          dTdx=0
          if (dim==2) then
             do j=sy,ey
                do i=sx,ex 
                   if (i==gsx) then
                      !---------------------------------------------------------------!
                      ! first order derivative computation
                      !---------------------------------------------------------------!
                      dTdx=dTdx+CoeffS(i,j,1)*(tp(i+1,j,1)-tp(i,j,1))/dxu(i+1)*dy(j)
                      write(unit+4,'(999(1x,1pe11.4))') grid_y(j),-2*CoeffV(i,j,1)*(tp(i+1,j,1)-tp(i,j,1))/dxu(i+1)
                      !---------------------------------------------------------------!
                   end if
                end do
             end do
          else
             do k=sz,ez
                do j=sy,ey
                   do i=sx,ex 
                      if (i==gsx) then
                         !---------------------------------------------------------------!
                         ! first order derivative computation
                         !---------------------------------------------------------------!
                         dTdx=dTdx+CoeffS(i,j,k)*(tp(i+1,j,k)-tp(i,j,k))/dxu(i+1)*dy(j)*dz(k)
                         !---------------------------------------------------------------!
                      end if
                   end do
                end do
             end do
          end if
          !--------------------------------------------------
          ! Nusselt number
          !--------------------------------------------------
          dTdx=-dTdx/(ymax-ymin)/(zmax-zmin)**(dim-2)
          write(unit+3,'(999(1x,1pe11.4))') time,dTdx
          !--------------------------------------------------
       end if
       !---------------------------------------------------------------!
    end if
    
  end subroutine outputs_cavity

  
  subroutine outputs_force_obstacle(unit,time,u,v,w,slvu,slvv,slvw)
    !----------------------------------------------------------------------------
    ! modules 
    !----------------------------------------------------------------------------
    use mod_Parameters, only: rank,dim, &
         & dx,dy,dz,dxu,dyv,dzw,        &
         & sxu,exu,syu,eyu,szu,ezu,     &
         & sxv,exv,syv,eyv,szv,ezv,     &
         & sxw,exw,syw,eyw,szw,ezw
    use mod_Constants, only: d1p2
    use mod_struct_thermophysics, only: fluids
    implicit none
    !----------------------------------------------------------------------------
    ! global variables
    !---------------------------------------------------------------------------
    integer, intent(in)                                :: unit
    real(8), intent(in)                                :: time
    real(8), allocatable, dimension(:,:,:), intent(in) :: u,v,w
    real(8), allocatable, dimension(:,:,:), intent(in) :: slvu,slvv,slvw
    !----------------------------------------------------------------------------
    ! local variables
    !----------------------------------------------------------------------------
    integer                                            :: i,j,k
    real(8)                                            :: var,var1,var2,var3
    !----------------------------------------------------------------------------


    var1=0
    var2=0
    var3=0
    !---------------------------------------------------------------
    ! F_x
    !---------------------------------------------------------------
    do k=szu,ezu 
       do j=syu,eyu
          do i=sxu,exu
             var1=var1+u(i,j,k)*slvu(i,j,k)*dxu(i)*dy(j)*dz(k)
          end do
       end do
    end do
    !---------------------------------------------------------------
    call mpi_allreduce(var1,var,1,mpi_double_precision,mpi_sum, &
         & comm3d,code)
    var1=var 
    !---------------------------------------------------------------
    ! F_y
    !---------------------------------------------------------------
    do k=szv,ezv
       do j=syv,eyv
          do i=sxv,exv
             var2=var2+v(i,j,k)*slvv(i,j,k)*dx(i)*dyv(j)*dz(k)
          end do
       end do
    end do
    !---------------------------------------------------------------
    call mpi_allreduce(var2,var,1,mpi_double_precision,mpi_sum, &
         & comm3d,code)
    var2=var 
    !---------------------------------------------------------------
    ! F_z
    !---------------------------------------------------------------
    if (dim==3) then
       do k=szw,ezw
          do j=syw,eyw
             do i=sxw,exw
                var3=var3+w(i,j,k)*slvw(i,j,k)*dx(i)*dy(j)*dzw(k)
             end do
          end do
       end do
       !---------------------------------------------------------------
       call mpi_allreduce(var3,var,1,mpi_double_precision,mpi_sum, &
            & comm3d,code)
       var3=var 
       !---------------------------------------------------------------
    end if
    !---------------------------------------------------------------

    !---------------------------------------------------------------
    ! writing outputs
    !---------------------------------------------------------------
    if (rank==0) then
       write(unit,'(999(1x,1pe13.6))') time,var1,var2,var3
       flush(unit)
    end if
    !---------------------------------------------------------------
    
  end subroutine outputs_force_obstacle
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  subroutine outputs_moving_cylinder(unit,time,u,v,w,pres)
    !----------------------------------------------------------------------------
    ! modules 
    !----------------------------------------------------------------------------
    use mod_Parameters, only: dx,dy,dz,sx,ex,sy,ey,sz,ez,gx,gy,gz,dim,&
         & xmin,xmax,ymin,ymax,zmin,zmax,grid_y
    use mod_struct_thermophysics, only: fluids
    use mod_interpolation
    implicit none
    !----------------------------------------------------------------------------
    ! global variables
    !---------------------------------------------------------------------------
    integer, intent(in)                                :: unit
    real(8), intent(in)                                :: time
    real(8), allocatable, dimension(:,:,:), intent(in) :: u,v,w,pres
    !----------------------------------------------------------------------------
    ! local variables
    !----------------------------------------------------------------------------
    integer                                            :: i,j,k
    real(8)                                            :: x,y,z
    real(8), allocatable, dimension(:)                 :: tab_interp
    logical, save                                      :: once=.false.
   !----------------------------------------------------------------------------
    
    !----------------------------------------------------------------------------
    ! Initialization
    !----------------------------------------------------------------------------
    if (once) then
       call InitCoeffVS
       once=.false.
    end if
    allocate(tab_interp(dim))
    !----------------------------------------------------------------------------

    if (dim==2) then
       !---------------------------------------------------------------!
       !   Slice over lines x=-0.6D, 0, 0.6D, 1.2D (D=1)
       !---------------------------------------------------------------!
       write(unit+0,*) "#time=", time 
       write(unit+1,*) "#time=", time 
       write(unit+2,*) "#time=", time 
       write(unit+3,*) "#time=", time 
       do j=sy,ey
          y=grid_y(j)
          !---------------------------------------------------------------!
          ! x=-0.6
          !---------------------------------------------------------------!
          x=-0.6d0
          call interpolation_uvw(time,u,v,w,(/x,y,z/),tab_interp(1:dim),2,dim)
          write(unit+0,'(999(1x,1pe11.4))') y, tab_interp
          !---------------------------------------------------------------!
          ! x=0
          !---------------------------------------------------------------!
          x=0
          call interpolation_uvw(time,u,v,w,(/x,y,z/),tab_interp(1:dim),2,dim)
          write(unit+1,'(999(1x,1pe11.4))') y, tab_interp
          !---------------------------------------------------------------!
          ! x=0.6
          !---------------------------------------------------------------!
          x=0.6d0
          call interpolation_uvw(time,u,v,w,(/x,y,z/),tab_interp(1:dim),2,dim)
          write(unit+2,'(999(1x,1pe11.4))') y, tab_interp
          !---------------------------------------------------------------!
          ! x=1.2
          !---------------------------------------------------------------!
          x=1.2d0
          call interpolation_uvw(time,u,v,w,(/x,y,z/),tab_interp(1:dim),2,dim)
          write(unit+3,'(999(1x,1pe11.4))') y, tab_interp
          !---------------------------------------------------------------!
       end do
       write(unit+0,*)
       write(unit+0,*)
       flush(unit+0)
       write(unit+1,*)
       write(unit+1,*)
       flush(unit+1)
       write(unit+2,*)
       write(unit+2,*)
       flush(unit+2)
       write(unit+3,*)
       write(unit+3,*)
       flush(unit+3)
       !---------------------------------------------------------------!
    end if
    
  end subroutine outputs_moving_cylinder

    subroutine outputs_inv_phase(unit,time,u,v,w,cou,rho)
    !----------------------------------------------------------------------------
    ! modules 
    !----------------------------------------------------------------------------
    use mod_Parameters, only: dx,dy,dz,sx,ex,sy,ey,sz,ez,gx,gy,gz,&
         & grav_x,grav_y,grav_z,dim,                              &
         & xmin,xmax,ymin,ymax,zmin,zmax,                         &
         & dx2,dy2,dz2
    use mod_Constants, only: d1p2,d1p4,d3p4,d1p8
    use mod_struct_thermophysics, only: fluids
    implicit none
    !----------------------------------------------------------------------------
    ! global variables
    !---------------------------------------------------------------------------
    integer, intent(in)                                     :: unit
    real(8), intent(in)                                     :: time
    real(8), allocatable, dimension(:,:,:), intent(in)      :: u,v,w,cou,rho
    !----------------------------------------------------------------------------
    ! local variables
    !----------------------------------------------------------------------------
    integer                                                 :: i,j,k
    integer, dimension(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz) :: Cb
    real(8)                                                 :: coul,varl,Vol2
    real(8)                                                 :: V2,R2,Ek1,Ek2,Ep1,Ep2,Er1,Er2,IArea,Di1,Di2
    real(8)                                                 :: gR2,gEp1,gEp2,gEk1,gEk2,gEr1,gEr2,gDi1,gDi2, &
                                                               gIArea,gVol2 ! SV NT
    !----------------------------------------------------------------------------
    
    R2=0
    Ek1=0
    Ek2=0
    Ep1=0
    Ep2=0
    Er1=0
    Er2=0
    IArea=0
    Di1=0 ! SV NT
    Di2=0 ! SV NT
    Cb=0

    if (dim==2) then

       do j=sy+1,ey
          do i=sx+1,ex
             coul=d1p4*(cou(i,j,1)+cou(i-1,j,1)+cou(i,j-1,1)+cou(i-1,j-1,1))
             !----------------------------------------------------------------------------
             ! R2 
             !----------------------------------------------------------------------------
             V2=d1p4*(xmax-xmin)*(ymax-ymin)
             if (j*dy(j)-dy2(j)>d3p4*(ymax-ymin)) then
                R2=R2+(1-coul)*dx(i)*dy(j)/V2
             end if
             !----------------------------------------------------------------------------

             !----------------------------------------------------------------------------
             ! potential energies Ep_1 and Ep_2
             !----------------------------------------------------------------------------
             varl=abs(grav_y)*(j*dy(j)-dy2(j))
             Ep1=Ep1+(1-coul)*fluids(1)%rho*varl*dx(i)*dy(j) 
             Ep2=Ep2+coul*fluids(2)%rho*varl*dx(i)*dy(j)
             !----------------------------------------------------------------------------

             !----------------------------------------------------------------------------
             ! kinetic energies Ek_1 and Ek_2
             !----------------------------------------------------------------------------
             varl=d1p2*(((u(i,j-1,1)+u(i,j,1))/2)**2+((v(i-1,j,1)+v(i,j,1))/2)**2)
             Ek1=Ek1+(1-coul)*fluids(1)%rho*varl*dx(i)*dy(j) 
             Ek2=Ek2+coul*fluids(2)%rho*varl*dx(i)*dy(j)
             !----------------------------------------------------------------------------
             
             !----------------------------------------------------------------------------
             ! Enstrophies Er_1 and Er_2
             !----------------------------------------------------------------------------
             varl=d1p2*(((v(i,j,1)-v(i-1,j,1))/dx(i))-((u(i,j,1)-u(i,j-1,1))/dy(j)))**2
             Er1=Er1+(1-coul)*varl*dx(i)*dy(j) 
             Er2=Er2+coul*varl*dx(i)*dy(j)
             !----------------------------------------------------------------------------
             
          end do
       end do

       !----------------------------------------------------------------------------
       ! Interfacial area
       !---------------------------------------------------------------------------- ! SV NT
       do j=sy-1,ey+1
          do i=sx-1,ex+1
             if (cou(i,j,1)>d1p2) then
                Cb(i,j,1)=1
             else
                Cb(i,j,1)=0
             end if
          enddo
       enddo
       
       do j=sy,ey
          do i=sx,ex
             if (0.5*(Cb(i,j,1)+Cb(i,j-1,1)) /= 0.5*(Cb(i-1,j,1)+Cb(i-1,j-1,1))) then 
                IArea=IArea+dx(i)
             else if (0.5*(Cb(i,j,1)+Cb(i-1,j,1)) /= 0.5*(Cb(i,j-1,1)+Cb(i-1,j-1,1))) then 
                IArea=IArea+dy(j)
             end if
          enddo
       enddo
       !----------------------------------------------------------------------------

       !----------------------------------------------------------------------------
       ! Volume light fluid
       !----------------------------------------------------------------------------
       call InitCoeffVS
       Vol2=0
       do j=sy,ey
          do i=sx,ex
             Vol2=Vol2+cou(i,j,1)*dx(i)*dy(j)*CoeffV(i,j,1)
          enddo
       enddo
       !----------------------------------------------------------------------------

    elseif (dim==3) then
       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                if (i>gsx.and.j>gsy.and.k>gsz) then
                   coul=d1p8*(cou(i,j,k)+cou(i-1,j,k)+cou(i,j-1,k)+cou(i-1,j-1,k) &
                        +cou(i,j,k-1)+cou(i-1,j,k-1)+cou(i,j-1,k-1)+cou(i-1,j-1,k-1) )              
                   !----------------------------------------------------------------------------
                   ! R2 
                   !----------------------------------------------------------------------------
                   V2=d1p8*(xmax-xmin)*(ymax-ymin)*(zmax-zmin)
                   if (k*dz(k)-dz2(k)>7.D0/8.D0*(zmax-zmin)) then
                      R2=R2+coul*dx(i)*dy(j)*dz(k)/V2
                   end if
                   !----------------------------------------------------------------------------
                   
                   !----------------------------------------------------------------------------
                   ! potential energies Ep_1 and Ep_2
                   !----------------------------------------------------------------------------
                   varl=abs(grav_z)*(k*dz(k)-dz2(k))
                   Ep1=Ep1+(1-coul)*fluids(1)%rho*varl*dx(i)*dy(j)*dz(k) 
                   Ep2=Ep2+coul*fluids(2)%rho*varl*dx(i)*dy(j)*dz(k)
                   !----------------------------------------------------------------------------
                   
                   !----------------------------------------------------------------------------
                   ! kinetic energies Ek_1 and Ek_2
                   !----------------------------------------------------------------------------
                   varl=d1p2*(((u(i,j,k)+u(i,j-1,k)+u(i,j,k-1)+u(i,j-1,k-1))/4)**2+((v(i,j,k)+v(i-1,j,k)+v(i,j,k-1)+v(i-1,j,k-1))/4)**2 &
                        +((w(i,j,k)+w(i-1,j,k)+w(i,j-1,k)+w(i-1,j-1,k))/4)**2)
                   Ek1=Ek1+(1-coul)*fluids(1)%rho*varl*dx(i)*dy(j)*dz(k)
                   Ek2=Ek2+coul*fluids(2)%rho*varl*dx(i)*dy(j)*dz(k)
                   !----------------------------------------------------------------------------
                   
                   !----------------------------------------------------------------------------
                   ! Enstrophies Er_1 and Er_2
                   !----------------------------------------------------------------------------
                   varl=d1p2*( ((((v(i,j,k)-v(i-1,j,k))/dx(i))-((u(i,j,k)-u(i,j-1,k))/dy(j)) &
                        +((v(i,j,k-1)-v(i-1,j,k-1))/dx(i))-((u(i,j,k-1)-u(i,j-1,k-1))/dy(j)))/2 )**2 &
                        +((((w(i,j,k)-w(i,j-1,k))/dy(j))-((v(i,j,k)-v(i,j,k-1))/dz(k)) &
                        +((w(i-1,j,k)-w(i-1,j-1,k))/dy(j))-((v(i-1,j,k)-v(i-1,j,k-1))/dz(k)))/2 )**2 &
                        +((((u(i,j,k)-u(i,j,k-1))/dz(k))-((w(i,j,k)-w(i-1,j,k))/dx(i)) &
                        +((u(i,j-1,k)-u(i,j-1,k-1))/dz(k))-((w(i,j-1,k)-w(i-1,j-1,k))/dx(i)))/2 )**2 )
                   Er1=Er1+(1-coul)*varl*dx(i)*dy(j)*dz(k)
                   Er2=Er2+coul*varl*dx(i)*dy(j)*dz(k)
                   !----------------------------------------------------------------------------
                   ! Dissipation Di_1 and Di_2 SV NT
                   !----------------------------------------------------------------------------
                   varl=d1p2*(2*( &
                        (((u(i,j,k)+u(i,j-1,k)+u(i,j,k-1)+u(i,j-1,k-1)+u(i+1,j,k)+u(i+1,j-1,k)+u(i+1,j,k-1)+u(i+1,j-1,k-1))/8 &
                        -(u(i,j,k)+u(i,j-1,k)+u(i,j,k-1)+u(i,j-1,k-1)+u(i-1,j,k)+u(i-1,j-1,k)+u(i-1,j,k-1)+u(i-1,j-1,k-1))/8 )/dx(i))**2 &
                        +(((v(i,j,k)+v(i-1,j,k)+v(i,j,k-1)+v(i-1,j,k-1)+v(i,j+1,k)+v(i-1,j+1,k)+v(i,j+1,k-1)+v(i-1,j+1,k-1))/8 &
                        -(v(i,j,k)+v(i-1,j,k)+v(i,j,k-1)+v(i-1,j,k-1)+v(i,j-1,k)+v(i-1,j-1,k)+v(i,j-1,k-1)+v(i-1,j-1,k-1))/8 )/dy(j))**2 &
                        +(((w(i,j,k)+w(i-1,j,k)+w(i,j-1,k)+w(i-1,j-1,k)+w(i,j,k+1)+w(i-1,j,k+1)+w(i,j-1,k+1)+w(i-1,j-1,k+1))/8 &
                        -(w(i,j,k)+w(i-1,j,k)+w(i,j-1,k)+w(i-1,j-1,k)+w(i,j,k-1)+w(i-1,j,k-1)+w(i,j-1,k-1)+w(i-1,j-1,k-1))/8 )/dz(k))**2 ) &
                        +((((v(i,j,k)-v(i-1,j,k))/dx(i))+((u(i,j,k)-u(i,j-1,k))/dy(j)) &
                        +((v(i,j,k-1)-v(i-1,j,k-1))/dx(i))+((u(i,j,k-1)-u(i,j-1,k-1))/dy(j)))/2 )**2 &
                        +((((w(i,j,k)-w(i,j-1,k))/dy(j))+((v(i,j,k)-v(i,j,k-1))/dz(k)) &
                        +((w(i-1,j,k)-w(i-1,j-1,k))/dy(j))+((v(i-1,j,k)-v(i-1,j,k-1))/dz(k)))/2 )**2 &
                        +((((u(i,j,k)-u(i,j,k-1))/dz(k))+((w(i,j,k)-w(i-1,j,k))/dx(i)) &
                        +((u(i,j-1,k)-u(i,j-1,k-1))/dz(k))+((w(i,j-1,k)-w(i-1,j-1,k))/dx(i)))/2 )**2 )
                   Di1=Di1+(1-coul)*fluids(1)%mu*varl*dx(i)*dy(j)*dz(k)
                   Di2=Di2+coul*fluids(2)%mu*varl*dx(i)*dy(j)*dz(k)
                   !----------------------------------------------------------------------------
                endif
                
             end do
          end do
       enddo
       !----------------------------------------------------------------------------
       ! Interfacial area
       !----------------------------------------------------------------------------
       do k=sz-1,ez+1
          do j=sy-1,ey+1
             do i=sx-1,ex+1
                if (cou(i,j,k)>d1p2) then
                   Cb(i,j,k)=1
                else
                   Cb(i,j,k)=0
                end if
             enddo
          enddo
       enddo
       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                if (i>gsx.and.j>gsy.and.k>gsz) then
                   if ( 0.25*(Cb(i,j,k)+Cb(i,j-1,k)+Cb(i,j,k-1)+Cb(i,j-1,k-1)) /= 0.25*(Cb(i-1,j,k)+Cb(i-1,j-1,k)+Cb(i-1,j,k-1)+Cb(i-1,j-1,k-1)) ) then 
                      IArea=IArea+dy(j)*dz(k)
                   else if ( 0.25*(Cb(i,j,k)+Cb(i-1,j,k)+Cb(i,j,k-1)+Cb(i-1,j,k-1)) /= 0.25*(Cb(i,j-1,k)+Cb(i-1,j-1,k)+Cb(i,j-1,k-1)+Cb(i-1,j-1,k-1)) ) then 
                      IArea=IArea+dx(i)*dz(k)
                   else if ( 0.25*(Cb(i,j,k)+Cb(i-1,j,k)+Cb(i,j-1,k)+Cb(i-1,j-1,k)) /= 0.25*(Cb(i,j,k-1)+Cb(i-1,j,k-1)+Cb(i,j-1,k-1)+Cb(i-1,j-1,k-1)) ) then 
                      IArea=IArea+dx(i)*dy(j)
                   end if
                endif
             enddo
          enddo
       enddo
       !----------------------------------------------------------------------------
       ! Volume light fluid
       !----------------------------------------------------------------------------
       call InitCoeffVS
       Vol2=0
       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                Vol2=Vol2+cou(i,j,k)*CoeffV(i,j,k)*dx(i)*dy(j)*dz(k)
             enddo
          enddo
       enddo
       !----------------------------------------------------------------------------
    end if

    !----------------------------------------------------------------------------
    ! Ecritures SV NT
    !----------------------------------------------------------------------------
    if (nproc>1) then
       call mpi_allreduce(R2,gR2,1,mpi_double_precision,mpi_sum,comm3d,code)
       R2=gR2
       call mpi_allreduce(Ep1,gEp1,1,mpi_double_precision,mpi_sum,comm3d,code)
       Ep1=gEp1
       call mpi_allreduce(Ep2,gEp2,1,mpi_double_precision,mpi_sum,comm3d,code)
       Ep2=gEp2
       call mpi_allreduce(Ek1,gEk1,1,mpi_double_precision,mpi_sum,comm3d,code)
       Ek1=gEk1
       call mpi_allreduce(Ek2,gEk2,1,mpi_double_precision,mpi_sum,comm3d,code)
       Ek2=gEk2
       call mpi_allreduce(Er1,gEr1,1,mpi_double_precision,mpi_sum,comm3d,code)
       Er1=gEr1
       call mpi_allreduce(Er2,gEr2,1,mpi_double_precision,mpi_sum,comm3d,code)
       Er2=gEr2
       if (dim==3) then
          call mpi_allreduce(Di1,gDi1,1,mpi_double_precision,mpi_sum,comm3d,code)
          Di1=gDi1
          call mpi_allreduce(Di2,gDi2,1,mpi_double_precision,mpi_sum,comm3d,code)
          Di2=gDi2
       endif
       call mpi_allreduce(IArea,gIArea,1,mpi_double_precision,mpi_sum,comm3d,code)
       IArea=gIArea
       call mpi_allreduce(Vol2,gVol2,1,mpi_double_precision,mpi_sum,comm3d,code)
       Vol2=gVol2  
    end if


    if (dim==3) then 
       call compute_interfacial_area(cou,IArea)
    end if
    

    
    if (nproc==1) then
       if (dim==2) then
          write(unit,'(999(1x,1pe13.6))') time,R2,Ek1,Ek2,Ep1,Ep2,Er1,Er2,IArea,Vol2
       else
          write(unit,'(999(1x,1pe13.6))') time,R2,Ek1,Ek2,Ep1,Ep2,Er1,Er2,Di1,Di2,IArea,Vol2
       endif
    flush(unit)
    elseif (nproc>1) then
       if (rank==0) then
          if (dim==2) then
             write(unit,'(999(1x,1pe13.6))') time,R2,Ek1,Ek2,Ep1,Ep2,Er1,Er2,IArea,Vol2
          else
             write(unit,'(999(1x,1pe13.6))') time,R2,Ek1,Ek2,Ep1,Ep2,Er1,Er2,Di1,Di2,IArea,Vol2
          end if
       flush(unit)
      end if
    end if
    !---------------------------------------------------------------------------
    
  end subroutine outputs_inv_phase

  
  subroutine outputs_drop_impact(unit,time,u,v,w,cou,rho)
    !----------------------------------------------------------------------------
    ! modules 
    !----------------------------------------------------------------------------
    use mod_Parameters, only: dx,dy,dz,sx,ex,sy,ey,sz,ez,gx,gy,gz,&
         & dim,grid_y,grid_z,dzw,                                 &
         & xmin,xmax,ymin,ymax,zmin,zmax,                         &
         & dx2,dy2,dz2,                                           &
         & VOF_init_icase
    use mod_Constants, only: d1p2,d1p4,d3p4,d1p8
    use mod_struct_thermophysics, only: fluids
    implicit none
    !----------------------------------------------------------------------------
    ! global variables
    !---------------------------------------------------------------------------
    integer, intent(in)                                :: unit
    real(8), intent(in)                                :: time
    real(8), allocatable, dimension(:,:,:), intent(in) :: u,v,w,cou,rho
    !----------------------------------------------------------------------------
    ! local variables
    !----------------------------------------------------------------------------
    integer                                            :: i,j,k
    real(8)                                            :: x,y,z
    real(8)                                            :: tmp
    !----------------------------------------------------------------------------
    
    
    select case (VOF_init_icase)
    case(12,120:129)
       z=zmax
       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                if (cou(i,j,k)<=0.5d0.and.cou(i,j,k-1)>0.5d0) then
                   x=(0.5d0-cou(i,j,k))/(cou(i,j,k-1)-cou(i,j,k))
                   x=grid_z(k)-x*dzw(k)
                   if (x<z) z=x
                end if
             end do
          end do
       end do
       CALL MPI_ALLREDUCE(z,tmp,1,MPI_DOUBLE_PRECISION,MPI_MIN,COMM3D,CODE);z=tmp
       if (rank==0) then
          write(unit,*) time,z
          flush(unit)
       end if
    end select
    
    if (dim==2)then 
       select case (VOF_init_icase)
       case(17)
          y=ymax
          do k=sz,ez
             do j=sy,ey
                do i=sx,ex

                   if (cou(i,j,k)==0) then
                      if (grid_y(j)<y) y=grid_y(j)
                   end if

                end do
             end do
          end do
          CALL MPI_ALLREDUCE(y,tmp,1,MPI_DOUBLE_PRECISION,MPI_MIN,COMM3D,CODE);y=tmp
          if (rank==0) then
             write(unit,*) time,y
             flush(unit)
          end if
       end select
    endif

  end subroutine outputs_drop_impact
  
  
  subroutine compute_interfacial_area(cou,area)
    !----------------------------------------------------------------------------
    ! modules 
    !----------------------------------------------------------------------------
    use mod_Parameters, only: dim,sx,ex,sy,ey,sz,ez,dx,dy,dz
    use mod_Constants, only: d1p2,d1p4,d3p4,d1p8
    use mod_struct_thermophysics, only: fluids
    use mod_VOF_Cons
    use mod_VOF_Tools
    implicit none
    !----------------------------------------------------------------------------
    ! global variables
    !---------------------------------------------------------------------------
    real(8), intent(out)                               :: area
    real(8), allocatable, dimension(:,:,:), intent(in) :: cou 
    !----------------------------------------------------------------------------
    ! local variables
    !----------------------------------------------------------------------------
    real(8) :: mxyz(3), stencil3x3(-1:1,-1:1,-1:1)
    real(8) :: tmp
    integer :: i,j,k
    integer :: i0,j0,k0
    !----------------------------------------------------------------------------

    area=0
    do k=sz,ez
       do j=sy,ey
          do i=sx,ex

             if ( cou(i,j,k) > 0 .and. cou(i,j,k) < 1 ) then
                do i0=-1,1
                   do j0=-1,1
                      do k0=-1,1
                         stencil3x3(i0,j0,k0)=cou(i+i0,j+j0,k+k0)
                      enddo
                   enddo
                enddo

                call mycs(stencil3x3,mxyz)

                if ( mxyz(1)/=0 .or. mxyz(2)/=0 .or. mxyz(3)/=0 ) then
                   call AREA3D_vof(mxyz,cou(i,j,k),tmp)
                   area=area+tmp*dx(i)*dy(j)   !!!!! WARNING CONSTANT STEP !!!!!
                endif

             end if

          end do
       end do
    end do


    call mpi_allreduce(area,tmp,1,mpi_double_precision,mpi_sum,comm3d,code)
    area=tmp

  end subroutine compute_interfacial_area



!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


  subroutine factor_Q(u,v,w,Q)
    !-------------------------------------------------------------------------------
    ! Modules
    !-------------------------------------------------------------------------------
    use mod_Parameters, only: dim,sx,ex,sy,ey,sz,ez,nx,ny,nz
    use mod_Constants
    use mod_deriv_xyz
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), allocatable, dimension(:,:,:), intent(in)    :: u,v,w
    real(8), allocatable, dimension(:,:,:), intent(inout) :: Q
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                               :: i,j,k,i1,j1,k1
    real(8), dimension(3,3)                               :: deriv
    real(8), allocatable, dimension(:,:,:)                :: dudx,dudy,dudz,dudyijk,dudzijk
    real(8), allocatable, dimension(:,:,:)                :: dvdx,dvdy,dvdz,dvdxijk,dvdzijk
    real(8), allocatable, dimension(:,:,:)                :: dwdx,dwdy,dwdz,dwdxijk,dwdyijk    
    !-------------------------------------------------------------------------------

    allocate(dudx(sx-1:ex+1,sy-1:ey+1,sz-1:ez+1))
    allocate(dudy(sx-1:ex+1,sy-1:ey+1,sz-1:ez+1))
    if (dim==3) allocate(dudz(sx-1:ex+1,sy-1:ey+1,sz-1:ez+1))
    allocate(dudyijk(sx-1:ex+1,sy-1:ey+1,sz-1:ez+1))
    if (dim==3) allocate(dudzijk(sx-1:ex+1,sy-1:ey+1,sz-1:ez+1))

    allocate(dvdx(sx-1:ex+1,sy-1:ey+1,sz-1:ez+1))
    allocate(dvdy(sx-1:ex+1,sy-1:ey+1,sz-1:ez+1))
    if (dim==3) allocate(dvdz(sx-1:ex+1,sy-1:ey+1,sz-1:ez+1))
    allocate(dvdxijk(sx-1:ex+1,sy-1:ey+1,sz-1:ez+1))
    if (dim==3) allocate(dvdzijk(sx-1:ex+1,sy-1:ey+1,sz-1:ez+1))

    if (dim==3) then 
       allocate(dwdx(sx-1:ex+1,sy-1:ey+1,sz-1:ez+1))
       allocate(dwdy(sx-1:ex+1,sy-1:ey+1,sz-1:ez+1))
       allocate(dwdz(sx-1:ex+1,sy-1:ey+1,sz-1:ez+1))
       allocate(dwdxijk(sx-1:ex+1,sy-1:ey+1,sz-1:ez+1))
       allocate(dwdyijk(sx-1:ex+1,sy-1:ey+1,sz-1:ez+1))
    end if

    call deriv_xyz(u,v,w,   &
         & dudx,dudy,dudz,  &
         & dvdx,dvdy,dvdz,  &
         & dwdx,dwdy,dwdz,  &
         & dudyijk,dudzijk, &
         & dvdxijk,dvdzijk, &
         & dwdxijk,dwdyijk)

    !------------------------------------
    ! Q factor 
    !------------------------------------

    deriv=0

    do k=sz,ez
       do j=sy,ey
          do i=sx,ex

             deriv(1,1) = dudx(i,j,k)*dudx(i,j,k)
             deriv(1,2) = dudyijk(i,j,k)*dvdxijk(i,j,k)

             deriv(2,1) = dvdxijk(i,j,k)*dudyijk(i,j,k)
             deriv(2,2) = dvdy(i,j,k)*dvdy(i,j,k)

             if (dim==3) then
                deriv(1,3) = dudzijk(i,j,k)*dwdxijk(i,j,k)
                deriv(2,3) = dvdzijk(i,j,k)*dwdyijk(i,j,k)

                deriv(3,1) = dwdxijk(i,j,k)*dudzijk(i,j,k)
                deriv(3,2) = dwdyijk(i,j,k)*dvdzijk(i,j,k)
                deriv(3,3) = dwdz(i,j,k)*dwdz(i,j,k)
             end if

             if (dim==3) then
                Q(i,j,k) = -d1p2*(                          &
                     & + deriv(1,1)+deriv(1,2)+deriv(1,3)   &
                     & + deriv(2,1)+deriv(2,2)+deriv(2,3)   &
                     & + deriv(3,1)+deriv(3,2)+deriv(3,3) )
             else
                Q(i,j,k) = -d1p2*(               &
                     & + deriv(1,1)+deriv(1,2)   &
                     & + deriv(2,1)+deriv(2,2) )
             end if

          end do
       end do
    end do

  end subroutine factor_Q


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  subroutine SkewFlat(u,v,w,S,F)
    !-------------------------------------------------------------------------------
    ! Modules
    !-------------------------------------------------------------------------------
    use mod_Parameters, only: dim,sx,ex,sy,ey,sz,ez,nx,ny,nz,dx,dy,dz,xmin,xmax,ymin,ymax,zmin,zmax
    use mod_Constants
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), allocatable, dimension(:,:,:), intent(in)   :: u,v,w
    real(8), intent(inout)                               :: S,F
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                              :: i,j,k,i1,j1,k1
    real(8), dimension(3,3)                              :: tensD
    real(8), dimension(sx-1:ex+1,sy-1:ey+1,sz-1:ez+1)    :: dudx,dvdy,dwdz
    real(8)                                              :: duidxi2
    real(8)                                              :: vol
    real(8)                                              :: sums,S1s,S2s,S3s
    real(8)                                              :: sumf,S1f,S2f,S3f
    logical                                              :: once=.true.
    real(8)                                              :: gsums,gsumf,gvol,gduidxi2
    !-------------------------------------------------------------------------------

    if (once) then
       call InitCoeffVS
       once=.false.
    end if

    k1=1
    if (dim==2) k1=0

    do k=sz-k1,ez+k1
       do j=sy-1,ey+1
          do i=sx-1,ex+1

             dudx(i,j,k)=(u(i+1,j,k)-u(i,j,k))/dx(i)
             dvdy(i,j,k)=(v(i,j+1,k)-v(i,j,k))/dy(j)
             if (dim==3) then 
                dwdz(i,j,k)=(w(i,j,k+1)-w(i,j,k))/dz(k)
             end if

          end do
       end do
    end do

    
    !**************************************
    ! Average of dui/dxi
    !**************************************
    
    ! du/dx
    
    sums = 0
    sumf = 0
    duidxi2 = 0
    vol = 0
    do k = sz,ez
       do j = sy,ey
          do i = sx,ex

             sums = sums + CoeffV(i,j,k)*dudx(i,j,k)**3 
             sumf = sumf + CoeffV(i,j,k)*dudx(i,j,k)**4 
             duidxi2 = duidxi2 + CoeffV(i,j,k)*dudx(i,j,k)**2
             vol = vol + CoeffV(i,j,k)

          end do
       end do
    end do

    if (nproc>1) then
       call mpi_allreduce(sums,gsums,1,mpi_double_precision,mpi_sum,comm3d,code)
       call mpi_allreduce(sumf,gsumf,1,mpi_double_precision,mpi_sum,comm3d,code)
       call mpi_allreduce(duidxi2,gduidxi2,1,mpi_double_precision,mpi_sum,comm3d,code)
       call mpi_allreduce(vol,gvol,1,mpi_double_precision,mpi_sum,comm3d,code)
    end if

    sums = gsums/gvol
    sumf = gsumf/gvol
    duidxi2 = gduidxi2/gvol

    S1s = (-1._8)**3 * sums/duidxi2**(1.5_8)
    S1f = (-1._8)**4 * sumf/duidxi2**2

    ! dv/dy 

    sums = 0
    sumf = 0
    duidxi2 = 0
    vol = 0
    do k = sz,ez
       do j = sy,ey
          do i = sx,ex

             sums = sums + CoeffV(i,j,k)*dvdy(i,j,k)**3
             sumf = sumf + CoeffV(i,j,k)*dvdy(i,j,k)**4
             duidxi2 = duidxi2 + CoeffV(i,j,k)*dvdy(i,j,k)**2
             vol = vol + CoeffV(i,j,k)

          end do
       end do
    end do

    if (nproc>1) then
       call mpi_allreduce(sums,gsums,1,mpi_double_precision,mpi_sum,comm3d,code)
       call mpi_allreduce(sumf,gsumf,1,mpi_double_precision,mpi_sum,comm3d,code)
       call mpi_allreduce(duidxi2,gduidxi2,1,mpi_double_precision,mpi_sum,comm3d,code)
       call mpi_allreduce(vol,gvol,1,mpi_double_precision,mpi_sum,comm3d,code)
    end if

    sums = gsums/gvol
    sumf = gsumf/gvol
    duidxi2 = gduidxi2/gvol

    S2s = (-1._8)**3 * sums/duidxi2**(1.5_8)
    S2f = (-1._8)**4 * sumf/duidxi2**2

    ! dw/dz

    S3s = 0
    S3f = 0

    if (dim==3) then

       sums = 0
       sumf = 0
       duidxi2 = 0
       vol = 0
       do k = sz,ez
          do j = sy,ey
             do i = sx,ex

                sums = sums + CoeffV(i,j,k)*dwdz(i,j,k)**3
                sumf = sumf + CoeffV(i,j,k)*dwdz(i,j,k)**4 
                duidxi2 = duidxi2 + CoeffV(i,j,k)*dwdz(i,j,k)**2
                vol = vol + CoeffV(i,j,k)

             end do
          end do
       end do

       if (nproc>1) then
          call mpi_allreduce(sums,gsums,1,mpi_double_precision,mpi_sum,comm3d,code)
          call mpi_allreduce(sumf,gsumf,1,mpi_double_precision,mpi_sum,comm3d,code)
          call mpi_allreduce(duidxi2,gduidxi2,1,mpi_double_precision,mpi_sum,comm3d,code)
          call mpi_allreduce(vol,gvol,1,mpi_double_precision,mpi_sum,comm3d,code)
       end if

       sums = gsums/gvol
       sumf = gsumf/gvol
       duidxi2 = gduidxi2/gvol


       S3s = (-1._8)**3 * sums/duidxi2**(1.5_8)
       S3f = (-1._8)**4 * sumf/duidxi2**2

    end if

    S = (S1s + S2s + S3s) / 3
    F = (S1f + S2f + S3f) / 3

  end subroutine SkewFlat

  
  subroutine prepa_outputs_turbulence(c,u,v,w,            &
       & tp,rho,mu_sgs,m_u,m_v,m_w,m_mu_sgs,m_k,m_SijSji, &
       & um,vm,wm)
    !----------------------------------------------------------------------------
    ! modules 
    !----------------------------------------------------------------------------
    use mod_Parameters, only: dx,dy,dz,sx,ex,sy,ey,sz,ez,restart_EQ
    use mod_Constants, only: pi,d1p2,d1p3,d3p4,d2p3,d3p2
    use mod_interpolation
    use Mod_deriv_xyz
    implicit none
    !----------------------------------------------------------------------------
    ! Global variables
    !---------------------------------------------------------------------------
    integer, intent(inout)                                :: c
    real(8), allocatable, dimension(:,:,:), intent(in)    :: u,v,w,tp,rho,mu_sgs
    real(8), allocatable, dimension(:,:,:), intent(inout) :: m_u,m_v,m_w
    real(8), allocatable, dimension(:,:,:), intent(inout) :: um,vm,wm
    real(8), allocatable, dimension(:,:,:), intent(inout) :: m_mu_sgs,m_k,m_SijSji
    !----------------------------------------------------------------------------
    ! Local variables
    !----------------------------------------------------------------------------
    integer                                               :: i,j,k
    real(8)                                               :: x,y,z
    real(8)                                               :: tmp1,tmp2,tmp3
    real(8), allocatable, dimension(:,:,:)                :: SijSji
    logical                                               :: once=.true.
    !----------------------------------------------------------------------------
    
    
    !----------------------------------------------------------------------------
    ! Initialization
    !----------------------------------------------------------------------------
    if (once) then
       call InitAvrgs
       call InitCoeffVS
       !---------------------------------------------------------------------------
       if (.not.allocated(m_u)) allocate(m_u(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
       if (.not.allocated(m_v)) allocate(m_v(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
       if (.not.allocated(m_w)) allocate(m_w(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
       if (.not.allocated(m_mu_sgs)) allocate(m_mu_sgs(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
       if (.not.allocated(m_k)) allocate(m_k(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
       if (.not.allocated(m_SijSji)) allocate(m_SijSji(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
       !---------------------------------------------------------------------------
       once=.false.
    end if
    !----------------------------------------------------------------------------

    !----------------------------------------------------------------------------
    ! reset stats 
    !----------------------------------------------------------------------------
    if (c<0) then
       c=0
       mean_SijSji=0
       mean_mu_sgs=0
       mean_epsilon=0
       mean_ubar=0
       mean_vbar=0
       if (dim==3) mean_wbar=0
       mean_ubar2=0
       mean_vbar2=0
       if (dim==3) mean_wbar2=0
       mean_k=0
       mean_kbar=0
       mean_epsilon=0
    end if
    !----------------------------------------------------------------------------
    
    
    !----------------------------------------------------------------------------
    ! Accumulation
    !----------------------------------------------------------------------------
    c=c+1

    !----------------------------------------------------------------------------
    ! compute mean solved velocities
    !         mean square solved velocites
    !         mean solved kinetic turbulent energy
    !----------------------------------------------------------------------------
    if (dim==2) then
       k=1
       do j=sy,ey
          do i=sx,ex
             tmp1=(dxu2(i+1)*u(i,j,k)+dxu2(i)*u(i+1,j,k))/dx(i)
             tmp2=(dyv2(j+1)*v(i,j,k)+dyv2(j)*v(i,j+1,k))/dy(j)
             mean_ubar (i,j,k)=((c-1)* mean_ubar(i,j,k)+tmp1)/c
             mean_vbar (i,j,k)=((c-1)* mean_vbar(i,j,k)+tmp2)/c
             mean_ubar2(i,j,k)=((c-1)*mean_ubar2(i,j,k)+tmp1**2)/c
             mean_vbar2(i,j,k)=((c-1)*mean_vbar2(i,j,k)+tmp2**2)/c
             mean_kbar (i,j,k)=((c-1)* mean_kbar(i,j,k)+d1p2*(  &
                  & (mean_ubar2(i,j,k)-mean_ubar(i,j,k)**2      &
                  & +mean_vbar2(i,j,k)-mean_vbar(i,j,k)**2)))/c
          end do
       end do
    else
       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                tmp1=(dxu2(i+1)*u(i,j,k)+dxu2(i)*u(i+1,j,k))/dx(i)
                tmp2=(dyv2(j+1)*v(i,j,k)+dyv2(j)*v(i,j+1,k))/dy(j)
                tmp3=(dzw2(k+1)*w(i,j,k)+dzw2(k)*w(i,j,k+1))/dz(k)
                mean_ubar(i,j,k)=((c-1)*mean_ubar(i,j,k)+tmp1)/c
                mean_vbar(i,j,k)=((c-1)*mean_vbar(i,j,k)+tmp2)/c
                mean_wbar(i,j,k)=((c-1)*mean_wbar(i,j,k)+tmp3)/c
                mean_ubar2(i,j,k)=((c-1)*mean_ubar2(i,j,k)+tmp1**2)/c
                mean_vbar2(i,j,k)=((c-1)*mean_vbar2(i,j,k)+tmp2**2)/c
                mean_wbar2(i,j,k)=((c-1)*mean_wbar2(i,j,k)+tmp3**2)/c
                mean_kbar(i,j,k)=((c-1)*mean_kbar(i,j,k)+d1p2*(    &
                     & (mean_ubar2(i,j,k)-mean_ubar(i,j,k)**2      &
                     & +mean_vbar2(i,j,k)-mean_vbar(i,j,k)**2      &
                     & +mean_wbar2(i,j,k)-mean_wbar(i,j,k)**2)))/c
             end do
          end do
       end do
    end if
    !----------------------------------------------------------------------------
    ! mean velocities on velocity grids
    !----------------------------------------------------------------------------
    um=((c-1)*um+u)/c
    vm=((c-1)*vm+v)/c
    if (dim==3) then
       wm=((c-1)*wm+w)/c
    end if
    !----------------------------------------------------------------------------

    !----------------------------------------------------------------------------
    ! compute mean epsilon^(2/3)
    !----------------------------------------------------------------------------
    allocate(SijSji(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
    call compute_SijSji(u,v,w,SijSji)
    do k = sz,ez
       do j = sy,ey
          do i = sx,ex
             mean_epsilon(i,j,k)=((c-1)*mean_epsilon(i,j,k)+2*mu_sgs(i,j,k)/rho(i,j,k)*SijSji(i,j,k))/c
          end do
       end do
    end do
    !----------------------------------------------------------------------------
    ! compute mean SijSji
    !----------------------------------------------------------------------------
    mean_SijSji(sx:ex,sy:ey,sz:ez)=((c-1)*mean_SijSji(sx:ex,sy:ey,sz:ez)+SijSji(sx:ex,sy:ey,sz:ez))/c
    !----------------------------------------------------------------------------

    !----------------------------------------------------------------------------
    ! compute mean sgs viscosity 
    !----------------------------------------------------------------------------
    do k=sz,ez
       do j=sy,ey
          do i=sx,ex
             mean_mu_sgs(i,j,k)=((c-1)*mean_mu_sgs(i,j,k)+mu_sgs(i,j,k))/c
          end do
       end do
    end do
    !----------------------------------------------------------------------------


    !----------------------------------------------------------------------------
    ! compute mean kinetic turbulent energy
    !          3   (  \Delta \epsilon  )^2/3
    ! k_sgs =  - C ( ------------------)
    !          2   (         pi        )
    !
    ! with        C = 3/2 (Kolmogorov constant)
    !      \epsilon = 2 mu_sgs SijSji
    !      \Delta   = (dx*dy*dz)**(1/3)
    !----------------------------------------------------------------------------
    tmp3=d3p2  ! C 
    do k=sz,ez
       do j=sy,ey
          do i=sx,ex
             tmp2=(dx(i)*dy(j)*dz(k)**(dim-2))**(1d0/dim)
             mean_k(i,j,k)=mean_kbar(i,j,k)+d3p2*tmp3*(tmp2/pi*mean_epsilon(i,j,k))**d2p3
          end do
       end do
    end do
    !----------------------------------------------------------------------------

    !----------------------------------------------------------------------------
    ! outputs variables
    !----------------------------------------------------------------------------
    m_u(sx:ex,sy:ey,sz:ez) = mean_ubar(sx:ex,sy:ey,sz:ez)
    m_v(sx:ex,sy:ey,sz:ez) = mean_vbar(sx:ex,sy:ey,sz:ez)
    if (dim==3) m_w(sx:ex,sy:ey,sz:ez) = mean_wbar(sx:ex,sy:ey,sz:ez)
    m_mu_sgs(sx:ex,sy:ey,sz:ez) = mean_mu_sgs(sx:ex,sy:ey,sz:ez)
    m_k(sx:ex,sy:ey,sz:ez)      = mean_k(sx:ex,sy:ey,sz:ez)
    m_SijSji(sx:ex,sy:ey,sz:ez) = mean_SijSji(sx:ex,sy:ey,sz:ez)
    !----------------------------------------------------------------------------

  end subroutine prepa_outputs_turbulence

  subroutine prepa_outputs_mean_age_air(time,dt,tp,tp0,int_tp)
    !----------------------------------------------------------------------------
    ! modules 
    !----------------------------------------------------------------------------
    use mod_Constants, only: d1p2
    implicit none
    !----------------------------------------------------------------------------
    ! global variables
    !---------------------------------------------------------------------------
    real(8)                                               :: time,dt
    real(8), allocatable, dimension(:,:,:), intent(in)    :: tp,tp0
    real(8), allocatable, dimension(:,:,:), intent(inout) :: int_tp
    !----------------------------------------------------------------------------
    ! local variables
    !----------------------------------------------------------------------------
    logical                                               :: once=.true.
    !----------------------------------------------------------------------------

    !----------------------------------------------------------------------------
    ! Init
    !----------------------------------------------------------------------------
    if (once) then 
       !----------------------------------------------------------------------------
       if (.not.allocated(int_tp)) then
          allocate(int_tp(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
          int_tp=0
       end if
       !----------------------------------------------------------------------------
       once=.false.
    end if
    !----------------------------------------------------------------------------
    
    !----------------------------------------------------------------------------
    ! int tp (mid point integral)
    !----------------------------------------------------------------------------
    int_tp(sx:ex,sy:ey,sz:ez)=int_tp(sx:ex,sy:ey,sz:ez) &
         & +dt*d1p2*(tp(sx:ex,sy:ey,sz:ez)+tp0(sx:ex,sy:ey,sz:ez))
    !----------------------------------------------------------------------------

  end subroutine prepa_outputs_mean_age_air

  subroutine outputs_interp_probes(icase,time,sca,u,v,w,nProbes,xyzProbe)
    !----------------------------------------------------------------------------
    ! modules 
    !----------------------------------------------------------------------------
    use mod_Parameters, only: dim,sx,ex,sy,ey,sz,ez,grid_x,grid_y,grid_z
    use mod_interpolation
    use mod_mpi
    implicit none
    !----------------------------------------------------------------------------
    ! global variables
    !---------------------------------------------------------------------------
    integer, intent(in)                                :: nProbes,icase
    real(8), intent(in)                                :: time
    real(8), allocatable, dimension(:,:,:), intent(in) :: sca,u,v,w
    real(8), dimension(:,:), intent(in)                :: xyzProbe
    !----------------------------------------------------------------------------
    ! local variables
    !----------------------------------------------------------------------------
    integer                                            :: n
    real(8)                                            :: scai
    real(8), dimension(dim)                            :: veli
    logical, save                                      :: once1=.true.
    logical, save                                      :: once2=.true.
    logical, save                                      :: once3=.true.
    !----------------------------------------------------------------------------

    !----------------------------------------------------------------------------
    ! find the proc which contains the probe
    !----------------------------------------------------------------------------
    if (dim==2) then
       do n=1,nProbes
          if (   xyzProbe(n,1)>=grid_x(sx).and.xyzProbe(n,1)<grid_x(ex+1).and. &
               & xyzProbe(n,2)>=grid_y(sy).and.xyzProbe(n,2)<grid_y(ey+1) ) then

             select case(icase)
             case(1) ! sca
                !----------------------------------------------------------------------------
                ! interpolation
                !----------------------------------------------------------------------------
                call interpolation_phi(time,sca,xyzProbe(n,:),scai,1,dim)
                !----------------------------------------------------------------------------
                ! write file 
                !----------------------------------------------------------------------------
                call write_sca_probes(rank+200000,n,time,xyzProbe(n,:),scai,once1)
                !----------------------------------------------------------------------------
             case(2) ! uvw
                !----------------------------------------------------------------------------
                ! interpolation
                !----------------------------------------------------------------------------
                call interpolation_uvw(time,u,v,w,xyzProbe(n,:),veli,1,dim)
                !----------------------------------------------------------------------------
                ! write file 
                !----------------------------------------------------------------------------
                call write_uvw_probes(rank+300000,n,time,xyzProbe(n,:),veli,once2)
                !----------------------------------------------------------------------------
             case(3) ! pres
                !----------------------------------------------------------------------------
                ! interpolation
                !----------------------------------------------------------------------------
                call interpolation_phi(time,sca,xyzProbe(n,:),scai,1,dim)
                !----------------------------------------------------------------------------
                ! write file 
                !----------------------------------------------------------------------------
                call write_p_probes(rank+400000,n,time,xyzProbe(n,:),scai,once3)
                !----------------------------------------------------------------------------
             end select
          end if
       end do
    else 
       do n=1,nProbes
          if (   xyzProbe(n,1)>=grid_x(sx).and.xyzProbe(n,1)<grid_x(ex+1).and. &
               & xyzProbe(n,2)>=grid_y(sy).and.xyzProbe(n,2)<grid_y(ey+1).and. &
               & xyzProbe(n,3)>=grid_z(sz).and.xyzProbe(n,3)<grid_z(ez+1) ) then

             select case(icase)
             case(1) ! sca
                !----------------------------------------------------------------------------
                ! interpolation
                !----------------------------------------------------------------------------
                call interpolation_phi(time,sca,xyzProbe(n,:),scai,1,dim)
                !----------------------------------------------------------------------------
                ! write file 
                !----------------------------------------------------------------------------
                call write_sca_probes(rank+200000,n,time,xyzProbe(n,:),scai,once1)
                !----------------------------------------------------------------------------
             case(2) ! uvw
                !----------------------------------------------------------------------------
                ! interpolation
                !----------------------------------------------------------------------------
                call interpolation_uvw(time,u,v,w,xyzProbe(n,:),veli,1,dim)
                !----------------------------------------------------------------------------
                ! write file 
                !----------------------------------------------------------------------------
                call write_uvw_probes(rank+300000,n,time,xyzProbe(n,:),veli,once2)
                !----------------------------------------------------------------------------
             case(3) ! pres
                !----------------------------------------------------------------------------
                ! interpolation
                !----------------------------------------------------------------------------
                call interpolation_phi(time,sca,xyzProbe(n,:),scai,1,dim)
                !----------------------------------------------------------------------------
                ! write file 
                !----------------------------------------------------------------------------
                call write_p_probes(rank+400000,n,time,xyzProbe(n,:),scai,once3)
                !----------------------------------------------------------------------------
             end select
          end if
       end do
    end if
    !----------------------------------------------------------------------------
    select case(icase) 
    case(1) ! sca
       once1=.false.
    case(2)
       once2=.false.
    case(3)
       once3=.false.
    end select
    !----------------------------------------------------------------------------
    
  end subroutine outputs_interp_probes

  subroutine write_p_probes(unit,p,time,xyz,val,once)
    !----------------------------------------------------------------------------
    ! modules 
    !----------------------------------------------------------------------------
    implicit none
    !----------------------------------------------------------------------------
    ! global variables
    !---------------------------------------------------------------------------
    integer, intent(in)               :: unit,p
    real(8), intent(in)               :: time,val
    real(8), dimension(:), intent(in) :: xyz
    logical, intent(in)               :: once
    !----------------------------------------------------------------------------
    ! local variables
    !----------------------------------------------------------------------------
    character(200)                    :: name,filename
    !----------------------------------------------------------------------------

    write(name,'(i3.3)') p
    filename="p_probe_"//trim(adjustl(name))//".dat"
    if (once) then 
       open(unit,FILE=filename,STATUS="replace")
    else
       open(unit,FILE=filename, status="old", position="append", action="write")
    end if

    if (once) write(unit,'("# (x,y,z)=",999(1x,1pe13.6))') xyz
    write(unit,'(999(1x,1pe13.6))') time,val

    close(unit)

  end subroutine write_p_probes

  subroutine write_sca_probes(unit,p,time,xyz,val,once)
    !----------------------------------------------------------------------------
    ! modules 
    !----------------------------------------------------------------------------
    implicit none
    !----------------------------------------------------------------------------
    ! global variables
    !---------------------------------------------------------------------------
    integer, intent(in)               :: unit,p
    real(8), intent(in)               :: time,val
    real(8), dimension(:), intent(in) :: xyz
    logical, intent(in)               :: once
    !----------------------------------------------------------------------------
    ! local variables
    !----------------------------------------------------------------------------
    character(200)                    :: name,filename
    !----------------------------------------------------------------------------

    write(name,'(i3.3)') p
    filename="sca_probe_"//trim(adjustl(name))//".dat"
    if (once) then 
       open(unit,FILE=filename,STATUS="replace")
    else
       open(unit,FILE=filename, status="old", position="append", action="write")
    end if

    if (once) write(unit,'("# (x,y,z)=",999(1x,1pe13.6))') xyz
    write(unit,'(999(1x,1pe13.6))') time,val

    close(unit)

  end subroutine write_sca_probes

  subroutine write_uvw_probes(unit,p,time,xyz,vec,once)
    !----------------------------------------------------------------------------
    ! modules 
    !----------------------------------------------------------------------------
    implicit none
    !----------------------------------------------------------------------------
    ! global variables
    !---------------------------------------------------------------------------
    integer, intent(in)               :: unit,p
    real(8), intent(in)               :: time
    real(8), dimension(:), intent(in) :: xyz
    real(8), dimension(:), intent(in) :: vec
    logical, intent(in)               :: once
    !----------------------------------------------------------------------------
    ! local variables
    !----------------------------------------------------------------------------
    character(200)                    :: name,filename
    !----------------------------------------------------------------------------

    write(name,'(i3.3)') p
    filename="uvw_probe_"//trim(adjustl(name))//".dat"
    if (once) then 
       open(unit,FILE=filename,STATUS="replace")
    else
       open(unit,FILE=filename, status="old", position="append", action="write")
    end if
    
    if (once) write(unit,'("# (x,y,z)=",999(1x,1pe13.6))') xyz
    write(unit,'(999(1x,1pe13.6))') time,vec

    close(unit)

  end subroutine write_uvw_probes

end module mod_outputs
