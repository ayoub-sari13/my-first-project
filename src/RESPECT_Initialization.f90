!===============================================================================
module Bib_VOFLag_RESPECT_Initialization
  !===============================================================================
  contains
  !-----------------------------------------------------------------------------  
  !> @author amine chadil
  ! 
  !> @brief cette routine appelle les routines de preparation du Module RESPECT
  !
  !> @param[out]   vl     : la structure contenant toutes les informations
  !! relatives aux particules.
  !> @param u,v,w         : velocity field components
  !> @param pres          : pressure
  !> @param[in]  penu, penv, penw : 
  !> @param[in]  uin, vin, win    : 
  !> @param[out] rho      : masse volumiques dans le maillage de pression
  !> @param[out] rovu     : masse volumiques dans le maillage de la premiere composante de la vitesse
  !> @param[out] rovv     : masse volumiques dans le maillage de la deuxieme composante de la vitesse
  !> @param[out] rovw     : masse volumiques dans le maillage de la troisieme composante de la vitesse
  !> @param[out] vie      : viscosite d'elongation stockee dans le maillage de pression 
  !> @param[out] vir      : viscosite de rotation stockee dans le maillage de viscosite 
  !> @param[out] vis      : viscosite de cisaillement stockee dans le maillage de viscosite 
  !> @param      cou      : la fonction couleur pour les viscosite calculees dans les points
  !! de pression.
  !-----------------------------------------------------------------------------
  subroutine RESPECT_Initialization(vl,u,v,w,Ru0,Rv0,Rw0,u1,v1,w1,pres,pres0,tp,&
                                    tp0,tp1,penu,penv,penw,uin,vin,win,rho,rovu,&
                                    rovv,rovw,vie,vir,vis,cou,cou_vis,cou_vts,  &
                                    couin0,cou_visin,cou_vtsin,xit,slvu,slvv,   &
                                    slvw,smvu,smvv,smvw,slvuin,slvvin,slvwin,   &
                                    smvuin,smvvin,smvwin,diff_pre,diff_u_mean,  &
                                    diff_v_mean,diff_w_mean)
    !===============================================================================
    !modules
    !===============================================================================
    !-------------------------------------------------------------------------------
    !Modules de definition des structures et variables => src/mod/module_...f90
    !-------------------------------------------------------------------------------
    use Module_VOFLag_ParticlesDataStructure
    use mod_Parameters,                       only : deeptracking,EN_activate,dim
    !-------------------------------------------------------------------------------
    !Modules de subroutines de la librairie RESPECT => src/Bib_VOFLag_...f90
    !-------------------------------------------------------------------------------
    use Bib_VOFLag_ParticlesDataStructure_Impression_Initialization
    use Bib_VOFLag_Particle_Collisions_InitializeData
    use Bib_VOFLag_ParticlesDataStructure_Initialize
    use Bib_VOFLag_ComputationTime_Initialisation
    use Bib_VOFLag_caracteristiques_thermophys
    use Bib_VOFLag_SubDomain_Initialisations
    use Bib_VOFLag_EulerStat_Initialization
    use Bib_VOFLag_Valid_OutputFileCreation
    use Bib_VOFLag_Particle_PhaseFunction
    use Bib_VOFLag_Init_Temperature
    use Bib_VOFLag_WallCreaction
    use Bib_VOFLag_RespectAlloc
    use Bib_VOFLag_BC_Velocity
    use Bib_VOFLag_Init_Flow
    !-------------------------------------------------------------------------------
    !===============================================================================
    !declarations des variables
    !===============================================================================
    implicit none
    !-------------------------------------------------------------------------------
    !variables globales
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:,:), allocatable, intent(inout) :: cou_vis,cou_vts,vir, &
                                                               vis,penu,penv,penw,  &
                                                               cou_visin,cou_vtsin
    real(8), dimension(:,:,:)  , allocatable, intent(inout) :: smvu,smvv,smvw
    real(8), dimension(:,:,:)  , allocatable, intent(inout) :: slvu,slvv,slvw
    real(8), dimension(:,:,:),   allocatable, intent(inout) :: slvuin,slvvin,slvwin
    real(8), dimension(:,:,:),   allocatable, intent(inout) :: smvuin,smvvin,smvwin
    real(8), dimension(:,:,:),   allocatable, intent(inout) :: cou,couin0,rho,rovu, &
                                                             & rovv,rovw,vie,u,v,w, &
                                                             & Ru0,Rv0,Rw0,u1,v1,w1,&
                                                             & pres,pres0,uin,vin,  &
                                                             & win,tp,tp0,tp1,xit,  &
                                                             & diff_pre,diff_u_mean,&
                                                             & diff_v_mean,diff_w_mean
    type(struct_vof_lag),                     intent(inout) :: vl
    !-------------------------------------------------------------------------------
    !variables locales 
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'entree RESPECT_Initialization'
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! preparation des domaines des procs 
    !-------------------------------------------------------------------------------
    call SubDomain_Initialisations

    !-------------------------------------------------------------------------------
    ! initialisation de la structure vl contenant les infos relatives aux particules
    !-------------------------------------------------------------------------------
    call ParticlesDataStructure_Initialize(cou,vl)

    !-------------------------------------------------------------------------------
    ! allocation des variables respect
    !-------------------------------------------------------------------------------
    call RespectAlloc(Ru0,Rv0,Rw0,cou,cou_vis,cou_vts,couin0,cou_visin,cou_vtsin,rovu,&
                      rovv,rovw,vie,vir,vis,xit,slvu,slvv,slvw,smvu,smvv,smvw,slvuin, &
                      slvvin,slvwin,smvuin,smvvin,smvwin,diff_pre,diff_u_mean,        &
                      diff_v_mean,diff_w_mean)

    !-------------------------------------------------------------------------------
    if (Wall_Creaction) call WallCreaction(cou,cou_vis,cou_vts,couin0,cou_visin,   &
                                           cou_vtsin,slvu,slvv,slvw,smvu,smvv,smvw,&
                                           slvuin,slvvin,slvwin,smvuin,smvvin,     &
                                           smvwin,WallPos)

    call Particle_PhaseFunction(cou,cou_vis,cou_vts,couin0,cou_visin,cou_vtsin,vl)
    !-------------------------------------------------------------------------------
    ! calcul des caracteristiques thermophysiques apres calcul de la fonction couleur
    !-------------------------------------------------------------------------------
    call caracteristiques_thermophys(rho,rovu,rovv,rovw,vie,vir,vis,cou,cou_vis,cou_vts,dim)
    !-------------------------------------------------------------------------------
    ! initialisation des donnees d'impression de la structure vl 
    !-------------------------------------------------------------------------------
    call ParticlesDataStructure_Impression_Initialization(vl)

    !-------------------------------------------------------------------------------
    ! initialisation des parametres d'impression du temps de calcul
    !-------------------------------------------------------------------------------
    call ComputationTime_Initialisation(vl)

    !-------------------------------------------------------------------------------
    ! initialisation des fichiers des stats euleriennes 
    !-------------------------------------------------------------------------------
    call EulerStat_Initialization()

    !-------------------------------------------------------------------------------
    ! initialisation d'ecoulements
    !-------------------------------------------------------------------------------
    call Init_Flow(u,v,w,Ru0,Rv0,Rw0,u1,v1,w1,pres,pres0,vl)

    !-------------------------------------------------------------------------------
    ! initialisation de temperature
    !-------------------------------------------------------------------------------
    if (EN_activate) call Init_Temperature(tp,tp0,tp1,vl)

    !-------------------------------------------------------------------------------
    ! penalisation des condtions aux limites pour des écoulements specifiques
    !-------------------------------------------------------------------------------
    call BC_Velocity(penu,penv,penw,uin,vin,win,vl)

    !-------------------------------------------------------------------------------
    ! creation du fichier de sortie pour les cas de validations
    !-------------------------------------------------------------------------------
    call Valid_OutputFileCreation()

    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'sortie RESPECT_Initialization'
    !-------------------------------------------------------------------------------
  end subroutine RESPECT_Initialization

  !===============================================================================
end module Bib_VOFLag_RESPECT_Initialization
!===============================================================================
