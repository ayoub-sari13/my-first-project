module mod_random
  
contains
  
  subroutine random_uniform(val,mean,amp)
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), intent(in)  :: mean,amp
    real(8), intent(out) :: val
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------
    call random_number(val)
    val=mean+(val-0.5d0)*amp
  end subroutine random_uniform
  
  subroutine random_gauss(val,mean,sig)
    use mod_constants
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), intent(in)  :: mean,sig
    real(8), intent(out) :: val
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    real(8)              :: v1,v2
    real(8)              :: two_pi=2*pi
    !-------------------------------------------------------------------------------
    call random_number(v1)
    call random_number(v2)
    do while (v1==0)
       call random_number(v1)
    end do
    val=sqrt(-2*log(v1))*cos(two_pi*v2)
    val=mean+sig*val
  end subroutine random_gauss

end module mod_random
