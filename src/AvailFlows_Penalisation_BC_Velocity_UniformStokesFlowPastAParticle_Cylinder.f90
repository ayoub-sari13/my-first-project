!===============================================================================
module Bib_VOFLag_BC_Velocity_UniformStokesFlowPastAParticle_Cylinder
   !===============================================================================
   contains
   !-----------------------------------------------------------------------------  
   !> @author  amine chadil
   ! 
   !> @brief impose des conditions aux limites egales a la solution exactes d'un ecoulement
   !! de stokes autour d'une cylinder. cette routine suppose que des condtions de diriclet
   !! ont ete imposees dans le don
   !
   !> @param[in]  penu, penv      : 
   !> @param[in]  uin, vin        : 
   !> @param[in]  centre_cylinder : 
   !-----------------------------------------------------------------------------
   subroutine BC_Velocity_UniformStokesFlowPastAParticle_Cylinder(penu,penv,uin,vin,&
                                                                  centre_cylinder,rayon_cylinder)
      !===============================================================================
      !modules
      !===============================================================================
      !-------------------------------------------------------------------------------
      !Modules de definition des structures et variables => src/mod/module_...f90
      !-------------------------------------------------------------------------------
      use mod_struct_thermophysics, only : fluids
      use Module_VOFLag_Flows
      use mod_Parameters,           only : deeptracking,sx,gx,ex,gx,sy,gy,ey,gy,sxu,exu,&
                                           syu,eyu,sxv,exv,syv,eyv,gsxu,gexu,gsxv,gexv, &
                                           gsyu,geyu,gsyv,geyv,grid_xu,grid_yv,grid_x,  &
                                           grid_y,NS_pen_BC
      use mod_Constants,            only : d1p2
      !-------------------------------------------------------------------------------
      !Modules de subroutines de la librairie RESPECT => src/Bib_VOFLag_...f90
      !-------------------------------------------------------------------------------
      use Bib_VOFLag_k0
      use Bib_VOFLag_k1
      !-------------------------------------------------------------------------------

      !===============================================================================
      !declarations des variables
      !===============================================================================
      implicit none
      !-------------------------------------------------------------------------------
      !variables globales
      !-------------------------------------------------------------------------------
      real(8), dimension(:,:,:,:), allocatable      :: penu,penv
      real(8), dimension(:,:,:)  , allocatable      :: uin,vin
      real(8), dimension(3)                         :: centre_cylinder
      real(8)                                       :: rayon_cylinder
      !-------------------------------------------------------------------------------
      !variables locales 
      !-------------------------------------------------------------------------------
      real(8), dimension(sxu-gx:exu+gx,syu-gy:eyu+gy) :: vr_u,vphi_u
      real(8), dimension(sxv-gx:exv+gx,syv-gy:eyv+gy) :: vr_v,vphi_v
      real(8), dimension(sx-gx:ex+gx,sy-gy:ey+gy)     :: vr_pres,vphi_pres
      real(8), dimension(2)                           :: r,cosphi,sinphi,vr,vphi
      real(8)                                         :: sigma,k_0,k_1
      real(8)                                         :: x,y,full_pen,half_pen
      integer                                         :: i,j,k
      !-------------------------------------------------------------------------------

      !-------------------------------------------------------------------------------
      if (deeptracking) write(*,*) 'entree BC_Velocity_UniformStokesFlowPastAParticle_Cylinder'
      !-------------------------------------------------------------------------------

      !-------------------------------------------------------------------------------
      ! Initialisation
      !-------------------------------------------------------------------------------
      full_pen=NS_pen_BC
      half_pen=d1p2*NS_pen_BC
      sigma=0.1D0 
      k_0=k0(sigma)
      k_1=k1(sigma)

      do j=syu,eyu
         do i=sxu,exu
            select case (direction)
               case(1)
                  x=grid_xu(i)-centre_cylinder(1)
                  y=grid_y (j)-centre_cylinder(2)
               case(2)
                  x=grid_y (j)-centre_cylinder(2)
                  y=grid_xu(i)-centre_cylinder(1)
            end select
            r(1) = sqrt(x**2+y**2)
            if ( r(1) > rayon_cylinder ) then 
               cosphi(1)=x/r(1)
               sinphi(1)=y/r(1)
               vr_u(i,j)    =(r(1)-(1d0+(2d0*k_1)/(sigma*k_0))/r(1)+ &
                            (2d0*k1(sigma*r(1)))/(sigma*k_0))*cosphi(1)/r(1)
               vphi_u(i,j)  =-(1d0+(1d0+(2d0*k_1)/(sigma*k_0))/r(1)**2 &
                            -(2d0/(k_0))*(k0(sigma*r(1))+(k1(sigma*r(1)))/(sigma*r(1))))*sinphi(1)
            end if 
         end do 
      end do 
      
      do j=syv,eyv
         do i=sxv,exv
            select case (direction)
               case(1)
                  x=grid_x (i)-centre_cylinder(1)
                  y=grid_yv(j)-centre_cylinder(2)
               case(2)
                  x=grid_yv(j)-centre_cylinder(2)
                  y=grid_x (i)-centre_cylinder(1)
            end select
            r(2) = sqrt(x**2+y**2)
            if ( r(2) > rayon_cylinder) then 
               cosphi(2)=x/r(2)
               sinphi(2)=y/r(2)
               vr_v(i,j)   =(r(2)-(1d0+(2d0*k_1)/(sigma*k_0))/r(2)+ &
                            (2d0*k1(sigma*r(2)))/(sigma*k_0))*cosphi(2)/r(2)
               vphi_v(i,j) =-(1d0+(1d0+(2d0*k_1)/(sigma*k_0))/r(2)**2 &
                            -(2d0/(k_0))*(k0(sigma*r(2))+(k1(sigma*r(2)))/(sigma*r(2))))*sinphi(2)
            end if 
         end do 
       end do 

      do j=sy,ey
         do i=sx,ex
            select case (direction)
               case(1)
                  x=grid_x(i)-centre_cylinder(1)
                  y=grid_y(j)-centre_cylinder(2)
               case(2)
                  x=grid_y(j)-centre_cylinder(2)
                  y=grid_x(i)-centre_cylinder(1)
            end select
            r(1)=sqrt(x**2+y**2)
            if ( r(1) > rayon_cylinder) then 
               cosphi(1)=x/r(1)
               sinphi(1)=y/r(1)

               vr_pres(i,j)  =(r(1) - (1.D0 + (2.D0*k_1)/(sigma*k_0+1D-40) )/(r(1)+1D-40)+ &
                              (2.D0*k1(sigma*r(1)))/(sigma*k_0+1D-40) )*cosphi(1)/(r(1)+1D-40)

               vphi_pres(i,j)=-( 1.D0 + (1.D0 + (2.D0*k_1)/(sigma*k_0+1D-40))/(r(1)**2+1D-40) &
                              -2.D0/(k_0+1D-40)* (k0(sigma*r(1))+k1(sigma*r(1))/(sigma*r(1)+1D-40)))*sinphi(1)
            end if 
         end do
       end do
      !===============================================================================
      ! Traitement des vitesses normales
      !===============================================================================
      !-------------------------------------------------------------------------------
      ! Limite GAUCHE : composante normale = conposante 1
      !-------------------------------------------------------------------------------
      do j=syu,eyu
         do i=sxu,exu
            if (i==gsxu) then
               penu(i,j,1,:)=0
               penu(i,j,1,1)=half_pen
               penu(i,j,1,3)=half_pen
               select case (direction)
                  case(1)
                     x=grid_x(i)-centre_cylinder(1)
                     y=grid_y(j)-centre_cylinder(2)
                  case(2)
                     x=grid_y(j)-centre_cylinder(2)
                     y=grid_x(i)-centre_cylinder(1)
               end select
               r(1)=sqrt(x**2+y**2)
               cosphi(1)=x/(r(1)+1D-40)
               sinphi(1)=y/(r(1)+1D-40)
               select case (direction)
                  case(1)
                     uin(i,j,1)=2d0*(cosphi(1)*vr_pres(i,j)-sinphi(1)*vphi_pres(i,j))
                  case(2)
                     uin(i,j,1)=2d0*(sinphi(1)*vr_pres(i,j)+cosphi(1)*vphi_pres(i,j))
               end select
            endif
         enddo
      enddo
      !-------------------------------------------------------------------------------
      ! Limite DROITE : composante normale = conposante 1
      !-------------------------------------------------------------------------------
      do j=syu,eyu
         do i=sxu,exu
            if (i==gexu) then
               penu(i,j,1,:)=0
               penu(i,j,1,1)=half_pen
               penu(i,j,1,2)=half_pen
               select case (direction)
                  case(1)
                     x=grid_x(i-1)-centre_cylinder(1)
                     y=grid_y(j)-centre_cylinder(2)
                  case(2)
                     x=grid_y(j)-centre_cylinder(2)
                     y=grid_x(i-1)-centre_cylinder(1)
               end select
               r(1)=sqrt(x**2+y**2)
               cosphi(1)=x/(r(1)+1D-40)
               sinphi(1)=y/(r(1)+1D-40)
               select case (direction)
                  case(1)
                     uin(i,j,1)=2d0*(cosphi(1)*vr_pres(i-1,j)-sinphi(1)*vphi_pres(i-1,j))
                  case(2)
                     uin(i,j,1)=2d0*(sinphi(1)*vr_pres(i-1,j)+cosphi(1)*vphi_pres(i-1,j))
               end select
            endif
         enddo
      enddo
      !-------------------------------------------------------------------------------
      ! Limite INF : composante normale = conposante 2
      !-------------------------------------------------------------------------------
      do j=syv,eyv
         do i=sxv,exv
            if (j==gsyv) then
               penv(i,j,1,:)=0
               penv(i,j,1,1)=half_pen
               penv(i,j,1,5)=half_pen
               select case (direction)
                  case(1)
                     x=grid_x(i)-centre_cylinder(1)
                     y=grid_y(j)-centre_cylinder(2)
                  case(2)
                     x=grid_y(j)-centre_cylinder(2)
                     y=grid_x(i)-centre_cylinder(1)
               end select
               r(1)=sqrt(x**2+y**2)
               cosphi(1)=x/(r(1)+1D-40)
               sinphi(1)=y/(r(1)+1D-40)
               select case (direction)
                  case(1)
                     vin(i,j,1)=2d0*(sinphi(1)*vr_pres(i,j)+cosphi(1)*vphi_pres(i,j))
                  case(2)
                     vin(i,j,1)=2d0*(cosphi(1)*vr_pres(i,j)-sinphi(1)*vphi_pres(i,j))
               end select
            endif
         enddo
      enddo
      !-------------------------------------------------------------------------------
      ! Limite SUP : composante normale = conposante 2
      !-------------------------------------------------------------------------------
      do j=syv,eyv
         do i=sxv,exv
            if (j==geyv) then
               penv(i,j,1,:)=0
               penv(i,j,1,1)=half_pen
               penv(i,j,1,4)=half_pen
               select case (direction)
                  case(1)
                     x=grid_x(i)-centre_cylinder(1)
                     y=grid_y(j-1)-centre_cylinder(2)
                  case(2)
                     x=grid_y(j-1)-centre_cylinder(2)
                     y=grid_x(i)-centre_cylinder(1)
               end select
               r(1)=sqrt(x**2+y**2)
               cosphi(1)=x/(r(1)+1D-40)
               sinphi(1)=y/(r(1)+1D-40)
               select case (direction)
                  case(1)
                     vin(i,j,1)=2d0*(sinphi(1)*vr_pres(i,j-1)+cosphi(1)*vphi_pres(i,j-1))
                  case(2)
                     vin(i,j,1)=2d0*(cosphi(1)*vr_pres(i,j-1)-sinphi(1)*vphi_pres(i,j-1))
               end select
            endif
         enddo
      enddo
      !===============================================================================
      ! Traitement des vitesses tangentielles
      !===============================================================================
      !-------------------------------------------------------------------------------
      ! Limite GAUCHE ou DROITE
      !-------------------------------------------------------------------------------
      ! traitement de la composante tangentielle de la vitesse (conposante 2)
      do j=syv,eyv
         do i=sxv,exv
            if (i==gsxv .or. i==gexv) then
               penv(i,j,1,:)=0
               penv(i,j,1,1)=full_pen
               select case (direction)
                  case(1)
                     x=grid_x (i)-centre_cylinder(1)
                     y=grid_yv(j)-centre_cylinder(2)
                  case(2)
                     x=grid_yv(j)-centre_cylinder(2)
                     y=grid_x (i)-centre_cylinder(1)
               end select
               r(1)=sqrt(x**2+y**2)
               cosphi(1)=x/(r(1)+1D-40)
               sinphi(1)=y/(r(1)+1D-40)
               select case (direction)
                  case(1)
                     vin(i,j,1)=sinphi(1)*vr_v(i,j)+cosphi(1)*vphi_v(i,j)
                  case(2)
                     vin(i,j,1)=cosphi(1)*vr_v(i,j)-sinphi(1)*vphi_v(i,j)
               end select
            endif
         enddo
      enddo
      !-------------------------------------------------------------------------------
      ! Limite INF ou SUP
      !-------------------------------------------------------------------------------
      ! traitement de la composante tangentielle de la vitesse (conposante 1)
      do j=syu,eyu
         do i=sxu,exu
            if (j==gsyu .or. j==geyu) then
               penu(i,j,1,:)=0
               penu(i,j,1,1)=full_pen
               select case (direction)
                  case(1)
                     x=grid_xu(i)-centre_cylinder(1)
                     y=grid_y (j)-centre_cylinder(2)
                  case(2)
                     x=grid_y (j)-centre_cylinder(2)
                     y=grid_xu(i)-centre_cylinder(1)
               end select
               r(1)=sqrt(x**2+y**2)
               cosphi(1)=x/(r(1)+1D-40)
               sinphi(1)=y/(r(1)+1D-40)
               select case (direction)
                  case(1)
                     uin(i,j,1)=cosphi(1)*vr_u(i,j)-sinphi(1)*vphi_u(i,j)
                  case(2)
                     uin(i,j,1)=sinphi(1)*vr_u(i,j)+cosphi(1)*vphi_u(i,j)
               end select
            endif
         enddo
      enddo
      !-------------------------------------------------------------------------------
      if (deeptracking) write(*,*) 'entree BC_Velocity_UniformStokesFlowPastAParticle_Cylinder'
      !-------------------------------------------------------------------------------
  end subroutine BC_Velocity_UniformStokesFlowPastAParticle_Cylinder

  !===============================================================================
end module Bib_VOFLag_BC_Velocity_UniformStokesFlowPastAParticle_Cylinder
!===============================================================================