!========================================================================
!
!                   FUGU Version 1.0.0
!
!========================================================================
!
! Copyright  Stéphane Vincent
!
! stephane.vincent@u-pem.fr          straightparadigm@gmail.com
!
! This file is part of the FUGU Fortran library, a set of Computational 
! Fluid Dynamics subroutines devoted to the simulation of multi-scale
! multi-phase flows for resolved scale interfaces (liquid/gas/solid). 
! FUGU uses the initial developments of DyJeAT and SHALA codes from 
! Jean-Luc Estivalezes (jean-luc.estivalezes@onera.fr) and 
! Frédéric Couderc (couderc@math.univ-toulouse.fr).
!
!========================================================================
!**
!**   NAME       : Init_NS.f90
!**
!**   AUTHOR     : Stéphane Vincent
!**                Benoit Trouette
!**
!**   FUNCTION   : Modules of subroutines for variable initialization of Navier-Stokes equations
!**
!**   DATES      : Version 1.0.0  : from : July, 2018
!**
!========================================================================

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#include "precomp.h"

module mod_thermophy

  use mod_Parameters, only: rank,dim,                                         &
       & sx,ex,sy,ey,sz,ez,sxs,exs,sys,eys,szs,ezs,                           &
       & sxu,exu,syu,eyu,szu,ezu,                                             &
       & sxv,exv,syv,eyv,szv,ezv,                                             &
       & sxw,exw,syw,eyw,szw,ezw,                                             &
       & sxus,exus,syus,eyus,szus,ezus,                                       &
       & sxvs,exvs,syvs,eyvs,szvs,ezvs,                                       &
       & sxws,exws,syws,eyws,szws,ezws,                                       &
       & nb_phase,VOF_activate,LS_activate,FT_Delta,NS_init_pre,FT_activate,  &
       & grid_x,grid_y,grid_z,grid_xu,grid_yv,grid_zw,                        &
       & ref_pres,ref_tp,AL_comp,VOF_mean_mu,                                 &
       & NS_compressible
  use mod_Constants, only: d1p2,d1p4
  use mod_struct_thermophysics


  !-------------------------------------------------------------------------------
  ! variables for vortex method 
  !-------------------------------------------------------------------------------
  integer, dimension(:), allocatable :: sgn
  real(8), dimension(:), allocatable :: yVortex0,zVortex0
  !-------------------------------------------------------------------------------
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
contains

  !*****************************************************************************************************
  subroutine thermophysics(rho,rovu,rovv,rovw,vie,vis,vir,xit,cou,pres,tp,fluids)
    !***************************************************************************************************
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable   :: rho,vie,xit,rovu,rovv,rovw
    real(8), dimension(:,:,:,:), allocatable :: vis,vir
    real(8), dimension(:,:,:), allocatable   :: cou,pres,tp
    type(phase_t), dimension(:), allocatable   :: fluids
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                  :: i,j,k
    integer                                  :: mean_mu
    real(8)                                  :: coul
    real(8)                                  :: tmp1,tmp2
    !-------------------------------------------------------------------------------
    ! Initialization of phase characteristics
    !-------------------------------------------------------------------------------

    mean_mu=VOF_mean_mu
    
    !-------------------------------------------------------------------------------
    if (nb_phase==1) then
       !-------------------------------------------------
       ! density according eos kind
       !-------------------------------------------------
       select case(fluids(1)%eos)
       case default 
          rho=fluids(1)%rho
          rovu=fluids(1)%rho
          rovv=fluids(1)%rho
          if (dim==3) rovw=fluids(1)%rho
       case(2)
          do k=szs,ezs
             do j=sys,eys
                do i=sxs,exs
                   rho(i,j,k)=(pres(i,j,k)+ref_pres)/(fluids(1)%r*(tp(i,j,k)+ref_tp))
                end do
             end do
          end do
          !-------------------------------------------------
          ! U component
          !-------------------------------------------------
          do k=szus,ezus
             do j=syus,eyus
                do i=sxus,exus
                   tmp1=d1p2*(pres(i,j,k)+pres(i-1,j,k))
                   tmp2=d1p2*(tp(i,j,k)+tp(i-1,j,k))
                   rovu(i,j,k)=(tmp1+ref_pres)/(fluids(1)%r*(tmp2+ref_tp))
                enddo
             enddo
          end do
          !-------------------------------------------------
          ! V component
          !-------------------------------------------------
          do k=szvs,ezvs
             do j=syvs,eyvs
                do i=sxvs,exvs
                   tmp1=d1p2*(pres(i,j,k)+pres(i,j-1,k))
                   tmp2=d1p2*(tp(i,j,k)+tp(i,j-1,k))
                   rovv(i,j,k)=(tmp1+ref_pres)/(fluids(1)%r*(tmp2+ref_tp))
                enddo
             enddo
          end do
          if (dim==3) then
             !-------------------------------------------------
             ! V component
             !-------------------------------------------------
             do k=szws,ezws
                do j=syws,eyws
                   do i=sxws,exws
                      tmp1=d1p2*(pres(i,j,k)+pres(i,j,k-1))
                      tmp2=d1p2*(tp(i,j,k)+tp(i,j,k-1))
                      rovw(i,j,k)=(tmp1+ref_pres)/(fluids(1)%r*(tmp2+ref_tp))
                   enddo
                enddo
             end do
          end if
       case(3)
          do k=szs,ezs
             do j=sys,eys
                do i=sxs,exs
                   rho(i,j,k)=(pres(i,j,k)+ref_pres)/(fluids(1)%r*ref_tp)
                end do
             end do
          end do
          !-------------------------------------------------
          ! U component
          !-------------------------------------------------
          do k=szus,ezus
             do j=syus,eyus
                do i=sxus,exus
                   tmp1=d1p2*(pres(i,j,k)+pres(i-1,j,k))
                   rovu(i,j,k)=(tmp1+ref_pres)/(fluids(1)%r*ref_tp)
                enddo
             enddo
          end do
          !-------------------------------------------------
          ! V component
          !-------------------------------------------------
          do k=szvs,ezvs
             do j=syvs,eyvs
                do i=sxvs,exvs
                   tmp1=d1p2*(pres(i,j,k)+pres(i,j-1,k))
                   rovv(i,j,k)=(tmp1+ref_pres)/(fluids(1)%r*ref_tp)
                enddo
             enddo
          end do
          if (dim==3) then
             !-------------------------------------------------
             ! V component
             !-------------------------------------------------
             do k=szws,ezws
                do j=syws,eyws
                   do i=sxws,exws
                      tmp1=d1p2*(pres(i,j,k)+pres(i,j,k-1))
                      rovw(i,j,k)=(tmp1+ref_pres)/(fluids(1)%r*ref_tp)
                   enddo
                enddo
             end do
          end if
       end select
       !-------------------------------------------------
       ! other properties
       !-------------------------------------------------
       if (AL_comp.or.NS_compressible) then
          select case(fluids(1)%eos)
          case default
             xit=fluids(1)%xit
          case(-1,3)
             xit=1/(pres + ref_pres)
          end select
       else
          xit=fluids(1)%xit
       end if
       !-------------------------------------------------
       vie=2*fluids(1)%mu
       vis=2*fluids(1)%mu
       vir=fluids(1)%mu
       !-------------------------------------------------

    else if (nb_phase==2) then

       !-------------------------------------------------
       ! sigma phase 2 = sigma phase 1
       !-------------------------------------------------
       fluids(2)%sigma = fluids(1)%sigma
       !-------------------------------------------------
       
       if (VOF_activate.or.LS_activate.or.FT_Delta) then
          
          !-------------------------------------------------
          ! compressibility
          !-------------------------------------------------
          if (AL_comp.or.NS_compressible) then
             xit=0
             !-------------------------------------------------
             ! fluid 1 
             !-------------------------------------------------
             select case(fluids(1)%eos)
             case default
                xit=xit+(1-cou)*fluids(1)%xit
             case(-1,3)
                xit=xit+(1-cou)/(pres+ref_pres)
             end select
             !-------------------------------------------------
             ! fluid 2
             !-------------------------------------------------
             select case(fluids(2)%eos)
             case default
                xit=xit+cou*fluids(2)%xit
             case(-1,3)
                xit=xit+cou/(pres+ref_pres)
             end select
             !-------------------------------------------------
          else
             xit=cou*fluids(2)%xit+(1-cou)*fluids(1)%xit
          end if
          !-------------------------------------------------
          ! viscosity (elongationnal) reconstruction at scalar nodes
          !-------------------------------------------------
          select case(mean_mu)
          case(1) ! arithmetic
             vie=2*(cou*fluids(2)%mu+(1-cou)*fluids(1)%mu)
          case(2) ! harmonic
             vie=2*fluids(1)%mu*fluids(2)%mu/(cou*fluids(1)%mu+(1-cou)*fluids(2)%mu)
          case(3) ! discontinuous
             where (cou>d1p2)
                vie=2*fluids(2)%mu
             elsewhere
                vie=2*fluids(1)%mu
             end where
          end select
          !-------------------------------------------------
          
          if (dim==2) then
             !-------------------------------------------------
             ! density according eos kind
             !-------------------------------------------------
             rho=0
             !-------------------------------------------------
             ! fluid 1 
             !-------------------------------------------------
             select case(fluids(1)%eos)
             case default
                rho=rho+(1-cou)*fluids(1)%rho
             case(2)
                do j=sys,eys
                   do i=sxs,exs
                      tmp1=(pres(i,j,1)+ref_pres)/(fluids(1)%r*(tp(i,j,1)+ref_tp))
                      rho(i,j,1)=rho(i,j,1)+(1-cou(i,j,1))*tmp1
                   end do
                end do
             case(3)
                do j=sys,eys
                   do i=sxs,exs
                      tmp1=(pres(i,j,1)+ref_pres)/(fluids(1)%r*ref_tp)
                      rho(i,j,1)=rho(i,j,1)+(1-cou(i,j,1))*tmp1
                   end do
                end do
             end select
             !-------------------------------------------------
             ! fluid 2
             !-------------------------------------------------
             select case(fluids(2)%eos)
             case default
                rho=rho+cou*fluids(2)%rho
             case(2)
                do j=sys,eys
                   do i=sxs,exs
                      tmp1=(pres(i,j,1)+ref_pres)/(fluids(2)%r*(tp(i,j,1)+ref_tp))
                      rho(i,j,1)=rho(i,j,1)+cou(i,j,1)*tmp1
                   end do
                end do
             case(3)
                do j=sys,eys
                   do i=sxs,exs
                      tmp1=(pres(i,j,1)+ref_pres)/(fluids(2)%r*ref_tp)
                      rho(i,j,1)=rho(i,j,1)+cou(i,j,1)*tmp1
                   end do
                end do
             end select
             !-------------------------------------------------
             ! U component
             !-------------------------------------------------
             rovu=0
             !-------------------------------------------------
             ! fluid 1
             !-------------------------------------------------
             select case(fluids(1)%eos)
             case default
                do j=syus,eyus
                   do i=sxus,exus
                      coul=d1p2*(cou(i,j,1)+cou(i-1,j,1))
                      rovu(i,j,1)=rovu(i,j,1)+(1-coul)*fluids(1)%rho
                   enddo
                enddo
             case(2)
                do j=syus,eyus
                   do i=sxus,exus
                      coul=d1p2*(cou(i,j,1)+cou(i-1,j,1))
                      tmp1=d1p2*(pres(i,j,1)+pres(i-1,j,1))
                      tmp2=d1p2*(tp(i,j,1)+tp(i-1,j,1))
                      rovu(i,j,1)=rovu(i,j,1)+(1-coul)*(tmp1+ref_pres)/(fluids(1)%r*(tmp2+ref_tp))
                   enddo
                enddo
             case(3)
                do j=syus,eyus
                   do i=sxus,exus
                      coul=d1p2*(cou(i,j,1)+cou(i-1,j,1))
                      tmp1=d1p2*(pres(i,j,1)+pres(i-1,j,1))
                      rovu(i,j,1)=rovu(i,j,1)+(1-coul)*(tmp1+ref_pres)/(fluids(1)%r*ref_tp)
                   enddo
                enddo
             end select
             !-------------------------------------------------
             ! fluid 2
             !-------------------------------------------------
             select case(fluids(2)%eos)
             case default
                do j=syus,eyus
                   do i=sxus,exus
                      coul=d1p2*(cou(i,j,1)+cou(i-1,j,1))
                      rovu(i,j,1)=rovu(i,j,1)+coul*fluids(2)%rho
                   enddo
                enddo
             case(2)
                do j=syus,eyus
                   do i=sxus,exus
                      coul=d1p2*(cou(i,j,1)+cou(i-1,j,1))
                      tmp1=d1p2*(pres(i,j,1)+pres(i-1,j,1))
                      tmp2=d1p2*(tp(i,j,1)+tp(i-1,j,1))
                      rovu(i,j,1)=rovu(i,j,1)+coul*(tmp1+ref_pres)/(fluids(2)%r*(tmp2+ref_tp))
                   enddo
                enddo
             case(3)
                do j=syus,eyus
                   do i=sxus,exus
                      coul=d1p2*(cou(i,j,1)+cou(i-1,j,1))
                      tmp1=d1p2*(pres(i,j,1)+pres(i-1,j,1))
                      rovu(i,j,1)=rovu(i,j,1)+coul*(tmp1+ref_pres)/(fluids(2)%r*ref_tp)
                   enddo
                enddo
             end select
             !-------------------------------------------------
             ! V component
             !-------------------------------------------------
             rovv=0
             !-------------------------------------------------
             ! fluid 1
             !-------------------------------------------------
             select case(fluids(1)%eos)
             case default
                do j=syvs,eyvs
                   do i=sxvs,exvs
                      coul=d1p2*(cou(i,j,1)+cou(i,j-1,1))
                      rovv(i,j,1)=rovv(i,j,1)+(1-coul)*fluids(1)%rho
                   enddo
                enddo
             case(2)
                do j=syvs,eyvs
                   do i=sxvs,exvs
                      coul=d1p2*(cou(i,j,1)+cou(i,j-1,1))
                      tmp1=d1p2*(pres(i,j,1)+pres(i,j-1,1))
                      tmp2=d1p2*(tp(i,j,1)+tp(i,j-1,1))
                      rovv(i,j,1)=rovv(i,j,1)+(1-coul)*(tmp1+ref_pres)/(fluids(1)%r*(tmp2+ref_tp))
                   enddo
                enddo
             case(3)
                do j=syvs,eyvs
                   do i=sxvs,exvs
                      coul=d1p2*(cou(i,j,1)+cou(i,j-1,1))
                      tmp1=d1p2*(pres(i,j,1)+pres(i,j-1,1))
                      rovv(i,j,1)=rovv(i,j,1)+(1-coul)*(tmp1+ref_pres)/(fluids(1)%r*ref_tp)
                   enddo
                enddo
             end select
             !-------------------------------------------------
             ! fluid 2
             !-------------------------------------------------
             select case(fluids(2)%eos)
             case default
                do j=syvs,eyvs
                   do i=sxvs,exvs
                      coul=d1p2*(cou(i,j,1)+cou(i,j-1,1))
                      rovv(i,j,1)=rovv(i,j,1)+coul*fluids(2)%rho
                   enddo
                enddo
             case(2)
                do j=syvs,eyvs
                   do i=sxvs,exvs
                      coul=d1p2*(cou(i,j,1)+cou(i,j-1,1))
                      tmp1=d1p2*(pres(i,j,1)+pres(i,j-1,1))
                      tmp2=d1p2*(tp(i,j,1)+tp(i,j-1,1))
                      rovv(i,j,1)=rovv(i,j,1)+coul*(tmp1+ref_pres)/(fluids(2)%r*(tmp2+ref_tp))
                   enddo
                enddo
             case(3)
                do j=syvs,eyvs
                   do i=sxvs,exvs
                      coul=d1p2*(cou(i,j,1)+cou(i,j-1,1))
                      tmp1=d1p2*(pres(i,j,1)+pres(i,j-1,1))
                      rovv(i,j,1)=rovv(i,j,1)+coul*(tmp1+ref_pres)/(fluids(2)%r*ref_tp)
                   enddo
                enddo
             end select
             !-------------------------------------------------
             ! viscosities (shear & rotational)
             !-------------------------------------------------
             select case(mean_mu)
             case(1)
                do j=sy,ey+1
                   do i=sx,ex+1
                      coul=d1p4*(cou(i,j,1)+cou(i,j-1,1)+cou(i-1,j,1)+cou(i-1,j-1,1))
                      vis(i,j,1,1)=2*(coul*fluids(2)%mu+(1-coul)*fluids(1)%mu)
                      vir(i,j,1,1)=coul*fluids(2)%mu+(1-coul)*fluids(1)%mu
                   enddo
                enddo
             case(2)
                do j=sy,ey+1
                   do i=sx,ex+1
                      coul=d1p4*(cou(i,j,1)+cou(i,j-1,1)+cou(i-1,j,1)+cou(i-1,j-1,1))
                      vis(i,j,1,1)=2*fluids(1)%mu*fluids(2)%mu/(coul*fluids(1)%mu+(1-coul)*fluids(2)%mu)
                      vir(i,j,1,1)=fluids(1)%mu*fluids(2)%mu/(coul*fluids(1)%mu+(1-coul)*fluids(2)%mu)
                   enddo
                enddo
             case(3)
                do j=sy,ey+1
                   do i=sx,ex+1
                      coul=d1p4*(cou(i,j,1)+cou(i,j-1,1)+cou(i-1,j,1)+cou(i-1,j-1,1))
                      if (coul>d1p2) then
                         vis(i,j,1,1)=2*fluids(2)%mu
                         vir(i,j,1,1)=fluids(2)%mu
                      else
                         vis(i,j,1,1)=2*fluids(1)%mu
                         vir(i,j,1,1)=fluids(1)%mu
                      end if
                   end do
                end do
             end select
             !-------------------------------------------------
          else ! dim = 3
             !-------------------------------------------------
             ! density according eos kind
             !-------------------------------------------------
             rho=0
             !-------------------------------------------------
             ! fluid 1 
             !-------------------------------------------------
             select case(fluids(1)%eos)
             case default
                rho=rho+(1-cou)*fluids(1)%rho
             case(2)
                do k=szs,ezs
                   do j=sys,eys
                      do i=sxs,exs
                         tmp1=(pres(i,j,k)+ref_pres)/(fluids(1)%r*(tp(i,j,k)+ref_tp))
                         rho(i,j,k)=rho(i,j,k)+(1-cou(i,j,k))*tmp1
                      end do
                   end do
                end do
             case(3)
                do k=szs,ezs
                   do j=sys,eys
                      do i=sxs,exs
                         tmp1=(pres(i,j,k)+ref_pres)/(fluids(1)%r*ref_tp)
                         rho(i,j,k)=rho(i,j,k)+(1-cou(i,j,k))*tmp1
                      end do
                   end do
                end do
             end select
             !-------------------------------------------------
             ! fluid 2
             !-------------------------------------------------
             select case(fluids(2)%eos)
             case default
                rho=rho+cou*fluids(2)%rho
             case(2)
                do k=szs,ezs
                   do j=sys,eys
                      do i=sxs,exs
                         tmp1=(pres(i,j,k)+ref_pres)/(fluids(2)%r*(tp(i,j,k)+ref_tp))
                         rho(i,j,k)=rho(i,j,k)+cou(i,j,k)*tmp1
                      end do
                   end do
                end do
             case(3)
                do k=szs,ezs
                   do j=sys,eys
                      do i=sxs,exs
                         tmp1=(pres(i,j,k)+ref_pres)/(fluids(2)%r*ref_tp)
                         rho(i,j,k)=rho(i,j,k)+cou(i,j,k)*tmp1
                      end do
                   end do
                end do
             end select
             !-------------------------------------------------
             ! U component
             !-------------------------------------------------
             rovu=0
             !-------------------------------------------------
             ! fluid 1
             !-------------------------------------------------
             select case(fluids(1)%eos)
             case default
                do k=szus,ezus
                   do j=syus,eyus
                      do i=sxus,exus
                         coul=d1p2*(cou(i,j,k)+cou(i-1,j,k))
                         rovu(i,j,k)=rovu(i,j,k)+(1-coul)*fluids(1)%rho
                      end do
                   end do
                end do
             case(2)
                do k=szus,ezus
                   do j=syus,eyus
                      do i=sxus,exus
                         coul=d1p2*(cou(i,j,k)+cou(i-1,j,k))
                         tmp1=d1p2*(pres(i,j,k)+pres(i-1,j,k))
                         tmp2=d1p2*(tp(i,j,k)+tp(i-1,j,k))
                         rovu(i,j,k)=rovu(i,j,k)+(1-coul)*(tmp1+ref_pres)/(fluids(1)%r*(tmp2+ref_tp))
                      end do
                   end do
                end do
             case(3)
                do k=szus,ezus
                   do j=syus,eyus
                      do i=sxus,exus
                         coul=d1p2*(cou(i,j,k)+cou(i-1,j,k))
                         tmp1=d1p2*(pres(i,j,k)+pres(i-1,j,k))
                         rovu(i,j,k)=rovu(i,j,k)+(1-coul)*(tmp1+ref_pres)/(fluids(1)%r*ref_tp)
                      end do
                   end do
                end do
             end select
             !-------------------------------------------------
             ! fluid 2
             !-------------------------------------------------
             select case(fluids(2)%eos)
             case default
                do k=szus,ezus
                   do j=syus,eyus
                      do i=sxus,exus
                         coul=d1p2*(cou(i,j,k)+cou(i-1,j,k))
                         rovu(i,j,k)=rovu(i,j,k)+coul*fluids(2)%rho
                      end do
                   end do
                end do
             case(2)
                do k=szus,ezus
                   do j=syus,eyus
                      do i=sxus,exus
                         coul=d1p2*(cou(i,j,k)+cou(i-1,j,k))
                         tmp1=d1p2*(pres(i,j,k)+pres(i-1,j,k))
                         tmp2=d1p2*(tp(i,j,k)+tp(i-1,j,k))
                         rovu(i,j,k)=rovu(i,j,k)+coul*(tmp1+ref_pres)/(fluids(2)%r*(tmp2+ref_tp))
                      end do
                   end do
                end do
             case(3)
                do k=szus,ezus
                   do j=syus,eyus
                      do i=sxus,exus
                         coul=d1p2*(cou(i,j,k)+cou(i-1,j,k))
                         tmp1=d1p2*(pres(i,j,k)+pres(i-1,j,k))
                         rovu(i,j,k)=rovu(i,j,k)+coul*(tmp1+ref_pres)/(fluids(2)%r*ref_tp)
                      end do
                   end do
                end do
             end select
             !-------------------------------------------------
             ! V component
             !-------------------------------------------------
             rovv=0
             !-------------------------------------------------
             ! fluid 1
             !-------------------------------------------------
             select case(fluids(1)%eos)
             case default
                do k=szvs,ezvs
                   do j=syvs,eyvs
                      do i=sxvs,exvs
                         coul=d1p2*(cou(i,j,k)+cou(i,j-1,k))
                         rovv(i,j,k)=rovv(i,j,k)+(1-coul)*fluids(1)%rho
                      end do
                   end do
                end do
             case(2)
                do k=szvs,ezvs
                   do j=syvs,eyvs
                      do i=sxvs,exvs
                         coul=d1p2*(cou(i,j,k)+cou(i,j-1,k))
                         tmp1=d1p2*(pres(i,j,k)+pres(i,j-1,k))
                         tmp2=d1p2*(tp(i,j,k)+tp(i,j-1,k))
                         rovv(i,j,k)=rovv(i,j,k)+(1-coul)*(tmp1+ref_pres)/(fluids(1)%r*(tmp2+ref_tp))
                      end do
                   end do
                end do
             case(3)
                do k=szvs,ezvs
                   do j=syvs,eyvs
                      do i=sxvs,exvs
                         coul=d1p2*(cou(i,j,k)+cou(i,j-1,k))
                         tmp1=d1p2*(pres(i,j,k)+pres(i,j-1,k))
                         rovv(i,j,k)=rovv(i,j,k)+(1-coul)*(tmp1+ref_pres)/(fluids(1)%r*ref_tp)
                      end do
                   end do
                end do
             end select
             !-------------------------------------------------
             ! fluid 2
             !-------------------------------------------------
             select case(fluids(2)%eos)
             case default
                do k=szvs,ezvs
                   do j=syvs,eyvs
                      do i=sxvs,exvs
                         coul=d1p2*(cou(i,j,k)+cou(i,j-1,k))
                         rovv(i,j,k)=rovv(i,j,k)+coul*fluids(2)%rho
                      end do
                   end do
                end do
             case(2)
                do k=szvs,ezvs
                   do j=syvs,eyvs
                      do i=sxvs,exvs
                         coul=d1p2*(cou(i,j,k)+cou(i,j-1,k))
                         tmp1=d1p2*(pres(i,j,k)+pres(i,j-1,k))
                         tmp2=d1p2*(tp(i,j,k)+tp(i,j-1,k))
                         rovv(i,j,k)=rovv(i,j,k)+coul*(tmp1+ref_pres)/(fluids(2)%r*(tmp2+ref_tp))
                      end do
                   end do
                end do
             case(3)
                do k=szvs,ezvs
                   do j=syvs,eyvs
                      do i=sxvs,exvs
                         coul=d1p2*(cou(i,j,k)+cou(i,j-1,k))
                         tmp1=d1p2*(pres(i,j,k)+pres(i,j-1,k))
                         rovv(i,j,k)=rovv(i,j,k)+coul*(tmp1+ref_pres)/(fluids(2)%r*ref_tp)
                      end do
                   end do
                end do
             end select
             !-------------------------------------------------
             ! W component
             !-------------------------------------------------
             rovw=0
             !-------------------------------------------------
             ! fluid 1
             !-------------------------------------------------
             select case(fluids(1)%eos)
             case default
                do k=szws,ezws
                   do j=syws,eyws
                      do i=sxws,exws
                         coul=d1p2*(cou(i,j,k)+cou(i,j,k-1))
                         rovw(i,j,k)=rovw(i,j,k)+(1-coul)*fluids(1)%rho
                      end do
                   end do
                end do
             case(2)
                do k=szws,ezws
                   do j=syws,eyws
                      do i=sxws,exws
                         coul=d1p2*(cou(i,j,k)+cou(i,j,k-1))
                         tmp1=d1p2*(pres(i,j,k)+pres(i,j,k-1))
                         tmp2=d1p2*(tp(i,j,k)+tp(i,j,k-1))
                         rovw(i,j,k)=rovw(i,j,k)+(1-coul)*(tmp1+ref_pres)/(fluids(1)%r*(tmp2+ref_tp))
                      end do
                   end do
                end do
             case(3)
                do k=szws,ezws
                   do j=syws,eyws
                      do i=sxws,exws
                         coul=d1p2*(cou(i,j,k)+cou(i,j,k-1))
                         tmp1=d1p2*(pres(i,j,k)+pres(i,j,k-1))
                         rovw(i,j,k)=rovw(i,j,k)+(1-coul)*(tmp1+ref_pres)/(fluids(1)%r*ref_tp)
                      end do
                   end do
                end do
             end select
             !-------------------------------------------------
             ! fluid 2
             !-------------------------------------------------
             select case(fluids(2)%eos)
             case default
                do k=szws,ezws
                   do j=syws,eyws
                      do i=sxws,exws
                         coul=d1p2*(cou(i,j,k)+cou(i,j,k-1))
                         rovw(i,j,k)=rovw(i,j,k)+coul*fluids(2)%rho
                      end do
                   end do
                end do
             case(2)
                do k=szws,ezws
                   do j=syws,eyws
                      do i=sxws,exws
                         coul=d1p2*(cou(i,j,k)+cou(i,j,k-1))
                         tmp1=d1p2*(pres(i,j,k)+pres(i,j,k-1))
                         tmp2=d1p2*(tp(i,j,k)+tp(i,j,k-1))
                         rovw(i,j,k)=rovw(i,j,k)+coul*(tmp1+ref_pres)/(fluids(2)%r*(tmp2+ref_tp))
                      end do
                   end do
                end do
             case(3)
                do k=szws,ezws
                   do j=syws,eyws
                      do i=sxws,exws
                         coul=d1p2*(cou(i,j,k)+cou(i,j,k-1))
                         tmp1=d1p2*(pres(i,j,k)+pres(i,j,k-1))
                         rovw(i,j,k)=rovw(i,j,k)+coul*(tmp1+ref_pres)/(fluids(2)%r*ref_tp)
                      end do
                   end do
                end do
             end select
             !-------------------------------------------------
             
             !-------------------------------------------------
             ! viscosities (shear & rotational)
             !-------------------------------------------------
             select case(mean_mu)
             case(1)
                do k=sz,ez+1
                   do j=sy,ey+1
                      do i=sx,ex+1
                         !-------------------------------------------------
                         ! plane with z-normal vector
                         !-------------------------------------------------
                         coul=d1p4*(cou(i,j,k)+cou(i,j-1,k)+cou(i-1,j,k)+cou(i-1,j-1,k))
                         vis(i,j,k,3)=2*(coul*fluids(2)%mu+(1-coul)*fluids(1)%mu)
                         vir(i,j,k,3)=coul*fluids(2)%mu+(1-coul)*fluids(1)%mu
                         !-------------------------------------------------
                         ! plane with y-normal vector
                         !-------------------------------------------------
                         coul=d1p4*(cou(i,j,k)+cou(i,j,k-1)+cou(i-1,j,k)+cou(i-1,j,k-1))
                         vis(i,j,k,2)=2*(coul*fluids(2)%mu+(1-coul)*fluids(1)%mu)
                         vir(i,j,k,2)=coul*fluids(2)%mu+(1-coul)*fluids(1)%mu
                         !-------------------------------------------------
                         ! plane with x-normal vector
                         !-------------------------------------------------
                         coul=d1p4*(cou(i,j,k)+cou(i,j,k-1)+cou(i,j-1,k)+cou(i,j-1,k-1))
                         vis(i,j,k,1)=2*(coul*fluids(2)%mu+(1-coul)*fluids(1)%mu)
                         vir(i,j,k,1)=coul*fluids(2)%mu+(1-coul)*fluids(1)%mu
                      end do
                   end do
                end do
             case(2)
                do k=sz,ez+1
                   do j=sy,ey+1
                      do i=sx,ex+1
                         !-------------------------------------------------
                         ! plane with z-normal vector
                         !-------------------------------------------------
                         coul=d1p4*(cou(i,j,k)+cou(i,j-1,k)+cou(i-1,j,k)+cou(i-1,j-1,k))
                         vis(i,j,k,3)=2*fluids(1)%mu*fluids(2)%mu/(coul*fluids(1)%mu+(1-coul)*fluids(2)%mu)
                         vir(i,j,k,3)=fluids(1)%mu*fluids(2)%mu/(coul*fluids(1)%mu+(1-coul)*fluids(2)%mu)
                         !-------------------------------------------------
                         ! plane with y-normal vector
                         !-------------------------------------------------
                         coul=d1p4*(cou(i,j,k)+cou(i,j,k-1)+cou(i-1,j,k)+cou(i-1,j,k-1))
                         vis(i,j,k,2)=2*fluids(1)%mu*fluids(2)%mu/(coul*fluids(1)%mu+(1-coul)*fluids(2)%mu)
                         vir(i,j,k,2)=fluids(1)%mu*fluids(2)%mu/(coul*fluids(1)%mu+(1-coul)*fluids(2)%mu)
                         !-------------------------------------------------
                         ! plane with x-normal vector
                         !-------------------------------------------------
                         coul=d1p4*(cou(i,j,k)+cou(i,j,k-1)+cou(i,j-1,k)+cou(i,j-1,k-1))
                         vis(i,j,k,1)=2*fluids(1)%mu*fluids(2)%mu/(coul*fluids(1)%mu+(1-coul)*fluids(2)%mu)
                         vir(i,j,k,1)=fluids(1)%mu*fluids(2)%mu/(coul*fluids(1)%mu+(1-coul)*fluids(2)%mu)
                      end do
                   end do
                end do
             case(3)
                do k=sz,ez+1
                   do j=sy,ey+1
                      do i=sx,ex+1
                         !-------------------------------------------------
                         ! plane with z-normal vector
                         !-------------------------------------------------
                         coul=d1p4*(cou(i,j,k)+cou(i,j-1,k)+cou(i-1,j,k)+cou(i-1,j-1,k))
                         if (coul>d1p2) then
                            vis(i,j,k,3)=2*fluids(2)%mu
                            vir(i,j,k,3)=fluids(2)%mu
                         else
                            vis(i,j,k,3)=2*fluids(1)%mu
                            vir(i,j,k,3)=fluids(1)%mu
                         end if
                         !-------------------------------------------------
                         ! plane with y-normal vector
                         !-------------------------------------------------
                         coul=d1p4*(cou(i,j,k)+cou(i,j,k-1)+cou(i-1,j,k)+cou(i-1,j,k-1))
                         if (coul>d1p2) then
                            vis(i,j,k,2)=2*fluids(2)%mu
                            vir(i,j,k,2)=fluids(2)%mu
                         else
                            vis(i,j,k,2)=2*fluids(1)%mu
                            vir(i,j,k,2)=fluids(1)%mu
                         end if
                         !-------------------------------------------------
                         ! plane with x-normal vector
                         !-------------------------------------------------
                         coul=d1p4*(cou(i,j,k)+cou(i,j,k-1)+cou(i,j-1,k)+cou(i,j-1,k-1))
                         if (coul>d1p2) then
                            vis(i,j,k,1)=2*fluids(2)%mu
                            vir(i,j,k,1)=fluids(2)%mu
                         else
                            vis(i,j,k,1)=2*fluids(1)%mu
                            vir(i,j,k,1)=fluids(1)%mu
                         end if
                      end do
                   end do
                end do

             end select
             !-------------------------------------------------
             
          endif
       else
          !write(*,*) 'No two phase flow model selected'
          !stop
       end if
    else
       write(*,*) 'More than 2 phases not done'
       stop
    endif
    !-------------------------------------------------------------------------------
  end subroutine Thermophysics
  !*****************************************************************************************************

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
END module mod_thermophy
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
module mod_Init_Navier
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  use mod_Parameters, only: NS_source_term,NS_linear_term,NS_pressure_pen,&
       & NS_inertial_scheme,dt,AL_it,Euler,xmin,xmax,ymin,ymax,zmin,zmax, &
       & gsx,gex,gsy,gey,gsz,gez,gsxu,gexu,gsyu,geyu,gszu,gezu,           &
       & gsxv,gexv,gsyv,geyv,gszv,gezv,gsxw,gexw,gsyw,geyw,gszw,gezw,     &
       & NS_pen_BC
  use mod_thermophy
  use mod_struct_thermophysics
  use mod_struct_solver

  type boundary_value
     integer               :: type=0
     real(8)               :: uin=0,vin=0,win=0,pin=0
     real(8)               :: r=0,amp=0
     real(8)               :: eps=0,tke=0
     real(8), dimension(3) :: xyz=0
     real(8), dimension(3) :: dxyz2=0
  end type boundary_value

  type faces
     type(boundary_value) :: left,right,bottom,top,backward,forward
  end type faces

  type(faces) :: bc_ns  

  namelist /nml_bc_navier/ bc_ns 


contains

  !*****************************************************************************************************
  subroutine Init_Navier_Stokes(u,v,w,u0,v0,w0,u1,v1,w1,pres,pres0,&
       & div,rho,vie,vis,vir,xit,rovu,rovv,rovw,                   &
       & slvu,slvv,slvw,smvu,smvv,smvw,cou,coub,tp,fluids,ns)
    !***************************************************************************************************

    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable   :: u,v,w,u0,v0,w0,u1,v1,w1
    real(8), dimension(:,:,:), allocatable   :: pres,pres0,div
    real(8), dimension(:,:,:), allocatable   :: rho,vie,xit,rovu,rovv,rovw
    real(8), dimension(:,:,:,:), allocatable :: vis,vir
    real(8), dimension(:,:,:), allocatable   :: slvu,slvv,slvw,smvu,smvv,smvw
    real(8), dimension(:,:,:), allocatable   :: cou,coub
    real(8), dimension(:,:,:), allocatable   :: tp
    type(phase_t), dimension(:), allocatable   :: fluids
    type(solver_ns_t)                          :: ns
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer :: i,j
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Initialization of Navier-Stokes structure
    !-------------------------------------------------------------------------------
    ns%dt=dt
    ns%output%it_lag=0
    allocate(ns%output%it_solver(AL_it))
    allocate(ns%output%res_solver(AL_it))
    allocate(ns%output%div(AL_it))
    allocate(ns%output%divmax(AL_it))
    ns%output%it_solver=0
    ns%output%res_solver=0
    ns%output%div=0
    !-------------------------------------------------------------------------------
    ! Initialization of velocity, pressure, divergence
    !-------------------------------------------------------------------------------
    u=0
    v=0
    if (dim==3) w=0
    u0=u
    v0=v
    if (dim==3) w0=w
    if (abs(Euler)==2) then
       u1=u
       v1=v
       if (dim==3) w1=w
    end if
    pres=0
    if (NS_init_pre.and.FT_activate) then
       do j=sy,ey
          do i=sx,ex
             if (coub(i,j,1)>=d1p2) pres(i,j,1)=1
          end do
       end do
    end if
    pres0=pres
    div=0
    !-------------------------------------------------------------------------------
    ! Initialization of molecular fluid characteristics 
    !-------------------------------------------------------------------------------
    if (FT_activate) then
       call thermophysics(rho,rovu,rovv,rovw,vie,vis,vir,xit,coub,pres,tp,fluids)
    else
       call thermophysics(rho,rovu,rovv,rovw,vie,vis,vir,xit,cou,pres,tp,fluids)
    end if
    !-------------------------------------------------------------------------------
    ! Initialization of specific physical terms
    !-------------------------------------------------------------------------------
    if (NS_linear_term) then
       slvu=0
       slvv=0
       if (dim==3) slvw=0
    endif
    if (NS_source_term) then
       smvu=0
       smvv=0
       if (dim==3) smvw=0
    endif
    !-------------------------------------------------------------------------------
  end subroutine Init_Navier_Stokes
  !*****************************************************************************************************

  !*****************************************************************************************************
  subroutine Init_BC_Navier_Stokes(uin,vin,win,bip,pin,penu,penv,penw,ns) 
    !***************************************************************************************************
    use mod_Parameters, only: Periodic,nx,ny,nz,sx,ex,sy,ey,sz,ez,                  &
         & sxu,exu,syu,eyu,szu,ezu,sxv,exv,syv,eyv,szv,ezv,sxw,exw,syw,eyw,szw,ezw, &
         & NS_pressure_pen,NS_TimeBC,dx,dy,dz,dx2,dy2,dz2,pi
    use mod_Constants, only: d1p2,d3p4,d1p4,zero
    use mod_struct_solver
    use mod_CoeffVS
    use mod_mpi

    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable   :: uin,vin,win,bip,pin
    real(8), dimension(:,:,:,:), allocatable :: penu,penv,penw
    type(solver_ns_t)                          :: ns
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                  :: i,j,k
    real(8)                                  :: half_pen,full_pen
    real(8)                                  :: var1,var1l,var2,var2l
    real(8)                                  :: vel_in,tanh_coeff=0.25d0
    real(8)                                  :: x,y,z,r,r2,rr,dn
    logical                                  :: once=.false.
    !-------------------------------------------------------------------------------
    integer                                  :: ip,n_points
    real(8), allocatable, dimension(:), save :: x_exp,y_exp,z_exp
    real(8), allocatable, dimension(:), save :: u_exp,v_exp,w_exp
    !-------------------------------------------------------------------------------
    
    !-------------------------------------------------------------------------------
    ! CoeffV and CoeffS initialization
    !-------------------------------------------------------------------------------
    call InitCoeffVS
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! penalisation values
    !-------------------------------------------------------------------------------
    full_pen=NS_pen_BC
    half_pen=d1p2*NS_pen_BC
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! reading BC
    ! left, right, bottom, top, backward, forward
    !    0: Dirichlet
    !    1: Neumann
    !    2: Symmetry
    !    3: Sliding
    !    4: Periodic
    !    5: Outflow
    !-------------------------------------------------------------------------------
    open(10,file="data.in",action="read")
    read(10,nml=nml_bc_navier) ; rewind(10)
    close(10)

    !-------------------------------------------------------------------------------
    ! check and force periodicity
    !-------------------------------------------------------------------------------
    if (Periodic(1)) then
       bc_ns%left%type=4
       bc_ns%right%type=4
    end if
    if (Periodic(2)) then
       bc_ns%bottom%type=4
       bc_ns%top%type=4
    end if
    if (Periodic(3)) then
       bc_ns%backward%type=4
       bc_ns%forward%type=4
    end if
    !-------------------------------------------------------------------------------
    ns%bound%left=bc_ns%left%type
    ns%bound%right=bc_ns%right%type
    ns%bound%bottom=bc_ns%bottom%type
    ns%bound%top=bc_ns%top%type
    ns%bound%backward=bc_ns%backward%type
    ns%bound%forward=bc_ns%forward%type
    !-------------------------------------------------------------------------------
    ! Time dependent BC
    !-------------------------------------------------------------------------------
    if (ns%bound%left<0.or.ns%bound%right<0.or.    &
         & ns%bound%bottom<0.or.ns%bound%top<0.or. &
         & ns%bound%backward<0.or.ns%bound%forward<0) then
       NS_TimeBC=.true.
    end if
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Order of imposition of velocity boundary conditions
    !
    ! Periodic first
    ! Neumann
    ! Outflow
    ! Sliding --> dirichlet with a non zero velocity
    ! Dirichlet
    !-------------------------------------------------------------------------------
    penu=0
    penv=0
    uin=0
    vin=0
    if (dim==3) then
       penw=0
       win=0
    end if
    !-------------------------------------------------------------------------------


    !-------------------------------------------------------------------------------
    ! general teatment (faces)
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! periodic --> nothing to do
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! neumann --> normal components
    !-------------------------------------------------------------------------------
    ! left 
    !-------------------------------------------------------------------------------
    if (ns%bound%left==1) then
       do k=szu,ezu
          do j=syu,eyu
             do i=sxu,exu
                !-------------------------------------------------------------------------------
                ! U component
                !-------------------------------------------------------------------------------
                if (i==gsxu) then
                   penu(i,j,k,1)=-full_pen
                   penu(i,j,k,3)=+full_pen
                end if
             end do
          end do
       end do
    end if
    !-------------------------------------------------------------------------------
    ! right
    !-------------------------------------------------------------------------------
    if (ns%bound%right==1) then
       do k=szu,ezu
          do j=syu,eyu
             do i=sxu,exu
                !-------------------------------------------------------------------------------
                ! U component
                !-------------------------------------------------------------------------------
                if (i==gexu) then
                   penu(i,j,k,1)=+full_pen
                   penu(i,j,k,2)=-full_pen
                end if
             end do
          end do
       end do
    end if
    !-------------------------------------------------------------------------------
    ! bottom
    !-------------------------------------------------------------------------------
    if (ns%bound%bottom==1) then
       do k=szv,ezv
          do j=syv,eyv
             do i=sxv,exv
                !-------------------------------------------------------------------------------
                ! V component
                !-------------------------------------------------------------------------------
                if (j==gsyv) then
                   penv(i,j,k,1)=-full_pen
                   penv(i,j,k,5)=+full_pen
                end if
             end do
          end do
       end do
    end if
    !-------------------------------------------------------------------------------
    ! top
    !-------------------------------------------------------------------------------
    if (ns%bound%top==1) then
       do k=szv,ezv
          do j=syv,eyv
             do i=sxv,exv
                !-------------------------------------------------------------------------------
                ! V component
                !-------------------------------------------------------------------------------
                if (j==geyv) then
                   penv(i,j,k,1)=+full_pen
                   penv(i,j,k,4)=-full_pen
                end if
             end do
          end do
       end do
    end if
    if (dim==3) then 
       !-------------------------------------------------------------------------------
       ! backward
       !-------------------------------------------------------------------------------
       if (ns%bound%backward==1) then
          do k=szw,ezw
             do j=syw,eyw
                do i=sxw,exw
                   !-------------------------------------------------------------------------------
                   ! W component
                   !-------------------------------------------------------------------------------
                   if (k==gszw) then
                      penw(i,j,k,1)=-full_pen
                      penw(i,j,k,7)=+full_pen
                   end if
                end do
             end do
          end do
       end if
       !-------------------------------------------------------------------------------
       ! forward
       !-------------------------------------------------------------------------------
       if (ns%bound%forward==1) then
          do k=szw,ezw
             do j=syw,eyw
                do i=sxw,exw
                   !-------------------------------------------------------------------------------
                   ! W component
                   !-------------------------------------------------------------------------------
                   if (k==gezw) then
                      penw(i,j,k,1)=+full_pen
                      penw(i,j,k,6)=-full_pen
                   end if
                end do
             end do
          end do
       end if
    end if
    !-------------------------------------------------------------------------------
    ! end neumann
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! outflow --> neumann normal components, dirichlet tengential components
    !-------------------------------------------------------------------------------
    ! left 
    !-------------------------------------------------------------------------------
    if (ns%bound%left==5) then
       do k=szu,ezu
          do j=syu,eyu
             do i=sxu,exu
                !-------------------------------------------------------------------------------
                ! U component
                !-------------------------------------------------------------------------------
                if (i==gsxu) then
                   penu(i,j,k,1)=-full_pen
                   penu(i,j,k,3)=+full_pen
                end if
             end do
          end do
       end do
       do k=szv,ezv
          do j=syv,eyv
             do i=sxv,exv
                !-------------------------------------------------------------------------------
                ! V component
                !-------------------------------------------------------------------------------
                if (i==gsxv) then
                   penv(i,j,k,1)=full_pen
                end if
             end do
          end do
       end do
       if (dim==3) then
          do k=szw,ezw
             do j=syw,eyw
                do i=sxw,exw
                   !-------------------------------------------------------------------------------
                   ! W component
                   !-------------------------------------------------------------------------------
                   if (i==gsxw) then
                      penw(i,j,k,1)=full_pen
                   end if
                end do
             end do
          end do
       end if
    end if
    !-------------------------------------------------------------------------------
    ! right
    !-------------------------------------------------------------------------------
    if (ns%bound%right==5) then
       do k=szu,ezu
          do j=syu,eyu
             do i=sxu,exu
                !-------------------------------------------------------------------------------
                ! U component
                !-------------------------------------------------------------------------------
                if (i==gexu) then
                   penu(i,j,k,1)=+full_pen
                   penu(i,j,k,2)=-full_pen
                end if
             end do
          end do
       end do
       do k=szv,ezv
          do j=syv,eyv
             do i=sxv,exv
                !-------------------------------------------------------------------------------
                ! V component
                !-------------------------------------------------------------------------------
                if (i==gexv) then
                   penv(i,j,k,1)=full_pen
                end if
             end do
          end do
       end do
       if (dim==3) then
          do k=szw,ezw
             do j=syw,eyw
                do i=sxw,exw
                   !-------------------------------------------------------------------------------
                   ! W component
                   !-------------------------------------------------------------------------------
                   if (i==gexw) then
                      penw(i,j,k,1)=full_pen
                   end if
                end do
             end do
          end do
       end if
    end if
    !-------------------------------------------------------------------------------
    ! bottom
    !-------------------------------------------------------------------------------
    if (ns%bound%bottom==5) then
       do k=szu,ezu
          do j=syu,eyu
             do i=sxu,exu
                !-------------------------------------------------------------------------------
                ! U component
                !-------------------------------------------------------------------------------
                if (j==gsyu) then
                   penu(i,j,k,1)=full_pen
                end if
             end do
          end do
       end do
       do k=szv,ezv
          do j=syv,eyv
             do i=sxv,exv
                !-------------------------------------------------------------------------------
                ! V component
                !-------------------------------------------------------------------------------
                if (j==gsyv) then
                   penv(i,j,k,1)=-full_pen
                   penv(i,j,k,5)=+full_pen
                end if
             end do
          end do
       end do
       if (dim==3) then
          do k=szw,ezw
             do j=syw,eyw
                do i=sxw,exw
                   !-------------------------------------------------------------------------------
                   ! W component
                   !-------------------------------------------------------------------------------
                   if (j==gsyw) then
                      penw(i,j,k,1)=full_pen
                   end if
                end do
             end do
          end do
       end if
    end if
    !-------------------------------------------------------------------------------
    ! top
    !-------------------------------------------------------------------------------
    if (ns%bound%top==5) then
       do k=szu,ezu
          do j=syu,eyu
             do i=sxu,exu
                !-------------------------------------------------------------------------------
                ! U component
                !-------------------------------------------------------------------------------
                if (j==geyu) then
                   penu(i,j,k,1)=full_pen
                end if
             end do
          end do
       end do
       do k=szv,ezv
          do j=syv,eyv
             do i=sxv,exv
                !-------------------------------------------------------------------------------
                ! V component
                !-------------------------------------------------------------------------------
                if (j==geyv) then
                   penv(i,j,k,1)=+full_pen
                   penv(i,j,k,4)=-full_pen
                end if
             end do
          end do
       end do
       if (dim==3) then 
          do k=szw,ezw
             do j=syw,eyw
                do i=sxw,exw
                   !-------------------------------------------------------------------------------
                   ! W component
                   !-------------------------------------------------------------------------------
                   if (j==geyw) then
                      penw(i,j,k,1)=+full_pen
                   end if
                end do
             end do
          end do
       end if
    end if
    if (dim==3) then 
       !-------------------------------------------------------------------------------
       ! backward
       !-------------------------------------------------------------------------------
       if (ns%bound%backward==5) then
          do k=szu,ezu
             do j=syu,eyu
                do i=sxu,exu
                   !-------------------------------------------------------------------------------
                   ! U component
                   !-------------------------------------------------------------------------------
                   if (k==gszu) then
                      penu(i,j,k,1)=full_pen
                   end if
                end do
             end do
          end do
          do k=szv,ezv
             do j=syv,eyv
                do i=sxv,exv
                   !-------------------------------------------------------------------------------
                   ! V component
                   !-------------------------------------------------------------------------------
                   if (k==gszv) then
                      penv(i,j,k,1)=full_pen
                   end if
                end do
             end do
          end do
          do k=szw,ezw
             do j=syw,eyw
                do i=sxw,exw
                   !-------------------------------------------------------------------------------
                   ! W component
                   !-------------------------------------------------------------------------------
                   if (k==gszw) then
                      penw(i,j,k,1)=-full_pen
                      penw(i,j,k,7)=+full_pen
                   end if
                end do
             end do
          end do
       end if
       !-------------------------------------------------------------------------------
       ! forward
       !-------------------------------------------------------------------------------
       if (ns%bound%forward==5) then
          do k=szu,ezu
             do j=syu,eyu
                do i=sxu,exu
                   !-------------------------------------------------------------------------------
                   ! U component
                   !-------------------------------------------------------------------------------
                   if (k==gezu) then
                      penu(i,j,k,1)=full_pen
                   end if
                end do
             end do
          end do
          do k=szv,ezv
             do j=syv,eyv
                do i=sxv,exv
                   !-------------------------------------------------------------------------------
                   ! V component
                   !-------------------------------------------------------------------------------
                   if (k==gezv) then
                      penv(i,j,k,1)=full_pen
                   end if
                end do
             end do
          end do
          do k=szw,ezw
             do j=syw,eyw
                do i=sxw,exw
                   !-------------------------------------------------------------------------------
                   ! W component
                   !-------------------------------------------------------------------------------
                   if (k==gezw) then
                      penw(i,j,k,1)=+full_pen
                      penw(i,j,k,6)=-full_pen
                   end if
                end do
             end do
          end do
       end if
    end if
    !-------------------------------------------------------------------------------
    ! end outflow
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! symetry --> normal components: dirichlet
    !             tangential components: neumann
    !-------------------------------------------------------------------------------
    ! left 
    !-------------------------------------------------------------------------------
    if (ns%bound%left==2) then
       do k=szu,ezu
          do j=syu,eyu
             do i=sxu,exu
                !-------------------------------------------------------------------------------
                ! U component
                !-------------------------------------------------------------------------------
                if (i==gsxu) then
                   penu(i,j,k,:)=0
                   penu(i,j,k,1)=half_pen
                   penu(i,j,k,3)=half_pen
                end if
             end do
          end do
       end do
    end if
    !-------------------------------------------------------------------------------
    ! right
    !-------------------------------------------------------------------------------
    if (ns%bound%right==2) then
       do k=szu,ezu
          do j=syu,eyu
             do i=sxu,exu
                !-------------------------------------------------------------------------------
                ! U component
                !-------------------------------------------------------------------------------
                if (i==gexu) then
                   penu(i,j,k,:)=0
                   penu(i,j,k,1)=half_pen
                   penu(i,j,k,2)=half_pen
                end if
             end do
          end do
       end do
    end if
    !-------------------------------------------------------------------------------
    ! bottom
    !-------------------------------------------------------------------------------
    if (ns%bound%bottom==2) then
       do k=szv,ezv
          do j=syv,eyv
             do i=sxv,exv
                !-------------------------------------------------------------------------------
                ! V component
                !-------------------------------------------------------------------------------
                if (j==gsyv) then
                   penv(i,j,k,:)=0
                   penv(i,j,k,1)=half_pen
                   penv(i,j,k,5)=half_pen
                end if
             end do
          end do
       end do
    end if
    !-------------------------------------------------------------------------------
    ! top
    !-------------------------------------------------------------------------------
    if (ns%bound%top==2) then
       do k=szv,ezv
          do j=syv,eyv
             do i=sxv,exv
                !-------------------------------------------------------------------------------
                ! V component
                !-------------------------------------------------------------------------------
                if (j==geyv) then
                   penv(i,j,k,:)=0
                   penv(i,j,k,1)=half_pen
                   penv(i,j,k,4)=half_pen
                end if
             end do
          end do
       end do
    end if
    if (dim==3) then 
       !-------------------------------------------------------------------------------
       ! backward
       !-------------------------------------------------------------------------------
       if (ns%bound%backward==2) then
          do k=szw,ezw
             do j=syw,eyw
                do i=sxw,exw
                   !-------------------------------------------------------------------------------
                   ! W component
                   !-------------------------------------------------------------------------------
                   if (k==gszw) then
                      penw(i,j,k,:)=0
                      penw(i,j,k,1)=half_pen
                      penw(i,j,k,7)=half_pen
                   end if
                end do
             end do
          end do
       end if
       !-------------------------------------------------------------------------------
       ! forward
       !-------------------------------------------------------------------------------
       if (ns%bound%forward==2) then
          do k=szw,ezw
             do j=syw,eyw
                do i=sxw,exw
                   !-------------------------------------------------------------------------------
                   ! W component
                   !-------------------------------------------------------------------------------
                   if (k==gezw) then
                      penw(i,j,k,:)=0
                      penw(i,j,k,1)=half_pen
                      penw(i,j,k,6)=half_pen
                   end if
                end do
             end do
          end do
       end if
    end if
    !-------------------------------------------------------------------------------
    ! end symetry
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! sliding (none zero dirichlet) --> normal and tangential components
    !-------------------------------------------------------------------------------
    ! left 
    !-------------------------------------------------------------------------------
    if (ns%bound%left==3) then
       do k=szu,ezu
          do j=syu,eyu
             do i=sxu,exu
                !-------------------------------------------------------------------------------
                ! U component
                !-------------------------------------------------------------------------------
                if (i==gsxu) then
                   penu(i,j,k,:)=0
                   penu(i,j,k,1)=half_pen
                   penu(i,j,k,3)=half_pen
                   uin(i,j,k)=2*bc_ns%left%uin
                end if
             end do
          end do
       end do
       do k=szv,ezv
          do j=syv,eyv
             do i=sxv,exv
                !-------------------------------------------------------------------------------
                ! V component
                !-------------------------------------------------------------------------------
                if (i==gsxv) then
                   penv(i,j,k,:)=0
                   penv(i,j,k,1)=full_pen
                   vin(i,j,k)=bc_ns%left%vin
                end if
             end do
          end do
       end do
       if (dim==3) then
          do k=szw,ezw
             do j=syw,eyw
                do i=sxw,exw
                   !-------------------------------------------------------------------------------
                   ! W component
                   !-------------------------------------------------------------------------------
                   if (i==gsxw) then
                      penw(i,j,k,:)=0
                      penw(i,j,k,1)=full_pen
                      win(i,j,k)=bc_ns%left%win
                   end if
                end do
             end do
          end do
       end if
    end if
    !-------------------------------------------------------------------------------
    ! right
    !-------------------------------------------------------------------------------
    if (ns%bound%right==3) then
       do k=szu,ezu
          do j=syu,eyu
             do i=sxu,exu
                !-------------------------------------------------------------------------------
                ! U component
                !-------------------------------------------------------------------------------
                if (i==gexu) then
                   penu(i,j,k,:)=0
                   penu(i,j,k,1)=half_pen
                   penu(i,j,k,2)=half_pen
                   uin(i,j,k)=2*bc_ns%right%uin
                end if
             end do
          end do
       end do
       do k=szv,ezv
          do j=syv,eyv
             do i=sxv,exv
                !-------------------------------------------------------------------------------
                ! V component
                !-------------------------------------------------------------------------------
                if (i==gexv) then
                   penv(i,j,k,:)=0
                   penv(i,j,k,1)=full_pen
                   vin(i,j,k)=bc_ns%right%vin
                end if
             end do
          end do
       end do
       if (dim==3) then
          do k=szw,ezw
             do j=syw,eyw
                do i=sxw,exw
                   !-------------------------------------------------------------------------------
                   ! W component
                   !-------------------------------------------------------------------------------
                   if (i==gexw) then
                      penw(i,j,k,:)=0
                      penw(i,j,k,1)=full_pen
                      win(i,j,k)=bc_ns%right%win
                   end if
                end do
             end do
          end do
       end if
    end if
    !-------------------------------------------------------------------------------
    ! bottom
    !-------------------------------------------------------------------------------
    if (ns%bound%bottom==3) then
       do k=szu,ezu
          do j=syu,eyu
             do i=sxu,exu
                !-------------------------------------------------------------------------------
                ! U component
                !-------------------------------------------------------------------------------
                if (j==gsyu) then
                   penu(i,j,k,:)=0
                   penu(i,j,k,1)=full_pen
                   uin(i,j,k)=bc_ns%bottom%uin
                end if
             end do
          end do
       end do
       do k=szv,ezv
          do j=syv,eyv
             do i=sxv,exv
                !-------------------------------------------------------------------------------
                ! V component
                !-------------------------------------------------------------------------------
                if (j==gsyv) then
                   penv(i,j,k,:)=0
                   penv(i,j,k,1)=half_pen
                   penv(i,j,k,5)=half_pen
                   vin(i,j,k)=2*bc_ns%bottom%vin
                end if
             end do
          end do
       end do
       if (dim==3) then
          do k=szw,ezw
             do j=syw,eyw
                do i=sxw,exw
                   !-------------------------------------------------------------------------------
                   ! W component
                   !-------------------------------------------------------------------------------
                   if (j==gsyw) then
                      penw(i,j,k,:)=0
                      penw(i,j,k,1)=full_pen
                      win(i,j,k)=bc_ns%bottom%win
                   end if
                end do
             end do
          end do
       end if
    end if
    !-------------------------------------------------------------------------------
    ! top
    !-------------------------------------------------------------------------------
    if (ns%bound%top==3) then
       do k=szu,ezu
          do j=syu,eyu
             do i=sxu,exu
                !-------------------------------------------------------------------------------
                ! U component
                !-------------------------------------------------------------------------------
                if (j==geyu) then
                   penu(i,j,k,:)=0
                   penu(i,j,k,1)=full_pen
                   uin(i,j,k)=bc_ns%top%uin
                end if
             end do
          end do
       end do
       do k=szv,ezv
          do j=syv,eyv
             do i=sxv,exv
                !-------------------------------------------------------------------------------
                ! V component
                !-------------------------------------------------------------------------------
                if (j==geyv) then
                   penv(i,j,k,:)=0
                   penv(i,j,k,1)=half_pen
                   penv(i,j,k,4)=half_pen
                   vin(i,j,k)=2*bc_ns%top%vin
                end if
             end do
          end do
       end do
       if (dim==3) then
          do k=szw,ezw
             do j=syw,eyw
                do i=sxw,exw
                   !-------------------------------------------------------------------------------
                   ! W component
                   !-------------------------------------------------------------------------------
                   if (j==geyw) then
                      penw(i,j,k,:)=0
                      penw(i,j,k,1)=full_pen
                      win(i,j,k)=bc_ns%top%win
                   end if
                end do
             end do
          end do
       end if
    end if
    if (dim==3) then
       !-------------------------------------------------------------------------------
       ! backward
       !-------------------------------------------------------------------------------
       if (ns%bound%backward==3) then
          do k=szu,ezu
             do j=syu,eyu
                do i=sxu,exu
                   !-------------------------------------------------------------------------------
                   ! U component
                   !-------------------------------------------------------------------------------
                   if (k==gszu) then
                      penu(i,j,k,:)=0
                      penu(i,j,k,1)=full_pen
                      uin(i,j,k)=bc_ns%backward%uin
                   end if
                end do
             end do
          end do
          do k=szv,ezv
             do j=syv,eyv
                do i=sxv,exv
                   !-------------------------------------------------------------------------------
                   ! V component
                   !-------------------------------------------------------------------------------
                   if (k==gszv) then
                      penv(i,j,k,:)=0
                      penv(i,j,k,1)=full_pen
                      vin(i,j,k)=bc_ns%backward%vin
                   end if
                end do
             end do
          end do
          do k=szw,ezw
             do j=syw,eyw
                do i=sxw,exw
                   !-------------------------------------------------------------------------------
                   ! W component
                   !-------------------------------------------------------------------------------
                   if (k==gszw) then
                      penw(i,j,k,:)=0
                      penw(i,j,k,1)=half_pen
                      penw(i,j,k,7)=half_pen
                      win(i,j,k)=2*bc_ns%backward%win
                   end if
                end do
             end do
          end do
       end if
       !-------------------------------------------------------------------------------
       ! forward
       !-------------------------------------------------------------------------------
       if (ns%bound%forward==3) then
          do k=szu,ezu
             do j=syu,eyu
                do i=sxu,exu
                   !-------------------------------------------------------------------------------
                   ! U component
                   !-------------------------------------------------------------------------------
                   if (k==gezu) then
                      penu(i,j,k,:)=0
                      penu(i,j,k,1)=full_pen
                      uin(i,j,k)=bc_ns%forward%uin
                   end if
                end do
             end do
          end do
          do k=szv,ezv
             do j=syv,eyv
                do i=sxv,exv
                   !-------------------------------------------------------------------------------
                   ! V component
                   !-------------------------------------------------------------------------------
                   if (k==gezv) then
                      penv(i,j,k,:)=0
                      penv(i,j,k,1)=full_pen
                      vin(i,j,k)=bc_ns%forward%vin
                   end if
                end do
             end do
          end do
          do k=szw,ezw
             do j=syw,eyw
                do i=sxw,exw
                   !-------------------------------------------------------------------------------
                   ! W component
                   !-------------------------------------------------------------------------------
                   if (k==gezw) then
                      penw(i,j,k,:)=0
                      penw(i,j,k,1)=half_pen
                      penw(i,j,k,6)=half_pen
                      win(i,j,k)=2*bc_ns%forward%win
                   end if
                end do
             end do
          end do
       end if
    end if
    !-------------------------------------------------------------------------------
    ! end sliding
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! dirichlet --> normal and tangential components
    !-------------------------------------------------------------------------------
    ! left 
    !-------------------------------------------------
    if (ns%bound%left==0) then
       do k=szu,ezu
          do j=syu,eyu
             do i=sxu,exu
                !-------------------------------------------------
                ! U component
                !-------------------------------------------------
                if (i==gsxu) then
                   penu(i,j,k,:)=0
                   penu(i,j,k,1)=half_pen
                   penu(i,j,k,3)=half_pen
                   uin(i-1:i+1,j,k)=0
                   uin(i,j-1:j+1,k)=0
                   if (dim==3) uin(i,j,k-1:k+1)=0
                end if
             end do
          end do
       end do
       do k=szv,ezv
          do j=syv,eyv
             do i=sxv,exv
                !-------------------------------------------------
                ! V component
                !-------------------------------------------------
                if (i==gsxv) then
                   penv(i,j,k,:)=0
                   penv(i,j,k,1)=full_pen
                   vin(i-1:i+1,j,k)=0
                   vin(i,j-1:j+1,k)=0
                   if (dim==3) vin(i,j,k-1:k+1)=0
                end if
             end do
          end do
       end do
       if (dim==3) then
          do k=szw,ezw
             do j=syw,eyw
                do i=sxw,exw
                   !-------------------------------------------------
                   ! W component
                   !-------------------------------------------------
                   if (i==gsxw) then
                      penw(i,j,k,:)=0
                      penw(i,j,k,1)=full_pen
                      win(i-1:i+1,j,k)=0
                      win(i,j-1:j+1,k)=0
                      if (dim==3) win(i,j,k-1:k+1)=0
                   end if
                end do
             end do
          end do
       end if
    end if
    !-------------------------------------------------
    ! right
    !-------------------------------------------------
    if (ns%bound%right==0) then
       do k=szu,ezu
          do j=syu,eyu
             do i=sxu,exu
                !-------------------------------------------------
                ! U component
                !-------------------------------------------------
                if (i==gexu) then
                   penu(i,j,k,:)=0
                   penu(i,j,k,1)=half_pen
                   penu(i,j,k,2)=half_pen
                   uin(i-1:i+1,j,k)=0
                   uin(i,j-1:j+1,k)=0
                   if (dim==3) uin(i,j,k-1:k+1)=0
                end if
             end do
          end do
       end do
       do k=szv,ezv
          do j=syv,eyv
             do i=sxv,exv
                !-------------------------------------------------
                ! V component
                !-------------------------------------------------
                if (i==gexv) then
                   penv(i,j,k,:)=0
                   penv(i,j,k,1)=full_pen
                   vin(i-1:i+1,j,k)=0
                   vin(i,j-1:j+1,k)=0
                   if (dim==3) vin(i,j,k-1:k+1)=0
                end if
             end do
          end do
       end do
       if (dim==3) then
          do k=szw,ezw
             do j=syw,eyw
                do i=sxw,exw
                   !-------------------------------------------------
                   ! W component
                   !-------------------------------------------------
                   if (i==gexw) then
                      penw(i,j,k,:)=0
                      penw(i,j,k,1)=full_pen
                      win(i-1:i+1,j,k)=0
                      win(i,j-1:j+1,k)=0
                      if (dim==3) win(i,j,k-1:k+1)=0
                   end if
                end do
             end do
          end do
       end if
    end if
    !-------------------------------------------------
    ! bottom
    !-------------------------------------------------
    if (ns%bound%bottom==0) then
       do k=szu,ezu
          do j=syu,eyu
             do i=sxu,exu
                !-------------------------------------------------
                ! U component
                !-------------------------------------------------
                if (j==gsyu) then
                   penu(i,j,k,:)=0
                   penu(i,j,k,1)=full_pen
                   uin(i-1:i+1,j,k)=0
                   uin(i,j-1:j+1,k)=0
                   if (dim==3) uin(i,j,k-1:k+1)=0
                end if
             end do
          end do
       end do
       do k=szv,ezv
          do j=syv,eyv
             do i=sxv,exv
                !-------------------------------------------------
                ! V component
                !-------------------------------------------------
                if (j==gsyv) then
                   penv(i,j,k,:)=0
                   penv(i,j,k,1)=half_pen
                   penv(i,j,k,5)=half_pen
                   vin(i-1:i+1,j,k)=0
                   vin(i,j-1:j+1,k)=0
                   if (dim==3) vin(i,j,k-1:k+1)=0
                end if
             end do
          end do
       end do
       if (dim==3) then
          do k=szw,ezw
             do j=syw,eyw
                do i=sxw,exw
                   !-------------------------------------------------
                   ! W component
                   !-------------------------------------------------
                   if (j==gsyw) then
                      penw(i,j,k,:)=0
                      penw(i,j,k,1)=full_pen
                      win(i-1:i+1,j,k)=0
                      win(i,j-1:j+1,k)=0
                      if (dim==3) win(i,j,k-1:k+1)=0
                   end if
                end do
             end do
          end do
       end if
    end if
    !-------------------------------------------------
    ! top
    !-------------------------------------------------
    if (ns%bound%top==0) then
       do k=szu,ezu
          do j=syu,eyu
             do i=sxu,exu
                !-------------------------------------------------
                ! U component
                !-------------------------------------------------
                if (j==geyu) then
                   penu(i,j,k,:)=0
                   penu(i,j,k,1)=full_pen
                   uin(i-1:i+1,j,k)=0
                   uin(i,j-1:j+1,k)=0
                   if (dim==3) uin(i,j,k-1:k+1)=0
                end if
             end do
          end do
       end do
       do k=szv,ezv
          do j=syv,eyv
             do i=sxv,exv
                !-------------------------------------------------
                ! V component
                !-------------------------------------------------
                if (j==geyv) then
                   penv(i,j,k,:)=0
                   penv(i,j,k,1)=half_pen
                   penv(i,j,k,4)=half_pen
                   vin(i-1:i+1,j,k)=0
                   vin(i,j-1:j+1,k)=0
                   if (dim==3) vin(i,j,k-1:k+1)=0
                end if
             end do
          end do
       end do
       if (dim==3) then
          do k=szw,ezw
             do j=syw,eyw
                do i=sxw,exw
                   !-------------------------------------------------
                   ! W component
                   !-------------------------------------------------
                   if (j==geyw) then
                      penw(i,j,k,:)=0
                      penw(i,j,k,1)=full_pen
                      win(i-1:i+1,j,k)=0
                      win(i,j-1:j+1,k)=0
                      if (dim==3) win(i,j,k-1:k+1)=0
                   end if
                end do
             end do
          end do
       end if
    end if
    if (dim==3) then
       !-------------------------------------------------
       ! backward
       !-------------------------------------------------
       if (ns%bound%backward==0) then
          do k=szu,ezu
             do j=syu,eyu
                do i=sxu,exu
                   !-------------------------------------------------
                   ! U component
                   !-------------------------------------------------
                   if (k==gszu) then
                      penu(i,j,k,:)=0
                      penu(i,j,k,1)=full_pen
                      uin(i-1:i+1,j,k)=0
                      uin(i,j-1:j+1,k)=0
                      if (dim==3) uin(i,j,k-1:k+1)=0
                   end if
                end do
             end do
          end do
          do k=szv,ezv
             do j=syv,eyv
                do i=sxv,exv
                   !-------------------------------------------------
                   ! V component
                   !-------------------------------------------------
                   if (k==gszv) then
                      penv(i,j,k,:)=0
                      penv(i,j,k,1)=full_pen
                      vin(i-1:i+1,j,k)=0
                      vin(i,j-1:j+1,k)=0
                      if (dim==3) vin(i,j,k-1:k+1)=0
                   end if
                end do
             end do
          end do
          do k=szw,ezw
             do j=syw,eyw
                do i=sxw,exw
                   !-------------------------------------------------
                   ! W component
                   !-------------------------------------------------
                   if (k==gszw) then
                      penw(i,j,k,:)=0
                      penw(i,j,k,1)=half_pen
                      penw(i,j,k,7)=half_pen
                      win(i-1:i+1,j,k)=0
                      win(i,j-1:j+1,k)=0
                      if (dim==3) win(i,j,k-1:k+1)=0
                   end if
                end do
             end do
          end do
       end if
       !-------------------------------------------------
       ! forward
       !-------------------------------------------------
       if (ns%bound%forward==0) then
          do k=szu,ezu
             do j=syu,eyu
                do i=sxu,exu
                   !-------------------------------------------------
                   ! U component
                   !-------------------------------------------------
                   if (k==gezu) then
                      penu(i,j,k,:)=0
                      penu(i,j,k,1)=full_pen
                      uin(i-1:i+1,j,k)=0
                      uin(i,j-1:j+1,k)=0
                      if (dim==3) uin(i,j,k-1:k+1)=0
                   end if
                end do
             end do
          end do
          do k=szv,ezv
             do j=syv,eyv
                do i=sxv,exv
                   !-------------------------------------------------
                   ! V component
                   !-------------------------------------------------
                   if (k==gezv) then
                      penv(i,j,k,:)=0
                      penv(i,j,k,1)=full_pen
                      vin(i-1:i+1,j,k)=0
                      vin(i,j-1:j+1,k)=0
                      if (dim==3) vin(i,j,k-1:k+1)=0
                   end if
                end do
             end do
          end do
          do k=szw,ezw
             do j=syw,eyw
                do i=sxw,exw
                   !-------------------------------------------------
                   ! W component
                   !-------------------------------------------------
                   if (k==gezw) then
                      penw(i,j,k,:)=0
                      penw(i,j,k,1)=half_pen
                      penw(i,j,k,6)=half_pen
                      win(i-1:i+1,j,k)=0
                      win(i,j-1:j+1,k)=0
                      if (dim==3) win(i,j,k-1:k+1)=0
                   end if
                end do
             end do
          end do
       end if
    end if
    !-------------------------------------------------------------------------------
    ! end dirichlet
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------


    !-------------------------------------------------------------------------------
    ! Force Compatibility conditions
    !-------------------------------------------------------------------------------
    do k=sz,ez
       do j=sy,ey
          do i=sx,ex

             if (dim==2) then
                if (i==gsx.and.j==gsy) then                   
                   if (ns%bound%left==3.and.ns%bound%bottom==3) then
                      penu(i,j,k,:)=0
                      penu(i,j,k,1)=half_pen
                      penu(i,j,k,3)=half_pen
                      uin(i,j,k)=0
                      penv(i,j,k,:)=0
                      penv(i,j,k,1)=half_pen
                      penv(i,j,k,5)=half_pen
                      vin(i,j,k)=0
                      write(*,*) "WARNING : COMPATIBLITY CONDITIONS HAVE BEEN FORCED AT (i,j)=",i,j
                   end if
                else if (i==gex.and.j==gsy) then   
                   if (ns%bound%right==3.and.ns%bound%bottom==3) then 
                      penu(i+1,j,k,:)=0
                      penu(i+1,j,k,1)=half_pen
                      penu(i+1,j,k,2)=half_pen
                      uin(i+1,j,k)=0
                      penv(i,j,k,:)=0
                      penv(i,j,k,1)=half_pen
                      penv(i,j,k,5)=half_pen
                      vin(i,j,k)=0
                      write(*,*) "WARNING : COMPATIBLITY CONDITIONS HAVE BEEN FORCED AT (i,j)=",i,j
                   end if
                else if (i==gsx.and.j==gey) then  
                   if (ns%bound%left==3.and.ns%bound%top==3) then 
                      penu(i,j,k,:)=0
                      penu(i,j,k,1)=half_pen
                      penu(i,j,k,3)=half_pen
                      uin(i,j,k)=0
                      penv(i,j+1,k,:)=0
                      penv(i,j+1,k,1)=half_pen
                      penv(i,j+1,k,4)=half_pen
                      vin(i,j+1,k)=0
                      write(*,*) "WARNING : COMPATIBLITY CONDITIONS HAVE BEEN FORCED AT (i,j)=",i,j
                   end if
                else if (i==gex.and.j==gey) then 
                   if (ns%bound%right==3.and.ns%bound%top==3) then 
                      penu(i+1,j,k,:)=0
                      penu(i+1,j,k,1)=half_pen
                      penu(i+1,j,k,2)=half_pen
                      uin(i+1,j,k)=0
                      penv(i,j+1,k,:)=0
                      penv(i,j+1,k,1)=half_pen
                      penv(i,j+1,k,4)=half_pen
                      vin(i,j+1,k)=0
                      write(*,*) "WARNING : COMPATIBLITY CONDITIONS HAVE BEEN FORCED AT (i,j)=",i,j
                   end if
                end if
             else
                ! stop "3D TODO"
             end if
             
          end do
       end do
    end do
    
    !-------------------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------------------





    ! ******************************************************************************
    ! ******************************************************************************
    ! ******************************************************************************
    ! ******************************************************************************
    !                     specific boundary conditions
    ! ******************************************************************************
    ! ******************************************************************************
    ! ******************************************************************************
    ! ******************************************************************************



    !-------------------------------------------------------------------------------
    ! cylindrical inlet
    !-------------------------------------------------------------------------------
    ! 10,-100  : flat, -100 with vortex method
    ! 11,-110  : Poiseulle (2D/3D), -110 with vortex method
    ! 12,-120  : tanh with tanh_coeff variation, -120 with vortex method
    ! 13,-130  : input profile (INRS case SVC)
    !-------------------------------------------------------------------------------
    ! left
    !-------------------------------------------------------------------------------
    select case(abs(ns%bound%left))
    case(10,11,12,13,100,110,120,130)
       var1l=0;var2l=0
       do k=szu,ezu
          do j=syu,eyu
             do i=sxu,exu
                !-------------------------------------------------------------------------------
                ! U component
                !-------------------------------------------------------------------------------
                if (i==gsxu) then
                   penu(i,j,k,:)=0
                   penu(i,j,k,1)=half_pen
                   penu(i,j,k,3)=half_pen
                   rr=(grid_y(j)-bc_ns%left%xyz(2))**2+(grid_z(k)-bc_ns%left%xyz(3))**2
                   if ( rr <= bc_ns%left%r**2 ) then
                      select case(abs(ns%bound%left))
                      case(10,100)
                         vel_in=bc_ns%left%uin
                      case(11,110)
                         vel_in=2*bc_ns%left%uin*(1-rr/bc_ns%left%r**2)
                      case(12,120)
                         vel_in=-tanh((sqrt(rr)-bc_ns%left%r)/(tanh_coeff*bc_ns%left%r))
                      case(13,130)
                         !-------------------------------------------------------------------------------
                         ! read input profile 
                         !-------------------------------------------------------------------------------
                         open(10,file="profile.in",action="read")
                         read(10,*)
                         read(10,*) n_points
                         !-------------------------------------------------------------------------------
                         if (.not.allocated(y_exp)) then 
                            allocate(y_exp(n_points))
                            allocate(z_exp(n_points))
                            allocate(u_exp(n_points))
                            allocate(w_exp(n_points))
                         end if
                         do ip=1,n_points
                            read(10,*) vel_in,y_exp(ip),z_exp(ip),u_exp(ip),vel_in,w_exp(ip)
                            if (abs(y_exp(ip))<tiny(zero)) y_exp(ip)=0
                            if (abs(z_exp(ip))<tiny(zero)) z_exp(ip)=0
                         end do
                         close(10)
                         !-------------------------------------------------------------------------------
                         call interpolation_profil(grid_y(j),grid_z(k),vel_in,y_exp,z_exp,u_exp,n_points)
                         !-------------------------------------------------------------------------------
                      end select
                      var1l=var1l+dy(j)*dz(k)*coeffS(i,j,k)
                      var2l=var2l+vel_in*dy(j)*dz(k)*coeffS(i,j,k)
                      uin(i,j,k)=2*vel_in
                   end if
                end if
             end do
          end do
       end do
       !-------------------------------------------------------------------------------
       ! mean value correction
       !-------------------------------------------------------------------------------
       if (abs(ns%bound%left)/=13.and.abs(ns%bound%left)/=130) then 
          if (dim==2) then
             var1=bc_ns%left%uin*2*bc_ns%left%r
          else
             var1=bc_ns%left%uin*pi*bc_ns%left%r**2
          end if
          call mpi_allreduce(var2l,var2,1,mpi_double_precision,mpi_sum,comm3d,code)
          do k=szu,ezu
             do j=syu,eyu
                do i=sxu,exu
                   if (i==gsxu) then
                      rr=(grid_y(j)-bc_ns%left%xyz(2))**2+(grid_z(k)-bc_ns%left%xyz(3))**2
                      if ( rr <= bc_ns%left%r**2 ) then
                         uin(i,j,k)=uin(i,j,k)*var1/var2
                      end if
                   end if
                end do
             end do
          end do
       end if
       !-------------------------------------------------------------------------------
       do k=szv,ezv
          do j=syv,eyv
             do i=sxv,exv
                !-------------------------------------------------------------------------------
                ! V component
                !-------------------------------------------------------------------------------
                if (i==gsxv) then
                   penv(i,j,k,:)=0
                   penv(i,j,k,1)=full_pen
                end if
             end do
          end do
       end do
       if (dim==3) then
          do k=szw,ezw
             do j=syw,eyw
                do i=sxw,exw
                   !-------------------------------------------------------------------------------
                   ! W component
                   !-------------------------------------------------------------------------------
                   if (i==gsxw) then
                      penw(i,j,k,:)=0
                      penw(i,j,k,1)=full_pen
                   end if
                end do
             end do
          end do
       end if
    end select
    !-------------------------------------------------------------------------------
    ! right
    !-------------------------------------------------------------------------------
    select case(abs(ns%bound%right))
    case(10,11,12,13)
       var1l=0;var2l=0
       do k=szu,ezu
          do j=syu,eyu
             do i=sxu,exu
                !-------------------------------------------------------------------------------
                ! U component
                !-------------------------------------------------------------------------------
                if (i==gexu) then
                   penu(i,j,k,:)=0
                   penu(i,j,k,1)=half_pen
                   penu(i,j,k,2)=half_pen
                   rr=(grid_y(j)-bc_ns%right%xyz(2))**2+(grid_z(k)-bc_ns%right%xyz(3))**2
                   if ( rr <= bc_ns%right%r**2 ) then
                      select case(abs(ns%bound%right))
                      case(10)                         
                         vel_in=bc_ns%right%uin
                      case(11)
                         vel_in=2*bc_ns%right%uin*(1-rr/bc_ns%right%r**2)
                      case(12)
                         vel_in=-tanh((sqrt(rr)-bc_ns%right%r)/(tanh_coeff*bc_ns%right%r))
                      end select
                      var1l=var1l+dy(j)*dz(k)*coeffS(i,j,k)
                      var2l=var2l+vel_in*dy(j)*dz(k)*coeffS(i,j,k)
                      uin(i,j,k)=2*vel_in
                   end if
                end if
             end do
          end do
       end do
       !-------------------------------------------------------------------------------
       ! mean value correction
       !-------------------------------------------------------------------------------
       var1=bc_ns%right%uin*pi*bc_ns%right%r**2
       call mpi_allreduce(var2l,var2,1,mpi_double_precision,mpi_sum,comm3d,code)
       do k=szu,ezu
          do j=syu,eyu
             do i=sxu,exu
                if (i==gexu) then
                   rr=(grid_y(j)-bc_ns%left%xyz(2))**2+(grid_z(k)-bc_ns%left%xyz(3))**2
                   if ( rr <= bc_ns%right%r**2 ) then
                      uin(i,j,k)=uin(i,j,k)*var1/var2
                   end if
                end if
             end do
          end do
       end do
       !-------------------------------------------------------------------------------
       do k=szv,ezv
          do j=syv,eyv
             do i=sxv,exv
                !-------------------------------------------------------------------------------
                ! V component
                !-------------------------------------------------------------------------------
                if (i==gexv) then
                   penv(i,j,k,:)=0
                   penv(i,j,k,1)=full_pen
                end if
             end do
          end do
       end do
       if (dim==3) then
          do k=szw,ezw
             do j=syw,eyw
                do i=sxw,exw
                   !-------------------------------------------------------------------------------
                   ! W component
                   !-------------------------------------------------------------------------------
                   if (i==gexw) then
                      penw(i,j,k,:)=0
                      penw(i,j,k,1)=full_pen
                   end if
                end do
             end do
          end do
       end if
    end select
    !-------------------------------------------------------------------------------
    ! bottom
    !-------------------------------------------------------------------------------
    select case(abs(ns%bound%bottom))
    case(10,11,12,13)
       do k=szu,ezu
          do j=syu,eyu
             do i=sxu,exu
                ! U component
                if (j==gsyu) then
                   penu(i,j,k,:)=0
                   penu(i,j,k,1)=full_pen
                end if
             end do
          end do
       end do
       var1l=0;var2l=0
       do k=szv,ezv
          do j=syv,eyv
             do i=sxv,exv
                !-------------------------------------------------------------------------------
                ! V component
                !-------------------------------------------------------------------------------
                if (j==gsyv) then
                   penv(i,j,k,:)=0
                   penv(i,j,k,1)=half_pen
                   penv(i,j,k,5)=half_pen
                   rr=(grid_x(i)-bc_ns%bottom%xyz(1))**2+(grid_z(k)-bc_ns%bottom%xyz(3))**2
                   if ( rr <= bc_ns%bottom%r**2 ) then
                      select case(abs(ns%bound%bottom))
                      case(10)
                         vel_in=bc_ns%bottom%vin
                      case(11)
                         vel_in=2*bc_ns%bottom%vin*(1-rr/bc_ns%bottom%r**2)
                      case(12)
                         vel_in=-tanh((sqrt(rr)-bc_ns%bottom%r)/(tanh_coeff*bc_ns%bottom%r))
                      end select
                      var1l=var1l+dx(i)*dz(k)*coeffS(i,j,k)
                      var2l=var2l+vel_in*dx(i)*dz(k)*coeffS(i,j,k)
                      vin(i,j,k)=2*vel_in
                   end if
                end if
             end do
          end do
       end do
       !-------------------------------------------------------------------------------
       ! mean value correction
       !-------------------------------------------------------------------------------
       var1=bc_ns%bottom%vin*pi*bc_ns%bottom%r**2
       call mpi_allreduce(var2l,var2,1,mpi_double_precision,mpi_sum,comm3d,code)
       do k=szv,ezv
          do j=syv,eyv
             do i=sxv,exv
                if (j==gsyv) then
                   rr=(grid_x(i)-bc_ns%bottom%xyz(1))**2+(grid_z(k)-bc_ns%bottom%xyz(3))**2
                   if ( rr <= bc_ns%bottom%r**2 ) then
                      vin(i,j,k)=vin(i,j,k)*var1/var2
                   end if
                end if
             end do
          end do
       end do
       !-------------------------------------------------------------------------------
       if (dim==3) then
          do k=szw,ezw
             do j=syw,eyw
                do i=sxw,exw
                   !-------------------------------------------------------------------------------
                   ! W component
                   !-------------------------------------------------------------------------------
                   if (j==gsyw) then
                      penw(i,j,k,:)=0
                      penw(i,j,k,1)=full_pen
                   end if
                end do
             end do
          end do
       end if
    end select
    !-------------------------------------------------------------------------------
    ! top
    !-------------------------------------------------------------------------------
    select case(abs(ns%bound%top))
    case(10,11,12,13)
       do k=szu,ezu
          do j=syu,eyu
             do i=sxu,exu
                !-------------------------------------------------------------------------------
                ! U component
                !-------------------------------------------------------------------------------
                if (j==geyu) then
                   penu(i,j,k,:)=0
                   penu(i,j,k,1)=full_pen
                end if
             end do
          end do
       end do
       var1l=0;var2l=0
       do k=szv,ezv
          do j=syv,eyv
             do i=sxv,exv
                !-------------------------------------------------------------------------------
                ! V component
                !-------------------------------------------------------------------------------
                if (j==geyv) then
                   penv(i,j,k,:)=0
                   penv(i,j,k,1)=half_pen
                   penv(i,j,k,4)=half_pen
                   rr=(grid_x(i)-bc_ns%top%xyz(1))**2+(grid_z(k)-bc_ns%top%xyz(3))**2
                   if ( rr <= bc_ns%top%r**2 ) then
                      select case(abs(ns%bound%top))
                      case(10)
                         vel_in=bc_ns%top%vin
                      case(11)
                         vel_in=2*bc_ns%top%vin*(1-rr/bc_ns%top%r**2)
                      case(12)
                         vel_in=-tanh((sqrt(rr)-bc_ns%top%r)/(tanh_coeff*bc_ns%top%r))
                      end select
                      var1l=var1l+dx(i)*dz(k)*coeffS(i,j,k)
                      var2l=var2l+vel_in*dx(i)*dz(k)*coeffS(i,j,k)
                      vin(i,j,k)=2*vel_in
                   end if
                end if
             end do
          end do
       end do
       !-------------------------------------------------------------------------------
       ! mean value correction
       !-------------------------------------------------------------------------------
       var1=bc_ns%top%vin*pi*bc_ns%top%r**2
       call mpi_allreduce(var2l,var2,1,mpi_double_precision,mpi_sum,comm3d,code)
       do k=szv,ezv
          do j=syv,eyv
             do i=sxv,exv
                if (j==geyv) then
                   rr=(grid_x(i)-bc_ns%top%xyz(1))**2+(grid_z(k)-bc_ns%top%xyz(3))**2
                   if ( rr <= bc_ns%top%r**2 ) then
                      vin(i,j,k)=vin(i,j,k)*var1/var2
                   end if
                end if
             end do
          end do
       end do
       !-------------------------------------------------------------------------------
       if (dim==3) then
          do k=szw,ezw
             do j=syw,eyw
                do i=sxw,exw
                   !-------------------------------------------------------------------------------
                   ! W component
                   !-------------------------------------------------------------------------------
                   if (j==geyw) then
                      penw(i,j,k,:)=0
                      penw(i,j,k,1)=full_pen
                   end if
                end do
             end do
          end do
       end if
    end select
    if (dim==3) then
       !-------------------------------------------------------------------------------
       ! backward
       !-------------------------------------------------------------------------------
       select case(abs(ns%bound%backward))
       case(10,11,12,13)
          do k=szu,ezu
             do j=syu,eyu
                do i=sxu,exu
                   !-------------------------------------------------------------------------------
                   ! U component
                   !-------------------------------------------------------------------------------
                   if (k==gszu) then
                      penu(i,j,k,:)=0
                      penu(i,j,k,1)=full_pen
                   end if
                end do
             end do
          end do
          do k=szv,ezv
             do j=syv,eyv
                do i=sxv,exv
                   !-------------------------------------------------------------------------------
                   ! V component
                   !-------------------------------------------------------------------------------
                   if (k==gszv) then
                      penv(i,j,k,:)=0
                      penv(i,j,k,1)=full_pen
                   end if
                end do
             end do
          end do
          var1l=0;var2l=0
          do k=szw,ezw
             do j=syw,eyw
                do i=sxw,exw
                   !-------------------------------------------------------------------------------
                   ! W component
                   !-------------------------------------------------------------------------------
                   if (k==gszw) then
                      penw(i,j,k,:)=0
                      penw(i,j,k,1)=half_pen
                      penw(i,j,k,7)=half_pen
                      rr=(grid_x(i)-bc_ns%backward%xyz(1))**2+(grid_y(j)-bc_ns%backward%xyz(2))**2
                      if ( rr <= bc_ns%backward%r**2 ) then
                         select case(abs(ns%bound%backward))
                         case(10)
                            vel_in=bc_ns%backward%win
                         case(11)
                            vel_in=2*bc_ns%backward%win*(1-rr/bc_ns%backward%r**2)
                         case(12)
                            vel_in=-tanh((sqrt(rr)-bc_ns%backward%r)/(tanh_coeff*bc_ns%backward%r))
                         end select
                         var1l=var1l+dx(i)*dy(j)*coeffS(i,j,k)
                         var2l=var2l+vel_in*dx(i)*dy(j)*coeffS(i,j,k)
                         win(i,j,k)=2*vel_in
                      end if
                   end if
                end do
             end do
          end do
          !-------------------------------------------------------------------------------
          ! mean value correction
          !-------------------------------------------------------------------------------
          var1=bc_ns%backward%win*pi*bc_ns%backward%r**2
          call mpi_allreduce(var2l,var2,1,mpi_double_precision,mpi_sum,comm3d,code)
          do k=szw,ezw
             do j=syw,eyw
                do i=sxw,exw
                   if (k==gszw) then
                      rr=(grid_x(i)-bc_ns%backward%xyz(1))**2+(grid_y(j)-bc_ns%backward%xyz(2))**2
                      if ( rr <= bc_ns%backward%r**2 ) then
                         win(i,j,k)=win(i,j,k)*var1/var2
                      end if
                   end if
                end do
             end do
          end do
       end select
       !-------------------------------------------------------------------------------
       ! forward
       !-------------------------------------------------------------------------------
       select case(abs(ns%bound%forward))
       case(10,11,12,13)
          do k=szu,ezu
             do j=syu,eyu
                do i=sxu,exu
                   !-------------------------------------------------------------------------------
                   ! U component
                   !-------------------------------------------------------------------------------
                   if (k==gezu) then
                      penu(i,j,k,:)=0
                      penu(i,j,k,1)=full_pen
                   end if
                end do
             end do
          end do
          do k=szv,ezv
             do j=syv,eyv
                do i=sxv,exv
                   !-------------------------------------------------------------------------------
                   ! V component
                   !-------------------------------------------------------------------------------
                   if (k==gezv) then
                      penv(i,j,k,:)=0
                      penv(i,j,k,1)=full_pen
                   end if
                end do
             end do
          end do
          if (dim==3) then
             var1l=0;var2l=0
             do k=szw,ezw
                do j=syw,eyw
                   do i=sxw,exw
                      !-------------------------------------------------------------------------------
                      ! W component
                      !-------------------------------------------------------------------------------
                      if (k==gezw) then
                         penw(i,j,k,:)=0
                         penw(i,j,k,1)=half_pen
                         penw(i,j,k,6)=half_pen
                         rr=(grid_x(i)-bc_ns%forward%xyz(1))**2+(grid_y(j)-bc_ns%forward%xyz(2))**2
                         if ( rr <= bc_ns%forward%r**2 ) then
                            select case(abs(ns%bound%forward))
                            case(10)
                               vel_in=bc_ns%forward%win
                            case(11)
                               vel_in=2*bc_ns%forward%win*(1-rr/bc_ns%forward%r**2)
                            case(12)
                               vel_in=-tanh((sqrt(rr)-bc_ns%forward%r)/(tanh_coeff*bc_ns%forward%r))
                            end select
                            var1l=var1l+dx(i)*dy(j)*coeffS(i,j,k)
                            var2l=var2l+vel_in*dx(i)*dy(j)*coeffS(i,j,k)
                            win(i,j,k)=2*vel_in
                         end if
                      end if
                   end do
                end do
             end do
             !-------------------------------------------------------------------------------
             ! mean value correction
             !-------------------------------------------------------------------------------
             var1=bc_ns%forward%win*pi*bc_ns%forward%r**2
             call mpi_allreduce(var2l,var2,1,mpi_double_precision,mpi_sum,comm3d,code)
             do k=szw,ezw
                do j=syw,eyw
                   do i=sxw,exw
                      if (k==gezw) then
                         rr=(grid_x(i)-bc_ns%forward%xyz(1))**2+(grid_y(j)-bc_ns%forward%xyz(2))**2
                         if ( rr <= bc_ns%forward%r**2 ) then
                            win(i,j,k)=win(i,j,k)*var1/var2
                         end if
                      end if
                   end do
                end do
             end do
             !-------------------------------------------------------------------------------
          end if
       end select
    end if
    !-------------------------------------------------------------------------------


    !-------------------------------------------------------------------------------
    ! rectangular inlet
    !-------------------------------------------------------------------------------
    ! 30 : flat
    ! 31 : expo
    !-------------------------------------------------------------------------------
    ! left
    !-------------------------------------------------------------------------------
    select case(abs(ns%bound%left))
    case(30,31)
       var1l=0;var2l=0
       do k=szu,ezu
          do j=syu,eyu
             do i=sxu,exu
                !-------------------------------------------------------------------------------
                ! U component
                !-------------------------------------------------------------------------------
                if (i==gsxu) then
                   penu(i,j,k,:)=0
                   penu(i,j,k,1)=half_pen
                   penu(i,j,k,3)=half_pen
                   if (   abs(grid_y(j)-bc_ns%left%xyz(2))<=bc_ns%left%dxyz2(2) .and. &
                        & abs(grid_z(k)-bc_ns%left%xyz(3))<=bc_ns%left%dxyz2(3)) then
                      select case(abs(ns%bound%left))
                      case(30)
                         vel_in=bc_ns%left%uin
                      case(31)
                         dn=min(abs(grid_y(j)-(bc_ns%left%xyz(2)-bc_ns%left%dxyz2(2))), &
                              & abs(grid_y(j)-(bc_ns%left%xyz(2)+bc_ns%left%dxyz2(2))), &
                              & abs(grid_z(k)-(bc_ns%left%xyz(3)-bc_ns%left%dxyz2(3))), &
                              & abs(grid_z(k)-(bc_ns%left%xyz(3)+bc_ns%left%dxyz2(3))))
                         vel_in=1-exp(-10*dn/min(bc_ns%left%dxyz2(2),bc_ns%left%dxyz2(3)))
                      end select
                      var1l=var1l+dy(j)*dz(k)*coeffS(i,j,k)
                      var2l=var2l+vel_in*dy(j)*dz(k)*coeffS(i,j,k)
                      uin(i,j,k)=2*vel_in
                   end if
                end if
             end do
          end do
       end do
       !-------------------------------------------------------------------------------
       ! mean value correction
       !-------------------------------------------------------------------------------
       var1=bc_ns%left%uin*4*bc_ns%left%dxyz2(2)*bc_ns%left%dxyz2(3)
       call mpi_allreduce(var2l,var2,1,mpi_double_precision,mpi_sum,comm3d,code)
       do k=szu,ezu
          do j=syu,eyu
             do i=sxu,exu
                if (i==gsxu) then
                   if (   abs(grid_y(j)-bc_ns%left%xyz(2))<=bc_ns%left%dxyz2(2) .and. &
                        & abs(grid_z(k)-bc_ns%left%xyz(3))<=bc_ns%left%dxyz2(3)) then
                      uin(i,j,k)=uin(i,j,k)*var1/var2
                   end if
                end if
             end do
          end do
       end do
       !-------------------------------------------------------------------------------
       do k=szv,ezv
          do j=syv,eyv
             do i=sxv,exv
                !-------------------------------------------------------------------------------
                ! V component
                !-------------------------------------------------------------------------------
                if (i==gsxv) then
                   penv(i,j,k,:)=0
                   penv(i,j,k,1)=full_pen
                end if
             end do
          end do
       end do
       if (dim==3) then
          do k=szw,ezw
             do j=syw,eyw
                do i=sxw,exw
                   !-------------------------------------------------------------------------------
                   ! W component
                   !-------------------------------------------------------------------------------
                   if (i==gsxw) then
                      penw(i,j,k,:)=0
                      penw(i,j,k,1)=full_pen
                   end if
                end do
             end do
          end do
       end if
    end select
    !-------------------------------------------------------------------------------
    ! right
    !-------------------------------------------------------------------------------
    select case(abs(ns%bound%right))
    case(30,31)
       var1l=0;var2l=0
       do k=szu,ezu
          do j=syu,eyu
             do i=sxu,exu
                !-------------------------------------------------------------------------------
                ! U component
                !-------------------------------------------------------------------------------
                if (i==gexu) then
                   penu(i,j,k,:)=0
                   penu(i,j,k,1)=half_pen
                   penu(i,j,k,2)=half_pen
                   if (   abs(grid_y(j)-bc_ns%right%xyz(2))<=bc_ns%right%dxyz2(2) .and. &
                        & abs(grid_z(k)-bc_ns%right%xyz(3))<=bc_ns%right%dxyz2(3)) then
                      select case(abs(ns%bound%right))
                      case(30)                         
                         vel_in=bc_ns%right%uin
                      case(31)
                         dn=min(abs(grid_y(j)-(bc_ns%right%xyz(2)-bc_ns%right%dxyz2(2))), &
                              & abs(grid_y(j)-(bc_ns%right%xyz(2)+bc_ns%right%dxyz2(2))), &
                              & abs(grid_z(k)-(bc_ns%right%xyz(3)-bc_ns%right%dxyz2(3))), &
                              & abs(grid_z(k)-(bc_ns%right%xyz(3)+bc_ns%right%dxyz2(3))))
                         vel_in=1-exp(-10*dn/min(bc_ns%right%dxyz2(2),bc_ns%right%dxyz2(3)))
                      end select
                      var1l=var1l+dy(j)*dz(k)*coeffS(i,j,k)
                      var2l=var2l+vel_in*dy(j)*dz(k)*coeffS(i,j,k)
                      uin(i,j,k)=2*vel_in
                   end if
                end if
             end do
          end do
       end do
       !-------------------------------------------------------------------------------
       ! mean value correction
       !-------------------------------------------------------------------------------
       var1=bc_ns%right%uin*4*bc_ns%right%dxyz2(2)*bc_ns%right%dxyz2(3)
       call mpi_allreduce(var2l,var2,1,mpi_double_precision,mpi_sum,comm3d,code)
       do k=szu,ezu
          do j=syu,eyu
             do i=sxu,exu
                if (i==gexu) then
                   if (   abs(grid_y(j)-bc_ns%right%xyz(2))<=bc_ns%right%dxyz2(2) .and. &
                        & abs(grid_z(k)-bc_ns%right%xyz(3))<=bc_ns%right%dxyz2(3)) then
                      uin(i,j,k)=uin(i,j,k)*var1/var2
                   end if
                end if
             end do
          end do
       end do
       !-------------------------------------------------------------------------------
       do k=szv,ezv
          do j=syv,eyv
             do i=sxv,exv
                !-------------------------------------------------------------------------------
                ! V component
                !-------------------------------------------------------------------------------
                if (i==gexv) then
                   penv(i,j,k,:)=0
                   penv(i,j,k,1)=full_pen
                end if
             end do
          end do
       end do
       if (dim==3) then
          do k=szw,ezw
             do j=syw,eyw
                do i=sxw,exw
                   !-------------------------------------------------------------------------------
                   ! W component
                   !-------------------------------------------------------------------------------
                   if (i==gexw) then
                      penw(i,j,k,:)=0
                      penw(i,j,k,1)=full_pen
                   end if
                end do
             end do
          end do
       end if
    end select
    !-------------------------------------------------------------------------------
    ! bottom
    !-------------------------------------------------------------------------------
    select case(abs(ns%bound%bottom))
    case(30,31)
       do k=szu,ezu
          do j=syu,eyu
             do i=sxu,exu
                !-------------------------------------------------------------------------------
                ! U component
                !-------------------------------------------------------------------------------
                if (j==gsyu) then
                   penu(i,j,k,:)=0
                   penu(i,j,k,1)=full_pen
                end if
             end do
          end do
       end do
       var1l=0;var2l=0
       do k=szv,ezv
          do j=syv,eyv
             do i=sxv,exv
                !-------------------------------------------------------------------------------
                ! V component
                !-------------------------------------------------------------------------------
                if (j==gsyv) then
                   penv(i,j,k,:)=0
                   penv(i,j,k,1)=half_pen
                   penv(i,j,k,5)=half_pen
                   if (   abs(grid_x(i)-bc_ns%bottom%xyz(1))<=bc_ns%bottom%dxyz2(1) .and. &
                        & abs(grid_z(k)-bc_ns%bottom%xyz(3))<=bc_ns%bottom%dxyz2(3)) then
                      select case(abs(ns%bound%bottom))
                      case(30)
                         vel_in=bc_ns%bottom%vin
                      case(31)
                         dn=min(abs(grid_x(i)-(bc_ns%bottom%xyz(1)-bc_ns%bottom%dxyz2(1))), &
                              & abs(grid_x(i)-(bc_ns%bottom%xyz(1)+bc_ns%bottom%dxyz2(1))), &
                              & abs(grid_z(k)-(bc_ns%bottom%xyz(3)-bc_ns%bottom%dxyz2(3))), &
                              & abs(grid_z(k)-(bc_ns%bottom%xyz(3)+bc_ns%bottom%dxyz2(3))))
                         vel_in=1-exp(-10*dn/min(bc_ns%bottom%dxyz2(1),bc_ns%bottom%dxyz2(3)))
                      end select
                      var1l=var1l+dx(i)*dz(k)*coeffS(i,j,k)
                      var2l=var2l+vel_in*dx(i)*dz(k)*coeffS(i,j,k)
                      vin(i,j,k)=2*vel_in
                   end if
                end if
             end do
          end do
       end do
       !-------------------------------------------------------------------------------
       ! mean value correction
       !-------------------------------------------------------------------------------
       var1=bc_ns%bottom%vin*4*bc_ns%bottom%dxyz2(1)*bc_ns%bottom%dxyz2(3)
       call mpi_allreduce(var2l,var2,1,mpi_double_precision,mpi_sum,comm3d,code)
       do k=szv,ezv
          do j=syv,eyv
             do i=sxv,exv
                if (j==gsyv) then
                   if (   abs(grid_x(i)-bc_ns%bottom%xyz(1))<=bc_ns%bottom%dxyz2(1) .and. &
                        & abs(grid_z(k)-bc_ns%bottom%xyz(3))<=bc_ns%bottom%dxyz2(3)) then
                      vin(i,j,k)=vin(i,j,k)*var1/var2
                   end if
                end if
             end do
          end do
       end do
       !-------------------------------------------------------------------------------
       if (dim==3) then
          do k=szw,ezw
             do j=syw,eyw
                do i=sxw,exw
                   !-------------------------------------------------------------------------------
                   ! W component
                   !-------------------------------------------------------------------------------
                   if (j==gsyw) then
                      penw(i,j,k,:)=0
                      penw(i,j,k,1)=full_pen
                   end if
                end do
             end do
          end do
       end if
    end select
    !-------------------------------------------------------------------------------
    ! top
    !-------------------------------------------------------------------------------
    select case(abs(ns%bound%top))
    case(30,31)
       do k=szu,ezu
          do j=syu,eyu
             do i=sxu,exu
                !-------------------------------------------------------------------------------
                ! U component
                !-------------------------------------------------------------------------------
                if (j==geyu) then
                   penu(i,j,k,:)=0
                   penu(i,j,k,1)=full_pen
                end if
             end do
          end do
       end do
       var1l=0;var2l=0
       do k=szv,ezv
          do j=syv,eyv
             do i=sxv,exv
                !-------------------------------------------------------------------------------
                ! V component
                !-------------------------------------------------------------------------------
                if (j==geyv) then
                   penv(i,j,k,:)=0
                   penv(i,j,k,1)=half_pen
                   penv(i,j,k,4)=half_pen
                   if (   abs(grid_x(i)-bc_ns%top%xyz(1))<=bc_ns%top%dxyz2(1) .and. &
                        & abs(grid_z(k)-bc_ns%top%xyz(3))<=bc_ns%top%dxyz2(3)) then
                      select case(abs(ns%bound%top))
                      case(30)
                         vel_in=bc_ns%top%vin
                      case(31)
                         dn=min(abs(grid_x(i)-(bc_ns%top%xyz(1)-bc_ns%top%dxyz2(1))), &
                              & abs(grid_x(i)-(bc_ns%top%xyz(1)+bc_ns%top%dxyz2(1))), &
                              & abs(grid_z(k)-(bc_ns%top%xyz(3)-bc_ns%top%dxyz2(3))), &
                              & abs(grid_z(k)-(bc_ns%top%xyz(3)+bc_ns%top%dxyz2(3))))
                         vel_in=1-exp(-10*dn/min(bc_ns%top%dxyz2(1),bc_ns%top%dxyz2(3)))
                      end select
                      var1l=var1l+dx(i)*dz(k)*coeffS(i,j,k)
                      var2l=var2l+vel_in*dx(i)*dz(k)*coeffS(i,j,k)
                      vin(i,j,k)=2*vel_in
                   end if
                end if
             end do
          end do
       end do
       !-------------------------------------------------------------------------------
       ! mean value correction
       !-------------------------------------------------------------------------------
       var1=bc_ns%top%vin*4*bc_ns%top%dxyz2(1)*bc_ns%top%dxyz2(3)
       call mpi_allreduce(var2l,var2,1,mpi_double_precision,mpi_sum,comm3d,code)
       do k=szv,ezv
          do j=syv,eyv
             do i=sxv,exv
                if (j==geyv) then
                   if (   abs(grid_x(i)-bc_ns%top%xyz(1))<=bc_ns%top%dxyz2(1) .and. &
                        & abs(grid_z(k)-bc_ns%top%xyz(3))<=bc_ns%top%dxyz2(3)) then
                      vin(i,j,k)=vin(i,j,k)*var1/var2
                   end if
                end if
             end do
          end do
       end do
       !-------------------------------------------------------------------------------
       if (dim==3) then
          do k=szw,ezw
             do j=syw,eyw
                do i=sxw,exw
                   !-------------------------------------------------------------------------------
                   ! W component
                   !-------------------------------------------------------------------------------
                   if (j==geyw) then
                      penw(i,j,k,:)=0
                      penw(i,j,k,1)=full_pen
                   end if
                end do
             end do
          end do
       end if
    case(50)
       do k=szu,ezu
          do j=syu,eyu
             do i=sxu,exu
                !-------------------------------------------------------------------------------
                ! U component
                !-------------------------------------------------------------------------------
                if (j==geyu) then
                   penu(i,j,k,:)=0
                   penu(i,j,k,1)=full_pen
                end if
             end do
          end do
       end do
       if (dim==3) then 
          do k=szw,ezw
             do j=syw,eyw
                do i=sxw,exw
                   !-------------------------------------------------------------------------------
                   ! W component
                   !-------------------------------------------------------------------------------
                   if (j==geyw) then
                      penw(i,j,k,:)=0
                      penw(i,j,k,1)=full_pen
                   end if
                end do
             end do
          end do
       end if
       var1l=0;var2l=0
       do k=szv,ezv
          do j=syv,eyv
             do i=sxv,exv
                !-------------------------------------------------------------------------------
                ! V component
                !-------------------------------------------------------------------------------
                if (j==geyv) then
                   penv(i,j,k,:)=0
                   penv(i,j,k,1)=+full_pen
                   penv(i,j,k,4)=-full_pen
                   if (   abs(grid_x(i)-bc_ns%top%xyz(1))<=bc_ns%top%dxyz2(1) .and. &
                        & abs(grid_z(k)-bc_ns%top%xyz(3))<=bc_ns%top%dxyz2(3)) then
                      penv(i,j,k,:)=0
                      penv(i,j,k,1)=half_pen
                      penv(i,j,k,4)=half_pen
                      vel_in=bc_ns%top%vin
                      var1l=var1l+dx(i)*dz(k)*coeffS(i,j,k)
                      var2l=var2l+vel_in*dx(i)*dz(k)*coeffS(i,j,k)
                      vin(i,j,k)=2*vel_in
                   end if
                end if
             end do
          end do
       end do
    case(60)
       do k=szu,ezu
          do j=syu,eyu
             do i=sxu,exu
                !-------------------------------------------------------------------------------
                ! U component
                !-------------------------------------------------------------------------------
                if (k==gezu) then
                   penu(i,j,k,:)=0
                   penu(i,j,k,1)=full_pen
                end if
             end do
          end do
       end do
       do k=szv,ezv
          do j=syv,eyv
             do i=sxv,exv
                !-------------------------------------------------------------------------------
                ! V component
                !-------------------------------------------------------------------------------
                if (j==geyv) then
                   penv(i,j,k,:)=0
                   penv(i,j,k,1)=+full_pen
                   penv(i,j,k,4)=-full_pen
                   if ((    grid_x(i)-bc_ns%top%xyz(1))**2 &
                        & +(grid_z(k)-bc_ns%top%xyz(3))**2<=bc_ns%top%r**2) then
                      penv(i,j,k,:)=0
                      penv(i,j,k,1)=half_pen
                      penv(i,j,k,4)=half_pen
                      select case(abs(ns%bound%top))
                      case(60)
                         vel_in=bc_ns%top%vin
                      end select
                      var1l=var1l+dx(i)*dz(k)*coeffS(i,j,k)
                      var2l=var2l+vel_in*dx(i)*dz(k)*coeffS(i,j,k)
                      vin(i,j,k)=2*vel_in
                   end if
                end if
             end do
          end do
       end do
       if (dim==3) then 
          do k=szw,ezw
             do j=syw,eyw
                do i=sxw,exw
                   !-------------------------------------------------------------------------------
                   ! W component
                   !-------------------------------------------------------------------------------
                   if (k==gezw) then
                      penw(i,j,k,:)=0
                      penw(i,j,k,1)=full_pen
                   end if
                end do
             end do
          end do
       end  if
    end select
    if (dim==3) then
       !-------------------------------------------------------------------------------
       ! backward
       !-------------------------------------------------------------------------------
       select case(abs(ns%bound%backward))
       case(30,31)
          do k=szu,ezu
             do j=syu,eyu
                do i=sxu,exu
                   !-------------------------------------------------------------------------------
                   ! U component
                   !-------------------------------------------------------------------------------
                   if (k==gszu) then
                      penu(i,j,k,:)=0
                      penu(i,j,k,1)=full_pen
                   end if
                end do
             end do
          end do
          do k=szv,ezv
             do j=syv,eyv
                do i=sxv,exv
                   !-------------------------------------------------------------------------------
                   ! V component
                   !-------------------------------------------------------------------------------
                   if (k==gszv) then
                      penv(i,j,k,:)=0
                      penv(i,j,k,1)=full_pen
                   end if
                end do
             end do
          end do
          var1l=0;var2l=0
          do k=szw,ezw
             do j=syw,eyw
                do i=sxw,exw
                   !-------------------------------------------------------------------------------
                   ! W component
                   !-------------------------------------------------------------------------------
                   if (k==gszw) then
                      penw(i,j,k,:)=0
                      penw(i,j,k,1)=half_pen
                      penw(i,j,k,7)=half_pen
                      if (   abs(grid_x(i)-bc_ns%backward%xyz(1))<=bc_ns%backward%dxyz2(1) .and. &
                           & abs(grid_y(j)-bc_ns%backward%xyz(2))<=bc_ns%backward%dxyz2(2)) then
                         select case(abs(ns%bound%backward))
                         case(30)
                            vel_in=bc_ns%backward%win
                         case(31)
                            dn=min(abs(grid_x(i)-(bc_ns%backward%xyz(1)-bc_ns%backward%dxyz2(1))), &
                                 & abs(grid_x(i)-(bc_ns%backward%xyz(1)+bc_ns%backward%dxyz2(1))), &
                                 & abs(grid_y(j)-(bc_ns%backward%xyz(2)-bc_ns%backward%dxyz2(2))), &
                                 & abs(grid_y(j)-(bc_ns%backward%xyz(2)+bc_ns%backward%dxyz2(2))))
                            vel_in=1-exp(-10*dn/min(bc_ns%backward%dxyz2(1),bc_ns%backward%dxyz2(2)))
                         end select
                         var1l=var1l+dx(i)*dy(j)*coeffS(i,j,k)
                         var2l=var2l+vel_in*dx(i)*dy(j)*coeffS(i,j,k)
                         win(i,j,k)=2*vel_in
                      end if
                   end if
                end do
             end do
          end do
          !-------------------------------------------------------------------------------
          ! mean value correction
          !-------------------------------------------------------------------------------
          var1=bc_ns%backward%win*4*bc_ns%backward%dxyz2(1)*bc_ns%backward%dxyz2(2)
          call mpi_allreduce(var2l,var2,1,mpi_double_precision,mpi_sum,comm3d,code)
          do k=szw,ezw
             do j=syw,eyw
                do i=sxw,exw
                   if (k==gszw) then
                      if (   abs(grid_x(i)-bc_ns%backward%xyz(1))<=bc_ns%backward%dxyz2(1) .and. &
                           & abs(grid_y(j)-bc_ns%backward%xyz(2))<=bc_ns%backward%dxyz2(2)) then
                         win(i,j,k)=win(i,j,k)*var1/var2
                      end if
                   end if
                end do
             end do
          end do
          !-------------------------------------------------------------------------------
       end select
       !-------------------------------------------------------------------------------
       ! forward
       !-------------------------------------------------------------------------------
       select case(abs(ns%bound%forward))
       case(30,31)
          do k=szu,ezu
             do j=syu,eyu
                do i=sxu,exu
                   !-------------------------------------------------------------------------------
                   ! U component
                   !-------------------------------------------------------------------------------
                   if (k==gezu) then
                      penu(i,j,k,:)=0
                      penu(i,j,k,1)=full_pen
                   end if
                end do
             end do
          end do
          do k=szv,ezv
             do j=syv,eyv
                do i=sxv,exv
                   !-------------------------------------------------------------------------------
                   ! V component
                   !-------------------------------------------------------------------------------
                   if (k==gezv) then
                      penv(i,j,k,:)=0
                      penv(i,j,k,1)=full_pen
                   end if
                end do
             end do
          end do
          var1l=0;var2l=0
          do k=szw,ezw
             do j=syw,eyw
                do i=sxw,exw
                   !-------------------------------------------------------------------------------
                   ! W component
                   !-------------------------------------------------------------------------------
                   if (k==gezw) then
                      penw(i,j,k,:)=0
                      penw(i,j,k,1)=half_pen
                      penw(i,j,k,6)=half_pen
                      if (   abs(grid_x(i)-bc_ns%forward%xyz(1))<=bc_ns%forward%dxyz2(1) .and. &
                           & abs(grid_y(j)-bc_ns%forward%xyz(2))<=bc_ns%forward%dxyz2(2)) then
                         select case(abs(ns%bound%forward))
                         case(30)
                            vel_in=bc_ns%forward%win
                         case(31)
                            dn=min(abs(grid_x(i)-(bc_ns%forward%xyz(1)-bc_ns%forward%dxyz2(1))), &
                                 & abs(grid_x(i)-(bc_ns%forward%xyz(1)+bc_ns%forward%dxyz2(1))), &
                                 & abs(grid_y(j)-(bc_ns%forward%xyz(2)-bc_ns%forward%dxyz2(2))), &
                                 & abs(grid_y(j)-(bc_ns%forward%xyz(2)+bc_ns%forward%dxyz2(2))))
                            vel_in=1-exp(-10*dn/min(bc_ns%forward%dxyz2(1),bc_ns%forward%dxyz2(2)))
                         end select
                         var1l=var1l+dx(i)*dy(j)*coeffS(i,j,k)
                         var2l=var2l+vel_in*dx(i)*dy(j)*coeffS(i,j,k)
                         win(i,j,k)=2*vel_in
                      end if
                   end if
                end do
             end do
          end do
          !-------------------------------------------------------------------------------
          ! mean value correction
          !-------------------------------------------------------------------------------
          var1=bc_ns%forward%win*4*bc_ns%forward%dxyz2(1)*bc_ns%forward%dxyz2(2)
          call mpi_allreduce(var2l,var2,1,mpi_double_precision,mpi_sum,comm3d,code)
          do k=szw,ezw
             do j=syw,eyw
                do i=sxw,exw
                   if (k==gezw) then
                      if (   abs(grid_x(i)-bc_ns%forward%xyz(1))<=bc_ns%forward%dxyz2(1) .and. &
                           & abs(grid_y(j)-bc_ns%forward%xyz(2))<=bc_ns%forward%dxyz2(2)) then
                         win(i,j,k)=win(i,j,k)*var1/var2
                      end if
                   end if
                end do
             end do
          end do
       case(50,51)
          do k=szu,ezu
             do j=syu,eyu
                do i=sxu,exu
                   !-------------------------------------------------------------------------------
                   ! U component
                   !-------------------------------------------------------------------------------
                   if (k==gezu) then
                      penu(i,j,k,:)=0
                      penu(i,j,k,1)=full_pen
                   end if
                end do
             end do
          end do
          do k=szv,ezv
             do j=syv,eyv
                do i=sxv,exv
                   !-------------------------------------------------------------------------------
                   ! V component
                   !-------------------------------------------------------------------------------
                   if (k==gezv) then
                      penv(i,j,k,:)=0
                      penv(i,j,k,1)=full_pen
                   end if
                end do
             end do
          end do
          var1l=0;var2l=0
          do k=szw,ezw
             do j=syw,eyw
                do i=sxw,exw
                   !-------------------------------------------------------------------------------
                   ! W component
                   !-------------------------------------------------------------------------------
                   if (k==gezw) then
                      penw(i,j,k,:)=0
                      penw(i,j,k,1)=+full_pen
                      penw(i,j,k,6)=-full_pen
                      if (   abs(grid_x(i)-bc_ns%forward%xyz(1))<=bc_ns%forward%dxyz2(1) .and. &
                           & abs(grid_y(j)-bc_ns%forward%xyz(2))<=bc_ns%forward%dxyz2(2)) then
                         penw(i,j,k,:)=0
                         penw(i,j,k,1)=half_pen
                         penw(i,j,k,6)=half_pen
                         select case(abs(ns%bound%forward))
                         case(50)
                            vel_in=bc_ns%forward%win
                         case(51)
                            dn=min(abs(grid_x(i)-(bc_ns%forward%xyz(1)-bc_ns%forward%dxyz2(1))), &
                                 & abs(grid_x(i)-(bc_ns%forward%xyz(1)+bc_ns%forward%dxyz2(1))), &
                                 & abs(grid_y(j)-(bc_ns%forward%xyz(2)-bc_ns%forward%dxyz2(2))), &
                                 & abs(grid_y(j)-(bc_ns%forward%xyz(2)+bc_ns%forward%dxyz2(2))))
                            vel_in=1-exp(-10*dn/min(bc_ns%forward%dxyz2(1),bc_ns%forward%dxyz2(2)))
                         end select
                         var1l=var1l+dx(i)*dy(j)*coeffS(i,j,k)
                         var2l=var2l+vel_in*dx(i)*dy(j)*coeffS(i,j,k)
                         win(i,j,k)=2*vel_in
                      end if
                   end if
                end do
             end do
          end do
          !-------------------------------------------------------------------------------
          ! mean value correction
          !-------------------------------------------------------------------------------
          var1=bc_ns%forward%win*4*bc_ns%forward%dxyz2(1)*bc_ns%forward%dxyz2(2)
          call mpi_allreduce(var2l,var2,1,mpi_double_precision,mpi_sum,comm3d,code)
          do k=szw,ezw
             do j=syw,eyw
                do i=sxw,exw
                   if (k==gezw) then
                      if (   abs(grid_x(i)-bc_ns%forward%xyz(1))<=bc_ns%forward%dxyz2(1) .and. &
                           & abs(grid_y(j)-bc_ns%forward%xyz(2))<=bc_ns%forward%dxyz2(2)) then
                         win(i,j,k)=win(i,j,k)*var1/var2
                      end if
                   end if
                end do
             end do
          end do
          !-------------------------------------------------------------------------------
       case(60)
          do k=szu,ezu
             do j=syu,eyu
                do i=sxu,exu
                   !-------------------------------------------------------------------------------
                   ! U component
                   !-------------------------------------------------------------------------------
                   if (k==gezu) then
                      penu(i,j,k,:)=0
                      penu(i,j,k,1)=full_pen
                   end if
                end do
             end do
          end do
          do k=szv,ezv
             do j=syv,eyv
                do i=sxv,exv
                   !-------------------------------------------------------------------------------
                   ! V component
                   !-------------------------------------------------------------------------------
                   if (k==gezv) then
                      penv(i,j,k,:)=0
                      penv(i,j,k,1)=full_pen
                   end if
                end do
             end do
          end do
          var1l=0;var2l=0
          do k=szw,ezw
             do j=syw,eyw
                do i=sxw,exw
                   !-------------------------------------------------------------------------------
                   ! W component
                   !-------------------------------------------------------------------------------
                   if (k==gezw) then
                      penw(i,j,k,:)=0
                      penw(i,j,k,1)=+full_pen
                      penw(i,j,k,6)=-full_pen
                      if ((    grid_x(i)-bc_ns%forward%xyz(1))**2 &
                           & +(grid_y(j)-bc_ns%forward%xyz(2))**2<=bc_ns%forward%r**2) then
                         penw(i,j,k,:)=0
                         penw(i,j,k,1)=half_pen
                         penw(i,j,k,6)=half_pen
                         select case(abs(ns%bound%forward))
                         case(60)
                            vel_in=bc_ns%forward%win
                         end select
                         var1l=var1l+dx(i)*dy(j)*coeffS(i,j,k)
                         var2l=var2l+vel_in*dx(i)*dy(j)*coeffS(i,j,k)
                         win(i,j,k)=2*vel_in
                      end if
                   end if
                end do
             end do
          end do
          !-------------------------------------------------------------------------------
          ! mean value correction
          !-------------------------------------------------------------------------------
!!$          var1=bc_ns%forward%win*pi*bc_ns%forward%r**2
!!$          call mpi_allreduce(var2l,var2,1,mpi_double_precision,mpi_sum,comm3d,code)
!!$          do k=szw,ezw
!!$             do j=syw,eyw
!!$                do i=sxw,exw
!!$                   if (k==gezw) then
!!$                      if ((    grid_x(i)-bc_ns%forward%xyz(1))**2 &
!!$                           & +(grid_y(j)-bc_ns%forward%xyz(2))**2<=bc_ns%forward%r**2) then
!!$                         win(i,j,k)=win(i,j,k)*var1/var2
!!$                      end if
!!$                   end if
!!$                end do
!!$             end do
!!$          end do
          !-------------------------------------------------------------------------------
       end select
    end if
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! cylindrical outlet
    !-------------------------------------------------------------------------------
    ! left
    !-------------------------------------------------------------------------------
    select case(ns%bound%left)
    case(20,21)
       var1l=0;var2l=0
       do k=szu,ezu
          do j=syu,eyu
             do i=sxu,exu
                !-------------------------------------------------------------------------------
                ! U component
                !-------------------------------------------------------------------------------
                if (i==gsxu) then
                   penu(i,j,k,:)=0
                   penu(i,j,k,1)=half_pen
                   penu(i,j,k,3)=half_pen
                   rr=(grid_y(j)-bc_ns%left%xyz(2))**2+(grid_z(k)-bc_ns%left%xyz(3))**2
                   if ( rr <= bc_ns%left%r**2 ) then
                      select case(ns%bound%left)
                      case(20)
                         penu(i,j,k,1)=-full_pen
                         penu(i,j,k,3)=+full_pen
                      case(21)
                         penu(i,j,k,1)=bc_ns%left%amp
                         penu(i,j,k,3)=bc_ns%left%amp
                         vel_in=bc_ns%left%uin
                         var1l=var1l+dy(j)*dz(k)*coeffS(i,j,k)
                         var2l=var2l+vel_in*dy(j)*dz(k)*coeffS(i,j,k)
                         uin(i,j,k)=2*vel_in
                      end select
                   end if
                end if
             end do
          end do
       end do
       !-------------------------------------------------------------------------------
       ! mean value correction
       !-------------------------------------------------------------------------------
       select case(ns%bound%left)
       case(21)
          var1=bc_ns%left%uin*4*bc_ns%left%dxyz2(2)*bc_ns%left%dxyz2(3)
          call mpi_allreduce(var2l,var2,1,mpi_double_precision,mpi_sum,comm3d,code)
          do k=szu,ezu
             do j=syu,eyu
                do i=sxu,exu
                   if (i==gsxu) then
                      rr=(grid_y(j)-bc_ns%left%xyz(2))**2+(grid_z(k)-bc_ns%left%xyz(3))**2
                      if ( rr <= bc_ns%left%r**2 ) then
                         uin(i,j,k)=uin(i,j,k)*var1/var2
                      end if
                   end if
                end do
             end do
          end do
       end select
       !-------------------------------------------------------------------------------
       do k=szv,ezv
          do j=syv,eyv
             do i=sxv,exv
                !-------------------------------------------------------------------------------
                ! V component
                !-------------------------------------------------------------------------------
                if (i==gsxv) then
                   penv(i,j,k,:)=0
                   penv(i,j,k,1)=full_pen
                end if
             end do
          end do
       end do
       if (dim==3) then
          do k=szw,ezw
             do j=syw,eyw
                do i=sxw,exw
                   !-------------------------------------------------------------------------------
                   ! W component
                   !-------------------------------------------------------------------------------
                   if (i==gsxw) then
                      penw(i,j,k,:)=0
                      penw(i,j,k,1)=full_pen
                   end if
                end do
             end do
          end do
       end if
    end select
    !-------------------------------------------------------------------------------
    ! right
    !-------------------------------------------------------------------------------
    select case(ns%bound%right)
    case(20,21)
       var1l=0;var2l=0
       do k=szu,ezu
          do j=syu,eyu
             do i=sxu,exu
                !-------------------------------------------------------------------------------
                ! U component
                !-------------------------------------------------------------------------------
                if (i==gexu) then
                   penu(i,j,k,:)=0
                   penu(i,j,k,1)=half_pen
                   penu(i,j,k,2)=half_pen
                   rr=(grid_y(j)-bc_ns%right%xyz(2))**2+(grid_z(k)-bc_ns%right%xyz(3))**2
                   if ( rr <= bc_ns%right%r**2 ) then
                      select case(ns%bound%right)
                      case(20)
                         penu(i,j,k,1)=+full_pen
                         penu(i,j,k,2)=-full_pen
                      case(21)
                         penu(i,j,k,1)=bc_ns%right%amp
                         penu(i,j,k,2)=bc_ns%right%amp
                         vel_in=bc_ns%right%uin
                         var1l=var1l+dy(j)*dz(k)*coeffS(i,j,k)
                         var2l=var2l+vel_in*dy(j)*dz(k)*coeffS(i,j,k)
                         uin(i,j,k)=2*vel_in
                      end select
                   end if
                end if
             end do
          end do
       end do
       !-------------------------------------------------------------------------------
       ! mean value correction
       !-------------------------------------------------------------------------------
       select case(ns%bound%right)
       case(21)
          var1=bc_ns%right%uin*4*bc_ns%right%dxyz2(2)*bc_ns%right%dxyz2(3)
          call mpi_allreduce(var2l,var2,1,mpi_double_precision,mpi_sum,comm3d,code)
          do k=szu,ezu
             do j=syu,eyu
                do i=sxu,exu
                   if (i==gexu) then
                      rr=(grid_y(j)-bc_ns%right%xyz(2))**2+(grid_z(k)-bc_ns%right%xyz(3))**2
                      if ( rr <= bc_ns%right%r**2 ) then
                         uin(i,j,k)=uin(i,j,k)*var1/var2
                      end if
                   end if
                end do
             end do
          end do
       end select
       !-------------------------------------------------------------------------------
       do k=szv,ezv
          do j=syv,eyv
             do i=sxv,exv
                !-------------------------------------------------------------------------------
                ! V component
                !-------------------------------------------------------------------------------
                if (i==gexv) then
                   penv(i,j,k,:)=0
                   penv(i,j,k,1)=full_pen
                end if
             end do
          end do
       end do
       if (dim==3) then
          do k=szw,ezw
             do j=syw,eyw
                do i=sxw,exw
                   !-------------------------------------------------------------------------------
                   ! W component
                   !-------------------------------------------------------------------------------
                   if (i==gexw) then
                      penw(i,j,k,:)=0
                      penw(i,j,k,1)=full_pen
                   end if
                end do
             end do
          end do
       end if
    end select
    !-------------------------------------------------------------------------------
    ! bottom
    !-------------------------------------------------------------------------------
    select case(ns%bound%bottom)
    case(20,21)
       var1l=0;var2l=0
       do k=szu,ezu
          do j=syu,eyu
             do i=sxu,exu
                !-------------------------------------------------------------------------------
                ! U component
                !-------------------------------------------------------------------------------
                if (j==gsyu) then
                   penu(i,j,k,:)=0
                   penu(i,j,k,1)=full_pen
                end if
             end do
          end do
       end do
       do k=szv,ezv
          do j=syv,eyv
             do i=sxv,exv
                !-------------------------------------------------------------------------------
                ! V component
                !-------------------------------------------------------------------------------
                if (j==gsyv) then
                   penv(i,j,k,:)=0
                   penv(i,j,k,1)=half_pen
                   penv(i,j,k,5)=half_pen
                   rr=(grid_x(i)-bc_ns%bottom%xyz(1))**2+(grid_z(k)-bc_ns%bottom%xyz(3))**2
                   if ( rr <= bc_ns%bottom%r**2 ) then
                      select case(ns%bound%bottom)
                      case(20)
                         penv(i,j,k,1)=-full_pen
                         penv(i,j,k,5)=+full_pen
                      case(21)
                         penv(i,j,k,1)=bc_ns%bottom%amp
                         penv(i,j,k,5)=bc_ns%bottom%amp
                         vel_in=bc_ns%bottom%vin
                         var1l=var1l+dx(i)*dz(k)*coeffS(i,j,k)
                         var2l=var2l+vel_in*dx(i)*dz(k)*coeffS(i,j,k)
                         vin(i,j,k)=2*vel_in
                      end select
                   end if
                end if
             end do
          end do
       end do
       !-------------------------------------------------------------------------------
       ! mean value correction
       !-------------------------------------------------------------------------------
       select case(ns%bound%bottom)
       case(21)
          var1=bc_ns%bottom%vin*4*bc_ns%bottom%dxyz2(1)*bc_ns%bottom%dxyz2(3)
          call mpi_allreduce(var2l,var2,1,mpi_double_precision,mpi_sum,comm3d,code)
          do k=szv,ezv
             do j=syv,eyv
                do i=sxv,exv
                   if (j==gsyv) then
                      rr=(grid_x(i)-bc_ns%bottom%xyz(1))**2+(grid_z(k)-bc_ns%bottom%xyz(3))**2
                      if ( rr <= bc_ns%bottom%r**2 ) then
                         vin(i,j,k)=vin(i,j,k)*var1/var2
                      end if
                   end if
                end do
             end do
          end do
       end select
       !-------------------------------------------------------------------------------
       if (dim==3) then
          do k=szw,ezw
             do j=syw,eyw
                do i=sxw,exw
                   !-------------------------------------------------------------------------------
                   ! W component
                   !-------------------------------------------------------------------------------
                   if (j==gsyw) then
                      penw(i,j,k,:)=0
                      penw(i,j,k,1)=full_pen
                   end if
                end do
             end do
          end do
       end if
    end select
    !-------------------------------------------------------------------------------
    ! top
    !-------------------------------------------------------------------------------
    select case(ns%bound%top)
    case(20,21)
       var1l=0;var2l=0
       do k=szu,ezu
          do j=syu,eyu
             do i=sxu,exu
                !-------------------------------------------------------------------------------
                ! U component
                !-------------------------------------------------------------------------------
                if (j==geyu) then
                   penu(i,j,k,:)=0
                   penu(i,j,k,1)=full_pen
                end if
             end do
          end do
       end do
       do k=szv,ezv
          do j=syv,eyv
             do i=sxv,exv
                !-------------------------------------------------------------------------------
                ! V component
                !-------------------------------------------------------------------------------
                if (j==geyv) then
                   penv(i,j,k,:)=0
                   penv(i,j,k,1)=half_pen
                   penv(i,j,k,4)=half_pen
                   rr=(grid_x(i)-bc_ns%top%xyz(1))**2+(grid_z(k)-bc_ns%top%xyz(3))**2
                   if ( rr <= bc_ns%top%r**2 ) then
                      select case(ns%bound%top)
                      case(20)
                         penv(i,j,k,1)=+full_pen
                         penv(i,j,k,4)=-full_pen
                      case(21)
                         penv(i,j,k,1)=bc_ns%top%amp
                         penv(i,j,k,4)=bc_ns%top%amp
                         vel_in=bc_ns%top%vin
                         var1l=var1l+dx(i)*dz(k)*coeffS(i,j,k)
                         var2l=var2l+vel_in*dx(i)*dz(k)*coeffS(i,j,k)
                         vin(i,j,k)=2*vel_in
                      end select
                   end if
                end if
             end do
          end do
       end do
       !-------------------------------------------------------------------------------
       ! mean value correction
       !-------------------------------------------------------------------------------
       select case(ns%bound%top)
       case(21)
          var1=bc_ns%top%vin*4*bc_ns%top%dxyz2(1)*bc_ns%top%dxyz2(3)
          call mpi_allreduce(var2l,var2,1,mpi_double_precision,mpi_sum,comm3d,code)
          do k=szv,ezv
             do j=syv,eyv
                do i=sxv,exv
                   if (j==geyv) then
                      rr=(grid_x(i)-bc_ns%top%xyz(1))**2+(grid_z(k)-bc_ns%top%xyz(3))**2
                      if ( rr <= bc_ns%top%r**2 ) then
                         vin(i,j,k)=vin(i,j,k)*var1/var2
                      end if
                   end if
                end do
             end do
          end do
       end select
       !-------------------------------------------------------------------------------
       if (dim==3) then
          do k=szw,ezw
             do j=syw,eyw
                do i=sxw,exw
                   !-------------------------------------------------------------------------------
                   ! W component
                   !-------------------------------------------------------------------------------
                   if (j==geyw) then
                      penw(i,j,k,:)=0
                      penw(i,j,k,1)=full_pen
                   end if
                end do
             end do
          end do
       end if
    end select
    if (dim==3) then
       !-------------------------------------------------------------------------------
       ! backward
       !-------------------------------------------------------------------------------
       select case(ns%bound%backward)
       case(20,21)
          var1l=0;var2l=0
          do k=szu,ezu
             do j=syu,eyu
                do i=sxu,exu
                   !-------------------------------------------------------------------------------
                   ! U component
                   !-------------------------------------------------------------------------------
                   if (k==gszu) then
                      penu(i,j,k,:)=0
                      penu(i,j,k,1)=full_pen
                   end if
                end do
             end do
          end do
          do k=szv,ezv
             do j=syv,eyv
                do i=sxv,exv
                   !-------------------------------------------------------------------------------
                   ! V component
                   !-------------------------------------------------------------------------------
                   if (k==gszv) then
                      penv(i,j,k,:)=0
                      penv(i,j,k,1)=full_pen
                   end if
                end do
             end do
          end do
          do k=szw,ezw
             do j=syw,eyw
                do i=sxw,exw
                   !-------------------------------------------------------------------------------
                   ! W component
                   !-------------------------------------------------------------------------------
                   if (k==gszw) then
                      penw(i,j,k,:)=0
                      penw(i,j,k,1)=half_pen
                      penw(i,j,k,7)=half_pen
                      rr=(grid_x(i)-bc_ns%backward%xyz(1))**2+(grid_y(j)-bc_ns%backward%xyz(2))**2
                      if ( rr <= bc_ns%backward%r**2 ) then
                         select case(ns%bound%backward)
                         case(20)
                            penw(i,j,k,1)=-full_pen
                            penw(i,j,k,7)=+full_pen
                         case(21)
                            penw(i,j,k,1)=bc_ns%backward%amp
                            penw(i,j,k,7)=bc_ns%backward%amp
                            vel_in=bc_ns%backward%win
                            var1l=var1l+dx(i)*dy(j)*coeffS(i,j,k)
                            var2l=var2l+vel_in*dx(i)*dy(j)*coeffS(i,j,k)
                            win(i,j,k)=2*vel_in
                         end select
                      end if
                   end if
                end do
             end do
          end do
          !-------------------------------------------------------------------------------
          ! mean value correction
          !-------------------------------------------------------------------------------
          select case(ns%bound%backward)
          case(21)
             var1=bc_ns%backward%win*4*bc_ns%backward%dxyz2(1)*bc_ns%backward%dxyz2(2)
             call mpi_allreduce(var2l,var2,1,mpi_double_precision,mpi_sum,comm3d,code)
             do k=szw,ezw
                do j=syw,eyw
                   do i=sxw,exw
                      if (k==gszw) then
                         rr=(grid_x(i)-bc_ns%backward%xyz(1))**2+(grid_y(j)-bc_ns%backward%xyz(2))**2
                         if ( rr <= bc_ns%backward%r**2 ) then
                            win(i,j,k)=win(i,j,k)*var1/var2
                         end if
                      end if
                   end do
                end do
             end do
          end select
          !-------------------------------------------------------------------------------
       end select
       !-------------------------------------------------------------------------------
       ! forward
       !-------------------------------------------------------------------------------
       select case(ns%bound%forward)
       case(20,21)
          var1l=0;var2l=0
          do k=szu,ezu
             do j=syu,eyu
                do i=sxu,exu
                   !-------------------------------------------------------------------------------
                   ! U component
                   !-------------------------------------------------------------------------------
                   if (k==gezu) then
                      penu(i,j,k,:)=0
                      penu(i,j,k,1)=full_pen
                   end if
                end do
             end do
          end do
          do k=szv,ezv
             do j=syv,eyv
                do i=sxv,exv
                   !-------------------------------------------------------------------------------
                   ! V component
                   !-------------------------------------------------------------------------------
                   if (k==gezv) then
                      penv(i,j,k,:)=0
                      penv(i,j,k,1)=full_pen
                   end if
                end do
             end do
          end do
          do k=szw,ezw
             do j=syw,eyw
                do i=sxw,exw
                   !-------------------------------------------------------------------------------
                   ! W component
                   !-------------------------------------------------------------------------------
                   if (k==gezw) then
                      penw(i,j,k,:)=0
                      penw(i,j,k,1)=half_pen
                      penw(i,j,k,6)=half_pen
                      rr=(grid_x(i)-bc_ns%forward%xyz(1))**2+(grid_y(j)-bc_ns%forward%xyz(2))**2
                      if ( rr <= bc_ns%forward%r**2 ) then
                         select case(ns%bound%forward)
                         case(20)
                            penw(i,j,k,1)=+full_pen
                            penw(i,j,k,6)=-full_pen
                         case(21)
                            penw(i,j,k,1)=bc_ns%forward%amp
                            penw(i,j,k,6)=bc_ns%forward%amp
                            vel_in=bc_ns%forward%win
                            var1l=var1l+dx(i)*dy(j)*coeffS(i,j,k)
                            var2l=var2l+vel_in*dx(i)*dy(j)*coeffS(i,j,k)
                            win(i,j,k)=2*vel_in
                         end select
                      end if
                   end if
                end do
             end do
          end do
          !-------------------------------------------------------------------------------
          ! mean value correction
          !-------------------------------------------------------------------------------
          select case(ns%bound%forward)
          case(21)
             var1=bc_ns%forward%win*4*bc_ns%forward%dxyz2(1)*bc_ns%forward%dxyz2(2)
             call mpi_allreduce(var2l,var2,1,mpi_double_precision,mpi_sum,comm3d,code)
             do k=szw,ezw
                do j=syw,eyw
                   do i=sxw,exw
                      if (k==gezw) then
                         rr=(grid_x(i)-bc_ns%forward%xyz(1))**2+(grid_y(j)-bc_ns%forward%xyz(2))**2
                         if ( rr <= bc_ns%forward%r**2 ) then
                            win(i,j,k)=win(i,j,k)*var1/var2
                         end if
                      end if
                   end do
                end do
             end do
          end select
          !-------------------------------------------------------------------------------
       end select
    end if
    !-------------------------------------------------------------------------------


    !-------------------------------------------------------------------------------
    ! rectangular outlet
    !-------------------------------------------------------------------------------
    ! left
    select case(abs(ns%bound%left))
    case(40,41)
       var1l=0;var2l=0
       do k=szu,ezu
          do j=syu,eyu
             do i=sxu,exu
                !-------------------------------------------------------------------------------
                ! U component
                !-------------------------------------------------------------------------------
                if (i==gsxu) then
                   penu(i,j,k,:)=0
                   penu(i,j,k,1)=half_pen
                   penu(i,j,k,3)=half_pen
                   if (   abs(grid_y(j)-bc_ns%left%xyz(2))<=bc_ns%left%dxyz2(2) .and. &
                        & abs(grid_z(k)-bc_ns%left%xyz(3))<=bc_ns%left%dxyz2(3)) then
                      select case(ns%bound%left)
                      case(40)
                         penu(i,j,k,1)=-full_pen
                         penu(i,j,k,3)=+full_pen
                      case(41)
                         penu(i,j,k,1)=bc_ns%left%amp
                         penu(i,j,k,3)=bc_ns%left%amp
                         vel_in=bc_ns%left%uin
                         var1l=var1l+dy(j)*dz(k)*coeffS(i,j,k)
                         var2l=var2l+vel_in*dy(j)*dz(k)*coeffS(i,j,k)
                         uin(i,j,k)=2*vel_in
                      end select
                   end if
                end if
             end do
          end do
       end do
       !-------------------------------------------------------------------------------
       ! mean value correction
       !-------------------------------------------------------------------------------
       select case(ns%bound%left)
       case(41)
          var1=bc_ns%left%uin*4*bc_ns%left%dxyz2(2)*bc_ns%left%dxyz2(3)
          call mpi_allreduce(var2l,var2,1,mpi_double_precision,mpi_sum,comm3d,code)
          do k=szu,ezu
             do j=syu,eyu
                do i=sxu,exu
                   if (i==gsxu) then
                      if (   abs(grid_y(j)-bc_ns%left%xyz(2))<=bc_ns%left%dxyz2(2) .and. &
                           & abs(grid_z(k)-bc_ns%left%xyz(3))<=bc_ns%left%dxyz2(3)) then
                         uin(i,j,k)=uin(i,j,k)*var1/var2
                      end if
                   end if
                end do
             end do
          end do
       end select
       !-------------------------------------------------------------------------------
       do k=szv,ezv
          do j=syv,eyv
             do i=sxv,exv
                !-------------------------------------------------------------------------------
                ! V component
                !-------------------------------------------------------------------------------
                if (i==gsxv) then
                   penv(i,j,k,:)=0
                   penv(i,j,k,1)=full_pen
                end if
             end do
          end do
       end do
       if (dim==3) then
          do k=szw,ezw
             do j=syw,eyw
                do i=sxw,exw
                   !-------------------------------------------------------------------------------
                   ! W component
                   !-------------------------------------------------------------------------------
                   if (i==gsxw) then
                      penw(i,j,k,:)=0
                      penw(i,j,k,1)=full_pen
                   end if
                end do
             end do
          end do
       end if
    end select
    !-------------------------------------------------------------------------------
    ! right
    !-------------------------------------------------------------------------------
    select case(abs(ns%bound%right))
    case(40,41)
       var1l=0;var2l=0
       do k=szu,ezu
          do j=syu,eyu
             do i=sxu,exu
                !-------------------------------------------------------------------------------
                ! U component
                !-------------------------------------------------------------------------------
                if (i==gexu) then
                   penu(i,j,k,:)=0
                   penu(i,j,k,1)=half_pen
                   penu(i,j,k,2)=half_pen
                   if (   abs(grid_y(j)-bc_ns%right%xyz(2))<=bc_ns%right%dxyz2(2) .and. &
                        & abs(grid_z(k)-bc_ns%right%xyz(3))<=bc_ns%right%dxyz2(3)) then
                      select case(ns%bound%right)
                      case(40)
                         penu(i,j,k,1)=+full_pen
                         penu(i,j,k,2)=-full_pen
                      case(41)
                         penu(i,j,k,1)=bc_ns%right%amp
                         penu(i,j,k,2)=bc_ns%right%amp
                         vel_in=bc_ns%right%uin
                         var1l=var1l+dy(j)*dz(k)*coeffS(i,j,k)
                         var2l=var2l+vel_in*dy(j)*dz(k)*coeffS(i,j,k)
                         uin(i,j,k)=2*vel_in
                      end select
                   end if
                end if
             end do
          end do
       end do
       !-------------------------------------------------------------------------------
       ! mean value correction
       !-------------------------------------------------------------------------------
       select case(ns%bound%right)
       case(41)
          var1=bc_ns%right%uin*4*bc_ns%right%dxyz2(2)*bc_ns%right%dxyz2(3)
          call mpi_allreduce(var2l,var2,1,mpi_double_precision,mpi_sum,comm3d,code)
          do k=szu,ezu
             do j=syu,eyu
                do i=sxu,exu
                   if (i==gexu) then
                      if (   abs(grid_y(j)-bc_ns%right%xyz(2))<=bc_ns%right%dxyz2(2) .and. &
                           & abs(grid_z(k)-bc_ns%right%xyz(3))<=bc_ns%right%dxyz2(3)) then
                         uin(i,j,k)=uin(i,j,k)*var1/var2
                      end if
                   end if
                end do
             end do
          end do
       end select
       !-------------------------------------------------------------------------------
       do k=szv,ezv
          do j=syv,eyv
             do i=sxv,exv
                !-------------------------------------------------------------------------------
                ! V component
                !-------------------------------------------------------------------------------
                if (i==gexv) then
                   penv(i,j,k,:)=0
                   penv(i,j,k,1)=full_pen
                end if
             end do
          end do
       end do
       if (dim==3) then
          do k=szw,ezw
             do j=syw,eyw
                do i=sxw,exw
                   !-------------------------------------------------------------------------------
                   ! W component
                   !-------------------------------------------------------------------------------
                   if (i==gexw) then
                      penw(i,j,k,:)=0
                      penw(i,j,k,1)=full_pen
                   end if
                end do
             end do
          end do
       end if
    end select
    !-------------------------------------------------------------------------------
    ! bottom
    !-------------------------------------------------------------------------------
    select case(abs(ns%bound%bottom))
    case(40,41)
       var1l=0;var2l=0
       do k=szu,ezu
          do j=syu,eyu
             do i=sxu,exu
                !-------------------------------------------------------------------------------
                ! U component
                !-------------------------------------------------------------------------------
                if (j==gsyu) then
                   penu(i,j,k,:)=0
                   penu(i,j,k,1)=full_pen
                end if
             end do
          end do
       end do
       do k=szv,ezv
          do j=syv,eyv
             do i=sxv,exv
                !-------------------------------------------------------------------------------
                ! V component
                !-------------------------------------------------------------------------------
                if (j==gsyv) then
                   penv(i,j,k,:)=0
                   penv(i,j,k,1)=half_pen
                   penv(i,j,k,5)=half_pen
                   if (   abs(grid_x(i)-bc_ns%bottom%xyz(1))<=bc_ns%bottom%dxyz2(1) .and. &
                        & abs(grid_z(k)-bc_ns%bottom%xyz(3))<=bc_ns%bottom%dxyz2(3)) then
                      select case(ns%bound%bottom)
                      case(40)
                         penv(i,j,k,1)=-full_pen
                         penv(i,j,k,5)=+full_pen
                      case(41)
                         penv(i,j,k,1)=bc_ns%bottom%amp
                         penv(i,j,k,5)=bc_ns%bottom%amp
                         vel_in=bc_ns%bottom%vin
                         var1l=var1l+dx(i)*dz(k)*coeffS(i,j,k)
                         var2l=var2l+vel_in*dx(i)*dz(k)*coeffS(i,j,k)
                         vin(i,j,k)=2*vel_in
                      end select
                   end if
                end if
             end do
          end do
       end do
       !-------------------------------------------------------------------------------
       ! mean value correction
       !-------------------------------------------------------------------------------
       select case(ns%bound%bottom)
       case(41)
          var1=bc_ns%bottom%vin*4*bc_ns%bottom%dxyz2(1)*bc_ns%bottom%dxyz2(3)
          call mpi_allreduce(var2l,var2,1,mpi_double_precision,mpi_sum,comm3d,code)
          do k=szv,ezv
             do j=syv,eyv
                do i=sxv,exv
                   if (j==gsyv) then
                      if (   abs(grid_x(i)-bc_ns%bottom%xyz(1))<=bc_ns%bottom%dxyz2(1) .and. &
                           & abs(grid_z(k)-bc_ns%bottom%xyz(3))<=bc_ns%bottom%dxyz2(3)) then
                         vin(i,j,k)=vin(i,j,k)*var1/var2
                      end if
                   end if
                end do
             end do
          end do
       end select
       !-------------------------------------------------------------------------------
       if (dim==3) then
          do k=szw,ezw
             do j=syw,eyw
                do i=sxw,exw
                   !-------------------------------------------------------------------------------
                   ! W component
                   !-------------------------------------------------------------------------------
                   if (j==gsyw) then
                      penw(i,j,k,:)=0
                      penw(i,j,k,1)=full_pen
                   end if
                end do
             end do
          end do
       end if
    end select
    !-------------------------------------------------------------------------------
    ! top
    !-------------------------------------------------------------------------------
    select case(abs(ns%bound%top))
    case(40,41)
       var1l=0;var2l=0
       do k=szu,ezu
          do j=syu,eyu
             do i=sxu,exu
                !-------------------------------------------------------------------------------
                ! U component
                !-------------------------------------------------------------------------------
                if (j==geyu) then
                   penu(i,j,k,:)=0
                   penu(i,j,k,1)=full_pen
                end if
             end do
          end do
       end do
       do k=szv,ezv
          do j=syv,eyv
             do i=sxv,exv
                !-------------------------------------------------------------------------------
                ! V component
                !-------------------------------------------------------------------------------
                if (j==geyv) then
                   penv(i,j,k,:)=0
                   penv(i,j,k,1)=half_pen
                   penv(i,j,k,4)=half_pen
                   if (   abs(grid_x(i)-bc_ns%top%xyz(1))<=bc_ns%top%dxyz2(1) .and. &
                        & abs(grid_z(k)-bc_ns%top%xyz(3))<=bc_ns%top%dxyz2(3)) then
                      select case(ns%bound%top)
                      case(40)
                         penv(i,j,k,1)=+full_pen
                         penv(i,j,k,4)=-full_pen
                      case(41)
                         penv(i,j,k,1)=bc_ns%top%amp
                         penv(i,j,k,4)=bc_ns%top%amp
                         vel_in=bc_ns%top%vin
                         var1l=var1l+dx(i)*dz(k)*coeffS(i,j,k)
                         var2l=var2l+vel_in*dx(i)*dz(k)*coeffS(i,j,k)
                         vin(i,j,k)=2*vel_in
                      end select
                   end if
                end if
             end do
          end do
       end do
       !-------------------------------------------------------------------------------
       ! mean value correction
       !-------------------------------------------------------------------------------
       select case(ns%bound%top)
       case(41)
          var1=bc_ns%top%vin*4*bc_ns%top%dxyz2(1)*bc_ns%top%dxyz2(3)
          call mpi_allreduce(var2l,var2,1,mpi_double_precision,mpi_sum,comm3d,code)
          do k=szv,ezv
             do j=syv,eyv
                do i=sxv,exv
                   if (j==geyv) then
                      if (   abs(grid_x(i)-bc_ns%top%xyz(1))<=bc_ns%top%dxyz2(1) .and. &
                           & abs(grid_z(k)-bc_ns%top%xyz(3))<=bc_ns%top%dxyz2(3)) then
                         vin(i,j,k)=vin(i,j,k)*var1/var2
                      end if
                   end if
                end do
             end do
          end do
       end select
       !-------------------------------------------------------------------------------
       if (dim==3) then
          do k=szw,ezw
             do j=syw,eyw
                do i=sxw,exw
                   !-------------------------------------------------------------------------------
                   ! W component
                   !-------------------------------------------------------------------------------
                   if (j==geyw) then
                      penw(i,j,k,:)=0
                      penw(i,j,k,1)=full_pen
                   end if
                end do
             end do
          end do
       end if
    end select
    if (dim==3) then
       !-------------------------------------------------------------------------------
       ! backward
       !-------------------------------------------------------------------------------
       select case(abs(ns%bound%backward))
       case(40,41)
          var1l=0;var2l=0
          do k=szu,ezu
             do j=syu,eyu
                do i=sxu,exu
                   !-------------------------------------------------------------------------------
                   ! U component
                   !-------------------------------------------------------------------------------
                   if (k==gszu) then
                      penu(i,j,k,:)=0
                      penu(i,j,k,1)=full_pen
                   end if
                end do
             end do
          end do
          do k=szv,ezv
             do j=syv,eyv
                do i=sxv,exv
                   !-------------------------------------------------------------------------------
                   ! V component
                   !-------------------------------------------------------------------------------
                   if (k==gszv) then
                      penv(i,j,k,:)=0
                      penv(i,j,k,1)=full_pen
                   end if
                end do
             end do
          end do
          do k=szw,ezw
             do j=syw,eyw
                do i=sxw,exw
                   !-------------------------------------------------------------------------------
                   ! W component
                   !-------------------------------------------------------------------------------
                   if (k==gszw) then
                      penw(i,j,k,:)=0
                      penw(i,j,k,1)=half_pen
                      penw(i,j,k,7)=half_pen
                      if (   abs(grid_x(i)-bc_ns%backward%xyz(1))<=bc_ns%backward%dxyz2(1) .and. &
                           & abs(grid_y(j)-bc_ns%backward%xyz(2))<=bc_ns%backward%dxyz2(2)) then
                         select case(ns%bound%backward)
                         case(40)
                            penw(i,j,k,1)=-full_pen
                            penw(i,j,k,7)=+full_pen
                         case(41)
                            penw(i,j,k,1)=bc_ns%backward%amp
                            penw(i,j,k,7)=bc_ns%backward%amp
                            vel_in=bc_ns%backward%win
                            var1l=var1l+dx(i)*dy(j)*coeffS(i,j,k)
                            var2l=var2l+vel_in*dx(i)*dy(j)*coeffS(i,j,k)
                            win(i,j,k)=2*vel_in
                         end select
                      end if
                   end if
                end do
             end do
          end do
          !-------------------------------------------------------------------------------
          ! mean value correction
          !-------------------------------------------------------------------------------
          select case(ns%bound%backward)
          case(41)
             var1=bc_ns%backward%win*4*bc_ns%backward%dxyz2(1)*bc_ns%backward%dxyz2(2)
             call mpi_allreduce(var2l,var2,1,mpi_double_precision,mpi_sum,comm3d,code)
             do k=szw,ezw
                do j=syw,eyw
                   do i=sxw,exw
                      if (k==gszw) then
                         if (   abs(grid_x(i)-bc_ns%backward%xyz(1))<=bc_ns%backward%dxyz2(1) .and. &
                              & abs(grid_y(j)-bc_ns%backward%xyz(2))<=bc_ns%backward%dxyz2(2)) then
                            win(i,j,k)=win(i,j,k)*var1/var2
                         end if
                      end if
                   end do
                end do
             end do
          end select
          !-------------------------------------------------------------------------------
       end select
       !-------------------------------------------------------------------------------
       ! forward
       !-------------------------------------------------------------------------------
       select case(abs(ns%bound%forward))
       case(40,41)
          var1l=0;var2l=0
          do k=szu,ezu
             do j=syu,eyu
                do i=sxu,exu
                   !-------------------------------------------------------------------------------
                   ! U component
                   !-------------------------------------------------------------------------------
                   if (k==gezu) then
                      penu(i,j,k,:)=0
                      penu(i,j,k,1)=full_pen
                   end if
                end do
             end do
          end do
          do k=szv,ezv
             do j=syv,eyv
                do i=sxv,exv
                   !-------------------------------------------------------------------------------
                   ! V component
                   !-------------------------------------------------------------------------------
                   if (k==gezv) then
                      penv(i,j,k,:)=0
                      penv(i,j,k,1)=full_pen
                   end if
                end do
             end do
          end do
          do k=szw,ezw
             do j=syw,eyw
                do i=sxw,exw
                   !-------------------------------------------------------------------------------
                   ! W component
                   !-------------------------------------------------------------------------------
                   if (k==gezw) then
                      penw(i,j,k,:)=0
                      penw(i,j,k,1)=half_pen
                      penw(i,j,k,6)=half_pen
                      if (   abs(grid_x(i)-bc_ns%forward%xyz(1))<=bc_ns%forward%dxyz2(1) .and. &
                           & abs(grid_y(j)-bc_ns%forward%xyz(2))<=bc_ns%forward%dxyz2(2)) then
                         select case(ns%bound%forward)
                         case(40)
                            penw(i,j,k,1)=+full_pen
                            penw(i,j,k,6)=-full_pen
                         case(41)
                            penw(i,j,k,1)=bc_ns%forward%amp
                            penw(i,j,k,6)=bc_ns%forward%amp
                            vel_in=bc_ns%forward%win
                            var1l=var1l+dx(i)*dy(j)*coeffS(i,j,k)
                            var2l=var2l+vel_in*dx(i)*dy(j)*coeffS(i,j,k)
                            win(i,j,k)=2*vel_in
                         end select
                      end if
                   end if
                end do
             end do
          end do
          !-------------------------------------------------------------------------------
          ! mean value correction
          !-------------------------------------------------------------------------------
          select case(ns%bound%forward)
          case(41)
             var1=bc_ns%forward%win*4*bc_ns%forward%dxyz2(1)*bc_ns%forward%dxyz2(2)
             call mpi_allreduce(var2l,var2,1,mpi_double_precision,mpi_sum,comm3d,code)
             do k=szw,ezw
                do j=syw,eyw
                   do i=sxw,exw
                      if (k==gezw) then
                         if (   abs(grid_x(i)-bc_ns%forward%xyz(1))<=bc_ns%forward%dxyz2(1) .and. &
                              & abs(grid_y(j)-bc_ns%forward%xyz(2))<=bc_ns%forward%dxyz2(2)) then
                            win(i,j,k)=win(i,j,k)*var1/var2
                         end if
                      end if
                   end do
                end do
             end do
          end select
          !-------------------------------------------------------------------------------
       end select
    end if
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Numerical flow rate check 
    !-------------------------------------------------------------------------------
    var1=0
    !-------------------------------------------------------------------------------
    ! X-direction, back
    !-------------------------------------------------------------------------------
    select case(abs(ns%bound%left))
    case(10,11,12,13,14,30,31,41)
       var2l=0
       once=.true.
       do k=szu,ezu
          do j=syu,eyu
             do i=sxu,exu
                if (i==gsxu) then
                   var2l=var2l+d1p2*uin(i,j,k)*dy(j)*dz(k)*coeffS(i,j,k)
                end if
             end do
          end do
       end do
       call mpi_allreduce(var2l,var2,1,mpi_double_precision,mpi_sum,comm3d,code)
       if (rank==0) then
          write(*,'(" * x- inlet (m3/s) =",1pe15.7)') var2
       end if
       var1=var1+var2
    end select
    !-------------------------------------------------------------------------------
    ! X-direction, front
    !-------------------------------------------------------------------------------
    select case(abs(ns%bound%right))
    case(10,11,12,13,14,30,31,41)
       var2l=0
       once=.true.
       do k=szu,ezu
          do j=syu,eyu
             do i=sxu,exu
                if (i==gexu) then
                   var2l=var2l+d1p2*uin(i,j,k)*dy(j)*dz(k)*coeffS(i,j,k)
                end if
             end do
          end do
       end do
       call mpi_allreduce(var2l,var2,1,mpi_double_precision,mpi_sum,comm3d,code)
       if (rank==0) then
          write(*,'(" * x+ inlet (m3/s) =",1pe15.7)') var2
       end if
       var1=var1-var2
    end select
    !-------------------------------------------------------------------------------
    ! Y-direction, back
    !-------------------------------------------------------------------------------
    select case(abs(ns%bound%bottom))
    case(10,11,12,13,14,30,31,41)
       var2l=0
       once=.true.
       do k=szv,ezv
          do j=syv,eyv
             do i=sxv,exv
                if (j==gsyv) then
                   var2l=var2l+d1p2*vin(i,j,k)*dx(i)*dz(k)*coeffS(i,j,k)
                end if
             end do
          end do
       end do
       call mpi_allreduce(var2l,var2,1,mpi_double_precision,mpi_sum,comm3d,code)
       if (rank==0) then
          write(*,'(" * y- inlet (m3/s) =",1pe15.7)') var2
       end if
       var1=var1+var2
    end select
    !-------------------------------------------------------------------------------
    ! Y-direction, front
    !-------------------------------------------------------------------------------
    select case(abs(ns%bound%top))
    case(10,11,12,13,14,30,31,41)
       var2l=0
       once=.true.
       do k=szv,ezv
          do j=syv,eyv
             do i=sxv,exv
                if (j==geyv) then
                   var2l=var2l+d1p2*vin(i,j,k)*dx(i)*dz(k)*coeffS(i,j,k)
                end if
             end do
          end do
       end do
       call mpi_allreduce(var2l,var2,1,mpi_double_precision,mpi_sum,comm3d,code)
       if (rank==0) then
          write(*,'(" * y+ inlet (m3/s) =",1pe15.7)') var2
       end if
       var1=var1-var2
    end select
    !-------------------------------------------------------------------------------
    ! Z-direction, back
    !-------------------------------------------------------------------------------
    select case(abs(ns%bound%backward))
    case(10,11,12,13,14,30,31,41)
       var2l=0
       once=.true.
       do k=szw,ezw
          do j=syw,eyw
             do i=sxw,exw
                if (k==gszw) then
                   var2l=var2l+d1p2*win(i,j,k)*dx(i)*dy(j)*coeffS(i,j,k)
                end if
             end do
          end do
       end do
       call mpi_allreduce(var2l,var2,1,mpi_double_precision,mpi_sum,comm3d,code)
       if (rank==0) then
          write(*,'(" * z- inlet (m3/s) =",1pe15.7)') var2
       end if
       var1=var1+var2
    end select
    !-------------------------------------------------------------------------------
    ! Z-direction, front
    !-------------------------------------------------------------------------------
    select case(abs(ns%bound%forward))
    case(10,11,12,13,14,30,31,41)
       var2l=0
       once=.true.
       do k=szw,ezw
          do j=syw,eyw
             do i=sxw,exw
                if (k==gezw) then
                   var2l=var2l+d1p2*win(i,j,k)*dx(i)*dy(j)*coeffS(i,j,k)
                end if
             end do
          end do
       end do
       call mpi_allreduce(var2l,var2,1,mpi_double_precision,mpi_sum,comm3d,code)
       if (rank==0) then
          write(*,'(" * z+ inlet (m3/s) =",1pe15.7)') var2
       end if
       var1=var1-var2
    end select
    !-------------------------------------------------------------------------------
    if (rank==0.and.once) then
       write(*,'(" *         balance =",1pe15.7)') var1
    end if
    !-------------------------------------------------------------------------------
    
    !-------------------------------------------------------------------------------
    ! Pressure boundary conditions
    !-------------------------------------------------------------------------------
    if (NS_pressure_pen) then
       bip=0
       pin=0
    endif
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! checking
    !-------------------------------------------------------------------------------
    i=0
    if (ns%bound%left==4.and.ns%bound%right/=4)       i=1
    if (ns%bound%left/=4.and.ns%bound%right==4)       i=1
    if (ns%bound%bottom==4.and.ns%bound%top/=4)       i=1
    if (ns%bound%bottom/=4.and.ns%bound%top==4)       i=1
    if (ns%bound%backward==4.and.ns%bound%forward/=4) i=1
    if (ns%bound%backward/=4.and.ns%bound%forward==4) i=1
    if (i==1.and.rank==0) then
       write(*,*) "Init_NS"
       write(*,*) "PERIODICITY ERROR, CHECK BCS, STOP"
       write(*,*) "ns%bound%left",ns%bound%left
       write(*,*) "ns%bound%right",ns%bound%right
       write(*,*) "ns%bound%bottom",ns%bound%bottom
       write(*,*) "ns%bound%top",ns%bound%top
       write(*,*) "ns%bound%backward",ns%bound%backward
       write(*,*) "ns%bound%forward",ns%bound%forward
       STOP
    end if
    !-------------------------------------------------------------------------------
    i=0
    j=ns%bound%left           &
         & *ns%bound%right    &
         & *ns%bound%bottom   &
         & *ns%bound%top      &
         & *ns%bound%backward &
         & *ns%bound%forward
    if (j==1.or.j==2) i=1
    if (i==1.and.rank==0) then
       write(*,*) "WARNING, ALL BCS ARE NEUMANN LIKE"
    end if
    !-------------------------------------------------------------------------------



    !-------------------------------------------------------------------------------
  end subroutine Init_BC_Navier_Stokes
  !*****************************************************************************************************


  subroutine Time_BC_Navier_Stokes(time,uin,vin,win,stdy_uin,stdy_vin,stdy_win,ns) 
    !***************************************************************************************************
    use mod_Parameters, only: rank,nproc,Periodic,nx,ny,nz,sx,ex,sy,ey,sz,ez,          &
         & sxu,exu,syu,eyu,szu,ezu,sxv,exv,syv,eyv,szv,ezv,sxw,exw,syw,eyw,szw,ezw,    &
         & NS_pressure_pen,NS_TimeBC,dx,dy,dz,dx2,dy2,dz2,pi,dpi,dx_min,dy_min,dz_min, &
         & dx,dy,dz,dxu,dyv,dzw,dt
    use mod_Constants, only: one,d1p2,d1p3,d3p4,d3p2,d1p4,zero
    use mod_struct_solver
    use mod_CoeffVS
    use mod_random
    use mod_mpi

    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), intent(in)                                   :: time
    real(8), dimension(:,:,:), allocatable, intent(inout) :: uin,vin,win
    real(8), dimension(:,:,:), allocatable, intent(inout) :: stdy_uin,stdy_vin,stdy_win
    type(solver_ns_t), intent(in)                           :: ns
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                  :: i,j,k
    real(8)                                  :: val,mean,sig,rr
    logical, save                            :: once=.true.
    !-------------------------------------------------------------------------------
    ! Vortex method
    !-------------------------------------------------------------------------------
    integer                                  :: nv,cpt
    integer, save                            :: nVortex,nVortex_tot
    real(8)                                  :: surf,dist2,den,var 
    real(8)                                  :: sigma,sigma0,tke,circ,eps,tau
    real(8)                                  :: cC,cC_mu
    real(8)                                  :: xVortex,yVortex,zVortex
    real(8), save                            :: tau0=0
    real(8), dimension(2)                    :: work
    real(8), dimension(3)                    :: work31,work32
    logical                                  :: rand_sgn,loop_xyzVortex
    logical, save                            :: init_vortex=.true.
    !-------------------------------------------------------------------------------
    ! MPI
    !-------------------------------------------------------------------------------
    integer                                  :: cNode,cNode_tot
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! init
    !-------------------------------------------------------------------------------
    if (.not.allocated(stdy_uin)) then
       allocate(stdy_uin(sxu-gx:exu+gx,syu-gy:eyu+gy,szu-gz:ezu+gz))
       allocate(stdy_vin(sxv-gx:exv+gx,syv-gy:eyv+gy,szv-gz:ezv+gz))
       if (dim==3) allocate(stdy_win(sxw-gx:exw+gx,syw-gy:eyw+gy,szw-gz:ezw+gz))
    end if
    !-------------------------------------------------------------------------------
    if (once) then 
       once=.false.
       !-------------------------------------------------------------------------------
       ! save steady part of the boundary condition
       !-------------------------------------------------------------------------------
       stdy_uin=uin
       stdy_vin=vin
       if (dim==3) stdy_win=win
       !-------------------------------------------------------------------------------
       ! nVortex per proc for vortex method
       !-------------------------------------------------------------------------------
       cNode=0
       select case(ns%bound%left)
       case(-100,-110,-120,-130)
          do k=sz,ez
             do j=sy,ey
                do i=sx,ex
                   if (i==gsx) then
                      rr=(grid_y(j)-bc_ns%left%xyz(2))**2+(grid_z(k)-bc_ns%left%xyz(3))**2
                      if ( rr <= bc_ns%left%r**2 ) then
                         cNode=cNode+1
                      end if
                   end if
                end do
             end do
          end do
       end select
       !-------------------------------------------------------------------------------
       call mpi_allreduce(cNode,cNode_tot,1,MPI_INTEGER,MPI_SUM, &
            & comm3d,code)
       !-------------------------------------------------------------------------------
       nVortex_tot=cNode_tot/4
       !-------------------------------------------------------------------------------
       if (bc_ns%left%amp>1) then
          nVortex_tot=floor(bc_ns%left%amp)
       end if
       !-------------------------------------------------------------------------------
       nVortex=nVortex_tot
       !-------------------------------------------------------------------------------
       ! init vortex sign and positions
       !-------------------------------------------------------------------------------
       allocate(sgn(nVortex))
       do nv=1,nVortex
          call random_number(var)
          sgn(nv)=sign(one,var-d1p2)
       end do
       !-------------------------------------------------------------------------------
       allocate(yVortex0(nVortex),zVortex0(nVortex))
       yVortex0=0
       zVortex0=0
       !-------------------------------------------------------------------------------
    end if
    !-------------------------------------------------------------------------------
    
    !-------------------------------------------------------------------------------
    ! Init the pertubation part of the considered variable
    !-------------------------------------------------------------------------------
    uin=0
    vin=0
    if (dim==3) win=0
    !-------------------------------------------------------------------------------
    
    !-------------------------------------------------------------------------------
    ! Add gaussian noise
    !-------------------------------------------------------------------------------
    mean=0
    !-------------------------------------------------------------------------------
    ! left
    !-------------------------------------------------------------------------------
    select case(ns%bound%left)
    case(-10,-11,-12)
       do k=szu,ezu
          do j=syu,eyu
             do i=sxu,exu
                !-------------------------------------------------------------------------------
                ! U component
                !-------------------------------------------------------------------------------
                if (i==gsxu) then
                   sig=bc_ns%left%amp*stdy_uin(i,j,k)
                   call random_gauss(val,mean,sig)
                   uin(i,j,k)=uin(i,j,k)+val
                end if
             end do
          end do
       end do
       do k=szv,ezv
          do j=syv,eyv
             do i=sxv,exv
                !-------------------------------------------------------------------------------
                ! V component
                !-------------------------------------------------------------------------------
                if (i==gsxv) then
                   sig=bc_ns%left%amp*stdy_vin(i,j,k)
                   call random_gauss(val,mean,sig)
                   vin(i,j,k)=vin(i,j,k)+val
                end if
             end do
          end do
       end do
       if (dim==3) then
          do k=szw,ezw
             do j=syw,eyw
                do i=sxw,exw
                   !-------------------------------------------------------------------------------
                   ! W component
                   !-------------------------------------------------------------------------------
                   if (i==gsxw) then
                      sig=bc_ns%left%amp*stdy_win(i,j,k)
                      call random_gauss(val,mean,sig)
                      win(i,j,k)=win(i,j,k)+val
                   end if
                end do
             end do
          end do
       end if
    end select
    !-------------------------------------------------------------------------------
    ! right
    !-------------------------------------------------------------------------------
    select case(ns%bound%right)
    case(-10,-11,-12)
       do k=szu,ezu
          do j=syu,eyu
             do i=sxu,exu
                ! U component
                if (i==gexu) then
                   sig=bc_ns%right%amp*stdy_uin(i,j,k)
                   call random_gauss(val,mean,sig)
                   uin(i,j,k)=uin(i,j,k)+val
                end if
             end do
          end do
       end do
       do k=szv,ezv
          do j=syv,eyv
             do i=sxv,exv
                ! V component
                if (i==gexv) then
                   sig=bc_ns%right%amp*stdy_vin(i,j,k)
                   call random_gauss(val,mean,sig)
                   vin(i,j,k)=vin(i,j,k)+val
                end if
             end do
          end do
       end do
       if (dim==3) then
          do k=szw,ezw
             do j=syw,eyw
                do i=sxw,exw
                   ! W component
                   if (i==gexw) then
                      sig=bc_ns%right%amp*stdy_win(i,j,k)
                      call random_gauss(val,mean,sig)
                      win(i,j,k)=win(i,j,k)+val
                   end if
                end do
             end do
          end do
       end if
    end select
    !-------------------------------------------------------------------------------
    ! bottom
    !-------------------------------------------------------------------------------
    select case(ns%bound%bottom)
    case(-10,-11,-12)
       do k=szu,ezu
          do j=syu,eyu
             do i=sxu,exu
                ! U component
                if (j==gsyu) then
                   sig=bc_ns%bottom%amp*stdy_uin(i,j,k)
                   call random_gauss(val,mean,sig)
                   uin(i,j,k)=uin(i,j,k)+val
                end if
             end do
          end do
       end do
       do k=szv,ezv
          do j=syv,eyv
             do i=sxv,exv
                ! V component
                if (j==gsyv) then
                   sig=bc_ns%bottom%amp*stdy_vin(i,j,k)
                   call random_gauss(val,mean,sig)
                   vin(i,j,k)=vin(i,j,k)+val
                end if
             end do
          end do
       end do
       if (dim==3) then
          do k=szw,ezw
             do j=syw,eyw
                do i=sxw,exw
                   ! W component
                   if (j==gsyw) then
                      sig=bc_ns%bottom%amp*stdy_win(i,j,k)
                      call random_gauss(val,mean,sig)
                      win(i,j,k)=win(i,j,k)+val
                   end if
                end do
             end do
          end do
       end if
    end select
    !-------------------------------------------------------------------------------
    ! top
    !-------------------------------------------------------------------------------
    select case(ns%bound%top)
    case(-10,-11,-12)
       do k=szu,ezu
          do j=syu,eyu
             do i=sxu,exu
                ! U component
                if (j==geyu) then
                   sig=bc_ns%top%amp*stdy_uin(i,j,k)
                   call random_gauss(val,mean,sig)
                   uin(i,j,k)=uin(i,j,k)+val
                end if
             end do
          end do
       end do
       do k=szv,ezv
          do j=syv,eyv
             do i=sxv,exv
                ! V component
                if (j==geyv) then
                   sig=bc_ns%top%amp*stdy_vin(i,j,k)
                   call random_gauss(val,mean,sig)
                   vin(i,j,k)=vin(i,j,k)+val
                end if
             end do
          end do
       end do
       if (dim==3) then
          do k=szw,ezw
             do j=syw,eyw
                do i=sxw,exw
                   ! W component
                   if (j==geyw) then
                      sig=bc_ns%top%amp*stdy_win(i,j,k)
                      call random_gauss(val,mean,sig)
                      win(i,j,k)=win(i,j,k)+val
                   end if
                end do
             end do
          end do
       end if
    end select
    if (dim==3) then
       !-------------------------------------------------------------------------------
       ! backward
       !-------------------------------------------------------------------------------
       select case(ns%bound%backward)
       case(-10,-11,-12)
          do k=szu,ezu
             do j=syu,eyu
                do i=sxu,exu
                   !-------------------------------------------------------------------------------
                   ! U component
                   !-------------------------------------------------------------------------------
                   if (k==gszu) then
                      sig=bc_ns%backward%amp*stdy_uin(i,j,k)
                      call random_gauss(val,mean,sig)
                      uin(i,j,k)=uin(i,j,k)+val
                   end if
                end do
             end do
          end do
          do k=szv,ezv
             do j=syv,eyv
                do i=sxv,exv
                   !-------------------------------------------------------------------------------
                   ! V component
                   !-------------------------------------------------------------------------------
                   if (k==gszv) then
                      sig=bc_ns%backward%amp*stdy_vin(i,j,k)
                      call random_gauss(val,mean,sig)
                      vin(i,j,k)=vin(i,j,k)+val
                   end if
                end do
             end do
          end do
          if (dim==3) then
             do k=szw,ezw
                do j=syw,eyw
                   do i=sxw,exw
                      !-------------------------------------------------------------------------------
                      ! W component
                      !-------------------------------------------------------------------------------
                      if (k==gszw) then
                         sig=bc_ns%backward%amp*stdy_win(i,j,k)
                         call random_gauss(val,mean,sig)
                         win(i,j,k)=win(i,j,k)+val
                      end if
                   end do
                end do
             end do
          end if
       end select
       !-------------------------------------------------------------------------------
       ! forward
       !-------------------------------------------------------------------------------
       select case(ns%bound%forward)
       case(-10,-11,-12)
          do k=szu,ezu
             do j=syu,eyu
                do i=sxu,exu
                   !-------------------------------------------------------------------------------
                   ! U component
                   !-------------------------------------------------------------------------------
                   if (k==gezu) then
                      sig=bc_ns%forward%amp*stdy_uin(i,j,k)
                      call random_gauss(val,mean,sig)
                      uin(i,j,k)=uin(i,j,k)+val
                   end if
                end do
             end do
          end do
          do k=szv,ezv
             do j=syv,eyv
                do i=sxv,exv
                   !-------------------------------------------------------------------------------
                   ! V component
                   !-------------------------------------------------------------------------------
                   if (k==gezv) then
                      sig=bc_ns%forward%amp*stdy_vin(i,j,k)
                      call random_gauss(val,mean,sig)
                      vin(i,j,k)=vin(i,j,k)+val
                   end if
                end do
             end do
          end do
          if (dim==3) then
             do k=szw,ezw
                do j=syw,eyw
                   do i=sxw,exw
                      !-------------------------------------------------------------------------------
                      ! W component
                      !-------------------------------------------------------------------------------
                      if (k==gezw) then
                         sig=bc_ns%forward%amp*stdy_win(i,j,k)
                         call random_gauss(val,mean,sig)
                         win(i,j,k)=win(i,j,k)+val
                      end if
                   end do
                end do
             end do
          end if
       end select
    end if
    !-------------------------------------------------------------------------------




    








    
    !-------------------------------------------------------------------------------
    ! vortex method
    !-------------------------------------------------------------------------------
    select case(ns%bound%left)
    case(-100,-110,-120,-130)
       !-------------------------------------------------------------------------------
       !  method variables
       !-------------------------------------------------------------------------------
       surf=pi*bc_ns%left%r**2
       !-------------------------------------------------------------------------------
       ! specify inlet dissipation and turbulent kinetic energy
       !-------------------------------------------------------------------------------
       tke=bc_ns%left%tke
       eps=bc_ns%left%eps
       !-------------------------------------------------------------------------------
       var=0.11778303565! = (2*log(3d0)-3*log(2d0))
       circ=4*sqrt((pi*surf*tke)/(3*nVortex_tot*var))
       cC_mu=0.09d0
       cC=cC_mu**d3p4
       sigma0=d1p2*cC*tke**d3p2/eps
       !-------------------------------------------------------------------------------
       ! chracteristic time
       !-------------------------------------------------------------------------------
       tau=tke/eps
       tau=100*sigma0/(0.05d0*bc_ns%left%uin)
       !-------------------------------------------------------------------------------
       ! change vortex sign 
       !-------------------------------------------------------------------------------
       tau0=tau0+dt
       !-------------------------------------------------------------------------------
       
       !-------------------------------------------------------------------------------
       ! vortexes on tangential components
       !-------------------------------------------------------------------------------
       if (rank==0) then
          do nv=1,nVortex
             if (init_vortex) then
                call random_number(work)
                !-------------------------------------------------------------------------------
                ! work(1) -> radius in [0:R[
                !-------------------------------------------------------------------------------
                work(1)=work(1)*bc_ns%left%r
                !-------------------------------------------------------------------------------
                ! work(2) -> angle in [0:2*pi[
                !-------------------------------------------------------------------------------
                work(2)=work(2)*2*pi
                !-------------------------------------------------------------------------------
                ! cartesian coordinates
                !-------------------------------------------------------------------------------
                yVortex=work(1)*cos(work(2))+bc_ns%left%xyz(2)
                zVortex=work(1)*sin(work(2))+bc_ns%left%xyz(3)
                !-------------------------------------------------------------------------------
                yVortex0(nv)=yVortex
                zVortex0(nv)=zVortex
             end if
             
             !-------------------------------------------------------------------------------
             ! work(2) -> angle in [0:2*pi[
             !-------------------------------------------------------------------------------
             call random_number(work)
             work(2)=work(2)*2*pi
             !-------------------------------------------------------------------------------
             ! displacement according to 5% of the bulk velocity
             !-------------------------------------------------------------------------------
             work(1)=work(1)*0.05d0*bc_ns%left%uin*dt
             !-------------------------------------------------------------------------------
             ! new positions
             !-------------------------------------------------------------------------------
             yVortex=yVortex0(nv)+work(1)*cos(work(2))
             zVortex=zVortex0(nv)+work(1)*sin(work(2))
             !-------------------------------------------------------------------------------
             ! test if still in the inlet
             !-------------------------------------------------------------------------------
             rr=(yVortex-bc_ns%left%xyz(2))**2+(zVortex-bc_ns%left%xyz(3))**2
             if ( rr > bc_ns%left%r**2 ) then
                yVortex=yVortex0(nv)
                zVortex=zVortex0(nv)
             end if
             !-------------------------------------------------------------------------------
             ! save old position
             !-------------------------------------------------------------------------------
             yVortex0(nv)=yVortex
             zVortex0(nv)=zVortex
             !-------------------------------------------------------------------------------
          end do
          !-------------------------------------------------------------------------------
          ! change vortex sign 
          !-------------------------------------------------------------------------------
          if (tau0>tau) then
             tau0=0 
             do nv=1,nVortex
                call random_number(var)
                sgn(nv)=sgn(nv)*sign(one,var-d1p2)
             end do
          end if
          !-------------------------------------------------------------------------------
       end if
       !-------------------------------------------------------------------------------
       init_vortex=.false.
       !-------------------------------------------------------------------------------
       
       !-------------------------------------------------------------------------------
       ! MPI
       !-------------------------------------------------------------------------------
       if (nproc>1) then
          call MPI_BCAST(yVortex0,nVortex,MPI_DOUBLE_PRECISION,0,COMM3D,CODE)
          call MPI_BCAST(zVortex0,nVortex,MPI_DOUBLE_PRECISION,0,COMM3D,CODE)
          call MPI_BCAST(sgn,nVortex,MPI_INTEGER,0,COMM3D,CODE)
       end if
       !-------------------------------------------------------------------------------
       
       !-------------------------------------------------------------------------------
       ! loop on vortexes
       !-------------------------------------------------------------------------------
       do nv=1,nVortex
          !-------------------------------------------------------------------------------
          ! V component
          !-------------------------------------------------------------------------------
          do k=szv,ezv
             do j=syv,eyv
                do i=sxv,exv
                   if (i==gsxv) then
                      rr=(grid_yv(j)-bc_ns%left%xyz(2))**2+(grid_z(k)-bc_ns%left%xyz(3))**2
                      if ( rr <= bc_ns%left%r**2 ) then                       
                         !-------------------------------------------------------------------------------
                         work(1)=yVortex0(nv)-grid_yv(j)
                         work(2)=zVortex0(nv)-grid_z(k)   
                         dist2=dot_product(work,work)
                         !-------------------------------------------------------------------------------
                         sigma=max(sigma0,dy(j),dz(k))
                         den=2*sigma**2
                         var=dist2/den
                         !-------------------------------------------------------------------------------
                         vin(i,j,k)=vin(i,j,k) &
                              & + d1p2*dpi*sgn(nv)*circ*(work(2)/dist2)*(1-exp(-var))*exp(-var)
                      end if
                   end if
                end do
             end do
          end do
          !-------------------------------------------------------------------------------
          ! W component
          !-------------------------------------------------------------------------------
          if (dim==3) then
             do k=szw,ezw
                do j=syw,eyw
                   do i=sxw,exw
                      if (i==gsxw) then
                         rr=(grid_y(j)-bc_ns%left%xyz(2))**2+(grid_zw(k)-bc_ns%left%xyz(3))**2
                         if ( rr <= bc_ns%left%r**2 ) then
                            !-------------------------------------------------------------------------------
                            work(1)=yVortex0(nv)-grid_y(j)
                            work(2)=zVortex0(nv)-grid_zw(k)
                            dist2=dot_product(work,work)
                            !-------------------------------------------------------------------------------
                            sigma=max(sigma0,dy(j),dz(k))
                            den=2*sigma**2
                            var=dist2/den
                            !-------------------------------------------------------------------------------
                            win(i,j,k)=win(i,j,k) &
                                 & +d1p2*dpi*sgn(nv)*circ*(work(1)/dist2)*(1-exp(-var))*exp(-var)
                         endif
                      end if
                   end do
                end do
             end do
          end if
       end do
       !-------------------------------------------------------------------------------

       !-------------------------------------------------------------------------------
       ! contribution to the streamwise direction
       !-------------------------------------------------------------------------------
       do k=szu,ezu
          do j=syu,eyu
             do i=sxu,exu
                !-------------------------------------------------------------------------------
                ! U component
                !-------------------------------------------------------------------------------
                if (i==gsxu) then
                   rr=(grid_y(j)-bc_ns%left%xyz(2))**2+(grid_z(k)-bc_ns%left%xyz(3))**2
                   if ( rr <= bc_ns%left%r**2 ) then
                      !-------------------------------------------------------------------------------
                      ! grad U
                      !-------------------------------------------------------------------------------
                      work32(1)=0
                      work32(2)=(stdy_uin(i,j+1,k)-stdy_uin(i,j-1,k))/(dy(j)+dy(j-1))
                      work32(3)=(stdy_uin(i,j,k+1)-stdy_uin(i,j,k-1))/(dz(k)+dz(k-1))
                      !-------------------------------------------------------------------------------
                      ! vec v in the plane (y,z)
                      !-------------------------------------------------------------------------------
                      work31(1)=0
                      work31(2)=d1p2*(dyv(j+1)*vin(i,j,k)+dyv(j)*vin(i,j+1,k))/(dyv(j+1)+dyv(j))
                      work31(3)=d1p2*(dzw(k+1)*win(i,j,k)+dzw(k)*win(i,j,k+1))/(dzw(k+1)+dzw(k))
                      !-------------------------------------------------------------------------------
                      ! normalize --> g
                      !-------------------------------------------------------------------------------
                      work32=work32/sqrt(sum(work32**2)+1d-40)
                      !-------------------------------------------------------------------------------
                      ! u = - v . g
                      !-------------------------------------------------------------------------------
                      var=-dot_product(work31,work32)
                      uin(i,j,k)=uin(i,j,k)+var
                      !-------------------------------------------------------------------------------
                   end if
                end if
             end do
          end do
       end do
       !-------------------------------------------------------------------------------

       !write(72,*) ((/yVortex0(nv),zVortex0(nv)/),nv=1,nVortex)
       
    end select

    !-------------------------------------------------------------------------------
    ! add steady part
    !-------------------------------------------------------------------------------
    uin=stdy_uin+uin
    vin=stdy_vin+vin
    if (dim==3) win=stdy_win+win
    !-------------------------------------------------------------------------------


    !-------------------------------------------------------------------------------
  end subroutine Time_BC_Navier_Stokes
  !*****************************************************************************************************



  !*****************************************************************************************************
  subroutine Init_BC_phi(pin,penp,phi,ns) 
    !***************************************************************************************************
    use mod_Parameters, only: nx,ny,nz,dx,dy,dz,&
         & sx,ex,sy,ey,sz,ez,                   &
         & sxs,exs,sys,eys,szs,ezs,             &
         & xmin,xmax,ymin,ymax,zmin,zmax,       &
         & NS_Neumann_p,NS_pressure_fix
    use mod_Constants, only: d1p2
    use mod_struct_solver
    use mod_struct_solver
    use mod_mpi

    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable   :: pin
    real(8), dimension(:,:,:,:), allocatable :: penp
    type(solver_sca_t)                      :: phi
    type(solver_ns_t)                          :: ns
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                  :: i,j,k,ii,i1,j1,k1
    real(8)                                  :: val,minx,miny,minz
    real(8)                                  :: x,y,z,rr
    real(8)                                  :: half_pen,full_pen
    real(8)                                  :: yc_in,yc_out,zc_in,zc_out,rc_in,rc_out
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! penalisation values
    !-------------------------------------------------------------------------------
    full_pen=NS_pen_BC
    half_pen=d1p2*NS_pen_BC
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Initialization of phi equation structure
    !-------------------------------------------------------------------------------
    phi%dt=dt
    allocate(phi%output%it_solver(1), &
         & phi%output%res_solver(1),  &
         & phi%output%div(1),         &
         & phi%output%divmax(1))
    phi%output%it_solver=0
    phi%output%res_solver=0
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! left, right, top, bottom, backward, forward = 
    !    1: Neumann
    !    4: Periodic
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! neumann general case !
    !-------------------------------------------------------------------------------
    phi%bound%left    = 1
    phi%bound%right   = 1
    phi%bound%top     = 1
    phi%bound%bottom  = 1
    phi%bound%backward= 1
    phi%bound%forward = 1
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! if periodicity
    !-------------------------------------------------------------------------------
    if (ns%bound%left==4)     phi%bound%left=4
    if (ns%bound%right==4)    phi%bound%right=4
    if (ns%bound%top==4)      phi%bound%top=4
    if (ns%bound%bottom==4)   phi%bound%bottom=4
    if (ns%bound%backward==4) phi%bound%backward=4
    if (ns%bound%forward==4)  phi%bound%forward=4
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Order of imposition of phi  boundary conditions
    !
    ! Periodic first --> all values at 0 
    ! Neumann --> all values at 0 : natural treatment of the Neaumann conditions
    !-------------------------------------------------------------------------------

    penp=0

    !-------------------------------------------------------------------------------
    ! Check BC types
    ! and value in one point as all BC are Neumann
    !-------------------------------------------------------------------------------
    i=0
    if (phi%bound%left==4.and.phi%bound%right/=4)       i=1
    if (phi%bound%left/=4.and.phi%bound%right==4)       i=1
    if (phi%bound%bottom==4.and.phi%bound%top/=4)       i=1
    if (phi%bound%bottom/=4.and.phi%bound%top==4)       i=1
    if (phi%bound%backward==4.and.phi%bound%forward/=4) i=1
    if (phi%bound%backward/=4.and.phi%bound%forward==4) i=1
    if (i==1) then
       write(*,*) "PERIODICITY ERROR, CHECK BCS, STOP"
       STOP
    end if

    ii=0
    pin=0
    select case(ns%bound%left)
    case(1,5,50)
       do i=sx,ex
          if (i==gsx) then 
             penp(i,:,:,1)=full_pen
             pin(i,:,:)=bc_ns%left%pin
             ii=1
          end if
       end do
    case(20)
       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                if (i==gsx) then
                   rr=(grid_y(j)-bc_ns%left%xyz(2))**2+(grid_z(k)-bc_ns%left%xyz(3))**2
                   if ( rr <= bc_ns%left%r**2 ) then
                      penp(i,j,k,1)=full_pen
                      pin(i,j,k)=bc_ns%left%pin
                      ii=1
                   end if
                end if
             end do
          end do
       end do
    case(40)
       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                if (i==gsx) then
                   if (   abs(grid_y(j)-bc_ns%left%xyz(2))<=bc_ns%left%dxyz2(2) .and. &
                        & abs(grid_z(k)-bc_ns%left%xyz(3))<=bc_ns%left%dxyz2(3)) then
                      penp(i,j,k,1)=full_pen
                      pin(i,j,k)=bc_ns%left%pin
                      ii=1
                   end if
                end if
             end do
          end do
       end do
    end select

    select case(ns%bound%right)
    case(1,5,50)
       do i=sx,ex
          if (i==gex) then 
             penp(i,:,:,1)=full_pen
             pin(i,:,:)=bc_ns%right%pin
             ii=1
          end if
       end do
    case(20)
       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                if (i==gex) then
                   rr= (grid_y(j)-bc_ns%right%xyz(2))**2+(grid_z(k)-bc_ns%right%xyz(3))**2
                   if ( rr <= bc_ns%right%r**2 ) then
                      penp(i,j,k,1)=full_pen
                      pin(i,j,k)=bc_ns%right%pin
                      ii=1
                   end if
                end if
             end do
          end do
       end do
    case(40)
       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                if (i==gex) then
                   if (   abs(grid_y(j)-bc_ns%right%xyz(2))<=bc_ns%right%dxyz2(2) .and. &
                        & abs(grid_z(k)-bc_ns%right%xyz(3))<=bc_ns%right%dxyz2(3)) then
                      penp(i,j,k,1)=full_pen
                      pin(i,j,k)=bc_ns%right%pin
                      ii=1
                   end if
                end if
             end do
          end do
       end do
    end select

    select case(ns%bound%bottom)
    case(1,5,50)
       do j=sy,ey
          if (j==gsy) then 
             penp(:,j,:,1)=full_pen
             pin(:,j,:)=bc_ns%bottom%pin
             ii=1
          end if
       end do
    case(20)
       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                if (j==gsy) then
                   rr=(grid_x(i)-bc_ns%bottom%xyz(1))**2+(grid_z(k)-bc_ns%bottom%xyz(3))**2
                   if ( rr <= bc_ns%bottom%r**2 ) then
                      penp(i,j,k,1)=full_pen
                      pin(i,j,k)=bc_ns%bottom%pin
                      ii=1
                   end if
                end if
             end do
          end do
       end do
    case(40)
       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                if (j==gsy) then
                   if (   abs(grid_x(i)-bc_ns%bottom%xyz(1))<=bc_ns%bottom%dxyz2(1) .and. &
                        & abs(grid_z(k)-bc_ns%bottom%xyz(3))<=bc_ns%bottom%dxyz2(3)) then
                      penp(i,j,k,1)=full_pen
                      pin(i,j,k)=bc_ns%bottom%pin
                      ii=1
                   end if
                end if
             end do
          end do
       end do
    end select

    select case(ns%bound%top)
    case(1,5,50)
       do j=sy,ey
          if (j==gey) then 
             penp(:,j,:,1)=full_pen
             pin(:,j,:)=bc_ns%top%pin
             ii=1
          end if
       end do
    case(20)
       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                if (j==gey) then
                   rr=(grid_x(i)-bc_ns%top%xyz(1))**2+(grid_z(k)-bc_ns%top%xyz(3))**2
                   if ( rr <= bc_ns%top%r**2 ) then
                      penp(i,j,k,1)=full_pen
                      pin(i,j,k)=bc_ns%top%pin
                      ii=1
                   end if
                end if
             end do
          end do
       end do
    case(40)
       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                if (j==gey) then
                   if (   abs(grid_x(i)-bc_ns%top%xyz(1))<=bc_ns%top%dxyz2(1) .and. &
                        & abs(grid_z(k)-bc_ns%top%xyz(3))<=bc_ns%top%dxyz2(3)) then
                      penp(i,j,k,1)=full_pen
                      pin(i,j,k)=bc_ns%top%pin
                      ii=1
                   end if
                end if
             end do
          end do
       end do
    end select

    if (dim==3) then 
       select case(ns%bound%backward)
       case(1,5,50)
          do k=sz,ez
             if (k==gsz) then 
                penp(:,:,k,1)=full_pen
                pin(:,:,k)=bc_ns%backward%pin
                ii=1
             end if
          end do
       case(20)
          do k=sz,ez
             do j=sy,ey
                do i=sx,ex
                   if (k==gsz) then
                      rr=(grid_x(i)-bc_ns%backward%xyz(1))**2+(grid_y(j)-bc_ns%backward%xyz(2))**2
                      if ( rr <= bc_ns%backward%r**2 ) then
                         penp(i,j,k,1)=full_pen
                         pin(i,j,k)=bc_ns%backward%pin
                         ii=1
                      end if
                   end if
                end do
             end do
          end do
       case(40)
          do k=sz,ez
             do j=sy,ey
                do i=sx,ex
                   if (k==gsz) then
                      if (   abs(grid_x(i)-bc_ns%backward%xyz(1))<=bc_ns%backward%dxyz2(1) .and. &
                           & abs(grid_y(j)-bc_ns%backward%xyz(2))<=bc_ns%backward%dxyz2(2)) then
                         penp(i,j,k,1)=full_pen
                         pin(i,j,k)=bc_ns%backward%pin
                         ii=1
                      end if
                   end if
                end do
             end do
          end do
       end select

       select case(ns%bound%forward)
       case(1,5,50)
          do k=sz,ez
             if (k==gez) then 
                penp(:,:,k,1)=full_pen
                pin(:,:,k)=bc_ns%forward%pin
                ii=1
             end if
          end do
       case(20)
          do k=sz,ez
             do j=sy,ey
                do i=sx,ex
                   if (k==gez) then
                      rr=(grid_x(i)-bc_ns%forward%xyz(1))**2+(grid_y(j)-bc_ns%forward%xyz(2))**2
                      if ( rr <= bc_ns%forward%r**2 ) then
                         penp(i,j,k,1)=full_pen
                         pin(i,j,k)=bc_ns%forward%pin
                         ii=1
                      end if
                   end if
                end do
             end do
          end do
       case(40)
          do k=sz,ez
             do j=sy,ey
                do i=sx,ex
                   if (k==gez) then
                      if (   abs(grid_x(i)-bc_ns%forward%xyz(1))<=bc_ns%forward%dxyz2(1) .and. &
                           & abs(grid_y(j)-bc_ns%forward%xyz(2))<=bc_ns%forward%dxyz2(2)) then
                         penp(i,j,k,1)=full_pen
                         pin(i,j,k)=bc_ns%forward%pin
                         ii=1
                      end if
                   end if
                end do
             end do
          end do

       end select
    end if
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! checking if all procs have only neumann BC
    !-------------------------------------------------------------------------------
    call mpi_allreduce(ii,i,1,mpi_integer,mpi_sum,comm3d,code)
    ii=i
    !-------------------------------------------------------------------------------
    if (ii==0) then
       if (rank==0) then 
          write(*,*) "WARNING, ALL BCS ARE NEUMANN LIKE"
       end if

       i1=gsx
       j1=gsy
       k1=gsz

       select case(NS_Neumann_p)
       case(0)
          if (rank==0) then
             write(*,*) "Pressure neumann condition is handeled by BC discretization"
          end if
       case(1)
          penp=0
          !-------------------------------------------------------------------------------
          ! X-direction 
          !-------------------------------------------------------------------------------
          do i=sx,ex
             if (i==gsx) then 
                penp(i,:,:,1)=+full_pen+penp(i,:,:,1)
                penp(i,:,:,3)=-full_pen+penp(i,:,:,3)
             end if
             if (i==gex) then 
                penp(i,:,:,1)=+full_pen+penp(i,:,:,1)
                penp(i,:,:,2)=-full_pen+penp(i,:,:,2)
             end if
          end do
          !-------------------------------------------------------------------------------
          ! Y-direction 
          !-------------------------------------------------------------------------------
          do j=sy,ey
             if (j==gsy) then 
                penp(:,j,:,1)=+full_pen+penp(:,j,:,1)
                penp(:,j,:,5)=-full_pen+penp(:,j,:,5)
             end if
             if (j==gey) then 
                penp(:,j,:,1)=+full_pen+penp(:,j,:,1)
                penp(:,j,:,4)=-full_pen+penp(:,j,:,4)
             end if
          end do
          !-------------------------------------------------------------------------------
          ! Z-direction 
          !-------------------------------------------------------------------------------
          if (dim==3) then
             do k=sz,ez
                if (k==gsz) then
                   penp(:,:,k,1)=+full_pen+penp(:,:,k,1)
                   penp(:,:,k,7)=-full_pen+penp(:,:,k,7)
                end if
                if (k==gez) then
                   penp(:,:,k,1)=+full_pen+penp(:,:,k,1)
                   penp(:,:,k,6)=-full_pen+penp(:,:,k,6)
                end if
             end do
          end if
          !-------------------------------------------------------------------------------
          if (rank==0) then
             write(*,*) "Pressure neumann condition is handeled by penalization"
          end if
       case(2)
          if (rank==0) then
             write(*,*) "Pressure neumann condition is handeled by connectitivities"
          end if
       end select
       

!!$       minx=1d40
!!$       miny=1d40
!!$       minz=1d40
!!$       i1=-10
!!$       j1=-10
!!$       k1=-10
!!$       if (bc_ns%left%type==21.or.bc_ns%left%type==41) then
!!$          i1=gsx
!!$          do k=sz,ez
!!$             do j=sy,ey
!!$                val=abs(grid_y(j)-bc_ns%left%xyz(2))
!!$                if (val<miny) then
!!$                   miny=val
!!$                   j1=j
!!$                end if
!!$                val=abs(grid_z(k)-bc_ns%left%xyz(3))
!!$                if (val<minz) then
!!$                   minz=val
!!$                   k1=k
!!$                end if
!!$             end do
!!$          end do
!!$       elseif (bc_ns%right%type==21.or.bc_ns%right%type==41) then
!!$       elseif (bc_ns%bottom%type==21.or.bc_ns%bottom%type==41) then
!!$       elseif (bc_ns%top%type==21.or.bc_ns%top%type==41) then
!!$       elseif (bc_ns%backward%type==21.or.bc_ns%backward%type==41) then
!!$       elseif (bc_ns%forward%type==21.or.bc_ns%forward%type==41) then
!!$          k1=gez
!!$          do j=sy,ey
!!$             do i=sx,ex
!!$                x=grid_x(i)
!!$                y=grid_y(j)
!!$                val=abs(x-bc_ns%forward%xyz(1))
!!$                if (val<minx) then
!!$                   minx=val
!!$                   i1=i
!!$                end if
!!$                val=abs(y-bc_ns%forward%xyz(2))
!!$                if (val<miny) then
!!$                   miny=val
!!$                   j1=j
!!$                end if
!!$             end do
!!$          end do
!!$       else
!!$       end if

       select case(NS_pressure_fix)
       case(1)
          if (rank==0) then 
             if (dim==2) then
                write(*,'(" Pressure correction is fixed at point index (",i3,",",i3,")")') i1,j1
                !write(*,*) "Pressure correction is fixed at point index (0,0)"
             else
                write(*,'(" Pressure correction is fixed at point index (",i3,",",i3,",",i3,")")') i1,j1,k1
                !write(*,'("Pressure correction is fixed at point index (0,0,0)")')
             end if
          end if

          pin=0
          do k=sz,ez
             do j=sy,ey
                do i=sx,ex
                   if (i==i1.and.j==j1.and.k==k1) then
                      penp(i,j,k,:)=0
                      penp(i,j,k,1)=full_pen
                   end if
                end do
             end do
          end do
       case(2)
          if (rank==0) write(*,'(" Mean pressure is substracted at each time step")') 
       end select

    end if
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
  end subroutine Init_BC_Phi
  !*****************************************************************************************************

  subroutine interpolation_profil(x_interp,y_interp,var_interp,x,y,var,n)
    !-------------------------------------------------------------------------------
    ! Description:
    !-------------------------------------------------------------------------------
    ! Fit experimental velocity profile from INRS
    !-------------------------------------------------------------------------------
    ! Modules
    !-------------------------------------------------------------------------------
    use mod_Constants, only: infty
    use mod_InterpLin
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)               :: n
    real(8), intent(in)               :: x_interp,y_interp
    real(8), intent(out)              :: var_interp
    real(8), dimension(n), intent(in) :: x,y,var
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                           :: i,i1,i2,i3
    real(8),dimension(n)              :: dist 
    !-------------------------------------------------------------------------------

    ! distance
    do i=1,n
       dist(i)=sqrt((x_interp-x(i))**2+(y_interp-y(i))**2)
    end do
    ! partial sort (3 nearest points)
    i1=minloc(dist,dim=1); dist(i1)=infty
    i2=minloc(dist,dim=1); dist(i2)=infty
    i3=minloc(dist,dim=1); dist(i3)=infty

    call interp3pts((/x_interp,y_interp/),var_interp,&
         & (/x(i1),y(i1)/),var(i1),                  &
         & (/x(i2),y(i2)/),var(i2),                  &
         & (/x(i3),y(i3)/),var(i3))

    return
    !-------------------------------------------------------------------------------
  end subroutine interpolation_profil

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
END module mod_Init_Navier
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
