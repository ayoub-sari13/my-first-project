!===============================================================================
module Bib_VOFLag_LtpOfPressureCellContainingThePoint_IJK
  !===============================================================================
  contains
  !-----------------------------------------------------------------------------  
  !> @author mohamed-amine chadil
  ! 
  !> @brief donne le ltp du point de pression le plus proche de p
  !
  !> @todo adapter cette routine pour des maillages cartesien non-constant
  !
  !> @param [in]  p    : les coordonnees du point
  !> @param [out] ltp  : the number of the pressure cell containing the
  ! the point p
  !-----------------------------------------------------------------------------
  subroutine LtpOfPressureCellContainingThePoint_IJK(point,i,j,k,ndim,grid_x,grid_y,&
                                                     grid_z)
    !===============================================================================
    !modules
    !===============================================================================
    !-------------------------------------------------------------------------------
    !Modules de definition des structures et variables => src/mod/module_...f90
    !-------------------------------------------------------------------------------
    use Module_VOFLag_SubDomain_Data,         only : s_domaines_int
    use mod_Parameters,                       only : deeptracking,sx,ex,sy,ey,sz,ez
    !-------------------------------------------------------------------------------
    !Modules de subroutines de la librairie VOF-Lag
    !-------------------------------------------------------------------------------
    use Bib_VOFLag_SubDomain_Limited_PointIn
    !-------------------------------------------------------------------------------

    !===============================================================================
    !declarations des variables
    !===============================================================================
    implicit none
    !-------------------------------------------------------------------------------
    !variables globales
    !-------------------------------------------------------------------------------
    real(8), dimension(:), allocatable, intent(in)  :: grid_x,grid_y, grid_z
    real(8), dimension(3)             , intent(in)  :: point
    integer                           , intent(in)  :: ndim
    integer                           , intent(out) :: i,j,k
    !-------------------------------------------------------------------------------
    !variables locales 
    !-------------------------------------------------------------------------------
    real(8), dimension(3)              :: p
    integer                            :: nd,for_while,for_whilex,for_whiley,for_whilez
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Mettre le point dans le domaine
    !-------------------------------------------------------------------------------
    p = 0.D0
    p(1:ndim) = max(point(1:ndim),s_domaines_int(1,1:ndim))
    p(1:ndim) = min(p    (1:ndim),s_domaines_int(2,1:ndim))
    !-------------------------------------------------------------------------------
    ! trouver la cellule de pression contenant le point de p
    !-------------------------------------------------------------------------------
    i=sx;j=sy;k=1
    for_whilex=1
    for_whiley=1
    for_whilez=1
    do while(p(1).gt.grid_x(i).and.(for_while.le.(ex-sx)))
      i=i+1
      for_whilex=for_whilex+1
    enddo
    do while(p(2).gt.grid_y(j).and.(for_while.le.(ey-sy)))
      j=j+1
      for_whiley=for_whiley+1
    enddo
    if (ndim==3) then 
      k=sz
      do while(p(3).gt.grid_z(k).and.(for_while.le.(ez-sz)))
        k=k+1
        for_whilez=for_whilez+1
      enddo
    end if 
    for_while=for_whilex+for_whiley+for_whilez
    !if (for_while .eq. (ex-sx)+(ey-sy)+(ez-sz)+1) then 
    !  stop "attention1 : LtpOfPressureCellContainingThePoint_IJK"
    !end if
    !if (abs(grid_x(i)-p(1)).gt.dx(i)) stop "attention2.1 : LtpOfPressureCellContainingThePoint_IJK"
    !if (abs(grid_y(j)-p(2)).gt.dy(j)) stop "attention2.2 : LtpOfPressureCellContainingThePoint_IJK"
    !if (abs(grid_z(k)-p(3)).gt.dz(k) .and. ndim.eq.3) stop "attention2.3 : LtpOfPressureCellContainingThePoint_IJK"

  end subroutine LtpOfPressureCellContainingThePoint_IJK

  !===============================================================================
end module Bib_VOFLag_LtpOfPressureCellContainingThePoint_IJK
!===============================================================================
  

