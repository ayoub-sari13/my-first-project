!===============================================================================
module Bib_VOFLag_SubDomain_Initialisations
    !===============================================================================
    contains
    !-----------------------------------------------------------------------------  
    !> @author amine chadil
    ! 
    !> @brief cette routine appelle les routines 
    !-----------------------------------------------------------------------------
    subroutine SubDomain_Initialisations
        !===============================================================================
        !modules
        !===============================================================================
        !-------------------------------------------------------------------------------
        !Modules de definition des structures et variables => src/mod/module_...f90
        !-------------------------------------------------------------------------------
        use mod_Parameters, only : deeptracking,grid_x,grid_y,grid_z,xmin,xmax,ymin,   &
                                   ymax,zmin,zmax,dim
        !-------------------------------------------------------------------------------
        !Modules de subroutines de la librairie RESPECT => src/Bib_VOFLag_...f90
        !-------------------------------------------------------------------------------
        use Bib_VOFLag_SubDomain_OurOwnVariablesDeclaration
        use Bib_VOFLag_SubDomain_Limited
        use Bib_VOFLag_SubDomain_periodicity
        use Bib_VOFLag_SubDomain_Neighbouring
        use Module_VOFLag_SubDomain_Data, only : s_domaines_int
        !-------------------------------------------------------------------------------

        !===============================================================================
        !declarations des variables
        !===============================================================================
        implicit none
        !-------------------------------------------------------------------------------
        !variables globales
        !-------------------------------------------------------------------------------
        !-------------------------------------------------------------------------------
        !variables locales 
        !-------------------------------------------------------------------------------
        !-------------------------------------------------------------------------------

        !-------------------------------------------------------------------------------
        if (deeptracking) write(*,*) 'entree SubDomain_Initialisations'
        !-------------------------------------------------------------------------------

        call SubDomain_OurOwnVariablesDeclaration
        call SubDomain_Limited(s_domaines_int,grid_x,grid_y,grid_z)
        !call SubDomain_periodicity(xmin,xmax,ymin,ymax,zmin,zmax,dim)
        call SubDomain_periodicity
        call SubDomain_Neighbouring(dim)

        !-------------------------------------------------------------------------------
        if (deeptracking) write(*,*) 'sortie SubDomain_Initialisations'
        !-------------------------------------------------------------------------------
    end subroutine SubDomain_Initialisations

    !===============================================================================
end module Bib_VOFLag_SubDomain_Initialisations
!===============================================================================
    




