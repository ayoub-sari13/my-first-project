!===============================================================================
module Bib_VOFLag_SubDomain_OurOwnVariablesDeclaration
   !===============================================================================
   contains
   !-----------------------------------------------------------------------------  
   !> @author jorge cesar brandle de motta
   ! 
   !> @brief remplie des variables pour simplifier le traitement des periodicite etc...
   !-----------------------------------------------------------------------------
   subroutine SubDomain_OurOwnVariablesDeclaration 
      !===============================================================================
      !modules
      !===============================================================================
      !-------------------------------------------------------------------------------
      !Modules de definition des structures et variables => src/mod/module_...f90
      !-------------------------------------------------------------------------------
      use Module_VOFLag_SubDomain_Data
      use mod_Parameters,               only : deeptracking,Periodic,dim,rank,nproc,mpi_dim
      use mod_mpi,                      only : ngbr,coords
      !-------------------------------------------------------------------------------

      !===============================================================================
      !declarations des variables
      !===============================================================================
      implicit none
      !-------------------------------------------------------------------------------
      !variables globales
      !-------------------------------------------------------------------------------
      !-------------------------------------------------------------------------------
      !variables locales 
      !-------------------------------------------------------------------------------
      integer :: nd,i
      !-------------------------------------------------------------------------------

      !-------------------------------------------------------------------------------
      if (deeptracking) write(*,*) 'entree SubDomain_OurOwnVariablesDeclaration'
      !-------------------------------------------------------------------------------

      !-------------------------------------------------------------------------------
      !m_period donne les periodicite
      !-------------------------------------------------------------------------------
      if (dim.eq.2) then
         m_period = (/Periodic(1),Periodic(2),.false./)
      else
         m_period = (/Periodic(1),Periodic(2),Periodic(3)/)
      end if 

      !-------------------------------------------------------------------------------
      !le nombre de proc dans chaque direction (la taille du tableau contenant les procs)
      !-------------------------------------------------------------------------------
      if (nproc .gt. 1) then
         m_dims(1:dim) = mpi_dim(1:dim)
      else
         m_dims = 1
      end if 
      if (dim.eq.2) m_dims(3) = -1

      !-------------------------------------------------------------------------------
      !coordonnees du proc courant dans le tableau contenant la repartition des procs
      !-------------------------------------------------------------------------------
      if (nproc .gt. 1) then
         m_coords(1:dim) = coords(1:dim)
      else
         m_coords = 0
      end if
      if (dim.eq.2) m_coords(3) = -1

      !-------------------------------------------------------------------------------
      !vois 
      !-------------------------------------------------------------------------------
      m_vois(1)=rank
      if (nproc .gt. 1) then
         m_vois(2:2*dim+1)=ngbr(1:2*dim)
         do i=2,2*dim+1
            if ( m_vois(i)==rank) then
               m_vois(i)=-1
            end if
         end do
      else 
         m_vois(1) = 0
         do nd=1,dim
            if (m_period(nd)) then
               m_vois(2*nd)=0
               m_vois(2*nd+1) = 0
            end if
         end do
      end if
      if (dim.eq.2) then
         m_vois(6:7) = -1
      end if

      !-------------------------------------------------------------------------------
      if (deeptracking) write(*,*) 'sortie SubDomain_OurOwnVariablesDeclaration'
      !-------------------------------------------------------------------------------
   end subroutine SubDomain_OurOwnVariablesDeclaration

   !===============================================================================
end module Bib_VOFLag_SubDomain_OurOwnVariablesDeclaration
!===============================================================================
  


