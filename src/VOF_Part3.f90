!========================================================================
!
!                   FUGU Version 1.0.0
!
!========================================================================
!
! Copyright  Stéphane Vincent
!
! stephane.vincent@u-pem.fr          straightparadigm@gmail.com
!
! This file is part of the FUGU Fortran library, a set of Computational 
! Fluid Dynamics subroutines devoted to the simulation of multi-scale
! multi-phase flows for resolved scale interfaces (liquid/gas/solid). 
! FUGU uses the initial developments of DyJeAT and SHALA codes from 
! Jean-Luc Estivalezes (jean-luc.estivalezes@onera.fr) and 
! Frédéric Couderc (couderc@math.univ-toulouse.fr).
!
!========================================================================
!**
!**   NAME       : Fugu.f90
!**
!**   AUTHOR     : Stéphane Vincent
!**
!**   FUNCTION   : 3D VOF subroutines of FUGU software
!**
!**   DATES      : Version 1.0.0  : from : january, 16, 2016
!**
!========================================================================

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
module mod_VOF_3D_bis
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  use mod_Constants
  use mod_Parameters

contains
  !*******************************************************************************
  !     VOF 3D PLIC
  !*******************************************************************************

  !*******************************************************************************
  subroutine calculf3d(xm1,xm2,xm3,xr,c1,c2,c3,a1,icas,ixr)
    !*******************************************************************************
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    integer, intent(out)   :: icas,ixr
    real(8), intent(inout) :: c1,c2,c3,xm1,xm2,xm3,xr
    real(8), intent(out)   :: a1
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    !We arrange the normals in the domain +
    !-------------------------------------------------------------------------------
    call rangexmxr3d(xm1,xm2,xm3,xr,c1,c2,c3,icas,ixr)
    !-------------------------------------------------------------------------------
    !Volume calculation
    !-------------------------------------------------------------------------------
    if ((icas==1) .or. (icas==2)) then
       a1 = (max(0.d0,xr)*xr*xr- &
            (max(0.d0,xr-xm3*c3)*(xr-xm3*c3)*(xr-xm3*c3))- &
            (max(0.d0,xr-xm2*c2)*(xr-xm2*c2)*(xr-xm2*c2))- &
            (max(0.d0,xr-xm1*c1)*(xr-xm1*c1)*(xr-xm1*c1))+ &
            (max(0.d0,xr-xm2*c2-xm3*c3)*(xr-xm2*c2-xm3*c3)**2)+ &
            (max(0.d0,xr-xm1*c1-xm3*c3)*(xr-xm1*c1-xm3*c3)**2)+ &
            (max(0.d0,xr-xm2*c2-xm1*c1)*(xr-xm2*c2-xm1*c1)**2)- &
            (1.d0*max(0.d0,xr-xm1*c1-xm2*c2-xm3*c3)* &
            (xr-xm1*c1-xm2*c2-xm3*c3)**2)) / (6.d0*xm1*xm2*xm3)
    elseif (icas == 3) then
       a1 = c1 * c2 * (max(0.d0,xr/xm3)-max(0.d0,xr/xm3-c3))
    else
       a1 = c1 * &
            (max(0.d0,xr)*xr-(max(0.d0,xr-xm2*c2)*(xr-xm2*c2))- &
            (max(0.d0,xr-xm3*c3)*(xr-xm3*c3))+ &
            (max(0.d0,xr-xm2*c2-xm3*c3)*(xr-xm2*c2-xm3*c3))) / &
            (2.d0*xm2*xm3)
    end if

    return

  end subroutine calculf3d
  !*******************************************************************************

  !*******************************************************************************
  subroutine calculxr3d(xm1,xm2,xm3,xr,f1,c1,c2,c3,icas)
    !*******************************************************************************
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)  :: icas
    real(8), intent(in)  :: c1,c2,c3,f1,xm1,xm2,xm3
    real(8), intent(out) :: xr
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    real(8)              :: a,a0,a1,a2,a3,b,q,v,v1,v2,v3,v4,v5,v6,v7
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    !Volume calculation with volume fraction
    !-------------------------------------------------------------------------------
    v = f1 * c1 * c2 * c3
    if (v < 0.d0) then
       v = 0.d0
    end if
    if (abs(v-1.d0) < 1d-12) then
       v = 1.d0
    end if
    !-------------------------------------------------------------------------------
    !xr calculation for icas = 1
    !-------------------------------------------------------------------------------
    if (icas == 1) then
       !-------------------------------------------------------------------------------
       !volume critique
       !-------------------------------------------------------------------------------
       a0 = (xm1*xm1*c1**3) / (6.d0*xm2*xm3)
       a1 = (xm1*c1*c1*c2) / (2.d0*xm3)
       a2 = (xm2*c2*c2*c1) / (2.d0*xm3)
       a3 = c1 * c2 * c3
       v1 = a0
       v2 = a2 - a1 + a0
       v3 = a2 + a1
       v4 = a3 - a1 - a2
       v5 = -a0 + a3 + a1 - a2
       v6 = a3 - a0
       v7 = a3
       !-------------------------------------------------------------------------------
       !calculation of xr
       !-------------------------------------------------------------------------------
       if ((v>=0.d0) .and. (v<=v1)) then
          !-------------------------------------------------------------------------------
          !0<v<v1
          !-------------------------------------------------------------------------------
          xr = (v*6.d0*xm1*xm2*xm3)**(1.d0/3.d0)
          return
       end if
       if ((v>v1) .and. (v<=v2)) then
          !-------------------------------------------------------------------------------
          !v1<v<v2
          !-------------------------------------------------------------------------------
          xr = (3.d0*xm1*c1*c1+sqrt(72.d0*v*c1*xm2*xm3-3.d0*xm1**2*c1**4)) &
               / (6.d0*c1)
          return
       end if
       if ((v>v2) .and. (v<=v3)) then
          !-------------------------------------------------------------------------------
          !v2<v<v3
          !-------------------------------------------------------------------------------
          a2 = -3.d0*(xm1*c1+xm2*c2)
          a1 = 3.d0 * (xm1*c1*xm1*c1+xm2*c2*xm2*c2)
          a0 = -xm1**3*c1**3 - xm2**3*c2**3 + v*6.d0*xm1*xm2*xm3
          q = a1/3.d0 - a2**2/9.d0
          a = (a1*a2-3.d0*a0)/6.d0 - a2**3/27.d0
          b = sqrt(-(q**3+a**2))
          xr = -(a*a+b*b)**(1.d0/6.d0)*cos(atan2(b,a)/3.d0) - a2/3.d0 + &
               (a*a+b*b)**(1.d0/6.d0)*sin(atan2(b,a)/3.d0)*sqrt(3.d0)
          return
       end if
       if ((v>v3) .and. (v<=v4)) then
          !-------------------------------------------------------------------------------
          !v3<v<v4
          !-------------------------------------------------------------------------------
          xr = (2.d0*v*xm3+xm1*c1*c1*c2+xm2*c2*c2*c1) / (2.d0*c1*c2)
          return
       end if
       if ((v>v4) .and. (v<=v5)) then
          !-------------------------------------------------------------------------------
          !v4<v<v5
          !-------------------------------------------------------------------------------
          a2 = -3.d0*xm3*c3
          a1 = 3.d0 * (xm3*c3*xm3*c3-2.d0*xm1*c1*xm2*c2)
          a0 = 3.d0*(xm1**2*c1**2*xm2*c2+xm2**2*c2**2*xm1*c1) + &
               v*6.d0*xm1*xm2*xm3 - xm3**3*c3**3
          q = a1/3.d0 - a2**2/9.d0
          a = (a1*a2-3.d0*a0)/6.d0 - a2**3/27.d0
          b = sqrt(-(q**3+a**2))
          xr = -(a*a+b*b)**(1.d0/6.d0)*cos(atan2(b,a)/3.d0) - a2/3.d0 + &
               (a*a+b*b)**(1.d0/6.d0)*sin(atan2(b,a)/3.d0)*sqrt(3.d0)
          return
       end if
       if ((v>v5) .and. (v<=v6)) then
          !-------------------------------------------------------------------------------
          !v5<v<v6
          !-------------------------------------------------------------------------------
          xr = xm2*c2 + xm3*c3 + xm1*c1/2.d0 - &
               sqrt(72.d0*c1*(c1*xm3*c3*xm2*c2-v*xm2*xm3)- &
               3.d0*xm1**2*c1**4)/(6.d0*c1)
       elseif ((v>v6) .and. (v<v7)) then
          !-------------------------------------------------------------------------------
          !v6<v<v7
          !-------------------------------------------------------------------------------
          xr = xm1*c1 + xm2*c2 + xm3*c3 - &
               (-6.d0*(v*xm1*xm2*xm3-xm1*c1*xm2*c2*xm3*c3))**(1.d0/3.d0)
       else
          xr = xm1*c1 + xm2*c2 + xm3*c3
       end if
       !-------------------------------------------------------------------------------
       !calculation of xr for icas=2
       !-------------------------------------------------------------------------------
    elseif (icas == 2) then
       !-------------------------------------------------------------------------------
       !Critical volume
       !-------------------------------------------------------------------------------
       a0 = (xm1*xm1*c1**3) / (6.d0*xm2*xm3)
       a1 = (xm1*c1*c1*c2) / (2.d0*xm3)
       a2 = (xm2*c2*c2*c1) / (2.d0*xm3)
       a3 = c1 * c2 * c3

       v1 = a0
       v2 = a2 - a1 + a0
       v3 = (xm3**3*c3**3-(xm3*c3-xm1*c1)**3.d0-(xm3*c3-xm2*c2)**3.d0) / &
            (6.d0*xm1*xm2*xm3)
       v4 = ((xm1*c1+xm2*c2)**3.d0-(xm1*c1+xm2*c2-xm3*c3)**3.d0- &
            xm1**3*c1**3-xm2**3*c2**3) / (6.d0*xm1*xm2*xm3)
       v5 = -a0 + a3 + a1 - a2
       v6 = a3 - a0
       v7 = a3
       !-------------------------------------------------------------------------------
       !calculation of xr
       !-------------------------------------------------------------------------------
       if ((v>=0.d0) .and. (v<=v1)) then
          !-------------------------------------------------------------------------------
          !0<v<v1
          !-------------------------------------------------------------------------------
          xr = (v*6.d0*xm1*xm2*xm3)**(1.d0/3.d0)
          return
       end if
       if ((v>v1) .and. (v<=v2)) then
          !-------------------------------------------------------------------------------
          !v1<v<v2
          !-------------------------------------------------------------------------------
          xr = (3.d0*xm1*c1*c1+sqrt(72.d0*v*c1*xm2*xm3-3.d0*xm1**2*c1**4)) &
               / (6.d0*c1)
          return
       end if
       if ((v>v2) .and. (v<=v3)) then
          !-------------------------------------------------------------------------------
          !v2<v<v3
          !-------------------------------------------------------------------------------
          a2 = -3.d0*(xm1*c1+xm2*c2)
          a1 = 3.d0 * (xm1*c1*xm1*c1+xm2*c2*xm2*c2)
          a0 = -xm1**3*c1**3 - xm2**3*c2**3 + v*6.d0*xm1*xm2*xm3
          q = a1/3.d0 - a2**2/9.d0
          a = (a1*a2-3.d0*a0)/6.d0 - a2**3/27.d0
          b = sqrt(-(q**3+a**2))
          xr = -(a*a+b*b)**(1.d0/6.d0)*cos(atan2(b,a)/3.d0) - a2/3.d0 + &
               (a*a+b*b)**(1.d0/6.d0)*sin(atan2(b,a)/3.d0)*sqrt(3.d0)
          return
       end if
       if ((v>v3) .and. (v<=v4)) then
          !-------------------------------------------------------------------------------
          !v3<v<v4
          !-------------------------------------------------------------------------------
          a2 = -3.d0*(xm1*c1+xm2*c2+xm3*c3)/2.d0
          a1 = 3.d0 * (xm1**2*c1**2+xm2**2*c2**2+xm3**2*c3**2) / 2.d0
          a0 = 3.d0*v*xm1*xm2*xm3 - &
               (xm1**3*c1**3+xm2**3*c2**3+xm3**3*c3**3)/2.d0
          q = a1/3.d0 - a2**2/9.d0
          a = (a1*a2-3.d0*a0)/6.d0 - a2**3/27.d0
          b = sqrt(-(q**3+a**2))
          xr = -(a*a+b*b)**(1.d0/6.d0)*cos(atan2(b,a)/3.d0) - a2/3.d0 + &
               (a*a+b*b)**(1.d0/6.d0)*sin(atan2(b,a)/3.d0)*sqrt(3.d0)
          return
       end if
       if ((v>v4) .and. (v<=v5)) then
          !-------------------------------------------------------------------------------
          !v4<v<v5
          !-------------------------------------------------------------------------------
          a2 = -3.d0*xm3*c3
          a1 = 3.d0 * (xm3*c3*xm3*c3-2.d0*xm1*c1*xm2*c2)
          a0 = 3.d0*(xm1**2*c1**2*xm2*c2+xm2**2*c2**2*xm1*c1) + &
               v*6.d0*xm1*xm2*xm3 - xm3**3*c3**3
          q = a1/3.d0 - a2**2/9.d0
          a = (a1*a2-3.d0*a0)/6.d0 - a2**3/27.d0
          b = sqrt(-(q**3+a**2))
          xr = -(a*a+b*b)**(1.d0/6.d0)*cos(atan2(b,a)/3.d0) - a2/3.d0 + &
               (a*a+b*b)**(1.d0/6.d0)*sin(atan2(b,a)/3.d0)*sqrt(3.d0)
          return
       end if
       if ((v>v5) .and. (v<=v6)) then
          !-------------------------------------------------------------------------------
          !v5<v<v6
          !-------------------------------------------------------------------------------
          xr = xm2*c2 + xm3*c3 + xm1*c1/2.d0 - &
               sqrt(72.d0*c1*(c1*xm3*c3*xm2*c2-v*xm2*xm3)- &
               3.d0*xm1**2*c1**4)/(6.d0*c1)
       elseif ((v>v6) .and. (v<v7)) then
          !-------------------------------------------------------------------------------
          !v6<v<v7
          !-------------------------------------------------------------------------------
          xr = xm1*c1 + xm2*c2 + xm3*c3 - &
               (-6.d0*(v*xm1*xm2*xm3-xm1*c1*xm2*c2*xm3*c3))**(1.d0/3.d0)
       else
          xr = xm1*c1 + xm2*c2 + xm3*c3
       end if
    else
       !-------------------------------------------------------------------------------
       !calculation of xr for icas=3
       !-------------------------------------------------------------------------------
       if (icas == 3) then
          xr = v * xm3 / (c1*c2)
          return
       end if
       !-------------------------------------------------------------------------------
       !calculation of xr for icas=4
       !-------------------------------------------------------------------------------
       if (icas == 4) then
          v1 = xm2 * c2 * c2 * c1 / (2.d0*xm3)
          v3 = c1 * c2 * c3
          v2 = v3 - v1
          a = 2.d0 * xm2 * c2 * xm3 * c3
          b = 2.d0 * v * xm2 * xm3 / c1

          if ((v>=0.d0) .and. (v<=v1)) then
             xr = sqrt(2.d0*v*xm2*xm3/c1)
             return
          end if
          if ((v>v1) .and. (v<=v2)) then
             xr = (xm2*c2*c2+2.d0*v*xm3/c1) / (2.d0*c2)
          elseif (a > b) then
             xr = xm2*c2 + xm3*c3 - sqrt(a-b)
          else
             xr = xm2*c2 + xm3*c3
          end if
       end if
    end if

    return

  end subroutine calculxr3d
  !*******************************************************************************

  !*******************************************************************************
  subroutine normaloc3d(cou,xm1,xm2,xm3,i,j,k,eps0,eps1)
    !*******************************************************************************
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)                                  :: i,j,k
    real(8), intent(in)                                  :: eps0,eps1
    real(8), intent(out)                                 :: xm1,xm2,xm3
    real(8), dimension(:,:,:),   allocatable, intent(in) :: cou
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    real(8)                                              :: pm1,pm2,pm3,inorme_pm
    real(8), dimension(3)                                :: lxcyczw,lxcywzc,lxcywzw, &
         lxwyczc,lxwyczw,lxwywzc,lxwywzw
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    !normal calculation
    !-------------------------------------------------------------------------------
    lxwywzw = (/i-1,j-1,k-1/)
    lxcywzw = (/i,j-1,k-1/)
    lxwyczw = (/i-1,j,k-1/)
    lxwywzc = (/i-1,j-1,k/)
    lxcyczw= (/i,j,k-1/)
    lxcywzc = (/i,j-1,k/)
    lxwyczc = (/i-1,j,k/)
    !-------------------------------------------------------------------------------
    !normal calculation in the domain
    !-------------------------------------------------------------------------------
    pm1 = ((normx(cou,i,j-1,k-1)- &
         normx(cou,i-1,j-1,k-1)))*0.5d0 + &
         ((normx(cou,i-1,j,k-1)- &
         normx(cou,i-1,j-1,k-1)))*0.5d0 + &
         ((normx(cou,i-1,j-1,k)- &
         normx(cou,i-1,j-1,k-1)))*0.5d0 + &
         ((normx(cou,i-1,j-1,k-1)+ &
         normx(cou,i,j,k-1)- &
         normx(cou,i,j-1,k-1)- &
         normx(cou,i-1,j,k-1)))*0.25d0 + &
         ((normx(cou,i,j-1,k)+ &
         normx(cou,i-1,j-1,k-1)- &
         normx(cou,i-1,j-1,k)- &
         normx(cou,i,j-1,k-1)))*0.25d0 + &
         ((normx(cou,i-1,j,k)+ &
         normx(cou,i-1,j-1,k-1)- &
         normx(cou,i-1,j-1,k)- &
         normx(cou,i-1,j,k-1)))*0.25d0 + &
         ((normx(cou,i-1,j,k-1)+ &
         normx(cou,i,j,k)+ &
         normx(cou,i-1,j-1,k)+ &
         normx(cou,i,j-1,k-1)- &
         normx(cou,i-1,j-1,k-1)- &
         normx(cou,i,j,k-1)- &
         normx(cou,i-1,j,k)- &
         normx(cou,i,j-1,k)))*0.125d0 + &
         normx(cou,i-1,j-1,k-1)
    pm2 = ((normz(cou,i,j-1,k-1)- &
         normz(cou,i-1,j-1,k-1)))*0.5d0 + &
         ((normz(cou,i-1,j,k-1)- &
         normz(cou,i-1,j-1,k-1)))*0.5d0 + &
         ((normz(cou,i-1,j-1,k)- &
         normz(cou,i-1,j-1,k-1)))*0.5d0 + &
         ((normz(cou,i-1,j-1,k-1)+ &
         normz(cou,i,j,k-1)- &
         normz(cou,i,j-1,k-1)- &
         normz(cou,i-1,j,k-1)))*0.25d0 + &
         ((normz(cou,i,j-1,k)+ &
         normz(cou,i-1,j-1,k-1)- &
         normz(cou,i-1,j-1,k)- &
         normz(cou,i,j-1,k-1)))*0.25d0 + &
         ((normz(cou,i-1,j,k)+ &
         normz(cou,i-1,j-1,k-1)- &
         normz(cou,i-1,j-1,k)- &
         normz(cou,i-1,j,k-1)))*0.25d0 + &
         ((normz(cou,i-1,j,k-1)+ &
         normz(cou,i,j,k)+ &
         normz(cou,i-1,j-1,k)+ &
         normz(cou,i,j-1,k-1)- &
         normz(cou,i-1,j-1,k-1)- &
         normz(cou,i,j,k-1)- &
         normz(cou,i-1,j,k)- &
         normz(cou,i,j-1,k)))*0.125d0 + &
         normz(cou,i-1,j-1,k-1)
    pm3 = ((normy(cou,i,j-1,k-1)- &
         normy(cou,i-1,j-1,k-1)))*0.5d0 + &
         ((normy(cou,i-1,j,k-1)- &
         normy(cou,i-1,j-1,k-1)))*0.5d0 + &
         ((normy(cou,i-1,j-1,k)- &
         normy(cou,i-1,j-1,k-1)))*0.5d0 + &
         ((normy(cou,i-1,j-1,k-1)+ &
         normy(cou,i,j,k-1)- &
         normy(cou,i,j-1,k-1)- &
         normy(cou,i-1,j,k-1)))*0.25d0 + &
         ((normy(cou,i,j-1,k)+ &
         normy(cou,i-1,j-1,k-1)- &
         normy(cou,i-1,j-1,k)- &
         normy(cou,i,j-1,k-1)))*0.25d0 + &
         ((normy(cou,i-1,j,k)+ &
         normy(cou,i-1,j-1,k-1)- &
         normy(cou,i-1,j-1,k)- &
         normy(cou,i-1,j,k-1)))*0.25d0 + &
         ((normy(cou,i-1,j,k-1)+ &
         normy(cou,i,j,k)+ &
         normy(cou,i-1,j-1,k)+ &
         normy(cou,i,j-1,k-1)- &
         normy(cou,i-1,j-1,k-1)- &
         normy(cou,i,j,k-1)- &
         normy(cou,i-1,j,k)- &
         normy(cou,i,j-1,k)))*0.125d0 + &
         normy(cou,i-1,j-1,k-1)
    !-------------------------------------------------------------------------------
    !if a component is small
    !-------------------------------------------------------------------------------
    if (abs(pm1) < eps0) then
       pm1 = 0.0d0
    end if
    if (abs(pm2) < eps0) then
       pm2 = 0.0d0
    end if
    if (abs(pm3) < eps0) then
       pm3 = 0.0d0
    end if
    !-------------------------------------------------------------------------------
    !if the norm of the vector is small
    !-------------------------------------------------------------------------------
    if (sqrt(pm1*pm1+pm2*pm2+pm3*pm3) < eps1) then
       pm1 = 0.0d0
       pm2 = 0.0d0
       pm3 = 1.0d0
    end if
    !-------------------------------------------------------------------------------
    !Vector normalization
    !-------------------------------------------------------------------------------
    inorme_pm=1.d0/sqrt(pm1*pm1+pm2*pm2+pm3*pm3)
    xm1 = pm1*inorme_pm 
    xm2 = pm2*inorme_pm 
    xm3 = pm3*inorme_pm 
    !-------------------------------------------------------------------------------
    !Vector reorientation
    !-------------------------------------------------------------------------------
    xm1 = -xm1
    xm2 = -xm2
    xm3 = -xm3
    !-------------------------------------------------------------------------------
    !if the vector is almost a unitary vector
    !-------------------------------------------------------------------------------
    if (cou(i,j,k) >= 1.d0) then
       xm1 = 0.0d0
       xm2 = 0.0d0
       xm3 = 1.0d0
       return
    end if
    if ((abs(abs(xm1)-1.d0)) < eps0) then
       if (xm1 < 0.d0) then
          xm1 = -1.0d0
       else
          xm1 = 1.0d0
       end if
       xm2 = 0.0d0
       xm3 = 0.0d0
    end if
    if ((abs(abs(xm2)-1.d0)) < eps0) then
       xm1 = 0.0d0
       if (xm2 < 0.d0) then
          xm2 = -1.0d0
       else
          xm2 = 1.0d0
       end if
       xm3 = 0.0d0
    end if
    if ((abs(abs(xm3)-1.d0)) < eps0) then
       xm1 = 0.0d0
       xm2 = 0.0d0
       if (xm3 < 0.d0) then
          xm3 = -1.0d0
       else
          xm3 = 1.0d0
       end if
    end if

    return

  end subroutine normaloc3d
  !*******************************************************************************

  !*******************************************************************************
  real(8) function normx(cou,i,j,k)
    !*******************************************************************************
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)                                  :: i,j,k
    real(8), dimension(:,:,:),   allocatable, intent(in) :: cou
    !-------------------------------------------------------------------------------
    normx = (cou(i+1,j  ,k  )-cou(i,j  ,k  )+ &
         cou(i+1,j+1,k  )-cou(i,j+1,k  )+ &
         cou(i+1,j  ,k+1)-cou(i,j  ,k+1)+ &
         cou(i+1,j+1,k+1)-cou(i,j+1,k+1)) / 4.d0

    return

  end function normx
  !*******************************************************************************

  !*******************************************************************************
  real(8) function normz(cou,i,j,k)
    !*******************************************************************************
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)                                  :: i,j,k
    real(8), dimension(:,:,:),   allocatable, intent(in) :: cou
    !-------------------------------------------------------------------------------
    normz = (cou(i  ,j+1,k  )-cou(i  ,j,k  )+ &
         cou(i  ,j+1,k+1)-cou(i  ,j,k+1)+ &
         cou(i+1,j+1,k  )-cou(i+1,j,k  )+ &
         cou(i+1,j+1,k+1)-cou(i+1,j,k+1)) / 4.d0

    return

  end function normz
  !*******************************************************************************

  !*******************************************************************************
  real(8) function normy(cou,i,j,k)
    !*******************************************************************************
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)                                  :: i,j,k
    real(8), dimension(:,:,:),   allocatable, intent(in) :: cou
    !-------------------------------------------------------------------------------
    normy = (cou(i  ,j  ,k+1)-cou(i  ,j  ,k)+ &
         cou(i  ,j+1,k+1)-cou(i  ,j+1,k)+ &
         cou(i+1,j  ,k+1)-cou(i+1,j  ,k)+ &
         cou(i+1,j+1,k+1)-cou(i+1,j+1,k)) / 4.d0

    return

  end function normy
  !*******************************************************************************

  !*******************************************************************************
  subroutine rangexm3d(tp0,xm1,xm2,xm3,c1,c2,c3,f1,icas,ixr)
    !*******************************************************************************
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    integer, intent(out)   :: icas,ixr
    real(8), intent(in)    :: tp0
    real(8), intent(inout) :: xm1,xm2,xm3
    real(8), intent(out)   :: c1,c2,c3,f1
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    real(8)                :: a0,b0,c0
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    !Determination of the case with 3^3 cases
    !-------------------------------------------------------------------------------
    if (xm1 > 0.0d0) then
       if (xm2 > 0.0d0) then
          if (xm3 > 0.0d0) then
             f1 = tp0
             a0 = xm1
             b0 = xm2
             c0 = xm3
             ixr = 1
             icas = 1
          elseif (xm3 < 0.0d0) then
             f1 = tp0
             a0 = xm1
             b0 = xm2
             c0 = -xm3
             ixr = 2
             icas = 1
          else
             f1 = tp0
             a0 = xm1
             b0 = xm2
             c0 = 0.0d0
             ixr = 1
             icas = 4
          end if
       elseif (xm2 < 0.0d0) then
          if (xm3 > 0.0d0) then
             f1 = tp0
             a0 = xm1
             b0 = -xm2
             c0 = xm3
             ixr = 2
             icas = 1
          elseif (xm3 < 0.0d0) then
             f1 = tp0
             a0 = xm1
             b0 = -xm2
             c0 = -xm3
             ixr = 3
             icas = 1
          else
             f1 = tp0
             a0 = xm1
             b0 = -xm2
             c0 = 0.0d0
             ixr = 2
             icas = 4
          end if
       elseif (xm3 > 0.0d0) then
          f1 = tp0
          a0 = xm1
          b0 = 0.0d0
          c0 = xm3
          ixr = 1
          icas = 4
       elseif (xm3 < 0.0d0) then
          f1 = tp0
          a0 = xm1
          b0 = 0.0d0
          c0 = -xm3
          ixr = 2
          icas = 4
       else
          f1 = tp0
          a0 = xm1
          b0 = 0.0d0
          c0 = 0.0d0
          ixr = 1
          icas = 3
       end if
    elseif (xm1 < 0.0d0) then
       if (xm2 > 0.0d0) then
          if (xm3 > 0.0d0) then
             f1 = tp0
             a0 = -xm1
             b0 = xm2
             c0 = xm3
             ixr = 2
             icas = 1
          elseif (xm3 < 0.0d0) then
             f1 = tp0
             a0 = -xm1
             b0 = xm2
             c0 = -xm3
             ixr = 3
             icas = 1
          else
             f1 = tp0
             a0 = -xm1
             b0 = xm2
             c0 = 0.0d0
             ixr = 2
             icas = 4
          end if
       elseif (xm2 < 0.0d0) then
          if (xm3 > 0.0d0) then
             f1 = tp0
             a0 = -xm1
             b0 = -xm2
             c0 = xm3
             ixr = 3
             icas = 1
          elseif (xm3 < 0.0d0) then
             f1 = 1.d0 - tp0
             a0 = -xm1
             b0 = -xm2
             c0 = -xm3
             ixr = 4
             icas = 1
          else
             f1 = 1.d0 - tp0
             a0 = -xm1
             b0 = -xm2
             c0 = 0.0d0
             ixr = 4
             icas = 4
          end if
       elseif (xm3 > 0.0d0) then
          f1 = tp0
          a0 = -xm1
          b0 = 0.0d0
          c0 = xm3
          ixr = 2
          icas = 4
       elseif (xm3 < 0.0d0) then
          f1 = 1.d0 - tp0
          a0 = -xm1
          b0 = 0.0d0
          c0 = -xm3
          ixr = 4
          icas = 4
       else
          f1 = 1.d0 - tp0
          a0 = -xm1
          b0 = 0.0d0
          c0 = 0.0d0
          ixr = 4
          icas = 3
       end if
    elseif (xm2 > 0.0d0) then
       if (xm3 > 0.0d0) then
          f1 = tp0
          a0 = 0.0d0
          b0 = xm2
          c0 = xm3
          ixr = 1
          icas = 4
       elseif (xm3 < 0.0d0) then
          f1 = tp0
          a0 = 0.0d0
          b0 = xm2
          c0 = -xm3
          ixr = 2
          icas = 4
       else
          f1 = tp0
          a0 = 0.0d0
          b0 = xm2
          c0 = 0.0d0
          ixr = 1
          icas = 3
       end if
    elseif (xm2 < 0.0d0) then
       if (xm3 > 0.0d0) then
          f1 = tp0
          a0 = 0.0d0
          b0 = -xm2
          c0 = xm3
          ixr = 2
          icas = 4
       elseif (xm3 < 0.0d0) then
          f1 = 1.d0 - tp0
          a0 = 0.0d0
          b0 = -xm2
          c0 = -xm3
          ixr = 4
          icas = 4
       else
          f1 = 1.d0 - tp0
          a0 = 0.0d0
          b0 = -xm2
          c0 = 0.0d0
          ixr = 4
          icas = 3
       end if
    elseif (xm3 > 0.0d0) then
       f1 = tp0
       a0 = 0.0d0
       b0 = 0.0d0
       c0 = xm3
       ixr = 1
       icas = 3
    elseif (xm3 < 0.0d0) then
       f1 = 1.d0 - tp0
       a0 = 0.0d0
       b0 = 0.0d0
       c0 = -xm3
       ixr = 4
       icas = 3
    else
       f1 = tp0
       a0 = 1.0d0
       b0 = 0.0d0
       c0 = 0.0d0
       ixr = 1
       icas = 3
    end if
    !-------------------------------------------------------------------------------
    !Storing normals by ascending order xm3>xm2>xm1
    !-------------------------------------------------------------------------------
    if (c0 >= b0) then
       if (b0 >= a0) then
          xm1 = a0
          xm2 = b0
          xm3 = c0
          c1 = 1.d0
          c2 = 1.d0
          c3 = 1.d0
       elseif (c0 >= a0) then
          xm1 = b0
          xm2 = a0
          xm3 = c0
          c1 = 1.d0
          c2 = 1.d0
          c3 = 1.d0
       else
          xm1 = b0
          xm2 = c0
          xm3 = a0
          c1 = 1.d0
          c2 = 1.d0
          c3 = 1.d0
       end if
    elseif (b0 <= a0) then
       xm1 = c0
       xm2 = b0
       xm3 = a0
       c1 = 1.d0
       c2 = 1.d0
       c3 = 1.d0
    elseif (c0 >= a0) then
       xm1 = a0
       xm2 = c0
       xm3 = b0
       c1 = 1.d0
       c2 = 1.d0
       c3 = 1.d0
    else
       xm1 = c0
       xm2 = a0
       xm3 = b0
       c1 = 1.d0
       c2 = 1.d0
       c3 = 1.d0
    end if
    !-------------------------------------------------------------------------------
    !check if xm1+xm2>xm3
    !-------------------------------------------------------------------------------

    if ((icas==1) .and. (xm1*c1+xm2*c2>xm3*c3)) then
       icas = 2
    end if

    return

  end subroutine rangexm3d
  !*******************************************************************************

  !*******************************************************************************
  subroutine rangexr3d(xr,xm1,xm2,xm3,c1,c2,c3,ixr)
    !*******************************************************************************
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)    :: ixr
    real(8), intent(in)    :: xm1,xm2,xm3
    real(8), intent(inout) :: xr
    real(8), intent(out)   :: c1,c2,c3
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    !Storing xr
    !-------------------------------------------------------------------------------
    c1 = 1.d0
    c2 = 1.d0
    c3 = 1.d0
    if (ixr == 1) then
       xr = xr
       return
    end if
    if (ixr == 2) then
       if (xm1 < 0.0d0) then
          xr = xm1*c1 + xr
       elseif (xm2 < 0.0d0) then
          xr = xm2*c2 + xr
       else
          xr = xm3*c3 + xr
       end if
    elseif (ixr == 3) then
       if ((xm1<0.0d0) .and. (xm2<0.0d0)) then
          xr = xm1*c1 + xm2*c2 + xr
       elseif ((xm1<0.0d0) .and. (xm3<0.0d0)) then
          xr = xm1*c1 + xm3*c3 + xr
       else
          xr = xm2*c2 + xm3*c3 + xr
       end if
    else
       xr = -xr
    end if

    return

  end subroutine rangexr3d
  !*******************************************************************************

  !*******************************************************************************
  subroutine rangexmxr3d(xm1,xm2,xm3,xr,c1,c2,c3,icas,ixr)
    !*******************************************************************************
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    integer, intent(out)   :: icas,ixr
    real(8), intent(inout) :: c1,c2,c3,xm1,xm2,xm3,xr
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    real(8)                :: a0,b0,c0,fxc,fyc,fzc
    !-------------------------------------------------------------------------------
    fxc = c1
    fyc = c2
    fzc = c3
    !-------------------------------------------------------------------------------
    !Determination of the case with 3^3 cases
    !-------------------------------------------------------------------------------
    if (xm1 > 0.0d0) then
       if (xm2 > 0.0d0) then
          if (xm3 > 0.0d0) then
             a0 = xm1
             b0 = xm2
             c0 = xm3
             xr = xr
             ixr = 1
             icas = 1
          elseif (xm3 < 0.0d0) then
             a0 = xm1
             b0 = xm2
             c0 = -xm3
             xr = xr - xm3*c3
             ixr = 2
             icas = 1
          else
             a0 = xm1
             b0 = xm2
             c0 = 0.0d0
             xr = xr
             ixr = 1
             icas = 4
          end if
       elseif (xm2 < 0.0d0) then
          if (xm3 > 0.0d0) then
             a0 = xm1
             b0 = -xm2
             c0 = xm3
             xr = xr - xm2*c2
             ixr = 2
             icas = 1
          elseif (xm3 < 0.0d0) then
             a0 = xm1
             b0 = -xm2
             c0 = -xm3
             xr = xr - xm2*c2 - xm3*c3
             ixr = 3
             icas = 1
          else
             a0 = xm1
             b0 = -xm2
             c0 = 0.0d0
             xr = xr - xm2*c2
             ixr = 2
             icas = 4
          end if
       elseif (xm3 > 0.0d0) then
          a0 = xm1
          b0 = 0.0d0
          c0 = xm3
          xr = xr
          ixr = 1
          icas = 4
       elseif (xm3 < 0.0d0) then
          a0 = xm1
          b0 = 0.0d0
          c0 = -xm3
          xr = xr - xm3*c3
          ixr = 2
          icas = 4
       else
          a0 = xm1
          b0 = 0.0d0
          c0 = 0.0d0
          xr = xr
          ixr = 1
          icas = 3
       end if
    elseif (xm1 < 0.0d0) then
       if (xm2 > 0.0d0) then
          if (xm3 > 0.0d0) then
             a0 = -xm1
             b0 = xm2
             c0 = xm3
             xr = xr - xm1*c1
             ixr = 2
             icas = 1
          elseif (xm3 < 0.0d0) then
             a0 = -xm1
             b0 = xm2
             c0 = -xm3
             xr = xr - xm1*c1 - xm3*c3
             ixr = 3
             icas = 1
          else
             a0 = -xm1
             b0 = xm2
             c0 = 0.0d0
             xr = xr - xm1*c1
             ixr = 2
             icas = 4
          end if
       elseif (xm2 < 0.0d0) then
          if (xm3 > 0.0d0) then
             a0 = -xm1
             b0 = -xm2
             c0 = xm3
             xr = xr - xm1*c1 - xm2*c2
             ixr = 3
             icas = 1
          elseif (xm3 < 0.0d0) then
             a0 = -xm1
             b0 = -xm2
             c0 = -xm3
             xr = -xr
             ixr = 4
             icas = 1
          else
             a0 = -xm1
             b0 = -xm2
             c0 = 0.0d0
             xr = -xr
             ixr = 4
             icas = 4
          end if
       elseif (xm3 > 0.0d0) then
          a0 = -xm1
          b0 = 0.0d0
          c0 = xm3
          xr = xr - xm1*c1
          ixr = 2
          icas = 4
       elseif (xm3 < 0.0d0) then
          a0 = -xm1
          b0 = 0.0d0
          c0 = -xm3
          xr = -xr
          ixr = 4
          icas = 4
       else
          a0 = -xm1
          b0 = 0.0d0
          c0 = 0.0d0
          xr = -xr
          ixr = 4
          icas = 3
       end if
    elseif (xm2 > 0.0d0) then
       if (xm3 > 0.0d0) then
          a0 = 0.0d0
          b0 = xm2
          c0 = xm3
          xr = xr
          ixr = 1
          icas = 4
       elseif (xm3 < 0.0d0) then
          a0 = 0.0d0
          b0 = xm2
          c0 = -xm3
          xr = xr - xm3*c3
          ixr = 2
          icas = 4
       else
          a0 = 0.0d0
          b0 = xm2
          c0 = 0.0d0
          xr = xr
          ixr = 1
          icas = 3
       end if
    elseif (xm2 < 0.0d0) then
       if (xm3 > 0.0d0) then
          a0 = 0.0d0
          b0 = -xm2
          c0 = xm3
          xr = xr - xm2*c2
          ixr = 2
          icas = 4
       elseif (xm3 < 0.0d0) then
          a0 = 0.0d0
          b0 = -xm2
          c0 = -xm3
          xr = -xr
          ixr = 4
          icas = 4
       else
          a0 = 0.0d0
          b0 = -xm2
          c0 = 0.0d0
          xr = -xr
          ixr = 4
          icas = 3
       end if
    elseif (xm3 > 0.0d0) then
       a0 = 0.0d0
       b0 = 0.0d0
       c0 = xm3
       xr = xr
       ixr = 1
       icas = 3
    elseif (xm3 < 0.0d0) then
       a0 = 0.0d0
       b0 = 0.0d0
       c0 = -xm3
       xr = -xr
       ixr = 4
       icas = 3
    else
       a0 = 1.0d0
       b0 = 0.0d0
       c0 = 0.0d0
       xr = xr
       ixr = 1
       icas = 3
    end if
    !-------------------------------------------------------------------------------
    !Storing normals by ascending order xm3>xm2>xm1
    !-------------------------------------------------------------------------------
    if (c0*fzc >= b0*fyc) then
       if (b0*fyc >= a0*fxc) then
          xm1 = a0
          xm2 = b0
          xm3 = c0
          c1 = fxc
          c2 = fyc
          c3 = fzc
       elseif (c0*fzc >= a0*fxc) then
          xm1 = b0
          xm2 = a0
          xm3 = c0
          c1 = fyc
          c2 = fxc
          c3 = fzc
       else
          xm1 = b0
          xm2 = c0
          xm3 = a0
          c1 = fyc
          c2 = fzc
          c3 = fxc
       end if
    elseif (b0*fyc <= a0*fxc) then
       xm1 = c0
       xm2 = b0
       xm3 = a0
       c1 = fzc
       c2 = fyc
       c3 = fxc
    elseif (c0*fzc >= a0*fxc) then
       xm1 = a0
       xm2 = c0
       xm3 = b0
       c1 = fxc
       c2 = fzc
       c3 = fyc
    else
       xm1 = c0
       xm2 = a0
       xm3 = b0
       c1 = fzc
       c2 = fxc
       c3 = fyc
    end if
    !-------------------------------------------------------------------------------
    !Check if xm1+xm2>xm3
    !-------------------------------------------------------------------------------

    if ((icas==1) .and. (xm1+xm2>xm3)) then
       icas = 2
    end if

    return

  end subroutine rangexmxr3d
  !*******************************************************************************



!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
end module mod_VOF_3D_bis
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
