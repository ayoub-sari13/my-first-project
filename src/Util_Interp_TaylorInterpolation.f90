!===============================================================================
module Bib_VOFLag_TaylorInterpolation
  !===============================================================================
  contains
  !-----------------------------------------------------------------------------  
  !> @author amine chadil
  ! 
  !> @brief cette routine interpole la grandeur pre de la grille de pression dans 
  !! le point ptlag en utilisant Taylor
  !
  !> @param [out]  ttp        : la valeur extrapolee au point ptlag.
  !> @param [in]  i,j,k      : le point de pression le plus proche du point ptlag.
  !> @param [in] ptlag      : le point ou on veut extrapoler la pression.
  !> @param [in]  pres       : la grandeur a extrapolee de la grille de pression
  !-----------------------------------------------------------------------------
  subroutine TaylorInterpolation(pres,i,j,k,norm,Order,ptlag,ttp,grad,Hess,&
                                 ThirdDerivative,ndim,grid_x,grid_y,grid_z) 
    !===============================================================================
    !modules
    !===============================================================================
    !-------------------------------------------------------------------------------
    !Modules de definition des structures et variables => src/mod/module_...f90
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------
    !Modules de subroutines de la librairie VOF-Lag
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------

    !===============================================================================
    !declarations des variables
    !===============================================================================
    implicit none
    !-------------------------------------------------------------------------------
    !variables globales
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable, intent(in)  :: pres
    real(8), dimension(:),     allocatable, intent(in)  :: grid_x,grid_y,grid_z
    real(8), dimension(ndim,ndim,ndim)    , intent(in)  :: ThirdDerivative
    real(8), dimension(ndim,ndim)         , intent(in)  :: Hess
    real(8), dimension(ndim)              , intent(in)  :: grad
    real(8), dimension(ndim)              , intent(in)  :: norm
    real(8), dimension(ndim)              , intent(in)  :: ptlag
    integer                               , intent(in)  :: i,j,k,Order,ndim
    real(8)                               , intent(out) :: ttp
    !-------------------------------------------------------------------------------
    !variables locales 
    !-------------------------------------------------------------------------------
    
    !-------------------------------------------------------------------------------
    ttp=pres(i,j,k)
    if (Order .ge. 2) then
      ttp = ttp + (ptlag(1)-grid_x(i))*grad(1)+&
                  (ptlag(2)-grid_y(j))*grad(2)
      if (ndim==3) then 
        ttp = ttp + (ptlag(3)-grid_z(k))*grad(3)
      end if 
    endif
    if (Order .ge. 3) then
      ttp = ttp + 0.5D0*((ptlag(1)-grid_x(i))*(ptlag(1)-grid_x(i))*Hess(1,1)+&
                         (ptlag(2)-grid_y(j))*(ptlag(2)-grid_y(j))*Hess(2,2))+&
                         (ptlag(1)-grid_x(i))*(ptlag(2)-grid_y(j))*Hess(1,2)
                  
      if (ndim==3) then 
        ttp = ttp +     (ptlag(1)-grid_x(i))*(ptlag(3)-grid_z(k))*Hess(1,3)+&
                        (ptlag(2)-grid_y(j))*(ptlag(3)-grid_z(k))*Hess(2,3)+&
                  0.5D0*(ptlag(3)-grid_z(k))*(ptlag(3)-grid_z(k))*Hess(3,3)
      end if 

    endif
    if (Order .ge. 4) then
      ttp = ttp + 1.D0/6.D0*((ptlag(1)-grid_x(i))*(ptlag(1)-grid_x(i))*&
                             (ptlag(1)-grid_x(i))*ThirdDerivative(1,1,1)+&
                             (ptlag(2)-grid_y(j))*(ptlag(2)-grid_y(j))*&
                             (ptlag(2)-grid_y(j))*ThirdDerivative(2,2,2)&
                            )+&
                  3.D0/6.D0*((ptlag(2)-grid_y(j))*(ptlag(1)-grid_x(i))*&
                             (ptlag(1)-grid_x(i))*ThirdDerivative(1,1,2)+& ! 2,1,1  1,2,1 pareil
                             (ptlag(2)-grid_y(j))*(ptlag(2)-grid_y(j))*&
                             (ptlag(1)-grid_x(i))*ThirdDerivative(2,2,1)& ! 2,1,2  1,2,2 pareil
                            )
      if (ndim==3) then 
          ttp = ttp + 1.D0/6.D0* (ptlag(3)-grid_z(k))*(ptlag(3)-grid_z(k))*&
                                 (ptlag(3)-grid_z(k))*ThirdDerivative(3,3,3)+&
                      3.D0/6.D0*((ptlag(2)-grid_y(j))*(ptlag(3)-grid_z(k))*&
                                 (ptlag(3)-grid_z(k))*ThirdDerivative(3,3,2)+& ! 2,3,3  3,2,3 pareil
                                 (ptlag(2)-grid_y(j))*(ptlag(2)-grid_y(j))*&
                                 (ptlag(3)-grid_z(k))*ThirdDerivative(2,2,3)+& ! 2,3,2  3,2,2 pareil
                                 (ptlag(1)-grid_x(i))*(ptlag(3)-grid_z(k))*&
                                 (ptlag(3)-grid_z(k))*ThirdDerivative(3,3,1)+& ! 1,3,3  3,1,3 pareil
                                 (ptlag(1)-grid_x(i))*(ptlag(1)-grid_x(i))*&
                                 (ptlag(3)-grid_z(k))*ThirdDerivative(1,1,3)& ! 1,3,1  3,1,1 pareil
                                )+&
                      6.D0/6.D0*((ptlag(1)-grid_x(i))*(ptlag(2)-grid_y(j))*&
                                 (ptlag(3)-grid_z(k))*ThirdDerivative(1,2,3)& ! 2,1,3  2,3,1  3,2,1 3,1,2  1,3,2 pareil
                                )

        end if 
    endif
  end subroutine TaylorInterpolation

  !===============================================================================
end module Bib_VOFLag_TaylorInterpolation
!===============================================================================
  

