!========================================================================
!
!                   FUGU Version 1.0.0
!
!========================================================================
!
! Copyright  St�phane Vincent
!
! stephane.vincent@u-pem.fr          straightparadigm@gmail.com
!
! This file is part of the FUGU Fortran library, a set of Computational 
! Fluid Dynamics subroutines devoted to the simulation of multi-scale
! multi-phase flows for resolved scale interfaces (liquid/gas/solid). 
!
!========================================================================
!**
!**   NAME       : Initialization_Lag.f90
!**
!**   AUTHOR     : Benoit Trouette
!**
!**   FUNCTION   : Modules of variables and initialization subroutines
!**
!**   DATES      : Version 1.0.0  : from : june, 16, 2015
!**
!========================================================================

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
module mod_Init_Lagrangian_Particle
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  use mod_Parameters
  use mod_Constants
  use mod_interpolation
  use mod_struct_particle_tracking
  use mod_struct_solver
  use mod_references
  use mod_struct_boundary
  
  type boundary_value
     integer :: type = -1 
  end type boundary_value
  
  type faces
     type(boundary_value) :: left,right
     type(boundary_value) :: bottom,top
     type(boundary_value) :: backward,forward
  end type faces
  
  type(faces) :: bc_lag
  
  namelist /nml_bc_lag/ bc_lag
  
contains
  
  !***********************************************************************************
  !***********************************************************************************
  subroutine Init_Particle_Lag(LPart,npart)
    !***********************************************************************************
    !***********************************************************************************
    !-------------------------------------------------------------------------------
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)                           :: npart
    type(particle_t), dimension(:), intent(inout) :: LPart
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    integer                                       :: n,i,j,k
    integer                                       :: ni,nj,nk
    real(8)                                       :: pc
    real(8)                                       :: x,y,z
    real(8)                                       :: xc,yc,r,xc0,yc0,radius
    real(8)                                       :: mu,sigma
    real(8)                                       :: xmin_loc,xmax_loc
    real(8)                                       :: ymin_loc,ymax_loc
    real(8)                                       :: zmin_loc,zmax_loc
    real(8)                                       :: dx_loc,dy_loc,dz_loc
    logical                                       :: init=.false.
    !-------------------------------------------------------------------------------
    
    !-----------------------------------------------------
    ! gravity initialization
    !-----------------------------------------------------
    gravity=(/grav_x,grav_y,grav_z/)
    !-----------------------------------------------------
    
    !-----------------------------------------------------
    ! classic Lagrangian Particle tracking
    !-----------------------------------------------------
    do n=1,nPart
       LPart(n)%active   = LPartIn%active
       LPart(n)%radius   = LPartIn%radius
       LPart(n)%radius   = min(minval(dx),minval(dy),minval(dz))/2.00000001_8
       
       ! masse volumique
       LPart(n)%rho      = LPartIn%rho
       
       Lpart(n)%mu       = LPartIn%mu
       LPart(n)%m        = LPart(n)%rho*LPart(n)%radius(1)**3*d4p3pi
       LPart(n)%RK       = LG_RK
       LPart(n)%interpol = LPartIn%interpol
       LPart(n)%r        = LPartIn%r
       LPart(n)%a        = LPartIn%a
       LPart(n)%v        = LPartIn%v
       LPart(n)%u        = LPartIn%u
       LPart(n)%phi      = LPartIn%phi

       if (sum(LPart(n)%r)==0) then
          pc=0
          call random_number(LPart(n)%r)
          LPart(n)%r(1) = LPart(n)%r(1)*(xmax-xmin-2*pc*(xmax-xmin))+xmin+pc*(xmax-xmin)
          LPart(n)%r(2) = LPart(n)%r(2)*(ymax-ymin-2*pc*(ymax-ymin))+ymin+pc*(ymax-ymin)
          if (dim==2)then
             LPart(n)%r(3)=zmin
          else
             LPart(n)%r(3) = LPart(n)%r(3)*(zmax-zmin-2*pc*(zmax-zmin))+zmin+pc*(zmax-zmin)
          end if
       end if

       LPart(n)%ijk=0
       
    end do
    
  end subroutine Init_Particle_Lag


  subroutine Init_SParticle_Lag(LPart,npart,sca)
    !-------------------------------------------------------------------------------
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)                                :: npart
    real(8), dimension(:,:,:), allocatable, intent(in) :: sca
    type(particle_t), dimension(:), intent(inout)      :: LPart
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    integer                                            :: n,i,j,k
    integer                                            :: gi,gj,gk
    integer                                            :: ni,nj,nk
    real(8)                                            :: x,y,z
    real(8)                                            :: xc,yc,r,xc0,yc0,zc0,radius
    real(8)                                            :: mu,sigma
    real(8)                                            :: xmin_loc,xmax_loc
    real(8)                                            :: ymin_loc,ymax_loc
    real(8)                                            :: zmin_loc,zmax_loc
    real(8)                                            :: dx_loc,dy_loc,dz_loc
    logical                                            :: init=.false.
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! extra particles in guard cells
    !-------------------------------------------------------------------------------
    gi=gx
    gj=gy
    gk=gz
    !-------------------------------------------------------------------------------
    select case(EN_inertial_scheme)
    case(-19:-10)
       gi=1
       gj=1
       gk=min(gk,1)
    end select
    !-------------------------------------------------------------------------------
    
    n=0
    !-----------------------------------------------------
    ! loop over all scalar cells
    !-----------------------------------------------------
    do k=sz-gk,ez+gk
       do j=sy-gj,ey+gj
          do i=sx-gi,ex+gi
             !-----------------------------------------------------
             ! loop over number of particle / cell / direction
             !-----------------------------------------------------
             do nk=1,nppdpc**(dim-2)
                do nj=1,nppdpc
                   do ni=1,nppdpc
                      !-----------------------------------------------------
                      ! index of the particle
                      ! and initialisation
                      !-----------------------------------------------------
                      n                 = n+1
                      LPart(n)%active   = .true.
                      LPart(n)%radius   = 0
                      LPart(n)%rho      = 1
                      Lpart(n)%mu       = 1
                      LPart(n)%m        = 1
                      LPart(n)%RK       = LG_RK
                      LPart(n)%interpol = LPartIn%interpol
                      LPart(n)%r        = 0
                      LPart(n)%a        = 0
                      LPart(n)%v        = 0
                      LPart(n)%u        = 0
                      LPart(n)%phi      = 0
                      LPart(n)%time     = 0
                      !-----------------------------------------------------
                      LPart(n)%ijk      = (/i,j,k/)
                      !-----------------------------------------------------
                      
                      !-----------------------------------------------------
                      ! initial local dx spacing between particles
                      ! also volume carried by a particle
                      !-----------------------------------------------------
                      LPart(n)%dl(1) = dx(i)/nppdpc
                      LPart(n)%dl(2) = dy(j)/nppdpc
                      LPart(n)%dl(3) = (dz(k)/nppdpc)**(dim-2)
                      !-----------------------------------------------------
                      
                      !-----------------------------------------------------
                      ! uniform repartition in a scalar cell
                      !-----------------------------------------------------
                      LPart(n)%r(1) = (grid_xu(i)+LPart(n)%dl(1)/2)+(ni-1)*LPart(n)%dl(1)
                      LPart(n)%r(2) = (grid_yv(j)+LPart(n)%dl(2)/2)+(nj-1)*LPart(n)%dl(2)
                      if (dim==2) then
                         LPart(n)%r(3) = d1p2*(zmax+zmin)
                      else
                         LPart(n)%r(3) = (grid_zw(k)+LPart(n)%dl(3)/2)+(nk-1)*LPart(n)%dl(3)
                      end if
                      !-----------------------------------------------------

                      !-----------------------------------------------------
                      ! value of the initial concentration
                      !-----------------------------------------------------
                      select case(EN_init_icase)
                      case(2)
                         !-----------------------------------------------------
                         ! creneau
                         !-----------------------------------------------------
                         if (dim==2) then
                            xc0    = xmin+0.5_8*(xmax-xmin)
                            yc0    = ymin+0.75_8*(ymax-ymin)
                            radius = 0.1_8
                            r=sqrt((LPart(n)%r(1)-xc0)**2+(LPart(n)%r(2)-yc0)**2)
                            if (r<radius) then
                               LPart(n)%phi = 1
                            end if
                            init = .true.
                         else
                            xc0    = xmin+0.35d0*(xmax-xmin)
                            yc0    = ymin+0.35d0*(ymax-ymin)
                            zc0    = zmin+0.35d0*(zmax-zmin)
                            radius = 0.15d0*(xmax-xmin)
                            r=sqrt((grid_x(i)-xc0)**2+(grid_y(j)-yc0)**2+(grid_z(k)-zc0)**2)
                            if (r<radius) then
                               LPart(n)%phi = 1
                            end if
                            init = .true.
                         endif
                         !-----------------------------------------------------
                      case(3) 
                         !-----------------------------------------------------
                         ! conic spot
                         !-----------------------------------------------------
                         if (dim==2) then
                            xc0    = xmin+0.5_8*(xmax-xmin)
                            yc0    = ymin+0.75_8*(ymax-ymin)
                            radius = 0.1_8
                            r=sqrt((LPart(n)%r(1)-xc0)**2+(LPart(n)%r(2)-yc0)**2)
                            if (r<radius) then
                               LPart(n)%phi = (radius-r)/(radius)
                            end if
                            init = .true.
                         else
                            call interpolation_phi(zero,sca, &
                                 & LPart(n)%r(1:dim),        &
                                 & LPart(n)%phi,1,dim)
                         end if
                         !-----------------------------------------------------
                      case(4)
                         !-----------------------------------------------------
                         ! temperature Gaussian spot
                         !-----------------------------------------------------
                         xc0    = xmin+0.5d0*(xmax-xmin)
                         yc0    = ymin+0.75d0*(ymax-ymin)
                         radius = 0.1d0
                         mu     = 0
                         sigma  = 0.05d0*max((xmax-xmin),(ymax-ymin))
                         !-------------------------------------------
                         r=sqrt((LPart(n)%r(1)-xc0)**2+(LPart(n)%r(2)-yc0)**2)
                         LPart(n)%phi = d1p8/sigma/sqrt(2*pi)*exp(-(r-mu)**2/2/sigma**2)
                         !-----------------------------------------------------
                         init = .true.
                      case(6)
                         !-------------------------------------------------------------------------------
                         ! 1D Spot
                         !-------------------------------------------------------------------------------
                         xc0    = d1p2*(xmax+xmin)
                         radius = 0.05d0*(xmax-xmin)
                         if (LPart(n)%r(1)>=xc0-radius.and.LPart(n)%r(1)<=xc0+radius) then 
                            LPart(n)%phi = 1
                         else
                            LPart(n)%phi = 0
                         end if
                         !-------------------------------------------------------------------------------
                         init = .true.
                      case(7)
                         xc0    = d1p2*(xmax+xmin)
                         radius = 0.05d0*(xmax-xmin)
                         if (LPart(n)%r(1)>=xc0-radius.and.LPart(n)%r(1)<=xc0+radius) then 
                            LPart(n)%phi = (radius-abs(LPart(n)%r(1)-xc0))/radius
                         else
                            LPart(n)%phi = 0
                         end if
                         init = .true.
                      case(22)
                         !-----------------------------------------------------
                         ! temperature spot
                         !-----------------------------------------------------
                         ! parameters
                         !-----------------------------------------------------
                         xc0    = 0.15d0
                         yc0    = 0.2d0
                         radius = 0.05d0
                         !-----------------------------------------------------
                         r=sqrt((LPart(n)%r(1)-xc0)**2+(LPart(n)%r(2)-yc0)**2)
                         if (r<radius) then
                            LPart(n)%phi = 1
                         else
                            LPart(n)%phi = 0
                         end if
                         !-----------------------------------------------------
                         init = .true.
                      case(31)
                         !-----------------------------------------------------
                         ! conic spot
                         !-----------------------------------------------------
                         xc0    = 0.15d0
                         yc0    = 0.2d0
                         radius = 0.05d0
                         r=sqrt((LPart(n)%r(1)-xc0)**2+(LPart(n)%r(2)-yc0)**2)
                         if (r<radius) then
                            LPart(n)%phi = (radius-r)/(radius)
                         end if
                         init = .true.
                         !-----------------------------------------------------
                      case default
                         call interpolation_phi(zero,sca, &
                              & LPart(n)%r(1:dim),        &
                              & LPart(n)%phi,1,dim)
                      end select
                      
                   end do
                end do
             end do
             
          end do
       end do
    end do
    
    !-----------------------------------------------------
    ! test n == nPart must be true
    !-----------------------------------------------------
    if (n/=nPart) then
       write(*,*) ' [ WARNING ] n/=nPart'
       write(*,*) 'n=',n
       write(*,*) 'nPart=',nPart
    end if
    !-----------------------------------------------------

    !--------------------------------------------------
    ! Out of bounds
    !--------------------------------------------------
    do n=1,nPart
       if (LPart(n)%r(1)<grid_xu(sx-2)) LPart(n)%active=.false.
       if (LPart(n)%r(1)>grid_xu(ex+2)) LPart(n)%active=.false.
       if (LPart(n)%r(2)<grid_yv(sy-2)) LPart(n)%active=.false.
       if (LPart(n)%r(2)>grid_yv(ey+2)) LPart(n)%active=.false.
       if (dim==3) then
          if (LPart(n)%r(3)<grid_zw(sz-2)) LPart(n)%active=.false.
          if (LPart(n)%r(3)>grid_zw(ez+2)) LPart(n)%active=.false.
       end if
    end do
    !--------------------------------------------------
    
    if (.not.init) then
       if (rank==0) then
          write(*,*) "WARNING !!! Particles initialization is missing"
          write(*,*) "WARNING !!! Particles initialization is missing"
          write(*,*) "WARNING !!! Particles initialization is missing"
          write(*,*) "WARNING !!! Particles initialization is missing"
          write(*,*) "WARNING !!! Particles initialization is missing"
          write(*,*) "Initialisation from tp field interpolation"
          write(*,*) "Initialisation from tp field interpolation"
          write(*,*) "Initialisation from tp field interpolation"
          write(*,*) "Initialisation from tp field interpolation"
          write(*,*) "Initialisation from tp field interpolation"
          write(*,*) "Consider an initialization case in LG_init.f90"
          write(*,*) "Consider an initialization case in LG_init.f90"
          write(*,*) "Consider an initialization case in LG_init.f90"
          write(*,*) "Consider an initialization case in LG_init.f90"
          write(*,*) "Consider an initialization case in LG_init.f90"
       end if
    end if

  end subroutine Init_SParticle_Lag
  
  
  subroutine Init_VParticle_Lag(LPart,npart,u,v,w)
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)                                :: npart
    real(8), dimension(:,:,:), allocatable, intent(in) :: u,v,w
    type(particle_t), dimension(:), intent(inout)      :: LPart
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    integer                                            :: n,i,j,k
    integer                                            :: ni,nj,nk
    real(8)                                            :: x,y,z
    real(8)                                            :: xc,yc,r,xc0,yc0,radius
    real(8)                                            :: mu,sigma
    real(8)                                            :: xmin_loc,xmax_loc
    real(8)                                            :: ymin_loc,ymax_loc
    real(8)                                            :: zmin_loc,zmax_loc
    real(8)                                            :: dx_loc,dy_loc,dz_loc
    logical                                            :: init=.false.
    !-------------------------------------------------------------------------------
    
    n=0
    !-----------------------------------------------------
    ! loop over all scalar cells
    !-----------------------------------------------------
    do k=sz-(dim-2),ez+(dim-2)
       do j=sy-1,ey+1
          do i=sx-1,ex+1
             !-----------------------------------------------------
             ! loop over number of particle / cell / direction
             !-----------------------------------------------------
             do nk=1,nppdpc**(dim-2)
                do nj=1,nppdpc
                   do ni=1,nppdpc
                      !-----------------------------------------------------
                      ! index of the particle
                      ! and initialisation
                      !-----------------------------------------------------
                      n=n+1
                      LPart(n)%active=.true.
                      LPart(n)%radius=0
                      LPart(n)%rho=1
                      Lpart(n)%mu=1
                      LPart(n)%m=1
                      LPart(n)%RK=LG_RK
                      LPart(n)%interpol=LPartIn%interpol
                      LPart(n)%r=0
                      LPart(n)%a=0
                      LPart(n)%v=0
                      LPart(n)%u=0
                      LPart(n)%phi=0
                      LPart(n)%time=0
                      !-----------------------------------------------------
                      LPart(n)%ijk=(/i,j,k/)
                      !-----------------------------------------------------
                      
                      !-----------------------------------------------------
                      ! initial local dx spacing between particles
                      ! also volume carried by a particle
                      !-----------------------------------------------------
                      LPart(n)%dl(1)=dx(i)/nppdpc
                      LPart(n)%dl(2)=dy(j)/nppdpc
                      LPart(n)%dl(3)=(dz(k)/nppdpc)**(dim-2)
                      !-----------------------------------------------------
                      
                      !-----------------------------------------------------
                      ! uniform repartition
                      !-----------------------------------------------------
                      LPart(n)%r(1)=(grid_xu(i)+LPart(n)%dl(1)/2)+(ni-1)*LPart(n)%dl(1)
                      LPart(n)%r(2)=(grid_yv(j)+LPart(n)%dl(2)/2)+(nj-1)*LPart(n)%dl(2)
                      if (dim==2) then
                         LPart(n)%r(3)=d1p2*(zmax+zmin)
                      else
                         LPart(n)%r(3)=(grid_zw(k)+LPart(n)%dl(3)/2)+(nk-1)*LPart(n)%dl(3)
                      end if
                      !-----------------------------------------------------
                      
                      !-----------------------------------------------------
                      ! Inital velocity field value
                      !-----------------------------------------------------
                      select case(NS_init_vel_icase)
                      case(5)
                         if (LPart(n)%r(1)>=0.and.LPart(n)%r(1)<=+1) then
                            LPart(n)%u0=(/1,0,0/)
                            LPart(n)%u=(/1,0,0/)
                            LPart(n)%phi=LPart(n)%u(1)
                         end if
                      case default
                         call interpolation_uvw(zero, &
                              & u,v,w,                &
                              & LPart(n)%r(1:dim),    &
                              & LPart(n)%u0,1,dim)
                      end select
                      !-----------------------------------------------------
                   end do
                end do
             end do
             
          end do
       end do
    end do


    !-----------------------------------------------------
    ! test n == nPart must be true
    !-----------------------------------------------------
    if (n/=nPart) then
       write(*,*) 'n/=nPart, STOP / WARNING (MPI)'
       write(*,*) 'n=',n
       write(*,*) 'nPart=',nPart
       stop       
    end if
    !-----------------------------------------------------

  end subroutine Init_VParticle_Lag


  subroutine Init_CParticles(LPart,npart,sca)
    !-------------------------------------------------------------------------------
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)                                :: npart
    real(8), dimension(:,:,:), allocatable, intent(in) :: sca
    type(particle_t), dimension(:), intent(inout)      :: LPart
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    integer                                            :: n,i,j,k
    integer                                            :: gi,gj,gk
    integer                                            :: ni,nj,nk
    real(8)                                            :: x,y,z
    real(8)                                            :: xc,yc,r,xc0,yc0,zc0,radius
    real(8)                                            :: mu,sigma
    real(8)                                            :: xmin_loc,xmax_loc
    real(8)                                            :: ymin_loc,ymax_loc
    real(8)                                            :: zmin_loc,zmax_loc
    real(8)                                            :: dx_loc,dy_loc,dz_loc
    logical                                            :: init=.false.
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! extra particles in guard cells
    !-------------------------------------------------------------------------------
    gi=gx
    gj=gy
    gk=gz
    !-------------------------------------------------------------------------------
    select case(VOF_method)
    case(-39:-10)
       gi=1
       gj=1
       gk=min(gk,1)
    end select
    !-------------------------------------------------------------------------------
    
    n=0
    !-----------------------------------------------------
    ! loop over all scalar cells
    !-----------------------------------------------------
    do k=sz-gk,ez+gk
       do j=sy-gj,ey+gj
          do i=sx-gi,ex+gi
             !-----------------------------------------------------
             ! loop over number of particle / cell / direction
             !-----------------------------------------------------
             do nk=1,nppdpc**(dim-2)
                do nj=1,nppdpc
                   do ni=1,nppdpc
                      !-----------------------------------------------------
                      ! index of the particle
                      ! and initialisation
                      !-----------------------------------------------------
                      n                 = n+1
                      LPart(n)%active   = .true.
                      LPart(n)%radius   = 0
                      LPart(n)%rho      = 1
                      Lpart(n)%mu       = 1
                      LPart(n)%m        = 1
                      LPart(n)%RK       = LG_RK
                      LPart(n)%interpol = LPartIn%interpol
                      LPart(n)%r        = 0
                      LPart(n)%a        = 0
                      LPart(n)%v        = 0
                      LPart(n)%u        = 0
                      LPart(n)%phi      = 0
                      LPart(n)%time     = 0
                      !-----------------------------------------------------
                      LPart(n)%ijk      = (/i,j,k/)
                      !-----------------------------------------------------
                      
                      !-----------------------------------------------------
                      ! initial local dx spacing between particles
                      ! also volume carried by a particle
                      !-----------------------------------------------------
                      LPart(n)%dl(1) = dx(i)/nppdpc
                      LPart(n)%dl(2) = dy(j)/nppdpc
                      LPart(n)%dl(3) = (dz(k)/nppdpc)**(dim-2)
                      !-----------------------------------------------------
                      
                      !-----------------------------------------------------
                      ! uniform repartition in a scalar cell
                      !-----------------------------------------------------
                      LPart(n)%r(1) = (grid_xu(i)+LPart(n)%dl(1)/2)+(ni-1)*LPart(n)%dl(1)
                      LPart(n)%r(2) = (grid_yv(j)+LPart(n)%dl(2)/2)+(nj-1)*LPart(n)%dl(2)
                      if (dim==2) then
                         LPart(n)%r(3) = zmin !d1p2*(zmax+zmin)
                      else
                         LPart(n)%r(3) = (grid_zw(k)+LPart(n)%dl(3)/2)+(nk-1)*LPart(n)%dl(3)
                      end if
                      !-----------------------------------------------------

                      !-----------------------------------------------------
                      ! value of the initial concentration
                      !-----------------------------------------------------
                      select case(VOF_init_icase)
                      case default
                         call interpolation_phi(zero,sca,LPart(n)%r(1:dim),LPart(n)%phi,1,dim)
                         if (LPart(n)%phi>d1p2) then
                            LPart(n)%phi = 1
                         else
                            LPart(n)%phi = 0
                         end if
                      end select
                      
                   end do
                end do
             end do
             
          end do
       end do
    end do

    !-----------------------------------------------------
    ! test n == nPart must be true
    !-----------------------------------------------------
    if (n/=nPart) then
       write(*,*) ' [ WARNING ] n/=nPart'
       write(*,*) 'n=',n
       write(*,*) 'nPart=',nPart
    end if
    !-----------------------------------------------------

    !--------------------------------------------------
    ! Out of bounds
    !--------------------------------------------------
    do n=1,nPart
       if (LPart(n)%r(1)<grid_xu(sx-2)) LPart(n)%active=.false.
       if (LPart(n)%r(1)>grid_xu(ex+2)) LPart(n)%active=.false.
       if (LPart(n)%r(2)<grid_yv(sy-2)) LPart(n)%active=.false.
       if (LPart(n)%r(2)>grid_yv(ey+2)) LPart(n)%active=.false.
       if (dim==3) then
          if (LPart(n)%r(3)<grid_zw(sz-2)) LPart(n)%active=.false.
          if (LPart(n)%r(3)>grid_zw(ez+2)) LPart(n)%active=.false.
       end if
    end do
    !--------------------------------------------------
    
    if (.not.init) then
       if (rank==0) then
          write(*,*) "WARNING !!! Particles initialization is missing"
          write(*,*) "WARNING !!! Particles initialization is missing"
          write(*,*) "WARNING !!! Particles initialization is missing"
          write(*,*) "WARNING !!! Particles initialization is missing"
          write(*,*) "WARNING !!! Particles initialization is missing"
          write(*,*) "Initialisation from cou field interpolation"
          write(*,*) "Initialisation from cou field interpolation"
          write(*,*) "Initialisation from cou field interpolation"
          write(*,*) "Initialisation from cou field interpolation"
          write(*,*) "Initialisation from cou field interpolation"
          write(*,*) "Consider an initialization case in LG_init.f90"
          write(*,*) "Consider an initialization case in LG_init.f90"
          write(*,*) "Consider an initialization case in LG_init.f90"
          write(*,*) "Consider an initialization case in LG_init.f90"
          write(*,*) "Consider an initialization case in LG_init.f90"
       end if
    end if

  end subroutine Init_CParticles

  
  subroutine Init_BC_LAG
    !-----------------------------------------------------
    ! global variables
    !-----------------------------------------------------
    !-----------------------------------------------------
    ! local variables
    !-----------------------------------------------------
    integer                    :: i
    !-----------------------------------------------------

    !-------------------------------------------------------------------------------
    ! left, right, top, bottom, backward, forward = 
    !    0: Dirichlet
    !    1: Neumann
    !    2: not used
    !    3: not used
    !    4: Periodic
    !   -1: nothing 
    !-------------------------------------------------------------------------------
    open(10,file="data.in",action="read")
    read(10,nml=nml_bc_lag) ; rewind(10)
    close(10)
    !-------------------------------------------------------------------------------
    
    !-----------------------------------------------------
    ! Lagragian BC cases
    !-----------------------------------------------------
    bcs_case(1,1)=bc_lag%left%type
    bcs_case(1,2)=bc_lag%right%type
    bcs_case(2,1)=bc_lag%bottom%type
    bcs_case(2,2)=bc_lag%top%type
    bcs_case(3,1)=bc_lag%backward%type
    bcs_case(3,2)=bc_lag%forward%type
    !-----------------------------------------------------
    
    !-----------------------------------------------------
    ! Warning
    !-----------------------------------------------------
    do i=1,dim
       do j=1,2
          if (abs(bcs_case(i,j))>4) then
             write(*,*) "Warning, check BC in direction",i, &
                  & ", border", j,                          & 
                  & ", for LPT if particles are not tracers"
          end if
          if (rank==0) then
             write(*,*) "bcs_case(i,j), i=",i,", j=",j," BC=",bcs_case(i,j)
          end if
       end do
    end do
    !-----------------------------------------------------
    
  end subroutine Init_BC_LAG

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
end module mod_Init_Lagrangian_Particle
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
