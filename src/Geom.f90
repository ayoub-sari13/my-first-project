!========================================================================
!
!                   FUGU Version 1.0.0
!
!========================================================================
!**
!**   NAME       : Geom.f90
!**
!**   AUTHOR     : Benoit Trouette
!**
!**   FUNCTION   : subroutines read geometry for files
!**
!**   DATES      : Version 1.0.0  : from : feb, 2019
!**
!========================================================================

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
module mod_geometry
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  !*******************************************************************************!
  !   Modules                                                                 
  !*******************************************************************************!
  use mod_Parameters, only: rank,nproc,                &
       & dim,xmin,xmax,ymin,ymax,zmin,zmax,            &
       & sxu,exu,syu,eyu,szu,ezu,                      &
       & sxv,exv,syv,eyv,szv,ezv,                      &
       & sxw,exw,syw,eyw,szw,ezw,                      &
       & dx,dy,dz,dx2,dy2,dz2,pi,dpi,                  &
       & grid_x,grid_y,grid_z,grid_xu,grid_yv,grid_zw 
  use mod_Constants, only: d1p2,d1p4,d1p5,d1p7
  use mod_mpi
  !*******************************************************************************!

contains

  subroutine read_geom_png(tab,x,y,z)
    implicit none
    !---------------------------------------------------------------------
    ! Global variables                                            
    !---------------------------------------------------------------------
    real(8), dimension(:), allocatable, intent(in) :: x,y,z
    real(8), dimension(:,:,:), allocatable, intent(inout) :: tab
    !---------------------------------------------------------------------
    ! Local variables                                             
    !---------------------------------------------------------------------
    integer                                :: i,j,k
    integer                                :: npi,npj,npk
    real(8)                                :: dxr,dyr,dzr
    real(8), dimension(:), allocatable     :: xr,yr,zr
    real(8), dimension(:,:,:), allocatable :: tmp1,tmp2
    character(len=100)                     :: command
    !---------------------------------------------------------------------

    !---------------------------------------------------------------------
    ! png file tranformation
    !---------------------------------------------------------------------
    if (rank==0) then
       command="./png2txt.sh geom.png > geom.txt"
       call system(command)
    end if
    !---------------------------------------------------------------------

    !---------------------------------------------------------------------
    ! MPI synch
    !---------------------------------------------------------------------
    if (nproc>1) call mpi_barrier(comm3d,code)
    !---------------------------------------------------------------------

    !---------------------------------------------------------------------
    ! read pixelized file geom.txt
    !---------------------------------------------------------------------
    open(11,file="geom.txt")
    read(11,*) npi,npj,npk
    !---------------------------------------------------------------------

    !---------------------------------------------------------------------
    ! build regular grid where the geometry is drawn
    !---------------------------------------------------------------------
    allocate(xr(npi))
    allocate(yr(npj))
    allocate(zr(npk))
    allocate(tmp1(1-gx:npi+gx,1-gy:npj+gy,1-gz:npk+gz))
    allocate(tmp2(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
    !---------------------------------------------------------------------
    dxr=(xmax-xmin)/npi
    dyr=(ymax-ymin)/npj
    dzr=(zmax-zmin)/npk
    !---------------------------------------------------------------------
    ! X direction 
    do i=1,npi
       xr(i)=xmin+(i-d1p2)*dxr
    end do
    ! Y direction 
    do j=1,npj
       yr(j)=ymin+(j-d1p2)*dyr
    end do
    ! Y direction 
    do k=1,npk
       zr(k)=zmin+(k-d1p2)*dzr
    end do
    !---------------------------------------------------------------------
    if (rank==0) then
       write(*,*) "geom.png,  X/Y aspect ratio:", real(npi)/npj
       write(*,*) "           X/Z aspect ratio:", real(npi)/npk
       write(*,*) "           Y/Z aspect ratio:", real(npj)/npk
       write(*,*) "real case, X/Y aspect ratio:", (xmax-xmin)/(ymax-ymin)
       write(*,*) "           X/Z aspect ratio:", (xmax-xmin)/(zmax-zmin)
       write(*,*) "           Y/Z aspect ratio:", (ymax-ymin)/(zmax-zmin)
    end if
    !---------------------------------------------------------------------

    !---------------------------------------------------------------------
    ! read pixelized file geom.txt
    !---------------------------------------------------------------------
    tmp1=0
    do k=1,npk
       do j=1,npj
          do i=1,npi
             read(11,*) tmp1(i,j,k)
             tmp1(i,j,k)=abs(tmp1(i,j,k)-1)
             if (i==1.and.tmp1(i,j,k)>=d1p2)   tmp1(i-1,j,k)=tmp1(i,j,k)
             if (i==npi.and.tmp1(i,j,k)>=d1p2) tmp1(i+1,j,k)=tmp1(i,j,k)
             if (j==1.and.tmp1(i,j,k)>=d1p2)   tmp1(i,j-1,k)=tmp1(i,j,k)
             if (j==npj.and.tmp1(i,j,k)>=d1p2) tmp1(i,j+1,k)=tmp1(i,j,k)
             if (dim==3) then 
                if (k==1.and.tmp1(i,j,k)>=d1p2)   tmp1(i,j,k-1)=tmp1(i,j,k)
                if (k==npk.and.tmp1(i,j,k)>=d1p2) tmp1(i,j,k+1)=tmp1(i,j,k)
             end if
          end do
       end do
    end do
    close(11)
    !---------------------------------------------------------------------
    if (nproc>1) call mpi_barrier(comm3d,code)
    !if (rank==0) call system("rm -f geom.txt")
    !---------------------------------------------------------------------
    do k=lbound(tmp1,3),ubound(tmp1,3)
       tmp1(:,:,k)=tmp1(:,:,1)
    end do
    !---------------------------------------------------------------------

    !---------------------------------------------------------------------
    ! interpolate on Eulerian grid 
    !---------------------------------------------------------------------
    call change_grid(xr,yr,zr,tmp1,x,y,z,tab)
    !---------------------------------------------------------------------
    ! removal of residues
    !---------------------------------------------------------------------
    do k=sz,ez
       do j=sy,ey
          do i=sx,ex
             tab(i,j,k)=tab(i,j,k)/(tab(i,j,k)+1d-40)
             tab(i,j,k)=floor(tab(i,j,k)+d1p2)
          end do
       end do
    end do
    !---------------------------------------------------------------------
    
  end subroutine read_geom_png

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  subroutine change_grid(x,y,z,var,xi,yi,zi,vari)
    implicit none
    !---------------------------------------------------------------------
    ! Global variables                                            
    !---------------------------------------------------------------------
    real(8), dimension(:), allocatable, intent(in)        :: x,y,z
    real(8), dimension(:,:,:), allocatable, intent(in)    :: var 
    real(8), dimension(:), allocatable, intent(in)        :: xi,yi,zi
    real(8), dimension(:,:,:), allocatable, intent(inout) :: vari
    !---------------------------------------------------------------------
    ! Local variables                                             
    !---------------------------------------------------------------------
    integer                                               :: i,j,k,k1
    integer                                               :: ii,jj,kk
    real(8)                                               :: rx,ry,rz
    !---------------------------------------------------------------------

!!$    write(*,*) x(127)
!!$    stop

    
    vari=0
    do k=lbound(vari,3),ubound(vari,3)
       do j=lbound(vari,2),ubound(vari,2)
          do i=lbound(vari,1),ubound(vari,1)

             ii=floor((xi(i)-(xmin-(x(2)-x(1))/2))/(x(2)-x(1)))
             jj=floor((yi(j)-(ymin-(y(2)-y(1))/2))/(y(2)-y(1)))
             kk=(floor((zi(k)-(zmin-(z(2)-z(1))/2))/(z(2)-z(1))))**(dim-2)
!!$             ii=floor((xi(i)-xmin)/(x(2)-x(1)))
!!$             jj=floor((yi(j)-ymin)/(y(2)-y(1)))
!!$             kk=(floor((zi(k)-zmin)/(z(2)-z(1))))**(dim-2)
!!$             write(*,*) ii,jj,kk
             
!!$             do kk=lbound(var,3),ubound(var,3)-(2**(dim-2)-1)
!!$                do jj=lbound(var,2),ubound(var,2)-1
!!$                   do ii=lbound(var,1),ubound(var,1)-1

             
!!$             write(*,*) i,ii,xi(i),x(ii),x(ii+1)
!!$             
                      if ( xi(i)>=x(ii).and.xi(i)<x(ii+1) &
                           & .and.yi(j)>=y(jj).and.yi(j)<y(jj+1) ) then
                         
                         rx=(xi(i)-x(ii))/(x(ii+1)-x(ii))
                         ry=(yi(j)-y(jj))/(y(jj+1)-y(jj))
                         if (dim==3.and.zi(k)>=z(kk).and.zi(k)<z(kk+1)) then
                            rz=(zi(k)-z(kk))/(z(kk+1)-z(kk))
                         else
                            rz=0
                         end if

                         vari(i,j,k) = var(ii  ,jj  ,kk  ) * (1-rx) * (1-ry) * (1-rz) &
                              &      + var(ii+1,jj  ,kk  ) *    rx  * (1-ry) * (1-rz) &
                              &      + var(ii  ,jj+1,kk  ) * (1-rx) *    ry  * (1-rz) &
                              &      + var(ii+1,jj+1,kk  ) *    rx  *    ry  * (1-rz)
                         
                         if (dim==3) then
                            vari(i,j,k) = vari(i,j,k)  &
                                 &      + var(ii  ,jj  ,kk+1) * (1-rx) * (1-ry) *    rz &
                                 &      + var(ii+1,jj  ,kk+1) *    rx  * (1-ry) *    rz &
                                 &      + var(ii  ,jj+1,kk+1) * (1-rx) *    ry  *    rz &
                                 &      + var(ii+1,jj+1,kk+1) *    rx  *    ry  *    rz
                         end if
                         
                      end if
                      
!!$                   end do
!!$                end do
!!$             end do
             
          end do
       end do
    end do


  end subroutine change_grid

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
end module mod_geometry
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
