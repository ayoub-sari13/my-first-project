!===============================================================================
module Bib_VOFLag_Particle_PhaseFunction
  !===============================================================================
  contains
  !-----------------------------------------------------------------------------  
  !> @author amine chadil
  ! 
  !> @brief cette routine donne la fraction volumique du solide dans chaque point 
  !! des deux grille de pression et de viscosite, avec la taille physiques de la particule 
  !! dans le cas de densite, et de dimensions reduites dans le cas de viscosite
  !! (le nombre de points fictifs est egale a 10). attention: la routine est ecrite pour nc=1
  !  
  !> @param      cou_rho: la fonction couleur pour la densite
  !> @param      cou_vim: la fonction couleur pour les viscosite calculees dans les points
  !! de pression.
  !> @param      cou_vic: la fonction couleur pour les viscosite calculees dans les points
  !! de viscosite
  !> @param[out] vl     : la structure contenant toutes les informations
  !! relatives aux particules
  !> @param      mtv    : les coordonnees des points de maillage de la grille vitesse.
  !> @param      jcoeft : tableau de connexion pour la grille de pression.
  !> @param[in]  ktj    : tableau indiquant les indices des points pression et de 
  !! vitesses voisins pour le tableau jcoeft.
  !> @param      jcoefm : tableau indiquant pour chaque points de la grille de viscosité
  !! les indices des voisins sur les differentes grille.
  !> @param[in]  krm    : 
  !> @param[in]  kkm    : nombre de points de la grille de viscosite.
  !> @param[in]  kkv    : nombre de points de la grille de vitesse.
  !> @param[in]  ndim   : dimension de l'espace.
  !> @param[in]  nc     : nombre d'especes (multiphasique).
  !-----------------------------------------------------------------------------
  subroutine Particle_PhaseFunction(cou,cou_vis,cou_vts,couin0,cou_visin,cou_vtsin,vl)
    !===============================================================================
    !modules
    !===============================================================================
    !-------------------------------------------------------------------------------
    !Modules de definition des structures et variables => src/mod/module_...f90
    !-------------------------------------------------------------------------------
    use Module_VOFLag_ParticlesDataStructure
    use mod_Parameters,           only : deeptracking,grid_xu,grid_yv,grid_zw,     &
                                         grid_x,grid_y,grid_z,dim
    !-------------------------------------------------------------------------------
    !Modules de subroutines de la librairie RESPECT => src/Bib_VOFLag_...f90
    !-------------------------------------------------------------------------------
    use Bib_VOFLag_Particle_PhaseFunction_PressureMesh
    use Bib_VOFLag_Particle_PhaseFunction_VelocityMesh
    use Bib_VOFLag_Particle_PhaseFunction_ViscousMesh
    !-------------------------------------------------------------------------------
    !===============================================================================
    !declarations des variables
    !===============================================================================
    implicit none
    !-------------------------------------------------------------------------------
    !variables globales
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:,:), allocatable, intent(inout) :: cou_vis,cou_vts
    real(8), dimension(:,:,:,:), allocatable, intent(inout) :: cou_visin,cou_vtsin
    real(8), dimension(:,:,:)  , allocatable, intent(inout) :: cou,couin0
    type(struct_vof_lag),                     intent(inout) :: vl
    !-------------------------------------------------------------------------------
    !variables locales 
    !-------------------------------------------------------------------------------
    integer                                                 :: tot_pt_fictif
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'entree Particle_PhaseFunction'
    !-------------------------------------------------------------------------------
    tot_pt_fictif=10
    call Particle_PhaseFunction_PressureMesh(cou,couin0,grid_xu,grid_yv,grid_zw,dim,&
                                             tot_pt_fictif,vl)

    call Particle_PhaseFunction_ViscousMesh(cou_vis,cou_visin,grid_x,grid_y,grid_z, &
                                            grid_xu,grid_yv,grid_zw,dim,            &
                                            tot_pt_fictif,vl)
    call Particle_PhaseFunction_VelocityMesh(cou_vts,cou_vtsin,grid_x,grid_y,grid_z,&
                                             grid_xu,grid_yv,grid_zw,dim,           &
                                             tot_pt_fictif,vl)

    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'sortie Particle_PhaseFunction'
    !-------------------------------------------------------------------------------
  end subroutine Particle_PhaseFunction

  !===============================================================================
end module Bib_VOFLag_Particle_PhaseFunction
!===============================================================================
  



