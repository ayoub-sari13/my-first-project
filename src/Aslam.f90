!========================================================================
!
!                   FUGU Version 1.0.0
!
!========================================================================
!
! Copyright  Stéphane Vincent
!
! stephane.vincent@u-pem.fr          straightparadigm@gmail.com
!
! This file is part of the FUGU Fortran library, a set of Computational 
! Fluid Dynamics subroutines devoted to the simulation of multi-scale
! multi-phase flows for resolved scale interfaces (liquid/gas/solid). 
!
!========================================================================
!**
!**   NAME        : Extrapolation ASLAM
!**
!**   AUTHOR      : Amine Chadil
!**                 Stephane Vincent
!**                 Benoit Trouette
!**
!**   FUNCTION    : Extrapolation ASLAM
!**
!**   DATES       : Version 1.0.0  : from : May, 2018
!**
!**   SUBROUTINES : 
!========================================================================

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
module mod_Aslam
  use mod_Parameters, only: dim,      &
       & nproc,rank,                  &
       & sx,ex,sy,ey,sz,ez,dx,dy,dz,  &
       & gx,gy,gz,                    &
       & grid_x,grid_y,grid_z,        &
       & dx,dy,dz,dxu,dyv,dzw,        &
       & EN_init_icase
  use mod_MPI
  
contains
  
  subroutine Interp_Aslam(sca,cou,order,tab1,tab2)
    !---------------------------------------------------------------------------------------
    use mod_Constants, only: one,d1p2
    implicit none 
    !---------------------------------------------------------------------------------------
    ! Global variables
    !---------------------------------------------------------------------------------------
    integer, intent(in)                                   :: order
    real(8), allocatable, dimension(:,:,:), intent(in)    :: cou
    real(8), allocatable, dimension(:,:,:), intent(inout) :: sca,tab1,tab2
    !---------------------------------------------------------------------------------------
    ! Local variables
    !---------------------------------------------------------------------------------------
    integer                                  :: i,j,k,m
    integer                                  :: iter,iter_max
    real(8)                                  :: dt,res,norm,tmp
    real(8)                                  :: delta_ext,delta_int,sgn
    real(8)                                  :: x0,y0,z0,r0
    real(8)                                  :: varx,vary,varz
    real(8), dimension(:,:,:),   allocatable :: heav
    real(8), dimension(:,:,:),   allocatable :: dist
    real(8), dimension(:,:,:),   allocatable :: normalx,normaly,normalz
    real(8), dimension(:,:,:),   allocatable :: gradx,grady,gradz
    real(8), dimension(:,:,:),   allocatable :: work1,work2
    real(8), dimension(:,:,:,:), allocatable :: gf
    !---------------------------------------------------------------------------------------

    if (rank==0) then
       write(*,*) ''//achar(27)//&
            & '[37m===================Aslam Extrapolation========================='//&
            & achar(27)//'[0m'
    end if
    !---------------------------------------------------------------------------------------
    ! init
    !---------------------------------------------------------------------------------------
    allocate(heav(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
    allocate(work1(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
    allocate(work2(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
    allocate(gf(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz,order+1))
    allocate(dist(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
    allocate(normalx(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
    allocate(normaly(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
    allocate(gradx(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
    allocate(grady(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
    if (dim==3) then
       allocate(normalz(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
       allocate(gradz(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
    end if
    !---------------------------------------------------------------------------------------

    
    select case(EN_init_icase)
    case default 
       if (rank==0) write(*,*) "WARNING, Default case in Aslam interpolation"
       if (rank==0) write(*,*) "WARNING, Default case in Aslam interpolation"
       if (rank==0) write(*,*) "WARNING, Default case in Aslam interpolation"
       if (rank==0) write(*,*) "WARNING, Default case in Aslam interpolation"
       if (rank==0) write(*,*) "WARNING, Default case in Aslam interpolation"
       !---------------------------------------------------------------------------------------
       ! parameters sphere
       !---------------------------------------------------------------------------------------
       x0=6.4d0
       y0=6.4d0
       r0=d1p2
       delta_ext=1*min(minval(dx),minval(dy))
       delta_int=2*delta_ext
       sgn=-1
    case(8)
       !---------------------------------------------------------------------------------------
       ! test Aslam case(8)
       !---------------------------------------------------------------------------------------
       x0=0
       y0=0
       r0=2
       sgn=1
       delta_ext=0.8d0
       delta_int=0
    case(9)
       !---------------------------------------------------------------------------------------
       ! test Aslam case(9)
       !---------------------------------------------------------------------------------------
       x0=0
       y0=0
       r0=2
       sgn=-1
       delta_ext=0
       delta_int=1.5d0
       !---------------------------------------------------------------------------------------
    end select
    
    !---------------------------------------------------------------------------------------
    ! sphere distance function (negative inside sphere)
    !---------------------------------------------------------------------------------------
    do k=sz,ez
       do j=sy,ey
          do i=sx,ex
             dist(i,j,k)=(sqrt((grid_x(i)-x0)**2+(grid_y(j)-y0)**2)-r0)
          end do
       end do
    end do
    !---------------------------------------------------------------------------------------
    if (nproc>1) then
       call comm_mpi_sca(dist)
    end if
    !---------------------------------------------------------------------------------------
    ! normal computation
    !---------------------------------------------------------------------------------------
    do k=sz,ez
       do j=sy,ey
          do i=sx,ex
             normalx(i,j,k)=(dxu(i)**2*(dist(i+1,j,k)-dist(i,j,k)) &
                  & -dxu(i+1)**2*(dist(i-1,j,k)-dist(i,j,k)))    &
                  & /(dxu(i)**2*dxu(i+1)+dxu(i+1)**2*dxu(i))
             normaly(i,j,k)=(dyv(j)**2*(dist(i,j+1,k)-dist(i,j,k)) &
                  & -dyv(j+1)**2*(dist(i,j-1,k)-dist(i,j,k)))    &
                  & /(dyv(j)**2*dyv(j+1)+dyv(j+1)**2*dyv(j))
             if (dim==3) then
                normalz(i,j,k)=(dzw(k)**2*(dist(i,j,k+1)-dist(i,j,k)) &
                     & -dzw(k+1)**2*(dist(i,j,k-1)-dist(i,j,k)))    &
                     & /(dzw(k)**2*dzw(k+1)+dzw(k+1)**2*dzw(k))
             end if
          end do
       end do
    end do
    !---------------------------------------------------------------------------------------
    ! and normalization
    !---------------------------------------------------------------------------------------
    if (dim==2) then
       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                normalx(i,j,k)=normalx(i,j,k)/(sqrt(normalx(i,j,k)**2+normaly(i,j,k)**2)+1d-40)
                normaly(i,j,k)=normaly(i,j,k)/(sqrt(normalx(i,j,k)**2+normaly(i,j,k)**2)+1d-40)
             end do
          end do
       end do
    else
       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                normalx(i,j,k)=normalx(i,j,k) &
                     & /(sqrt(normalx(i,j,k)**2+normaly(i,j,k)**2+normalz(i,j,k)**2)+1d-40)
                normaly(i,j,k)=normaly(i,j,k) &
                     & /(sqrt(normalx(i,j,k)**2+normaly(i,j,k)**2+normalz(i,j,k)**2)+1d-40)
                normalz(i,j,k)=normalz(i,j,k) &
                     & /(sqrt(normalx(i,j,k)**2+normaly(i,j,k)**2+normalz(i,j,k)**2)+1d-40)
             end do
          end do
       end do
    end if
    !---------------------------------------------------------------------------------------
    
    !---------------------------------------------------------------------------------------
    ! extrapolation zone computation
    !---------------------------------------------------------------------------------------
    heav=0
    do k=sz,ez
       do j=sy,ey
          do i=sx,ex
             if (dist(i,j,k)<delta_ext.and.dist(i,j,k)>-abs(delta_int)) then 
                heav(i,j,k)=1
             end if
          end do
       end do
    end do
    !---------------------------------------------------------------------------------------
    
    !---------------------------------------------------------------------------------------
    ! g function initialization
    !---------------------------------------------------------------------------------------
    gf=0
    gf(:,:,:,1)=abs(heav-1)*sca
    !---------------------------------------------------------------------------------------
    
    !---------------------------------------------------------------------------------------
    ! rhs of PDE (from exterior), grad(f)
    !---------------------------------------------------------------------------------------
    do m=2,order
       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                if (heav(i,j,k)<d1p2) then
                   !------------------------------------------------------------------------
                   ! X-direction
                   !------------------------------------------------------------------------
                   if (sgn*normalx(i,j,k)<0) then
                      varx=-(3*gf(i,j,k,m-1)-4*gf(i+1,j,k,m-1)+gf(i+2,j,k,m-1)) &
                           & /(2*dx(i)+1d-40)
                   else
                      varx=(3*gf(i,j,k,m-1)-4*gf(i-1,j,k,m-1)+gf(i-2,j,k,m-1)) &
                           & /(2*dx(i)+1d-40)
                   end if
                   !------------------------------------------------------------------------
                   ! Y-direction
                   !------------------------------------------------------------------------
                   if (sgn*normaly(i,j,k)<0) then
                      vary=-(3*gf(i,j,k,m-1)-4*gf(i,j+1,k,m-1)+gf(i,j+2,k,m-1)) &
                           & /(2*dy(j)+1d-40)
                   else
                      vary=(3*gf(i,j,k,m-1)-4*gf(i,j-1,k,m-1)+gf(i,j-2,k,m-1)) &
                           & /(2*dy(j)+1d-40)
                   end if
                   !------------------------------------------------------------------------
                   ! Z-direction
                   !------------------------------------------------------------------------
                   if (dim==3) then
                      if (sgn*normalz(i,j,k)<0) then
                         varz=-(3*gf(i,j,k,m-1)-4*gf(i,j,k+1,m-1)+gf(i,j,k+2,m-1)) &
                              & /(2*dz(k)+1d-40)
                      else
                         varz=(3*gf(i,j,k,m-1)-4*gf(i,j,k-1,m-1)+gf(i,j,k-2,m-1)) &
                              & /(2*dz(k)+1d-40)
                      end if
                   else
                      varz=0
                   end if
                   !------------------------------------------------------------------------
                   ! g_m = grad(g_{m-1}).(-n)
                   !------------------------------------------------------------------------
                   gf(i,j,k,m)=(normalx(i,j,k)*varx+normaly(i,j,k)*vary)
                   if (dim==3) then
                      gf(i,j,k,m)=gf(i,j,k,m)+normalz(i,j,k)*varz
                   end if
                   gf(i,j,k,m)=sgn*gf(i,j,k,m)
                   !------------------------------------------------------------------------
                end if
             end do
          end do
       end do
       !write(*,*) m, maxval(abs(heav-1)*gf(:,:,:,m)),maxval(gf(:,:,:,m))
       !---------------------------------------------------------------------------------------
       if (nproc>1) then
          work1=gf(:,:,:,m)
          call comm_mpi_sca(work1)
          gf(:,:,:,m)=work1
       end if
       !---------------------------------------------------------------------------------------
    end do
    !---------------------------------------------------------------------------------------
    
    !---------------------------------------------------------------------------------------
    ! PDE resolution
    !---------------------------------------------------------------------------------------
    do m=order,1,-1
       dt=min(minval(dx(sx:ex)),minval(dy(sy:ey)))*d1p2/2
       res=1
       iter=0
       iter_max=10000
       work1=0       
       do while (res>1d-8.and.iter<iter_max)
          iter=iter+1
          !---------------------------------------------------------------------------------
          ! first order gradiant
          !---------------------------------------------------------------------------------
          do k=sz,ez
             do j=sy,ey
                do i=sx,ex
                   if (heav(i,j,k)>d1p2) then
                      !---------------------------------------------------------------------
                      ! X-direction
                      !---------------------------------------------------------------------
                      if (sgn*normalx(i,j,k)>0) then
                         gradx(i,j,k)=(gf(i,j,k,m)-gf(i-1,j,k,m))/(dx(i)+1d-40)
                      else
                         gradx(i,j,k)=(gf(i+1,j,k,m)-gf(i,j,k,m))/(dx(i)+1d-40)
                      end if
                      !---------------------------------------------------------------------
                      ! Y-direction
                      !---------------------------------------------------------------------
                      if (sgn*normaly(i,j,k)>0) then
                         grady(i,j,k)=(gf(i,j,k,m)-gf(i,j-1,k,m))/(dy(j)+1d-40)
                      else
                         grady(i,j,k)=(gf(i,j+1,k,m)-gf(i,j,k,m))/(dy(j)+1d-40)
                      end if
                      !---------------------------------------------------------------------
                      ! Z-direction
                      !---------------------------------------------------------------------
                      if (dim==3) then
                         if (sgn*normalz(i,j,k)>0) then
                            gradz(i,j,k)=(gf(i,j,k,m)-gf(i,j,k-1,m))/(dz(k)+1d-40)
                         else
                            gradz(i,j,k)=(gf(i,j,k+1,m)-gf(i,j,k,m))/(dz(k)+1d-40)
                         end if
                      end if
                      !---------------------------------------------------------------------
                   end if
                end do
             end do
          end do
          !---------------------------------------------------------------------------------
          !write(*,*) m, maxval(abs(gradx)), maxval(abs(grady)), maxval(gf(:,:,:,m+1))
          
          !---------------------------------------------------------------------------------
          ! Upwind scheme
          !---------------------------------------------------------------------------------
          work1=gf(:,:,:,m)
          do k=sz,ez
             do j=sy,ey
                do i=sx,ex
                   if (heav(i,j,k)>d1p2) then
                      gf(i,j,k,m)=gf(i,j,k,m)                    &
                           & -sgn*dt*normalx(i,j,k)*gradx(i,j,k) &
                           & -sgn*dt*normaly(i,j,k)*grady(i,j,k)
                      if (dim==3) then
                         gf(i,j,k,m)=gf(i,j,k,m) &
                              & -sgn*dt*normalz(i,j,k)*gradz(i,j,k)
                      end if
                      gf(i,j,k,m)=gf(i,j,k,m)+dt*gf(i,j,k,m+1)
                   end if
                end do
             end do
          end do
          !---------------------------------------------------------------------------------
          if (nproc>1) then
             work2=gf(:,:,:,m)
             call comm_mpi_sca(work2)
             gf(:,:,:,m)=work2
          end if
          !---------------------------------------------------------------------------------

          !---------------------------------------------------------------------------------
          ! Residual
          !---------------------------------------------------------------------------------
          res=0
          norm=0
          do k=sz,ez
             do j=sy,ey
                do i=sx,ex
                   if (heav(i,j,k)>d1p2) then
                      res=res+(work1(i,j,k)-gf(i,j,k,m))**2*dx(i)*dy(j)*dz(k)
                      norm=norm+work1(i,j,k)**2*dx(i)*dy(j)*dz(k)
                   end if
                end do
             end do
          end do
          !---------------------------------------------------------------------------------
          if (nproc>1) then
             call mpi_allreduce(res,tmp,1,MPI_DOUBLE_PRECISION,MPI_SUM, &
                  & COMM3D,CODE); res=tmp
             call mpi_allreduce(norm,tmp,1,MPI_DOUBLE_PRECISION,MPI_SUM, &
                  & COMM3D,CODE); norm=tmp
          end if
          !---------------------------------------------------------------------------------
          res=sqrt(res/norm)
          if (modulo(iter,10)==0) then
             if (rank==0) write(*,*) m,iter,res
          end if
       end do
       if (rank==0) write(*,*) m,iter,res
    end do
    !---------------------------------------------------------------------------------------
    ! extrapolated field
    !---------------------------------------------------------------------------------------
    sca=gf(:,:,:,1)
    !---------------------------------------------------------------------------------------

  end subroutine Interp_Aslam

end module mod_Aslam
