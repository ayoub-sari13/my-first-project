!===============================================================================
module Bib_VOFLag_ParticlesDataStructure_Impression_Initialization
  !===============================================================================
  contains
  !---------------------------------------------------------------------------  
  !> @author jorge cesar brandle de motta, amine chadil
  ! 
  !> @brief initialise les donnees d'impression de la structure vl et creer les 
  !! fichiers contenant ces donnees 
  !
  !> @param[in] vl   : la structure contenant toutes les informations
  !! relatives aux particules       
  !---------------------------------------------------------------------------  
  subroutine ParticlesDataStructure_Impression_Initialization(vl)
    !===============================================================================
    !modules
    !===============================================================================
    !-------------------------------------------------------------------------------
    !Modules de definition des structures et variables => src/mod/module_...f90
    !-------------------------------------------------------------------------------
    use Module_VOFLag_ParticlesDataStructure
    use mod_Parameters,                       only : rank,nproc,dim,deeptracking
    use mod_mpi
    !-------------------------------------------------------------------------------
    !Modules de subroutines de la librairie RESPECT => src/Bib_VOFLag_...f90
    !-------------------------------------------------------------------------------
    use Bib_VOFLag_ParticlesDataStructure_Impression_AddParticle
    use Bib_VOFLag_ParticlesDataStructure_Impression_FileCreation
    !-------------------------------------------------------------------------------

    !===============================================================================
    !declarations des variables
    !===============================================================================
    implicit none
    !-------------------------------------------------------------------------------
    !variables globales
    !-------------------------------------------------------------------------------
    type(struct_vof_lag), intent(inout)  :: vl
    !-------------------------------------------------------------------------------
    !variables locales 
    !-------------------------------------------------------------------------------
    integer                              :: np,ilen,na,nd
    external ilen
    double precision                     :: aff_priority,vals
    logical                              :: there
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'entree ParticlesDataStructure_Impression_Initialization'
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    !affichage de toutes les particules
    !-------------------------------------------------------------------------------
    vl%objet(1:vl%kpt)%aff=.true.

    !-------------------------------------------------------------------------------
    !repartition des particules pour l'impression sur les differents procs
    !-------------------------------------------------------------------------------
    vl%nbaffich=0
    if (nproc.eq.1) then
       do np=1,vl%kpt
          if (vl%objet(np)%aff) then
             call ParticlesDataStructure_Impression_AddParticle(vl%affich,np)
             vl%nbaffich=vl%nbaffich+1
          end if
       end do
    else
       do np=1,vl%kpt
          if (vl%objet(np)%aff) then
             aff_priority = vl%nbaffich+rank/(nproc*0.9d0+1D-40)
             vals = aff_priority
             if (nproc>1) call mpi_allreduce(vals,aff_priority,1,mpi_double_precision,mpi_min,comm3d,code)
             if (dabs(vl%nbaffich+rank/(nproc*0.9d0+1D-40)-aff_priority).le.(1.d-8/(nproc+1D-40))) then
                call ParticlesDataStructure_Impression_AddParticle(vl%affich,np)
                vl%nbaffich=vl%nbaffich+1
             end if
          end if
       end do
    end if

    !-------------------------------------------------------------------------------
    !allocation des tableaux contenants les noms des fichiers "data_part_****.dat"
    !-------------------------------------------------------------------------------
    if (vl%nbaffich .ge. 1) then
      if (allocated(vl%filec))  deallocate(vl%filec)
      allocate(vl%filec(1:vl%nbaffich))
    endif

    !-------------------------------------------------------------------------------
    !boucle sur les fichiers des particules a afficher
    !-------------------------------------------------------------------------------
    do na=1,vl%nbaffich
       np = vl%affich(na)
       write(vl%filec(na),'(a,i5.5,a)') 'data_part_',np,'.dat'
       call ParticlesDataStructure_Impression_FileCreation(vl,na)
    end do

    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'sortie ParticlesDataStructure_Impression_Initialization'
    !-------------------------------------------------------------------------------
  end subroutine ParticlesDataStructure_Impression_Initialization

  !===============================================================================
end module Bib_VOFLag_ParticlesDataStructure_Impression_Initialization
!===============================================================================
  


