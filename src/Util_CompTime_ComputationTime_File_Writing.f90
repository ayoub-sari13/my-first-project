!===============================================================================
module Bib_VOFLag_ComputationTime_File_Writing
  !===============================================================================
  contains
  !---------------------------------------------------------------------------  
  !> @author jorge cesar brandle de motta, amine chadil
  ! 
  !> @brief cette routine note les temps de calcul des differentes routine choisies 
  !! durant l'iteration courante
  !
  !> @param [in] iteration_temps   : le numero de l'iteration courante
  !---------------------------------------------------------------------------  
  subroutine ComputationTime_File_Writing(iteration_temps,vl)
    !===============================================================================
    !modules
    !===============================================================================
    !-------------------------------------------------------------------------------
    !Modules de definition des structures et variables => src/mod/module_...f90
    !-------------------------------------------------------------------------------
    use Module_VOFLag_ParticlesDataStructure
    use Module_VOFLag_ComputationTime,        only : ctime_iter,ctime_total,ctime_n,ctime_file
    use mod_Parameters,                       only : deeptracking,EN_activate,rank
    use mod_mpi               
    !-------------------------------------------------------------------------------
    !Modules de subroutines de la librairie RESPECT => src/Bib_VOFLag_...f90
    !-------------------------------------------------------------------------------
    use Bib_VOFLag_ComputationTime_File_Creation
    !-------------------------------------------------------------------------------
    !===============================================================================
    !declarations des variables
    !===============================================================================
    implicit none
    !-------------------------------------------------------------------------------
    !variables globales
    !-------------------------------------------------------------------------------
    type(struct_vof_lag), intent(inout) :: vl
    integer,              intent(in)    :: iteration_temps
    !-------------------------------------------------------------------------------
    !variables locales 
    !-------------------------------------------------------------------------------
    double precision, dimension(ctime_n)  :: total
    integer                               :: n
    logical                               :: there
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'entree ComputationTime_File_Writing'
    !-------------------------------------------------------------------------------
    
    ctime_total = ctime_total+ctime_iter
    total = ctime_total(1:ctime_n)

    do n=1,ctime_n
      call mpisum(ctime_iter(n))
      call mpisum(total(n))
    enddo

    if (rang .eq. 0) then
      inquire(file=ctime_file,exist=there)
      if (.not.there) call ComputationTime_File_Creation(ctime_file,vl)

      open(unit=46,file=ctime_file,status='old',position='append')

      write(46,'(1E13.5)',advance='no')  dfloat(iteration_temps)
      do n=1,3
        write(46,'(2E13.5)',advance='no') ctime_iter(n),total(n)
      end do
      if (is_energie) write(46,'(2E13.5)',advance='no') ctime_iter(4),total(4)
      if (vl%deplacement .or. vl%vrot) write(46,'(2E13.5)',advance='no') ctime_iter(5),total(5)
      if (vl%postforce) write(46,'(2E13.5)',advance='no') ctime_iter(6),total(6)
      if (vl%postHeatFlux) write(46,'(2E13.5)',advance='no') ctime_iter(7),total(7)

      close(46)
    end if

    ctime_iter = 0.0D0
    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'sortie ComputationTime_File_Writing'
    !-------------------------------------------------------------------------------
  end subroutine ComputationTime_File_Writing

  !===============================================================================
end module Bib_VOFLag_ComputationTime_File_Writing
!===============================================================================
  


