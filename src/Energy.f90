!========================================================================
!
!                   FUGU Version 1.0.0
!
!========================================================================
!
! Copyright  Stéphane Vincent
!
! stephane.vincent@u-pem.fr          straightparadigm@gmail.com
!
! This file is part of the FUGU Fortran library, a set of Computational 
! Fluid Dynamics subroutines devoted to the simulation of multi-scale
! multi-phase flows for resolved scale interfaces (liquid/gas/solid). 
! FUGU uses the initial developments of DyJeAT and SHALA codes from 
! Jean-Luc Estivalezes (jean-luc.estivalezes@onera.fr) and 
! Frédéric Couderc (couderc@math.univ-toulouse.fr).
!
!========================================================================
!**
!**   NAME       : Energy.f90
!**
!**   AUTHOR     : Stéphane Vincent
!**              : Benoit Trouette
!**
!**   FUNCTION   : Energy Equation modules of FUGU software
!**
!**   DATES      : Version 1.0.0  : from : feb, 20, 2018
!**
!========================================================================

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#include "precomp.h"
module mod_Heat
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
contains

  subroutine Energy_Matrix (mat,u,v,w,tp,condu,condv,condw,rho,cp,sltp,sptp,pentp,thetatp,penu,penv,penw, &
       & smc_tab,energy,EQ_linear_term,EQ_penalty_term,EQ_inertial_scheme)
    use mod_Parameters, only: dim 
    use mod_Constants, only: zero,d1p2,d1p8,d3p8,d6p8,d3p2
    use mod_struct_solver
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    logical, intent(in)                                    :: EQ_linear_term,EQ_penalty_term
    integer, intent(in)                                    :: EQ_inertial_scheme
    real(8), dimension(:,:,:), allocatable,  intent(in)    :: u,v,w
    real(8), dimension(:,:,:), allocatable,  intent(inout) :: tp
    real(8), dimension(:,:,:), allocatable,  intent(inout) :: smc_tab
    real(8), dimension(:,:,:), allocatable,  intent(in)    :: condu,condv,condw,rho,cp 
    real(8), dimension(:,:,:), allocatable,  intent(in)    :: sltp,thetatp
    real(8), dimension(:,:,:,:), allocatable,  intent(in)  :: sptp
    real(8), dimension(:,:,:,:), allocatable,  intent(in)  :: pentp,penu,penv,penw
    type(solver_sca_t), intent(inout)                      :: energy
    type(matrix_t), intent(inout)                          :: mat
    !-------------------------------------------------------------------------------

    if (dim==2) then
       call Energy_Matrix_2D(mat%coef,mat%jcof,mat%icof,u,v,tp,condu,condv,rho,cp, &
            & sltp,sptp,pentp,thetatp,penu,penv,smc_tab,mat%kdv,mat%nps,energy,    &
            & EQ_linear_term,EQ_penalty_term,EQ_inertial_scheme)
    else
       call Energy_Matrix_3D(mat%coef,mat%jcof,mat%icof,u,v,w,tp,condu,condv,condw,rho,cp, &
            & sltp,sptp,pentp,thetatp,penu,penv,penw,mat%kdv,mat%nps,energy,               &
            & EQ_linear_term,EQ_penalty_term,EQ_inertial_scheme)
    end if

  end subroutine Energy_Matrix
  !*****************************************************************************************************
  !*****************************************************************************************************
  !*****************************************************************************************************
  !*****************************************************************************************************
  subroutine Energy_Matrix_2D (coef,jcof,icof,u,v,tp,condu,condv,rho,cp,                    &
       & sltp,sptp,pentp,thetatp,penu,penv,smc_tab,nb_coef_matrix,nb_matrix_element,energy, &
       & EQ_linear_term,EQ_penalty_term,EQ_inertial_scheme)
    !*****************************************************************************************************
    !*****************************************************************************************************
    !*****************************************************************************************************
    !*****************************************************************************************************
    !-------------------------------------------------------------------------------
    ! Finite volume discretization of the Energy equation
    !-------------------------------------------------------------------------------
    use mod_Parameters, only: nproc,         &
         & dim,coef_time_1,dx,dy,dxu,dyv,    &
         & sx,ex,sy,ey,                      &
         & sxs,exs,sys,eys
    use mod_Constants, only: zero,d1p2,d1p8,d3p8,d6p8,d3p2
    use mod_struct_solver
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    logical, intent(in)                                    :: EQ_linear_term,EQ_penalty_term
    integer, intent(in)                                    :: EQ_inertial_scheme
    integer, intent(in)                                    :: nb_coef_matrix,nb_matrix_element
    integer, dimension(:), intent(in), allocatable         :: jcof,icof
    real(8), dimension(:), intent(inout), allocatable      :: coef
    real(8), dimension(:,:,:), allocatable,  intent(in)    :: u,v
    real(8), dimension(:,:,:), allocatable,  intent(inout) :: tp
    real(8), dimension(:,:,:), allocatable,  intent(inout) :: smc_tab
    real(8), dimension(:,:,:), allocatable,  intent(in)    :: condu,condv,rho,cp 
    real(8), dimension(:,:,:), allocatable,  intent(in)    :: sltp,thetatp
    real(8), dimension(:,:,:,:), allocatable,  intent(in)  :: sptp
    real(8), dimension(:,:,:,:), allocatable,  intent(in)  :: pentp,penu,penv
    type(solver_sca_t), intent(inout)                      :: energy
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    real(8)                                                :: difb,diff,inerb,inerf,inerbb,inerff
    real(8)                                                :: rbm,rbp,rfm,rfp,alphab,alphaf
    real(8)                                                :: rocp
    real(8)                                                :: dx1,dx2
    integer                                                :: i,j,lvs
    integer                                                :: code
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Matrix initialization
    !-------------------------------------------------------------------------------
    do lvs = 1,nb_matrix_element
       coef(lvs) = 0
    end do
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! vector B initialization
    !-------------------------------------------------------------------------------
    if (EQ_inertial_scheme>10) then
       smc_tab=0
    endif
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Finite volume discretization
    !-------------------------------------------------------------------------------
    lvs=1
    do j=sys,eys
       do i=sxs,exs

          !if (nproc>1.and.(i==sxs.or.i==exs.or.j==sys.or.j==eys)) then
          if (nproc>1.and.(i<sx.or.i>ex.or.j<sy.or.j>ey)) then
             !-------------------------------------------------------------------------------
             ! Penalty term for MPI exchange cells
             !-------------------------------------------------------------------------------
             coef(lvs)=1!;coef(lvs+1:lvs+4)=0
          else           
             !-------------------------------------------------------------------------------
             ! Time integration scheme
             !-------------------------------------------------------------------------------
             coef(lvs) = rho(i,j,1) * cp (i,j,1) * coef_time_1 * dx(i) * dy(j)
             !-------------------------------------------------------------------------------
             ! Conductivity
             !-------------------------------------------------------------------------------
             difb= - condu(i  ,j,1)/dxu(i  ) * dy(j) ! backward contribution
             diff=   condu(i+1,j,1)/dxu(i+1) * dy(j) ! forward contribution
             coef(lvs  )= coef(lvs)   - (difb  - diff)
             coef(lvs+1)= coef(lvs+1) + difb
             coef(lvs+2)= coef(lvs+2) - diff
             difb= - condv(i,j  ,1)/dyv(j  ) * dx(i) ! backward contribution
             diff=   condv(i,j+1,1)/dyv(j+1) * dx(i) ! forward contribution
             coef(lvs  )= coef(lvs)   - (difb  - diff)
             coef(lvs+3)= coef(lvs+3) + difb
             coef(lvs+4)= coef(lvs+4) - diff
             !-------------------------------------------------------------------------------
             ! Inertial terms
             !-------------------------------------------------------------------------------
             select case(EQ_inertial_scheme)
             case(-100:0)
                ! Lagrangian
                ! Lax Wendroff TVD
                ! Weno Conservative
                ! Weno Non Conservative
             case(1)
                !-------------------------------------------------------------------------------
                ! centered scheme
                !-------------------------------------------------------------------------------
                inerb= - rho(i,j,1) * cp(i,j,1) * u(i  ,j,1) * d1p2 * dy(j) ! backward contribution
                inerf=   rho(i,j,1) * cp(i,j,1) * u(i+1,j,1) * d1p2 * dy(j) ! forward contribution
                coef(lvs  )= coef(lvs  ) + inerb + inerf
                coef(lvs+1)= coef(lvs+1) + inerb
                coef(lvs+2)= coef(lvs+2) + inerf
                inerb= - rho(i,j,1) * cp(i,j,1) * v(i,j  ,1) * d1p2 * dx(i) ! backward contribution
                inerf=   rho(i,j,1) * cp(i,j,1) * v(i,j+1,1) * d1p2 * dx(i) ! forward contribution
                coef(lvs  )= coef(lvs  ) + inerb + inerf
                coef(lvs+3)= coef(lvs+3) + inerb
                coef(lvs+4)= coef(lvs+4) + inerf
                !-------------------------------------------------------------------------------
             case(2)
                !-------------------------------------------------------------------------------
                ! upwind
                !-------------------------------------------------------------------------------
                inerb= - rho(i,j,1)* cp(i,j,1)*u(i  ,j,1) * dy(j) ! backward contribution
                inerf=   rho(i,j,1)* cp(i,j,1)*u(i+1,j,1) * dy(j) ! forward contribution
                coef(lvs  )= coef(lvs  ) + (max( inerb,zero)+max(inerf,zero))
                coef(lvs+1)= coef(lvs+1) -  max(-inerb,zero)
                coef(lvs+2)= coef(lvs+2) -  max(-inerf,zero)
                inerb= - rho(i,j,1)* cp(i,j,1)*v(i,j  ,1) * dx(i) ! backward contribution
                inerf=   rho(i,j,1)* cp(i,j,1)*v(i,j+1,1) * dx(i) ! forward contribution
                coef(lvs  )= coef(lvs  ) + (max( inerb,zero)+max(inerf,zero))
                coef(lvs+3)= coef(lvs+3) -  max(-inerb,zero)
                coef(lvs+4)= coef(lvs+4) -  max(-inerf,zero)
                !-------------------------------------------------------------------------------
             case(3)
                !-------------------------------------------------------------------------------
                ! Hybrid scheme
                !-------------------------------------------------------------------------------
                inerb= - rho(i,j,1) * cp(i,j,1) * u(i  ,j,1) * d1p2 * dy(j) ! backward contribution
                inerf=   rho(i,j,1) * cp(i,j,1) * u(i+1,j,1) * d1p2 * dy(j) ! forward contribution
                coef(lvs  )= coef(lvs  ) + thetatp(i,j,1)*(inerb + inerf)
                coef(lvs+1)= coef(lvs+1) + thetatp(i,j,1)* inerb
                coef(lvs+2)= coef(lvs+2) + thetatp(i,j,1)* inerf
                inerb= - rho(i,j,1) * cp(i,j,1) * v(i,j  ,1) * d1p2 * dx(i) ! backward contribution
                inerf=   rho(i,j,1) * cp(i,j,1) * v(i,j+1,1) * d1p2 * dx(i) ! forward contribution
                coef(lvs  )= coef(lvs  ) + thetatp(i,j,1)*(inerb + inerf)
                coef(lvs+3)= coef(lvs+3) + thetatp(i,j,1)* inerb
                coef(lvs+4)= coef(lvs+4) + thetatp(i,j,1)* inerf

                inerb= - rho(i,j,1)* cp(i,j,1)*u(i  ,j,1) * dy(j) ! backward contribution
                inerf=   rho(i,j,1)* cp(i,j,1)*u(i+1,j,1) * dy(j) ! forward contribution
                coef(lvs  )= coef(lvs  ) + (1-thetatp(i,j,1))*(max( inerb,zero)+max(inerf,zero))
                coef(lvs+1)= coef(lvs+1) - (1-thetatp(i,j,1))* max(-inerb,zero)
                coef(lvs+2)= coef(lvs+2) - (1-thetatp(i,j,1))* max(-inerf,zero)
                inerb= - rho(i,j,1)* cp(i,j,1)*v(i,j  ,1) * dx(i) ! backward contribution
                inerf=   rho(i,j,1)* cp(i,j,1)*v(i,j+1,1) * dx(i) ! forward contribution
                coef(lvs  )= coef(lvs  ) + (1-thetatp(i,j,1))*(max( inerb,zero)+max(inerf,zero))
                coef(lvs+3)= coef(lvs+3) - (1-thetatp(i,j,1))* max(-inerb,zero)
                coef(lvs+4)= coef(lvs+4) - (1-thetatp(i,j,1))* max(-inerf,zero)
                !-------------------------------------------------------------------------------
             case(4)
                !-------------------------------------------------------------------------------
                ! 2nd order upwind scheme
                !-------------------------------------------------------------------------------
                rocp=rho(i,j,1)*cp(i,j,1)
                !-------------------------------------------------------------------------------
                inerb = - rocp * (max( (1+d1p2*dxu(i)/dxu(i-1))*u(i  ,j,1),zero) &
                     & +max( d1p2*dxu(i+1)/dxu(i)*u(i+1,j,1),zero)) * dy(j) 
                inerbb= + rocp *  max( d1p2*dxu(i)/dxu(i-1)*u(i  ,j,1),zero) * dy(j)
                inerf = - rocp * (max(-(1+d1p2*dxu(i+1)/dxu(i+2))*u(i+1,j,1),zero) &
                     & +max(-d1p2*dxu(i)/dxu(i+1)*u(i  ,j,1),zero)) * dy(j)
                inerff= + rocp *  max(-d1p2*dxu(i+1)/dxu(i+2)*u(i+1,j,1),zero) * dy(j)
                coef(lvs  )= coef(lvs  ) -(inerb + inerf + inerbb + inerff)
                coef(lvs+1)= coef(lvs+1) + inerb
                coef(lvs+2)= coef(lvs+2) + inerf
                coef(lvs+5)= coef(lvs+5) + inerbb
                coef(lvs+6)= coef(lvs+6) + inerff
                !-------------------------------------------------------------------------------
!!$                inerb = - rocp * (max( d3p2*v(i,j  ,1),zero)+max( d1p2*v(i,j+1,1),zero)) * dx(i) 
!!$                inerbb= + rocp *  max( d1p2*v(i,j  ,1),zero)                             * dx(i)
!!$                inerf = - rocp * (max(-d3p2*v(i,j+1,1),zero)+max(-d1p2*v(i,j  ,1),zero)) * dx(i)
!!$                inerff= + rocp *  max(-d1p2*v(i,j+1,1),zero)                             * dx(i)
                inerb = - rocp * (max( (1+d1p2*dyv(j)/dyv(j-1))*v(i,j  ,1),zero) &
                     & +max( d1p2*dyv(j+1)/dyv(j)*v(i,j+1,1),zero)) * dx(i) 
                inerbb= + rocp *  max( d1p2*dyv(j)/dyv(j-1)*v(i,j  ,1),zero)* dx(i)
                inerf = - rocp * (max(-(1+d1p2*dyv(j+1)/dyv(j+2))*v(i,j+1,1),zero) &
                     & +max(-d1p2*dyv(j)/dyv(j+1)*v(i,j  ,1),zero)) * dx(i)
                inerff= + rocp *  max(-d1p2*dyv(j+1)/dyv(j+2)*v(i,j+1,1),zero) * dx(i)
                coef(lvs  )= coef(lvs  ) -(inerb + inerf + inerbb + inerff)
                coef(lvs+3)= coef(lvs+3) + inerb
                coef(lvs+4)= coef(lvs+4) + inerf
                coef(lvs+7)= coef(lvs+7) + inerbb
                coef(lvs+8)= coef(lvs+8) + inerff
             case(5)
                !-------------------------------------------------------------------------------
                ! QUICK scheme
                !-------------------------------------------------------------------------------
                rocp=rho(i,j,1)*cp(i,j,1)
                !-------------------------------------------------------------------------------
                alphab=0
                alphaf=0
                if (u(i  ,j,1)>0) alphab=1
                if (u(i+1,j,1)>0) alphaf=1
!!$                inerb =   rocp * u(i  ,j,1) * d6p8 * dy(j) * alphab     &
!!$                     &  + rocp * u(i+1,j,1) * d1p8 * dy(j) * alphaf     &
!!$                     &  + rocp * u(i  ,j,1) * d3p8 * dy(j) * (1-alphab)
!!$                inerbb= - rocp * u(i  ,j,1) * d1p8 * dy(j) * alphab
!!$                inerf = - rocp * u(i+1,j,1) * d3p8 * dy(j) * alphaf     &
!!$                     &  - rocp * u(i+1,j,1) * d6p8 * dy(j) * (1-alphaf) &
!!$                     &  - rocp * u(i  ,j,1) * d1p8 * dy(j) * (1-alphab)
!!$                inerff= + rocp * u(i+1,j,1) * d1p8 * dy(j) * (1-alphaf)
                inerb =   rocp * u(i  ,j,1) * d1p2*(d1p2*dxu(i)+dxu(i-1))/((d1p2*dxu(i)+dxu(i-1))-(d1p2*dxu(i))) * dy(j) * alphab           &
                     &  + rocp * u(i+1,j,1) * (d1p2*dxu(i+1))**2/((d1p2*dxu(i+1)+dxu(i))**2-(d1p2*dxu(i+1))**2) * dy(j) * alphaf            &
                     &  + rocp * u(i  ,j,1) * (d1p2*dxu(i))*(d1p2*dxu(i)+dxu(i+1))/2/((d1p2*dxu(i))**2+(d1p2*dxu(i))*(d1p2*dxu(i)+dxu(i+1))) * dy(j) * (1-alphab)
                inerbb= - rocp * u(i  ,j,1) * (d1p2*dxu(i))**2/((d1p2*dxu(i)+dxu(i-1))**2-(d1p2*dxu(i))**2) * dy(j) * alphab
                inerf = - rocp * u(i+1,j,1) * d1p2*(d1p2*dxu(i+1)+dxu(i))/((d1p2*dxu(i+1))+(d1p2*dxu(i+1)+dxu(i))) * dy(j) * alphaf         &
                     &  - rocp * u(i+1,j,1) * d1p2*(d1p2*dxu(i+1)+dxu(i+2))/((d1p2*dxu(i+1)+dxu(i+2))-(d1p2*dxu(i+1))) * dy(j) * (1-alphaf) &
                     &  - rocp * u(i  ,j,1) * (d1p2*dxu(i))**2/((d1p2*dxu(i))+(d1p2*dxu(i)+dxu(i+1)))/((d1p2*dxu(i)+dxu(i+1))-(d1p2*dxu(i))) * dy(j) * (1-alphab)
                inerff= + rocp * u(i+1,j,1) * (d1p2*dxu(i+1))**2/((d1p2*dxu(i+1))+(d1p2*dxu(i+1)+dxu(i+2)))/((d1p2*dxu(i+1)+dxu(i+2))-(d1p2*dxu(i+1))) * dy(j) * (1-alphaf)
                coef(lvs  )= coef(lvs  ) + inerb + inerf + inerbb + inerff
                coef(lvs+1)= coef(lvs+1) - inerb
                coef(lvs+2)= coef(lvs+2) - inerf
                coef(lvs+5)= coef(lvs+5) - inerbb
                coef(lvs+6)= coef(lvs+6) - inerff
                !-------------------------------------------------------------------------------
                alphab=0
                alphaf=0
                if (v(i,j  ,1)>0) alphab=1
                if (v(i,j+1,1)>0) alphaf=1
                inerb =   rocp * v(i,j  ,1) * d1p2*(d1p2*dyv(j)+dyv(j-1))/((d1p2*dyv(j)+dyv(j-1))-(d1p2*dyv(j))) * dx(i) * alphab           &
                     &  + rocp * v(i,j+1,1) * (d1p2*dyv(j+1))**2/((d1p2*dyv(j+1)+dyv(j))**2-(d1p2*dyv(j+1))**2) * dx(i) * alphaf            &
                     &  + rocp * v(i,j  ,1) * (d1p2*dyv(j))*(d1p2*dyv(j)+dyv(j+1))/2/((d1p2*dyv(j))**2+(d1p2*dyv(j))*(d1p2*dyv(j)+dyv(j+1))) * dx(i) * (1-alphab)
                inerbb= - rocp * v(i,j  ,1) * (d1p2*dyv(j))**2/((d1p2*dyv(j)+dyv(j-1))**2-(d1p2*dyv(j))**2) * dx(i) * alphab
                inerf = - rocp * v(i,j+1,1) * d1p2*(d1p2*dyv(j+1)+dyv(j))/((d1p2*dyv(j+1))+(d1p2*dyv(j+1)+dyv(j))) * dx(i) * alphaf         &
                     &  - rocp * v(i,j+1,1) * d1p2*(d1p2*dyv(j+1)+dyv(j+2))/((d1p2*dyv(j+1)+dyv(j+2))-(d1p2*dyv(j+1))) * dx(i) * (1-alphaf) &
                     &  - rocp * v(i,j  ,1) * (d1p2*dyv(j))**2/((d1p2*dyv(j))+(d1p2*dyv(j)+dyv(j+1)))/((d1p2*dyv(j)+dyv(j+1))-(d1p2*dyv(j))) * dx(i) * (1-alphab)
                inerff= + rocp * v(i,j+1,1) * (d1p2*dyv(j+1))**2/((d1p2*dyv(j+1))+(d1p2*dyv(j+1)+dyv(j+2)))/((d1p2*dyv(j+1)+dyv(j+2))-(d1p2*dyv(j+1))) * dx(i) * (1-alphaf)
                coef(lvs  )= coef(lvs  ) + inerb + inerf + inerbb + inerff
                coef(lvs+3)= coef(lvs+3) - inerb
                coef(lvs+4)= coef(lvs+4) - inerf
                coef(lvs+7)= coef(lvs+7) - inerbb
                coef(lvs+8)= coef(lvs+8) - inerff
             case(11)
                !-------------------------------------------------------------------------------
                ! explicit centered scheme
                !-------------------------------------------------------------------------------
                inerb= - rho(i,j,1) * cp(i,j,1) * u(i  ,j,1) * d1p2 * dy(j) ! backward contribution
                inerf=   rho(i,j,1) * cp(i,j,1) * u(i+1,j,1) * d1p2 * dy(j) ! forward contribution
                smc_tab(i,j,1) = smc_tab(i,j,1) - (inerb*(tp(i,j,1)+tp(i-1,j,1))+inerf*(tp(i,j,1)+tp(i+1,j,1)))
                inerb= - rho(i,j,1) * cp(i,j,1) * v(i,j  ,1) * d1p2 * dx(i) ! backward contribution
                inerf=   rho(i,j,1) * cp(i,j,1) * v(i,j+1,1) * d1p2 * dx(i) ! forward contribution
                smc_tab(i,j,1) = smc_tab(i,j,1) - (inerb*(tp(i,j,1)+tp(i,j-1,1))+inerf*(tp(i,j,1)+tp(i,j+1,1)))
                !-------------------------------------------------------------------------------
             case(12)
                !-------------------------------------------------------------------------------
                ! explicit upwind scheme
                !-------------------------------------------------------------------------------
                inerb= - rho(i,j,1) * cp(i,j,1) * u(i  ,j,1) * dy(j) ! backward contribution
                inerf=   rho(i,j,1) * cp(i,j,1) * u(i+1,j,1) * dy(j) ! forward contribution
                smc_tab(i,j,1)=smc_tab(i,j,1)-(min(inerb,zero)*tp(i-1,j,1)+max(inerb,zero)*tp(i  ,j,1))
                smc_tab(i,j,1)=smc_tab(i,j,1)-(max(inerf,zero)*tp(i  ,j,1)+min(inerf,zero)*tp(i+1,j,1))
                inerb= - rho(i,j,1) * cp(i,j,1) * v(i,j  ,1) * dx(i) ! backward contribution
                inerf=   rho(i,j,1) * cp(i,j,1) * v(i,j+1,1) * dx(i) ! forward contribution
                smc_tab(i,j,1)=smc_tab(i,j,1)-(min(inerb,zero)*tp(i,j-1,1)+max(inerb,zero)*tp(i,j  ,1))
                smc_tab(i,j,1)=smc_tab(i,j,1)-(max(inerf,zero)*tp(i,j  ,1)+min(inerf,zero)*tp(i,j+1,1))
                !-------------------------------------------------------------------------------
             case(101:200)
                !-------------------------------------------------------------------------------
                ! TVD like schemes --> code fixes the flux limiter
                !-------------------------------------------------------------------------------
                code=EQ_inertial_scheme
                !-------------------------------------------------------------------------------
                ! implicit part --> upwind scheme
                !-------------------------------------------------------------------------------
                inerb= - rho(i,j,1)* cp(i,j,1)*u(i  ,j,1) * dy(j) ! backward contribution
                inerf=   rho(i,j,1)* cp(i,j,1)*u(i+1,j,1) * dy(j) ! forward contribution
                coef(lvs  )= coef(lvs  ) + (max( inerb,zero)+max(inerf,zero))
                coef(lvs+1)= coef(lvs+1) -  max(-inerb,zero)
                coef(lvs+2)= coef(lvs+2) -  max(-inerf,zero)
                !-------------------------------------------------------------------------------
                ! explicit part
                !-------------------------------------------------------------------------------
                rbm=((tp(i+1,j,1)-tp(i  ,j,1))/dxu(i+1))/((tp(i  ,j,1)-tp(i-1,j,1))/dxu(i  )+1d-40)
                rbp=((tp(i-1,j,1)-tp(i-2,j,1))/dxu(i-1))/((tp(i  ,j,1)-tp(i-1,j,1))/dxu(  i)+1d-40)
                rfm=((tp(i+2,j,1)-tp(i+1,j,1))/dxu(i+2))/((tp(i+1,j,1)-tp(i  ,j,1))/dxu(i+1)+1d-40)
                rfp=((tp(i  ,j,1)-tp(i-1,j,1))/dxu(i  ))/((tp(i+1,j,1)-tp(i  ,j,1))/dxu(i+1)+1d-40)
                alphab=max(sign(-inerb,1d0),zero)
                alphaf=max(sign(+inerf,1d0),zero)
                smc_tab(i,j,1)=smc_tab(i,j,1) &
                     & + d1p2*inerb*((1-alphab)*psi(rbm,code)-alphab*psi(rbp,code))*(tp(i  ,j,1)-tp(i-1,j,1)) &
                     & + d1p2*inerf*((1-alphaf)*psi(rfm,code)-alphaf*psi(rfp,code))*(tp(i+1,j,1)-tp(i  ,j,1))
                !-------------------------------------------------------------------------------
                ! implicit part --> upwind scheme
                !-------------------------------------------------------------------------------
                inerb= - rho(i,j,1)* cp(i,j,1)*v(i,j  ,1) * dx(i) ! backward contribution
                inerf=   rho(i,j,1)* cp(i,j,1)*v(i,j+1,1) * dx(i) ! forward contribution
                coef(lvs  )= coef(lvs  ) + (max( inerb,zero)+max(inerf,zero))
                coef(lvs+3)= coef(lvs+3) -  max(-inerb,zero)
                coef(lvs+4)= coef(lvs+4) -  max(-inerf,zero)
                !-------------------------------------------------------------------------------
                ! explicit part
                !-------------------------------------------------------------------------------
                rbm=((tp(i,j+1,1)-tp(i,j  ,1))/dyv(j+1))/((tp(i,j  ,1)-tp(i,j-1,1))/dyv(j  )+1d-40)
                rbp=((tp(i,j-1,1)-tp(i,j-2,1))/dyv(j-1))/((tp(i,j  ,1)-tp(i,j-1,1))/dyv(j  )+1d-40)
                rfm=((tp(i,j+2,1)-tp(i,j+1,1))/dyv(j+2))/((tp(i,j+1,1)-tp(i,j  ,1))/dyv(j+1)+1d-40)
                rfp=((tp(i,j  ,1)-tp(i,j-1,1))/dyv(j  ))/((tp(i,j+1,1)-tp(i,j  ,1))/dyv(j+1)+1d-40)
                alphab=max(sign(-inerb,1d0),zero)
                alphaf=max(sign(+inerf,1d0),zero)
                smc_tab(i,j,1)=smc_tab(i,j,1) &
                     & + d1p2*inerb*((1-alphab)*psi(rbm,code)-alphab*psi(rbp,code))*(tp(i,j  ,1)-tp(i,j-1,1)) &
                     & + d1p2*inerf*((1-alphaf)*psi(rfm,code)-alphaf*psi(rfp,code))*(tp(i,j+1,1)-tp(i,j  ,1))
                !-------------------------------------------------------------------------------
             case DEFAULT
                write(*,*) 'Energy 2D inertial scheme does not exist'
                stop
             end select
             !-------------------------------------------------------------------------------
             ! Linear term
             !-------------------------------------------------------------------------------
             if (EQ_linear_term) coef(lvs) = coef(lvs) + sltp(i,j,1) * dx(i) * dy(j)
             !-------------------------------------------------------------------------------
             ! Penalty term for boundary conditions
             ! pentp (i,j,k,l)
             ! l=1 central T component
             ! l=2 left T component
             ! l=3 right T component
             ! l=4 bottom T component
             ! l=5 top T component
             !-------------------------------------------------------------------------------
             coef(lvs)   = coef(lvs)   + pentp(i,j,1,1) * dx(i) * dy(j)
             coef(lvs+1) = coef(lvs+1) + pentp(i,j,1,2) * dx(i) * dy(j)
             coef(lvs+2) = coef(lvs+2) + pentp(i,j,1,3) * dx(i) * dy(j)
             coef(lvs+3) = coef(lvs+3) + pentp(i,j,1,4) * dx(i) * dy(j)
             coef(lvs+4) = coef(lvs+4) + pentp(i,j,1,5) * dx(i) * dy(j)
             !-------------------------------------------------------------------------------
             ! Penalty term for immersed objects
             !-------------------------------------------------------------------------------
             if (EQ_penalty_term) then 
                !-------------------------------------------------------------------------------
                ! sptp (i,j,k,l)
                ! l=1 central T component
                ! l=2 left T component
                ! l=3 right T component
                ! l=4 bottom T component
                ! l=5 top T component
                !-------------------------------------------------------------------------------
                coef(lvs)   = coef(lvs)   + sptp(i,j,1,1) * dx(i) * dy(j)
                coef(lvs+1) = coef(lvs+1) + sptp(i,j,1,2) * dx(i) * dy(j)
                coef(lvs+2) = coef(lvs+2) + sptp(i,j,1,3) * dx(i) * dy(j)
                coef(lvs+3) = coef(lvs+3) + sptp(i,j,1,4) * dx(i) * dy(j)
                coef(lvs+4) = coef(lvs+4) + sptp(i,j,1,5) * dx(i) * dy(j)
                !-------------------------------------------------------------------------------
             end if
             !-------------------------------------------------------------------------------
             ! End of discretization for T
             !-------------------------------------------------------------------------------
          end if

          !-------------------------------------------------------------------------------
          ! next line of the matrix
          !-------------------------------------------------------------------------------
          lvs=lvs+nb_coef_matrix 
       enddo
    enddo
    return
  end subroutine  Energy_Matrix_2D
  !*****************************************************************************************************
  !*****************************************************************************************************
  !*****************************************************************************************************
  !*****************************************************************************************************


  function psi(r,code)
    use mod_Constants, only: zero
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in) :: code
    real(8), intent(in) :: r
    real(8)             :: psi
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    real(8)             :: beta
    !-------------------------------------------------------------------------------

    select case(code)
    case(101) ! TVD centered
       psi=1
    case(102) ! TVD 1st order upwind
       psi=0
    case(103) ! TVD 2nd order upwind
       psi=r
    case(104) ! TVD QUICK (non Symmetric, i.e. psi/r \ne psi(1/r)
       psi=max(zero,min(2*r,(3+r)/4,2d0))
    case(105) ! TVD Van-Leer
       psi=(r+abs(r))/(1+r+1d-40)
    case(106) ! TVD Van-Albada
       psi=(r+r**2)/(1+r**2)
    case(107) ! TVD Min-Mod
       psi=0
       if (r>0) psi=min(r,1d0)
    case(108) ! TVD SUPERBEE
       psi=max(zero,min(2*r,1d0),min(r,2d0))
    case(109) ! TVD Sweby
       beta=1.5d0
       psi=max(zero,min(beta*r,1d0),min(r,beta))
    case(110) ! TVD UMIST
       psi=max(zero,min(2*r,(1+3*r)/4,(3+r)/4,2d0))
    case DEFAULT
       write(*,*) "TVD scheme unknown, STOP"
       stop
    end select


  end function psi

  !*****************************************************************************************************
  !*****************************************************************************************************
  !*****************************************************************************************************
  !*****************************************************************************************************
  subroutine Energy_Matrix_3D (coef,jcof,icof,u,v,w,tp,condu,condv,condw,rho,cp,         &
       & sltp,sptp,pentp,thetatp,penu,penv,penw,nb_coef_matrix,nb_matrix_element,energy, &
       & EQ_linear_term,EQ_penalty_term,EQ_inertial_scheme)
    !*****************************************************************************************************
    !*****************************************************************************************************
    !*****************************************************************************************************
    !*****************************************************************************************************
    !-------------------------------------------------------------------------------
    ! Finite volume discretization of the Energy equation
    !-------------------------------------------------------------------------------
    use mod_Parameters, only: nproc,         &
         & dim,coef_time_1,                  &
         & dx,dy,dz,dxu,dyv,dzw,             &
         & sx,ex,sy,ey,sz,ez,                &
         & sxs,exs,sys,eys,szs,ezs
    use mod_Constants, only: zero,d1p2
    use mod_struct_solver
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    logical, intent(in)                                    :: EQ_linear_term,EQ_penalty_term
    integer, intent(in)                                    :: EQ_inertial_scheme
    integer, intent(in)                                    :: nb_coef_matrix,nb_matrix_element
    integer, dimension(:), intent(in), allocatable         :: jcof,icof
    real(8), dimension(:), intent(inout), allocatable      :: coef
    real(8), dimension(:,:,:), allocatable,  intent(in)    :: u,v,w
    real(8), dimension(:,:,:), allocatable,  intent(inout) :: tp
    real(8), dimension(:,:,:), allocatable,  intent(in)    :: condu,condv,condw,rho,cp 
    real(8), dimension(:,:,:), allocatable,  intent(in)    :: sltp,thetatp
    real(8), dimension(:,:,:,:), allocatable,  intent(in)  :: sptp
    real(8), dimension(:,:,:,:), allocatable,  intent(in)  :: pentp,penu,penv,penw
    type(solver_sca_t), intent(inout)                     :: energy
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    real(8)                                                :: difb,diff,inerb,inerf,inerbb,inerff
    real(8)                                                :: alphab,alphaf
    real(8)                                                :: rocp
    integer                                                :: i,j,k,lvs
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Matrix initialization
    !-------------------------------------------------------------------------------
    do lvs = 1,nb_matrix_element
       coef(lvs) = 0
    end do
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Finite volume discretization
    !-------------------------------------------------------------------------------
    lvs=1
    do k=szs,ezs
       do j=sys,eys
          do i=sxs,exs

             !if (nproc>1.and.(i==sxs.or.i==exs.or.j==sys.or.j==eys.or.k==szs.or.k==ezs)) then
             if (nproc>1.and.(i<sx.or.i>ex.or.j<sy.or.j>ey.or.k<sz.or.k>ez)) then
                !-------------------------------------------------------------------------------
                ! Penalty term for MPI exchange cells
                !-------------------------------------------------------------------------------
                coef(lvs)=1!;coef(lvs+1:lvs+6)=0
             else

                !-------------------------------------------------------------------------------
                ! Time integration scheme
                !-------------------------------------------------------------------------------
                coef(lvs) = rho(i,j,k) * cp(i,j,k) * coef_time_1 * dx(i) * dy(j) * dz(k)
                !-------------------------------------------------------------------------------
                ! Conductivity
                !-------------------------------------------------------------------------------
                difb= - condu(i  ,j,k)/dxu(i  ) * dy(j) * dz(k) ! backward contribution
                diff=   condu(i+1,j,k)/dxu(i+1) * dy(j) * dz(k) ! forward contribution
                coef(lvs  )= coef(lvs)   - (difb  - diff)
                coef(lvs+1)= coef(lvs+1) + difb
                coef(lvs+2)= coef(lvs+2) - diff
                difb= - condv(i,j  ,k)/dyv(j  ) * dx(i) * dz(k) ! backward contribution
                diff=   condv(i,j+1,k)/dyv(j+1) * dx(i) * dz(k) ! forward contribution
                coef(lvs  )= coef(lvs)   - (difb  - diff)
                coef(lvs+3)= coef(lvs+3) + difb
                coef(lvs+4)= coef(lvs+4) - diff
                difb= - condw(i,j,k  )/dzw(k  ) * dx(i) * dy(j) ! backward contribution
                diff=   condw(i,j,k+1)/dzw(k+1) * dx(i) * dy(j) ! forward contribution
                coef(lvs  )= coef(lvs)   - (difb  - diff)
                coef(lvs+5)= coef(lvs+5) + difb
                coef(lvs+6)= coef(lvs+6) - diff
                !-------------------------------------------------------------------------------
                ! Inertial terms
                !-------------------------------------------------------------------------------
                select case(EQ_inertial_scheme)
                case(-100:0)
                   ! Lagrangian
                   ! Lax Wendroff TVD
                   ! Weno Conservative
                   ! Weno Non Conservative
                case(1) ! centered scheme         
                   inerb= - rho(i,j,k) * cp(i,j,k) * u(i  ,j,k) * d1p2 * dy(j) * dz(k) ! backward contribution
                   inerf=   rho(i,j,k) * cp(i,j,k) * u(i+1,j,k) * d1p2 * dy(j) * dz(k) ! forward contribution
                   coef(lvs  )= coef(lvs  ) + inerb + inerf
                   coef(lvs+1)= coef(lvs+1) + inerb
                   coef(lvs+2)= coef(lvs+2) + inerf
                   inerb= - rho(i,j,k) * cp(i,j,k) * v(i,j  ,k) * d1p2 * dx(i) * dz(k) ! backward contribution
                   inerf=   rho(i,j,k) * cp(i,j,k) * v(i,j+1,k) * d1p2 * dx(i) * dz(k) ! forward contribution
                   coef(lvs  )= coef(lvs  ) + inerb + inerf
                   coef(lvs+3)= coef(lvs+3) + inerb
                   coef(lvs+4)= coef(lvs+4) + inerf             
                   inerb= - rho(i,j,k) * cp(i,j,k) * w(i,j,k  ) * d1p2 * dx(i) * dy(j) ! backward contribution
                   inerf=   rho(i,j,k) * cp(i,j,k) * w(i,j,k+1) * d1p2 * dx(i) * dy(j) ! forward contribution
                   coef(lvs  )= coef(lvs  ) + inerb + inerf
                   coef(lvs+5)= coef(lvs+5) + inerb
                   coef(lvs+6)= coef(lvs+6) + inerf
                case(2) !upwind
                   inerb= - rho(i,j,k) * cp(i,j,k) * u(i  ,j,k) * dy(j) * dz(k) ! backward contribution
                   inerf=   rho(i,j,k) * cp(i,j,k) * u(i+1,j,k) * dy(j) * dz(k) ! forward contribution
                   coef(lvs  )= coef(lvs  ) + (max( inerb,zero)+max(inerf,zero))
                   coef(lvs+1)= coef(lvs+1) -  max(-inerb,zero)
                   coef(lvs+2)= coef(lvs+2) -  max(-inerf,zero)
                   inerb= - rho(i,j,k) * cp(i,j,k) * v(i,j  ,k) * dx(i) * dz(k) ! backward contribution
                   inerf=   rho(i,j,k) * cp(i,j,k) * v(i,j+1,k) * dx(i) * dz(k) ! forward contribution
                   coef(lvs  )= coef(lvs  ) + (max( inerb,zero)+max(inerf,zero))
                   coef(lvs+3)= coef(lvs+3) -  max(-inerb,zero)
                   coef(lvs+4)= coef(lvs+4) -  max(-inerf,zero)
                   inerb= - rho(i,j,k) * cp(i,j,k) * w(i,j,k  ) * dx(i) * dy(j) ! backward contribution
                   inerf=   rho(i,j,k) * cp(i,j,k) * w(i,j,k+1) * dx(i) * dy(j) ! forward contribution
                   coef(lvs  )= coef(lvs  ) + (max( inerb,zero)+max(inerf,zero))
                   coef(lvs+5)= coef(lvs+5) -  max(-inerb,zero)
                   coef(lvs+6)= coef(lvs+6) -  max(-inerf,zero)
                case(3)
                   inerb= - rho(i,j,k) * cp(i,j,k) * u(i  ,j,k) * d1p2 * dy(j) * dz(k) ! backward contribution
                   inerf=   rho(i,j,k) * cp(i,j,k) * u(i+1,j,k) * d1p2 * dy(j) * dz(k) ! forward contribution
                   coef(lvs  )= coef(lvs  ) + thetatp(i,j,k)*(inerb + inerf)
                   coef(lvs+1)= coef(lvs+1) + thetatp(i,j,k)* inerb
                   coef(lvs+2)= coef(lvs+2) + thetatp(i,j,k)* inerf
                   inerb= - rho(i,j,k) * cp(i,j,k) * v(i,j  ,k) * d1p2 * dx(i) * dz(k) ! backward contribution
                   inerf=   rho(i,j,k) * cp(i,j,k) * v(i,j+1,k) * d1p2 * dx(i) * dz(k) ! forward contribution
                   coef(lvs  )= coef(lvs  ) + thetatp(i,j,k)*(inerb + inerf)
                   coef(lvs+3)= coef(lvs+3) + thetatp(i,j,k)* inerb
                   coef(lvs+4)= coef(lvs+4) + thetatp(i,j,k)* inerf             
                   inerb= - rho(i,j,k) * cp(i,j,k) * w(i,j,k  ) * d1p2 * dx(i) * dy(j) ! backward contribution
                   inerf=   rho(i,j,k) * cp(i,j,k) * w(i,j,k+1) * d1p2 * dx(i) * dy(j) ! forward contribution
                   coef(lvs  )= coef(lvs  ) + thetatp(i,j,k)*(inerb + inerf)
                   coef(lvs+5)= coef(lvs+5) + thetatp(i,j,k)* inerb
                   coef(lvs+6)= coef(lvs+6) + thetatp(i,j,k)* inerf

                   inerb= - rho(i,j,k) * cp(i,j,k) * u(i  ,j,k) * dy(j) * dz(k) ! backward contribution
                   inerf=   rho(i,j,k) * cp(i,j,k) * u(i+1,j,k) * dy(j) * dz(k) ! forward contribution
                   coef(lvs  )= coef(lvs  ) + (1-thetatp(i,j,k))*(max( inerb,zero)+max(inerf,zero))
                   coef(lvs+1)= coef(lvs+1) - (1-thetatp(i,j,k))* max(-inerb,zero)
                   coef(lvs+2)= coef(lvs+2) - (1-thetatp(i,j,k))* max(-inerf,zero)
                   inerb= - rho(i,j,k) * cp(i,j,k) * v(i,j  ,k) * dx(i) * dz(k) ! backward contribution
                   inerf=   rho(i,j,k) * cp(i,j,k) * v(i,j+1,k) * dx(i) * dz(k) ! forward contribution
                   coef(lvs  )= coef(lvs  ) + (1-thetatp(i,j,k))*(max( inerb,zero)+max(inerf,zero))
                   coef(lvs+3)= coef(lvs+3) - (1-thetatp(i,j,k))* max(-inerb,zero)
                   coef(lvs+4)= coef(lvs+4) - (1-thetatp(i,j,k))* max(-inerf,zero)
                   inerb= - rho(i,j,k) * cp(i,j,k) * w(i,j,k  ) * dx(i) * dy(j) ! backward contribution
                   inerf=   rho(i,j,k) * cp(i,j,k) * w(i,j,k+1) * dx(i) * dy(j) ! forward contribution
                   coef(lvs  )= coef(lvs  ) + (1-thetatp(i,j,k))*(max( inerb,zero)+max(inerf,zero))
                   coef(lvs+5)= coef(lvs+5) - (1-thetatp(i,j,k))* max(-inerb,zero)
                   coef(lvs+6)= coef(lvs+6) - (1-thetatp(i,j,k))* max(-inerf,zero)
                case(4)
                   !-------------------------------------------------------------------------------
                   ! 2nd order upwind scheme
                   !-------------------------------------------------------------------------------
                   rocp=rho(i,j,k)*cp(i,j,k)
                   !-------------------------------------------------------------------------------
                   inerb = - rocp * (max( (1+d1p2*dxu(i)/dxu(i-1))*u(i  ,j,k),zero) &
                        & +max( d1p2*dxu(i+1)/dxu(i)*u(i+1,j,k),zero)) * dy(j) * dz(k)
                   inerbb= + rocp *  max( d1p2*dxu(i)/dxu(i-1)*u(i  ,j,k),zero) * dy(j) * dz(k)
                   inerf = - rocp * (max(-(1+d1p2*dxu(i+1)/dxu(i+2))*u(i+1,j,k),zero) &
                        & +max(-d1p2*dxu(i)/dxu(i+1)*u(i  ,j,k),zero)) * dy(j) * dz(k)
                   inerff= + rocp *  max(-d1p2*dxu(i+1)/dxu(i+2)*u(i+1,j,k),zero) * dy(j) * dz(k)
                   coef(lvs  )= coef(lvs  ) -(inerb + inerf + inerbb + inerff)
                   coef(lvs+1)= coef(lvs+1) + inerb
                   coef(lvs+2)= coef(lvs+2) + inerf
                   coef(lvs+7)= coef(lvs+7) + inerbb
                   coef(lvs+8)= coef(lvs+8) + inerff
                   !-------------------------------------------------------------------------------
                   inerb = - rocp * (max( (1+d1p2*dyv(j)/dyv(j-1))*v(i,j  ,k),zero) &
                        & +max( d1p2*dyv(j+1)/dyv(j)*v(i,j+1,k),zero)) * dx(i) * dz(k) 
                   inerbb= + rocp *  max( d1p2*dyv(j)/dyv(j-1)*v(i,j  ,k),zero)* dx(i) * dz(k)
                   inerf = - rocp * (max(-(1+d1p2*dyv(j+1)/dyv(j+2))*v(i,j+1,k),zero) &
                        & +max(-d1p2*dyv(j)/dyv(j+1)*v(i,j  ,k),zero)) * dx(i) * dz(k)
                   inerff= + rocp *  max(-d1p2*dyv(j+1)/dyv(j+2)*v(i,j+1,k),zero) * dx(i) * dz(k)
                   coef(lvs  )= coef(lvs  ) -(inerb + inerf + inerbb + inerff)
                   coef(lvs+3)= coef(lvs+3) + inerb
                   coef(lvs+4)= coef(lvs+4) + inerf
                   coef(lvs+9)= coef(lvs+9) + inerbb
                   coef(lvs+10)= coef(lvs+10) + inerff
                   !-------------------------------------------------------------------------------
                   inerb = - rocp * (max( (1+d1p2*dzw(k)/dzw(k-1))*w(i,j,k  ),zero) &
                        & +max( d1p2*dzw(k+1)/dzw(k)*w(i,j,k+1),zero)) * dx(i) * dy(j) 
                   inerbb= + rocp *  max( d1p2*dzw(k)/dzw(k-1)*w(i,j,k  ),zero)* dx(i) * dy(j)
                   inerf = - rocp * (max(-(1+d1p2*dzw(k+1)/dzw(k+2))*w(i,j,k+1),zero) &
                        & +max(-d1p2*dzw(k)/dzw(k+1)*w(i,j,k  ),zero)) * dx(i) * dy(j)
                   inerff= + rocp *  max(-d1p2*dzw(k+1)/dzw(k+2)*w(i,j,k+1),zero) * dx(i) * dy(j)
                   coef(lvs  )= coef(lvs  ) -(inerb + inerf + inerbb + inerff)
                   coef(lvs+5)= coef(lvs+5) + inerb
                   coef(lvs+6)= coef(lvs+6) + inerf
                   coef(lvs+11)= coef(lvs+11) + inerbb
                   coef(lvs+12)= coef(lvs+12) + inerff
                case(5)
                   !-------------------------------------------------------------------------------
                   ! QUICK scheme
                   !-------------------------------------------------------------------------------
                   rocp=rho(i,j,k)*cp(i,j,k)
                   !-------------------------------------------------------------------------------
                   alphab=0
                   alphaf=0
                   if (u(i  ,j,k)>0) alphab=1
                   if (u(i+1,j,k)>0) alphaf=1
                   inerb =   rocp * u(i  ,j,k) * d1p2*(d1p2*dxu(i)+dxu(i-1))/((d1p2*dxu(i)+dxu(i-1))-(d1p2*dxu(i))) * dy(j) * dz(k) * alphab           &
                        &  + rocp * u(i+1,j,k) * (d1p2*dxu(i+1))**2/((d1p2*dxu(i+1)+dxu(i))**2-(d1p2*dxu(i+1))**2) * dy(j) * dz(k) * alphaf            &
                        &  + rocp * u(i  ,j,k) * (d1p2*dxu(i))*(d1p2*dxu(i)+dxu(i+1))/2/((d1p2*dxu(i))**2+(d1p2*dxu(i))*(d1p2*dxu(i)+dxu(i+1))) * dy(j) * dz(k) * (1-alphab)
                   inerbb= - rocp * u(i  ,j,k) * (d1p2*dxu(i))**2/((d1p2*dxu(i)+dxu(i-1))**2-(d1p2*dxu(i))**2) * dy(j) * dz(k) * alphab
                   inerf = - rocp * u(i+1,j,k) * d1p2*(d1p2*dxu(i+1)+dxu(i))/((d1p2*dxu(i+1))+(d1p2*dxu(i+1)+dxu(i))) * dy(j) * dz(k) * alphaf         &
                        &  - rocp * u(i+1,j,k) * d1p2*(d1p2*dxu(i+1)+dxu(i+2))/((d1p2*dxu(i+1)+dxu(i+2))-(d1p2*dxu(i+1))) * dy(j) * dz(k) * (1-alphaf) &
                        &  - rocp * u(i  ,j,k) * (d1p2*dxu(i))**2/((d1p2*dxu(i))+(d1p2*dxu(i)+dxu(i+1)))/((d1p2*dxu(i)+dxu(i+1))-(d1p2*dxu(i))) * dy(j) * dz(k) * (1-alphab)
                   inerff= + rocp * u(i+1,j,k) * (d1p2*dxu(i+1))**2/((d1p2*dxu(i+1))+(d1p2*dxu(i+1)+dxu(i+2)))/((d1p2*dxu(i+1)+dxu(i+2))-(d1p2*dxu(i+1))) * dy(j) * dz(k) * (1-alphaf)
                   coef(lvs  )= coef(lvs  ) + inerb + inerf + inerbb + inerff
                   coef(lvs+1)= coef(lvs+1) - inerb
                   coef(lvs+2)= coef(lvs+2) - inerf
                   coef(lvs+7)= coef(lvs+7) - inerbb
                   coef(lvs+8)= coef(lvs+8) - inerff
                   !-------------------------------------------------------------------------------
                   alphab=0
                   alphaf=0
                   if (v(i,j  ,k)>0) alphab=1
                   if (v(i,j+1,k)>0) alphaf=1
                   inerb =   rocp * v(i,j  ,k) * d1p2*(d1p2*dyv(j)+dyv(j-1))/((d1p2*dyv(j)+dyv(j-1))-(d1p2*dyv(j))) * dx(i) * dz(k) * alphab           &
                        &  + rocp * v(i,j+1,k) * (d1p2*dyv(j+1))**2/((d1p2*dyv(j+1)+dyv(j))**2-(d1p2*dyv(j+1))**2) * dx(i) * dz(k) * alphaf            &
                        &  + rocp * v(i,j  ,k) * (d1p2*dyv(j))*(d1p2*dyv(j)+dyv(j+1))/2/((d1p2*dyv(j))**2+(d1p2*dyv(j))*(d1p2*dyv(j)+dyv(j+1))) * dx(i) * dz(k) * (1-alphab)
                   inerbb= - rocp * v(i,j  ,k) * (d1p2*dyv(j))**2/((d1p2*dyv(j)+dyv(j-1))**2-(d1p2*dyv(j))**2) * dx(i) * dz(k) * alphab
                   inerf = - rocp * v(i,j+1,k) * d1p2*(d1p2*dyv(j+1)+dyv(j))/((d1p2*dyv(j+1))+(d1p2*dyv(j+1)+dyv(j))) * dx(i) * dz(k) * alphaf         &
                        &  - rocp * v(i,j+1,k) * d1p2*(d1p2*dyv(j+1)+dyv(j+2))/((d1p2*dyv(j+1)+dyv(j+2))-(d1p2*dyv(j+1))) * dx(i) * dz(k) * (1-alphaf) &
                        &  - rocp * v(i,j  ,k) * (d1p2*dyv(j))**2/((d1p2*dyv(j))+(d1p2*dyv(j)+dyv(j+1)))/((d1p2*dyv(j)+dyv(j+1))-(d1p2*dyv(j))) * dx(i) * dz(k) * (1-alphab)
                   inerff= + rocp * v(i,j+1,k) * (d1p2*dyv(j+1))**2/((d1p2*dyv(j+1))+(d1p2*dyv(j+1)+dyv(j+2)))/((d1p2*dyv(j+1)+dyv(j+2))-(d1p2*dyv(j+1))) * dx(i) * dz(k) * (1-alphaf)
                   coef(lvs  ) = coef(lvs  )  + inerb + inerf + inerbb + inerff
                   coef(lvs+3) = coef(lvs+3)  - inerb
                   coef(lvs+4) = coef(lvs+4)  - inerf
                   coef(lvs+9) = coef(lvs+9)  - inerbb
                   coef(lvs+10)= coef(lvs+10) - inerff
                   !-------------------------------------------------------------------------------
                   alphab=0
                   alphaf=0
                   if (w(i,j,k  )>0) alphab=1
                   if (w(i,j,k+1)>0) alphaf=1
                   inerb =   rocp * w(i,j,k  ) * d1p2*(d1p2*dzw(k)+dzw(k-1))/((d1p2*dzw(k)+dzw(k-1))-(d1p2*dzw(k))) * dx(i) * dy(j) * alphab           &
                        &  + rocp * w(i,j,k+1) * (d1p2*dzw(k+1))**2/((d1p2*dzw(k+1)+dzw(k))**2-(d1p2*dzw(k+1))**2) * dx(i) * dy(j) * alphaf            &
                        &  + rocp * w(i,j,k  ) * (d1p2*dzw(k))*(d1p2*dzw(k)+dzw(k+1))/2/((d1p2*dzw(k))**2+(d1p2*dzw(k))*(d1p2*dzw(k)+dzw(k+1))) * dx(i) * dy(j) * (1-alphab)
                   inerbb= - rocp * w(i,j,k  ) * (d1p2*dzw(k))**2/((d1p2*dzw(k)+dzw(k-1))**2-(d1p2*dzw(k))**2) * dx(i) * dy(j) * alphab
                   inerf = - rocp * w(i,j,k+1) * d1p2*(d1p2*dzw(k+1)+dzw(k))/((d1p2*dzw(k+1))+(d1p2*dzw(k+1)+dzw(k))) * dx(i) * dy(j) * alphaf         &
                        &  - rocp * w(i,j,k+1) * d1p2*(d1p2*dzw(k+1)+dzw(k+2))/((d1p2*dzw(k+1)+dzw(k+2))-(d1p2*dzw(k+1))) * dx(i) * dy(j) * (1-alphaf) &
                        &  - rocp * w(i,j,k  ) * (d1p2*dzw(k))**2/((d1p2*dzw(k))+(d1p2*dzw(k)+dzw(k+1)))/((d1p2*dzw(k)+dzw(k+1))-(d1p2*dzw(k))) * dx(i) * dy(j) * (1-alphab)
                   inerff= + rocp * w(i,j,k+1) * (d1p2*dzw(k+1))**2/((d1p2*dzw(k+1))+(d1p2*dzw(k+1)+dzw(k+2)))/((d1p2*dzw(k+1)+dzw(k+2))-(d1p2*dzw(k+1))) * dx(i) * dy(j) * (1-alphaf)
                   coef(lvs  ) = coef(lvs  )  + inerb + inerf + inerbb + inerff
                   coef(lvs+5) = coef(lvs+5)  - inerb
                   coef(lvs+6) = coef(lvs+6)  - inerf
                   coef(lvs+11)= coef(lvs+11) - inerbb
                   coef(lvs+12)= coef(lvs+12) - inerff
                case DEFAULT
                   write(*,*) 'Energy 3D inertial scheme does not exist'
                   stop

                end select
                !-------------------------------------------------------------------------------
                ! Linear term
                !-------------------------------------------------------------------------------
                if (EQ_linear_term) coef(lvs) = coef(lvs) + sltp(i,j,k) * dx(i) * dy(j) * dz(k)
                !-------------------------------------------------------------------------------
                ! Penalty term for boundary conditions
                ! pentp (i,j,k,l)
                ! l=1 central T component
                ! l=2 left T component
                ! l=3 right T component
                ! l=4 bottom T component
                ! l=5 top T component
                ! l=6 backward T component
                ! l=7 forward T component
                !-------------------------------------------------------------------------------
                coef(lvs)   = coef(lvs)   + pentp(i,j,k,1) * dx(i) * dy(j) * dz(k)
                coef(lvs+1) = coef(lvs+1) + pentp(i,j,k,2) * dx(i) * dy(j) * dz(k)
                coef(lvs+2) = coef(lvs+2) + pentp(i,j,k,3) * dx(i) * dy(j) * dz(k)
                coef(lvs+3) = coef(lvs+3) + pentp(i,j,k,4) * dx(i) * dy(j) * dz(k)
                coef(lvs+4) = coef(lvs+4) + pentp(i,j,k,5) * dx(i) * dy(j) * dz(k)
                coef(lvs+5) = coef(lvs+5) + pentp(i,j,k,6) * dx(i) * dy(j) * dz(k)
                coef(lvs+6) = coef(lvs+6) + pentp(i,j,k,7) * dx(i) * dy(j) * dz(k)
                !-------------------------------------------------------------------------------
                ! Penalty term for immersed objects
                !-------------------------------------------------------------------------------
                if (EQ_penalty_term) then 
                   !-------------------------------------------------------------------------------
                   ! sptp (i,j,k,l)
                   ! l=1 central T component
                   ! l=2 left T component
                   ! l=3 right T component
                   ! l=4 bottom T component
                   ! l=5 top T component
                   !-------------------------------------------------------------------------------
                   coef(lvs)   = coef(lvs)   + sptp(i,j,k,1) * dx(i) * dy(j) * dz(k)
                   coef(lvs+1) = coef(lvs+1) + sptp(i,j,k,2) * dx(i) * dy(j) * dz(k)
                   coef(lvs+2) = coef(lvs+2) + sptp(i,j,k,3) * dx(i) * dy(j) * dz(k)
                   coef(lvs+3) = coef(lvs+3) + sptp(i,j,k,4) * dx(i) * dy(j) * dz(k)
                   coef(lvs+4) = coef(lvs+4) + sptp(i,j,k,5) * dx(i) * dy(j) * dz(k)
                   coef(lvs+5) = coef(lvs+5) + sptp(i,j,k,6) * dx(i) * dy(j) * dz(k)
                   coef(lvs+6) = coef(lvs+6) + sptp(i,j,k,7) * dx(i) * dy(j) * dz(k)
                   !-------------------------------------------------------------------------------
                end if
                !-------------------------------------------------------------------------------
                ! End of discretization for T
                !-------------------------------------------------------------------------------
             end if

             !-------------------------------------------------------------------------------
             ! next line of the matrix
             !-------------------------------------------------------------------------------
             lvs=lvs+nb_coef_matrix 
          enddo
       enddo
    end do

    return
  end subroutine  Energy_Matrix_3D
  !*****************************************************************************************************
  !*****************************************************************************************************
  !*****************************************************************************************************
  !*****************************************************************************************************








  !*****************************************************************************************************
  !*****************************************************************************************************
  !*****************************************************************************************************
  !*****************************************************************************************************
  ! Energy equation for one fluid model in delta formulation
  !*****************************************************************************************************
  !*****************************************************************************************************
  !*****************************************************************************************************
  !*****************************************************************************************************
  subroutine Energy_Delta (u,v,w,tp,rho,cou,sltp,smtp,sptp,tpin,   &
       & tp0,tp1,condu,condv,condw,cp,pentp,penu,penv,penw,energy, &
       & EQ_source_term,EQ_linear_term,EQ_penalty_term,            &
       & EQ_inertial_scheme,                                       &
       & EQ_solver_type,EQ_solver_threshold,EQ_solver_it,          &
       & EQ_solver_precond,impp)
    !*****************************************************************************************************
    !*****************************************************************************************************
    !*****************************************************************************************************
    !*****************************************************************************************************
    use mod_mpi
    use mod_borders
    use mod_Parameters, only: dim,nb_T,gx,gy,gz,            &
         & ex,sx,ey,sy,ez,sz,                               &
         & exs,sxs,eys,sys,ezs,szs,                         &
         & gsx,gex,gsy,gey,gsz,gez,                         &
         & mesh,switch2D3D,                                 &
         & dx,dy,dz,                                        &
         & Euler,coef_time_1,coef_time_2,coef_time_3
    use mod_Constants, only: one
    use mod_Connectivity
    use mod_operators
    use mod_solver_new
    use mod_struct_solver
    use mod_timers
    !-------------------------------------------------------------------------------
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    logical, intent(in)                                     :: EQ_source_term,EQ_linear_term,EQ_penalty_term
    integer, intent(in)                                     :: EQ_inertial_scheme,EQ_solver_type
    integer, intent(in)                                     :: EQ_solver_it,EQ_solver_precond,impp
    real(8), intent(in)                                     :: EQ_solver_threshold
    real(8), dimension(:,:,:),   allocatable, intent(in)    :: rho
    real(8), dimension(:,:,:),   allocatable, intent(in)    :: cou
    real(8), dimension(:,:,:),   allocatable, intent(in)    :: condu,condv,condw,cp 
    real(8), dimension(:,:,:),   allocatable, intent(in)    :: u,v,w
    real(8), dimension(:,:,:),   allocatable, intent(inout) :: tp,tp0,tp1
    real(8), dimension(:,:,:),   allocatable, intent(in)    :: sltp,smtp
    real(8), dimension(:,:,:),   allocatable, intent(in)    :: tpin
    real(8), dimension(:,:,:,:), allocatable, intent(in)    :: pentp,penu,penv,penw
    real(8), dimension(:,:,:,:), allocatable, intent(in)    :: sptp
    type(solver_sca_t), intent(inout)                       :: energy
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                                 :: i,j,k,l,lts,it_solv,la
    integer                                                 :: ii,jj,kk
    integer                                                 :: nic
    integer, dimension(:), allocatable                      :: jcof,icof
    integer, dimension(:), allocatable                      :: kic
    real(8)                                                 :: pe_m,pe_p,pe_l,pe_r,pe_b,pe_f
    real(8)                                                 :: res,div_norm
    real(8),  dimension(:), allocatable                     :: coef,smc,sol
    real(8),  dimension(:,:,:), allocatable                 :: thetatp
    real(8),  dimension(:,:,:), allocatable                 :: smc_tab
    real(8)                                                 :: eps_slope=1d-15
    type(solver_t)                                          :: solver
    type(system_t)                                          :: system
    !-------------------------------------------------------------------------------

    call compute_time(TIMER_START,"[sub] energy_delta")

    !-------------------------------------------------------------------------------
    ! Hybrid and theta schemes
    !-------------------------------------------------------------------------------
    if (EQ_inertial_scheme==3) then
       allocate(thetatp(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
       thetatp=1
    endif
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Explicit and TVD schemes schemes
    !-------------------------------------------------------------------------------
    if (EQ_inertial_scheme>10) then
       allocate(smc_tab(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
       smc_tab=0
    endif
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Periodicity for explicit variables 
    !-------------------------------------------------------------------------------
    call sca_periodicity(mesh,tp)
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Connectitivities
    !-------------------------------------------------------------------------------
    select case(EQ_inertial_scheme)
    case(4,5)
       call Energy_Connectivity_Large(system%mat,mesh)
    case DEFAULT
       call Energy_Connectivity(system%mat,mesh)
    end select
    !-------------------------------------------------------------------------------
    ! alloc
    !-------------------------------------------------------------------------------
    allocate(sol(system%mat%npt))
    allocate(smc(system%mat%npt))

    !-------------------------------------------------------------------------------
    ! Solved nodes
    !-------------------------------------------------------------------------------
    call Energy_kic(system%mat,mesh)
    !-------------------------------------------------------------------------------
    
    !*******************************************************************************
    ! Solving of A . U = F
    ! coef = A
    ! sol  = U
    ! smc  = F
    !*******************************************************************************


    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------
    ! 2D/3D Energy equation Solving
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Second member of the problem
    !-------------------------------------------------------------------------------
    smc=0
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! External source terms
    !-------------------------------------------------------------------------------
    if (EQ_source_term) then
       do kk=szs,ezs 
          do jj=sys,eys
             do ii=sxs,exs
                i=ii-sxs
                j=jj-sys
                k=kk-szs
                l=i+j*(exs-sxs+1)+switch2D3D*k*(exs-sxs+1)*(eys-sys+1)+1
                smc(l)=smc(l)+smtp(ii,jj,kk) * dx(ii) * dy(jj) * dz(kk) 
             enddo
          enddo
       end do
    endif
    !-------------------------------------------------------------------------------


    !-------------------------------------------------------------------------------
    ! Time integration for temperature time derivative
    !-------------------------------------------------------------------------------
    do kk=szs,ezs
       do jj=sys,eys
          do ii=sxs,exs
             i=ii-sxs
             j=jj-sys
             k=kk-szs
             l=i+j*(exs-sxs+1)+switch2D3D*k*(exs-sxs+1)*(eys-sys+1)+1
             smc(l)=smc(l)-rho(ii,jj,kk)*cp(ii,jj,kk)*coef_time_2*tp0(ii,jj,kk) * dx(ii) * dy(jj) * dz(kk) 
          enddo
       enddo
    end do
    if (abs(Euler)==2) then
       do kk=szs,ezs
          do jj=sys,eys
             do ii=sxs,exs
                i=ii-sxs
                j=jj-sys
                k=kk-szs
                l=i+j*(exs-sxs+1)+switch2D3D*k*(exs-sxs+1)*(eys-sys+1)+1
                smc(l)=smc(l)-rho(ii,jj,kk)*cp(ii,jj,kk)*coef_time_3*tp1(ii,jj,kk) * dx(ii) * dy(jj) * dz(kk) 
             enddo
          enddo
       end do
    endif
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Boundary conditions
    !-------------------------------------------------------------------------------
    do kk=szs,ezs 
       do jj=sys,eys
          do ii=sxs,exs
             i=ii-sxs
             j=jj-sys
             k=kk-szs
             l=i+j*(exs-sxs+1)+switch2D3D*k*(exs-sxs+1)*(eys-sys+1)+1
             smc(l)=smc(l)+pentp(ii,jj,kk,1)*tpin(ii,jj,kk) * dx(ii) * dy(jj) * dz(kk) 
          enddo
       enddo
    end do
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Hybrid scheme
    !-------------------------------------------------------------------------------
    if (EQ_inertial_scheme==3) then
       thetatp=1
       do k=szs,ezs
          do j=sys,eys
             do i=sxs,exs
                pe_p=1
                pe_m=1
                pe_b=(tp(i-1,j,k)-tp(i-2,j,k))*(tp(i,j,k)-tp(i-1,j,k)) ! slope backward
                if (abs(pe_b)>eps_slope) pe_m=sign(one,pe_b)
                pe_f=(tp(i+1,j,k)-tp(i,j,k))*(tp(i+2,j,k)-tp(i+1,j,k)) ! slope forward
                if (abs(pe_f)>eps_slope) pe_p=sign(one,pe_f)
                if((pe_m<0).or.(pe_p<0)) thetatp(i,j,k)=0
                pe_p=1
                pe_m=1
                pe_b=(tp(i,j-1,k)-tp(i,j-2,k))*(tp(i,j,k)-tp(i,j-1,k)) ! slope backward
                if (abs(pe_b)>eps_slope) pe_m=sign(one,pe_b)
                pe_f=(tp(i,j+1,k)-tp(i,j,k))*(tp(i,j+2,k)-tp(i,j+1,k)) ! slope forward
                if (abs(pe_f)>eps_slope) pe_p=sign(one,pe_f)
                if((pe_m<0).or.(pe_p<0)) thetatp(i,j,k)=0
                pe_p=1
                pe_m=1
                pe_b=(tp(i,j,k-1)-tp(i,j,k-2))*(tp(i,j,k)-tp(i,j,k-1)) ! slope backward
                if (abs(pe_b)>eps_slope) pe_m=sign(one,pe_b)
                pe_f=(tp(i,j,k+1)-tp(i,j,k))*(tp(i,j,k+2)-tp(i,j,k+1)) ! slope forward
                if (abs(pe_f)>eps_slope) pe_p=sign(one,pe_f)
                if((pe_m<0).or.(pe_p<0)) thetatp(i,j,k)=0
             end do
          end do
       end do
    endif
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Making the Matrix
    !-------------------------------------------------------------------------------
    if (Euler==1) then
       do kk=szs,ezs 
          do jj=sys,eys
             do ii=sxs,exs
                i=ii-sxs
                j=jj-sys
                k=kk-szs
                l=i+j*(exs-sxs+1)+switch2D3D*k*(exs-sxs+1)*(eys-sys+1)+1
                sol(l)=tp0(ii,jj,kk)
             enddo
          enddo
       end do
    else
       do kk=szs,ezs 
          do jj=sys,eys
             do ii=sxs,exs
                i=ii-sxs
                j=jj-sys
                k=kk-szs
                l=i+j*(exs-sxs+1)+switch2D3D*k*(exs-sxs+1)*(eys-sys+1)+1
                sol(l)=2*tp0(ii,jj,kk)-tp1(ii,jj,kk)
             enddo
          enddo
       end do
    endif
    !-------------------------------------------------------------------------------
    call Energy_Matrix(system%mat,u,v,w,tp,condu,condv,condw,rho,cp,sltp,sptp,pentp,thetatp, &
         & penu,penv,penw,smc_tab,energy,EQ_linear_term,EQ_penalty_term,EQ_inertial_scheme)
    !-------------------------------------------------------------------------------
    
    !-------------------------------------------------------------------------------
    ! Solving the linear system
    !-------------------------------------------------------------------------------
    
    solver%type    = EQ_solver_type
    solver%eqs     = 0
    solver%precond = EQ_solver_precond
    solver%res_max = EQ_solver_threshold
    solver%it_max  = EQ_solver_it

    system%mat%eqs = solver%eqs
!!$    system%mat%npt = mesh%nb%tot%p
!!$    system%mat%nps = energy%nb_matrix_element
!!$    system%mat%ndd = energy%nb_diagonal_precond
!!$    system%mat%kdv = energy%nb_coef_matrix
!!$    system%mat%nic = nic
!!$
!!$    allocate(system%mat%coef(system%mat%nps))   ; system%mat%coef = coef
!!$    allocate(system%mat%icof(system%mat%npt+1)) ; system%mat%icof = icof
!!$    allocate(system%mat%jcof(system%mat%nps))   ; system%mat%jcof = jcof
!!$    allocate(system%mat%kic(system%mat%npt))    ; system%mat%kic  = kic
    
    call solve_linear_system(system,smc,sol,solver,mesh)

    deallocate(system%mat%coef)
    deallocate(system%mat%icof)
    deallocate(system%mat%jcof)
    deallocate(system%mat%kic)
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! The new temperature solution is updated
    !-------------------------------------------------------------------------------
    do kk=szs,ezs 
       do jj=sys,eys
          do ii=sxs,exs
             i=ii-sxs
             j=jj-sys
             k=kk-szs
             l=i+j*(exs-sxs+1)+switch2D3D*k*(exs-sxs+1)*(eys-sys+1)+1
             tp(ii,jj,kk)=sol(l)
          enddo
       enddo
    end do
    !-------------------------------------------------------------------------------

    !===============================================================================
    ! End of solving
    !===============================================================================

    call Interp_Scal_Boundary(tp,energy%bound)

    !-------------------------------------------------------------------------------
    ! Saving solving parameters
    !-------------------------------------------------------------------------------
    energy%output%res_solver(1) = solver%res
    energy%output%it_solver(1)  = solver%it
    !-------------------------------------------------------------------------------
    
    call compute_time(TIMER_END,"[sub] energy_delta")

    !*******************************************************************************
  end subroutine Energy_Delta
  !*******************************************************************************

  !*****************************************************************************************************
  subroutine EN_print (nt,energy)
    !*****************************************************************************************************
    use mod_Parameters, only: rank,EN_solver_type
    use mod_struct_solver
    !-------------------------------------------------------------------------------
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)             :: nt
    type(solver_sca_t), intent(in) :: energy
    !-------------------------------------------------------------------------------
    
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer :: l
    !-------------------------------------------------------------------------------
    
    if (rank==0) then 
       write(*,*) ''//achar(27)//'[32m=========================Energy================================'//achar(27)//'[0m'
    end if
    
101 format(a,1x,i3)
102 format(a,1x,1pe15.8)

    !*****************************************************************************************************
  end subroutine EN_print
  !*****************************************************************************************************


  subroutine EN_res(dt,tp,tp0)
    use mod_mpi
    use mod_Parameters, only: rank, &
         & dim,sx,ex,sy,ey,sz,ez
    !-------------------------------------------------------------------------------
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), intent(in)                                :: dt
    real(8), dimension(:,:,:), allocatable, intent(in) :: tp,tp0
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    real(8)                                            :: deltatp,normtp
    real(8)                                            :: ldeltatp,lnormtp
    real(8)                                            :: maxtp,mintp
    real(8)                                            :: val
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! tp increment 
    !-------------------------------------------------------------------------------
    ldeltatp=maxval(abs(tp(sx:ex,sy:ey,sz:ez)-tp0(sx:ex,sy:ey,sz:ez)))
    lnormtp=maxval(abs(tp(sx:ex,sy:ey,sz:ez)))
    
    call mpi_allreduce(ldeltatp,deltatp,1,mpi_double_precision,mpi_max,comm3d,code)
    call mpi_allreduce(lnormtp,normtp,1,mpi_double_precision,mpi_max,comm3d,code)
    
    if (rank==0) then
       write(*,101) 'Max(|tp-tp0|)/Max(|tp|)/dt=', deltatp/normtp/dt
       write(*,101) 'Max(|tp-tp0|)/dt=', deltatp/dt
    end if
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Maximum values
    !-------------------------------------------------------------------------------
    val=maxval(tp(sx:ex,sy:ey,sz:ez))
    call mpi_allreduce(val,maxtp,1,mpi_double_precision,mpi_max,comm3d,code)
    val=minval(tp(sx:ex,sy:ey,sz:ez))
    call mpi_allreduce(val,mintp,1,mpi_double_precision,mpi_min,comm3d,code)

    if (rank==0) then
       write(*,102) 'Max(tp)=',maxtp,'| Min(tp)=',mintp
    end if
    !-------------------------------------------------------------------------------

101 format(a,1pe15.8)
102 format(999(a,1pe12.5,1x))
    
  end subroutine EN_res
  !*****************************************************************************************************

  
  !*****************************************************************************************************
  subroutine EN_time(tp,tp0,tp1,nt)
    !*****************************************************************************************************
    use mod_Parameters, only: EN_activate
    !-------------------------------------------------------------------------------
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)                                   :: nt
    real(8), dimension(:,:,:), allocatable, intent(inout) :: tp,tp0,tp1
    !-------------------------------------------------------------------------------
    if (EN_activate) then
       
       if (allocated(tp1)) tp1=tp0
       tp0=tp
       
    end if
    
  end subroutine EN_time
  !*****************************************************************************************************

end module mod_Heat



