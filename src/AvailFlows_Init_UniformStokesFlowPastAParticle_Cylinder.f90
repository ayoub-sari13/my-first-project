!===============================================================================
module Bib_VOFLag_Init_UniformStokesFlowPastAParticle_Cylinder
  !===============================================================================
  contains
  !-----------------------------------------------------------------------------  
  !> @author  amine chadil, wided bizid
  ! 
  !> @brief initialisation of the uniform flow past cylinder, with a velocity field in x
  !> direction and his magnitude is equal to 1, and also the radii of the cylinder is equal 
  !> to 1.
  !
  !> @param u,v             : velocity field components
  !> @param pres            : pressure
  !> @param rayon_cylinder  : the cylinder's radii
  !> @param centre_cylinder : the cylinder's center
  !-----------------------------------------------------------------------------
  subroutine Init_UniformStokesFlowPastAParticle_Cylinder(u,v,pres,centre_cylinder,rayon_cylinder)
    !===============================================================================
    !modules
    !===============================================================================
    !-------------------------------------------------------------------------------
    !Modules de definition des structures et variables => src/mod/module_...f90
    !-------------------------------------------------------------------------------
    use mod_struct_thermophysics, only : fluids
    use Module_VOFLag_Flows
    use mod_Parameters          , only : deeptracking,dim,sx,gx,ex,gx,sy,gy,ey,gy,sxu,exu,&
                                         syu,eyu,sxv,exv,syv,eyv,grid_xu,grid_yv,grid_x,grid_y
    !-------------------------------------------------------------------------------
    !Modules de subroutines de la librairie RESPECT => src/Bib_VOFLag_...f90
    !-------------------------------------------------------------------------------
    use Bib_VOFLag_k0
    use Bib_VOFLag_k1
    !-------------------------------------------------------------------------------
    
    !===============================================================================
    !declarations des variables
    !===============================================================================
    implicit none
    !-------------------------------------------------------------------------------
    !variables globales
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable, intent(inout) :: u,v,pres
    real(8), dimension(3)                                 :: centre_cylinder
    real(8)                                               :: rayon_cylinder
    !-------------------------------------------------------------------------------
    !variables locales 
    !-------------------------------------------------------------------------------
    real(8), dimension(2)                                 :: r,x,z,cosphi,sinphi,vr,vphi
    real(8)                                               :: sigma,k_0,k_1,xy,Cxy
    integer                                               :: i,j,k
    !-------------------------------------------------------------------------------
    
    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'entree Init_UniformStokesFlowPastAParticle_Cylinder'
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    !initialisation
    !-------------------------------------------------------------------------------
    sigma=0.1D0 
    k_0=k0(sigma)
    k_1=k1(sigma)
    u=0.d0;v=0.d0
    pres=0.0d0

    !-------------------------------------------------------------------------------
    !imposition de la solution exacte de la vitesse pour un ecoulement autour d'un cylindre
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------
    ! calcul des vitesses exactes et des erreurs sur les vitesses au points de vitesse
    !-------------------------------------------------------------------------------
    do j=syu,eyu
      do i=sxu,exu
        select case (direction)
           case(1)
              x(1)=grid_xu(i)-centre_cylinder(1)
              z(1)=grid_y (j)-centre_cylinder(2)
           case(2)
              x(1)=grid_y (j)-centre_cylinder(2)
              z(1)=grid_xu(i)-centre_cylinder(1)
        end select
        r(1) = sqrt(x(1)**2+z(1)**2)
        cosphi(1)=x(1)/r(1)
        sinphi(1)=z(1)/r(1)
        vr(1)    =(r(1)-(1d0+(2d0*k_1)/(sigma*k_0))/r(1)+ &
                  (2d0*k1(sigma*r(1)))/(sigma*k_0))*cosphi(1)/r(1)
        vphi(1)  =-(1d0+(1d0+(2d0*k_1)/(sigma*k_0))/r(1)**2 &
                  -(2d0/(k_0))*(k0(sigma*r(1))+(k1(sigma*r(1)))/(sigma*r(1))))*sinphi(1)
        if (r(1).gt.rayon_cylinder) then 
          select case (direction)
           case(1)
              u(i,j,1)=cosphi(1)*vr(1)-sinphi(1)*vphi(1)
           case(2)
              u(i,j,1)=sinphi(1)*vr(1)+cosphi(1)*vphi(1)
          end select  
        end if 
      end do 
    end do 

    do j=syv,eyv
      do i=sxv,exv
        select case (direction)
           case(1)
              x(2)=grid_x (i)-centre_cylinder(1)
              z(2)=grid_yv(j)-centre_cylinder(2)
           case(2)
              x(2)=grid_yv(j)-centre_cylinder(2)
              z(2)=grid_x (i)-centre_cylinder(1)
        end select
        r(2) = sqrt(x(2)**2+z(2)**2)
        cosphi(2)=x(2)/r(2)
        sinphi(2)=z(2)/r(2)
        vr(2)    =(r(2)-(1d0+(2d0*k_1)/(sigma*k_0))/r(2)+ &
                  (2d0*k1(sigma*r(2)))/(sigma*k_0))*cosphi(2)/r(2)
        vphi(2)  =-(1d0+(1d0+(2d0*k_1)/(sigma*k_0))/r(2)**2 &
             -(2d0/(k_0))*(k0(sigma*r(2))+(k1(sigma*r(2)))/(sigma*r(2))))*sinphi(2)
        if (r(2).gt.rayon_cylinder) then 
        select case (direction)
           case(1)
              v(i,j,1)=sinphi(2)*vr(2)+cosphi(2)*vphi(2)
           case(2)
              v(i,j,1)=cosphi(2)*vr(2)-sinphi(2)*vphi(2)
        end select
        end if 
      end do 
    end do 
    !-------------------------------------------------------------------------------
    !imposition de la sol exacte de la pression pour un ecoulement autour d'un cylindre
    !-------------------------------------------------------------------------------
    do j=sy,ey
      do i=sx,ex
        select case (direction)
          case(1)
            xy  = grid_x(i)
            Cxy = centre_cylinder(1)
          case(2)
            xy  = grid_y(j)
            Cxy = centre_cylinder(2)
        end select
        r(1)=sqrt((grid_x(i)-centre_cylinder(1))**2+(grid_y(j)-centre_cylinder(2))**2)
        if (r(1).gt.rayon_cylinder) then
          pres(i,j,1)=-fluids(1)%mu*sigma**2*( (1.D0 + (2.D0*k_1)/&
          (sigma*k_0+1D-40) )/(r(1)+1D-40) + r(1))*(xy-Cxy)/(r(1)+1D-40)
        endif
      end do
    end do

    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'entree Init_UniformStokesFlowPastAParticle_Cylinder'
    !-------------------------------------------------------------------------------
  end subroutine Init_UniformStokesFlowPastAParticle_Cylinder

  !===============================================================================
end module Bib_VOFLag_Init_UniformStokesFlowPastAParticle_Cylinder
!===============================================================================