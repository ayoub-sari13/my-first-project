!========================================================================
!
!                   FUGU Version 1.0.0
!
!========================================================================
!
! Copyright  Stéphane Vincent
!
! stephane.vincent@u-pem.fr          straightparadigm@gmail.com
!
! This file is part of the FUGU Fortran library, a set of Computational 
! Fluid Dynamics subroutines devoted to the simulation of multi-scale
! multi-phase flows for resolved scale interfaces (liquid/gas/solid). 
! FUGU uses the initial developments of DyJeAT and SHALA codes from 
! Jean-Luc Estivalezes (jean-luc.estivalezes@onera.fr) and 
! Frédéric Couderc (couderc@math.univ-toulouse.fr).
!
!========================================================================
!**
!**   NAME       : Fugu.f90
!**
!**   AUTHOR     : Stéphane Vincent
!**
!**   FUNCTION   : Level Set subroutines of FUGU software
!**
!**   DATES      : Version 1.0.0  : from : january, 16, 2016
!**
!========================================================================

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
module mod_LevelSet
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  use mod_borders
  use mod_Parameters
  use mod_Constants
  use mod_mpi
  
contains
  !##################################################################################
  !
  !                              level set method RK 3 TVD + WENO 5
  !
  !##################################################################################

  
  !**********************************************************************************
  subroutine levelset_2d (phi,u,v)
    !*******************************************************************************
    !   Modules                                                                     !
    !*******************************************************************************!
    !*******************************************************************************!
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable, intent(in)    :: u,v
    real(8), dimension(:,:,:), allocatable, intent(inout) :: phi
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    integer                                               :: nbr,i,j
    real(8)                                               :: dv
    real(8), dimension(:,:,:), allocatable                :: ut,vt
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------
    allocate(ut(sxu-gx:exu+gx,syu-gy:eyu+gy,szu-gz:ezu+gz))
    allocate(vt(sxv-gx:exv+gx,syv-gy:eyv+gy,szv-gz:ezv+gz))
    call transformation_2d3d(u,ut,ddxu,1)
    call transformation_2d3d(v,vt,ddyv,2) 
    call schemewenonc_2d(phi,ut,vt)
    deallocate(ut,vt)
!!$    dv=0.5d0; nbr=3
!!$
!!$    nbr=nbr*1
!!$    call redistw5_2d(phi,dv,nbr)

    return
  end subroutine levelset_2d
  !*******************************************************************************
  !**********************************************************************************
  subroutine levelset_3d (phi,u,v,w)
    !-------------------------------------------------------------------------------
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:),   allocatable, intent(in)    :: u,v,w
    real(8), dimension(:,:,:),   allocatable, intent(inout) :: phi
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    integer                                                 :: nbr
    real(8)                                                 :: dv
    real(8),  dimension(:,:,:),   allocatable               :: ut,vt,wt
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------
    allocate(ut(sxu-gx:exu+gx,syu-gy:eyu+gy,szu-gz:ezu+gz))
    allocate(vt(sxv-gx:exv+gx,syv-gy:eyv+gy,szv-gz:ezv+gz))
    allocate(wt(sxw-gx:exw+gx,syw-gy:eyw+gy,szw-gz:ezw+gz))
    call transformation_2d3d(u,ut,ddxu,1)
    call transformation_2d3d(v,vt,ddyv,2)
    call transformation_2d3d(w,wt,ddzw,3) 
    call schemewenonc_3d(phi,ut,vt,wt)
    deallocate(ut,vt,wt)
!!$    !if (mod(iteration_temps,frls).eq.0) then
!!$    dv=0.5d0; nbr=3
!!$    call redistw5_3d(phi,dv,nbr)
!!$    !endif

    return
  end subroutine levelset_3d
  !******************************************************************************


  subroutine scheme_laxwendrofftvd_2d(phi,ut,vt)
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------
    ! Solving of ut + vtx.ux = 0 with Lax Wendroff TVD schemme
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------
    !*******************************************************************************!
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable, intent(in)    :: ut,vt
    real(8), dimension(:,:,:), allocatable, intent(inout) :: phi
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                               :: nbcfl,i,j,k,nbb,ntt
    real(8)                                               :: dtcfl,dtrest,dv
    real(8)                                               :: uf,ub,vf,vb
    real(8)                                               :: diff,eps_diff
    real(8)                                               :: phi1,phi2,phi11,phi22
    real(8)                                               :: theta1,theta2,theta11,theta22
    real(8)                                               :: omega1,omega2
    real(8)                                               :: aa,bb
    real(8)                                               :: gts_lvb_dv,gts_lvf_dv
    real(8)                                               :: sign_gts_lvb,sign_gts_lvf
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable                :: phi0
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------

    eps_diff=1d-14
    
    call cfl_ls_2d3d(ut,vt,vt,dtcfl,dtrest,nbcfl)

    if (dtrest /= 0) then
       nbb = nbcfl + 1
    else
       nbb = nbcfl
    end if
    !------------------------------------------------------------------------------
    allocate(phi0(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
    phi0=0    
    !-------------------------------------------------------------------------------
    ! Local time loop
    !-------------------------------------------------------------------------------
    do ntt=1,nbb
       !-------------------------------------------------------------------------------
       if (ntt <= nbcfl) then
          dv=dtcfl
       else
          dv=dtrest
       end if

       !-------------------------------------------------------------------------------
       ! x-direction
       !-------------------------------------------------------------------------------
       k=1
       do j=sy,ey
          do i=sx,ex
             diff=phi(i+1,j,k)-phi(i,j,k)
             if (abs(diff)>=eps_diff) then
                theta1=(phi(i,j,k)-phi(i-1,j,k))/(diff+eps_diff)
                aa=min(one,2*theta1)
                bb=min(2d0,theta1)
                phi1=max(zero,aa,bb)
             else
                phi1=0
             endif
             omega1=diff

             diff=phi(i,j,k)-phi(i-1,j,k)
             if (abs(diff)>=eps_diff) then   
                theta2=(phi(i-1,j,k)-phi(i-2,j,k))/(diff+eps_diff)
                aa=min(one,2*theta2)
                bb=min(2d0,theta2)
                phi2=max(zero,aa,bb)
             else
                phi2=0
             endif
             omega2=diff

             diff=omega1
             if (abs(diff)>=eps_diff) then  
                theta11=(phi(i+2,j,k)-phi(i+1,j,k))/(diff+eps_diff)
                phi11=max(zero,min(one,2*theta11),min(2d0,theta11))
             else
                phi11=0
             endif
             
             diff=omega2
             if (abs(diff)>=eps_diff) then 
                theta22=(phi(i+1,j,k)-phi(i,j,k))/(diff+eps_diff)
                phi22=max(zero,min(one,2*theta22),min(2d0,theta22))
             else
                phi22=0
             endif
             
             ub=ut(i-1,j,k)
             uf=ut(i,j,k)
             gts_lvb_dv=ub*dv
             gts_lvf_dv=uf*dv
             sign_gts_lvb=sign(one,ub)
             sign_gts_lvf=sign(one,uf)

             phi0(i,j,k)=phi(i,j,k) +             &
                  (-dv*                           &
                  (max(zero,ub)*phi(i,j,k)-       &
                  max(zero,ub)*phi(i-1,j,k) )     &
                  -dv*                            &
                  (min(zero,uf)*phi(i+1,j,k)      &
                  -min(zero,uf)*phi(i,j,k)) -     &
                  d1p2*max(zero,sign_gts_lvb)*    &
                  gts_lvb_dv*                     &
                  (gts_lvb_dv-1)*phi2*omega2+     &
                  d1p2*max(zero,sign_gts_lvf)*    &
                  gts_lvf_dv*                     &
                  (gts_lvf_dv-1)*phi1*omega1+     &
                  d1p2*min(zero,sign_gts_lvb)*    &
                  gts_lvb_dv*                     &
                  (gts_lvb_dv+1)*phi22*omega2-    &
                  d1p2*min(zero,sign_gts_lvf)*    &
                  gts_lvf_dv*                     &
                  (gts_lvf_dv+1)*phi11*omega1)
             
          enddo
       end do
       phi=phi0
       
       call sca_borders_and_periodicity(mesh,phi)
       call comm_mpi_sca(phi)

       !-------------------------------------------------------------------------------
       ! y-direction
       !-------------------------------------------------------------------------------
       k=1
       do j=sy,ey
          do i=sx,ex
             diff=phi(i,j+1,k)-phi(i,j,k)
             if (abs(diff)>=eps_diff) then
                theta1=(phi(i,j,k)-phi(i,j-1,k))/(diff+eps_diff)
                aa=min(one,2*theta1)
                bb=min(2d0,theta1)
                phi1=max(zero,aa,bb)
             else
                phi1=0
             endif
             omega1=diff

             diff=phi(i,j,k)-phi(i,j-1,k)
             if (abs(diff)>=eps_diff) then   
                theta2=(phi(i,j-1,k)-phi(i,j-2,k))/(diff+eps_diff)
                aa=min(one,2*theta2)
                bb=min(2d0,theta2)
                phi2=max(zero,aa,bb)
             else
                phi2=0
             endif
             omega2=diff
             
             diff=omega1
             if (abs(diff)>=eps_diff) then  
                theta11=(phi(i,j+2,k)-phi(i,j+1,k))/(diff+eps_diff)
                phi11=max(zero,min(one,2*theta11),min(2d0,theta11))
             else
                phi11=0
             endif
             
             diff=omega2
             if (abs(diff)>=eps_diff) then 
                theta22=(phi(i,j+1,k)-phi(i,j,k))/(diff+eps_diff)
                phi22=max(zero,min(one,2*theta22),min(2d0,theta22))
             else
                phi22=0
             endif

             vb=vt(i,j-1,k)
             vf=vt(i,j,k)
             gts_lvb_dv=vb*dv
             gts_lvf_dv=vf*dv
             sign_gts_lvb=sign(one,vb)
             sign_gts_lvf=sign(one,vf)

             phi0(i,j,k)=phi(i,j,k) +             &
                  (-dv*                           &
                  (max(zero,vb)*phi(i,j,k)-       &
                  max(zero,vb)*phi(i,j-1,k) )     &
                  -dv*                            &
                  (min(zero,vf)*phi(i,j+1,k)      &
                  -min(zero,vf)*phi(i,j,k)) -     &
                  d1p2*max(zero,sign_gts_lvb)*    &
                  gts_lvb_dv*                     &
                  (gts_lvb_dv-1)*phi2*omega2+     &
                  d1p2*max(zero,sign_gts_lvf)*    &
                  gts_lvf_dv*                     &
                  (gts_lvf_dv-1)*phi1*omega1+     & 
                  d1p2*min(zero,sign_gts_lvb)*    &
                  gts_lvb_dv*                     &
                  (gts_lvb_dv+1)*phi22*omega2-    &
                  d1p2*min(zero,sign_gts_lvf)*    &
                  gts_lvf_dv*                     &
                  (gts_lvf_dv+1)*phi11*omega1)

          end do
       enddo
       phi=phi0

       call sca_borders_and_periodicity(mesh,phi)
       call comm_mpi_sca(phi)
       
    end do
    
  end subroutine scheme_laxwendrofftvd_2d

  
  
  !******************************************************************************
  subroutine schemewenoc_2d(phi,ut,vt)
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------
    ! Solving of ut + vtx.ux = 0 with a weno 5 scheme
    ! coupled to a Runge-Kutta method of 3rd order
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------
    !*******************************************************************************!
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:),   allocatable, intent(in)    :: ut,vt
    real(8), dimension(:,:,:),   allocatable, intent(inout) :: phi
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                                 :: nbcfl,i,j,k,nbb,ntt
    real(8)                                                 :: dtcfl,dtrest,dv
    real(8)                                                 :: up,um,vp,vm
    real(8)                                                 :: fx,fy,fp,fm
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:),   allocatable                :: phi0,phi1
    !-------------------------------------------------------------------------------

    call cfl_ls_2d3d(ut,vt,vt,dtcfl,dtrest,nbcfl)

    if (dtrest /= 0) then
       nbb = nbcfl + 1
    else
       nbb = nbcfl
    end if
    !------------------------------------------------------------------------------
    allocate(phi0(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
    allocate(phi1(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
    phi0=0;phi1=0    
    !-------------------------------------------------------------------------------
    ! Local time loop
    !-------------------------------------------------------------------------------
    do ntt = 1,nbb
       !-------------------------------------------------------------------------------
       if (ntt <= nbcfl) then
          dv = dtcfl
       else
          dv = dtrest
       end if

       do j = sy,ey
          do i = sx,ex

             !---------------------------------------
             ! x-direction
             !---------------------------------------
             up=ut(i+1,j,1)
             um=ut(i,j,1)
             if (up>zero) then
                fp=up*weno5_shala(   &
                     & phi(i-2,j,1), &
                     & phi(i-1,j,1), &
                     & phi(i+0,j,1), &
                     & phi(i+1,j,1), &
                     & phi(i+2,j,1))
             else
                fp=up*weno5_shala(   &
                     & phi(i+3,j,1), &
                     & phi(i+2,j,1), &
                     & phi(i+1,j,1), &
                     & phi(i+0,j,1), &
                     & phi(i-1,j,1))
             end if
             if (um>zero) then
                fm=um*weno5_shala(   &
                     & phi(i-3,j,1), &
                     & phi(i-2,j,1), &
                     & phi(i-1,j,1), &
                     & phi(i+0,j,1), &
                     & phi(i+1,j,1))
             else
                fm=um*weno5_shala(   &
                     & phi(i+2,j,1), &
                     & phi(i+1,j,1), &
                     & phi(i+0,j,1), &
                     & phi(i-1,j,1), &
                     & phi(i-2,j,1))
             end if
             fx=(fp-fm)
             !---------------------------------------

             !---------------------------------------
             ! y-direction 
             !---------------------------------------
             vp=vt(i,j+1,1)
             vm=vt(i,j,1)
             if (vp>zero) then
                fp=vp*weno5_shala(   &
                     & phi(i,j-2,1), &
                     & phi(i,j-1,1), &
                     & phi(i,j+0,1), &
                     & phi(i,j+1,1), &
                     & phi(i,j+2,1))
             else
                fp=vp*weno5_shala(   &
                     & phi(i,j+3,1), &
                     & phi(i,j+2,1), &
                     & phi(i,j+1,1), &
                     & phi(i,j+0,1), &
                     & phi(i,j-1,1))
             end if
             if (vm>zero) then
                fm=vm*weno5_shala(   &
                     & phi(i,j-3,1), &
                     & phi(i,j-2,1), &
                     & phi(i,j-1,1), &
                     & phi(i,j+0,1), &
                     & phi(i,j+1,1))
             else
                fm=vm*weno5_shala(   &
                     & phi(i,j+2,1), &
                     & phi(i,j+1,1), &
                     & phi(i,j+0,1), &
                     & phi(i,j-1,1), &
                     & phi(i,j-2,1))
             end if
             fy=(fp-fm)
             !---------------------------------------

             phi0(i,j,1)=phi(i,j,1)-dv*(fx+fy)
             
          end do
       end do

       call sca_borders_and_periodicity(mesh,phi0)
       call comm_mpi_sca(phi0)
       
       do j = sy,ey
          do i = sx,ex
             
             !---------------------------------------
             ! x-direction
             !---------------------------------------
             up=ut(i+1,j,1)
             um=ut(i,j,1)
             if (up>zero) then
                fp=up*weno5_shala(    &
                     & phi0(i-2,j,1), &
                     & phi0(i-1,j,1), &
                     & phi0(i+0,j,1), &
                     & phi0(i+1,j,1), &
                     & phi0(i+2,j,1))
             else
                fp=up*weno5_shala(    &
                     & phi0(i+3,j,1), &
                     & phi0(i+2,j,1), &
                     & phi0(i+1,j,1), &
                     & phi0(i+0,j,1), &
                     & phi0(i-1,j,1))
             end if
             if (um>zero) then
                fm=um*weno5_shala(    &
                     & phi0(i-3,j,1), &
                     & phi0(i-2,j,1), &
                     & phi0(i-1,j,1), &
                     & phi0(i+0,j,1), &
                     & phi0(i+1,j,1))
             else
                fm=um*weno5_shala(    &
                     & phi0(i+2,j,1), &
                     & phi0(i+1,j,1), &
                     & phi0(i+0,j,1), &
                     & phi0(i-1,j,1), &
                     & phi0(i-2,j,1))
             end if
             fx=(fp-fm)
             !---------------------------------------

             !---------------------------------------
             ! y-direction 
             !---------------------------------------
             vp=vt(i,j+1,1)
             vm=vt(i,j,1)
             if (vp>zero) then
                fp=vp*weno5_shala(    &
                     & phi0(i,j-2,1), &
                     & phi0(i,j-1,1), &
                     & phi0(i,j+0,1), &
                     & phi0(i,j+1,1), &
                     & phi0(i,j+2,1))
             else
                fp=vp*weno5_shala(    &
                     & phi0(i,j+3,1), &
                     & phi0(i,j+2,1), &
                     & phi0(i,j+1,1), &
                     & phi0(i,j+0,1), &
                     & phi0(i,j-1,1))
             end if
             if (vm>zero) then
                fm=vm*weno5_shala(    &
                     & phi0(i,j-3,1), &
                     & phi0(i,j-2,1), &
                     & phi0(i,j-1,1), &
                     & phi0(i,j+0,1), &
                     & phi0(i,j+1,1))
             else
                fm=vm*weno5_shala(    &
                     & phi0(i,j+2,1), &
                     & phi0(i,j+1,1), &
                     & phi0(i,j+0,1), &
                     & phi0(i,j-1,1), &
                     & phi0(i,j-2,1))
             end if
             fy=(fp-fm)
             !---------------------------------------
             
             phi1(i,j,1)=d3p4*phi(i,j,1)+d1p4*(&
                  phi0(i,j,1)-dv*(fx+fy))
          end do
       end do

       call sca_borders_and_periodicity(mesh,phi1)
       call comm_mpi_sca(phi1)
       
       do j = sy,ey
          do i = sx,ex
             
             !---------------------------------------
             ! x-direction
             !---------------------------------------
             up=ut(i+1,j,1)
             um=ut(i,j,1)
             if (up>zero) then
                fp=up*weno5_shala(    & 
                     & phi1(i-2,j,1), &
                     & phi1(i-1,j,1), &
                     & phi1(i+0,j,1), &
                     & phi1(i+1,j,1), &
                     & phi1(i+2,j,1))
             else
                fp=up*weno5_shala(    &
                     & phi1(i+3,j,1), &
                     & phi1(i+2,j,1), &
                     & phi1(i+1,j,1), &
                     & phi1(i+0,j,1), &
                     & phi1(i-1,j,1))
             end if
             if (um>zero) then
                fm=um*weno5_shala(    &
                     & phi1(i-3,j,1), &
                     & phi1(i-2,j,1), &
                     & phi1(i-1,j,1), &
                     & phi1(i+0,j,1), &
                     & phi1(i+1,j,1))
             else
                fm=um*weno5_shala(    &
                     & phi1(i+2,j,1), &
                     & phi1(i+1,j,1), &
                     & phi1(i+0,j,1), &
                     & phi1(i-1,j,1), &
                     & phi1(i-2,j,1))
             end if
             fx=(fp-fm)
             !---------------------------------------

             !---------------------------------------
             ! y-direction 
             !---------------------------------------
             vp=vt(i,j+1,1)
             vm=vt(i,j,1)
             if (vp>zero) then
                fp=vp*weno5_shala(    &
                     & phi1(i,j-2,1), &
                     & phi1(i,j-1,1), &
                     & phi1(i,j+0,1), &
                     & phi1(i,j+1,1), &
                     & phi1(i,j+2,1))
             else
                fp=vp*weno5_shala(    &
                     & phi1(i,j+3,1), &
                     & phi1(i,j+2,1), &
                     & phi1(i,j+1,1), &
                     & phi1(i,j+0,1), &
                     & phi1(i,j-1,1))
             end if
             if (vm>zero) then
                fm=vm*weno5_shala(    &
                     & phi1(i,j-3,1), &
                     & phi1(i,j-2,1), &
                     & phi1(i,j-1,1), &
                     & phi1(i,j+0,1), &
                     & phi1(i,j+1,1))
             else
                fm=vm*weno5_shala(    &
                     & phi1(i,j+2,1), &
                     & phi1(i,j+1,1), &
                     & phi1(i,j+0,1), &
                     & phi1(i,j-1,1), &
                     & phi1(i,j-2,1))
             end if
             fy=(fp-fm)
             !---------------------------------------
             
             phi(i,j,1)=d1p3*phi(i,j,1)+d2p3*(&
                  phi1(i,j,1)-dv*(fx+fy))
          end do
       end do

       call sca_borders_and_periodicity(mesh,phi)
       call comm_mpi_sca(phi)
       
    end do
    deallocate(phi0,phi1)
    return
  end subroutine  schemewenoc_2d


  !******************************************************************************
  !******************************************************************************
  subroutine schemewenonc_2d(phi_weno,ut,vt)
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------
    ! Solving of ut + vtx.ux = 0 with a weno 5 scheme
    ! coupled to a Runge-Kutta method of 3rd order
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------
    !*******************************************************************************!
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:),   allocatable, intent(in)    :: ut,vt
    real(8), dimension(:,:,:),   allocatable, intent(inout) :: phi_weno
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    integer                                                 :: nbcfl,i,j,k,nbb,ntt
    real(8)                                                 :: dtcfl,dtrest,dv
    real(8)                                                 :: up,um,vp,vm
    !-------------------------------------------------------------------------------
    real(8), dimension(-3:3)                                :: veci,vecj
    real(8), dimension(:,:,:),   allocatable                :: phi0,phi1
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------

    call cfl_ls_2d3d(ut,vt,vt,dtcfl,dtrest,nbcfl)

    if (dtrest /= 0) then
       nbb = nbcfl + 1
    else
       nbb = nbcfl
    end if
    !------------------------------------------------------------------------------
    allocate(phi0(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
    allocate(phi1(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
    phi0=0;phi1=0
    !-------------------------------------------------------------------------------
    ! Local time loop
    !-------------------------------------------------------------------------------
    do ntt = 1,nbb
       !-------------------------------------------------------------------------------
       if (ntt <= nbcfl) then
          dv = dtcfl
       else
          dv = dtrest
       end if

       do j = sy,ey
          do i = sx,ex
             up=ut(i+1,j,1) ; um=ut(i,j,1)
             vp=vt(i,j+1,1) ; vm=vt(i,j,1)
             veci=phi_weno(i-3:i+3,j,1)
             vecj=phi_weno(i,j-3:j+3,1)
             phi0(i,j,1)=phi_weno(i,j,1)-dv*(      &
                  (up+um)*d1p2*weno5(veci,up,um) + &
                  (vp+vm)*d1p2*weno5(vecj,vp,vm)   &
                  )  
          end do
       end do

       call sca_borders_and_periodicity(mesh,phi0)
       call comm_mpi_sca(phi0)
       
       do j = sy,ey
          do i = sx,ex
             up=ut(i+1,j,1) ; um=ut(i,j,1)
             vp=vt(i,j+1,1) ; vm=vt(i,j,1)
             veci=phi0(i-3:i+3,j,1)
             vecj=phi0(i,j-3:j+3,1)
             phi1(i,j,1)=d3p4*phi_weno(i,j,1)+d1p4*( &
                  phi0(i,j,1)-dv*(                   &
                  (up+um)*d1p2*weno5(veci,up,um) +   &
                  (vp+vm)*d1p2*weno5(vecj,vp,vm)     &
                  ))
          end do
       end do

       call sca_borders_and_periodicity(mesh,phi1)
       call comm_mpi_sca(phi1)

       do j = sy,ey
          do i = sx,ex
             up=ut(i+1,j,1) ; um=ut(i,j,1)
             vp=vt(i,j+1,1) ; vm=vt(i,j,1)
             veci=phi1(i-3:i+3,j,1)
             vecj=phi1(i,j-3:j+3,1)
             phi_weno(i,j,1)=d1p3*phi_weno(i,j,1)+d2p3*( &
                  phi1(i,j,1)-dv*(                       &
                  (up+um)*d1p2*weno5(veci,up,um) +       & 
                  (vp+vm)*d1p2*weno5(vecj,vp,vm)         &
                  ))
          end do
       end do

       call sca_borders_and_periodicity(mesh,phi_weno)
       call comm_mpi_sca(phi_weno)

    end do
    deallocate(phi0,phi1)
    return
  end subroutine  schemewenonc_2d
  !******************************************************************************
  !******************************************************************************
  subroutine schemewenoc_3d(phi_weno,ut,vt,wt)
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------
    ! Solving of ut + vtx.ux = 0 with a weno 5 scheme
    ! coupled to a Runge-Kutta method of 3rd order
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------
    !*******************************************************************************!
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:),   allocatable, intent(in)    :: ut,vt,wt
    real(8), dimension(:,:,:),   allocatable, intent(inout) :: phi_weno
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                                 :: nbcfl,i,j,k,nbb,ntt
    real(8)                                                 :: dtcfl,dtrest,dv
    real(8)                                                 :: up,um,vp,vm,wp,wm
    real(8)                                                 :: fx,fy,fz,fp,fm
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:),   allocatable                :: phi0,phi1
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------

    call cfl_ls_2d3d(ut,vt,wt,dtcfl,dtrest,nbcfl)

    if (dtrest /= 0) then
       nbb = nbcfl + 1
    else
       nbb = nbcfl
    end if
    !------------------------------------------------------------------------------
    allocate(phi0(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
    allocate(phi1(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
    phi0=0;phi1=0
    !-------------------------------------------------------------------------------
    ! Local time loop
    !-------------------------------------------------------------------------------
    do ntt = 1,nbb
       !-------------------------------------------------------------------------------
       if (ntt <= nbcfl) then
          dv = dtcfl
       else
          dv = dtrest
       end if

       do k = sz,ez 
          do j = sy,ey
             do i = sx,ex
                
                !---------------------------------------
                ! x-direction
                !---------------------------------------
                up=ut(i+1,j,k)
                um=ut(i,j,k)
                if (up>zero) then
                   fp=up*weno5_shala(       &
                        & phi_weno(i-2,j,k),&
                        & phi_weno(i-1,j,k),&
                        & phi_weno(i+0,j,k),&
                        & phi_weno(i+1,j,k),&
                        & phi_weno(i+2,j,k))
                else
                   fp=up*weno5_shala(       &
                        & phi_weno(i+3,j,k),&
                        & phi_weno(i+2,j,k),&
                        & phi_weno(i+1,j,k),&
                        & phi_weno(i+0,j,k),&
                        & phi_weno(i-1,j,k))
                end if
                if (um>zero) then
                   fm=um*weno5_shala(       &
                        & phi_weno(i-3,j,k),&
                        & phi_weno(i-2,j,k),&
                        & phi_weno(i-1,j,k),&
                        & phi_weno(i+0,j,k),&
                        & phi_weno(i+1,j,k))
                else
                   fm=um*weno5_shala(       &
                        & phi_weno(i+2,j,k),&
                        & phi_weno(i+1,j,k),&
                        & phi_weno(i+0,j,k),&
                        & phi_weno(i-1,j,k),&
                        & phi_weno(i-2,j,k))
                end if
                fx=(fp-fm)
                !---------------------------------------

                !---------------------------------------
                ! y-direction 
                !---------------------------------------
                vp=vt(i,j+1,k)
                vm=vt(i,j,k)
                if (vp>zero) then
                   fp=vp*weno5_shala(       &
                        & phi_weno(i,j-2,k),&
                        & phi_weno(i,j-1,k),&
                        & phi_weno(i,j+0,k),&
                        & phi_weno(i,j+1,k),&
                        & phi_weno(i,j+2,k))
                else
                   fp=vp*weno5_shala(       &
                        & phi_weno(i,j+3,k),&
                        & phi_weno(i,j+2,k),&
                        & phi_weno(i,j+1,k),&
                        & phi_weno(i,j+0,k),&
                        & phi_weno(i,j-1,k))
                end if
                if (vm>zero) then
                   fm=vm*weno5_shala(       &
                        & phi_weno(i,j-3,k),&
                        & phi_weno(i,j-2,k),&
                        & phi_weno(i,j-1,k),&
                        & phi_weno(i,j+0,k),&
                        & phi_weno(i,j+1,k))
                else
                   fm=vm*weno5_shala(       &
                        & phi_weno(i,j+2,k),&
                        & phi_weno(i,j+1,k),&
                        & phi_weno(i,j+0,k),&
                        & phi_weno(i,j-1,k),&
                        & phi_weno(i,j-2,k))
                end if
                fy=(fp-fm)
                !---------------------------------------
                
                !---------------------------------------
                ! z-direction 
                !---------------------------------------
                wp=wt(i,j,k+1)
                wm=wt(i,j,k)
                if (wp>zero) then
                   fp=wp*weno5_shala(       &
                        & phi_weno(i,j,k-2),&
                        & phi_weno(i,j,k-1),&
                        & phi_weno(i,j,k+0),&
                        & phi_weno(i,j,k+1),&
                        & phi_weno(i,j,k+2))
                else
                   fp=wp*weno5_shala(       &
                        & phi_weno(i,j,k+3),&
                        & phi_weno(i,j,k+2),&
                        & phi_weno(i,j,k+1),&
                        & phi_weno(i,j,k+0),&
                        & phi_weno(i,j,k-1))
                end if
                if (wm>zero) then
                   fm=wm*weno5_shala(       &
                        & phi_weno(i,j,k-3),&
                        & phi_weno(i,j,k-2),&
                        & phi_weno(i,j,k-1),&
                        & phi_weno(i,j,k+0),&
                        & phi_weno(i,j,k+1))
                else
                   fm=wm*weno5_shala(       &
                        & phi_weno(i,j,k+2),&
                        & phi_weno(i,j,k+1),&
                        & phi_weno(i,j,k+0),&
                        & phi_weno(i,j,k-1),&
                        & phi_weno(i,j,k-2))
                end if
                fz=(fp-fm)
                !---------------------------------------

                phi0(i,j,k)=phi_weno(i,j,k)-dv*(fx+fy+fz)
             end do
          end do
       end do

       call sca_borders_and_periodicity(mesh,phi0)
       call comm_mpi_sca(phi0)
       
       do k = sz,ez 
          do j = sy,ey
             do i = sx,ex

                !---------------------------------------
                ! x-direction
                !---------------------------------------
                up=ut(i+1,j,k)
                um=ut(i,j,k)
                if (up>zero) then
                   fp=up*weno5_shala(   &
                        & phi0(i-2,j,k),&
                        & phi0(i-1,j,k),&
                        & phi0(i+0,j,k),&
                        & phi0(i+1,j,k),&
                        & phi0(i+2,j,k))
                else
                   fp=up*weno5_shala(   &
                        & phi0(i+3,j,k),&
                        & phi0(i+2,j,k),&
                        & phi0(i+1,j,k),&
                        & phi0(i+0,j,k),&
                        & phi0(i-1,j,k))
                end if
                if (um>zero) then
                   fm=um*weno5_shala(   &
                        & phi0(i-3,j,k),&
                        & phi0(i-2,j,k),&
                        & phi0(i-1,j,k),&
                        & phi0(i+0,j,k),&
                        & phi0(i+1,j,k))
                else
                   fm=um*weno5_shala(   &
                        & phi0(i+2,j,k),&
                        & phi0(i+1,j,k),&
                        & phi0(i+0,j,k),&
                        & phi0(i-1,j,k),&
                        & phi0(i-2,j,k))
                end if
                fx=(fp-fm)
                !---------------------------------------

                !---------------------------------------
                ! y-direction 
                !---------------------------------------
                vp=vt(i,j+1,k)
                vm=vt(i,j,k)
                if (vp>zero) then
                   fp=vp*weno5_shala(   &
                        & phi0(i,j-2,k),&
                        & phi0(i,j-1,k),&
                        & phi0(i,j+0,k),&
                        & phi0(i,j+1,k),&
                        & phi0(i,j+2,k))
                else
                   fp=vp*weno5_shala(   &
                        & phi0(i,j+3,k),&
                        & phi0(i,j+2,k),&
                        & phi0(i,j+1,k),&
                        & phi0(i,j+0,k),&
                        & phi0(i,j-1,k))
                end if
                if (vm>zero) then
                   fm=vm*weno5_shala(   &
                        & phi0(i,j-3,k),&
                        & phi0(i,j-2,k),&
                        & phi0(i,j-1,k),&
                        & phi0(i,j+0,k),&
                        & phi0(i,j+1,k))
                else
                   fm=vm*weno5_shala(   &
                        & phi0(i,j+2,k),&
                        & phi0(i,j+1,k),&
                        & phi0(i,j+0,k),&
                        & phi0(i,j-1,k),&
                        & phi0(i,j-2,k))
                end if
                fy=(fp-fm)
                !---------------------------------------
                
                !---------------------------------------
                ! z-direction 
                !---------------------------------------
                wp=wt(i,j,k+1)
                wm=wt(i,j,k)
                if (wp>zero) then
                   fp=wp*weno5_shala(   &
                        & phi0(i,j,k-2),&
                        & phi0(i,j,k-1),&
                        & phi0(i,j,k+0),&
                        & phi0(i,j,k+1),&
                        & phi0(i,j,k+2))
                else
                   fp=wp*weno5_shala(   &
                        & phi0(i,j,k+3),&
                        & phi0(i,j,k+2),&
                        & phi0(i,j,k+1),&
                        & phi0(i,j,k+0),&
                        & phi0(i,j,k-1))
                end if
                if (wm>zero) then
                   fm=wm*weno5_shala(   &
                        & phi0(i,j,k-3),&
                        & phi0(i,j,k-2),&
                        & phi0(i,j,k-1),&
                        & phi0(i,j,k+0),&
                        & phi0(i,j,k+1))
                else
                   fm=wm*weno5_shala(   &
                        & phi0(i,j,k+2),&
                        & phi0(i,j,k+1),&
                        & phi0(i,j,k+0),&
                        & phi0(i,j,k-1),&
                        & phi0(i,j,k-2))
                end if
                fz=(fp-fm)
                !---------------------------------------

                phi1(i,j,k)=d3p4*phi_weno(i,j,k)+d1p4*(&
                     phi0(i,j,k)-dv*(fx+fy+fz))
             end do
          end do
       end do

       call sca_borders_and_periodicity(mesh,phi1)
       call comm_mpi_sca(phi1)

       do k = sz,ez 
          do j = sy,ey
             do i = sx,ex

                !---------------------------------------
                ! x-direction
                !---------------------------------------
                up=ut(i+1,j,k)
                um=ut(i,j,k)
                if (up>zero) then
                   fp=up*weno5_shala(   &
                        & phi1(i-2,j,k),&
                        & phi1(i-1,j,k),&
                        & phi1(i+0,j,k),&
                        & phi1(i+1,j,k),&
                        & phi1(i+2,j,k))
                else
                   fp=up*weno5_shala(   &
                        & phi1(i+3,j,k),&
                        & phi1(i+2,j,k),&
                        & phi1(i+1,j,k),&
                        & phi1(i+0,j,k),&
                        & phi1(i-1,j,k))
                end if
                if (um>zero) then
                   fm=um*weno5_shala(   &
                        & phi1(i-3,j,k),&
                        & phi1(i-2,j,k),&
                        & phi1(i-1,j,k),&
                        & phi1(i+0,j,k),&
                        & phi1(i+1,j,k))
                else
                   fm=um*weno5_shala(   &
                        & phi1(i+2,j,k),&
                        & phi1(i+1,j,k),&
                        & phi1(i+0,j,k),&
                        & phi1(i-1,j,k),&
                        & phi1(i-2,j,k))
                end if
                fx=(fp-fm)
                !---------------------------------------

                !---------------------------------------
                ! y-direction 
                !---------------------------------------
                vp=vt(i,j+1,k)
                vm=vt(i,j,k)
                if (vp>zero) then
                   fp=vp*weno5_shala(   &
                        & phi1(i,j-2,k),&
                        & phi1(i,j-1,k),&
                        & phi1(i,j+0,k),&
                        & phi1(i,j+1,k),&
                        & phi1(i,j+2,k))
                else
                   fp=vp*weno5_shala(   &
                        & phi1(i,j+3,k),&
                        & phi1(i,j+2,k),&
                        & phi1(i,j+1,k),&
                        & phi1(i,j+0,k),&
                        & phi1(i,j-1,k))
                end if
                if (vm>zero) then
                   fm=vm*weno5_shala(   &
                        & phi1(i,j-3,k),&
                        & phi1(i,j-2,k),&
                        & phi1(i,j-1,k),&
                        & phi1(i,j+0,k),&
                        & phi1(i,j+1,k))
                else
                   fm=vm*weno5_shala(   &
                        & phi1(i,j+2,k),&
                        & phi1(i,j+1,k),&
                        & phi1(i,j+0,k),&
                        & phi1(i,j-1,k),&
                        & phi1(i,j-2,k))
                end if
                fy=(fp-fm)
                !---------------------------------------
                
                !---------------------------------------
                ! z-direction 
                !---------------------------------------
                wp=wt(i,j,k+1)
                wm=wt(i,j,k)
                if (wp>zero) then
                   fp=wp*weno5_shala(   &
                        & phi1(i,j,k-2),&
                        & phi1(i,j,k-1),&
                        & phi1(i,j,k+0),&
                        & phi1(i,j,k+1),&
                        & phi1(i,j,k+2))
                else
                   fp=wp*weno5_shala(   &
                        & phi1(i,j,k+3),&
                        & phi1(i,j,k+2),&
                        & phi1(i,j,k+1),&
                        & phi1(i,j,k+0),&
                        & phi1(i,j,k-1))
                end if
                if (wm>zero) then
                   fm=wm*weno5_shala(   &
                        & phi1(i,j,k-3),&
                        & phi1(i,j,k-2),&
                        & phi1(i,j,k-1),&
                        & phi1(i,j,k+0),&
                        & phi1(i,j,k+1))
                else
                   fm=wm*weno5_shala(   &
                        & phi1(i,j,k+2),&
                        & phi1(i,j,k+1),&
                        & phi1(i,j,k+0),&
                        & phi1(i,j,k-1),&
                        & phi1(i,j,k-2))
                end if
                fz=(fp-fm)
                !---------------------------------------
                
                phi_weno(i,j,k)=d1p3*phi_weno(i,j,k)+d2p3*(&
                     phi1(i,j,k)-dv*(fx+fy+fz))
             end do
          end do
       end do

       call sca_borders_and_periodicity(mesh,phi_weno)
       call comm_mpi_sca(phi_weno)
       
    end do
    deallocate(phi0,phi1)
    return
  end subroutine schemewenoc_3d

  !******************************************************************************
  !******************************************************************************
  subroutine schemewenonc_3d(phi_weno,ut,vt,wt)
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------
    ! Solving of ut + vtx.ux = 0 with a weno 5 scheme
    ! coupled to a Runge-Kutta method of 3rd order
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------
    !*******************************************************************************!
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:),   allocatable, intent(inout) :: phi_weno
    real(8), dimension(:,:,:),   allocatable, intent(in)    :: ut,vt,wt
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    integer                                                 :: nbcfl,i,j,k,nbb,ntt
    real(8)                                                 :: dtcfl,dtrest,dv
    real(8)                                                 :: up,um,vp,vm,wp,wm
    !-------------------------------------------------------------------------------
    real(8), dimension(-3:3)                                :: veci,vecj,veck
    real(8), dimension(:,:,:),   allocatable                :: phi0,phi1
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------

    call cfl_ls_2d3d(ut,vt,wt,dtcfl,dtrest,nbcfl)

    if (dtrest /= 0) then
       nbb = nbcfl + 1
    else
       nbb = nbcfl
    end if
    !------------------------------------------------------------------------------
    allocate(phi0(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
    allocate(phi1(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
    phi0=0;phi1=0
    !-------------------------------------------------------------------------------
    ! Local time loop
    !-------------------------------------------------------------------------------
    do ntt = 1,nbb
       !-------------------------------------------------------------------------------
       if (ntt <= nbcfl) then
          dv = dtcfl
       else
          dv = dtrest
       end if
       
       do k = sz,ez
          do j = sy,ey
             do i = sx,ex
                up=ut(i+1,j,k) ; um=ut(i,j,k)
                vp=vt(i,j+1,k) ; vm=vt(i,j,k)
                wp=wt(i,j,k+1) ; wm=wt(i,j,k)
                veci=phi_weno(i-3:i+3,j,k)
                vecj=phi_weno(i,j-3:j+3,k)
                veck=phi_weno(i,j,k-3:k+3)              
                phi0(i,j,k)=phi_weno(i,j,k)-dv*(      &
                     (up+um)*d1p2*weno5(veci,up,um) + &
                     (vp+vm)*d1p2*weno5(vecj,vp,vm) + &
                     (wp+wm)*d1p2*weno5(veck,wp,wm)   &
                     ) 
             end do
          end do
       end do

       call sca_borders_and_periodicity(mesh,phi0)
       call comm_mpi_sca(phi0)
       
       do k = sz,ez
          do j = sy,ey
             do i = sx,ex
                up=ut(i+1,j,k) ; um=ut(i,j,k)
                vp=vt(i,j+1,k) ; vm=vt(i,j,k)
                wp=wt(i,j,k+1) ; wm=wt(i,j,k)
                veci=phi0(i-3:i+3,j,k)
                vecj=phi0(i,j-3:j+3,k)
                veck=phi0(i,j,k-3:k+3)              
                phi1(i,j,k)=d3p4*phi_weno(i,j,k)+d1p4*(&
                     phi0(i,j,k)-dv*(                  &
                     (up+um)*d1p2*weno5(veci,up,um) +  &
                     (vp+vm)*d1p2*weno5(vecj,vp,vm) +  &
                     (wp+wm)*d1p2*weno5(veck,wp,wm)    &
                     ))
             end do
          end do
       end do

       call sca_borders_and_periodicity(mesh,phi1)
       call comm_mpi_sca(phi1)

       do k = sz,ez
          do j = sy,ey
             do i = sx,ex
                up=ut(i+1,j,k) ; um=ut(i,j,k)
                vp=vt(i,j+1,k) ; vm=vt(i,j,k)
                wp=wt(i,j,k+1) ; wm=wt(i,j,k)
                veci=phi1(i-3:i+3,j,k)
                vecj=phi1(i,j-3:j+3,k)
                veck=phi1(i,j,k-3:k+3)              
                phi_weno(i,j,k)=d1p3*phi_weno(i,j,k)+d2p3*(&
                     phi1(i,j,k)-dv*(                      &
                     (up+um)*d1p2*weno5(veci,up,um) +      &
                     (vp+vm)*d1p2*weno5(vecj,vp,vm) +      &
                     (wp+wm)*d1p2*weno5(veck,wp,wm)        &
                     )) 
             end do
          end do
       end do

       call sca_borders_and_periodicity(mesh,phi_weno)
       call comm_mpi_sca(phi_weno)
       
    end do
    deallocate(phi0,phi1)
    return
  end subroutine  schemewenonc_3d

  !*******************************************************************************
  subroutine redistw5_2d(phi,dv,nbr)
    USE mod_InOut
    !*******************************************************************************
    !   Modules                                                                     !
    !*******************************************************************************!
    !*******************************************************************************!
    !-------------------------------------------------------------------------------
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)                                     :: nbr
    real(8), intent(in)                                     :: dv
    real(8), dimension(:,:,:),   allocatable, intent(inout) :: phi
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    integer                                                 :: itau,i,j
    real(8), dimension(:,:,:),   allocatable                :: opd,phi1
    !-------------------------------------------------------------------------------
    allocate(opd(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz),&
         & phi1(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
    !phi=phi*ddx
    ! BENOIT COMPREND PAS METRIQUE ICI
    do j=sy-gy,ey+gy
       do i=sx-gx,ex+gx
          phi(i,j,1)=phi(i,j,1)*ddx(i)
       end do
    end do
    
    opd=0
    do itau=1,nbr
       
       !call Write_file_2d(phi*dx,itau-1)
       
       call redisweno_2d(opd,phi,phi)
       do j= sy-3,ey+3
          do i = sx-3,ex+3
             phi1(i,j,1)=phi(i,j,1)+dv*opd(i,j,1)
          end do
       end do
       call symmetry_2d(phi1)
       
       call redisweno_2d(opd,phi1,phi)
       do j= sy-3,ey+3
          do i = sx-3,ex+3
             phi1(i,j,1)=d3p4*phi(i,j,1)+d1p4*(phi1(i,j,1)+dv*opd(i,j,1))
          enddo
       end do
       
       call symmetry_2d(phi1)
       
       !call redisweno_2d(opd,phi2,phi0)
       call redisweno_2d(opd,phi1,phi)
       do j= sy-3,ey+3
          do i = sx-3,ex+3
             phi(i,j,1)=d1p3*phi(i,j,1)+d2p3*(phi1(i,j,1)+dv*opd(i,j,1))
          end do
       enddo
       call symmetry_2d(phi)
       !call erreur_redis_2d(phi)
    end do
    !phi=phi*dx
    deallocate(phi1)
    !call Write_file_2d(phi,itau)
    !write(*,*) itau
    !call erreur_redis_2d(phi)

    return
  end subroutine redistw5_2d
  !*******************************************************************************


  subroutine redistw5_3d(phi,dv,nbr)
    !*******************************************************************************!
    !-------------------------------------------------------------------------------
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)                                     :: nbr
    real(8), intent(in)                                     :: dv
    real(8), dimension(:,:,:),   allocatable, intent(inout) :: phi
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    integer                                                 :: itau,i,j,k
    real(8), dimension(:,:,:),   allocatable                :: opd,phi0,phi1,phi2
    !-------------------------------------------------------------------------------
    allocate(phi0(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz),&
         & phi1(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz),  &
         & phi2(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz),  &
         & opd(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
    
    phi0=phi
    do itau=1,nbr
       call redisweno_3d(opd,phi,phi0)
       do k= sz,ez
          do j = sy,ey
             do i = sx,ex
                phi1(i,j,k)=phi(i,j,k)+dv*opd(i,j,k)
             end do
          end do
       end do
       call periodicity_3d(phi1)


       call redisweno_3d(opd,phi1,phi0)
       do k= sz,ez
          do j = sy,ey
             do i = sx,ex
                phi2(i,j,k)=3.d0/4.d0*phi(i,j,k)+1.d0/4.d0*(phi1(i,j,k)+dv*opd(i,j,k))
             end do
          end do
       end do
       call periodicity_3d(phi2)


       call redisweno_3d(opd,phi2,phi0)
       do k= sz,ez
          do j = sy,ey
             do i = sx,ex
                phi(i,j,k)=1.d0/3.d0*phi(i,j,k)+2.d0/3.d0*(phi2(i,j,k)+dv*opd(i,j,k))
             end do
          end do
       end do
       call periodicity_3d(phi)


    end do
    deallocate(phi0,phi1,phi2,opd)
    !
    return
    !
  end subroutine redistw5_3d
  !*******************************************************************************

  !*******************************************************************************
  subroutine redisweno_2d(opd,phi,phi0)
    !*******************************************************************************
    !-------------------------------------------------------------------------------
    implicit none
    !-------------------------------------------------------------------------------
    !Global variabes
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:),   allocatable, intent(in) :: phi,phi0
    real(8), dimension(:,:,:),   allocatable, intent(out):: opd
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    integer                                              :: i,j,k
    real(8)                                              :: uplus,umoins,vplus,vmoins,a0,b0,sp0       
    real(8), dimension(:,:,:),   allocatable             :: dphix,dphiy
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------
    ! Weno 2d discretization
    !-------------------------------------------------------------------------------
    allocate(dphix(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
    allocate(dphiy(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
    do j= sy,ey
       do i = sx,ex
          dphix(i,j,1)=(phi(i+1,j,1)-2.d0*phi(i,j,1)+phi(i-1,j,1))
          dphiy(i,j,1)=(phi(i,j+1,1)-2.d0*phi(i,j,1)+phi(i,j-1,1))
       end do
    end do

    do j= sy,ey
       do i = sx,ex
          a0=1.d0/12.d0*(-phi(i-1,j,1)+phi(i-2,j,1)+7.d0*(phi(i+1,j,1)-phi(i-1,j,1))-&
               phi(i+2,j,1)+phi(i+1,j,1))

          umoins=a0-weno5d(dphix(i-2,j,1),dphix(i-1,j,1),dphix(i,j,1),dphix(i+1,j,1))
          uplus =a0+weno5d(dphix(i+2,j,1),dphix(i+1,j,1),dphix(i,j,1),dphix(i-1,j,1))

          b0=1.d0/12.d0*(-phi(i,j-1,1)+phi(i,j-2,1)+7.d0*(phi(i,j+1,1)-phi(i,j-1,1))-&
               phi(i,j+2,1)+phi(i,j+1,1))

          vmoins=b0-weno5d(dphiy(i,j-2,1),dphiy(i,j-1,1),dphiy(i,j,1),dphiy(i,j+1,1))
          vplus =b0+weno5d(dphiy(i,j+2,1),dphiy(i,j+1,1),dphiy(i,j,1),dphiy(i,j-1,1))

          sp0=phi0(i,j,1)/sqrt(phi0(i,j,1)**2.d0+1) 
          if (sp0>=0.d0) then
             opd(i,j,1)=-sp0*(sqrt(max(-min(uplus,0.d0),max(umoins,0.d0))**2.d0+ &
                  max(-min(vplus,0.d0),max(vmoins,0.d0))**2.d0)-1.d0)

          else
             opd(i,j,1)=-sp0*(sqrt(max(-min(umoins,0.d0),max(uplus,0.d0))**2.d0+ &
                  max(-min(vmoins,0.d0),max(vplus,0.d0))**2.d0)-1.d0)
          endif
       end do
    end do
    deallocate(dphix,dphiy)
    !
    return
    !
  end subroutine redisweno_2d
  !*******************************************************************************

  !*******************************************************************************
  subroutine redisweno_3d(opd,phi,phi0)
    !*******************************************************************************
    !-------------------------------------------------------------------------------
    implicit none
    !-------------------------------------------------------------------------------
    !Global variabes
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:),   allocatable, intent(in) :: phi,phi0
    real(8), dimension(:,:,:),   allocatable, intent(out):: opd
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    integer                                              :: i,j,k
    real(8)                                              :: uplus,umoins,vplus,vmoins,wplus,wmoins,a0,b0,c0,sp0       
    real(8), dimension(:,:,:),   allocatable             :: dphix,dphiy,dphiz
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------
    ! Weno 3d discretization
    !-------------------------------------------------------------------------------
    allocate(dphix(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz),dphiy(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz), &
             dphiz(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
    do k= sz,ez
       do j = sy,ey
          do i = sx,ex
             dphix(i,j,k)=(phi(i+1,j,k)-2.d0*phi(i,j,k)+phi(i-1,j,k))
             dphiy(i,j,k)=(phi(i,j+1,k)-2.d0*phi(i,j,k)+phi(i,j-1,k))
             dphiz(i,j,k)=(phi(i,j,k+1)-2.d0*phi(i,j,k)+phi(i,j,k-1))
          end do
       end do
    end do

    do k= sz,ez
       do j = sy,ey
          do i = sx,ex
             a0=1.d0/12.d0*(phi(i-1,j,k)+phi(i-2,j,k)+7.d0*(phi(i+1,j,k)-phi(i-1,j,k))-&
                  phi(i+2,j,k)+phi(i+1,j,k))

             umoins=a0-weno5d(dphix(i-2,j,k),dphix(i-1,j,k),dphix(i,j,k),dphix(i+1,j,k))
             uplus=a0+weno5d(dphix(i+2,j,k),dphix(i+1,j,k),dphix(i,j,k),dphix (i-1,j,k))


             b0=1.d0/12.d0*(-phi(i,j-1,k)+phi(i,j-2,k)+7.d0*(phi(i,j+1,k)-phi(i,j-1,k))-&
                  phi(i,j+2,k)+phi(i,j+1,k))


             vmoins=b0-weno5d(dphiy(i,j-2,k),dphiy(i,j-1,k),dphiy(i,j,k),dphiy(i,j+1,k))
             vplus=b0-weno5d(dphiy(i,j+2,k),dphiy(i,j+1,k),dphiy(i,j,k),dphiy(i,j-1,k))


             c0=1.d0/12.d0*(phi(i,j,k-1)+phi(i,j,k-2)+7.d0*(phi(i,j,k+1)-phi(i,j,k-1))-&
                  phi(i,j,k+2)+phi(i,j,k+1))

             wmoins=c0-weno5d(dphiz(i,j,k-2),dphiz(i,j,k-1),dphiz(i,j,k),dphiz(i,j,k+1))
             wplus=c0+weno5d(dphiz(i,j,k+2),dphiz(i,j,k+1),dphiz(i,j,k),dphiz(i,j,k-1))


             sp0=phi0(i,j,k)/sqrt(phi0(i,j,k)**2+1.d0)

             if (sp0>=0.d0) then
                opd(i,j,k)=-sp0*(sqrt(&
                     max(min(uplus,0.d0),max(umoins,0.d0))**2.d0+ &
                     max(min(wplus,0.d0),max(wmoins,0.d0))**2.d0+ &
                     max(min(vplus,0.d0),max(vmoins,0.d0))**2.d0)-1.d0)
             else
                opd(i,j,k)=-sp0*(sqrt(&
                     max(min(umoins,0.d0),max(uplus,0.d0))**2.d0+ &
                     max(min(wmoins,0.d0),max(wplus,0.d0))**2.d0+ &
                     max(min(vmoins,0.d0),max(vplus,0.d0))**2.d0)-1.d0)
             endif
          end do
       end do
    end do
    deallocate(dphix,dphiy,dphiz)
    !
    return
    !
  end subroutine redisweno_3d
  !*******************************************************************************

  !*******************************************************************************
  real(8) function weno5d(a,b,c,d)
    !*******************************************************************************
    !-------------------------------------------------------------------------------
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables 
    !-------------------------------------------------------------------------------
    real(8),intent(in) :: a,b,c,d
    !-------------------------------------------------------------------------------
    !Local variables 
    !-------------------------------------------------------------------------------
    real(8) :: q0,q2
    real(8) :: is0,is1,is2
    real(8) :: alpha0,alpha1,alpha2,quo
    !-------------------------------------------------------------------------------
    is0=13.d0*(a-b)**2+3.d0*(a-3.d0*b)**2 +eps_weno
    is1=13.d0*(b-c)**2+3.d0*(b+c)**2      +eps_weno
    is2=13.d0*(c-d)**2+3.d0*(3.d0*c-d)**2 +eps_weno
    alpha0=is0*is0
    alpha1=is1*is1/6.d0
    alpha2=is2*is2/3.d0
    quo=alpha1*alpha2+alpha0*alpha2+alpha0*alpha1
    q0=alpha1*alpha2
    q2=alpha0*alpha1
    weno5d=q0*(a-2.d0*b+c)/3.d0+(q2-quo/2.d0)*(b-2.d0*c+d)/6.d0
    weno5d=weno5d/quo
    !
    return
    !
  end function weno5d
  !*********************************************************************************

  !******************************************************************************
  subroutine cfl_ls_2d3d(ut,vt,wt,dtcfl,dtrest,nbcfl)
    !****************************************************************************
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    integer, intent(out)                                 :: nbcfl
    !-------------------------------------------------------------------------------
    real(8), intent(out)                                 :: dtcfl,dtrest
    real(8), dimension(:,:,:),   allocatable, intent(in) :: ut,vt,wt
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    real(8)                                              :: umax,gumax
    real(8), parameter                                   :: epsc = 1e-20_8
    !-------------------------------------------------------------------------------

    dtrest=0
    nbcfl=1
    dtcfl=dt
    
    return

    if (dim==2) then
       umax=max( maxval(abs(ut)), maxval(abs(vt)) )
    else
       umax=max( maxval(abs(ut)), maxval(abs(vt)), maxval(abs(wt)) )
    end if
    !-------------------------------------------------------------------------------
    if (nproc>1) then
       call mpi_allreduce(umax,gumax,1,mpi_double_precision,mpi_max,comm3d,code)
       umax=gumax
    end if
    !-------------------------------------------------------------------------------
    
    !-------------------------------------------------------------------------------
    !Time step with cfl = 0.5
    !-------------------------------------------------------------------------------
    if (umax>epsc) then
       dtcfl = cfl%val / umax
    else
       dtcfl = dt
    end if
    !-------------------------------------------------------------------------------
    if (dtcfl >= dt) then
       dtcfl = dt
       nbcfl = 1
       dtrest = 0.d0
    else
       nbcfl = int(dt/dtcfl)
       dtcfl= dt/(nbcfl+1)
       dtrest=dt-nbcfl*dtcfl
    end if
    !-------------------------------------------------------------------------------
    return
  end subroutine cfl_ls_2d3d
  !******************************************************************************

  !*********************************************************************************
  subroutine transformation_2d3d (var,vart,ddx,dir)
    !*******************************************************************************
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)                                   :: dir
    real(8), dimension(:,:,:), allocatable, intent(in)    :: var
    real(8), dimension(:,:,:), allocatable, intent(inout) :: vart
    real(8), dimension(:), allocatable, intent(in)        :: ddx
    !-------------------------------------------------------------------------------
    integer                                               :: i,imax,imin
    !-------------------------------------------------------------------------------
    
    imin=lbound(var,dir)
    imax=ubound(var,dir)
    
    if (dir==1) then
       do i=imin,imax
          vart(i,:,:)=var(i,:,:)*ddx(i)
       end do
    elseif (dir==2) then
       do i=imin,imax
          vart(:,i,:)=var(:,i,:)*ddx(i)
       end do
    elseif (dir==3) then
       do i=imin,imax
          vart(:,:,i)=var(:,:,i)*ddx(i)
       end do
    end if

    return
  end subroutine transformation_2d3d
  !******************************************************************************
  
  !*******************************************************************************
  real(8) function weno5(phi_weno,up,um)
    !*******************************************************************************
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(-3:3), intent(in) :: phi_weno
    real(8), intent(in)                  :: up,um
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    real(8)                              :: v1,v2,v3,v4,v5,s1,s2,s3,a1,a2,a3,w1,w2,w3
    real(8)                              :: umoy
    !-------------------------------------------------------------------------------
    umoy=(abs(up)-abs(um))/(up-um+1e-40_8)
    if ((up>=0.d0).and.(um>=0.d0)) then
       v1=phi_weno(-2)-phi_weno(-3)
       v2=phi_weno(-1)-phi_weno(-2)
       v3=phi_weno(0 )-phi_weno(-1)
       v4=phi_weno(+1)-phi_weno(0 )
       v5=phi_weno(+2)-phi_weno(+1)
    else if ((up<=0.d0).and.(um<=0.d0)) then
       v1=phi_weno(+3)-phi_weno(+2)
       v2=phi_weno(+2)-phi_weno(+1)
       v3=phi_weno(+1)-phi_weno(0 )
       v4=phi_weno(0 )-phi_weno(-1)
       v5=phi_weno(-1)-phi_weno(-2)
    else if ((up.ge.0.d0).and.(um.le.0.d0)) then
       v1=0.d0
       v2=0.d0
       v3=0.d0
       v4=0.d0
       v5=0.d0
    else if ((up.le.0.d0).and.(um.ge.0.d0)) then
       if (umoy.gt.0.d0) then
          v1=phi_weno(-2)-phi_weno(-3)
          v2=phi_weno(-1)-phi_weno(-2)
          v3=phi_weno(0 )-phi_weno(-1)
          v4=phi_weno(+1)-phi_weno(0 )
          v5=phi_weno(+2)-phi_weno(+1)
       else
          v1=phi_weno(+3)-phi_weno(+2)
          v2=phi_weno(+2)-phi_weno(+1)
          v3=phi_weno(+1)-phi_weno(0 )
          v4=phi_weno(0 )-phi_weno(-1)
          v5=phi_weno(-1)-phi_weno(-2)
       endif
    endif
    s1=13.d0/12.d0*(v1-2.d0*v2+v3)**2+1.d0/4.d0*(v1-4.d0*v2+3.d0*v3)**2
    s2=13.d0/12.d0*(v2-2.d0*v3+v4)**2+1.d0/4.d0*(v2-v4)**2
    s3=13.d0/12.d0*(v3-2.d0*v4+v5)**2+1.d0/4.d0*(3.d0*v3-4.d0*v4+v5)**2
    a1=1.d0/10.d0*1.d0/(eps_weno+s1)**2 !!!!1 ou 2
    a2=6.d0/10.d0*1.d0/(eps_weno+s2)**2
    a3=3.d0/10.d0*1.d0/(eps_weno+s3)**2
    w1=a1/(a1+a2+a3+1e-40_8)
    w2=a2/(a1+a2+a3+1e-40_8)
    w3=a3/(a1+a2+a3+1e-40_8)

    weno5=w1*(v1/3.d0-7.d0*v2/6.d0+11.d0*v3/6.d0) &
         & +w2*(-v2/6.d0+5.d0*v3/6.d0+v4/3.d0)    &
         & +w3*(v3/3.d0+5.d0*v4/6.d0-v5/6.d0)
  end function weno5
  !*************************************************************************************
  real(8) function weno5_shala(a,b,c,d,e)
    !*******************************************************************************
    use mod_Constants
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    real(8), intent(in) :: a,b,c,d,e
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    real(8)             :: q1,q2,q3
    real(8)             :: is1,is2,is3
    real(8)             :: alpha1,alpha2,alpha3
    !-------------------------------------------------------------------------------
    
    q1 = a * d1p3  + b * d7p6m + c * d11p6
    q2 = b * d1p6m + c * d5p6  + d * d1p3
    q3 = c * d1p3  + d * d5p6  + e * d1p6m

#if 1
    is1 = c13*( a - two*b + c )**2 + c3*( a - c4*b + c3*c )**2  + eps_weno
    is2 = c13*( b - two*c + d )**2 + c3*( d - b )**2            + eps_weno
    is3 = c13*( c - two*d + e )**2 + c3*( c3*c - c4*d + e )**2  + eps_weno
    
    is1 = is1**2
    is2 = is2**2
    is3 = is3**2

    alpha1 = is2*is3*d1p18
    alpha2 = is1*is3*d1p3
    alpha3 = is1*is2*d1p6

    weno5_shala = alpha1 * q1 + alpha2 * q2 + alpha3 * q3
    weno5_shala = weno5_shala / ( alpha1 + alpha2 + alpha3 )
#else
    is1 = c13/12*( a - two*b + c )**2 + c3/12*( a - c4*b + c3*c )**2  + eps_weno
    is2 = c13/12*( b - two*c + d )**2 + c3/12*( d - b )**2            + eps_weno
    is3 = c13/12*( c - two*d + e )**2 + c3/12*( c3*c - c4*d + e )**2  + eps_weno
    
    alpha1 = d1p10/(is1+eps_weno)**2
    alpha2 = d6p10/(is2+eps_weno)**2
    alpha3 = d3p10/(is3+eps_weno)**2

    weno5_shala = alpha1 * q1 + alpha2 * q2 + alpha3 * q3
    weno5_shala = weno5_shala / ( alpha1 + alpha2 + alpha3 )
#endif
    
    return
    
  end function weno5_shala
  !*************************************************************************************

  !*************************************************************************************
  subroutine periodicity_2d(phi)
  !*************************************************************************************
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:),   allocatable, intent(inout) :: phi
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    integer                                                 :: i,j
    !-------------------------------------------------------------------------------
    do j = sy,ey
       phi(sx-3:sx-1,j,1)=phi(ex-3:ex-1,j,1)
       phi(ex+1:ex+3,j,1)=phi(sx+1:sx+3,j,1) 
    end do

    do i = sx,ex
       phi(i,sy-3:sy-1,1)=phi(i,ey-3:ey-1,1)
       phi(i,ey+1:ey+3,1)=phi(i,sy+1:sy+3,1)
    end do
    
  end subroutine periodicity_2d
  !*************************************************************************************

  !*************************************************************************************
  subroutine symmetry_2d(phi)
  !*************************************************************************************
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:),   allocatable, intent(inout) :: phi
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    integer                                                 :: i,j
    !-------------------------------------------------------------------------------
    do i = 1,3
       phi(sx-i,:,:)=phi(sx+i,:,:)
       phi(ex+i,:,:)=phi(ex-i,:,:)
    end do

    do j = 1,3
       phi(:,sy-j,:)=phi(:,sy+j,:)
       phi(:,ey+j,:)=phi(:,ey-j,:)
    end do
    return

  end subroutine symmetry_2d
  !*************************************************************************************

  !*************************************************************************************
  subroutine symmetry_3d(phi)
    !*************************************************************************************
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:),   allocatable, intent(inout) :: phi
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    integer                                                 :: i,j,k
    !-------------------------------------------------------------------------------
    do i = 1,3
       phi(sx-i,:,:)=phi(sx+i,:,:)
       phi(ex+i,:,:)=phi(ex-i,:,:)
    end do

    do j = 1,3
       phi(:,sy-j,:)=phi(:,sy+j,:)
       phi(:,ey+j,:)=phi(:,ey-j,:)
    end do

    do k = 1,3
       phi(:,:,sz-k)=phi(:,:,sz+k)
       phi(:,:,ez+k)=phi(:,:,ez-k)
    end do
    return

  end subroutine symmetry_3d
  !*************************************************************************************

  
  !*************************************************************************************
  subroutine periodicity_3d(phi)
  !*************************************************************************************
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:),   allocatable, intent(inout) :: phi
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    integer                                                 :: i,j,k
    !-------------------------------------------------------------------------------
    do j = sy,ey
       do i = sx,ex
          phi(i,j,sz-3:sz-1)=phi(i,j,ez-2:ez)
          phi(i,j,ez+1:ez+3)=phi(i,j,sz:sz+2)  
       end do
    end do
    do k = sz,ez
       do j = sy,ey
          phi(sx-3:sx-1,j,k)=phi(ex-2:ex,j,k)
          phi(ex+1:ex+3,j,k)=phi(sx:sx+2,j,k) 
       end do
    end do
    do k = sz,ez
       do i = sx,ex
          phi(i,sy-3:sy-1,k)=phi(i,ey-2:ey,k)
          phi(i,ey+1:ey+3,k)=phi(i,sy:sy+2,k)  
       end do
    end do
    return

  end subroutine periodicity_3d
  !*************************************************************************************


!!$#if 0
!!$  subroutine erreur_2d(phi,u,v,nt)
!!$    use m_Initialisation_CL
!!$    !*******************************************************************************!
!!$    implicit none
!!$    !-------------------------------------------------------------------------------
!!$    !Global variables
!!$    !-------------------------------------------------------------------------------
!!$    real(8),  dimension(sx-3:ex+3,sy-3:ey+3,1),intent(in)    :: phi
!!$    real(8),  dimension(sx-3:ex+3,sy-3:ey+3,1),intent(in)    :: u,v
!!$    integer,                                            intent(in)    :: nt
!!$
!!$    real(8),  dimension(:,:,:),allocatable    :: phi0
!!$    real(8),  dimension(:),allocatable    ::M
!!$    !-------------------------------------------------------------------------------
!!$    !Local variables
!!$    !-------------------------------------------------------------------------------
!!$    real(8)::erreur,erreur_L2,erreur_L1,erreur_Linfty
!!$    integer::i,j,cpt
!!$    real(8)::rayon,rayon_2
!!$    real(8),dimension(2)::centre_cercle
!!$
!!$    !-------------------------------------------------------------------------------
!!$
!!$    select case(trim(cas))
!!$    case("plan")
!!$       erreur=0 
!!$       do i=sx,ex
!!$          do j=sy,ey
!!$             erreur= erreur+ ((j*dy-(ymax-ymin)/2.d0)-(v(i,j+1,1)+v(i,j,1))/2.d0 *nt*dt - phi(i,j,1) )**2.d0
!!$          end do
!!$       end do
!!$       erreur=erreur/(nx+1)/(ny+1)
!!$       erreur=sqrt(erreur)
!!$       write(*,*) erreur,nt*dt
!!$    case("sinus")
!!$       erreur=0 
!!$       do i=sx,ex
!!$          do j=sy,ey
!!$             erreur=erreur+ (sin(j*dy-(ymax-ymin)/2.d0 -(v(i,j+1,1)+v(i,j,1))/2.d0 *nt*dt) - phi(i,j,1) )**2.d0
!!$          end do
!!$       end do
!!$       erreur=erreur/(nx+1)/(ny+1)
!!$       erreur=sqrt(erreur)
!!$       write(*,*) erreur,nt*dt
!!$    case("circle_pm_1")
!!$       allocate(M(2)) 
!!$       erreur=0 
!!$       do i=sx,ex
!!$          do j=sy,ey
!!$             M(1)=i*dx+xmin ;  M(2)=j*dy+ymin
!!$             erreur=erreur+((rayon - sqrt((M(1) - centre_cercle(1))**2 + (M(2) - centre_cercle(2))**2)) -phi(i,j,1) )**2
!!$          end do
!!$       end do
!!$       erreur=erreur/(nx+1)/(ny+1)
!!$       erreur=sqrt(erreur)
!!$       write(*,*) erreur,nt*dt
!!$    case("zalesak")
!!$       centre_cercle(1)=50+xmin
!!$       centre_cercle(2)=75+ymin
!!$       rayon=15
!!$       allocate(phi0(sx-3:ex+3,sy-3:ey+3,1))
!!$       call Initialisation_CL_2d(phi0)
!!$
!!$       allocate(M(2))
!!$       erreur_L2=0 ; erreur_L1=0 ; erreur_Linfty=0 ; cpt=0
!!$       do i=sx,ex
!!$          do j=sy,ey
!!$             M(1)=i*dx+xmin ;  M(2)=j*dy+ymin
!!$             rayon_2=sqrt((M(1) - centre_cercle(1))**2 + (M(2) - centre_cercle(2))**2)
!!$             !if (abs(phi0(i,j,1)<2) then
!!$             if (rayon_2<=20) then
!!$                erreur_L2=erreur_L2+(phi(i,j,1)-phi0(i,j,1))**2
!!$                erreur_L1=erreur_L1+abs(phi(i,j,1)-phi0(i,j,1))
!!$                erreur_Linfty=max(erreur_Linfty,abs(phi(i,j,1)-phi0(i,j,1)))
!!$                cpt=cpt+1
!!$                cpt=(nx+1)*(ny+1)
!!$             end if
!!$          end do
!!$       end do
!!$       erreur_L2=erreur_L2/cpt ; erreur_L1=erreur_L1/cpt
!!$       erreur_L2=sqrt(erreur_L2)
!!$       write(*,'("Normes : L2= ",1pe8.1," L1= ",1pe8.1," Linfty= ",1pe8.1)')erreur_L2,erreur_L1,erreur_Linfty
!!$    end select
!!$
!!$  end subroutine erreur_2d
!!$!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!$
!!$  subroutine erreur_heaviside_2D(phi)
!!$    use m_Initialisation_CL
!!$    implicit none
!!$    !-------------------------------------------------------------------------------
!!$    !Global variables
!!$    !-------------------------------------------------------------------------------
!!$    real(8),  dimension(sx-3:ex+3,sy-3:ey+3,1),intent(in)    :: phi
!!$    !-------------------------------------------------------------------------------
!!$    !Local variables
!!$    !-------------------------------------------------------------------------------
!!$    real(8),  dimension(:,:,:),allocatable    :: phi0,Heaviside_reg,Heaviside0_reg
!!$    real(8),  dimension(:),allocatable        :: M
!!$    !-------------------------------------------------------------------------------
!!$    real(8)::erreur_L2,erreur_L1,erreur_Linfty,erreur_heaviside
!!$    integer::i,j,cpt
!!$    real(8)::rayon,rayon_2,L
!!$    real(8),dimension(2)::centre_cercle
!!$    !-------------------------------------------------------------------------------
!!$    select case(trim(cas))
!!$    case("serpentin")
!!$       centre_cercle(1)=0.50+xmin
!!$       centre_cercle(2)=0.75+ymin
!!$       rayon=0.15
!!$
!!$       L=2*pi*rayon
!!$       allocate(phi0(sx-3:ex+3,sy-3:ey+3,1),Heaviside_reg(sx-3:ex+3,sy-3:ey+3,1),Heaviside0_reg(sx-3:ex+3,sy-3:ey+3,1))
!!$       call Initialisation_CL_2d(phi0)
!!$       call Heaviside_reg_2d(phi,Heaviside_reg)
!!$       call Heaviside_reg_2d(phi0,Heaviside0_reg)
!!$       allocate(M(2))
!!$       erreur_L1=0 ; erreur_L2=0 ; erreur_Heaviside=0 ; erreur_Linfty=0 ; cpt=0
!!$       do i=sx,ex
!!$          do j=sy,ey
!!$             M(1)=i*dx+xmin ;  M(2)=j*dy+ymin
!!$             rayon_2=sqrt((M(1) - centre_cercle(1))**2 + (M(2) - centre_cercle(2))**2)
!!$             !if (rayon_2<=0.20) then
!!$             erreur_L1=erreur_L1+abs(phi(i,j,1)-phi0(i,j,1))
!!$             erreur_L2=erreur_L2+(phi(i,j,1)-phi0(i,j,1))**2
!!$             erreur_Heaviside=(erreur_L1+abs(Heaviside0_reg(i,j,1)-Heaviside_reg(i,j,1)))
!!$             erreur_Linfty=max(erreur_Linfty,abs(phi(i,j,1)-phi0(i,j,1)))
!!$             !cpt=cpt+1
!!$             !end if
!!$          end do
!!$       end do
!!$       erreur_Heaviside=(erreur_L1*dx*dy)/L
!!$       erreur_L2=erreur_L2/(nx+1)/(ny+1) ; erreur_L1=erreur_L1/(nx+1)/(ny+1)
!!$       !erreur_L2=erreur_L2/cpt ; erreur_L1=erreur_L1/cpt
!!$       erreur_L2=sqrt(erreur_L2)
!!$       write(*,'("Normes : L2= ",1pe8.1," L1= ",1pe8.1," Linfty= ",1pe8.1," LHeaviside= ",1pe8.1)')erreur_L2,erreur_L1,erreur_Linfty,erreur_Heaviside
!!$    end select
!!$
!!$  end subroutine erreur_heaviside_2D
!!$
!!$!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  !*************************************************************************************
  subroutine Heaviside_reg_2d(phi,Heaviside_reg)
  !*************************************************************************************
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:),   allocatable, intent(in) :: phi
    real(8), dimension(:,:,:),   allocatable, intent(out):: Heaviside_reg
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    integer                                              :: i,j
    real(8)                                              :: epsilon_reg
    !-------------------------------------------------------------------------------
    epsilon_reg=1.5*min(minval(dx),minval(dy),minval(dz))
    do j =sy,ey
       do i=sx,ex
          if (phi(i,j,1)<-epsilon_reg) then 
             Heaviside_reg(i,j,1)=0
          else if (abs(phi(i,j,1))<=epsilon_reg) then 
             Heaviside_reg(i,j,1)=0.5*(1+phi(i,j,1)/epsilon_reg+(1/pi)*sin((pi*phi(i,j,1))/epsilon_reg))
          else 
             Heaviside_reg(i,j,1)=1
          end if
       end do
    end do
    return

  end subroutine Heaviside_reg_2d
  !*************************************************************************************

  !*************************************************************************************
  subroutine Heaviside_2d(phi,Heaviside)
  !*************************************************************************************
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:), allocatable, intent(in)  :: phi
    real(8), dimension(:,:,:), allocatable, intent(out) :: Heaviside
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    integer                                             :: i,j
    !-------------------------------------------------------------------------------
    do j =sy,ey
       do i=sx,ex
          if (phi(i,j,1)<0) then 
             Heaviside(i,j,1)=0
          else if (phi(i,j,1)==0) then 
             Heaviside(i,j,1)=0.5
          else 
             Heaviside(i,j,1)=1
          end if
       end do
    end do
    return

  end subroutine Heaviside_2d
  !*************************************************************************************

!!$  subroutine conservation_volume_2d(phi,nt)
!!$    use m_Initialisation_CL
!!$    !*******************************************************************************
!!$    implicit none
!!$    !-------------------------------------------------------------------------------
!!$    real(8),dimension(sx-3:ex+3,sy-3:ey+3,1),intent(in):: phi
!!$    integer,                                 intent(in):: nt
!!$
!!$    !-------------------------------------------------------------------------------
!!$    real(8),  dimension(:,:,:),allocatable    :: phi0,Heaviside_reg,Heaviside0_reg
!!$
!!$    integer::i,j
!!$    real(8)::volume_exact,volume_calc,erreur_volume
!!$   
!!$    Heaviside_reg=0.d0 ; Heaviside0_reg=0.d0
!!$
!!$    allocate(phi0(sx-3:ex+3,sy-3:ey+3,1),Heaviside_reg(sx-3:ex+3,sy-3:ey+3,1),Heaviside0_reg(sx-3:ex+3,sy-3:ey+3,1))
!!$    call Initialisation_CL_2d(phi0)
!!$    call Heaviside_reg_2d(phi,Heaviside_reg)
!!$    call Heaviside_reg_2d(phi0,Heaviside0_reg)
!!$    volume_calc=0 ; volume_exact=0 ; erreur_volume=0
!!$    do j=sy,ey
!!$       do i=sx,ex
!!$          volume_calc=volume_calc+(Heaviside_reg(i,j,1))
!!$          volume_exact=volume_exact+(Heaviside0_reg(i,j,1))
!!$       end do
!!$    end do
!!$    volume_calc=volume_calc*dx*dy
!!$    volume_exact=volume_exact*dx*dy
!!$    erreur_volume=abs(volume_exact-volume_calc)/volume_exact
!!$    !write(*,*) volume_calc,volume_exact
!!$    !write(*,'("Normes : L1= ",1pe8.1)')erreur_volume*100
!!$    write(16,*) nt*dt,volume_calc
!!$
!!$  end subroutine conservation_volume_2d
!!$#endif 

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
end module mod_LevelSet
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
