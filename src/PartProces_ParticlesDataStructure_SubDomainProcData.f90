!===============================================================================
module Bib_VOFLag_ParticlesDataStructure_SubDomainProcData
  !===============================================================================
  contains
  !-----------------------------------------------------------------------------  
  !> @author jorge cesar brandle de motta, amine chadil
  ! 
  !> @brief cette routine donne le status de chaque particle par rapport au domaine
  !! du proc courant:
  !> 1. IN : le centre de la particule est a l'interieur du domaine du proc courant
  !! 2. ON : la particule intersecte le domaine du proc courant
  !! 3. AR : la particule est dans le domaine d'un proc voisin su proc courant
  !
  !> @todo 1. voir comment les voisins sont calculer afin d'optimiser la verification des
  !! on.
  !
  !> @param[out] vl   : la structure contenant toutes les informations
  !! relatives aux particules   
  !-----------------------------------------------------------------------------
  subroutine ParticlesDataStructure_SubDomainProcData(vl)
    !===============================================================================
    !modules
    !===============================================================================
    !-------------------------------------------------------------------------------
    !Modules de definition des structures et variables => src/mod/module_...f90
    !-------------------------------------------------------------------------------
    use Module_VOFLag_ParticlesDataStructure
    use Module_VOFLag_SubDomain_Data,             only : nb_voisins,rank_voisins,s_domaines_int
    use mod_Parameters,                           only : rank,nproc,dim,deeptracking
    use mod_mpi
    !-------------------------------------------------------------------------------
    !Modules de subroutines de la librairie RESPECT => src/Bib_VOFLag_...f90
    !-------------------------------------------------------------------------------
    use Bib_VOFLag_Particle_LimitedSubDomain_On
    use Bib_VOFLag_SubDomain_Limited_PointIn
    !-------------------------------------------------------------------------------

    !===============================================================================
    !declarations des variables
    !===============================================================================
    implicit none
    !-------------------------------------------------------------------------------
    !variables globales
    !-------------------------------------------------------------------------------
    type(struct_vof_lag), intent(inout)  :: vl
    !-------------------------------------------------------------------------------
    !variables locales 
    !-------------------------------------------------------------------------------
    integer                              :: np,nd,nbt,nvois, vals
    logical                              :: point_in_domain_int,on_ext
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'entree ParticlesDataStructure_SubDomainProcData'
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    !tous les procs traiteront toutes les particules
    !-------------------------------------------------------------------------------
    do np=1,vl%kpt       
      !-------------------------------------------------------------------------------
      !actualisation du status de la particule np par rapport au processeur courant
      !-------------------------------------------------------------------------------
      if (allocated(vl%objet(np)%symperon)) deallocate(vl%objet(np)%symperon)
      allocate(vl%objet(np)%symperon(27))
      call SubDomain_Limited_PointIn(vl%objet(np)%pos,vl%objet(np)%in)
      vl%objet(np)%on=vl%objet(np)%in
      vl%objet(np)%symperon(1)=vl%objet(np)%on! la copie numero 1 est la particule elle meme
      vl%objet(np)%ar=vl%objet(np)%on

      !-------------------------------------------------------------------------------
      !donne le rang du proc qui contient le centre de la particule np
      !-------------------------------------------------------------------------------
      vl%objet(np)%locpart = -1
      if (vl%objet(np)%in) then
        vl%objet(np)%locpart = rank
      end if
      vals = vl%objet(np)%locpart
      if (nproc>1) call mpi_allreduce(vals,vl%objet(np)%locpart,1,mpi_integer,mpi_max,comm3d,code)
      if (vl%objet(np)%locpart .eq. -1 ) then
        write(6,*) "warning : la part ",np," est sortie"
      end if

      !-------------------------------------------------------------------------------
      !construction des copies de la particule np en fonction des periodicites
      !-------------------------------------------------------------------------------
      if (allocated(vl%objet(np)%symper)) deallocate(vl%objet(np)%symper)
      allocate(vl%objet(np)%symper(27,3))
      vl%objet(np)%symper=0.D0
      if (vl%objet(np)%in) then
        vl%objet(np)%nbtimes = rank_nbtimes
        do nd=1,dim
           do nbt=1,vl%objet(np)%nbtimes
              vl%objet(np)%symper(nbt,nd)  = vl%objet(np)%pos(nd) + rank_symper(nbt,nd) 
           end do
        end do
      end if
      call mpi_bcast(vl%objet(np)%nbtimes,1,mpi_integer,vl%objet(np)%locpart,comm3d,code)
      do nbt=1,vl%objet(np)%nbtimes
        do nd=1,dim
          call mpi_bcast(vl%objet(np)%symper(nbt,nd),1,mpi_double_precision,vl%objet(np)%locpart,comm3d,code)
        end do
      end do

      !-------------------------------------------------------------------------------
      !Mettre a jour le status on et ar de la particle ainsi que le status on de ses copies
      !-------------------------------------------------------------------------------
      do nbt=1,vl%objet(np)%nbtimes
        call Particle_LimitedSubDomain_On(vl,np,nbt,on_ext)
        if ( on_ext ) then
          vl%objet(np)%on = .true.
          vl%objet(np)%symperon(nbt) = .true.
        else
          vl%objet(np)%symperon(nbt) = .false.
        end if
      end do
      vl%objet(np)%ar = vl%objet(np)%on
      if (.not. vl%objet(np)%on) then
        do nvois=1,nb_voisins
          if ( rank_voisins(nvois,1) .eq. vl%objet(np)%locpart ) vl%objet(np)%ar = .true.
        end do
      end if

    end do

    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'sortie ParticlesDataStructure_SubDomainProcData'
    !-------------------------------------------------------------------------------
  end subroutine ParticlesDataStructure_SubDomainProcData

  !===============================================================================
end module Bib_VOFLag_ParticlesDataStructure_SubDomainProcData
!===============================================================================