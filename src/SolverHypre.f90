!========================================================================
!**
!**   NAME       : SolverHypre.f90
!**
!**   AUTHOR     : Stéphane Vincent
!**              : Mohamed Elouafa
!**              : B. Trouette
!**
!**   FUNCTION   : iterative and direct solver modules of FUGU software
!**
!**   DATES      : Version 1.0.0  : from : May, 2022
!**
!========================================================================

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#include "precomp.h"
module mod_solver_hypre
  use mod_hypre_dep
  use mod_solver
  use mod_solver_comm_mpi
  use mod_timers
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

contains

#if HYPRE

  subroutine Stencil_HYPRE_A(graph,grid,lsx,lex,lsy,ley,lsz,lez)
    use mod_parameters, only: dim
    use mod_mpi     
    implicit none
    include 'HYPREf.h'

    integer ,   intent(in)             :: lsx,lex,lsy,ley,lsz,lez
    integer*8 , intent(out)            :: graph,grid
    integer, dimension(:), allocatable :: periodic_hypre
    integer*8  stencil
    integer    nvars,ierr
    integer    ls
    integer    var
    integer    part
    integer    ilower(dim),iupper(dim)
    integer    offsets(dim,2*dim+1)
    integer    ent,lvs
    integer    imin,imax
    integer    nentries, nvalues
    integer    xvar(2*dim+1)
    integer    HYPRE_SSTRUCT_VARIABLE_CELL
    parameter( HYPRE_SSTRUCT_VARIABLE_CELL = 0 )
    integer    HYPRE_SSTRUCT_VARIABLE_XFACE
    parameter( HYPRE_SSTRUCT_VARIABLE_XFACE = 2)
    integer    HYPRE_SSTRUCT_VARIABLE_YFACE
    parameter( HYPRE_SSTRUCT_VARIABLE_YFACE = 3 )

    nvars  = 1
    Part   = 0
    
    var=0
    call HYPRE_SStructGridCreate(comm3d,dim,npart_hypre,grid,ierr)
    
    ilower(1) = lsx
    ilower(2) = lsy
    iupper(1) = lex
    iupper(2) = ley
    if (dim==3) then
       ilower(3) = lsz
       iupper(3) = lez
    endif

    !-----------------------------------------------------------------------------------
    ! periodicity
    !-----------------------------------------------------------------------------------
    allocate(periodic_hypre(dim))
    periodic_hypre=0
    !-----------------------------------------------------------------------------------
    ! x-direction 
    !-----------------------------------------------------------------------------------
    call MPI_ALLREDUCE(lsx,imin,1,MPI_INTEGER,MPI_MIN,COMM3D,MPI_CODE)
    call MPI_ALLREDUCE(lex,imax,1,MPI_INTEGER,MPI_MAX,COMM3D,MPI_CODE)
    if (Periodic(1)) periodic_hypre(1)=imax-imin+1
    !-----------------------------------------------------------------------------------
    ! y-direction 
    !-----------------------------------------------------------------------------------
    call MPI_ALLREDUCE(lsy,imin,1,MPI_INTEGER,MPI_MIN,COMM3D,MPI_CODE)
    call MPI_ALLREDUCE(ley,imax,1,MPI_INTEGER,MPI_MAX,COMM3D,MPI_CODE)
    if (Periodic(2)) periodic_hypre(2)=imax-imin+1
    !-----------------------------------------------------------------------------------
    ! z-direction 
    !-----------------------------------------------------------------------------------
    if (dim==3) then
       call MPI_ALLREDUCE(lsz,imin,1,MPI_INTEGER,MPI_MIN,COMM3D,MPI_CODE)
       call MPI_ALLREDUCE(lez,imax,1,MPI_INTEGER,MPI_MAX,COMM3D,MPI_CODE)
       if (Periodic(3)) periodic_hypre(3)=imax-imin+1
    end if
    !-----------------------------------------------------------------------------------
    
    call HYPRE_SStructGridSetExtents(grid,part,ilower,iupper,ierr)
    call HYPRE_SStructGridSetVariables(grid,part,nvars,HYPRE_SSTRUCT_VARIABLE_CELL,ierr)
    
    call HYPRE_SStructGridSetPeriodic(grid,PART,periodic_hypre,ierr)
    call HYPRE_SStructGridAssemble(grid,ierr)
    call HYPRE_SStructStencilCreate(dim,dim*2+1,stencil,ierr)
    
    if (dim==2) then
       offsets(1,1) =  0
       offsets(2,1) =  0
       offsets(1,2) = -1
       offsets(2,2) =  0
       offsets(1,3) =  1
       offsets(2,3) =  0
       offsets(1,4) =  0
       offsets(2,4) = -1
       offsets(1,5) =  0
       offsets(2,5) =  1
    else
       offsets(1,1) =  0
       offsets(2,1) =  0
       offsets(3,1) =  0
       offsets(1,2) = -1
       offsets(2,2) =  0
       offsets(3,2) =  0
       offsets(1,3) =  1
       offsets(2,3) =  0
       offsets(3,3) =  0
       offsets(1,4) =  0
       offsets(2,4) = -1
       offsets(3,4) =  0
       offsets(1,5) =  0
       offsets(2,5) =  1
       offsets(3,5) =  0
       offsets(1,6) =  0
       offsets(2,6) =  0
       offsets(3,6) = -1
       offsets(1,7) =  0
       offsets(2,7) =  0
       offsets(3,7) =  1
    endif
    
    xvar=0
    do ent=1,2*dim+1
       call HYPRE_SStructStencilSetEntry(stencil,ent-1,offsets(1:dim,ent),xvar(ent),ierr)
    enddo
    
    call HYPRE_SStructGraphCreate(comm3d,grid,graph,ierr)
    call HYPRE_SStructGraphSetObjectType(graph,HYPRE_STRUCT,ierr)
    call HYPRE_SStructGraphSetStencil(graph,part,var,stencil,ierr)
    call HYPRE_SStructGraphAssemble(graph,ierr)
    call HYPRE_SStructStencilDestroy(stencil,ierr)
    
  end subroutine Stencil_HYPRE_A
  
  subroutine Init_HYPRE_Vect(StructY,StructX,grid)
    use mod_mpi    
    implicit none
    include 'HYPREf.h'
    
    integer*8, intent(in)  :: grid
    integer*8, intent(out) :: StructY,StructX  
    integer                :: ierr
    integer*8              :: x,y
    
    call HYPRE_SStructVectorCreate(comm3d,grid,y,ierr)
    call HYPRE_SStructVectorCreate(comm3d,grid,x,ierr)

    call HYPRE_SStructVectorSetObjectTyp(x,HYPRE_STRUCT,ierr)
    call HYPRE_SStructVectorSetObjectTyp(y,HYPRE_STRUCT,ierr)

    call HYPRE_SStructVectorInitialize(y,ierr)
    call HYPRE_SStructVectorInitialize(x,ierr)

    call HYPRE_SStructVectorGetObject(y,StructY,ierr)
    call HYPRE_SStructVectorGetObject(x,StructX,ierr)
    
  end subroutine Init_HYPRE_Vect

  subroutine Prepa_HYPRE_Auvwp(coef,MatA,graph,dir)
    use mod_Parameters, only: dim,nb_Vx,nb_Vy, &
         & sx,ex,sy,ey,sz,ez,                  &
         & sxu,exu,syu,eyu,szu,ezu,            &
         & sxv,exv,syv,eyv,szv,ezv,            &
         & sxw,exw,syw,eyw,szw,ezw,            &
         & sxs,exs,sys,eys,szs,ezs,            &
         & sxus,exus,syus,eyus,szus,ezus,      &
         & sxvs,exvs,syvs,eyvs,szvs,ezvs,      &
         & sxws,exws,syws,eyws,szws,ezws,      &
         & nb_vx_no_ghost,nb_vy_no_ghost,      &
         & nb_vz_no_ghost,nb_p_no_ghost,       &
         & NS_Neumann_p
    use mod_mpi
    implicit none
    include 'HYPREf.h'

    integer, intent(in)                            :: dir
    real(8), dimension(:), allocatable, intent(in) :: coef    
    integer*8, intent(out)                         :: MatA
    integer*8, intent(in)                          :: graph
    integer    nb_no_ghost,nb_jump,nb_coeff
    integer    lsx,lex,lsy,ley,lsz,lez
    integer    lsxs,lexs,lsys,leys,lszs,lezs
    integer    nvars
    integer    ls,i,j,k,l,ierr,z,m
    integer    var
    integer    part
    integer    ilower(dim),iupper(dim)
    integer    ent,lvs
    integer    nentries,nvalues,stencil_indices(2*dim+1)
    integer    matrix_case
    real(8), dimension(:), allocatable :: values

    call compute_time(TIMER_START,"[sub] Prepa_HYPRE_Auvwp")
    
    select case(dir)
    case(0)
       nb_jump=0
       nb_coeff=5+2*(dim-2)
       nb_no_ghost=nb_p_no_ghost
       lsx=sx;lex=ex
       lsy=sy;ley=ey
       lsz=sz;lez=ez
       lsxs=sxs;lexs=exs
       lsys=sys;leys=eys
       lszs=szs;lezs=ezs
    case(1)
       nb_jump=0
       nb_coeff=11+6*(dim-2)
       nb_no_ghost=nb_vx_no_ghost
       lsx=sxu;lex=exu
       lsy=syu;ley=eyu
       lsz=szu;lez=ezu
       lsxs=sxus;lexs=exus
       lsys=syus;leys=eyus
       lszs=szus;lezs=ezus
    case(2)
       nb_jump=nb_Vx
       nb_coeff=11+6*(dim-2)
       nb_no_ghost=nb_vy_no_ghost
       lsx=sxv;lex=exv
       lsy=syv;ley=eyv
       lsz=szv;lez=ezv
       lsxs=sxvs;lexs=exvs
       lsys=syvs;leys=eyvs
       lszs=szvs;lezs=ezvs
    case(3)
       nb_jump=nb_Vx+nb_Vy
       nb_coeff=11+6*(dim-2)
       nb_no_ghost=nb_vz_no_ghost
       lsx=sxw;lex=exw
       lsy=syw;ley=eyw
       lsz=szw;lez=ezw
       lsxs=sxws;lexs=exws
       lsys=syws;leys=eyws
       lszs=szws;lezs=ezws
    end select

    allocate(values((2*dim+1)*nb_no_ghost))
    values=0
    Part = 0
    var  = 0

    ilower(1) = lsx
    ilower(2) = lsy
    iupper(1) = lex
    iupper(2) = ley
    if (dim==3) then 
       ilower(3) = lsz
       iupper(3) = lez
    endif

    call HYPRE_SStructMatrixCreate(comm3d,graph,MatA,ierr)
    call HYPRE_SStructMatrixSetObjectTyp(MatA,HYPRE_STRUCT,ierr)
    call HYPRE_SStructMatrixInitialize(MatA,ierr)

    do i = 1,2*dim+1
       stencil_indices(i) = i-1
    enddo
    nentries = 2*dim+1
    
    nvalues=nentries*nb_no_ghost

    matrix_case=1
    if (NS_Neumann_p==0) then
       if (dir==0) then
          matrix_case=0
       end if
    end if
    
    select case(matrix_case)
    case(0)
       if (dim==2) then
          lvs=1+nb_coeff*nb_jump
          l=1
          do j=lsys,leys
             do i=lsxs,lexs

                if (nproc>1.and.(i==lsxs.or.i==lexs.or.j==lsys.or.j==leys)) then
                else 
                   values(l)   = coef(lvs) 
                   values(l+1) = coef(lvs+1)
                   values(l+2) = coef(lvs+2)
                   values(l+3) = coef(lvs+3)
                   values(l+4) = coef(lvs+4)
                   if      (i==gsx .and. j==gsy) then
                      if      (     periodic(1) .and.      periodic(2)) then
                      else if (.not.periodic(1) .and.      periodic(2)) then
                         values(l+1) = 0
                         values(l+2) = 2*coef(lvs+2)
                      else if (     periodic(1) .and. .not.periodic(2)) then
                         values(l+3) = 0
                         values(l+4) = 2*coef(lvs+4)
                      else if (.not.periodic(1) .and. .not.periodic(2)) then
                         values(l+1) = 0
                         values(l+2) = 2*coef(lvs+2)
                         values(l+3) = 0
                         values(l+4) = 2*coef(lvs+4)
                      end if
                   else if (i==gsx .and. j==gey) then
                      if      (     periodic(1) .and.      periodic(2)) then
                      else if (.not.periodic(1) .and.      periodic(2)) then
                         values(l+1) = 0
                         values(l+2) = 2*coef(lvs+2)
                      else if (     periodic(1) .and. .not.periodic(2)) then
                         values(l+3) = 2*coef(lvs+3)
                         values(l+4) = 0
                      else if (.not.periodic(1) .and. .not.periodic(2)) then
                         values(l+1) = 0
                         values(l+2) = 2*coef(lvs+2)
                         values(l+3) = 2*coef(lvs+3)
                         values(l+4) = 0
                      end if
                   else if (i==gex .and. j==gsy) then
                      if      (     periodic(1) .and.      periodic(2)) then
                      else if (.not.periodic(1) .and.      periodic(2)) then
                         values(l+1) = 2*coef(lvs+1)
                         values(l+2) = 0
                      else if (     periodic(1) .and. .not.periodic(2)) then
                         values(l+3) = 0
                         values(l+4) = 2*coef(lvs+4)
                      else if (.not.periodic(1) .and. .not.periodic(2)) then
                         values(l+1) = 2*coef(lvs+1)
                         values(l+2) = 0
                         values(l+3) = 0
                         values(l+4) = 2*coef(lvs+4)
                      end if
                   else if (i==gex .and. j==gey) then
                      if      (     periodic(1) .and.      periodic(2)) then
                      else if (.not.periodic(1) .and.      periodic(2)) then
                         values(l+1) = 2*coef(lvs+1)
                         values(l+2) = 0
                      else if (     periodic(1) .and. .not.periodic(2)) then
                         values(l+3) = 2*coef(lvs+3)
                         values(l+4) = 0
                      else if (.not.periodic(1) .and. .not.periodic(2)) then
                         values(l+1) = 2*coef(lvs+1)
                         values(l+2) = 0
                         values(l+3) = 2*coef(lvs+3)
                         values(l+4) = 0
                      end if
                   else if (i==gsx) then
                      if (periodic(1)) then
                      else 
                         values(l+1) = 0
                         values(l+2) = 2*coef(lvs+2)
                      end if
                   else if (i==gex) then
                      if (periodic(1)) then
                      else 
                         values(l+1) = 2*coef(lvs+1)
                         values(l+2) = 0
                      end if
                   else if (j==gsy) then
                      if (periodic(2)) then
                      else 
                         values(l+3) = 0
                         values(l+4) = 2*coef(lvs+4)
                      end if
                   else if (j==gey) then
                      if (periodic(2)) then
                      else 
                         values(l+3) = 2*coef(lvs+3)
                         values(l+4) = 0
                      end if
                   end if
                   l=l+5
                end if
                lvs=lvs+nb_coeff
             enddo
          enddo
       else 
          lvs=1+nb_coeff*nb_jump
          l=1
          do k=lszs,lezs
             do j=lsys,leys
                do i=lsxs,lexs

                   if (nproc>1.and.(i==lsxs.or.i==lexs.or.j==lsys.or.j==leys.or.k==lszs.or.k==lezs)) then
                   else 
                      values(l)   = coef(lvs) 
                      values(l+1) = coef(lvs+1)
                      values(l+2) = coef(lvs+2)
                      values(l+3) = coef(lvs+3)
                      values(l+4) = coef(lvs+4)
                      values(l+5) = coef(lvs+5)
                      values(l+6) = coef(lvs+6)
                      if     (j==gsy.and.i==gsx.and.k==gsz) then ! BOTTOM / LEFT / BACKWARD CORNER (1/8)
                         if      (     periodic(1) .and.      periodic(2) .and.      periodic(3)) then
                         else if (.not.periodic(1) .and.      periodic(2) .and.      periodic(3)) then
                         else if (     periodic(1) .and. .not.periodic(2) .and.      periodic(3)) then
                         else if (.not.periodic(1) .and. .not.periodic(2) .and.      periodic(3)) then
                         else if (     periodic(1) .and.      periodic(2) .and. .not.periodic(3)) then
                         else if (.not.periodic(1) .and.      periodic(2) .and. .not.periodic(3)) then
                         else if (     periodic(1) .and. .not.periodic(2) .and. .not.periodic(3)) then
                         else if (.not.periodic(1) .and. .not.periodic(2) .and. .not.periodic(3)) then
                            values(l+1) = 0
                            values(l+2) = 2*coef(lvs+2)
                            values(l+3) = 0
                            values(l+4) = 2*coef(lvs+4)
                            values(l+5) = 0
                            values(l+6) = 2*coef(lvs+6)
                         end if
                      elseif (j==gsy.and.i==gex.and.k==gsz) then ! BOTTOM / RIGHT / BACKWARD CORNER (2/8)
                         if      (     periodic(1) .and.      periodic(2) .and.      periodic(3)) then
                         else if (.not.periodic(1) .and.      periodic(2) .and.      periodic(3)) then
                         else if (     periodic(1) .and. .not.periodic(2) .and.      periodic(3)) then
                         else if (.not.periodic(1) .and. .not.periodic(2) .and.      periodic(3)) then
                         else if (     periodic(1) .and.      periodic(2) .and. .not.periodic(3)) then
                         else if (.not.periodic(1) .and.      periodic(2) .and. .not.periodic(3)) then
                         else if (     periodic(1) .and. .not.periodic(2) .and. .not.periodic(3)) then
                         else if (.not.periodic(1) .and. .not.periodic(2) .and. .not.periodic(3)) then
                            values(l+2) = 0
                            values(l+1) = 2*coef(lvs+1)
                            values(l+3) = 0
                            values(l+4) = 2*coef(lvs+4)
                            values(l+5) = 0
                            values(l+6) = 2*coef(lvs+6)
                         end if
                      elseif (j==gey.and.i==gsx.and.k==gsz) then ! TOP / LEFT / BACKWARD CORNER (3/8)
                         if      (     periodic(1) .and.      periodic(2) .and.      periodic(3)) then
                         else if (.not.periodic(1) .and.      periodic(2) .and.      periodic(3)) then
                         else if (     periodic(1) .and. .not.periodic(2) .and.      periodic(3)) then
                         else if (.not.periodic(1) .and. .not.periodic(2) .and.      periodic(3)) then
                         else if (     periodic(1) .and.      periodic(2) .and. .not.periodic(3)) then
                         else if (.not.periodic(1) .and.      periodic(2) .and. .not.periodic(3)) then
                         else if (     periodic(1) .and. .not.periodic(2) .and. .not.periodic(3)) then
                         else if (.not.periodic(1) .and. .not.periodic(2) .and. .not.periodic(3)) then
                            values(l+1) = 0
                            values(l+2) = 2*coef(lvs+2)
                            values(l+4) = 0
                            values(l+3) = 2*coef(lvs+3)
                            values(l+5) = 0
                            values(l+6) = 2*coef(lvs+6)
                         end if
                      elseif (j==gey.and.i==gex.and.k==gsz) then ! TOP / RIGHT / BACKWARD CORNER (4/8)
                         if      (     periodic(1) .and.      periodic(2) .and.      periodic(3)) then
                         else if (.not.periodic(1) .and.      periodic(2) .and.      periodic(3)) then
                         else if (     periodic(1) .and. .not.periodic(2) .and.      periodic(3)) then
                         else if (.not.periodic(1) .and. .not.periodic(2) .and.      periodic(3)) then
                         else if (     periodic(1) .and.      periodic(2) .and. .not.periodic(3)) then
                         else if (.not.periodic(1) .and.      periodic(2) .and. .not.periodic(3)) then
                         else if (     periodic(1) .and. .not.periodic(2) .and. .not.periodic(3)) then
                         else if (.not.periodic(1) .and. .not.periodic(2) .and. .not.periodic(3)) then
                            values(l+2) = 0
                            values(l+1) = 2*coef(lvs+1)
                            values(l+4) = 0
                            values(l+3) = 2*coef(lvs+3)
                            values(l+5) = 0
                            values(l+6) = 2*coef(lvs+6)
                         end if
                      elseif (j==gsy.and.i==gsx.and.k==gez) then ! BOTTOM / LEFT / FORWARD CORNER (5/8)
                         if      (     periodic(1) .and.      periodic(2) .and.      periodic(3)) then
                         else if (.not.periodic(1) .and.      periodic(2) .and.      periodic(3)) then
                         else if (     periodic(1) .and. .not.periodic(2) .and.      periodic(3)) then
                         else if (.not.periodic(1) .and. .not.periodic(2) .and.      periodic(3)) then
                         else if (     periodic(1) .and.      periodic(2) .and. .not.periodic(3)) then
                         else if (.not.periodic(1) .and.      periodic(2) .and. .not.periodic(3)) then
                         else if (     periodic(1) .and. .not.periodic(2) .and. .not.periodic(3)) then
                         else if (.not.periodic(1) .and. .not.periodic(2) .and. .not.periodic(3)) then
                            values(l+1) = 0
                            values(l+2) = 2*coef(lvs+2)
                            values(l+3) = 0
                            values(l+4) = 2*coef(lvs+4)
                            values(l+6) = 0
                            values(l+5) = 2*coef(lvs+5)
                         end if
                      elseif (j==gsy.and.i==gex.and.k==gez) then ! BOTTOM / RIGHT / FORWARD CORNER (6/8)
                         if      (     periodic(1) .and.      periodic(2) .and.      periodic(3)) then
                         else if (.not.periodic(1) .and.      periodic(2) .and.      periodic(3)) then
                         else if (     periodic(1) .and. .not.periodic(2) .and.      periodic(3)) then
                         else if (.not.periodic(1) .and. .not.periodic(2) .and.      periodic(3)) then
                         else if (     periodic(1) .and.      periodic(2) .and. .not.periodic(3)) then
                         else if (.not.periodic(1) .and.      periodic(2) .and. .not.periodic(3)) then
                         else if (     periodic(1) .and. .not.periodic(2) .and. .not.periodic(3)) then
                         else if (.not.periodic(1) .and. .not.periodic(2) .and. .not.periodic(3)) then
                            values(l+2) = 0
                            values(l+1) = 2*coef(lvs+1)
                            values(l+3) = 0
                            values(l+4) = 2*coef(lvs+4)
                            values(l+6) = 0
                            values(l+5) = 2*coef(lvs+5)
                         end if
                      elseif (j==gey.and.i==gsx.and.k==gez) then ! TOP / LEFT / FORWARD CORNER (7/8)
                         if      (     periodic(1) .and.      periodic(2) .and.      periodic(3)) then
                         else if (.not.periodic(1) .and.      periodic(2) .and.      periodic(3)) then
                         else if (     periodic(1) .and. .not.periodic(2) .and.      periodic(3)) then
                         else if (.not.periodic(1) .and. .not.periodic(2) .and.      periodic(3)) then
                         else if (     periodic(1) .and.      periodic(2) .and. .not.periodic(3)) then
                         else if (.not.periodic(1) .and.      periodic(2) .and. .not.periodic(3)) then
                         else if (     periodic(1) .and. .not.periodic(2) .and. .not.periodic(3)) then
                         else if (.not.periodic(1) .and. .not.periodic(2) .and. .not.periodic(3)) then
                            values(l+1) = 0
                            values(l+2) = 2*coef(lvs+2)
                            values(l+4) = 0
                            values(l+3) = 2*coef(lvs+3)
                            values(l+6) = 0
                            values(l+5) = 2*coef(lvs+5)
                         end if
                      elseif (j==gey.and.i==gex.and.k==gez) then ! TOP / RIGHT / FORWARD CORNER (8/8)
                         if      (     periodic(1) .and.      periodic(2) .and.      periodic(3)) then
                         else if (.not.periodic(1) .and.      periodic(2) .and.      periodic(3)) then
                         else if (     periodic(1) .and. .not.periodic(2) .and.      periodic(3)) then
                         else if (.not.periodic(1) .and. .not.periodic(2) .and.      periodic(3)) then
                         else if (     periodic(1) .and.      periodic(2) .and. .not.periodic(3)) then
                         else if (.not.periodic(1) .and.      periodic(2) .and. .not.periodic(3)) then
                         else if (     periodic(1) .and. .not.periodic(2) .and. .not.periodic(3)) then
                         else if (.not.periodic(1) .and. .not.periodic(2) .and. .not.periodic(3)) then
                            values(l+2) = 0
                            values(l+1) = 2*coef(lvs+1)
                            values(l+4) = 0
                            values(l+3) = 2*coef(lvs+3)
                            values(l+6) = 0
                            values(l+5) = 2*coef(lvs+5)
                         end if
                      elseif (k==gsz.and.i==gsx) then ! BACKWARD / LEFT EDGE (1/12)
                         if      (     periodic(1) .and.      periodic(3)) then
                         else if (.not.periodic(1) .and.      periodic(3)) then
                         else if (     periodic(1) .and. .not.periodic(3)) then
                         else if (.not.periodic(1) .and. .not.periodic(3)) then
                            values(l+1) = 0
                            values(l+2) = 2*coef(lvs+2)
                            values(l+5) = 0
                            values(l+6) = 2*coef(lvs+6)
                         end if
                      elseif (k==gsz.and.i==gex) then ! BACKWARD / RIGHT EDGE (2/12)
                         if      (     periodic(1) .and.      periodic(3)) then
                         else if (.not.periodic(1) .and.      periodic(3)) then
                         else if (     periodic(1) .and. .not.periodic(3)) then
                         else if (.not.periodic(1) .and. .not.periodic(3)) then
                            values(l+2) = 0
                            values(l+1) = 2*coef(lvs+1)
                            values(l+5) = 0
                            values(l+6) = 2*coef(lvs+6)
                         end if
                      elseif (k==gsz.and.j==gsy) then ! BACKWARD / BOTTOM EDGE (3/12)
                         if      (     periodic(2) .and.      periodic(3)) then
                         else if (.not.periodic(2) .and.      periodic(3)) then
                         else if (     periodic(2) .and. .not.periodic(3)) then
                         else if (.not.periodic(2) .and. .not.periodic(3)) then
                            values(l+3) = 0
                            values(l+4) = 2*coef(lvs+4)
                            values(l+5) = 0
                            values(l+6) = 2*coef(lvs+6)
                         end if
                      elseif (k==gsz.and.j==gey) then ! BACKWARD / TOP EDGE (4/12)
                         if      (     periodic(2) .and.      periodic(3)) then
                         else if (.not.periodic(2) .and.      periodic(3)) then
                         else if (     periodic(2) .and. .not.periodic(3)) then
                         else if (.not.periodic(2) .and. .not.periodic(3)) then
                            values(l+4) = 0
                            values(l+3) = 2*coef(lvs+3)
                            values(l+5) = 0
                            values(l+6) = 2*coef(lvs+6)
                         end if
                      elseif (i==gsx.and.j==gsy) then ! LEFT / BOTTOM EDGE (5/12)
                         if      (     periodic(1) .and.      periodic(2)) then
                         else if (.not.periodic(1) .and.      periodic(2)) then
                         else if (     periodic(1) .and. .not.periodic(2)) then
                         else if (.not.periodic(1) .and. .not.periodic(2)) then
                            values(l+1) = 0
                            values(l+2) = 2*coef(lvs+2)
                            values(l+3) = 0
                            values(l+4) = 2*coef(lvs+4)
                         end if
                      elseif (i==gex.and.j==gsy) then ! RIGHT / BOTTOM EDGE (6/12)
                         if      (     periodic(1) .and.      periodic(2)) then
                         else if (.not.periodic(1) .and.      periodic(2)) then
                         else if (     periodic(1) .and. .not.periodic(2)) then
                         else if (.not.periodic(1) .and. .not.periodic(2)) then
                            values(l+2) = 0
                            values(l+1) = 2*coef(lvs+1)
                            values(l+3) = 0
                            values(l+4) = 2*coef(lvs+4)
                         end if
                      elseif (i==gsx.and.j==gey) then ! LEFT / TOP EDGE (7/12)
                         if      (     periodic(1) .and.      periodic(2)) then
                         else if (.not.periodic(1) .and.      periodic(2)) then
                         else if (     periodic(1) .and. .not.periodic(2)) then
                         else if (.not.periodic(1) .and. .not.periodic(2)) then
                            values(l+1) = 0
                            values(l+2) = 2*coef(lvs+2)
                            values(l+4) = 0
                            values(l+3) = 2*coef(lvs+3)
                         end if
                      elseif (i==gex.and.j==gey) then ! RIGHT / TOP EDGE (8/12)
                         if      (     periodic(1) .and.      periodic(2)) then
                         else if (.not.periodic(1) .and.      periodic(2)) then
                         else if (     periodic(1) .and. .not.periodic(2)) then
                         else if (.not.periodic(1) .and. .not.periodic(2)) then
                            values(l+2) = 0
                            values(l+1) = 2*coef(lvs+1)
                            values(l+4) = 0
                            values(l+3) = 2*coef(lvs+3)
                         end if
                      elseif (k==gez.and.i==gsx) then ! FORWARD / LEFT EDGE (9/12)
                         if      (     periodic(1) .and.      periodic(3)) then
                         else if (.not.periodic(1) .and.      periodic(3)) then
                         else if (     periodic(1) .and. .not.periodic(3)) then
                         else if (.not.periodic(1) .and. .not.periodic(3)) then
                            values(l+1) = 0
                            values(l+2) = 2*coef(lvs+2)
                            values(l+6) = 0
                            values(l+5) = 2*coef(lvs+5)
                         end if
                      elseif (k==gez.and.i==gex) then ! FORWARD / RIGHT EDGE (10/12)
                         if      (     periodic(1) .and.      periodic(3)) then
                         else if (.not.periodic(1) .and.      periodic(3)) then
                         else if (     periodic(1) .and. .not.periodic(3)) then
                         else if (.not.periodic(1) .and. .not.periodic(3)) then
                            values(l+2) = 0
                            values(l+1) = 2*coef(lvs+1)
                            values(l+6) = 0
                            values(l+5) = 2*coef(lvs+5)
                         end if
                      elseif (k==gez.and.j==gsy) then ! FORWARD / BOTTOM EDGE (11/12)
                         if      (     periodic(2) .and.      periodic(3)) then
                         else if (.not.periodic(2) .and.      periodic(3)) then
                         else if (     periodic(2) .and. .not.periodic(3)) then
                         else if (.not.periodic(2) .and. .not.periodic(3)) then
                            values(l+3) = 0
                            values(l+4) = 2*coef(lvs+4)
                            values(l+6) = 0
                            values(l+5) = 2*coef(lvs+5)
                         end if
                      elseif (k==gez.and.j==gey) then ! FORWARD / TOP EDGE (12/12)
                         if      (     periodic(2) .and.      periodic(3)) then
                         else if (.not.periodic(2) .and.      periodic(3)) then
                         else if (     periodic(2) .and. .not.periodic(3)) then
                         else if (.not.periodic(2) .and. .not.periodic(3)) then
                            values(l+4) = 0
                            values(l+3) = 2*coef(lvs+3)
                            values(l+6) = 0
                            values(l+5) = 2*coef(lvs+5)
                         end if
                      elseif (i==gsx) then ! LEFT FACE (1/6)
                         if (periodic(1)) then
                         else 
                            values(l+1) = 0
                            values(l+2) = 2*coef(lvs+2)
                         end if
                      elseif (i==gex) then ! RIGHT FACE (2/6)
                         if (periodic(1)) then
                         else 
                            values(l+2) = 0
                            values(l+1) = 2*coef(lvs+1)
                         end if
                      elseif (j==gsy) then ! BOTTOM FACE (3/6)
                         if (periodic(2)) then
                         else 
                            values(l+3) = 0
                            values(l+4) = 2*coef(lvs+4)
                         end if
                      elseif (j==gey) then ! TOP FACE (4/6)
                         if (periodic(2)) then
                         else 
                            values(l+4) = 0
                            values(l+3) = 2*coef(lvs+3)
                         end if
                      elseif (k==gsz) then ! BACKWARD FACE (5/6)
                         if (periodic(3)) then
                         else 
                            values(l+5) = 0
                            values(l+6) = 2*coef(lvs+6)
                         end if
                      elseif (k==gez) then ! FORWARD FACE (6/6)
                         if (periodic(3)) then
                         else 
                            values(l+6) = 0
                            values(l+5) = 2*coef(lvs+5)
                         end if
                      end if
                      l=l+7
                   end if
                   lvs=lvs+nb_coeff
                enddo
             enddo
          enddo
       endif
    case(1,2)
       if (dim==2) then
          lvs=1+nb_coeff*nb_jump
          l=1
          do j=lsys,leys
             do i=lsxs,lexs
                if (nproc>1.and.(i==lsxs.or.i==lexs.or.j==lsys.or.j==leys)) then
                else 
                   values(l)   = coef(lvs) 
                   values(l+1) = coef(lvs+1)
                   values(l+2) = coef(lvs+2)
                   values(l+3) = coef(lvs+3)
                   values(l+4) = coef(lvs+4)
                   l=l+5
                end if
                lvs=lvs+nb_coeff
             enddo
          enddo
       else 
          lvs=1+nb_coeff*nb_jump
          l=1
          do k=lszs,lezs
             do j=lsys,leys
                do i=lsxs,lexs
                   if (nproc>1.and.(i==lsxs.or.i==lexs.or.j==lsys.or.j==leys.or.k==lszs.or.k==lezs)) then
                   else 
                      values(l)=coef(lvs) 
                      values(l+1)=coef(lvs+1)
                      values(l+2)=coef(lvs+2)
                      values(l+3)=coef(lvs+3)
                      values(l+4)=coef(lvs+4)
                      values(l+5)=coef(lvs+5)
                      values(l+6)=coef(lvs+6)
                      l=l+7
                   end if
                   lvs=lvs+nb_coeff
                enddo
             enddo
          enddo
       endif
    end select
    
    call HYPRE_SStructMatrixSetBoxValues(MatA,part,ilower,iupper,var,nentries,stencil_indices,values,ierr)
    call HYPRE_SStructMatrixAssemble(MatA,ierr)


!!$    select case(dir)
!!$    case(1)
!!$       call HYPRE_SStructMatrixPrint("matrixu",MatA,0,ierr)
!!$    case(2)
!!$       call HYPRE_SStructMatrixPrint("matrixv",MatA,0,ierr)
!!$    case(3)
!!$       call HYPRE_SStructMatrixPrint("matrixw",MatA,0,ierr)
!!$    end select
    
    deallocate(values)

    call compute_time(TIMER_END,"[sub] Prepa_HYPRE_Auvwp")
  end subroutine Prepa_HYPRE_Auvwp

  Subroutine create_precon_smg_or_pfmg(solverp,solveru,solverv,solverw)
    use mod_Parameters, only: dim,                                      &
         & NS_HYPRE_precond,NS_HYPRE_nb_pre_post_relax,NS_HYPRE_maxLvL, &
         & NS_HYPRE_typeRelax,NS_HYPRE_skipRelax,                       &
         & NS_MomentumConserving,NS_invicid
    use mod_mpi
    implicit none
    include 'HYPREf.h'

    integer*8,intent(out):: solveru,solverv,solverp,solverw
    integer ::    ierr
    integer :: maxLvL,nb_pre_post_relax,typeRelax,skipRelax
    real(8) :: tol

    call compute_time(TIMER_START,"[sub] create_precon_smg_or_pfmg")
    
!!$      !> Maximum number of multigrid grid levels --> maxLvL
!!$      integer(C_INT) :: max_levels
!!$      !!
!!$ 
!!$      !> Initial guess --> 0
!!$      integer        :: zero_guess      !! Do not set
!!$      !integer       :: zero_guess = 0  !! Use zero initial guess
!!$      !integer       :: zero_guess = 1  !! Use a non-zero initial guess
!!$      !!
!!$      !> Relaxation type --> typeRelax
!!$      !integer(C_INT) :: relax_type = 0 !! Jacobi
!!$      integer(C_INT)  :: relax_type = 1 !! Weighted Jacobi
!!$      !integer(C_INT) :: relax_type = 2 !! Red-Black Gauss-Seidel
!!$      !integer(C_INT) :: relax_type = 3 !! Red-Black Gauss-Seidel, non-symmetric
!!$      !!
!!$      !> Corase grid operator type
!!$      integer(C_INT)  :: rap_type = 0  !! Galerkin
!!$      !integer(C_INT) :: rap_type = 1  !! Non-Galerkin ParFlow operatos
!!$      !integer(C_INT) :: rap_type = 2  !! Galerkin, general operators
!!$
!!$      Current operators set by rap type are:
!!$      0 – Galerkin (default)
!!$      1 – non-Galerkin 5-pt or 7-pt stencils
!!$      Both operators are constructed algebraically. The non-Galerkin option maintains a 5-pt stencil in 2D and
!!$      a 7-pt stencil in 3D on all grid levels. The stencil coefficients are computed by averaging techniques.
!!$      
!!$      !> Number of relaxation sweeps before coarse-grid correction --> nb_pre_post_relax
!!$      integer(C_INT) :: num_pre_relax = 1
!!$      
!!$      !> Number of relaxation sweeps after coarse-grid correction --> nb_pre_post_relax
!!$      integer(C_INT) :: num_post_relax = 1
!!$      
!!$      !> Skip relaxation on certain grids for isotropic problems --> skipRelax
!!$      integer(C_INT) :: skip_relax = 0   1 anisentropic
!!$      !> @}
!!$
!!$
!!$
!!$    
!!$      smg
!!$
!!$      !> Initial guess
!!$      integer        :: zero_guess      !! Do not set
!!$      !integer        :: zero_guess = 0  !! Use zero initial guess
!!$      !integer        :: zero_guess = 1  !! Use a non-zero initial guess
!!$      !!
!!$      !> Number of relaxation sweeps before coarse-grid correction
!!$      integer(C_INT) :: num_pre_relax = 1
!!$      !!
!!$      !> Number of relaxation sweeps after coarse-grid correction
!!$      integer(C_INT) :: num_post_relax = 1
!!$      !!
!!$      !> @}

    nb_pre_post_relax=NS_HYPRE_nb_pre_post_relax
    tol=1d-14
    maxLvL=NS_HYPRE_maxLvL
    typeRelax=NS_HYPRE_typeRelax
    skipRelax=NS_HYPRE_skipRelax

    ! 0 : PFMG
    ! 1 : SMG
    select case(NS_HYPRE_precond)
    case(0)
       call HYPRE_StructPFMGCreate(COMM3D,solverp,ierr)
       call HYPRE_StructPFMGSetMaxIter(solverp,1,ierr)
       call HYPRE_StructPFMGSetTol(solverp,tol,ierr)
       call HYPRE_StructPFMGsetmaxlevels(solverp,maxLvL,ierr)
       call HYPRE_StructPFMGSetZeroGuess(solverp,ierr)
       !call HYPRE_StructPFMGSetNonZeroGuess(solverp,ierr)
       call HYPRE_StructPFMGSetNumPreRelax(solverp,nb_pre_post_relax,ierr)
       call HYPRE_StructPFMGSetNumPostRelax(solverp,nb_pre_post_relax,ierr)
       call HYPRE_STructPFMGSetRAPType(solverp,1,ierr)
       call HYPRE_StructPFMGSetPrintLevel(solverp,1,ierr)
       call HYPRE_StructPFMGSetLogging(solverp,1,ierr)
       call HYPRE_structPFMGsetrelaxtype(solverp,typeRelax,ierr) 
       call HYPRE_structPFMGsetskiprelax(solverp,skipRelax,ierr)

       call HYPRE_StructPFMGCreate(COMM3D,solveru,ierr)
       call HYPRE_StructPFMGsetmaxlevels(solveru,maxLvL,ierr)
       call HYPRE_StructPFMGSetMaxIter(solveru,1,ierr)
       call HYPRE_StructPFMGSetTol(solveru,tol,ierr)
       call HYPRE_StructPFMGSetZeroGuess(solveru,ierr)
       !call HYPRE_StructPFMGSetNonZeroGuess(solveru,ierr)
       call HYPRE_StructPFMGSetNumPreRelax(solveru,nb_pre_post_relax,ierr)
       call HYPRE_StructPFMGSetNumPostRelax(solveru,nb_pre_post_relax,ierr)
       call HYPRE_STructPFMGSetRAPType(solveru,1,ierr)
       call HYPRE_StructPFMGSetPrintLevel(solveru,1,ierr)
       call HYPRE_StructPFMGSetLogging(solveru,1,ierr)
       if (NS_invicid.and.NS_MomentumConserving) then
          call HYPRE_structPFMGsetrelaxtype(solveru,0,ierr)
          call HYPRE_structPFMGsetskiprelax(solveru,1,ierr)
       else
          call HYPRE_structPFMGsetrelaxtype(solveru,typeRelax,ierr)
          call HYPRE_structPFMGsetskiprelax(solveru,skipRelax,ierr)
       end if
       
       call HYPRE_StructPFMGCreate(COMM3D,solverv,ierr)
       call HYPRE_StructPFMGSetMaxIter(solverv,1,ierr)
       call HYPRE_StructPFMGSetTol(solverv,tol,ierr)
       call HYPRE_StructPFMGsetmaxlevels(solverv,maxLvL,ierr)
       call HYPRE_StructPFMGSetZeroGuess(solverv,ierr)
       !call HYPRE_StructPFMGSetNonZeroGuess(solverv,ierr)
       call HYPRE_StructPFMGSetNumPreRelax(solverv,nb_pre_post_relax,ierr)
       call HYPRE_StructPFMGSetNumPostRelax(solverv,nb_pre_post_relax,ierr)
       call HYPRE_STructPFMGSetRAPType(solverv,1,ierr)
       call HYPRE_StructPFMGSetPrintLevel(solverv,1,ierr)
       call HYPRE_StructPFMGSetLogging(solverv,1,ierr)
       if (NS_invicid.and.NS_MomentumConserving) then
          call HYPRE_structPFMGsetrelaxtype(solverv,0,ierr)
          call HYPRE_structPFMGsetskiprelax(solverv,1,ierr)
       else
          call HYPRE_structPFMGsetrelaxtype(solverv,typeRelax,ierr)
          call HYPRE_structPFMGsetskiprelax(solverv,skipRelax,ierr)
       end if
       
       if (dim==3) then
          call HYPRE_StructPFMGCreate(COMM3D,solverw,ierr)
          call HYPRE_StructPFMGSetMaxIter(solverw,1,ierr)
          call HYPRE_StructPFMGSetTol(solverw,tol,ierr)
          call HYPRE_StructPFMGsetmaxlevels(solverw,maxLvL,ierr)
          call HYPRE_StructPFMGSetZeroGuess(solverw,ierr)
          !call HYPRE_StructPFMGSetNonZeroGuess(solverw,ierr)
          call HYPRE_StructPFMGSetNumPreRelax(solverw,nb_pre_post_relax,ierr)
          call HYPRE_StructPFMGSetNumPostRelax(solverw,nb_pre_post_relax,ierr)
          call HYPRE_STructPFMGSetRAPType(solverw,1,ierr)
          call HYPRE_StructPFMGSetPrintLevel(solverw,1,ierr)
          call HYPRE_StructPFMGSetLogging(solverw,1,ierr)
          if (NS_invicid.and.NS_MomentumConserving) then
             call HYPRE_structPFMGsetrelaxtype(solverw,0,ierr)
             call HYPRE_structPFMGsetskiprelax(solverw,1,ierr)
          else
             call HYPRE_structPFMGsetrelaxtype(solverw,typeRelax,ierr)
             call HYPRE_structPFMGsetskiprelax(solverw,skipRelax,ierr)
          end if
       end if
       
    case(1)    
       call HYPRE_StructSMGCreate(COMM3D,solverp,ierr)
       call HYPRE_StructSMGSetMemoryUse(solverp,0,ierr)
       call HYPRE_StructSMGSetMaxIter(solverp,1,ierr)
       call HYPRE_StructSMGSetTol(solverp,tol,ierr)
       call HYPRE_StructSMGSetZeroGuess(solverp,ierr)
       !call HYPRE_StructSMGSetNonZeroGuess(solverp,ierr)
       call HYPRE_StructSMGSetNumPreRelax(solverp,nb_pre_post_relax,ierr)
       call HYPRE_StructSMGSetNumPostRelax(solverp,nb_pre_post_relax,ierr)
       call HYPRE_StructSMGSetPrintLevel(solverp,1,ierr)
       call HYPRE_StructSMGSetLogging(solverp,1,ierr)

       call HYPRE_StructSMGCreate(COMM3D,solveru,ierr)
       call HYPRE_StructSMGSetMemoryUse(solveru,0,ierr)
       call HYPRE_StructSMGSetMaxIter(solveru,1,ierr)
       call HYPRE_StructSMGSetTol(solveru,tol,ierr)
       call HYPRE_StructSMGSetZeroGuess(solveru,ierr)
       !call HYPRE_StructSMGSetNonZeroGuess(solveru,ierr)
       call HYPRE_StructSMGSetNumPreRelax(solveru,nb_pre_post_relax,ierr)
       call HYPRE_StructSMGSetNumPostRelax(solveru,nb_pre_post_relax,ierr)
       call HYPRE_StructSMGSetPrintLevel(solveru,1,ierr)
       call HYPRE_StructSMGSetLogging(solveru,1,ierr)

       call HYPRE_StructSMGCreate(COMM3D,solverv,ierr)
       call HYPRE_StructSMGSetMemoryUse(solverv,0,ierr)
       call HYPRE_StructSMGSetMaxIter(solverv,1,ierr)
       call HYPRE_StructSMGSetTol(solverv,tol,ierr)
       call HYPRE_StructSMGSetZeroGuess(solverv,ierr)
       !call HYPRE_StructSMGSetNonZeroGuess(solverv,ierr)
       call HYPRE_StructSMGSetNumPreRelax(solverv,nb_pre_post_relax,ierr)
       call HYPRE_StructSMGSetNumPostRelax(solverv,nb_pre_post_relax,ierr)
       call HYPRE_StructSMGSetPrintLevel(solverv,1,ierr)
       call HYPRE_StructSMGSetLogging(solverv,1,ierr)
       if (dim==3) then
          call HYPRE_StructSMGCreate(COMM3D,solverw,ierr)
          call HYPRE_StructSMGSetMemoryUse(solverw,0,ierr)
          call HYPRE_StructSMGSetMaxIter(solverw,1,ierr)
          call HYPRE_StructSMGSetTol(solverw,tol,ierr)
          call HYPRE_StructSMGSetZeroGuess(solverw,ierr)
          !call HYPRE_StructSMGSetNonZeroGuess(solverw,ierr)
          call HYPRE_StructSMGSetNumPreRelax(solverw,nb_pre_post_relax,ierr)
          call HYPRE_StructSMGSetNumPostRelax(solverw,nb_pre_post_relax,ierr)
          call HYPRE_StructSMGSetPrintLevel(solverw,1,ierr)
          call HYPRE_StructSMGSetLogging(solverw,1,ierr)
       endif
    end select

    call compute_time(TIMER_END,"[sub] create_precon_smg_or_pfmg")
  end Subroutine create_precon_SMG_or_PFMG

  subroutine setup(solverp,sAp,solveru,sAu,solverv,sAv,solverw,sAw)
    use mod_Parameters, only: NS_HYPRE_precond,dim
    use mod_mpi
    implicit none
    include 'HYPREf.h'

    integer*8, intent(inout) :: Sau,Sav,Sap,Saw
    integer*8, intent(inout) :: solveru,solverv,solverp,solverw
    integer                  :: ierr  

    call compute_time(TIMER_START,"[sub] setup")
    
    select case(NS_HYPRE_precond)
    case(0)
       call HYPRE_StructPFMGSetup(solverp,sAp,Struct_hypre_yp,Struct_hypre_xp,ierr)
       call HYPRE_StructPFMGSetup(solveru,sAu,Struct_hypre_yu,Struct_hypre_xu,ierr)
       call HYPRE_StructPFMGSetup(solverv,sAv,Struct_hypre_yv,Struct_hypre_xv,ierr)
       if (dim==3) call HYPRE_StructPFMGSetup(solverw,sAw,Struct_hypre_yw,Struct_hypre_xw,ierr)
    case(1)
       call HYPRE_StructSMGSetup(solverp,sAp,Struct_hypre_yp,Struct_hypre_xp,ierr)
       call HYPRE_StructSMGSetup(solveru,sAu,Struct_hypre_yu,Struct_hypre_xu,ierr)
       call HYPRE_StructSMGSetup(solverv,sAv,Struct_hypre_yv,Struct_hypre_xv,ierr)
       if (dim==3) call HYPRE_StructSMGSetup(solverw,sAw,Struct_hypre_yw,Struct_hypre_xw,ierr)
    end select

    call compute_time(TIMER_END,"[sub] setup")
  end subroutine setup

  subroutine solver_FC_BiCGStab2_iLU(coef,jcof,icof,sol,rhs,vie,npt,nps, &
       & coef_pcd,jcof_pcd,icof_pcd,rovu,rovv,rovw,kic,nic,res,kt, &
       & solveru,solverp,solverv,solverw,sAp,Sau,Sav,Saw,          &
       & solver_it,solver_threshold,icode)
    use mod_Parameters, only: dim,nb_Vx,nb_Vy, &
         & ex,sx,ey,sy,ez,sz,gx,gy,gz,         &
         & gsx,gex,gsy,gey,gsz,gez,            &
         & sxu,exu,syu,eyu,szu,ezu,            &
         & sxv,exv,syv,eyv,szv,ezv,            &
         & sxw,exw,syw,eyw,szw,ezw,            &
         & sxs,exs,sys,eys,szs,ezs,            &
         & sxus,exus,syus,eyus,szus,ezus,      &
         & sxvs,exvs,syvs,eyvs,szvs,ezvs,      &
         & sxws,exws,syws,eyws,szws,ezws,      &
         & t1_tab2sol,t2_tab2sol,NS_method,    &
         & nb_vz,nb_p_no_ghost,nb_p_no_ghost,  &
         & nb_vx_no_ghost,nb_vy_no_ghost,      &
         & nb_vz_no_ghost,dx,dy,dz,            &
         & nb_p,nb_v,nb_vp,ires
    use mod_mpi
    implicit none
    include 'HYPREf.h'
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    integer,dimension(:),intent(IN),allocatable         :: jcof,jcof_pcd
    integer, dimension(:),intent(IN),allocatable        :: icof,icof_pcd
    real(8), dimension(:),intent(INout),allocatable     :: coef,coef_pcd
    real(8), dimension(:),intent(INout),allocatable     :: rhs
    real(8), dimension(:),intent(INout),allocatable     :: sol
    real(8), dimension(:,:,:),intent(INout),allocatable :: rovu,rovv,rovw
    integer,intent(in)                                  :: nps,npt
    real(8), dimension(:,:,:),intent(IN),allocatable    :: vie
    integer, dimension(npt)                             :: kic
    integer, intent(in)                                 :: solver_it
    real(8), intent(in)                                 :: solver_threshold
    integer, intent(out)                                :: kt,icode
    real(8), intent(out)                                :: res
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                            :: nic
    integer                                            :: l,k,na,ierr,lvs,impp,i,lts
    !-------------------------------------------------------------------------------
    real(8)                                            :: eps,bet,prec,rho1,tol,residu,ressq,res0
    !-------------------------------------------------------------------------------
    real(8)                                            :: diag
    real(8)                                            :: alp,bl2,ome2,restemp,rho0,rl2
    real(8)                                            :: bl20,rl20
    real(8)                                            :: gam,ome1,smu,snu,tau
    real(8), dimension(:), allocatable                 :: psa,qsa,rsa,soltemp,ssa,tsa
    real(8), dimension(:), allocatable                 :: usa,vsa,wsa,ysa,msa,smc
    character(len=100)                                 :: fmt1   
    real(8), dimension(:), allocatable                 :: tmp,rhs0
    !--------------------------------------------------------------------------------
    ! Hypre  variables
    !-------------------------------------------------------------------------------
    integer*8                            :: sSyu,sSxu,sSyv,sSxv,sSyp,sSxp,ssyw,ssxw    
    integer*8, intent(inout)             :: Sau,Sav,Sap,Saw
    integer*8, intent(inout)             :: solveru,solverv,solverp,solverw
    !-------------------------------------------------------------------------------
    integer  :: test
    real(8), dimension(:), allocatable   :: bp,coefb
    integer, dimension(:), allocatable   :: icofb,jcofb
    !-------------------------------------------------------------------------------
    integer, dimension(:), allocatable   :: jcof_uv,icof_uv
    real(8), dimension(:), allocatable   :: coef_uv
    integer, dimension(:), allocatable   :: jcof_uvw,icof_uvw
    real(8), dimension(:), allocatable   :: coef_uvw
    !-------------------------------------------------------------------------------

    call compute_time(TIMER_START,"[sub] solver_FC_BiCGStab2_iLU")
    
    !-------------------------------------------------------------------------------
    ! init
    !-------------------------------------------------------------------------------
    mpi_ieq_solve=0
    icode=0
    !-------------------------------------------------------------------------------

    allocate(coefb(2*nb_V),jcofb(2*nb_v),icofb(nb_v+1))
    allocate(jcof_uv(4*nb_vx),icof_uv(nb_vx+1),coef_uv(4*nb_vx))
    allocate(psa(npt),qsa(npt),rsa(npt),soltemp(npt),ssa(npt), &
         & tsa(npt),usa(npt),vsa(npt),wsa(npt),ysa(npt),msa(npt),smc(npt))
    if (dim==3) then
       allocate(jcof_uvw(4*(nb_vx+nb_vy)),icof_uvw(nb_vx+nb_vy+1),coef_uvw(4*(nb_vx+nb_vy)))
    endif

    coefb=0
    jcofb=0
    icofb=0

    coef_uv=0
    jcof_uv=0
    icof_uv=0

    if (dim==3) then
       coef_uvw=0
       jcof_uvw=0
       icof_uvw=0
    end if

    soltemp=0

    call construct_Bp(icof,jcof,coef,icofb,jcofb,coefb)
    call construct_Fuv(icof,jcof,coef,icof_uv,jcof_uv,coef_uv)
    if (dim==3) then
       call construct_Fuw_Fvw(icof,jcof,coef,icof_uvw,jcof_uvw,coef_uvw)
    endif

    
    !-------------------------------------------------------------------------------
    ! initial residual values
    !-------------------------------------------------------------------------------
    call pscalee(rhs,rhs,bl20,npt,kic,nic) !
    bl20 = sqrt(bl20+1.d-40)
    !-------------------------------------------------------------------------------
    call matveke(npt,sol,psa,coef,jcof,icof,nps)
    psa=psa-rhs
    call pscalee(psa,psa,res,npt,kic,nic)
    rl20 = res
    rl20 = sqrt(rl20+1.d-40)
    !-------------------------------------------------------------------------------

    
    !sol = 0.0d0
    tol =1.1d-5
    !-------------------------------------------------------------------------------
    ! Initialization
    !-------------------------------------------------------------------------------
    msa = 0.d0
    psa = 0.d0
    qsa = 0.d0
    rsa = 0.d0
    ssa = 0.d0
    tsa = 0.d0
    usa = 0.d0
    vsa = 0.d0
    wsa = 0.d0
    ysa = 0.d0
    qsa = rhs
    smc = rhs
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! preconditionning
    !-------------------------------------------------------------------------------
    call SMG_solver(coef,jcof,rhs,qsa,&
         SAp,solverp,Sau,solveru,Sav,solverv,Saw,solverw,vie,&
         coef_pcd,jcof_pcd,icof_pcd,coefb,jcofb,icofb,coef_uv,jcof_uv,icof_uv,&
         icof_uvw,jcof_uvw,coef_uvw)
    call pscalee (rhs,rhs,bl2,npt,kic,nic)  

    bl2 = sqrt(bl2+1.d-40)

    call  matveke(npt,sol,psa,coef,jcof,icof,nps)

    call SMG_solver(coef,jcof,tsa,psa,&
         SAp,solverp,Sau,solveru,Sav,solverv,Saw,solverw,vie,&
         coef_pcd,jcof_pcd,icof_pcd,coefb,jcofb,icofb,coef_uv,jcof_uv,icof_uv,&
         icof_uvw,jcof_uvw,coef_uvw)

    rsa = rhs - tsa
    qsa = rsa !rsa
    psa = rsa !rsa

    call pscalee (rsa,rsa,rl2,npt,kic,nic)
    rl2 = sqrt(rl2+1.d-40)

    if ( rl2 < 1.d-10)  then
       !exit  
    end if

    if (bl2 <= 1.d-80) then
       write (6,*) 'erreur dans le g.c. : second membre nul', bl2
       stop
    endif
    if (rl2 <= 1.d-80) then
       write (6,*) 'erreur dans le g.c. : residu nul', rl2
       stop
    endif
!!$

    !-------------------------------------------------------------------------------
    restemp = 1.d20
    !-------------------------------------------------------------------------------
    !bicgstab(2)
    !-------------------------------------------------------------------------------
    rho0 = 1.d0
    alp = 0.d0
    ome2 = 1.d0
    res = 1
    res0 =1
    !-------------------------------------------------------------------------------

    
    if (ires>0) then 
       if (rank==0) then
          write(*,*)
          write(*,101) "-----", "--------------------", &
               & "--------------------","--------------------"
          write(*,*) "  resol.sys.",mpi_ieq_solve
          write(*,*) "      |b|_2=",bl20
          write(*,*) " |A.x0-b|_2=",rl20
!!$          write(*,*) "     |Pb|_2=",bl2
!!$          write(*,*) "|PA.x-Pb|_2=",rl2
          write(*,101) "Iters", "|A.x-b|_2", "conv.rate", "|A.x-b|_2/|b|_2"
          write(*,101) "-----", "--------------------", &
               & "--------------------","--------------------"
       end if
    end if

    
    do kt=1,solver_it

       prec= 1.d-60
       rho0 = -ome2*rho0
       !-------------------------------------------------------------------------------
       ! Even step
       !-------------------------------------------------------------------------------
       call pscalee (qsa,rsa,rho1,npt,kic,nic)

       bet = alp * rho1 / rho0

       rho0 = rho1

       do l = 1,npt
          usa(l) = rsa(l) - bet*usa(l) ! u=r-bet*u
       enddo

       call  matveke(npt,usa,psa,coef,jcof,icof,nps)

       call SMG_solver(coef,jcof,vsa,psa,&
            SAp,solverp,Sau,solveru,Sav,solverv,Saw,solverw,vie,&
            coef_pcd,jcof_pcd,icof_pcd,coefb,jcofb,icofb,coef_uv,jcof_uv,icof_uv,&
            icof_uvw,jcof_uvw,coef_uvw)

       call pscalee (qsa,vsa,gam,npt,kic,nic)

       alp = rho0 / gam

       do l = 1,npt
          rsa(l) = rsa(l) - alp*vsa(l) !r=r-alpha*v
       enddo

       call matveke(npt,rsa,psa,coef,jcof,icof,nps)

       call SMG_solver(coef,jcof,ssa,psa,&
            SAp,solverp,Sau,solveru,Sav,solverv,Saw,solverw,vie,&
            coef_pcd,jcof_pcd,icof_pcd,coefb,jcofb,icofb,coef_uv,jcof_uv,icof_uv,&
            icof_uvw,jcof_uvw,coef_uvw)

       sol = sol + alp*usa    ! x=x+alp*u

       !-------------------------------------------------------------------------------
       !Odd step
       !-------------------------------------------------------------------------------

       !===============================================================================
       call pscalee (qsa,ssa,rho1,npt,kic,nic)
       bet = alp * rho1 / rho0
       rho0 = rho1
       vsa = ssa - bet*vsa

       call matveke(npt,vsa,psa,coef,jcof,icof,nps)
       call SMG_solver(coef,jcof,wsa,psa,&
            SAp,solverp,Sau,solveru,Sav,solverv,Saw,solverw,vie,&
            coef_pcd,jcof_pcd,icof_pcd,coefb,jcofb,icofb,coef_uv,jcof_uv,icof_uv,&
            icof_uvw,jcof_uvw,coef_uvw)

       call pscalee (qsa,wsa,gam,npt,kic,nic)

       alp = (rho0+prec) / (gam+prec)

       usa = rsa - bet*usa
       rsa = rsa - alp*vsa
       ssa = ssa - alp*wsa

       call matveke(npt,ssa,psa,coef,jcof,icof,nps)
       call SMG_solver(coef,jcof,tsa,psa,&
            SAp,solverp,Sau,solveru,Sav,solverv,Saw,solverw,vie,&
            coef_pcd,jcof_pcd,icof_pcd,coefb,jcofb,icofb,coef_uv,jcof_uv,icof_uv,&
            icof_uvw,jcof_uvw,coef_uvw)

       !-------------------------------------------------------------------------------
       !cgr(2) part
       !-------------------------------------------------------------------------------
       !===============================================================================
#if OPTIM_PSCALEE
       call pscalee5(rsa,ssa,tsa,ome1,smu,snu,tau,ome2,npt,kic,nic)
#else
       call pscalee (rsa,ssa,ome1,npt,kic,nic)
       call pscalee (ssa,ssa,smu,npt,kic,nic)
       call pscalee (ssa,tsa,snu,npt,kic,nic)
       call pscalee (tsa,tsa,tau,npt,kic,nic)
       !===============================================================================
       call pscalee (rsa,tsa,ome2,npt,kic,nic)
#endif 
       tau = tau - snu*snu/smu
       ome2 = (ome2-(snu+prec)*(ome1+prec)/(smu+prec))/(tau+prec)
       ome1 = (ome1-snu*ome2) / (smu+prec)

       sol = sol + ome1*rsa + ome2*ssa + alp*usa
       rsa = rsa - ome1*ssa - ome2*tsa 
       usa = usa - ome1*vsa - ome2*wsa

       !call pscalee (rsa,rsa,res,npt,kic,nic)
       
       rl20=res
       
       call matveke(npt,sol,ysa,coef,jcof,icof,nps)
       ysa = ysa - smc
       call pscalee (ysa,ysa,res,npt,kic,nic)

       !bl2 = sqrt(bl2+1.d-40)

       !write(*,*)k,residu
       !write(77,*)k,residu/bl2
       
       
       if (res<1d20 .and. res>1d-32) then
          res = sqrt(res+1.d-40)
       else
          res = 1d-16
       endif

       !-------------------------------------------------------------------------------
       ! normalisation with sec_membre
       !-------------------------------------------------------------------------------
       res=res/bl20
       !-------------------------------------------------------------------------------
       
       if (res < restemp) then
          restemp = res
          soltemp = sol
       endif
       !-------------------------------------------------------------------------------
       if (ires>0 .and. modulo(kt,ires)==0) then
          if (rank==0) then
             !write(50,*) kt,res
             write(*,102) kt,res*bl20,res/rl20,res
             !write(*,102) kt,res*bl2,res/rl2,res
          end if
       end if
       !-------------------------------------------------------------------------------
       if (res <= solver_threshold) exit
    end do

    !-------------------------------------------------------------------------------
    ! restart cases
    !-------------------------------------------------------------------------------
    if (res>100*solver_threshold.and.abs(1-res/rl20)<0.5d0) then
       icode=1
    end if
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    kt=min(kt,solver_it)
    !-------------------------------------------------------------------------------
    sol = soltemp
    res = restemp
    rhs = rsa
    !-------------------------------------------------------------------------------
    
    
    if (nproc>1) call solver_comm_mpi(sol,npt,0)
    
    deallocate(psa,qsa,rsa,soltemp,ssa,tsa, usa,vsa,wsa,ysa,msa)
    deallocate(coefb,jcofb,icofb)
    deallocate(coef_uv,jcof_uv,icof_uv)
    if (dim==3) deallocate(jcof_uvw,icof_uvw,coef_uvw)
    
101 format(a5,1x,3(a20,1x))
102 format(i5,1x,9(1pe20.6,1x))


    call compute_time(TIMER_END,"[sub] solver_FC_BiCGStab2_iLU")
  end subroutine solver_FC_BiCGStab2_iLU

  !-------------------------------------------------------------------------------
  ! Matrix-vector product
  !-------------------------------------------------------------------------------
  subroutine matveke_tri_sup (npt1,npt2,sol,ysa,coef,jcof,icof,nps1)
    !*******************************************************************************            
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)                  :: npt1,nps1,npt2
    integer, dimension(:),allocatable,intent(in) :: icof
    integer, dimension(:),allocatable,intent(in)  :: jcof
    !-------------------------------------------------------------------------------
    real(8), dimension(:),allocatable, intent(in) :: coef
    real(8), dimension(:),allocatable, intent(in) :: sol
    real(8),  dimension(npt2), intent(out):: ysa
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                              :: i,k
    !-------------------------------------------------------------------------------
    real(8)                              :: t
    !-------------------------------------------------------------------------------

    call compute_time(TIMER_START,"[sub] matveke_tri_sup")

    ysa= 0.d0

    !-------------------------------------------------------------------------------
    do i = 1,npt2
       t = 0.0d0
       do k = icof(i),icof(i+1)-1
          if (jcof(k)<=0) then
             t=t
          else
             t = t + coef(k)*sol(jcof(k))
          endif
          !c     !write(*,*)"t",t
       enddo
       ysa(i) = t
    enddo
    !-------------------------------------------------------------------------------
    call compute_time(TIMER_END,"[sub] matveke_tri_sup")
  end subroutine matveke_tri_sup

  subroutine SMG_solver(coef,jcof,msa,qsa,&
       SAp,solverp,Sau,solveru,Sav,solverv,Saw,solverw,vie,&
       coef_pcd,jcof_pcd,icof_pcd,coefb,jcofb,icofb,coef_uv,jcof_uv,icof_uv,&
       icof_uvw,jcof_uvw,coef_uvw)
    use mod_Parameters, only: rank,ires,       &
         & dim,nb_Vx,nb_Vy,                    &
         & ex,sx,ey,sy,ez,sz,gx,gy,gz,         &
         & gsx,gex,gsy,gey,gsz,gez,            &
         & sxu,exu,syu,eyu,szu,ezu,            &
         & sxv,exv,syv,eyv,szv,ezv,            &
         & sxw,exw,syw,eyw,szw,ezw,            &
         & sxs,exs,sys,eys,szs,ezs,            &
         & sxus,exus,syus,eyus,szus,ezus,      &
         & sxvs,exvs,syvs,eyvs,szvs,ezvs,      &
         & sxws,exws,syws,eyws,szws,ezws,      &
         & t1_tab2sol,t2_tab2sol,NS_method,nb_vz,nb_p_no_ghost, &
         & nb_p_no_ghost,nb_vx_no_ghost,nb_vy_no_ghost, &
         & nb_vz_no_ghost,dx,dy,dz,nb_p,nb_v,nb_vp, &
         & NS_HYPRE_precond,                   &
         & deeptracking
    use mod_mpi
    implicit none
    include 'HYPREf.h'

    real(8), dimension(:),intent(INout),allocatable::qsa
    real(8), dimension(:),intent(INout),allocatable::msa
    real(8), dimension(:),intent(IN),allocatable::coef,coef_pcd,coef_uv,coefb,coef_uvw
    integer, dimension(:),intent(IN),allocatable::jcof,jcof_pcd,icof_pcd,jcofb,icofb,jcof_uv,icof_uv,icof_uvw,jcof_uvw
    integer*8,intent(in)::solverp,Sau,solveru,Sav,solverv,solverw,SAw,Sap
    real(8), dimension(:,:,:),intent(IN),allocatable::vie
    integer*8  x
    integer*8  y
    integer    ls,npt_pcd
    integer    nvars
    integer    part
    integer    varu,varv,varp
    integer    object_type
    integer    ilower(dim), iupper(dim)
    integer    vartypes(1)
    integer    ent,lvs
    integer    nentries, nvalues, stencil_indices_1(dim*2+1)
    integer    xvar(dim*2+1),yvar(dim*2+1),pvar(1)
    integer    num_iterations,i,j,ierr,k,l,lvp,z,ii,jj,kk,c
    real(8)    final_res_norm,r
    real(8)    final_res_norm_precondu,final_res_norm_precondv,final_res_norm_precondw
    real(8)    final_res_norm_precondp
    integer*8  ssy
    integer*8  ssx
    real(8),dimension(:),allocatable::xp,p,viscosityp,valuesp,valuesv,valuesu,valuesw,px,p1
    real(8),dimension(:),allocatable::w1,ysa,bp,w2,zsa
    integer :: job,nzmax
    real(8) ::t,t1,t2

    call compute_time(TIMER_START,"[sub] SMG_solver")

    !---------------------------------------------------------------------
    allocate(w1(nb_vy),Bp(nb_V),ysa(nb_vx))
    w1=0
    bp=0
    ysa=0
    !---------------------------------------------------------------------
    ! Pressure Varibale
    !---------------------------------------------------------------------
    allocate(viscosityp(nb_p_no_ghost),xp(nb_p_no_ghost), &
         & p(nb_p),valuesp(nb_p_no_ghost),px(nb_p))
    viscosityp=0
    xp=0
    p=0
    valuesp=0
    px=0
    allocate(p1(nb_p))
    p1=0
    !---------------------------------------------------------------------
    ! Velocity u variable
    !---------------------------------------------------------------------
    allocate(valuesu(nb_vx_no_ghost))
    valuesu=0
    !---------------------------------------------------------------------
    ! Velocity v variable
    !---------------------------------------------------------------------
    allocate(valuesv(nb_vy_no_ghost))
    valuesv=0
    !---------------------------------------------------------------------
    ! Velocity w variable
    !---------------------------------------------------------------------
    if (dim==3) then
       allocate(valuesw(nb_vz_no_ghost))
       valuesw=0
       allocate(w2(nb_vz),zsa(nb_vx+nb_vy))
       w2=0
       zsa=0
    endif
    !---------------------------------------------------------------------

    msa = qsa
    nvars  = 1
    Part   = 0
    Object_type = HYPRE_STRUCT
    varu  = 0

    xp(:)= 0.D0
    p(:) =0.D0
    if (dim==3) then
       zsa(:)=0.d0
    endif
    npt_pcd=nb_p*(2*dim+1)

    !----------------------------------------------------------------------
    ! Approximate inverse Schur complement ( Pressure connvection diffusion )
    !---------------------------------------------------------------------
    if (dim==2) then
       c=0
       do jj=sy,ey
          do ii=sx,ex
             i=ii-sxs
             j=jj-sys
             lvp=i+j*(exs-sxs+1)+1+nb_Vx+nb_Vy
             ls=i+j*(exs-sxs+1)+1!+nb_Vx+nb_Vy
             c = c+1
             p(c) = qsa(lvp)/(dx(ii)*dy(jj))
             p1(ls)=qsa(lvp)/(dx(ii)*dy(jj))
          end do
       end do
    elseif (dim==3) then
       c=0
       do kk=sz,ez
          do jj=sy,ey
             do ii=sx,ex
                i=ii-sxs
                j=jj-sys
                k=kk-szs
                l=i+j*(exs-sxs+1)&
                     & +k *(exs-sxs+1)*(eys-sys+1)+1+nb_Vx+nb_Vy+nb_Vz
                ls=i+j*(exs-sxs+1)&
                     & +k *(exs-sxs+1)*(eys-sys+1)+1
                c=c+1
                p(c)  =qsa(l)/(dx(ii)*dy(jj)*dz(kk))
                p1(ls)=qsa(l)/(dx(ii)*dy(jj)*dz(kk))
             end do
          end do
       end do
    endif

    if (nproc>1) call solver_comm_mpi(p1,nb_p,1)
    call matveke_tri_sup(nb_P,nb_p,p1,px,coef_PCD,jcof_PCD,icof_PCD,npt_pcd)
    
    call HYPRE_SStructVectorCreate(comm3d,grid_hypre_p,y,ierr)
    call HYPRE_SStructVectorCreate(comm3d,grid_hypre_p,x,ierr)

    call HYPRE_SStructVectorSetObjectTyp(x,object_type,ierr)
    call HYPRE_SStructVectorSetObjectTyp(y,object_type,ierr)

    call HYPRE_SStructVectorInitialize(y,ierr)
    call HYPRE_SStructVectorInitialize(x,ierr)

    if (dim==2) then
       ilower(1) = sx
       ilower(2) = sy
       iupper(1) = ex
       iupper(2) = ey
    else
       ilower(1) = sx
       ilower(2) = sy
       ilower(3) = sz
       iupper(1) = ex
       iupper(2) = ey
       iupper(3) = ez
    endif

    do i = 1, nb_p_no_ghost
       valuesp(i) = 0.d0
    enddo
    call HYPRE_SStructVectorSetBoxValues(x,part,ilower,iupper,varu,valuesp,ierr)

    valuesp(:)=0.0d0

    if (dim==2) then
       c=0
       do jj=sy,ey
          do ii=sx,ex
             i=ii-sxs
             j=jj-sys
             lvp=i+j*(exs-sxs+1)+1+nb_Vx+nb_Vy
             ls=i+j*(exs-sxs+1)+1!+nb_Vx+nb_Vy
             c=c+1
             valuesp(c)=px(ls)
          end do
       end do
    else
       c=0
       do kk=sz,ez
          do jj=sy,ey
             do ii=sx,ex
                i=ii-sxs
                j=jj-sys
                k=kk-szs
                l=i+j*(exs-sxs+1)&
                     & +k *(exs-sxs+1)*(eys-sys+1)+1
                c=c+1
                valuesp(c)=px(l)
             end do
          end do
       end do
    endif

    !valuesp(1)=0.0d0

    if (dim==2) then
       ilower(1) = sx
       ilower(2) = sy
       iupper(1) = ex
       iupper(2) = ey
    else
       ilower(1) = sx
       ilower(2) = sy
       ilower(3) = sz
       iupper(1) = ex
       iupper(2) = ey
       iupper(3) = ez
    endif

    call HYPRE_SStructVectorSetBoxValues(y,part,ilower,iupper,varu,valuesp,ierr)

    call HYPRE_SStructVectorAssemble(x,ierr)
    call HYPRE_SStructVectorAssemble(y,ierr)

    call HYPRE_SStructVectorGetObject(y,ssy,ierr)
    call HYPRE_SStructVectorGetObject(x,ssx,ierr)

    select case(NS_HYPRE_precond)
    case(0)
       call HYPRE_StructPFMGSolve(solverp,sAp,ssy,ssx,ierr)
       call HYPRE_StructPFMGGetFinalRelativ(solverp,final_res_norm_precondp,ierr)
    case(1)
       call HYPRE_StructSMGSolve(solverp,sAp,ssy,ssx,ierr)
       call HYPRE_StructSMGGetNumIterations(solverp,num_iterations,ierr)
       call HYPRE_StructSMGGetFinalRelative(solverp,final_res_norm_precondp,ierr)
    end select

    xp=0

    !call  HYPRE_SStructVectorGather(x,ierr)

    call  HYPRE_SStructVectorGetBoxValues(x,PART,ilower,iupper,varu,xp,ierr)

    call HYPRE_SStructVectorDestroy(y,ierr)
    call HYPRE_SStructVectorDestroy(x,ierr)

    !!xp = xp - sum(xp)/nb_P

    p(:)=0.D0

    do i=1,nb_p_no_ghost
       if (abs(xp(i)) < 1.d-16 ) then
          xp(i)=0.0d0
       endif
    enddo

    if (dim==2) then
       c=0
       do jj=sy,ey
          do ii=sx,ex
             i=ii-sxs
             j=jj-sys
             lvp=i+j*(exs-sxs+1)+1
             c=c+1
             viscosityp(c)=vie(ii,jj,1)
             !rep_al(c)    =rep(ii,jj,1)
          end do
       end do
    else
       c=0
       do kk=sz,ez
          do jj=sy,ey
             do ii=sx,ex
                i=ii-sxs
                j=jj-sys
                k=kk-szs
                c=c+1
                viscosityp(c)=vie(ii,jj,kk)
             end do
          end do
       end do
    endif

    if (dim==2) then
       p=0.0d0
       c=0
       do jj=sy,ey
          do ii=sx,ex
             i=ii-sxs
             j=jj-sys
             lvp=i+j*(exs-sxs+1)+1+nb_Vx+nb_Vy
             ls=i+j*(exs-sxs+1)+1!+nb_Vx+nb_Vy
             c=c+1
             msa(lvp)=xp(c)+p1(ls)*viscosityp(c)
             p(ls)=msa(lvp)
          end do
       end do
    else
       p=0.0d0
       c=0
       do kk=sz,ez
          do jj=sy,ey
             do ii=sx,ex
                i=ii-sxs
                j=jj-sys
                k=kk-szs
                lvp=i+j*(exs-sxs+1)&
                     & +k *(exs-sxs+1)*(eys-sys+1)+1+nb_Vx+nb_Vy+nb_Vz
                ls=i+j*(exs-sxs+1)&
                     & +k *(exs-sxs+1)*(eys-sys+1)+1
                c=c+1
                msa(lvp)=xp(c)+p1(ls)*viscosityp(c)
                !msa(lvp)=p1(ls)*viscosityp(c) ! sans l'operateur PCD
                p(ls)=msa(lvp)
             end do
          end do
       end do
    end if

    do i=1+nb_v,nb_vp
       k=i-nb_v
       p(k)   = msa(i)
    enddo

    !----------------------------------------------------------------------
    ! Calcul of bp = Bp * P
    !----------------------------------------------------------------------
    Bp = 0.d0

    if (nproc>1) call solver_comm_mpi(p,nb_p,1)
    call matveke_tri_sup(nb_p,nb_v,p,Bp,coefb,jcofb,icofb,2*nb_V)
    !Bp = 0.d0

    if (dim==3) then
       !----------------------------------------------------------------------
       ! Approximate inverse of the BLock F_ww velocity
       !---------------------------------------------------------------------

       call HYPRE_SStructVectorCreate(comm3d,grid_hypre_w,y,ierr)
       call HYPRE_SStructVectorCreate(comm3d,grid_hypre_w,x,ierr)

       call HYPRE_SStructVectorSetObjectTyp(x,object_type,ierr)
       call HYPRE_SStructVectorSetObjectTyp(y,object_type,ierr)

       call HYPRE_SStructVectorInitialize(y,ierr)
       call HYPRE_SStructVectorInitialize(x,ierr)

       ilower(1) = sxw
       ilower(2) = syw
       ilower(3) = szw

       iupper(1) = exw
       iupper(2) = eyw
       iupper(3) = ezw

       do i = 1, nb_vz_no_ghost
          valuesw(i) =0.d0
       enddo
       call HYPRE_SStructVectorSetBoxValues(x,part,ilower,iupper,varu,valuesw,ierr)

       valuesw(:)=0.d0

!!$       do i = 1,nb_vz
!!$          j= i + nb_vx+nb_vy
!!$          valuesw(i) = qsa(j)-bp(j)
!!$       enddo

       c=0

       do kk=szw,ezw
          do jj=syw,eyw
             do ii=sxw,exw
                i=ii-sxws
                j=jj-syws
                k=kk-szws
                l=i+j*(exws-sxws+1)&
                     & +k*(exws-sxws+1)*(eyws-syws+1)+1+nb_Vx+nb_Vy
                c=c+1
                valuesw(c)=qsa(l)-bp(l)
             end do
          end do
       end do

       call HYPRE_SStructVectorSetBoxValues(y,part,ilower,iupper,varu,valuesw,ierr)

       call HYPRE_SStructVectorAssemble(x,ierr)
       call HYPRE_SStructVectorAssemble(y,ierr)

       call HYPRE_SStructVectorGetObject(y,ssy,ierr)
       call HYPRE_SStructVectorGetObject(x,ssx,ierr)

       select case(NS_HYPRE_precond)
       case(0)
          call HYPRE_StructPFMGSolve(solverw,sAw,ssy,ssx,ierr)
          call HYPRE_StructPFMGGetFinalRelativ(solverw,final_res_norm_precondw,ierr)
       case(1)
          call HYPRE_StructSMGSolve(solverw,sAw,ssy,ssx,ierr)
          call HYPRE_StructSMGGetNumIterations(solverw,num_iterations,ierr)
          call HYPRE_StructSMGGetFinalRelative(solverw,final_res_norm_precondw,ierr)
       end select

       valuesw(:)=0.d0

       !call  HYPRE_SStructVectorGather(x,ierr)

       ilower(1) = sxw
       ilower(2) = syw
       ilower(3) = szw

       iupper(1) = exw
       iupper(2) = eyw
       iupper(3) = ezw

       call  HYPRE_SStructVectorGetBoxValues(x,PART,ilower,iupper,varu,valuesw,ierr)

       c=0
       do kk=szw,ezw
          do jj=syw,eyw
             do ii=sxw,exw
                i=ii-sxws
                j=jj-syws
                k=kk-szws
                l=i+j*(exws-sxws+1)&
                     & +k*(exws-sxws+1)*(eyws-syws+1)+1+nb_Vx+nb_Vy
                ls=i+j*(exws-sxws+1)&
                     & +k*(exws-sxws+1)*(eyws-syws+1)+1
                c=c+1
                msa(l) = valuesw(c)
                w2(ls) = msa(l)
             end do
          end do
       end do

       call HYPRE_SStructVectorDestroy(y,ierr)
       call HYPRE_SStructVectorDestroy(x,ierr) 

       !----------------------------------------------------------------------
       ! Calcul of zsa = Fuv_w * P
       !----------------------------------------------------------------------

       zsa = 0.0d0
       if (nproc>1) call solver_comm_mpi(w2,nb_vz,4)
       call matveke_tri_sup (nb_vx+nb_vy,nb_vy,w2,zsa,coef_uvw,jcof_uvw,icof_uvw,4*(nb_vx+nb_vy))
       ! zsa = 0.0D0

    endif

    !----------------------------------------------------------------------
    ! Approximate inverse of the BLock F_vv velocity
    !---------------------------------------------------------------------

    call HYPRE_SStructVectorCreate(comm3d,grid_hypre_v,y,ierr)
    call HYPRE_SStructVectorCreate(comm3d,grid_hypre_v,x,ierr)

    call HYPRE_SStructVectorSetObjectTyp(x,object_type,ierr)
    call HYPRE_SStructVectorSetObjectTyp(y,object_type,ierr)

    call HYPRE_SStructVectorInitialize(y,ierr)
    call HYPRE_SStructVectorInitialize(x,ierr)

    if (dim==2) then
       ilower(1) = sxv
       ilower(2) = syv
       iupper(1) = exv
       iupper(2) = eyv
    else 
       ilower(1) = sxv
       ilower(2) = syv
       ilower(3) = szv
       iupper(1) = exv
       iupper(2) = eyv
       iupper(3) = ezv
    endif

!!$    do i = 1, nb_vy
!!$       valuesv(i) =0.d0
!!$    enddo

    do i = 1, nb_vy_no_ghost
       valuesv(i) =0.d0
    enddo
    call HYPRE_SStructVectorSetBoxValues(x,part,ilower,iupper,varu,valuesv,ierr)

    valuesv(:)=0.d0

!!$    do i = 1,nb_vy
!!$       j= i + nb_vx
!!$       ! k= i + nb_vx + nb_vy
!!$       if (dim==2) then
!!$          valuesv(i) = qsa(j)-bp(j)
!!$       elseif (dim==3) then
!!$          valuesv(i) = qsa(j)-bp(j)-zsa(j)
!!$       end if
!!$    enddo

    if (dim==2) then
       c=0
       do jj=syv,eyv
          do ii=sxv,exv
             i=ii-sxvs
             j=jj-syvs
             lvp=i+j*(exvs-sxvs+1)+1+nb_Vx
             ls=i+j*(exvs-sxvs+1)+1!+nb_Vx+nb_Vy
             c=c+1
             valuesv(c) = qsa(lvp)-bp(lvp)
          end do
       end do
    else
       c=0
       do kk=szv,ezv
          do jj=syv,eyv
             do ii=sxv,exv
                i=ii-sxvs
                j=jj-syvs
                k=kk-szvs 
                lvp=i+j*(exvs-sxvs+1)&
                     & +k*(exvs-sxvs+1)*(eyvs-syvs+1)+1+nb_Vx
                c=c+1
                valuesv(c) = qsa(lvp)-bp(lvp)-zsa(lvp)
             end do
          end do
       end do
    endif

    call HYPRE_SStructVectorSetBoxValues(y,part,ilower,iupper,varu,valuesv,ierr)

    call HYPRE_SStructVectorAssemble(x,ierr)
    call HYPRE_SStructVectorAssemble(y,ierr)

    call HYPRE_SStructVectorGetObject(y,ssy,ierr)
    call HYPRE_SStructVectorGetObject(x,ssx,ierr)

    select case(NS_HYPRE_precond)
    case(0)       
       call HYPRE_StructPFMGSolve(solverv,sAv,ssy,ssx,ierr)
       call HYPRE_StructPFMGGetFinalRelativ(solverv,final_res_norm_precondv,ierr)
    case(1)
       call HYPRE_StructSMGSolve(solverv,sAv,ssy,ssx,ierr)
       call HYPRE_StructSMGGetNumIterations(solverv,num_iterations,ierr)
       call HYPRE_StructSMGGetFinalRelative(solverv,final_res_norm_precondv,ierr)
    end select

    valuesv(:)=0.d0

    ! call  HYPRE_SStructVectorGather(x,ierr)

    if (dim==2) then
       ilower(1) = sxv
       ilower(2) = syv
       iupper(1) = exv
       iupper(2) = eyv
    else
       ilower(1) = sxv
       ilower(2) = syv
       ilower(3) = szv
       iupper(1) = exv
       iupper(2) = eyv
       iupper(3) = ezv
    endif

    call  HYPRE_SStructVectorGetBoxValues(x,PART,ilower,iupper,varu,valuesv,ierr)

!!$    do i=1+nb_vx,nb_vy+nb_vx
!!$       j= i - nb_vx
!!$       msa(i)= valuesv(j)
!!$       w1(j) = msa(i)
!!$    enddo

    if (dim==2) then
       c=0
       do jj=syv,eyv
          do ii=sxv,exv
             i=ii-sxvs
             j=jj-syvs
             lvp=i+j*(exvs-sxvs+1)+1+nb_Vx
             ls=i+j*(exvs-sxvs+1)+1!+nb_Vx+nb_Vy
             c=c+1
             msa(lvp)= valuesv(c)
             w1(ls)  = msa(lvp)
          end do
       end do
    else
       c=0
       do kk=szv,ezv
          do jj=syv,eyv
             do ii=sxv,exv
                i=ii-sxvs
                j=jj-syvs
                k=kk-szvs 
                l=i+j*(exvs-sxvs+1)&
                     & +k*(exvs-sxvs+1)*(eyvs-syvs+1)+1+nb_Vx
                ls=i+j*(exvs-sxvs+1)&
                     & +k*(exvs-sxvs+1)*(eyvs-syvs+1)+1
                c=c+1
                msa(l) = valuesv(c)
                w1(ls) = msa(l)
             end do
          end do
       end do
    endif

    call HYPRE_SStructVectorDestroy(y,ierr)
    call HYPRE_SStructVectorDestroy(x,ierr) 

    ysa = 0.d0

    if (nproc>1) call solver_comm_mpi(w1,nb_vy,3)
    call  matveke_tri_sup(nb_vy,nb_vx,w1,ysa,coef_uv,jcof_uv,icof_uv,4*nb_vx)
    ! call matveke (nb_vx,w1,ysa,coef_uv,jcof_uv,icof_uv,4*nb_vx)

    !ysa(:)= 0.d0

!!$       do i = 1,nb_vx
!!$          t = 0.0d0
!!$          do k = icof_uv(i),icof_uv(i+1)-1
!!$             t = t + coef_uv(k)*w1(jcof_uv(k))
!!$          enddo
!!$          if (abs(t) < 1.d-30) then
!!$             t = 0.d0
!!$          endif
!!$          ysa(i) = t
!!$       enddo

    !----------------------------------------------------------------------
    ! Approximate inverse of the BLock F_uu velocity
    !---------------------------------------------------------------------

    call HYPRE_SStructVectorCreate(comm3d,grid_hypre_u,y,ierr)
    call HYPRE_SStructVectorCreate(comm3d,grid_hypre_u,x,ierr)

    call HYPRE_SStructVectorSetObjectTyp(x,object_type,ierr)
    call HYPRE_SStructVectorSetObjectTyp(y,object_type,ierr)

    call HYPRE_SStructVectorInitialize(y,ierr)
    call HYPRE_SStructVectorInitialize(x,ierr)

    if (dim==2) then
       ilower(1) = sxu
       ilower(2) = syu
       iupper(1) = exu
       iupper(2) = eyu
    else
       ilower(1) = sxu
       ilower(2) = syu
       ilower(3) = szu
       iupper(1) = exu
       iupper(2) = eyu
       iupper(3) = ezu
    endif

!!$       do i = 1, nb_vx
!!$          valuesu(i) =0.d0
!!$       enddo

    do i = 1, nb_vx_no_ghost
       valuesu(i) =0.d0
    enddo
    call HYPRE_SStructVectorSetBoxValues(x,part,ilower,iupper,varu,valuesu,ierr)

    valuesu=0.0d0
!!$       do i = 1, nb_vx
!!$          if (dim==2) then
!!$             valuesu(i) = qsa(i)-bp(i)-ysa(i)
!!$          elseif (dim==3) then
!!$             valuesu(i) = qsa(i)-bp(i)-ysa(i)-zsa(i)
!!$          endif
!!$       enddo

    if (dim==2) then
       c=0
       do jj=syu,eyu
          do ii=sxu,exu
             i=ii-sxus
             j=jj-syus
             lvp=i+j*(exus-sxus+1)+1
             ls=i+j*(exus-sxus+1)+1!+nb_Vx+nb_Vy
             c=c+1
             valuesu(c)=qsa(lvp)-bp(lvp)-ysa(lvp)
          end do
       end do
    else
       c=0
       do kk=szu,ezu
          do jj=syu,eyu
             do ii=sxu,exu
                i=ii-sxus
                j=jj-syus
                k=kk-szus 
                lvp=i+j*(exus-sxus+1)&
                     & +k*(exus-sxus+1)*(eyus-syus+1)+1
                c=c+1
                valuesu(c)=qsa(lvp)-bp(lvp)-ysa(lvp)-zsa(lvp)
             end do
          end do
       end do
    endif

    call HYPRE_SStructVectorSetBoxValues(y,part,ilower,iupper,varu,valuesu,ierr)

    call HYPRE_SStructVectorAssemble(x,ierr)
    call HYPRE_SStructVectorAssemble(y,ierr)

    call HYPRE_SStructVectorGetObject(y,ssy,ierr)
    call HYPRE_SStructVectorGetObject(x,ssx,ierr)
    select case(NS_HYPRE_precond)
    case(0)
       call HYPRE_StructPFMGSolve(solveru,sAu,ssy,ssx,ierr)
       !call HYPRE_StructPFMGGetNumIteration(solveru,num_iterations,ierr)
       call HYPRE_StructPFMGGetFinalRelativ(solveru,final_res_norm_precondu,ierr)
    case(1)
       call HYPRE_StructSMGSolve(solveru,sAu,ssy,ssx,ierr)
       call HYPRE_StructSMGGetNumIterations(solveru,num_iterations,ierr)
       call HYPRE_StructSMGGetFinalRelative(solveru,final_res_norm_precondu,ierr)
    end select

    if (deeptracking) then 
       if (rank==0) then
          if (ires>0) then
             if (dim==2) then 
                write(*,101)"Puvp", &
                     & final_res_norm_precondu, &
                     & final_res_norm_precondv, &
                     & final_res_norm_precondp
             else
                write(*,101)"Puvwp", &
                     & final_res_norm_precondu, &
                     & final_res_norm_precondv, &
                     & final_res_norm_precondw, &
                     & final_res_norm_precondp
             end if
          end if
       end if
    end if
    
    valuesu = 0.0d0

    ! call  HYPRE_SStructVectorGather(x,ierr)

    if (dim==2) then
       ilower(1) = sxu
       ilower(2) = syu
       iupper(1) = exu
       iupper(2) = eyu
    else
       ilower(1) = sxu
       ilower(2) = syu
       ilower(3) = szu
       iupper(1) = exu
       iupper(2) = eyu
       iupper(3) = ezu
    endif

    call  HYPRE_SStructVectorGetBoxValues(x,PART,ilower,iupper,varu,valuesu,ierr)

!!$       do i=1,nb_vx
!!$          msa(i)=  valuesu(i)
!!$          !write(77,*)i,valuesu(i)
!!$       enddo

    if (dim==2) then
       c=0
       do jj=syu,eyu
          do ii=sxu,exu
             i=ii-sxus
             j=jj-syus
             lvp=i+j*(exus-sxus+1)
             ls=i+j*(exus-sxus+1)+1!+nb_Vx+nb_Vy
             c=c+1
             msa(ls)=valuesu(c)
          end do
       end do
    else
       c=0
       do kk=szu,ezu
          do jj=syu,eyu
             do ii=sxu,exu
                i=ii-sxus
                j=jj-syus
                k=kk-szus 
                l=i+j*(exus-sxus+1)&
                     & +k*(exus-sxus+1)*(eyus-syus+1)+1
                c=c+1
                msa(l)=valuesu(c)
             end do
          end do
       end do
    endif

    call HYPRE_SStructVectorDestroy(y,ierr)
    call HYPRE_SStructVectorDestroy(x,ierr)

    !  msa(1:Nb_vx) = qsa(1:Nb_vx)
    !msa = qsa 

    deallocate(viscosityp,xp,p,valuesp,valuesu,valuesv,px,p1)
    deallocate(w1,Bp,ysa)
    if (dim==3) then
       deallocate(valuesw)
       deallocate(w2,zsa)
    endif

101 format(a5,1x,5(1pe20.6,1x))
  
    call compute_time(TIMER_END,"[sub] SMG_solver")
  end subroutine SMG_solver

  !-----------------------------------------------------------------------
  ! Construct the Block Bp of pressure gradient
  !-----------------------------------------------------------------------
  subroutine construct_Bu(icof,jcof,coef,icofbu,jcofbu,coefbu)
    use mod_Parameters, only: dim,nb_Vx,nb_Vy, &
         & sxs,exs,sys,eys,szs,ezs,            &
         & nb_V,nb_p
    implicit none

    integer, dimension(:),allocatable,intent(in) :: icof
    integer, dimension(:),allocatable,intent(in)  :: jcof
    !-------------------------------------------------------------------------------
    real(8), dimension(:),allocatable, intent(in)    :: coef
    real(8), dimension(:),allocatable, intent(inout) :: coefbu
    integer, dimension(:),allocatable,intent(inout)  :: icofbu
    integer, dimension(:),allocatable,intent(inout)  :: jcofbu
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                              :: i,k,j,z,lvs
    !-------------------------------------------------------------------------------

    call compute_time(TIMER_START,"[sub] construct_Bu")

    if (dim==2) then
       lvs=1+11*nb_V
       k =1
       do j=sys,eys
          do i=sxs,exs
             coefbu(k)  =coef(lvs) 
             coefbu(k+1)=coef(lvs+1)
             coefbu(k+2)=coef(lvs+2)
             coefbu(k+3)=coef(lvs+3)
             jcofbu(k)  =jcof(lvs)
             jcofbu(k+1)=jcof(lvs+1)
             jcofbu(k+2)=jcof(lvs+2)
             jcofbu(k+3)=jcof(lvs+3)
             k=k+4
             lvs=lvs+4
          enddo
       enddo

       icofbu(1)= 1
       do k=2,nb_p+1
          icofbu(k)= icofbu(k-1) +4 
       enddo
    else
       write(*,*) "STOP, construct_Bu not done in 3D"
       write(*,*) "STOP, construct_Bu not done in 3D"
       write(*,*) "STOP, construct_Bu not done in 3D"
       write(*,*) "STOP, construct_Bu not done in 3D"
       write(*,*) "STOP, construct_Bu not done in 3D"
       stop
    endif

    call compute_time(TIMER_END,"[sub] construct_Bu")
  end subroutine construct_Bu

  !-----------------------------------------------------------------------
  ! Construct Blcok Bp of pressure gradient
  !-----------------------------------------------------------------------
  subroutine construct_Bp(icof,jcof,coef,icofb,jcofb,coefb)

    use mod_Parameters, only: dim,nb_Vx,nb_Vy, &
         & sxus,exus,syus,eyus,szus,ezus,      &
         & sxvs,exvs,syvs,eyvs,szvs,ezvs,      &
         & sxws,exws,syws,eyws,szws,ezws,      &
         & nb_V
    implicit none

    integer, dimension(:),allocatable,intent(in) :: icof
    integer, dimension(:),allocatable,intent(in)  :: jcof
    !-------------------------------------------------------------------------------
    real(8), dimension(:),allocatable, intent(in)    :: coef
    real(8), dimension(:),allocatable, intent(inout) :: coefb
    integer, dimension(:),allocatable,intent(inout)  :: icofb
    integer, dimension(:),allocatable,intent(inout)  :: jcofb
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                              :: i,k,j,z,lvs
    !-------------------------------------------------------------------------------

    call compute_time(TIMER_START,"[sub] construct_Bp")

    if (dim==2) then
       coefb=0.d0
       jcofb=0
       k=1
       lvs=10
       do j=syus,eyus
          do i=sxus,exus
             coefb(k)  =coef(lvs) 
             coefb(k+1)=coef(lvs+1)
             jcofb(k)  =jcof(lvs)-nb_V
             jcofb(k+1)=jcof(lvs+1)-nb_V
             k=k+2
             lvs=lvs+11
          enddo
       enddo

       lvs=10+11*nb_Vx
       k=1+2*nb_Vx
       do j=syvs,eyvs
          do i=sxvs,exvs
             coefb(k)  =coef(lvs) 
             coefb(k+1)=coef(lvs+1)
             jcofb(k)  =jcof(lvs)-nb_V
             jcofb(k+1)=jcof(lvs+1)-nb_V
             k=k+2
             lvs=lvs+11
          enddo
       enddo

       icofb(1)= 1

       do k=2,nb_v+1
          icofb(k)= icofb(k-1) +2 
       enddo
    else
       coefb=0
       jcofb=0
       k=1
       lvs=16
       do z=szus,ezus
          do j=syus,eyus
             do i=sxus,exus
                coefb(k)  =coef(lvs) 
                coefb(k+1)=coef(lvs+1)
                jcofb(k)  =jcof(lvs)-nb_V
                jcofb(k+1)=jcof(lvs+1)-nb_V
                k=k+2
                lvs=lvs+17
             enddo
          enddo
       enddo
       lvs=16+17*nb_Vx
       k=1+2*nb_Vx
       do z=szvs,ezvs
          do j=syvs,eyvs
             do i=sxvs,exvs
                coefb(k)  =coef(lvs) 
                coefb(k+1)=coef(lvs+1)
                jcofb(k)  =jcof(lvs)-nb_V
                jcofb(k+1)=jcof(lvs+1)-nb_V
                k=k+2
                lvs=lvs+17
             enddo
          enddo
       enddo

       lvs=16+17*(nb_Vx+nb_vy)
       k=1+2*(nb_Vx+nb_vy)
       do z=szws,ezws
          do j=syws,eyws
             do i=sxws,exws
                coefb(k)  =coef(lvs) 
                coefb(k+1)=coef(lvs+1)
                jcofb(k)  =jcof(lvs)-nb_V
                jcofb(k+1)=jcof(lvs+1)-nb_V
                k=k+2
                lvs=lvs+17
             enddo
          enddo
       enddo

       icofb(1)= 1

       do k=2,nb_v+1
          icofb(k)= icofb(k-1) +2 
       enddo
    endif

    call compute_time(TIMER_END,"[sub] construct_Bp")
  end subroutine construct_Bp

  !-----------------------------------------------------------------------
  ! Construct Blcok Fuv of velocity
  !-----------------------------------------------------------------------
  subroutine construct_Fuv(icof,jcof,coef,icofuv,jcofuv,coefuv)
    use mod_Parameters, only: dim,nb_Vx,nb_Vy, &
         & sxus,exus,syus,eyus,szus,ezus
    implicit none

    integer, dimension(:),allocatable,intent(in) :: icof
    integer, dimension(:),allocatable,intent(in)  :: jcof
    !-------------------------------------------------------------------------------
    real(8), dimension(:),allocatable, intent(in)    :: coef
    real(8), dimension(:),allocatable, intent(inout) :: coefuv
    integer, dimension(:),allocatable,intent(inout)  :: icofuv
    integer, dimension(:),allocatable,intent(inout)  :: jcofuv
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                              :: i,k,j,z,lvs,l
    !-------------------------------------------------------------------------------

    call compute_time(TIMER_START,"[sub] construct_Fuv")

    ! construction du block F_uv
    if (dim==2) then
       l=6
       k=1
       do j=syus,eyus
          do i=sxus,exus
             coefuv(k)= coef (l)
             coefuv(k+1)= coef (l+1)
             coefuv(k+2)= coef (l+2)
             coefuv(k+3)= coef (l+3)
             jcofuv(k)= jcof(l)-nb_vx
             jcofuv(k+1)= jcof (l+1)-nb_Vx
             jcofuv(k+2)= jcof (l+2)-nb_vx
             jcofuv(k+3)= jcof (l+3)-nb_vx
             k=k+4
             l=l+11
          enddo
       enddo

       do k=1,nb_vx*4
          if (abs(coefuv(k)) < 1.d-30) then
             coefuv(k) = 0.d0
          endif
       enddo

       icofuv(1)=1

       do l=2,nb_vx+1
          icofuv(l)= icofuv(l-1)+4
       enddo
    else
       l=8
       k=1
       do z=szus,ezus
          do j=syus,eyus
             do i=sxus,exus
                coefuv(k)= coef (l)
                coefuv(k+1)= coef (l+1)
                coefuv(k+2)= coef (l+2)
                coefuv(k+3)= coef (l+3)
                jcofuv(k)= jcof(l)-nb_vx
                jcofuv(k+1)= jcof (l+1)-nb_Vx
                jcofuv(k+2)= jcof (l+2)-nb_vx
                jcofuv(k+3)= jcof (l+3)-nb_vx
                k=k+4
                l=l+17
             enddo
          enddo
       enddo

       do k=1,nb_vx*4
          if (abs(coefuv(k)) < 1.d-30) then
             coefuv(k) = 0.d0
          endif
       enddo

       icofuv(1)=1

       do l=2,nb_vx+1
          icofuv(l)= icofuv(l-1)+4
       enddo
    endif

    call compute_time(TIMER_END,"[sub] construct_Fuv")
  end subroutine construct_Fuv


  !-----------------------------------------------------------------------
  ! Construct Blcok Fvu of velocity
  !-----------------------------------------------------------------------
  subroutine construct_Fvu(icof,jcof,coef,icofvu,jcofvu,coefvu)
    use mod_Parameters, only: dim,nb_Vx,nb_Vy, &
         & sxvs,exvs,syvs,eyvs,szvs,ezvs
    implicit none

    integer, dimension(:),allocatable,intent(in) :: icof
    integer, dimension(:),allocatable,intent(in)  :: jcof
    !-------------------------------------------------------------------------------
    real(8), dimension(:),allocatable, intent(in)    :: coef
    real(8), dimension(:),allocatable, intent(inout) :: coefvu
    integer, dimension(:),allocatable,intent(inout)  :: icofvu
    integer, dimension(:),allocatable,intent(inout)  :: jcofvu
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                              :: i,k,j,z,lvs,l
    !-------------------------------------------------------------------------------

    call compute_time(TIMER_START,"[sub] construct_Fvu")

    ! construction du block F_vu
    if (dim==2) then
       l=6+11*nb_vx
       k=1
       do j=syvs,eyvs
          do i=sxvs,exvs
             coefvu(k)  = coef (l)
             coefvu(k+1)= coef (l+1)
             coefvu(k+2)= coef (l+2)
             coefvu(k+3)= coef (l+3)
             jcofvu(k)= jcof(l)
             jcofvu(k+1)= jcof (l+1)
             jcofvu(k+2)= jcof (l+2)
             jcofvu(k+3)= jcof (l+3)
             k=k+4
             l=l+11
          enddo
       enddo

       do k=1,nb_vy*4
          if (abs(coefvu(k)) < 1.d-30) then
             coefvu(k) = 0.d0
          endif
       enddo

       icofvu(1)=1

       do l=2,nb_vy+1
          icofvu(l)= icofvu(l-1)+4
       enddo

    else
       write(*,*) "STOP, construct_Fvu not done in 3D" 
       write(*,*) "STOP, construct_Fvu not done in 3D" 
       write(*,*) "STOP, construct_Fvu not done in 3D" 
       write(*,*) "STOP, construct_Fvu not done in 3D" 
       write(*,*) "STOP, construct_Fvu not done in 3D" 
    endif

    call compute_time(TIMER_END,"[sub] construct_Fvu")
  end subroutine construct_Fvu

  !-----------------------------------------------------------------------
  ! Construct Blcok Fuw+Fvw of velocity
  !-----------------------------------------------------------------------
  subroutine construct_Fuw_Fvw(icof,jcof,coef,icofuv_w,jcofuv_w,coefuv_w)
    use mod_Parameters, only: dim,nb_Vx,nb_Vy, &
         & sxus,exus,syus,eyus,szus,ezus,      &
         & sxvs,exvs,syvs,eyvs,szvs,ezvs
    implicit none

    integer, dimension(:),allocatable,intent(in) :: icof
    integer, dimension(:),allocatable,intent(in)  :: jcof
    !-------------------------------------------------------------------------------
    real(8), dimension(:),allocatable, intent(in)    :: coef
    real(8), dimension(:),allocatable, intent(inout) :: coefuv_w
    integer, dimension(:),allocatable,intent(inout)  :: icofuv_w
    integer, dimension(:),allocatable,intent(inout)  :: jcofuv_w
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                              :: i,k,j,z,lvs,l
    !-------------------------------------------------------------------------------

    call compute_time(TIMER_START,"[sub] construct_Fuw_Fvw")

    ! construction du block F_uv

    l=12
    k=1
    do z=szus,ezus
       do j=syus,eyus
          do i=sxus,exus
             coefuv_w(k)= coef (l)
             coefuv_w(k+1)= coef (l+1)
             coefuv_w(k+2)= coef (l+2)
             coefuv_w(k+3)= coef (l+3)
             jcofuv_w(k)= jcof(l)-nb_vx-nb_vy
             jcofuv_w(k+1)= jcof (l+1)-nb_Vx-nb_vy
             jcofuv_w(k+2)= jcof (l+2)-nb_vx-nb_vy
             jcofuv_w(k+3)= jcof (l+3)-nb_vx-nb_vy
!!$             write(65,*) jcofuv_w(k), jcofuv_w(k),jcofuv_w(k+1), jcofuv_w(k+2), jcofuv_w(k+3)
             k=k+4
             l=l+17
          enddo
       enddo
    enddo

!!$    write(65,*)""
    ! write(*,*)l
    l=12+nb_vx*17
    k=1+4*nb_vx
    do z=szvs,ezvs
       do j=syvs,eyvs
          do i=sxvs,exvs
             coefuv_w(k)= coef (l)
             coefuv_w(k+1)= coef (l+1)
             coefuv_w(k+2)= coef (l+2)
             coefuv_w(k+3)= coef (l+3)
             jcofuv_w(k)= jcof(l)-nb_vx-nb_vy
             jcofuv_w(k+1)= jcof (l+1)-nb_Vx-nb_vy
             jcofuv_w(k+2)= jcof (l+2)-nb_vx-nb_vy
             jcofuv_w(k+3)= jcof (l+3)-nb_vx-nb_vy
!!$              write(65,*) jcofuv_w(k), jcofuv_w(k), jcofuv_w(k+1), jcofuv_w(k+2), jcofuv_w(k+3)
             k=k+4
             l=l+17
          enddo
       enddo
    enddo

    do k=1,nb_vx*4
       if (abs(coefuv_w(k)) < 1.d-30) then
          coefuv_w(k) = 0.d0
       endif
    enddo

    icofuv_w(1)=1

    do l=2,nb_vx+nb_vy+1
       icofuv_w(l)= icofuv_w(l-1)+4
    enddo

    call compute_time(TIMER_END,"[sub] construct_Fuw_Fvw")
  end subroutine construct_Fuw_Fvw
  

#if 1
  subroutine solver_lap_BiCG_mg(sol_phi,smc_phi,matrix_hypre_Ap, &
       & Struct_hypreyp,Struct_hyprexp,res,iter,                 &
       & solver_it,solver_res)
    use mod_Parameters
    use mod_hypre_dep
    use mod_mpi
    implicit none
    include 'HYPREf.h'
    real(8), dimension(:), intent(IN), allocatable    :: smc_phi
    real(8), dimension(:), intent(INout), allocatable :: sol_phi
    real(8), intent(out)                              :: res
    integer, intent(out)                              :: iter
     !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                               :: i,j,k,ii,jj,kk,c,ls,l
    integer                               :: solver_it,ierr,Object_type
    real(8)                               :: solver_res,final_res_norm
    integer*8,intent(in)::matrix_hypre_Ap,Struct_hypreyp,Struct_hyprexp
    integer*8 solverpro,projection_sx,projection_sy,vect_x,vect_y,matrix_hypre_sAprojection,precond
    integer    ilower(dim), iupper(dim),num_iterations
    real(8), dimension(:),allocatable::values,sol_phi_int
    integer    part,varu
    integer :: bicg_precond=1
    integer :: maxLvL,nb_pre_post_relax,typeRelax,skipRelax,MaxIt
    real(8) :: tol

    call compute_time(TIMER_START,"[sub] solver_lap_BiCG_mg")

    allocate(values(nb_p_no_ghost))
    values=0

    allocate(sol_phi_int(nb_p_no_ghost))
    sol_phi_int=0

    
    
    part=0
    varu=0
    Object_type = HYPRE_STRUCT

    bicg_precond=abs(PJ_HYPRE_precond-1)  !!!! warning change value 0 -> 1, 1 -> 0 !!!!
    nb_pre_post_relax=PJ_HYPRE_nb_pre_post_relax
    tol=PJ_HYPRE_tol
    maxLvL=PJ_HYPRE_maxLvL
    typeRelax=PJ_HYPRE_typeRelax
    skipRelax=PJ_HYPRE_skipRelax
    MaxIt=PJ_HYPRE_MaxIt

    call HYPRE_StructBiCGSTABCreate(COMM3D,solverpro,ierr)
    call HYPRE_StructBiCGSTABSetTol(solverpro,PJ_SOLVER_THRESHOLD,ierr)
    call HYPRE_StructBiCGSTABSetMaxIter(solverpro,solver_it,ierr)
    if (ires>0) then
       call hypre_structbicgstabsetprintlev(solverpro,2,ierr)
       call hypre_structbicgstabsetlogging(solverpro,1,ierr)
    end if

    select case(PJ_HYPRE_precond)
    case(1) ! SMG
       CALL HYPRE_StructSMGCreate(COMM3D,precond,ierr)
       cALL HYPRE_StructSMGSetMemoryUse(precond, 0,ierr)
       cALL HYPRE_StructSMGSetMaxIter(precond, MaxIt,ierr)
       cALL HYPRE_StructSMGSetTol(precond,tol,ierr)
       !CALL HYPRE_StructSMGSetZeroGuess(precond,ierr)
       CALL HYPRE_StructSMGSetNonZeroGuess(precond,ierr)
       CALL HYPRE_StructSMGSetNumPreRelax(precond, 1,ierr)
       CALL HYPRE_StructSMGSetNumPostRelax(precond, 1,ierr)
    case(0) ! PFMG
       call HYPRE_StructPFMGCreate(COMM3D,precond,ierr)
       call HYPRE_StructPFMGsetmaxlevels(precond,maxLvL,ierr)
       call HYPRE_StructPFMGSetMaxIter(precond,MaxIt,ierr)
       call HYPRE_StructPFMGSetTol(precond,tol,ierr)
       !call HYPRE_StructPFMGSetZeroGuess(precond,ierr)
       call HYPRE_StructPFMGSetNonZeroGuess(precond,ierr)
       call HYPRE_StructPFMGSetNumPreRelax(precond,nb_pre_post_relax,ierr)
       call HYPRE_StructPFMGSetNumPostRelax(precond,nb_pre_post_relax,ierr)
       call HYPRE_STructPFMGSetRAPType(precond,1,ierr)
       call HYPRE_StructPFMGSetPrintLevel(precond,1,ierr)
       call HYPRE_StructPFMGSetLogging(precond,1,ierr)
       call HYPRE_structPFMGsetrelaxtype(precond,typeRelax,ierr)
       call HYPRE_structPFMGsetskiprelax(precond,skipRelax,ierr)
    end select
    
    
    call HYPRE_StructBiCGSTABSetPrecond(solverpro,bicg_precond,precond,ierr)

    call HYPRE_SStructMatrixGetObject(matrix_hypre_Ap,matrix_hypre_SAprojection, ierr)        
!!$   ! call HYPRE_StructSMGSetup(solverpro,matrix_hypre_SAprojection,Struct_hypre_yp,Struct_hypre_xp, ierr)

    call HYPRE_StructBiCGSTABSetup(solverpro,matrix_hypre_SAprojection,Struct_hypre_yp,Struct_hypre_xp, ierr)



    
    call HYPRE_SStructVectorCreate(COMM3D, grid_hypre_p, vect_y, ierr)
    call HYPRE_SStructVectorCreate(COMM3D, grid_hypre_p, vect_x, ierr)
    
    call HYPRE_SStructVectorSetObjectTyp(vect_x, object_type, ierr)
    call HYPRE_SStructVectorSetObjectTyp(vect_y, object_type, ierr)

    call HYPRE_SStructVectorInitialize(vect_y, ierr)
    call HYPRE_SStructVectorInitialize(vect_x, ierr)

    if (dim==2) then 
       ilower(1) = sx
       ilower(2) = sy
       iupper(1) = ex
       iupper(2) = ey
    else
       ilower(1) = sx
       ilower(2) = sy
       ilower(3) = sz
       iupper(1) = ex
       iupper(2) = ey
       iupper(3) = ez
    end if

    values=0
    if (dim==2) then
       c=0
       do jj=sy,ey
          do ii=sx,ex
             i=ii-sxs
             j=jj-sys
             ls=i+j*(exs-sxs+1)+1
             c=c+1
             values(c)=sol_phi(ls)
          end do
       end do
    else
       c=0
       do kk=sz,ez
          do jj=sy,ey
             do ii=sx,ex
                i=ii-sxs
                j=jj-sys
                k=kk-szs 
                ls=i+j*(exs-sxs+1)+k*(exs-sxs+1)*(eys-sys+1)+1
                c=c+1
                values(c)=sol_phi(ls)
             end do
          end do
       end do
    end if

    
    call HYPRE_SStructVectorSetBoxValues(vect_x, part, ilower, iupper,varu, values, ierr)

    values=0
    if (dim==2) then
       c=0
       do jj=sy,ey
          do ii=sx,ex
             i=ii-sxs
             j=jj-sys
             ls=i+j*(exs-sxs+1)+1
             c=c+1
             values(c)=smc_phi(ls)
          end do
       end do
    else
       c=0
       do kk=sz,ez
          do jj=sy,ey
             do ii=sx,ex
                i=ii-sxs
                j=jj-sys
                k=kk-szs 
                ls=i+j*(exs-sxs+1)+k*(exs-sxs+1)*(eys-sys+1)+1
                c=c+1
                values(c)=smc_phi(ls)
             end do
          end do
       end do
    end if

    call HYPRE_SStructVectorSetBoxValues(vect_y, part, ilower,iupper,varu,values, ierr)
    
    call HYPRE_SStructVectorAssemble(vect_x, ierr)
    call HYPRE_SStructVectorAssemble(vect_y, ierr)
    
!!$    call HYPRE_SStructVectorPrint("Vect_x",vect_x,ierr)
!!$    call HYPRE_SStructVectorPrint("Vect_y",vect_y,ierr)
!!$
!!$    stop
    
    call HYPRE_SStructVectorGetObject(vect_y, projection_sy, ierr)
    call HYPRE_SStructVectorGetObject(vect_x, projection_sx, ierr)
    
!!$   ! call HYPRE_StructSMGSolve(solverpro,matrix_hypre_SAprojection ,projection_sy,projection_sx, ierr)
!!$   ! call HYPRE_StructSMGGetNumIterations(solverpro,num_iterations, ierr)
!!$   ! call HYPRE_StructSMGGetFinalRelative(solverpro,final_res_norm, ierr)
    
    call HYPRE_StructBiCGSTABSolve(solverpro,matrix_hypre_SAprojection,projection_sy,projection_sx,ierr)
    call hypre_structbicgstabgetfinalrel(solverpro,final_res_norm,ierr)
    call HYPRE_StructBiCGSTABGetNumItera(solverpro,num_iterations,ierr)
    
    res=final_res_norm
    iter=num_iterations
    
    if (dim==2) then 
       ilower(1) = sx
       ilower(2) = sy
       iupper(1) = ex
       iupper(2) = ey
    else
       ilower(1) = sx
       ilower(2) = sy
       ilower(3) = sz
       iupper(1) = ex
       iupper(2) = ey
       iupper(3) = ez
    end if

    sol_phi=0
    sol_phi_int=0
    
    call  HYPRE_SStructVectorGetBoxValues(vect_x, PART, ilower,iupper, varu, sol_phi_int,ierr)
    
    if (dim==2) then
       c=0
       do jj=sy,ey
          do ii=sx,ex
             i=ii-sxs
             j=jj-sys
             ls=i+j*(exs-sxs+1)+1
             c=c+1
             sol_phi(ls)=sol_phi_int(c)
          end do
       end do
    else
       c=0
       do kk=sz,ez
          do jj=sy,ey
             do ii=sx,ex
                i=ii-sxs
                j=jj-sys
                k=kk-szs 
                ls=i+j*(exs-sxs+1)+k*(exs-sxs+1)*(eys-sys+1)+1
                c=c+1
                sol_phi(ls)=sol_phi_int(c)
             end do
          end do
       end do
    end if


    if (nproc>1) call solver_comm_mpi(sol_phi,nb_P,1)
    
    call HYPRE_SStructVectorDestroy(vect_y, ierr)
    call HYPRE_SStructVectorDestroy(vect_x, ierr)
    call hypre_structbicgstabdestroy(solverpro,ierr)

    select case(PJ_HYPRE_precond)
    case(1)
       call hypre_structsmgdestroy(precond,ierr)
    case(0)
       call hypre_structpfmgdestroy(precond,ierr)
    end select

    call compute_time(TIMER_END,"[sub] solver_lap_BiCG_mg")
  end subroutine solver_lap_BiCG_mg

  Subroutine create_precon_mg_phi(solverp)
    use mod_Parameters, only: dim,      &
         & PJ_HYPRE_precond,            &
         & PJ_HYPRE_nb_pre_post_relax,  &
         & PJ_HYPRE_maxLvL,             &
         & PJ_HYPRE_typeRelax,          &
         & PJ_HYPRE_skipRelax,          &
         & PJ_HYPRE_tol,                &
         & PJ_HYPRE_MaxIt 
    use mod_mpi
    implicit none
    include 'HYPREf.h'

    integer*8, intent(out) :: solverp
    integer                :: ierr
    integer                :: maxLvL,nb_pre_post_relax,typeRelax,skipRelax,MaxIt
    real(8)                :: tol

    call compute_time(TIMER_START,"[sub] create_precon_mg_phi")
    
    nb_pre_post_relax=PJ_HYPRE_nb_pre_post_relax
    tol=PJ_HYPRE_tol
    maxLvL=PJ_HYPRE_maxLvL
    typeRelax=PJ_HYPRE_typeRelax
    skipRelax=PJ_HYPRE_skipRelax
    MaxIt=PJ_HYPRE_MaxIt
    
    ! 0 : PFMG
    ! 1 : SMG
    select case(PJ_HYPRE_precond)
    case(1)    
       call HYPRE_StructSMGCreate(COMM3D,solverp,ierr)
       call HYPRE_StructSMGSetMemoryUse(solverp,0,ierr)
       call HYPRE_StructSMGSetMaxIter(solverp,MaxIt,ierr)
       call HYPRE_StructSMGSetTol(solverp,tol,ierr)
       call HYPRE_StructSMGSetZeroGuess(solverp,ierr)
       call HYPRE_StructSMGSetNumPreRelax(solverp,nb_pre_post_relax,ierr)
       call HYPRE_StructSMGSetNumPostRelax(solverp,nb_pre_post_relax,ierr)
       call HYPRE_StructSMGSetPrintLevel(solverp,1,ierr)
       call HYPRE_StructSMGSetLogging(solverp,1,ierr)
    case(0)
       call HYPRE_StructPFMGCreate(COMM3D,solverp,ierr)
       call HYPRE_StructPFMGSetMaxIter(solverp,MaxIt,ierr)
       call HYPRE_StructPFMGSetTol(solverp,tol,ierr)
       call HYPRE_StructPFMGsetmaxlevels(solverp,maxLvL,ierr)
       call HYPRE_StructPFMGSetZeroGuess(solverp,ierr)
       call HYPRE_StructPFMGSetNumPreRelax(solverp,nb_pre_post_relax,ierr)
       call HYPRE_StructPFMGSetNumPostRelax(solverp,nb_pre_post_relax,ierr)
       call HYPRE_STructPFMGSetRAPType(solverp,1,ierr)
       call HYPRE_StructPFMGSetPrintLevel(solverp,1,ierr)
       call HYPRE_StructPFMGSetLogging(solverp,1,ierr)
       call HYPRE_structPFMGsetrelaxtype(solverp,typeRelax,ierr) 
       call HYPRE_structPFMGsetskiprelax(solverp,skipRelax,ierr)
    end select

    call compute_time(TIMER_END,"[sub] create_precon_mg_phi")
  end Subroutine create_precon_mg_phi


  subroutine setup_phi(solverp,sAp)
    use mod_Parameters, only: PJ_HYPRE_precond,dim
    use mod_mpi
    implicit none
    include 'HYPREf.h'
    
    integer*8, intent(inout) :: sAp
    integer*8, intent(inout) :: solverp
    integer                  :: ierr  
    
    call compute_time(TIMER_START,"[sub] setup_phi")

    select case(PJ_HYPRE_precond)
    case(1)
       call HYPRE_StructSMGSetup(solverp,sAp,Struct_hypre_yp,Struct_hypre_xp,ierr)
    case(0)
       call HYPRE_StructPFMGSetup(solverp,sAp,Struct_hypre_yp,Struct_hypre_xp,ierr)
    end select

    call compute_time(TIMER_END,"[sub] setup_phi")
  end subroutine setup_phi
  
  subroutine solver_lap_BiCGStab2_iLU(coef,jcof,icof,sol,rhs,npt,nps, &
       & kic,nic,res,kt,solverp,sAp,solver_it,solver_threshold)
    use mod_Parameters
    use mod_mpi
    use mod_solver_comm_mpi
    
    implicit none
    include 'HYPREf.h'
    
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    integer                                           :: solver_it
    real(8)                                           :: solver_threshold
    integer                                           :: nps,kt,npt,nic
    integer, allocatable, dimension(:), intent(inout) :: icof
    integer, allocatable, dimension(:), intent(inout) :: jcof
    integer, allocatable, dimension(:), intent(inout) :: kic
    real(8), allocatable, dimension(:), intent(inout) :: coef
    real(8), allocatable, dimension(:), intent(inout) :: sol,rhs
    real(8)                                           :: res
    integer*8                                         :: solverp,sAp
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                :: i,j,l,k
    integer                                :: ii,jj,kk
    !-------------------------------------------------------------------------------
    real(8)                                :: eps,bet,prec,rho1,tol,residu,ressq,res0
    !-------------------------------------------------------------------------------
    real(8)                                :: diag
    real(8)                                :: alp,bl2,ome2,restemp,rho0,rl2,bl20,rl20
    real(8)                                :: gam,ome1,smu,snu,tau
    real(8), allocatable, dimension(:)     :: psa,qsa,rsa,ssa,tsa,usa,vsa,wsa,ysa,msa
    real(8), allocatable, dimension(:)     :: smc,soltemp,rhs0,tmp
    !-------------------------------------------------------------------------------

    call compute_time(TIMER_START,"[sub] solver_lap_BiCGStab2_iLU")
        
    !-------------------------------------------------------------------------------
    ! solved equation (0:nvstks,1:energy)
    !-------------------------------------------------------------------------------
    mpi_ieq_solve=1
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Initialization
    !-------------------------------------------------------------------------------
    allocate(psa(npt), &
         & qsa(npt),   &
         & rsa(npt),   &
         & ssa(npt),   &
         & tsa(npt),   &
         & usa(npt),   &
         & vsa(npt),   &
         & wsa(npt),   &
         & ysa(npt),   &
         & msa(npt),   &
         & smc(npt),   &
         & rhs0(npt),  &
         & tmp(npt),   &
         & soltemp(npt))
    
    msa  = 0
    psa  = 0
    qsa  = 0
    rsa  = 0
    ssa  = 0
    tsa  = 0
    usa  = 0
    vsa  = 0
    wsa  = 0
    ysa  = 0
    rhs0 = 0
    tmp  = 0
    qsa  = rhs
    smc  = rhs
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! initial residual values
    !-------------------------------------------------------------------------------
    rhs0=rhs
    call pscalee(rhs,rhs,bl20,npt,kic,nic) !
    bl20 = sqrt(bl20+1.d-40)
    !-------------------------------------------------------------------------------
    call matveke(npt,sol,psa,coef,jcof,icof,nps)
    psa=psa-rhs
    call pscalee(psa,psa,rl20,npt,kic,nic)
    rl20 = sqrt(rl20+1.d-40)
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! preconditionning
    !-------------------------------------------------------------------------------
    call precond_mg(rhs,qsa,SAp,solverp)
    !-------------------------------------------------------------------------------
    
    call pscalee(rhs,rhs,bl2,npt,kic,nic)  
    bl2 = sqrt(bl2+1.d-40)
    
    call matveke(npt,sol,psa,coef,jcof,icof,nps)
    call precond_mg(tsa,psa,SAp,solverp)
    
    rsa = rhs - tsa
    qsa = rsa
    psa = rsa
    
    call pscalee (rsa,rsa,rl2,npt,kic,nic)
    rl2 = sqrt(rl2+1.d-40)
    
    if (bl2 <= 1.d-80) then
       write (6,*) 'error in CG. : second member is zero', bl2
       stop
    endif
    if (rl2 <= 1.d-80) then
       write (6,*) 'error in CG : residual is zero', rl2
       stop
    endif

    !-------------------------------------------------------------------------------
    restemp = 1.d20
    !-------------------------------------------------------------------------------
    !bicgstab(2)
    !-------------------------------------------------------------------------------
    rho0 = 1
    alp  = 0
    ome2 = 1
    res  = rl20/bl20
    !-------------------------------------------------------------------------------
    if (ires>0) then 
       if (rank==0) then
          write(*,*)
          write(*,101) "-----", "--------------------", &
               & "--------------------","--------------------"
          write(*,*) "  resol.sys.",mpi_ieq_solve
          write(*,*) "      |b|_2=",bl20
          write(*,*) " |A.x0-b|_2=",rl20
          write(*,101) "Iters", "|A.x-b|_2", "conv.rate", "|A.x-b|_2/|b|_2"
          write(*,101) "-----", "--------------------", &
               & "--------------------","--------------------"
       end if
    end if

    do kt=1,solver_it

       prec= 1.d-60
       rho0 = -ome2*rho0
       !-------------------------------------------------------------------------------
       ! Even step
       !-------------------------------------------------------------------------------
       call pscalee (qsa,rsa,rho1,npt,kic,nic)

       bet = alp * rho1 / rho0
       
       rho0 = rho1
       
       do l = 1,npt
          usa(l) = rsa(l) - bet*usa(l) ! u=r-bet*u
       enddo

       call matveke(npt,usa,psa,coef,jcof,icof,nps)

       call precond_mg(vsa,psa,SAp,solverp)
    
       call pscalee (qsa,vsa,gam,npt,kic,nic)

       alp = rho0 / gam

       do l = 1,npt
          rsa(l) = rsa(l) - alp*vsa(l) !r=r-alpha*v
       enddo

       call matveke(npt,rsa,psa,coef,jcof,icof,nps)

       call precond_mg(ssa,psa,SAp,solverp)

       sol = sol + alp*usa    ! x=x+alp*u
       
       !-------------------------------------------------------------------------------
       !Odd step
       !-------------------------------------------------------------------------------

       !===============================================================================
       call pscalee (qsa,ssa,rho1,npt,kic,nic)
       bet = alp * rho1 / rho0
       rho0 = rho1
       vsa = ssa - bet*vsa
       
       call matveke(npt,vsa,psa,coef,jcof,icof,nps)

       call precond_mg(wsa,psa,SAp,solverp)

       call pscalee (qsa,wsa,gam,npt,kic,nic)

       alp = (rho0+prec) / (gam+prec)

       usa = rsa - bet*usa
       rsa = rsa - alp*vsa
       ssa = ssa - alp*wsa

       call matveke(npt,ssa,psa,coef,jcof,icof,nps)

       call precond_mg(tsa,psa,SAp,solverp)

       !-------------------------------------------------------------------------------
       !cgr(2) part
       !-------------------------------------------------------------------------------
       !===============================================================================
       call pscalee (rsa,ssa,ome1,npt,kic,nic)
       call pscalee (ssa,ssa,smu,npt,kic,nic)
       call pscalee (ssa,tsa,snu,npt,kic,nic)
       call pscalee (tsa,tsa,tau,npt,kic,nic)
       !===============================================================================
       call pscalee (rsa,tsa,ome2,npt,kic,nic)
       tau = tau - snu*snu/smu
       ome2 = (ome2-(snu+prec)*(ome1+prec)/(smu+prec))/(tau+prec)
       ome1 = (ome1-snu*ome2) / (smu+prec)

       sol = sol + ome1*rsa + ome2*ssa + alp*usa
       rsa = rsa - ome1*ssa - ome2*tsa 
       usa = usa - ome1*vsa - ome2*wsa

       ! end

       rl20=res
       tmp=0
       
       call matveke(npt,sol,tmp,coef,jcof,icof,nps)
       tmp = tmp - rhs0 
       call pscalee (tmp,tmp,res,npt,kic,nic)

       !-------------------------------------------------------------------------------
       ! residual
       !-------------------------------------------------------------------------------
       if (res<1d20 .and. res>1d-32) then
          res = sqrt(res+1.d-40)
       else
          if (res>=1d20) then
             res = 1d10
          else
             res = 1d-16
          end if
       endif
       !-------------------------------------------------------------------------------
       
       !-------------------------------------------------------------------------------
       ! normalisation with sec_membre
       !-------------------------------------------------------------------------------
       res=res/bl20
       !-------------------------------------------------------------------------------
       
       !-------------------------------------------------------------------------------
       ! save best solution
       !-------------------------------------------------------------------------------
       if (res < restemp) then
          restemp = res
          soltemp = sol
       endif
       !-------------------------------------------------------------------------------
       if (ires>0 .and. modulo(kt,ires)==0) then
          if (rank==0) then
             write(*,102) kt,res*bl20,res/rl20,res
          end if
       end if
       !-------------------------------------------------------------------------------
       if (res <= solver_threshold) exit
       if (kt>30.and.res>1) exit
    end do
    !-------------------------------------------------------------------------------
    kt=min(kt,solver_it)
    !-------------------------------------------------------------------------------
    sol = soltemp
    res = restemp
    rhs = rsa

    if (ires>0) then
       if (rank==0) then
          write(*,101) "-----", "--------------------", &
               & "--------------------","--------------------"
          write(*,*)
       end if
    end if
    
    if (nproc>1) call solver_comm_mpi(sol,npt,1)
    
101 format(a5,1x,3(a20,1x))
102 format(i5,1x,9(1pe20.6,1x))

    call compute_time(TIMER_END,"[sub] solver_lap_BiCGStab2_iLU")
  end subroutine solver_lap_BiCGStab2_iLU
#endif 


  subroutine precond_mg(msa,qsa,SAp,solverp)
    use mod_Parameters
    use mod_mpi
    implicit none
    include 'HYPREf.h'
    
    real(8), dimension(:),intent(INout),allocatable::qsa
    real(8), dimension(:),intent(INout),allocatable::msa
    integer*8,intent(in)::solverp,Sap

    
    integer*8  vect_x,vect_y
    integer*8  ssy,ssx
    integer    ls
    integer    nvars
    integer    part
    integer    varp
    integer    object_type
    integer    ilower(dim), iupper(dim)
    integer    vartypes(1)
    integer    ent,lvs
    integer    nentries, nvalues, stencil_indices_1(dim*2+1)
    integer    xvar(dim*2+1),yvar(dim*2+1),pvar(1)
    integer    i,j,ierr,k,l,lvp,z,ii,jj,kk,c
    integer    num_iterations
    real(8)    final_res_norm,r
    real(8),dimension(:),allocatable::xp,values

    integer :: job,nzmax
    real(8) :: t,t1,t2

    call compute_time(TIMER_START,"[sub] precond_mg")
    
    !---------------------------------------------------------------------
    ! Pressure Varibale
    !---------------------------------------------------------------------
    allocate(xp(nb_p_no_ghost),values(nb_p_no_ghost))
    xp=0
    values=0
    !---------------------------------------------------------------------
    
    msa   = qsa
    nvars = 1
    Part  = 0
    Object_type = HYPRE_STRUCT
    varp  = 0

    xp=0
    
    call HYPRE_SStructVectorCreate(COMM3D, grid_hypre_p, vect_y, ierr)
    call HYPRE_SStructVectorCreate(COMM3D, grid_hypre_p, vect_x, ierr)

    call HYPRE_SStructVectorSetObjectTyp(vect_x, object_type, ierr)
    call HYPRE_SStructVectorSetObjectTyp(vect_y, object_type, ierr)
    
    call HYPRE_SStructVectorInitialize(vect_y, ierr)
    call HYPRE_SStructVectorInitialize(vect_x, ierr)

    if (dim==2) then 
       ilower(1) = sx
       ilower(2) = sy
       iupper(1) = ex
       iupper(2) = ey
    else
       ilower(1) = sx
       ilower(2) = sy
       ilower(3) = sz
       iupper(1) = ex
       iupper(2) = ey
       iupper(3) = ez
    end if

    values=0
    call HYPRE_SStructVectorSetBoxValues(vect_x, part, ilower, iupper, varp, values, ierr)

    values=0
    if (dim==2) then
       c=0
       do jj=sy,ey
          do ii=sx,ex
             i=ii-sxs
             j=jj-sys
             ls=i+j*(exs-sxs+1)+1
             c=c+1
             values(c)=qsa(ls)
          end do
       end do
    else
       c=0
       do kk=sz,ez
          do jj=sy,ey
             do ii=sx,ex
                i=ii-sxs
                j=jj-sys
                k=kk-szs 
                ls=i+j*(exs-sxs+1)+k*(exs-sxs+1)*(eys-sys+1)+1
                c=c+1
                values(c)=qsa(ls)
             end do
          end do
       end do
    end if

    call HYPRE_SStructVectorSetBoxValues(vect_y, part, ilower,iupper, varp, values, ierr)
    
    call HYPRE_SStructVectorAssemble(vect_x, ierr)
    call HYPRE_SStructVectorAssemble(vect_y, ierr)
    
    call HYPRE_SStructVectorGetObject(vect_y, ssy, ierr)
    call HYPRE_SStructVectorGetObject(vect_x, ssx, ierr)


    select case(PJ_HYPRE_precond)
    case(1)
       call HYPRE_StructSMGSolve(solverp, sAp, ssy, ssx, ierr)
       call HYPRE_StructSMGGetNumIterations(solverp, num_iterations, ierr)
       call HYPRE_StructSMGGetFinalRelative(solverp, final_res_norm, ierr)
    case(0)
       call HYPRE_StructPFMGSolve(solverp, sAp, ssy, ssx, ierr)
    end select
    
    call  HYPRE_SStructVectorGetBoxValues(vect_x, PART, ilower, iupper, varp, xp, ierr)
    
    if (dim==2) then
       c=0
       do jj=sy,ey
          do ii=sx,ex
             i=ii-sxs
             j=jj-sys
             ls=i+j*(exs-sxs+1)+1
             c=c+1
             msa(ls)=xp(c)
          end do
       end do
    else
       c=0
       do kk=sz,ez
          do jj=sy,ey
             do ii=sx,ex
                i=ii-sxs
                j=jj-sys
                k=kk-szs 
                ls=i+j*(exs-sxs+1)+k*(exs-sxs+1)*(eys-sys+1)+1
                c=c+1
                msa(ls)=xp(c)
             end do
          end do
       end do
    end if

    call HYPRE_SStructVectorDestroy(vect_y, ierr)
    call HYPRE_SStructVectorDestroy(vect_x, ierr)

    call compute_time(TIMER_END,"[sub] precond_mg")
  end subroutine precond_mg
  
#endif 

end module mod_solver_hypre
