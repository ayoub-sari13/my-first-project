!===============================================================================
module Bib_VOFLag_BC_Velocity_UniformStokesFlowPastAParticle_Sphere
   !===============================================================================
   contains
   !-----------------------------------------------------------------------------  
   !> @author  amine chadil
   ! 
   !> @brief impose des conditions aux limites egales a la solution exactes d'un ecoulement
   !! de stokes autour d'une sphere. cette routine suppose que des condtions de diriclet
   !! ont ete imposees dans le don
   !
   !> @param[in]  penu, penv, penw : 
   !> @param[in]  uin, vin, win    : 
   !> @param[in]  centre_sphere  : 
   !> @param[in]  rayon_sphere     : 
   !> @param[in]  U_inf            : 
   !-----------------------------------------------------------------------------
   subroutine BC_Velocity_UniformStokesFlowPastAParticle_Sphere(penu,penv,penw,uin,vin,win,&
               centre_sphere,rayon_sphere,U_inf)
      !===============================================================================
      !modules
      !===============================================================================
      !-------------------------------------------------------------------------------
      !Modules de definition des structures et variables => src/mod/module_...f90
      !-------------------------------------------------------------------------------
      use mod_struct_thermophysics, only : fluids
      use mod_Parameters,           only : deeptracking,sx,gx,ex,gx,sy,gy,ey,gy,sz,gz,ez,   &
                                           gz,sxu,exu,syu,eyu,szu,ezu,sxv,exv,syv,eyv,szv,  &
                                           ezv,sxw,exw,syw,eyw,szw,ezw,gsxu,gexu,gsxv,gexv, &
                                           gsxw,gexw,gsyu,geyu,gsyv,geyv,gsyw,geyw,gszu,    &
                                           gezu,gszv,gezv,gszw,gezw,grid_xu,grid_yv,grid_zw,&
                                           grid_x,grid_y,grid_z,NS_pen_BC
      use mod_Constants,  only : d1p2
      !-------------------------------------------------------------------------------

      !===============================================================================
      !declarations des variables
      !===============================================================================
      implicit none
      !-------------------------------------------------------------------------------
      !variables globales
      !-------------------------------------------------------------------------------
      real(8), dimension(:,:,:,:), allocatable :: penu,penv,penw
      real(8), dimension(:,:,:),   allocatable :: uin,vin,win
      real(8), dimension(3)                    :: centre_sphere
      real(8)                                  :: U_inf,rayon_sphere
      !-------------------------------------------------------------------------------
      !variables locales 
      !-------------------------------------------------------------------------------
      real(8)                                  :: x,y,z,r,full_pen,half_pen
      integer                                  :: i,j,k
      !-------------------------------------------------------------------------------

      !-------------------------------------------------------------------------------
      if (deeptracking) write(*,*) 'entree BC_Velocity_UniformStokesFlowPastAParticle_Sphere'
      !-------------------------------------------------------------------------------

      !===============================================================================
      ! Traitement des vitesses normales
      !===============================================================================
      !-------------------------------------------------------------------------------
      ! Limite GAUCHE : composante normale = conposante 1
      !-------------------------------------------------------------------------------
      full_pen=NS_pen_BC
      half_pen=d1p2*NS_pen_BC
      do k=szu,ezu
         do j=syu,eyu
            do i=sxu,exu
               if (i==gsxu) then
                  penu(i,j,k,:)=0
                  penu(i,j,k,1)=half_pen
                  penu(i,j,k,3)=half_pen
                  x=grid_x(i)-centre_sphere(1)
                  y=grid_y(j)-centre_sphere(2)
                  z=grid_z(k)-centre_sphere(3)
                  r=sqrt(x**2+y**2+z**2)
                  uin(i,j,k)=2d0*(U_inf-3d0*rayon_sphere/4d0*(U_inf/(r+1D-40)+x**2*U_inf/(r**3+1D-40))&
                            -3d0*rayon_sphere**3/4d0*(U_inf/(3d0*r**3+1D-40)-x**2*U_inf/(r**5+1D-40)))
               endif
            enddo
         enddo
      enddo
      !-------------------------------------------------------------------------------
      ! Limite Droite : composante normale = conposante 1
      !-------------------------------------------------------------------------------
      do k=szu,ezu
         do j=syu,eyu
            do i=sxu,exu
               if (i==gexu) then
                  penu(i,j,k,:)=0
                  penu(i,j,k,1)=half_pen
                  penu(i,j,k,2)=half_pen
                  x=grid_x(i-1)-centre_sphere(1)
                  y=grid_y(j)  -centre_sphere(2)
                  z=grid_z(k)  -centre_sphere(3)
                  r=sqrt(x**2+y**2+z**2)
                  uin(i,j,k)=2d0*(U_inf-3d0*rayon_sphere/4d0*(U_inf/(r+1D-40)+x**2*U_inf/(r**3+1D-40))&
                            -3d0*rayon_sphere**3/4d0*(U_inf/(3d0*r**3+1D-40)-x**2*U_inf/(r**5+1D-40)))
               endif
            enddo
         enddo
      enddo

      !-------------------------------------------------------------------------------
      ! Limite INF : composante normale = conposante 2
      !-------------------------------------------------------------------------------
      do k=szv,ezv
         do j=syv,eyv
            do i=sxv,exv
               if (j==gsyv) then
                  penv(i,j,k,:)=0
                  penv(i,j,k,1)=half_pen
                  penv(i,j,k,5)=half_pen
                  x=grid_x(i)-centre_sphere(1)
                  y=grid_y(j)-centre_sphere(2)
                  z=grid_z(k)-centre_sphere(3)
                  r=sqrt(x**2+y**2+z**2)
                  vin(i,j,k)=2d0*(-3d0*rayon_sphere/4d0*(x*y*U_inf/&
                             (r**3+1D-40))+3d0*rayon_sphere**3/4d0*(x*y*U_inf/(r**5+1D-40)))
               endif
            enddo
         enddo
      enddo
      !-------------------------------------------------------------------------------
      ! Limite SUP : composante normale = conposante 2
      !-------------------------------------------------------------------------------
      do k=szv,ezv
         do j=syv,eyv
            do i=sxv,exv
               if (j==geyv) then
                  penv(i,j,k,:)=0
                  penv(i,j,k,1)=half_pen
                  penv(i,j,k,4)=half_pen
                  x=grid_x(i)  -centre_sphere(1)
                  y=grid_y(j-1)-centre_sphere(2)
                  z=grid_z(k)  -centre_sphere(3)
                  r=sqrt(x**2+y**2+z**2)
                  vin(i,j,k)=2d0*(-3d0*rayon_sphere/4d0*(x*y*U_inf/&
                             (r**3+1D-40))+3d0*rayon_sphere**3/4d0*(x*y*U_inf/(r**5+1D-40)))
               endif
            enddo
         enddo
      enddo
      !-------------------------------------------------------------------------------
      ! Limite Backward: composante normale = conposante 3
      !-------------------------------------------------------------------------------
      do k=szw,ezw
         do j=syw,eyw
            do i=sxw,exw
               if (k==gszw) then
                  penw(i,j,k,:)=0
                  penw(i,j,k,1)=half_pen
                  penw(i,j,k,7)=half_pen
                  x=grid_x(i)-centre_sphere(1)
                  y=grid_y(j)-centre_sphere(2)
                  z=grid_z(k)-centre_sphere(3)
                  r=sqrt(x**2+y**2+z**2)
                  win(i,j,k)=2d0*(-3d0*rayon_sphere/4d0*(x*z*U_inf/(r**3+1D-40)) & 
                                  +3d0*rayon_sphere**3/4d0*(x*z*U_inf/(r**5+1D-40)))
               endif
            enddo
         enddo
      enddo
      !-------------------------------------------------------------------------------
      ! Limite Forward: composante normale = conposante 3
      !-------------------------------------------------------------------------------
      do k=szw,ezw
         do j=syw,eyw
            do i=sxw,exw
               if (k==gezw) then
                  penw(i,j,k,:)=0
                  penw(i,j,k,1)=half_pen
                  penw(i,j,k,6)=half_pen
                  x=grid_x(i)  -centre_sphere(1)
                  y=grid_y(j)  -centre_sphere(2)
                  z=grid_z(k-1)-centre_sphere(3)
                  r=sqrt(x**2+y**2+z**2)
                  win(i,j,k)=2d0*(-3d0*rayon_sphere/4d0*(x*z*U_inf/(r**3+1D-40)) & 
                                  +3d0*rayon_sphere**3/4d0*(x*z*U_inf/(r**5+1D-40)))
               endif
            enddo
         enddo
      enddo

      !===============================================================================
      ! Traitement des vitesses tangentielles
      !===============================================================================
      !-------------------------------------------------------------------------------
      ! Limite GAUCHE ou DROITE
      !-------------------------------------------------------------------------------
      ! traitement de la composante tangentielle de la vitesse (conposante 2)
      do k=szv,ezv
         do j=syv,eyv
            do i=sxv,exv
               if (i==gsxv .or. i==gexv) then
                  penv(i,j,k,:)=0
                  penv(i,j,k,1)=full_pen
                  x=grid_x (i)-centre_sphere(1)
                  y=grid_yv(j)-centre_sphere(2)
                  z=grid_z (k)-centre_sphere(3)
                  r=sqrt(x**2+y**2+z**2)
                  vin(i,j,k)=-3d0*rayon_sphere/4d0*(x*y*U_inf/(r**3+1D-40))&
                             +3d0*rayon_sphere**3/4d0*(x*y*U_inf/(r**5+1D-40))
               endif
            enddo
         enddo
      enddo
      ! traitement de la composante tangentielle de la vitesse (conposante 3)
      do k=szw,ezw
         do j=syw,eyw
            do i=sxw,exw
               if (i==gsxw .or. i==gexw) then
                  penw(i,j,k,:)=0
                  penw(i,j,k,1)=full_pen
                  x=grid_x (i)-centre_sphere(1)
                  y=grid_y (j)-centre_sphere(2)
                  z=grid_zw(k)-centre_sphere(3)
                  r=sqrt(x**2+y**2+z**2)
                  win(i,j,k)=-3d0*rayon_sphere/4d0*(x*z*U_inf/(r**3+1D-40)) & 
                             +3d0*rayon_sphere**3/4d0*(x*z*U_inf/(r**5+1D-40))
               endif
            enddo
         enddo
      enddo

      !-------------------------------------------------------------------------------
      ! Limite INF ou SUP
      !-------------------------------------------------------------------------------
      ! traitement de la composante tangentielle de la vitesse (conposante 1)
      do k=szu,ezu
         do j=syu,eyu
            do i=sxu,exu
               if (j==gsyu .or. j==geyu) then
                  penu(i,j,k,:)=0
                  penu(i,j,k,1)=full_pen
                  x=grid_xu(i)-centre_sphere(1)
                  y=grid_y (j)-centre_sphere(2)
                  z=grid_z (k)-centre_sphere(3)
                  r=sqrt(x**2+z**2+y**2)
                  uin(i,j,k)=U_inf-3d0*rayon_sphere/4d0*(U_inf/(r+1D-40)+x**2*U_inf/(r**3+1D-40)) - &
                             3d0*rayon_sphere**3/4d0*(U_inf/(3d0*r**3+1D-40)-x**2*U_inf/(r**5+1D-40))
               endif
            enddo
         enddo
      enddo
      ! traitement de la composante tangentielle de la vitesse (conposante 3)
      do k=szw,ezw
         do j=syw,eyw
            do i=sxw,exw
               if (j==gsyw .or. j==geyw) then
                  penw(i,j,k,:)=0
                  penw(i,j,k,1)=full_pen
                  x=grid_x (i)-centre_sphere(1)
                  y=grid_y (j)-centre_sphere(2)
                  z=grid_zw(k)-centre_sphere(3)
                  r=sqrt(x**2+z**2+y**2)
                  win(i,j,k)=-3d0*rayon_sphere/4d0*(x*z*U_inf/(r**3+1D-40)) & 
                             +3d0*rayon_sphere**3/4d0*(x*z*U_inf/(r**5+1D-40))
               endif
            enddo
         enddo
      enddo

      !-------------------------------------------------------------------------------
      ! Limite Backward ou Forward
      !-------------------------------------------------------------------------------
      ! traitement de la composante tangentielle de la vitesse (conposante 1)
      do k=szu,ezu
         do j=syu,eyu
            do i=sxu,exu
               if (k==gszu .or. k==gezu) then
                  penu(i,j,k,:)=0
                  penu(i,j,k,1)=full_pen
                  x=grid_xu(i)-centre_sphere(1)
                  y=grid_y (j)-centre_sphere(2)
                  z=grid_z (k)-centre_sphere(3)
                  r=sqrt(x**2+z**2+y**2)
                  uin(i,j,k)=U_inf-3d0*rayon_sphere/4d0*(U_inf/(r+1D-40)+x**2*U_inf/(r**3+1D-40)) - &
                             3d0*rayon_sphere**3/4d0*(U_inf/(3d0*r**3)-x**2*U_inf/(r**5+1D-40))
               endif
            enddo
         enddo
      enddo
      ! traitement de la composante tangentielle de la vitesse (conposante 2)
      do k=szv,ezv
         do j=syv,eyv
            do i=sxv,exv
               if (k==gszv .or. k==gezv) then
                  penv(i,j,k,:)=0
                  penv(i,j,k,1)=full_pen
                  x=grid_x (i)-centre_sphere(1)
                  y=grid_yv(j)-centre_sphere(2)
                  z=grid_z (k)-centre_sphere(3)
                  r=sqrt(x**2+y**2+z**2)
                  vin(i,j,k)=-3d0*rayon_sphere/4d0*(x*y*U_inf/&
                             (r**3+1D-40))+3d0*rayon_sphere**3/4d0*(x*y*U_inf/(r**5+1D-40))
               endif
            enddo
         enddo
      enddo

      !-------------------------------------------------------------------------------
      if (deeptracking) write(*,*) 'entree BC_Velocity_UniformStokesFlowPastAParticle_Sphere'
      !-------------------------------------------------------------------------------
  end subroutine BC_Velocity_UniformStokesFlowPastAParticle_Sphere

  !===============================================================================
end module Bib_VOFLag_BC_Velocity_UniformStokesFlowPastAParticle_Sphere
!===============================================================================