!========================================================================
!
!                   FUGU Version 1.0.0
!
!========================================================================
!**
!**   NAME        : Lagrangian2Eulerian.f90
!**
!**   AUTHOR      : Beno\^it Trouette
!**                 
!**   FUNCTION    : modules for Lagrangian particle tracking of FUGU software
!**
!**   DATES       : Version 1.0.0  : from : December, 2018
!**
!========================================================================

module mod_Lagrangian2Eulerian
  use mod_timers


  integer, parameter :: AVE_STAT_NO_WEIGHT = 0
  integer, parameter :: AVE_STAT_WEIGHED   = 1


contains

  subroutine ComputeScaAverages2(iavg,mesh,Lpart,nPartL,avrg)
    !*******************************************************************************
    !   Modules
    !*******************************************************************************
    use mod_Parameters, only: nppdpc
    use mod_struct_grid
    use mod_struct_particle_tracking
    use mod_search
    !*******************************************************************************
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)                                        :: nPartL
    integer, intent(in)                                        :: iavg
    real(8), allocatable, dimension(:,:,:), intent(inout)      :: avrg
    type(grid_t), intent(in)                                   :: mesh
    type(particle_t), allocatable, dimension(:), intent(inout) :: Lpart
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                                    :: i,j,k,n
    real(8)                                                    :: tmin,tmax,dump,amp
    integer, allocatable, dimension(:,:,:)                     :: iwght
    real(8), allocatable, dimension(:,:,:)                     :: wght
    real(8), allocatable, dimension(:,:,:,:)                   :: davrg,dwght
    logical                                                    :: found = .false.
    !-------------------------------------------------------------------------------
    call compute_time(TIMER_START,"[sub] ComputeScaAverages2") 

    !-------------------------------------------------------------------------------
    ! initialization
    !-------------------------------------------------------------------------------
    ! avrg=0 ! done before the call
    !-------------------------------------------------------------------------------
    allocate(iwght(mesh%sx-mesh%gx:mesh%ex+mesh%gx,mesh%sy-mesh%gy:mesh%ey+mesh%gy,mesh%sz-mesh%gz:mesh%ez+mesh%gz))
    allocate( wght(mesh%sx-mesh%gx:mesh%ex+mesh%gx,mesh%sy-mesh%gy:mesh%ey+mesh%gy,mesh%sz-mesh%gz:mesh%ez+mesh%gz))
    allocate(dwght(mesh%sx-mesh%gx:mesh%ex+mesh%gx,mesh%sy-mesh%gy:mesh%ey+mesh%gy,mesh%sz-mesh%gz:mesh%ez+mesh%gz,4*nppdpc**mesh%dim))
    allocate(davrg(mesh%sx-mesh%gx:mesh%ex+mesh%gx,mesh%sy-mesh%gy:mesh%ey+mesh%gy,mesh%sz-mesh%gz:mesh%ez+mesh%gz,4*nppdpc**mesh%dim))
    iwght = 0
    wght  = 0
    dwght = 0
    davrg = 0
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    do n = 1,nPartL
       if (Lpart(n)%active) then
          !-------------------------------------------------------------------------------
          ! index of the cell
          !-------------------------------------------------------------------------------
          call search_cell_ijk(mesh,Lpart(n)%r,Lpart(n)%ijk,PRESS_MESH,found)
          if (found) then 
             i = Lpart(n)%ijk(1)
             j = Lpart(n)%ijk(2)
             k = Lpart(n)%ijk(3)
             !-------------------------------------------------------------------------------
             ! Particule cell weight
             !-------------------------------------------------------------------------------
             select case(iavg)
             case(AVE_STAT_NO_WEIGHT)
                iwght(i,j,k)              = iwght(i,j,k) + 1 ! count
                davrg(i,j,k,iwght(i,j,k)) = Lpart(n)%phi     ! value
                dwght(i,j,k,iwght(i,j,k)) = 1                ! weight
             case(AVE_STAT_WEIGHED)
                iwght(i,j,k)              = iwght(i,j,k) + 1                 ! count
                davrg(i,j,k,iwght(i,j,k)) = Lpart(n)%phi                     ! value
                dwght(i,j,k,iwght(i,j,k)) = product(Lpart(n)%dl(1:mesh%dim)) ! weight
             case default
                stop
             end select
          end if
          !-------------------------------------------------------------------------------
       end if
    end do
    !-------------------------------------------------------------------------------
    
    !-------------------------------------------------------------------------------
    ! averaged value 
    !-------------------------------------------------------------------------------
    do k = mesh%sz-mesh%gz,mesh%ez+mesh%gz
       do j = mesh%sy-mesh%gy,mesh%ey+mesh%gy
          do i = mesh%sx-mesh%gx,mesh%ex+mesh%gx
             !-------------------------------------------------------------------------------
             if (iwght(i,j,k)>0) then
                avrg(i,j,k) = sum(davrg(i,j,k,:)*dwght(i,j,k,:)) ! sum of pondered values
                wght(i,j,k) = sum(dwght(i,j,k,:))                ! sum of weight
                if (wght(i,j,k)>0) then
                   avrg(i,j,k) = avrg(i,j,k)/wght(i,j,k)         ! average
                end if
             end if
             !-------------------------------------------------------------------------------
          end do
       end do
    end do
    !-------------------------------------------------------------------------------

    call compute_time(TIMER_END,"[sub] ComputeScaAverages2") 
  end subroutine ComputeScaAverages2
  
end module mod_Lagrangian2Eulerian
