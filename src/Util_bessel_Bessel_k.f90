!===============================================================================
module Bib_VOFLag_Bessel_k
  !===============================================================================
  contains
  !-----------------------------------------------------------------------------  
  !> @author  Wided Bizid, mohamed-amine chadil
  ! 
  !> @brief cette routine 
  !
  !>
  !-----------------------------------------------------------------------------
  function Bessel_k(n,x)  !bessk
    !===============================================================================
    !modules
    !===============================================================================
    !-------------------------------------------------------------------------------
    !Modules de definition des structures et variables => src/mod/module_...f90
    !-------------------------------------------------------------------------------
    use module_commun_gui,  only : is_debug
    !-------------------------------------------------------------------------------
    !Modules de subroutines de la librairie VOF-Lag
    !-------------------------------------------------------------------------------
    use Bib_VOFLag_k0
    use Bib_VOFLag_k1
    !-------------------------------------------------------------------------------
    
    !===============================================================================
    !declarations des variables
    !===============================================================================
    implicit none
    !-------------------------------------------------------------------------------
    !variables globales
    !-------------------------------------------------------------------------------
    double precision  :: x 
    integer           :: n   
    !-------------------------------------------------------------------------------
    !variables locales 
    !-------------------------------------------------------------------------------
    double precision  :: Bessel_k,bk,bkm,bkp,tox
    integer           :: j  
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    !if (is_debug) write(*,*) 'entree Bessel_k'
    !-------------------------------------------------------------------------------

    if (n.lt.2) then 
      write (*,*) 'bad argument n in Bessel_k'  
      stop
    end if

    tox=2.0/(x+1D-40) 
    bkm=k0(x+1D-40)  
    bk=k1(x+1D-40)  

    do 11 j=1,n-1  
      bkp=bkm+j*tox*bk  
      bkm=bk  
      bk=bkp  
    11     continue  
       
    Bessel_k=bk  

    return  

    !-------------------------------------------------------------------------------
    !if (is_debug) write(*,*) 'sortie Bessel_k'
    !-------------------------------------------------------------------------------
  end function Bessel_k

  !===============================================================================
end module Bib_VOFLag_Bessel_k
!===============================================================================

  


