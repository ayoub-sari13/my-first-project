/* Compile with MUMPS (yes:1, no:0) */
#define MUMPS 0

/* Compile with HYPRE (yes:1, no:0) */
#define HYPRE 1

/* Compile with FFTW (yes:1, no:0) */
#define FFTW3 0

/* Compile with LAPACK (yes:1, no:0) */
#define LAPACK 0

/* Compile with HDF5 libs (yes:1, no:0) */
#define WRITE_HDF5 0

/* Ifort ? (optional) */
#define IFORT 0

/* INRS CONFIG */
#define INRS 0

/* RESPECT */
#define RESPECT 1


