!===============================================================================
module Bib_VOFLag_caracteristiques_thermophys
  !===============================================================================
  contains
  !---------------------------------------------------------------------------  
  !> @author amine chadil
  ! 
  !> @brief cette routine calcul à partir de la fonction couleur et les caracteristiques 
  !> de chacune des phases les variables diphasiques correspondantes, avec la loi harmonique
  !> pour les viscosites
  !
  !> @param[out] rho   : masse volumiques dans le maillage de pression
  !> @param[out] rovu  : masse volumiques dans le maillage de la premiere composante de la vitesse
  !> @param[out] rovv  : masse volumiques dans le maillage de la deuxieme composante de la vitesse
  !> @param[out] rovw  : masse volumiques dans le maillage de la troisieme composante de la vitesse
  !> @param[out] vie   : viscosite d'elongation stockee dans le maillage de pression 
  !> @param[out] vir   : viscosite de rotation stockee dans le maillage de viscosite 
  !> @param[out] vis   : viscosite de cisaillement stockee dans le maillage de viscosite 
  !> @param[in]  cou   : fonction couleur calculee dans le maillage de pression
  !---------------------------------------------------------------------------  
  subroutine caracteristiques_thermophys(rho,rovu,rovv,rovw,vie,vir,vis,cou,cou_vis,cou_vts,ndim)
    !===============================================================================
    !modules
    !===============================================================================
    !-------------------------------------------------------------------------------
    !Modules de definition des structures et variables => src/mod/module_...f90
    !-------------------------------------------------------------------------------
    use mod_struct_thermophysics, only : fluids
    use mod_Parameters,           only : deeptracking,sxus,exus,syus,eyus,szus,ezus,sxvs,exvs,&
                               syvs,eyvs,szvs,ezvs,sxws,exws,syws,eyws,szws,ezws,sx,ex,sy,ey,sz,ez
    !-------------------------------------------------------------------------------

    !===============================================================================
    !declarations des variables
    !===============================================================================
    implicit none
    !-------------------------------------------------------------------------------
    !variables globales
    !-------------------------------------------------------------------------------
    real(8), dimension(:,:,:,:), allocatable :: vir,vis,cou_vis,cou_vts
    real(8), dimension(:,:,:),   allocatable :: cou,rho,rovu,rovv,rovw,vie
    integer                                  :: i,j,k
    !-------------------------------------------------------------------------------
    !variables locales 
    !-------------------------------------------------------------------------------
    real(8)                                :: cou_rov,cou_vir
    integer                                :: ndim
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'entree caracteristiques_thermophys'
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! calcul en moyenne arithmetiques de la masse volumique dans les points de pression 
    !-------------------------------------------------------------------------------
    rho=cou*fluids(2)%rho+(1.d0-cou)*fluids(1)%rho

    !-------------------------------------------------------------------------------
    ! calcul en moyenne arithmetiques de la masse volumique dans les points de vitesses
    !-------------------------------------------------------------------------------
    if (ndim .eq. 2) then
      do j=syus,eyus
          do i=sxus,exus
            cou_rov=cou_vts(i,j,1,1)
            !cou_rov=0.5d0*(cou(i-1,j,1)+cou(i,j,1))
            rovu(i,j,1)=cou_rov*fluids(2)%rho+(1.d0-cou_rov)*fluids(1)%rho
          enddo
      enddo
      do j=syvs,eyvs
          do i=sxvs,exvs
            cou_rov=cou_vts(i,j,1,2)
            !cou_rov=0.5d0*(cou(i,j-1,1)+cou(i,j,1))
            rovv(i,j,1)=cou_rov*fluids(2)%rho+(1.d0-cou_rov)*fluids(1)%rho
          enddo
      enddo
    else
      do k=szus,ezus
          do j=syus,eyus
            do i=sxus,exus
              cou_rov=cou_vts(i,j,k,1)
              !cou_rov=0.5d0*(cou(i-1,j,k)+cou(i,j,k))
              rovu(i,j,k)=cou_rov*fluids(2)%rho+(1.d0-cou_rov)*fluids(1)%rho
            end do
          end do
      end do
      do k=szvs,ezvs
          do j=syvs,eyvs
            do i=sxvs,exvs
              cou_rov=cou_vts(i,j,k,2)!0.5d0*(cou(i,j-1,k)+cou(i,j,k))
              !cou_rov=0.5d0*(cou(i,j-1,k)+cou(i,j,k))
              rovv(i,j,k)=cou_rov*fluids(2)%rho+(1.d0-cou_rov)*fluids(1)%rho
            end do
          end do
      end do
      do k=szws,ezws
          do j=syws,eyws
            do i=sxws,exws
              cou_rov=cou_vts(i,j,k,3)!0.5d0*(cou(i,j,k-1)+cou(i,j,k))
              !cou_rov=0.5d0*(cou(i,j,k-1)+cou(i,j,k))
              rovw(i,j,k)=cou_rov*fluids(2)%rho+(1.d0-cou_rov)*fluids(1)%rho
            end do
          end do
      end do
    endif

    !-------------------------------------------------------------------------------
    ! calcul de la viscosite stockee sur la grille de pression avec le modele harmonique 
    !-------------------------------------------------------------------------------
    vie=2.d0*fluids(1)%mu*fluids(2)%mu/(cou*fluids(1)%mu+(1-cou)*fluids(2)%mu+1D-40)

    !-------------------------------------------------------------------------------
    ! calcul de la viscosite stockee sur la grille de viscosite avec le modele harmonique 
    !-------------------------------------------------------------------------------
    if (ndim .eq. 2) then
      do j=sy,ey+1
          do i=sx,ex+1
            cou_vir=cou_vis(i,j,1,1)
            vir(i,j,1,1)=fluids(1)%mu*fluids(2)%mu/(cou_vir*fluids(1)%mu+(1-cou_vir)*fluids(2)%mu)
            vis(i,j,1,1)=2d0*vir(i,j,1,1)
          enddo
      enddo
    else
      do k=sz,ez+1
          do j=sy,ey+1
            do i=sx,ex+1
                !-------------------------------------------------
                ! plane with z-normal vector
                !-------------------------------------------------
                cou_vir=cou_vis(i,j,k,3)
                !cou_vir=0.25d0*(cou(i,j,k)+cou(i,j-1,k)+cou(i-1,j,k)+cou(i-1,j-1,k))
                vir(i,j,k,3)=fluids(1)%mu*fluids(2)%mu/(cou_vir*fluids(1)%mu+(1-cou_vir)*fluids(2)%mu)
                vis(i,j,k,3)=2d0*vir(i,j,k,3)
                !-------------------------------------------------
                ! plane with y-normal vector
                !-------------------------------------------------
                cou_vir=cou_vis(i,j,k,2)
                !cou_vir=0.25d0*(cou(i,j,k)+cou(i,j,k-1)+cou(i-1,j,k)+cou(i-1,j,k-1))
                vir(i,j,k,2)=fluids(1)%mu*fluids(2)%mu/(cou_vir*fluids(1)%mu+(1-cou_vir)*fluids(2)%mu)
                vis(i,j,k,2)=2d0*vir(i,j,k,2)
                !-------------------------------------------------
                ! plane with x-normal vector
                !-------------------------------------------------
                cou_vir=cou_vis(i,j,k,1)
                !cou_vir=0.25d0*(cou(i,j,k)+cou(i,j,k-1)+cou(i,j-1,k)+cou(i,j-1,k-1))
                vir(i,j,k,1)=fluids(1)%mu*fluids(2)%mu/(cou_vir*fluids(1)%mu+(1-cou_vir)*fluids(2)%mu)
                vis(i,j,k,1)=2d0*vir(i,j,k,1)
            end do
          end do
      end do
    endif

    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'sortie caracteristiques_thermophys'
    !-------------------------------------------------------------------------------
  end subroutine caracteristiques_thermophys

  !===============================================================================
end module Bib_VOFLag_caracteristiques_thermophys
!===============================================================================