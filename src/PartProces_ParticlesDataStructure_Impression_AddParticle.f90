!===============================================================================
module Bib_VOFLag_ParticlesDataStructure_Impression_AddParticle
  !===============================================================================
  contains
  !-----------------------------------------------------------------------------  
  !> @author jorge cesar brandle de motta, amine chadil
  ! 
  !> @brief permet d'ajouter un element de plus a un tableau d'entiers
  !
  !> @param [out] tab : le tableau a modifier
  !> @param [in]  n   : l'element a ajouter au tableau
  !-----------------------------------------------------------------------------
  subroutine ParticlesDataStructure_Impression_AddParticle(tab,n)
    !===============================================================================
    !modules
    !===============================================================================
    !-------------------------------------------------------------------------------
    !Modules de definition des structures et variables => src/mod/module_...f90
    !-------------------------------------------------------------------------------
    use mod_Parameters, only : deeptracking
    !-------------------------------------------------------------------------------

    !===============================================================================
    !declarations des variables
    !===============================================================================
    implicit none
    !-------------------------------------------------------------------------------
    !variables globales
    !-------------------------------------------------------------------------------
    integer, dimension(:), allocatable  :: tab
    integer                             :: n
    !-------------------------------------------------------------------------------
    !variables locales 
    !-------------------------------------------------------------------------------
    integer, dimension(:), allocatable  :: temp_tab
    integer                             :: i
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'entree ParticlesDataStructure_Impression_AddParticle'
    !-------------------------------------------------------------------------------

    if (allocated(tab)) then
       allocate(temp_tab(size(tab)+1))
       do i=1,size(tab)
          temp_tab(i) = tab(i)
       end do
       temp_tab(size(tab)+1)=n
       deallocate(tab)
       allocate(tab(size(temp_tab)))
       do i=1,size(temp_tab)
          tab(i) = temp_tab(i)
       end do
       deallocate(temp_tab) 
    else
       allocate(tab(1))
       tab(1)=n
    end if

    !-------------------------------------------------------------------------------
    if (deeptracking) write(*,*) 'sortie ParticlesDataStructure_Impression_AddParticle'
    !-------------------------------------------------------------------------------
  end subroutine ParticlesDataStructure_Impression_AddParticle

  !===============================================================================
end module Bib_VOFLag_ParticlesDataStructure_Impression_AddParticle
!===============================================================================
  



