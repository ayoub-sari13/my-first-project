!========================================================================
!
!                   FUGU Version 1.0.0
!
!========================================================================
!**
!**   NAME        : ParticleTracking.f90
!**
!**   AUTHOR      : St\'ephane Vincent
!**                 Beno\^it Trouette
!**                 Georges Halim Atallah
!**
!**   FUNCTION    : modules for Lagrangian particle tracking of FUGU software
!**
!**   DATES       : Version 1.0.0  : from : December, 2018
!**
!========================================================================

!===============================================================================
module mod_Lagrangian_Particle
  use mod_Parameters
  use mod_Constants
  use mod_struct_grid
  use mod_struct_Particle_Tracking
  use mod_interpolation
  use mod_deriv_xyz
  use mod_LevelSet
  use mod_material_derivative
  use mod_struct_thermophysics
  use mod_references
  use mod_ParticleConnectivity
  use mod_search
  use mod_timers
  use mod_borders

  !----------------------------------------------------------------------------
  ! Global variables
  !----------------------------------------------------------------------------
  real(8), dimension(3)                    :: tmp1,tmp2,tmp3
  !----------------------------------------------------------------------------
  ! max velocity for lagrangian CFL
  !----------------------------------------------------------------------------
  real(8), dimension(3)                    :: umax_lag=0
  !----------------------------------------------------------------------------
  ! periodicity of the computationnal domain
  !----------------------------------------------------------------------------
  logical, dimension(3)                    :: periodicity=.true.
  !----------------------------------------------------------------------------
  ! fraction volume of particle and force from the particles to the flow
  !----------------------------------------------------------------------------
  real(8)                                  :: theta,dtheta
  real(8), dimension(3)                    :: dforce_PartFlow
  real(8), allocatable, dimension(:,:,:)   :: porosity
  real(8), allocatable, dimension(:,:,:,:) :: force_PartFlow
  !----------------------------------------------------------------------------
  ! Particles arrays
  !----------------------------------------------------------------------------
  integer, allocatable, dimension(:)       :: tabCell
  !----------------------------------------------------------------------------

contains

  !==================================================================================================
  !==================================================================================================
  !==================================================================================================
  !==================================================================================================
  !==================================================================================================
  !==================================================================================================
  !==================================================================================================
  !==================================================================================================
  !==================================================================================================
  !==================================================================================================
  !==================================================================================================
  !==================================================================================================
  !==================================================================================================
  !==================================================================================================

  subroutine Lagrangian_Particle_Tracking(mesh,u,v,w,u0,v0,w0, &
       & LPart,nPart,nt,dim)
    !-------------------------------------------------------------------------------
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)                                        :: nt,nPart,dim
    real(8), allocatable, dimension(:,:,:), intent(in)         :: u,v,w,u0,v0,w0
    type(grid_t), intent(in)                                   :: mesh
    type(particle_t), allocatable, dimension(:), intent(inout) :: LPart
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                                    :: i,j,k
    integer                                                    :: i1,j1,n
    integer                                                    :: ntt,nbcfl,interp=2
    integer                                                    :: order_extra=2,order_time_derivative=1
    real(8)                                                    :: trest,time_lag_iter_start
    real(8)                                                    :: dteff,dtcfl,dtrest,dtcoll
    real(8)                                                    :: dt_star0,dt_star1,dt_star2
    real(8)                                                    :: var1,var2,var3
    real(8), dimension(dim)                                    :: vf
    real(8), allocatable, dimension(:,:,:)                     :: ut,vt,wt
    real(8), allocatable, dimension(:,:,:)                     :: dudx,dudy,dudz,dudyijk,dudzijk
    real(8), allocatable, dimension(:,:,:)                     :: dvdx,dvdy,dvdz,dvdxijk,dvdzijk
    real(8), allocatable, dimension(:,:,:)                     :: dwdx,dwdy,dwdz,dwdxijk,dwdyijk
    real(8), allocatable, dimension(:,:,:)                     :: alpha
    real(8), allocatable, dimension(:,:,:)                     :: dudt,dvdt,dwdt
    real(8), allocatable, dimension(:,:,:)                     :: u_extra0,v_extra0,w_extra0
    real(8), allocatable, dimension(:,:,:)                     :: u_extra1,v_extra1,w_extra1
    real(8), allocatable, dimension(:,:,:)                     :: u_extra2,v_extra2,w_extra2
    real(8), allocatable, dimension(:,:,:)                     :: rotx,roty,rotz
    real(8), allocatable, dimension(:,:,:)                     :: rotx0,roty0,rotz0
    real(8), allocatable, dimension(:,:,:)                     :: dudt0,dvdt0,dwdt0
    real(8), allocatable, dimension(:,:,:)                     :: alpha0
    real(8), allocatable, dimension(:,:,:)                     :: rotx_extra0,roty_extra0,rotz_extra0
    real(8), allocatable, dimension(:,:,:)                     :: rotx_extra1,roty_extra1,rotz_extra1
    real(8), allocatable, dimension(:,:,:)                     :: rotx_extra2,roty_extra2,rotz_extra2
    real(8), allocatable, dimension(:,:,:)                     :: dudt_extra0,dvdt_extra0,dwdt_extra0
    real(8), allocatable, dimension(:,:,:)                     :: dudt_extra1,dvdt_extra1,dwdt_extra1
    real(8), allocatable, dimension(:,:,:)                     :: dudt_extra2,dvdt_extra2,dwdt_extra2
    real(8), allocatable, dimension(:,:,:)                     :: alpha_extra0,alpha_extra1,alpha_extra2
    !-------------------------------------------------------------------------------

    call compute_time(TIMER_START,"[sub] Lagrangian_Particle_Tracking") 
    
    !-------------------------------------------------------------------------------
    ! Matrix allocation
    !-------------------------------------------------------------------------------
    if (LG_lift.or.LG_addm) then
       allocate(&
            & dudx(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz),    &
            & dudy(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz),    &
            & dvdx(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz),    &
            & dvdy(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz),    &
            & dudyijk(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz), &
            & dvdxijk(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
       dudx=0
       dudy=0
       dvdx=0
       dvdy=0
       dudyijk=0
       dvdxijk=0
       
       if (dim==3) then
          allocate(dudz(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz),  &
               & dvdz(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz),    &
               & dwdx(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz),    &
               & dwdy(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz),    &
               & dwdz(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz),    &
               & dudzijk(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz), &
               & dvdzijk(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz), &
               & dwdxijk(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz), & 
               & dwdyijk(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
          dudz=0
          dvdz=0
          dwdx=0
          dwdy=0
          dwdz=0
          dudzijk=0    
          dvdzijk=0    
          dwdxijk=0
          dwdyijk=0
       end if
    end if

    if (LG_lift) then
       allocate(rotz(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
       allocate(rotz0(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
       allocate(alpha(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
       allocate(alpha0(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
       allocate(rotz_extra0(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
       allocate(rotz_extra1(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
       allocate(alpha_extra0(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
       allocate(alpha_extra1(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
       rotz=0
       rotz0=0
       alpha=0
       alpha0=0
       rotz_extra0=0
       rotz_extra1=0
       alpha_extra0=0
       alpha_extra1=0
       
       if (dim==3) then
          allocate(rotx(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
          allocate(roty(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
          allocate(rotx0(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
          allocate(roty0(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
          allocate(rotx_extra0(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
          allocate(roty_extra0(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
          allocate(rotx_extra1(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
          allocate(roty_extra1(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
          rotx=0
          roty=0
          rotx0=0
          roty0=0
          rotx_extra0=0
          roty_extra0=0
          rotx_extra1=0
          roty_extra1=0
       end if
    end if

    if (LG_addm) then
       allocate(dudt(sxu-gx:exu+gx,syu-gy:eyu+gy,szu-gz:ezu+gz))
       allocate(dvdt(sxv-gx:exv+gx,syv-gy:eyv+gy,szv-gz:ezv+gz))
       allocate(dudt0(sxu-gx:exu+gx,syu-gy:eyu+gy,szu-gz:ezu+gz))
       allocate(dvdt0(sxv-gx:exv+gx,syv-gy:eyv+gy,szv-gz:ezv+gz))
       allocate(dudt_extra0(sxu-gx:exu+gx,syu-gy:eyu+gy,szu-gz:ezu+gz))
       allocate(dvdt_extra0(sxv-gx:exv+gx,syv-gy:eyv+gy,szv-gz:ezv+gz))
       allocate(dudt_extra1(sxu-gx:exu+gx,syu-gy:eyu+gy,szu-gz:ezu+gz))
       allocate(dvdt_extra1(sxv-gx:exv+gx,syv-gy:eyv+gy,szv-gz:ezv+gz))
       dudt=0
       dvdt=0
       dudt0=0
       dvdt0=0
       dudt_extra0=0
       dvdt_extra0=0
       dudt_extra1=0
       dvdt_extra1=0

       if (dim==3) then 
          allocate(dwdt(sxw-gx:exw+gx,syw-gy:eyw+gy,szw-gz:ezw+gz))
          allocate(dwdt0(sxw-gx:exw+gx,syw-gy:eyw+gy,szw-gz:ezw+gz))
          allocate(dwdt_extra0(sxw-gx:exw+gx,syw-gy:eyw+gy,szw-gz:ezw+gz))
          allocate(dwdt_extra1(sxw-gx:exw+gx,syw-gy:eyw+gy,szw-gz:ezw+gz))
          dwdt=0
          dwdt0=0
          dwdt_extra0=0
          dwdt_extra1=0
       end if
    end if

    allocate(u_extra0(sxu-gx:exu+gx,syu-gy:eyu+gy,szu-gz:ezu+gz))
    allocate(v_extra0(sxv-gx:exv+gx,syv-gy:eyv+gy,szv-gz:ezv+gz))
    allocate(u_extra1(sxu-gx:exu+gx,syu-gy:eyu+gy,szu-gz:ezu+gz))
    allocate(v_extra1(sxv-gx:exv+gx,syv-gy:eyv+gy,szv-gz:ezv+gz))
    if (dim==3) then 
       allocate(w_extra0(sxw-gx:exw+gx,syw-gy:eyw+gy,szw-gz:ezw+gz))
       allocate(w_extra1(sxw-gx:exw+gx,syw-gy:eyw+gy,szw-gz:ezw+gz))
    end if

    if (LG_RK==3.or.LG_RK==4.or.LG_RK==0) then
       allocate(u_extra2(sxu-gx:exu+gx,syu-gy:eyu+gy,szu-gz:ezu+gz))
       allocate(v_extra2(sxv-gx:exv+gx,syv-gy:eyv+gy,szv-gz:ezv+gz))
       if (LG_lift) then
          allocate(rotz_extra2(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
          allocate(alpha_extra2(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
       end if
       if (LG_addm) then 
          allocate(dudt_extra2(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
          allocate(dvdt_extra2(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
       end if
       if (dim==3) then
          allocate(w_extra2(sxw-gx:exw+gx,syw-gy:eyw+gy,szw-gz:ezw+gz))
          if (LG_lift) then
             allocate(rotx_extra2(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
             allocate(roty_extra2(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
          end if
          if (LG_addm) then 
             allocate(dwdt_extra2(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
          end if
       end if
    end if
    !-------------------------------------------------------------------------------


    !-------------------------------------------------------------------------------
    ! compute_cfl_lag
    !-------------------------------------------------------------------------------
    if (PartAreMarkers) then
       allocate(ut(sxu-gx:exu+gx,syu-gy:eyu+gy,szu-gz:ezu+gz))
       allocate(vt(sxv-gx:exv+gx,syv-gy:eyv+gy,szv-gz:ezv+gz))
       call transformation_2d3d(u,ut,ddxu,1)
       call transformation_2d3d(v,vt,ddyv,2)
       if (dim==3) then
          allocate(wt(sxw-gx:exw+gx,syw-gy:eyw+gy,szw-gz:ezw+gz))
          call transformation_2d3d(w,wt,ddzw,3)
       end if
       call cfl_ls_2d3d(ut,vt,wt,dtcfl,dtrest,nbcfl)
       deallocate(ut,vt)
       if (dim==3) deallocate(wt)
    else
       call compute_cfl_lag(umax_lag,dtcfl,dim)
       if (nproc>1) then
          write(*,*) "WARNING, CFL probably not good"
          write(*,*) "WARNING, CFL probably not good"
          write(*,*) "WARNING, CFL probably not good"
          write(*,*) "WARNING, CFL probably not good"
          write(*,*) "WARNING, CFL probably not good"
       end if
    end if
    !-------------------------------------------------------------------------------
    dtcfl=dt
    dtrest=0
    nbcfl=1
    !-------------------------------------------------------------------------------


    !-------------------------------------------------------------------------------
    ! extrapolation order (1 for first iteration)
    !-------------------------------------------------------------------------------
    order_extra=min(nt,2) 
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! spatial derivatives for rot or u grad u
    !-------------------------------------------------------------------------------
    if (LG_lift.or.LG_addm) then
       call deriv_xyz(u,v,w,dudx,dudy,dudz,dvdx,dvdy,dvdz,dwdx,dwdy,dwdz,&
            & dudyijk,dudzijk,dvdxijk,dvdzijk,dwdxijk,dwdyijk)
    end if
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! components of the rotationnal 
    !-------------------------------------------------------------------------------
    if (LG_lift) then
       if (dim==3) then 
          rotx=dwdy-dvdz    
          roty=dudz-dwdx
       end if
       rotz=dvdx-dudy
    end if
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! compute Du/Dt = \partial u / \partial t + u grad u
    !                            for Added mass & Tchen forces
    !-------------------------------------------------------------------------------
    if (LG_addm) then
       !----------------------------------------------------------------------------
       ! partial time derivative
       !----------------------------------------------------------------------------
       order_time_derivative=min(nt-1,1) 
       select case (order_time_derivative)
       case(0) ! zeroth order
          dudt=0
          dvdt=0
          dwdt=0
       case(1) ! first order
          dudt=(u-u0)/dt
          dvdt=(v-v0)/dt
          dwdt=(w-w0)/dt
       case default
          write(*,*) "order_time_derivative must be less than 2,STOP"
          stop
       end select
       !----------------------------------------------------------------------------

       !----------------------------------------------------------------------------
       ! material derivative 
       !----------------------------------------------------------------------------
       call Material_derivative(u,v,w,DuDt,DvDt,DwDt)
       !----------------------------------------------------------------------------
    end if
    !------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Time loop
    !  * dt is the eulerian (Navier-Stokes) time step
    !-------------------------------------------------------------------------------

    time_lag_iter_start=(nt-1)*dt
    dt_star1=0
    trest=dt
    do while (trest>0) 

       !-------------------------------------------------------------------------------
       ! Effective time step
       !-------------------------------------------------------------------------------
    
       dtcoll=1e15_8
       dteff=min(dtcfl,dtcoll)

       !--------------------
       ! Field Extrapolation
       !--------------------

       !----------------------------------------------------------------------------
       !        <----------dt----------->
       ! -------|-----------------------|-----------------------|-----------> (NS)
       !      var0                     var
       !                                             extra2
       !                       t+dteff         extra0  |
       !                          |                 |  |
       ! -|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----> (LAG)
       !        <----->     t                             |
       !         dteff                  <--dt_star0->     |
       !                                                  |
       !                                <----dt_star1---->|
       !                                                extra1
       !----------------------------------------------------------------------------

       dt_star0=dt_star1
       dt_star1=dt_star1+dteff
       dt_star2=dt_star1-dteff/2

       !----------------------------------------------------------------------------
       ! extrapolation at iteration start, index : 0
       !----------------------------------------------------------------------------
       if (dt_star0==0) then
          u_extra0=u
          v_extra0=v
          if (LG_lift) rotz_extra0=rotz
          if (LG_addm) dudt_extra0=dudt
          if (LG_addm) dvdt_extra0=dvdt
          if (LG_lift) alpha_extra0=alpha
          if (dim==3) then
             w_extra0=w
             if (LG_lift) rotx_extra0=rotx
             if (LG_lift) roty_extra0=roty
             if (LG_addm) dwdt_extra0=dwdt
          end if
       else 
          call extrapolation(u_extra0,u,u0,dt,dt_star0,order_extra)
          call extrapolation(v_extra0,v,v0,dt,dt_star0,order_extra)
          if (LG_lift) call extrapolation(rotz_extra0,rotz,rotz0,dt,dt_star0,order_extra)
          if (LG_addm) call extrapolation(dudt_extra0,dudt,dudt0,dt,dt_star0,order_extra) 
          if (LG_addm) call extrapolation(dvdt_extra0,dvdt,dvdt0,dt,dt_star0,order_extra)
          if (LG_lift) call extrapolation(alpha_extra0,alpha,alpha0,dt,dt_star0,order_extra)
          if (dim==3) then
             call extrapolation(w_extra0,w,w0,dt,dt_star0,order_extra)
             if (LG_lift) call extrapolation(rotx_extra0,rotx,rotx0,dt,dt_star0,order_extra)
             if (LG_lift) call extrapolation(roty_extra0,roty,roty0,dt,dt_star0,order_extra)
             if (LG_addm) call extrapolation(dwdt_extra0,dwdt,dwdt0,dt,dt_star0,order_extra)
          end if
       end if
       !----------------------------------------------------------------------------

       !----------------------------------------------------------------------------
       ! extrapolation over a full time step, index : 1
       !----------------------------------------------------------------------------
       call extrapolation(u_extra1,u,u0,dt,dt_star1,order_extra)
       call extrapolation(v_extra1,v,v0,dt,dt_star1,order_extra)
       if (LG_lift) call extrapolation(rotz_extra1,rotz,rotz0,dt,dt_star1,order_extra)
       if (LG_addm) call extrapolation(dudt_extra1,dudt,dudt0,dt,dt_star1,order_extra) 
       if (LG_addm) call extrapolation(dvdt_extra1,dvdt,dvdt0,dt,dt_star1,order_extra)
       if (LG_lift) call extrapolation(alpha_extra1,alpha,alpha0,dt,dt_star1,order_extra)
       if (dim==3) then
          call extrapolation(w_extra1,w,w0,dt,dt_star1,order_extra)
          if (LG_lift) call extrapolation(rotx_extra1,rotx,rotx0,dt,dt_star1,order_extra)
          if (LG_lift) call extrapolation(roty_extra1,roty,roty0,dt,dt_star1,order_extra)
          if (LG_addm) call extrapolation(dwdt_extra1,dwdt,dwdt0,dt,dt_star1,order_extra)
       end if
       !----------------------------------------------------------------------------

       !----------------------------------------------------------------------------
       ! over half time step, index : 2
       !----------------------------------------------------------------------------
       if (LG_RK==3.or.LG_RK==4.or.LG_RK==0) then 
          call extrapolation(u_extra2,u,u0,dt,dt_star2,order_extra)
          call extrapolation(v_extra2,v,v0,dt,dt_star2,order_extra)
          if (LG_lift) call extrapolation(rotz_extra2,rotz,rotz0,dt,dt_star2,order_extra)
          if (LG_addm) call extrapolation(dudt_extra2,dudt,dudt0,dt,dt_star2,order_extra) 
          if (LG_addm) call extrapolation(dvdt_extra2,dvdt,dvdt0,dt,dt_star2,order_extra)
          if (LG_lift) call extrapolation(alpha_extra2,alpha,alpha0,dt,dt_star2,order_extra)
          if (dim==3) then 
             call extrapolation(w_extra2,w,w0,dt,dt_star2,order_extra)
             if (LG_lift) call extrapolation(rotx_extra2,rotx,rotx0,dt,dt_star2,order_extra)
             if (LG_lift) call extrapolation(roty_extra2,roty,roty0,dt,dt_star2,order_extra)
             if (LG_addm) call extrapolation(dwdt_extra2,dwdt,dwdt0,dt,dt_star2,order_extra)
          end if
       end if
       !----------------------------------------------------------------------------

       !----------------------------------------------------------------------------
       ! 2D Particle tracking (time integration scheme)
       !----------------------------------------------------------------------------
       call LPT(mesh,u_extra0,v_extra0,w_extra0,      &
            & u_extra1,v_extra1,w_extra1,             &
            & u_extra2,v_extra2,w_extra2,             &
            & rotx_extra0,roty_extra0,rotz_extra0,    &
            & rotx_extra1,roty_extra1,rotz_extra1,    &
            & rotx_extra2,roty_extra2,rotz_extra2,    &
            & dudt_extra0,dvdt_extra0,dwdt_extra0,    &
            & dudt_extra1,dvdt_extra1,dwdt_extra1,    &
            & dudt_extra2,dvdt_extra2,dwdt_extra2,    &
            & alpha_extra0,alpha_extra1,alpha_extra2, &
            & LPart,nPart,dteff,time_lag_iter_start)
       !----------------------------------------------------------------------------

!!$       !----------------------------------------------------------------------------
!!$       ! possible collisions
!!$       !----------------------------------------------------------------------------
!!$       select case (type_collision)
!!$       case(1)
!!$          if (dteff==dtcoll) call hard_sphere_collision(dtcoll,LPart,nPart,2)
!!$       case(2)
!!$       end select
!!$       !----------------------------------------------------------------------------

       !----------------------------------------------------------------------------
       ! remaining time
       !----------------------------------------------------------------------------
       time_lag_iter_start=time_lag_iter_start+dteff
       trest=trest-dteff
       if (trest<dtcfl) dtcfl=trest
       !----------------------------------------------------------------------------

    end do

    !-------------------------------------------------------------------------------
    call compute_time(TIMER_END,"[sub] Lagrangian_Particle_Tracking") 
  end subroutine Lagrangian_Particle_Tracking
  !***************************************************************************************
  !*******************************************************************************

  
  subroutine LPT(mesh,u,v,w,                  &
       & u_extra1,v_extra1,w_extra1,          &
       & u_extra2,v_extra2,w_extra2,          &
       & rotx,roty,rotz,                      &
       & rotx_extra1,roty_extra1,rotz_extra1, &
       & rotx_extra2,roty_extra2,rotz_extra2, &
       & dudt,dvdt,dwdt,                      &
       & dudt_extra1,dvdt_extra1,dwdt_extra1, &
       & dudt_extra2,dvdt_extra2,dwdt_extra2, &
       & alpha,alpha_extra1,alpha_extra2,     &
       & LPart,nPart,dv,time)
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables 
    !-------------------------------------------------------------------------------
    integer, intent(in)                                        :: nPart
    real(8), allocatable, dimension(:,:,:), intent(in)         :: u,v,w
    real(8), allocatable, dimension(:,:,:), intent(in)         :: u_extra1,v_extra1,w_extra1
    real(8), allocatable, dimension(:,:,:), intent(in)         :: u_extra2,v_extra2,w_extra2
    real(8), allocatable, dimension(:,:,:), intent(in)         :: rotx,roty,rotz
    real(8), allocatable, dimension(:,:,:), intent(in)         :: rotx_extra1,roty_extra1,rotz_extra1
    real(8), allocatable, dimension(:,:,:), intent(in)         :: rotx_extra2,roty_extra2,rotz_extra2
    real(8), allocatable, dimension(:,:,:), intent(in)         :: dudt,dvdt,dwdt
    real(8), allocatable, dimension(:,:,:), intent(in)         :: dudt_extra1,dvdt_extra1,dwdt_extra1
    real(8), allocatable, dimension(:,:,:), intent(in)         :: dudt_extra2,dvdt_extra2,dwdt_extra2
    real(8), allocatable, dimension(:,:,:), intent(in)         :: alpha,alpha_extra1,alpha_extra2
    type(grid_t), intent(in)                                   :: mesh
    type(particle_t), allocatable, dimension(:), intent(inout) :: LPart
    real(8), intent(in)                                        :: dv,time
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                                    :: i,j,k,n
    integer                                                    :: dir 
    integer                                                    :: code
    real(8), dimension(1:dim)                                  :: xyz,xyzn,xyznn
    real(8), dimension(1:dim)                                  :: xk1,xk2,xk3,xk4
    real(8), dimension(1:dim)                                  :: vk1,vk2,vk3,vk4
    real(8), dimension(1:dim)                                  :: vp,vpn,vpnn,vf,vfn,vfnn
    real(8), dimension(1:dim)                                  :: vr,vrn,vrnn,ap,apn
    real(8), dimension(1:dim)                                  :: xyz_extra2,vr_extra1
    real(8)                                                    :: coeff_m
    logical                                                    :: RK_loc=.false.
    !-------------------------------------------------------------------------------

    call compute_time(TIMER_START,"[sub] LPT") 

    !**************************************************************************************
    !************************************Collisions****************************************
    !**************************************************************************************

!!$    do n=1,nPart
!!$       LPart(n)%a=0
!!$    end do
!!$    call neighbor_list(LPart,nPart)
!!$    call compute_particle_interaction_force(LPart,nPart)

!!$    do n=1,nPart
!!$       write(*,*) LPart(n)%a
!!$    end do
!!$    write(*,*)
!!$    stop

    !**************************************************************************************
    !***************************lagrangian advection particles*****************************
    !**************************************************************************************

    !----------------------------------------------------
    ! init
    !----------------------------------------------------
    if (LG_2Way) force_PartFlow=0
    !----------------------------------------------------
    if (LG_RK==4) RK_loc=.true.
    !----------------------------------------------------


    do n=1,nPart
       
       !-----------------------------------------------------
       ! local dx spacing between particles
       ! also volume carried by a particle
       !-----------------------------------------------------
       if (.not.regular_mesh) then
!!$          call search_cell_ijk(LPart(n)%r,LPart(n)%ijk,0)
          LPart(n)%dl(1)=dx(LPart(n)%ijk(1))/nppdpc
          LPart(n)%dl(2)=dy(LPart(n)%ijk(2))/nppdpc
          LPart(n)%dl(3)=(dz(LPart(n)%ijk(3))/nppdpc)**(dim-2)
       end if
       !-----------------------------------------------------
       
       !--------------------------------------------------------------------
       ! Low order RK scheme if near boundaries
       !--------------------------------------------------------------------
       if (RK_loc .and. (LG_RK>2.or.LG_RK==0) ) then
          LPart(n)%RK=LG_RK
          call search_cell_ijk(mesh,LPart(n)%r,LPart(n)%ijk,PRESS_MESH)
          if (LPart(n)%ijk(1)<=gsx        &
               & .or.LPart(n)%ijk(1)>=gex &
               & .or.LPart(n)%ijk(2)<=gsy &
               & .or.LPart(n)%ijk(2)>=gey) then
             LPart(n)%RK=2
          end if
          if (dim==3) then 
             if (LPart(n)%ijk(3)<=gsz &
                  & .or.LPart(n)%ijk(3)>=gez) then
                LPart(n)%RK=2
             end if
          end if
       end if
       !--------------------------------------------------------------------
       
       if (LPart(n)%active) then

          !----------------------------------------------------
          !------ Interpolation method with RK1 ---------------
          !----------------------------------------------------
          if (LPart(n)%RK==1) then

             !--------------------------------------------------------------------
             ! initial position & relative velocity
             !--------------------------------------------------------------------
             xyz=LPart(n)%r(1:dim)
             vr=LPart(n)%v(1:dim)
             !--------------------------------------------------------------------


             !--------------------------------------------------------------------
             ! RHS and coeff_m of coeff_m dV/dt=sum f
             !--------------------------------------------------------------------
             call compute_particle_force(time,ap,coeff_m, &
                  & xyz,vr,                               & 
                  & rotx,roty,rotz,dudt,dvdt,dwdt,alpha,  &
                  & dv,LPart(n),dim)
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! acceleration
             !--------------------------------------------------------------------
             ap=ap/coeff_m              
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! eulerian velocity U_f interpolation at initial particle position 
             !--------------------------------------------------------------------
             call interpolation_uvw(time,u,v,w,xyz,vf,LPart(n)%interpol,dim)
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! total particle velocity
             !--------------------------------------------------------------------
             vp=vr+vf
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! new particle relative velocity & position
             !--------------------------------------------------------------------
             vrn=vr+dv*ap
             xyzn=xyz+dv*vp
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! eulerian velocity U_f interpolation at new particle position 
             !--------------------------------------------------------------------
             call interpolation_uvw(time+dv,u_extra1,v_extra1,w_extra1,&
                  & xyzn,vfn,LPart(n)%interpol,dim)
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! new total velocity
             !--------------------------------------------------------------------
             vpn=vrn+vfn
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! boundary conditions
             !--------------------------------------------------------------------
             call bound_cond(xyzn,vpn,LPart(n),code)
             if (code==1) then
                call interpolation_uvw(time+dv,u_extra1,v_extra1,w_extra1,&
                     & xyzn,vfn,LPart(n)%interpol,dim)
                vrn=vpn-vfn
             end if
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! maximum particle velocity for Lagrangian cfl condition
             !--------------------------------------------------------------------
             umax_lag(1:dim)=max(abs(vpn(1:dim)),umax_lag(1:dim))
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! update of the Lagrangian structure
             !--------------------------------------------------------------------
             LPart(n)%r(1:dim)=xyzn
             LPart(n)%v(1:dim)=vrn
             LPart(n)%u(1:dim)=vpn
             !--------------------------------------------------------------------

             !-----------------------------------------------------
             !------Interpolation method with RK2 EULER------------
             !-----------------------------------------------------
          elseif (LPart(n)%RK==2) then

             !--------------------------------------------------------------------
             ! initial position & relative velocity
             !--------------------------------------------------------------------
             xyz=LPart(n)%r(1:dim)
             vr=Lpart(n)%v(1:dim)
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! eulerian velocity U_f interpolation at initial particle position 
             !--------------------------------------------------------------------
             call interpolation_uvw(time,u,v,w,xyz,vf,LPart(n)%interpol,dim) 
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! total velocity
             !--------------------------------------------------------------------
             vp=vr+vf
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! RHS and coeff_m of coeff_m dV/dt=sum f
             !--------------------------------------------------------------------
             call compute_particle_force(time,ap,coeff_m, &
                  & xyz,vr,                               &
                  & rotx,roty,rotz,dudt,dvdt,dwdt,alpha,  &
                  & dv,LPart(n),dim)
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! acceleration
             !--------------------------------------------------------------------
             ap=ap/coeff_m
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! intermediate particle relative velocity & position
             !--------------------------------------------------------------------
             vrn=vr+dv*ap
             xyzn=xyz+dv*vp
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! eulerian velocity U_f interpolation at intermediate particle position 
             !--------------------------------------------------------------------
             call interpolation_uvw(time+dv,u_extra1,v_extra1,w_extra1,&
                  & xyzn,vfn,LPart(n)%interpol,dim)
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! intermediate total velocity
             !--------------------------------------------------------------------
             vpn=vrn+vfn
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! RHS and coeff_m of coeff_m dV/dt=sum f & acceleration
             !--------------------------------------------------------------------
             call compute_particle_force(time+dv,apn,coeff_m,&
                  & xyzn,vrn,                                &
                  & rotx_extra1,roty_extra1,rotz_extra1,     &
                  & dudt_extra1,dvdt_extra1,dwdt_extra1,     &
                  & alpha_extra1,dv,LPart(n),dim)
             !--------------------------------------------------------------------
             apn=apn/coeff_m
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! new particle relative velocity & position
             !--------------------------------------------------------------------
             vrnn=vr+d1p2*dv*(ap+apn)
             xyznn=xyz+d1p2*dv*(vp+vpn)
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! eulerian velocity U_f interpolation at new particle position 
             !--------------------------------------------------------------------
             call interpolation_uvw(time+dv,u_extra1,v_extra1,w_extra1,&
                  & xyznn,vfnn,LPart(n)%interpol,dim)
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! new total velocity
             !--------------------------------------------------------------------
             vpnn=vrnn+vfnn
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! boundary conditions
             !--------------------------------------------------------------------
             call bound_cond(xyznn,vpnn,LPart(n),code)
             if (code==1) then
                call interpolation_uvw(time+dv,u_extra1,v_extra1,w_extra1,&
                     & xyznn,vfnn,LPart(n)%interpol,dim)
                vrnn=vpnn-vfnn
             end if
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! maximum particle velocity for Lagrangian cfl condition
             !--------------------------------------------------------------------
             umax_lag(1:dim)=max(abs(vpnn(1:dim)),umax_lag(1:dim))
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! UPDATE of the Lagrangian structure
             !--------------------------------------------------------------------
             LPart(n)%r(1:dim)=xyznn
             LPart(n)%v(1:dim)=vrnn
             LPart(n)%u(1:dim)=vpnn
             !--------------------------------------------------------------------

             !-----------------------------------------------------
             !------Interpolation method with RK2 MID-POINT--------
             !-----------------------------------------------------
          elseif (LPart(n)%RK==3) then

             !--------------------------------------------------------------------
             ! initial position & relative velocity
             !--------------------------------------------------------------------
             xyz=LPart(n)%r(1:dim)
             vr=Lpart(n)%v(1:dim)
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! eulerian velocity U_f interpolation at initial particle position 
             !--------------------------------------------------------------------
             call interpolation_uvw(time,u,v,w,xyz,vf,LPart(n)%interpol,dim)
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! total particle velocity
             !--------------------------------------------------------------------
             vp=vr+vf
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! RHS and coeff_m of coeff_m dV/dt=sum f & acceleration
             !--------------------------------------------------------------------
             call compute_particle_force(time,ap,coeff_m, &
                  & xyz,vr,                               &
                  & rotx,roty,rotz,dudt,dvdt,dwdt,alpha,  &     
                  & dv,LPart(n),dim)
             !--------------------------------------------------------------------
             ap=ap/coeff_m
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! vector k1  ; k1=(v^(n)*dt \\ a^(n)*dt)
             !--------------------------------------------------------------------
             xk1=dv*vp
             vk1=dv*ap
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! intermediate position & relative velocity
             !--------------------------------------------------------------------
             xyzn=xyz+d1p2*xk1
             vrn=vr+d1p2*vk1
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! eulerian velocity U_f interpolation at intermediate particle position 
             !--------------------------------------------------------------------
             call interpolation_uvw(time+dv/2,u_extra2,v_extra2,w_extra2,&
                  & xyzn,vfn,LPart(n)%interpol,dim)
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! intermediate total velocity
             !--------------------------------------------------------------------
             vpn=vrn+vfn
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! RHS and coeff_m of coeff_m dV/dt=sum f
             !--------------------------------------------------------------------
             call compute_particle_force(time+dv/2,apn,coeff_m,&
                  & xyzn,vrn,                                  &
                  & rotx_extra2,roty_extra2,rotz_extra2,       &
                  & dudt_extra2,dvdt_extra2,dwdt_extra2,       &
                  & alpha_extra2,dv,LPart(n),dim)
             !--------------------------------------------------------------------
             apn=apn/coeff_m
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! new particle relative velocity & position
             !--------------------------------------------------------------------
             vrnn=vr+dv*apn
             xyznn=xyz+dv*vpn
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! eulerian velocity U_f interpolation at new particle position
             !--------------------------------------------------------------------
             call interpolation_uvw(time+dv,u_extra1,v_extra1,w_extra1,&
                  & xyznn,vfnn,LPart(n)%interpol,dim)
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! new total velocity
             !--------------------------------------------------------------------
             vpnn=vrnn+vfnn
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! boundary conditions
             !--------------------------------------------------------------------
             call bound_cond(xyznn,vpnn,LPart(n),code)
             if (code==1) then
                call interpolation_uvw(time+dv,u_extra1,v_extra1,w_extra1,&
                     & xyznn,vfnn,LPart(n)%interpol,dim)
                vrnn=vpnn-vfnn
             end if
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! maximum particle velocity for Lagrangian cfl condition
             !--------------------------------------------------------------------
             umax_lag(1:dim)=max(abs(vpnn(1:dim)),umax_lag(1:dim))
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! UPDATE of the Lagrangian structure
             LPart(n)%r(1:dim)=xyznn
             LPart(n)%v(1:dim)=vrnn
             LPart(n)%u(1:dim)=vpnn
             !--------------------------------------------------------------------


             !-----------------------------------------------------
             !------Interpolation method with RK4------------------
             !-----------------------------------------------------
          elseif (LPart(n)%RK==4) then

             !--------------------------------------------------------------------
             ! initial position & relative velocity
             !--------------------------------------------------------------------
             xyz=LPart(n)%r(1:dim)
             vr=Lpart(n)%v(1:dim)
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! eulerian velocity U_f interpolation at initial particle position 
             !--------------------------------------------------------------------
             call interpolation_uvw(time,u,v,w,xyz,vf,LPart(n)%interpol,dim)
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! total particle velocity
             !--------------------------------------------------------------------
             vp=vr+vf 
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! RHS and coeff_m of coeff_m dV/dt=sum f & acceleration
             !--------------------------------------------------------------------
             call compute_particle_force(time,ap,coeff_m, &
                  & xyz,vr,                               &
                  & rotx,roty,rotz,dudt,dvdt,dwdt,alpha,  &     
                  & dv,LPart(n),dim)
             !--------------------------------------------------------------------
             ap=ap/coeff_m
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! vector k1 ! k1=(dt*v^(n) \\ dt*a^(n))
             !--------------------------------------------------------------------
             xk1=dv*vp  
             vk1=dv*ap
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! intermediate position & relative velocity
             !--------------------------------------------------------------------
             xyzn=xyz+d1p2*xk1
             vrn=vr+d1p2*vk1  
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! eulerian velocity U_f interpolation at intermediate particle position 
             !--------------------------------------------------------------------
             call interpolation_uvw(time+dv/2,u_extra2,v_extra2,w_extra2,&
                  & xyzn,vfn,LPart(n)%interpol,dim)         
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! intermediate total velocity
             !--------------------------------------------------------------------
             vpn=vrn+vfn   ! v1^(n+1/2)=vr1^(n+1/2) + vf1^(n+1/2)_(r^(n+1/2))
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! RHS and coeff_m of coeff_m dV/dt=sum f
             !--------------------------------------------------------------------
             call compute_particle_force(time+dv/2,apn,coeff_m, &
                  & xyzn,vrn,                                   &
                  & rotx_extra2,roty_extra2,rotz_extra2,        &
                  & dudt_extra2,dvdt_extra2,dwdt_extra2,        &
                  & alpha_extra2,dv,LPart(n),dim)
             !--------------------------------------------------------------------
             apn=apn/coeff_m
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! vector k2
             !--------------------------------------------------------------------
             xk2=dv*vpn
             vk2=dv*apn
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! intermediate position & relative velocity
             !--------------------------------------------------------------------
             xyzn=xyz+d1p2*xk2 ! r2^(n+1/2)
             vrn=vr+d1p2*vk2   ! vr2^(n+1/2)
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! eulerian velocity U_f interpolation at intermediate particle position 
             !--------------------------------------------------------------------
             call interpolation_uvw(time+dv/2,u_extra2,v_extra2,w_extra2,&
                  & xyzn,vfn,LPart(n)%interpol,dim)
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! intermediate total velocity
             !--------------------------------------------------------------------
             vpn=vrn+vfn
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! RHS and coeff_m of coeff_m dV/dt=sum f
             !--------------------------------------------------------------------
             call compute_particle_force(time+dv/2,apn,coeff_m, &
                  & xyzn,vrn,                                   &
                  & rotx_extra2,roty_extra2,rotz_extra2,        &
                  & dudt_extra2,dvdt_extra2,dwdt_extra2,        &
                  & alpha_extra2,dv,LPart(n),dim)
             !--------------------------------------------------------------------
             apn=apn/coeff_m
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! vector k3
             !--------------------------------------------------------------------
             xk3=dv*vpn
             vk3=dv*apn
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! intermediate position & relative velocity
             !--------------------------------------------------------------------
             xyzn=xyz+xk3
             vrn=vr+vk3
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! eulerian velocity U_f interpolation at intermediate particle position 
             !--------------------------------------------------------------------
             call interpolation_uvw(time+dv,u_extra1,v_extra1,w_extra1,&
                  & xyzn,vfn,LPart(n)%interpol,dim)
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! intermediate total velocity
             !--------------------------------------------------------------------
             vpn=vrn+vfn
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! RHS and coeff_m of coeff_m dV/dt=sum f
             !--------------------------------------------------------------------
             call compute_particle_force(time+dv,apn,coeff_m, &
                  & xyzn,vrn,                         &
                  & rotx_extra1,roty_extra1,rotz_extra1, &
                  & dudt_extra1,dvdt_extra1,dwdt_extra1, &
                  & alpha_extra1,dv,LPart(n),dim)
             !--------------------------------------------------------------------
             apn=apn/coeff_m
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! vector k4
             !--------------------------------------------------------------------
             xk4=dv*vpn
             vk4=dv*apn
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! new particle relative velocity & position
             !--------------------------------------------------------------------
             xyznn=xyz+d1p6*(xk1+2*(xk2+xk3)+xk4)
             vrnn=vr+d1p6*(vk1+2*(vk2+vk3)+vk4)
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! eulerian velocity U_f interpolation at intermediate particle position 
             !--------------------------------------------------------------------
             call interpolation_uvw(time+dv,u_extra1,v_extra1,w_extra1,&
                  & xyznn,vfnn,LPart(n)%interpol,dim)
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! final total velocity
             !--------------------------------------------------------------------
             vpnn=vrnn+vfnn
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! boundary conditions
             !--------------------------------------------------------------------
             call bound_cond(xyznn,vpnn,LPart(n),code)
             if (code==1) then
                call interpolation_uvw(time+dv,u_extra1,v_extra1,w_extra1,&
                     & xyznn,vfnn,LPart(n)%interpol,dim)
                vrnn=vpnn-vfnn
             end if
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! maximum particle velocity for Lagrangian cfl condition
             !--------------------------------------------------------------------
             umax_lag(1:dim)=max(abs(vpnn(1:dim)),umax_lag(1:dim))
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! UPDATE of the Lagrangian structure
             !--------------------------------------------------------------------
             LPart(n)%r(1:dim)=xyznn
             LPart(n)%v(1:dim)=vrnn
             LPart(n)%u(1:dim)=vpnn
             !--------------------------------------------------------------------

             !------------------------------------------------------------------------
             !----------Interpolation method with Velocity-Verlet (explicit)----------
             !------------------------------------------------------------------------
          else if (LPart(n)%RK==0) then

             !--------------------------------------------------------------------
             ! intial position, relative velocity & acceleration
             !--------------------------------------------------------------------
             xyz=LPart(n)%r(1:dim)
             vr=LPart(n)%v(1:dim)
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! particle initial acceleration
             !--------------------------------------------------------------------
             call compute_particle_force(time,ap,coeff_m, &
                  & xyz,vr,                               &
                  & rotx,roty,rotz,dudt,dvdt,dwdt,alpha,  &     
                  & dv,LPart(n),dim)
             !--------------------------------------------------------------------
             ap=ap/coeff_m
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! leap-frog step 1
             !--------------------------------------------------------------------
             vrn=vr+d1p2*dv*ap
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! prediction of position at half-step iteration (second order) 
             !--------------------------------------------------------------------
             xyz_extra2=xyz+d1p2*dv*LPart(n)%u(1:dim)+d1p2*(d1p2*dv)**2*LPart(n)%a(1:dim)
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! eulerian velocity U_f interpotation at the half step iteration
             !--------------------------------------------------------------------
             call interpolation_uvw(time+dv/2,u_extra2,v_extra2,w_extra2,&
                  & xyz_extra2,vf,LPart(n)%interpol,dim)
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! half-step relative velocity and final particle postion
             !--------------------------------------------------------------------
             vpn=vrn+vf
             xyzn=xyz+dv*vpn
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! prediction of relative velocity at the end ofthe time step (first order ?)
             !--------------------------------------------------------------------
             vr_extra1=vr+dv*LPart(n)%a(1:dim)
             ! or
             ! vr_extra1=vrn+d1p2*dv*LPart(n)%a(1:dim) --> ordre 1 ?
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! RHS and coeff_m of coeff_m dV/dt=sum f
             !--------------------------------------------------------------------
             call compute_particle_force(time+dv,ap,coeff_m, &
                  & xyzn,vr_extra1,                          &
                  & rotx_extra1,roty_extra1,rotz_extra1,     &
                  & dudt_extra1,dvdt_extra1,dwdt_extra1,     &
                  & alpha_extra1,dv,LPart(n),dim)
             !--------------------------------------------------------------------
             ap=ap/coeff_m
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! leap-frog step 2
             !--------------------------------------------------------------------
             vrnn=vrn+d1p2*dv*ap
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! final total velocity
             !--------------------------------------------------------------------
             call interpolation_uvw(time+dv,u_extra1,v_extra1,w_extra1,&
                  & xyzn,vfn,LPart(n)%interpol,dim)
             !--------------------------------------------------------------------
             vpnn=vrnn+vfn
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! boundary conditions (velocity and postion are at the same time)
             !--------------------------------------------------------------------
             call bound_cond(xyzn,vpnn,LPart(n),code)
             if (code==1) then
                call interpolation_uvw(time+dv,u_extra1,v_extra1,w_extra1,&
                     & xyzn,vfn,LPart(n)%interpol,dim)
                vrnn=vpnn-vfn
             end if
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! maximum particle velocity for Lagrangian cfl condition
             !--------------------------------------------------------------------
             umax_lag(1:dim)=max(abs(vpnn(1:dim)),umax_lag(1:dim))
             !--------------------------------------------------------------------

             !--------------------------------------------------------------------
             ! UPDATE of the Lagrangian structure
             !--------------------------------------------------------------------
             LPart(n)%r(1:dim)=xyzn
             LPart(n)%v(1:dim)=vrnn
             LPart(n)%u(1:dim)=vpnn
             !--------------------------------------------------------------------

          else
             write(*,*) " Scheme LPart(n)%RK =",LPart(n)%RK,"is not implemented, STOP"
             stop
          end if

          
          !--------------------------------------------------------------------
          ! Active time
          !--------------------------------------------------------------------
          LPart(n)%time=LPart(n)%time+dv
          !--------------------------------------------------------------------

          
          !--------------------------------------------------------------------
          ! If tracers
          !--------------------------------------------------------------------
          if (PartAreMarkers) then
	     if (sum(abs(LPart(n)%v))>=1e-14_8) LPart(n)%v=0
          end if
          !--------------------------------------------------------------------
          
          
          !--------------------------------------------------------------------
          ! Hydrodynamical Forces (NOT ACCELERATION) applided over Particle n
          !--------------------------------------------------------------------
          coeff_m=LPart(n)%m
          call compute_particle_force_hydro(time+dt,LPart(n)%a(1:dim),coeff_m, &
               & LPart(n)%r(1:dim),LPart(n)%v(1:dim),                          &
               & rotx_extra1,roty_extra1,rotz_extra1,                          &
               & dudt_extra1,dvdt_extra1,dwdt_extra1,                          &
               & alpha_extra1,dv,LPart(n),dim)
          !--------------------------------------------------------------------
          
       end if ! endif LG_advection

    end do

    !----------------------------
    ! Force Particles on the Flow 
    !----------------------------

    if (LG_2Way) force_PartFlow=force_PartFlow/(dx(i)*dy(j)*dz(k)**(dim-2))

    !-------------End of lagrangian advection for each elements-------------------
    call compute_time(TIMER_END,"[sub] LPT") 
  end subroutine LPT
  !*****************************************************************************************
  subroutine extrapolation(var_extra,var,var0,dtm,dtp,order,var1,var2)
    implicit none 
    !***************************************************************************************
    !Global variables
    !***************************************************************************************
    integer, intent(in)                                          :: order 
    real(8)                                                      :: dtm,dtp
    real(8), allocatable, dimension(:,:,:), intent(in)           :: var,var0
    real(8), allocatable, dimension(:,:,:), intent(in), optional :: var1,var2
    real(8), allocatable, dimension(:,:,:), intent(inout)        :: var_extra
    !***************************************************************************************
    !Local variables
    !***************************************************************************************
    real(8)                                                      :: tmp
    !***************************************************************************************

    tmp=dtp/dtm

    select case (order)
    case(1) ! first order
       var_extra=var
    case(2) ! secondorder
       var_extra=((dtp+dtm)*var-dtp*var0)/dtm
    case(3) ! thirdorder
       var_extra=var*(1+d3p2*tmp+d1p2*tmp**2)&
            -var0*(2*tmp+tmp**2)&
            +var1*d1p2*(tmp+tmp**2)
    case(4) ! fourthorder
       var_extra=var*(1+d11p6*tmp+tmp**2+d1p6*tmp**3)&
            -var0*(3*tmp+d5p2*tmp**2+d1p2*tmp**3)&
            +var1*(d3p2*tmp+2*tmp**2+d1p2*tmp**3)&
            -var2*(d1p3*tmp+d1p2*tmp**2+d1p6*tmp**3)
    case DEFAULT
       write(*,*) "EXTRAPOLATION IS NOT POSSIBLE"
       write(*,*) "STOP"
       stop
    end select

  end subroutine extrapolation
  !*****************************************************************************************
  !************************************************************************************
  subroutine bound_cond(xyz,vp,LPart,code)
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables 
    !-------------------------------------------------------------------------------
    integer, intent(out)                   :: code
    real(8), dimension(dim), intent(inout) :: xyz,vp
    type(particle_t), intent(inout)          :: LPart
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                :: i,j
    real(8)                                :: dist
    real(8), dimension(3)                  :: VecXMin,VecXMax
    !-------------------------------------------------------------------------------
    
    !-------------------------------------------------------------------------------
    ! Init
    !-------------------------------------------------------------------------------
    VecXMin(1)=xmin;VecXMax(1)=xmax
    VecXMin(2)=ymin;VecXMax(2)=ymax
    VecXMin(3)=zmin;VecXMax(3)=zmax
    code=0
    !-------------------------------------------------------------------------------

    do i=1,dim
       do j=1,2
          select case(bcs_case(i,j))
             !-------------------------------------------------------------------------------
             ! outflow cases -> inlet/outlet and neumann
             !-------------------------------------------------------------------------------
          case(1,10,11,12,13,20,30,50,51,110)
             if ( xyz(i)<VecXMin(i) .or. xyz(i)>VecXMax(i) ) then
                LPart%active=.false.
             end if
             !-------------------------------------------------------------------------------
             ! periodic
             !-------------------------------------------------------------------------------
          case(4)
             if ( xyz(i)<VecXMin(i) ) then
                xyz(i)=xyz(i)+(VecXMax(i)-VecXMin(i))
                code=1
             else if ( xyz(i)>VecXMax(i) ) then
                xyz(i)=xyz(i)-(VecXMax(i)-VecXMin(i))
                code=1
             end if
             !-------------------------------------------------------------------------------
             ! Symmetry (case MEL)
             !-------------------------------------------------------------------------------
          case(2)
             !-------------------------------------------------------------------------------
             ! modify bounds
             !-------------------------------------------------------------------------------
             select case(i)
             case(1)
                VecXMin(1)=xmin-dxu(gsxu);VecXMax(1)=xmax+dxu(gexu)
             case(2)
                VecXMin(2)=ymin-dyv(gsyv);VecXMax(2)=ymax+dyv(geyv)
             case(3)
                VecXMin(3)=zmin-dzw(gszw);VecXMax(3)=zmax+dzw(gezw)
             end select
             !-------------------------------------------------------------------------------
             if ( xyz(i)-VecXMax(i)>0 ) then
                dist=xyz(i)-VecXMax(i)
                xyz(i)=VecXMax(i)-dist
                vp(i)=-vp(i)
                code=1
             end if
             if ( VecXMin(i)-xyz(i)>0 ) then
                dist=VecXMin(i)-xyz(i)
                xyz(i)=VecXMin(i)+dist
                vp(i)=-vp(i)
                code=1
             end if
             !-------------------------------------------------------------------------------
             ! bounce
             !-------------------------------------------------------------------------------
          case(0,3)
             if ( xyz(i)-VecXMax(i)>0 ) then
                dist=xyz(i)-VecXMax(i)
                xyz(i)=VecXMax(i)-dist
                vp(i)=-vp(i)
                code=1
             end if
             if ( VecXMin(i)-xyz(i)>0 ) then
                dist=VecXMin(i)-xyz(i)
                xyz(i)=VecXMin(i)+dist
                vp(i)=-vp(i)
                code=1
             end if
          case(-1)
             !-------------------------------------------------------------------------------
             ! analytical test case
             !-------------------------------------------------------------------------------
             code=1
          case default
             write(*,*) "bound_cond", bcs_case(i,j) ," not considered for LPT"
             stop
          end select
       end do
    end do

  end subroutine bound_cond

  !*******************************************************************************
  subroutine compute_cfl_lag(umax,dtcfl,dim)
    !*******************************************************************************!
    implicit none
    !-------------------------------------------------------------------------------
    !Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)                                            :: dim
    real(8), intent(out)                                            :: dtcfl
    real(8), dimension(dim), intent(inout)                          :: umax
    !-------------------------------------------------------------------------------
    !Local variables
    !-------------------------------------------------------------------------------
    real(8), parameter      :: epsc=1e-20_8
    real(8), dimension(3)   :: vecd
    integer                :: i
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    !Time step with cfl_lag
    !-------------------------------------------------------------------------------

    vecd= (/ dx(1), dy(1), dz(1) /) 
    dtcfl=dt
    do i=1,dim
       if (umax(i)>epsc) then
          dtcfl=min(dtcfl,vecd(i)*cfl_lag/umax(i))
       end if
    end do
    !dtcfl=min(dtcfl,MINVAL(vecd(1:dim))*cfl_lag/SQRT(SUM(umax**2)))
    
    umax=0
  end subroutine compute_cfl_lag
  !******************************************************************************
  subroutine compute_particle_force(time,force,coeff_m, &
       & X,V,rotx,roty,rotz,dudt,dvdt,dwdt,alpha,       &
       & dt,LPart,dim)
    implicit none
    !---------------------------------------------------------------
    ! Global variables 
    !---------------------------------------------------------------
    type(particle_t), intent(in)                         :: LPart
    integer, intent(in)                                :: dim
    real(8), dimension(dim), intent(in)                :: X,V
    real(8), allocatable, dimension(:,:,:), intent(in) :: rotx,roty,rotz
    real(8), allocatable, dimension(:,:,:), intent(in) :: dudt,dvdt,dwdt
    real(8), allocatable, dimension(:,:,:), intent(in) :: alpha
    real(8), dimension(dim), intent(out)               :: force
    real(8), intent(in)                                :: dt,time
    real(8), intent(out)                               :: coeff_m
    !---------------------------------------------------------------
    ! Local variables 
    !---------------------------------------------------------------
    real(8), dimension(dim)                            :: force_grav,force_hydro
    real(8), dimension(6)                              :: norm_force
    real(8), dimension(3)                              :: tmp1,tmp2,tmp3,pos
    real(8)                                            :: stmp1
    !---------------------------------------------------------------

    !----------------------------------------------------------------------------
    ! initialization
    !----------------------------------------------------------------------------
    coeff_m=Lpart%m
    force=0
    force_grav=0
    force_hydro=0
    !----------------------------------------------------------------------------

    !----------------------------------------------------------------------------
    ! exit if particles are markers 
    !----------------------------------------------------------------------------
    if (PartAreMarkers) return        
    !----------------------------------------------------------------------------
    
    !----------------------------------------------------------------------------
    ! gravity forces
    !----------------------------------------------------------------------------
    call compute_particle_force_grav(time,force_grav,coeff_m, &
         & X,V,rotx,roty,rotz,dudt,dvdt,dwdt,alpha,             &
         & dt,LPart,dim)
    !----------------------------------------------------------------------------

    !----------------------------------------------------------------------------
    ! hydrodynamical forces
    !----------------------------------------------------------------------------
    call compute_particle_force_hydro(time,force_hydro,coeff_m, &
         & X,V,rotx,roty,rotz,dudt,dvdt,dwdt,alpha,               &
         & dt,LPart,dim)
    !----------------------------------------------------------------------------

    !----------------------------------------------------------------------------
    ! sum
    !----------------------------------------------------------------------------
    force=force_grav+force_hydro
    !----------------------------------------------------------------------------

  end subroutine compute_particle_force

  !******************************************************************************

  subroutine compute_particle_force_grav(time,force,coeff_m, &
       & X,V,rotx,roty,rotz,dudt,dvdt,dwdt,alpha,            &
       & dt,LPart,dim)
    implicit none
    !---------------------------------------------------------------
    ! Global variables 
    !---------------------------------------------------------------
    type(particle_t), intent(in)                         :: LPart
    integer, intent(in)                                :: dim
    real(8), dimension(dim), intent(in)                :: X,V
    real(8), allocatable, dimension(:,:,:), intent(in) :: rotx,roty,rotz
    real(8), allocatable, dimension(:,:,:), intent(in) :: dudt,dvdt,dwdt
    real(8), allocatable, dimension(:,:,:), intent(in) :: alpha
    real(8), dimension(dim), intent(out)               :: force
    real(8), intent(in)                                :: dt,time
    real(8), intent(inout)                             :: coeff_m
    !---------------------------------------------------------------
    ! Local variables 
    !---------------------------------------------------------------
    real(8), dimension(dim)                            :: dforce
    real(8), dimension(6)                              :: norm_force
    real(8), dimension(3)                              :: tmp1,tmp2,tmp3,pos
    real(8)                                            :: stmp1
    !---------------------------------------------------------------

    !----------------------------------------------------------------------------
    ! initialization
    !----------------------------------------------------------------------------
    force=0;dforce=0
    !----------------------------------------------------------------------------

    !----------------------------------------------------------------------------
    ! gravity 
    !----------------------------------------------------------------------------
    if (LG_grav) then
       call force_gravity(time,LPart,coeff_m,dforce)
       norm_force(1)=sqrt(sum(dforce(1:dim)**2))
       force=force+dforce
       dforce=0
    end if
    !----------------------------------------------------------------------------

    !----------------------------------------------------------------------------
    ! archimede
    !----------------------------------------------------------------------------
    if (LG_arch) then
       call force_archi(time,LPart,coeff_m,dforce)
       norm_force(2)=sqrt(sum(dforce(1:dim)**2))
       force=force+dforce
       dforce=0
    end if
    !----------------------------------------------------------------------------

  end subroutine compute_particle_force_grav

  !******************************************************************************

  subroutine compute_particle_force_hydro(time,force,coeff_m, &
       & X,V,rotx,roty,rotz,dudt,dvdt,dwdt,alpha,       &
       & dt,LPart,dim)
    implicit none
    !---------------------------------------------------------------
    ! Global variables 
    !---------------------------------------------------------------
    type(particle_t), intent(in)                         :: LPart
    integer, intent(in)                                :: dim
    real(8), dimension(dim), intent(in)                :: X,V
    real(8), allocatable, dimension(:,:,:), intent(in) :: rotx,roty,rotz
    real(8), allocatable, dimension(:,:,:), intent(in) :: dudt,dvdt,dwdt
    real(8), allocatable, dimension(:,:,:), intent(in) :: alpha
    real(8), dimension(dim), intent(out)               :: force
    real(8), intent(in)                                :: dt,time
    real(8), intent(out)                               :: coeff_m
    !---------------------------------------------------------------
    ! local variables 
    !---------------------------------------------------------------
    real(8), dimension(dim)                            :: dforce
    real(8), dimension(6)                              :: norm_force
    real(8), dimension(3)                              :: tmp1,tmp2,tmp3,pos
    real(8)                                            :: stmp1
    !---------------------------------------------------------------

    !----------------------------------------------------------------------------
    ! initialization
    !----------------------------------------------------------------------------
    force=0;dforce=0
    !----------------------------------------------------------------------------

    !----------------------------------------------------------------------------
    ! lift
    !----------------------------------------------------------------------------
    if (LG_drag) then
       call force_drag(time,V,LPart,coeff_m,dforce)
       norm_force(3)=sqrt(sum(dforce(1:dim)**2))
       force=force+dforce
       dforce=0
    end if
    !----------------------------------------------------------------------------

    !----------------------------------------------------------------------------
    ! drag
    !----------------------------------------------------------------------------
    if (LG_lift) then
       call force_lift(time,X,V,LPart,coeff_m,dforce,rotx,roty,rotz,alpha)
       norm_force(4)=sqrt(sum(dforce(1:dim)**2))
       force=force+dforce
       dforce=0
    end if
    !----------------------------------------------------------------------------

    !----------------------------------------------------------------------------
    ! basset
    !----------------------------------------------------------------------------
    if (LG_bass) then
       call force_basset(time,LPart,coeff_m,dforce)
       norm_force(5)=sqrt(sum(dforce(1:dim)**2))
       force=force+dforce
       dforce=0
    end if
    !----------------------------------------------------------------------------

    !----------------------------------------------------------------------------
    ! added mass
    !----------------------------------------------------------------------------
    if (LG_addm) then
       call force_addmass(time,X,V,LPart,coeff_m,dforce,dudt,dvdt,dwdt,dt)
       norm_force(6)=sqrt(sum(dforce(1:dim)**2))
       force=force+dforce
       dforce=0
    end if
    !----------------------------------------------------------------------------

  end subroutine compute_particle_force_hydro

  ! ===============================================================================
  ! ===============================================================================
  ! ===============================================================================
  ! ===============================================================================
  ! ===============================================================================

  subroutine force_addmass(time,X,V,LPart,coeff_m,force,dudt,dvdt,dwdt,dt)
    !---------------------------------------------------------------
    ! Global variables 
    !---------------------------------------------------------------
    type(particle_t), intent(in)                         :: LPart
    real(8), dimension(dim), intent(in)                :: X,V
    real(8), intent(inout)                             :: coeff_m
    real(8), intent(in)                                :: time
    real(8), intent(in)                                :: dt
    real(8), dimension(dim), intent(inout)             :: force
    real(8), allocatable, dimension(:,:,:), intent(in) :: dudt,dvdt,dwdt
    !---------------------------------------------------------------
    ! local variables 
    !---------------------------------------------------------------
    real(8), dimension(dim)                            :: dudtp
    real(8)                                            :: C_M,dV,m_f,vol 
    !---------------------------------------------------------------

    !---------------------------------------------------------------
    ! initialization
    !---------------------------------------------------------------
    C_M=demi
    vol=d4p3pi*LPart%radius(1)**3
    m_f=fluids(1)%rho*vol
    coeff_m=coeff_m+m_f*C_M
    !---------------------------------------------------------------

    !---------------------------------------------------------------
    ! Du/Dt at X
    !---------------------------------------------------------------
    call interpolation_uvw(time,dudt,dvdt,dwdt,X,dudtp(1:dim),1,dim) ! attention interpolation : div(Du/Dt)=0 ???
    !---------------------------------------------------------------

    !---------------------------------------------------------------
    ! Volume variation
    !---------------------------------------------------------------
    dV=d4p3pi*(LPart%radius(1)**3-LPart%radius(2)**3)
    !---------------------------------------------------------------

    !---------------------------------------------------------------
    ! force
    !---------------------------------------------------------------
    force(1:dim)=force(1:dim)+m_f*(1+C_M)*dudtp(1:dim) & ! constant volume (radius)
         & -C_M*fluids(1)%rho*dV/dt*V(1:dim)             ! effect induced by volume variation
    !---------------------------------------------------------------


  end subroutine force_addmass
  ! ===============================================================================
  ! ===============================================================================
  ! ===============================================================================
  ! ===============================================================================
  ! ===============================================================================

  subroutine force_basset(time,LPart,coeff_m,force)
    !---------------------------------------------------------------
    ! Global variables 
    !---------------------------------------------------------------
    type(particle_t), intent(in)             :: LPart
    real(8), intent(inout)                 :: coeff_m
    real(8), intent(in)                    :: time
    real(8), dimension(dim), intent(inout) :: force
    !---------------------------------------------------------------

    write(*,*) "BASSET NON CODE STOP"
    stop
  end subroutine force_basset
  ! ===============================================================================
  ! ===============================================================================
  ! ===============================================================================
  ! ===============================================================================
  ! ===============================================================================

  subroutine force_lift(time,X,V,LPart,coeff_m,force,rotx,roty,rotz,alpha)
    !---------------------------------------------------------------
    ! Global variables 
    !---------------------------------------------------------------
    type(particle_t), intent(in)                         :: LPart
    real(8), dimension(dim), intent(in)                :: X,V 
    real(8), intent(inout)                             :: coeff_m
    real(8), intent(in)                                :: time
    real(8), dimension(dim), intent(inout)             :: force
    real(8), allocatable, dimension(:,:,:), intent(in) :: rotx,roty,rotz
    real(8), allocatable, dimension(:,:,:), intent(in) :: alpha
    !---------------------------------------------------------------
    ! Local variables 
    !---------------------------------------------------------------
    real(8)                                            :: m_f,vol 
    real(8)                                            :: C_L,Re,Sr,eps1,Jeps,Jinf
    real(8)                                            :: CL1,CL50,phi_mu,normV,alphap
    real(8), dimension(3)                              :: ROT,VIT,tmp
    !---------------------------------------------------------------

    Jinf=2.225_8
    vol=d4p3pi*LPart%radius(1)**3
    m_f=fluids(1)%rho*vol

    !---------------------------------------------------------------
    ! Initialization
    !---------------------------------------------------------------
    ROT=0
    VIT=0
    VIT(1:dim)=V(1:dim)
    phi_mu=LPart%mu/fluids(1)%mu
    !---------------------------------------------------------------

    !---------------------------------------------------------------
    ! rotational at X
    !---------------------------------------------------------------
    call interpolation_rot(time,rotx,roty,rotz,X,rot,1,dim)
    !---------------------------------------------------------------

    !---------------------------------------------------------------
    ! maximum shear stress at X
    !---------------------------------------------------------------
    call interpolation_phi(time,alpha,X,alphap,1,dim)
    !---------------------------------------------------------------

    !---------------------------------------------------------------
    ! Renolds & Co.
    !---------------------------------------------------------------
    normV=sqrt(sum(V(1:dim)**2))
    Re=(fluids(1)%rho/fluids(1)%mu)*2*Lpart%radius(1)*normV
    Sr=0
    if (Re/=0.and.alphap/=0) then
       Sr=2*Lpart%radius(1)*abs(alphap)/normV
    end if
    !---------------------------------------------------------------

    !---------------------------------------------------------------
    ! Lift coefficient (cf. Legendre)
    !---------------------------------------------------------------
    C_L=0
    if (Re /= 0) then
       eps1=sqrt(Sr/Re)
       Jeps=Jinf/(1+0.2_8*eps1**(-2))**(3._8/2)
       if (Sr/=0) then 
          !---------------------------------
          ! C_L For Re < 1 & C_L For Re > 50
          !---------------------------------
          CL1=6*dpi**2*(2+3*phi_mu)/(2+2*phi_mu)*Jeps/sqrt(Re*Sr)
          CL50=d1p2*(1+16/Re)/(1+29/Re)
          !----------------------------------
          ! General expression (cf. Legendre)
          !----------------------------------
          C_L=sqrt(CL1**2+CL50**2)
          C_L=demi
       end if
    end if
    C_L=demi
    tmp=-C_L*m_f*                      &
         & (/ VIT(2)*ROT(3)-VIT(3)*ROT(2), &
         & VIT(3)*ROT(1)-VIT(1)*ROT(3),    &
         & VIT(1)*ROT(2)-VIT(2)*ROT(1) /)
    !---------------------------------------------------------------

    !---------------------------------------------------------------
    ! force
    !---------------------------------------------------------------
    force(1:dim)=force(1:dim)+tmp(1:dim)
    !---------------------------------------------------------------

  end subroutine force_lift
  ! ===============================================================================
  ! ===============================================================================
  ! ===============================================================================
  ! ===============================================================================
  ! ===============================================================================

  subroutine force_drag(time,V,LPart,coeff_m,force)
    type(particle_t), intent(in)             :: LPart
    real(8), dimension(dim), intent(in)    :: V   
    real(8), intent(inout)                 :: coeff_m
    real(8), intent(in)                    :: time
    real(8), dimension(dim), intent(inout) :: force
    real(8) :: normV,Re,C_D,phi_mu,phi_rho,diam

    ! --------------------------------------------
    ! initialization
    !---------------------------------------------

    normV=sqrt(sum(V(1:dim)**2))
    diam=2*LPart%radius(1)
    Re=(fluids(1)%rho/fluids(1)%mu)*diam*normV

    !---------------------------------------
    ! Schiller-Naumann formula
    !---------------------------------------
    C_D=0
    if (Re>0) then 
       C_D=24/Re*(1+0.15_8*Re**(0.687_8))
       C_D=d3p4/diam*fluids(1)%rho*d4p3pi*LPart%radius(1)**3*C_D
    end if
    !---------------------------------
    force(1:dim)=force(1:dim)-C_D*normV*V(1:dim)
  end subroutine force_drag
  ! ===============================================================================
  ! ===============================================================================
  ! ===============================================================================
  ! ===============================================================================
  ! ===============================================================================

  subroutine force_gravity(time,LPart,coeff_m,force)
    type(particle_t), intent(in)             :: LPart
    real(8), intent(inout)                 :: coeff_m
    real(8), intent(in)                    :: time
    real(8), dimension(dim), intent(inout) :: force
    real(8)                                :: m_p

    m_p=LPart%m
    force(1:dim)=force(1:dim)+m_p*gravity(1:dim)
  end subroutine force_gravity

  subroutine force_archi(time,LPart,coeff_m,force)
    type(particle_t), intent(in)             :: LPart
    real(8), intent(inout)                 :: coeff_m
    real(8), intent(in)                    :: time
    real(8), dimension(dim), intent(inout) :: force
    real(8)                                :: m_f,vol 

    vol=d4p3pi*LPart%radius(1)**3
    m_f=fluids(1)%rho*vol
    force(1:dim)=force(1:dim)-m_f*gravity(1:dim)
  end subroutine force_archi
  ! ===============================================================================
  ! ===============================================================================
  ! ===============================================================================
  ! ===============================================================================
  ! ===============================================================================
  subroutine LG_print(nt,LPart,nPart)
    integer, intent(in)                       :: nt,nPart
    type(particle_t), dimension(:), intent(in)  :: LPart

    integer                                   :: n,cpt
    real(8)                                   :: St,Re,St_max,St_min,Re_max,Re_min
    real(8)                                   :: normV
    real(8), dimension(3)                     :: v
    character(len=50)                         :: name,name0,name1

    !-----------------------------------------
    ! init
    !-----------------------------------------
    St_max=-infty;St_min=+infty
    Re_max=-infty;Re_min=+infty
    cpt=0
    !-----------------------------------------


    do n=1,nPart

       if (LPart(n)%active) then 
          cpt=cpt+1

          ! compute stokes number
          v=LPart(n)%u-LPart(n)%v           ! fluid velocity 
          normV=sqrt(sum(v(1:dim)**2))
          St=d1p18*LPart(n)%rho*(2*LPart(n)%radius(1))*normV/fluids(1)%mu

          ! compute reynolds number
          v=LPart(n)%v 
          normV=sqrt(sum(v(1:dim)**2))
          Re=(fluids(1)%rho/fluids(1)%mu)*2*Lpart(n)%radius(1)*normV

          if (sum(LPart(n)%v**2)<eps_zero) then 
             Re=0
             St=0
          end if

          if (Re>=Re_max) then
             Re_max=Re
          end if
          if (Re<=Re_min) then
             Re_min=Re
          end if

          if (St>=St_max) then
             St_max=St
          end if
          if(St<=St_min) then
             St_min=St
          end if

       end if
    end do
    
    write(*,*) ''//achar(27)//'[35m====================Particle Tracking=========================='//achar(27)//'[0m'
    !write(*,*) 'Cummulative Time step', nt

    write(name,'(i12)') cpt
    write(name0,'(i12)') nPart
    write(name1,'(f5.1)') 1d2*cpt/nPart

    write(*,'(" Active Particle(s): ",a,"/",a," (",a,"%)")') &
         & trim(adjustl(name)),                                 &
         & trim(adjustl(name0)),                                &
         & trim(adjustl(name1))
    if (.not.PartAreMarkers) then 
       write(*,'(" Re_p: Max =",1pe9.2,", Min =",1pe9.2)') Re_max, Re_min
       write(*,'(" St_p: Max =",1pe9.2,", Min =",1pe9.2)') St_max, St_min
    end if

  end subroutine LG_print


  subroutine ComputeScaAverages(mesh,LPart,nPartL,avrg)
    !*******************************************************************************
    !   Modules                                                                     !
    !*******************************************************************************!
    use mod_Parameters, only: EN_inertial_scheme,nppdpc
    !*******************************************************************************!
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)                                        :: nPartL
    real(8), allocatable, dimension(:,:,:), intent(inout)      :: avrg
    type(grid_t), intent(in)                                   :: mesh
    type(particle_t), allocatable, dimension(:), intent(inout) :: LPart
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                                    :: i,j,k,n
    integer                                                    :: case_avrg
    real(8)                                                    :: tmin,tmax,dump,amp
    integer, allocatable, dimension(:,:,:)                     :: iwght
    real(8), allocatable, dimension(:,:,:)                     :: wght
    real(8), allocatable, dimension(:,:,:,:)                   :: davrg,dwght
    !-------------------------------------------------------------------------------
    call compute_time(TIMER_START,"[sub] ComputeScaAverages") 

    !-------------------------------------------------------------------------------
    ! ponderation case
    !   0 : vol
    !   1 : linear time
    !   2 : tanh time
    !-------------------------------------------------------------------------------
    case_avrg = mod(abs(EN_inertial_scheme),10)
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! initialization
    !-------------------------------------------------------------------------------
    ! avrg=0 ! done before the call
    !-------------------------------------------------------------------------------
    allocate(iwght(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
    allocate(wght(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
    allocate(dwght(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz,4*nppdpc**dim))
    allocate(davrg(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz,4*nppdpc**dim))
    iwght = 0
    wght  = 0
    dwght = 0
    davrg = 0
    !-------------------------------------------------------------------------------

    do n=1,nPartL
       if (LPart(n)%active) then
          !-------------------------------------------------------------------------------
          ! index of the cell
          !-------------------------------------------------------------------------------
          call search_cell_ijk(mesh,LPart(n)%r,LPart(n)%ijk,PRESS_MESH)
          i = LPart(n)%ijk(1)
          j = LPart(n)%ijk(2)
          k = LPart(n)%ijk(3)
          !-------------------------------------------------------------------------------

          !-------------------------------------------------------------------------------
          ! Particule cell weight
          !-------------------------------------------------------------------------------
          iwght(i,j,k)              = iwght(i,j,k)+1
          davrg(i,j,k,iwght(i,j,k)) = LPart(n)%phi
          !-------------------------------------------------------------------------------
          select case(case_avrg)
          case(0)
             !------------------------------------
             ! volume 
             !------------------------------------
             dwght(i,j,k,iwght(i,j,k))=product(LPart(n)%dl(1:dim))
             !------------------------------------
          case(1,2)
             !------------------------------------
             ! time
             !------------------------------------
             dwght(i,j,k,iwght(i,j,k))=LPart(n)%time
             !------------------------------------
          end select
          !-------------------------------------------------------------------------------
          
!!$          !-------------------------------------------------------------------------------
!!$          ! sum of pondered values
!!$          !-------------------------------------------------------------------------------
!!$          avrg(i,j,k)=avrg(i,j,k)+davrg(i,j,k,iwght(i,j,k))*dwght(i,j,k,iwght(i,j,k))
!!$          !-------------------------------------------------------------------------------
!!$
!!$          !-------------------------------------------------------------------------------
!!$          ! sum of weight
!!$          !-------------------------------------------------------------------------------
!!$          wght(i,j,k)=wght(i,j,k)+dwght(i,j,k,iwght(i,j,k))
!!$          !-------------------------------------------------------------------------------
       end if
    end do

    !-------------------------------------------------------------------------------
    ! averaged value 
    !-------------------------------------------------------------------------------
    do k=sz-gz,ez+gz
       do j=sy-gy,ey+gy
          do i=sx-gx,ex+gx
             !-------------------------------------------------------------------------------
             ! weight normalisation
             !-------------------------------------------------------------------------------
             select case(case_avrg)
             case(2)
                n=iwght(i,j,k)
                if (n>0) then
                   tmax=maxval(dwght(i,j,k,1:n))
                   tmin=minval(dwght(i,j,k,1:n))
                   amp=tmax-tmin+1d-28
                   dump=2d-1*amp
                   dwght(i,j,k,1:n)=amp*tanh((dwght(i,j,k,1:n)-tmin)/dump)+1
                else
                   dwght(i,j,k,:)=0
                end if
!!$                write(*,*) n,tmax,tmin,amp,dump
!!$                write(*,*) dwght(i,j,k,1:n)
             end select

             !-------------------------------------------------------------------------------
             ! sum of pondered values
             !-------------------------------------------------------------------------------
             avrg(i,j,k)=sum(davrg(i,j,k,:)*dwght(i,j,k,:))
             !-------------------------------------------------------------------------------
             ! weight
             !-------------------------------------------------------------------------------
             wght(i,j,k)=sum(dwght(i,j,k,:))
             !-------------------------------------------------------------------------------
             ! average
             !-------------------------------------------------------------------------------
             if (wght(i,j,k)>0) then
                avrg(i,j,k)=avrg(i,j,k)/wght(i,j,k)
             end if
             !-------------------------------------------------------------------------------
          end do
       end do
    end do
    !-------------------------------------------------------------------------------

    call compute_time(TIMER_END,"[sub] ComputeScaAverages") 
  end subroutine ComputeScaAverages
  
  
  subroutine ComputeVelAverages(mesh,LPart,nPart,avu,avv,avw)
    !*******************************************************************************
    !   Modules                                                                     !
    !*******************************************************************************!
    !*******************************************************************************!
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)                                        :: nPart
    real(8), allocatable, dimension(:,:,:), intent(inout)      :: avu,avv,avw
    type(grid_t), intent(in)                                   :: mesh
    type(particle_t), allocatable, dimension(:), intent(inout) :: LPart
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                                    :: d,i,j,k,n
    real(8)                                                    :: dvol
    real(8), dimension(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz)    :: volu,volv,volw
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! initialization
    !-------------------------------------------------------------------------------
    avu=0;volu=0 
    avv=0;volv=0
    if (dim==3) then
       avw=0;volw=0
    end if
    !-------------------------------------------------------------------------------

    do n=1,nPart
       if (LPart(n)%active) then
          do d=1,mesh%dim
             !-------------------------------------------------------------------------------
             ! index of the cell
             !-------------------------------------------------------------------------------
             call search_cell_ijk(mesh,LPart(n)%r,LPart(n)%ijk,d) 
             i=LPart(n)%ijk(1)
             j=LPart(n)%ijk(2)
             k=LPart(n)%ijk(3)
             !-------------------------------------------------------------------------------
             
             !-------------------------------------------------------------------------------
             ! Particule cell volume
             !-------------------------------------------------------------------------------
             dvol=product(LPart(n)%dl(1:dim))
             !-------------------------------------------------------------------------------
             
             !-------------------------------------------------------------------------------
             ! sum of volume pondered values and volumes
             !-------------------------------------------------------------------------------
             select case(d)
             case(1)
                avu(i,j,k)=avu(i,j,k)+LPart(n)%u0(d)*dvol
                volu(i,j,k)=volu(i,j,k)+dvol
             case(2)
                avv(i,j,k)=avv(i,j,k)+LPart(n)%u0(d)*dvol
                volv(i,j,k)=volv(i,j,k)+dvol
             case(3)
                avw(i,j,k)=avw(i,j,k)+LPart(n)%u0(d)*dvol
                volw(i,j,k)=volw(i,j,k)+dvol
             end select
             !-------------------------------------------------------------------------------
          end do
       end if
    end do
    
    !-------------------------------------------------------------------------------
    ! averaged value 
    !-------------------------------------------------------------------------------
    do k=sz-gz,ez+gz
       do j=sy-gy,ey+gy
          do i=sx-gx,ex+gx
             if (volu(i,j,k)>0) avu(i,j,k)=avu(i,j,k)/volu(i,j,k)
             if (volv(i,j,k)>0) avv(i,j,k)=avv(i,j,k)/volv(i,j,k)
             if (dim==3.and.volw(i,j,k)>0) avw(i,j,k)=avw(i,j,k)/volw(i,j,k)
          end do
       end do
    end do
    !-------------------------------------------------------------------------------
    
  end subroutine ComputeVelAverages


  subroutine InterpScalar(LPart,nPart,phi)
    !*******************************************************************************
    !   Description
    !*******************************************************************************!
    ! affect the interpolated valeur of the phi field at the particle position.
    ! the result is stoked in LPart(n)%phi
    !*******************************************************************************
    !   Modules                                                                     !
    !*******************************************************************************!
    !*******************************************************************************!
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)                                      :: nPart
    real(8), allocatable, dimension(:,:,:), intent(in)       :: phi
    type(particle_t), allocatable, dimension(:), intent(inout) :: LPart
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                                  :: n
    !-------------------------------------------------------------------------------

    do n=1,nPart
       call interpolation_phi(zero,phi,LPart(n)%r,LPart(n)%phi,1,dim)
    end do

  end subroutine InterpScalar

  subroutine ReallocationParticle(LPart,nPart,u,v,w,phi)
    !-------------------------------------------------------------------------------
    ! Description:
    !-------------------------------------------------------------------------------
    ! Compute ...
    !-------------------------------------------------------------------------------
    ! Modules
    !-------------------------------------------------------------------------------
    use mod_mpi
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)                                          :: nPart
    real(8), allocatable, dimension(:,:,:), intent(in)           :: u,v,w
    real(8), allocatable, dimension(:,:,:), intent(in), optional :: phi
    type(particle_t), allocatable, dimension(:), intent(inout)   :: LPart
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                                      :: nInactive,nActive,nbM,nbM_min
    integer                                                      :: c,i,j,k,m,n,n1,n2,l,lmin
    integer                                                      :: iStncl
    integer                                                      :: ii,jj,kk
    integer, dimension(2)                                        :: locmin
    integer, dimension(1:nPart)                                  :: Tab,TabInactive,TabActive
!!$    integer, dimension(sx-1:ex+1,sy-1:ey+1,sz-1:ez+1)         :: tabPart
!!$    integer, dimension((nx+1)*(ny+1)*(nz+1)**(dim-2))         :: tabCell
    real(8)                                                      :: rxb,rxf,ryb,ryf,rzb,rzf
    real(8)                                                      :: min,var1,var2,var3
    real(8), allocatable, dimension(:,:)                         :: dist
    !-------------------------------------------------------------------------------
    real(8)                                                      :: norm
    real(8)                                                      :: lmax_norm_grad_phi,lmin_norm_grad_phi
    real(8)                                                      :: max_norm_grad_phi,min_norm_grad_phi
    real(8), allocatable, dimension(:,:,:)                       :: gradphi,gradphi0
    !-------------------------------------------------------------------------------
    
    call compute_time(TIMER_START,"[sub] ReallocationParticle") 

    !-------------------------------------------------------------------------------
    ! init global arrays
    !-------------------------------------------------------------------------------
    if (.not.allocated(tabCell)) then
       allocate(TabCell((ex-sx+3)*(ey-sy+3)*(ez-sz+3)**(dim-2)))
    end if
    !-------------------------------------------------------------------------------

    !--------------------------------------------------
    ! Out of bounds
    !--------------------------------------------------
    do n=1,nPart
       if (LPart(n)%r(1)<grid_xu(sx-1)) LPart(n)%active = .false.
       if (LPart(n)%r(1)>grid_xu(ex+2)) LPart(n)%active = .false.
       if (LPart(n)%r(2)<grid_yv(sy-1)) LPart(n)%active = .false.
       if (LPart(n)%r(2)>grid_yv(ey+2)) LPart(n)%active = .false.
       if (dim==3) then
          if (LPart(n)%r(3)<grid_zw(sz-1)) LPart(n)%active = .false.
          if (LPart(n)%r(3)>grid_zw(ez+2)) LPart(n)%active = .false.
       end if
    end do
    !--------------------------------------------------

    !--------------------------------------------------
    ! Cell List computation
    !--------------------------------------------------
    cells = (/(ex-sx+3),(ey-sy+3),(ez-sz+3)**(dim-2)/)
    call compute_CellList(mesh,CellList,cells,ncNgbr,vcNgbr,LPart,nPart)
    !--------------------------------------------------
    
    !--------------------------------------------------
    ! compute grad(phi) norm
    !--------------------------------------------------
    allocate(gradphi(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
    allocate(gradphi0(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
    var1               = 0
    var2               = 0
    var3               = 0
    gradphi            = 0
    gradphi0           = 0
    lmax_norm_grad_phi = -10000000
    lmin_norm_grad_phi = -lmax_norm_grad_phi
    do k = sz,ez
       do j = sy,ey
          do i = sx,ex
             var1 = (dxu(i  )**2*(phi(i+1,j,k)-phi(i  ,j,k))   &
                  & +dxu(i+1)**2*(phi(i  ,j,k)-phi(i-1,j,k)))  &
                  & /(dxu(i)**2*dxu(i+1)+dxu(i)*dxu(i+1)**2)
             var2 = (dyv(j  )**2*(phi(i,j+1,k)-phi(i,j  ,k))   &
                  & +dyv(j+1)**2*(phi(i,j  ,k)-phi(i,j-1,k)))  &
                  & /(dyv(j)**2*dyv(j+1)+dyv(j)*dyv(j+1)**2)
             if (dim == 3) then
                var3 = (dzw(k  )**2*(phi(i,j,k+1)-phi(i,j,k  ))   &
                     & +dzw(k+1)**2*(phi(i,j,k  )-phi(i,j,k-1)))  &
                     & /(dzw(k)**2*dzw(k+1)+dzw(k)*dzw(k+1)**2)
             end if
             norm            = sqrt(var1**2+var2**2+var3**2)
             gradphi0(i,j,k) = norm
             if (norm>lmax_norm_grad_phi) lmax_norm_grad_phi = norm
             if (norm<lmin_norm_grad_phi) lmin_norm_grad_phi = norm
          end do
       end do
    end do
    !--------------------------------------------------
    call mpi_allreduce(lmax_norm_grad_phi,max_norm_grad_phi,1, &
         & mpi_double_precision,mpi_max,comm3d,code)
    call mpi_allreduce(lmin_norm_grad_phi,min_norm_grad_phi,1, &
         & mpi_double_precision,mpi_min,comm3d,code)
    call comm_mpi_sca(gradphi0)
    !--------------------------------------------------
    
    !--------------------------------------------------
    ! stencil for reseeding processes
    !--------------------------------------------------
    iStncl = 1
    ii     = iStncl
    jj     = iStncl
    if (dim==2) then
       kk = 0
    else
       kk = iStncl
    end if
    !--------------------------------------------------
    do k = sz,ez
       do j = sy,ey
          do i = sx,ex
             gradphi(i,j,k) = maxval(gradphi0(i-ii:i+ii,j-jj:j+jj,k-kk:k+kk))
          end do
       end do
    end do
    !--------------------------------------------------
    
    !--------------------------------------------------
    ! Inactive particles
    !--------------------------------------------------
    m=0
    do n = 1,nPart
       if (.not.LPart(n)%active) then
          m              = m+1
          TabInactive(m) = n
       end if
    end do
    !--------------------------------------------------
    
    !--------------------------------------------------
    ! number of Marker per cell
    !--------------------------------------------------
    nbM     = nppdpc**dim
    nbM_min = min(nppdpc_min,nppdpc)**dim
    !nbM_min = nppdpc_min
    !--------------------------------------------------
    
    !--------------------------------------------------
    ! impoverish
    !--------------------------------------------------
    TabCell = 0
    do n = nPart+1,nPart+product(cells)
       !--------------------------------------------------
       ! j cell
       !--------------------------------------------------
       j=CellList(n)
       !--------------------------------------------------
       
       !--------------------------------------------------
       ! cell indexes (ii,jj,kk)
       !--------------------------------------------------
       if (dim==2) then
          !--------------------------------------------------
          ! local index
          !--------------------------------------------------
          ii = modulo(n-nPart-1,ex-sx+3)
          jj = (n-nPart-ii)/(ex-sx+3)
          !--------------------------------------------------
          ! global index
          !--------------------------------------------------
          ii = ii+(sx-1)
          jj = jj+(sy-1)
          kk = 1
          !--------------------------------------------------
       else
          !--------------------------------------------------
          ! local index
          !--------------------------------------------------
          ii = modulo(n-nPart-1,ex-sx+3)
          kk = ((n-nPart-1)-modulo(n-nPart-1,(ex-sx+3)*(ey-sy+3)))/(ex-sx+3)/(ey-sy+3)
          jj = (n-nPart-ii-kk*(ex-sx+3)*(ey-sy+3))/(ex-sx+3)
          !--------------------------------------------------
          ! global index
          !--------------------------------------------------
          ii = ii+(sx-1)
          jj = jj+(sy-1)
          kk = kk+(sz-1)
          !--------------------------------------------------
       end if
       !--------------------------------------------------
       
       !--------------------------------------------------
       ! compute grad(phi) norm
       !--------------------------------------------------
       !norm=sqrt(gradphix(ii,jj,kk)**2+gradphiy(ii,jj,kk)**2+gradphiz(ii,jj,kk)**2)
       norm = gradphi(ii,jj,kk)
       norm = (norm-min_norm_grad_phi)/(max_norm_grad_phi-min_norm_grad_phi+1d-40)
       !--------------------------------------------------
       ! tanh repartition
       !--------------------------------------------------
       c = anint(d1p2*(nbM-nbM_min)*tanh((norm-d1p2)/2d-1)+d1p2*(nbM+nbM_min))
       !--------------------------------------------------
       ! binary repartition
       !--------------------------------------------------
!!$       if (norm > 0) then
!!$          c = nbM
!!$       else
!!$          c = 0
!!$       end if
       !--------------------------------------------------

       !write(*,*) c,nbM,nbM_min
       
       if (j>0) then
          i = 0
          !--------------------------------------------------
          ! number of particle and local array Tab
          !--------------------------------------------------
          do while (j>0)
             i      = i+1
             Tab(i) = j
             j      = CellList(j)
          end do
          !--------------------------------------------------

          !--------------------------------------------------
          ! cells where there is a lack of particles
          !--------------------------------------------------
          if (i<c) then
             TabCell(n-nPart) = c-i
          end if
          !--------------------------------------------------

          !--------------------------------------------------
          ! impoverish if number of particles > nbM
          !--------------------------------------------------
          do while (i>c) 
             !--------------------------------------------------
             ! get distances 
             !--------------------------------------------------
             allocate(dist(i,i))
             dist=infty
             do n1=1,i-1
                do n2=n1+1,i
                   dist(n1,n2) = sum((LPart(Tab(n1))%r(1:dim)-LPart(Tab(n2))%r(1:dim))**2)
                   dist(n2,n1) = dist(n1,n2)
                end do
             end do
             !--------------------------------------------------

             !--------------------------------------------------
             ! nearest particles
             !--------------------------------------------------
             locmin = minloc(dist)
             !--------------------------------------------------

             !--------------------------------------------------
             ! unactive first particle of the identified couple
             ! and keep the particle index
             !--------------------------------------------------
             LPart(Tab(locmin(1)))%active = .false.
             m                            = m+1
             TabInactive(m)               = Tab(locmin(1))
             !--------------------------------------------------

             !--------------------------------------------------
             ! Sort local array Tab
             !--------------------------------------------------
             Tab(locmin(1)) = Tab(i)
             !--------------------------------------------------

             !--------------------------------------------------
             ! while loop 
             !--------------------------------------------------
             deallocate(dist)
             i = i-1
             !--------------------------------------------------
          end do
          !--------------------------------------------------

       else
          !--------------------------------------------------
          ! no particle in cell 
          !--------------------------------------------------
          TabCell(n-nPart) = c
          !--------------------------------------------------
       end if
       !--------------------------------------------------
       !write(*,*) n,ii,jj,i,TabCell(n-nPart)
    end do
    !--------------------------------------------------
    
    !--------------------------------------------------
    ! enrichment
    !--------------------------------------------------
    m=0
    do k=sz-(dim-2),ez+(dim-2)
       do j=sy-1,ey+1
          do i=sx-1,ex+1
             
             !--------------------------------------------------
             ! vectorized cell index (with local indexes)
             !--------------------------------------------------
             if (dim==2) then
                ii = i-(sx-1)
                jj = j-(sy-1)
                kk = k
                l  = vlinear((/ii,jj,kk/)+(/1,1,0/),cells)
             else
                ii = i-(sx-1)
                jj = j-(sy-1)
                kk = k-(sz-1)
                l  = vlinear((/ii,jj,kk/)+(/1,1,1/),cells)
             end if
             !--------------------------------------------------
             
             if (TabCell(l)>0) then
                do n1=1,TabCell(l)
                   !--------------------------------------------------
                   ! active particle
                   !--------------------------------------------------
                   m               = m+1
                   n               = TabInactive(m)
                   LPart(n)%active = .true.
                   LPart(n)%time   = 0
                   !--------------------------------------------------

                   !--------------------------------------------------
                   ! new position (random draw)
                   !--------------------------------------------------
                   ! bounds (b)ack, (f)ront
                   !--------------------------------------------------
                   rxb = grid_xu(i)
                   rxf = grid_xu(i+1)
                   ryb = grid_yv(j)
                   ryf = grid_yv(j+1)
                   if (dim == 3) then
                      rzb = grid_zw(k)
                      rzf = grid_zw(k+1)
                   end if
                   !--------------------------------------------------
                   call random_number(LPart(n)%r)
                   LPart(n)%r(1) = LPart(n)%r(1)*(rxf-rxb)+rxb
                   LPart(n)%r(2) = LPart(n)%r(2)*(ryf-ryb)+ryb
                   if (dim==2) then
                      LPart(n)%r(3) = zmin !d1p2*(zmax+zmin)
                   else
                      LPart(n)%r(3) = LPart(n)%r(3)*(rzf-rzb)+rzb
                   end if
                   !--------------------------------------------------

                   !--------------------------------------------------
                   ! Particle Velocity = Velocity cell value
                   !--------------------------------------------------
                   if (LPart(n)%r(1)>grid_x(i)) then
                      LPart(n)%u0(1) = u(i+1,j,k)
                   else
                      LPart(n)%u0(1) = u(i,j,k)
                   end if
                   if (LPart(n)%r(2)>grid_y(j)) then
                      LPart(n)%u0(2) = v(i,j+1,k)
                   else
                      LPart(n)%u0(2) = v(i,j,k)
                   end if

                   if (dim==3) then
                      if (LPart(n)%r(3)>grid_z(k)) then
                         LPart(n)%u0(3) = w(i,j,k+1)
                      else
                         LPart(n)%u0(3) = w(i,j,k)
                      end if
                   end if
                   !--------------------------------------------------
                   
                   !--------------------------------------------------
                   ! New particle Particle
                   !--------------------------------------------------
                   if (present(phi)) LPart(n)%phi = phi(i,j,k)
                   LPart(n)%new = .true.
                   !--------------------------------------------------
                   
                end do
             end if

          end do
       end do
    end do

    call compute_time(TIMER_END,"[sub] ReallocationParticle") 
  end subroutine ReallocationParticle


  subroutine ReallocationParticle2(mesh,LPart,nPart,phi)
    !-------------------------------------------------------------------------------
    ! Description:
    !-------------------------------------------------------------------------------
    ! Compute ...
    !-------------------------------------------------------------------------------
    ! Modules
    !-------------------------------------------------------------------------------
    use mod_struct_grid
    use mod_mpi
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)                                          :: nPart
    real(8), allocatable, dimension(:,:,:), intent(in)           :: phi
    type(particle_t), allocatable, dimension(:), intent(inout)   :: LPart
    type(grid_t), intent(in)                                     :: mesh
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                                      :: nInactive,nActive,nbM,nbM_min
    integer                                                      :: c,i,j,k,m,n,n1,n2,l,lmin
    integer                                                      :: iStncl
    integer                                                      :: ii,jj,kk
    integer, dimension(2)                                        :: locmin
    integer, dimension(1:nPart)                                  :: Tab,TabInactive,TabActive
!!$    integer, dimension(sx-1:ex+1,sy-1:ey+1,sz-1:ez+1)         :: tabPart
!!$    integer, dimension((nx+1)*(ny+1)*(nz+1)**(dim-2))         :: tabCell
    real(8)                                                      :: rxb,rxf,ryb,ryf,rzb,rzf
    real(8)                                                      :: min,var1,var2,var3
    real(8), allocatable, dimension(:,:)                         :: dist
    !-------------------------------------------------------------------------------
    real(8)                                                      :: norm
    real(8)                                                      :: lmax_norm_grad_phi,lmin_norm_grad_phi
    real(8)                                                      :: max_norm_grad_phi,min_norm_grad_phi
    real(8), allocatable, dimension(:,:,:)                       :: gradphi,gradphi0
    !-------------------------------------------------------------------------------
    
    call compute_time(TIMER_START,"[sub] ReallocationParticle2") 

    !-------------------------------------------------------------------------------
    ! init global arrays
    !-------------------------------------------------------------------------------
    if (.not.allocated(tabCell)) then
       allocate(TabCell((ex-sx+3)*(ey-sy+3)*(ez-sz+3)**(dim-2)))
    end if
    !-------------------------------------------------------------------------------

    !--------------------------------------------------
    ! Out of bounds
    !--------------------------------------------------
    do n=1,nPart
       if (LPart(n)%r(1)<grid_xu(sx-1)) LPart(n)%active = .false.
       if (LPart(n)%r(1)>grid_xu(ex+2)) LPart(n)%active = .false.
       if (LPart(n)%r(2)<grid_yv(sy-1)) LPart(n)%active = .false.
       if (LPart(n)%r(2)>grid_yv(ey+2)) LPart(n)%active = .false.
       if (dim==3) then
          if (LPart(n)%r(3)<grid_zw(sz-1)) LPart(n)%active = .false.
          if (LPart(n)%r(3)>grid_zw(ez+2)) LPart(n)%active = .false.
       end if
    end do
    !--------------------------------------------------

    !--------------------------------------------------
    ! Cell List computation
    !--------------------------------------------------
    cells = (/(ex-sx+3),(ey-sy+3),(ez-sz+3)**(dim-2)/)
    call compute_CellList(mesh,CellList,cells,ncNgbr,vcNgbr,LPart,nPart)
    !--------------------------------------------------
    
    !--------------------------------------------------
    ! compute grad(phi) norm
    !--------------------------------------------------
    allocate(gradphi(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
    allocate(gradphi0(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
    gradphi  = 0
    gradphi0 = 0
    !--------------------------------------------------
    do k = sz,ez
       do j = sy,ey
          do i = sx,ex
             if (phi(i+1,j,k)/=phi(i,j,k)) gradphi0(i,j,k) = 1
             if (phi(i-1,j,k)/=phi(i,j,k)) gradphi0(i,j,k) = 1
             if (phi(i,j+1,k)/=phi(i,j,k)) gradphi0(i,j,k) = 1
             if (phi(i,j-1,k)/=phi(i,j,k)) gradphi0(i,j,k) = 1
             if (mesh%dim==3) then
                if (phi(i,j,k+1)/=phi(i,j,k)) gradphi0(i,j,k) = 1
                if (phi(i,j,k-1)/=phi(i,j,k)) gradphi0(i,j,k) = 1
             end if
          end do
       end do
    end do
    !--------------------------------------------------
    
    !--------------------------------------------------
    ! stencil for reseeding processes
    !--------------------------------------------------
    iStncl = 2
    ii = iStncl
    jj = iStncl
    kk = iStncl*(mesh%dim-2)
    !--------------------------------------------------
    do k = sz,ez
       do j = sy,ey
          do i = sx,ex
             gradphi(i,j,k) = maxval(gradphi0(i-ii:i+ii,j-jj:j+jj,k-kk:k+kk))
          end do
       end do
    end do
    !--------------------------------------------------
    
    !--------------------------------------------------
    ! Inactive particles
    !--------------------------------------------------
    m=0
    do n = 1,nPart
       if (.not.LPart(n)%active) then
          m              = m+1
          TabInactive(m) = n
       end if
    end do
    !--------------------------------------------------
    
    !--------------------------------------------------
    ! number of Marker per cell
    !--------------------------------------------------
    nbM     = nppdpc**dim
    nbM_min = min(nppdpc_min,nppdpc)**dim
    !--------------------------------------------------
    
    !--------------------------------------------------
    ! impoverish
    !--------------------------------------------------
    TabCell = 0
    do n = nPart+1,nPart+product(cells)
       !--------------------------------------------------
       ! j cell
       !--------------------------------------------------
       j=CellList(n)
       !--------------------------------------------------
       
       !--------------------------------------------------
       ! cell indexes (ii,jj,kk)
       !--------------------------------------------------
       if (dim==2) then
          !--------------------------------------------------
          ! local index
          !--------------------------------------------------
          ii = modulo(n-nPart-1,ex-sx+3)
          jj = (n-nPart-ii)/(ex-sx+3)
          !--------------------------------------------------
          ! global index
          !--------------------------------------------------
          ii = ii+(sx-1)
          jj = jj+(sy-1)
          kk = 1
          !--------------------------------------------------
       else
          !--------------------------------------------------
          ! local index
          !--------------------------------------------------
          ii = modulo(n-nPart-1,ex-sx+3)
          kk = ((n-nPart-1)-modulo(n-nPart-1,(ex-sx+3)*(ey-sy+3)))/(ex-sx+3)/(ey-sy+3)
          jj = (n-nPart-ii-kk*(ex-sx+3)*(ey-sy+3))/(ex-sx+3)
          !--------------------------------------------------
          ! global index
          !--------------------------------------------------
          ii = ii+(sx-1)
          jj = jj+(sy-1)
          kk = kk+(sz-1)
          !--------------------------------------------------
       end if
       !--------------------------------------------------
       
       !--------------------------------------------------
       ! grad(phi) /= 0
       !--------------------------------------------------
       if (gradphi(ii,jj,kk) > 0) then
          c = nbM
       else
          c = 0
       end if
       !--------------------------------------------------
       
       !write(*,*) c,nbM,nbM_min
       
       if (j>0) then
          i = 0
          !--------------------------------------------------
          ! number of particle and local array Tab
          !--------------------------------------------------
          do while (j>0)
             i      = i+1
             Tab(i) = j
             j      = CellList(j)
          end do
          !--------------------------------------------------

          !--------------------------------------------------
          ! cells where there is a lack of particles
          !--------------------------------------------------
          if (i<c) then
             TabCell(n-nPart) = c-i
          end if
          !--------------------------------------------------

          !--------------------------------------------------
          ! impoverish if number of particles > nbM
          !--------------------------------------------------
          do while (i>c) 
             !--------------------------------------------------
             ! get distances 
             !--------------------------------------------------
             allocate(dist(i,i))
             dist=infty
             do n1=1,i-1
                do n2=n1+1,i
                   dist(n1,n2) = sum((LPart(Tab(n1))%r(1:dim)-LPart(Tab(n2))%r(1:dim))**2)
                   dist(n2,n1) = dist(n1,n2)
                end do
             end do
             !--------------------------------------------------

             !--------------------------------------------------
             ! nearest particles
             !--------------------------------------------------
             locmin = minloc(dist)
             !--------------------------------------------------

             !--------------------------------------------------
             ! unactive first particle of the identified couple
             ! and keep the particle index
             !--------------------------------------------------
             LPart(Tab(locmin(1)))%active = .false.
             m                            = m+1
             TabInactive(m)               = Tab(locmin(1))
             !--------------------------------------------------

             !--------------------------------------------------
             ! Sort local array Tab
             !--------------------------------------------------
             Tab(locmin(1)) = Tab(i)
             !--------------------------------------------------

             !--------------------------------------------------
             ! while loop 
             !--------------------------------------------------
             deallocate(dist)
             i = i-1
             !--------------------------------------------------
          end do
          !--------------------------------------------------

       else
          !--------------------------------------------------
          ! no particle in cell 
          !--------------------------------------------------
          TabCell(n-nPart) = c
          !--------------------------------------------------
       end if
       !--------------------------------------------------
       !write(*,*) n,ii,jj,i,TabCell(n-nPart)
    end do
    !--------------------------------------------------
    
    !--------------------------------------------------
    ! enrichment
    !--------------------------------------------------
    m=0
    do k=sz-(dim-2),ez+(dim-2)
       do j=sy-1,ey+1
          do i=sx-1,ex+1
             
             !--------------------------------------------------
             ! vectorized cell index (with local indexes)
             !--------------------------------------------------
             if (dim==2) then
                ii = i-(sx-1)
                jj = j-(sy-1)
                kk = k
                l  = vlinear((/ii,jj,kk/)+(/1,1,0/),cells)
             else
                ii = i-(sx-1)
                jj = j-(sy-1)
                kk = k-(sz-1)
                l  = vlinear((/ii,jj,kk/)+(/1,1,1/),cells)
             end if
             !--------------------------------------------------
             
             if (TabCell(l)>0) then
                do n1=1,TabCell(l)
                   !--------------------------------------------------
                   ! active particle
                   !--------------------------------------------------
                   m               = m+1
                   n               = TabInactive(m)
                   LPart(n)%active = .true.
                   LPart(n)%time   = 0
                   !--------------------------------------------------

                   !--------------------------------------------------
                   ! new position (random draw)
                   !--------------------------------------------------
                   ! bounds (b)ack, (f)ront
                   !--------------------------------------------------
                   rxb = grid_xu(i)
                   rxf = grid_xu(i+1)
                   ryb = grid_yv(j)
                   ryf = grid_yv(j+1)
                   if (dim == 3) then
                      rzb = grid_zw(k)
                      rzf = grid_zw(k+1)
                   end if
                   !--------------------------------------------------
                   call random_number(LPart(n)%r)
                   LPart(n)%r(1) = LPart(n)%r(1)*(rxf-rxb)+rxb
                   LPart(n)%r(2) = LPart(n)%r(2)*(ryf-ryb)+ryb
                   if (dim==2) then
                      LPart(n)%r(3) = zmin !d1p2*(zmax+zmin)
                   else
                      LPart(n)%r(3) = LPart(n)%r(3)*(rzf-rzb)+rzb
                   end if
                   !--------------------------------------------------
                   
                   !--------------------------------------------------
                   ! New particle Particle
                   !--------------------------------------------------
                   LPart(n)%new = .true.
                   !--------------------------------------------------

                end do
             end if

          end do
       end do
    end do

    call compute_time(TIMER_END,"[sub] ReallocationParticle2") 
  end subroutine ReallocationParticle2


  subroutine compute_volume_fraction(mesh,vfrctn,LPart,nPartL,icode)
    use mod_coeffvs
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)                                        :: icode ! 0 pressure mesh, 1->u, 2->v, 3->w
    integer, intent(in)                                        :: nPartL
    real(8), allocatable, dimension(:,:,:), intent(inout)      :: vfrctn
    type(grid_t), intent(in)                                   :: mesh
    type(particle_t), allocatable, dimension(:), intent(inout) :: LPart
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                                    :: i,j,k,n
    real(8)                                                    :: overlap_xm,overlap_xp
    real(8)                                                    :: overlap_ym,overlap_yp
    real(8)                                                    :: overlap_zm,overlap_zp
    real(8)                                                    :: overlap_xmym,overlap_xmyp
    real(8)                                                    :: overlap_xpym,overlap_xpyp
    real(8)                                                    :: overlap_xmzm,overlap_xpzm
    real(8)                                                    :: overlap_ymzm,overlap_ypzm
    real(8)                                                    :: overlap_xmzp,overlap_xpzp
    real(8)                                                    :: overlap_ymzp,overlap_ypzp
    real(8)                                                    :: overlap_xmymzm,overlap_xmypzm
    real(8)                                                    :: overlap_xpymzm,overlap_xpypzm
    real(8)                                                    :: overlap_xmymzp,overlap_xmypzp
    real(8)                                                    :: overlap_xpymzp,overlap_xpypzp
    real(8)                                                    :: sum_edges,sum_corners,sum_faces
    real(8)                                                    :: corr,d,dd,d2,vol_cube,vol_cell
    !-------------------------------------------------------------------------------
    
    !-------------------------------------------------------------------------------
    ! allocation
    !-------------------------------------------------------------------------------
    if (.not.allocated(vfrctn)) then
       select case(icode)
       case (0,1,2,3)
          allocate(vfrctn(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
       end select
    end if
    !-------------------------------------------------------------------------------
    
    !-------------------------------------------------------------------------------
    ! correction due to cublic particle approximation
    ! corr = pi/4 (2D) or pi/6 (3D)
    !-------------------------------------------------------------------------------
    corr = pi/(2**dim-2**(dim-2)+(3-dim))
    !-------------------------------------------------------------------------------
    
    !-------------------------------------------------------------------------------
    ! volume fraction computation = vol_fluid / vol_cell
    !                             = 1 --> if only particles
    !                             = 0 --> if only fluid 
    !-------------------------------------------------------------------------------
    vfrctn = 0
    !-------------------------------------------------------------------------------
    
    do n = 1,nPartL
       if (LPart(n)%active) then
          
          !-------------------------------------------------------------------------------
          ! particle diameter and diameter*diameter and cubic volume
          !-------------------------------------------------------------------------------
          d        = 2*LPart(n)%radius(1)
          dd       = d*d
          d2       = d/2
          vol_cube = d**dim
          !-------------------------------------------------------------------------------
          
          !-------------------------------------------------------------------------------
          ! search index and cell volume
          !-------------------------------------------------------------------------------
          call search_cell_ijk(mesh,LPart(n)%r,LPart(n)%ijk,icode) 
          i        = LPart(n)%ijk(1)
          j        = LPart(n)%ijk(2)
          k        = LPart(n)%ijk(3)
          vol_cell = dx(i)*dy(j)*dz(k)**(dim-2)
          !-------------------------------------------------------------------------------
          
          !-------------------------------------------------------------------------------
          ! compute overlaping with neighbouring cells
          !-------------------------------------------------------------------------------
          ! X-direction
          !-------------------------------------------------------------------------------
          overlap_xm=abs(min((LPart(n)%r(1)-d2)-grid_xu(i),zero))
          overlap_xp=abs(min(zero,grid_xu(i+1)-(LPart(n)%r(1)+d2)))
          !-------------------------------------------------------------------------------
          ! Y-direction
          !-------------------------------------------------------------------------------
          overlap_ym=abs(min((LPart(n)%r(2)-d2)-grid_yv(j),zero))
          overlap_yp=abs(min(zero,grid_yv(j+1)-(LPart(n)%r(2)+d2)))
          !-------------------------------------------------------------------------------
          ! Z-direction
          !-------------------------------------------------------------------------------
          if (dim==2) then
             overlap_zm=0
             overlap_zp=0
          elseif (dim==3) then
             overlap_zm=abs(min((LPart(n)%r(3)-d2)-grid_zw(k),zero))
             overlap_zp=abs(min(zero,grid_zw(k+1)-(LPart(n)%r(3)+d2)))
          end if
          !-------------------------------------------------------------------------------
          sum_faces = overlap_xm+overlap_xp &
               &    + overlap_ym+overlap_yp &
               &    + overlap_zm+overlap_zp
          !-------------------------------------------------------------------------------

          if (PartAreMarkers) then
             !-------------------------------------------------------------------------------
             ! Particles are tracers 
             !-------------------------------------------------------------------------------
             if (dim==2) then
                vfrctn(i,j,1)=vfrctn(i,j,1)+1*CoeffV(i,j,1)
             else
                vfrctn(i,j,k)=vfrctn(i,j,k)+1*CoeffV(i,j,k)
             end if
          else
             !-------------------------------------------------------------------------------
             ! Real particles
             !-------------------------------------------------------------------------------
             if (dim==2) then

                !-------------------------------------------------------------------------------
                ! overlap corners (>0)
                !-------------------------------------------------------------------------------
                overlap_xmym = overlap_xm*overlap_ym
                overlap_xmyp = overlap_xm*overlap_yp
                overlap_xpym = overlap_xp*overlap_ym
                overlap_xpyp = overlap_xp*overlap_yp
                !-------------------------------------------------------------------------------
                sum_corners = overlap_xmym+overlap_xmyp &
                     &      + overlap_xpym+overlap_xpyp
                !-------------------------------------------------------------------------------
                
                !-------------------------------------------------------------------------------
                ! cell 
                !-------------------------------------------------------------------------------
                vfrctn(i,j,k+1)=vfrctn(i,j,k+1) & ! initial particle volume fraction
                     & +corr/vol_cell*(vol_cube & ! add cube particle volume
                     & -d*sum_faces             & ! remove overlap bands
                     & +sum_corners)              ! add corners (twice removed in previous line)

                !-------------------------------------------------------------------------------
                ! corners (x4)
                !-------------------------------------------------------------------------------
                vfrctn(i-1,j-1,1) = vfrctn(i-1,j-1,1) + overlap_xmym*corr/vol_cell
                vfrctn(i-1,j+1,1) = vfrctn(i-1,j+1,1) + overlap_xmyp*corr/vol_cell
                vfrctn(i+1,j-1,1) = vfrctn(i+1,j-1,1) + overlap_xpym*corr/vol_cell
                vfrctn(i+1,j+1,1) = vfrctn(i+1,j+1,1) + overlap_xpyp*corr/vol_cell
                !-------------------------------------------------------------------------------

                !-------------------------------------------------------------------------------
                ! edges/faces (x4)
                !-------------------------------------------------------------------------------
                vfrctn(i-1,j,1)=vfrctn(i-1,j,1) &
                     & +(overlap_xm*d-(overlap_xmym+overlap_xmyp))*corr/vol_cell
                vfrctn(i+1,j,1)=vfrctn(i+1,j,1) &
                     & +(overlap_xp*d-(overlap_xpym+overlap_xpyp))*corr/vol_cell
                vfrctn(i,j-1,1)=vfrctn(i,j-1,1) &
                     & +(overlap_ym*d-(overlap_xmym+overlap_xpym))*corr/vol_cell
                vfrctn(i,j+1,1)=vfrctn(i,j+1,1) &
                     & +(overlap_yp*d-(overlap_xmyp+overlap_xpyp))*corr/vol_cell
                !-------------------------------------------------------------------------------

             elseif (dim==3) then

                !-------------------------------------------------------------------------------
                ! overlap corners (>0)
                !-------------------------------------------------------------------------------
                overlap_xmymzm = abs(overlap_xm*overlap_ym*overlap_zm)
                overlap_xmypzm = abs(overlap_xm*overlap_yp*overlap_zm)
                overlap_xpymzm = abs(overlap_xp*overlap_ym*overlap_zm)
                overlap_xpypzm = abs(overlap_xp*overlap_yp*overlap_zm)
                !-------------------------------------------------------------------------------
                overlap_xmymzp = abs(overlap_xm*overlap_ym*overlap_zp)
                overlap_xmypzp = abs(overlap_xm*overlap_yp*overlap_zp)
                overlap_xpymzp = abs(overlap_xp*overlap_ym*overlap_zp)
                overlap_xpypzp = abs(overlap_xp*overlap_yp*overlap_zp)
                !-------------------------------------------------------------------------------
                sum_corners = overlap_xmymzm+overlap_xmypzm &
                     &      + overlap_xpymzm+overlap_xpypzm &
                     &      + overlap_xmymzp+overlap_xmypzp &
                     &      + overlap_xpymzp+overlap_xpypzp
                !-------------------------------------------------------------------------------

                !-------------------------------------------------------------------------------
                ! overlap edges
                !-------------------------------------------------------------------------------
                overlap_xmym = overlap_xm*overlap_ym
                overlap_xmyp = overlap_xm*overlap_yp
                overlap_xpym = overlap_xp*overlap_ym
                overlap_xpyp = overlap_xp*overlap_yp
                !-------------------------------------------------------------------------------
                overlap_xmzm = overlap_xm*overlap_zm
                overlap_ymzm = overlap_ym*overlap_zm
                overlap_xpzm = overlap_xp*overlap_zm
                overlap_ypzm = overlap_yp*overlap_zm
                !-------------------------------------------------------------------------------
                overlap_xmzp = overlap_xm*overlap_zp
                overlap_ymzp = overlap_ym*overlap_zp
                overlap_xpzp = overlap_xp*overlap_zp
                overlap_ypzp = overlap_yp*overlap_zp
                !-------------------------------------------------------------------------------
                sum_edges = overlap_xmym+overlap_xmyp+overlap_xpym+overlap_xpyp &
                     &    + overlap_xmzm+overlap_ymzm+overlap_xpzm+overlap_ypzm &
                     &    + overlap_xmzp+overlap_ymzp+overlap_xpzp+overlap_ypzp
                !-------------------------------------------------------------------------------

                !-------------------------------------------------------------------------------
                ! cell 
                !-------------------------------------------------------------------------------
                vfrctn(i,j,k)=vfrctn(i,j,k)     &
                     & +corr/vol_cell*(vol_cube & ! add cube volume
                     & -dd*sum_faces            & ! remove faces
                     & +sum_edges               & ! add edges (twice removed above)
                     & +2*sum_corners)            ! add corners (thice removed above)

                !-------------------------------------------------------------------------------
                ! faces (x6)
                !-------------------------------------------------------------------------------
                vfrctn(i-1,j,k)=vfrctn(i-1,j,k)                                         &
                     & +corr/vol_cell*(overlap_xm*dd                                    & ! face
                     & -(d*(overlap_xmzm+overlap_xmzp+overlap_xmym+overlap_xmyp)        & ! remove edges
                     & +(overlap_xmymzm+overlap_xmymzp+overlap_xmypzm+overlap_xmypzp)))   ! add corners (twice removed above)
                vfrctn(i+1,j,k)=vfrctn(i+1,j,k)                                         &
                     & +corr/vol_cell*(overlap_xp*dd                                    &
                     & -(d*(overlap_xpzm+overlap_xpzp+overlap_xpym+overlap_xpyp)        &
                     & +(overlap_xpymzm+overlap_xpymzp+overlap_xpypzm+overlap_xpypzp)))
                vfrctn(i,j-1,k)=vfrctn(i,j-1,k)                                         &
                     & +corr/vol_cell*(overlap_ym*dd                                    &
                     & -(d*(overlap_ymzm+overlap_ymzp+overlap_xmym+overlap_xpym)        &
                     & +(overlap_xmymzm+overlap_xmymzp+overlap_xpymzm+overlap_xpymzp)))
                vfrctn(i,j+1,k)=vfrctn(i,j+1,k)                                         &
                     & +corr/vol_cell*(overlap_yp*dd                                    &
                     & -(d*(overlap_ypzm+overlap_ypzp+overlap_xmyp+overlap_xpyp)        &
                     & +(overlap_xmypzm+overlap_xmypzp+overlap_xpypzm+overlap_xpypzp)))
                vfrctn(i,j,k-1)=vfrctn(i,j,k-1)                                         &
                     & +corr/vol_cell*(overlap_zm*dd                                    &
                     & -(d*(overlap_xmzm+overlap_xpzm+overlap_ymzm+overlap_ypzm)        &
                     & +(overlap_xmymzm+overlap_xmypzm+overlap_xpymzm+overlap_xpypzm)))
                vfrctn(i,j,k+1)=vfrctn(i,j,k+1)                                         &
                     & +corr/vol_cell*(overlap_zp*dd                                    &
                     & -(d*(overlap_xmzp+overlap_xpzp+overlap_ymzp+overlap_ypzp)        &
                     & +(overlap_xmymzp+overlap_xmypzp+overlap_xpymzp+overlap_xpypzp)))
                !-------------------------------------------------------------------------------

                !-------------------------------------------------------------------------------
                ! corners (x8)            
                !-------------------------------------------------------------------------------
                vfrctn(i-1,j-1,k-1)=vfrctn(i-1,j-1,k-1)+overlap_xmymzm*corr/vol_cell
                vfrctn(i-1,j+1,k-1)=vfrctn(i-1,j+1,k-1)+overlap_xmypzm*corr/vol_cell
                vfrctn(i+1,j-1,k-1)=vfrctn(i+1,j-1,k-1)+overlap_xpymzm*corr/vol_cell
                vfrctn(i+1,j+1,k-1)=vfrctn(i+1,j+1,k-1)+overlap_xpypzm*corr/vol_cell
                !-------------------------------------------------------------------------------
                vfrctn(i-1,j-1,k+1)=vfrctn(i-1,j-1,k+1)+overlap_xmymzp*corr/vol_cell
                vfrctn(i-1,j+1,k+1)=vfrctn(i-1,j+1,k+1)+overlap_xmypzp*corr/vol_cell
                vfrctn(i+1,j-1,k+1)=vfrctn(i+1,j-1,k+1)+overlap_xpymzp*corr/vol_cell
                vfrctn(i+1,j+1,k+1)=vfrctn(i+1,j+1,k+1)+overlap_xpypzp*corr/vol_cell
                !-------------------------------------------------------------------------------

                !-------------------------------------------------------------------------------
                ! edges (x12)
                !-------------------------------------------------------------------------------
                vfrctn(i-1,j,k-1)=vfrctn(i-1,j,k-1)      &
                     & +corr/vol_cell*(overlap_xmzm*d    & ! add edge
                     & -(overlap_xmymzm+overlap_xmypzm))   ! remove corners
                vfrctn(i+1,j,k-1)=vfrctn(i+1,j,k-1)      &
                     & +corr/vol_cell*(overlap_xpzm*d    &
                     & -(overlap_xpymzm+overlap_xpypzm)) 
                vfrctn(i,j-1,k-1)=vfrctn(i,j-1,k-1)      &
                     & +corr/vol_cell*(overlap_ymzm*d    &
                     & -(overlap_xmymzm+overlap_xpymzm))
                vfrctn(i,j+1,k-1)=vfrctn(i,j+1,k-1)      &
                     & +corr/vol_cell*(overlap_ypzm*d    &
                     & -(overlap_xmypzm+overlap_xpypzm))
                !-------------------------------------------------------------------------------
                vfrctn(i-1,j,k+1)=vfrctn(i-1,j,k+1)      &
                     & +corr/vol_cell*(overlap_xmzp*d    & 
                     & -(overlap_xmymzp+overlap_xmypzp))
                vfrctn(i+1,j,k+1)=vfrctn(i+1,j,k+1)      &
                     & +corr/vol_cell*(overlap_xpzp*d    &
                     & -(overlap_xpymzp+overlap_xpypzp))
                vfrctn(i,j-1,k+1)=vfrctn(i,j-1,k+1)      &
                     & +corr/vol_cell*(overlap_ymzp*d    &
                     & -(overlap_xmymzp+overlap_xpymzp))
                vfrctn(i,j+1,k+1)=vfrctn(i,j+1,k+1)      &
                     & +corr/vol_cell*(overlap_ypzp*d    &
                     & -(overlap_xmypzp+overlap_xpypzp))
                !-------------------------------------------------------------------------------
                vfrctn(i-1,j+1,k)=vfrctn(i-1,j+1,k)      &
                     & +corr/vol_cell*(overlap_xmyp*d    &
                     & -(overlap_xmypzm+overlap_xmypzp))
                vfrctn(i+1,j+1,k)=vfrctn(i+1,j+1,k)      &
                     & +corr/vol_cell*(overlap_xpyp*d    &
                     & -(overlap_xpypzm+overlap_xpypzp))
                vfrctn(i-1,j-1,k)=vfrctn(i-1,j-1,k)      &
                     & +corr/vol_cell*(overlap_xmym*d    &
                     & -(overlap_xmymzm+overlap_xmymzp))
                vfrctn(i+1,j+1,k)=vfrctn(i+1,j+1,k)      &
                     & +corr/vol_cell*(overlap_xpyp*d    &
                     & -(overlap_xpypzm+overlap_xpypzp))
                !-------------------------------------------------------------------------------
             end if
             
          end if
          
       end if
    end do

  end subroutine compute_volume_fraction

#if 0
  subroutine two_way_coupling_force(vfrctn,LPart,nPart,code)
    !*******************************************************************************
    !   Modules                                                                     !
    !*******************************************************************************!
    !*******************************************************************************!
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)                                      :: code ! 0 pressure mesh, 1->u, 2->v, 3->w
    integer, intent(in)                                      :: nPart
    real(8), allocatable, dimension(:,:,:), intent(inout)    :: vfrctn
    type(particle_t), allocatable, dimension(:), intent(inout) :: LPart
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                                  :: i,j,k,n
    real(8)                                                  :: overlap_xm,overlap_xp
    real(8)                                                  :: overlap_ym,overlap_yp
    real(8)                                                  :: overlap_zm,overlap_zp 
    real(8)                                                  :: overlap_xmym,overlap_xmyp
    real(8)                                                  :: overlap_xpym,overlap_xpyp
    real(8)                                                  :: overlap_xmzm,overlap_xpzm
    real(8)                                                  :: overlap_ymzm,overlap_ypzm
    real(8)                                                  :: overlap_xmzp,overlap_xpzp
    real(8)                                                  :: overlap_ymzp,overlap_ypzp
    real(8)                                                  :: overlap_xmymzm,overlap_xmypzm
    real(8)                                                  :: overlap_xpymzm,overlap_xpypzm
    real(8)                                                  :: overlap_xmymzp,overlap_xmypzp
    real(8)                                                  :: overlap_xpymzp,overlap_xpypzp
    real(8)                                                  :: sum_edges,sum_corners,sum_faces
    real(8)                                                  :: corr,d,dd,d2,vol_cube,vol_cell
    real(8), dimension(3)                                    :: vec_dir
    !-------------------------------------------------------------------------------


    !-------------------------------------------------------------------------------
    ! correction due to cublic particle approximation
    ! corr = pi/4 (2D) or pi/6 (3D)
    !-------------------------------------------------------------------------------
    corr=pi/(2**dim-2**(dim-2)+(3-dim))
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! volume fraction computation = vol_fluid / vol_cell
    !                             = 1 --> if only particles
    !                             = 0 --> if only fluid 
    !-------------------------------------------------------------------------------
    vfrctn=0
    !-------------------------------------------------------------------------------

    do n=1,nPart

       !-------------------------------------------------------------------------------
       ! particle diameter and diameter*diameter and cubic volume
       !-------------------------------------------------------------------------------
       d=2*LPart(n)%radius(1)
       dd=d*d
       d2=d/2
       vol_cube=d**dim
       !-------------------------------------------------------------------------------

       do dir=1,dim

          !-------------------------------------------------------------------------------
          ! search index and cell volume
          !-------------------------------------------------------------------------------
          call search_cell_ijk(LPart(n)%r,LPart(n)%ijk,dir)
          i=LPart(n)%ijk(1)
          j=LPart(n)%ijk(2)
          k=LPart(n)%ijk(3)
          vol_cell=dx(i)*dy(j)*dz(k)**(dim-2)
          !-------------------------------------------------------------------------------

          !-------------------------------------------------------------------------------
          ! compute overlaping with neighbouring cells
          !-------------------------------------------------------------------------------
          select case(dir)
          case(1)
             ! X-direction
             overlap_xm=abs(min((LPart(n)%r(1)-d2)-grid_x(i-1),zero))
             overlap_xp=abs(min(zero,grid_x(i)-(LPart(n)%r(1)+d2)))
             ! Y-direction
             overlap_ym=abs(min((LPart(n)%r(2)-d2)-grid_yv(j),zero))
             overlap_yp=abs(min(zero,grid_yv(j+1)-(LPart(n)%r(2)+d2)))
             ! Z-direction
             if (dim==2) then
                overlap_zm=0
                overlap_zp=0
             elseif (dim==3) then
                overlap_zm=abs(min((LPart(n)%r(3)-d2)-grid_zw(k),zero))
                overlap_zp=abs(min(zero,grid_zw(k+1)-(LPart(n)%r(3)+d2)))
             end if
          case(2)
             ! X-direction
             overlap_xm=abs(min((LPart(n)%r(1)-d2)-grid_xu(i),zero))
             overlap_xp=abs(min(zero,grid_xu(i+1)-(LPart(n)%r(1)+d2)))
             ! Y-direction
             overlap_ym=abs(min((LPart(n)%r(2)-d2)-grid_y(j-1),zero))
             overlap_yp=abs(min(zero,grid_y(j)-(LPart(n)%r(2)+d2)))
             ! Z-direction
             if (dim==2) then
                overlap_zm=0
                overlap_zp=0
             elseif (dim==3) then
                overlap_zm=abs(min((LPart(n)%r(3)-d2)-grid_zw(k),zero))
                overlap_zp=abs(min(zero,grid_zw(k+1)-(LPart(n)%r(3)+d2)))
             end if
          case(2)
             ! X-direction
             overlap_xm=abs(min((LPart(n)%r(1)-d2)-grid_xu(i),zero))
             overlap_xp=abs(min(zero,grid_xu(i+1)-(LPart(n)%r(1)+d2)))
             ! Y-direction
             overlap_ym=abs(min((LPart(n)%r(2)-d2)-grid_yv(j),zero))
             overlap_yp=abs(min(zero,grid_yv(j+1)-(LPart(n)%r(2)+d2)))
             ! Z-direction
             if (dim==2) then
                overlap_zm=0
                overlap_zp=0
             elseif (dim==3) then
                overlap_zm=abs(min((LPart(n)%r(3)-d2)-grid_z(k-1),zero))
                overlap_zp=abs(min(zero,grid_z(k)-(LPart(n)%r(3)+d2)))
             end if
          end select
          !-------------------------------------------------------------------------------

          !-------------------------------------------------------------------------------
          ! force on the fluid flow
          !-------------------------------------------------------------------------------
          if (dim==2) then

             !-------------------------------------------------------------------------------
             ! overlap corners
             !-------------------------------------------------------------------------------
             overlap_xmym=overlap_xm*overlap_ym
             overlap_xmyp=overlap_xm*overlap_yp
             overlap_xpym=overlap_xp*overlap_ym
             overlap_xpyp=overlap_xp*overlap_yp
             !-------------------------------------------------------------------------------

             dtheta=0

             !--------
             ! CORNERS 
             !--------

             ! BOTTOM / LEFT
             theta=abs(overlap_xmym)/vol_cube
             dtheta=dtheta+theta
             dforce_PartFlow(1:dim)=theta*Lpart(n)%a(1:dim)
             force_PartFlow(dir,i-1,j-1,k+1)=force_PartFlow(dir,i-1,j-1,k+1) &
                  & +dot_product(dforce_PartFlow,vec_dir)

             ! TOP / LEFT
             theta=abs(overlap_xmyp)/vol_cube
             dtheta=dtheta+theta
             dforce_PartFlow(1:dim)=theta*Lpart(n)%a(1:dim)
             force_PartFlow(dir,i-1,j+1,k+1)=force_PartFlow(dir,i-1,j+1,k+1) &
                  & +dot_product(dforce_PartFlow,vec_dir)

             ! BOTTOM / RIGHT
             theta=abs(overlap_xpym)/vol_cube
             dtheta=dtheta+theta
             dforce_PartFlow(1:dim)=theta*Lpart(n)%a(1:dim)
             force_PartFlow(dir,i+1,j-1,k+1)=force_PartFlow(dir,i+1,j-1,k+1) &
                  & +dot_product(dforce_PartFlow,vec_dir)

             ! TOP / RIGHT
             theta=abs(overlap_xpyp)/vol_cube
             dtheta=dtheta+theta
             dforce_PartFlow(1:dim)=theta*Lpart(n)%a(1:dim)
             force_PartFlow(dir,i+1,j+1,k+1)=force_PartFlow(dir,i+1,j+1,k+1) &
                  & +dot_product(dforce_PartFlow,vec_dir)

             !------
             ! EDGES 
             !------

             ! LEFT
             theta=(abs(overlap_xm)*d-abs(overlap_xmym)-abs(overlap_xmyp))/vol_cube
             dtheta=dtheta+theta
             dforce_PartFlow(1:dim)=theta*Lpart(n)%a(1:dim)
             force_PartFlow(dir,i-1,j,k+1)=force_PartFlow(dir,i-1,j,k+1) &
                  & +dot_product(dforce_PartFlow,vec_dir)

             ! RIGHT
             theta=(abs(overlap_xp)*d-abs(overlap_xpym)-abs(overlap_xpyp))/vol_cube
             dtheta=dtheta+theta
             dforce_PartFlow(1:dim)=theta*Lpart(n)%a(1:dim)
             force_PartFlow(dir,i+1,j,k+1)=force_PartFlow(dir,i+1,j,k+1) &
                  & +dot_product(dforce_PartFlow,vec_dir)

             ! BOTTOM
             theta=(abs(overlap_ym)*d-abs(overlap_xmym)-abs(overlap_xpym))/vol_cube
             dtheta=dtheta+theta
             dforce_PartFlow(1:dim)=theta*Lpart(n)%a(1:dim)
             force_PartFlow(dir,i,j-1,k+1)=force_PartFlow(dir,i,j-1,k+1) &
                  & +dot_product(dforce_PartFlow,vec_dir)

             ! TOP
             theta=(abs(overlap_yp)*d-abs(overlap_xmyp)-abs(overlap_xpyp))/vol_cube
             dtheta=dtheta+theta
             dforce_PartFlow(1:dim)=theta*Lpart(n)%a(1:dim)
             force_PartFlow(dir,i,j+1,k+1)=force_PartFlow(dir,i,j+1,k+1) &
                  & +dot_product(dforce_PartFlow,vec_dir)

             !------------
             ! CELL (REST)
             !------------

             theta=1-dtheta
             dforce_PartFlow(1:dim)=theta*Lpart(n)%a(1:dim)
             force_PartFlow(dir,i,j,k+1)=force_PartFlow(dir,i,j,k+1) &
                  & +dot_product(dforce_PartFlow,vec_dir)

          else

             ! overlap corners
             overlap_xmymzm=abs(overlap_xm*overlap_ym*overlap_zm)   ! > 0
             overlap_xmypzm=abs(overlap_xm*overlap_yp*overlap_zm)
             overlap_xpymzm=abs(overlap_xp*overlap_ym*overlap_zm)
             overlap_xpypzm=abs(overlap_xp*overlap_yp*overlap_zm)

             overlap_xmymzp=abs(overlap_xm*overlap_ym*overlap_zp)   ! > 0
             overlap_xmypzp=abs(overlap_xm*overlap_yp*overlap_zp)
             overlap_xpymzp=abs(overlap_xp*overlap_ym*overlap_zp)
             overlap_xpypzp=abs(overlap_xp*overlap_yp*overlap_zp)

             ! overlap edges
             overlap_xmym=overlap_xm*overlap_ym   ! > 0
             overlap_xmyp=overlap_xm*overlap_yp
             overlap_xpym=overlap_xp*overlap_ym
             overlap_xpyp=overlap_xp*overlap_yp
             
             overlap_xmzm=overlap_xm*overlap_zm   ! > 0
             overlap_ymzm=overlap_ym*overlap_zm
             overlap_xpzm=overlap_xp*overlap_zm
             overlap_ypzm=overlap_yp*overlap_zm

             overlap_xmzp=overlap_xm*overlap_zp   ! > 0
             overlap_ymzp=overlap_ym*overlap_zp
             overlap_xpzp=overlap_xp*overlap_zp
             overlap_ypzp=overlap_yp*overlap_zp

             dtheta=0

             !--------
             ! CORNERS 
             !--------

             ! LEFT / DOWN / FRONT
             theta=abs(overlap_xmymzm)/vol_cube
             dtheta=dtheta+theta
             dforce_PartFlow(1:dim)=theta*Lpart(n)%a(1:dim)
             force_PartFlow(dir,i-1,j-1,k-1)=force_PartFlow(dir,i-1,j-1,k-1) &
                  & +dot_product(dforce_PartFlow,vec_dir)

             ! RIGHT / DOWN / FRONT
             theta=abs(overlap_xpymzm)/vol_cube
             dtheta=dtheta+theta
             dforce_PartFlow(1:dim)=theta*Lpart(n)%a(1:dim)
             force_PartFlow(dir,i+1,j-1,k-1)=force_PartFlow(dir,i+1,j-1,k-1) &
                  & +dot_product(dforce_PartFlow,vec_dir)

             ! LEFT / UP / FRONT
             theta=abs(overlap_xmypzm)/vol_cube
             dtheta=dtheta+theta
             dforce_PartFlow(1:dim)=theta*Lpart(n)%a(1:dim)
             force_PartFlow(dir,i-1,j+1,k-1)=force_PartFlow(dir,i-1,j+1,k-1) &
                  & +dot_product(dforce_PartFlow,vec_dir)

             ! RIGHT / UP / FRONT
             theta=abs(overlap_xpypzm)/vol_cube
             dtheta=dtheta+theta
             dforce_PartFlow(1:dim)=theta*Lpart(n)%a(1:dim)
             force_PartFlow(dir,i+1,j+1,k-1)=force_PartFlow(dir,i+1,j+1,k-1) &
                  & +dot_product(dforce_PartFlow,vec_dir)

             ! BOTTOM ...
             theta=abs(overlap_xmymzp)/vol_cube
             dtheta=dtheta+theta
             dforce_PartFlow(1:dim)=theta*Lpart(n)%a(1:dim)
             force_PartFlow(dir,i-1,j-1,k+1)=force_PartFlow(dir,i-1,j-1,k+1) &
                  & +dot_product(dforce_PartFlow,vec_dir)

             theta=abs(overlap_xpymzp)/vol_cube
             dtheta=dtheta+theta
             dforce_PartFlow(1:dim)=theta*Lpart(n)%a(1:dim)
             force_PartFlow(dir,i+1,j-1,k+1)=force_PartFlow(dir,i+1,j-1,k+1) &
                  & +dot_product(dforce_PartFlow,vec_dir)

             theta=abs(overlap_xmypzp)/vol_cube
             dtheta=dtheta+theta
             dforce_PartFlow(1:dim)=theta*Lpart(n)%a(1:dim)
             force_PartFlow(dir,i-1,j+1,k+1)=force_PartFlow(dir,i-1,j+1,k+1) &
                  & +dot_product(dforce_PartFlow,vec_dir)

             theta=abs(overlap_xpypzp)/vol_cube
             dtheta=dtheta+theta
             dforce_PartFlow(1:dim)=theta*Lpart(n)%a(1:dim)
             force_PartFlow(dir,i+1,j+1,k+1)=force_PartFlow(dir,i+1,j+1,k+1) &
                  & +dot_product(dforce_PartFlow,vec_dir)

             !------
             ! EDGES 
             !------

             ! FRONT
             theta=(abs(overlap_xmzm)*d-(overlap_xmymzm+overlap_xmypzm))/vol_cube
             dtheta=dtheta+theta
             dforce_PartFlow(1:dim)=theta*Lpart(n)%a(1:dim)
             force_PartFlow(dir,i-1,j,k-1)=force_PartFlow(dir,i-1,j,k-1) &
                  & +dot_product(dforce_PartFlow,vec_dir)

             theta=(abs(overlap_xpzm)*d-(overlap_xpymzm+overlap_xpypzm))/vol_cube
             dtheta=dtheta+theta
             dforce_PartFlow(1:dim)=theta*Lpart(n)%a(1:dim)
             force_PartFlow(dir,i+1,j,k-1)=force_PartFlow(dir,i+1,j,k-1) &
                  & +dot_product(dforce_PartFlow,vec_dir)

             theta=(abs(overlap_ymzm)*d-(overlap_xmymzm+overlap_xpymzm))/vol_cube
             dtheta=dtheta+theta
             dforce_PartFlow(1:dim)=theta*Lpart(n)%a(1:dim)
             force_PartFlow(dir,i,j-1,k-1)=force_PartFlow(dir,i,j-1,k-1) &
                  & +dot_product(dforce_PartFlow,vec_dir)

             theta=(abs(overlap_ypzm)*d-(overlap_xmypzm+overlap_xpypzm))/vol_cube
             dtheta=dtheta+theta
             dforce_PartFlow(1:dim)=theta*Lpart(n)%a(1:dim)
             force_PartFlow(dir,i,j+1,k-1)=force_PartFlow(dir,i,j+1,k-1) &
                  & +dot_product(dforce_PartFlow,vec_dir)

             ! BOTTOM
             theta=(abs(overlap_xmzp)*d-(overlap_xmymzp+overlap_xmypzp))/vol_cube
             dtheta=dtheta+theta
             dforce_PartFlow(1:dim)=theta*Lpart(n)%a(1:dim)
             force_PartFlow(dir,i-1,j,k+1)=force_PartFlow(dir,i-1,j,k+1) &
                  & +dot_product(dforce_PartFlow,vec_dir)

             theta=(abs(overlap_xpzp)*d-(overlap_xpymzp+overlap_xpypzp))/vol_cube
             dtheta=dtheta+theta
             dforce_PartFlow(1:dim)=theta*Lpart(n)%a(1:dim)
             force_PartFlow(dir,i+1,j,k+1)=force_PartFlow(dir,i+1,j,k+1) &
                  & +dot_product(dforce_PartFlow,vec_dir)

             theta=(abs(overlap_ymzp)*d-(overlap_xmymzp+overlap_xpymzp))/vol_cube
             dtheta=dtheta+theta
             dforce_PartFlow(1:dim)=theta*Lpart(n)%a(1:dim)
             force_PartFlow(dir,i,j-1,k+1)=force_PartFlow(dir,i,j-1,k+1) &
                  & +dot_product(dforce_PartFlow,vec_dir)

             theta=(abs(overlap_ypzp)*d-(overlap_xmypzp+overlap_xpypzp))/vol_cube
             dtheta=dtheta+theta
             dforce_PartFlow(1:dim)=theta*Lpart(n)%a(1:dim)
             force_PartFlow(dir,i,j+1,k+1)=force_PartFlow(dir,i,j+1,k+1) &
                  & +dot_product(dforce_PartFlow,vec_dir)

             ! MIDDLE
             theta=(abs(overlap_xmym)*d-(overlap_xmymzm+overlap_xmymzp))/vol_cube
             dtheta=dtheta+theta
             dforce_PartFlow(1:dim)=theta*Lpart(n)%a(1:dim)
             force_PartFlow(dir,i-1,j-1,k)=force_PartFlow(dir,i-1,j-1,k) &
                  & +dot_product(dforce_PartFlow,vec_dir)

             theta=(abs(overlap_xpym)*d-(overlap_xpymzm+overlap_xpymzp))/vol_cube
             dtheta=dtheta+theta
             dforce_PartFlow(1:dim)=theta*Lpart(n)%a(1:dim)
             force_PartFlow(dir,i+1,j-1,k)=force_PartFlow(dir,i+1,j-1,k) &
                  & +dot_product(dforce_PartFlow,vec_dir)

             theta=(abs(overlap_xmyp)*d-(overlap_xmypzm+overlap_xmypzp))/vol_cube
             dtheta=dtheta+theta
             dforce_PartFlow(1:dim)=theta*Lpart(n)%a(1:dim)
             force_PartFlow(dir,i-1,j+1,k)=force_PartFlow(dir,i-1,j+1,k) &
                  & +dot_product(dforce_PartFlow,vec_dir)

             theta=(abs(overlap_xpyp)*d-(overlap_xpypzm+overlap_xpypzp))/vol_cube
             dtheta=dtheta+theta
             dforce_PartFlow(1:dim)=theta*Lpart(n)%a(1:dim)
             force_PartFlow(dir,i+1,j+1,k)=force_PartFlow(dir,i+1,j+1,k) &
                  & +dot_product(dforce_PartFlow,vec_dir)

             !------
             ! FACES
             !------

             ! LEFT
             theta=(abs(overlap_xm)*dd                                                    &
                  & -d*(overlap_xmzm+overlap_xmzp+overlap_xmym+overlap_xmyp)              &
                  & +(overlap_xmymzm+overlap_xmymzp+overlap_xmypzm+overlap_xmypzp))/vol_cube   ! les coins sont soustraits 2 fois etape precedente
             dtheta=dtheta+theta
             dforce_PartFlow(1:dim)=theta*Lpart(n)%a(1:dim)
             force_PartFlow(dir,i-1,j,k)=force_PartFlow(dir,i-1,j,k) &
                  & +dot_product(dforce_PartFlow,vec_dir)

             ! RIGHT
             theta=(abs(overlap_xp)*dd                                                    &
                  & -d*(overlap_xpzm+overlap_xpzp+overlap_xpym+overlap_xpyp)              &
                  & +(overlap_xpymzm+overlap_xpymzp+overlap_xpypzm+overlap_xpypzp))/vol_cube
             dtheta=dtheta+theta
             dforce_PartFlow(1:dim)=theta*Lpart(n)%a(1:dim)
             force_PartFlow(dir,i+1,j,k)=force_PartFlow(dir,i+1,j,k) &
                  & +dot_product(dforce_PartFlow,vec_dir)
             
             ! DOWN
             theta=(abs(overlap_ym)*dd                                                    &
                  & -d*(overlap_ymzm+overlap_ymzp+overlap_xmym+overlap_xpym)              &
                  & +(overlap_xmymzm+overlap_xmymzp+overlap_xpymzm+overlap_xpymzp))/vol_cube
             dtheta=dtheta+theta
             dforce_PartFlow(1:dim)=theta*Lpart(n)%a(1:dim)
             force_PartFlow(dir,i,j-1,k)=force_PartFlow(dir,i,j-1,k) &
                  & +dot_product(dforce_PartFlow,vec_dir)

             ! UP
             theta=(abs(overlap_yp)*dd                                                    &
                  & -d*(overlap_ypzm+overlap_ypzp+overlap_xmyp+overlap_xpyp)              &
                  & +(overlap_xmypzm+overlap_xmypzp+overlap_xpypzm+overlap_xpypzp))/vol_cube
             dtheta=dtheta+theta
             dforce_PartFlow(1:dim)=theta*Lpart(n)%a(1:dim)
             force_PartFlow(dir,i,j+1,k)=force_PartFlow(dir,i,j+1,k) &
                  & +dot_product(dforce_PartFlow,vec_dir)

             ! FRONT
             theta=(abs(overlap_zm)*dd                                                    &
                  & -d*(overlap_ymzm+overlap_ypzm+overlap_xmzm+overlap_xpzm)              &
                  & +(overlap_xmymzm+overlap_xpymzm+overlap_xmypzm+overlap_xpypzm))/vol_cube
             dtheta=dtheta+theta
             dforce_PartFlow(1:dim)=theta*Lpart(n)%a(1:dim)
             force_PartFlow(dir,i,j,k-1)=force_PartFlow(dir,i,j,k-1) &
                  & +dot_product(dforce_PartFlow,vec_dir)

             ! FRONT
             theta=(abs(overlap_zp)*dd                                                    &
                  & -d*(overlap_ymzp+overlap_ypzp+overlap_xmzp+overlap_xpzp)              &
                  & +(overlap_xmymzp+overlap_xpymzp+overlap_xmypzp+overlap_xpypzp))/vol_cube
             dtheta=dtheta+theta
             dforce_PartFlow(1:dim)=theta*Lpart(n)%a(1:dim)
             force_PartFlow(dir,i,j,k+1)=force_PartFlow(dir,i,j,k+1) &
                  & +dot_product(dforce_PartFlow,vec_dir)

             !------------
             ! CELL (REST)
             !------------

             theta=1-dtheta
             dforce_PartFlow(1:dim)=theta*Lpart(n)%a(1:dim)
             force_PartFlow(dir,i,j,k)=force_PartFlow(dir,i,j,k) &
                  & +dot_product(dforce_PartFlow,vec_dir)
             
          end if

       end do
       
    end do
    
  end subroutine two_way_coupling_force
  
#endif

  
  subroutine PIC_Sca_PartSplit_Remesh(nt,time,LPart,nPart,sca,sca0,u,v,w)
    use mod_Parameters, only: dim,mesh,EN_inertial_scheme
    use mod_Constants, only: zero,one,d1p2,d1p4,d1p6,d1p8
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)                                      :: nPart,nt
    real(8)                                                  :: time
    real(8), allocatable, dimension(:,:,:), intent(in)       :: sca0
    real(8), allocatable, dimension(:,:,:), intent(in)       :: u,v,w
    real(8), allocatable, dimension(:,:,:), intent(inout)    :: sca
    type(particle_t), dimension(:), allocatable, intent(inout) :: LPart
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                :: i,j,k,n
    integer                                :: case_remesh
    integer, dimension(3)                  :: ijk
    real(8)                                :: c1,c2,c3,c4
    real(8)                                :: alphaL2,gammaL2
    real(8)                                :: alphaM3,gammaM3
    real(8)                                :: sigma
    real(8)                                :: y
    real(8)                                :: r_p,r_m,phi_p,phi_m
    real(8)                                :: eps,eps_r
    real(8)                                :: num,den
    real(8), dimension(3)                  :: x,xt,vel,velt
    real(8), allocatable, dimension(:,:,:) :: sca_pic
    !-------------------------------------------------------------------------------

    ! pp. 117 PhD Magni

    !-------------------------------------------------------------------------------
    ! remesh case
    !   0,1 --> lambda 2
    !   2   --> M3
    !   3   --> lambda 3
    !   4   --> M'4
    !   6   --> Mix lambda2 lim with M3
    !-------------------------------------------------------------------------------
    case_remesh=mod(abs(EN_inertial_scheme),20)
    !-------------------------------------------------------------------------------
    sigma=d1p8
    eps_r=10*epsilon(1d0)
    eps_r=1d-12
    eps=1d-20
    !-------------------------------------------------------------------------------

    allocate(sca_pic(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
    sca_pic=sca0
    sca=0

    
    do n=1,nPart
       if (LPart(n)%active) then

          !-------------------------------------------------------------------------------
          ! initial value
          !-------------------------------------------------------------------------------
          x=LPart(n)%r
          ijk=LPart(n)%ijk
          call search_cell_ijk(mesh,x,ijk,PRESS_MESH)
          i=ijk(1);j=ijk(2);k=ijk(3)
          LPart(n)%phi=sca_pic(i,j,k)
          !-------------------------------------------------------------------------------

          !-------------------------------------------------------------------------------
          ! X-direction
          !-------------------------------------------------------------------------------
          ! mid point position
          !-------------------------------------------------------------------------------
          x=LPart(n)%r
          call interpolation_uvw(time,u,v,w,x,vel,1,dim)
          xt=x+vel*d1p2*dt
          !-------------------------------------------------------------------------------
          ! mid point directional velocity
          !-------------------------------------------------------------------------------
          call interpolation_uvw(time,u,v,w,xt,vel,1,dim)
          velt=0
          velt(1)=vel(1)
          !-------------------------------------------------------------------------------
          ! advection
          !-------------------------------------------------------------------------------
          x=x+velt*dt
          !-------------------------------------------------------------------------------
          ! scalar cell
          !-------------------------------------------------------------------------------
          call search_cell_ijk(mesh,x,ijk,U_VEL_MESH)
          i=ijk(1);j=ijk(2);k=ijk(3)
          !-------------------------------------------------------------------------------
          ! remeshing
          !-------------------------------------------------------------------------------
          y=(x(1)-grid_x(i-1))/dxu(i)
          select case(case_remesh)
          case(0,1)
             !-------------------------------------------------------------------------------
             ! Lambda_2
             !-------------------------------------------------------------------------------
             if (y<=d1p2) then
                c1=y*(y-1)*d1p2
                c2= -(y-1)*(y+1)
                c3=y*(y+1)*d1p2
                !-------------------------------------------------------------------------------
                sca(i-2,j,k)=sca(i-2,j,k)+c1*LPart(n)%phi
                sca(i-1,j,k)=sca(i-1,j,k)+c2*LPart(n)%phi
                sca(i  ,j,k)=sca(i  ,j,k)+c3*LPart(n)%phi
             else
                y=y-1
                c1=y*(y-1)*d1p2
                c2= -(y-1)*(y+1)
                c3=y*(y+1)*d1p2
                !-------------------------------------------------------------------------------
                sca(i-1,j,k)=sca(i-1,j,k)+c1*LPart(n)%phi
                sca(i  ,j,k)=sca(i  ,j,k)+c2*LPart(n)%phi
                sca(i+1,j,k)=sca(i+1,j,k)+c3*LPart(n)%phi
             end if
             !-------------------------------------------------------------------------------
          case(2)
             !-------------------------------------------------------------------------------
             ! M_3
             !-------------------------------------------------------------------------------
             if (y<=d1p2) then
                c1=y*(y-1)*d1p2+sigma
                c2= -(y-1)*(y+1)-2*sigma
                c3=y*(y+1)*d1p2+sigma
                !-------------------------------------------------------------------------------
                sca(i-2,j,k)=sca(i-2,j,k)+c1*LPart(n)%phi
                sca(i-1,j,k)=sca(i-1,j,k)+c2*LPart(n)%phi
                sca(i  ,j,k)=sca(i  ,j,k)+c3*LPart(n)%phi
             else
                y=y-1
                c1=y*(y-1)*d1p2+sigma
                c2= -(y-1)*(y+1)-2*sigma
                c3=y*(y+1)*d1p2+sigma
                !-------------------------------------------------------------------------------
                sca(i-1,j,k)=sca(i-1,j,k)+c1*LPart(n)%phi
                sca(i  ,j,k)=sca(i  ,j,k)+c2*LPart(n)%phi
                sca(i+1,j,k)=sca(i+1,j,k)+c3*LPart(n)%phi
             end if
             !-------------------------------------------------------------------------------
          case(3)
             !-------------------------------------------------------------------------------
             ! Lambda_3
             !-------------------------------------------------------------------------------
             c1=-y*(y-1)*(y-2)*d1p6
             c2=   (y-2)*(y**2-1)*d1p2
             c3= y*(y+1)*(2-y)*d1p2
             c4= y*(y**2-1)*d1p6
             !-------------------------------------------------------------------------------
             sca(i-2,j,k)=sca(i-2,j,k)+c1*LPart(n)%phi
             sca(i-1,j,k)=sca(i-1,j,k)+c2*LPart(n)%phi
             sca(i  ,j,k)=sca(i  ,j,k)+c3*LPart(n)%phi
             sca(i+1,j,k)=sca(i+1,j,k)+c4*LPart(n)%phi
             !-------------------------------------------------------------------------------
          case(4)
             !-------------------------------------------------------------------------------
             ! M'_4
             !-------------------------------------------------------------------------------
             c1=-y*(y-1)**2*d1p2
             c2=   (y-1)*(3*y**2-2*y-2)*d1p2
             c3= y*(1+4*y-3*y**2)*d1p2
             c4= y**2*(y-1)*d1p2
             !-------------------------------------------------------------------------------
             sca(i-2,j,k)=sca(i-2,j,k)+c1*LPart(n)%phi
             sca(i-1,j,k)=sca(i-1,j,k)+c2*LPart(n)%phi
             sca(i  ,j,k)=sca(i  ,j,k)+c3*LPart(n)%phi
             sca(i+1,j,k)=sca(i+1,j,k)+c4*LPart(n)%phi
             !-------------------------------------------------------------------------------
          case(6:9)
             !-------------------------------------------------------------------------------
             ! Lambda_2 lim
             !-------------------------------------------------------------------------------
             if (y<=d1p2) then
                !-------------------------------------------------------------------------------
                ! gradients ratio
                !-------------------------------------------------------------------------------
!!$                num=(sca0(i-1,j,k)-sca0(i-2,j,k))
!!$                den=(sca0(i,j,k)-sca0(i-1,j,k)+eps)
!!$                if (num<eps_r) then
!!$                   r_p=0
!!$                else
!!$                   r_p=num/den
!!$                end if
!!$                num=(sca0(i-2,j,k)-sca0(i-3,j,k))
!!$                den=(sca0(i-1,j,k)-sca0(i-2,j,k)+eps)
!!$                if (num<eps_r) then
!!$                   r_m=0
!!$                else
!!$                   r_m=num/den
!!$                end if
                r_p=(sca0(i-1,j,k)-sca0(i-2,j,k))/(sca0(i,j,k)-sca0(i-1,j,k)+eps)
                r_m=(sca0(i-2,j,k)-sca0(i-3,j,k))/(sca0(i-1,j,k)-sca0(i-2,j,k)+eps)
                !-------------------------------------------------------------------------------
                ! minmod
                !-------------------------------------------------------------------------------
                select case(case_remesh)
                case(6)
                   phi_p=max(zero,min(one,r_p))
                   phi_m=max(zero,min(one,r_m))
                case(7)
                   phi_p=max(zero,min(one,4*r_p))
                   phi_m=max(zero,min(one,4*r_m))
                case(8)
                   phi_p=max(zero,min(4*(y+d1p2)**2,(6-8*y**2)*r_p))
                   phi_m=max(zero,min(4*(y+d1p2)**2,(6-8*y**2)*r_m))
                case(9)
                   phi_p=max(zero,min(one,4*(y+d1p2)**2,(6-8*y**2)*r_p))
                   phi_m=max(zero,min(one,4*(y+d1p2)**2,(6-8*y**2)*r_m))
                end select
                alphaM3=y*(y-1)*d1p2+sigma
                gammaM3=y*(y+1)*d1p2+sigma
                alphaM3=(y-d1p2)**2*d1p2
                gammaM3=(y+d1p2)**2*d1p2
                alphaL2=y*(y-1)*d1p2
                gammaL2=y*(y+1)*d1p2
                !-------------------------------------------------------------------------------
                c1=alphaM3+phi_m*(alphaL2-alphaM3)
                c2=1-alphaM3-gammaM3-phi_m*(alphaL2-alphaM3)-phi_p*(gammaL2-gammaM3)
                c3=gammaM3+phi_p*(gammaL2-gammaM3)
                !-------------------------------------------------------------------------------
                sca(i-2,j,k)=sca(i-2,j,k)+c1*LPart(n)%phi
                sca(i-1,j,k)=sca(i-1,j,k)+c2*LPart(n)%phi
                sca(i  ,j,k)=sca(i  ,j,k)+c3*LPart(n)%phi
                !-------------------------------------------------------------------------------
             else
                y=y-1
                !-------------------------------------------------------------------------------
                ! gradients ratio
                !-------------------------------------------------------------------------------
!!$                num=(sca0(i+2,j,k)-sca0(i+1,j,k))
!!$                den=(sca0(i+1,j,k)-sca0(i,j,k)+eps)
!!$                if (num<eps_r) then
!!$                   r_p=0
!!$                else
!!$                   r_p=num/den
!!$                end if
!!$                num=(sca0(i+1,j,k)-sca0(i,j,k))
!!$                den=(sca0(i,j,k)-sca0(i-1,j,k)+eps)
!!$                if (num<eps_r) then
!!$                   r_m=0
!!$                else
!!$                   r_m=num/den
!!$                end if
                r_p=(sca0(i+2,j,k)-sca0(i+1,j,k))/(sca0(i+1,j,k)-sca0(i,j,k)+eps)
                r_m=(sca0(i+1,j,k)-sca0(i,j,k))/(sca0(i,j,k)-sca0(i-1,j,k)+eps)
                !-------------------------------------------------------------------------------
                ! minmod
                !-------------------------------------------------------------------------------
                select case(case_remesh)
                case(6)
                   phi_p=max(zero,min(one,r_p))
                   phi_m=max(zero,min(one,r_m))
                case(7)
                   phi_p=max(zero,min(one,4*r_p))
                   phi_m=max(zero,min(one,4*r_m))
                case(8)
                   phi_p=max(zero,min(4*(y-d1p2)**2,(6-8*y**2)*r_p))
                   phi_m=max(zero,min(4*(y-d1p2)**2,(6-8*y**2)*r_m))
                case(9)
                   phi_p=max(zero,min(one,4*(y-d1p2)**2,(6-8*y**2)*r_p))
                   phi_m=max(zero,min(one,4*(y-d1p2)**2,(6-8*y**2)*r_m))
                end select
                alphaM3=y*(y-1)*d1p2+sigma
                gammaM3=y*(y+1)*d1p2+sigma
                alphaM3=(y-d1p2)**2*d1p2
                gammaM3=(y+d1p2)**2*d1p2
                alphaL2=y*(y-1)*d1p2
                gammaL2=y*(y+1)*d1p2
                !-------------------------------------------------------------------------------
                c1=alphaM3+phi_m*(alphaL2-alphaM3)
                c2=1-alphaM3-gammaM3-phi_m*(alphaL2-alphaM3)-phi_p*(gammaL2-gammaM3)
                c3=gammaM3+phi_p*(gammaL2-gammaM3)
                !-------------------------------------------------------------------------------
                sca(i-1,j,k)=sca(i-1,j,k)+c1*LPart(n)%phi
                sca(i  ,j,k)=sca(i  ,j,k)+c2*LPart(n)%phi
                sca(i+1,j,k)=sca(i+1,j,k)+c3*LPart(n)%phi
                !-------------------------------------------------------------------------------
             end if
             !-------------------------------------------------------------------------------
          end select

       end if
    end do

    return
    call sca_borders_and_periodicity(mesh,sca)
    
    sca_pic=sca
    sca=0

    do n=1,nPart
       if (LPart(n)%active) then
          
          !-------------------------------------------------------------------------------
          ! initial value
          !-------------------------------------------------------------------------------
          x=LPart(n)%r
          ijk=LPart(n)%ijk
          call search_cell_ijk(mesh,x,ijk,PRESS_MESH)
          i=ijk(1);j=ijk(2);k=ijk(3)
          LPart(n)%phi=sca_pic(i,j,k)
          !-------------------------------------------------------------------------------

          !-------------------------------------------------------------------------------
          ! Y-direction
          !-------------------------------------------------------------------------------
          ! mid point position
          !-------------------------------------------------------------------------------
          x=LPart(n)%r
          call interpolation_uvw(time,u,v,w,x,vel,1,dim)
          xt=x-vel(1)*d1p2*dt+vel(2)*d1p2*dt
          !xt=x+vel(2)*d1p2*dt
          !-------------------------------------------------------------------------------
          ! mid point directional velocity
          !-------------------------------------------------------------------------------
          call interpolation_uvw(time,u,v,w,xt,vel,1,dim)
          velt=0
          velt(2)=vel(2)
          !-------------------------------------------------------------------------------
          ! advection
          !-------------------------------------------------------------------------------
          x=x+velt*dt
          !-------------------------------------------------------------------------------
          ! scalar cell
          !-------------------------------------------------------------------------------
          call search_cell_ijk(mesh,x,ijk,V_VEL_MESH)
          i=ijk(1);j=ijk(2);k=ijk(3)
          !-------------------------------------------------------------------------------
          ! remeshing
          !-------------------------------------------------------------------------------
          y=(x(2)-grid_y(j-1))/dyv(j)
          select case(case_remesh)
          case(0,1)
             !-------------------------------------------------------------------------------
             ! Lambda_2
             !-------------------------------------------------------------------------------
             if (y<=d1p2) then
                c1=y*(y-1)*d1p2
                c2= -(y-1)*(y+1)
                c3=y*(y+1)*d1p2
                !-------------------------------------------------------------------------------
                sca(i,j-2,k)=sca(i,j-2,k)+c1*LPart(n)%phi
                sca(i,j-1,k)=sca(i,j-1,k)+c2*LPart(n)%phi
                sca(i,j  ,k)=sca(i,j  ,k)+c3*LPart(n)%phi
             else 
                y=y-1
                c1=y*(y-1)*d1p2
                c2= -(y-1)*(y+1)
                c3=y*(y+1)*d1p2
                !-------------------------------------------------------------------------------
                sca(i,j-1,k)=sca(i,j-1,k)+c1*LPart(n)%phi
                sca(i,j  ,k)=sca(i,j  ,k)+c2*LPart(n)%phi
                sca(i,j+1,k)=sca(i,j+1,k)+c3*LPart(n)%phi
             end if
             !-------------------------------------------------------------------------------
          case(2)
             !-------------------------------------------------------------------------------
             ! M_3
             !-------------------------------------------------------------------------------
             if (y<=d1p2) then
                c1=y*(y-1)*d1p2+sigma
                c2= -(y-1)*(y+1)-2*sigma
                c3=y*(y+1)*d1p2+sigma
                !-------------------------------------------------------------------------------
                sca(i,j-2,k)=sca(i,j-2,k)+c1*LPart(n)%phi
                sca(i,j-1,k)=sca(i,j-1,k)+c2*LPart(n)%phi
                sca(i,j  ,k)=sca(i,j  ,k)+c3*LPart(n)%phi
             else 
                y=y-1
                c1=y*(y-1)*d1p2+sigma
                c2= -(y-1)*(y+1)-2*sigma
                c3=y*(y+1)*d1p2+sigma
                !-------------------------------------------------------------------------------
                sca(i,j-1,k)=sca(i,j-1,k)+c1*LPart(n)%phi
                sca(i,j  ,k)=sca(i,j  ,k)+c2*LPart(n)%phi
                sca(i,j+1,k)=sca(i,j+1,k)+c3*LPart(n)%phi
             end if
             !-------------------------------------------------------------------------------
          case(3)
             !-------------------------------------------------------------------------------
             ! Lambda_3
             !-------------------------------------------------------------------------------
             c1=-y*(y-1)*(y-2)*d1p6
             c2=   (y-2)*(y**2-1)*d1p2
             c3= y*(y+1)*(2-y)*d1p2
             c4= y*(y**2-1)*d1p6
             !-------------------------------------------------------------------------------
             sca(i,j-2,k)=sca(i,j-2,k)+c1*LPart(n)%phi
             sca(i,j-1,k)=sca(i,j-1,k)+c2*LPart(n)%phi
             sca(i,j  ,k)=sca(i,j  ,k)+c3*LPart(n)%phi
             sca(i,j+1,k)=sca(i,j+1,k)+c4*LPart(n)%phi
             !-------------------------------------------------------------------------------
          case(4)
             !-------------------------------------------------------------------------------
             ! M'_4
             !-------------------------------------------------------------------------------
             c1=-y*(y-1)**2*d1p2
             c2=   (y-1)*(3*y**2-2*y-2)*d1p2
             c3= y*(1+4*y-3*y**2)*d1p2
             c4= y**2*(y-1)*d1p2
             !-------------------------------------------------------------------------------
             sca(i,j-2,k)=sca(i,j-2,k)+c1*LPart(n)%phi
             sca(i,j-1,k)=sca(i,j-1,k)+c2*LPart(n)%phi
             sca(i,j  ,k)=sca(i,j  ,k)+c3*LPart(n)%phi
             sca(i,j+1,k)=sca(i,j+1,k)+c4*LPart(n)%phi
             !-------------------------------------------------------------------------------
          case(6:9)
             !-------------------------------------------------------------------------------
             ! Lambda_2 lim
             !-------------------------------------------------------------------------------
             if (y<=d1p2) then
                !-------------------------------------------------------------------------------
                ! gradients ratio
                !-------------------------------------------------------------------------------
!!$                num=(sca0(i,j-1,k)-sca0(i,j-2,k))
!!$                den=(sca0(i,j,k)-sca0(i,j-1,k)+eps)
!!$                if (num<eps_r) then
!!$                   r_p=0
!!$                else
!!$                   r_p=num/den
!!$                end if
!!$                num=(sca0(i,j-2,k)-sca0(i,j-3,k))
!!$                den=(sca0(i,j-1,k)-sca0(i,j-2,k)+eps)
!!$                if (num<eps_r) then
!!$                   r_m=0
!!$                else
!!$                   r_m=num/den
!!$                end if
                r_p=(sca0(i,j-1,k)-sca0(i,j-2,k))/(sca0(i,j,k)-sca0(i,j-1,k)+eps)
                r_m=(sca0(i,j-2,k)-sca0(i,j-3,k))/(sca0(i,j-1,k)-sca0(i,j-2,k)+eps)
                !-------------------------------------------------------------------------------
                ! minmod
                !-------------------------------------------------------------------------------
                select case(case_remesh)
                case(6)
                   phi_p=max(zero,min(one,r_p))
                   phi_m=max(zero,min(one,r_m))
                case(7)
                   phi_p=max(zero,min(one,4*r_p))
                   phi_m=max(zero,min(one,4*r_m))
                case(8)
                   phi_p=max(zero,min(4*(y+d1p2)**2,(6-8*y**2)*r_p))
                   phi_m=max(zero,min(4*(y+d1p2)**2,(6-8*y**2)*r_m))
                case(9)
                   phi_p=max(zero,min(one,4*(y+d1p2)**2,(6-8*y**2)*r_p))
                   phi_m=max(zero,min(one,4*(y+d1p2)**2,(6-8*y**2)*r_m))
                end select
                alphaM3=y*(y-1)*d1p2+sigma
                gammaM3=y*(y+1)*d1p2+sigma
                alphaM3=(y-d1p2)**2*d1p2
                gammaM3=(y+d1p2)**2*d1p2
                alphaL2=y*(y-1)*d1p2
                gammaL2=y*(y+1)*d1p2
                !-------------------------------------------------------------------------------
                c1=alphaM3+phi_m*(alphaL2-alphaM3)
                c2=1-alphaM3-gammaM3-phi_m*(alphaL2-alphaM3)-phi_p*(gammaL2-gammaM3)
                c3=gammaM3+phi_p*(gammaL2-gammaM3)
                !-------------------------------------------------------------------------------
                sca(i,j-2,k)=sca(i,j-2,k)+c1*LPart(n)%phi
                sca(i,j-1,k)=sca(i,j-1,k)+c2*LPart(n)%phi
                sca(i,j  ,k)=sca(i,j  ,k)+c3*LPart(n)%phi
                !-------------------------------------------------------------------------------
             else
                y=y-1
                !-------------------------------------------------------------------------------
                ! gradients ratio
                !-------------------------------------------------------------------------------
!!$                num=(sca0(i,j+2,k)-sca0(i,j+1,k))
!!$                den=(sca0(i,j+1,k)-sca0(i,j,k)+eps)
!!$                if (num<eps_r) then
!!$                   r_p=0
!!$                else
!!$                   r_p=num/den
!!$                end if
!!$                num=(sca0(i,j+1,k)-sca0(i,j,k))
!!$                den=(sca0(i,j,k)-sca0(i,j-1,k)+eps)
!!$                if (num<eps_r) then
!!$                   r_m=0
!!$                else
!!$                   r_m=num/den
!!$                end if
                r_p=(sca0(i,j+2,k)-sca0(i,j+1,k))/(sca0(i,j+1,k)-sca0(i,j,k)+eps)
                r_m=(sca0(i,j+1,k)-sca0(i,j,k))/(sca0(i,j,k)-sca0(i,j-1,k)+eps)
                !-------------------------------------------------------------------------------
                ! minmod
                !-------------------------------------------------------------------------------
                select case(case_remesh)
                case(6)
                   phi_p=max(zero,min(one,r_p))
                   phi_m=max(zero,min(one,r_m))
                case(7)
                   phi_p=max(zero,min(one,4*r_p))
                   phi_m=max(zero,min(one,4*r_m))
                case(8)
                   phi_p=max(zero,min(4*(y-d1p2)**2,(6-8*y**2)*r_p))
                   phi_m=max(zero,min(4*(y-d1p2)**2,(6-8*y**2)*r_m))
                case(9)
                   phi_p=max(zero,min(one,4*(y-d1p2)**2,(6-8*y**2)*r_p))
                   phi_m=max(zero,min(one,4*(y-d1p2)**2,(6-8*y**2)*r_m))
                end select
                !-------------------------------------------------------------------------------
                alphaM3=y*(y-1)*d1p2+sigma
                gammaM3=y*(y+1)*d1p2+sigma
                alphaM3=(y-d1p2)**2*d1p2
                gammaM3=(y+d1p2)**2*d1p2
                alphaL2=y*(y-1)*d1p2
                gammaL2=y*(y+1)*d1p2
                !-------------------------------------------------------------------------------
                c1=alphaM3+phi_m*(alphaL2-alphaM3)
                c2=1-alphaM3-gammaM3-phi_m*(alphaL2-alphaM3)-phi_p*(gammaL2-gammaM3)
                c3=gammaM3+phi_p*(gammaL2-gammaM3)
                !-------------------------------------------------------------------------------
                sca(i,j-1,k)=sca(i,j-1,k)+c1*LPart(n)%phi
                sca(i,j  ,k)=sca(i,j  ,k)+c2*LPart(n)%phi
                sca(i,j+1,k)=sca(i,j+1,k)+c3*LPart(n)%phi
                !-------------------------------------------------------------------------------
             end if
             !-------------------------------------------------------------------------------
          end select

       end if
    end do

  end subroutine PIC_Sca_PartSplit_Remesh
  
end module mod_Lagrangian_Particle
!===============================================================================
