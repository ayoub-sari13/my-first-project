!===============================================================================
module Bib_VOFLag_BC_Velocity
    !===============================================================================
    contains
    !-----------------------------------------------------------------------------  
    !> @author  amine chadil
    ! 
    !> @brief penalisation des vitesses de toutes les particules et a la meme vitesse
    !
    !> @param[in]  penu, penv, penw : 
    !> @param[in]  uin, vin, win    : 
    !> @param[in]  vl               : 
    !-----------------------------------------------------------------------------
    subroutine BC_Velocity(penu,penv,penw,uin,vin,win,vl)
        !===============================================================================
        !modules
        !===============================================================================
        !-------------------------------------------------------------------------------
        !Modules de definition des structures et variables => src/mod/module_...f90
        !-------------------------------------------------------------------------------
        use Module_VOFLag_ParticlesDataStructure
        use Module_VOFLag_Flows,                  only : uniform_past_part,U_inf
        use mod_Parameters,                       only : deeptracking,dim
        !-------------------------------------------------------------------------------
        !Modules de subroutines de la librairie RESPECT => src/Bib_VOFLag_...f90
        !-------------------------------------------------------------------------------
        use Bib_VOFLag_BC_Velocity_UniformStokesFlowPastAParticle_Sphere
        use Bib_VOFLag_BC_Velocity_UniformStokesFlowPastAParticle_Cylinder
        !-------------------------------------------------------------------------------

        !===============================================================================
        !declarations des variables
        !===============================================================================
        implicit none
        !-------------------------------------------------------------------------------
        !variables globales
        !-------------------------------------------------------------------------------
        real(8), dimension(:,:,:,:), allocatable :: penu,penv,penw
        real(8), dimension(:,:,:),   allocatable :: uin,vin,win
        type(struct_vof_lag),      intent(inout) :: vl
        !-------------------------------------------------------------------------------
        !variables locales 
        !-------------------------------------------------------------------------------
        !-------------------------------------------------------------------------------

        !-------------------------------------------------------------------------------
        if (deeptracking) write(*,*) 'entree BC_Velocity'
        !-------------------------------------------------------------------------------

        !-------------------------------------------------------------------------------
        ! Uniform flow past a particle
        !-------------------------------------------------------------------------------
        if (uniform_past_part) then 
            if (dim .eq. 2) call BC_Velocity_UniformStokesFlowPastAParticle_Cylinder(penu,penv,&
                                    uin,vin,vl%objet(1)%pos,vl%objet(1)%sca(1))
            if (dim .eq. 3) call BC_Velocity_UniformStokesFlowPastAParticle_Sphere(penu,penv,penw,&
                                    uin,vin,win,vl%objet(1)%pos,vl%objet(1)%sca(1),U_inf)
        end if

        !-------------------------------------------------------------------------------
        if (deeptracking) write(*,*) 'sortie BC_Velocity'
        !-------------------------------------------------------------------------------
    end subroutine BC_Velocity

    !===============================================================================
end module Bib_VOFLag_BC_Velocity
!===============================================================================