!========================================================================
!**
!**   NAME       : Initialization.f90
!**
!**   AUTHOR     : St�phane Vincent
!**                Benoit Trouette
!**
!**   FUNCTION   : Modules of subroutines for variable initialization of all equations 
!**
!**   DATES      : Version 1.0.0  : from : May, 2020
!**
!========================================================================

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#include "precomp.h"
module mod_Init_EQ
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  use mod_Parameters, only: dim,dt,                    &
       & xmin,xmax,ymin,ymax,zmin,zmax,                &
       & dx,dy,dz,                                     &
       & grid_x,grid_xu,grid_y,grid_yv,grid_z,grid_zw, &
       & sx,ex,sy,ey,sz,ez,gx,gy,gz,                   &
       & sxu,exu,syu,eyu,szu,ezu,                      &
       & sxv,exv,syv,eyv,szv,ezv,                      &
       & sxw,exw,syw,eyw,szw,ezw,                      &
       & NS_init_vel,NS_time_vel,                      &
       & NB_phase,LS_activate,Euler,                   &
       & pi,                                           &
       & NS_init_vel_icase,NS_init_pre_icase,          &
       & EN_activate,EN_init,EN_init_icase,            &
       & VOF_activate,VOF_init,VOF_init_icase,         &
       & VOF_init_case,                                &
       & grav_x,grav_y,grav_z
  use mod_Constants
  use mod_Init_Navier
  use mod_mpi
  
contains
  !*****************************************************************************************************
  subroutine Init_Time_Variables(nt,start_loop)
    !***************************************************************************************************

    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    integer :: nt,start_loop
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    nt=0
    start_loop=0
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
  end subroutine Init_Time_Variables
  !*****************************************************************************************************

  !*****************************************************************************************************
  subroutine EQ_time(u,v,w,u0,v0,w0,u1,v1,w1,pres,pres0,nt)
    !*****************************************************************************************************
    !-------------------------------------------------------------------------------
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)                                   :: nt
    real(8), dimension(:,:,:), allocatable, intent(inout) :: pres,pres0
    real(8), dimension(:,:,:), allocatable, intent(inout) :: u,v,w,u0,v0,w0,u1,v1,w1
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------

    u1=u0
    u0=u
    v1=v0
    v0=v
    if (dim==3) then
       w1=w0
       w0=w
    endif
    pres0=pres

    !*****************************************************************************************************
  end subroutine EQ_time
  !*****************************************************************************************************

  !*****************************************************************************************************
  subroutine EQ_init(mesh,t,u,v,w,u0,v0,w0,u1,v1,w1,uin,vin,win,grdpu,grdpv,grdpw,&
       & pres,pres0,tp,tp0,tp1,cou,dist,nt)
    !*****************************************************************************************************
    use mod_parameters, only: Euler, &
         & NS_activate,EN_activate,  &
         & PF_activate,NB_phase,     &
         & LS_activate,NS_init_vel,  &
         & NS_time_vel,EN_init,      &
         & VOF_activate,VOF_init
    use mod_struct_grid
    use mod_mpi
 
    !-------------------------------------------------------------------------------
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)                                   :: nt
    real(8), intent(in)                                   :: t
    real(8), dimension(:,:,:), allocatable, intent(inout) :: pres,pres0
    real(8), dimension(:,:,:), allocatable, intent(inout) :: u,v,w,u0,v0,w0,u1,v1,w1
    real(8), dimension(:,:,:), allocatable, intent(in)    :: uin,vin,win
    real(8), dimension(:,:,:), allocatable, intent(inout) :: grdpu,grdpv,grdpw
    real(8), dimension(:,:,:), allocatable, intent(inout) :: tp,tp0,tp1
    real(8), dimension(:,:,:), allocatable, intent(inout) :: cou,dist
    type(grid_t), intent(in)                              :: mesh
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                               :: i,j,k
    real(8)                                               :: x,y,z,r
    real(8)                                               :: x0,y0,x1,y1,a,b,alpha
    real(8)                                               :: xc0,yc0,zc0,radius
    real(8)                                               :: eta,h,d,c,g,eps,etas
    real(8)                                               :: detadt,detadt2,detadt3
    real(8)                                               :: mu,sigma,delta
    real(8)                                               :: ul,ug
    real(8)                                               :: d1,d2,d3,d4
    real(8), dimension(2)                                 :: m,mbis
    !-------------------------------------------------------------------------------


    !-------------------------------------------------------------------------------
    ! init at 0 for all allocated arrays
    !-------------------------------------------------------------------------------
    if (nt==0) then
       u=0;v=0
       u0=u;v0=v
       grdpu=0;grdpv=0
       if (dim==3) then
          w=0
          w0=w
          grdpw=0
       endif
       if (abs(Euler)==2.and.NS_activate) then
          u1=u0;v1=v0
          if (mesh%dim==3) w1=w0
       endif
       !-------------------------------------------------------------------------------
       pres=0; pres0=0
       !-------------------------------------------------------------------------------
       if (EN_activate) then
          if (PF_activate .eqv. .false.) then
             tp=0
             tp0=tp
             if (abs(Euler)==2.and.EN_activate) tp1=tp0
          endif
       end if
!RESPECT
       !if (NB_phase==2) cou=0
!RESPECT
       if (LS_activate) dist=0
    end if
    !-------------------------------------------------------------------------------


    !-------------------------------------------------------------------------------
    ! specific init
    !-------------------------------------------------------------------------------
    if (NS_init_vel.or.NS_time_vel) then
       call init_uvw(t,u,v,w,pres)
       call comm_mpi_sca(pres)
       call comm_mpi_uvw(u,v,w)
    end if
    !-------------------------------------------------------------------------------
    if (EN_activate.and.EN_init.and.nt==0) then
       call init_tp(t,tp)
       call comm_mpi_sca(tp)
    end if
    !-------------------------------------------------------------------------------
    if (VOF_activate.and.VOF_init.and.nt==0) then
       if (mesh%id/=0) then
          write(*,*) "WARNING, INIT ON MESH%ID=",mesh%id
          write(*,*) "WARNING, INIT ON MESH%ID=",mesh%id
          write(*,*) "WARNING, INIT ON MESH%ID=",mesh%id
          write(*,*) "WARNING, INIT ON MESH%ID=",mesh%id
          write(*,*) "WARNING, INIT ON MESH%ID=",mesh%id
       end if
       call init_vof(mesh,t,u,v,w,pres,uin,vin,win,cou)
       call comm_mpi_sca(cou)
    end if
    !-------------------------------------------------------------------------------


    !-------------------------------------------------------------------------------
    ! n and n-1 variables
    !-------------------------------------------------------------------------------
    if (nt==0) then
       u0=u
       v0=v
       if (mesh%dim==3) w0=w
       if (allocated(u1)) then
          u1=u0
          v1=v0
          if (mesh%dim==3) w1=w0
       end if
       pres0=pres
       !-------------------------------------------------------------------------------
       if (EN_activate) then
          tp0=tp
          if (allocated(tp1)) tp1=tp0
       end if
       !-------------------------------------------------------------------------------
    end if
    !-------------------------------------------------------------------------------

    return

    !*****************************************************************************************************
  end subroutine EQ_init
  !*****************************************************************************************************

  subroutine init_uvw(t,u,v,w,pres)
    use mod_thi
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), intent(in)                                   :: t
    real(8), dimension(:,:,:), allocatable, intent(inout) :: u,v,w,pres
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                               :: i,j,k
    real(8)                                               :: x,y,z,r
    real(8)                                               :: x0,y0,x1,y1,a,b,alpha
    real(8)                                               :: xc0,yc0,zc0,radius
    real(8)                                               :: eta,h,d,c,g,eps,etas
    real(8)                                               :: detadt,detadt2,detadt3
    real(8)                                               :: mu,sigma,delta
    real(8)                                               :: ul,ug
    real(8)                                               :: d1,d2,d3,d4
    real(8), dimension(2)                                 :: m,mbis
    real(8), dimension(:,:,:), allocatable                :: phi,phi2,phi3
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Specific velocity initialization
    !-------------------------------------------------------------------------------
    select case(abs(NS_init_vel_icase))
    case(-999)
       do k=sz-gz,ez+gz
          do j=sy-gy,ey+gy
             do i=sx-gx,ex+gx
                u(i,j,k)=i*j*k
                v(i,j,k)=i*j*k
                pres(i,j,k)=i*j*k
                if (dim==3) w(i,j,k)=i*j*k
             end do
          end do
       end do
    case(0)
       u=0
       v=0
       if (dim==3) w=0
    case(1)
       !-------------------------------------------------------------------------------
       ! Taylor Green 2D/3D
       !-------------------------------------------------------------------------------
       if (dim==2) then
          do j=sy-gy,ey+gy
             do i=sx-gx,ex+gx
                u(i,j,1)=+cos(grid_xu(i))*sin(grid_y(j))
                v(i,j,1)=-sin(grid_x(i))*cos(grid_yv(j))
             end do
          end do
       else
          w=0
          do k=sz-gz,ez+gz
             do j=sy-gy,ey+gy
                do i=sx-gx,ex+gx
                   u(i,j,k)=+sin(grid_xu(i))*cos(grid_y(j))*cos(grid_z(k))
                   v(i,j,k)=-cos(grid_x(i))*sin(grid_yv(j))*cos(grid_z(k))
                end do
             end do
          end do
       endif
       !-------------------------------------------------------------------------------
    case(101)
       !-------------------------------------------------------------------------------
       ! Taylor Green 2D in 3D box, x-invariant
       !-------------------------------------------------------------------------------
       do k=sz-gz,ez+gz
          do j=sy-gy,ey+gy
             do i=sx-gx,ex+gx
                u(i,j,k)=0
                v(i,j,k)=+cos(grid_yv(j))*sin(grid_z(k))
                if (dim==3) w(i,j,k)=-sin(grid_y(j))*cos(grid_zw(k))
             end do
          end do
       end do
       !-------------------------------------------------------------------------------
    case(102)
       !-------------------------------------------------------------------------------
       ! Taylor Green 2D in 3D box, y-invariant
       !-------------------------------------------------------------------------------
       do k=sz-gz,ez+gz
          do j=sy-gy,ey+gy
             do i=sx-gx,ex+gx
                u(i,j,k)=+cos(grid_xu(i))*sin(grid_z(k))
                v(i,j,k)=0
                if (dim==3) w(i,j,k)=-sin(grid_x(i))*cos(grid_zw(k))
             end do
          end do
       end do
       !-------------------------------------------------------------------------------
    case(103)
       !-------------------------------------------------------------------------------
       ! Taylor Green 2D in 3D box, z-invariant
       !-------------------------------------------------------------------------------
       do k=sz-gz,ez+gz
          do j=sy-gy,ey+gy
             do i=sx-gx,ex+gx
                u(i,j,k)=+cos(grid_xu(i))*sin(grid_y(j))
                v(i,j,k)=-sin(grid_x(i))*cos(grid_yv(j))
                if (dim==3) w(i,j,k)=0
             end do
          end do
       end do
       !------------------------------------------------------------------------------
    case(2,4)
       !-------------------------------------------------------------------------------
       ! Rotating 2D velocity field
       !-------------------------------------------------------------------------------
       do k=sz-gz,ez+gz
          do j=sy-gy,ey+gy
             do i=sx-gx,ex+gx
                u(i,j,k)=-pi/2*(ymin+grid_y(j)-(ymax-ymin)/2)
                v(i,j,k)=+pi/2*(xmin+grid_x(i)-(xmax-xmin)/2)
             end do
          end do
       end do
       !-------------------------------------------------------------------------------
    case(3)
       !-------------------------------------------------------------------------------
       ! shear serpentin 2D
       !-------------------------------------------------------------------------------
       do k=sz-gz,ez+gz
          do j=sy-gy,ey+gy
             do i=sx-gx,ex+gx
                u(i,j,k)=-pi/2*(sin(pi*grid_xu(i)))**2*cos(pi*grid_y(j))*sin(pi*grid_y(j))
                if (NS_init_vel_icase<0) u(i,j,k)=u(i,j,k)*cos(pi*t/4)
                v(i,j,k)=+pi/2*(sin(pi*grid_yv(j)))**2*cos(pi*grid_x(i))*sin(pi*grid_x(i))
                if (NS_init_vel_icase<0) v(i,j,k)=v(i,j,k)*cos(pi*t/4)
             end do
          end do
       end do
       !-------------------------------------------------------------------------------
    case(31)
       !-------------------------------------------------------------------------------
       ! shear serpentin 2D
       !-------------------------------------------------------------------------------
       do k=sz-gz,ez+gz
          do j=sy-gy,ey+gy
             do i=sx-gx,ex+gx
                u(i,j,k)=-2*(sin(pi*grid_xu(i)))**2*cos(pi*grid_y(j))*sin(pi*grid_y(j))
                v(i,j,k)=+2*(sin(pi*grid_yv(j)))**2*cos(pi*grid_x(i))*sin(pi*grid_x(i))
             end do
          end do
       end do
    case(32)
       ! T = 2
       !-------------------------------------------------------------------------------
       ! shear serpentin 2D
       !-------------------------------------------------------------------------------
       do k=sz-gz,ez+gz
          do j=sy-gy,ey+gy
             do i=sx-gx,ex+gx
                u(i,j,k)= 0.5d0*pi*(sin(pi*grid_xu(i)))**2*cos(pi*grid_y(j))*sin(pi*grid_y(j))*cos(pi*t/2)
                v(i,j,k)=-0.5d0*pi*(sin(pi*grid_yv(j)))**2*cos(pi*grid_x(i))*sin(pi*grid_x(i))*cos(pi*t/2)
             end do
          end do
       end do
       !-------------------------------------------------------------------------------
    case(5)
       !-------------------------------------------------------------------------------
       ! test Riemann problem
       !-------------------------------------------------------------------------------
       u=0
       v=0
       do k=sz-gz,ez+gz
          do j=sy-gy,ey+gy
             do i=sx-gx,ex+gx
                x=grid_xu(i)
                if (x>=0 .and. x<=1) then 
                   u(i,j,k)=1
                end if
             end do
          end do
       end do
       !-------------------------------------------------------------------------------
    case(6)
       !-------------------------------------------------------------------------------
       ! Mukundan et al. JCP 2020
       ! double shear layer
       !-------------------------------------------------------------------------------
       delta=(ymax-ymin)/10 ! pc !
       d1 = ymin
       d2 = d1p2*(ymin+ymax)-d1p2*delta
       d3 = d1p2*(ymin+ymax)+d1p2*delta
       d4 = ymax
       delta = d3-d2
       sigma=0.01d0
       ul=2
       ug=30
       !-------------------------------------------------------------------------------
       do j=sy-gy,ey+gy
          do i=sx-gx,ex+gx
             !----------------------------------------------------
             ! x component 
             !----------------------------------------------------
             x=grid_xu(i)
             y=grid_y(j)+d1p2*(ymax-ymin)
             if((grid_y(j)>=d1.and.grid_y(j)<=d2).or.(grid_y(j)>=d3.and.grid_y(j)<=d4)) then
                u(i,j,1)=ug
             else 
                u(i,j,1)=ul
             end if
             !u(i,j,1)=u(i,j,1)+sigma*cos(2*pi*x/(xmax-xmin))*(xmax-xmin)/delta/pi*exp(-2*y/delta)
             !----------------------------------------------------
             ! y component 
             !----------------------------------------------------
             x=grid_x(i)
             y=grid_yv(j) !+d1p2*(ymax-ymin)
             v(i,j,1)=sigma*ul*sin(2*pi*x/(xmax-xmin))*exp(-(2*y/delta)**2)
             !----------------------------------------------------
          end do
       enddo
       !----------------------------------------------------
    case(7)
       !-------------------------------------------------------------------------------
       ! Mc Dermott and Pope 2008
       ! 3D / steady
       !-------------------------------------------------------------------------------
       do k=sz-gz,ez+gz
          do j=sy-gy,ey+gy
             do i=sx-gx,ex+gx
                !----------------------------------------------------
                ! x component 
                !----------------------------------------------------
                x=grid_xu(i)
                y=grid_y(j)
                z=grid_z(k)
                u(i,j,k)=1-2*cos(2*pi*x)*sin(2*pi*y)*sin(2*pi*z)
                u(i,j,k)=1-2*cos(x)*sin(y)*sin(z)
                !----------------------------------------------------
                ! y component 
                !----------------------------------------------------
                x=grid_x(i)
                y=grid_yv(j)
                z=grid_z(k)
                v(i,j,k)=1+4*sin(2*pi*x)*cos(2*pi*y)*sin(2*pi*z)
                v(i,j,k)=1+4*sin(x)*cos(y)*sin(z)
                !----------------------------------------------------
                ! z component 
                !----------------------------------------------------
                x=grid_x(i)
                y=grid_y(j)
                z=grid_zw(k)
                w(i,j,k)=1-2*sin(2*pi*x)*sin(2*pi*y)*cos(2*pi*z)
                w(i,j,k)=1-2*sin(x)*sin(y)*cos(z)
                !----------------------------------------------------
             end do
          enddo
       end do
       !----------------------------------------------------
    case(8)
       !-------------------------------------------------------------------------------
       ! Enright et al. JCP 2004
       !-------------------------------------------------------------------------------
       do k=sz-gz,ez+gz
          do j=sy-gy,ey+gy
             do i=sx-gx,ex+gx
                !----------------------------------------------------
                ! x component 
                !----------------------------------------------------
                x=grid_xu(i)
                y=grid_y(j)
                z=grid_z(k)
                u(i,j,k)=2*(sin(pi*x))**2*sin(2*pi*y)*sin(2*pi*z)
                !----------------------------------------------------
                ! y component 
                !----------------------------------------------------
                x=grid_x(i)
                y=grid_yv(j)
                z=grid_z(k)
                v(i,j,k)=-sin(2*pi*x)*(sin(pi*y))**2*sin(2*pi*z)
                !----------------------------------------------------
                ! z component 
                !----------------------------------------------------
                x=grid_x(i)
                y=grid_y(j)
                z=grid_zw(k)
                w(i,j,k)=-sin(2*pi*x)*sin(2*pi*y)*(sin(pi*z))**2
                !----------------------------------------------------
             end do
          enddo
       end do
       !----------------------------------------------------
    case(9)
       !-------------------------------------------------------------------------------
       ! Constant (1D case)
       !-------------------------------------------------------------------------------
       u=1
       v=0
       if (dim==3) w=0
       !----------------------------------------------------
    case(10)
       !-------------------------------------------------------------------------------
       ! Constant (2-3 case)
       !-------------------------------------------------------------------------------
       u=1
       v=1
       if (dim==3) w=1
       !----------------------------------------------------
    case(11)
       allocate(phi(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
       phi=0

       xc0=0.5d0
       yc0=0.5d0
       zc0=0.5d0

       if (dim==2) then 
          do k=sz-gz,ez+gz
             do j=sy,ey+gy
                do i=sx-2,ex+2
                   phi(i,j,1) =0.20d0+0.5d0*dx(i)*dx(i) - sqrt((grid_x(i)-xc0)**2+(grid_y(j)-yc0)**2)
                   if ( 0.5d0*(phi(i-1,j,1) + phi(i,j,1)) >= -4*dx(i) ) then 
                      u(i,j,1) = 1.0d0
                   else
                      u(i,j,1) = 0.0d0
                   end if
                end do
             end do
          end do
       else
          do k=sz-gz,ez+gz
             do j=sy,ey+gy
                do i=sx-2,ex+2
                   phi(i,j,k) =  0.20d0+0.5d0*dx(i)*dx(i) &
                        & - sqrt((grid_x(i)-xc0)**2+(grid_y(j)-yc0)**2+(grid_z(k)-zc0)**2)
                   if ( 0.5d0*(phi(i-1,j,k) + phi(i,j,k)) >= -4*dx(i) ) then 
                      u(i,j,k) = 1.0d0
                   else
                      u(i,j,k) = 0.0d0
                   end if
                end do
             end do
          end do

       end if
       v=0
       if (dim==3) w=0
       deallocate(phi)
       !----------------------------------------------------
    case(12)
#if FFTW3
       !-------------------------------------------------------------------------------
       ! Can thi: we construct the thi on a collocated N^3 grid in sequential.
       ! We then interpolate/read these values on the staggered grid
       ! The final step is to broadcast these values to the other threads
       !-------------------------------------------------------------------------------
       call thi3D (u,v,w)     
       call compute_Ek_spectrum (u,v,w,0)
#endif 
       ! call mpi_finalize(code)
       !-------------------------------------------------------------------------------
    case(20)
       !-------------------------------------------------------------------------------
       ! Can debug init: we testing here the gather and spread of fields
       !-------------------------------------------------------------------------------
#if FFTW3
       call debug_init_can(u,v,w)
#endif
       
       d1 = sum(u(sx:ex,sy:ey,sz:ez))
       d2 = 0
       call MPI_ALLREDUCE (d1, d2,1, MPI_double_precision , MPI_sum , MPI_COMM_WORLD ,code)
       print*,'init -- rank = , sum(u) = ',rank, d2

       if (rank == 0) then
          allocate (phi(gsx:gex, gsy:gey, gsz:gez),phi2(gsx:gex, gsy:gey, gsz:gez),phi3(gsx:gex, gsy:gey, gsz:gez))
          phi = 0
       endif

#if FFTW3
       call gather_vector_from_threads(phi,phi2,phi3,u,v,w)
#endif
       
       if (rank ==0) print*,'after gather -- sum(phi) = ', sum(phi)

#if FFTW3
       call spread_vector_to_threads(phi,phi2,phi3,u,v,w)
#endif 
       
       d1 = sum(u(sx:ex,sy:ey,sz:ez))
       d2 = 0
       call MPI_ALLREDUCE (d1, d2,1, MPI_double_precision , MPI_sum , MPI_COMM_WORLD ,code)
       print*,'after spread -- rank = , sum(u) = ',rank, d2

       stop
    end select
    !-------------------------------------------------------------------------------
  end subroutine init_uvw

  subroutine init_tp(t,tp)
    !-------------------------------------------------------------------------------
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), intent(in)                                   :: t
    real(8), dimension(:,:,:), allocatable, intent(inout) :: tp
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                               :: i,j,k
    real(8)                                               :: x,y,z,r
    real(8)                                               :: x0,y0,x1,y1,a,b,alpha
    real(8)                                               :: xc0,yc0,zc0,radius
    real(8)                                               :: eta,h,d,c,g,eps,etas
    real(8)                                               :: detadt,detadt2,detadt3
    real(8)                                               :: mu,sigma,delta
    real(8)                                               :: ul,ug
    real(8)                                               :: d1,d2,d3,d4
    real(8), dimension(2)                                 :: m,mbis
    real(8)                                               :: valuetp
    !-------------------------------------------------------------------------------

    !-------------------------------------------------------------------------------
    ! Specific temperature initialisation
    !-------------------------------------------------------------------------------
    select case(EN_init_icase)
    case(0)
       tp=0
    case(1)
       !-------------------------------------------------------------------------------
       ! linear x-temperature profile
       !-------------------------------------------------------------------------------
       do k=sz-gz,ez+gz 
          do j=sy-gy,ey+gy
             do i=sx-gx,ex+gx
                tp(i,j,k)=(xmax-grid_x(i))/(xmax-xmin)
             end do
          end do
       end do
       !-------------------------------------------------------------------------------
    case(2)
       !-------------------------------------------------------------------------------
       ! temperature spot
       !-------------------------------------------------------------------------------
       if (dim==2) then 
          !-------------------------------------------------------------------------------
          ! parameters
          !-------------------------------------------------------------------------------
          xc0=xmin+0.5d0*(xmax-xmin)
          yc0=ymin+0.75d0*(ymax-ymin)
          radius=0.1d0
          !-------------------------------------------------------------------------------
          tp=0
          valuetp = 1.
          call init_circle_const(xc0,yc0,radius,tp,valuetp,0)
         
          !-------------------------------------------------------------------------------
       else
          !-------------------------------------------------------------------------------
          ! parameters
          !-------------------------------------------------------------------------------
          xc0=xmin+0.35d0*(xmax-xmin)
          yc0=ymin+0.35d0*(ymax-ymin)
          zc0=zmin+0.35d0*(zmax-zmin)
          radius=0.15d0*(xmax-xmin)
          !-------------------------------------------------------------------------------
          tp=0
          do k=sz-gz,ez+gz
             do j=sy-gy,ey+gy
                do i=sx-gx,ex+gx
                   r=sqrt((grid_x(i)-xc0)**2+(grid_y(j)-yc0)**2+(grid_z(k)-zc0)**2)
                   if (r<radius) then
                      tp(i,j,k)=1
                   end if
                end do
             end do
          end do
          !-------------------------------------------------------------------------------
       end if
    case(21)
       !-------------------------------------------------------------------------------
       ! temperature spot
       !-------------------------------------------------------------------------------
       ! parameters
       !-------------------------------------------------------------------------------
       xc0=1.25d0
       yc0=0
       radius=0.4d0
       !-------------------------------------------------------------------------------
       tp=0
       valuetp = 1.
       call init_circle_const(xc0,yc0,radius,tp,valuetp,0)
       !-------------------------------------------------------------------------------
    case(22)
       !-------------------------------------------------------------------------------
       ! temperature spot
       !-------------------------------------------------------------------------------
       ! parameters
       !-------------------------------------------------------------------------------
       xc0=0.15d0
       yc0=0.2
       radius=0.05d0
       !-------------------------------------------------------------------------------
       tp=0
       valuetp = 1.
       call init_circle_const(xc0,yc0,radius,tp,valuetp,0)
       !-------------------------------------------------------------------------------
    case(3)
       !-------------------------------------------------------------------------------
       ! temperature conic spot
       !-------------------------------------------------------------------------------
       tp=0
       if (dim==2) then
          !-------------------------------------------------------------------------------
          ! parameters
          !-------------------------------------------------------------------------------
          xc0=xmin+0.5d0*(xmax-xmin)
          yc0=ymin+0.75d0*(ymax-ymin)
          radius=0.1d0
          !-------------------------------------------------------------------------------
          do k=sz-gz,ez+gz
             do j=sy-gy,ey+gy
                do i=sx-gx,ex+gx
                   r=sqrt((grid_x(i)-xc0)**2+(grid_y(j)-yc0)**2)
                   if (r<radius) then
                      tp(i,j,k)=(radius-r)/(radius)
                   end if
                end do
             end do
          end do
       else
          !-------------------------------------------------------------------------------
          ! parameters
          !-------------------------------------------------------------------------------
          xc0=xmin+0.35d0*(xmax-xmin)
          yc0=ymin+0.35d0*(ymax-ymin)
          zc0=ymin+0.35d0*(zmax-zmin)
          radius=0.15d0
          !-------------------------------------------------------------------------------
          do k=sz-gz,ez+gz
             do j=sy-gy,ey+gy
                do i=sx-gx,ex+gx
                   r=sqrt((grid_x(i)-xc0)**2+(grid_y(j)-yc0)**2+(grid_z(k)-zc0)**2)
                   if (r<radius) then
                      tp(i,j,k)=(radius-r)/(radius)
                   end if
                end do
             end do
          end do
       end if
       !-------------------------------------------------------------------------------
    case(4)
       !-------------------------------------------------------------------------------
       ! temperature Gaussian spot
       !-------------------------------------------------------------------------------
       ! parameters
       !-------------------------------------------------------------------------------
       xc0=xmin+0.5d0*(xmax-xmin)
       yc0=ymin+0.75d0*(ymax-ymin)
       radius=0.1d0
       mu=0
       sigma=0.05d0*max((xmax-xmin),(ymax-ymin))
       !-------------------------------------------------------------------------------
       tp=0
       do k=sz-gz,ez+gz
          do j=sy-gy,ey+gy
             do i=sx-gx,ex+gx
                r=sqrt((grid_x(i)-xc0)**2+(grid_y(j)-yc0)**2)
                tp(i,j,k)=0.125d0/sigma/sqrt(2*pi)*exp(-(r-mu)**2/2/sigma**2)
             end do
          end do
       end do
       !-------------------------------------------------------------------------------
    case(6)
       !-------------------------------------------------------------------------------
       ! 1D Spot
       !-------------------------------------------------------------------------------
       xc0=d1p2*(xmax+xmin)
       radius=0.05d0*(xmax-xmin)
       do k=sz-gz,ez+gz
          do j=sy-gy,ey+gy
             do i=sx-gx,ex+gx
                x=grid_x(i)
                if (x>=xc0-radius.and.x<=xc0+radius) then 
                   tp(i,j,k)=1
                else
                   tp(i,j,k)=0
                end if
             end do
          end do
       end do
       !-------------------------------------------------------------------------------
    case(7)
       !-------------------------------------------------------------------------------
       ! 1D Conic Spot
       !-------------------------------------------------------------------------------
       xc0=d1p2*(xmax+xmin)
       radius=0.05d0*(xmax-xmin)
       do k=sz-gz,ez+gz
          do j=sy-gy,ey+gy
             do i=sx-gx,ex+gx
                x=grid_x(i)
                if (x>=xc0-radius.and.x<=xc0+radius) then 
                   tp(i,j,k)=(radius-abs(x-xc0))/radius
                else
                   tp(i,j,k)=0
                end if
             end do
          end do
       end do
       !-------------------------------------------------------------------------------
    case(8)
       !-------------------------------------------------------------------------------
       ! Aslam Chadil et al. 2018
       !-------------------------------------------------------------------------------
       xc0=d1p2*(xmax+xmin)
       yc0=d1p2*(ymax+ymin)
       radius=2
       do k=sz-gz,ez+gz
          do j=sy-gy,ey+gy
             do i=sx-gx,ex+gx
                x=grid_x(i)
                y=grid_y(j)
                if ((x-xc0)**2+(y-yc0)**2<radius**2) then
                   tp(i,j,k)=cos(x)*sin(y)
                end if
             end do
          end do
       end do
       !-------------------------------------------------------------------------------
    case(9)
       !-------------------------------------------------------------------------------
       ! Derived of case(8) Aslam Chadil et al. 2018
       !-------------------------------------------------------------------------------
       xc0=d1p2*(xmax+xmin)
       yc0=d1p2*(ymax+ymin)
       radius=2
       do k=sz-gz,ez+gz
          do j=sy-gy,ey+gy
             do i=sx-gx,ex+gx
                x=grid_x(i)
                y=grid_y(j)
                if ((x-xc0)**2+(y-yc0)**2>radius**2) then
                   tp(i,j,k)=cos(x)*sin(y)
                end if
             end do
          end do
       end do
       !-------------------------------------------------------------------------------
    case(13)
       !-------------------------------------------------------------------------------
       ! HCERES 2019 case 1
       !-------------------------------------------------------------------------------
       do k=sz-gz,ez+gz
          do j=sy-gy,ey+gy
             do i=sx-gx,ex+gx
                x=grid_x(i)
                y=grid_y(j)
                z=grid_z(k)
                if (x>=0.6d0.and.x<=1.and.y>=0.5d0.and.y<=0.65.and.z<=0.6d0.and.z>=0.4) then
                   tp(i,j,k)=100
                else
                   tp(i,j,k)=20
                end if
             end do
          end do
       end do
       !-------------------------------------------------------------------------------
    case(14)
       ! case thesis Magni pp 34
       tp=0
       do i=sx,ex
          x=grid_x(i)
          if (x>=-0.8d0.and.x<=-0.6d0) then
             tp(i,:,:)=f1d(x,-0.7d0,log(2d4/9),5d-5)
          else if (x>=-0.4d0.and.x<=-0.2d0) then
             tp(i,:,:)=1
          else if (x>=0d0.and.x<=0.2d0) then
             tp(i,:,:)=1-abs(1d1*(x-1d-1))
          else if (x>=0.4d0.and.x<=0.6d0) then
             tp(i,:,:)=g1d(x,1d1,5d-3)
          else if (x>=0.8d0.and.x<=1d0) then
             tp(i,:,:)=f1d(x,9d-1,log(5d-5/36),5d-3)
          else if (x>=1.2d0.and.x<=1.8d0) then
             tp(i,:,:)=3d-1*cos(-1d1*x)
          end if
       end do

    end select
  end subroutine init_tp
  
  subroutine init_vof(mesh,t,u,v,w,pres,uin,vin,win,cou)
    !-------------------------------------------------------------------------------
    use mod_struct_grid
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8)                                               :: t
    real(8), dimension(:,:,:), allocatable, intent(inout) :: u,v,w,pres
    real(8), dimension(:,:,:), allocatable, intent(in)    :: uin,vin,win
    real(8), dimension(:,:,:), allocatable, intent(inout) :: cou
    type(grid_t), intent(in)                              :: mesh
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                               :: i,j,k,n
    real(8)                                               :: x,y,z,r
    real(8)                                               :: fx,fy,fz
    real(8)                                               :: x0,y0,z0,a,b,alpha
    real(8)                                               :: x1,x2,y1,y2,z1,z2
    real(8)                                               :: x_max,x_min
    real(8)                                               :: y_max,y_min
    real(8)                                               :: z_max,z_min
    real(8)                                               :: xc0,yc0,zc0,radius
    real(8)                                               :: eta,h,d,c,g,eps,etas
    real(8)                                               :: detadt,detadt2,detadt3
    real(8)                                               :: mu,sigma,delta
    real(8)                                               :: ul,ug
    real(8)                                               :: d1,d2,d3,d4
    real(8), dimension(2)                                 :: m,mbis
    !-------------------------------------------------------------------------------
    real(8)                                               :: spray_layer_h,spray_velocity
    real(8)                                               :: spray_drop_radius
    !----------------------------------------------------------------
    ! Specific VOF initialization
    !----------------------------------------------------------------
    select case(VOF_init_icase)
    case(1000)
       !----------------------------------------------------------------
       ! drops
       !----------------------------------------------------------------
       cou=0
       call init_sphere_sca(           &
            & VOF_init_case%center(1), &
            & VOF_init_case%center(2), &
            & VOF_init_case%center(3), &
            & VOF_init_case%radius,    &
            & cou,                     &
            & 1)
       !----------------------------------------------------------------
    case(1001)
       !----------------------------------------------------------------
       ! planes
       !----------------------------------------------------------------
       cou=0
       call init_plane_sca(             &
            & VOF_init_case%normal(1),  &
            & VOF_init_case%normal(2),  &
            & VOF_init_case%normal(3),  &
            & VOF_init_case%thick,      &
            & cou,                      &
            & 1)
       !----------------------------------------------------------------
    case(1002)
       !----------------------------------------------------------------
       ! Partial cube
       !----------------------------------------------------------------
       x0=VOF_init_case%pos(1)
       y0=VOF_init_case%pos(2)
       z0=VOF_init_case%pos(3)
       !----------------------------------------------------
       cou=0
       if (dim==2) then
          k=1
          do j=sy-1,ey+1
             do i=sx-1,ex+1
                !----------------------------------------------------
                ! x-direction
                !----------------------------------------------------
                x1=grid_xu(i)
                x2=grid_xu(i+1)
                !----------------------------------------------------
                fx=(x2-x0)/(x2-x1)
                !----------------------------------------------------
                if (fx<0) then
                   fx=1
                elseif (fx>1) then
                   fx=0
                else
                   fx=1-fx
                end if
                !----------------------------------------------------
                ! y-direction
                !----------------------------------------------------
                y1=grid_yv(j)
                y2=grid_yv(j+1)
                !----------------------------------------------------
                fy=(y2-y0)/(y2-y1)
                !----------------------------------------------------
                if (fy<0) then
                   fy=1
                elseif (fy>1) then
                   fy=0
                else
                   fy=1-fy
                end if
                !----------------------------------------------------
                ! cou function
                !----------------------------------------------------
                cou(i,j,k)=fx*fy
                !----------------------------------------------------
             end do
          end do
       else
          do k=sz-1,ez+1
             do j=sy-1,ey+1
                do i=sx-1,ex+1
                   !----------------------------------------------------
                   ! x-direction
                   !----------------------------------------------------
                   x1=grid_xu(i)
                   x2=grid_xu(i+1)
                   !----------------------------------------------------
                   fx=(x2-x0)/(x2-x1)
                   !----------------------------------------------------
                   if (fx<0) then
                      fx=1
                   elseif (fx>1) then
                      fx=0
                   else
                      fx=1-fx
                   end if
                   !----------------------------------------------------
                   ! y-direction
                   !----------------------------------------------------
                   y1=grid_yv(j)
                   y2=grid_yv(j+1)
                   !----------------------------------------------------
                   fy=(y2-y0)/(y2-y1)
                   !----------------------------------------------------
                   if (fy<0) then
                      fy=1
                   elseif (fy>1) then
                      fy=0
                   else
                      fy=1-fy
                   end if
                   !----------------------------------------------------
                   ! z-direction
                   !----------------------------------------------------
                   z1=grid_zw(k)
                   z2=grid_zw(k+1)
                   !----------------------------------------------------
                   fz=(z2-z0)/(z2-z1)
                   !----------------------------------------------------
                   if (fz<0) then
                      fz=1
                   elseif (fz>1) then
                      fz=0
                   else
                      fz=1-fz
                   end if
                   !----------------------------------------------------
                   ! cou function
                   !----------------------------------------------------
                   cou(i,j,k)=fx*fy*fz
                   !----------------------------------------------------
                end do
             end do
          end do
       end if
       !----------------------------------------------------
    case(1100)
       !----------------------------------------------------------------
       ! drops smooth
       !----------------------------------------------------------------
       cou=0
       !----------------------------------------------------------------
       xc0=VOF_init_case%center(1)
       yc0=VOF_init_case%center(2)
       zc0=VOF_init_case%center(3)
       radius=VOF_init_case%radius
       !----------------------------------------------------------------
       do k=sz,ez
          do j=sy,ey
             do i=sx,ex
                if (dim==2) then 
                   sigma=radius-sqrt((grid_x(i)-xc0)**2+(grid_y(j)-yc0)**2)
                else
                   sigma=radius-sqrt((grid_x(i)-xc0)**2+(grid_y(j)-yc0)**2+(grid_z(k)-zc0)**2)
                end if
                cou (i,j,k) = 0.5d0 * (1+ tanh(sigma/(0.25d0*dx(i))))
             end do
          end do
       end do
       !----------------------------------------------------------------
    case(1200)
       !----------------------------------------------------------------
       ! random liquid spray on liquid sheet
       !----------------------------------------------------------------
       cou=0
       !----------------------------------------------------------------
       ! n random drops
       !----------------------------------------------------------------
       do n=1,VOF_init_case%n
          !----------------------------------------------------------------
          call random_number(xc0)
          call MPI_BCAST(xc0,1,MPI_DOUBLE_PRECISION,0,COMM3D,MPI_CODE)
          x1=xmin+VOF_init_case%radius
          x2=xmax-VOF_init_case%radius
          xc0=x1+xc0*(x2-x1)
          !----------------------------------------------------------------
          call random_number(yc0)
          call MPI_BCAST(yc0,1,MPI_DOUBLE_PRECISION,0,COMM3D,MPI_CODE)
          if (dim==2) then
             y1=2*(ymax-ymin)/10
             y2=9*(ymax-ymin)/10
          else 
             y1=ymin+VOF_init_case%radius
             y2=ymax-VOF_init_case%radius
          end if
          yc0=y1+yc0*(y2-y1)
          !----------------------------------------------------------------
          if (dim==3) then
             call random_number(zc0)
             call MPI_BCAST(zc0,1,MPI_DOUBLE_PRECISION,0,COMM3D,MPI_CODE)
             z1=2*(zmax-zmin)/10
             z2=9*(zmax-zmin)/10
             zc0=z1+zc0*(z2-z1)
          end if
          !----------------------------------------------------------------
          call init_sphere_sca(        &
               & xc0,                  &
               & yc0,                  &
               & zc0,                  &
               & VOF_init_case%radius, &
               & cou,                  &
               & 1)
       end do
       !----------------------------------------------------------------
       ! impact velocity
       !----------------------------------------------------------------
       alpha=1
       do k=sz-gz,ez+gz
          do j=sy-gy,ey+gy
             do i=sx-gx,ex+gx
                !----------------------------------------------------
                ! impact velocity
                !----------------------------------------------------
                if (dim==2) then
                   v(i,j,k)=-alpha*cou(i,j,k)
                else
                   w(i,j,k)=-alpha*cou(i,j,k)
                end if
             end do
          end do
       end do
       !----------------------------------------------------------------
       ! liquid layer
       !----------------------------------------------------------------
       call init_plane_sca(             &
            & VOF_init_case%normal(1),  &
            & VOF_init_case%normal(2),  &
            & VOF_init_case%normal(3),  &
            & VOF_init_case%thick,      &
            & cou,                      &
            & 1)
    case(6)
       !----------------------------------------------------------------
       ! 3D init VOF for HCERES 2019 case 
       !----------------------------------------------------------------
       cou=0
       do k=sz-gz,ez+gz
          do j=sy-gy,ey+gy
             do i=sx-gx,ex+gx
                if (grid_y(j) <= 0.25d0 ) then
                   cou(i,j,k)=1
                end if
             end do
          end do
       end do
       do k=sz-gz,ez+gz
          do j=sy-gy,ey+gy
             do i=sx-gx,ex+gx
                if (grid_x(i)<0.00001) then
                   if (((grid_y(j)-0.75D0)**2+(grid_z(k)-0.5D0)**2) <= 0.1d0**2) then
                      cou(i,j,k)=1
                   end if
                endif
             end do
          end do
       end do
    case(7)
       !----------------------------------------------------------------
       ! Tsunamis Sou-Heurtebize - Soliton
       !----------------------------------------------------------------
       ! these lubin annexe C
       !----------------------------------------------------------------
       h=0.21d0   ! wave heigh
       d=0.7d0   ! water depth
       xc0=4     ! wave initial position
       !----------------------------------------------------------------
       cou=0
       do k=sz-gz,ez+gz
          do j=sy-gy,ey+gy
             do i=sx-gx,ex+gx
                x=grid_x(i)
                y=grid_y(j)
                !----------------------------------------------------------------
                ! solition wade definition
                !----------------------------------------------------------------
                eta=h*(one/cosh(sqrt(3*h/4/d**3)*(x-xc0)))**2
                if (y<=eta+d) then
                   cou(i,j,k)=1
                end if
                !----------------------------------------------------------------
                ! gravity
                !----------------------------------------------------------------
                g=sqrt(grav_x**2+grav_y**2+grav_z**2)
                !----------------------------------------------------------------
                ! wave celerity
                !----------------------------------------------------------------
                c = sqrt(g*(d+h))
                !----------------------------------------------------------------
                ! time derivative of eta
                !----------------------------------------------------------------
                detadt=H*2*c*sqrt(3*h/4/d**3)*sinh(sqrt(3*h/4/d**3)*(x-xc0))/(cosh(sqrt(3*h/4/d**3)*(x-xc0)))**3
                detadt2=c**2*d3p2*H/d**3*(2*cosh(sqrt(3*h/4/d**3)*(x-xc0))**2-3)/cosh(sqrt(3*h/4/d**3)*(x-xc0))**4
                detadt3=6*c**3*H/d**3*sqrt(3*h/4/d**3)*sinh(sqrt(3*h/4/d**3)*(x-xc0))*(cosh(sqrt(3*h/4/d**3)*(x-xc0))**2-3) &
                     & / cosh(sqrt(3*h/4/d**3)*(x-xc0))**5
                !----------------------------------------------------------------
                ! velocities
                !----------------------------------------------------------------
                eps=h/d
                etas=eta/h
                !----------------------------------------------------------------
!!$                   u(i,j,k)=sqrt(g*d)*eps*(etas-d1p4*eps*etas**2+d**2/3/c**2*(1-d3p2*y**2/d**2)*detadt2/h)
!!$                   v(i,j,k)=sqrt(g*d)*y*eps/c*((1-d1p2*eps*etas)*detadt/h+d**2/3/c**2*(1-y**2/2/d**2)*detadt3/h)
                !----------------------------------------------------------------
                ! first order
                !----------------------------------------------------------------
                u(i,j,k)=sqrt(g*d)*eta/d
                v(i,j,k)=sqrt(g*d)*y/d/c*detadt
                !----------------------------------------------------------------
                ! slope 
                !----------------------------------------------------------------
!!$                   alpha=5
!!$                   y0=0
!!$                   x0=8
!!$                   x1=9
!!$                   a=1d0/15
!!$                   y1=a*(x1-x0)+y0
!!$                   !y1=tan(alpha*pi/180)*(x1-x0)+y0
!!$                   a=(y1-y0)/(x1-x0)
!!$                   b=y0-a*x0
!!$                   if (y<=a*x+b) then
!!$                      cou(i,j,k)=1
!!$                   end if
                !----------------------------------------------------------------
             end do
          end do
       end do
       !----------------------------------------------------------------
    case(8)
       !-------------------------------------------------------------------------------
       ! N. P�rinet, D. Juric and L. S. Tuckerman
       ! Numerical simulation of Faraday waves
       ! JFM 2009
       !-------------------------------------------------------------------------------
       cou=0
       alpha=10d0/100
       if (dim==2) then
          k=1
          do j=sy-gy,ey+gy
             do i=sx-gx,ex+gx
                if (grid_y(j)<=alpha*(ymax-ymin) &
                     & *d1p2*cos(2*pi*grid_x(i)/(xmax-xmin))&
                     & +1.6d-3) then
                   cou(i,j,k)=1
                end if
             end do
          end do
       else
          do k=sz-gz,ez+gz
             do j=sy-gy,ey+gy
                do i=sx-gx,ex+gx
                   if (grid_z(k)<=alpha*(zmax-zmin) &
                        & *d1p2*cos(2*pi*grid_x(i)/(xmax-xmin)) &
                        &      *cos(2*pi*grid_y(j)/(ymax-ymin)) &
                        & +1.6d-3) then
                      cou(i,j,k)=1
                   end if
                end do
             end do
          end do
       end if
    case(9)
       !----------------------------------------------------
       ! Mukundan et al. JCP 2020
       ! Double shear layer
       !----------------------------------------------------
       !----------------------------------------------------
       cou=0
       if (dim==2) then
          delta=(ymax-ymin)/10 ! pc !
          d2 = d1p2*(ymin+ymax)-d1p2*delta
          d3 = d1p2*(ymin+ymax)+d1p2*delta
          call init_plane_sca(0d0,1d0,0d0,d3,cou,1)
          call init_plane_sca(0d0,1d0,0d0,d2,cou,-1)
       else
          delta=(zmax-zmin)*5d-2 ! pc !
          d2 = d1p2*(zmin+zmax)-d1p2*delta
          d3 = d1p2*(zmin+zmax)+d1p2*delta

          if (rank==0) then
             write(*,*) "-------------------------------------------------"
             write(*,*) "Case"
             write(*,*) "We =", fluids(1)%rho*1*delta/fluids(2)%sigma
          end if

          call init_plane_sca(0d0,0d0,1d0,d3,cou,1)
          call init_plane_sca(0d0,0d0,1d0,d2,cou,-1)
       end if
       !----------------------------------------------------
    case(12,120:129)
       !----------------------------------------------------
       ! CASES CEA
       !----------------------------------------------------
       xc0=0
       yc0=0
       select case(VOF_init_icase)
       case(12,120)
          !----------------------------------------------------
          ! case A
          !----------------------------------------------------
          radius=(2.2d-3)/2
          alpha=2.4d0
          delta=15
       case(121)
          !----------------------------------------------------
          ! case B
          !----------------------------------------------------
          radius=(1.8d-3)/2
          alpha=3d0
          delta=15
       case(122)
          !----------------------------------------------------
          ! case C
          !----------------------------------------------------
          radius=(2.3d-3)/2
          alpha=3.6d0
          delta=15
       case(123)
          !----------------------------------------------------
          ! case D
          !----------------------------------------------------
          radius=(2.95d-3)/2!(2.8d-3)/2
          alpha=4.2d0
          delta=15
       case(124)
          !----------------------------------------------------
          ! cas E
          !----------------------------------------------------
          radius=(2.9d-3)/2
          alpha=4.4d0
          delta=15
       case(125)
          !----------------------------------------------------
          ! Cossali multi drop
          !----------------------------------------------------
          radius=(3.1d-3)/2
          alpha=3.0d0
          delta=0.1935
       case(126)
          !----------------------------------------------------
          ! cas JL
          !----------------------------------------------------
          radius=(3.82d-3)/2
          alpha=3.94d0
          delta=0.67
       case(127)
          !----------------------------------------------------
          ! cas OKAWA
          !----------------------------------------------------
          radius=(3.03d-3)/2
          alpha=3.753d0
          delta=0.198
       case(128)
          !----------------------------------------------------
          ! cas santini (double impact)
          !----------------------------------------------------
          x0     = x0+2.552d-3
          radius = (2.27d-3)/2
          alpha  = 1d0
          delta  = 10
       case(129)
          !----------------------------------------------------
          ! cas Santini (double impact)
          !----------------------------------------------------
          x0     = x0+2.552d-3
          radius = (2.32d-3)/2
          alpha  = 2.0d0
          delta  = 10
       end select
       !----------------------------------------------------
       if (dim==2) then
          yc0=delta*(2*radius)+radius+0.4d0*radius
          zc0=grid_z(sz)
       else
          zc0=delta*(2*radius)+radius+0.5d0*radius
       endif
       !----------------------------------------------------
       if (rank==0) then
          write(*,*) "-------------------------------------------------"
          write(*,*) "Case"
          write(*,*) "Re =", fluids(2)%rho*2*radius*alpha/fluids(2)%mu
          write(*,*) "We =", fluids(2)%rho*alpha**2*2*radius/fluids(2)%sigma
          write(*,*) "Fr =", alpha**2/2/radius/max(abs(grav_y),abs(grav_z))
          write(*,*) "Oh =", sqrt(fluids(2)%rho*alpha**2*2*radius/fluids(2)%sigma) &
               & / (fluids(2)%rho*2*radius*alpha/fluids(2)%mu)
          write(*,*) "K =", (fluids(2)%rho*alpha**2*2*radius/fluids(2)%sigma)**0.8d0 &
               & * (fluids(2)%rho*2*radius*alpha/fluids(2)%mu)**0.4d0
          write(*,*) "Ma =", alpha/340
          write(*,*) "T_ref=D/V=", 2*radius/alpha
       end if
       !----------------------------------------------------
       cou=0
       !call init_sphere_sca(xc0+2.552d-3,yc0,zc0,radius,cou,1)
       call init_sphere_sca(xc0,yc0,zc0,radius,cou,1)
       !call init_sphere_sca(xc0+7.5d-3,yc0,zc0,radius,cou,1)
       do k=sz-gz+1,ez+gz
          do j=sy-gy,ey+gy
             do i=sx-gx,ex+gx
                !----------------------------------------------------
                ! impact velocity
                !----------------------------------------------------
                if (dim==2) then
                   v(i,j,1)=-alpha*cou(i,j,1)
                   v(i,j-1,1)=-alpha*cou(i,j-1,1)
                else
                   w(i,j,k)=-alpha*cou(i,j,k)
                   w(i,j,k-1)=-alpha*cou(i,j,k-1)
                end if
                !----------------------------------------------------
                ! laplace pressure
                !----------------------------------------------------
                !  if (cou(i,j,k)>0.5d0) then
                !     pres(i,j,k)=2*fluids(2)%sigma/radius
                !  end if
                !----------------------------------------------------
             end do
          end do
       end do
       ! plane
       if (dim==2) then
          call init_plane_sca(0d0,1d0,0d0,delta*(2*radius),cou,1)
       else
          call init_plane_sca(0d0,0d0,1d0,delta*(2*radius),cou,1)
       end if
       !----------------------------------------------------
    case(18)
       !----------------------------------------------------------------
       !   Fluid cou=1 inlets
       !----------------------------------------------------------------
       cou=0
       if (dim==2) then
          k = 1
          do j=sy-gy,ey+gy
             do i=sx-gx,ex+gx

                if (i==gsx) then
                   if (uin(i  ,j  ,k)>0) cou(i:i-gx,j,k)=1
                else if (i==gex) then
                   if (uin(i+1,j  ,k)<0) cou(i:i+gx,j,k)=1
                end if

                if (j==gsy) then
                   if (vin(i  ,j  ,k)>0) cou(i,j:j+gy,k)=1
                else if (j==gey) then 
                   if (vin(i  ,j+1,k)<0) cou(i,j:j+gy,k)=1
                end if
                
             end do
          end do
       else if (dim==3) then 
          do k=sz-gz,ez+gz
             do j=sy-gy,ey+gy
                do i=sx-gx,ex+gx

                   if (i==gsx) then
                      if (uin(i  ,j  ,k  )>0) cou(i:i-gx,j,k)=1
                   else if (i==gex) then
                      if (uin(i+1,j  ,k  )<0) cou(i:i+gx,j,k)=1
                   end if
                   
                   if (j==gsy) then
                      if (vin(i  ,j  ,k  )>0) cou(i,j:j+gy,k)=1
                   else if (j==gey) then 
                      if (vin(i  ,j+1,k  )<0) cou(i,j:j+gy,k)=1
                   end if

                   if (k==gsz) then
                      if (win(i  ,j  ,k  )>0) cou(i,j,k:k-gz)=1
                   else if (k==gez) then 
                      if (win(i  ,j  ,k+1)<0) cou(i,j,k:k+gz)=1
                   end if
                   
                end do
             end do
          end do
       end if
    case(19)
       ! ligament, case 5.5 Herrmann, JCP, 2013

       a   = 4d-2
       r   = 1d-1
       cou = 0
       
       if (mesh%dim==2) then
          do j = mesh%sy-mesh%gy,mesh%ey+mesh%gy
             do i = mesh%sx-mesh%gx,mesh%ex+mesh%gx
                
                if (mesh%y(j) <= a*sin(mesh%x(i)*pi)+r) then
                   if (mesh%y(j) >= -a*sin(mesh%x(i)*pi)-r) then
                      cou(i,j,1) = 1
                   end if
                end if

             end do
          end do
       else
          do k = mesh%sz-mesh%gz,mesh%ez+mesh%gz
             do j = mesh%sy-mesh%gy,mesh%ey+mesh%gy
                do i = mesh%sx-mesh%gx,mesh%ex+mesh%gx

                   b = sqrt(mesh%y(j)**2+mesh%z(k)**2)
                   if (b < r-a*cos(2*pi*mesh%x(i))) then
                      cou(i,j,k) = 1
                   end if

                end do
             end do
          end do
       end if
    case(70)
       !----------------------------------------------------------------
       ! drops
       !----------------------------------------------------------------
       cou=0
       call init_sphere_sca(           &
            & VOF_init_case%center(1), &
            & VOF_init_case%center(2), &
            & VOF_init_case%center(3), &
            & VOF_init_case%radius,    &
            & cou,                     &
            & 1)
       !----------------------------------------------------------------
       do j=sy-gy,ey+gy
          if (grid_y(j)>=1.5d0) then
             cou(:,j,:) = 1
          end if
       end do
       !----------------------------------------------------------------
    case(71)
       ! bubble/air interaction
       cou = 1
       d1  = 1.5d0
       call init_plane_sca(0d0,1d0,0d0,d1,cou,-1)

       call init_sphere_sca(           &
            & VOF_init_case%center(1), &
            & VOF_init_case%center(2), &
            & VOF_init_case%center(3), &
            & VOF_init_case%radius,    &
            & cou,                     &
            & 1)
    case(72)
       ! Impact 2D
       cou = 0
       d1  = 3*2.2d-3
       call init_plane_sca(0d0,1d0,0d0,d1,cou,1)

       call init_sphere_sca(           &
            & VOF_init_case%center(1), &
            & VOF_init_case%center(2), &
            & VOF_init_case%center(3), &
            & VOF_init_case%radius,    &
            & cou,                     &
            & 1)

       
       do j=sy-gy,ey+gy
          do i=sx-gx,ex+gx
             if ( (grid_y(j)>= d1) ) then
                if (cou(i,j,1)>=1d0) then 
                   ! v(i,j+2,1)=-2d0
                   !v(i,j+1,1)=-2d0
                   v(i,j,1)=-2d0
                   !v(i,j-1,1)=-2d0
                   !v(i,j-2,1)=-2d0
                   !pres(i,j,k)=2*fluids(2)%sigma/radius
                end if
             end if
          end do
       end do

    case(73)
       ! ImpactJFM Lyes, surfactant
       cou   = 0
       d1    = 0.2d0
       alpha = 1
       call init_plane_sca(0d0,0d0,1d0,d1,cou,1)
       
       call init_sphere_sca(                    &
            & VOF_init_case%center(1),          &
            & VOF_init_case%center(2),          &
            & VOF_init_case%center(3)+2*dz(sz), &
            & VOF_init_case%radius,             &
            & cou,                              &
            & 1)

       do k=sz-gz,ez+gz 
          do j=sy-gy,ey+gy
             do i=sx-gx,ex+gx
                if ( (grid_z(k)>= d1) ) then
                   if (cou(i,j,k)>=1d0) then 
                      ! v(i,j+2,1)=-2d0
                      !v(i,j+1,1)=-2d0
                      w(i,j,k)  = -alpha
                      w(i,j,k+1)= -alpha
                      !v(i,j-1,1)=-2d0
                      !v(i,j-2,1)=-2d0
                      !pres(i,j,k)=2*fluids(2)%sigma/radius
!!$                      call random_number(x)
!!$                      x = 1d-2*(x - d1p2)*d1p2
!!$                      cou(i,j,k) = cou(i,j,k) + x
                   end if
                end if
             end do
          end do
       end do
       

      write(*,*) maxval(abs(w))
       if (rank==0) then
          write(*,*) "-------------------------------------------------"
          write(*,*) "Case"
          write(*,*) "Re =", fluids(2)%rho*2*VOF_init_case%radius*alpha/fluids(2)%mu
          write(*,*) "We =", fluids(2)%rho*alpha**2*2*VOF_init_case%radius/fluids(2)%sigma
          write(*,*) "Fr =", alpha**2/2/VOF_init_case%radius/max(abs(grav_y),abs(grav_z)+1d-40)
          write(*,*) "Oh =", sqrt(fluids(2)%rho*alpha**2*2*VOF_init_case%radius/fluids(2)%sigma) &
               & / (fluids(2)%rho*2*VOF_init_case%radius*alpha/fluids(2)%mu)
          write(*,*) "K  =", (fluids(2)%rho*alpha**2*2*VOF_init_case%radius/fluids(2)%sigma)**0.8d0 &
               & * (fluids(2)%rho*2*VOF_init_case%radius*alpha/fluids(2)%mu)**0.4d0
          write(*,*) "Ma =", alpha/340
          write(*,*) "T_ref = D/V =", 2*VOF_init_case%radius/alpha
       end if
    case(999)
      cou=0d0
      if (dim==2) then 
         call init_sphere(VOF_init_case%center(1),VOF_init_case%center(2),&
                        1d0,VOF_init_case%radius,cou,1)
      else 
         call init_sphere(VOF_init_case%center(1),VOF_init_case%center(2),&
                        VOF_init_case%center(3),VOF_init_case%radius,cou,1)
      end if  

       
    end select
    !----------------------------------------------------------------
  end subroutine init_vof
  
  subroutine init_sphere_sca(x0,y0,z0,radius,cou,val)
    !-------------------------------------------------------------------------------
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)                                   :: val
    real(8), intent(in)                                   :: x0,y0,z0,radius
    real(8), dimension(:,:,:), allocatable, intent(inout) :: cou
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    real(8), dimension(3)                                 :: shift
    !-------------------------------------------------------------------------------

    shift(1)=xmax-xmin
    shift(2)=ymax-ymin
    shift(3)=zmax-zmin

    if (dim==2) then
       call init_sphere(x0,y0,z0,radius,cou,val)
       if (periodic(1) .and.periodic(2)) then
          call init_sphere(x0+shift(1),y0+shift(2),z0,radius,cou,val)
          call init_sphere(x0+shift(1),y0,z0,radius,cou,val)
          call init_sphere(x0,y0+shift(2),z0,radius,cou,val)
       elseif (.not.periodic(1) .and. periodic(2)) then
          call init_sphere(x0,y0+shift(2),z0,radius,cou,val)
       elseif (periodic(1) .and. .not.periodic(2)) then
          call init_sphere(x0+shift(1),y0,z0,radius,cou,val)
       end if
    else
       call init_sphere(x0,y0,z0,radius,cou,val)
       if (periodic(1) .and. periodic(2) .and. periodic(3)) then
          call init_sphere(x0+shift(1),y0+shift(2),z0+shift(3),radius,cou,val)
          call init_sphere(x0+shift(1),y0,z0,radius,cou,val)
          call init_sphere(x0,y0+shift(2),z0,radius,cou,val)
          call init_sphere(x0,y0,z0+shift(3),radius,cou,val)
          call init_sphere(x0+shift(1),y0+shift(2),z0,radius,cou,val)
          call init_sphere(x0+shift(1),y0,z0+shift(3),radius,cou,val)
          call init_sphere(x0,y0+shift(2),z0+shift(3),radius,cou,val)
       elseif (periodic(1) .and. .not.periodic(2) .and. .not.periodic(3)) then
          call init_sphere(x0+shift(1),y0,z0,radius,cou,val)
       elseif (.not.periodic(1) .and. periodic(2) .and. .not.periodic(3)) then
          call init_sphere(x0,y0+shift(2),z0,radius,cou,val)
       elseif (.not.periodic(1) .and. .not.periodic(2) .and. periodic(3)) then
          call init_sphere(x0,y0,z0+shift(3),radius,cou,val)
       elseif (periodic(1) .and. periodic(2) .and. .not.periodic(3)) then
          call init_sphere(x0+shift(1),y0,z0,radius,cou,val)
          call init_sphere(x0,y0+shift(2),z0,radius,cou,val)
          call init_sphere(x0+shift(1),y0+shift(2),z0,radius,cou,val)
       elseif (periodic(1) .and. .not.periodic(2) .and. periodic(3)) then
          call init_sphere(x0+shift(1),y0,z0,radius,cou,val)
          call init_sphere(x0,y0,z0+shift(3),radius,cou,val)
          call init_sphere(x0+shift(1),y0,z0+shift(3),radius,cou,val)
       elseif (.not.periodic(1) .and. periodic(2) .and. periodic(3)) then
          call init_sphere(x0+shift(1),y0,z0,radius,cou,val)
          call init_sphere(x0,y0,z0+shift(3),radius,cou,val)
          call init_sphere(x0+shift(1),y0,z0+shift(3),radius,cou,val)
       end if
    end if

  end subroutine init_sphere_sca

  subroutine init_sphere(x0,y0,z0,radius,cou,val)
    !-------------------------------------------------------------------------------
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)                                   :: val
    real(8), intent(in)                                   :: x0,y0,z0,radius
    real(8), dimension(:,:,:), allocatable, intent(inout) :: cou
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                               :: n=15
    integer                                               :: ni,nj,nk
    integer                                               :: i,j,k,k0
    integer                                               :: ii,jj,kk
    real(8)                                               :: cpt
    real(8)                                               :: x,y,z
    real(8)                                               :: deltax,deltay,deltaz
    integer, dimension(:,:,:), allocatable                :: coub
    real(8), dimension(:,:,:), allocatable                :: cou0
    !----------------------------------------------------------------

    !----------------------------------------------------------------
    ! init
    !----------------------------------------------------------------
    allocate(coub(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
    allocate(cou0(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
    cou0=0
    coub=0
    !----------------------------------------------------------------
    ni=n
    nj=n
    nk=n
    k0=1
    if (dim==2) k0=0
    !----------------------------------------------------------------


    !----------------------------------------------------------------
    ! binary fct
    !----------------------------------------------------------------
    do k=sz,ez
       do j=sy,ey
          do i=sx,ex

             if (dim==2) then
                if ((grid_x(i)-x0)**2+(grid_y(j)-y0)**2<=radius**2) then
                   coub(i,j,k)=1
                endif
             else
                if ((grid_x(i)-x0)**2+(grid_y(j)-y0)**2+(grid_z(k)-z0)**2<=radius**2) then
                   coub(i,j,k)=1
                endif
             end if

          end do
       end do
    end do
    !----------------------------------------------------------------


    do k=sz,ez
       do j=sy,ey
          do i=sx,ex

             cpt=sum(coub(i-1:i+1,j-1:j+1,k-k0:k+k0))
             !----------------------------------------------------------------
             ! test if cou >0 or <1
             !----------------------------------------------------------------
             if (cpt>0.or.cpt<3**dim) then

                deltax=(grid_xu(i+1)-grid_xu(i))/ni
                deltay=(grid_yv(j+1)-grid_yv(j))/nj
                if (dim==3) deltaz=(grid_zw(k+1)-grid_zw(k))/nk

                cpt=0
                do kk=1,nk
                   do jj=1,nj
                      do ii=1,ni

                         x=grid_xu(i)+deltax/2+(ii-1)*deltax
                         y=grid_yv(j)+deltay/2+(jj-1)*deltay
                         if (dim==3) z=grid_zw(k)+deltaz/2+(kk-1)*deltaz

                         if (dim==2) then
                            if ((x-x0)**2+(y-y0)**2<=radius**2) then
                               cpt=cpt+1
                            endif
                         else
                            if ((x-x0)**2+(y-y0)**2+(z-z0)**2<=radius**2) then
                               cpt=cpt+1
                            endif
                         end if

                      end do
                   end do
                end do

             end if
             !----------------------------------------------------------------

             !----------------------------------------------------------------
             ! volume fraction
             !----------------------------------------------------------------
             cou0(i,j,k)=cpt/(ni*nj*nk)
             !----------------------------------------------------------------

          end do
       end do
    end do

    !----------------------------------------------------------------
    ! volume fraction
    !----------------------------------------------------------------
    cou=cou+cou0
    !----------------------------------------------------------------
    ! cut-off
    !----------------------------------------------------------------
    do k=sz,ez
       do j=sy,ey
          do i=sx,ex
             if (cou(i,j,k)>1) cou(i,j,k)=1
          end do
       end do
    end do
    !----------------------------------------------------------------

    deallocate(coub,cou0)
    call comm_mpi_sca(cou)

  end subroutine init_sphere

  subroutine init_plane_sca(a,b,c,d,cou,val)
    !-------------------------------------------------------------------------------
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    integer, intent(in)                                   :: val
    real(8), intent(in)                                   :: a,b,c,d
    real(8), dimension(:,:,:), allocatable, intent(inout) :: cou
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                               :: n=10
    integer                                               :: ni,nj,nk
    integer                                               :: i,j,k,k0
    integer                                               :: ii,jj,kk
    real(8)                                               :: cpt
    real(8)                                               :: x,y,z
    real(8)                                               :: deltax,deltay,deltaz
    integer, dimension(:,:,:), allocatable                :: coub
    real(8), dimension(:,:,:), allocatable                :: cou0
    !----------------------------------------------------------------

    !----------------------------------------------------------------
    ! init
    !----------------------------------------------------------------
    allocate(coub(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
    allocate(cou0(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
    cou0=0
    coub=0
    !----------------------------------------------------------------
    ni=n
    nj=n
    nk=n
    k0=1
    if (dim==2) k0=0
    !----------------------------------------------------------------


    !----------------------------------------------------------------
    ! binary fct
    !----------------------------------------------------------------
    do k=sz,ez
       do j=sy,ey
          do i=sx,ex

             if (dim==2) then
                if (a*grid_x(i)+b*grid_y(j)<=d) then
                   coub(i,j,k)=1
                endif
             else
                if (a*grid_x(i)+b*grid_y(j)+c*grid_z(k)<=d) then
                   coub(i,j,k)=1
                endif
             end if

          end do
       end do
    end do
    !----------------------------------------------------------------


    do k=sz,ez
       do j=sy,ey
          do i=sx,ex

             cpt=sum(coub(i-1:i+1,j-1:j+1,k-k0:k+k0))
             !----------------------------------------------------------------
             ! test if cou >0 or <1
             !----------------------------------------------------------------
             if (cpt>0.or.cpt<3**dim) then

                deltax=(grid_xu(i+1)-grid_xu(i))/ni
                deltay=(grid_yv(j+1)-grid_yv(j))/nj
                if (dim==3) deltaz=(grid_zw(k+1)-grid_zw(k))/nk

                cpt=0
                do kk=1,nk
                   do jj=1,nj
                      do ii=1,ni

                         x=grid_xu(i)+deltax/2+(ii-1)*deltax
                         y=grid_yv(j)+deltay/2+(jj-1)*deltay
                         if (dim==3) z=grid_zw(k)+deltaz/2+(kk-1)*deltaz

                         if (dim==2) then
                            if (a*x+b*y<=d) then
                               cpt=cpt+1
                            endif
                         else
                            if (a*x+b*y+c*z<=d) then
                               cpt=cpt+1
                            endif
                         end if

                      end do
                   end do
                end do

             end if
             !----------------------------------------------------------------

             !----------------------------------------------------------------
             ! volume fraction
             !----------------------------------------------------------------
             cou0(i,j,k)=val*cpt/(ni*nj*nk)
             !----------------------------------------------------------------

          end do
       end do
    end do

    !----------------------------------------------------------------
    ! volume fraction
    !----------------------------------------------------------------
    cou=cou+cou0
    !----------------------------------------------------------------
    ! cut-off
    !----------------------------------------------------------------
    do k=sz,ez
       do j=sy,ey
          do i=sx,ex
             if (cou(i,j,k)>1) cou(i,j,k)=1
             if (cou(i,j,k)<0) cou(i,j,k)=0
          end do
       end do
    end do
    !----------------------------------------------------------------

    deallocate(coub,cou0)

  end subroutine init_plane_sca

  subroutine init_circle_const(x0,y0,radius,cou,val,flag)
    !-------------------------------------------------------------------------------
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), intent(in)                                   :: val,x0,y0,radius
    real(8), dimension(:,:,:), allocatable, intent(inout) :: cou
    integer, intent(in)                                   :: flag
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                               :: i,j
    real(8) :: r
    !----------------------------------------------------------------
    cou = 0
    !----------------------------------------------------------------
    ! initialize the scalar field "cou" to 1 inside a circle of radius "radius", centered at x0, y0 
    !----------------------------------------------------------------
    do j=sy-gy,ey+gy
       do i=sx-gx,ex+gx
          r = sqrt((grid_x(i)-x0)**2 + (grid_y(j)-y0)**2)
          if (r < radius) then
             cou(i,j,:) = val
          else
             if (flag .eq. 1) then
             cou(i,j,:) = -val
             endif
          end if
       end do
    end do

    ! Mpi sync
    call comm_mpi_sca(cou)
    
  end subroutine init_circle_const

  subroutine init_wavy_cosine(cou,val,flag)
    !-------------------------------------------------------------------------------
    implicit none
    !-------------------------------------------------------------------------------
    ! Global variables
    !-------------------------------------------------------------------------------
    real(8), intent(in)                                   :: val
    real(8), dimension(:,:,:), allocatable, intent(inout) :: cou
    integer, intent(in)                                   :: flag
    !-------------------------------------------------------------------------------
    ! Local variables
    !-------------------------------------------------------------------------------
    integer                                               :: i,j,k
    real(8)                                               :: r,xloc,yloc
    real(8), dimension(:), allocatable                    :: s
    !----------------------------------------------------------------
    cou = 0
    allocate(s(sx:ex))
    k = 1

    do j = sy,ey
       do i = sx,ex
          s(i) = grid_x(i)/(xmax - xmin)
          yloc = (1.d0 + cos(2.d0*pi*s(i)))*0.25d0
          xloc = 1 - s(i)
          if  (grid_y(j) < yloc) then
             cou(i,j,k) = -val
          else
             if (flag .eq. 1) then
                cou(i,j,k) = val
             end if
          end if
       end do
    end do

    ! Mpi sync
    call comm_mpi_sca(cou)
    deallocate(s)
  end subroutine init_wavy_cosine

  function f1d(x,z,beta,delta)
    implicit none
    real(8), intent(in) :: x,z,beta,delta
    real(8)             :: f1d

    f1d=1d0/6*(exp(-beta*(x+delta-z)**2) &
         & +exp(-beta*(x-delta-z)**2)   &
         & +4*exp(-beta*(x-z)**2))
  end function f1d

  function g1d(x,alpha,delta)
    implicit none
    real(8), intent(in) :: x,alpha,delta
    real(8)             :: g1d

    g1d=1d0/6*(h1d(x,alpha,0.5d0-delta) &
         & +h1d(x,alpha,0.5d0+delta)   &
         & +4*h1d(x,alpha,0.5d0-delta))
  end function g1d

  function h1d(x,alpha,d)
    implicit none
    real(8), intent(in) :: x,alpha,d
    real(8)             :: h1d

    h1d=sqrt(max(1-alpha**2*(x-d)**2,0d0))
  end function h1d

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
end module mod_Init_EQ
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
