subroutine LU(A, b, x)
    real(8), dimension(3, 3), intent(inout) :: A
    real(8), dimension(3), intent(in) :: b
    real(8), dimension(3), intent(out) :: x
    real(8) :: L(3, 3), U(3, 3), y(3)
    integer :: i, j, k
    
    ! LU Decomposition
    do i = 1, 3
        U(i, i) = A(i, i)
        L(i, i) = 1.0
        do j = i + 1, 3
            L(j, i) = A(j, i) / U(i, i)
            U(i, j) = A(i, j)
        end do
        do j = i + 1, 3
            do k = i + 1, 3
                A(j, k) = A(j, k) - L(j, i) * U(i, k)
            end do
        end do
    end do
    
    ! Solve Ly = b using forward substitution
    do i = 1, 3
        y(i) = b(i)
        do j = 1, i - 1
            y(i) = y(i) - L(i, j) * y(j)
        end do
    end do
    
    ! Solve Ux = y using backward substitution
    do i = 3, 1, -1
        x(i) = y(i)
        do j = i + 1, 3
            x(i) = x(i) - U(i, j) * x(j)
        end do
        x(i) = x(i) / U(i, i)
    end do
end subroutine LU
