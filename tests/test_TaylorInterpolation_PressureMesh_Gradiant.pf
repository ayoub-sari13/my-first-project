!===============================================================================
module test_TaylorInterpolation_PressureMesh_Gradiant
  !===============================================================================
  !-------------------------------------------------------------------------------
  !Modules de subroutines de la librairie VOF-Lag
  !-------------------------------------------------------------------------------
  use Bib_VOFLag_TaylorInterpolation_PressureMesh_Gradiant
  use test_MPI_Parameters,                                 only : t_MPI_Parameters
  use test_Parameters,                                     only : t_Parameters,    &
                                                                  Polynome,d_polynome
  use pfunit
  !-------------------------------------------------------------------------------
  implicit none

  contains
  !-----------------------------------------------------------------------------
  !> @author Andelhamid Hakkoum 07/2022
  ! 
  !> @brief 
  !! 
  !
  !-----------------------------------------------------------------------------
  @mpitest(npes=[1])
  subroutine t_TaylorInterpolation_PressureMesh_Gradiant(this)
    !===============================================================================
      !modules
      !===============================================================================
      use, intrinsic :: iso_fortran_env, only : dp => real64
      !-------------------------------------------------------------------------------
      !Modules de definition des structures et variables
      !-------------------------------------------------------------------------------
      use Module_VOFLag_ParticlesDataStructure
      use mod_Parameters,                      only : dim,deeptracking,sx,ex, & 
                                                      sy,ey,sz,ez,gsxu,gx,gy,gz,sxu,exu,syu,&
                                                      eyu,szu,ezu,sxv,exv,syv,eyv,szv,ezv,  & 
                                                      sxw,exw,syw,eyw,szw,ezw
      !-------------------------------------------------------------------------------
      
      !===============================================================================
      !declarations des variables
      !===============================================================================
      !
      !-------------------------------------------------------------------------------
      class (MpiTestMethod), intent(inout)   :: this
      !-------------------------------------------------------------------------------
      real(8), parameter  :: PI = 3.14159265358979323846264338327950288419716939937510
      !-------------------------------------------------------------------------------
      !-------------------------------------------------------------------------------
      !variables globales
      !-------------------------------------------------------------------------------
      real(8), allocatable, dimension(:)       :: grid_xu,grid_yv, grid_zw
      real(8), allocatable, dimension(:)       :: grid_x,grid_y, grid_z
      real(8)                                  :: Xmin,Ymin,Zmin,Xmax,Ymax,Zmax
      integer                                  :: nx,ny,nz,ndim,meshprop
      logical                                  :: test_debug
      !-------------------------------------------------------------------------------
      !variables locales
      !-------------------------------------------------------------------------------
      real(8), allocatable, dimension(:,:,:)   :: pres
      real(8), allocatable, dimension(:,:,:)   :: ThirdDerivative
      real(8), allocatable, dimension(:,:)     :: coef
      real(8), allocatable, dimension(:,:)     :: Hess
      real(8), allocatable, dimension(:)       :: grad_num,grad_exact
      real(8), allocatable, dimension(:)       :: norm
      real(8), allocatable, dimension(:)       :: dx,dy,dz  
      real(8)                                  :: signe,rand_num,min_err,max_err
      integer                                  :: i,j,k,Order,itr
      !-------------------------------------------------------------------------------
      Order=2
      test_debug  =.false.
      deeptracking=.false.
      meshprop=0
      ndim=3
      Xmin=0d0;Xmax=2d0;
      Ymin=0d0;Ymax=2d0;
      if (ndim==3) Zmin=0d0;Zmax=2d0
      if ( meshprop == 0) then 
        nx=32
        ny=32
        if (ndim==3) nz=32
      elseif ( meshprop == 1) then 
        nx=16
        ny=20
        if (ndim==3) nz=24
      end if 
      call t_MPI_Parameters(nx,ny,nz,ndim,this%getMpiCommunicator())

      call t_Parameters(Xmin,Xmax,Ymin,Ymax,Zmin,Zmax,nx,ny,nz,ndim,meshprop,&
                         grid_xu,grid_yv,grid_zw,grid_x,grid_y,grid_z)

      allocate(pres(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
      allocate(norm(ndim),grad_num(ndim),grad_exact(ndim),Hess(ndim,ndim))
      allocate(ThirdDerivative(ndim,ndim,ndim))
      allocate(dx(sx-gx:ex+gx),dy(sy-gy:ey+gy))
      allocate(coef(3,Order+1))
      
      if (ndim==3) allocate(dz(sz-gz:ez+gz))
      do i=sx,ex
        dx(i)=grid_x(i)-grid_x(i-1)
      end do
      do j=sy,ey
        dy(j)=grid_y(j)-grid_y(j-1)
      end do
      if (ndim==3) then 
        do k=sz,ez
          dz(k)=grid_z(k)-grid_z(k-1)
        end do 
      end if
      signe=1d0
      min_err=1d40
      max_err=-1d40
      do Order=1,3
        print*, 'Order : ', Order
        do itr=1,1000
          call random_number(coef)
          call random_number(norm)
          signe=-1d0*signe
          norm=signe*norm
          do i=sx,ex
            do j=sy,ey
              do k=sz,ez
                 call Polynome(grid_x(i),grid_y(j),grid_z(k),Order,coef,pres(i,j,k))
              end do 
            end do 
          end do 
          call random_seed()
          call random_number(rand_num)
          i=int(rand_num*((nx-2*Order)-2*Order)+1)+2*Order
          call random_seed()
          call random_number(rand_num)
          j=int(rand_num*((ny-2*Order)-2*Order)+1)+2*Order
          call random_seed()
          call random_number(rand_num)
          k=int(rand_num*((nz-2*Order)-2*Order)+1)+2*Order
          call TaylorInterpolation_PressureMesh_Gradiant (pres,i,j,k,Order,-norm,grad_num,&
                                                          ndim,dx,dy,dz)

          call d_polynome(grid_x(i),grid_y(j),grid_z(k),Order,coef,grad_exact,Hess,ThirdDerivative)

          min_err=min(min_err,abs(grad_exact(1)-grad_num(1)),abs(grad_exact(2)-grad_num(2)),&
                      abs(grad_exact(3)-grad_num(3)))
          max_err=max(max_err,grad_exact(1)-grad_num(1),grad_exact(2)-grad_num(2),grad_exact(3)-grad_num(3))
          if (test_debug) then 
            print*, Order, 'Order'
            print*, i, j, k ,'i j k'
            print*, grid_x(i), grid_y(j),grid_z(k), 'grid_x', ' grid_y', ' grid_z'
            print*, grad_exact(1),grad_num(1), 'grad_exact', ' grad_num'
            print*, grad_exact(2),grad_num(2), 'grad_exact', ' grad_num'
            print*, grad_exact(3),grad_num(3), 'grad_exact', ' grad_num'
            print*, grad_exact(1)-grad_num(1)
            print*, grad_exact(2)-grad_num(2)
            print*, grad_exact(3)-grad_num(3)
          end if 
          @assertEqual(grad_exact,grad_num, 1.0e-11_dp)
        end do 
        print*, 'Min err: ', min_err
        print*, 'Max err: ', max_err
      end do 
      
  end subroutine t_TaylorInterpolation_PressureMesh_Gradiant

  !===============================================================================
end module test_TaylorInterpolation_PressureMesh_Gradiant
!===============================================================================