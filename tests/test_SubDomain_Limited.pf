!===============================================================================
module test_SubDomain_Limited
  use Bib_VOFLag_SubDomain_Limited
  use test_MPI_Parameters,         only : t_MPI_Parameters
  use test_Parameters,             only : t_Parameters
  use pfunit
  use mpi 
  
  !===============================================================================
  contains
  !> @author Andelhamid Hakkoum 11/2022
  ! 
  !> @brief test la routine qui calcul les coordonnees des sous-domaines
  !         en vérifions (par exemple que la coord x du min d'un sous-domaine est 
  !         égale à la coordle max du sous-domaine qui est à son gauche)
  !         en 3D et pour deux types de maillage regulier et expo    
  !
  !> @todo faire le test pour les deux types de maillages en meme temps
  !    
  !ligne 442 MPI.f90 Deux if paraille
  !-----------------------------------------------------------------------------
  @test(npes=[1])
  subroutine t_SubDomain_Limited(this)
    !===============================================================================
    !modules
    !===============================================================================
      use, intrinsic :: iso_fortran_env, only : dp => real64
      
      use Module_VOFLag_SubDomain_Data,  only : m_dims,m_coords
      use mod_Parameters,                only : deeptracking,sx, ex, sy, ey, sz, ez,&
                                                rank,nproc
      use mod_mpi,                       only : mpi_code, coords
      !-------------------------------------------------------------------------------
      !Modules de definition des structures et variables
      !------------------------------------------------------------------------------- 
      !-------------------------------------------------------------------------------
      !===============================================================================
      !declarations des variables
      !===============================================================================
      !implicit none
      !-------------------------------------------------------------------------------
      !variables globales
      !-------------------------------------------------------------------------------
      class (MpiTestMethod), intent(inout) :: this
      !-------------------------------------------------------------------------------
      !variables locales 
      !-------------------------------------------------------------------------------
      real(8), allocatable, dimension(:,:,:) :: s_domaines_int_proc
      real(8),              dimension(2,3)   :: s_domaines_int
      real(8), allocatable, dimension(:)     :: grid_x,grid_y, grid_z
      real(8), allocatable, dimension(:)     :: grid_xu,grid_yv, grid_zw
      real(8), allocatable, dimension(:)     :: dx, dy, dz
      real(8)                                :: Xmin,Ymin,Zmin,Xmax,Ymax,Zmax
      integer                                :: i
      !-------------------------------------------------------------------------------
      deeptracking = .false.
      
      meshprop=1
      ndim=3
      Xmin=0;Xmax=1d0;Ymin=0;Ymax=1d0;Zmin=0;Zmax=1d0

      if ( meshprop == 0)     then 

         nx=32;ny=64;nz=128

      elseif ( meshprop == 1) then 

         nx=16;ny=20;nz=24 ! il faut qu'il soit multiple de 4

      end if 

      call t_MPI_Parameters(nx,ny,nz,ndim,this%getMpiCommunicator())
      allocate(s_domaines_int_proc(0:nproc-1,2,3))
      m_coords = coords

      call t_Parameters(Xmin,Xmax,Ymin,Ymax,Zmin,Zmax,nx,ny,nz,ndim,meshprop,&
                         grid_xu,grid_yv, grid_zw,grid_x,grid_y, grid_z)

      
      s_domaines_int_proc = 0d0
      s_domaines_int = 0d0
      call SubDomain_Limited(s_domaines_int,grid_x,grid_y,grid_z)
      call Mpi_barrier(this%getMpiCommunicator(), mpi_code)

      s_domaines_int_proc(rank,:,:) = s_domaines_int

      
      do k=0, nproc-1
         call mpi_bcast(s_domaines_int_proc(k,:,:),6,mpi_double_precision,k,&
                        this%getMpiCommunicator(),mpi_code)
      end do

      if ( m_coords(1) < m_dims(1)-1 ) then 
         @assertEqual(s_domaines_int_proc(rank,2,1),s_domaines_int_proc(rank+m_dims(2),1,1), 1.0e-15_dp)
      end if
      if ( m_coords(2) < m_dims(2)-1 ) then 
         @assertEqual(s_domaines_int_proc(rank,2,2),s_domaines_int_proc(rank+1,1,2), 1.0e-15_dp)
      end if

      if ( m_coords(3) < m_dims(3)-1 ) then 
         @assertEqual(s_domaines_int_proc(rank,2,3),s_domaines_int_proc(rank+m_dims(1)*m_dims(2),1,3), 1.0e-15_dp)
      end if

  end subroutine t_SubDomain_Limited

end module test_SubDomain_Limited
