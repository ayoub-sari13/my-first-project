import matplotlib.pyplot as plt
import numpy as np
from math import *
import os,sys


def Affiche_plot(direction,num):
    if direction == 'p':
        d = 1
    elif direction == 'vis':
        d = 3
    elif direction == 'vts':
        d = 6
    else:
        print(f"Invalid direction argument: {direction}")
        return
    
    filename = 'AR_1_d_40_tpf__5/Phase_function_err_tir.dat'  
    #filename = 'AR_tpf/ARS40/Phase_function_err_fortpf.dat'  
    
    if not os.path.isfile(filename):
        print("Error: File does not exist.")
        return
    
    print(f"Reading data from {filename}")
    data = np.loadtxt(filename)
    
    # Split data into columns
    #itr = data[:, 0]
    nb_tir = data[:, 0]
    err = data[:, d]
    
    if num != 0:
       err= np.column_stack((data[:, d], data[:, d+1], data[:, d+4]))
       
    #vts = data[:, d + 3]
    
    # Plot the data
    fig, ax = plt.subplots()
    #ax.plot(nb_tir,, color='blue', label='Data')
    ax.plot(nb_tir, err)
    ax.set_title('Case: AR=1, D/16, tpf=5')
    ax.set_xlabel('iteration')
    ax.set_ylabel('Relative Err [%]')
    plt.legend(['p', 'vis', 'vts'])
    plt.show()
    #print(err[:100,:])
    
    # Open the output file and write the column
    #with open("output.txt", "w") as outfile:
        # Write header row
        #outfile.write("-------itr-------   -------err------- \n")
        
        # Write data rows
        #for t, v in zip(nb_tir, err):
            #outfile.write("{:+.14f} {:+.14f}\n".format(t, v))
    
    #return 0

def Affiche_2plot(direction, option):
    if direction == 'p':
        d = 1
    elif direction == 'vis':
        d = 3
    elif direction == 'vts':
        d = 6
    else:
        print(f"Invalid direction argument: {direction}")
        return
    
    
    filename1 = 'AR_1_d_16_tpf_20/Phase_function2_err.dat'
    filename2 = 'AR_S_d_40_tpf_20/Phase_function3_err.dat'
    filename3 = 'AR_1_d_16_tpf_20/Phase_function2_err.dat'
    filename4 = 'AR_S_d_16_tpf_20/Phase_function3_err.dat'
      
    filenames = [filename1, filename2, filename3, filename4]
    
    for filename in filenames:
      if not os.path.isfile(filename):
          print("Error: File does not exist.")
          return
    
    print(f"Reading data from {filename1}")
    data1 = np.loadtxt(filename1)
    nb_tir = data1[:, 0]
    err1 = data1[:, d]
    print(f"Reading data from {filename2}")
    data2 = np.loadtxt(filename2)    
    err2 = data2[:, d]
    print(f"Reading data from {filename3}")
    data3 = np.loadtxt(filename3)    
    err3 = data3[:, d]
    print(f"Reading data from {filename4}")
    data4 = np.loadtxt(filename4)    
    err4 = data4[:, d]

    if option == 'maille':
                                   
      fig= plt.figure()
      ax1 = fig.add_subplot(1, 2, 1)
      ax1.plot(nb_tir, err1, label='AR=1')
      ax1.plot(nb_tir, err2, label='Sphere')
      ax1.set_title('Case: D/40 ')
      ax1.set_xlabel('iteration')
      ax1.set_ylabel('Relative Err [%]')
      
      ax2 = fig.add_subplot(1, 2, 2)
      ax2.plot(nb_tir, err3, label=' AR=1')
      ax2.plot(nb_tir, err4, label=' Sphere')
      ax2.set_title('Case: D/16 ')
      ax2.set_xlabel('iteration')
      ax2.set_ylabel('Relative Err [%]')
          
      plt.legend()
      fig.tight_layout()
      plt.show()
      
    if option == 'tpf':
    
#      filename1 = 'AR_1_d_40_tpf_20/Phase_function1_err_20.dat'
#      filename2 = 'AR_1_d_16_tpf_20/Phase_function1_err_20.dat'
#      filenames = [filename1, filename2]
    
#      for filename in filenames:
#        if not os.path.isfile(filename):
#            print("Error: File does not exist.")
#            return
    
#      print(f"Reading data from {filename1}")
#      data1 = np.loadtxt(filename1)
      #nb_tir = data1[:, 1]
#      nb1 = data1[:, 1]
#      err1 = data1[:, d]
#      print(f"Reading data from {filename2}")
#      nb2 = data1[:, 1]
#      data2 = np.loadtxt(filename2)   
#      err2 = data2[:, d]
    
    # Plot the data

      fig= plt.figure()
      ax1 = fig.add_subplot(1, 2, 1)
      ax1.semilogy(nb_tir, err1, label='40/D')
      ax1.semilogy(nb_tir, err3, label='16/D')
      ax1.set_title('Case: AR=2, tpf=20 ')
      ax1.set_xlabel('iteration')
      ax1.set_ylabel('Relative Err [%]')
      
      ax2 = fig.add_subplot(1, 2, 2)
      ax2.semilogy(nb_tir, err2, label='40/D')
      ax2.semilogy(nb_tir, err4, label='16/D')
      ax2.set_title('Case: AR=2, tpf=5 ')
      ax2.set_xlabel('iteration')
      ax2.set_ylabel('Relative Err [%]')
          
      plt.legend()
      fig.tight_layout()
      plt.show()
      
def Affiche_nplot(direction):
    if direction == 'p':
        d = 1
    elif direction == 'vis':
        d = 3
    elif direction == 'vts':
        d = 6
    else:
        print(f"Invalid direction argument: {direction}")
        return
    
    filename1 = 'AR_1_d_40_tpf_5/Phase_function_err_tir.dat'  
    filename2 = 'AR_2_d_40_tpf_5/Phase_function_err_tir.dat'  
    filename3 = 'AR_3_d_40_tpf_5/Phase_function_err_tir.dat'  
    filename4 = 'AR_4_d_40_tpf_5/Phase_function_err_tir.dat'  
    filename5 = 'AR_5_d_40_tpf_5/Phase_function_err_tir.dat'  
    filename6 = 'AR_6_d_40_tpf_5/Phase_function_err_tir.dat'  
    
#    filename1 = 'AR_1_d_40_tpf_5/Phase_function_err_tir.dat'
#    filename2 = 'AR_2i_d_40_tpf_5/Phase_function_err_tir.dat'
#    filename3 = 'AR_3i_d_40_tpf_5/Phase_function_err_tir.dat'
#    filename4 = 'AR_4i_d_40_tpf_5/Phase_function_err_tir.dat'
#    filename5 = 'AR_5i_d_40_tpf_5/Phase_function_err_tir.dat'
 #   filename6 = 'AR_6i_d_40_tpf_5/Phase_function_err_tir.dat'
      
    filenames = [filename1, filename2, filename3, filename4, filename5, filename6]
    
    for filename in filenames:
      if not os.path.isfile(filename):
          print("Error: File does not exist.")
          return
    
    print(f"Reading data from {filename1}")
    data1 = np.loadtxt(filename1)
    nb_tir = data1[:, 0]
    err1 = data1[:, d]
    print(f"Reading data from {filename2}")
    data2 = np.loadtxt(filename2)    
    err2 = data2[:, d]
    print(f"Reading data from {filename3}")
    data3 = np.loadtxt(filename3)    
    err3 = data3[:, d]
    print(f"Reading data from {filename4}")
    data4 = np.loadtxt(filename4)    
    err4 = data4[:, d]
    print(f"Reading data from {filename5}")
    data5 = np.loadtxt(filename5)    
    err5 = data5[:, d]
    print(f"Reading data from {filename6}")
    data6 = np.loadtxt(filename6)    
    err6 = data6[:, d]

    fig, ax = plt.subplots()
    ax.plot(nb_tir, err1, label='AR=1')
    ax.plot(nb_tir, err2, label='AR=2')    
    ax.plot(nb_tir, err3, label='AR=3')
    ax.plot(nb_tir, err4, label='AR=4')
    ax.plot(nb_tir, err5, label='AR=5')    
    ax.plot(nb_tir, err6, label='AR=6')

#    ax.plot(nb_tir, err2, label='AR=1/2')
#    ax.plot(nb_tir, err3, label='AR=1/3')
#    ax.plot(nb_tir, err4, label='AR=1/4')
#    ax.plot(nb_tir, err5, label='AR=1/5')
#    ax.plot(nb_tir, err6, label='AR=1/6')

    ax.set_title('Case: Ellipsoid, D/40, tpf=5, Pressure mesh',fontsize=16)
    ax.set_xlabel('number shooting',fontsize=14)
    ax.set_ylabel('Relative Err [%]',fontsize=14)
    #plt.legend(['p', 'vis', 'vts'])
    plt.legend()
    ax.set_ylim([0,0.005])
    plt.show()
    

# Program
direction = 'p'
option='maille'

#Affiche_plot(direction, 0)
#Affiche_2plot(direction,option)
Affiche_nplot(direction)
