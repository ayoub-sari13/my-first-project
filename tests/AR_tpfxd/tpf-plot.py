import matplotlib.pyplot as plt
import numpy as np
import os,sys

def Convergence_plot(order):
 
   # filename = 'd_6i_d_40_tpf_20/Phase_function2_err.dat'  
    filename = 'd16/Phase_function_err_inter1per1.dat'  
    
    if not os.path.isfile(filename):
        print("Error: File does not exist.")
        return
    
    print(f"Reading data from {filename}")
    data = np.loadtxt(filename)
    
    # Split data into columns
    #itr = data[:, 0]
    nb_tpf = data[1:, 0]
    err = data[1:, order]
    
    fig, axes= plt.subplots()
    
    
    axes.plot(nb_tpf,err,'-o',label='n/D= 16')
    axes.set_xlabel('tpf')
    axes.set_ylabel('relative error')
    axes.set_title('convergence study on number of fictif points (D/40)')
    axes.set_ylim(0,0.01)
    axes.legend()
    plt.show()

def Convergence_nplot(d,split):
 
    filename1 = 'd16/Phase_function_err_inter1per1.dat'  
    filename2 = 'd24/Phase_function_err_inter1per1.dat'  
    filename3 = 'd32/Phase_function_err_inter1per1.dat'  
    filename4 = 'd40/Phase_function_err_inter1per1.dat'  
    filename5 = 'd52/Phase_function_err_inter1per1.dat'  
    filename6 = 'd64/Phase_function_err_inter1per1.dat'  
    filename7 = 'd80/Phase_function_err_inter.dat'  
    
#    filename1 = 'd140/Phase_function_err_inter1per1.dat'  
#    filename7 = 'd2i40/Phase_function_err_inter1per1.dat'  
#    filename8 = 'd3i40/Phase_function_err_inter1per1.dat'  
#    filename9 = 'd4i40/Phase_function_err_inter1per1.dat'  
#    filename10 = 'd5i40/Phase_function_err_inter1per1.dat'  
#    filename11 = 'd6i40/Phase_function_err_inter1per1.dat'  
      
    filenames = [filename1, filename2, filename3, filename4, filename5, filename6, filename7]
 #    ,filename7, filename8, filename9, filename10, filename11]
    
    for filename in filenames:
      if not os.path.isfile(filename):
          print("Error: File does not exist.")
          return
    
    print(f"Reading data from {filename1}")
    data1 = np.loadtxt(filename1)
    nb_tpf = data1[:, 0]
    err1 = data1[:, d]
    print(f"Reading data from {filename2}")
    data2 = np.loadtxt(filename2)    
    err2 = data2[:, d]
    print(f"Reading data from {filename3}")
    data3 = np.loadtxt(filename3)    
    err3 = data3[:, d]
    print(f"Reading data from {filename4}")
    data4 = np.loadtxt(filename4)    
    nb_tpf4 = data4[:, 0]        
    err4 = data4[:, d]
    print(f"Reading data from {filename5}")
    data5 = np.loadtxt(filename5)    
    nb_tpf5 = data5[:, 0]            
    err5 = data5[:, d]
    print(f"Reading data from {filename6}")
    data6 = np.loadtxt(filename6)
    nb_tpf6 = data6[:28, 0]        
    err6 = data6[:28, d]
    print(f"Reading data from {filename7}")
    data77 = np.loadtxt(filename7)
    nb_tpf77 = data77[:, 0]        
    err77 = data77[:, d]

#    print(f"Reading data from {filename2}")
    data7 = np.loadtxt(filename1)    
    err7 = data7[:, 2]
#    print(f"Reading data from {filename3}")
    data8 = np.loadtxt(filename2)    
    err8 = data8[:, 2]
#    print(f"Reading data from {filename4}")
    data9 = np.loadtxt(filename3)    
    err9 = data9[:, 2]
#    print(f"Reading data from {filename5}")
    data10 = np.loadtxt(filename4)    
    err10 = data10[:, 2]
#    print(f"Reading data from {filename6}")
    data11 = np.loadtxt(filename5)    
    err11 = data11[:, 2]
#
    data12 = np.loadtxt(filename6)    
    err12 = data12[:28, 2]
#    
    data13 = np.loadtxt(filename7)    
    err13 = data13[:, 2]    
    
    if split==True :
    	fig, ax = plt.subplots(1,2)
    	ax[0].plot(nb_tpf, err1, '-o', label='n=16')
    	ax[0].plot(nb_tpf, err2, '-o', label='n=24')
    	ax[0].plot(nb_tpf, err3, '-o', label='n=32')
    	ax[0].plot(nb_tpf, err4, '-o', label='n=40')
    	ax[0].plot(nb_tpf, err5, '-o', label='n=52')
    	ax[0].plot(nb_tpf6, err6, '-o', label='n=64')
    	ax[0].plot(nb_tpf77, err77, '-o', label='n=80')	
    	ax[0].set_xlabel('tpf', fontsize=14)
    	ax[0].set_ylabel('relative error',fontsize=14)
    	ax[0].set_title('Convergence study on n/D full Volume',fontsize=16)
    	ax[0].set_ylim(0,0.005)
    	
	#ax[1].legend()
    	ax[1].plot(nb_tpf, err7, '-o', label='n=16')
    	ax[1].plot(nb_tpf, err8, '-o', label='n=24')
    	ax[1].plot(nb_tpf, err9, '-o', label='n=32')
    	ax[1].plot(nb_tpf, err10, '-o', label='n=40')
    	ax[1].plot(nb_tpf, err11, '-o', label='n=52')
    	ax[1].plot(nb_tpf6, err12, '-o', label='n=64')
    	ax[1].plot(nb_tpf77, err13, '-o', label='n=80')
    	ax[1].set_xlabel('tpf', fontsize=14)
    	ax[1].set_ylabel('relative error',fontsize=14)
    	ax[1].set_title('Convergence study on n/D Shared Volume',fontsize=16)
    	ax[1].set_ylim(0,0.01)
    	ax[1].legend()
    	fig.subplots_adjust(wspace=0.25)
    	plt.show()
	
    else:	
    	fig, ax = plt.subplots()
    	ax.plot(nb_tpf, err1, '-o', label='d=1')
    	ax.plot(nb_tpf, err2, '-o', label='d=2')
    	ax.plot(nb_tpf, err3, '-o', label='d=3')
    	ax.plot(nb_tpf, err4, '-o', label='d=4')
    	ax.plot(nb_tpf, err5, '-o', label='d=5')
    	ax.plot(nb_tpf, err6, '-o', label='d=6')
#    	ax.plot(nb_tpf, err7, '-o', label='d=1/2')
#    	ax.plot(nb_tpf, err8, '-o', label='d=1/3')
#    	ax.plot(nb_tpf, err9, '-o', label='d=1/4')
#    	ax.plot(nb_tpf, err10, '-o', label='d=1/5')
#    	ax.plot(nb_tpf, err11, '-o', label='d=1/6')
    	ax.set_xlabel('tpf', fontsize=14)
    	ax.set_ylabel('relative error', fontsize=14)
#    ax[1].set_title('convergence study on number of fictif points (D/40)')
    	ax.legend()
    	plt.show()

####    
#Convergence_plot(1)
Convergence_nplot(1,True)
