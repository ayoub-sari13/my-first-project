!===============================================================================
module test_Particle_PhaseFunction_PressureMesh_tpf
      !===============================================================================
      use Bib_VOFLag_Particle_PhaseFunction_PressureMesh
      use Bib_VOFLag_Particle_PhaseFunction_VelocityMesh
      use Bib_VOFLag_Particle_PhaseFunction_ViscousMesh
      use Bib_VOFLag_Wall_PhaseFunction
      use test_Particle_Initialisation,                  only : t_Particle_Initialisation
      use test_MPI_Parameters,                           only : t_MPI_Parameters
      use test_Parameters,                               only : t_Parameters
      use pfunit
      use mpi 
      implicit none

      contains
      !> @author Andelhamid Hakkoum 11/2022
      ! 
      !> @brief 
      !        
      !        
      !
      !
      !> @todo
      !    
      !
      !-----------------------------------------------------------------------------
      @mpitest(npes=[1])
      subroutine t_Particle_PhaseFunction_PressureMesh_tpf(this)
      !===============================================================================
      !modules
      !===============================================================================
      !-------------------------------------------------------------------------------
      !Modules de definition des structures et variables
      !-------------------------------------------------------------------------------
      use Module_VOFLag_ParticlesDataStructure
      use Module_VOFLag_SubDomain_Data
      use mod_Parameters,                      only : rank,nproc,dim,deeptracking,sx,ex, & 
                                                      sy,ey,sz,ez,gsxu,gx,gy,gz,sxu,exu,syu,&
                                                      eyu,szu,ezu,sxv,exv,syv,eyv,szv,ezv,  & 
                                                      sxw,exw,syw,eyw,szw,ezw
      use mod_mpi,                             only : mpi_code,comm3d, coords
      !-------------------------------------------------------------------------------
      
      !===============================================================================
      !declarations des variables
      !===============================================================================
      !implicit none
      !-------------------------------------------------------------------------------
      !variables globales
      !-------------------------------------------------------------------------------
      class (MpiTestMethod), intent(inout)   :: this
      !-------------------------------------------------------------------------------
      !variables locales 
      !-------------------------------------------------------------------------------
      type(struct_vof_lag)                   :: vl
      !-------------------------------------------------------------------------------
      real(8), PARAMETER  :: PI = 3.14159265358979323846264338327950288419716939937510
      !-------------------------------------------------------------------------------
      real(8), allocatable, dimension(:,:,:,:) :: cou_vis,cou_vts,cou_visin,cou_vtsin
      real(8), allocatable, dimension(:,:,:)   :: cou,couin0
      real(8), allocatable, dimension(:,:)     :: Erreur_relative
      real(8), allocatable, dimension(:)       :: grid_xu,grid_yv, grid_zw
      real(8), allocatable, dimension(:)       :: grid_x,grid_y, grid_z
      real(8),              dimension(2)       :: moyenne_stat
      real(8),              dimension(3)       :: Pos, coin1, coin2
      real(8)                                  :: Xmin,Ymin,Zmin,Xmax,Ymax,Zmax
      real(8)                                  :: sum,sum0,sum_s, sum_s0, sum_inter, sum_inter0
      integer                                  :: nx,ny,nz,tpf,itr,itrr,nb_tir,nbt,CelPerDia
      integer                                  :: ndim,a,np,nb,i,j,k,b,nb_total,meshprop
      logical                                  :: test_debug
      character(len=200) :: filename,filename1
      !-------------------------------------------------------------------------------
      test_debug = .false.
      deeptracking = .false.

      WallPos(1)=0.67187500000000000!0.5d0
      WallPos(2)=0d0
      WallPos(3)=0d0
      meshprop=0
      ndim=3
      Xmin=-2d0;Xmax=2d0;Ymin=-2d0;Ymax=2d0;Zmin=-2d0;Zmax=2d0
      nb_tir= 3000
      if ( meshprop == 0)     then 
         !nx=64;ny=64;nz=64
         !nx=160;ny=160;nz=160	
	 nx=320;ny=320;nz=320
      elseif ( meshprop == 1) then 

         nx=16;ny=20;nz=24 ! il faut qu'il soit multiple de 4

      end if 

      call t_MPI_Parameters(nx,ny,nz,ndim,this%getMpiCommunicator())
      m_coords = coords

      call t_Parameters(Xmin,Xmax,Ymin,Ymax,Zmin,Zmax,nx,ny,nz,ndim,meshprop,&
                         grid_xu,grid_yv, grid_zw,grid_x,grid_y, grid_z)

      call t_Particle_Initialisation(vl,1,[1],1,ndim)
      allocate(cou(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
      allocate(cou_vis(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz,1:3))
      allocate(cou_vts(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz,1:3))
      allocate(Erreur_relative(1:nb_tir,1:7))

      coin1(1)= grid_xu(exu/2); coin1(2)= grid_yv(eyv/2); coin1(3)= grid_zw(ezw/2)
      coin2(1)= grid_xu(exu/2+1); coin2(2)= grid_yv(eyv/2+1); coin2(3)= grid_zw(ezw/2+1)
      CelPerDia=(int(Xmax)-int(Xmin))*16
      
      
      if (rank .eq. 0) then
        close(1000)
        write(filename,'(A,I0,A)') 'Phase_function_err_tpfformap.dat'
        open(unit=1000,file=filename,status='replace')
         write(1000,'(a1,8a16)',advance='no')'#', ' ----tpf---- ',&
                                                   ' Err Press  ', &
                                                   ' Err Press inter '
         close(1000)
      endif
      do tpf=0,30,2

         moyenne_stat=0d0
         do itr=1,nb_tir
            call random_number(Pos)
            vl%objet(1)%symper(1,:) = coin1 + (coin2-coin1)*Pos
	    !print*, 'itr =', itr

            !print*, grid_xu!, grid_y, grid_z
            !call Wall_PhaseFunction(cou,cou_vis,cou_vts,grid_x,grid_y,grid_z,&
            !                     grid_xu,grid_yv,grid_zw,ndim)
            do np=1, vl%kpt
               call Particle_PhaseFunction_PressureMesh(cou,couin0,grid_xu,grid_yv,grid_zw,ndim,tpf,vl)
               !call Particle_PhaseFunction_ViscousMesh(cou_vis,cou_visin,grid_x,grid_y,grid_z,grid_xu, &
               !                                           grid_yv,grid_zw,ndim,tpf,vl)
               !call Particle_PhaseFunction_VelocityMesh(cou_vts,cou_vtsin,grid_x,grid_y,grid_z,grid_xu,&
               !                                            grid_yv,grid_zw,ndim,tpf,vl)
            end do
            !print*, maxval(cou), '(macval cou)'
            sum=0d0; sum_inter=0d0; sum_s=0d0
            nb =0 
            do i=sx,ex 
               do j=sy,ey
                  do k=sz,ez
                      sum=sum+cou(i,j,k)*(grid_xu(i+1)-grid_xu(i))*(grid_yv(j+1)-grid_yv(j))*&
                                          (grid_zw(k+1)-grid_zw(k))
					  
                      if ( cou(i,j,k) /= 0d0 .and. cou(i,j,k) /= 1d0) then 
                         sum_inter = sum_inter + cou(i,j,k)*(grid_xu(i+1)-grid_xu(i))*(grid_yv(j+1)-grid_yv(j))*&
                                          (grid_zw(k+1)-grid_zw(k))
                      else 
                        sum_s=sum_s+cou(i,j,k)*(grid_xu(i+1)-grid_xu(i))*(grid_yv(j+1)-grid_yv(j))*&
                                          (grid_zw(k+1)-grid_zw(k))
                      end if 
                  end do
               end do  
            end do 


            call MPI_BARRIER(comm3d, mpi_code)
            call MPI_BARRIER(this%getMpiCommunicator(), mpi_code)
            call MPI_REDUCE(sum    ,sum0    ,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,comm3d,mpi_code) 
            call MPI_REDUCE(sum_inter,sum_inter0,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,comm3d,mpi_code) 
            call MPI_REDUCE(sum_s,sum_s0,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,comm3d,mpi_code) 	    
            call MPI_REDUCE(nb     ,nb_total,1,MPI_INTEGER         ,MPI_SUM,0,comm3d,mpi_code) 
            call MPI_BARRIER(comm3d, mpi_code)
            call MPI_BARRIER(this%getMpiCommunicator(), mpi_code)

            !Erreur_relative(itr,1)=(WallPos(1)*Ymax*Zmax-sum0)/((WallPos(1)*Ymax*Zmax))
            !if ( rank == 0) then 
            !   print*, sum0
            !   print*, WallPos(1)*Ymax*Zmax
            !   print*, Erreur_relative(itr,1)*100
            !end if 

	    
            Erreur_relative(itr,1)=100*((4d0/3d0*PI*vl%objet(1)%sca(1)**3)-sum0)/((4d0/3d0*PI*vl%objet(1)%sca(1)**3))
            Erreur_relative(itr,2)=100*((4d0/3d0*PI*vl%objet(1)%sca(1)**3- sum_s0)-sum_inter0)/&
	    (4d0/3d0*PI*vl%objet(1)%sca(1)**3- sum_s0)


	 do nbt=1,2
             moyenne_stat(nbt) = moyenne_stat(nbt) + abs(Erreur_relative(itr,nbt))
         end do 
         end do

         if (rank .eq. 0) then
               open(unit=1000,file=filename,status='old',position='append')
               write(1000,'(8E16.8)',advance='no') dfloat(tpf),&
                                                   moyenne_stat(1)/nb_tir,&
                                                   moyenne_stat(2)/nb_tir
               close(1000)
         endif 

       print*, 'nb_tir=', nb_tir,'AR= S ','Maille/D',nx/4, 'tpf= ', tpf
	 
      end do
      
      
   end subroutine t_Particle_PhaseFunction_PressureMesh_tpf
        
end module test_Particle_PhaseFunction_PressureMesh_tpf
!===============================================================================
