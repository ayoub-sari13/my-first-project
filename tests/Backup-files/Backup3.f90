!===============================================================================
module test_Ellipsoid_PhaseFunction_PressureMesh
      !===============================================================================
      use Bib_VOFLag_Particle_PhaseFunction_PressureMesh
      use Bib_VOFLag_Particle_PhaseFunction_VelocityMesh
      use Bib_VOFLag_Particle_PhaseFunction_ViscousMesh
      use Bib_VOFLag_Wall_PhaseFunction
      use test_Particle_Initialisation,                  only : t_Particle_Initialisation
      use test_MPI_Parameters,                           only : t_MPI_Parameters
      use test_Parameters,                               only : t_Parameters
      use pfunit
      use mpi 
      implicit none

      contains
      !> @author Andelhamid Hakkoum 11/2022
      ! 
      !> @brief 
      !        
      !        
      !
      !
      !> @todo
      !    
      !
      !-----------------------------------------------------------------------------
      @mpitest(npes=[1])
      subroutine t_Ellipsoid_PhaseFunction_PressureMesh(this)
      !===============================================================================
      !modules
      !===============================================================================
      !-------------------------------------------------------------------------------
      !Modules de definition des structures et variables
      !-------------------------------------------------------------------------------
      use Module_VOFLag_ParticlesDataStructure
      use Module_VOFLag_SubDomain_Data
      use mod_Parameters,                      only : rank,nproc,dim,deeptracking,sx,ex, & 
                                                      sy,ey,sz,ez,gsxu,gx,gy,gz,sxu,exu,syu,&
                                                      eyu,szu,ezu,sxv,exv,syv,eyv,szv,ezv,  & 
                                                      sxw,exw,syw,eyw,szw,ezw
      use mod_mpi,                             only : mpi_code,comm3d, coords
      !-------------------------------------------------------------------------------
      
      !===============================================================================
      !declarations des variables
      !===============================================================================
      !implicit none
      !-------------------------------------------------------------------------------
      !variables globales
      !-------------------------------------------------------------------------------
      class (MpiTestMethod), intent(inout)   :: this
      !-------------------------------------------------------------------------------
      !variables locales 
      !-------------------------------------------------------------------------------
      type(struct_vof_lag)                   :: vl
      !-------------------------------------------------------------------------------
      real(8), PARAMETER  :: PI = acos(-1.d0)
      !-------------------------------------------------------------------------------
      real(8), allocatable, dimension(:,:,:,:) :: cou_vis,cou_vts,cou_visin,cou_vtsin
      real(8), allocatable, dimension(:,:,:)   :: cou,couin0
      real(8), allocatable, dimension(:,:)     :: Erreur_relative
      real(8), allocatable, dimension(:)       :: grid_xu,grid_yv, grid_zw
      real(8), allocatable, dimension(:)       :: grid_x,grid_y, grid_z
      real(8),              dimension(7)       :: moyenne_stat
      real(8),              dimension(3)       :: Pos, direction, coin1, coin2,Delta
      real(8)                                  :: Xmin,Ymin,Zmin,Xmax,Ymax,Zmax
      real(8)                                  :: sum,sum0,sumvisx,sumvisx0,sumvisy, &
                                                  sumvisy0,sumvisz,sumvisz0,sumvtsu, & 
                                                  sumvtsu0,sumvtsv,sumvtsv0,sumvtsw, &
                                                  sumvtsw0, exact_vol, angle, ang, AR
      integer                                  :: nx,ny,nz,tpf,itr,nb_tir,nbt,CelPerDia,&
                                                  nb_ang, itrr
      integer                                  :: ndim,a,np,nb,i,j,k,b,nb_total,meshprop
      logical                                  :: test_debug
      character(len=200) :: filename,filename1
      character(len=20)  :: nat_ellipsoid
      !-------------------------------------------------------------------------------
      test_debug = .false.
      deeptracking = .false.

      meshprop=0
      ndim=3
      Xmin=-2d0;Xmax=2d0;Ymin=-2d0;Ymax=2d0;Zmin=-2d0;Zmax=2d0
      nb_tir= 2000  
      AR= 1d0
      
      if ( meshprop == 0)     then 
       nx=64;ny=64;nz=64
       !nx=160;ny=160;nz=160
      elseif ( meshprop == 1) then 

         nx=16;ny=20;nz=24 ! il faut qu'il soit multiple de 4

      end if 

      call t_MPI_Parameters(nx,ny,nz,ndim,this%getMpiCommunicator())
      m_coords = coords

      call t_Parameters(Xmin,Xmax,Ymin,Ymax,Zmin,Zmax,nx,ny,nz,ndim,meshprop,&
                         grid_xu,grid_yv, grid_zw,grid_x,grid_y, grid_z)

      call t_Particle_Initialisation(vl,1,[1],2,ndim)

      allocate(cou(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
      allocate(cou_vis(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz,1:3))
      allocate(cou_vts(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz,1:3))
      allocate(Erreur_relative(1:nb_tir,1:7))

      coin1(1)= grid_xu(exu/2); coin1(2)= grid_yv(eyv/2); coin1(3)= grid_zw(ezw/2)
      coin2(1)= grid_xu(exu/2+1); coin2(2)= grid_yv(eyv/2+1); coin2(3)= grid_zw(ezw/2+1)

      Delta(1)= (Xmax-Xmin)/real(nx)
      Delta(2)= (Ymax-Ymin)/real(ny) 
      Delta(3)= (Zmax-Zmin)/real(nz)
      
      vl%objet(1)%sca(1:2) = AR**(-1d0/3d0)/2d0
      vl%objet(1)%sca(3) = vl%objet(1)%sca(1)*AR
     !print*, 'Ar num', vl%objet(1)%sca(3)/vl%objet(1)%sca(1)
        
         write(filename,'(A,I0,A)') 'Phase_function3_err.dat'        
         open(unit=1000,file=filename,status='replace')
         write(1000,'(a1,11a16)',advance='no')'#',' ----tpf---- ',&   
                                               ' ---Err Press-- ',&
                                               ' --Err Visc 1-- ',&
                                               ' --Err Visc 2-- ',&
                                               ' --Err Visc 3-- ',&
                                               ' --Err Vts 1--- ',&
                                               ' --Err Vts 2--- ',&
                                               ' --Err Vts 3--- '
         close(1000)

	
      do tpf= 1,30
      !do tpf= 5,5
	 
	 print*, nb_tir, AR, nx/4, tpf
         do itr=1,nb_tir
            !print*, itr
	    call random_number(Pos)	    
            vl%objet(1)%symper(1,:) = coin1 + (coin2-coin1)*Pos

	    call random_number(ang)
	    angle = 2*pi*ang
	    call random_number(direction)
	    direction = 10d0*direction
	    vl%objet(1)%orientation(1)= angle
	    if (ndim ==3) then
	       vl%objet(1)%orientation(1)= cos(angle/2d0)
	       vl%objet(1)%orientation(2:4)= sin(angle/2d0)*direction(:)/(norm2(direction(:))+ 1d-40)
	    end if

            do np=1, vl%kpt
              call Particle_PhaseFunction_PressureMesh(cou,couin0,grid_xu,grid_yv,grid_zw,ndim,tpf,vl)
              !call Particle_PhaseFunction_ViscousMesh(cou_vis,cou_visin,grid_x,grid_y,grid_z,grid_xu, &
              !                                            grid_yv,grid_zw,ndim,tpf,vl)
              !call Particle_PhaseFunction_VelocityMesh(cou_vts,cou_vtsin,grid_x,grid_y,grid_z,grid_xu,&
              !                                             grid_yv,grid_zw,ndim,tpf,vl)
            end do

            sum=0d0 
            nb =0 
              do i=sx,ex 
                 do j=sy,ey
                    do k=sz,ez
                       sum=sum+cou(i,j,k)*(grid_xu(i+1)-grid_xu(i))*(grid_yv(j+1)-grid_yv(j))*&
                                          (grid_zw(k+1)-grid_zw(k))
                    end do
                 end do  
              end do 
            sumvisx=0d0
            do i=sx,ex
               do j=sy,ey
                  do k=sz,ez
                     sumvisx = sumvisx + cou_vis(i,j,k,3)*(grid_x(i)-grid_x(i-1))*&
                                          (grid_y(j)-grid_y(j-1))*(grid_zw(k+1)-grid_zw(k))
                  end do
               end do  
            end do
            sumvisy=0d0
            do i=sx,ex
               do j=sy,ey
                  do k=sz,ez
                     sumvisy = sumvisy + cou_vis(i,j,k,2)*(grid_x(i)-grid_x(i-1))*&
                                          (grid_z(k)-grid_z(k-1))*(grid_yv(j+1)-grid_yv(j))
                  end do
               end do  
            end do
            sumvisz=0d0
            do i=sx,ex
               do j=sy,ey
                  do k=sz,ez
                     sumvisz = sumvisz + cou_vis(i,j,k,1)*(grid_z(k)-grid_z(k-1))*&
                                          (grid_y(j)-grid_y(j-1))*(grid_xu(i+1)-grid_xu(i))
                  end do
               end do  
            end do 
            sumvtsu=0d0
            do i=sxu,exu
               do j=syu,eyu
                  do k=szu,ezu
                     sumvtsu = sumvtsu + cou_vts(i,j,k,1)*(grid_zw(k+1)-grid_zw(k))*&
                                          (grid_yv(j+1)-grid_yv(j))*(grid_x(i)-grid_x(i-1))
                  end do
               end do  
            end do 
            sumvtsv=0d0
            do i=sxv,exv
               do j=syv,eyv
                  do k=szv,ezv
                     sumvtsv = sumvtsv + cou_vts(i,j,k,2)*(grid_zw(k+1)-grid_zw(k))*&
                                          (grid_y(j)-grid_y(j-1))*(grid_xu(i+1)-grid_xu(i))
                  end do
               end do  
            end do 
            sumvtsw=0d0
            do i=sxw,exw
               do j=syw,eyw
                  do k=szw,ezw
                     sumvtsw = sumvtsw + cou_vts(i,j,k,3)*(grid_z(k)-grid_z(k-1))*&
                                          (grid_yv(j+1)-grid_yv(j))*(grid_xu(i+1)-grid_xu(i))
                  end do
               end do  
            end do 
            call MPI_BARRIER(comm3d, mpi_code)
            call MPI_BARRIER(this%getMpiCommunicator(), mpi_code)
            call MPI_REDUCE(sum    ,sum0    ,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,comm3d,mpi_code) 
            call MPI_REDUCE(sumvisx,sumvisx0,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,comm3d,mpi_code) 
            call MPI_REDUCE(sumvisy,sumvisy0,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,comm3d,mpi_code) 
            call MPI_REDUCE(sumvisz,sumvisz0,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,comm3d,mpi_code) 
            call MPI_REDUCE(sumvtsu,sumvtsu0,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,comm3d,mpi_code) 
            call MPI_REDUCE(sumvtsv,sumvtsv0,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,comm3d,mpi_code) 
            call MPI_REDUCE(sumvtsw,sumvtsw0,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,comm3d,mpi_code) 
            call MPI_REDUCE(nb     ,nb_total,1,MPI_INTEGER         ,MPI_SUM,0,comm3d,mpi_code) 
            call MPI_BARRIER(comm3d, mpi_code)
            call MPI_BARRIER(this%getMpiCommunicator(), mpi_code)



            Erreur_relative(itr,1)=100*((4d0/3d0*PI*vl%objet(1)%sca(1)*vl%objet(1)%sca(2)*vl%objet(1)%sca(3))-sum0)/&
	    ((4d0/3d0*PI*vl%objet(1)%sca(1)*vl%objet(1)%sca(2)*vl%objet(1)%sca(3)))
            Erreur_relative(itr,2)=100*((4d0/3d0*PI*vl%objet(1)%sca(1)*vl%objet(1)%sca(2)*vl%objet(1)%sca(3))-sumvisx0)/&
	    ((4d0/3d0*PI*vl%objet(1)%sca(1)*vl%objet(1)%sca(2)*vl%objet(1)%sca(3)))
            Erreur_relative(itr,3)=100*((4d0/3d0*PI*vl%objet(1)%sca(1)*vl%objet(1)%sca(2)*vl%objet(1)%sca(3))-sumvisy0)/&
	    ((4d0/3d0*PI*vl%objet(1)%sca(1)*vl%objet(1)%sca(2)*vl%objet(1)%sca(3)))
            Erreur_relative(itr,4)=100*((4d0/3d0*PI*vl%objet(1)%sca(1)*vl%objet(1)%sca(2)*vl%objet(1)%sca(3))-sumvisz0)/&
	    ((4d0/3d0*PI*vl%objet(1)%sca(1)*vl%objet(1)%sca(2)*vl%objet(1)%sca(3)))
            Erreur_relative(itr,5)=100*((4d0/3d0*PI*vl%objet(1)%sca(1)*vl%objet(1)%sca(2)*vl%objet(1)%sca(3))-sumvtsu0)/&
	    ((4d0/3d0*PI*vl%objet(1)%sca(1)*vl%objet(1)%sca(2)*vl%objet(1)%sca(3)))
            Erreur_relative(itr,6)=100*((4d0/3d0*PI*vl%objet(1)%sca(1)*vl%objet(1)%sca(2)*vl%objet(1)%sca(3))-sumvtsv0)/&
	    ((4d0/3d0*PI*vl%objet(1)%sca(1)*vl%objet(1)%sca(2)*vl%objet(1)%sca(3)))
            Erreur_relative(itr,7)=100*((4d0/3d0*PI*vl%objet(1)%sca(1)*vl%objet(1)%sca(2)*vl%objet(1)%sca(3))-sumvtsw0)/&
	    ((4d0/3d0*PI*vl%objet(1)%sca(1)*vl%objet(1)%sca(2)*vl%objet(1)%sca(3)))
          
	    !if (rank == 0) then 

            !   print*, 'Vol Press' ,sum0
            !   print*, 'Vol Visc 1',sumvisx0
            !   print*, 'Vol Visc 2',sumvisy0
            !   print*, 'Vol Visc 3',sumvisz0
            !   print*, 'Vol Velo 1',sumvtsu0
            !   print*, 'Vol Velo 2',sumvtsv0
            !   print*, 'Vol Velo 3',sumvtsw0
            !   print*, 'Vol exact ',4d0/3d0*PI*vl%objet(1)%sca(1)*vl%objet(1)%sca(2)*vl%objet(1)%sca(3)
	    !	 print*, 'err rel Press' ,Erreur_relative(itr,1)
            !   print*, 'position centre', vl%objet(1)%symper(1,:), 'angle', 2d0*acos(vl%objet(1)%orientation(1))
	    !   print*, 'direction_vector', direction,
	    !	print*,  NEW_LINE('a')
            !end if
         
	 do nbt=1,7
            moyenne_stat(nbt)=0d0
            do itrr=1,itr 
               moyenne_stat(nbt) = moyenne_stat(nbt) + abs(Erreur_relative(itrr,nbt))
            end do 
         end do 
         if (rank .eq. 0) then
               open(unit=1000,file=filename,status='old',position='append')
               write(1000,'(8E16.8)',advance='no') dfloat(itr),&
                                                   moyenne_stat(1)/itr,&
                                                   moyenne_stat(2)/itr,&
                                                   moyenne_stat(3)/itr,&
                                                   moyenne_stat(4)/itr,&
                                                   moyenne_stat(5)/itr,&
                                                   moyenne_stat(6)/itr,&
                                                   moyenne_stat(7)/itr
               close(1000)
         endif 
	    	    	    
         end do

       print*, 'nb_tir=', nb_tir,'AR= ', AR,'Maille/D',1/Delta(1), 'tpf= ', tpf
            if (rank == 0) then 
               print*, 'errMoy Press' ,moyenne_stat(1)/nb_tir
               print*, 'errMoy Visc 1',moyenne_stat(2)/nb_tir
               print*, 'errMoy Visc 2',moyenne_stat(3)/nb_tir
               print*, 'errMoy Visc 3',moyenne_stat(4)/nb_tir
               print*, 'errMoy Velo 1',moyenne_stat(5)/nb_tir
               print*, 'errMoy Velo 2',moyenne_stat(6)/nb_tir
               print*, 'errMoy Velo 3',moyenne_stat(7)/nb_tir
	       print*, '----------------------------------------------------'
            end if
	 
      end do
      
      
   end subroutine t_Ellipsoid_PhaseFunction_PressureMesh
        
end module test_Ellipsoid_PhaseFunction_PressureMesh
!===============================================================================
 

!===============================================================================
module test_Ellipsoid_PhaseFunction_PressureMesh
      !===============================================================================
      use Bib_VOFLag_Particle_PhaseFunction_PressureMesh
      use Bib_VOFLag_Particle_PhaseFunction_VelocityMesh
      use Bib_VOFLag_Particle_PhaseFunction_ViscousMesh
      use Bib_VOFLag_Wall_PhaseFunction
      use test_Particle_Initialisation,                  only : t_Particle_Initialisation
      use test_MPI_Parameters,                           only : t_MPI_Parameters
      use test_Parameters,                               only : t_Parameters
      use pfunit
      use mpi 
      implicit none

      contains
      !> @author Andelhamid Hakkoum 11/2022
      ! 
      !> @brief 
      !        
      !        
      !
      !
      !> @todo
      !    
      !
      !-----------------------------------------------------------------------------
      @mpitest(npes=[1])
      subroutine t_Ellipsoid_PhaseFunction_PressureMesh(this)
      !===============================================================================
      !modules
      !===============================================================================
      !-------------------------------------------------------------------------------
      !Modules de definition des structures et variables
      !-------------------------------------------------------------------------------
      use Module_VOFLag_ParticlesDataStructure
      use Module_VOFLag_SubDomain_Data
      use mod_Parameters,                      only : rank,nproc,dim,deeptracking,sx,ex, & 
                                                      sy,ey,sz,ez,gsxu,gx,gy,gz,sxu,exu,syu,&
                                                      eyu,szu,ezu,sxv,exv,syv,eyv,szv,ezv,  & 
                                                      sxw,exw,syw,eyw,szw,ezw
      use mod_mpi,                             only : mpi_code,comm3d, coords
      !-------------------------------------------------------------------------------
      
      !===============================================================================
      !declarations des variables
      !===============================================================================
      !implicit none
      !-------------------------------------------------------------------------------
      !variables globales
      !-------------------------------------------------------------------------------
      class (MpiTestMethod), intent(inout)   :: this
      !-------------------------------------------------------------------------------
      !variables locales 
      !-------------------------------------------------------------------------------
      type(struct_vof_lag)                   :: vl
      !-------------------------------------------------------------------------------
      real(8), PARAMETER  :: PI = acos(-1.d0)
      !-------------------------------------------------------------------------------
      real(8), allocatable, dimension(:,:,:,:) :: cou_vis,cou_vts,cou_visin,cou_vtsin
      real(8), allocatable, dimension(:,:,:)   :: cou,couin0
      real(8), allocatable, dimension(:,:)     :: Erreur_relative
      real(8), allocatable, dimension(:)       :: grid_xu,grid_yv, grid_zw
      real(8), allocatable, dimension(:)       :: grid_x,grid_y, grid_z
      real(8),              dimension(7)       :: moyenne_stat
      real(8),              dimension(3)       :: Pos, direction, coin1, coin2,Delta
      real(8)                                  :: Xmin,Ymin,Zmin,Xmax,Ymax,Zmax
      real(8)                                  :: sum,sum0,sumvisx,sumvisx0,sumvisy, &
                                                  sumvisy0,sumvisz,sumvisz0,sumvtsu, & 
                                                  sumvtsu0,sumvtsv,sumvtsv0,sumvtsw, &
                                                  sumvtsw0, exact_vol, angle, ang, AR
      integer                                  :: nx,ny,nz,tpf,itr,nb_tir,nbt,CelPerDia,&
                                                  nb_ang, itrr
      integer                                  :: ndim,a,np,nb,i,j,k,b,nb_total,meshprop
      logical                                  :: test_debug
      character(len=200) :: filename,filename1
      character(len=20)  :: nat_ellipsoid
      !-------------------------------------------------------------------------------
      test_debug = .false.
      deeptracking = .false.

      meshprop=0
      ndim=3
      Xmin=-2d0;Xmax=2d0;Ymin=-2d0;Ymax=2d0;Zmin=-2d0;Zmax=2d0
      nb_tir= 20
      AR= 1d0
      
      if ( meshprop == 0)     then 
       nx=64;ny=64;nz=64
       !nx=160;ny=160;nz=160
      elseif ( meshprop == 1) then 

         nx=16;ny=20;nz=24 ! il faut qu'il soit multiple de 4

      end if 

      call t_MPI_Parameters(nx,ny,nz,ndim,this%getMpiCommunicator())
      m_coords = coords

      call t_Parameters(Xmin,Xmax,Ymin,Ymax,Zmin,Zmax,nx,ny,nz,ndim,meshprop,&
                         grid_xu,grid_yv, grid_zw,grid_x,grid_y, grid_z)

      call t_Particle_Initialisation(vl,1,[1],2,ndim)

      allocate(cou(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
      allocate(cou_vis(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz,1:3))
      allocate(cou_vts(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz,1:3))
      allocate(Erreur_relative(1:nb_tir,1:7))

      coin1(1)= grid_xu(exu/2); coin1(2)= grid_yv(eyv/2); coin1(3)= grid_zw(ezw/2)
      coin2(1)= grid_xu(exu/2+1); coin2(2)= grid_yv(eyv/2+1); coin2(3)= grid_zw(ezw/2+1)

      Delta(1)= (Xmax-Xmin)/real(nx)
      Delta(2)= (Ymax-Ymin)/real(ny) 
      Delta(3)= (Zmax-Zmin)/real(nz)
      
      vl%objet(1)%sca(1:2) = AR**(-1d0/3d0)/2d0
      vl%objet(1)%sca(3) = vl%objet(1)%sca(1)*AR
     !print*, 'Ar num', vl%objet(1)%sca(3)/vl%objet(1)%sca(1)
        
         write(filename,'(A,I0,A)') 'Phase_function_err_comparetoS.dat'        
         open(unit=1000,file=filename,status='replace')
         write(1000,'(a1,11a16)',advance='no')'#',' ----nb_tir---- ',&   
                                               ' ---Err Press-- ',&
                                               ' --Err Visc 1-- ',&
                                               ' --Err Visc 2-- ',&
                                               ' --Err Visc 3-- ',&
                                               ' --Err Vts 1--- ',&
                                               ' --Err Vts 2--- ',&
                                               ' --Err Vts 3--- '
         close(1000)

	
      do tpf= 20,20
      !do tpf= 5,5
	 
	 !print*, nb_tir, AR, nx/4, tpf
         do itr=1,nb_tir
            print*, itr
	    call random_number(Pos)	    
            vl%objet(1)%symper(1,:) = coin1 + (coin2-coin1)*Pos

	    call random_number(ang)
	    angle = 2*pi*ang
	    call random_number(direction)
	    direction = 10d0*direction
	    vl%objet(1)%orientation(1)= angle
	    if (ndim ==3) then
	       vl%objet(1)%orientation(1)= cos(angle/2d0)
	       vl%objet(1)%orientation(2:4)= sin(angle/2d0)*direction(:)/(norm2(direction(:))+ 1d-40)
	    end if

            do np=1, vl%kpt
              call Particle_PhaseFunction_PressureMesh(cou,couin0,grid_xu,grid_yv,grid_zw,ndim,tpf,vl)
              !call Particle_PhaseFunction_ViscousMesh(cou_vis,cou_visin,grid_x,grid_y,grid_z,grid_xu, &
              !                                            grid_yv,grid_zw,ndim,tpf,vl)
              !call Particle_PhaseFunction_VelocityMesh(cou_vts,cou_vtsin,grid_x,grid_y,grid_z,grid_xu,&
              !                                             grid_yv,grid_zw,ndim,tpf,vl)
            end do

            sum=0d0 
            nb =0 
              do i=sx,ex 
                 do j=sy,ey
                    do k=sz,ez
                       sum=sum+cou(i,j,k)*(grid_xu(i+1)-grid_xu(i))*(grid_yv(j+1)-grid_yv(j))*&
                                          (grid_zw(k+1)-grid_zw(k))
                    end do
                 end do  
              end do 
            sumvisx=0d0
            do i=sx,ex
               do j=sy,ey
                  do k=sz,ez
                     sumvisx = sumvisx + cou_vis(i,j,k,3)*(grid_x(i)-grid_x(i-1))*&
                                          (grid_y(j)-grid_y(j-1))*(grid_zw(k+1)-grid_zw(k))
                  end do
               end do  
            end do
            sumvisy=0d0
            do i=sx,ex
               do j=sy,ey
                  do k=sz,ez
                     sumvisy = sumvisy + cou_vis(i,j,k,2)*(grid_x(i)-grid_x(i-1))*&
                                          (grid_z(k)-grid_z(k-1))*(grid_yv(j+1)-grid_yv(j))
                  end do
               end do  
            end do
            sumvisz=0d0
            do i=sx,ex
               do j=sy,ey
                  do k=sz,ez
                     sumvisz = sumvisz + cou_vis(i,j,k,1)*(grid_z(k)-grid_z(k-1))*&
                                          (grid_y(j)-grid_y(j-1))*(grid_xu(i+1)-grid_xu(i))
                  end do
               end do  
            end do 
            sumvtsu=0d0
            do i=sxu,exu
               do j=syu,eyu
                  do k=szu,ezu
                     sumvtsu = sumvtsu + cou_vts(i,j,k,1)*(grid_zw(k+1)-grid_zw(k))*&
                                          (grid_yv(j+1)-grid_yv(j))*(grid_x(i)-grid_x(i-1))
                  end do
               end do  
            end do 
            sumvtsv=0d0
            do i=sxv,exv
               do j=syv,eyv
                  do k=szv,ezv
                     sumvtsv = sumvtsv + cou_vts(i,j,k,2)*(grid_zw(k+1)-grid_zw(k))*&
                                          (grid_y(j)-grid_y(j-1))*(grid_xu(i+1)-grid_xu(i))
                  end do
               end do  
            end do 
            sumvtsw=0d0
            do i=sxw,exw
               do j=syw,eyw
                  do k=szw,ezw
                     sumvtsw = sumvtsw + cou_vts(i,j,k,3)*(grid_z(k)-grid_z(k-1))*&
                                          (grid_yv(j+1)-grid_yv(j))*(grid_xu(i+1)-grid_xu(i))
                  end do
               end do  
            end do 
            call MPI_BARRIER(comm3d, mpi_code)
            call MPI_BARRIER(this%getMpiCommunicator(), mpi_code)
            call MPI_REDUCE(sum    ,sum0    ,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,comm3d,mpi_code) 
            call MPI_REDUCE(sumvisx,sumvisx0,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,comm3d,mpi_code) 
            call MPI_REDUCE(sumvisy,sumvisy0,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,comm3d,mpi_code) 
            call MPI_REDUCE(sumvisz,sumvisz0,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,comm3d,mpi_code) 
            call MPI_REDUCE(sumvtsu,sumvtsu0,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,comm3d,mpi_code) 
            call MPI_REDUCE(sumvtsv,sumvtsv0,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,comm3d,mpi_code) 
            call MPI_REDUCE(sumvtsw,sumvtsw0,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,comm3d,mpi_code) 
            call MPI_REDUCE(nb     ,nb_total,1,MPI_INTEGER         ,MPI_SUM,0,comm3d,mpi_code) 
            call MPI_BARRIER(comm3d, mpi_code)
            call MPI_BARRIER(this%getMpiCommunicator(), mpi_code)



            Erreur_relative(itr,1)=100*((4d0/3d0*PI*vl%objet(1)%sca(1)*vl%objet(1)%sca(2)*vl%objet(1)%sca(3))-sum0)/&
	    ((4d0/3d0*PI*vl%objet(1)%sca(1)*vl%objet(1)%sca(2)*vl%objet(1)%sca(3)))
            Erreur_relative(itr,2)=100*((4d0/3d0*PI*vl%objet(1)%sca(1)*vl%objet(1)%sca(2)*vl%objet(1)%sca(3))-sumvisx0)/&
	    ((4d0/3d0*PI*vl%objet(1)%sca(1)*vl%objet(1)%sca(2)*vl%objet(1)%sca(3)))
            Erreur_relative(itr,3)=100*((4d0/3d0*PI*vl%objet(1)%sca(1)*vl%objet(1)%sca(2)*vl%objet(1)%sca(3))-sumvisy0)/&
	    ((4d0/3d0*PI*vl%objet(1)%sca(1)*vl%objet(1)%sca(2)*vl%objet(1)%sca(3)))
            Erreur_relative(itr,4)=100*((4d0/3d0*PI*vl%objet(1)%sca(1)*vl%objet(1)%sca(2)*vl%objet(1)%sca(3))-sumvisz0)/&
	    ((4d0/3d0*PI*vl%objet(1)%sca(1)*vl%objet(1)%sca(2)*vl%objet(1)%sca(3)))
            Erreur_relative(itr,5)=100*((4d0/3d0*PI*vl%objet(1)%sca(1)*vl%objet(1)%sca(2)*vl%objet(1)%sca(3))-sumvtsu0)/&
	    ((4d0/3d0*PI*vl%objet(1)%sca(1)*vl%objet(1)%sca(2)*vl%objet(1)%sca(3)))
            Erreur_relative(itr,6)=100*((4d0/3d0*PI*vl%objet(1)%sca(1)*vl%objet(1)%sca(2)*vl%objet(1)%sca(3))-sumvtsv0)/&
	    ((4d0/3d0*PI*vl%objet(1)%sca(1)*vl%objet(1)%sca(2)*vl%objet(1)%sca(3)))
            Erreur_relative(itr,7)=100*((4d0/3d0*PI*vl%objet(1)%sca(1)*vl%objet(1)%sca(2)*vl%objet(1)%sca(3))-sumvtsw0)/&
	    ((4d0/3d0*PI*vl%objet(1)%sca(1)*vl%objet(1)%sca(2)*vl%objet(1)%sca(3)))
          
	    !if (rank == 0) then 

            !   print*, 'Vol Press' ,sum0
            !   print*, 'Vol Visc 1',sumvisx0
            !   print*, 'Vol Visc 2',sumvisy0
            !   print*, 'Vol Visc 3',sumvisz0
            !   print*, 'Vol Velo 1',sumvtsu0
            !   print*, 'Vol Velo 2',sumvtsv0
            !   print*, 'Vol Velo 3',sumvtsw0
            !   print*, 'Vol exact ',4d0/3d0*PI*vl%objet(1)%sca(1)*vl%objet(1)%sca(2)*vl%objet(1)%sca(3)
	    !	 print*, 'err rel Press' ,Erreur_relative(itr,1)
            !   print*, 'position centre', vl%objet(1)%symper(1,:), 'angle', 2d0*acos(vl%objet(1)%orientation(1))
	    !   print*, 'direction_vector', direction,
	    !	print*,  NEW_LINE('a')
            !end if
         
	    	    	    
         end do
	 do nbt=1,7
            moyenne_stat(nbt)=0d0
            do itrr=1,nb_tir 
               moyenne_stat(nbt) = moyenne_stat(nbt) + abs(Erreur_relative(nb_tir,nbt))
            end do 
         end do 
         if (rank .eq. 0) then
               open(unit=1000,file=filename,status='old',position='append')
               write(1000,'(8E16.8)',advance='no') dfloat(nb_tir),&
                                                   moyenne_stat(1)/nb_tir,&
                                                   moyenne_stat(2)/nb_tir,&
                                                   moyenne_stat(3)/nb_tir,&
                                                   moyenne_stat(4)/nb_tir,&
                                                   moyenne_stat(5)/nb_tir,&
                                                   moyenne_stat(6)/nb_tir,&
                                                   moyenne_stat(7)/nb_tir
               close(1000)
         endif 

       print*, 'nb_tir=', nb_tir,'AR= ', AR,'Maille/D',1/Delta(1), 'tpf= ', tpf
	 
      end do
      
      
   end subroutine t_Ellipsoid_PhaseFunction_PressureMesh
        
end module test_Ellipsoid_PhaseFunction_PressureMesh
!===============================================================================
 


  	 do nbt=1,7
            moyenne_stat(nbt)=0d0
            do itrr=1,itr 
               moyenne_stat(nbt) = moyenne_stat(nbt) + abs(Erreur_relative(itrr,nbt))
            end do 
         end do 
         if (rank .eq. 0) then
               open(unit=1000,file=filename,status='old',position='append')
               write(1000,'(8E16.8)',advance='no') dfloat(tpf),&
                                                   moyenne_stat(1)/itr,&
                                                   moyenne_stat(2)/itr,&
                                                   moyenne_stat(3)/itr,&
                                                   moyenne_stat(4)/itr,&
                                                   moyenne_stat(5)/itr,&
                                                   moyenne_stat(6)/itr,&
                                                   moyenne_stat(7)/itr
               close(1000)
         endif 
         end do
	 
       print*, 'nb_tir=', nb_tir,'AR= 1',achar(9),'Maille/D',nx/4, 'tpf= ', tpf
            if (rank == 0) then 
               print*, 'errMoy Press' ,moyenne_stat(1)/nb_tir
               print*, 'errMoy Visc 1',moyenne_stat(2)/nb_tir
               print*, 'errMoy Visc 2',moyenne_stat(3)/nb_tir
               print*, 'errMoy Visc 3',moyenne_stat(4)/nb_tir
               print*, 'errMoy Velo 1',moyenne_stat(5)/nb_tir
               print*, 'errMoy Velo 2',moyenne_stat(6)/nb_tir
               print*, 'errMoy Velo 3',moyenne_stat(7)/nb_tir
	       print*, '----------------------------------------------------'
            end if

      end do



   end subroutine t_Particle_PhaseFunction_PressureMesh
        
end module test_Particle_PhaseFunction_PressureMesh
!===============================================================================
 
