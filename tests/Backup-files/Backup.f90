         p(1) = 0.0d0; p(2) =  vl%objet(np)%sca(2)
	 if (ndim == 3) then 
	 p(3)= vl%objet(np)%sca(3); p(1)= 0d0; p(2)= 0d0
	 end if
	 call Vector_FromInertial2ParticleFrame(vl,np,1,ndim,.false.,p,p_rotated)
         call Particle_PointIn(vl,np,1,vl%objet(np)%sca,p_rotated,ndim,in_particle)
	 write(*,*) p ,NEW_LINE('a'), p_rotated, NEW_LINE('a')
         a = 0
         if (in_particle) a = 1
         @assertEqualUserDefined(a,1)
	 
!         p(1) = vl%objet(np)%sca(1)/root_2; p(2) =  vl%objet(np)%sca(2)/root_2
!	 if (ndim == 3) then 
!	 p(3)= vl%objet(np)%sca(3)/root_2; p(2)=p(1)
!	 end if
!	 call Vector_FromInertial2ParticleFrame(vl,np,1,ndim,.false.,p,p_rotated)
!         call Particle_PointIn(vl,np,1,vl%objet(np)%sca,p_rotated,ndim,in_particle)
!	 write(*,*) p ,NEW_LINE('a'), p_rotated, NEW_LINE('a')
!         a = 0
!         if (in_particle) a = 1
!         @assertEqualUserDefined(a,1)

          p(1) = -vl%objet(np)%sca(1); p(2) =  0d0
	 if (ndim == 3) then 
	 p(3)= 0.0d0; p(1)= -vl%objet(np)%sca(1)/root_2; p(2)= -vl%objet(np)%sca(2)/root_2
	 end if
	 call Vector_FromInertial2ParticleFrame(vl,np,1,ndim,.false.,p,p_rotated)
         call Particle_PointIn(vl,np,1,vl%objet(np)%sca,p_rotated,ndim,in_particle)
	 write(*,*) p ,NEW_LINE('a'), p_rotated, NEW_LINE('a')
         a = 0
         if (in_particle) a = 1
         @assertEqualUserDefined(a,1)
	 
!	 p(1) = vl%objet(np)%sca(1)/root_2; p(2) =  -vl%objet(np)%sca(2)/root_2 
!	 if (ndim == 3) then 
!	 p(3)= -vl%objet(np)%sca(3)/root_2; p(2)=p(1)
!	 end if
!	 call Vector_FromInertial2ParticleFrame(vl,np,1,ndim,.false.,p,p_rotated)
!         call Particle_PointIn(vl,np,1,vl%objet(np)%sca,p_rotated,ndim,in_particle)
!	 write(*,*) p ,NEW_LINE('a'), p_rotated, NEW_LINE('a')
!         a = 0
!         if (in_particle) a = 1
!         @assertEqualUserDefined(a,1)

         p(1) = 0.0d0; p(2) =  -vl%objet(np)%sca(2)
	 if (ndim == 3) then 
	 p(3)= -vl%objet(np)%sca(3); p(1)=0d0; p(2)= 0d0
	 end if
	 call Vector_FromInertial2ParticleFrame(vl,np,1,ndim,.false.,p,p_rotated)
         call Particle_PointIn(vl,np,1,vl%objet(np)%sca,p_rotated,ndim,in_particle)
	 write(*,*) p ,NEW_LINE('a'), p_rotated, NEW_LINE('a')
         a = 0
         if (in_particle) a = 1
         @assertEqualUserDefined(a,1)

!	 p(1) = -vl%objet(np)%sca(1)/root_2; p(2) =  -vl%objet(np)%sca(2)/root_2 
!	 if (ndim == 3) then 
!	 p(3)= -vl%objet(np)%sca(3)/root_2; p(2)= p(1)
!	 end if
!	 call Vector_FromInertial2ParticleFrame(vl,np,1,ndim,.false.,p,p_rotated)
!         call Particle_PointIn(vl,np,1,vl%objet(np)%sca,p_rotated,ndim,in_particle)
!	 write(*,*) p ,NEW_LINE('a'), p_rotated, NEW_LINE('a')
!         a = 0
!         if (in_particle) a = 1
!         @assertEqualUserDefined(a,1)

         p(1) = vl%objet(np)%sca(1); p(2) =  0.0d0
	 if (ndim == 3) then 
	 p(3)= 0.0d0; p(1)= vl%objet(np)%sca(1)/root_2; p(2)= vl%objet(np)%sca(2)/root_2
	 end if
	 call Vector_FromInertial2ParticleFrame(vl,np,1,ndim,.false.,p,p_rotated)
         call Particle_PointIn(vl,np,1,vl%objet(np)%sca,p_rotated,ndim,in_particle)
	 write(*,*) p ,NEW_LINE('a'), p_rotated, NEW_LINE('a')
         a = 0
         if (in_particle) a = 1
         @assertEqualUserDefined(a,1)

!	 p(1) = -vl%objet(np)%sca(1)/root_2; p(2) =  vl%objet(np)%sca(2)/root_2 
!	 if (ndim == 3) then 
!	 p(3)= vl%objet(np)%sca(3)/root_2; p(2)= p(1)
!	 end if
!	 call Vector_FromInertial2ParticleFrame(vl,np,1,ndim,.false.,p,p_rotated)
!         call Particle_PointIn(vl,np,1,vl%objet(np)%sca,p_rotated,ndim,in_particle)
!	 write(*,*) p ,NEW_LINE('a'), p_rotated, NEW_LINE('a')
!         a = 0
!         if (in_particle) a = 1
!         @assertEqualUserDefined(a,1)
	 
	 if (ndim == 3) then 
	 p(1)= vl%objet(np)%sca(1)/root_2/root_2; p(2)= vl%objet(np)%sca(2)/root_2/root_2
	 p(3)= vl%objet(np)%sca(3)/root_2 
	 end if
	 call Vector_FromInertial2ParticleFrame(vl,np,1,ndim,.false.,p,p_rotated)
         call Particle_PointIn(vl,np,1,vl%objet(np)%sca,p_rotated,ndim,in_particle)
         write(*,*) p_rotated
         a = 0
         if (in_particle) a = 1
         @assertEqualUserDefined(a,1)
	 
	 
	 
	 
-------------------------------------------------------------------------------------------------------------------------------	 
	 
	 
	 	do i=1, 1000
            call random_number(r)
	    if (ndim==2) p(3) = 0d0
            if (((p(1)/radii(1))**2+ (p(2)/radii(2))**2 + (p(3)/radii(3))**2) > 1d0)  goto 102
            call Particle_PointIn(vl,np,1,vl%objet(np)%sca,p,ndim,in_particle)
	    count = count +1
            a = 0
            if (in_particle) a = 1
            @assertEqualUserDefined(a,1)
	    102 continue
         end do
	 write(*,*) p
         write(*,*) count



	 do i=1, 10
            call random_number(r)
	    p(1)= -vl%objet(np)%sca(1) + 2d0*vl%objet(np)%sca(1)*r(1) 
	    p(2)= -vl%objet(np)%sca(2) + 2d0*vl%objet(np)%sca(2)*r(2)
            if (((p(1)/radii(1))**2+ (p(2)/radii(2))**2 ) >= 1d0)  goto 102	    
            call Particle_PointIn(vl,np,1,vl%objet(np)%sca,p,ndim,in_particle)
            a = 0
            if (in_particle) a = 1
            @assertEqualUserDefined(a,1)
	    write(*,*) p
	    102 continue	    
         end do

----------------
angle_random
----------------
     	   angle = angle_0 
           if (ndim ==3) then 
             angle = angle_0
	     direction = direction_0
    	   end if	

	   angle = angle/2d0 + i*angle
	   vl%objet(np)%orientation(1)= angle
	   if (ndim ==3) then
	   vl%objet(np)%orientation(1)= cos(angle/2d0)
           vl%objet(np)%orientation(2:4)= sin(angle/2d0)*direction(:)
	   end if
	   print*, pos, angle

--------------------------------------------------------------------------------------
test_ellipsoid_PhaseFunction
--------------------------------------------------------------------------------------
            if (rank == 0) then 
               !@assertEqual(mod(nb_total,2),0)
               write(*,*) 'val Press',achar(9), 'val exact ',achar(9), 'err rel Press %'
               write(*,*) sum0, 4d0/3d0*PI*vl%objet(1)%sca(1)*vl%objet(1)%sca(2)*vl%objet(1)%sca(3), Erreur_relative(itr,1)
               write(*,*) NEW_LINE('a') 
            end if
 
 
 
             Erreur_relative(itr,1)=100*((4d0/3d0*PI*vl%objet(1)%sca(1)*vl%objet(1)%sca(2)*vl%objet(1)%sca(3))-sum0)&
	    /(4d0/3d0*PI*vl%objet(1)%sca(1)*vl%objet(1)%sca(2)*vl%objet(1)%sca(3))
            Erreur_relative(itr,2)=100*((4d0/3d0*PI*vl%objet(1)%sca(1)*vl%objet(1)%sca(2)*vl%objet(1)%sca(3))-sumvisx0)/&
             (4d0/3d0*PI*vl%objet(1)%sca(1)*vl%objet(1)%sca(2)*vl%objet(1)%sca(3))
            Erreur_relative(itr,3)=100*((4d0/3d0*PI*vl%objet(1)%sca(1)*vl%objet(1)%sca(2)*vl%objet(1)%sca(3))-sumvisy0)&
             /(4d0/3d0*PI*vl%objet(1)%sca(1)*vl%objet(1)%sca(2)*vl%objet(1)%sca(3))
            Erreur_relative(itr,4)=100*((4d0/3d0*PI*vl%objet(1)%sca(1)*vl%objet(1)%sca(2)*vl%objet(1)%sca(3))-sumvisz0)&
             /(4d0/3d0*PI*vl%objet(1)%sca(1)*vl%objet(1)%sca(2)*vl%objet(1)%sca(3))
            Erreur_relative(itr,5)=100*((4d0/3d0*PI*vl%objet(1)%sca(1)*vl%objet(1)%sca(2)*vl%objet(1)%sca(3))-sumvtsu0)&
             /(4d0/3d0*PI*vl%objet(1)%sca(1)*vl%objet(1)%sca(2)*vl%objet(1)%sca(3))
            Erreur_relative(itr,6)=100*((4d0/3d0*PI*vl%objet(1)%sca(1)*vl%objet(1)%sca(2)*vl%objet(1)%sca(3))-sumvtsv0)&
             /(4d0/3d0*PI*vl%objet(1)%sca(1)*vl%objet(1)%sca(2)*vl%objet(1)%sca(3))
            Erreur_relative(itr,7)=100*((4d0/3d0*PI*vl%objet(1)%sca(1)*vl%objet(1)%sca(2)*vl%objet(1)%sca(3))-sumvtsw0)&
             /(4d0/3d0*PI*vl%objet(1)%sca(1)*vl%objet(1)%sca(2)*vl%objet(1)%sca(3))



            if (ndim==2) then
	      sum=0d0 
	      do i=sx,ex 
                 do j=sy,ey
                     sum=sum+cou(i,j,1)*(grid_xu(i+1)-grid_xu(i))*(grid_yv(j+1)-grid_yv(j))
                  end do  
               end do 
               sumvisx=0d0
               do i=sx,ex
                  do j=sy,ey
                      sumvisx = sumvisx + cou_vis(i,j,1,1)*(grid_x(i)-grid_x(i-1))*(grid_y(j)-grid_y(j-1))
                  end do  
               end do 
               sumvtsu=0d0
               do i=sxu,exu
                  do j=syu,eyu
                      sumvtsu = sumvtsu + cou_vts(i,j,1,1)*(grid_yv(j+1)-grid_yv(j))*(grid_x(i)-grid_x(i-1))
                  end do  
               end do 
               sumvtsv=0d0
               do i=sxv,exv
                  do j=syv,eyv
                      sumvtsv = sumvtsv + cou_vts(i,j,1,2)*(grid_y(j)-grid_y(j-1))*(grid_xu(i+1)-grid_xu(i))
                  end do  
               end do 

