!===============================================================================
module test_Ellipsoid_PhaseFunction_PressureMesh
      !===============================================================================
      use Bib_VOFLag_Particle_PhaseFunction_PressureMesh
      use Bib_VOFLag_Particle_PhaseFunction_VelocityMesh
      use Bib_VOFLag_Particle_PhaseFunction_ViscousMesh
!      use Bib_VOFLag_Wall_PhaseFunction
      use test_Particle_Initialisation,                  only : t_Particle_Initialisation
      use test_MPI_Parameters,                           only : t_MPI_Parameters
      use test_Parameters,                               only : t_Parameters
      use pfunit
      use mpi 
      implicit none

      contains
      !> @author Ayoub Sari 05/2022
      ! 
      !> @brief 
      !        
      !        
      !
      !
      !> @todo
      !    
      !
      !-----------------------------------------------------------------------------
      @mpitest(npes=[1])
      subroutine t_Ellipsoid_PhaseFunction_PressureMesh(this)
      !===============================================================================
      !modules
      !===============================================================================
      !-------------------------------------------------------------------------------
      !Modules de definition des structures et variables
      !-------------------------------------------------------------------------------
      use Module_VOFLag_ParticlesDataStructure
      use Module_VOFLag_SubDomain_Data
      use mod_Parameters,                      only : rank,nproc,dim,deeptracking,sx,ex, & 
                                                      sy,ey,sz,ez,gsxu,gx,gy,gz,sxu,exu,syu,&
                                                      eyu,szu,ezu,sxv,exv,syv,eyv,szv,ezv,  & 
                                                      sxw,exw,syw,eyw,szw,ezw
      use mod_mpi,                             only : mpi_code,comm3d, coords
      !-------------------------------------------------------------------------------
      
      !===============================================================================
      !declarations des variables
      !===============================================================================
      !implicit none
      !-------------------------------------------------------------------------------
      !variables globales
      !-------------------------------------------------------------------------------
      class (MpiTestMethod), intent(inout)   :: this
      !-------------------------------------------------------------------------------
      !variables locales 
      !-------------------------------------------------------------------------------
      type(struct_vof_lag)                   :: vl
      !-------------------------------------------------------------------------------
      real(8), PARAMETER  :: PI = acos(-1.d0)
      !-------------------------------------------------------------------------------
      real(8), allocatable, dimension(:,:,:,:) :: cou_vis,cou_vts,cou_visin,cou_vtsin
      real(8), allocatable, dimension(:,:,:)   :: cou,couin0
      real(8), allocatable, dimension(:,:)     :: Erreur_relative
      real(8), allocatable, dimension(:)       :: grid_xu,grid_yv, grid_zw
      real(8), allocatable, dimension(:)       :: grid_x,grid_y, grid_z
      real(8),              dimension(7)       :: moyenne_stat
      real(8),              dimension(3)       :: Pos, direction
      real(8)                                  :: Xmin,Ymin,Zmin,Xmax,Ymax,Zmax
      real(8)                                  :: sum,sum0,sumvisx,sumvisx0,sumvisy, &
                                                  sumvisy0,sumvisz,sumvisz0,sumvtsu, & 
                                                  sumvtsu0,sumvtsv,sumvtsv0,sumvtsw, &
                                                  sumvtsw0, exact_vol, angle, ang
      integer                                  :: nx,ny,nz,tpf,itr,nb_tir,nbt,CelPerDia,&
                                                  nb_ang, itrr
      integer                                  :: ndim,a,np,nb,i,j,k,b,nb_total,meshprop
      logical                                  :: test_debug
      character(len=200) :: filename,filename1
      !-------------------------------------------------------------------------------
      test_debug = .false.
      deeptracking = .false.

      ndim=3
      Xmin=-2d0;Xmax=2d0;Ymin=-2d0;Ymax=2d0;Zmin=-2d0;Zmax=2d0
      nb_tir=100
      nb_ang=1
      tpf=10
      nx=16;ny=20;nz=24 ! il faut qu'il soit multiple de 4
      meshprop =0

      call t_MPI_Parameters(nx,ny,nz,ndim,this%getMpiCommunicator())
      m_coords = coords
      call t_Parameters(Xmin,Xmax,Ymin,Ymax,Zmin,Zmax,nx,ny,nz,ndim,meshprop,&
                         grid_xu,grid_yv, grid_zw,grid_x,grid_y, grid_z)
      call t_Particle_Initialisation(vl,1,[1],2,ndim)
      
      allocate(cou(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz))
      allocate(cou_vis(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz,1:3))
      allocate(cou_vts(sx-gx:ex+gx,sy-gy:ey+gy,sz-gz:ez+gz,1:3))
      allocate(Erreur_relative(1:nb_tir,1:7))

      angle =  vl%objet(1)%orientation(1)
      if (ndim ==3) then
        angle = 2d0*acos(vl%objet(1)%orientation(1))
        direction = vl%objet(1)%orientation(2:4)/sin(angle/2d0)
      end if	


         do itr=1,nb_tir
            call random_number(Pos(1))
            call random_number(Pos(2))
            call random_number(Pos(3))
            vl%objet(1)%symper(1,:) = Pos
!	 do itrr= 1,nb_ang
!	   call random_number(ang)
!	   angle = 2*pi*ang
	   
            do np=1, vl%kpt
!	       vl%objet(np)%orientation(1)= angle
!	       if (ndim ==3) then
!		  vl%objet(np)%orientation(1)= cos(angle/2d0)
!		  vl%objet(np)%orientation(2:4)= sin(angle/2d0)*direction(:)
!	       end if
!	   print*, pos, angle
!	   print*, vl%objet(np)%orientation, norm2(vl%objet(np)%orientation(:))
	    	   	    
               call Particle_PhaseFunction_PressureMesh(cou,couin0,grid_xu,grid_yv,grid_zw,ndim,tpf,vl)
               call Particle_PhaseFunction_ViscousMesh(cou_vis,cou_visin,grid_x,grid_y,grid_z,grid_xu, &
                                                          grid_yv,grid_zw,ndim,tpf,vl)
               call Particle_PhaseFunction_VelocityMesh(cou_vts,cou_vtsin,grid_x,grid_y,grid_z,grid_xu,&
                                                           grid_yv,grid_zw,ndim,tpf,vl)
            end do

            nb =0 
      	    sum=0d0
	    do i=sx,ex 
               do j=sy,ey
                  do k=sz,ez
                     sum=sum+cou(i,j,k)*(grid_xu(i+1)-grid_xu(i))*(grid_yv(j+1)-grid_yv(j))*&
                                            (grid_zw(k+1)-grid_zw(k))
                  end do
               end do  
            end do 
            sumvisx=0d0
            do i=sx,ex
               do j=sy,ey
                  do k=sz,ez
                     sumvisx = sumvisx + cou_vis(i,j,k,3)*(grid_x(i)-grid_x(i-1))*&
                                          (grid_y(j)-grid_y(j-1))*(grid_zw(k+1)-grid_zw(k))
                  end do
               end do  
            end do
            sumvisy=0d0
            do i=sx,ex
               do j=sy,ey
                  do k=sz,ez
                     sumvisy = sumvisy + cou_vis(i,j,k,2)*(grid_x(i)-grid_x(i-1))*&
                                          (grid_z(k)-grid_z(k-1))*(grid_yv(j+1)-grid_yv(j))
                  end do
               end do  
            end do
            sumvisz=0d0
            do i=sx,ex
               do j=sy,ey
                  do k=sz,ez
                     sumvisz = sumvisz + cou_vis(i,j,k,1)*(grid_z(k)-grid_z(k-1))*&
                                          (grid_y(j)-grid_y(j-1))*(grid_xu(i+1)-grid_xu(i))
                  end do
               end do  
            end do 
            sumvtsu=0d0
            do i=sxu,exu
               do j=syu,eyu
                  do k=szu,ezu
                     sumvtsu = sumvtsu + cou_vts(i,j,k,1)*(grid_zw(k+1)-grid_zw(k))*&
                                          (grid_yv(j+1)-grid_yv(j))*(grid_x(i)-grid_x(i-1))
                  end do
               end do  
            end do 
            sumvtsv=0d0
            do i=sxv,exv
               do j=syv,eyv
                  do k=szv,ezv
                     sumvtsv = sumvtsv + cou_vts(i,j,k,2)*(grid_zw(k+1)-grid_zw(k))*&
                                          (grid_y(j)-grid_y(j-1))*(grid_xu(i+1)-grid_xu(i))
                  end do
               end do  
            end do 
            sumvtsw=0d0
            do i=sxw,exw
               do j=syw,eyw
                  do k=szw,ezw
                     sumvtsw = sumvtsw + cou_vts(i,j,k,3)*(grid_z(k)-grid_z(k-1))*&
                                          (grid_yv(j+1)-grid_yv(j))*(grid_xu(i+1)-grid_xu(i))
                  end do
               end do  
            end do 
            call MPI_BARRIER(comm3d, mpi_code)
            call MPI_BARRIER(this%getMpiCommunicator(), mpi_code)
            call MPI_REDUCE(sum    ,sum0    ,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,comm3d,mpi_code) 
            call MPI_REDUCE(sumvisx,sumvisx0,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,comm3d,mpi_code) 
            call MPI_REDUCE(sumvisy,sumvisy0,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,comm3d,mpi_code) 
            call MPI_REDUCE(sumvisz,sumvisz0,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,comm3d,mpi_code) 
            call MPI_REDUCE(sumvtsu,sumvtsu0,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,comm3d,mpi_code) 
            call MPI_REDUCE(sumvtsv,sumvtsv0,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,comm3d,mpi_code) 
            call MPI_REDUCE(sumvtsw,sumvtsw0,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,comm3d,mpi_code) 
            call MPI_REDUCE(nb     ,nb_total,1,MPI_INTEGER         ,MPI_SUM,0,comm3d,mpi_code) 
            call MPI_BARRIER(comm3d, mpi_code)
            call MPI_BARRIER(this%getMpiCommunicator(), mpi_code)

            if (ndim==2) then
	       exact_vol = PI*vl%objet(1)%sca(1)*vl%objet(1)%sca(2)
	    else
	       exact_vol = 4d0/3d0*PI*vl%objet(1)%sca(1)*vl%objet(1)%sca(2)*vl%objet(1)%sca(3)
	    end if

            Erreur_relative(itr,1)=100*((exact_vol)-sum0)/(exact_vol)
            Erreur_relative(itr,2)=100*((exact_vol)-sumvisx0)/(exact_vol)
            Erreur_relative(itr,3)=100*((exact_vol)-sumvisy0)/(exact_vol)
	    Erreur_relative(itr,4)=100*((exact_vol)-sumvisz0)/(exact_vol)
            Erreur_relative(itr,3)=100*((exact_vol)-sumvtsu0)/(exact_vol)
            Erreur_relative(itr,4)=100*((exact_vol)-sumvtsv0)/(exact_vol)
            Erreur_relative(itr,7)=100*((exact_vol)-sumvtsw0)/(exact_vol)
            if (rank == 0) then 
               print*, 'err rel Press' ,Erreur_relative(itr,1)
	       print*, 'err rel Visc 1',Erreur_relative(itr,2), 'err rel Visc 2',&
	        Erreur_relative(itr,3), 'err rel Visc 3',Erreur_relative(itr,4)
	       print*, 'err rel Velo 1',Erreur_relative(itr,5), 'err rel Velo 2',Erreur_relative(itr,6),&
	        'err rel Velo 3',Erreur_relative(itr,7)
            end if        
!	end do
        end do
      
       if (rank .eq. 0) then
         close(1000)
	 write(filename1,'(A,I0,A)') 'valid_color_function_average',CelPerDia,'.dat'
	 open(unit=1000,file=filename1,status='replace')
         write(1000,'(a1,8a16)',advance='no')'#', ' ----pt fict---- ',&
                                                   'Err Press Moy  ', &
                                                   ' Err Visc 1 Moy ',&
                                                   ' Err Visc 2 Moy ',&
                                                   ' Err Visc 3 Moy ',&
                                                   ' Err Vts 1 Moy ', &
                                                   ' Err Vts 2 Moy ', &
                                                   ' Err Vts 3 Moy '
         close(1000)
       endif

       do nbt=1,7
         moyenne_stat(nbt)=0d0
	 do itr=1,nb_tir 
	    do itrr=1,nb_ang
              moyenne_stat(nbt) = moyenne_stat(nbt) + abs(Erreur_relative(itr,nbt))
            end do
	 end do
       end do 
	 
       if (rank .eq. 0) then
         open(unit=1000,file=filename1,status='old', position='append')
            write(1000,'(8E16.8)',advance='no')    dfloat(tpf),&
                                                   moyenne_stat(1)/nb_tir,&
                                                   moyenne_stat(2)/nb_tir,&
                                                   moyenne_stat(3)/nb_tir,&
                                                   moyenne_stat(4)/nb_tir,&
                                                   moyenne_stat(5)/(nb_tir*nb_ang),&
                                                   moyenne_stat(6)/(nb_tir*nb_ang),&
                                                   moyenne_stat(7)/(nb_tir*nb_ang)
         close(1000)
       endif 
       print*, tpf, exact_vol, moyenne_stat(1)/(nb_tir*nb_ang)



   end subroutine t_Ellipsoid_PhaseFunction_PressureMesh
        
end module test_Ellipsoid_PhaseFunction_PressureMesh
!===============================================================================
 
 
-------------------------------------------------------------------------------------------------------------------- 
 
 
 
      print*, sx,ex,sy,ey,sz,ez
      print*, gx, gy,gz
      print*, gsx,gex,gsy,gey,gsz,gez
      print*, gsxu,gexu,gsyv,geyv,gszw,gezw


!      print*, sx,ex,sy,ey,sz,ez
!      print*, gx,gy,gz
!      print*, sxu,exu,syu,eyu,szu,ezu
!      print*, sxv,exv,syv,eyv,szv,ezv
!      print*, sxw,exw,syw,eyw,szw,ezw
!      print*, size(grid_x), size(grid_y), size(grid_z)
!      print*, size(cou, dim=1), size(cou, dim=2), size(cou, dim=3)
!      print*, size(grid_xu), size(grid_yv), size(grid_zw)

      Delta_x= 1/16d0!(Xmax-Xmin)/real(nx)
      Delta_y= 1/16d0!(Ymax-Ymin)/real(ny) 
      Delta_z= 1/16d0!(Zmax-Zmin)/real(nz)

---------------------------------------------------------------------------------------------------------------------
      
!      CelPerDia=(int(Xmax)-int(Xmin))*16
!      if (rank .eq. 0) then
!        close(1000)
!        write(filename1,'(A,I0,A)') 'Phase_function_err_moyenne',CelPerDia,'.dat'
!        open(unit=1000,file=filename1,status='replace')
!         write(1000,'(a1,8a16)',advance='no')'#', ' ----pt fict---- ',&
!                                                   'Err Press Moy  ', &
!                                                   ' Err Visc 1 Moy ',&
!                                                   ' Err Visc 2 Moy ',&
!                                                   ' Err Visc 3 Moy ',&
!                                                   ' Err Vts 1 Moy ', &
!                                                   ' Err Vts 2 Moy ', &
!                                                   ' Err Vts 3 Moy '
!         close(1000)
!      endif


            ! if (rank .eq. 0) then 
            !    open(unit=1000,file=filename,status='old',position='append')
               
            !    if (ndim==2) write(1000,'(2E16.8)',advance='no') dfloat(tpf),&
            !                                                  ((PI*vl%objet(1)%sca(1)**2)-sumvisx0)/&
            !                                                  & ((PI*vl%objet(1)%sca(1)**2)),&
            !                                                  ((PI*vl%objet(1)%sca(1)**2)-sumvisy0)/&
            !                                                  & ((PI*vl%objet(1)%sca(1)**2)),&
            !                                                  ((PI*vl%objet(1)%sca(1)**2)-sumvisz0)/&
            !                                                  & ((PI*vl%objet(1)%sca(1)**2))
            !    if (ndim==3) write(1000,'(11E16.8)',advance='no') dfloat(tpf),&
            !                                                  Pos(1),Pos(2),Pos(3),&
            !                                                  Erreur_relative(itr,1),&
            !                                                  Erreur_relative(itr,2),&
            !                                                  Erreur_relative(itr,3),&
            !                                                  Erreur_relative(itr,4),&
            !                                                  Erreur_relative(itr,5),&
            !                                                  Erreur_relative(itr,6),&
            !                                                  Erreur_relative(itr,7)
            !    close(1000)
            ! end if




                !      if ( cou(i,j,k) /= 0d0 .and. cou(i,j,k) /= 1d0) then 
                !         print*, grid_x(i), grid_y(j), grid_z(k),' grid_x 0.?'
                !      end if 
                !      if ( cou(i,j,k) == 1d0) then 
                !         print*, grid_x(i), grid_y(j), grid_z(k),' grid_x 1'
                !      end if 
                !      if ( cou(i,j,k) == 0d0) then 
                !         print*, grid_xu(i), grid_yv(j), grid_zw(k),' grid_x 0'
                !      end if 
 
 
 	do AR=1,6
           vl%objet(1)%sca(1:2) = AR**(1d0/3d0)
           vl%objet(1)%sca(3) = vl%objet(1)%sca(1)/AR	    
               
 
