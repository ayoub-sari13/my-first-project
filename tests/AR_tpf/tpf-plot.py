import matplotlib.pyplot as plt
import numpy as np
import os,sys

def Convergence_plot(order):
 
   # filename = 'AR_6i_d_16_tpf_20/Phase_function2_err.dat'  
    filename = 'AR116/Phase_function_err_tpfformap.dat'  
    
    if not os.path.isfile(filename):
        print("Error: File does not exist.")
        return
    
    print(f"Reading data from {filename}")
    data = np.loadtxt(filename)
    
    # Split data into columns
    #itr = data[:, 0]
    nb_tpf = data[1:, 0]
    err = data[1:, order]
    
    fig, axes= plt.subplots()
    
    
    axes.plot(nb_tpf,err,'-o',label='AR=1')
    axes.set_xlabel('tpf')
    axes.set_ylabel('relative error')
    axes.set_title('convergence study on number of fictif points (D/16)')
    axes.legend()
    plt.show()

def Convergence_nplot(d,split):
 
    filename1 = 'AR116/Phase_function_err_tpfformap.dat'  
    filename2 = 'AR216/Phase_function_err_tpfformap.dat'  
    filename3 = 'AR316/Phase_function_err_tpfformap.dat'  
    filename4 = 'AR416/Phase_function_err_tpfformap.dat'  
    filename5 = 'AR516/Phase_function_err_tpfformap.dat'  
    filename6 = 'AR616/Phase_function_err_tpfformap.dat'  
    
#    filename1 = 'AR116/Phase_function_err_tpfformap.dat'  
    filename7 = 'AR2i16/Phase_function_err_tpfformap.dat'  
    filename8 = 'AR3i16/Phase_function_err_tpfformap.dat'  
    filename9 = 'AR4i16/Phase_function_err_tpfformap.dat'  
    filename10 = 'AR5i16/Phase_function_err_tpfformap.dat'  
    filename11 = 'AR6i16/Phase_function_err_tpfformap.dat'  
      
    filenames = [filename1, filename2, filename3, filename4, filename5, filename6\
    ,filename7, filename8, filename9, filename10, filename11]
    
    for filename in filenames:
      if not os.path.isfile(filename):
          print("Error: File does not exist.")
          return
    
    print(f"Reading data from {filename1}")
    data1 = np.loadtxt(filename1)
    nb_tpf = data1[2:, 0]
    err1 = data1[2:, d]
    print(f"Reading data from {filename2}")
    data2 = np.loadtxt(filename2)    
    err2 = data2[2:, d]
    print(f"Reading data from {filename3}")
    data3 = np.loadtxt(filename3)    
    err3 = data3[2:, d]
    print(f"Reading data from {filename4}")
    data4 = np.loadtxt(filename4)    
    err4 = data4[2:, d]
    print(f"Reading data from {filename5}")
    data5 = np.loadtxt(filename5)    
    err5 = data5[2:, d]
    print(f"Reading data from {filename6}")
    data6 = np.loadtxt(filename6)    
    err6 = data6[2:, d]
    print(f"Reading data from {filename2}")
    data7 = np.loadtxt(filename7)    
    err7 = data7[2:, d]
    print(f"Reading data from {filename3}")
    data8 = np.loadtxt(filename8)    
    err8 = data8[2:, d]
    print(f"Reading data from {filename4}")
    data9 = np.loadtxt(filename9)    
    err9 = data9[2:, d]
    print(f"Reading data from {filename5}")
    data10 = np.loadtxt(filename10)    
    err10 = data10[2:, d]
    print(f"Reading data from {filename6}")
    data11 = np.loadtxt(filename11)    
    err11 = data11[2:, d]
    
    if split==True :
    	fig, ax = plt.subplots(1,2)
    	ax[0].plot(nb_tpf, err1, '-o', label='AR=1')
    	ax[0].plot(nb_tpf, err2, '-o', label='AR=2')
    	ax[0].plot(nb_tpf, err3, '-o', label='AR=3')
    	ax[0].plot(nb_tpf, err4, '-o', label='AR=4')
    	ax[0].plot(nb_tpf, err5, '-o', label='AR=5')
    	ax[0].plot(nb_tpf, err6, '-o', label='AR=6')
    	ax[0].set_xlabel('tpf')
    	ax[0].set_ylabel('relative error')
#    ax[0].set_title('convergence study on number of fictif points (D/16)')
    	ax[0].legend()
    	ax[1].plot(nb_tpf, err7, '-o', label='AR=1/2')
    	ax[1].plot(nb_tpf, err8, '-o', label='AR=1/3')
    	ax[1].plot(nb_tpf, err9, '-o', label='AR=1/4')
    	ax[1].plot(nb_tpf, err10, '-o', label='AR=1/5')
    	ax[1].plot(nb_tpf, err11, '-o', label='AR=1/6')
    	ax[1].set_xlabel('tpf')
    	ax[1].set_ylabel('relative error')
#    ax[1].set_title('convergence study on number of fictif points (D/80)')
    	ax[1].legend()
    
    	fig.subplots_adjust(wspace=0.25)
    	plt.show()
	
    else:	
    	fig, ax = plt.subplots()
    	ax.plot(nb_tpf, err1, 'b-o', label='AR=1')
    	ax.plot(nb_tpf, err2, '-o', label='AR=2')
    	ax.plot(nb_tpf, err3, '-o', label='AR=3')
    	ax.plot(nb_tpf, err4, '-o', label='AR=4')
    	ax.plot(nb_tpf, err5, '-o', label='AR=5')
    	ax.plot(nb_tpf, err6, '-o', label='AR=6')
    	ax.plot(nb_tpf, err7, '-o', label='AR=1/2')
    	ax.plot(nb_tpf, err8, '-o', label='AR=1/3')
    	ax.plot(nb_tpf, err9, '-o', label='AR=1/4')
    	ax.plot(nb_tpf, err10, '-o', label='AR=1/5')
    	ax.plot(nb_tpf, err11, '-o', label='AR=1/6')
    	ax.set_xlabel('tpf',fontsize=14)
    	ax.set_ylabel('relative error',fontsize=14)
    	ax.set_title('convergence study on number of fictif points (D/16 - full cells)',fontsize=14)
    	ax.legend()
    	plt.show()

####    
#Convergence_plot(1)
Convergence_nplot(1,False)
