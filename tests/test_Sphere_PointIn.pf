!===============================================================================
module test_Sphere_PointIn
      !===============================================================================
      use test_Particle_Initialisation, only : t_Particle_Initialisation
      use Bib_VOFLag_Particle_PointIn
      use pfunit
      implicit none

      contains
      !> @author Andelhamid Hakkoum 11/2022
      ! 
      !> @brief 
      !        
      !        
      !
      !
      !> @todo
      !    
      !
      !-----------------------------------------------------------------------------
      !@suite(name='test_projectile_motion_mod_suite')
      @mpitest(npes=[1])
      subroutine t_Sphere_PointIn(this)
      !===============================================================================
      !modules
      !===============================================================================
      !-------------------------------------------------------------------------------
      !Modules de definition des structures et variables
      !-------------------------------------------------------------------------------
      use Module_VOFLag_ParticlesDataStructure
      use Module_VOFLag_SubDomain_Data
      use mod_Parameters,                       only : rank,nproc,dim,deeptracking
      !-------------------------------------------------------------------------------
      
      !===============================================================================
      !declarations des variables
      !===============================================================================
      !implicit none
      !-------------------------------------------------------------------------------
      !variables globales
      !-------------------------------------------------------------------------------
      class (MpiTestMethod), intent(inout)   :: this
      !-------------------------------------------------------------------------------
      !variables locales 
      !-------------------------------------------------------------------------------
      type(struct_vof_lag)               :: vl
      !-------------------------------------------------------------------------------
      real(8), dimension(3)              :: p
      integer                            :: ndim ,np, a, i
      logical                            :: in_particle
      !-------------------------------------------------------------------------------
      ndim = 3 
      call t_Particle_Initialisation(vl,1,[1])

      do np=1, vl%kpt
         p(1) = 0d0; p(2) =  0.5d0; p(3) =  0.0d0
         call Particle_PointIn(vl,np,1,vl%objet(np)%sca,p,ndim,in_particle)
         a = 0
         if (in_particle) a = 1
         @assertEqualUserDefined(a,1)
         p(1) = 0d0; p(2) = -0.25d0; p(3) = -0.25d0
         call Particle_PointIn(vl,np,1,vl%objet(np)%sca,p,ndim,in_particle)
         a = 0
         if (in_particle) a = 1
         @assertEqualUserDefined(a,1)
         p(1) = 0.1d0; p(2) = 0.0d0; p(3) = 0.1d0
         call Particle_PointIn(vl,np,1,vl%objet(np)%sca,p,ndim,in_particle)
         a = 0
         if (in_particle) a = 1
         @assertEqualUserDefined(a,1)
         p(1) = -0.75d0; p(2) = 0.0d0; p(3) = 0.6d0
         call Particle_PointIn(vl,np,1,vl%objet(np)%sca,p,ndim,in_particle)
         a = 0
         if (in_particle) a = 1
         @assertEqualUserDefined(a,1)
         p(1) = -1d0; p(2) = 0.0d0; p(3) = 0.0d0
         call Particle_PointIn(vl,np,1,vl%objet(np)%sca,p,ndim,in_particle)
         a = 0
         if (.not.in_particle) a = 1
         @assertEqualUserDefined(a,1)
         p(1) = -1d0; p(2) = -1.0d0; p(3) = -1.0d0
         call Particle_PointIn(vl,np,1,vl%objet(np)%sca,p,ndim,in_particle)
         a = 0
         if (.not.in_particle) a = 1
         @assertEqualUserDefined(a,1)
         p(1) = 1d0; p(2) = 1.0d0  
         call Particle_PointIn(vl,np,1,vl%objet(np)%sca,p,ndim,in_particle)
         a = 0
         if (.not.in_particle) a = 1
         @assertEqualUserDefined(a,1)
         p(1) = 10.5d0; p(2) = 0.0d0  
         call Particle_PointIn(vl,np,1,vl%objet(np)%sca,p,ndim,in_particle)
         a = 0
         if (.not.in_particle) a = 1
         @assertEqualUserDefined(a,1)
         do i=1, 1000
            call random_number(p)
            p = p/(p(1)*p(1)+p(2)*p(2)+p(3)*p(3))**0.5

            if ( (p(1)*p(1)+p(2)*p(2)+p(3)*p(3)) /= 0d0)  goto 102
            call Particle_PointIn(vl,np,1,vl%objet(np)%sca,p,ndim,in_particle)
            a = 0
            if (.not.in_particle) a = 1
            @assertEqualUserDefined(a,1)
            102 continue 
         end do 

         do i=1, 1000
            call random_number(p)
            if ( (p(1)*p(1)+p(2)*p(2)+p(3)*p(3)) > 1d0)  goto 103
            call Particle_PointIn(vl,np,1,vl%objet(np)%sca,p,ndim,in_particle)
            a = 0
            if (in_particle) a = 1
            @assertEqualUserDefined(a,1)
            103 continue 
         end do 
      end do

      
      end subroutine t_Sphere_PointIn
    !===============================================================================
        
end module test_Sphere_PointIn
!===============================================================================
 
