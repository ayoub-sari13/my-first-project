!===============================================================================
module Bib_VOFLag_Ellipsoid_LagrangianVelocity
  !===============================================================================
  contains
  !-----------------------------------------------------------------------------  
  !> @author mohamed-amine chadil
  ! 
  !> @brief cette routine permet 
  !
  !> @param[in]  vts             : vitesse au temps n+1.
  !> @param[out] vl              : la structure contenant toutes les informations
  !! relatives aux particules.
  !-----------------------------------------------------------------------------
    subroutine Ellipsoid_LagrangianVelocity(u,v,w,vl,ndim,grid_x,grid_y,&
                                   grid_z,grid_xu,grid_yv,grid_zw,dx,dy,dz,dxu,dyv,dzw)  
    !LagrangianVelocityComputation_Ellipsoid
      !===============================================================================
      !modules
      !===============================================================================
      !-------------------------------------------------------------------------------
      !Modules de definition des structures et variables => src/mod/module_...f90
      !-------------------------------------------------------------------------------
      use Module_VOFLag_ParticlesDataStructure
      use Module_VOFLag_SubDomain_Data,        only : m_period
      use mod_Parameters,                      only : deeptracking,xmin,xmax,ymin,&
                                                      ymax,zmin,zmax
      use mod_mpi

      !-------------------------------------------------------------------------------
      !Modules de subroutines de la librairie imft => src/bib_imft_...f90
      !-------------------------------------------------------------------------------
      use Bib_VOFLag_Vector_FromInertial2ParticleFrame
      use Bib_VOFLag_Vector_FromComoving2ParticleFrame
      use Bib_VOFLag_SubDomain_Limited_PointIn
      use Bib_VOFLag_VelocityInterpolationFromEulerianMesh2Point
      use Bib_VOFLag_Matrix3_Inverse

      !-------------------------------------------------------------------------------
      !===============================================================================
      !declarations des variables
      !===============================================================================
      implicit none
      !-------------------------------------------------------------------------------
      !variables globales
      !-------------------------------------------------------------------------------
      type(struct_vof_lag),                   intent(inout) :: vl
      real(8), dimension(:,:,:), allocatable, intent(in)    :: u,v,w
      integer,                                intent(in)    :: ndim

      !-------------------------------------------------------------------------------
      !variables locales 
      !-------------------------------------------------------------------------------
      real(8), dimension(2*ndim,3,vl%kpt)             :: vts_all_points
      real(8), dimension(2*ndim,3)                    :: point, point_co
      real(8), dimension(3)                           :: vts_point,point_part_ellipsoid_frame,&
                                                         coord_deb,coord_fin, b
      real(8), dimension(3,3)                         :: Coef, Coef_inv					       
      real(8)                                         :: vals      
      integer                                         :: nd,np,i
      logical                                         :: point_in_domain_int
      real(8), dimension(:), allocatable, intent(in)  :: grid_xu,grid_yv, grid_zw
      real(8), dimension(:), allocatable, intent(in)  :: grid_x,grid_y, grid_z
      real(8), dimension(:), allocatable, intent(in)  :: dx,dy,dz
      real(8), dimension(:), allocatable, intent(in)  :: dxu,dyv,dzw
      
      !-------------------------------------------------------------------------------

      !-------------------------------------------------------------------------------
      if (deeptracking) write(*,*) 'entree Ellipsoid_LagrangianVelocity'
      !-------------------------------------------------------------------------------
      !-------------------------------------------------------------------------------
      ! Initialisation
      !-------------------------------------------------------------------------------
      coord_deb(1)=xmin
      coord_deb(2)=ymin
      coord_fin(1)=xmax
      coord_fin(2)=ymax
      if (ndim.eq.3) then
         coord_deb(3)=zmin
         coord_fin(3)=zmax
      endif

      !-------------------------------------------------------------------------------
      !!Calcul des vitesses des points fictifs a l'interieur des ellipsoides
      !-------------------------------------------------------------------------------
      vts_all_points = 0.0d0
      do np=1,vl%kpt
         if (vl%objet(np)%on) then
            point = 0.0d0
            do nd = 1,ndim
               do i = 0,1
                  !-------------------------------------------------------------------------------
                  !Construction des points d'interpolation
                  !-------------------------------------------------------------------------------
                  point_part_ellipsoid_frame = 0.0d0
                  point_part_ellipsoid_frame(nd) = 0.5d0*(2*i-1)*vl%objet(np)%sca(nd)

                  !-------------------------------------------------------------------------------
                  !Ecrire les pts dans le repere globale
                  !-------------------------------------------------------------------------------
                  call Vector_FromInertial2ParticleFrame(vl,np,1,ndim,.false.,point_part_ellipsoid_frame,point(2*nd+i-1,:))

                  !-------------------------------------------------------------------------------
                  !Ecrire les pts dans le repere en translation (Comoving)
                  !-------------------------------------------------------------------------------
                  call Vector_FromComoving2ParticleFrame(vl,np,ndim,.false.,point_part_ellipsoid_frame,point_co(2*nd+i-1,:))
	       end do
            end do
            do i = 1,2*ndim
               do nd = 1,ndim
                  !-------------------------------------------------------------------------------
                  !Gestion de la periodicite
                  !-------------------------------------------------------------------------------
                  if (m_period(nd)) then
                     if ( (  point(i,nd) .gt. coord_fin(nd) ) ) then  
                         point(i,nd) =  point(i,nd) - (coord_fin(nd) - coord_deb(nd) )
                     else if (  point(i,nd) .lt. coord_deb(nd)) then
                        point(i,nd) =   point(i,nd) + (coord_fin(nd) - coord_deb(nd) )
                     end if
                  end if
               end do
               !-------------------------------------------------------------------------------
               !Interpolation de la vitesse
               !-------------------------------------------------------------------------------
               vts_point = 0.0d0 
               call SubDomain_Limited_PointIn(point(i,:),point_in_domain_int)
               if (point_in_domain_int) then
                  call  VelocityInterpolationFromEulerianMesh2Point(point(i,:),u,v,w,vts_point,ndim,grid_x,&
	          grid_y,grid_z,grid_xu,grid_yv,grid_zw,dx,dy,dz,dxu,dyv,dzw)
                  vts_all_points(i,1:ndim,np) =  vts_point(1:ndim)
                  print*, "vitesse d'interpol lagrangienne_routine", vts_point, char(10)
	       end if
            end do
         end if

         !do nd=1,ndim
         !   do i = 1,2*ndim
         !      vals=vts_all_points(i,nd,np)
         !      call mpi_allreduce(vals,vts_all_points(i,nd,np),1,mpi_double_precision,mpi_sum,comm3d,code)  
         !   end do
         !end do
      end do

      !-------------------------------------------------------------------------------
      !Actualisation des vitesses de translation: vn et v
      !-------------------------------------------------------------------------------
      if (vl%deplacement) then 
         do np=1,vl%kpt
            do nd=1,ndim
               !-------------------------------------------------------------------------------
               !Initialisation des vitesses et des postions des iterations n;n+1
               !-------------------------------------------------------------------------------
               vl%objet(np)%vn(nd)   =vl%objet(np)%v(nd)
               vl%objet(np)%v(nd)    =0.0d0

               !-------------------------------------------------------------------------------
               !Calcul de la vitesse de la part a l'instant n+1
               !-------------------------------------------------------------------------------
               do i = 1,2*ndim 
                  vl%objet(np)%v(nd) =  vl%objet(np)%v(nd) + vts_all_points(i,nd,np) / ( 2 * ndim+1D-40 )
                 ! print*, vl%objet(np)%v(nd)
	       end do
            end do
         end do
      end if

      if (vl%vrot) then
         !-------------------------------------------------------------------------------
         !calcul de la vitesse angulaire
         !-------------------------------------------------------------------------------
         do np=1,vl%kpt
            vl%objet(np)%omeg = 0.0d0
         end do
	 Coef =0d0
	 b = 0d0
         do np=1,vl%kpt
            if (vl%objet(np)%on) then ! a voir,les angles de rotations qui n'annullent pas le denominateur
	    
	      !print*, 'denominator ',point_co(4,2)-point_co(3,2)+point_co(2,1)-point_co(1,1)
	      vl%objet(np)%omeg(3) = (vts_all_points(3,1,np)-vts_all_points(4,1,np)-&
	      (vts_all_points(1,2,np)-vts_all_points(2,2,np)))/(point_co(4,2)-point_co(3,2)+point_co(2,1)-point_co(1,1)+1D-40)  
	      !print*, 'omega_z ',vl%objet(np)%omeg(3) 
               if (ndim .eq. 3) then
              	Coef(1,1)= (point_co(4,2)-point_co(3,2) +point_co(6,3) -point_co(5,3))
	      	Coef(1,2)= -(point_co(4,1) -point_co(3,1))
	      	Coef(1,3)= -(point_co(6,1) -point_co(5,1))
              	Coef(2,1)= -(point_co(2,2) -point_co(1,2))
	      	Coef(2,2)= (point_co(6,3) -point_co(5,3) +point_co(2,1) -point_co(1,1))
	      	Coef(2,3)= -(point_co(6,2) -point_co(5,2))
              	Coef(3,1)= -(point_co(2,3) -point_co(1,3))
	      	Coef(3,2)= -(point_co(4,3) -point_co(3,3))
	      	Coef(3,3)= (point_co(4,2)-point_co(3,2) +point_co(2,1) -point_co(1,1))
               ! print*, 'A=', Coef
	      	b(1)= vts_all_points(4,3,np)-vts_all_points(3,3,np) - (vts_all_points(6,2,np)-vts_all_points(5,2,np))
		b(2)= vts_all_points(6,1,np)-vts_all_points(5,1,np) - (vts_all_points(2,3,np)-vts_all_points(1,3,np))
		b(3)= vts_all_points(3,1,np)-vts_all_points(4,1,np) - (vts_all_points(1,2,np)-vts_all_points(2,2,np))
               ! print*, 'b=', b
               !-------------------------------------------------------------------------------
               !Inverser la matrice de coefficients
               !-------------------------------------------------------------------------------		
	        call Matrix3_Inverse(Coef,Coef_inv)
	       ! print*, 'A_inv=', Coef_inv
                vl%objet(np)%omeg(1) = Coef_inv(1,1)*b(1) + Coef_inv(1,2)*b(2) + Coef_inv(1,3)*b(3) 
		vl%objet(np)%omeg(2) = Coef_inv(2,1)*b(1) + Coef_inv(2,2)*b(2) + Coef_inv(2,3)*b(3)
		vl%objet(np)%omeg(3) = Coef_inv(3,1)*b(1) + Coef_inv(3,2)*b(2) + Coef_inv(3,3)*b(3)		
               ! print*, 'omega final', vl%objet(np)%omeg
	       end if
            end if
         end do

         !-------------------------------------------------------------------------------
         !diffuser la vitesse angulaire a tous les procs
         !-------------------------------------------------------------------------------
         !do np=1,vl%kpt
         !   do nd=1,3
         !      call mpi_bcast(vl%objet(np)%omeg(nd),1,mpi_double_precision,vl%objet(np)%locpart,comm3d,code)  
         !   end do
         !end do
      end if

      !-------------------------------------------------------------------------------
      if (deeptracking) write(*,*) 'sortie Ellipsoid_LagrangianVelocity'
      !-------------------------------------------------------------------------------
    end subroutine Ellipsoid_LagrangianVelocity

  !===============================================================================
end module Bib_VOFLag_Ellipsoid_LagrangianVelocity
!===============================================================================
  


